---
title: Let's talk about a few tools for finding out about Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DIFttANs77U) |
| Published | 2022/03/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the popularity of open source intelligence in providing updates on Ukraine.
- Addresses the interest in geolocation and how it reveals risky information.
- Talks about tracking military planes near borders and how easily accessible this information is.
- Mentions NASA's FIRMS system tracking fires and its applications in monitoring conflict zones.
- Encourages using these tools to observe without causing harm or spending money.
- Emphasizes the value of freely available information in understanding global events.
- Leaves room for diving into more topics based on specific interests and questions from the audience.

### Quotes

- "They're not making that stuff up."
- "You'll be told what type of aircraft it is."
- "You can get a good picture without having to spend any money on those."

### Oneliner

Beau explains open-source intelligence tools for tracking military activities and fires without risking lives or money.

### Audience

Tech enthusiasts, researchers, analysts

### On-the-ground actions from transcript

- Use open-source intelligence tools to monitor and understand global events (implied)

### Whats missing in summary

Demonstration of the power of freely available information in analyzing global events.

### Tags

#OpenSourceIntelligence #Geolocation #MilitaryPlanes #FIRMS #NASA #GlobalEvents


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk a little bit
about open source stuff because I had some questions come in.
I guess I used that term in a video recently
and people said, hey, I want to know how to do this.
And if you have no idea what I'm talking about,
all over social media right now there
are people who are pretty well versed in open source
intelligence and they are providing updates
on what's going on in Ukraine.
I actually sent messages back trying
to find out specifically what people wanted to know
and none of you all responded.
So I'm guessing here.
There are like three things that are super popular to look at
right now.
One of them I'm not really going to go in depth on
because it can have unforeseen consequences.
But in lieu of that, I will give you
something else that I recently found out
about which is really cool.
So I think most people would want
to know about the geolocation stuff
and how they're looking at a photo and being like,
OK, this is here at this time.
Just know there are ways to do it.
They're not making that stuff up.
The problem with it is you're literally
giving away somebody's position when you do that.
Most of them, I would imagine, I know
the accounts that I follow do not
release the location of troops until they have moved.
If you release that information too soon,
you can get somebody hurt.
So we're not really going to go over that.
But another one that people are really interested in
is how they know where certain planes are
and how they can say, yeah, there's a US spy plane flying
right now.
That's way easier than you might think.
The screenshots that they're sharing, that's not hard to do.
I can tell you right now at time of filming,
there is a US Army Chinook surprisingly close
to the Ukrainian border right now.
There is a plane from the Netherlands up,
one from Italy, and a couple Polish jets,
all relatively close to the area in question.
Easiest way to look for this is to go to planefinder.net.
And there's little icons of planes flying over a map.
You tap the plane, and poof, there you go.
You'll be told what type of aircraft it is.
Now, one of the obvious questions
is why is this information available like this?
It seems like it shouldn't be.
Right now, a lot of military aircraft, especially
near borders, are lighting themselves up.
They're sending out a signal to identify themselves and say,
hey, I'm a military aircraft.
And this is to prevent, let's say,
a Russian MiG flying really close to the Polish border.
It might turn its transponder on,
turn its signal on to say, hey, F-16,
please don't shoot me down and start World War III
by accident.
Now, they're also doing this with ships along the coast.
You can do that yourself at marinetraffic.com.
Same basic principle.
But a cool one, and I think it's cool maybe just because I
recently found out about it, is called FIRMS, Fire Information
for Resource Management System.
It's NASA.
NASA has a satellite that tracks fires.
I never would have thought about this.
So this satellite identifies where fires occur.
So you can pretty easily locate where missiles have hit,
where there was a tank battle, where
there was damage to, say, an oil depot that has caught fire.
You can kind of keep track and see where the fighting is
occurring this way.
And when you're talking about the more static battles
like around the cities where the Russians aren't getting
much movement, you can see where they're trying to move.
This is also probably a useful tool
for being prepared for events in the United States
as far as wildfires and stuff like that.
Or if you're somebody who is tracking deforestation,
this is also probably a pretty good tool.
Those are methods that you can use
to kind of observe what's going on that don't run any risk
of getting anybody hurt.
So take a look at those.
All three of the sites I gave, they're all free to use.
I'm sure they probably have some kind of pay upgrade
here or there.
But you can get a good picture without having
to spend any money on those.
At some point, we might go into some other stuff later.
But until I know specifically what people are talking about,
there's a brief overview.
And you would be surprised at the information you can get
and how much intent you can discern
by the information that's freely available like this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}