---
title: Let's talk about why Lindsey Graham is on Russian TV....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VXQ_h9y4HxY) |
| Published | 2022/03/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Lindsey Graham made statements calling for the oligarchs in Russia to overthrow Putin, potentially causing an international incident.
- Graham's remarks could rally support behind Putin, convince Russia of an existential battle, and be used for propaganda purposes within Russia.
- The senator's call for Putin's removal may have unintended consequences, such as impacting the security of Ukraine's President Zelensky.
- Graham's posturing to look tough domestically lacks consideration for the international implications of his statements.
- The senator's suggestion to Russian oligarchs to act against Putin is unnecessary, as they do not need his advice and are capable decision-makers.
- The inflammatory remarks by Graham could escalate tensions and hinder diplomatic efforts in a conflict with a major power like Russia.
- Graham's actions, driven by a quest to appear strong, have real impacts beyond domestic politics.
- The habit of making provocative statements for attention needs to be curbed, especially given the heightened stakes in international relations.
- There are legal and ethical considerations for U.S. government officials when discussing foreign leaders, particularly in times of heightened tensions.
- Graham's call for Putin's removal could unintentionally strengthen Russian propaganda and influence the conflict dynamics.

### Quotes

- "The habit that arose under Trump of saying the most inflammatory stuff possible in order to get views, retweets, or whatever, oh yeah, on top of saying it live, he also tweeted out the same stuff."
- "Some things can't be said by certain people."
- "It made it harder for the oligarchs to act if they chose to."
- "There are a whole lot of people in the U.S. government that need to learn that they need to be quiet."
- "He did it to get on TV and seem tough."

### Oneliner

Senator Lindsey Graham's call for Russian oligarchs to overthrow Putin risks escalating tensions, strengthening Russian propaganda, and impacting international relations, driven by a quest for toughness.

### Audience

U.S. Government Officials

### On-the-ground actions from transcript

- Refrain from making inflammatory statements that could escalate international tensions (implied).
- Recognize the legal and ethical boundaries when discussing foreign leaders, especially during times of heightened tensions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of Senator Graham's remarks on international relations and propaganda efforts, urging caution and responsibility in public statements.

### Tags

#SenatorLindseyGraham #InternationalRelations #Propaganda #InflammatoryStatements #Ethics


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Lindsey Graham, the senator from South Carolina, who
made some statements last night that are certain to draw the attention of the international
community and are certain to be replayed over and over and over again in one country in
particular.
The senator got on national TV last night and basically called for the oligarchs in
Russia to Julius Caesar Putin.
And he was really clear about it.
So right now as you're watching this, people in Russia are undoubtedly watching a US official
call for the removal of their head of state.
This is something that could possibly rally support behind him.
It could convince Russia that they are actually in an existential battle for their country.
Something that isn't actually occurring.
The senator, knowingly or not, produced some footage that will be used for propaganda purposes
within Russia.
That's what he did.
Why?
Why would he make this claim?
Why would he say this?
To look tough.
It's posturing here at home.
And he doesn't have any apparent understanding of the impacts this is going to have overseas.
You know, it's hard for the West to condemn Putin sending a team after Zelensky when we
have a senator in the United States asking for that to happen over there.
Russia will undoubtedly double their efforts to take out Zelensky now.
On top of this, what was it supposed to accomplish?
He was addressing the oligarchs, Putin's inner circle, and asking them to do it.
Because the oligarchs need Lindsey Graham, the great senator from South Carolina, they
need him to tell them when it's time to do this?
I don't think so.
On top of this, the fact that they absolutely do not need his advice.
They're pretty sharp.
They wouldn't have survived long enough to be at the top if they weren't.
So aside from it being completely unneeded there, it causing an international incident,
and it probably putting Zelensky in jeopardy, there's also the other side to this.
There's a pretty good chance that some of the oligarchs were already thinking about
this.
But see, now Putin has footage of a U.S. government official calling for it.
What do you think is going to happen to his security?
It's probably going to get a little bit tighter, right?
Because now they're not concerned about the possibility of maybe one of the oligarchs
losing their taste for this.
They're concerned about a U.S. operation.
So they're going on high alert.
Lindsey Graham made it a whole lot harder for that to happen.
And he did it for votes.
He did it to get on TV and seem tough.
This here from leading officials, they have impacts.
This isn't a conflict with a minor power.
This is a conflict with a major power.
The habit that arose under Trump of saying the most inflammatory stuff possible in order
to get views, retweets, or whatever, oh yeah, on top of saying it live, he also tweeted
out the same stuff.
That little habit, it needs to stop.
Stakes are a whole lot higher now.
This is going to be Russian propaganda.
It is going to be on TV.
I assure you, as you're watching this, it's on TV over there.
There are a whole lot of people in the U.S. government that need to learn that they need
to be quiet.
There are...
I actually believe there are laws about this.
I'm pretty sure he's not actually legally allowed to even suggest what he suggested.
And again, it doesn't even matter if you agree with what he said.
Some things can't be said by certain people.
If you're a U.S. government official and there are heightened tensions, generally speaking,
it is totally uncool to suggest the removal of a foreign head of state.
And there's a whole bunch of reasons why.
It made it harder for the oligarchs to act if they chose to.
They certainly didn't need Lindsey Graham to tell them to do it.
And he produced propaganda that can be used to further the war effort for them.
That's what he actually accomplished in his quest to look tough.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}