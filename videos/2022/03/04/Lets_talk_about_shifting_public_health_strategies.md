---
title: Let's talk about shifting public health strategies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9PQnDXOKGO4) |
| Published | 2022/03/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The federal government is altering public health strategies in the United States, releasing new conditional guidance without clear explanations.
- The shift in strategies is motivated by increased vaccination rates and the perception of a less severe strain circulating.
- The new public health strategy consists of four main components: vaccination, masking, and testing/treatment.
- Vaccination guidelines remain consistent: go get vaccinated.
- Most Americans are now advised they can go without masks, but it depends on the risk level in their area (green, yellow, orange).
- Testing and treatment are emphasized together, with options like covidtest.gov and one-stop shops for testing and antiviral treatment.
- The goal of the new strategy is to relax public health measures while remaining prepared to reintroduce them if needed to prevent fatigue.
- Beau personally plans to continue following public health guidelines: staying vaccinated, wearing masks, and getting tested if sick.
- He stresses the importance of social responsibility in maintaining lower levels of COVID-19 transmission.

### Quotes

- "Go get vaccinated."
- "They're saying most Americans can go without a mask."
- "I'll continue wearing a mask."
- "Wash your hands, wear a mask, get vaccinated, kind of keep your distance."
- "The idea here is we're at the point where hopefully social responsibility kicks in."

### Oneliner

The federal government is shifting public health strategies in the US, focusing on vaccination, masking, testing, and treatment, aiming to relax measures while staying prepared for future adjustments and relying on social responsibility.

### Audience

US Residents

### On-the-ground actions from transcript

- Stay updated on the latest public health guidance and follow recommendations (exemplified)
- Get vaccinated if eligible and encourage others to do the same (exemplified)
- Wear masks in areas with higher risk levels and around vulnerable populations (exemplified)
- Get tested if experiencing symptoms or potentially exposed to COVID-19 (exemplified)

### Whats missing in summary

The full transcript provides more context on the rationale behind the shifting public health strategies and Beau's personal commitment to following guidelines for vaccination, masking, and testing.

### Tags

#PublicHealth #COVID19 #Vaccination #Masking #Testing #SocialResponsibility


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about shifting public health
strategies in the United States.
The federal government is altering its strategy,
and they're releasing new guidance for everybody,
things that they're suggesting you do.
It's led to questions primarily because a lot of it
is conditional.
It's if this, then this type of stuff.
And they're not really doing a great job
of explaining the conditions.
They've got that talking point going that, hey,
things are getting better.
But they're doing, in my opinion,
a poor job of disseminating information.
So why are the strategies changing?
The general idea behind it is that because of vaccination
and people just getting it, there's
more protection out there in the general public.
So that, combined with the idea that this
is a less severe strain, has motivated this shift.
That's what appears to be driving it.
The new strategy has four main components,
but the last two just kind of go together.
So it's really three.
You have vaccination, masking, and then testing and treatment.
Those last two, those go together.
So what do the vaccination guidelines say?
Go get vaccinated.
That really hasn't changed.
Go get vaccinated.
That's the guidance.
Masking, it's coming out.
What they're saying is most Americans can go without a mask.
Well, who are most Americans?
There's a map, and counties are listed
by green, yellow, and orange.
I think it might be red.
I think it's green, yellow, and orange, though.
And this shows how much risk the hospitals in your area are at.
If you're in green, hey, you can go without a mask
if you want to.
If you're in yellow, they're advising more caution,
particularly of those who are at risk.
They're aging, or they have some other factor that
puts them at higher risk.
And then you have orange, which means you should wear a mask.
Then you have testing and treatment.
And we're doing these together because they really
do go together.
So you have covidtest.gov.
You can call or go online.
You can go online, and they'll send you tests,
even if you got them in that last cycle.
Then there's also going to be hundreds of one-stop shops
opening up, where you go in, you get tested, you pop positive.
Hey, here's your antivirals.
And they're trying to mitigate it early.
It's a good plan.
So that's really the shifting here.
So they're trying to get us to the phase
where people have relaxed a little bit.
One thing I do want to point out is
that when you listen to the people making the decision,
when you listen to them talk about it,
they're not saying that this is over.
They're saying, we're relaxing.
So if we need to, we can pull these tools out again later.
They're trying to prevent fatigue.
I have had people ask me what I'm going to do.
And for me personally, as far as vaccination goes, yeah,
I'm vaccinated.
I will stay vaccinated.
Every once in a while, I get one of those messages or questions
that comes in that's, well, how many boosters
are you going to get?
I don't know.
I don't keep track.
Do you know how many tetanus shots I've had?
So to me, that's never been a big deal.
Yeah, I will stay vaccinated.
The next one is masking.
I live in Florida.
As far as I know, I think only one county is green.
We've very much been Florida manning this up.
I'll stay masking.
I will stay wearing a mask, not just because we're
in yellow and red everywhere or yellow and orange.
I live in an area with an aging population.
So even though it may not be super risky for me,
I could give it to somebody else.
So I'll continue wearing a mask.
It's never bothered me to wear one.
I'm one of those people who will be driving in the car
by themselves wearing a mask.
It's not because I'm paranoid of getting it in the car.
It's because it doesn't bother me.
So I'll continue masking.
As far as testing and treatment, I guess if I get sick,
I'll go get tested.
I don't know what people want to hear on that one.
So at the end of the day, I don't
plan on altering my behavior much based on this.
So it's the same stuff.
Wash your hands, wear a mask, get vaccinated,
kind of keep your distance.
The idea here is we're at the point
where hopefully social responsibility kicks in
and we keep things at a lower level.
That's the general idea.
We'll find out how successful this strategy is
going to be.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}