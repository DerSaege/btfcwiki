---
title: Let's talk about mapping the unthinkable....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NhPa1t7M_4E) |
| Published | 2022/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares insights on a recent poll from the AP on American perceptions regarding a potential nuclear threat within the country.
- Notes that 80% of Americans expressed concern about the possibility of a nuclear event occurring domestically.
- Emphasizes the importance of knowledge in reducing fear and introduces a tool to understand nuclear impacts better.
- Recommends using nukesecrecy.com/nukemap to visualize nuclear scenarios, including location, yield, and fallout rings.
- Mentions that maps typically depict a 2,000 warhead exchange, but suggests focusing on a 500 warhead scenario for a clearer picture.
- Describes the presets for Russian warheads ranging from 300 to 800 kilotons, with larger devices included as novelty items.
- Encourages individuals to gather information on nuclear threats but cautions against letting fear dominate their lives.
- Acknowledges historical proximity to nuclear disasters and the subtle nature of these risks.
- Urges viewers to seek information and use tools like the mentioned map to understand their proximity to potential nuclear events.
- Concludes by suggesting that while concerns are valid, excessive fear might not be warranted given the low probability of a nuclear incident.

### Quotes

- "Knowledge hinders fear."
- "There's no reason to let this fear rule your life."
- "The numbers in that poll were really high, so maybe a little bit of information might help calm people's nerves."

### Oneliner

Beau addresses American concerns over a potential domestic nuclear threat, advocating for knowledge over fear and showcasing a tool to understand nuclear impacts better.

### Audience

General Public

### On-the-ground actions from transcript

- Visit nukesecrecy.com/nukemap to understand potential nuclear scenarios (suggested).
- Gather information on nuclear threats from reliable sources (suggested).

### Whats missing in summary

The full transcript provides detailed insights on American perceptions of a domestic nuclear threat, urging individuals to seek knowledge to alleviate fear and offering a tool for visualizing potential nuclear scenarios.

### Tags

#NuclearThreat #AmericanPerceptions #KnowledgeOverFear #Nukemap #RiskAssessment


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today, we are going to talk about a poll
that came out from the AP, and American perception,
and we're gonna talk about a tool that may or may not
help alleviate some concern.
A poll came out from the AP said that 80% of Americans
We're concerned about the possibility of the unthinkable happening here, not in Ukraine,
not Russia using it over there, but it occurring here.
And I want to say it was 50%.
We're very concerned about this.
I'm one of those people who believes that knowledge hinders fear.
Little bit of knowledge can make you feel better.
Now with this particular topic, when you're talking about nukes, the fear isn't irrational,
but it might help to understand it.
So there's a tool you can use.
It's at nukesecrecy.com slash nukemap, N-U-K-E, map, all one word.
And this tool allows you to set the location, the yield, whether it's surface or air burst,
all of that.
And it allows you to get a map that would show you the rings that we have talked about
in other videos, the fallout based on probable wind, and all of that stuff.
And it helps you get a clear picture of what it would look like.
you can go online and find maps that show probable targets.
Now, it is worth noting that most of the maps I've seen
are based on a 2,000 warhead exchange, which, just so you
know, that's the high end.
There are also maps, I think, for 500,
which is probably what you should look at to get
a clear picture.
because at 2000, even if you're not in a red zone on the map
from Nukes secrecy, you're going to wish you were.
Most Russian warheads are 300 to 800 kilotons.
And there are presets for those.
There are much larger devices that it
has presets for.
It's important to note that those are novelty items.
Those aren't expected to be used.
That's more of, let's see if we can rather than,
should we really build this?
The range you're looking at is 300 to 800 kilotons.
And you can pull it up.
I've looked.
Before I realized where the map came from,
I actually started to fact check it.
And everything I saw initially was right.
And then I realized where the map came from.
And I mean, it's kind of like a sixth grader fact checking
Stephen Hawking's work.
I'm sure it's right.
So I would take a look at that.
There may be a lot of people who are concerned that really
don't have a need to be about the lower
end of an exchange, I would point out what I've talked about this entire time
is that for that to really happen, both sides
have to make a mistake at the same time. And
neither side wants this. We have come much closer
to that point in history
than we are right now. And it's
it's one of those things where most times when we're actually close to it,
Nobody knows.
It's not in the headlines.
I'm not saying that it's irrational to be worried
about it.
I'm not saying that you shouldn't be.
I'm saying that the risk, because of the severity
of what occurs, if it does happen,
it may be weighing more on your mind than it really should.
So take a look at the map.
Get some information.
That website has tons of information
about this topic in particular.
And then you can always look at other sites that deal with it
and get more information.
But the map is helpful because either you
find out where you're at.
And on some level, it may be comforting to know
that you really don't need to worry about it because you're
not going to know that it happened
or that it isn't close to you.
and then you just have to deal with the inevitable
utter breakdown of society that occurs afterward.
But again, this is something that the world
has actually been living under for a very long time.
It's just now, it's pushed back up into the forefront.
It's always been there.
It's always been in the background.
There's no reason to,
There's no reason to let this fear rule your life.
The numbers in that poll were really high, so maybe a little bit of information might
help calm people's nerves.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}