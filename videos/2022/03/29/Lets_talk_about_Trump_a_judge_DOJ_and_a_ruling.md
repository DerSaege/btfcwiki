---
title: Let's talk about Trump, a judge, DOJ, and a ruling.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KuuBwNooYr4) |
| Published | 2022/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump World news and a ruling about emails hint at shaping events.
- Committee sought access to emails; lawyer cited attorney-client privilege.
- Judge ordered the emails to be turned over, citing evidence of Trump's corrupt obstruction on January 6, 2021.
- The judge's use of "corruptly" implies a federal crime by the former president.
- DOJ now faces pressure to act with a federal judge pointing to high likelihood of a crime.
- DOJ can act independently of the January 6th committee's referral.
- American people may question DOJ's inaction following the judge's ruling.
- Protecting the presidency institution is a longstanding tradition in the U.S.
- DOJ's handling of this case will determine its commitment to upholding this tradition.
- Failure to properly investigate actions undermining the presidency goes against protecting the institution.
- DOJ must decide its course of action post this ruling.
- The ruling may have significant impacts despite being seemingly minor.
- DOJ's lack of action may lead to public uproar if not transparent in their investigations.
- The importance lies in the implications and potential consequences of the judge's ruling.

### Quotes

- "Based on the evidence, the court finds it more likely than not that President Trump corruptly attempted to obstruct the joint session of Congress on January 6, 2021."
- "Allowing those actions to not be properly investigated or to be swept under the rug is the exact opposite of protecting the institution."
- "This ruling, while minor in a lot of ways, is probably going to have some big impacts."

### Oneliner

Beau breaks down a ruling hinting at Trump's obstructive actions on January 6, urging DOJ to act and uphold the presidency institution.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact your representatives to demand transparency and accountability from the DOJ (suggested).
- Stay informed about developments in this case and share them with your community (implied).

### Whats missing in summary

Detailed analysis and legal implications of the judge's ruling.

### Tags

#DOJ #Trump #Obstruction #PresidencyInstitution #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk a little bit about the news
coming out of Trump World and a ruling
about some emails that included a pretty interesting line
that is probably going to shape things to come.
The committee investigating the events of January 6th
was looking to gain access to some emails.
And the lawyer was like, no, attorney-client privilege.
So it went before a judge.
And the judge ordered that a bunch of the emails
needed to be turned over.
And this is part of the reasoning.
Based on the evidence, the court finds it more likely than not
that President Trump corruptly attempted
to obstruct the joint session of Congress on January 6, 2021.
In and of itself, the judge ruling the way the judge ruled isn't a massive deal.
The inclusion of that word corruptly though, that matters.
That's straight out of the statute.
This is the judge saying that more likely than not, the former president committed a
federal crime.
The reason this matters and the reason this may shape things to come is because now it
It just became really hard for DOJ to sit on their hands.
At this point, they have a federal judge indicating that this is a high likelihood.
It's something that is going, in theory, should warrant their attention.
The January 6th committee, it can't actually, it can't press charges.
It can only refer it to DOJ.
But DOJ doesn't need the committee to act.
They can act on their own.
And at this point, the American people are really going to start to question why they're
not.
There's a long running tradition in the United States of protecting the institution of the
presidency.
And that's one of those things that has been kind of gnawing at the back of my mind this
entire time, whether or not the Department of Justice is going to try to uphold that
tradition when in this case you're talking about actions that certainly appear to have
been designed to undermine the institution of the presidency.
Allowing those actions to not be properly investigated or to be swept under the rug
is the exact opposite of protecting the institution.
At this point, DOJ has to kind of take a look
and see where it wants to move from here.
This ruling, while minor in a lot of ways,
is probably going to have some big impacts.
It's one of those things that now the Department of Justice
has this sitting out there. And if they do not act, if they do not begin to investigate,
if they do not let the American people know because they may be investigating behind the
scenes, if there aren't updates, there's going to start to be uproar. Anyway, it's
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}