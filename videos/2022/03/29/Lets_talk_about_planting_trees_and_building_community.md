---
title: Let's talk about planting trees and building community....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H73sGB5bIIA) |
| Published | 2022/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocates for planting yard for food rather than lawns, promoting the idea of replacing lawns with food-producing plants like fruit trees and raised beds.
- Shares experiences of planting peach and pear trees, humorously noting the inevitable dirtiness that comes with gardening.
- Emphasizes the long time it takes for fruit trees to bear fruit, suggesting focusing on the time spent together rather than just the end result.
- Points out the benefits of planting fruit trees to increase food security, have control over the food on the table, and build a strong community network.
- Encourages making planting trees a community or family activity to strengthen relationships and create lasting bonds.
- Views planting fruit trees as a starting point for building a strong community network that can provide support in everyday life and natural disasters.
- Mentions the importance of developing a strong community network to alleviate worries and solve problems collectively.
- Suggests approaching planting trees as family time and a screen-free activity that brings everyone together.
- Stresses the numerous benefits of planting fruit trees, mentioning that once people start, they rarely stop due to the positive outcomes it brings.

### Quotes

- "Make it about the time rather than just the fruit."
- "The best time to plant a tree was twenty years ago. The second best time is today."
- "Approach it as family time."
- "There are so many benefits to this that there's got to be something that appeals to them."
- "Once people start, I've never known anybody to start doing this who stopped."

### Oneliner

Beau advocates for planting fruit trees not just for food but to build a strong community network, encouraging focusing on the time spent together rather than just the end result.

### Audience

Gardeners, community builders, families

### On-the-ground actions from transcript

- Organize a community gardening day where everyone plants fruit trees together, creating a strong community bond (implied).

### Whats missing in summary

The importance of building a strong community network through planting fruit trees and the long-lasting benefits it can bring to individuals and communities.

### Tags

#CommunityBuilding #FoodSecurity #Gardening #FamilyTime #CommunityNetworks


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about something I love to talk about,
uh... planting.
If you're new to the channel, you probably don't know,
but I am a huge advocate
of
planting your yard, food not lawns, plant something, whatever
buzzword you want to assign to it.
The idea
of
getting rid of your lawn
and
replacing it with stuff that produces food.
Fruit trees, berry bushes, raised beds, whatever.
Over on the other channel
we've been working on a series on how to build community gardens from the
ground up
with no budget, using just stuff you can scrounge
to having a huge budget, raised beds, greenhouses, everything
step-by-step.
And I've been working on this over the last
week or two
and
I've spent yesterday
planting a whole bunch of peach and pear trees.
And I made a joke on Twitter about how, you know,
I don't understand how the Homestead channels on YouTube, how the people on them
are always so clean, because you get dirty doing it.
But Bothered Boy, who is another person on YouTube, does political
commentary.
He's like, I've been trying to get my family to do this,
but they say that it takes too long for the fruit trees
to bear fruit. If you don't know,
you plant a
a decent-sized fruit tree, you probably have three years before you're going to
get fruit
in any meaningful way.
So it's true, it does take a long time.
And he was asking for advice on how to convince his family to do it.
So if
you,
your family, your friend group,
whatever,
if you're trying to get them to do this and you're running into issues,
make it about
the time
rather than just the fruit.
You know, if you,
if everybody in your friend group
plants trees that produce a different kind of fruit, in a few years,
y'all aren't buying fruit anymore.
Because most times you'll end up with more than any one person
can use.
So you end up switching them out.
It's a good way to increase food security.
It helps the environment, because you can plant stuff that's supposed to be there.
You know, the exercise keeps you healthy, you have more control over
the food that goes onto your table, so on and so forth. There's a bunch of reasons
to do it.
But when you're trying to get people,
when you're trying to get people involved to begin with, it's hard.
Because they're like, we plant this tree and wait three years.
Yeah, but it's just the starting point.
One of the reasons I'm a huge advocate for this
is that it builds
intentionally or not, it builds community networks.
It builds
a group of people
who in some way are committed to each other
through this method. And that spills over
into other stuff and it creates a strong community. It creates a strong
network
of people that you can rely on.
And this is important in everyday life and in
natural disasters.
It goes beyond
just the fruit.
That's just the starting point of it.
But if you're talking about your family,
I would
try to make it about the time
rather than
the fruit.
Try to make it about the time you're going to spend together.
You know, everybody,
parents,
kids, grandkids, everybody's going to plant a tree.
And we'll see who does the best.
Make it a community activity, a family activity.
And it might
help spur that interest. You know, there's that old saying, the best time to plant a
tree was twenty years ago.
The second best time is today.
This is one of those things that
it's a
starting point
for something a lot better.
Because if you can develop a community network and if you're newer to the
channel,
there's a whole playlist on this,
it helps you out
throughout your life.
If you build one that's strong enough,
a lot of things that worry other people don't worry you anymore.
Because you have access to people in your network
that can help you and you can help them and you can eventually leverage that
into
solving bigger and bigger problems for people.
So,
yeah,
the food security, all of that stuff is good,
but it's also that starting point for that community.
And if you're talking about your family,
approach it as family time.
Approach it as something that you're doing that
doesn't involve a screen.
You know, it's something that you all can do together.
And
maybe that'll work.
If not,
find
whatever,
whatever thing motivates you.
There are so many benefits to this
that there's got to be something that appeals to them.
And once people start,
I've never known anybody to start doing this who stopped.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}