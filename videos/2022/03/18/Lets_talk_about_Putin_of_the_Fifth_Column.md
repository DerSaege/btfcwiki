---
title: Let's talk about Putin of the Fifth Column....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=02F5ACZu_7E) |
| Published | 2022/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin recently used the term "fifth column," sparking curiosity about its origin.
- "Fifth column" refers to a small group within a larger one working to undermine it.
- The term's origin story involves a general claiming to have four approaching columns and a fifth inside the city ready to open the gates.
- Putin, however, was referring to the protesters in Russia criticizing his actions in Ukraine as the fifth column.
- The invasion into Ukraine has diminished Russia's global reputation, reducing it from a potential superpower to a military joke.
- Despite disagreement about the protesters being labeled the fifth column, Beau points out that Putin and his oligarchs fit this description perfectly.
- Putin and his circle are the ones undermining Russia from within due to corruption and ineffective leadership.
- Beau concludes by suggesting that Putin himself is the real fifth column, causing damage for his own ego and refusing to acknowledge his failures.

### Quotes

- "Putin is the fifth column."
- "It's those who refuse to face reality."
- "Putin and his oligarchs are undermining a large group from within."
- "Russia brought to its knees by corruption and ineffective leadership."
- "A major power, not a superpower."

### Oneliner

Putin's misuse of "fifth column" reveals the true underminers of Russia: Putin and his oligarchs, not the protesters. 

### Audience

Activists, Russia watchers

### On-the-ground actions from transcript

- Organize protests against corruption and ineffective leadership within Russia (exemplified)
- Support independent journalism exposing the truth about Putin and his oligarchs (exemplified)

### Whats missing in summary

Beau's emotional tone and emphasis on the damaging impact of Putin's leadership can be best understood by watching the full video.

### Tags

#Russia #Putin #FifthColumn #Oligarchs #Corruption


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today, we are going to talk about Putin of the fifth column.
If you missed it, in a recent address,
Putin used the term fifth column.
Because of that, my inbox filled up pretty quickly.
I'm fairly certain it had absolutely nothing to do with me,
but that doesn't change the fact that a whole bunch of people
messages asking about the origin of the term. Generally, it means a small group or organization
that is intent on undermining a larger group or organization from within. That's what it means.
For me, I want to say seven or eight years ago, there were a whole bunch of journalists who were
concerned about gatekeeping within journalism. One of the original stories
using the term fifth column had to do with a general who was approaching a
city and in an attempt to get it to surrender he said you know I have four
columns one approaching from the north the south east and west and a fifth
column inside the city ready to open the gates. So if you're a journalist
concerned about gatekeeping, well, it made sense. I continued to use the term.
That's not what Putin was talking about either. He was talking about the larger
group being Russia itself, and the fifth column being the protesters who are
speaking out against his disastrous war, his ill-conceived, ill-planned, ill-
prepared, ill-fated adventure into Ukraine. That's who he's talking about.
Now, the reality is, this invasion, it reduced Russia. It reduced Russia's
stature. Russia was on the verge of being a superpower again. It could have
happened. Instead, it's become a military laughing stock. A military that was once
respected is now synonymous with a meme about tractors, and it's undermined the
standing on the international stage to the point where, at best, it walks away
from this a major power, not a superpower. And at worst, if he doesn't, if he doesn't
And in the soon, it is incredibly likely that Russia becomes a failed state.
So while I disagree with the idea that those protesters are a fifth column within Russia,
I do believe there is a fifth column within Russia.
It's Putin and his oligarchs.
If you're talking about a small group of people who are undermining a large group from within,
it's them.
It's not the people out there speaking.
It's those who, for the sake of their own ego, refuse to face reality.
Refuse to accept the fact that their corruption, their ineffective leadership, brought an incredibly
powerful country to its knees.
Putin is the fifth column.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}