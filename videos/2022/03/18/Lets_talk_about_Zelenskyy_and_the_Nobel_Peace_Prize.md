---
title: Let's talk about Zelenskyy and the Nobel Peace Prize....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fNafdoALgqI) |
| Published | 2022/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Zelensky of Ukraine has been nominated for a Nobel Peace Prize by 36 politicians from Europe.
- The politicians are asking the Nobel Committee to extend the nomination deadline for Zelensky.
- However, the criteria for the Nobel Peace Prize are focused on fraternity between nations, reduction of standing armies, and promotion of peace congresses.
- Zelensky, as a wartime president fighting against a powerful country, does not fit the criteria for the Peace Prize.
- Awarding Zelensky the Nobel Peace Prize for his actions in a war setting might imply failure in his leadership role.
- Despite not fitting the criteria, Zelensky is acknowledged as the leader Ukraine currently needs.
- Zelensky's nomination for the Peace Prize may not succeed, as his actions are more related to war efforts rather than peace initiatives.
- The nomination could potentially be a setup for Zelensky not winning the award.
- There are other awards more suitable for Zelensky's actions, but the Peace Prize seems like an odd choice.
- The nomination could have propaganda value in portraying Zelensky as a peacetime president bullied by Putin.

### Quotes

- "He's not a peacetime president."
- "He has rallied the world for the purposes of war, not for the purposes of peace."
- "Almost, it seems like they're setting him up to not win."
- "There are a whole bunch of awards that Zelensky totally meets the criteria for."
- "It seems like it could be made in other ways."

### Oneliner

President Zelensky's nomination for the Nobel Peace Prize raises questions about fitting the criteria for a wartime leader fighting against authoritarianism.

### Audience

Politicians and policymakers

### On-the-ground actions from transcript

- Contact the Nobel Committee to express opinions on the nomination (suggested)
- Stay informed about the criteria and purpose of the Nobel Peace Prize (implied)

### Whats missing in summary

The full transcript provides a deeper analysis of President Zelensky's nomination for the Nobel Peace Prize and raises questions about the appropriateness of this recognition in a wartime context.

### Tags

#Zelensky #NobelPeacePrize #WartimeLeader #Propaganda #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about President Zelensky
of Ukraine being nominated for a Nobel Peace Prize.
36 politicians from countries in Europe,
past and present politicians, have
petitioned the Nobel Committee to extend the deadline.
The deadline for nominations has already passed,
but they're asking the committee to push it back,
to allow Zelensky to be nominated.
That seems like a really odd move to me.
This is mainly a symbolic gesture.
They're saying, hey, it's important to honor somebody
who is standing up against authoritarianism
and fighting for democracy and all of that.
And yes, that's an important thing to honor people who do that.
However, that's not what the Nobel Peace Prize is for.
According to the will that actually laid out the criteria,
the award is to go to the person who has done the most or the best
work for fraternity between nations, for the abolition or reduction
standing armies and for the holding and promotion of peace congresses.
That's not what Zelensky has been doing because that's not what he's supposed to be doing.
He's not a peacetime president.
This is a man who is running a war against an incredibly powerful country.
If he was doing the things that would allow him to win a Nobel Peace Prize, he would be
a failure as a leader.
He's not.
He has turned out to be the exact leader that Ukraine needs right now.
If you go back prior to any of this starting, I said, he's doing a great job in the run-up.
He's really holding his own, but once the fighting starts, he needs to get out of the way.
I have never been happier to eat my words. He's doing a great job, and I don't think that anybody
else would have been able to do it the way that he is. He has rallied the world,
But, he has rallied the world for the purposes of war, not for the purposes of peace.
And I think that those who made this nomination know that.
I think they know that this isn't a nomination that is going to succeed.
It is unlikely that Zelensky be awarded this.
doesn't fit any of the criteria and he shouldn't fit any of the criteria
because he's doing his job. I think it was a weird move, I'm gonna be honest.
Almost, it seems like they're setting him up to not win. I mean it would be
be incredibly surprising for the committee to select somebody who is really known for
encouraging and prosecuting a war that doesn't seem to be in the spirit of the Nobel Peace
Prize.
There are a whole bunch of awards that Zelensky totally meets the criteria for.
I don't know that this is one, and I think it was a really bizarre move by these politicians.
I'm not sure why they did it, other than the possible propaganda value of painting Zelensky
as, you know, this peacetime president who is being bullied by, you know, Putin,
the evil KGB guy. I mean, I can see the value in that, but, I mean, that seems
like a long way to go to get to that point. It seems like it could be made in
other ways. Maybe it was a genuine thing and they feel that his efforts in the
beginning, when he was telling everybody to stay calm and not to inflame things,
maybe they feel that warranted it because he did attempt to stop this
before it starts. But I don't know that that's gonna actually mean anything to
the committee that makes the selections. But that's your news for the morning,
something I don't think anybody saw coming. Anyway, it's just a thought. Y'all
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}