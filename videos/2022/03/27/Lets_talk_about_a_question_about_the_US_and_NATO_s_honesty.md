---
title: Let's talk about a question about the US and NATO's honesty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=719eAvh8Ir8) |
| Published | 2022/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about NATO, the US, and foreign policy, particularly addressing the perception younger people have.
- Shares a message he received from a viewer questioning why the US isn't seen as the bad guy in conflicts.
- Beau challenges the perception that only the US has lied about wars throughout history.
- Explains that all major powers engaging in military interventions lie, deceive, and manufacture consent.
- Points out that the behavior associated with the US and NATO is not unique to them; all major powers behave similarly.
- Emphasizes that interventions by major powers are inherently problematic, regardless of the country or alliance involved.
- Notes that the viewer's perception is shaped by the dominance of the US and NATO on the international stage in recent history.
- Suggests that as other countries become more active internationally, similar behavior will become more visible.
- Draws parallels between the actions of different countries in international conflicts.
- Concludes by stating that the behavior of large powers in international affairs has been consistent throughout history.

### Quotes

- "Our entire lives, it's been the US lying about wars."
- "It isn't that a particular state using violence on the international scene is bad. It's that all states using violence on the international scene are bad."
- "It's just that this is what large powers do."
- "Every country does it."
- "Now that other countries are starting to make plays on the international scene, you're going to see it more and more."

### Oneliner

Beau challenges the perception that only the US lies about wars, stating that all major powers engaging in military interventions behave similarly on the international stage.

### Audience

Younger viewers under 30

### On-the-ground actions from transcript

- Challenge perceptions about specific countries' actions (suggested)
- Advocate for transparency in international affairs (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on international conflicts and challenges viewers to rethink their perceptions based on historical behaviors of major powers.

### Tags

#NATO #US #foreignpolicy #perception #internationalconflicts


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about NATO.
We're going to talk about the US.
We're going to talk about foreign policy.
And we're going to talk about a perception
that people have, particularly if you are younger.
If you are under the age of 30, watch this video.
Um, we're going to do this because I got a message.
And it made so many things just suddenly, like,
fall into place and make sense for me.
So here's the message.
I've watched you since my senior year in high school
when I met you after the hurricane
when your son locked your keys in your truck.
I remember this.
I remember you.
Yeah, that happened.
That was a very frustrating day.
So I know you have no problem calling out US adventurism,
to use your word.
Cough, imperialism, cough.
Why do you believe the US isn't the bad guy here?
Our entire lives, it's been the US lying about wars.
Now I'm supposed to believe them?
I'm at FSU now.
And one of my professors, who is always
very critical of the US and NATO,
is beating the war drum suddenly.
Thanks for not doing that, by the way.
I just want to know what you and she see that made you change.
There's one line in here that just, it's like a light bulb
went off over my head.
Our entire lives, it's been the US lying about wars.
No, no.
Your entire life, it's been the US lying about wars.
I'm old.
I'm willing to bet your professor is too.
From your scope, from your entire life experience,
any time the US or NATO was engaged
in a military action on the international scene, it lied.
It did things it wasn't supposed to.
It did all sorts of deceitful things.
It manufactured consent, all of this stuff, right?
Because of that, your perception of it
is that this is what the US and NATO do.
And then when you started looking back through history,
my guess is that that perception colored
all of the events of the past that you learned about,
because it was true through your entire life experience.
I want to help reframe that a little bit.
Something that is also true through your entire life
experience.
100% of the time, large powers that
engage in military interventions lie, engage in deceit,
do things they're not supposed to, manufacture consent.
I think what has happened, at least for some people,
and it explains a lot of things that I've
seen over the last month, is that because
of that perception, because of the fact
that your entire life, only the US and NATO
were on the international stage making
moves of any international level,
you were able to make a decision that was
on the international stage making moves
of any international significance,
that behavior is solely associated
with the US and NATO.
It's not the case.
It's not the case.
I'm not saying NATO or the US doesn't do that.
All major powers do that.
It's not that US or NATO intervention is bad.
Interventions like this are bad, and all major powers
that engage in military intervention,
they all do the same things.
It isn't that a particular state using violence
on the international scene is bad.
It's that all states using violence
on the international scene are bad.
It's just that through the scope of your life,
there's only been one major player.
There's been one group that has engaged in this.
Now, there are other countries that
are doing things that are international in scope.
They're not just hitting smaller countries around the border.
They're doing things that impact the rest of the world.
You're going to see it more and more.
It isn't that NATO or the US is good or bad.
It's just that this is what large powers do.
It isn't isolated to any particular nation or alliance.
They all do it, and they all will.
This has been true through all of modern history
and going back to antiquity.
It's just that for your life, for a lot of people's lives,
the only player was US and NATO.
So that behavior has become associated with them
when it's really something that every large power does.
And now that you can see another large power doing it,
it's disconcerting.
If you look at Ukraine and compared it
to the run-up to Iraq, the manufacturing of consent,
the Z instead of the yellow ribbon, the pretexts,
it's all the same.
Every country does it.
It's not just NATO, not just the US.
And now that other countries are starting
to make plays on the international scene,
you're going to see it more and more.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}