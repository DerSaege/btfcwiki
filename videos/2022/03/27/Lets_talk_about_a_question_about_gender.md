---
title: Let's talk about a question about gender....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vQ53lVyi4so) |
| Published | 2022/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Addressing a question on gender and biology that has arisen in various platforms, including Congress.
- Explaining societal norms around gendered colors and toys, showcasing the lack of biological link.
- Pointing out how politicians use gender and biology debates as a tool for manipulation.
- Gender is a societal perception that changes as societal views evolve.
- Gender is not inherently tied to biology; it's a perception shaped by society.
- Pink and blue being associated with girls and boys is a societal norm, not a biological fact.
- The gendering of products like toys, coffee cups, and marketing strategies is based on societal norms, not biology.
- Society's perception of gender changes over time, showing its lack of biological basis.
- Politicians manipulate the gender and biology debate to control certain groups by creating division and distraction.
- Gender is not fixed and has been viewed differently in various societies throughout history.

### Quotes

- "Gender is a perception. It's not linked to biology."
- "It's a societal perception."
- "Politicians manipulate the gender and biology debate to control those people who see this as a serious affront to their values."
- "Gender is not linked to biology in any way."
- "A whole lot of societies throughout history had more than two genders, just so you know."

### Oneliner

Beau Young explains how gender is a societal perception, not linked to biology, debunking the manipulation by politicians.

### Audience

Educators, Activists, Parents

### On-the-ground actions from transcript

- Challenge gender stereotypes by promoting gender-neutral colors and toys (exemplified)
- Educate others on the societal perception of gender (suggested)
- Advocate for inclusive policies and representations of gender diversity (suggested)

### Whats missing in summary

In-depth examples of how societal norms shape perceptions of gender and the historical context of gender fluidity could be better understood by watching the full transcript.

### Tags

#Gender #Society #Biology #Perception #Politics


## Transcript
Well, howdy there, internet people.
It's Beau Young.
So today, we're going to talk about a question that
has arisen.
And it's, for some reason, all over the country,
even been in Congress.
Now, I did a video pretty similar to this
a couple of years ago.
But I had a question come in.
And it's one of these questions that I can't tell if the person
is trying to do one of those gotcha questions
or the person's being sincere and is actually
trying to figure this out.
So I'm going to answer it in good faith.
Here's the question.
Since leftists try to say that gender isn't linked to biology,
can you tell me how?
Yeah.
Yeah.
That's actually pretty easy.
I got you.
No worries.
First thing you have to know is that this isn't really
a left thing.
It's very much a right-wing thing, a capitalist thing,
for real.
OK, so I have a daughter and a son.
Without thinking about it too much, which one?
Flashlight.
Which flashlight do I give to which kid?
Why?
Well, because pink's the girl's color.
And blue is the boy's color, right?
Why?
Is that somehow linked to biology?
These are gendered.
Why?
What about the drones over there, over my shoulder?
One's pink.
One's blue.
One came out of the boy's aisle.
One came out of the girl's aisle.
They're gendered.
Why?
Because we as society have decided that pink
is the girl's color.
Blue is the boy's color.
It's a societal norm.
Has nothing to do with biology, right?
But they're gendered.
What about this?
Extreme three for women.
Marketing, capitalist in this case.
It's gendered.
Is it somehow super different than this one?
Not really.
It's made up.
It is made up.
It's a marketing tool in that case.
What about the coffee cups back there?
Obviously, the one with the mustache is the cup for men.
And the one with Wonder Woman is the cup for women, right?
Stands to reason.
Except the Wonder Woman cup's mine.
The mustache cup, that's my wife's.
Because it's a societal norm.
Norm doesn't mean that you can't deviate from it.
And if an opinion is enough to shift it,
it's certainly not hardwired to biology, right?
I mean, it's really that simple.
What about the two GI Joes back there?
One's man, one's woman.
Both came out of the boys' toy aisle.
Why?
Because the societal norm is that war toys are for boys.
So women characters end up in the boys' aisle.
It's all it is.
It's a societal norm.
It is not linked to biology in any way.
There's no separating going on.
It wasn't linked.
It's very hard to suggest that gender is somehow
linked to biology when I can show you
a whole bunch of gendered things that
aren't biological creatures.
It's a societal perception.
And because it's based entirely on how society views it,
as society's views change, well, so do the views of gender.
If you want it to be linked biologically,
you have to show me a biological reason for pink and blue.
Why is blue the boys' color?
Why is pink the girls' color?
There's no biological reason for that.
It's just a societal norm.
You have to establish the biological link, which
is going to be super hard because it wasn't that long ago
when pink was actually the boys' color.
This isn't really a huge debate here.
The two things are different.
What's happening is a bunch of politicians
who probably know better are trying to conflate the two
and blend them together.
Because then those people who haven't participated
in that societal shift, those people can be appealed to.
Those people can be appealed to.
And those politicians can look at those people,
those people who haven't shifted with the rest of society,
and say, look, these people, they're different.
Other them.
Blame them for your problems.
Look down at these people and don't pay attention to me
while I take advantage of you and manipulate you.
That's all it is.
It's a tool by politicians to control those people who
see this as a serious affront to their values.
It's all it is.
That's why it's a topic.
Gender is a perception.
It's not linked to biology.
A whole lot of societies throughout history
had more than two genders, just so you know.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}