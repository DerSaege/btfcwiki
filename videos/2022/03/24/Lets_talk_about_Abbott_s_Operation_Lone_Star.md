---
title: Let's talk about Abbott's Operation Lone Star....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KZWVrjmTVmg) |
| Published | 2022/03/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Operation Lone Star in Texas uses the Texas Military Department and National Guard troops under state authority.
- Troops are attempting to unionize due to poor conditions.
- ProPublica, a news outlet, found issues with Operation Lone Star's success claims.
- Texas is taking credit for arrests and seizures that occurred before the operation.
- Governor Abbott's claims of success with Operation Lone Star are being called into question.
- Many of the claimed arrests are unrelated to the border situation.
- Only a fraction of the claimed fentanyl seizures actually came from Operation Lone Star.
- The operation is seen as a political stunt and an election device.
- It aims to portray certain people as bad and the governor as taking action.
- The operation costs Texas $2.5 million a week, totaling $3 billion.
- Beau suggests that the money could be better spent, possibly on improving the electric grid.
- Operation Lone Star's lack of success is indicated by the troops' attempt to unionize.

### Quotes

- "Operation Lone Star is using the Texas Military Department, National Guard troops under state authority."
- "It's a stunt. It's an election device."
- "Imagine what else Texas could use that money for."
- "Operation that is so bad, it has National Guard troops trying to unionize."
- "Not really that successful."

### Oneliner

Beau breaks down Operation Lone Star in Texas, exposing its flaws and political motives while questioning its claimed success and high cost.

### Audience

Texans, Activists

### On-the-ground actions from transcript

- Contact local representatives to express concerns about Operation Lone Star's effectiveness and cost (implied).
- Support investigative journalism like ProPublica by sharing their reports and subscribing (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Operation Lone Star, shedding light on its shortcomings, financial implications, and political nature.

### Tags

#Texas #OperationLoneStar #PoliticalStunt #GovernorAbbott #ProPublica


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
Operation Lone Star down there in Texas again. We have discussed it on the channel before.
If you need a quick recap, this is the political stunt... I'm sorry, this is the operation
that the governor there has put into action that is using the Texas Military Department,
the National Guard troops, under state authority, and the conditions are so bad down there that
the troops are trying to unionize. If that's not bad enough, ProPublica, if you're not
familiar with this outfit, they're a news outlet, and there are a lot of news outlets
that are great at getting into the nitty gritty details, the numbers behind domestic politics
and stuff that goes on. ProPublica is one of the few that can take those numbers after
they do the investigation and put it into a readable format that doesn't make you want
to bang your head on the desk. If you're not familiar with them, definitely check them
out. But after their investigation, it seems like one of the real issues lies in the fact
that the successes of Operation Lone Star are being, let's just say, overstated. It
appears that the numbers that are being distributed by Texas include arrests and seizures that
have taken place in 63 counties using assets that were in place prior to Operation Lone
Star starting. So they're taking credit for stuff that they had nothing to do with.
They have at one point claimed 11,000 arrests. They've had to remove a lot of those. ProPublica
did a really good job of tracking some of them down and finding out what they were.
Some of them are hundreds of miles from the border. Some of them are domestic disputes
having nothing to do with the border. It's pretty wild. The claim that Abbott is touting
right now about 887 pounds of fentanyl or whatever, it turns out only a quarter of that,
less than a quarter of that, came from Operation Lone Star. It's a stunt. It's an election
device. That's all it is. It's a way to try to get the people of Texas to say, oh, those
people are bad. Other these people, look down on them, and that governor of ours, well,
doing something about it. Not so much, looks like, by the statistics, by the numbers. I
mean, except for the two and a half million dollars a week that is spent on this. That's
a real number. Two and a half million dollars a week, three billion dollars. Imagine what
else Texas could use that money for. I mean, off the top of my head, I could say it might
be invested in the electric grid. That might be a good idea. Rather than using it to prolong
an operation that is so bad, it has National Guard troops trying to unionize and is apparently
not really that successful. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}