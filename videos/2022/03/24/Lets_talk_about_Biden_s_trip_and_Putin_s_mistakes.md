---
title: Let's talk about Biden's trip and Putin's mistakes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ceLppv1UqTk) |
| Published | 2022/03/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's trip to Europe reveals insights into the scene in Europe and implications for Putin's long-term goals and mistakes.
- Putin wanted Western troops out of Eastern Europe back into traditional NATO member states from the Cold War, but this plan has backfired.
- Putin's actions have led to the US increasing troop numbers by 20,000 and caused Germany to re-evaluate its defense policy.
- Rather than weakening NATO, Putin's actions have put the alliance on alert and prompted talk of creating a separate EU army, which could challenge NATO.
- Biden is expected to announce more sanctions targeting individuals in Russia, primarily elected members of government, to increase pressure on Putin.
- There are concerns about potential cyber attacks, the use of chemical weapons, and even nuclear threats from Russia.
- The Kremlin stated that Russia would only use nuclear weapons if facing an existential threat, clarifying that it's part of Russian doctrine.
- Despite concerns, it's uncertain if Russia will actually use nuclear weapons, with chemical weapons being considered a more likely option.
- The Biden administration is preparing for various scenarios, including cyber attacks and potential unconventional warfare from Russia.
- Overall, Putin's actions have resulted in strategic losses on the world stage, failing to achieve his objectives both outside and inside Ukraine.

### Quotes

- "Putin wanted the West to move its troops back, get them out of Eastern Europe and back into traditional NATO members. That's not happening."
- "Putin lost in every way, shape, and form."
- "Zelensky offered an olive branch and said, hey, I'm open to agreeing to a stipulation saying we won't join NATO."
- "Biden is expected to announce another round of sanctions, this one targeting by last count like 400 people individually in Russia."
- "Overall, on the world stage, Putin lost in every way, shape, and form."

### Oneliner

Biden's trip to Europe reveals Putin's strategic miscalculations and the potential creation of an EU army, while increasing pressure through sanctions and preparing for possible Russian threats.

### Audience

Policymakers, analysts, activists

### On-the-ground actions from transcript

- Monitor developments in Europe and Russia closely for potential impacts on global security (implied).
- Support diplomatic efforts to de-escalate tensions and prevent conflict escalation (implied).
- Stay informed about the implications of NATO actions and potential responses from Russia (implied).

### Whats missing in summary

Insights on specific actions individuals or organizations can take to support diplomatic efforts and understanding of the situation. 

### Tags

#Biden #Putin #Europe #NATO #Sanctions


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Biden's trip and what that trip can tell us about
the scene in Europe and what it means for Putin's long-term goals and mistakes, basically,
and how this may have been a more grave miscalculation than he originally thought.
So fair warning, I'm recording this before the trip so some of this is likely to change
a little bit, but Biden's agenda is to meet with NATO.
That's going to be a big part of it, meeting with different NATO members and shoring up
support and talking about troop redeployments.
It's important to know that one of the things that Putin wanted was for the West to move
its troops back, get them out of Eastern Europe and back into traditional NATO members, the
NATO members from the Cold War.
That's not happening.
That's already gone awry.
This little stunt of his has caused the US alone to increase troop numbers by 20,000.
It has caused, as discussed earlier, Germany to completely reevaluate its whole policy
on defense.
Rather than shaking NATO, it's stilled it.
It has put it on alert.
Now one of the other things that has come up, and we're starting to see talk about this
in magazines that provide commentary on this type of stuff that are normally pretty good
at kind of predicting the future, is the creation of an EU army, a military for the European
Union, which is something that the US is probably going to oppose because it would run parallel
and therefore be a competitor to NATO.
If the European Union gets its own military, the United States loses influence via NATO,
which it's not about right and wrong, not about ideology, it's about power.
The US isn't going to like that unless it is somehow under the auspices of NATO.
So that's yet another development that Putin probably didn't anticipate.
I don't think many people did, that may come to pass.
Biden is also expected to announce another round of sanctions, this one targeting by
last count like 400 people individually in Russia.
Most of them are people within government.
There are other oligarchs and Russian elites that are being targeted, but the overwhelming
majority of them are elected members of government.
This is again an attempt to put more pressure on Putin, which will probably make him even
more paranoid.
Biden has already kind of warned people about the possibility of a cyber attack because
of scanning that went on when it comes to US energy agencies.
There's also the constant rumble about the use of chemicals or nuclear devices.
It's worth noting that the Kremlin's spokesperson said during an interview that the only way
Russia would use nukes is if it was facing an existential threat, using those exact words.
And I had people ask me about that because it was phrased the exact way I phrased it.
It's straight out of Russian doctrine.
That's why the words are the same.
He's not watching the channel.
Basically the idea is that Russia would only use nukes if it felt it was no longer going
to be Russia.
There are people that are trying to read into this.
I would point out that the spokesperson was asked a direct question about whether or not
he could guarantee that Putin wouldn't use nukes.
And the answer was to basically read the doctrine and say this is the only time we would use
them.
I don't think that that was a signal to the West that they're just dead set against using
them.
I don't think that that was some kind of bluff.
I think they were asked a direct question and answered it.
I wouldn't read too much into that.
As far as the use of chemicals, which is something we haven't really talked about, I think that's
more likely than a nuclear option.
I still wouldn't say that it's incredibly likely, but as Russia's advance continues
to grind to a halt and as more is displayed, as the Russian military is put on display
and its weaknesses are shown, it may provoke Putin into authorizing something like that.
I think that we would see a lot heavier artillery bombardment first.
I think there would be signs that we were reaching that stage, and I think there would
be signs that hopefully Western intelligence would be able to read.
But again, that's up in the air.
I think the Biden administration is trying to prepare people for any eventuality, and
that's why they're talking about all of this stuff.
Cyber attacks, we've talked about it before.
I think that's pretty likely.
They're going to try at some point, maybe in response to the sanctions.
But the key takeaway here is that overall, on the world stage, Putin lost in every way,
shape, and form.
Obviously on the foreign policy scene, the defense strategy, the message he was trying
to send was not received.
In fact, the exact opposite is happening.
It didn't go well from a strategic point of view outside of Ukraine, and it's not going
well inside Ukraine.
I would hope that some of his advisors still have the courage to explain that it's time
to back out.
Zelensky offered an olive branch and said, hey, I'm open to agreeing to a stipulation
saying we won't join NATO as long as there's some kind of assurance, some kind of guarantee
from NATO that they'll back us up if you ever invade, which is basically joining NATO without
joining NATO.
So that's what the trip's about.
That's what it's supposed to be about.
That's what it's forecasted to be about.
When it comes to the sanctions, those things get reworked all the time.
So by the time it's all said and done, it may only be 100 people.
It may be every member of the elected government.
There may be changes, and we'll have to see how that plays out.
But as far as the defense stuff, that's pretty much set in stone.
They are shoring up defenses along the eastern flank of NATO, and American troops are preparing
to enter that phase like it was during the Cold War, where there were just tens of thousands
of American troops all over Europe in case something went wrong.
Countries that are of particular concern, Estonia, Lithuania, and Latvia, because of
their small size and their proximity to Russia.
I don't know what the plan is there to try to guarantee their security.
But anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}