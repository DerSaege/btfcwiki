---
title: Let's talk about hypersonic tech and masculine militaries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0ZDPs_c0tX4) |
| Published | 2022/03/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the resurgence of the idea of a super-masculine military.
- Russia's use of hypersonic technology in war.
- Contrasting approaches between Russia and the United States in military tactics.
- Defending the importance of diversity training for troops.
- Ukraine's diverse military composition effectively halting the super-masculine Russian military.
- Emphasizing the value of understanding opposition through diversity.
- Referencing historical military wisdom and the importance of knowing one's enemy.
- The traditional belief that understanding one's opposition leads to victory.
- The negative implications of creating an outgroup by dismissing diversity.
- The significance of cultural understanding in military success.
- US Army Special Forces' cultural training as a key to their success.
- Warning about the negative tone associated with dismissing diversity.
- Acknowledging the potentially harmful framing of the discourse on military masculinity.

### Quotes

- "Maybe that hypermasculine stuff isn't what it's got out to be."
- "Understanding your opposition is important. It wins wars."
- "If you know your enemy and yourself, you need not fear the result of a hundred battles."
- "If they assimilate and they just become another cog in the wheel, they don't provide the added insight."
- "It's worth just acknowledging that and being ready to see it."

### Oneliner

Beau dives into the resurgence of super-masculine military ideals, contrasting diverse approaches between Russia, Ukraine, and the US, underscoring the vital role of understanding opposition through diversity in achieving military success.

### Audience

Peace advocates, military analysts

### On-the-ground actions from transcript

- Understand and advocate for diversity training in military institutions (implied).
- Support diverse military compositions for better understanding of opposition (implied).
- Emphasize cultural understanding and training in military operations (implied).

### Whats missing in summary

In-depth analysis on the impact of diverse military compositions on operational success.

### Tags

#Military #Diversity #Russia #Ukraine #HypersonicTechnology


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Russia and Ukraine
and hypersonic technologies and diversity and masculinity
and traditional military values and all of that stuff.
Because for whatever reason, right now,
which seems like a really weird time to bring this up,
The idea of needing that super-masculine military
has become a talking point again, primarily on the right.
So we're going to go over it.
This statement is from George Papadopoulos,
but it's just one of many from the commentators at large.
While we have been giving our troops diversity training,
Russia has now developed and used
hypersonic missile in the war for the first time in history and I mean yeah
that's true. They did use one recently to hit a
low-priority target and it's worth noting that most analysts believe they
used it because they're out of precision guided munitions and it's also worth
noting that it didn't perform the way that they thought it would and it's
also worth noting the reason that Russia really needs this technology to function
One is to penetrate the opposition's air defenses, whereas the United States hasn't
put a lot of effort into publishing its advances in this technology or announcing how it has
been applied because, generally speaking, the United States just destroys the opposition's
air defenses.
Don't really worry too much about them after that.
But after all of that commentary on the hypersonic side, there's that idea again.
We're giving our troops diversity training.
The implication being that that's weak.
That's not a good idea.
But that's not true.
I mean, let's be clear, the super masculine Russian military that everybody likes to hold
up as the example, it has been ground to a halt by the they, them military of Ukraine
that allows trans troops and has an entire unit of LGBTQ people whose crest, I'm not
joking about this, it's a unicorn.
I'm dead serious, and that's who's stopping them.
Maybe that hypermasculine stuff isn't what it's got out to be.
Maybe it's more important to understand your opposition, and you can get that through diversity.
get it through training, or you can get it through employing a whole bunch of
people, which then you'll need training on how they can interact with each
other. And if you have that kind of diversity and that kind of
understanding of other cultures, it helps you avoid mistakes. Like say, I don't know,
assuming that simply because a country you're about to invade has a whole bunch
of people that speak the same language as you do, that they'll fight on your
side. I seem to remember sometime recently one of those super masculine
militaries making that mistake and accosting them dearly. The reality is
that you don't have to be a military scholar to understand this. You just need
Netflix. Watch The Last Kingdom. Why did Edward want Uhtred? Because he
understood the Danes. Understanding your opposition is important. It wins wars.
Being culturally diverse allows you to understand your opposition. I would like
to quote the great military philosophers, Rage Against the Machine. Yes, I know my
enemy. That references a quote, if you know your enemy in yourself, you
You need not fear the result of a hundred battles.
If you know yourself but not the enemy, you will suffer defeat for every victory.
If you know neither yourself or the enemy, you will succumb in every battle.
It's from the art of war.
It's written in the fifth century, BC.
It doesn't really get more traditional than that.
When it comes to military thought, that's pretty much the definition of traditional
belief systems.
This idea has been around a long time, and generally speaking, the militaries that use
it, that employ it, win.
The general tone of all of this is, once again, to try to create an outgroup to other people.
Well, those people, we don't need to pay any attention to being diverse.
If they can't just assimilate into the way we do things, well, then we don't need them.
The problem is, if they assimilate and they just become another cog in the wheel, they
They don't provide the added insight, and you lose the value of being diverse.
It's probably worth understanding that America's elite, the US Army Special Forces, go through
a whole lot of training to understand various cultures before they deploy, before they go
so they don't make mistakes. And it helps them a lot. It's one of the reasons they're so successful.
So for whatever reason, in the midst of the the super masculine military that they're pointing
to just failing, this has become a talking point again. It's worth just acknowledging that and
being ready to see it, because generally it takes a pretty nasty tone. You know,
what Papadopoulos said wasn't horrible or offensive in any real way, but that's
That's not always how it's framed.
sound.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}