---
title: Let's talk about preparing for a possibility....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7zsTGA0EV3g) |
| Published | 2022/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NATO is making moves that will upset Russia, potentially leading to a response.
- Russia might choose a cyber offensive as a way to send a message.
- An offensive is likely to be annoying rather than earth-shattering.
- Simple preparations can help in case of disruptions.
- Suggestions include having cash on hand, keeping gas in the car, and ensuring access to prescription meds.
- It's advised to be prepared, not scared, in the face of a cyber offensive.
- Moscow is expected to send some form of messaging to DC due to current tensions.
- The messaging is not anticipated to be extremely destructive.
- Trust in the protection measures in place on our side.
- Minor inconveniences are expected, not major infrastructure disruptions.

### Quotes

- "Be prepared, not scared."
- "You're not scrambling to get cash to go get gas in your car."

### Oneliner

NATO's moves may prompt a Russian cyber offensive, but simple preparations can alleviate stress and minor inconveniences.

### Audience

Civilians

### On-the-ground actions from transcript

- Keep cash on hand, ensure access to prescription meds, and maintain gas in your car (suggested)
- Print out sensitive documents if needed and destroy them afterward (suggested)

### Whats missing in summary

Preparations for potential cyber disruptions and minor inconveniences can help alleviate stress and ensure readiness in case of a Russian response.

### Tags

#Preparedness #CyberSecurity #Russia #NATO #Tensions


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a possibility
and how to be prepared for that possibility in case it happens.
Because if you're prepared, you don't have to worry about it.
And in this case, it doesn't really
take a whole lot to be prepared.
So we're going to go over that.
Because right now, NATO's making a lot of moves
that might upset Russia.
And by might, I mean definitely is going to upset Russia.
And Russia may want to send a message in response.
And we've talked about the different ways
that Russia could attempt to level the playing field
or send a message.
To me, the most likely way for them to do this
would be some kind of cyber offensive.
Now, realistically, they wouldn't
want it to be too big, because the whole point of doing
something like this and sending a message
is to not elicit a response.
If they make it too big, such as going
after infrastructure like electricity
or financial services or something like that,
it's likely the US would respond in kind.
So it would probably be something
a little bit lower level.
Annoying, not earth shattering.
But it's easy to get prepared, because it doesn't
take much in this case.
Simple stuff.
Make sure you have some cash, paper money,
on the off chance that maybe they
don't go after the financial services,
but they go after internet service, phone service,
something like that that disrupts the ability
to use a debit card.
So you have some cash on hand.
I'm not talking about going and taking all of your money
out of the bank.
Just a little bit, because this would only
last a few days at most.
Make sure you have gas in your car.
While this is going on, maybe don't let it go below half
tank if you can.
Make sure you have your prescription meds.
Make sure that anything that's computerized
like that that could be disrupted
that isn't super guarded.
Make sure you're kind of up to date on it.
Make sure you have it available.
I know a lot of people digitize important documents.
If you're going to need them anytime soon,
maybe print them out.
If it's something sensitive, make sure you destroy it
afterward.
And that's kind of it.
But the key part here is be prepared, not scared.
And especially don't get scared if there
is some kind of cyber offensive that
takes down communications, like social media stuff
that we're used to having.
Because when people exist in a vacuum of information,
when you're used to having a bunch of information
and now you don't have it anymore,
you picture the worst.
To me, at this point in the game,
especially with NATO attempting to get Ukraine planes
and using some unconventional transfer methods for it,
I would expect something.
I would expect some kind of messaging
to come from Moscow aimed at DC saying, hey,
you need to back off.
I don't think they're going to stick to just random
statements.
At some point, they'll message if they can.
And this seems like the most likely way to do it.
But a message isn't supposed to be super destructive.
And with tensions of the way they are,
I doubt they would risk going too far with it.
And not only that, we do actually
have people who are protecting our side.
And we have to assume that they kind of know what they're doing,
we would hope.
So again, this is more one of those just in case,
here's some real simple things you can do
to kind of alleviate any stress.
And if it does happen, well, OK, it's annoying.
It's not a big deal.
But I wouldn't expect anything serious.
I wouldn't expect infrastructure to go down
or for them to somehow lock out all of the banks
or something wild.
That would be something they would reserve for a situation
if things actually got hot.
I think at this point, we should probably
be ready for them to message, not actually really attack.
So don't go overboard with this.
But just maybe take a few little steps
to make sure that if things went down for a couple of days,
minor stuff went down, it's an annoyance.
It's not a huge deal.
You're not scrambling to get cash
to go get gas in your car or something like that.
So if you have the ability to take those steps,
maybe now's the time to do it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}