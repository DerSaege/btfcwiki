---
title: Let's talk about truth, lies, and video in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nYSGZn6OyKM) |
| Published | 2022/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing truth, lies, and reality in videos, particularly in relation to captured Russian videos surfacing during conflicts.
- Mention of Geneva Conventions and the rules on handling prisoners of war to prevent their use for propaganda.
- Not all videos of captured individuals necessarily violate these conventions, as civilians may be unaware of the rules.
- Exploring the concept of victor's justice in conflicts and the significance of propaganda wars in shaping outcomes.
- The blurred lines between truth and reality in propaganda operations and the focus on tangible impacts rather than factual accuracy.
- The role of belief in shaping what becomes real and eventually perceived as truth, especially in the context of war narratives.
- Importance of creating narratives that the target audience wants to believe for effective propaganda.
- Beau's belief that lower-ranking Russian soldiers may not have been fully aware of the situation due to potential lack of information sharing among higher ranks.
- Speculation on the reasons behind failures on the Russian side, pointing to potential issues with information flow and decision-making processes.
- Acknowledgment that discerning truth from fiction in conflicts often requires hindsight and analysis by historians, influenced by national perspectives.

### Quotes

- "Truth is the first casualty."
- "Even the lies, especially the lies."
- "The truth is normally somewhere in the middle."

### Oneliner

Beau delves into the complex interplay between truth, lies, and belief in wartime narratives, shedding light on the blurred lines of propaganda and reality.

### Audience

History Buffs, Conflict Analysts

### On-the-ground actions from transcript

- Support organizations conducting fact-checking and historical analysis post-conflicts (implied).
- Encourage critical thinking and awareness of national biases in interpreting historical events (implied).

### Whats missing in summary

Context on the potential long-term impacts of propaganda and misinformation in shaping historical narratives. 

### Tags

#Truth #Propaganda #Warfare #Belief #HistoricalPerspectives


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about truth and lies
and what's real and what's not in videos.
We're going to talk about it in relation
to one particular set of videos and information,
but it applies to most in this context.
But before we get into that, I want
to say hi to Carol in Oregon.
You have people behind you.
So I was asked a question about the videos of captured Russians
and whether or not they were telling the truth.
If you don't know, there are a lot
of videos surfacing of captured Russians,
and they're saying, I didn't know what was going on.
I had no clue.
I thought I was on a training exercise.
We didn't know.
And there are people wondering if it's true.
I don't know.
Do you want to believe it?
Because that matters.
But before we get into this, I would feel bad
if I didn't mention something.
Under the Geneva Conventions, you
are supposed to keep prisoners of war
safe from public curiosity.
You're not supposed to use them for propaganda purposes.
Does that mean that every video you've seen
has been a violation?
No.
Sometimes the people capturing them are civilians,
and they have no reason to know these rules.
Sometimes there's one in particular
where there is a captured Russian in a cell,
and you can tell that he is scared.
And they come in, and they get him to insult Putin on camera.
Yeah, that's a violation.
That's a violation.
The reason this is in the Geneva Conventions
is because this kind of propaganda
is incredibly effective.
And what you don't want is, for one side,
to come to somebody who's captured and say, hey,
you're going to say this or.
It leads to a lot of bad stuff.
That being said, as far as violations that I've seen,
this is kind of the least of the concerns.
There's also something called, it's
a concept called victor's justice.
Basically means we'll sort it out after the war.
And if your side wins, well, you really
don't have much to worry about.
So in conflicts, people generally greatly
underestimate how important the propaganda war is.
It shapes outcomes a lot.
And people tend to ignore it.
And sometimes they don't realize when
something is propaganda or it's an information operation.
In this particular case, it makes sense for both sides
to be pushing this narrative.
And generally, in war, truth is the first casualty.
And if you're running a propaganda operation
or an information operation, the concept of truth and real,
that gets fuzzy real quick.
Because it's real if it has a tangible effect
on the battlefield.
Did it make the other side feel bad?
Did it demoralize them?
Did it boost morale on your side?
Did it cause an ally to intervene?
Because if it did, well, it's real.
Is it true?
At that point, does it matter?
Because it's real.
Because it had that impact.
How much of this is true?
All of it.
Even the lies, especially the lies.
And that's a funny conversation.
But the reality is, at the end, those people
who believe the lies, they're going
to defend them so much more than they would ever
defend the truth, because they want to believe it.
And that's a key part of it.
It's a key part of creating good propaganda.
The person that it's aimed at has to want to believe it.
That pickle story.
If you don't know, there is a story floating around
that a Ukrainian grandma in an apartment building
threw a can of pickles at a drone, hit it just right,
and sent it crashing into the ground.
I have seen zero evidence to back this up.
Man, do I want to believe it.
That's part of it.
That helps shape what becomes real,
and eventually what becomes truth
when you're talking about war.
At this point in time, it is incredibly valuable
for the Ukrainian side to believe
that the Russians didn't know, that they
don't want to be there.
They want to believe that.
Because if that's true, well, maybe they surrender.
Maybe they don't fight so hard.
Maybe there's a mutiny.
But it's beneficial if they believe
that the Russian soldier doesn't support Putin's war.
Gives them hope.
And right now, that is something that is in short supply there.
From the Russian side, it makes sense
to tell all of your soldiers if they get captured to say this,
in hopes of creating that tangible effect, better
treatment for your troops.
It makes sense for both sides to push this narrative.
Does it make it true?
No, not so much.
When you look at the videos, some of them
really appear coached.
Some of them appear completely voluntary.
Some of them are unprompted to civilians.
Some of them really look like they're just
trying to say anything they can think of because they're scared.
But it's those that are unprompted
that make me lean towards at least some of it being true.
Me, personally, I would say that the conscripts, lower enlisted,
they didn't have a clue.
They didn't have a clue.
But that's not really based on the videos.
That's based on my belief that there
were a whole lot of Russian officers
who didn't know what was going on,
who should have been involved in the planning.
But due to paranoia in the Kremlin,
they were kept out of the loop.
If officers who should have been involved in the planning
didn't know, there's no way the lower enlisted does.
And I believe that officers were kept out of the loop
because that's one of the few things that
can explain a whole lot of the failures on the Russian side
is that those who were the most trusted by the Kremlin
were in that position because they were the most trusted,
not because they were the best at the job.
So there were a whole lot of people who probably
could have pointed some things out that
weren't involved in the planning.
And we're seeing the effects of that.
So what's true, what's real, we're not going to know
until afterward.
It'll be sorted out after the war.
And much like Victor's justice, there will be Victor's truth.
A lot of what we know to be true about different conflicts,
it may be shrouded, it may be skewed.
You can see this if you look at history books
from different countries.
They highlight different things
because every nation has its own lens
that it views something through.
What we see here is not the same as what Russians see in Russia.
And it's not just because of media censorship
or something like that.
It's because there's a national identity.
There's a set of beliefs that go along with it.
And a lot of it boils down to, do you want to believe it?
Because as far as discerning fact from fiction and all
of that, that's not really going to happen
until the historians get in there afterward
and start piecing it all together
and start looking at what was made up and what wasn't.
What was propaganda?
What was designed to influence this side
at this particular time?
And remember, we're spectators.
We're on the sidelines.
This stuff isn't really aimed at us.
We're just catching bits and pieces of it.
But because of our own national lens,
we highlight the pickle jar.
We highlight the stuff we want to believe.
The truth is normally somewhere in the middle.
But people will end up defending the lies at the end of it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}