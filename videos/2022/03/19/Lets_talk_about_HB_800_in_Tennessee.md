---
title: Let's talk about HB 800 in Tennessee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UKQGop2r_5g) |
| Published | 2022/03/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tennessee's HB 800 is a diversion tactic by politicians to scapegoat and blame the LGBTQ+ community to deflect from their poor performance.
- The bill aims to avoid offending Christian constituents by prohibiting the promotion or normalization of LGBTQ+ issues in public schools.
- Tennessee's legislature is failing in education and healthcare, but they want people to focus on culture wars instead.
- The bill mirrors Russia's 2013 gay propaganda law, aiming to control and manipulate public opinion.
- Politicians in Tennessee are using authoritarian tactics to keep constituents in line and secure re-election.
- The bill violates the spirit of the Constitution by targeting a specific demographic and undermining the separation of church and state.
- It's a manipulative tactic to distract from the state's failures and manipulate easily influenced individuals for political gains.
- The bill is a means for failed politicians to maintain power rather than serve the best interests of the people.
- Tennessee ranks poorly in education and healthcare, yet the focus is shifted to divisive issues like LGBTQ+ representation in schools.
- The bill is an example of how politicians prioritize their own agendas over the well-being and rights of the people.

### Quotes

- "Tennessee's HB 800 is a diversion tactic by politicians to scapegoat and blame the LGBTQ+ community to deflect from their poor performance."
- "The bill aims to avoid offending Christian constituents by prohibiting the promotion or normalization of LGBTQ+ issues in public schools."
- "It's a manipulative tactic to distract from the state's failures and manipulate easily influenced individuals for political gains."

### Oneliner

Tennessee's HB 800 scapegoats the LGBTQ+ community to divert attention from political failures, manipulating constituents for re-election.

### Audience

Tennessee residents, LGBTQ+ supporters

### On-the-ground actions from transcript

- Contact local representatives to voice opposition to HB 800 (suggested)
- Support LGBTQ+ organizations and initiatives in Tennessee (exemplified)
- Educate others about the harmful impact of discriminatory bills like HB 800 (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how politicians use divisive tactics to manipulate public opinion and distract from real issues.

### Tags

#Tennessee #HB800 #LGBTQ+ #Politics #Manipulation


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk
about a bill in Tennessee, HB 800. And it's what we've just come to expect from
those in political office now. You know, those who aren't doing well at their job
and they need to divert their constituents attention away from their
poor performance. So, you know, they find somebody to scapegoat. They find somebody
to blame, create an outrage of some sort, and deflect attention. The legislature in
Tennessee has chosen children to pick on and they're doing this because they
don't want to offend Christian people. I'm not even joking. You know, I just read
it. It's in the bill.
Whereas the promotion of LGBT issues and lifestyles in public schools offends a
significant portion of students, parents, and Tennessee residents with Christian
values. That's in the Statement of Reasons. It goes on to basically say, well, not
basically, I'll read it. Schools shall not locally adopt or use in the public schools
of this state textbooks and instruction materials or supplemental instruction
materials that promote, normalize, support, or address LGBT issues. Address. Can't
talk about it at all because it might offend people. Makes sense. This is K
through 12. It's everybody. Yeah, it's basically diverting attention from the
fact that the state legislature in Tennessee is a failure. They want the
people of Tennessee to be upset and worried and busy othering this group and
arguing about this in a culture war so they don't realize that Tennessee's, you
know, below average in education, not really good on access to health care.
It's pretty much bad all the way around with the exception of stuff related to
business. But that doesn't actually mean anything to the average person in
Tennessee because the median household income there is the 40th. You don't see
any benefit from that. That goes off to the people up at the top, not you. I did find
one thing that Tennessee was in the top 10 in. Homicide rates. But what really
matters is the fact that there might be a kid in a public school who reads a
book with a gay character. It's about deflecting blame and it's the same
authoritarian playbook that Putin would use. And that isn't something just
to say that because he's the villain of the day. I mean that literally. This is
this is pretty much the 2013 gay propaganda bill from Russia. The only
real difference is that in Tennessee it's limited just to the schools where
in Russia it was everywhere. It's limited to the schools in Tennessee because, you
know, there's that pesky Constitution in the way. Otherwise I'm fairly certain it
would be statewide. It always boggles my mind that those people who run with
campaign ads of eagles and American flags and, you know, we the people on a
piece of parchment behind them, they always seem to just try to go right up
to the the line of what they're allowed to do by the Constitution rather than it
you know accepting the spirit of it. That document that they claim to love but
have never read. This is another example of authoritarians trying to scapegoat a
demographic so they can keep everybody in line because it's offensive to
Christians. So if it's not offensive to you, you're obviously not a good
Christian, right? And this is what the state wants you to do. I'm pretty sure
that we have a kind of a guideline when it comes to the separation of church and
state in this country. Maybe I'm wrong, but it's another garbage bill designed to
scapegoat people. It's another garbage bill designed to manipulate those who
are easily influenced. So people who have failed in their jobs can get re-elected.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}