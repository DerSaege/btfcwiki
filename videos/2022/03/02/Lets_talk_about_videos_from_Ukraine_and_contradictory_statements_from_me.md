---
title: Let's talk about videos from Ukraine and contradictory statements from me...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vJldG1nh6lg) |
| Published | 2022/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses seemingly contradictory statements he has made and the need for additional context to understand them.
- Explains the advantage of amateurs over professional soldiers in terms of predictability and surprise.
- Talks about the importance of speed, surprise, and violence of action to win an engagement.
- Mentions the Ukrainian resistance confusing the Russian military with unpredictable tactics.
- Describes videos from Ukraine showing unconventional tactics against armored vehicles.
- Points out that amateurs in combat often have the element of surprise but may not always be effective.
- Emphasizes that in unconventional conflicts, it's about breaking resolve rather than traditional military victory.
- States the importance of endurance and resilience in prolonged conflicts.
- Warns against glorifying civil conflict and encourages reflecting on the true nature of such conflicts.
- Recommends picturing the reality of conflict through images like cars with "children" signs rather than romanticizing videos of resistance fighters.

### Quotes

- "Professional soldiers are predictable, but the world is full of amateurs."
- "In these types of conflicts, it's not about who can dish out the most, it's about who can take the most."
- "Eventually, either the hammer or the anvil is going to break. It's normally the hammer."

### Oneliner

Beau delves into the dynamics of predictability, surprise, and endurance in unconventional conflicts, cautioning against romanticizing resistance videos and urging a realistic perspective on civil conflict.

### Audience

Observers of conflicts

### On-the-ground actions from transcript

- Analyze and understand the dynamics of unconventional conflicts (implied)
- Avoid glorifying or romanticizing civil conflict (implied)

### Whats missing in summary

In watching the full transcript, one can better grasp the nuanced realities of unconventional conflicts and the dangers of glorifying civil conflict.

### Tags

#ConflictDynamics #AmateurVsProfessional #UnconventionalTactics #CivilConflict #Realism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about two seemingly
contradictory statements that I have made.
And they do definitely seem as though they are in direct
opposition to each other.
We're going to do this because somebody sent me a message.
And I just had two quotes, two things that I do say a lot.
And below it, it says, these are contradictory.
And they certainly seem that way without a little bit of additional context.
So that's what we're going to do right now.
If you've been watching this last week, you have undoubtedly heard me say at least once
or twice that professional soldiers are predictable, but the world is full of amateurs.
This is why the Russian military is having such a hard time dealing with the Ukrainian
resistance because they're doing things that don't make any sense. They're
unpredictable. Because they're unpredictable, they achieve surprise.
Something else we have talked about on this channel is that to win an
engagement you need three things. Speed, surprise, and violence of action. The
amateur pretty much always has surprise because what they're doing doesn't make
any sense. They come out of nowhere. So as long as whatever they're doing is done
quickly and they send everything they've got, they stand a pretty good chance of
making it, right? So there's that statement and we see that this is true
via the videos that make the rounds on social media. And if you aren't aware,
there are a lot of videos coming out of Ukraine
that show people rolling by in a vehicle, in a car,
rolling by, tossing something out of a window at an APC,
or just rolling up and stealing an armored vehicle,
or walking up and getting way too close
doing what things, doing what people do to each other during the war. And because
of that, it certainly demonstrates that that first quote is correct, right?
It shows it's true. Something else I am known for saying more than once is that
the untrained, they do one thing really well in combat, die. And those seem like
contradictory statements until you provide the added context. For every
video you see of wild Ukrainians doing wild Ukrainian things and pulling it
off, there's at least one more video where in the last few minutes of the video, it's
just the camera filming the sky.
In this type of conflict, it's not about who can dish out the most.
In fact, if you look at most of those videos that make the rounds, they're militarily
ineffective.
They don't really matter in a traditional military sense.
They aren't going to defeat the opposition on the battlefield.
They're going to break the resolve because it's terrifying because you never know what's
going to happen.
You never know where it's coming from.
In these types of conflicts, and this is going to be especially true if this becomes a protracted
thing, it's not about who can dish out the most, it's about who can take the most.
Something else I've said a lot. Eventually, either the hammer or the anvil is going to
break. It's normally the hammer. When you see these videos, just understand
that that's not a clear picture of what this stuff is like. If you want an
image to burn into your memory about what these types of conflicts are like,
don't look at these videos, especially in the United States where you have a whole
bunch of people who are under the impression that civil conflict is a good
thing. Don't think about these videos, think about those photos of those cars
and they have that piece of paper in the window that looks like it says D-E-T
backward I a backward in a backward in is an I sound by the way it means
children the subtext baby on board please don't like this car up that's
probably a better image for people to to really absorb the videos to get shared
over and over and over again. They're inspirational for the resistance, but
they're not a clear picture. You're only seeing those who made it because those
who didn't weren't able to upload anyway it's just a thought y'all have a good
day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}