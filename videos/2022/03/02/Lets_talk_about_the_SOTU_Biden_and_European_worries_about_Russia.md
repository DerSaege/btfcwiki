---
title: Let's talk about the SOTU, Biden, and European worries about Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=naddJbhUYjw) |
| Published | 2022/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau gives his take on the state of the union address by Biden, mentioning a specific part that gave him pause.
- People in Europe, particularly in NATO countries, have expressed concerns about a potential conventional offensive from Russia.
- Beau reassures that a conventional offensive from Russia against NATO is not likely to happen.
- He points out that Russia is not faring well in Ukraine, making critical errors that NATO had demonstrated to exploit 30 years ago.
- The mistakes made by Russia in Ukraine are being capitalized on by the Ukrainians, which could be unforgiving against NATO.
- Beau explains how a similar situation to what's happening in Ukraine now played out in the first Gulf War, leading to devastating consequences for the Russian convoy.
- Russia's technological and logistical gaps compared to NATO make a conventional fight against NATO highly unlikely.
- Beau suggests that even Russian generals might oppose Putin if he orders a conventional fight with NATO due to its futility.
- Non-NATO European countries, except for Moldova, are unlikely to face significant risks due to the technological gap between Russia and NATO.

### Quotes

- "No disrespect meant to the military in Ukraine. Ukraine is not NATO."
- "They understand that the technological gap that has developed and the logistical and intelligence gaps that have developed were just demonstrated to the world."
- "Imagine what happened in NATO's technology and it is clear that Russian technology has not actually kept up."

### Oneliner

Beau analyzes the state of the union, addresses concerns about a potential Russian offensive against NATO, and underscores Russia's technological and logistical shortcomings compared to NATO.

### Audience

International observers

### On-the-ground actions from transcript

- Keep abreast of international developments and diplomatic relations (implied)

### Whats missing in summary

Insights on the implications of Russia's military capabilities and NATO's strength for international security.

### Tags

#StateOfTheUnion #Russia #NATO #MilitaryTechnology #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the state of the union and Biden and a whole
bunch of questions that came in from people in Europe about Russia and this
weird overlap that occurred.
Uh, since we're going to talk about the state of the union, I know people are
going to ask for like a general take on how it went given the situation, the
state of the world I think he did really well but there was one part in it that
gave me pause and I don't know that it would have if I hadn't received all of
these questions in the previous 12 hours you know because I'm an American and I'm
used to that kind of rhetoric but there's that part in the speech where he
gets all fiery and he's you know he's like we will defend every inch of NATO
NATO territory, making it seem like that's something they might actually have to do.
That's what the questions were about.
Questions from people in Europe, everybody who sent one was in a NATO country, and they're
concerned about a conventional offensive from Russia.
And they're wondering if that's a likely thing.
No, it's not.
really. As has been covered all over the news, it is not going well for Russia in
Ukraine. No disrespect meant to the military in Ukraine. Ukraine is not NATO.
Every Russian war planner is aware of how bad it's going. They're aware that
that they are making mistakes that NATO demonstrated 30 years ago that you
couldn't make if you expected to even stand a chance. And they're doing them
now. So unless Putin has just completely lost the plot, that's not a realistic
thing. Russian war planners understand how badly it's going. When I talk about
it on this channel I tend to focus on the unconventional aspects, the irregular
stuff. The conventional stuff is going even worse. Ukraine should not have been
this much of an issue.
The errors that they're making in Ukraine, yeah, the Ukrainians are making them pay for
it because it's a mistake.
Against NATO, that would be unforgiving.
No Russian war planner believes that it can tangle with NATO now.
You know, I've talked about it on the channel for years, how Russia is a near-peer.
They're not actually a peer nation.
They don't have the same capabilities.
The performance in Ukraine demonstrated that they are even further from a peer than most
people believed.
Those mistakes, that convoy is a perfect example.
Convoy near the capital there.
alone will tell Russian war planners they cannot, they can't mess with NATO because
30 years ago a similar situation occurred, a whole bunch of vehicles massed on a road.
All of those Russian war planners remember what happened on Highway 80 in Iraq during
the first Gulf War.
All the vehicles lined up, some A6 intruders, the aircraft made famous during Vietnam.
They took out the lead vehicles and the end vehicles.
And then NATO pounded those vehicles into the ground for 10 hours.
When the smoke cleared, the devastation was so immense that the incident became known
as the highway of death.
To this day, there's still not an accurate count on the number of vehicles.
It's somewhere between 1,200 and 2,000.
That's a pretty big margin.
The mistake that is being made in Ukraine right now is something that NATO proved it
would exploit 30 years ago using aircraft from Vietnam that the U.S. deemed were obsolete
20 years ago.
The idea that Russia would be able to even remotely hold its own against all of NATO,
it's not a thing.
And every Russian war planner knows that.
So unless Putin has just completely lost it, a conventional fight between Russian forces
and NATO forces because Russia invaded, that's not a thing.
And I think that Russian commanders are so aware of this, I think if Putin actually ordered
it, he might face, let's just call it direct opposition from his generals because they
know it would be pointless. That's not something I would worry about if I was
in a NATO country. Now, for other countries in Europe that aren't part of
NATO, there's a slightly higher risk, but with the exception of Moldova, I wouldn't
that have it on my radar. It is incredibly unlikely. They understand that
the technological gap that has developed and the logistical and
intelligence gaps that have developed were just demonstrated to the world.
What happened on Highway 80 happened 30 years ago. Think about all of the
technological developments that happened in your phone in the last 30 years.
Imagine what happened in NATO's technology and it is clear that Russian
technology has not actually kept up.
It is clear that Russian logistical ability is lacking.
So this isn't something I would worry about.
Anyway it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}