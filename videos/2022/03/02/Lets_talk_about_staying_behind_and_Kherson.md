---
title: Let's talk about staying behind and Kherson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ws3p8nw8ZHA) |
| Published | 2022/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the concept of staying behind when others leave, a topic previously discussed historically.
- Mentions the city of Kherson in Ukraine coming under Russian control and the potential development of stay-behind tactics.
- Describes the goal of stay-behind campaigns as being a thorn and draining the resolve of the opposition.
- Explains that stay-behind organizations wait, watch, and learn before becoming active.
- Points out that the success of such campaigns depends on making the occupying forces relax and divert resources.
- Differentiates between organized and less organized campaigns in terms of how they start.
- Explains that success for resistance forces involves demoralizing the opposition and forcing them to divert resources back.
- Notes the irregular phase where conventional forces may fall, leaving irregular forces to continue resistance.
- Mentions the comparison to tactics used by American forces in the Mideast.
- Speculates on the success of the campaign based on the training and caliber of troops involved.
- Acknowledges uncertainty about whether the campaign will start and its coverage due to being behind Russian lines.
- Suggests getting ready to see potential developments in Ukraine.

### Quotes

- "It's likely something we're going to see start to develop in the coming weeks."
- "This is the beginning of the truly irregular phase."
- "It's incredibly likely that in the beginning it is going to be very, very successful."
- "We probably won't know right away because unlike other events, this isn't going to get a lot of coverage."
- "I don't think that Ukraine's ready to give up yet."

### Oneliner

Beau talks about the potential development of stay-behind tactics in Ukraine, draining the resolve of the opposition in a campaign that may go unnoticed.

### Audience

Ukrainian Resistance Supporters

### On-the-ground actions from transcript

- Prepare for potential developments in Ukraine (implied).

### Whats missing in summary

Details on the historical context of stay-behind tactics and their potential impact on the conflict.

### Tags

#Ukraine #Conflict #Resistance #StayBehindTactics #Russia


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about staying behind
when other people leave.
It's a topic that has come up on the channel before,
something we've talked about.
But when we've talked about it before,
it's been a history lesson.
It's been us talking about how NATO planned
to do it during the Cold War, or how maybe some groups that
were set off kind of went off the rails.
It was normally a history lesson or something for context.
Now, it's likely something we're going
to see start to develop in the coming weeks.
At time of filming, the Ukrainian city of Kherson
has come under Russian control.
This is a city large enough to facilitate
a certain kind of campaign.
The goal of this campaign isn't to retake the city, though.
It's to be a thorn.
It's to be a drain on the resolve of the opposition,
of Russia.
It's a stay behind organization, stay
behind groups, stay behind network.
And as the name suggests, when something like this happens,
there is a group of people who knew
they were going to do this before it happened,
and they stay in the city.
And in the beginning, they don't do anything.
They watch.
They learn.
They wait.
And they hope that during this period of inactivity,
the occupation forces, the opposition,
that they relax a little bit.
They settle into a routine.
Even better, they divert some of their resources.
They send troops to somewhere else.
And then that's when they get active.
Ukraine knows how to do this.
They had the time to set it up.
They had the supplies.
Now we're going to find out if they did.
Now, when they decide to get active,
these campaigns normally start off in one of two ways.
If it is well organized, well supplied, well trained,
they announce themselves.
And by that, I mean a troop barracks or a command post
ceases to be.
If it is less organized or less supplied,
it's kind of a turning up the temperature slowly
type of thing.
It's a shot here, a shot there, in hopes
of inspiring other people within the city who
aren't affiliated with that element of the resistance
to act on their own.
And the idea here is to be just demoralizing,
to break the hammer.
Things that could be counted as successes
would be Russia having to pull troops back
to go back to the city.
And if that happens, everybody just kind of chills out again.
They don't do anything.
And they wait, wait for those forces to relax.
Maybe they get moved off again.
And then it starts right back up.
This is the beginning of the truly irregular phase.
And we're probably going to see more and more of it
because even as well as the conventional forces of Ukraine
have done, Russia has a lot of manpower.
They have a lot of material that they
can throw into the grinder.
And if Putin is committed to this course of action,
the conventional forces can only hold out for so long.
And once the conventional forces fall,
then it's up to the irregular forces.
And that's when the real test comes
for the Ukrainian resistance.
That's when the decision really gets
made as to whether or not it's over
or whether or not Russia walked itself into a conflict that's
going to last years.
American forces who were over in the Mideast,
it's going to be a lot of the same tactics.
A lot of the same stuff's going to get used.
The caliber of troop that Russia has sent,
they're not up to this.
They are not up to this.
If it takes off, if this type of campaign begins,
it is incredibly likely that in the beginning
it is going to be very, very successful
until Russia starts sending in better trained troops
and not relying on conscripts.
It's going to be incredibly successful
because the forces that are there now, a lot of them
at most had four months of training.
They have never even seen a manual
on how to deal with this, much less been
trained on how to do it.
We don't know for certain that this campaign
is going to start.
This is the test.
This is the beginning.
And the thing is we probably won't know right away
because unlike other events, this
isn't going to get a lot of coverage
because it's going to be what amounts to behind Russian lines.
There's not going to be a lot of news coverage of it
in the beginning.
And it's not even going to start.
If they're smart, it won't start for a week at least.
But it's something that we should get ready to see
because I don't think that Ukraine's ready to give up yet.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}