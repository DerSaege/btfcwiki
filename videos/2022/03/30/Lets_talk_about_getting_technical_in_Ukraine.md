---
title: Let's talk about getting technical in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J7HQWlida2k) |
| Published | 2022/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia is using technicals in Ukraine, civilian vehicles mounted with crew-served weapons, behavior typically associated with non-state actors.
- Initially assumed the footage was edited, but the Russian embassy confirmed that the vehicles were Ukrainian and now serving Russia.
- Describes the embarrassment of a country aspiring to be a world power resorting to using technicals.
- Compares the use of technicals to the U.S. military's utilization of Humvees for similar purposes.
- Suggests that Russia's use of technicals may be to conserve gas due to poor mileage of T-72 tanks stolen by Ukrainians.
- Points out the trade-off where Ukraine gets T-72 tanks and Russia gets Toyotas, implying Ukraine might be getting the better end of the deal.
- Speculates that Russia may be using technicals to counter the mud that is hindering their vehicles in Ukraine.
- Mentions the lack of armor on the vehicles, making them vulnerable to small arms fire.
- Views the use of technicals as a sign of Russia's military situation worsening and their desperation in combat scenarios.
- Concludes by expressing concern about Russia's tactical choices and their implications in the ongoing conflict in Ukraine.

### Quotes

- "For a country that wants to be viewed as a near peer to be using technicals is embarrassing on a bunch of levels."
- "This should be your sign that that's not the case."
- "The use of technicals by a country that wants to be viewed as a world power, as a near peer, is just wild."
- "It's not a good sign for the Russian side."
- "Y'all have a good day."

### Oneliner

Russia's use of technicals in Ukraine reveals tactical desperation and military shortcomings, undermining their aspirations as a global power.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Monitor and analyze Russia's military tactics in Ukraine (exemplified)
- Advocate for diplomatic resolutions to conflicts (implied)
- Support efforts to provide aid and assistance to affected civilians in conflict zones (implied)

### Whats missing in summary

Detailed analysis of the potential consequences of Russia's tactical choices in Ukraine.

### Tags

#Russia #Ukraine #MilitaryTactics #GlobalPower #Desperation


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
getting technical in Ukraine. And I'm not going to, you know, bury this one. I'm just
going to go out and say the news so those who understand the term can finish laughing
while I explain what the term means to everybody else. Russia is in fact using technicals in
Ukraine. That's where they're at. For those who don't know what that term means, a technical
is where you take a crew-served weapon such as a machine gun, like a big machine, like
with the belt coming out of the side, or a coilless rifle, something like that, and mount
it to a civilian vehicle, typically a pickup truck, insert Toyota jokes here, although
I did once see a Volvo station wagon with its roof cut out that I thought was really
cool. This is behavior we typically associate with non-state actors. When I first saw the
footage, I assumed the video had been edited. I thought it was a joke put out by the Ukrainian
military. You know, the trucks were rolling by and they had the Z on them. And down below,
the Russian embassy in the United Kingdom had actually confirmed it. They said that
the vehicles were Ukrainian and now they're serving Russia. That's embarrassing. That
is embarrassing. For a country that wants to be viewed as a near peer to be using technicals
is embarrassing on a bunch of levels. A comparable vehicle in the United States would be the
Humvee. That's what we would use, that's what the US would use in this situation to
fill that role. You know, it's armored, it's designed for that purpose. Pressing
civilian vehicles into service like that is something, it is, it's associated with a
non-state actor. Or it would be acceptable for Ukraine to do that in this situation where
you're pressing civilians into service and trying to maintain mobility. So what can we
get from this? What can we kind of pull out from this piece of news? I mean, the first
thing to take away from it is that if you are one of those who believes that Russia
is holding their good stuff in reserve, this should be your sign that that's not the
case. Now, aside from that, why would they be doing this? Probably gas would be my guess.
The T-72s that the Ukrainians keep stealing, they don't exactly get good gas mileage.
So this may be Russia's way of trying to limit consumption. It is interesting to note
that the trade that is occurring here is Ukraine's getting T-72s and Russia's getting Toyotas.
I think Ukraine's getting the better deal. So it may have something to do with gas consumption.
Another reason they may be using it is trying to find some way to counter the mud that is
bogging down their vehicles. I don't know that this is actually going to help. They
are lighter. Pickup trucks do tend to have four-wheel drive, this type. So it may be
a little better, but not much. And there's also the fact that these vehicles aren't
really armored. Most of the ones I saw, they didn't have any additional plating. It didn't
look like they had done anything to the doors. Sometimes they'll take the doors off and
put, take the interior of the door off and put plates in there to protect the drivers.
Didn't look like any of that had been done, but can't be certain. But now the vehicles
they're using can be taken out by small arms fire, rifles. They don't need the javelins
anymore. I mean, I guess you could use one if you wanted. But this does not speak well
of Russia's current military situation. This may be one of those, those signs a lot
like losing all of the generals and stuff like that, that should tell everybody that
they're in an even worse situation than they're letting on. The use of technicals by a country
that wants to be viewed as a world power, as a near peer, is just wild. They do have
some tactical advantages, but I don't think there's any that are going to matter in the
type of combat they're currently fighting. This seems to be more of, we didn't have a
vehicle that could make it through the mud. We're low on gas. Our stuff keeps getting
blown up or stolen, so we're going to try this. It's not a good sign for the Russian
side. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}