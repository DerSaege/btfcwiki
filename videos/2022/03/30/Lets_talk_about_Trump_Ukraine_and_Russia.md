---
title: Let's talk about Trump, Ukraine, and Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HH2bw3GfMx4) |
| Published | 2022/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why Trump's recent statement in response to Russian media shouldn't be surprising.
- Points out that Putin didn't need to counter NATO because Trump was doing it for him by trying to undermine it.
- Mentions headlines showing Trump's actions weakening NATO and boosting Russia.
- Disputes the idea that Trump was willing to protect Ukraine, citing incidents of Russian forces advancing under Trump's presidency.
- Shares a quote from Zelensky about buying anti-tank weapons from the US, to which Trump responded with a famous line.
- Criticizes Trump for prioritizing political interests over national security when asked for help countering Russia.
- Talks about Russian TV broadcasting Trump's supporters as propaganda and a statement on Russian state TV supporting Trump's presidency.
- Concludes that the notion of Trump preventing these actions is laughable and suggests he sought Putin's help in the election.

### Quotes

- "I want to read you something, and part of it you'll recognize, but most of it I don't think most people will."
- "He's worried about going after his political opposition, which is no surprise, right?"
- "It's time to again help our partner Trump to become president."
- "The idea that Trump would have stopped this is laughable."
- "But we know that the Russians think he's on their team."

### Oneliner

Beau explains why Trump's actions benefited Russia and why expecting him to protect Ukraine is unrealistic, with evidence of his detrimental impact on NATO and willingness to seek Putin's support in elections.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Contact elected representatives to advocate for strong alliances like NATO (implied)
- Support international efforts to counter Russian aggression (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's foreign policy decisions and their implications, shedding light on his relationship with Russia.

### Tags

#Trump #Russia #NATO #Ukraine #ForeignPolicy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about something that Trump said
in the response in Russian media and how it is, well, kind of catching on
and how it also shouldn't come as a surprise at all.
But before we get to that, we have to get some background information straight
because there's a lot of debate over things that actually occurred.
You know, the standard right-wing talking point right now is that this wouldn't have happened under Trump.
I mean, there's some truth to that, but let's just kind of examine the truth as it is.
It wouldn't have happened because Vladimir Putin would have had no reason to counter NATO
because he had his buddy Trump trying to destroy it, trying to undermine it at every turn for years.
Let's read some headlines real quick.
Trump discussed pulling US from NATO, 2019.
Trump's attacks on NATO need European action, 2020.
Donald Trump's tirades weaken NATO, please, Vladimir Putin, 2018.
And that's the USA Today.
This isn't a secret.
Donald Trump's Germany pullout will boost Russia, 2020.
Trump weakens US commitment to NATO.
Donald Trump's dangerous NATO strategy.
Putin didn't have to counter NATO.
Trump was countering it for him.
So, I mean, there is a little bit of truth to the statement.
And then what about the idea of him being willing to protect Ukraine?
Because that's part of the talking point.
That, of course, relies on people believing that Russia didn't advance on Ukraine under Trump,
which is a lie.
That's not true.
In fact, the first time that Russian forces openly encountered Ukrainian forces and fired on them
was in 2018, the Kurt Strait incident under Trump.
And he did nothing because he didn't counter Putin.
He doesn't care about Ukraine.
He never did.
He didn't care about European security.
He didn't care about American security.
I want to read you something, and part of it you'll recognize,
but most of it I don't think most people will.
This is Zelensky talking.
We are ready to continue to cooperate for the next steps.
Specifically, we are almost ready to buy more of javelins from the United States for defense purposes.
Javelins are the anti-tank weapon system that right now is the backbone of the Ukrainian defense.
Do you know what Trump said in response to this?
It's a famous line.
I would like you to do us a favor, though.
Yeah, the phone called it, you know, that whole impeachment thing.
He never had any intention on doing it.
Didn't care about it.
When Zelensky is asking him for something that is vitally important to counter Russia,
he's worried about going after his political opposition, which is no surprise, right?
He called for Russia's help before with Hillary.
He wasn't held accountable, so now he's doing it again,
asking for Putin to release information on his political opposition.
While Putin is threatening the world with nuclear annihilation and is an opposition nation,
he's there asking for favors.
Why? Because he did it once, wasn't held accountable, so now he's doing it again.
There's probably a lesson there.
And then we have this.
On Russian TV now, you have Trump's cheerleaders being broadcast because it's good propaganda for Russia.
And then this statement on official Russian state TV,
it says that it's time to again help our partner Trump to become president.
I mean, I think that pretty much covers it.
The idea that Trump would have stopped this is laughable.
And of course, he's reaching out to his friend, Vladimir Putin, for help with the election.
Of course, he's being held up as an ally to Putin in Russian media because he was.
You can argue about whether or not he meant to.
I don't have evidence to say that Trump was an agent of influence.
But we know that the Russians think he's on their team.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}