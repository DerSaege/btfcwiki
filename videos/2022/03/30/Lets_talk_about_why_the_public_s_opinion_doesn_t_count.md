---
title: Let's talk about why the public's opinion doesn't count....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eY4gSKwqV2M) |
| Published | 2022/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the irrelevance of polling American citizens on foreign policy issues and how leaders don't take it into account.
- Breaks down three questions from a poll about Ukraine, showing how differently they are answered based on phrasing.
- Points out the inconsistency between public opinion and headlines about Biden's handling of the Ukraine situation.
- Stresses the importance of informed opinions in decision-making and how less informed opinions are discounted.
- Compares discounting less informed opinions to medical experts not listening to self-appointed medical experts on Facebook.
- Talks about Biden's intentional ambiguity in his statements, aimed at keeping Moscow off balance in understanding US intentions.
- Mentions past criticisms of Trump's approach in Afghanistan and how giving away intent led to predictable outcomes during the withdrawal.
- Concludes by explaining why polling is often disregarded in decision-making.

### Quotes

- "It's good to understand where the American people are sitting, but those who are making these decisions, they do not care what these polls say."
- "This is why education in the US matters. This is why people really should understand this stuff."
- "When he is sitting there and he's talking, most of that information is for Moscow. It's not for us."
- "For those who think this is just me trying to defend Biden, which isn't really something I do, but go back and look at my criticisms when it comes to Trump."
- "So while people may be criticizing it, doesn't mean it's not the right move."

### Oneliner

Polling American citizens on foreign policy issues is irrelevant to decision-makers, as demonstrated by the varied responses to differently phrased questions on Ukraine and the discrepancy between public opinion and headlines about Biden's actions.

### Audience

Citizens, policymakers, voters

### On-the-ground actions from transcript

- Understand foreign policy issues more deeply and form informed opinions (implied)
- Be critical of headlines and seek to understand the full context behind political decisions (implied)

### Whats missing in summary

The full transcript provides detailed insights into the discrepancies between public opinion and foreign policy decisions, as well as the intentional ambiguity in political statements for strategic purposes. Viewing the full text offers a comprehensive understanding of the nuances in polling and decision-making processes.

### Tags

#Polling #ForeignPolicy #PublicOpinion #DecisionMaking #Biden #Trump #Education


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about polling
and why polling of American citizens
when it comes to foreign policy issues is irrelevant
and it doesn't matter.
And it is almost never taken into account
when leaders are making decisions.
Right now, you've probably seen all of the headlines
saying that most Americans feel like Biden
isn't doing enough when it comes to Ukraine.
He's being too soft on Russia is how it's being framed.
So what we're going to do is we're
going to go through three questions from that poll.
And you're going to understand by the end of it
why foreign policy people do not care what
the American public thinks.
OK, so let's start with this question.
Recently, some have called for the North Atlantic Treaty
Organization, which includes the US,
to enforce a no-fly zone over Ukraine.
Would you approve or disapprove of a no-fly zone?
27.26% strongly approve.
41.22% somewhat approve.
Here's another question.
Some have argued that a no-fly zone would result in NATO
entering a war with Russia.
Given this information, would you
approve or disapprove of a no-fly zone over Ukraine?
Suddenly, those numbers drop, and strongly approved
is 16.89%, and somewhat approve is 35.51%.
Then we have this question.
Suppose stronger economic sanctions
do not stop Russia from taking military actions in Ukraine.
Would you favor or oppose deploying US troops
into the conflict?
Drops again.
Strongly is now 15%, and somewhat is 26.85%
or something like that.
That's the same question.
Same question phrased three different ways,
three drastically different answers.
Which one is the most accurate phrasing?
The one that says deploy US troops into the conflict.
And it goes back to that whole boots on the ground conversation
and why drones are more likely to increase intervention,
because we feel like there isn't a risk.
But there's always a risk.
It's always there.
Now, as far as the way it's phrased and the headlines that
are coming out, do you believe the US
has done too much, too little, or just enough to aid Ukraine?
The headline is people believe that he's done too little,
that he's too soft on Russia.
And yeah, too little is 41.17%.
However, just enough is 45.54%, and too much is 13.28%,
meaning that most people, the majority of people,
believe that he's doing the right amount or too much.
Doesn't match with the headlines.
Now, the interesting thing about this, once again,
is that these are all from the same poll.
The same people answered these questions
in these different ways.
This is why foreign policy people, foreign policy experts,
do not care what the American public says,
what the perception is.
It's good to understand where the American people are
sitting, but those who are making these decisions,
they do not care what these polls say.
It's one of those things where people discount
less informed opinions.
And that's what this is.
When you have this massive shift,
three questions that are really the same question,
but they're phrased in different ways,
and you get that wide of a response,
it's safe to just discount that.
This is kind of like medical experts
not listening to the self-appointed medical experts
on Facebook.
It's the same thing.
This is why education in the US matters.
This is why people really should understand this stuff.
Because if people's opinions were more informed,
those who are making the decisions
would probably be more likely to listen to them.
Another one right now that's going around is Biden.
He's saying one thing and saying something else,
and then going back and forth, and we
don't know what to think.
Yeah, that's what he's supposed to do.
Recently, we talked about it on the channel
when we were talking about information operations
and how we, as spectators, wind up getting caught up in it.
When he is sitting there and he's talking,
most of that information is for Moscow.
It's not for us.
We're bystanders in it.
When we talk about intelligence, we say the key thing is intent.
You're trying to figure out your opposition's intent.
If you're confused, as the American public,
you best believe that Moscow is.
Biden's saying, oh, well, you'll find out when you're there.
You're in Ukraine.
Well, maybe we're going to give them this equipment.
Maybe we're going to train them.
Maybe we won't.
We don't know.
It keeps them off balance.
Now, is this some grand plan by the Biden administration?
Probably not.
A lot of this is due to him not being a great speaker.
But it helps.
It doesn't hurt.
It throws them off balance.
It makes it harder for them to discern intent.
For those who think this is just me trying to defend Biden,
which isn't really something I do,
but go back and look at my criticisms
when it comes to Trump.
One of the big things, as soon as the negotiations
started in Afghanistan, it's like, this is over.
They're going to wait.
They're going to do nothing.
They're going to chill out.
And then the second we get ready to leave,
they're going to take over the country, which is what happened.
Because right from the beginning,
Trump was like, we want to leave.
We don't care.
We just want it to stop.
He gave them the intent.
As soon as he did that, what happened during the withdrawal
was inevitable.
So while people may be criticizing it,
doesn't mean it's not the right move.
And when it comes to the polling,
this is why it gets disregarded so often.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}