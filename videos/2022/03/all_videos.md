# All videos from March, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-03-31: Let's talk about mom and social constructs... (<a href="https://youtube.com/watch?v=79P26WSAUXo">watch</a> || <a href="/videos/2022/03/31/Lets_talk_about_mom_and_social_constructs">transcript &amp; editable summary</a>)

Starting with a basic understanding of social constructs can pave the way for acceptance and broader societal change, as Beau explains.

</summary>

"A lot of these things that you believe are just something that can't be changed, it's really just peer pressure from dead bigots."
"This is why gender roles and gender norms and all of this stuff, it's kind of up in the air because we get to determine what it is."
"People have to understand that a whole lot of the things we accept as something that is just set in stone and cannot be changed is made up."
"You want that basic understanding and the understanding that it's OK."
"They may not be a great ally, they may not get out there and join the fight or anything like that, but they're going to be accepting."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of starting with a basic understanding of social constructs to explain concepts like transgenderism.
Emphasizes that once people understand social constructs, they tend to become more accepting.
Suggests finding an example from an older person's past where a social construct shifted to help them understand the concept.
Uses the example of public pools and segregation to illustrate the shift in social constructs.
Mentions the significance of understanding that many beliefs are just peer pressure from past bigots.
Recommends focusing on the fluidity and arbitrary nature of social constructs like race to broaden understanding.
Points out that many accepted beliefs are not grounded in anything concrete but are societal perceptions.
Encourages starting with the core concept of social constructs before delving into more specific issues.
Urges individuals to recognize their place in history and how their views may be judged in the future.
Stresses the need to confront uncomfortable truths about the past and present to move towards a more accepting society.

Actions:

for all individuals seeking to foster understanding and acceptance in their communities.,
Share stories of social construct shifts with older individuals to help them understand and accept new concepts (exemplified).
Encourage open dialogues about the fluidity of social constructs, like race, to broaden perspectives and challenge ingrained beliefs (exemplified).
Initiate educational sessions or workshops on social constructs to deepen community understanding and acceptance (suggested).
</details>
<details>
<summary>
2022-03-31: Let's talk about how intelligence failures happen.... (<a href="https://youtube.com/watch?v=YQF7K4_KuQg">watch</a> || <a href="/videos/2022/03/31/Lets_talk_about_how_intelligence_failures_happen">transcript &amp; editable summary</a>)

Beau explains how intelligence failures stem from groupthink, outdated information, assumptions about the opposition, and political pressure, leading to self-inflicted errors despite available data.

</summary>

"They had the information, they just didn't use it."
"Most of the real errors are self-inflicted."
"In today's age, the information is out there. You just have to look for it."

### AI summary (High error rate! Edit errors on video page)

Explains how intelligence failures occur, despite having access to information.
Identifies four main factors contributing to intelligence assessment failures.
Groupthink is a prevalent issue, where assessments are influenced by persuasive individuals rather than accurate information.
Advises against sharing assessments and estimates to prevent groupthink.
Warns against using outdated information to form estimates, as circumstances change.
Emphasizes the danger of assuming the opposition won't make mistakes due to lack of awareness.
Points out the risk of misjudging the opposition's applied doctrine versus their public statements.
Notes the impact of political pressure on producing estimates that may be more palatable but less accurate.
Stresses the importance of independent analysis to avoid intelligence failures.
Mentions that intelligence failures have been a challenge for the US since the fall of the Berlin Wall.
Draws parallels between current events in Ukraine and past intelligence failures, like those in Iraq.
Encourages seeking accurate information in the age of available data to prevent errors.
Mentions the possibility of intentionally seeded false information leading to inaccurate estimates.

Actions:

for analysts, strategists, policymakers,
Conduct independent analysis to avoid groupthink and reliance on outdated information (implied).
Seek accurate information sources and avoid sharing assessments and estimates widely (implied).
Stay vigilant against political pressure influencing assessments (implied).
</details>
<details>
<summary>
2022-03-31: Let's talk about Amazon in Alabama.... (<a href="https://youtube.com/watch?v=rpPj-AeqzwI">watch</a> || <a href="/videos/2022/03/31/Lets_talk_about_Amazon_in_Alabama">transcript &amp; editable summary</a>)

Update on Bessemer, Alabama union vote on Amazon, reflecting growing momentum for union activity in the U.S., beyond wage fights towards better living standards.

</summary>

"It's fight for 15 in a union."
"They want the dream, the promise that they were handed."
"I'm hopeful for the vote in Amazon."
"I think that the momentum has started and we're going to see a unionized Amazon in the near future."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Update on union news in Bessemer, Alabama regarding Amazon's second vote, results possibly coming in today.
National Labor Relations Board found Amazon unfairly influenced the last vote, prompting a new one.
Union expected to do better this time since they were able to talk to people without pandemic restrictions.
Amazon faces more union votes in New York City, likely to succeed due to local support.
Grievances at Amazon not primarily about pay (employees make more than $15/hour), but about working conditions and standard of living.
Similar to Warrior Met coal mine workers still on strike, Amazon workers seek better conditions and the promise of a decent living.
Union activity organizing in the U.S. gaining momentum beyond expected places like Amazon and Starbucks.
People are looking beyond just dollar amounts, seeking a better standard of living and ownership opportunities.
Hopeful for a union win at Amazon in Bessemer, with potential for success in New York as well.
Momentum is building for unionization at Amazon regardless of the outcome in Bessemer.

Actions:

for workers, activists, supporters,
Support union organizing efforts in your area (implied)
Stay informed about labor rights and support workers' rights movements (implied)
</details>
<details>
<summary>
2022-03-30: Let's talk about why the public's opinion doesn't count.... (<a href="https://youtube.com/watch?v=eY4gSKwqV2M">watch</a> || <a href="/videos/2022/03/30/Lets_talk_about_why_the_public_s_opinion_doesn_t_count">transcript &amp; editable summary</a>)

Polling American citizens on foreign policy issues is irrelevant to decision-makers, as demonstrated by the varied responses to differently phrased questions on Ukraine and the discrepancy between public opinion and headlines about Biden's actions.

</summary>

"It's good to understand where the American people are sitting, but those who are making these decisions, they do not care what these polls say."
"This is why education in the US matters. This is why people really should understand this stuff."
"When he is sitting there and he's talking, most of that information is for Moscow. It's not for us."
"For those who think this is just me trying to defend Biden, which isn't really something I do, but go back and look at my criticisms when it comes to Trump."
"So while people may be criticizing it, doesn't mean it's not the right move."

### AI summary (High error rate! Edit errors on video page)

Explains the irrelevance of polling American citizens on foreign policy issues and how leaders don't take it into account.
Breaks down three questions from a poll about Ukraine, showing how differently they are answered based on phrasing.
Points out the inconsistency between public opinion and headlines about Biden's handling of the Ukraine situation.
Stresses the importance of informed opinions in decision-making and how less informed opinions are discounted.
Compares discounting less informed opinions to medical experts not listening to self-appointed medical experts on Facebook.
Talks about Biden's intentional ambiguity in his statements, aimed at keeping Moscow off balance in understanding US intentions.
Mentions past criticisms of Trump's approach in Afghanistan and how giving away intent led to predictable outcomes during the withdrawal.
Concludes by explaining why polling is often disregarded in decision-making.

Actions:

for citizens, policymakers, voters,
Understand foreign policy issues more deeply and form informed opinions (implied)
Be critical of headlines and seek to understand the full context behind political decisions (implied)
</details>
<details>
<summary>
2022-03-30: Let's talk about getting technical in Ukraine.... (<a href="https://youtube.com/watch?v=J7HQWlida2k">watch</a> || <a href="/videos/2022/03/30/Lets_talk_about_getting_technical_in_Ukraine">transcript &amp; editable summary</a>)

Russia's use of technicals in Ukraine reveals tactical desperation and military shortcomings, undermining their aspirations as a global power.

</summary>

"For a country that wants to be viewed as a near peer to be using technicals is embarrassing on a bunch of levels."
"This should be your sign that that's not the case."
"The use of technicals by a country that wants to be viewed as a world power, as a near peer, is just wild."
"It's not a good sign for the Russian side."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Russia is using technicals in Ukraine, civilian vehicles mounted with crew-served weapons, behavior typically associated with non-state actors.
Initially assumed the footage was edited, but the Russian embassy confirmed that the vehicles were Ukrainian and now serving Russia.
Describes the embarrassment of a country aspiring to be a world power resorting to using technicals.
Compares the use of technicals to the U.S. military's utilization of Humvees for similar purposes.
Suggests that Russia's use of technicals may be to conserve gas due to poor mileage of T-72 tanks stolen by Ukrainians.
Points out the trade-off where Ukraine gets T-72 tanks and Russia gets Toyotas, implying Ukraine might be getting the better end of the deal.
Speculates that Russia may be using technicals to counter the mud that is hindering their vehicles in Ukraine.
Mentions the lack of armor on the vehicles, making them vulnerable to small arms fire.
Views the use of technicals as a sign of Russia's military situation worsening and their desperation in combat scenarios.
Concludes by expressing concern about Russia's tactical choices and their implications in the ongoing conflict in Ukraine.

Actions:

for military analysts, policymakers,
Monitor and analyze Russia's military tactics in Ukraine (exemplified)
Advocate for diplomatic resolutions to conflicts (implied)
Support efforts to provide aid and assistance to affected civilians in conflict zones (implied)
</details>
<details>
<summary>
2022-03-30: Let's talk about Trump, Ukraine, and Russia.... (<a href="https://youtube.com/watch?v=HH2bw3GfMx4">watch</a> || <a href="/videos/2022/03/30/Lets_talk_about_Trump_Ukraine_and_Russia">transcript &amp; editable summary</a>)

Beau explains why Trump's actions benefited Russia and why expecting him to protect Ukraine is unrealistic, with evidence of his detrimental impact on NATO and willingness to seek Putin's support in elections.

</summary>

"I want to read you something, and part of it you'll recognize, but most of it I don't think most people will."
"He's worried about going after his political opposition, which is no surprise, right?"
"It's time to again help our partner Trump to become president."
"The idea that Trump would have stopped this is laughable."
"But we know that the Russians think he's on their team."

### AI summary (High error rate! Edit errors on video page)

Explains why Trump's recent statement in response to Russian media shouldn't be surprising.
Points out that Putin didn't need to counter NATO because Trump was doing it for him by trying to undermine it.
Mentions headlines showing Trump's actions weakening NATO and boosting Russia.
Disputes the idea that Trump was willing to protect Ukraine, citing incidents of Russian forces advancing under Trump's presidency.
Shares a quote from Zelensky about buying anti-tank weapons from the US, to which Trump responded with a famous line.
Criticizes Trump for prioritizing political interests over national security when asked for help countering Russia.
Talks about Russian TV broadcasting Trump's supporters as propaganda and a statement on Russian state TV supporting Trump's presidency.
Concludes that the notion of Trump preventing these actions is laughable and suggests he sought Putin's help in the election.

Actions:

for voters, political analysts,
Contact elected representatives to advocate for strong alliances like NATO (implied)
Support international efforts to counter Russian aggression (implied)
</details>
<details>
<summary>
2022-03-29: Let's talk about planting trees and building community.... (<a href="https://youtube.com/watch?v=H73sGB5bIIA">watch</a> || <a href="/videos/2022/03/29/Lets_talk_about_planting_trees_and_building_community">transcript &amp; editable summary</a>)

Beau advocates for planting fruit trees not just for food but to build a strong community network, encouraging focusing on the time spent together rather than just the end result.

</summary>

"Make it about the time rather than just the fruit."
"The best time to plant a tree was twenty years ago. The second best time is today."
"Approach it as family time."
"There are so many benefits to this that there's got to be something that appeals to them."
"Once people start, I've never known anybody to start doing this who stopped."

### AI summary (High error rate! Edit errors on video page)

Advocates for planting yard for food rather than lawns, promoting the idea of replacing lawns with food-producing plants like fruit trees and raised beds.
Shares experiences of planting peach and pear trees, humorously noting the inevitable dirtiness that comes with gardening.
Emphasizes the long time it takes for fruit trees to bear fruit, suggesting focusing on the time spent together rather than just the end result.
Points out the benefits of planting fruit trees to increase food security, have control over the food on the table, and build a strong community network.
Encourages making planting trees a community or family activity to strengthen relationships and create lasting bonds.
Views planting fruit trees as a starting point for building a strong community network that can provide support in everyday life and natural disasters.
Mentions the importance of developing a strong community network to alleviate worries and solve problems collectively.
Suggests approaching planting trees as family time and a screen-free activity that brings everyone together.
Stresses the numerous benefits of planting fruit trees, mentioning that once people start, they rarely stop due to the positive outcomes it brings.

Actions:

for gardeners, community builders, families,
Organize a community gardening day where everyone plants fruit trees together, creating a strong community bond (implied).
</details>
<details>
<summary>
2022-03-29: Let's talk about mapping the unthinkable.... (<a href="https://youtube.com/watch?v=NhPa1t7M_4E">watch</a> || <a href="/videos/2022/03/29/Lets_talk_about_mapping_the_unthinkable">transcript &amp; editable summary</a>)

Beau addresses American concerns over a potential domestic nuclear threat, advocating for knowledge over fear and showcasing a tool to understand nuclear impacts better.

</summary>

"Knowledge hinders fear."
"There's no reason to let this fear rule your life."
"The numbers in that poll were really high, so maybe a little bit of information might help calm people's nerves."

### AI summary (High error rate! Edit errors on video page)

Shares insights on a recent poll from the AP on American perceptions regarding a potential nuclear threat within the country.
Notes that 80% of Americans expressed concern about the possibility of a nuclear event occurring domestically.
Emphasizes the importance of knowledge in reducing fear and introduces a tool to understand nuclear impacts better.
Recommends using nukesecrecy.com/nukemap to visualize nuclear scenarios, including location, yield, and fallout rings.
Mentions that maps typically depict a 2,000 warhead exchange, but suggests focusing on a 500 warhead scenario for a clearer picture.
Describes the presets for Russian warheads ranging from 300 to 800 kilotons, with larger devices included as novelty items.
Encourages individuals to gather information on nuclear threats but cautions against letting fear dominate their lives.
Acknowledges historical proximity to nuclear disasters and the subtle nature of these risks.
Urges viewers to seek information and use tools like the mentioned map to understand their proximity to potential nuclear events.
Concludes by suggesting that while concerns are valid, excessive fear might not be warranted given the low probability of a nuclear incident.

Actions:

for general public,
Visit nukesecrecy.com/nukemap to understand potential nuclear scenarios (suggested).
Gather information on nuclear threats from reliable sources (suggested).
</details>
<details>
<summary>
2022-03-29: Let's talk about Trump, a judge, DOJ, and a ruling..... (<a href="https://youtube.com/watch?v=KuuBwNooYr4">watch</a> || <a href="/videos/2022/03/29/Lets_talk_about_Trump_a_judge_DOJ_and_a_ruling">transcript &amp; editable summary</a>)

Beau breaks down a ruling hinting at Trump's obstructive actions on January 6, urging DOJ to act and uphold the presidency institution.

</summary>

"Based on the evidence, the court finds it more likely than not that President Trump corruptly attempted to obstruct the joint session of Congress on January 6, 2021."
"Allowing those actions to not be properly investigated or to be swept under the rug is the exact opposite of protecting the institution."
"This ruling, while minor in a lot of ways, is probably going to have some big impacts."

### AI summary (High error rate! Edit errors on video page)

Trump World news and a ruling about emails hint at shaping events.
Committee sought access to emails; lawyer cited attorney-client privilege.
Judge ordered the emails to be turned over, citing evidence of Trump's corrupt obstruction on January 6, 2021.
The judge's use of "corruptly" implies a federal crime by the former president.
DOJ now faces pressure to act with a federal judge pointing to high likelihood of a crime.
DOJ can act independently of the January 6th committee's referral.
American people may question DOJ's inaction following the judge's ruling.
Protecting the presidency institution is a longstanding tradition in the U.S.
DOJ's handling of this case will determine its commitment to upholding this tradition.
Failure to properly investigate actions undermining the presidency goes against protecting the institution.
DOJ must decide its course of action post this ruling.
The ruling may have significant impacts despite being seemingly minor.
DOJ's lack of action may lead to public uproar if not transparent in their investigations.
The importance lies in the implications and potential consequences of the judge's ruling.

Actions:

for american citizens,
Contact your representatives to demand transparency and accountability from the DOJ (suggested).
Stay informed about developments in this case and share them with your community (implied).
</details>
<details>
<summary>
2022-03-28: Let's talk about how we know Russia is lying.... (<a href="https://youtube.com/watch?v=F7xeddeyj3A">watch</a> || <a href="/videos/2022/03/28/Lets_talk_about_how_we_know_Russia_is_lying">transcript &amp; editable summary</a>)

Beau explains how Russia's actions in Ukraine clearly indicate an attempt to take over the entire country, despite their claims otherwise, with Ukraine's strategic moves thwarting their plans.

</summary>

"Russia's goal was to take over the whole country of Ukraine."
"If this was a distraction, they're even worse generals."
"The only surprising aspect is that Ukraine still has conventional forces."

### AI summary (High error rate! Edit errors on video page)

Russia's goal was to take over the whole country of Ukraine, not just settle for a small area.
He believes Russia's claim of not intending to take the capital is not truthful.
Beau explains how Russia's actions in Ukraine clearly indicate an attempt to take over the entire country.
He references a video he made on February 10th, accurately predicting Russia's military strategy in Ukraine before the war started.
Beau describes how Ukraine's resistance and strategic moves have thwarted Russia's attempts to take over the country.
Russia's failed attempts, including a decapitation strike and paratrooper insertion, support the fact that their goal was to take the capital.
Russia's actions, giving Ukraine a month to prepare and then invading, are seen as a huge error in military strategy.
Beau points out that Russia's failure to achieve its goals in Ukraine makes them look incompetent in military planning.
Despite the conflict, Ukraine still maintains conventional forces and has even increased its tank numbers.
Beau concludes that Russia's actions in Ukraine match the operational template he outlined in a previous video about taking over a whole country.

Actions:

for military analysts, policymakers,
Watch Beau's video from February 10th to understand the accurate prediction of Russia's military strategy in Ukraine (suggested).
Study military strategy and operational aspects to better comprehend the ongoing conflict (implied).
</details>
<details>
<summary>
2022-03-28: Let's talk about a follow up on major powers.... (<a href="https://youtube.com/watch?v=fCNntReQWjk">watch</a> || <a href="/videos/2022/03/28/Lets_talk_about_a_follow_up_on_major_powers">transcript &amp; editable summary</a>)

Beau responds to a viewer's message, comparing major powers' imperialism, explaining US economic empire maintenance, and advocating for a broader perspective to transcend nationalism for a better world.

</summary>

"No major power is good."
"Foreign policy isn't about right and wrong, good and evil. It's about power."
"Nationalism is politics for basic people."
"We have more in common with the soldiers in Ukraine than with our own representative in DC."
"We have to start thinking broader than a border."

### AI summary (High error rate! Edit errors on video page)

Responds to a viewer's message about NATO and whether major powers are inherently good.
Compares Russia and the US in terms of imperialism and maintaining empire.
Explains how the US maintains its empire primarily through economic power.
Talks about China's strategy of state-to-state deals for stability and economic gain.
Emphasizes that foreign policy is about power, not right or wrong.
Stresses the need to move past nationalism for a better world.
Advocates for a broader perspective that transcends borders and focuses on commonalities among people.

Actions:

for global citizens,
Communicate with people globally to foster understanding and unity (implied)
Shift focus from nationalism to global commonalities for international political change (implied)
</details>
<details>
<summary>
2022-03-27: Let's talk about a question about the US and NATO's honesty.... (<a href="https://youtube.com/watch?v=719eAvh8Ir8">watch</a> || <a href="/videos/2022/03/27/Lets_talk_about_a_question_about_the_US_and_NATO_s_honesty">transcript &amp; editable summary</a>)

Beau challenges the perception that only the US lies about wars, stating that all major powers engaging in military interventions behave similarly on the international stage.

</summary>

"Our entire lives, it's been the US lying about wars."
"It isn't that a particular state using violence on the international scene is bad. It's that all states using violence on the international scene are bad."
"It's just that this is what large powers do."
"Every country does it."
"Now that other countries are starting to make plays on the international scene, you're going to see it more and more."

### AI summary (High error rate! Edit errors on video page)

Talks about NATO, the US, and foreign policy, particularly addressing the perception younger people have.
Shares a message he received from a viewer questioning why the US isn't seen as the bad guy in conflicts.
Beau challenges the perception that only the US has lied about wars throughout history.
Explains that all major powers engaging in military interventions lie, deceive, and manufacture consent.
Points out that the behavior associated with the US and NATO is not unique to them; all major powers behave similarly.
Emphasizes that interventions by major powers are inherently problematic, regardless of the country or alliance involved.
Notes that the viewer's perception is shaped by the dominance of the US and NATO on the international stage in recent history.
Suggests that as other countries become more active internationally, similar behavior will become more visible.
Draws parallels between the actions of different countries in international conflicts.
Concludes by stating that the behavior of large powers in international affairs has been consistent throughout history.

Actions:

for younger viewers under 30,
Challenge perceptions about specific countries' actions (suggested)
Advocate for transparency in international affairs (implied)
</details>
<details>
<summary>
2022-03-27: Let's talk about a question about gender.... (<a href="https://youtube.com/watch?v=vQ53lVyi4so">watch</a> || <a href="/videos/2022/03/27/Lets_talk_about_a_question_about_gender">transcript &amp; editable summary</a>)

Beau Young explains how gender is a societal perception, not linked to biology, debunking the manipulation by politicians.

</summary>

"Gender is a perception. It's not linked to biology."
"It's a societal perception."
"Politicians manipulate the gender and biology debate to control those people who see this as a serious affront to their values."
"Gender is not linked to biology in any way."
"A whole lot of societies throughout history had more than two genders, just so you know."

### AI summary (High error rate! Edit errors on video page)

Addressing a question on gender and biology that has arisen in various platforms, including Congress.
Explaining societal norms around gendered colors and toys, showcasing the lack of biological link.
Pointing out how politicians use gender and biology debates as a tool for manipulation.
Gender is a societal perception that changes as societal views evolve.
Gender is not inherently tied to biology; it's a perception shaped by society.
Pink and blue being associated with girls and boys is a societal norm, not a biological fact.
The gendering of products like toys, coffee cups, and marketing strategies is based on societal norms, not biology.
Society's perception of gender changes over time, showing its lack of biological basis.
Politicians manipulate the gender and biology debate to control certain groups by creating division and distraction.
Gender is not fixed and has been viewed differently in various societies throughout history.

Actions:

for educators, activists, parents,
Challenge gender stereotypes by promoting gender-neutral colors and toys (exemplified)
Educate others on the societal perception of gender (suggested)
Advocate for inclusive policies and representations of gender diversity (suggested)
</details>
<details>
<summary>
2022-03-26: Let's talk about the arctic and antarctic... (<a href="https://youtube.com/watch?v=6mSRi919jb4">watch</a> || <a href="/videos/2022/03/26/Lets_talk_about_the_arctic_and_antarctic">transcript &amp; editable summary</a>)

Unprecedented warm weather at the poles signals the urgent need for bold climate action now, bridging political divides for a sustainable future.

</summary>

"The reliance on old technology, stuff that burns, right? It's a foreign policy issue. It's a defense issue. It's an economic issue."
"The longer we wait, the worse it gets."
"We're going to experience impacts from it. We're going to have problems already."
"There is no do-over on this."
"We have to make the case and we've got to do it soon."

### AI summary (High error rate! Edit errors on video page)

Unprecedented warm weather at the poles with Antarctica 40 degrees above average and the Arctic 30 degrees above average.
Both the Arctic and Antarctica should be experiencing different seasons, but they are both warm, leading to melting in both places.
Extreme weather events are linked to these temperature anomalies.
Experts suggest this may be a freak occurrence, but the situation needs careful monitoring as it could be a bad sign if it repeats.
Urges for a shift from outdated tech causing problems to address environmental, foreign policy, defense, and economic issues.
Emphasizes the need for bold action now to avoid worsening impacts of climate change.
Calls for someone, possibly from the Republican Party, to step up and advocate for urgent climate action to bridge the political divide.
Warns of irreversible impacts and stresses the importance of addressing climate change promptly.

Actions:

for climate advocates, policymakers,
Advocate for urgent climate action within your community and with policymakers (suggested)
Stay informed about climate change impacts and spread awareness (exemplified)
Support political leaders who prioritize environmental issues (implied)
</details>
<details>
<summary>
2022-03-26: Let's talk about Russia's new plan, numbers, and comparisons.... (<a href="https://youtube.com/watch?v=y6LyFFBQIYk">watch</a> || <a href="/videos/2022/03/26/Lets_talk_about_Russia_s_new_plan_numbers_and_comparisons">transcript &amp; editable summary</a>)

Beau explains the flawed Russian strategy and catastrophic failure in Ukraine, debunking theories of intentional losses and underscoring the significant casualties incurred.

</summary>

"This is not planned. It is more than Afghanistan, Iraq, both times."
"This is a catastrophic failure. It's not some 4D chess that Putin's playing. They failed."
"This theory is garbage. It needs to go away."
"It is not going according to plan, unless the plan was to lose in a spectacular fashion."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains how Russia's strategy is not going according to plan.
Compares estimates of Russian losses from different sources.
Points out discrepancies in Russia's official loss estimate.
Mentions a Russian news outlet publishing and then deleting a more accurate loss estimate.
Analyzes the comparison of Russian troop losses to U.S. combat deaths.
Disputes the theory that Russia is strategically sacrificing weaker units first.
Provides insight into the types of military equipment being used by Russia in the conflict.
Emphasizes the significant failure and high casualty rate on Russia's side.
Concludes that the situation is far from planned and constitutes a catastrophic failure.
Suggests that Russia may be shifting its focus and abandoning certain strategic offensives.

Actions:

for analytical viewers,
Monitor international news outlets for updates on the conflict (implied).
Support diplomatic efforts to end the conflict (implied).
</details>
<details>
<summary>
2022-03-26: Let's talk about Putin changing his victory conditions.... (<a href="https://youtube.com/watch?v=rppmae_tqSY">watch</a> || <a href="/videos/2022/03/26/Lets_talk_about_Putin_changing_his_victory_conditions">transcript &amp; editable summary</a>)

Putin's shift in victory conditions reveals weaknesses and costs, leading Beau to believe Russia can't achieve a true victory.

</summary>

"Even if he wins, he lost."
"There is no winning this for Putin anymore."

### AI summary (High error rate! Edit errors on video page)

Putin has shifted victory conditions by focusing on areas previously contested prior to the war.
He now plans to create a land bridge to connect these areas.
Putin called the drive towards the capital a distraction, which Beau calls a lie.
Beau expresses frustration at the disregard for the lives lost in this so-called distraction.
The Ukrainian military has shown success in counter-offensives around the capital.
There's doubt about Russia being able to hold the areas they are targeting now.
Beau suggests that Putin may still be misled by his generals regarding logistics issues.
The Russian economy is struggling, making it hard to sustain the offensive.
Even if Putin "wins," the war has already been lost due to significant costs and weaknesses exposed in the Russian military.
Beau believes there is no way for Russia to walk away with a true victory.

Actions:

for activists, policymakers, analysts,
Support Ukrainian aid efforts (implied)
Stay informed on the situation and spread awareness (implied)
</details>
<details>
<summary>
2022-03-25: Let's talk about possibilities of Ukraine winning conventionally.... (<a href="https://youtube.com/watch?v=hjMWa2rgGD0">watch</a> || <a href="/videos/2022/03/25/Lets_talk_about_possibilities_of_Ukraine_winning_conventionally">transcript &amp; editable summary</a>)

Beau explains Ukraine's potential to win conventionally against Russia due to strategic moves and shifts in initiative, marking a military miracle in progress.

</summary>

"This is nothing short of a military miracle."
"Once those lines start collapsing, the Russian morale is just going to drop like a rock."
"They've got a really good chance of greatly altering the way those maps look."

### AI summary (High error rate! Edit errors on video page)

Explains the possibility of Ukraine winning conventionally against Russia.
Mentions that Western reporting lacks critical information about the situation.
Points out Ukrainian forces making strategic moves to potentially encircle Russian troops.
Describes the significance of Ukraine gaining the initiative by encircling Russian troops.
Notes the importance of Russian logistics and morale in the conflict.
Emphasizes the unpredictability and unlikelihood of Ukraine's potential conventional victory.
Urges to monitor the northwestern side of the capital for possible developments.
Indicates that Russian advance has halted, creating a stalemate.
Stresses the potential for Ukraine to shift the initiative and put Russia on the defensive.
Concludes by encouraging viewers to stay updated on the situation.

Actions:

for global citizens, analysts,
Monitor and analyze developments on the northwestern side of the capital (suggested)
Stay informed about the ongoing conflict and potential shifts in initiative (suggested)
</details>
<details>
<summary>
2022-03-25: Let's talk about direct NATO/US involvement.... (<a href="https://youtube.com/watch?v=ofYarlMoh2g">watch</a> || <a href="/videos/2022/03/25/Lets_talk_about_direct_NATO_US_involvement">transcript &amp; editable summary</a>)

Beau believes in Ukraine's ability to succeed independently and opposes direct NATO involvement in the conflict, citing the significance of self-reliance in shaping a powerful nation.

</summary>

"I don't like the idea of NATO getting involved directly."
"Wars are typically the things that make nations, that make them powerful."
"If they come out of this on the other side, and they did it on their own, they will be one of the major powers in Europe pretty quickly."

### AI summary (High error rate! Edit errors on video page)

Beau revisits the question of whether the US or NATO should directly intervene in Ukraine, particularly in light of his previous statement about Mariupol possibly being a turning point.
He maintains his long-standing support for providing Ukraine with the tools to defend themselves rather than direct intervention.
Beau acknowledges there are respected analysts who advocate for Western involvement, but he personally believes Ukraine has the ability to succeed on their own.
The risk versus reward calculation is a key factor for Beau, with the potential escalation of conflict being a major concern.
He views Ukraine's ability to win as a critical factor and believes that achieving victory independently will solidify their position as a significant power.
Beau draws a parallel with the War of 1812 and how Ukraine's successful defense could elevate them to a major global player.
While recognizing differing opinions, Beau stresses his current stance against NATO intervention unless circumstances drastically change.
He underlines the importance of Ukraine maintaining its independence and strength through self-reliance in overcoming the conflict.
Beau acknowledges the uncertainty of war but expresses confidence in Ukraine's capabilities if they maintain the will to fight.
He concludes by advocating for Ukraine to emerge stronger and more independent from the conflict, potentially becoming a powerful nation in Europe.

Actions:

for global citizens,
Support organizations aiding Ukraine (exemplified)
</details>
<details>
<summary>
2022-03-25: Let's talk about a free library to satisfy your curiosity.... (<a href="https://youtube.com/watch?v=EQk-h9xWmpM">watch</a> || <a href="/videos/2022/03/25/Lets_talk_about_a_free_library_to_satisfy_your_curiosity">transcript &amp; editable summary</a>)

Beau talks about the importance of curiosity, providing access to free military manuals on archive.org funded by taxpayers.

</summary>

"So don't hesitate to use it."
"You paid for these texts to be developed."
"It's a great resource, and it's free."

### AI summary (High error rate! Edit errors on video page)

Talks about people being curious and wanting to know more about different subjects.
Mentions difficulty in finding reliable information on various topics.
Provides a link to archive.org where hundreds of military field manuals and technical manuals are available for download.
Points out that the US military is a self-contained society with manuals on various subjects like home improvement, plumbing, electrical work, and carpentry.
Emphasizes that the texts available for download were developed using taxpayer money.
Mentions that while most manuals are declassified, companies that reprint them usually focus on those of interest to survivalists.
Notes that archive.org has a wide range of manuals available, including some from the 70s, with still relevant information on home improvement and auto repair.
Encourages browsing through the titles even if not interested in war-related topics as they can be downloaded for free.
Suggests downloading manuals for offline use in case of emergencies.
Reminds viewers that they have already paid for these resources and should not hesitate to use them.

Actions:

for knowledge seekers,
Visit archive.org and download military field manuals and technical manuals (suggested)
Browse through the available titles on archive.org for various subjects (suggested)
Print out manuals for offline use in case of emergencies (suggested)
</details>
<details>
<summary>
2022-03-24: Let's talk about hypersonic tech and masculine militaries.... (<a href="https://youtube.com/watch?v=0ZDPs_c0tX4">watch</a> || <a href="/videos/2022/03/24/Lets_talk_about_hypersonic_tech_and_masculine_militaries">transcript &amp; editable summary</a>)

Beau dives into the resurgence of super-masculine military ideals, contrasting diverse approaches between Russia, Ukraine, and the US, underscoring the vital role of understanding opposition through diversity in achieving military success.

</summary>

"Maybe that hypermasculine stuff isn't what it's got out to be."
"Understanding your opposition is important. It wins wars."
"If you know your enemy and yourself, you need not fear the result of a hundred battles."
"If they assimilate and they just become another cog in the wheel, they don't provide the added insight."
"It's worth just acknowledging that and being ready to see it."

### AI summary (High error rate! Edit errors on video page)

Exploring the resurgence of the idea of a super-masculine military.
Russia's use of hypersonic technology in war.
Contrasting approaches between Russia and the United States in military tactics.
Defending the importance of diversity training for troops.
Ukraine's diverse military composition effectively halting the super-masculine Russian military.
Emphasizing the value of understanding opposition through diversity.
Referencing historical military wisdom and the importance of knowing one's enemy.
The traditional belief that understanding one's opposition leads to victory.
The negative implications of creating an outgroup by dismissing diversity.
The significance of cultural understanding in military success.
US Army Special Forces' cultural training as a key to their success.
Warning about the negative tone associated with dismissing diversity.
Acknowledging the potentially harmful framing of the discourse on military masculinity.

Actions:

for peace advocates, military analysts,
Understand and advocate for diversity training in military institutions (implied).
Support diverse military compositions for better understanding of opposition (implied).
Emphasize cultural understanding and training in military operations (implied).
</details>
<details>
<summary>
2022-03-24: Let's talk about Biden's trip and Putin's mistakes.... (<a href="https://youtube.com/watch?v=ceLppv1UqTk">watch</a> || <a href="/videos/2022/03/24/Lets_talk_about_Biden_s_trip_and_Putin_s_mistakes">transcript &amp; editable summary</a>)

Biden's trip to Europe reveals Putin's strategic miscalculations and the potential creation of an EU army, while increasing pressure through sanctions and preparing for possible Russian threats.

</summary>

"Putin wanted the West to move its troops back, get them out of Eastern Europe and back into traditional NATO members. That's not happening."
"Putin lost in every way, shape, and form."
"Zelensky offered an olive branch and said, hey, I'm open to agreeing to a stipulation saying we won't join NATO."
"Biden is expected to announce another round of sanctions, this one targeting by last count like 400 people individually in Russia."
"Overall, on the world stage, Putin lost in every way, shape, and form."

### AI summary (High error rate! Edit errors on video page)

Biden's trip to Europe reveals insights into the scene in Europe and implications for Putin's long-term goals and mistakes.
Putin wanted Western troops out of Eastern Europe back into traditional NATO member states from the Cold War, but this plan has backfired.
Putin's actions have led to the US increasing troop numbers by 20,000 and caused Germany to re-evaluate its defense policy.
Rather than weakening NATO, Putin's actions have put the alliance on alert and prompted talk of creating a separate EU army, which could challenge NATO.
Biden is expected to announce more sanctions targeting individuals in Russia, primarily elected members of government, to increase pressure on Putin.
There are concerns about potential cyber attacks, the use of chemical weapons, and even nuclear threats from Russia.
The Kremlin stated that Russia would only use nuclear weapons if facing an existential threat, clarifying that it's part of Russian doctrine.
Despite concerns, it's uncertain if Russia will actually use nuclear weapons, with chemical weapons being considered a more likely option.
The Biden administration is preparing for various scenarios, including cyber attacks and potential unconventional warfare from Russia.
Overall, Putin's actions have resulted in strategic losses on the world stage, failing to achieve his objectives both outside and inside Ukraine.

Actions:

for policymakers, analysts, activists,
Monitor developments in Europe and Russia closely for potential impacts on global security (implied).
Support diplomatic efforts to de-escalate tensions and prevent conflict escalation (implied).
Stay informed about the implications of NATO actions and potential responses from Russia (implied).
</details>
<details>
<summary>
2022-03-24: Let's talk about Abbott's Operation Lone Star.... (<a href="https://youtube.com/watch?v=KZWVrjmTVmg">watch</a> || <a href="/videos/2022/03/24/Lets_talk_about_Abbott_s_Operation_Lone_Star">transcript &amp; editable summary</a>)

Beau breaks down Operation Lone Star in Texas, exposing its flaws and political motives while questioning its claimed success and high cost.

</summary>

"Operation Lone Star is using the Texas Military Department, National Guard troops under state authority."
"It's a stunt. It's an election device."
"Imagine what else Texas could use that money for."
"Operation that is so bad, it has National Guard troops trying to unionize."
"Not really that successful."

### AI summary (High error rate! Edit errors on video page)

Operation Lone Star in Texas uses the Texas Military Department and National Guard troops under state authority.
Troops are attempting to unionize due to poor conditions.
ProPublica, a news outlet, found issues with Operation Lone Star's success claims.
Texas is taking credit for arrests and seizures that occurred before the operation.
Governor Abbott's claims of success with Operation Lone Star are being called into question.
Many of the claimed arrests are unrelated to the border situation.
Only a fraction of the claimed fentanyl seizures actually came from Operation Lone Star.
The operation is seen as a political stunt and an election device.
It aims to portray certain people as bad and the governor as taking action.
The operation costs Texas $2.5 million a week, totaling $3 billion.
Beau suggests that the money could be better spent, possibly on improving the electric grid.
Operation Lone Star's lack of success is indicated by the troops' attempt to unionize.

Actions:

for texans, activists,
Contact local representatives to express concerns about Operation Lone Star's effectiveness and cost (implied).
Support investigative journalism like ProPublica by sharing their reports and subscribing (implied).
</details>
<details>
<summary>
2022-03-23: Let's talk about the Supreme Court and the LSAT.... (<a href="https://youtube.com/watch?v=XwGMtXQdS8c">watch</a> || <a href="/videos/2022/03/23/Lets_talk_about_the_Supreme_Court_and_the_LSAT">transcript &amp; editable summary</a>)

Beau explains the unjust focus on LSAT scores to discredit the highly qualified black woman Supreme Court nominee.

</summary>

"There's no reason to be questioning her LSAT score. It was good enough to get her into Harvard."
"She has more experience, a wider range of experience, than anybody on the court."
"It's just manufacturing an issue to turn people against someone that they can other."

### AI summary (High error rate! Edit errors on video page)

Explains the current process of nominating a new justice to the Supreme Court, the first-ever black woman nominee.
Notes the lack of diversity on the current Supreme Court and the unique legal experience of the nominee.
Criticizes attempts to discredit the nominee by focusing on her LSAT scores, despite her extensive legal background.
Provides insights into what the LSAT test is and its purpose for law school admission.
Mentions the nominee's acceptance into Harvard Law School and her graduation with honors.
Points out the manufactured controversy around questioning the nominee's qualifications based on irrelevant factors like LSAT scores.
Concludes that the opposition is more about her being a black woman rather than her qualifications.

Actions:

for legal system observers,
Support and advocate for qualified diverse Supreme Court nominees (exemplified)
Challenge manufactured controversies and biases in judicial nominations (exemplified)
</details>
<details>
<summary>
2022-03-23: Let's talk about Ukraine political parties and the mirror.... (<a href="https://youtube.com/watch?v=aWCgFVmkgOs">watch</a> || <a href="/videos/2022/03/23/Lets_talk_about_Ukraine_political_parties_and_the_mirror">transcript &amp; editable summary</a>)

Beau examines Ukraine's political crackdown through a lens of war realities, cautioning against violent rhetoric and urging Americans to self-reflect on potential parallels.

</summary>

"War is bad. It is dirty. It is nasty. It is ugly. It is cruel."
"If you are calling to restore the Republic for the Glorious Revolution that requires violence, you don't love this country."
"We acknowledge that we are much closer to looking like that than we may really want to admit."

### AI summary (High error rate! Edit errors on video page)

Zelensky and the National Security Council in Ukraine suspended 11 political parties in Ukraine due to their pro-Russian sympathies.
The opposition party for life, a significant banned party, has ties to Moscow, including a main figure with connections to Vladimir Putin.
These banned parties were suspected of collaborating with Russia, possibly sharing information or being potential puppet governments.
Beau questions the decision to ban the parties, suggesting imprisonment might have been a more suitable action.
He points out the realities of war in Ukraine, with death and destruction, justifying the need to remove potential threats.
Beau draws parallels to historical actions in the US during times of threat, challenging notions of freedom of speech versus national security.
He warns against glorifying violent rhetoric and politicians posing with guns, as seen in Ukraine, and urges self-reflection for Americans.
Beau stresses the importance of acknowledging the harsh realities of war and avoiding the idealization of violence and conflict.
He calls for a mirror to be held up to American exceptionalism and a recognition of the potential for similar actions in the US.
Beau concludes by urging viewers to learn from Ukraine’s situation and recognize the dangers of escalating rhetoric and political tensions.

Actions:

for americans,
Hold a mirror up to American exceptionalism and violent rhetoric (suggested)
Refrain from glorifying violence and militaristic politicians (implied)
Acknowledge the risks of escalating political tensions (implied)
</details>
<details>
<summary>
2022-03-23: Let's talk about Putin's problem and Germany's defense spending.... (<a href="https://youtube.com/watch?v=-IdwjJUtuJE">watch</a> || <a href="/videos/2022/03/23/Lets_talk_about_Putin_s_problem_and_Germany_s_defense_spending">transcript &amp; editable summary</a>)

Germany's significant increase in defense spending will have repercussions for Russia and signal a shift in European defense strategies amidst rising tensions.

</summary>

"Germany's decision to spend about $110 billion annually on defense is a significant move that will have repercussions for Russia."
"Putin's actions have inadvertently put Europe on alert, leading to NATO countries increasing their defense spending."
"The West and NATO's reaction to Russia's invasion is a bad sign for Putin's long-term plans."

### AI summary (High error rate! Edit errors on video page)

Germany's decision to spend about $110 billion annually on defense is a significant move that will have repercussions for Russia.
Historically, Germany has not focused on building a large military due to events in the 1930s and 40s, but now it will become the third largest spender on defense.
Russia's budget of about $67 to $70 billion for defense will be surpassed by Germany's new budget.
Putin's actions have inadvertently put Europe on alert, leading to NATO countries increasing their defense spending.
The increased defense spending by NATO countries, particularly a significant player like Germany, will put Russia in a difficult position to keep up.
This situation has put NATO on a more aggressive footing reminiscent of the Cold War era.
Russia's excessive spending on prestige items like nukes and Navy may not be strategically beneficial, especially in conflicts like Ukraine.
The West and NATO's reaction to Russia's invasion is a bad sign for Putin's long-term plans.
While most Germans may support the increased defense spending, neighbors like Poland might be relieved, as Germany previously didn't meet NATO's defense spending threshold.
Germany's choice of military expenditures will provide insights into their future defense strategies and contributions to European defense outside of NATO.

Actions:

for european policymakers,
Monitor developments in European defense strategies and military spending to anticipate future geopolitical shifts (implied).
</details>
<details>
<summary>
2022-03-22: Let's talk about a claim about Mariupol and consistency.... (<a href="https://youtube.com/watch?v=gVBawnCyQkA">watch</a> || <a href="/videos/2022/03/22/Lets_talk_about_a_claim_about_Mariupol_and_consistency">transcript &amp; editable summary</a>)

Beau addresses the claim of Ukraine using people as shields in Mariupol, stressing the importance of not justifying Russia's actions and condemning violations of laws of armed conflict.

</summary>

"It's war, and horrible, horrible stuff happens in war."
"If something is true, it's true across all systems."
"The idea that there are civilians being held in Mariupol that don't want to be there makes the Russian attack, the bombardment, worse."
"If you can't condemn Russia deliberately targeting civilians, what does that say?"
"We don't know that this is happening. We don't know that it's happening on any widespread level."

### AI summary (High error rate! Edit errors on video page)

Addressing a claim about Ukraine using people as shields in Mariupol.
Uncertain about the claim's validity but stresses the need to not justify Russia's actions.
Exploring the idea of civilians being held against their will in Mariupol and its implications.
Condemning the use of civilians as shields and holding those accountable.
Challenging individuals who support Russian targeting of civilians based on this claim.
Emphasizing the importance of consistency in condemning all violations of laws of armed conflict.
Urging against justifying atrocities in war under any circumstances.
Stating that the presence of unwilling civilians in Mariupol worsens Russian actions but does not justify them.
Questioning the moral stance of individuals who fail to condemn deliberate targeting of civilians.
Reminding that justifying such actions leads to a slippery slope of supporting atrocities.

Actions:

for advocates for peace,
Hold those who hold civilians against their will accountable (implied).
Condemn deliberate targeting of civilians (implied).
</details>
<details>
<summary>
2022-03-22: Let's talk about Zelenskyy taking over the media.... (<a href="https://youtube.com/watch?v=BZL8NVUAmUY">watch</a> || <a href="/videos/2022/03/22/Lets_talk_about_Zelenskyy_taking_over_the_media">transcript &amp; editable summary</a>)

President Zelensky's control of TV stations in Ukraine sparks concern among Americans, but Beau points out the longstanding emergency broadcast system in the US mirrors this action, criticizing the sensationalism by American media.

</summary>

"This is an example of the American media taking a story that isn't a story and trying to reframe it so Americans get outraged by it."
"If they're on network TV, they have to know that this system exists."

### AI summary (High error rate! Edit errors on video page)

President Zelensky taking control of TV stations in Ukraine sparked concern among Americans.
Americans were alarmed by the idea of Zelensky becoming tyrannical for controlling media.
Beau draws a parallel to the long-standing emergency broadcast system in the United States.
The emergency alert system has been in place for over 70 years to counter propaganda and communicate during wartime.
Beau points out that the system can take over radio and TV stations, as well as send messages to phones.
He mentions that during wartime, such actions are common to manage communication and propaganda.
Beau criticizes American media for sensationalizing a non-issue about Zelensky's actions.
He notes that most countries have similar systems in place for emergencies and wartime.
Beau expresses surprise that the US didn't implement such control on day one of a conflict.
He concludes by underscoring that the outrage over Zelensky's actions is misplaced, as it mirrors what the US system does.

Actions:

for media consumers,
Educate others about the purpose and history of emergency broadcast systems (implied).
Share information on social media to debunk sensationalized stories (implied).
</details>
<details>
<summary>
2022-03-22: Let's talk about Rule 303 Belorussian style.... (<a href="https://youtube.com/watch?v=k6A0gnUc6yo">watch</a> || <a href="/videos/2022/03/22/Lets_talk_about_Rule_303_Belorussian_style">transcript &amp; editable summary</a>)

Belarusian railway workers and partisan groups disrupt rail traffic to impede Putin's invasion, showing active resistance beyond the government's control.

</summary>

"Belarusian railway workers disrupt rail traffic between Belarus and Ukraine to impede Putin's invasion."
"The people of Belarus are actively resisting being a pawn for Putin."
"Every day without Russian resupply is a win for Ukraine."

### AI summary (High error rate! Edit errors on video page)

Belarusian railway workers disrupt rail traffic between Belarus and Ukraine to impede Putin's invasion.
Chief of Ukraine's railway service publicly thanks Belarusian railway workers for their swift action.
Some tracks in Belarus are in poor condition, contributing to the disruption of rail traffic.
Signaling equipment has been attacked, central switching stations disrupted, and tracks fallen into disrepair quickly.
The disruption has completely severed all rail traffic between Belarus and Ukraine, giving Ukrainians breathing room.
Partisan groups in Belarus have also contributed to disrupting rail traffic.
The people of Belarus are actively resisting being a pawn for Putin, showing resistance beyond the government's control.
Continued disruption of rail traffic could have a significant impact on impeding Russian supplies to Ukraine.
Every day without Russian resupply is a win for Ukraine during this critical phase.
The collective efforts of Belarusian railway workers and partisan groups are making a difference in the conflict.

Actions:

for activists, allies,
Support Belarusian railway workers in their efforts to disrupt rail traffic (exemplified)
Stand in solidarity with the people of Belarus resisting external control (exemplified)
</details>
<details>
<summary>
2022-03-21: Let's talk about Russian reinforcements, contracts, and conscripts.... (<a href="https://youtube.com/watch?v=Lfvm09_Dtyo">watch</a> || <a href="/videos/2022/03/21/Lets_talk_about_Russian_reinforcements_contracts_and_conscripts">transcript &amp; editable summary</a>)

Beau breaks down the Russian military's numbers, revealing challenges in mounting a successful operation in Ukraine due to lack of trained personnel and numbers.

</summary>

"They don't have the numbers."
"It's math at this point."
"The only choice Russia is going to have is to do what they've been doing in Mariupol."

### AI summary (High error rate! Edit errors on video page)

Explains the structure of the Russian military and details about contract troops and conscripted troops.
Contract troops, around 350,000 to 410,000, are the core and elite units of the Russian military.
Conscripted troops, around 260,000 additional forces drafted twice a year, are not well-trained and only serve for a year.
Russia's total forces on the high end are around 670,000, not all of which are combat-ready.
Due to commitments and reserves needed for defensive purposes, Russia may only have about 150,000 troops available for offensive actions.
Russian reserves of 2 million consist mainly of individuals who were in the military and are still young enough to be recalled.
Only around 5,000 out of 2 million Russian reserves actively train regularly, unlike traditional reserves seen in other countries.
Russia lacks the numbers and trained personnel required for a successful large-scale military operation in Ukraine.
A potential massive call-up could train more troops, but the success of such training remains questionable.
The likelihood of Russia effectively running a professional military operation with its current resources seems bleak.

Actions:

for military analysts,
Monitor developments in the conflict in Ukraine (implied)
Stay informed about military capabilities and limitations (implied)
</details>
<details>
<summary>
2022-03-21: Let's talk about Mariupol.... (<a href="https://youtube.com/watch?v=Wi_kDPvDs9Q">watch</a> || <a href="/videos/2022/03/21/Lets_talk_about_Mariupol">transcript &amp; editable summary</a>)

Beau explains the high-stakes situation in Mary Opal, Ukraine, where Ukrainian forces are sacrificing lives to resist Russian aggression and break their resolve.

</summary>

"It's the Alamo for your dad."
"If you can lose like that long enough, you win."
"Russia is able to take this city because they leveled it, they'll try to duplicate it."

### AI summary (High error rate! Edit errors on video page)

Explains the situation in Mary Opal, a city in Ukraine that is currently surrounded and under attack by Russian forces.
Describes the importance of the city to Russia, mentioning the need for a win and a potential route to Russia proper if conquered.
Points out that Russia's hesitation to move in is due to the challenges of urban combat and the inability of their conscripts to handle it.
Details the motivations of Ukrainian forces in defending Mary Opal, which include preventing a template of destroying cities from succeeding and delaying Russian advances to other key locations.
Compares the Ukrainian defense strategy to historical examples like the Battle of Bunker Hill to illustrate the concept of strategic losses leading to eventual victory.
Emphasizes that the Ukrainians are not fighting to win battles but to break Russian resolve through prolonged resistance.
Raises concerns about the potential consequences of Russia successfully taking Mary Opal and the domino effect it could have on other cities.

Actions:

for global citizens,
Support Ukrainian relief efforts (suggested)
Raise awareness about the situation in Mary Opal and Ukraine (implied)
</details>
<details>
<summary>
2022-03-20: Let's talk about the Russian space agency's new tone.... (<a href="https://youtube.com/watch?v=C5kzZzhfrJw">watch</a> || <a href="/videos/2022/03/20/Lets_talk_about_the_Russian_space_agency_s_new_tone">transcript &amp; editable summary</a>)

Russian Space Agency softens stance amidst impacts of sanctions, revealing industry vulnerabilities and potential wider consequences.

</summary>

"We got rockets."
"Couldn't find sanctions, but I did find a special economic operation."
"This is actually an industry that's pretty sensitive to stuff like this."

### AI summary (High error rate! Edit errors on video page)

Russian Space Agency enacted sanctions against the West, particularly Americans, regarding rocket engines and maintenance.
SpaceX offered rockets in response to Russian sanctions, diminishing the impact.
Russian Space Agency softened its tone, sending written appeals to NASA and other space agencies to lift "illegal sanctions."
Sanctions are affecting Russia's space sector, revealing its dependence on external components.
Beau couldn't find evidence of illegal sanctions but identified a special economic operation.
The sensitivity of the space industry to sanctions indicates potential impacts on other sectors.
Russian government's stance may need to soften to prevent harm to the average Russian citizen.

Actions:

for space enthusiasts, policymakers.,
Contact NASA, the Canadian Space Agency, and the European Space Agency to advocate for lifting sanctions (suggested).
Monitor developments in the Russian space sector and related industries (implied).
</details>
<details>
<summary>
2022-03-20: Let's talk about Putin removing his staff.... (<a href="https://youtube.com/watch?v=ITvTWN_dcmg">watch</a> || <a href="/videos/2022/03/20/Lets_talk_about_Putin_removing_his_staff">transcript &amp; editable summary</a>)

Putin's purge in Russia may backfire, as scapegoating and paranoia overshadow addressing critical failures, potentially leading to dangerous outcomes in Ukraine.

</summary>

"Putin is enacting a purge."
"Historically speaking, these purges like this don't actually solve anything."
"He's playing the normal authoritarian goon game."
"All of this paranoia makes him a little bit more dangerous."
"He might be willing to sign off on more brutal tactics in Ukraine."

### AI summary (High error rate! Edit errors on video page)

Putin has been purging individuals in the upper echelons of Russian power, detaining generals and spy chiefs.
Putin's actions suggest scapegoating rather than addressing major failures within his staff.
When rulers like Putin conduct purges, there are typically three outcomes: promoting yes-men, appointing unqualified but trusted individuals, or micromanaging everything.
Historically, such purges rarely solve underlying issues and can even make the ruler vulnerable to removal.
The paranoia within Putin's inner circle may lead to drastic measures, potentially affecting Ukraine.
Putin's recent actions indicate a focus on maintaining power rather than solving critical issues like the situation in Ukraine.

Actions:

for political analysts, international observers,
Monitor the situation in Russia and Ukraine closely to understand potential escalations (implied).
Support diplomatic efforts to address the instability in the region (implied).
</details>
<details>
<summary>
2022-03-19: Let's talk about supplying better gear and risks.... (<a href="https://youtube.com/watch?v=2U1tUVhNp_4">watch</a> || <a href="/videos/2022/03/19/Lets_talk_about_supplying_better_gear_and_risks">transcript &amp; editable summary</a>)

Exploring the risks and variables of providing higher tier equipment to Ukraine without triggering direct confrontation with Russia, amid concerns about misinterpretation leading to a nuclear exchange.

</summary>

"That direct confrontation between NATO and Russia is what people are trying to avoid, most people, a lot of people."
"My concern has been two mistakes at the same time, things being misread."
"So when it comes to the higher tier equipment, which is really what's prompting this, first understand there are really good analysts who are saying to do this."
"But it sets the conditions to get closer to it."
"That's probably why they're being leery though."

### AI summary (High error rate! Edit errors on video page)

Exploring the possibility of providing higher tier equipment to Ukraine and the potential risks and variables involved.
The debate around supplying advanced equipment to Ukraine without triggering a nuclear exchange.
Factors influencing the Biden administration's decision not to provide high-grade equipment to Ukraine despite pressure from various sources.
Concerns about the lack of training and potential consequences if Ukraine is supplied with higher grade equipment.
Speculating on the outcomes and risks of direct NATO confrontation with Russia if advanced equipment is provided to Ukraine.
The complex logistics and potential variables that could arise if NATO decides to intervene directly in the conflict.
The risk of misinterpretation or misjudgment leading to a nuclear exchange in a scenario of heightened tensions between NATO and Russia.
The current stance of the Biden administration and NATO on avoiding direct confrontation and the reluctance to provide high-grade equipment to Ukraine.
Mention of Eastern European nations within NATO possibly considering independent action to support Ukraine.
The potential implications of individual NATO countries acting alone in supporting Ukraine and the risks of misinterpretation by Russia.

Actions:

for policy analysts, decision-makers,
Contact policy advisors to urge caution and thorough consideration of potential risks before providing high-grade equipment to Ukraine (implied).
Organize community forums to discuss the implications of direct NATO involvement and advocate for peaceful resolutions to the conflict (generated).
</details>
<details>
<summary>
2022-03-19: Let's talk about HB 800 in Tennessee.... (<a href="https://youtube.com/watch?v=UKQGop2r_5g">watch</a> || <a href="/videos/2022/03/19/Lets_talk_about_HB_800_in_Tennessee">transcript &amp; editable summary</a>)

Tennessee's HB 800 scapegoats the LGBTQ+ community to divert attention from political failures, manipulating constituents for re-election.

</summary>

"Tennessee's HB 800 is a diversion tactic by politicians to scapegoat and blame the LGBTQ+ community to deflect from their poor performance."
"The bill aims to avoid offending Christian constituents by prohibiting the promotion or normalization of LGBTQ+ issues in public schools."
"It's a manipulative tactic to distract from the state's failures and manipulate easily influenced individuals for political gains."

### AI summary (High error rate! Edit errors on video page)

Tennessee's HB 800 is a diversion tactic by politicians to scapegoat and blame the LGBTQ+ community to deflect from their poor performance.
The bill aims to avoid offending Christian constituents by prohibiting the promotion or normalization of LGBTQ+ issues in public schools.
Tennessee's legislature is failing in education and healthcare, but they want people to focus on culture wars instead.
The bill mirrors Russia's 2013 gay propaganda law, aiming to control and manipulate public opinion.
Politicians in Tennessee are using authoritarian tactics to keep constituents in line and secure re-election.
The bill violates the spirit of the Constitution by targeting a specific demographic and undermining the separation of church and state.
It's a manipulative tactic to distract from the state's failures and manipulate easily influenced individuals for political gains.
The bill is a means for failed politicians to maintain power rather than serve the best interests of the people.
Tennessee ranks poorly in education and healthcare, yet the focus is shifted to divisive issues like LGBTQ+ representation in schools.
The bill is an example of how politicians prioritize their own agendas over the well-being and rights of the people.

Actions:

for tennessee residents, lgbtq+ supporters,
Contact local representatives to voice opposition to HB 800 (suggested)
Support LGBTQ+ organizations and initiatives in Tennessee (exemplified)
Educate others about the harmful impact of discriminatory bills like HB 800 (implied)
</details>
<details>
<summary>
2022-03-18: Let's talk about what it would take for Russia to win.... (<a href="https://youtube.com/watch?v=ZRCy43NKFXA">watch</a> || <a href="/videos/2022/03/18/Lets_talk_about_what_it_would_take_for_Russia_to_win">transcript &amp; editable summary</a>)

Russia's chances in Ukraine hinge on Putin's ego and acknowledgment of failures, requiring significant changes in leadership and strategy.

</summary>

"It's not impossible that Russia still wins, but it's going to be costly."
"Russian leadership needs to admit that they were wrong, which it seems unlikely."
"It's up to Putin. It's his ego."
"Leadership failures and poor decision-making are on Putin."

### AI summary (High error rate! Edit errors on video page)

Russia's chances of winning in Ukraine are slim, especially in turning it into a puppet state.
Victory conditions for Russia now depend more on conference table negotiations than on the battlefield.
Putin's acknowledgment of failures and willingness to fix them are key to improving Russian military capabilities.
Russia needs to address logistical issues and bring in more trained troops to make any advances.
Ukrainian resistance is wearing down Russian resolve, making it difficult for Russia to achieve its objectives.
Russia needs to consolidate its fronts, secure supply lines, and train troops to make any progress.
Siege tactics may not be effective in taking the Ukrainian capital.
Corruption has led to breakdowns in Russian equipment, posing a significant challenge to their military operations.
Putin's ego and reluctance to acknowledge mistakes are major obstacles to a potential Russian victory.
Leadership failures and poor decision-making by Putin's inner circle have led to the current situation in Ukraine.
Putin may need to replace advisors and adopt new strategies to have a chance at success in Ukraine.

Actions:

for analysts, policymakers, activists,
Contact policymakers to advocate for diplomatic solutions (implied)
Support organizations providing aid to Ukraine (implied)
</details>
<details>
<summary>
2022-03-18: Let's talk about Zelenskyy and the Nobel Peace Prize.... (<a href="https://youtube.com/watch?v=fNafdoALgqI">watch</a> || <a href="/videos/2022/03/18/Lets_talk_about_Zelenskyy_and_the_Nobel_Peace_Prize">transcript &amp; editable summary</a>)

President Zelensky's nomination for the Nobel Peace Prize raises questions about fitting the criteria for a wartime leader fighting against authoritarianism.

</summary>

"He's not a peacetime president."
"He has rallied the world for the purposes of war, not for the purposes of peace."
"Almost, it seems like they're setting him up to not win."
"There are a whole bunch of awards that Zelensky totally meets the criteria for."
"It seems like it could be made in other ways."

### AI summary (High error rate! Edit errors on video page)

President Zelensky of Ukraine has been nominated for a Nobel Peace Prize by 36 politicians from Europe.
The politicians are asking the Nobel Committee to extend the nomination deadline for Zelensky.
However, the criteria for the Nobel Peace Prize are focused on fraternity between nations, reduction of standing armies, and promotion of peace congresses.
Zelensky, as a wartime president fighting against a powerful country, does not fit the criteria for the Peace Prize.
Awarding Zelensky the Nobel Peace Prize for his actions in a war setting might imply failure in his leadership role.
Despite not fitting the criteria, Zelensky is acknowledged as the leader Ukraine currently needs.
Zelensky's nomination for the Peace Prize may not succeed, as his actions are more related to war efforts rather than peace initiatives.
The nomination could potentially be a setup for Zelensky not winning the award.
There are other awards more suitable for Zelensky's actions, but the Peace Prize seems like an odd choice.
The nomination could have propaganda value in portraying Zelensky as a peacetime president bullied by Putin.

Actions:

for politicians and policymakers,
Contact the Nobel Committee to express opinions on the nomination (suggested)
Stay informed about the criteria and purpose of the Nobel Peace Prize (implied)
</details>
<details>
<summary>
2022-03-18: Let's talk about Putin of the Fifth Column.... (<a href="https://youtube.com/watch?v=02F5ACZu_7E">watch</a> || <a href="/videos/2022/03/18/Lets_talk_about_Putin_of_the_Fifth_Column">transcript &amp; editable summary</a>)

Putin's misuse of "fifth column" reveals the true underminers of Russia: Putin and his oligarchs, not the protesters.

</summary>

"Putin is the fifth column."
"It's those who refuse to face reality."
"Putin and his oligarchs are undermining a large group from within."
"Russia brought to its knees by corruption and ineffective leadership."
"A major power, not a superpower."

### AI summary (High error rate! Edit errors on video page)

Putin recently used the term "fifth column," sparking curiosity about its origin.
"Fifth column" refers to a small group within a larger one working to undermine it.
The term's origin story involves a general claiming to have four approaching columns and a fifth inside the city ready to open the gates.
Putin, however, was referring to the protesters in Russia criticizing his actions in Ukraine as the fifth column.
The invasion into Ukraine has diminished Russia's global reputation, reducing it from a potential superpower to a military joke.
Despite disagreement about the protesters being labeled the fifth column, Beau points out that Putin and his oligarchs fit this description perfectly.
Putin and his circle are the ones undermining Russia from within due to corruption and ineffective leadership.
Beau concludes by suggesting that Putin himself is the real fifth column, causing damage for his own ego and refusing to acknowledge his failures.

Actions:

for activists, russia watchers,
Organize protests against corruption and ineffective leadership within Russia (exemplified)
Support independent journalism exposing the truth about Putin and his oligarchs (exemplified)
</details>
<details>
<summary>
2022-03-17: Let's talk about negotiations, Budapest, Ukraine, and peace.... (<a href="https://youtube.com/watch?v=9HvUptFSZ24">watch</a> || <a href="/videos/2022/03/17/Lets_talk_about_negotiations_Budapest_Ukraine_and_peace">transcript &amp; editable summary</a>)

Beau talks about ongoing negotiations for peace, Ukraine's request for defense guarantee, and NATO's hesitations due to liability concerns.

</summary>

"Negotiations between countries are ongoing, with hopes of reaching a peace agreement to end the fighting."
"Ukraine seeks a guarantee from a Western nuclear power for defense in case of Russian invasion."
"NATO's hesitation to provide a guarantee stems from the potential liability and lack of power increase."

### AI summary (High error rate! Edit errors on video page)

Negotiations between countries are ongoing, with hopes of reaching a peace agreement to end the fighting.
The current proposal involves Ukraine remaining neutral and Russia withdrawing.
Ukraine seeks a guarantee from a Western nuclear power for defense in case of Russian invasion.
The Budapest memo in 1994 provided assurances, not guarantees, and Russia violated it by invading Ukraine.
NATO's hesitation to provide a guarantee stems from the potential liability and lack of power increase.
The situation also involves the status of territories recognized by Russia within Ukraine.
Finding a middle ground where recognized territories stay with Russia and Ukraine can join NATO could be a possible solution.
The uncertainty of reaching peace soon due to possible breakdowns in talks and Russian leadership's detachment from reality.

Actions:

for foreign policy analysts,
Contact organizations working on conflict resolution and peace negotiations (suggested)
Support efforts that aim to de-escalate tensions between countries (implied)
</details>
<details>
<summary>
2022-03-17: Let's talk about a few tools for finding out about Ukraine.... (<a href="https://youtube.com/watch?v=DIFttANs77U">watch</a> || <a href="/videos/2022/03/17/Lets_talk_about_a_few_tools_for_finding_out_about_Ukraine">transcript &amp; editable summary</a>)

Beau explains open-source intelligence tools for tracking military activities and fires without risking lives or money.

</summary>

"They're not making that stuff up."
"You'll be told what type of aircraft it is."
"You can get a good picture without having to spend any money on those."

### AI summary (High error rate! Edit errors on video page)

Explains the popularity of open source intelligence in providing updates on Ukraine.
Addresses the interest in geolocation and how it reveals risky information.
Talks about tracking military planes near borders and how easily accessible this information is.
Mentions NASA's FIRMS system tracking fires and its applications in monitoring conflict zones.
Encourages using these tools to observe without causing harm or spending money.
Emphasizes the value of freely available information in understanding global events.
Leaves room for diving into more topics based on specific interests and questions from the audience.

Actions:

for tech enthusiasts, researchers, analysts,
Use open-source intelligence tools to monitor and understand global events (implied)
</details>
<details>
<summary>
2022-03-17: Let's talk about Ukraine's personnel problem.... (<a href="https://youtube.com/watch?v=aoGFfr1ahGQ">watch</a> || <a href="/videos/2022/03/17/Lets_talk_about_Ukraine_s_personnel_problem">transcript &amp; editable summary</a>)

Beau sheds light on the dangers of fascist groups, the intersection of ideology and reality, and the importance of countering their narrative with progressive voices in Ukraine.

</summary>

"Fascists are a lot like lead in your drinking water. There is no acceptable safe level."
"There's no acceptable safe level."
"This is something that actually occurs way more than, I guess, people realize."
"No acceptable safe level."
"Rather than looking at them and sharing articles about them, there are left-wing, progressive groups there as well. Get the press talking about those."

### AI summary (High error rate! Edit errors on video page)

Addresses a specific group in Ukraine, the Ukrainian resistance, labeled as fascists due to their ideology.
Makes a comparison to the United States, hypothetical scenario of civilians organizing due to the military being incapacitated.
Points out that the groups leaning into organizing in the scenario are mostly far-right-wing with aesthetics similar to those at the US Capitol on January 6th.
Raises the possibility of a similar scenario occurring in the US if faced with a conflict like Ukraine's.
Explores the challenges of cleaning up such groups historically and proposes potential solutions for Ukraine.
Expresses concern over the attention and coverage given to the group, potentially turning them into heroes through media portrayal.
Provides statistics on the group's size, political presence, and electoral success to contextualize their impact in Ukraine.
Identifies two main reasons for the heightened attention on this group: Russian propaganda and the inherent danger of fascism.
Advises focusing on uplifting left-wing, progressive groups to counterbalance the narrative surrounding the fascist group.
Emphasizes the importance of not ignoring the fascist group despite their relatively small numbers due to the inherent risks associated with fascism.

Actions:

for activists, progressives, community leaders,
Elevate left-wing, progressive groups in Ukraine through sharing and discussing their work (suggested)
Counterbalance media coverage by promoting progressive voices and initiatives in Ukraine (suggested)
</details>
<details>
<summary>
2022-03-16: Let's talk about new subscribers and my philosophy.... (<a href="https://youtube.com/watch?v=eDuo0S1Dl_M">watch</a> || <a href="/videos/2022/03/16/Lets_talk_about_new_subscribers_and_my_philosophy">transcript &amp; editable summary</a>)

Beau challenges stereotypes, advocates for social progressiveness, and encourages viewers to broaden perspectives on social issues to combat bigotry.

</summary>

"Do not let my accent fool you. We are not on the same team."
"If you have the means at hand to help, you have the responsibility to do so."
"Maybe it stops you from being a bigot and that would be a good start."

### AI summary (High error rate! Edit errors on video page)

Welcoming new subscribers interested in foreign policy coverage, history, philosophy, politics, and social issues.
Shares a disturbing message received regarding their stance on BLM with racial slurs.
Clarifies his social and political stance, challenging stereotypes associated with his Southern background.
Expresses disgust towards memes mocking progressive ideas, wishing Biden was as cool as portrayed.
Advocates for social progressiveness, stating the importance of using means to help and not turning against those with less institutional power.
Emphasizes the changing world and the need to adapt, offering advice to viewers to challenge themselves in understanding social issues.
Encourages viewers to watch videos on social issues, suggesting it may shift perspectives and prevent bigotry.

Actions:

for viewers,
Watch one video a week on social issues to challenge yourself (suggested)
Research and look into topics discussed to broaden understanding (suggested)
</details>
<details>
<summary>
2022-03-16: Let's talk about foreign policy's new normal and anti-Russian sentiment.... (<a href="https://youtube.com/watch?v=FnbOJKriTi8">watch</a> || <a href="/videos/2022/03/16/Lets_talk_about_foreign_policy_s_new_normal_and_anti-Russian_sentiment">transcript &amp; editable summary</a>)

Be cautious of playing into anti-Russian sentiment in American foreign policy to avoid repeating historical mistakes and perpetuating harmful othering dynamics.

</summary>

"Just once, it would be great to get through a major foreign policy shift without having to apologize to a demographic of Americans."
"Don't play into this. It's not the Russian people, it's certainly not Russian Americans, it's the Russian leadership."

### AI summary (High error rate! Edit errors on video page)

Describes the new normal in American foreign policy involving a three-way power struggle between China, the United States, and Russia.
Mentions how the presence of a near-peer like Russia influences American foreign policy decisions, shifting focus away from other opposition nations.
Talks about the historical trend of American politicians using external threats to rally votes and consolidate voter support by othering certain groups.
Points out the negative impact of anti-Russian sentiment on American society and foreign policy, urging against playing into it.
Suggests reinstating programs like the Sister City program to foster solidarity between people caught in the crossfire of international tensions.
Warns against politicians exploiting anti-Russian sentiment to prolong conflicts and stay in power, potentially leading to a prolonged Cold War scenario.

Actions:

for foreign policy observers,
Reinstate programs like the Sister City program to show solidarity with people caught in the crossfire (suggested).
Avoid perpetuating anti-Russian sentiment in American society and politics to prevent further harm (implied).
</details>
<details>
<summary>
2022-03-16: Let's talk about Russia's next mistake.... (<a href="https://youtube.com/watch?v=yfNgZCt9vYo">watch</a> || <a href="/videos/2022/03/16/Lets_talk_about_Russia_s_next_mistake">transcript &amp; editable summary</a>)

Russia's mistake in starting a conflict with unforeseen impacts leads to a risky scorched earth strategy, jeopardizing Russian resolve and potentially causing long-lasting devastation.

</summary>

"Russia is making a mistake by starting a conflict with unforeseen impacts."
"Ukrainian strategy is to continue fighting to break Russian resolve."
"Individuals seeking vengeance may carry out extreme measures, not the Ukrainian government."

### AI summary (High error rate! Edit errors on video page)

Russia is making a mistake by starting a conflict with unforeseen impacts.
Russian troops are not performing well and are now in the conventional phase of the conflict.
They lack the skill set for the heavy unconventional phase.
Ukrainian strategy is to continue fighting to break Russian resolve.
Russia seems to be considering a scorched earth strategy, risking unintended consequences.
Shift in strategy may lead to incidents within Russia itself.
World powers are supporting Ukraine, which may change if extreme actions are taken.
Individuals seeking vengeance may carry out extreme measures, not the Ukrainian government.
Russian command's failure in this conflict may have long-lasting consequences.
Devastation and cyclical consequences could result from Russia's new strategy.

Actions:

for world powers,
Stop the chain of events by preventing extreme measures from being taken (implied)
Russia needs to change its strategy to avoid devastating consequences (implied)
</details>
<details>
<summary>
2022-03-15: Let's talk about what an oligarchy is.... (<a href="https://youtube.com/watch?v=5dTXMvxp45Y">watch</a> || <a href="/videos/2022/03/15/Lets_talk_about_what_an_oligarchy_is">transcript &amp; editable summary</a>)

Beau explains oligarchs and oligarchies, focusing on financial influence, corrupt power, and the concentration of wealth leading to a more apparent oligarchical system.

</summary>

"As we look at a world where wealth is being concentrated in fewer and fewer and fewer hands, and those hands end up shaking a lot."
"I don't know how much weight you want to put on that distinction."
"So whether you want to adopt that terminology now or wait a little bit, hope that things get better, I guess that's up to you."

### AI summary (High error rate! Edit errors on video page)

Defines oligarchs and oligarchies as power structures where a small number of people hold corrupt and selfish power.
Differentiates financial oligarchs who use money to influence government decisions from other forms of oligarchs like military dictatorships.
Points out the focus on Russian and Ukrainian oligarchs who gained power through privatization post-Soviet Union.
Mentions a 2014 study that showed US billionaires influencing policy decisions to favor business interests over public opinion.
Refers to the US as a civil oligarchy where financial power is used selfishly and corruptly within legal boundaries.
Notes former President Jimmy Carter referred to the US as an oligarchy in 2015.
Suggests a significant difference between US billionaires and Eastern European oligarchs is the latter's willingness to use violence outside of state power.
Emphasizes the concentration of wealth in fewer hands leading to a more apparent oligarchical system.

Actions:

for activists, educators, advocates,
Advocate for anti-corruption initiatives in your community (suggested)
Support policies that prioritize public interest over business interests (implied)
</details>
<details>
<summary>
2022-03-15: Let's talk about the 2024 Republican primary.... (<a href="https://youtube.com/watch?v=UlFiPcaNs8o">watch</a> || <a href="/videos/2022/03/15/Lets_talk_about_the_2024_Republican_primary">transcript &amp; editable summary</a>)

Beau analyzes potential Republican candidates for 2024, focusing on those aiming to block Trump's nomination and the impact of independent runs on the election.

</summary>

"Their goal isn't necessarily to win, I don't think. It's just to block Trump."
"Recent polling suggests that 30% of Republicans do not want Trump on the ticket."
"If they run as independents, just 3% of the vote, 2% of the vote, that is probably enough to swing the whole election."

### AI summary (High error rate! Edit errors on video page)

Talking about the 2024 Republican ticket and the candidates interested in running for president.
Two distinct groups emerging to challenge a potential Donald Trump nomination again.
Group one includes Cheney, Hogan, and Kinzinger, focused on salvaging the Republican Party from further damage by Trump.
Cheney will seek re-election, while Hogan and Kinzinger are considering a presidential run.
The second group likely to make an impact in the primary run are Pence and DeSantis, targeting Trump as well.
Speculation on potential independent runs by Cheney and Hogan to block Trump's nomination.
Recent polling indicates 30% of Republicans do not want Trump on the ticket.
Running as independents, even a small percentage of votes could sway the election away from Trump.
The situation will become clearer after the midterms as candidates position themselves for the nomination.

Actions:

for political analysts,
Stay informed about potential Republican candidates for the 2024 presidential election (suggested).
Participate in midterms to shape the future political landscape (implied).
</details>
<details>
<summary>
2022-03-15: Let's talk about Ukraine, numbers, and a guess.... (<a href="https://youtube.com/watch?v=KecumfRuNi4">watch</a> || <a href="/videos/2022/03/15/Lets_talk_about_Ukraine_numbers_and_a_guess">transcript &amp; editable summary</a>)

Russian military failure in Ukraine attributed to corruption, lack of supplies, and strategic misjudgments, viewed as a PR stunt more than a serious operation. China's potential assistance to Russia seen as strategic rather than genuine friendship.

</summary>

"They truly believed that Ukraine was just gonna drop to its knees and be like, oh yes, please let me be part of greater Russia."
"Will China help Russia since they have been asked? I think they will a little bit, but not enough to really help."
"As much as they stand out in public and say, we're the best of friends with no limits, that's not really true."

### AI summary (High error rate! Edit errors on video page)

Russian military operation in Ukraine is failing due to lack of supplies and corruption within the military.
Conservative estimates place Russian losses at six to seven thousand in 20 days, with wounded estimates being three times that.
Russia's poor performance in Ukraine is attributed to a lack of basic tactics, supplies, and strategic planning.
The Russian command underestimated Ukraine's resistance, leading to strategic failures in the military operation.
The military operation in Ukraine is seen as more of a PR stunt by Russia rather than a well-planned mission.
Speculation suggests that Russia believed Ukraine would surrender easily, leading to reckless military actions.
China may provide some assistance to Russia to keep them in the fight, not out of genuine friendship but to weaken Russia economically.
China and Russia are not as close allies as portrayed, with China likely assisting Russia strategically rather than militarily.

Actions:

for military analysts, policymakers,
Assist Ukraine with humanitarian aid and support (implied)
Monitor international relations and developments between Russia and China (implied)
</details>
<details>
<summary>
2022-03-14: Let's talk about Russian sanctioning the US.... (<a href="https://youtube.com/watch?v=u8Nz_HL2E_c">watch</a> || <a href="/videos/2022/03/14/Lets_talk_about_Russian_sanctioning_the_US">transcript &amp; editable summary</a>)

Russia, the most sanctioned country, retaliates against the US while NASA's Artemis program offers hope and diversion through lunar involvement for all, especially the youth.

</summary>

"There are slivers of hope. There are things that can cheer you up and can divert your attention from the constant news cycle."
"So if you have kids, it might be something worth doing with them, getting them a boarding pass for it."

### AI summary (High error rate! Edit errors on video page)

Russia has become the most sanctioned country globally, surpassing Iran, with 5,500 different sanctions.
The chief of the Russian space agency announced that they will stop delivering rocket engines to US customers for space.
NASA's program called Artemis is set to return humans to the moon.
Artemis allows people to have their names flown around the moon by signing up on their website.
This initiative aims to involve young people and kids in space exploration.
It serves as a unique diversion from the constant issues on Earth.
People overwhelmed by world events can find hope and distraction in projects like Artemis.
Beau suggests signing up for Artemis' boarding pass if you have kids or are interested.
He believes the US will manage fine despite Russia's sanctions and it won't impact NASA significantly.
Beau encourages viewers to find moments of positivity and cheer in the midst of the relentless news cycle.

Actions:

for space enthusiasts, parents, curious minds.,
Sign up for NASA's Artemis program to have your name flown around the moon (suggested).
Involve kids in space exploration by getting them a boarding pass for Artemis (suggested).
</details>
<details>
<summary>
2022-03-14: Let's talk about Russia and Tucker.... (<a href="https://youtube.com/watch?v=JxGwyAJCmxI">watch</a> || <a href="/videos/2022/03/14/Lets_talk_about_Russia_and_Tucker">transcript &amp; editable summary</a>)

The Russian memo linking Tucker Carlson to propaganda challenges beliefs and could break information silos.

</summary>

"This little bit of information is something you may want to file away."
"Nations diametrically opposed to the United States view him as an ally."
"Clips of him have started showing up on Chinese propaganda channels as well."
"That thing that triggers cognitive dissonance, that thing that forces them to break out of the information silo."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The Department of Information and Telecommunications Support in Russia issues memos to media, including talking points and propaganda guidance.
Mention of Tucker Carlson in one of the memos as a popular Fox News host who criticizes the actions of the United States, NATO, and Western leadership.
Russians believe Tucker Carlson uses fragments from his broadcasts to criticize Western actions and support Russian views.
Beau speculates on Tucker Carlson's influence, pointing out that even though he may not be an agent of influence, Russians perceive him that way.
Contrarians like Tucker Carlson, who go against the mainstream support for Ukraine, are seen as patriots by some Americans but potentially as allies by nations opposed to the United States.
Clips of Tucker Carlson have started appearing on Chinese propaganda channels, despite his strong criticism of China.
Beau suggests that Tucker Carlson's influence might challenge some people's beliefs and help break information silos that emerged around 2016.

Actions:

for informative citizens.,
Research and critically analyze media sources for potential propaganda influences (implied).
</details>
<details>
<summary>
2022-03-14: Let's talk about China, supply chains, and not being done.... (<a href="https://youtube.com/watch?v=tIzN2LqjRS4">watch</a> || <a href="/videos/2022/03/14/Lets_talk_about_China_supply_chains_and_not_being_done">transcript &amp; editable summary</a>)

China's megacity Shenzhen goes back on lockdown due to a "stealth variant," impacting global tech supply chains and potentially worsening inflation, with broader implications on revenue and geopolitics.

</summary>

"Shenzhen is pretty much the factory for every tech company."
"Everybody around the world should just go ahead and get prepared for another interruption."
"The revenue loss from the lockdown may influence China's decisions."
"Prices will go up, exacerbating the inflation issue."
"We will keep an eye on this, I'll keep an eye on this."

### AI summary (High error rate! Edit errors on video page)

China's megacity Shenzhen, with a population of 17-18 million, is going back on lockdown due to community spread of a "stealth variant."
Despite reporting 66 new cases on Sunday, totaling over 400 cases in the last couple of weeks, they believe they caught it relatively early.
Shenzhen is a vital tech manufacturing hub, and its lockdown could lead to disruptions in the global supply chain.
The impact could affect tech companies like Apple initially but may escalate to other sectors as well.
This disruption adds to the existing inflation concerns globally.
The geopolitical situation with Russia and Ukraine reaching out to China for help adds another layer of uncertainty to China's decisions.
The revenue loss from the lockdown may influence China's future choices.
The supply chain interruption could lead to higher prices as supply diminishes.
Beau suggests everyone should prepare for interruptions in products that rely on electricity.
He plans to monitor the situation and provide updates as more information becomes available.

Actions:

for global citizens, tech industry players,
Monitor news updates on the situation in Shenzhen and its impact globally (suggested)
Prepare for potential disruptions in tech products and anticipate price increases (suggested)
</details>
<details>
<summary>
2022-03-13: Let's talk about the US, a timeline, and Ukraine... (<a href="https://youtube.com/watch?v=Z7rE38x4ypE">watch</a> || <a href="/videos/2022/03/13/Lets_talk_about_the_US_a_timeline_and_Ukraine">transcript &amp; editable summary</a>)

Recalling the early days of COVID-19, Beau criticizes American exceptionalism and calls for reevaluation of priorities and genuine leadership in facing global challenges.

</summary>

"Money and power couldn't stop a virus."
"The United States has succumbed to bumper sticker patriotism."
"We're still looking around as countries all over the world pass us."
"We've become apathetic to everything around us."
"It's probably something that is way past time to address."

### AI summary (High error rate! Edit errors on video page)

Recalls memories of disinfecting in the kitchen triggering thoughts about the early days of the COVID-19 pandemic when his wife worked in a COVID wing.
Mentions the decontamination ritual his wife had to undergo every day after work.
Talks about the initial advice to disinfect packages and leave them outside in the sun due to lack of information about the virus.
Shares an encounter at a gas station where people express disbelief that a crisis like Ukraine could happen in the US due to its wealth and power.
Describes American exceptionalism as the belief that the US could handle any crisis because of its financial and political standing.
Provides a timeline of major events during the COVID-19 pandemic, including milestones like the declaration of a global pandemic by the World Health Organization and vaccine developments.
Notes the significant number of COVID-19 cases and deaths in the US compared to other countries despite its wealth and power.
Criticizes American exceptionalism for leading to complacency and inefficient handling of crises.
Urges Americans to re-evaluate their priorities, focusing on community, policy, and leadership rather than empty patriotism.
Encourages reflection on the need for genuine leadership to address national and global challenges.

Actions:

for americans,
Invest more in policy than in party and personality (implied)
Care about local communities by actively engaging and supporting them (implied)
Prioritize genuine leadership over empty patriotism (implied)
</details>
<details>
<summary>
2022-03-13: Let's talk about staying positive through this and letting it go.... (<a href="https://youtube.com/watch?v=wmEgl6TfX74">watch</a> || <a href="/videos/2022/03/13/Lets_talk_about_staying_positive_through_this_and_letting_it_go">transcript &amp; editable summary</a>)

Beau addresses staying positive amidst chaos, celebrating small wins, and avoiding comparison in activism efforts.

</summary>

"Acknowledge the wins."
"It's not a competition."
"There's nothing wrong with feeling sad or angry."
"There's nothing wrong with acknowledging the wins, even if they're small."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses the struggle of staying positive amidst doomscrolling and feeling helpless.
Acknowledges the importance of celebrating small wins in the midst of chaos.
Shares a heartwarming example of a little girl singing in a bomb shelter bringing warmth and hope.
Encourages people to find and acknowledge moments of positivity in their lives.
Advises to not compare activism efforts with others; it's not a competition.
Advocates for doing what you can, where you can, for as long as you can.
Emphasizes the importance of taking breaks and not letting awareness consume you.

Actions:

for individuals seeking guidance on staying positive amidst challenging times.,
Acknowledge the wins in your life, no matter how small (implied).
Celebrate moments of positivity and hope (implied).
Take breaks when needed to prevent feeling overwhelmed (implied).
</details>
<details>
<summary>
2022-03-13: Let's talk about a unique proposal going after oligarchs.... (<a href="https://youtube.com/watch?v=RN4vP74kZhY">watch</a> || <a href="/videos/2022/03/13/Lets_talk_about_a_unique_proposal_going_after_oligarchs">transcript &amp; editable summary</a>)

Beau explains legislation allowing President Biden to issue letters of mark to target Russian oligarchs, proposing effectiveness and a potential amendment to include cryptocurrencies.

</summary>

"Is this constitutional? Surprisingly, yeah."
"Those that are sanctioned will remove their assets."
"It might be a really good idea to amend this to include the ability to go after financial assets in, say, I don't know, cryptocurrencies."

### AI summary (High error rate! Edit errors on video page)

Discovers legislation allowing President Biden to issue letters of mark and reprisal to go after Russian oligarchs.
Explains letters of mark as documents authorizing private citizens to act as privateers with legal backing.
Mentions potential effectiveness of the bill in prompting oligarchs to move their assets out of foreign countries.
Points out the constitutional basis for issuing letters of mark under Article 1, section 8, clause 11 of the US Constitution.
Connects the bill's concept to historical practices and the use of private contractors.
Suggests amending the bill to target financial assets like cryptocurrencies for increased effectiveness.

Actions:

for legislative enthusiasts,
Track the progress of bill 6869 (suggested)
Stay informed about the implications of the bill on targeting oligarchs (suggested)
Advocate for potential amendments to include cryptocurrencies in the legislation (suggested)
</details>
<details>
<summary>
2022-03-12: Let's talk about why the US is pursuing economic measures.... (<a href="https://youtube.com/watch?v=al0_M0HOrOY">watch</a> || <a href="/videos/2022/03/12/Lets_talk_about_why_the_US_is_pursuing_economic_measures">transcript &amp; editable summary</a>)

Beau explains why the U.S. and the West are prioritizing sanctions over arms supply to Ukraine, revealing the power play behind foreign policy decisions.

</summary>

"Foreign policy viewed through lens of power, not morality."
"It is that international poker game and everybody's cheating."

### AI summary (High error rate! Edit errors on video page)

Foreign policy rule discussed repeatedly on the channel.
Question about the U.S. and the West behaving differently this time.
Normally flood favored side with arms, but this time rushing with sanctions on Russia and delaying arms to Ukraine.
Debate over responsibility to provide Ukraine with necessary defense.
Cold feet on transferring MiGs from Poland to Ukraine.
Foreign policy viewed through lens of power, not morality.
West's actions aimed at economically devastating Russia for the long term.
Severe sanctions to take Russia out of the game early on.
Logistics and concerns about tipping the scales reasons for delaying weapons to Ukraine.
Concerns about potential escalation if certain weapons were provided to Ukraine.
NATO's strategic move to destabilize Russia's economy and reduce its influence.

Actions:

for policy analysts, activists,
Build awareness on the importance of understanding foreign policy decisions (exemplified)
Advocate for ethical foreign policy decisions in your community (exemplified)
</details>
<details>
<summary>
2022-03-12: Let's talk about the country in the mirror.... (<a href="https://youtube.com/watch?v=H4gedJZqm3o">watch</a> || <a href="/videos/2022/03/12/Lets_talk_about_the_country_in_the_mirror">transcript &amp; editable summary</a>)

The US and its Western allies must confront uncomfortable truths about their past actions to lead the world in a better direction.

</summary>

"If something is wrong, it is wrong. If something is right and good and true, it is."
"We haven't been guiding the world to a better place, but we could if we wanted to."
"It's OK to be uncomfortable."
"We haven't been doing what we should as a society, but we can fix it."
"There are going to be topics coming up that are going to make people uncomfortable."

### AI summary (High error rate! Edit errors on video page)

The US and its Western allies have historically been the main countries capable of projecting force, but now other countries are gaining that capability.
The story of Hassan, an 11-year-old boy who fled violence alone, illustrates a stark contrast in how different countries treat migrants and refugees.
Beau criticizes the US for its treatment of migrants, contrasting it with the help offered to refugees in other countries like Slovakia.
He points out the hypocrisy in differentiating between refugees and migrants based on race or shade, citing examples from European countries like Hungary.
Beau draws parallels between American actions in conflicts like Afghanistan and Iraq and criticizes the media's portrayal of similar actions by other countries.
He stresses the importance of acknowledging past mistakes and changing course to lead the world in a better direction.
Beau urges Americans and Western societies to embrace discomfort, as it signals an opportunity to correct past wrongs and strive for a more just future.

Actions:

for policy makers, activists, global citizens,
Acknowledge past mistakes and work towards leading the world in a better direction (implied).
</details>
<details>
<summary>
2022-03-12: Let's talk about an injuction in Texas.... (<a href="https://youtube.com/watch?v=OrOmLAU27PU">watch</a> || <a href="/videos/2022/03/12/Lets_talk_about_an_injuction_in_Texas">transcript &amp; editable summary</a>)

Good news out of Texas as courts step in to block Governor Abbott's edict undermining medical advice for kids, but the fight continues against harmful legislation.

</summary>

"Parents don't have any rights when it comes to medical procedures and advice."
"The fight's not over. People are still going to need help and people in Texas are still going to be worried."
"This may have been a really bad move for Abbott politically, set aside the fact it's obviously a bad move morally."

### AI summary (High error rate! Edit errors on video page)

Good news out of Texas: courts intervene in a case where the governor issued an edict undermining medical advice for kids, motivated by upcoming elections.
The edict prohibited parents from following medical professionals' advice regarding their kids' gender identity.
Governor threatened investigations by Child Protective Services if parents followed medical advice over his statement.
Aim of the edict was to rally his base by demonizing kids.
Courts issued an injunction against the edict until the case is heard in July.
Legal experts agree that the edict violates both US and Texas state constitutions.
Governor Abbott's actions contradict the policies of medical freedom and parental rights that were resonating with voters.
Abbott's timing of the edict coincided with another controversial story to possibly divert attention.
Despite the failed attempt to divert attention, the fight is ongoing, and people in Texas still need help.
Big businesses are beginning to push back against such legislation due to its unpopularity and potential impact on their bottom line.

Actions:

for texans, advocates,
Support organizations advocating for medical freedom and parental rights (implied).
Stay informed and engaged in local politics to support initiatives protecting children's rights (implied).
</details>
<details>
<summary>
2022-03-11: Let's talk about wheat, Russia, Ukraine, and downstream effects.... (<a href="https://youtube.com/watch?v=Uy_BLhOyVWs">watch</a> || <a href="/videos/2022/03/11/Lets_talk_about_wheat_Russia_Ukraine_and_downstream_effects">transcript &amp; editable summary</a>)

Beau explains the global impact of wheat shortages, warning of potential social unrest and advising preparation for rising bread prices.

</summary>

"A lack of wheat means a lack of bread."
"The higher the percentage of income that goes to food, the more it's going to hurt because this is a staple and it has been interrupted."

### AI summary (High error rate! Edit errors on video page)

Explains the impact of the current state of the world on wheat, corn, and soybeans.
Notes that Russia and Ukraine, significant wheat exporters, are facing disruptions that may affect the global market.
Mentions factors like disruptions in transportation, lack of fertilizer, and combat affecting crop transportation.
Shares that wheat prices in Chicago are at an all-time high, with corn and soybeans prices up as well.
Warns of potential social unrest due to a lack of bread caused by the wheat shortage.
Anticipates heavy impacts in the Middle East and Africa due to the shortage.
Predicts ripple effects worldwide as a result of the wheat supply disruptions.
Foresees an increase in bread prices in the United States.
Advises people to stock up on ingredients if they took up bread making during the pandemic.
Stresses the importance of addressing food insecurity globally.

Actions:

for global citizens,
Support relief agencies that may need help due to food supply disruptions (implied)
Prepare by stocking up on ingredients for staple foods (implied)
</details>
<details>
<summary>
2022-03-11: Let's talk about Russian claims, chemicals, and masks.... (<a href="https://youtube.com/watch?v=2PiJ4xsBQnQ">watch</a> || <a href="/videos/2022/03/11/Lets_talk_about_Russian_claims_chemicals_and_masks">transcript &amp; editable summary</a>)

Beau explains the dangers of using Russian surplus filters and advises against their use, suggesting alternative filters for protection.

</summary>

"Don't use them. Don't use them."
"If I had any alternative whatsoever, I would not use one, even a new one."
"Don't do it."
"Do not use those filters."
"Don't use it, even just to play around in a costume."

### AI summary (High error rate! Edit errors on video page)

Talks about Russian equipment and masks, providing a PSA about surplus Russian masks.
Mentions looking at photos and footage of Russian equipment, including gas masks from the mid-Soviet era.
Shares the distribution of obsolete or less than modern equipment by Russians, like Mosins.
Expresses skepticism towards Russian claims based on past instances of misinformation.
Addresses whether the presence of masks indicates Russia's intention to use them.
Warns about the dangers of using Russian surplus filters due to lead and asbestos content.
Advises against using Russian filters and suggests alternatives for actual protection.
Emphasizes the importance of not using filters with Cyrillic writing, even for costume purposes.
Comments on the dated equipment being handed out by Russians, including steel helmets and aged weapons.
Concludes by speculating on Russia's motives for distributing such equipment and masks.

Actions:

for preppers, surplus collectors,
Replace Russian surplus filters with new ones for CBRN protection (implied).
Avoid using filters with Cyrillic writing, even for costume purposes (implied).
</details>
<details>
<summary>
2022-03-11: Let's talk about Russia, sanctions, and McDonald's.... (<a href="https://youtube.com/watch?v=zYCQpPlPzRk">watch</a> || <a href="/videos/2022/03/11/Lets_talk_about_Russia_sanctions_and_McDonald_s">transcript &amp; editable summary</a>)

Western companies leaving Russia creates a domino effect impacting industries, GDP, and Putin's war efforts, setting conditions for economic change and potentially requiring Western banks for recovery.

</summary>

"Most economies are like a house of cards. Once large segments yank themselves out, everything else falls."
"Reducing economic activity limits tax revenue which reduces the amount of money Putin can spend on the war."
"It makes it harder for Putin to sell his war at home when people are getting economically devastated because of it."
"Russia is now the most sanctioned country on the planet and they're still planning on piling on more sanctions."
"All the king's horses and all the king's men are going to have to try to put that egg of an economy back together again."

### AI summary (High error rate! Edit errors on video page)

Explains the impact of Western companies leaving Russia due to economic policies.
Mentions how thousands of employees lost their jobs, affecting the GDP.
Uses McDonald's as an example to illustrate the ripple effects of a company leaving.
Describes the interconnected nature of various industries affected by a single company's departure.
Points out that economic pressure is aimed at leadership, not the average person.
Emphasizes that reducing economic activity limits tax revenue for Putin's war efforts.
Suggests that economic pressure sets conditions that make it harder for Putin to maintain support for the war.
Notes that while economic pressure alone may not shift leadership, it creates conditions for change.
Speculates on the need for Western banks to assist in rebuilding Russia's economy post-sanctions.

Actions:

for economic observers,
Support local businesses affected by economic policies (implied)
Advocate for policies that prioritize economic stability for all (implied)
</details>
<details>
<summary>
2022-03-10: Let's talk about reinforcements headed to Ukraine.... (<a href="https://youtube.com/watch?v=4-p2N3EVvlU">watch</a> || <a href="/videos/2022/03/10/Lets_talk_about_reinforcements_headed_to_Ukraine">transcript &amp; editable summary</a>)

Russia sends Wagner and Syrian fighters as reinforcements to Ukraine; Ukrainian forces bolstered by international volunteers, posing tactical challenges for opposition.

</summary>

"Russia now has to review like three dozen sets of tapes because their tactics are going to be different."
"This is going to boost morale, and it's going to give them needed forces."
"The world is full of amateurs. That's what makes the amateur so dangerous."
"These are people motivated by their own ideological reasons."
"From the Ukrainian side, what they have coming to help is going to help on a lot of different fronts."

### AI summary (High error rate! Edit errors on video page)

Russia is sending Wagner, known for lacking compassion and professionalism, as reinforcements to Ukraine.
Syrian fighters with experience in urban combat are also being called in.
Approximately 5,000 troops are expected, which may not be enough to alter the current situation in Ukraine.
Wagner troops are likely to use force to subdue resistance during the occupation phase.
The security clampdown by Wagner may lead to increased resistance among the local populace.
Ukrainian reinforcements include international volunteers, estimated between 16,000 to 25,000 from various countries.
These volunteers are mostly combat veterans motivated by their own ideological reasons.
The presence of international volunteers is likely to boost morale among Ukrainian forces.
Volunteers from different countries will bring varied tactics and operational approaches, posing a challenge for the opposition.
The involvement of international volunteers provides plausible deniability for special operations.

Actions:

for military analysts, peace advocates,
Join international relief efforts (exemplified)
Support local humanitarian aid for conflict zones (suggested)
</details>
<details>
<summary>
2022-03-10: Let's talk about Russia's Potemkin Village military.... (<a href="https://youtube.com/watch?v=3DF3iRrzK_A">watch</a> || <a href="/videos/2022/03/10/Lets_talk_about_Russia_s_Potemkin_Village_military">transcript &amp; editable summary</a>)

Beau dives into the concept of a Potemkin village, disputes the comparison to the Russian army, blames Putin's leadership and corruption for their failures, and warns against underestimating their capabilities.

</summary>

"It's far safer to overestimate your opposition. Estimates shouldn't be reduced that much."
"I wouldn't classify the Russian military as worthless just yet."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of a Potemkin village with the story of Potemkin and Catherine the Great, where fake villages were built to deceive her.
Disagrees with the comparison of the Russian army to a Potemkin village, despite their poor performance in Ukraine.
Points out that while Russia is performing poorly, their capabilities may be underestimated due to overestimation of Russia and underestimation of Ukraine.
Mentions basic errors made by the Russian military, like air drops in contested airspace and lack of guided munitions.
Suggests that Russia might be holding back advanced equipment to prevent a NATO counter-attack.
Blames Putin's leadership for the failures of the Russian military, citing his paranoia and habit of surrounding himself with yes-men.
Attributes part of the Russian military's failures to corruption within the ranks, with examples of embezzlement and negligence.
Warns against underestimating the Russian military and advocates for overestimating opposition for safety.
Concludes by cautioning against dismissing the Russian military as worthless and suggesting it could be a grave error.

Actions:

for military analysts, policymakers,
Analyze and address corruption within military ranks (implied)
Avoid underestimating opposition forces (implied)
</details>
<details>
<summary>
2022-03-10: Let's talk about Putin banning words and what we can learn.... (<a href="https://youtube.com/watch?v=fPZQmP8ZYW4">watch</a> || <a href="/videos/2022/03/10/Lets_talk_about_Putin_banning_words_and_what_we_can_learn">transcript &amp; editable summary</a>)

Different national lenses shape views globally, impacting policies and narratives to maintain the status quo and deter change.

</summary>

"Don't say climate change. Don't say war. Don't say history. Don't say gay."
"The only thing that really changes is what they don't want you to talk about."
"They care about maintaining the status quo and making sure that those without a lot of institutional power, well, they don't ask for change."

### AI summary (High error rate! Edit errors on video page)

Different countries have their own national lens that influences how they view the world and themselves.
The lens is shaped by media discourse and narratives pushed by politicians, impacting societal views.
Examples like Russians not calling the war in Ukraine a war and states in the US avoiding the term climate change show how narratives can shape policy.
Authoritarian regimes use tactics like controlling narratives to maintain the status quo and prevent demands for change.
In the US, there are instances where topics are avoided in fear of upsetting the status quo and sparking demands for change.
Media and politicians play a significant role in shaping public perception and what topics are deemed acceptable for discourse.
The playbook of authoritarian leaders involves restricting certain topics to prevent challenges to their power.
The goal is to ensure that those without much power do not demand change by marginalizing certain topics and people.
By examining how other countries handle certain issues, it provides a reflection for individuals to analyze their own national lens.
The avoidance of discussing certain topics is a common tactic used to maintain the status quo and prevent societal change.

Actions:

for global citizens,
Challenge narratives pushed by media and politicians (implied)
Encourage open discourse on topics that are marginalized (implied)
</details>
<details>
<summary>
2022-03-09: Let's talk about why analysts are sure Russia won't win.... (<a href="https://youtube.com/watch?v=XeolyxyzuX4">watch</a> || <a href="/videos/2022/03/09/Lets_talk_about_why_analysts_are_sure_Russia_won_t_win">transcript &amp; editable summary</a>)

Beau explains the math behind why Putin will likely lose in Ukraine due to insufficient troop numbers and resistance dynamics.

</summary>

"If there are not 20 soldiers per 1000, the occupation fails."
"The math says Putin will lose."
"All Ukraine has to do is keep fighting and they win."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of showing his work regarding the math behind a geopolitical situation.
Mentions the importance of minimal resistance when occupying an area.
References a 2003 Rand Corporation study that determined the number of troops needed to successfully occupy an area.
States that one soldier per 50 civilians is the ratio required for successful occupation.
Notes that Russia's current ratio in Ukraine is four troops per 50 civilians, significantly lower than what's needed.
Estimates that Russia needs about 800,000 troops to occupy Ukraine, but realistically they don't have close to that number.
Points out that even if Russia shipped more troops in, they still wouldn't have enough for the occupation.
Suggests that Ukraine can win by engaging in minimal resistance over time.
Expresses doubt in Putin's ability to hold Ukraine due to lack of troops and financial strain.
Concludes that the math indicates Putin will lose unless significant changes occur.

Actions:

for military analysts, geopolitical strategists,
Contact organizations supporting Ukrainian resistance (implied)
Support humanitarian efforts in Ukraine (implied)
Stay informed on the situation in Ukraine and advocate for diplomatic solutions (implied)
</details>
<details>
<summary>
2022-03-09: Let's talk about Russian oil and American costs.... (<a href="https://youtube.com/watch?v=L3YtOnpTYH8">watch</a> || <a href="/videos/2022/03/09/Lets_talk_about_Russian_oil_and_American_costs">transcript &amp; editable summary</a>)

The United States stopping Russian oil imports leads to rising gas prices, urging individuals to reduce usage and cooperate to control costs.

</summary>

"It's time to start thinking about your neighbor. It's time to start cooperating."
"Y'all definitely start trying to figure out a way to work together."
"Start looking at stuff geared towards environmentalists."

### AI summary (High error rate! Edit errors on video page)

The United States has decided to stop buying Russian oil, which could have significant impacts both in Russia and in the US.
Biden's potential plans regarding the situation are unknown, leaving uncertainty about the future.
Rising gas prices are expected, leading to financial strain for many individuals.
Suggestions are provided for individuals to cope with the increasing gas prices, such as carpooling, biking, or walking.
Social and community responsibility are emphasized in times of crisis like this.
Beau encourages people to plan trips efficiently, cut down on driving, and reduce overall demand to help lower gas prices.
It is mentioned that releasing reserves may not be a viable solution due to heightened tensions.
Cooperation and mutual support at the local level are vital to alleviate the effects of the situation.
Beau stresses the importance of individuals taking action to reduce usage, decrease demand, and control costs.
Looking towards environmentalists for ideas on cutting usage and reducing carbon footprint is recommended.

Actions:

for individuals, communities,
Plan trips efficiently and cut down on unnecessary driving (implied)
Offer carpooling opportunities to reduce overall usage and costs (implied)
Cooperate with neighbors for mutual support and assistance in daily tasks (implied)
</details>
<details>
<summary>
2022-03-09: Let's talk about North Carolina, Pennsylvania, and the Supreme Court.... (<a href="https://youtube.com/watch?v=k7hpnjr7-FU">watch</a> || <a href="/videos/2022/03/09/Lets_talk_about_North_Carolina_Pennsylvania_and_the_Supreme_Court">transcript &amp; editable summary</a>)

Three Supreme Court cases on redistricting show Republican Party's push for extreme gerrymandering and control over elections.

</summary>

"Republican Party pushing for Supreme Court permission to redraw maps as they see fit."
"Acceptance of this doctrine could lead to extreme gerrymandering and less representation for voters."
"Republican Party aiming to control state-level power to influence federal elections."

### AI summary (High error rate! Edit errors on video page)

Three Supreme Court cases, two decided, one pending, involving North Carolina, Pennsylvania, and Wisconsin, all about redistricting for elections.
Republican Party pushing for Supreme Court permission to redraw maps as they see fit.
North Carolina case involved heavily gerrymandered maps rejected by state courts.
Republicans argued that only state legislature should draw maps, not state courts.
Acceptance of this doctrine could lead to extreme gerrymandering and less representation for voters.
Similar situation in Pennsylvania with the governor vetoing maps drawn by state legislature.
Wisconsin case likely to have a similar outcome as the Supreme Court ruled against Republicans in the other cases.
Republican Party aiming to control state-level power to influence federal elections.
Possibility of maps being drawn based on party affiliation rather than voter preference.
Concerns about less responsive government if gerrymandering continues unchecked.

Actions:

for voters, election watchdogs,
Watchdog groups: Monitor closely for further developments on redistricting cases (implied).
</details>
<details>
<summary>
2022-03-08: Let's talk about a report on the Ukrainian pickle grandma.... (<a href="https://youtube.com/watch?v=_wyux48_NRc">watch</a> || <a href="/videos/2022/03/08/Lets_talk_about_a_report_on_the_Ukrainian_pickle_grandma">transcript &amp; editable summary</a>)

Beau shares a humorous tale of a Ukrainian woman thwarting a drone with pickled tomatoes, raising questions about the story's credibility and the power of belief in narratives.

</summary>

"Ukraine has perfected surface to air tomato technology."
"I choose to believe that some grandma took out a drone with a jar of pickled tomatoes."

### AI summary (High error rate! Edit errors on video page)

Beau recounts a story about a woman in Ukraine who supposedly took down a drone with a jar of pickled tomatoes.
The woman mistook the drone for a bird and thought it might harm her, prompting her to throw the jar at it.
She then stomped on the drone out of fear that it could track her.
The story lacks concrete evidence and the woman's full identity is kept anonymous.
The outlet reporting this story is pro-Ukraine, raising questions about its credibility.
Beau admits to wanting to believe the story, but acknowledges the lack of solid proof.
The incident is framed humorously, with Beau joking about Ukraine's "surface to air tomato technology."
The narrative touches on the theme of heroism and how stories can be distorted in the retelling.
There's a playful tone in Beau's delivery as he shares this quirky and amusing anecdote.
The focus is on the absurdity of the situation and the entertaining aspect of a grandma thwarting a drone with pickled tomatoes.

Actions:

for social media users,
Verify stories before sharing online (implied)
</details>
<details>
<summary>
2022-03-08: Let's talk about Poland, planes, and power in Ukraine.... (<a href="https://youtube.com/watch?v=wYxCvMI-vHE">watch</a> || <a href="/videos/2022/03/08/Lets_talk_about_Poland_planes_and_power_in_Ukraine">transcript &amp; editable summary</a>)

Beau clarifies a trade deal involving aircraft between Poland, Ukraine, and the US, debunking commentary that it weakens US air power and stressing America's unmatched military strength.

</summary>

"U.S. warfighting capability is second to none."
"This isn't going to degrade U.S. air power, period, full stop, end of story."
"It's not an issue."
"These pundits bought that propaganda and they believed it."
"Anybody who's providing commentary suggesting otherwise is probably somebody you should ignore on all military matters."

### AI summary (High error rate! Edit errors on video page)

Explains the deal between Poland, Ukraine, and the United States involving the transfer of aircraft.
Clarifies why the US is giving F-16s to Poland instead of Ukraine.
Counters the commentary suggesting that this trade will weaken US air power.
Provides a breakdown of the numbers regarding the world's largest air forces.
Stresses the US's unmatched capability in air power and its ability to combat multiple adversaries simultaneously.
Addresses the significance of the doctrine adopted by the US military post-World War II.
Emphasizes that the transfer of F-16s to Poland will not significantly impact US air power.
Points out the strategic importance of pre-positioning aircraft in allied countries.
Dismisses concerns about degrading US defense capabilities due to this trade.
Condemns misleading commentary based on inaccurate perceptions of military strength.

Actions:

for military analysts, policymakers,
Support NATO initiatives to enhance defense capabilities in allied countries (implied)
</details>
<details>
<summary>
2022-03-08: Let's talk about NATO, oil, Venezuela, and Ukraine... (<a href="https://youtube.com/watch?v=K3bkV2EN_o4">watch</a> || <a href="/videos/2022/03/08/Lets_talk_about_NATO_oil_Venezuela_and_Ukraine">transcript &amp; editable summary</a>)

Beau addresses leftist criticisms on imperialism and sanctions, discussing NATO expansion, capitalist aggression, and the misconception of Russia as left-wing.

</summary>

"If you want to reach somebody, you have to kind of meet them where they're at."
"Russia is a capitalist oligarchy. It is right-wing."
"A lot of this is being parroted by people on the right."

### AI summary (High error rate! Edit errors on video page)

Reads a message from a leftist critiquing his views on imperialism and sanctions.
Responds to the message by discussing NATO expansion, imperialism, and capitalist aggression.
Explains the importance of understanding military strategy and the capitalist economy.
Criticizes the idea that Russia is synonymous with leftism, pointing out Russia's capitalist nature.
Advises engaging in productive discourse without resorting to insults and understanding others' perspectives.

Actions:

for online activists and political commentators,
Engage in productive discourse with those holding different views (exemplified)
Avoid insulting others in opening paragraphs to facilitate constructive dialogues (implied)
</details>
<details>
<summary>
2022-03-07: Let's talk about truth, lies, and video in Ukraine.... (<a href="https://youtube.com/watch?v=nYSGZn6OyKM">watch</a> || <a href="/videos/2022/03/07/Lets_talk_about_truth_lies_and_video_in_Ukraine">transcript &amp; editable summary</a>)

Beau delves into the complex interplay between truth, lies, and belief in wartime narratives, shedding light on the blurred lines of propaganda and reality.

</summary>

"Truth is the first casualty."
"Even the lies, especially the lies."
"The truth is normally somewhere in the middle."

### AI summary (High error rate! Edit errors on video page)

Addressing truth, lies, and reality in videos, particularly in relation to captured Russian videos surfacing during conflicts.
Mention of Geneva Conventions and the rules on handling prisoners of war to prevent their use for propaganda.
Not all videos of captured individuals necessarily violate these conventions, as civilians may be unaware of the rules.
Exploring the concept of victor's justice in conflicts and the significance of propaganda wars in shaping outcomes.
The blurred lines between truth and reality in propaganda operations and the focus on tangible impacts rather than factual accuracy.
The role of belief in shaping what becomes real and eventually perceived as truth, especially in the context of war narratives.
Importance of creating narratives that the target audience wants to believe for effective propaganda.
Beau's belief that lower-ranking Russian soldiers may not have been fully aware of the situation due to potential lack of information sharing among higher ranks.
Speculation on the reasons behind failures on the Russian side, pointing to potential issues with information flow and decision-making processes.
Acknowledgment that discerning truth from fiction in conflicts often requires hindsight and analysis by historians, influenced by national perspectives.

Actions:

for history buffs, conflict analysts,
Support organizations conducting fact-checking and historical analysis post-conflicts (implied).
Encourage critical thinking and awareness of national biases in interpreting historical events (implied).
</details>
<details>
<summary>
2022-03-07: Let's talk about preparing for a possibility.... (<a href="https://youtube.com/watch?v=7zsTGA0EV3g">watch</a> || <a href="/videos/2022/03/07/Lets_talk_about_preparing_for_a_possibility">transcript &amp; editable summary</a>)

NATO's moves may prompt a Russian cyber offensive, but simple preparations can alleviate stress and minor inconveniences.

</summary>

"Be prepared, not scared."
"You're not scrambling to get cash to go get gas in your car."

### AI summary (High error rate! Edit errors on video page)

NATO is making moves that will upset Russia, potentially leading to a response.
Russia might choose a cyber offensive as a way to send a message.
An offensive is likely to be annoying rather than earth-shattering.
Simple preparations can help in case of disruptions.
Suggestions include having cash on hand, keeping gas in the car, and ensuring access to prescription meds.
It's advised to be prepared, not scared, in the face of a cyber offensive.
Moscow is expected to send some form of messaging to DC due to current tensions.
The messaging is not anticipated to be extremely destructive.
Trust in the protection measures in place on our side.
Minor inconveniences are expected, not major infrastructure disruptions.

Actions:

for civilians,
Keep cash on hand, ensure access to prescription meds, and maintain gas in your car (suggested)
Print out sensitive documents if needed and destroy them afterward (suggested)
</details>
<details>
<summary>
2022-03-06: Let's talk about the new Russia-US hotline.... (<a href="https://youtube.com/watch?v=1Yh-l0J5LHc">watch</a> || <a href="/videos/2022/03/06/Lets_talk_about_the_new_Russia-US_hotline">transcript &amp; editable summary</a>)

The US and Russia establish a direct military communication channel to de-escalate tensions and prevent unwanted conflicts, showing the significance of back channels in avoiding wars and protecting civilians.

</summary>

"Back channels like these are vital in avoiding wars, de-escalating situations, and maintaining conflicts at a skirmish level."
"The goal of this direct military communication is to prevent great power wars and protect civilians from indiscriminate acts."

### AI summary (High error rate! Edit errors on video page)

The United States and Russia have established a direct line of communication over Ukraine to de-escalate tensions.
The hotline between DC and Moscow, similar to the famous red phone, has always existed but is now being used for military-to-military communications.
This communication channel can be used for scenarios like providing real-time intelligence to the Ukrainian military or clarifying misunderstandings about military activities.
In case of emergencies, like an aircraft issue outside Ukraine, the US might use the hotline to inform Russia of sending in pararescue teams.
The goal of this direct military communication is to prevent great power wars and protect civilians from indiscriminate acts.
The channel is a form of frank military-to-military diplomacy, with diplomats sometimes being involved as well.
Having this communication channel in place is a preventive measure to avoid NATO direct involvement and potential escalation into unwanted conflicts.
Back channels like these are vital in avoiding wars, de-escalating situations, and maintaining conflicts at a skirmish level.
Establishing this communication line is considered a positive step that should have happened earlier to prevent misunderstandings and potential conflicts.

Actions:

for diplomats, military personnel, peace advocates,
Establish direct lines of communication within your community or organization to prevent misunderstandings and conflicts (exemplified)
Advocate for the use of back channels for de-escalation and conflict prevention in international relations (suggested)
</details>
<details>
<summary>
2022-03-06: Let's talk about a teachable moment about race from Ukraine.... (<a href="https://youtube.com/watch?v=DD3XEatgJiw">watch</a> || <a href="/videos/2022/03/06/Lets_talk_about_a_teachable_moment_about_race_from_Ukraine">transcript &amp; editable summary</a>)

Beau addresses discrimination in Ukraine, questions Western media bias, and challenges global perceptions of white supremacy, urging immediate reflection on biases and inequalities beyond borders.

</summary>

"Beyond America's borders, do not live a lesser people. Beyond Europe's borders, do not live a lesser people."
"There are a lot of times when dynamic situations unfold that you wait until afterward to talk about stuff like this. This isn't one of them."
"It's something we need to fix."

### AI summary (High error rate! Edit errors on video page)

Talks about Ukraine as a teachable moment for the rest of the world, especially the United States.
Mentions reports and strong evidence of discrimination in Ukraine based on nationality and skin color.
Notes the lack of widespread media coverage on these discriminatory actions.
Criticizes Western media for heavily supporting Ukraine and potentially overlooking critical issues.
Questions the West's response to Ukraine compared to potential responses in other regions like Africa.
Addresses the concept of global white supremacy and its impact on perceptions and actions worldwide.
Points out the immediate unity of the Western world in response to Ukraine, raising questions about consistency in other regions.
Calls for a critical examination of media portrayals of different parts of the world as "semi-civilized."
Suggests that the West's strong response to Ukraine is influenced by familiarity and identification with the people there.
Urges acknowledgment of biases and inequalities beyond borders and advocates for addressing them promptly.

Actions:

for world citizens,
Challenge biases in media portrayals (implied)
Educate others on global inequalities and biases (implied)
Advocate for fair and unbiased media coverage (implied)
</details>
<details>
<summary>
2022-03-05: Let's talk about Zelenskyy and information operations.... (<a href="https://youtube.com/watch?v=2UBOFjtZv7w">watch</a> || <a href="/videos/2022/03/05/Lets_talk_about_Zelenskyy_and_information_operations">transcript &amp; editable summary</a>)

Information campaigns disrupt opposition by sowing doubt; plausibility is key, sidelining and destabilizing, forcing investigations and manipulating beliefs.

</summary>

"You're not the target. You're collateral in this."
"During wartime, there are a lot of information operations going on that are designed to influence people all over the world."
"But in order to get that information out, in order to make it believable, well they have to lie to you too."

### AI summary (High error rate! Edit errors on video page)

Information campaigns aim to disrupt unit cohesion in the opposition by making them doubt their cause or the people they are with.
A story broke about Russians planning to remove Zelensky, but Ukrainian special operations thwarted the attempt, possibly with a tip-off from Russian intelligence.
The effectiveness of an information campaign lies in its plausibility, not necessarily in whether it actually happened.
Possible reasons for the leak include allegiances to Ukraine or being a patriot for Russia trying to protect Putin from a bad decision.
The leak has sidelined those involved in the operation, causing internal security in the FSB to scramble to find the leak.
Well-trained individuals involved in the operation are now relegated to less sensitive positions to prevent future leaks.
The success of this information operation lies in its self-reinforcing nature and the doubt it sows within Russian intelligence.
While the information may not be true, the Russians are compelled to investigate to maintain loyalty and trust within their ranks.
Information operations during wartime are designed to influence people globally, often requiring the spread of misinformation.
Consumers of information should be aware of the potential for deception and manipulation in the information they receive.

Actions:

for global citizens,
Verify information sources (implied)
Stay critical of information received (implied)
Educate others on the potential for misinformation in information campaigns (implied)
</details>
<details>
<summary>
2022-03-05: Let's talk about No-fly zones and other options.... (<a href="https://youtube.com/watch?v=sPzQ1DyGZHs">watch</a> || <a href="/videos/2022/03/05/Lets_talk_about_No-fly_zones_and_other_options">transcript &amp; editable summary</a>)

Exploring the risks of advocating for a no-fly zone in Ukraine and the importance of transparent intentions to prevent unintended consequences.

</summary>

"Shallow foreign policy takes tend to lead to shallow graves."
"You see what's happening to civilians. And you don't like it, because you're human, right?"
"A force on force engagement between NATO and Russia on Ukrainian soil would be devastating for Ukrainians."
"We have to make sure that the something we do actually helps, doesn't hurt."
"For the overwhelming majority of people pushing this, it's not because they care. It's because they want to do something."

### AI summary (High error rate! Edit errors on video page)

Addressing the desire to help and get involved in response to distressing events.
Explaining the push for a no-fly zone in Ukraine without a comprehensive understanding.
Urging caution about the consequences of implementing a no-fly zone in Ukraine.
Describing the no-fly zone as a bluff that could potentially escalate into a force-on-force confrontation.
Emphasizing the importance of being transparent about intentions, especially when advocating for actions that may lead to war.
Pointing out the risks of NATO involvement escalating the situation in Ukraine.
Advocating for covert methods of support to avoid direct confrontation and maintain plausible deniability.
Suggesting the deployment of international volunteers as a potential solution while avoiding direct military intervention.
Critiquing the emotional rhetoric and lack of consideration for consequences in advocating for certain actions.
Warning against actions that could inadvertently harm civilians in Ukraine and provoke a larger conflict.

Actions:

for policy advocates and concerned citizens,
Coordinate an international force of volunteers to provide support to Ukraine (implied)
Provide supplies, weapons, and intelligence to Ukraine openly while maintaining plausible deniability for other forms of support (implied)
</details>
<details>
<summary>
2022-03-05: Let's talk about Biden, Congress, and Russian oil.... (<a href="https://youtube.com/watch?v=sh-L8k3RHnM">watch</a> || <a href="/videos/2022/03/05/Lets_talk_about_Biden_Congress_and_Russian_oil">transcript &amp; editable summary</a>)

Congress considers banning Russian oil, but Biden opposes it due to potential economic impacts on Americans and uncertainties about outcomes in Russia.

</summary>

"I do not like sanctions, economic warfare that hits the average person."
"Biden is already getting hammered for this because there are a lot of people who believe the president of the United States has way more control over gas prices than he actually does."
"The only good thing that I can see coming of this is that if something like this passes Congress, they're handing Biden a really good negotiation tool."
"I don't like kicking down. This will have a marked impact on the average Russian."
"Biden is already getting hammered for this because there are a lot of people who believe the president of the United States has way more control over gas prices than he actually does."

### AI summary (High error rate! Edit errors on video page)

Congress is considering a bipartisan effort to ban Russian oil, which the White House disagrees with.
Congress believes they may be able to affect regime change in Russia through economic means.
There are risks associated with creating chaos in Russia, including uncertainty about who may take over and the security of Russia's arsenal.
Shutting down Russian oil could lead to increased reliance on OPEC, impacting gas prices and causing economic strain.
Biden opposes the move to ban Russian oil, as it could lead to higher gas prices for Americans.
Congress aims to pressure Putin through economic measures, hoping for resistance from the people or oligarchs.
Biden may face sharp debate with Congress, including members of his own party, over this issue.
If Congress passes the ban, it could give Biden leverage in negotiations with Russia.
Economic sanctions can have a significant impact on the target country but come with uncertainties and potential consequences.
The debate between Congress and the White House revolves around the effectiveness and consequences of banning Russian oil.

Actions:

for policy makers, congress members,
Engage in informed debate and decision-making on economic measures affecting international relations (implied).
Advocate for diplomatic negotiations and alternative solutions to address political issues (implied).
Stay informed about geopolitical events and their potential impacts on global economies (implied).
</details>
<details>
<summary>
2022-03-04: Let's talk about why Lindsey Graham is on Russian TV.... (<a href="https://youtube.com/watch?v=VXQ_h9y4HxY">watch</a> || <a href="/videos/2022/03/04/Lets_talk_about_why_Lindsey_Graham_is_on_Russian_TV">transcript &amp; editable summary</a>)

Senator Lindsey Graham's call for Russian oligarchs to overthrow Putin risks escalating tensions, strengthening Russian propaganda, and impacting international relations, driven by a quest for toughness.

</summary>

"The habit that arose under Trump of saying the most inflammatory stuff possible in order to get views, retweets, or whatever, oh yeah, on top of saying it live, he also tweeted out the same stuff."
"Some things can't be said by certain people."
"It made it harder for the oligarchs to act if they chose to."
"There are a whole lot of people in the U.S. government that need to learn that they need to be quiet."
"He did it to get on TV and seem tough."

### AI summary (High error rate! Edit errors on video page)

Senator Lindsey Graham made statements calling for the oligarchs in Russia to overthrow Putin, potentially causing an international incident.
Graham's remarks could rally support behind Putin, convince Russia of an existential battle, and be used for propaganda purposes within Russia.
The senator's call for Putin's removal may have unintended consequences, such as impacting the security of Ukraine's President Zelensky.
Graham's posturing to look tough domestically lacks consideration for the international implications of his statements.
The senator's suggestion to Russian oligarchs to act against Putin is unnecessary, as they do not need his advice and are capable decision-makers.
The inflammatory remarks by Graham could escalate tensions and hinder diplomatic efforts in a conflict with a major power like Russia.
Graham's actions, driven by a quest to appear strong, have real impacts beyond domestic politics.
The habit of making provocative statements for attention needs to be curbed, especially given the heightened stakes in international relations.
There are legal and ethical considerations for U.S. government officials when discussing foreign leaders, particularly in times of heightened tensions.
Graham's call for Putin's removal could unintentionally strengthen Russian propaganda and influence the conflict dynamics.

Actions:

for u.s. government officials,
Refrain from making inflammatory statements that could escalate international tensions (implied).
Recognize the legal and ethical boundaries when discussing foreign leaders, especially during times of heightened tensions (implied).
</details>
<details>
<summary>
2022-03-04: Let's talk about shifting public health strategies.... (<a href="https://youtube.com/watch?v=9PQnDXOKGO4">watch</a> || <a href="/videos/2022/03/04/Lets_talk_about_shifting_public_health_strategies">transcript &amp; editable summary</a>)

The federal government is shifting public health strategies in the US, focusing on vaccination, masking, testing, and treatment, aiming to relax measures while staying prepared for future adjustments and relying on social responsibility.

</summary>

"Go get vaccinated."
"They're saying most Americans can go without a mask."
"I'll continue wearing a mask."
"Wash your hands, wear a mask, get vaccinated, kind of keep your distance."
"The idea here is we're at the point where hopefully social responsibility kicks in."

### AI summary (High error rate! Edit errors on video page)

The federal government is altering public health strategies in the United States, releasing new conditional guidance without clear explanations.
The shift in strategies is motivated by increased vaccination rates and the perception of a less severe strain circulating.
The new public health strategy consists of four main components: vaccination, masking, and testing/treatment.
Vaccination guidelines remain consistent: go get vaccinated.
Most Americans are now advised they can go without masks, but it depends on the risk level in their area (green, yellow, orange).
Testing and treatment are emphasized together, with options like covidtest.gov and one-stop shops for testing and antiviral treatment.
The goal of the new strategy is to relax public health measures while remaining prepared to reintroduce them if needed to prevent fatigue.
Beau personally plans to continue following public health guidelines: staying vaccinated, wearing masks, and getting tested if sick.
He stresses the importance of social responsibility in maintaining lower levels of COVID-19 transmission.

Actions:

for us residents,
Stay updated on the latest public health guidance and follow recommendations (exemplified)
Get vaccinated if eligible and encourage others to do the same (exemplified)
Wear masks in areas with higher risk levels and around vulnerable populations (exemplified)
Get tested if experiencing symptoms or potentially exposed to COVID-19 (exemplified)
</details>
<details>
<summary>
2022-03-04: Let's talk about Russia shelling a nuclear plant.... (<a href="https://youtube.com/watch?v=bPQo1LMENhQ">watch</a> || <a href="/videos/2022/03/04/Lets_talk_about_Russia_shelling_a_nuclear_plant">transcript &amp; editable summary</a>)

International Atomic Energy Agency confirmed no radiation changes after Russian military shelled a nuclear plant during combat, raising concerns and potential international repercussions.

</summary>

"Crisis averted for the moment."
"If this is how they're going to engage around something like this, we can expect things in cities to get even worse."

### AI summary (High error rate! Edit errors on video page)

International Atomic Energy Agency reported no changes in radiation levels after the Russian military shelled a nuclear power plant during active combat.
Firefighters were unable to reach the scene due to the ongoing combat, causing tense moments and sparking concerns about potential consequences.
The attacked plant is different from Chernobyl, being a light water system that can be shut down quickly in emergencies.
The incident may impact international opinion, especially in Europe, where concerns were high due to the risks posed.
Further intervention and condemnation of Russia's behavior in the war are possible outcomes following this incident.
The plant, which generates about 20% of Ukraine's power, was strategically significant for Russia in the context of the war.
Russia's methods of obtaining control over the plant were criticized, raising concerns about potential global impacts.
There are lingering questions about the aftermath of the shelling and its potential long-term effects.
While the situation was averted for now, the behavior exhibited by Russia sets an ominous tone for future engagements.
The incident hints at potential escalations in cities if similar actions continue.

Actions:

for global citizens,
Contact local representatives to express concerns about nuclear safety and international conflicts (implied).
</details>
<details>
<summary>
2022-03-03: Let's talk about whether Putin already lost.... (<a href="https://youtube.com/watch?v=VXKSM8lpqKM">watch</a> || <a href="/videos/2022/03/03/Lets_talk_about_whether_Putin_already_lost">transcript &amp; editable summary</a>)

Beau provides an in-depth analysis of Putin's failed operation in Ukraine, showcasing how it has backfired across objectives, geopolitics, military, and personal impacts, leading to inevitable losses for Russia.

</summary>

"Taking a country and holding a country are not the same thing."
"Even if he wins, he loses at this point."
"It's really easy to get into a place like this. It's hard to get out."
"There's no real coming back from this."
"It was an abject failure across the board."

### AI summary (High error rate! Edit errors on video page)

Providing an in-depth analysis of Putin's operation and its success or failure seven days in.
Four metrics to analyze such operations: stated objectives, geopolitical fallout, military applications, and personal impacts.
The operation's pretext was to protect Russian lives and stop NATO expansion, but the real reason was to unite Belarus, Ukraine, and Russia into a super state.
The operation failed to accomplish its objectives, leading to animosity towards Russia and strengthening ties between Ukraine and the West.
Geopolitically, the operation backfired by reuniting NATO, speeding up European bloc cooperation, and damaging Russia's economy.
Militarily, the operation showcased Russian weaknesses rather than strength.
Personally, Putin's legacy is at stake, as this move could relegate Russia to a major power instead of a great power.
Oligarchs who greenlit the operation may face heavy consequences, losing billions and being sanctioned.
Russia may be able to take Ukraine but likely won't be able to hold it due to displayed military weaknesses.
Putin faces a tough decision between withdrawing to save his legacy or doubling down and facing further losses.
The operation is deemed a failure across the board, even if Putin were to succeed in taking Ukraine.

Actions:

for global policymakers,
Organize aid for Ukrainians (implied)
Advocate for diplomatic solutions (implied)
Support efforts to strengthen ties between Ukraine and the West (implied)
</details>
<details>
<summary>
2022-03-03: Let's talk about Ukraine offering $40k to Russians.... (<a href="https://youtube.com/watch?v=3C_VUbVOhhM">watch</a> || <a href="/videos/2022/03/03/Lets_talk_about_Ukraine_offering_40k_to_Russians">transcript &amp; editable summary</a>)

Ukraine offers asylum and money to Russian soldiers in a humane and effective program to provide a new life and remove soldiers from the battlefield, urging social media to spread awareness and potentially save lives.

</summary>

"It's the best idea I have ever heard of in wartime."
"They're human. They're people. And a lot of them didn't sign up. They were forced into it."
"This is probably something that should be mentioned on social media a lot."
"You could literally save a life or two."
"I think it's a great idea."

### AI summary (High error rate! Edit errors on video page)

Ukraine offers asylum and money to Russian soldiers who surrender.
The offer aims to provide a new life to Russian soldiers.
Many Russian troops in Ukraine are conscripts with no choice.
Conscription in Russia is used as punishment for dissidents.
The program offers a way out for those forced into combat.
It's a humane and effective idea to remove soldiers from the battlefield.
Some soldiers may have signed up due to limited options.
Social media should spread awareness about the offer to reach more soldiers.

Actions:

for social media users in europe.,
Spread awareness on social media about Ukraine's offer to Russian soldiers (suggested).
Share information about the program to potentially save lives (suggested).
</details>
<details>
<summary>
2022-03-03: Let's talk about Trump, Eastman, and the committee.... (<a href="https://youtube.com/watch?v=cvEUK_iYoWw">watch</a> || <a href="/videos/2022/03/03/Lets_talk_about_Trump_Eastman_and_the_committee">transcript &amp; editable summary</a>)

Beau explains the context behind the committee's filing, alleging criminal behavior by Trump, signaling potential legal troubles ahead.

</summary>

"They believe they already have evidence of a crime."
"We think Trump broke the law."
"I have to say that I would be worried."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the context behind the news involving the committee, Trump, and Eastman.
John Eastman, a key figure in Trump's election amendment efforts, invoked attorney-client privilege.
The committee argued that this privilege should not apply, suggesting evidence of a crime already exists.
The committee believes there is a good faith basis for a criminal conspiracy involving Trump and his campaign.
Allegations include felony obstruction of official proceedings and conspiracy to defraud the United States.
A smoking gun is an email from Eastman to Pence, suggesting a minor violation of the law to delay election certification.
Trump's aides reportedly informed him that his legal actions post-election were futile.
This indicates that Trump's persistence despite being aware of the situation could be seen as corrupt behavior.
The committee's filing signals their intent to pursue criminal prosecution against Trump.
While there are hurdles to overcome, the committee is determined and confident in their evidence.
The focus seems to be on moving towards criminal prosecution rather than preserving the institution of the presidency.
There is a long road from the current filing to actual prosecution, but the intention is clear.

Actions:

for legal analysts, political commentators,
Contact legal experts for insights on the implications of the committee's filing (suggested).
Stay informed about the developments in the legal proceedings against Trump (implied).
</details>
<details>
<summary>
2022-03-02: Let's talk about videos from Ukraine and contradictory statements from me... (<a href="https://youtube.com/watch?v=vJldG1nh6lg">watch</a> || <a href="/videos/2022/03/02/Lets_talk_about_videos_from_Ukraine_and_contradictory_statements_from_me">transcript &amp; editable summary</a>)

Beau delves into the dynamics of predictability, surprise, and endurance in unconventional conflicts, cautioning against romanticizing resistance videos and urging a realistic perspective on civil conflict.

</summary>

"Professional soldiers are predictable, but the world is full of amateurs."
"In these types of conflicts, it's not about who can dish out the most, it's about who can take the most."
"Eventually, either the hammer or the anvil is going to break. It's normally the hammer."

### AI summary (High error rate! Edit errors on video page)

Addresses seemingly contradictory statements he has made and the need for additional context to understand them.
Explains the advantage of amateurs over professional soldiers in terms of predictability and surprise.
Talks about the importance of speed, surprise, and violence of action to win an engagement.
Mentions the Ukrainian resistance confusing the Russian military with unpredictable tactics.
Describes videos from Ukraine showing unconventional tactics against armored vehicles.
Points out that amateurs in combat often have the element of surprise but may not always be effective.
Emphasizes that in unconventional conflicts, it's about breaking resolve rather than traditional military victory.
States the importance of endurance and resilience in prolonged conflicts.
Warns against glorifying civil conflict and encourages reflecting on the true nature of such conflicts.
Recommends picturing the reality of conflict through images like cars with "children" signs rather than romanticizing videos of resistance fighters.

Actions:

for observers of conflicts,
Analyze and understand the dynamics of unconventional conflicts (implied)
Avoid glorifying or romanticizing civil conflict (implied)
</details>
<details>
<summary>
2022-03-02: Let's talk about the SOTU, Biden, and European worries about Russia.... (<a href="https://youtube.com/watch?v=naddJbhUYjw">watch</a> || <a href="/videos/2022/03/02/Lets_talk_about_the_SOTU_Biden_and_European_worries_about_Russia">transcript &amp; editable summary</a>)

Beau analyzes the state of the union, addresses concerns about a potential Russian offensive against NATO, and underscores Russia's technological and logistical shortcomings compared to NATO.

</summary>

"No disrespect meant to the military in Ukraine. Ukraine is not NATO."
"They understand that the technological gap that has developed and the logistical and intelligence gaps that have developed were just demonstrated to the world."
"Imagine what happened in NATO's technology and it is clear that Russian technology has not actually kept up."

### AI summary (High error rate! Edit errors on video page)

Beau gives his take on the state of the union address by Biden, mentioning a specific part that gave him pause.
People in Europe, particularly in NATO countries, have expressed concerns about a potential conventional offensive from Russia.
Beau reassures that a conventional offensive from Russia against NATO is not likely to happen.
He points out that Russia is not faring well in Ukraine, making critical errors that NATO had demonstrated to exploit 30 years ago.
The mistakes made by Russia in Ukraine are being capitalized on by the Ukrainians, which could be unforgiving against NATO.
Beau explains how a similar situation to what's happening in Ukraine now played out in the first Gulf War, leading to devastating consequences for the Russian convoy.
Russia's technological and logistical gaps compared to NATO make a conventional fight against NATO highly unlikely.
Beau suggests that even Russian generals might oppose Putin if he orders a conventional fight with NATO due to its futility.
Non-NATO European countries, except for Moldova, are unlikely to face significant risks due to the technological gap between Russia and NATO.

Actions:

for international observers,
Keep abreast of international developments and diplomatic relations (implied)
</details>
<details>
<summary>
2022-03-02: Let's talk about staying behind and Kherson.... (<a href="https://youtube.com/watch?v=Ws3p8nw8ZHA">watch</a> || <a href="/videos/2022/03/02/Lets_talk_about_staying_behind_and_Kherson">transcript &amp; editable summary</a>)

Beau talks about the potential development of stay-behind tactics in Ukraine, draining the resolve of the opposition in a campaign that may go unnoticed.

</summary>

"It's likely something we're going to see start to develop in the coming weeks."
"This is the beginning of the truly irregular phase."
"It's incredibly likely that in the beginning it is going to be very, very successful."
"We probably won't know right away because unlike other events, this isn't going to get a lot of coverage."
"I don't think that Ukraine's ready to give up yet."

### AI summary (High error rate! Edit errors on video page)

Talks about the concept of staying behind when others leave, a topic previously discussed historically.
Mentions the city of Kherson in Ukraine coming under Russian control and the potential development of stay-behind tactics.
Describes the goal of stay-behind campaigns as being a thorn and draining the resolve of the opposition.
Explains that stay-behind organizations wait, watch, and learn before becoming active.
Points out that the success of such campaigns depends on making the occupying forces relax and divert resources.
Differentiates between organized and less organized campaigns in terms of how they start.
Explains that success for resistance forces involves demoralizing the opposition and forcing them to divert resources back.
Notes the irregular phase where conventional forces may fall, leaving irregular forces to continue resistance.
Mentions the comparison to tactics used by American forces in the Mideast.
Speculates on the success of the campaign based on the training and caliber of troops involved.
Acknowledges uncertainty about whether the campaign will start and its coverage due to being behind Russian lines.
Suggests getting ready to see potential developments in Ukraine.

Actions:

for ukrainian resistance supporters,
Prepare for potential developments in Ukraine (implied).
</details>
<details>
<summary>
2022-03-01: Let's talk about the most expensive energy auction in US history.... (<a href="https://youtube.com/watch?v=hIsbjuCE01k">watch</a> || <a href="/videos/2022/03/01/Lets_talk_about_the_most_expensive_energy_auction_in_US_history">transcript &amp; editable summary</a>)

The U.S. witnesses a record-breaking $4.37 billion wind power auction, signaling a significant shift towards addressing environmental concerns through profitable investments.

</summary>

"If it doesn't make money, it's not worth anything."
"It shouldn't take this to get change, but when companies are willing to drop this much money, they're going to make it work."
"It's sad that it takes commodifying everything to get anything good done."
"They're not going to be environmentalists; they're going to care about the return on their investment."
"Once they make their money here, they're going to want what every other company wants, more."

### AI summary (High error rate! Edit errors on video page)

The largest, most expensive energy auction in U.S. history just happened, grossing $4.37 billion over three days involving six companies.
This auction is how the U.S. government manages federal lands for energy production, allowing companies to bid for access to certain sites.
Surprisingly, the auction wasn't for oil or natural gas but for wind power sites three miles off the coast of New Jersey and New York.
This shift towards wind power signifies a significant step in addressing environmental and climate issues in the United States.
Despite the positive impact on the environment, the focus for many Americans remains on the financial aspect, with $4.37 billion and thousands of jobs being key points of interest.
Companies are willing to invest heavily in wind power because they see a profitable return on investment due to the abundance and free access to wind as an energy source.
The lucrative nature of wind power will likely lead to a domino effect where more companies will invest, leading to increased campaign contributions and political interests in green energy.
While it's unfortunate that profit drives environmental change, the significant investments in wind power are seen as a positive step towards a greener future.
The scenario underscores the importance of financial interests in driving change, where companies prioritize returns on investments over environmental concerns.
Beau concludes by noting the necessity of commodifying environmental efforts to ensure efficient and rapid progress towards widespread renewable energy usage.

Actions:

for environment enthusiasts, policymakers,
Advocate for policies that support renewable energy initiatives (implied)
Support companies investing in green energy by choosing sustainable options (implied)
Stay informed and engaged in environmental issues to drive positive change (implied)
</details>
<details>
<summary>
2022-03-01: Let's talk about a Russian article from the future.... (<a href="https://youtube.com/watch?v=WWIYfKWSAxw">watch</a> || <a href="/videos/2022/03/01/Lets_talk_about_a_Russian_article_from_the_future">transcript &amp; editable summary</a>)

Understanding the leaked article reveals Russia's imperialistic ambitions in Ukraine, driven by Putin's quest for power and legacy, shedding light on the deceptive nature of foreign policy.

</summary>

"Countries don't announce what they want to do. They come up with pretexts to explain their actions."
"It's imperialism, nothing more."
"For those who have been going to bat for Putin on this, you need to read it the most."
"It's about power. It's about some old guy wanting more power, his name in a history book."
"It's always the same thing."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of understanding intent in foreign policy and how countries don't openly reveal their intentions.
Mentions a leaked article from Russian state media discussing their victory in Ukraine.
Indicates that the article was likely pre-written and accidentally published ahead of schedule.
Describes the content of the article, which boasts about Russia's actions and goals in Ukraine.
Explains that the ultimate goal is to bring Belarus and Ukraine under Moscow's direct control to elevate Russia's power.
Criticizes the imperialistic nature of Russia's actions and Putin's desire for a more powerful Russia.
Suggests that Putin sees this as his legacy and Russian soldiers may be unaware of the true motives behind the invasion.
Encourages reading the leaked article for insights into Kremlin's messaging and Putin's mindset.
Points out the racist undertones in the article, aimed at stoking ethnic divisions in the West.
Condemns the false justifications put forward by Putin and urges those who defended him to reconsider.

Actions:

for foreign policy analysts,
Read the leaked article to understand Kremlin's messaging and Putin's goals (suggested)
Challenge false justifications and narratives surrounding Russia's actions (implied)
</details>
<details>
<summary>
2022-03-01: Let's talk about Psaki, Hawley, energy, and national security.... (<a href="https://youtube.com/watch?v=HEKAnEfpr64">watch</a> || <a href="/videos/2022/03/01/Lets_talk_about_Psaki_Hawley_energy_and_national_security">transcript &amp; editable summary</a>)

Beau fact-checks Senator Hawley's claims on energy production, while supporting Psaki's argument that green energy is vital for national security.

</summary>

"If you are against protecting the environment, if you are against green energy, you are also against American national security, period, full stop."
"The Department of Defense tends to know a little bit about national security."
"Wars will be fought over this."

### AI summary (High error rate! Edit errors on video page)

Analyzing statements made by Senator Hawley and Press Secretary Psaki regarding national security and energy.
Senator Hawley criticizes President Biden for shutting down American energy production and greenlighting Russian energy production.
Beau fact-checks Senator Hawley's claim by pointing out that the US has increased natural gas and oil production under Biden.
Beau explains that Biden's focus on renewables actually leads to more energy production, not less.
Beau suggests that Senator Hawley may be referring to Biden's desire to stop federal leases, which was halted by a court order.
Press Secretary Psaki argues that national security is tied to transitioning to green, independent energy sources produced within the country.
Beau supports Psaki's statement by citing the Department of Defense's emphasis on the importance of green energy for national security.
Beau warns that failure to switch to cleaner energy sources could lead to conflicts over energy resources in the future.

Actions:

for internet viewers,
Contact your representatives to support policies that prioritize green energy and national security (implied)
</details>
