---
title: Let's talk about Russia's new plan, numbers, and comparisons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=y6LyFFBQIYk) |
| Published | 2022/03/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains how Russia's strategy is not going according to plan.
- Compares estimates of Russian losses from different sources.
- Points out discrepancies in Russia's official loss estimate.
- Mentions a Russian news outlet publishing and then deleting a more accurate loss estimate.
- Analyzes the comparison of Russian troop losses to U.S. combat deaths.
- Disputes the theory that Russia is strategically sacrificing weaker units first.
- Provides insight into the types of military equipment being used by Russia in the conflict.
- Emphasizes the significant failure and high casualty rate on Russia's side.
- Concludes that the situation is far from planned and constitutes a catastrophic failure.
- Suggests that Russia may be shifting its focus and abandoning certain strategic offensives.

### Quotes

- "This is not planned. It is more than Afghanistan, Iraq, both times."
- "This is a catastrophic failure. It's not some 4D chess that Putin's playing. They failed."
- "This theory is garbage. It needs to go away."
- "It is not going according to plan, unless the plan was to lose in a spectacular fashion."
- "Anyway, it's just a thought."

### Oneliner

Beau explains the flawed Russian strategy and catastrophic failure in Ukraine, debunking theories of intentional losses and underscoring the significant casualties incurred.

### Audience

Analytical viewers

### On-the-ground actions from transcript

- Monitor international news outlets for updates on the conflict (implied).
- Support diplomatic efforts to end the conflict (implied).

### Whats missing in summary

Deeper analysis of the potential implications of Russia's shifting strategy and the impact on the ongoing conflict in Ukraine.

### Tags

#Russia #Ukraine #MilitaryStrategy #Comparisons #CatastrophicFailure


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk about numbers
and comparisons and technology.
And we're going to examine a theory
that keeps getting thrown out.
And we're going to demonstrate
why that theory is less than accurate.
I'm sure you've probably heard somebody say, oh, this is all going according to plan.
This is the way Russia wants it.
It's a strategy to lull the opposition into a false insecurity or something.
Some story that is basically very similar to, I would have won that race, but I left
my good shoes at home.
type of thing. We're going to demonstrate why that's not actually the case. We're
going to start by examining Russian lost and providing a comparison. In any war,
you have multiple estimates when it comes to the lost. You have the estimate
that is produced by the country taking the losses. Those are always low. You have
have the estimate produced by the country inflicting them.
Those are always high.
You have a third party estimate that is normally a range.
The low end of that range is, hey, we've confirmed this.
We have seen this with our own eyes via satellite or photo or social media post or whatever.
And then they produce an aggressive estimate based on that.
And in the middle ground is where that normally, that's what the accurate number normally
is.
Okay, so what is Ukraine's estimate when it comes to Russian lost?
It's ridiculously high.
I want to say like 18,000, 19,000, something like that.
It's high.
The third party estimate ranges 7 to 15, 7,000 being, we have seen this via satellite.
We've seen this with our own eyes.
That is 100% confirmed.
And then 15,000 is the aggressive estimate, which would put the real number somewhere
between 10 and 13.
Then you have the estimate coming from Russia itself, which I want to say is like a thousand,
something like that.
They are definitely not telling the truth, which is common, most countries do that.
I know that when the estimate was right around 500, that at that point I had personally viewed
more loss than that via footage.
They are not telling the truth.
However, we had this interesting thing happen.
A Russian news outlet published an article going over the lost and put the number right
under 10,000, like 9,950 or something like that.
This was a few days ago.
That article quickly got deleted and then was replaced with the official line.
I'm going to say that's the right number.
It's in line.
It's on the low end of the US estimates, of the NATO estimates.
But that's an accurate number.
It's going to be right around that.
So we're going to use that number, 10,000, as our baseline here.
As soon as the conflict started, you had Western media making comparisons to put things in
perspective for Western viewers.
A few days into it, you heard they've lost as many troops as we lost in Afghanistan.
And then it was as we lost in Iraq.
And then it was as we lost in Afghanistan and Iraq.
And then they just stopped making those comparisons.
What if we decided to go ahead and make it again?
What would it be?
Russia has lost more troops in 30-something days than total U.S. combat deaths since the
end of Vietnam, all of them.
I want you to think about how often the United States has troops in harm's way, all the
way back to the 70s.
That's how bad it is.
This is not planned.
It is more than Afghanistan, Iraq, both times.
So 2003 and Desert Storm, Somalia, Bosnia, Panama, Grenada.
These are all the ones that people would immediately think of, but it also includes the stuff you're
not thinking of, like Beirut or the US involvement in the Civil War in El Salvador.
of it since the end of Vietnam. But even that isn't really an accurate comparison
because the Russian number is about 20% higher. This isn't planned. This is a
catastrophic failure. It's not some 4D chess that Putin's playing. They failed.
The idea that it is, that it's going according to plan, that's propaganda put
out for people who may have access to the Internet in Russia. That's what that's
there for, to help ease their mind. Now, when this theory gets thrown out, it's
It's normally accompanied by the idea that Russia is putting in its bad units first.
First let me just go ahead and tell you, that's not a thing.
Nobody does that.
This isn't Sid Meier civilization where you send in your units that weren't upgraded to
be destroyed.
That's not a thing.
And the way they try to justify this is by saying, look, these are T-72 tanks.
Those are old tanks.
And yes, and admittedly, there have been some, like, original T-72s spotted in Ukraine.
That's a fact.
However, saying it's a T-72 and not really going any further is a lot like looking at
Ford Mustang and saying, oh, that's from the 60s. The T-72B3 is an upgraded design that
was produced in 2016, 2013, 2016. There are quite a few variants that are pretty modern
and they have been found on fire in Ukraine.
They're not just sending in their junk equipment.
They do have a lot of junk equipment, but that's from poor maintenance.
That's from not modernizing, not professionalizing.
It's not part of some 4D chess plan.
This is a failure.
They are losing troops at a staggering level.
Think about how often the United States sends its troops overseas to protect American interests,
all of them, since the end of Vietnam.
And that's not even really matching their total.
And they managed to lose that in a little more than a month.
This theory is garbage.
needs to go away.
Now what does this mean?
What can we infer from this?
Putin, it does appear that Russia is now shoring up everything on the eastern side of things,
and it looks like they're trying to reposition, maybe regroup a little bit, but it certainly
appears as though they have given up on the idea of offensives around major cities further
to the west, including the capital, at least at this moment.
It is not going according to plan, unless the plan was to lose in a spectacular fashion.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}