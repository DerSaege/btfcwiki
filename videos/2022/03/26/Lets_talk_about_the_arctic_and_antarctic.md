---
title: Let's talk about the arctic and antarctic...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6mSRi919jb4) |
| Published | 2022/03/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unprecedented warm weather at the poles with Antarctica 40 degrees above average and the Arctic 30 degrees above average.
- Both the Arctic and Antarctica should be experiencing different seasons, but they are both warm, leading to melting in both places.
- Extreme weather events are linked to these temperature anomalies.
- Experts suggest this may be a freak occurrence, but the situation needs careful monitoring as it could be a bad sign if it repeats.
- Urges for a shift from outdated tech causing problems to address environmental, foreign policy, defense, and economic issues.
- Emphasizes the need for bold action now to avoid worsening impacts of climate change.
- Calls for someone, possibly from the Republican Party, to step up and advocate for urgent climate action to bridge the political divide.
- Warns of irreversible impacts and stresses the importance of addressing climate change promptly.

### Quotes

- "The reliance on old technology, stuff that burns, right? It's a foreign policy issue. It's a defense issue. It's an economic issue."
- "The longer we wait, the worse it gets."
- "We're going to experience impacts from it. We're going to have problems already."
- "There is no do-over on this."
- "We have to make the case and we've got to do it soon."

### Oneliner

Unprecedented warm weather at the poles signals the urgent need for bold climate action now, bridging political divides for a sustainable future.

### Audience

Climate advocates, policymakers

### On-the-ground actions from transcript

- Advocate for urgent climate action within your community and with policymakers (suggested)
- Stay informed about climate change impacts and spread awareness (exemplified)
- Support political leaders who prioritize environmental issues (implied)

### Whats missing in summary

The transcript stresses the critical need for immediate action to address climate change and advocates for political leaders to step up and make the case for urgent environmental action.

### Tags

#ClimateChange #GlobalWarming #UrgentAction #PoliticalLeadership #EnvironmentalAdvocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the Arctic and Antarctica
and the weather and what we're doing.
There has been some wild weather occurring at the poles.
It's been overshadowed because of all of the other news.
But in Antarctica, it's 40 degrees above average.
40 degrees.
For Americans, it's 75-ish, somewhere in there,
degrees above average.
The Arctic is 30 degrees above average, or 50-ish above
average.
It's substantial.
It's a big deal.
The other thing that's really important to understand
about this bizarre weather is that there
are different seasons.
They're on opposite ends, and they
should be doing different things.
They both shouldn't be warm right now.
The Arctic should be just now slowly coming out of winter.
And Antarctica should be kind of quickly cooling after summer.
But they're both warm.
You have melting in both places.
It is unprecedented.
It's a word that gets thrown around a lot,
but really, there's no precedent for it.
It's the right word here.
The Intergovernmental Panel on Climate Change,
it talks about a whole bunch of different things
that become thresholds.
And one of the problems is that as you cross each threshold,
as each thing happens, some things are irreversible.
But more importantly, it snowballs,
for lack of a better term.
And it accelerates.
This is a sign that there's an issue, a huge one,
as if we needed more.
All of the extreme weather that we've been having,
it's all related to this.
Now, it is worth noting that there
are a couple of people who are really well-versed
in this field who are not climate change deniers who
are saying, this may just be a truly freak occurrence,
but we have to watch it.
Because if it happens again, it's a really bad sign.
So this is something that we need to pay attention to.
But aside from just paying attention to it,
when are we going to make the switch?
How long are we going to continue
to use old, outdated tech that's causing problems?
I can't think of a better time than right now
to really make this push and sell it to the American people
and to the world.
The reliance on old technology, stuff that burns, right?
It's a foreign policy issue.
It's a defense issue.
It's an economic issue.
It's an environmental issue.
And it's all going to be a problem.
It's an environmental issue.
And it's all coming together right now.
This is the time for that bold moment, for somebody
to step out and really sell it.
Get out there and make the case that this has to happen.
It has to happen now.
The longer we wait, the worse it gets.
I don't know who in American politics
has the ability to make that case.
Because whoever it is, it's got to be somebody
who can appeal to the Republican side of the aisle.
You have a lot of young, energetic people
in the Democratic Party who can talk about this
and talk about it from an informed place.
And they can make the case.
The problem is, because the US has become so polarized,
the people that need convincing, they won't listen to them.
We're looking for somebody in the Republican Party
who can make this case.
Or somebody new to politics who happens
to run as a Republican that may have other ideas.
That may work, too.
And it may be necessary at some point.
I'm just saying.
Because this is one of those things we can't lose.
Messing up here, getting this wrong, that's it.
There is no do-over on this.
And when you start reading these studies,
even the optimistic ones say that we're
going to experience impacts from it.
We're going to have problems already.
Even if we start fixing everything now,
we will still have issues.
We will still have to deal with the effects
of being bad stewards of the world,
of putting off a change that we knew we had to make.
This, to me, given the current geopolitical situation,
seems like the ideal time for somebody to make the case.
When the average person is aware of some of these problems,
they see how it's impacting the world.
Because if you wait and you don't make the case now,
we don't talk about it now, we don't talk about it.
If we make the case now, we don't talk about it now,
the American people have a pretty short attention span.
We have to make the case and we've got to do it soon.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}