---
title: Let's talk about the country in the mirror....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H4gedJZqm3o) |
| Published | 2022/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US and its Western allies have historically been the main countries capable of projecting force, but now other countries are gaining that capability.
- The story of Hassan, an 11-year-old boy who fled violence alone, illustrates a stark contrast in how different countries treat migrants and refugees.
- Beau criticizes the US for its treatment of migrants, contrasting it with the help offered to refugees in other countries like Slovakia.
- He points out the hypocrisy in differentiating between refugees and migrants based on race or shade, citing examples from European countries like Hungary.
- Beau draws parallels between American actions in conflicts like Afghanistan and Iraq and criticizes the media's portrayal of similar actions by other countries.
- He stresses the importance of acknowledging past mistakes and changing course to lead the world in a better direction.
- Beau urges Americans and Western societies to embrace discomfort, as it signals an opportunity to correct past wrongs and strive for a more just future.

### Quotes

- "If something is wrong, it is wrong. If something is right and good and true, it is."
- "We haven't been guiding the world to a better place, but we could if we wanted to."
- "It's OK to be uncomfortable."
- "We haven't been doing what we should as a society, but we can fix it."
- "There are going to be topics coming up that are going to make people uncomfortable."

### Oneliner

The US and its Western allies must confront uncomfortable truths about their past actions to lead the world in a better direction.

### Audience

Policy Makers, Activists, Global Citizens

### On-the-ground actions from transcript

- Acknowledge past mistakes and work towards leading the world in a better direction (implied).

### Whats missing in summary

The full transcript provides a deeper exploration of the need for self-reflection and uncomfortable truths to pave the way for positive change on a global scale.

### Tags

#US #WesternAllies #Migrants #Refugees #GlobalLeadership


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about the country in the mirror,
because people in the United States,
in Western countries in general, but particularly
the United States, they need to get ready to look
into that mirror and be prepared for the fact
that they are not going to like what they see,
because we are going to have that mirror held up to us.
See, for a long time, the US and its Western allies,
really the only country is capable of projecting force,
of really using muscle to influence world affairs.
See, that's changing.
Things are shifting, and you have other countries
capable of doing it and doing it and now that it's another country where we're
seeing things and we don't like it but if you were to kind of hold a mirror up
to it what would you see like right now the whole country is just enamored with
the story of Hassan, an 11-year-old boy who fled the violence by himself. He had
nothing but a plastic bag and a phone number scribbled on his hand. And he
traveled hundreds of miles by himself with a group of people he didn't know by
himself without his family. He was with people he didn't know though. He gets to
the border of Slovakia and what happens? He's given water and people are like, hey
let's call this number. Find your people. So what happens there? What happened here?
A young boy travels hundreds of miles by himself with a group of people he doesn't know.
Let's call it a caravan. His number scribbled on his hand. He gets to this border. What happens?
When we break out those little baby handcuffs, throw him in a cage, and wash that number off,
make sure he can't find his family. That's what we do.
to what we've done.
And I know somebody's going to say, well, that's different, you know, refugee and migrant.
Yeah, you can hear that rhetoric in some countries in Europe.
Hungary, there's a guy named Orb??n who's saying that exact thing.
Refugees can get all the help they want, but they're going to stop those migrants.
Given that he is a right-wing, nationalistic, racist person who often gets championed on
Fox, I'm willing to bet it's pretty easy for them to determine who is who, and it
has to do with their shade. We're going to see our own actions, and we're not
going to like them, and it doesn't stop there. From people talking about the
civilian casualties that Russia is inflicting. Yeah. Ever look at the numbers
from Afghanistan and Iraq? They're not small. Then you can look to American
media just being beside itself that Russians are adorning themselves with
the letter Z to show support. I don't know how that's much different than the
yellow ribbon that was on everybody's car in the early 2000s. It's not that
different. Even on this channel, you know, I said I didn't like the average Russians
getting hit by these sanctions the way they're going to. People are like, no,
that's how it should be. Yeah, if that had happened to us, we'd still be recovering from 2003.
We are not going to like what we see, if we're honest. And that's okay. Because the reality is,
we haven't been leading. Not in a way to make the world better. We've been profiting. That's
not the same as leading. And we can see it. We'll be able to see it because Russia and China are
going to start making moves that are going to reflect stuff that we've done
and we're not going to like it and that's okay because it gives us a baseline.
These things are wrong. If something is wrong, it is wrong. If something is
right and good and true, it is. We're going to be able to see it and then we
should be able to correct our own path and acknowledge that we haven't been
leading. We've been profiting and that's all that we've been focused on. We
haven't been guiding the world to a better place, but we could if we wanted
to. We're going to have examples of our behavior thrown right back at us and
And we're going to have it for a few years.
And the longer we put off acknowledging it,
the longer we put off changing course,
the longer it's going to happen, the more uncomfortable
it's going to get.
So from the American standpoint, from the Western standpoint,
while this is happening, as we start
talking about all of the different things going on,
it's OK to be uncomfortable.
We haven't been doing what we should
nations, as a society, but we can fix it because there are going to be topics
coming up that are going to make people uncomfortable. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}