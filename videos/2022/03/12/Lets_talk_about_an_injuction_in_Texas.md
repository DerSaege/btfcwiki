---
title: Let's talk about an injuction in Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OrOmLAU27PU) |
| Published | 2022/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Good news out of Texas: courts intervene in a case where the governor issued an edict undermining medical advice for kids, motivated by upcoming elections.
- The edict prohibited parents from following medical professionals' advice regarding their kids' gender identity.
- Governor threatened investigations by Child Protective Services if parents followed medical advice over his statement.
- Aim of the edict was to rally his base by demonizing kids.
- Courts issued an injunction against the edict until the case is heard in July.
- Legal experts agree that the edict violates both US and Texas state constitutions.
- Governor Abbott's actions contradict the policies of medical freedom and parental rights that were resonating with voters.
- Abbott's timing of the edict coincided with another controversial story to possibly divert attention.
- Despite the failed attempt to divert attention, the fight is ongoing, and people in Texas still need help.
- Big businesses are beginning to push back against such legislation due to its unpopularity and potential impact on their bottom line.

### Quotes

- "Parents don't have any rights when it comes to medical procedures and advice."
- "The fight's not over. People are still going to need help and people in Texas are still going to be worried."
- "This may have been a really bad move for Abbott politically, set aside the fact it's obviously a bad move morally."

### Oneliner

Good news out of Texas as courts step in to block Governor Abbott's edict undermining medical advice for kids, but the fight continues against harmful legislation.

### Audience

Texans, Advocates

### On-the-ground actions from transcript

- Support organizations advocating for medical freedom and parental rights (implied).
- Stay informed and engaged in local politics to support initiatives protecting children's rights (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Governor Abbott's controversial edict and the legal and political implications surrounding it.

### Tags

#Texas #GovernorAbbott #Courts #MedicalAdvice #ParentalRights #Legislation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So we have some good news fresh out of Texas.
The courts have decided to step in in a case that they definitely needed to step in.
If you're not aware of what's going on,
the governor out there,
in an attempt to
rally his base
prior to
upcoming elections,
issued an edict
that basically said parents were to disregard
medical professionals and listen to a politician when it comes to their
their kids.
That's really what it boils down to. They can dress it up however they want, but that's what it was.
Now the courts have stepped in
and said, yeah, this isn't flying. They've issued an injunction
and that injunction will remain in place until July when the case is heard.
This edict stopped parents from following the advice of medical professionals
when it comes to
gender identity of their kids.
A politician single-handedly
wrote out a statement and said, hey, if you follow the advice of docs or docs give you this advice,
well, we're going to have them investigated, have you investigated
by Child Protective Services.
And the whole goal of this
was to rally his base
because he's got to give them somebody to demonize,
to kick down at, and in this case it's kids.
But again, the courts have stepped in
which, to be honest, I was a little worried
because it's Texas. I'm not going to lie. That was my sole reason for being worried.
Every,
every single lawyer I've talked to about this says that this is not only against the US Constitution,
it also violates Texas' state constitution.
Now for the political aspects,
it doesn't make any sense there either.
The only two policies that Republicans had that were gaining any traction,
that were resonating with voters,
was the idea of medical freedom,
which basically meant they didn't want to wear a mask,
and the idea of parental rights,
which meant they didn't want to read, didn't want their kids to read.
Those were the two terms that were resonating with voters.
And what does Governor Abbott do?
Issues a proclamation
saying that
parents don't have any rights when it comes to
medical
procedures and advice.
It doesn't make any sense.
It undermines the only two things that they really had that were
motivating their base.
But it is worth noting
that he
issued this and had it come out
right around the same time that Ercott story broke.
That's a...
I mean, I guess that's a big coincidence, I'm sure.
Not like he was trying to divert attention from it.
But it does appear that at least for the moment it failed.
The thing is,
the fight's not over.
People are still going to need help and people in Texas are still going to be
worried.
The good news is
you have some time
to get out if that's the option that you're
going to look at.
Now,
with everything else that's happening in the country
concerning
this particular demographic, it's worth noting
that a lot of big businesses
are
starting to push back,
even if ever so slightly,
on this type of legislation.
Because they know it's unpopular and they know if they give money to these
candidates it's going to be found out and it's going to impact their bottom line.
It's not like the company suddenly grew a conscience.
So
this may have been a really bad move for Abbott politically,
set aside the fact it's obviously a bad move morally.
But at least we can
go into the weekend with a little bit of good news.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}