---
title: Let's talk about why the US is pursuing economic measures....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=al0_M0HOrOY) |
| Published | 2022/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Foreign policy rule discussed repeatedly on the channel.
- Question about the U.S. and the West behaving differently this time.
- Normally flood favored side with arms, but this time rushing with sanctions on Russia and delaying arms to Ukraine.
- Debate over responsibility to provide Ukraine with necessary defense.
- Cold feet on transferring MiGs from Poland to Ukraine.
- Foreign policy viewed through lens of power, not morality.
- West's actions aimed at economically devastating Russia for the long term.
- Severe sanctions to take Russia out of the game early on.
- Logistics and concerns about tipping the scales reasons for delaying weapons to Ukraine.
- Concerns about potential escalation if certain weapons were provided to Ukraine.
- NATO's strategic move to destabilize Russia's economy and reduce its influence.

### Quotes

- "Foreign policy viewed through lens of power, not morality."
- "It is that international poker game and everybody's cheating."

### Oneliner

Beau explains why the U.S. and the West are prioritizing sanctions over arms supply to Ukraine, revealing the power play behind foreign policy decisions.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Build awareness on the importance of understanding foreign policy decisions (exemplified)
- Advocate for ethical foreign policy decisions in your community (exemplified)

### Whats missing in summary

Insights on the potential consequences of viewing foreign policy solely through a moral lens.

### Tags

#ForeignPolicy #Ukraine #Sanctions #PowerDynamics #NATO


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a foreign policy rule that I have talked about over
and over again on this channel.
We're talking about a question I got and why the United States and the West is behaving
differently this time.
They're not following a normal pattern.
So here's the question.
Beau I don't understand something.
Normally in a situation like this we flood the side we like with arms so they can fight
off the side we don't like.
We normally only use sanctions against countries that don't have a group we can arm.
This statement is more accurate than I think a lot of people would want to admit.
It's not always true, but that's a fair statement.
This time we seem to be rushing ahead with sanctions on Russia and slow walking the arms
to Ukraine.
This seems wrong to me.
Don't we have a responsibility to give Ukraine what it needs to defend itself?
If we aren't going to do that, why sanction Russia at all?
Yeah I assume you're talking about the planes and if you don't know that three-way deal
where the MiGs for the F-16s and the MiGs were going to go from Poland to Ukraine, the
US got cold feet on that.
Decided not to do it.
You don't say, but I imagine that's what you're talking about.
And yeah, I was furious when I found out.
There are a couple of reasons they may have done that and we'll get to that in a minute,
but before we do, I want to get to something in this message.
This seems wrong to me.
Don't we have a responsibility?
Stop right there.
What lens are you looking at foreign policy through?
Right and wrong, good and evil, moral and immoral.
It's not what foreign policy is about.
It never is.
It's about power.
If you try to look at it through any other lens, things aren't going to make sense.
That's the way it is.
It's not the way it should be, but it's the way it is.
If you look at US and Western actions through the lens of power, well everything makes a
whole lot more sense.
On this channel, we've talked about it for years.
There's a near-peer contest, China, United States, and Russia, and it's finally getting
going.
There's finally moves being made and right out the gate, Russia turned the entire world
against it.
Just as soon as it's really starting, Russia made a move that turned the entire world against
it.
Even China is like, well, okay, we're not going to fix your planes.
We're not going all in, but they're throwing a token response behind, okay, yeah, this
was bad right from the beginning.
If you were looking at it through terms of power and national interest for the countries
that are making the decisions, the West, NATO, United States, what is actually more important?
Combat on the ground in Ukraine or economically devastating a near-peer competitor for decades
to come?
That's why the sanctions went in the way they did.
That's why they're going all in on them, and they're incredibly severe.
This is taking one of those near-peers out of the game as soon as it starts.
That's why.
It's not right.
It's not what we should do as far as ethical responsibilities or moral responsibilities,
but this is foreign policy.
None of that stuff has anything to do with it.
Now as far as slow walking the weapons, some of that is just logistics.
Some of that is... it is hard to move stuff into a war zone.
That's difficult.
When you're talking about the planes, there's two possible reasons.
There's two theories that people have.
One is incredibly cynical, and they believe that the US and NATO, it's like, well, if
we give them the MIGs, that might turn the tide a little bit too much, and Russia hasn't
committed completely yet, and we want to make sure that they get sucked into a years-long
war.
Incredibly cynical?
I actually don't think it's that.
Not because the US wouldn't do something like that, but because we're providing way too
much assistance on the ground for that to be the case.
All of the surface to air stuff and the ground to ground stuff, the anti-tank stuff, if they
were trying to suck Russia in, they wouldn't be providing that in the quantities that they
are.
The other theory is that the US, the West, NATO, however you want to frame it, they're
concerned that providing the planes would tip the scales too much to the point where
Putin might use a weapon he shouldn't, and they're worried about that escalation.
I think that's probably the most likely reason.
I don't agree with it.
I don't think that the MIGs would have been the catalyst for that.
At the same time, I'm working off of basically open source stuff.
I don't have access to the same types of intelligence that the people making these decisions do.
I don't know that for sure.
It's not cool to gamble when you're talking about that as the outcome.
If you're wrong and he does use a weapon like that, that's not good.
So they may have assessed the risk and decided it was too much.
I don't like it.
I was, I'm going to be honest, absolutely furious when I found out.
But at the end of this, what you're left with is you can't look at foreign policy decisions
through the lens of morality because they're normally not made through that lens.
This was a golden opportunity for NATO to pile sanctions on Russia, make it the most
sanctioned country on the planet, destabilize their economy, and therefore make sure they're
even less of a peer.
Push them down to major power status rather than, you know, jockeying to become a superpower.
That's probably your answer.
Anytime you have a question about foreign policy, just kind of back out for a second
and ask yourself what lens you're looking at it through.
And if you're using words like wrong or responsibility, stuff like that, you're looking at it through
a moral lens.
And sadly, that's not how these decisions normally get made.
It is that international poker game and everybody's cheating.
And it's, as is normally the way, it's the average person that ends up paying the price.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}