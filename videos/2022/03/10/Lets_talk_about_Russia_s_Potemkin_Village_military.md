---
title: Let's talk about Russia's Potemkin Village military....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3DF3iRrzK_A) |
| Published | 2022/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of a Potemkin village with the story of Potemkin and Catherine the Great, where fake villages were built to deceive her.
- Disagrees with the comparison of the Russian army to a Potemkin village, despite their poor performance in Ukraine.
- Points out that while Russia is performing poorly, their capabilities may be underestimated due to overestimation of Russia and underestimation of Ukraine.
- Mentions basic errors made by the Russian military, like air drops in contested airspace and lack of guided munitions.
- Suggests that Russia might be holding back advanced equipment to prevent a NATO counter-attack.
- Blames Putin's leadership for the failures of the Russian military, citing his paranoia and habit of surrounding himself with yes-men.
- Attributes part of the Russian military's failures to corruption within the ranks, with examples of embezzlement and negligence.
- Warns against underestimating the Russian military and advocates for overestimating opposition for safety.
- Concludes by cautioning against dismissing the Russian military as worthless and suggesting it could be a grave error.

### Quotes

- "It's far safer to overestimate your opposition. Estimates shouldn't be reduced that much."
- "I wouldn't classify the Russian military as worthless just yet."

### Oneliner

Beau dives into the concept of a Potemkin village, disputes the comparison to the Russian army, blames Putin's leadership and corruption for their failures, and warns against underestimating their capabilities.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Analyze and address corruption within military ranks (implied)
- Avoid underestimating opposition forces (implied)

### Whats missing in summary

In-depth analysis of the potential consequences of underestimating the Russian military.

### Tags

#RussianArmy #PotemkinVillage #Putin #MilitaryCorruption #Underestimation


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about whether or not the Russian army is a Potemkin village. It's an interesting
concept and I love that it's being described that way, but I don't think
it's accurate and I think it may lead to something bad, so we're going to talk
about it. If you're not familiar with the idea of a Potemkin village, it comes from
a story. There was this guy named Potemkin and he was, well let's just say
friends with Catherine the Great, and he was tasked with improving this region
that she had just acquired. Well she comes to visit, see how things are going,
and he puts her on a barge and they float down the river and she sees all
these new settlements along the river bank. And there's fires going and people
moving around and it makes her feel better. But the reality is
none of those villages were real. It was just a facade. His men built them, built
these fake villages, and as soon as the barge passed, well they would pick them
up and move them down the river and rebuild them again before she got there.
So it looked a lot more impressive than it really was. That's the story, that's
where the term comes from. I would point out that historically speaking, this
almost certainly didn't happen. It is unlikely that Potemkin tried to deceive
her in this manner. But that term, it stuck and it's around. And that's what
people are now saying about the Russian military. That it's all for show. That
it's not really capable of anything because of their performance in Ukraine.
I don't agree with that because there's a number of things feeding into it.
One is their poor performance. But their poor performance is exaggerated.
Because while Russia was overestimated, Ukraine was underestimated. I'm on video
saying that I don't think it's a foregone conclusion that they just roll
over. I openly had doubts about that estimate. I didn't expect them to do this.
I didn't expect them to perform as well as they did. So those people who just
assumed they would roll over, well they had a lower opinion. So their
expectations of what Russia was going to be able to accomplish were higher. So
it's leading to the idea that Russia is performing more poorly than it really is.
Now don't get me wrong, Russia is performing poorly. And then that has led
to people pointing out a lot of basic errors. You know, air drops in contested
airspace, not having infantry with your tanks. Stuff that a modern military
should know not to do. But one of the big things that people are pointing to is
the lack of guided munitions. Russia is not using smart bombs. They're using
old-school stuff. And that's putting their planes at risk because they have
to come in lower, low enough to be in range of a US-made Stinger. And that has
happened a lot. And because of that, people are assuming that Russia doesn't
have those munitions. I don't know that that's a good assumption. It seems more
likely to me that they have the equipment we know that they were capable
of producing, but they're not using it right now. Not out of some, you know, 4D
chess method, but because they're worried about a NATO counter-attack. And they
want to save that stuff for NATO if they have to fight it. So I don't know that
that's a good indication of the state of the Russian military. But then you still
have to explain why they are doing as badly as they're doing. Because even
though the appearance is exaggerated because of the different performances,
they're still doing really, really badly. So why? I have two possible answers.
This is gut, to be clear. I think it's Putin's fault, primarily. I think it's
Putin's fault. He failed them. He let them down. I think his paranoia, his need for
secrecy, and his habit of promoting people with the right connections and
people who would echo back to him what he wanted to hear, created a situation
where you had a whole bunch of people in the room agreeing with him. And those
people who knew better, well, they weren't trusted enough because he was too
paranoid. So they weren't in the meeting when the plans were devised. Those people
who could have said, hey, that's a really bad idea. You know, people stopped doing
that during World War II because it always goes badly. They weren't in the room.
So that explains why at the very beginning it went so poorly and then it
just snowballed. Each mistake compounded the next. And you have that. That to me is
probably the primary reason we're seeing what we're seeing. Another thing is just
good old-fashioned Russian corruption. Every military has corruption. Every
military on the planet has corruption of some kind. However, in most militaries it
would be surprising if a colonel took all of the night vision and gave it to
somebody to sell on eBay or took a couple semi trucks full of rations and
left the old ones. But since the generals do it, they take a little bite of the
Russian defense budget to pad their pocket. Well, the colonels do too. And because the
colonels do it, well, the majors, they also kind of slack off a little bit. Maybe they
sign off saying preventative maintenance was done when it wasn't. Maybe they sign
off and say, hey, yeah, those wheeled vehicles, they were moved around so the
tires don't go bad. It seems to me like those are the two main factors here. And
the only reason I say this is because overestimating the opposition, that's not
a danger. If you overestimate the opposition, ok, so it was easier than you
thought. But if you underestimate them, you look like the Russians in Ukraine
right now. I do not think it's a good idea to go from saying they're a near
peer all the way down to they don't matter. They're just for show. That's very
dangerous thinking. It is far safer to overestimate your opposition. Estimates
shouldn't be reduced that much. Because it's not like the mysteries in
Scooby-Doo. You know, at the end of those shows, they come out and they pull the
mask off the monster, and it's always a doddering old man like Putin. In real
life, you can pull the mask off the monster and there'd still be a monster
underneath. Just has a different face. I wouldn't classify the Russian military
as worthless just yet. I think that that would be a grave error. Anyway, it's just
a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}