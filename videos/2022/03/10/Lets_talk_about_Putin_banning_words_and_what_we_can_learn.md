---
title: Let's talk about Putin banning words and what we can learn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fPZQmP8ZYW4) |
| Published | 2022/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Different countries have their own national lens that influences how they view the world and themselves.
- The lens is shaped by media discourse and narratives pushed by politicians, impacting societal views.
- Examples like Russians not calling the war in Ukraine a war and states in the US avoiding the term climate change show how narratives can shape policy.
- Authoritarian regimes use tactics like controlling narratives to maintain the status quo and prevent demands for change.
- In the US, there are instances where topics are avoided in fear of upsetting the status quo and sparking demands for change.
- Media and politicians play a significant role in shaping public perception and what topics are deemed acceptable for discourse.
- The playbook of authoritarian leaders involves restricting certain topics to prevent challenges to their power.
- The goal is to ensure that those without much power do not demand change by marginalizing certain topics and people.
- By examining how other countries handle certain issues, it provides a reflection for individuals to analyze their own national lens.
- The avoidance of discussing certain topics is a common tactic used to maintain the status quo and prevent societal change.

### Quotes

- "Don't say climate change. Don't say war. Don't say history. Don't say gay."
- "The only thing that really changes is what they don't want you to talk about."
- "They care about maintaining the status quo and making sure that those without a lot of institutional power, well, they don't ask for change."

### Oneliner

Different national lenses shape views globally, impacting policies and narratives to maintain the status quo and deter change.

### Audience

Global citizens

### On-the-ground actions from transcript

- Challenge narratives pushed by media and politicians (implied)
- Encourage open discourse on topics that are marginalized (implied)

### Whats missing in summary

The full transcript provides a deeper dive into how narratives influence policy and societal views, urging individuals to critically analyze their national lens. 

### Tags

#NationalLens #Narratives #PolicyInfluence #Authoritarianism #SocietalChange


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk about
mirrors and lenses and how we see ourselves and how we see other countries. You know,
we talked recently about the fact that every country has its own national lens and it's
through that lens that they view the rest of the world and they view themselves. And
that lens shapes what they deem is acceptable. And that's why there are such divergent
views and why sometimes you can look at another country and just complete disbelief that something
is normalized there. That lens, it's created by discussions that occur in the media in
that country, by narratives that are pushed by politicians, and it shapes the very fabric
of that society and how it views other societies. In the US, when we found out that Russians
couldn't describe the war in Ukraine as a war, blank faces all around. Everybody was
just utterly surprised, shocked, that that was something that would even be remotely
acceptable. What a bizarre authoritarian measure, you know. Only totalitarian goons would do
something like that. Would never happen here. You know there are certain states in the US
where the Department of Environmental Protection was ordered by the state to not use the term
climate change. It's the exact same principle. A narrative, something that went out, deny
it, deny it, deny it, for so long that it became state policy. We don't want people
thinking about this, so we just don't talk about it. We use another term, we skirt around
the issue, because to address it might upset the status quo. And when it comes to authoritarian
goons, that's what they really care about, because they're in power and they want to
stay there. And as long as the status quo remains the same, they do. So they use measures
like this all over the world to further that complacency. You don't want the people at
the bottom worried about something that, you know, might not go well. They might demand
a change. And it's not a one-off thing. When it comes to things that upset the status quo
in the United States, the idea of not talking about it, of the state saying, you can't discuss
this, it's more common than you might imagine. You have climate change as an example. All
over the country right now, there are meetings with school boards, with people saying, hey,
we shouldn't talk about these things, even though they objectively happened, they occurred,
it's history. We shouldn't talk about it, because, well, people might ask for change.
People might demand the status quo shift. So we just shouldn't discuss it. And that
viewpoint was shaped by people in the media and politicians that pushed that narrative,
that created that lens, that got people fired up about the outrage of the day, right? Don't
say climate change. Don't say war. Don't say history. Don't say gay. Yet another. It's
more common than you might imagine. Just make sure the people don't talk about it. They
won't ask for change. It's the same playbook used by authoritarian goons all over the world.
The only thing that really changes is what they don't want you to talk about, because
the status quo in each country is different. And that's what matters. It's not that they
actually care about these topics. They care about maintaining the status quo and making
sure that those without a lot of institutional power, well, they don't ask for change.
And the easiest way to do that is to marginalize them, other them, or make that topic something
that's just not discussed. When stuff happens in other countries, it holds up a mirror to
our own national lens. And if you're paying attention, you'll probably see some blemishes.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}