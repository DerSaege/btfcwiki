---
title: Let's talk about reinforcements headed to Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4-p2N3EVvlU) |
| Published | 2022/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia is sending Wagner, known for lacking compassion and professionalism, as reinforcements to Ukraine.
- Syrian fighters with experience in urban combat are also being called in.
- Approximately 5,000 troops are expected, which may not be enough to alter the current situation in Ukraine.
- Wagner troops are likely to use force to subdue resistance during the occupation phase.
- The security clampdown by Wagner may lead to increased resistance among the local populace.
- Ukrainian reinforcements include international volunteers, estimated between 16,000 to 25,000 from various countries.
- These volunteers are mostly combat veterans motivated by their own ideological reasons.
- The presence of international volunteers is likely to boost morale among Ukrainian forces.
- Volunteers from different countries will bring varied tactics and operational approaches, posing a challenge for the opposition.
- The involvement of international volunteers provides plausible deniability for special operations.

### Quotes

- "Russia now has to review like three dozen sets of tapes because their tactics are going to be different."
- "This is going to boost morale, and it's going to give them needed forces."
- "The world is full of amateurs. That's what makes the amateur so dangerous."
- "These are people motivated by their own ideological reasons."
- "From the Ukrainian side, what they have coming to help is going to help on a lot of different fronts."

### Oneliner

Russia sends Wagner and Syrian fighters as reinforcements to Ukraine; Ukrainian forces bolstered by international volunteers, posing tactical challenges for opposition.

### Audience

Military analysts, peace advocates

### On-the-ground actions from transcript

- Join international relief efforts (exemplified)
- Support local humanitarian aid for conflict zones (suggested)

### Whats missing in summary

Insights on the potential long-term impacts and implications of international reinforcements in the Ukraine conflict.

### Tags

#Ukraine #Russia #Reinforcements #InternationalVolunteers #Conflict


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the reinforcements
because Ukraine and Russia have fresh people coming in
and they're unique, not traditional reinforcements.
Russia has indicated that they will be sending in Wagner.
Now, if you aren't familiar with them,
they are a Russian contracting firm
and they have a reputation as not necessarily
super professional, but they lack compassion.
And then you have the call that went out
and they're pulling in Syrian fighters
because they have experience in urban combat.
Now, as near as we can tell based on the numbers,
we're talking about 5,000 troops.
Not really enough to turn the tide.
It's not.
I don't think it would turn the tide
in the current phase of fighting.
It is certainly not enough to matter
when it comes to the occupation side.
Now, one of the things to note is that it appears
they're going to be using the Wagner troops together,
which to me isn't the wisest move,
but it's par for the course so far.
So when it comes to the occupation phase,
Wagner will probably try to subdue any resistance with force.
When we were talking about the various demonstrations
and all of that stuff,
kept talking about the manual.
That manual, those steps, that was designed,
developed for this, for dealing with a military occupation
and that kind of resistance.
Just so happens it's transferable.
The security clampdown that Wagner is likely to provide
is probably going to make the resistance grow.
Generally speaking, that tends to upset the local populace
more than keep them in line.
So from the Russian side of things,
the reinforcements, they're getting some cracked troops.
They really are.
Wagner's forces, they're trained.
They're combat vets.
They're hardened.
They've been there.
The Syrians also, they have experience,
but it doesn't look like there's enough of them
to truly turn the tide here.
What about Ukraine?
The reinforcements they're getting.
The international volunteers are beginning to show up.
A lot of them.
The estimations that I have seen have ranged from 16,000 to 25,000
are on their way.
They come from a whole bunch of different countries
and we're going to run through them.
Understand that some of this reporting
is more substantiated than others.
Some of it is more reports of people on social media
from these places, and yeah, I'm on my way.
And some of it is newspaper articles,
doing interviews with people as they get on the flight.
But there are volunteers apparently
coming from the United States, the United Kingdom,
all over NATO member nations, non-aligned European nations,
Australia, Syria, Afghanistan, Japan, all over Africa.
The reporting there is kind of sparse,
but it's worth noting there is more than one country
that has issued a statement saying, hey, don't go.
Not a statement you would have to make
if you didn't have people going.
So again, these are going to be vets.
They're specifically trying to find combat vets.
My understanding is they're also just taking people who served,
but may not necessarily have seen combat.
But that's gray.
I'm not real sure what the requirements are right now.
But what you have here are a whole lot of people
who are crack troops.
They're hardened.
And they're volunteering to go.
There's no flag being waved that matters to them,
that they have an emotional attachment to.
There's no state mandating it.
There's no orders.
These are people motivated by their own ideological reasons.
They see it as a just fight, so they're showing up.
That could probably be a whole discussion on whether or not
countries should really try to manufacture consent,
because it appears if people view it as a just cause,
they'll show up without orders.
So Ukraine is getting, let's just say, 16,000.
Take the low end.
16,000 vets, mostly combat vets.
That's going to matter.
Professional soldiers, right?
Professional soldiers are predictable,
but the world is full of amateurs.
The reason that saying exists is because
of the unpredictability.
That's what makes the amateur so dangerous,
because you don't know what they're going to do.
The US, UK, and Australia, pretty much the same.
Most NATO nations, pretty much the same.
Non-NATO European nations, well, their operational level
and their tactical level, what they do,
it's a little bit different.
Syria, Afghanistan, different.
The way they're going to behave, the way
they're going to operate.
Africa, different.
They fight wars differently.
If you're the opposition, now you
have to counter all of these different doctrines,
because it's going to carry over,
because they engage in warfare differently.
I'm sure most people have seen a movie at some point
about a boxer, and there's always
that scene where the boxer is watching the person they're
going to fight at the climax of the film,
watching their previous fights and trying
to learn their moves and all of that stuff.
Yeah, Russia now has to review like three dozen sets of tapes,
because their tactics are going to be different.
They're going to be unpredictable.
This is especially true if Ukraine deploys them correctly.
Meaning by country of origin, which they're probably
going to have to just for language reasons.
This is going to matter to Ukraine,
that this will bolster their forces.
Not just the troops, but imagine you're there
and you're fighting for your home,
and you have people show up to help.
It's going to boost morale.
And you are not going to let those people who
left their homes to come help you defend yours.
You're not going to want to let them down.
It's going to be a big win for them.
This is going to boost morale, and it's
going to give them needed forces.
It's going to vary their tactics.
And then there's the other aspect.
Once these volunteers are in, special operations
from all over the world, they can go do their thing,
because they're not affiliated with any country.
They're volunteers.
Provides them that plausible deniability.
This is going to matter.
So this is one of the shifting things
when it comes to this conflict.
From Russia's standpoint, yeah, they're
getting some crack troops.
This may help them take some of the cities
that they're stalled at.
But it's not going to be a turning point.
It's not enough to truly turn the tide.
It's just enough to give them a little push.
From the Ukrainian side, what they have coming to help
is going to help on a lot of different fronts.
But this is also a very clear indication
that this is going to be protracted.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}