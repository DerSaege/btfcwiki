---
title: Let's talk about Ukraine, numbers, and a guess....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KecumfRuNi4) |
| Published | 2022/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russian military operation in Ukraine is failing due to lack of supplies and corruption within the military.
- Conservative estimates place Russian losses at six to seven thousand in 20 days, with wounded estimates being three times that.
- Russia's poor performance in Ukraine is attributed to a lack of basic tactics, supplies, and strategic planning.
- The Russian command underestimated Ukraine's resistance, leading to strategic failures in the military operation.
- The military operation in Ukraine is seen as more of a PR stunt by Russia rather than a well-planned mission.
- Speculation suggests that Russia believed Ukraine would surrender easily, leading to reckless military actions.
- China may provide some assistance to Russia to keep them in the fight, not out of genuine friendship but to weaken Russia economically.
- China and Russia are not as close allies as portrayed, with China likely assisting Russia strategically rather than militarily.

### Quotes

- "They truly believed that Ukraine was just gonna drop to its knees and be like, oh yes, please let me be part of greater Russia."
- "Will China help Russia since they have been asked? I think they will a little bit, but not enough to really help."
- "As much as they stand out in public and say, we're the best of friends with no limits, that's not really true."

### Oneliner

Russian military failure in Ukraine attributed to corruption, lack of supplies, and strategic misjudgments, viewed as a PR stunt more than a serious operation. China's potential assistance to Russia seen as strategic rather than genuine friendship.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Assist Ukraine with humanitarian aid and support (implied)
- Monitor international relations and developments between Russia and China (implied)

### Whats missing in summary

Insights on the potential future implications of China's strategic assistance to Russia.

### Tags

#Russia #Ukraine #China #MilitaryStrategy #Corruption


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
some numbers and provide some comparisons and we're going to talk about the things that caused
those numbers. So it's not a secret that the special military operation in Ukraine is not
going well for the Russians. They're using gas masks from the 70s and 80s. They lack precision
guided munitions. It's just not going well all around. They're begging for food. They're begging
for MREs. And all of this is mostly due to corruption. This is due to people within the
military skimming a little bit out of the defense budget to pad their own pocket. Now that they need
the stuff they don't have it. But there are some other things that caused this as well.
But before we get into it, let's talk about the numbers real quick. So conservative estimates
right now are placing Russian lost at six to seven thousand in that range. Conservative
estimates. Informed estimates on the low end are six to seven thousand. Some range up to
I want to say eleven. I want to say Ukraine is claiming like thirteen thousand at the moment,
but most people agree that's high. Six to seven thousand in 20 days roughly. For comparison,
the United States lost about the same number in Iraq and Afghanistan in 20 years. It's going
really badly. Now the standard formula for this is whatever that number is, you take it and you
multiply it by three and that gives you your wounded. Which basically means one out of ten Russian troops
that was there in the original formation to invade, they're out of the fight. Which is why Russia is
now begging for fighters from other countries, reaching out to the Chinese, asking for MREs
and food and stuff like that. I think a lot of this is due to Russian command believing their
own propaganda to the degree that they didn't take this seriously. They truly believed that
Ukraine was just gonna drop to its knees and be like, oh yes, please let me be part of greater
Russia. And they believed that so much that they didn't actually plan a military operation.
A lot of the losses, they can be attributed to lack of certain supplies, some of them. But at
the same time you have defense analysts all over the world scratching their heads trying to figure
out why Russia is performing so poorly. Why they aren't using basic tactics. Why they're running
tanks without infantry. Why their paratroopers don't have air support. Why they were even thinking
about an amphibious assault that didn't need to happen. Because from a military standpoint,
at the strategic, operational, and tactical level, this has been an utter failure.
And it shouldn't have been. Russia's not this bad. And it doesn't make any sense
until you stop thinking about it like a military operation. And you start thinking about it like
a PR stunt. Because they truly believed Ukraine was just gonna give up. So they let everybody
come play. Needless amphibious operations, the airdrops, everything starts to make sense
if you look at it through that lens. As if it was just a giant show for NATO.
So they could say, we took this entire country in three days and didn't lose anybody.
A PR stunt. That's the best explanation I can come up with. And that explains why
they did that airdrop. Because they didn't expect anybody to be on the ground shooting at them.
It explains a whole lot. But to be clear, that's speculation. I can't prove that.
We know it went bad. We know they're using bad tactical maneuvering. We know that at the
operational level, it's not going well. Their logistics are shot. Their intelligence is bad.
The strategy wasn't good. We know that they lack certain supplies. Why? You can attribute it to a
bunch of things. But I think it's a combination of those things. I don't think it's any one thing in
particular. Now, on to the question I'm sure everybody's wondering about.
Will China help Russia since they have been asked? I think they will a little bit,
but not enough to really help. My guess is that China is going to stick to its
standard line when it comes to Europe, which is basically, that's not my problem.
But I think they'll provide a little bit of assistance to Russia, not really to help it,
but to keep it in the fight. In the West, we have this view of Russia and China as being
like super great allies. They're not. Not really. They say that. They say that they are. But
when this is all said and done, when you really remember you're at that international poker game
and everybody's cheating, the Chinese will probably help Russia stay in the fight, so it drains Russia.
Russia is China's competitor as well. And they are a little bit better at being cynical.
And assisting when they're not really assisting. That's what I would expect. I wouldn't expect
them to send troops or any advisors or anything along those lines. But they may provide them some
stuff to keep them in the fight because it weakens them. And as much as they stand by their
own words, they may provide them some stuff to keep them in the fight because it weakens them.
And as much as they stand out in public and say, we're the best of friends with no limits,
that's not really true. That's not really true. That's a poker face for everybody else.
And they share more of a bond than a lot of other countries do. But China is trying to win the world
with its economy, not with force. And going too far in with a country that is as economically
as Russia has become doesn't really fit with their foreign policy.
While it may seem from the outside that this draws them closer together,
China is probably looking at it through the lens of this could be another country that is dependent
on us. So if they help, I think it'll be cynical in nature. I don't think it will be out of the
spirit of friendship. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}