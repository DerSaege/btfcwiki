---
title: Let's talk about what an oligarchy is....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5dTXMvxp45Y) |
| Published | 2022/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines oligarchs and oligarchies as power structures where a small number of people hold corrupt and selfish power.
- Differentiates financial oligarchs who use money to influence government decisions from other forms of oligarchs like military dictatorships.
- Points out the focus on Russian and Ukrainian oligarchs who gained power through privatization post-Soviet Union.
- Mentions a 2014 study that showed US billionaires influencing policy decisions to favor business interests over public opinion.
- Refers to the US as a civil oligarchy where financial power is used selfishly and corruptly within legal boundaries.
- Notes former President Jimmy Carter referred to the US as an oligarchy in 2015.
- Suggests a significant difference between US billionaires and Eastern European oligarchs is the latter's willingness to use violence outside of state power.
- Emphasizes the concentration of wealth in fewer hands leading to a more apparent oligarchical system.

### Quotes

- "As we look at a world where wealth is being concentrated in fewer and fewer and fewer hands, and those hands end up shaking a lot."
- "I don't know how much weight you want to put on that distinction."
- "So whether you want to adopt that terminology now or wait a little bit, hope that things get better, I guess that's up to you."

### Oneliner

Beau explains oligarchs and oligarchies, focusing on financial influence, corrupt power, and the concentration of wealth leading to a more apparent oligarchical system.

### Audience

Activists, Educators, Advocates

### On-the-ground actions from transcript

- Advocate for anti-corruption initiatives in your community (suggested)
- Support policies that prioritize public interest over business interests (implied)

### Whats missing in summary

The full transcript provides a detailed historical context and comparison between different types of oligarchs and their methods of wielding power.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about oligarchs
and oligarchies, what it is.
Because that term's being thrown around a lot right now,
and it has prompted a bunch of questions.
So we'll start at the beginning.
What's an oligarch?
Somebody who participates in an oligarchy.
What's an oligarchy?
That is a power structure where power is in the hands
of a small number of people, and they use that power corruptly
and selfishly.
But that's kind of broad, too, because that could describe
a military dictatorship.
When people use the term oligarch today
in common conversation, they're talking
about financial oligarchs, because there
are a bunch of different kinds of oligarchy.
So they are talking about those who use financial power, power
coupons, currency, to influence the government to do its
bidding or operate outside of the government.
Right now, most of the conversation
is focused on Russian or Ukrainian oligarchs.
Both countries have them.
They both came to power pretty much the same way.
When the Soviet Union fell, things that were state-held
became privatized.
Those who had the power to get things privatized
in the way that would benefit them,
using that power selfishly and corruptly, they came out on top.
They became the oligarchs.
They became the new ruling class of those countries.
And absent anti-corruption initiatives,
they've pretty much remained in power.
So from there, there's been a lot of people asking,
well, when do we start referring to US billionaires as oligarchs?
When do we talk about it in this country?
And the answer to that is 2014.
There was a study that came out of Princeton and Northwestern
in 2014 that looked at policy decisions that
were made between 1981 and 2002, looked at 1,800 policy decisions,
paying particular attention to those where the public wanted one thing
and the business class, well, they wanted something else.
Almost without fail, the business class got what it wanted.
Now, in the instances where the public got what it wanted,
generally it was either the business class wanted it as well
or they just didn't oppose it.
But pretty much any time the business class wanted A
and the public wanted B, the government went with A.
So that led to the idea of a civil oligarchy,
which is where that financial power, those power coupons, they get used
and they get used selfishly and corruptly.
However, they stay within the confines of the law.
I don't know how much weight I put into that little distinction
because if you have enough power coupons, if you have enough money
and you can use that money to influence the law,
you can just change the law so you stay within it.
I don't know about that.
Aside from that study, which was successfully defended,
you have Jimmy Carter, former president,
referred to the United States as an oligarchy in 2015.
So that comparison has already been made.
That terminology is already in use.
Now I would suggest there is a pretty substantial difference
between the US billionaires, the US version of oligarchy,
and the Russian or Ukrainian ones.
And that would be the willingness to use violence outside of state power
to influence what they want, to obtain their goals.
I don't know how much weight you want to put on that distinction.
But to me, that's far more important than the willingness
to kind of stay within the rules generally,
is that Eastern European oligarchs tend to have their own enforcement class,
their own people at their disposal that can do things extrajudicially.
But all of it centers around the idea of using money, using finance, wealth,
to wield power informally and use that power corruptly and selfishly.
As we look at a world where wealth is being concentrated in fewer and fewer and fewer hands,
and those hands end up shaking a lot,
we will move towards a more apparent oligarchical system.
So whether you want to adopt that terminology now or wait a little bit,
hope that things get better, I guess that's up to you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}