---
title: Let's talk about the 2024 Republican primary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UlFiPcaNs8o) |
| Published | 2022/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the 2024 Republican ticket and the candidates interested in running for president.
- Two distinct groups emerging to challenge a potential Donald Trump nomination again.
- Group one includes Cheney, Hogan, and Kinzinger, focused on salvaging the Republican Party from further damage by Trump.
- Cheney will seek re-election, while Hogan and Kinzinger are considering a presidential run.
- The second group likely to make an impact in the primary run are Pence and DeSantis, targeting Trump as well.
- Speculation on potential independent runs by Cheney and Hogan to block Trump's nomination.
- Recent polling indicates 30% of Republicans do not want Trump on the ticket.
- Running as independents, even a small percentage of votes could sway the election away from Trump.
- The situation will become clearer after the midterms as candidates position themselves for the nomination.

### Quotes

- "Their goal isn't necessarily to win, I don't think. It's just to block Trump."
- "Recent polling suggests that 30% of Republicans do not want Trump on the ticket."
- "If they run as independents, just 3% of the vote, 2% of the vote, that is probably enough to swing the whole election."

### Oneliner

Beau analyzes potential Republican candidates for 2024, focusing on those aiming to block Trump's nomination and the impact of independent runs on the election.

### Audience

Political analysts

### On-the-ground actions from transcript

- Stay informed about potential Republican candidates for the 2024 presidential election (suggested).
- Participate in midterms to shape the future political landscape (implied).

### Whats missing in summary

Insights into the specific policies or stances of the mentioned candidates for the 2024 Republican ticket.

### Tags

#Republican #2024Election #Trump #IndependentRun #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to go ahead and talk a little bit about the 2024 Republican
ticket because it's already shaping up the, uh, the candidates who are interested
in the presidency are kind of making it clearer already, and you have two
distinct groups that will be, uh, trying to dislodge what is presumed to be
a Donald Trump attempt to get the nomination again.
The first group is Cheney, Hogan, and Kinzinger.
These are people who really, to be honest, I don't think they really care necessarily
about winning the presidency.
I think this is more about attempting to salvage the Republican Party and make sure that Trump
can't damage it further from their point of view.
All three have made it pretty clear that they're at least entertaining the idea of running.
Now Hogan and Kinzinger, they're not going to be seeking re-election.
Cheney will be.
Some people are reading that as the two that aren't are more serious about the presidential
run.
I would suggest that maintaining an election staff could be scaled up for a presidential
run.
I don't know if I agree with that analysis.
The other thing that's important to note here is that all three in this group, they're pretty
friendly.
They won't be slinging a lot of mud at each other.
So they may make it a little bit further than people might anticipate.
Now it's also important to note, again, their goal isn't necessarily to win, I don't think.
It's just to block Trump.
Now people who are outside of this group who are likely to get somewhere in a primary run,
The two obvious ones are Pence and DeSantis.
There's a lot of people throwing Christie's name in there, I'm going to be honest, I don't
see that one.
Maybe he attempts the run?
I don't see it going anywhere though.
Now Pence and DeSantis will also be taking shots at Trump.
They'll be trying to knock him out of the way because they're going to try to appeal
to that same base, so they will definitely be swinging mud at each other, which might
leave the first group of candidates kind of open because they're not going to be perceived
as a threat, even though in one way they may be the biggest threat of all to anybody from
the Republican Party taking the White House in 2024, and that's that it doesn't appear
that they have ruled out a run as an independent.
Recent polling suggests that 30% of Republicans do not want Trump on the ticket, period.
So if two members from that first group, say Cheney and Hogan, decided to run as independents,
they wouldn't have to get much to achieve their goal because, again, it doesn't appear
that their goal is getting in the White House.
It appears the goal is trying to salvage the Republican Party and keep it from being under
Trump for another four years.
If they run as independents, just 3% of the vote, 2% of the vote, that is probably enough
to swing the whole election.
Now, we'll start to see this really shape up right after the midterms.
They're jockeying for position and kind of feeling people out at the moment, but it will
become much more public right after the midterm elections.
So there's our first little sneak peek at who's likely to be trying to get the presidential
nomination for the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}