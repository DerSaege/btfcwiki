---
title: Let's talk about Zelenskyy taking over the media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BZL8NVUAmUY) |
| Published | 2022/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- President Zelensky taking control of TV stations in Ukraine sparked concern among Americans.
- Americans were alarmed by the idea of Zelensky becoming tyrannical for controlling media.
- Beau draws a parallel to the long-standing emergency broadcast system in the United States.
- The emergency alert system has been in place for over 70 years to counter propaganda and communicate during wartime.
- Beau points out that the system can take over radio and TV stations, as well as send messages to phones.
- He mentions that during wartime, such actions are common to manage communication and propaganda.
- Beau criticizes American media for sensationalizing a non-issue about Zelensky's actions.
- He notes that most countries have similar systems in place for emergencies and wartime.
- Beau expresses surprise that the US didn't implement such control on day one of a conflict.
- He concludes by underscoring that the outrage over Zelensky's actions is misplaced, as it mirrors what the US system does.

### Quotes

- "This is an example of the American media taking a story that isn't a story and trying to reframe it so Americans get outraged by it."
- "If they're on network TV, they have to know that this system exists."

### Oneliner

President Zelensky's control of TV stations in Ukraine sparks concern among Americans, but Beau points out the longstanding emergency broadcast system in the US mirrors this action, criticizing the sensationalism by American media.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Educate others about the purpose and history of emergency broadcast systems (implied).
- Share information on social media to debunk sensationalized stories (implied).

### Whats missing in summary

Beau's detailed explanation and historical context on emergency broadcast systems and media sensationalism.

### Tags

#Media #EmergencyBroadcastSystem #Propaganda #Sensationalism #Communication


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the media in Ukraine
because the government of President Zelensky
has taken control of the TV stations there.
And I guess Tucker Carlson ran a little spot on it last night.
I don't know.
I didn't watch.
But from the giant influx of messages
I got about it, it was cast as something that should really concern Americans because it
demonstrates that Zelensky may be becoming tyrannical, totalitarian, because he's taking
over the TV stations.
And I've had enough people send messages about this that I honestly wish I had some way to
send out a message to everybody in the United States.
Do y'all hear that?
tongue? Do y'all hear it? This has been a test of the emergency broadcast system.
This is only a test. Had it been an actual emergency or invasion, we would
have seized control of every TV and radio station in the country. Now I'm
not particularly concerned that Ukraine has adopted a policy that the United
States has had for more than 70 years. That's not not high on my list of things
to worry about. From 1951 to 1963 it was called control of electromagnetic
radiation and it was administered by civil defense. From 1963 to 1997 it was
the emergency broadcast system. From 1997 to today it's the emergency alert
system and not only can it take over radio stations and TV stations and all
that stuff. It can send messages straight to your phone. Now, I'm not particularly
concerned about it. During wartime, this is pretty common. You want to be able to
counter your opposition's propaganda, send instructions to civilians trying to
avoid fighting, send out messages to troops in the field. Most people who are
probably complaining about this watched Red Dawn, right? The chair is against the
wall. The chair is against the wall. Same thing. No, this isn't a sign. This isn't
something you should be really concerned about. It's been around a long time. Most
countries have something very similar to this. The only shocking part about
this to me is that this didn't happen day one because it should have. This is
another example of Americans being insulated from conflict. We're not aware
of stuff like this. I mean keep in mind when this system gets used most times in
the US it's about natural disaster but when it came about it was administered
by civil defense. It's a wartime system. That's what it's there for. They just use it for
other stuff. I'm not concerned about it. This is an example of the American media taking
a story that isn't a story and trying to reframe it so Americans get outraged by it. Understand
There isn't a single person on network TV who's going to cover this, who isn't aware
that the United States has the exact same system.
They have to be.
They've heard the test.
You've heard the test your entire life.
But if it gets framed as Zelensky sees the media, well, that's different than our own
system that does the exact same thing, because you're used to it here, because you don't
necessarily even know that that's its real purpose because most times when it gets used
it's for something regional.
There's a hurricane, there's a flood, wildfire, something like that.
But make no mistake about it, its original purpose had to do with Russian bombers.
People who are pushing this and trying to paint it as the outrage of the day and something
you should be concerned about, they have to know.
If they're on network TV, they have to know that this system exists.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}