---
title: Let's talk about Rule 303 Belorussian style....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=k6A0gnUc6yo) |
| Published | 2022/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Belarusian railway workers disrupt rail traffic between Belarus and Ukraine to impede Putin's invasion.
- Chief of Ukraine's railway service publicly thanks Belarusian railway workers for their swift action.
- Some tracks in Belarus are in poor condition, contributing to the disruption of rail traffic.
- Signaling equipment has been attacked, central switching stations disrupted, and tracks fallen into disrepair quickly.
- The disruption has completely severed all rail traffic between Belarus and Ukraine, giving Ukrainians breathing room.
- Partisan groups in Belarus have also contributed to disrupting rail traffic.
- The people of Belarus are actively resisting being a pawn for Putin, showing resistance beyond the government's control.
- Continued disruption of rail traffic could have a significant impact on impeding Russian supplies to Ukraine.
- Every day without Russian resupply is a win for Ukraine during this critical phase.
- The collective efforts of Belarusian railway workers and partisan groups are making a difference in the conflict.

### Quotes

- "Belarusian railway workers disrupt rail traffic between Belarus and Ukraine to impede Putin's invasion."
- "The people of Belarus are actively resisting being a pawn for Putin."
- "Every day without Russian resupply is a win for Ukraine."

### Oneliner

Belarusian railway workers and partisan groups disrupt rail traffic to impede Putin's invasion, showing active resistance beyond the government's control.

### Audience

Activists, Allies

### On-the-ground actions from transcript

- Support Belarusian railway workers in their efforts to disrupt rail traffic (exemplified)
- Stand in solidarity with the people of Belarus resisting external control (exemplified)

### Whats missing in summary

The full transcript provides more context on the role of partisan groups and the potential impact of continued disruption on impeding Russian supplies.

### Tags

#Belarus #RailwayWorkers #Ukraine #Resistance #Putin #PartisanGroups


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
Rule 303, Belarusian style. Because there have been some developments that are interesting
to say the least. It appears that Belarusian railway workers are not thrilled with the
idea of their country being used as a staging ground for Putin's invasion. Now I want to
say it was the day before yesterday. News broke and it was minor news that said there
wasn't going to be any rail service between Belarus and Ukraine. This was good news because
it slows down Russian supplies. When the news broke it sounded as though it would be temporary
and that the trains just weren't running on time. Well the chief of Ukraine's railway
service has publicly thanked the Belarusian railway workers for their swift action. And
while he said he wouldn't go into detail, he did confirm that some of the tracks are
in quote poor condition. It appears as though the railway workers there decided that they
had the means at hand and the responsibility to act. Signaling equipment has been attacked,
central switching stations have been disrupted, tracks have apparently fallen into disrepair
very quickly. And all of this has completely severed all rail traffic between Belarus and
Ukraine. This is going to give Ukrainians some breathing room. Even if it's just a few
days that this is down, it's going to help because right now they're in that phase where
they're just drawing it out. So every couple of days that they can get where Russia isn't
being resupplied, that's a big win. There are also partisan groups inside Belarus who
have contributed to this effort in their own way. So it appears as though Putin's hold
on this country only extends to the government. It doesn't appear like the people of Belarus
are very interested in being his pawn and they are actively resisting it. If this keeps
up, if they can shut down rail traffic for any extended period, this is going to matter
a lot. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}