---
title: Let's talk about a claim about Mariupol and consistency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gVBawnCyQkA) |
| Published | 2022/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a claim about Ukraine using people as shields in Mariupol.
- Uncertain about the claim's validity but stresses the need to not justify Russia's actions.
- Exploring the idea of civilians being held against their will in Mariupol and its implications.
- Condemning the use of civilians as shields and holding those accountable.
- Challenging individuals who support Russian targeting of civilians based on this claim.
- Emphasizing the importance of consistency in condemning all violations of laws of armed conflict.
- Urging against justifying atrocities in war under any circumstances.
- Stating that the presence of unwilling civilians in Mariupol worsens Russian actions but does not justify them.
- Questioning the moral stance of individuals who fail to condemn deliberate targeting of civilians.
- Reminding that justifying such actions leads to a slippery slope of supporting atrocities.

### Quotes

- "It's war, and horrible, horrible stuff happens in war."
- "If something is true, it's true across all systems."
- "The idea that there are civilians being held in Mariupol that don't want to be there makes the Russian attack, the bombardment, worse."
- "If you can't condemn Russia deliberately targeting civilians, what does that say?"
- "We don't know that this is happening. We don't know that it's happening on any widespread level."

### Oneliner

Beau addresses the claim of Ukraine using people as shields in Mariupol, stressing the importance of not justifying Russia's actions and condemning violations of laws of armed conflict.

### Audience

Advocates for peace

### On-the-ground actions from transcript

- Hold those who hold civilians against their will accountable (implied).
- Condemn deliberate targeting of civilians (implied).

### Whats missing in summary

Deeper insights on the nuances of justifying atrocities in war.

### Tags

#Mariupol #War #Accountability #CivilianProtection #HumanRights #PeaceAdvocacy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Mary Opal again.
And we're going to talk about a claim.
Something that people have said repeatedly
in an attempt to justify certain things.
And we're going to talk about that,
and then we're going to talk about the reality of it.
And we're going to just kind of accept it for a second.
Because that's kind of the hallmark of intelligence,
is being able to entertain an idea
without accepting it as fact.
Now, if you don't know what I'm talking about,
there are a lot of people who are claiming
that Ukraine is using people as shields in Mary Opal.
Now, I've briefly looked into it.
I can't confirm it.
But I also can't debunk it.
And even if I could debunk it,
I couldn't say it wasn't happening at all.
And even if I could confirm it,
I couldn't say how widespread it was happening.
That's something that we won't really find out until afterward.
But this is being used as justification
for Russia's deliberate targeting of civilians.
Because this is happening,
well, it makes what Russia's doing okay.
Let's entertain it for a second.
Let's just follow that.
So the idea is that there are civilians there
who aren't sympathetic to the cause of Ukraine,
and they're not even people who just couldn't get out in time.
They're people who actively wanted to leave,
and they were held against their will, right?
Okay, but you understand how that makes it worse, right?
You understand how that makes the Russian position worse.
Not better.
It doesn't justify it.
Not in any way, shape, or form.
It does the exact opposite.
It makes them more cowardly, more brutal, more unprofessional.
It makes it a crime.
The idea that this practice somehow justifies Russian actions,
that doesn't make you a free thinker.
It makes you a propagandized tool of a government, and that's it.
You're not anti-war.
You're not anti-imperialism.
You are justifying a violation of the laws of armed conflict
in furtherance of imperialism.
The reality is that if you're making this case,
if you are using some kind of mental gymnastics
to support the idea that Russia should deliberately target
civilians because they're being held there against their will,
you never get to talk about US drone policy again.
You never get to talk about civilians caught in the crossfire
when it comes to law enforcement,
because that's the argument you're making.
Well, they're just collateral.
It's acceptable.
That's not a good stance.
That's not justification for it.
It's the exact opposite.
We don't know that this is happening.
We don't know that it's happening on any widespread level.
We know the clips that we've seen.
We understand that it probably is on some level.
I'm sure at some point this has occurred.
What do you think my position is on those who did it?
Those who may have held civilians against their will,
what do you think I believe should happen to those people?
I think they should be held accountable.
In the most severe sense, especially given the region
and the crews that are there and the people that would likely
have done it, nobody's going to miss them.
I can condemn it.
If you can't condemn Russia deliberately targeting
civilians, what does that say?
Are you really anti-war?
Are you really concerned about the civilians and the loss?
Or are you quite literally cheering it on,
attempting to support it, to justify it?
If something is true, it's true across all systems.
What you believe to be right and good and true
is right and good and true, or it's not.
And if you're saying it's OK to do this in this situation,
understand what else you have to justify in order
to remain ideologically consistent.
It's war, and horrible, horrible stuff happens in war.
And sometimes you can even understand it.
You can say, yeah, I get why that happened.
That was a trolley car decision.
I get it.
But you can't actually justify it.
You can't condone it.
The idea that there are civilians being held
in Mariupol that don't want to be there
makes the Russian attack, the bombardment, worse.
It doesn't justify it.
It shows exactly how far they're willing to go in their attempt
to affect their empire.
And that's it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}