---
title: Let's talk about Russia, sanctions, and McDonald's....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zYCQpPlPzRk) |
| Published | 2022/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the impact of Western companies leaving Russia due to economic policies.
- Mentions how thousands of employees lost their jobs, affecting the GDP.
- Uses McDonald's as an example to illustrate the ripple effects of a company leaving.
- Describes the interconnected nature of various industries affected by a single company's departure.
- Points out that economic pressure is aimed at leadership, not the average person.
- Emphasizes that reducing economic activity limits tax revenue for Putin's war efforts.
- Suggests that economic pressure sets conditions that make it harder for Putin to maintain support for the war.
- Notes that while economic pressure alone may not shift leadership, it creates conditions for change.
- Speculates on the need for Western banks to assist in rebuilding Russia's economy post-sanctions.

### Quotes

- "Most economies are like a house of cards. Once large segments yank themselves out, everything else falls."
- "Reducing economic activity limits tax revenue which reduces the amount of money Putin can spend on the war."
- "It makes it harder for Putin to sell his war at home when people are getting economically devastated because of it."
- "Russia is now the most sanctioned country on the planet and they're still planning on piling on more sanctions."
- "All the king's horses and all the king's men are going to have to try to put that egg of an economy back together again."

### Oneliner

Western companies leaving Russia creates a domino effect impacting industries, GDP, and Putin's war efforts, setting conditions for economic change and potentially requiring Western banks for recovery.

### Audience

Economic observers

### On-the-ground actions from transcript

- Support local businesses affected by economic policies (implied)
- Advocate for policies that prioritize economic stability for all (implied)

### Whats missing in summary

The detailed examples and nuances of the interconnected economic impacts discussed by Beau.

### Tags

#EconomicImpact #WesternCompanies #Putin #Sanctions #RebuildingEconomy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Faberg?? Egg McMuffins.
We're going to talk about the egg of an economy
that Russia has and how it has been pushed off a wall.
And we're going to talk about the Western companies that
are leaving Russia and the impacts that that's going to have.
We're just going to focus on things
have already happened when it comes to these economic policies that have been enacted.
We're not going to talk about the ones that Biden is going to call for, I think the day
y'all watch this, because he will soon reportedly be calling for revoking Russia's favored nation
trading status, which will open the door to a whole new wave of economic policies like
this.
Now when this first came up, I made it clear I don't like these.
I don't like those that hit the average person, but I can't deny that they're effective.
And I had a whole bunch of people ask, you know, how does this work?
It doesn't seem like it would be effective.
How does it actually produce pressure on Moscow?
So when you're talking about all these Western companies that
left, most people's understanding of this
is those companies left, they close their locations,
their employees are without a job.
And yeah, I mean, that's part of it.
But that's just the beginning.
That is just the beginning.
The beginning is that when you're
talking about companies this size with this many locations,
tens of thousands, if not hundreds of thousands of people
lost their jobs overnight.
Think back to our public health issue.
You know how damaging that can be to the GDP.
That is just the start.
But to show that it's not just the employees losing their job
that causes the real hits, we're going
to talk about McDonald's.
Because for whatever reason, and something
that was incredibly surprising to me,
apparently McDonald's Russia, kind of a cool company.
At time of filming, they are making
it seem as though they are going to continue
to pay their employees even though they're not working, which is cool, but that doesn't
stop the effects of them leaving.
Most people, at some point in their life, you've been by a fast food joint somewhere
near the dumpster when it's hot, and there's that really weird smell that's not trash.
It's a different smell.
That's grease.
There's a person whose whole job is to come and get that grease and dispose of it.
Inside the McDonald's, above the grill, there's this hood that provides ventilation.
There are these silver slat things that the employees can clean, but up inside that hood,
that's normally left to a specialist, somebody from outside, who comes in to clean it.
Those people don't have jobs anymore.
And the way those professions typically work is they sign a contract to handle all the
McDonald's in a certain area.
So if they're all closed, they're just out of work.
Some of them may have other clients, but probably not.
Then think about, I mean, what does McDonald's sell?
Burgers, right?
How many millions of buns do you think all the McDonald's in Russia go through every
day. The bakeries that make them, they don't need to make them anymore. So they
either close or they lay people off. And since they laid off the bakers, they're
not making as many buns, bread products, whatever. They don't need as many truck
drivers to move the stuff around. Right? And since you, all three of those
examples, they all used vehicles, right? You had the person to come and collect
the grease, they've got a truck for that, the hood cleaning person, and the
delivery trucks for the bakery. None of those are rolling. People who work on
fleets, commercial vehicles, now they have less work, have to lay people off, and it
just keeps going. It's an endless chain. Once large segments of the economy just yank themselves
out, it's a house of cards. Most economies are. And when you remove significant sections,
everything else falls. And this is going to have a lot of damage. This is going to cause
lot of damage. And people have asked, you know, why admitting that it's effective,
why I don't like it, because I don't think the person working at McDonald's in
Chalabinsk has anything to do with what's going on in Ukraine. I don't think
it's that same thing. Don't kick down. The person with less power than you
probably isn't the source of your issue but they end up as well they were just
in the way because the goal is to reduce economic activity which reduces tax
revenue which reduces the amount of money Putin can spend on the war aside
from that it increases discontent which means maybe it's not such a good idea to
call people up and send them over because you may need those troops at home because
people might get mad, might cause unrest.
All of this is designed to put pressure on the leadership of these countries.
Now, generally, this alone isn't enough to cause a shift in leadership, most times.
But it helps set the conditions for it.
It makes it harder for Putin to sell his war at home when people are getting economically
devastated because of it.
So that's how this works.
And when you think about all of the other stuff that's at play, you can see how damaging
it's going to be.
This is just a few key companies pulling out.
When you're talking about the rest of the sanctions, it's a big deal.
I don't think Putin really anticipated this to the degree that it's happened.
Russia is now the most sanctioned country on the planet and they're still planning on
piling on more sanctions. They have surpassed Iran. So the economic state in Russia is going
to be, it's going to be in a bad way for a while. And when it's all said and done, all
the king's horses and all the king's men are going to have to try to put that egg of
an economy back together again.
And when they do, they will probably need Western banks to do it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}