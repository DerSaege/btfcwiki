---
title: Let's talk about Russian claims, chemicals, and masks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2PiJ4xsBQnQ) |
| Published | 2022/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about Russian equipment and masks, providing a PSA about surplus Russian masks.
- Mentions looking at photos and footage of Russian equipment, including gas masks from the mid-Soviet era.
- Shares the distribution of obsolete or less than modern equipment by Russians, like Mosins.
- Expresses skepticism towards Russian claims based on past instances of misinformation.
- Addresses whether the presence of masks indicates Russia's intention to use them.
- Warns about the dangers of using Russian surplus filters due to lead and asbestos content.
- Advises against using Russian filters and suggests alternatives for actual protection.
- Emphasizes the importance of not using filters with Cyrillic writing, even for costume purposes.
- Comments on the dated equipment being handed out by Russians, including steel helmets and aged weapons.
- Concludes by speculating on Russia's motives for distributing such equipment and masks.

### Quotes

- "Don't use them. Don't use them."
- "If I had any alternative whatsoever, I would not use one, even a new one."
- "Don't do it."
- "Do not use those filters."
- "Don't use it, even just to play around in a costume."

### Oneliner

Beau explains the dangers of using Russian surplus filters and advises against their use, suggesting alternative filters for protection.

### Audience

Preppers, Surplus Collectors

### On-the-ground actions from transcript

- Replace Russian surplus filters with new ones for CBRN protection (implied).
- Avoid using filters with Cyrillic writing, even for costume purposes (implied).

### Whats missing in summary

Beau's thorough explanation and warnings about the risks associated with using Russian surplus filters can be best understood by watching the full video.

### Tags

#Russian #MilitaryEquipment #GasMasks #PSA #FilterSafety


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Russian stories,
Russian equipment, Russian masks,
and we're gonna provide a little PSA.
At any point in time,
you have purchased a surplus Russian mask,
you need to watch this video in its entirety
because there is some information
that apparently people aren't aware of
when it comes to those.
Now, last night I was looking through photos and footage
of Russian equipment,
whether it be captured by Ukrainians
or being distributed to Russian troops and allies.
I was trying to get a read
on where they were at in their warehouses,
their logistics, seeing what they were handed out.
I shared an image that had a bunch of gas masks in it.
I shared it because I found it entertaining
because the gas masks are less than modern,
to put it nicely.
They are mid-Soviet era, let's say 71-ish.
And to me, that was funny.
They're also handing out a lot of other obsolete
or less than modern equipment.
For those who have a good read on this kind of stuff,
they're handing out Mosins.
It looks like they're handing them out
to the designated marksman rifles.
I guess they're out of SVDs
or they don't want to give them out.
I don't know, but I found that interesting as well.
The gas masks prompted a lot of questions.
First, does this lend any credibility to Russian claims?
I am highly skeptical of the Russian claims
because I've seen it before.
I have seen it before,
and I'm not talking about when the US
made very similar claims about Iraq.
I'm talking about when Russia made these exact same claims
about Georgia in 2018, and then again in 2020.
It honestly looks like they just changed the locations.
It's the exact same campaign.
Because they were less than accurate
and honest about it before,
if they want me to believe it,
they're going to have to show me
some kind of substantial evidence.
So no, it doesn't lend any credibility to that.
The presence of the masks,
does that indicate that Russia plans on using it?
I don't think you can read intent from that.
I don't think that this goes in the
they will or they won't column.
This is just, these are just masks.
This is pretty standard,
with the exception of the vintage.
It's standard to provide this kind of protection.
I would also point out that in today's age,
a mask alone, not super effective.
So I don't see that as being evidence
that they're planning on using it,
or that they truly believe Ukraine would.
Now, to the PSA part,
we were joking in the comment section
about how dangerous the equipment was.
My inbox filled up with people saying,
hey, I have one of these masks,
is what you're saying true?
Yes, yes it is.
The masks themselves, meh.
If they're pliable, they get a good seal,
they're probably all right, more or less.
The filters are not.
Russian surplus filters, don't use them.
Don't use them.
Stuff looks like this.
This one's Israeli.
I don't have any Russian ones
for all of the reasons I'm about to explain.
The lining of them,
most of them are based on the filter for the GP5.
The lining is lead.
If that's not bad enough,
the cotton in the early ones
contains a whole bunch of asbestos.
Don't use them.
Now, in the 80s, they switched the design up a little bit,
started using activated charcoal.
You would think that that would solve the asbestos problem.
It didn't.
Those that have been popped open and tested still have it.
Don't use them.
Do not use those filters.
Apparently, a lot of people have bought these for costumes.
Do not use them.
New filters, if you want actual protection,
CBRN protection, chemical, biological,
radiological, and nuclear,
the filters run about what you probably paid for that mask,
somewhere between 40 and 100 bucks for new ones, brand new,
full spectrum protection there.
If you're just looking for something for a costume,
you can get particulate filters for like $15.
Do not use those filters.
If it was me, I wouldn't use any Russian filter.
If I had any alternative whatsoever,
I would not use one, even a new one.
The old ones, certainly not.
They're dangerous.
Don't do it.
And you can pick up replacement filters relatively easily.
So just bear that in mind.
Do not use those.
If you picked up a mask at a flea market,
surplus store, eBay, wherever,
and that filter has Cyrillic writing on it, don't use it.
Don't use it, even just to play around in a costume.
Okay, so at the end, what do we have here?
As far as what I was looking at,
the Russians are handing out some pretty dated equipment.
Steel helmets, most of this stuff is Soviet era.
The weapons that are being handed out are aged.
So you can read into that as you wish.
As far as the gas masks and establishing intent
and what they plan on doing, this doesn't mean anything.
It doesn't provide any evidence one way or the other.
That kind of protection is pretty standard.
And if it was me, I would assume,
because they're handing out ancient stuff,
and it's not complete, it's not a full suit,
it's just the mask,
that maybe they don't actually believe there's a threat.
Or they just really don't care about their troops.
And at this point, either one of those beliefs
is, could be accurate.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}