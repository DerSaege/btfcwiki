---
title: Let's talk about wheat, Russia, Ukraine, and downstream effects....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Uy_BLhOyVWs) |
| Published | 2022/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the impact of the current state of the world on wheat, corn, and soybeans.
- Notes that Russia and Ukraine, significant wheat exporters, are facing disruptions that may affect the global market.
- Mentions factors like disruptions in transportation, lack of fertilizer, and combat affecting crop transportation.
- Shares that wheat prices in Chicago are at an all-time high, with corn and soybeans prices up as well.
- Warns of potential social unrest due to a lack of bread caused by the wheat shortage.
- Anticipates heavy impacts in the Middle East and Africa due to the shortage.
- Predicts ripple effects worldwide as a result of the wheat supply disruptions.
- Foresees an increase in bread prices in the United States.
- Advises people to stock up on ingredients if they took up bread making during the pandemic.
- Stresses the importance of addressing food insecurity globally.

### Quotes

- "A lack of wheat means a lack of bread."
- "The higher the percentage of income that goes to food, the more it's going to hurt because this is a staple and it has been interrupted."

### Oneliner

Beau explains the global impact of wheat shortages, warning of potential social unrest and advising preparation for rising bread prices.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support relief agencies that may need help due to food supply disruptions (implied)
- Prepare by stocking up on ingredients for staple foods (implied)

### Whats missing in summary

Potential strategies for addressing the global wheat supply chain disruptions. 

### Tags

#WheatShortage #GlobalImpact #FoodInsecurity #Prepare #SocialUnrest


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about wheat
and corn and soybeans
and how all of this is going to impact you
because of the current state of the world.
Two countries that have been in the headlines a whole lot
over the last couple of weeks
are really important to the world's wheat supply.
Russia is the world's largest exporter of wheat.
Ukraine is fifth.
Because of the state of those two countries, it is unlikely
that all of their crop is going to make it to market.
And that could be because the people who would bring it in
are currently all fighting.
Disruptions in transportation, a lack of fertilizer,
sanctions, literal the inability to get it from point
2.B combat taking place in the field, it's going to be disrupted. The prices
are already starting to reflect that. Wheat closed in Chicago at its highest
price ever. Corn and soybeans are both up 26% and that has to do with the oils. So
So, wheat makes bread. A lack of wheat means a lack of bread. A lack of bread is historically
associated with social unrest. When you think of social unrest, you tend to think of large
population centers. Now, India and China, they're not exporters of wheat, but they
produce a lot. They just consume it domestically. They should probably be alright. This is going
have the heaviest impact probably in the Middle East and Africa.
So when this occurs, it's likely to cause a lot of ripple effects all over the place.
And in the United States, you can certainly expect the price for a loaf of bread to go
up. So if you took up bread making during the pandemic, maybe lay on some ingredients.
There isn't really anything that anybody can do about this at this point. This is something
that people are going to have to ride out. It's not something that there's an easy fix
for here. This is one of the reasons that it's incredibly important for food
insecurity to be addressed worldwide because it doesn't matter what happens
now in Russia and Ukraine. This is probably already a done deal. It hasn't
happened yet we haven't seen the impacts but just the chain of events up to this
point it's probably it's probably gonna happen. So you can imagine that there
will be relief agencies that are gonna need help because they're not going to
be able to get the things they normally would and they're gonna have to find
some way to substitute. What that substitution is going to be I don't have
clue, but it's something they're going to have to work out and they're going to have to do it quick.
I would imagine that this is going to become front page news in the next week. There's already
coverage of it, but right now it's kind of buried in the back. Given the the likely impacts
when it comes to a lack of food, and what a lack of food normally does when stability is in question, sometimes anyway,
the spillover from Russia's adventure into Ukraine, it's going to widen.
Even if the conflict itself does not, there are going to be negative impacts all around
the world and is typically the case it's going to be visited on the people that have absolutely
nothing to do with it.
They're going to be the ones who suffer the most.
So this is something to kind of get ready for.
prepare yourself for this. How bad? We don't know. Is it going to impact the
United States to the point where we don't have wheat? No. But it is going to
get more expensive. It's going to hurt those on the bottom the most. Those
people who have a large portion of their income dedicated to food. The higher the
percentage of income that goes to food, the more it's going to hurt because this
is a staple and it has been interrupted and there's probably nothing
anybody can do about it at this point. So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}