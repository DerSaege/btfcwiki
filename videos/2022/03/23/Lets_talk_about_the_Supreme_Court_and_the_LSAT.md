---
title: Let's talk about the Supreme Court and the LSAT....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XwGMtXQdS8c) |
| Published | 2022/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the current process of nominating a new justice to the Supreme Court, the first-ever black woman nominee.
- Notes the lack of diversity on the current Supreme Court and the unique legal experience of the nominee.
- Criticizes attempts to discredit the nominee by focusing on her LSAT scores, despite her extensive legal background.
- Provides insights into what the LSAT test is and its purpose for law school admission.
- Mentions the nominee's acceptance into Harvard Law School and her graduation with honors.
- Points out the manufactured controversy around questioning the nominee's qualifications based on irrelevant factors like LSAT scores.
- Concludes that the opposition is more about her being a black woman rather than her qualifications.

### Quotes

- "There's no reason to be questioning her LSAT score. It was good enough to get her into Harvard."
- "She has more experience, a wider range of experience, than anybody on the court."
- "It's just manufacturing an issue to turn people against someone that they can other."

### Oneliner

Beau explains the unjust focus on LSAT scores to discredit the highly qualified black woman Supreme Court nominee.

### Audience

Legal system observers

### On-the-ground actions from transcript

- Support and advocate for qualified diverse Supreme Court nominees (exemplified)
- Challenge manufactured controversies and biases in judicial nominations (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the unjust scrutiny faced by a highly qualified Supreme Court nominee based on irrelevant factors like LSAT scores.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the LSAT and Supreme Court nominations.
For those overseas, the United States is in the process of putting a new justice on the
Supreme Court.
The person nominated will be the first black woman ever.
Because of that, a whole lot of folks have come out of the woodwork trying to find a
reason for her not to be there because it's still the United States.
I have a whole video going over her background, and I'll put it down below.
But suffice it to say, there is literally nobody sitting on the Supreme Court that has
her wide range of legal experience.
Not a single person.
Period.
Full stop.
Since they can't go after her for a lack of experience, they have decided to ask for
her LSAT scores.
And they have made this super important.
A whole bunch of people who got bored pretending to understand vaccines now are experts in
the legal system.
So what is the LSAT?
It's the test you take to get into law school.
To get into it.
It's on it.
Analytical reasoning.
Logical reasoning.
A writing sample and reading comprehension.
Weird, right?
I imagine, since they're holding this up as something that would be a requirement for
somebody to sit on the Supreme Court, you would think that it would have something to
do with the law.
It's not.
It's a placement test to see if you'll be able to handle the courses.
That's what it is.
Now I was going to try to sneakily find out what her LSAT score was, but in the process
of looking I found out that I would have to get into Harvard's records to find out.
Because that's where she got into.
Given the fact that she got into Harvard Law with this LSAT score, I'm going to assume
that it's at least a little above average.
But it really doesn't matter because this is the pre-test.
This is what you take to get into law school.
So what you would really need to know is her GPA.
When she left.
And again, I was going to try to find that out, but when I looked at her Wikipedia biography,
I realized she graduated cum laude.
I'm going to guess it was probably pretty high.
The reality is they don't have a reason to oppose her.
She has more experience, a wider range of experience, than anybody on the court.
She graduated from Harvard Law.
She probably knows what she's doing.
I mean, it seems as though she's an incredibly qualified candidate.
But you know, she is a black woman.
So there's a whole bunch of people who don't want to see that happen.
This is exactly what it looks like.
There's no reason to be questioning her LSAT score.
It was good enough to get her into Harvard.
And there is no doubt that she is qualified.
There's no doubt that she is experienced.
It's just manufacturing an issue to turn people against someone that they feel they can other.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}