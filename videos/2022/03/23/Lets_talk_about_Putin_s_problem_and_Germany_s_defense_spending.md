---
title: Let's talk about Putin's problem and Germany's defense spending....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-IdwjJUtuJE) |
| Published | 2022/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Germany's decision to spend about $110 billion annually on defense is a significant move that will have repercussions for Russia.
- Historically, Germany has not focused on building a large military due to events in the 1930s and 40s, but now it will become the third largest spender on defense.
- Russia's budget of about $67 to $70 billion for defense will be surpassed by Germany's new budget.
- Putin's actions have inadvertently put Europe on alert, leading to NATO countries increasing their defense spending.
- The increased defense spending by NATO countries, particularly a significant player like Germany, will put Russia in a difficult position to keep up.
- This situation has put NATO on a more aggressive footing reminiscent of the Cold War era.
- Russia's excessive spending on prestige items like nukes and Navy may not be strategically beneficial, especially in conflicts like Ukraine.
- The West and NATO's reaction to Russia's invasion is a bad sign for Putin's long-term plans.
- While most Germans may support the increased defense spending, neighbors like Poland might be relieved, as Germany previously didn't meet NATO's defense spending threshold.
- Germany's choice of military expenditures will provide insights into their future defense strategies and contributions to European defense outside of NATO.

### Quotes

- "Germany's decision to spend about $110 billion annually on defense is a significant move that will have repercussions for Russia."
- "Putin's actions have inadvertently put Europe on alert, leading to NATO countries increasing their defense spending."
- "The West and NATO's reaction to Russia's invasion is a bad sign for Putin's long-term plans."

### Oneliner

Germany's significant increase in defense spending will have repercussions for Russia and signal a shift in European defense strategies amidst rising tensions.

### Audience

European policymakers

### On-the-ground actions from transcript

- Monitor developments in European defense strategies and military spending to anticipate future geopolitical shifts (implied).

### Whats missing in summary

Insights into the potential impact of Germany's increased defense spending on European security dynamics.

### Tags

#Germany #Russia #NATO #DefenseSpending #EuropeanSecurity


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about Germany
and how it's a bad sign for Putin.
Early on, we talked about how no matter what goes on
on the ground, Putin lost the war on the international stage.
And because of that, Russia will be dealing with the consequences
for years and years and years to come.
The first big sign of that, beyond the sanctions,
happened this month as well.
Historically, because of events in the 1930s and 40s,
Germany has not really had a military, not a large one.
They focused mainly on territorial defense
and having enough capability to contribute to NATO missions.
But they never really built back up the massive military
that a country like Germany can't afford.
Germany has announced that it will now
be spending about $110 billion a year on defense.
Now, for Americans, you're going, OK.
And for everybody else in the world, it's a big deal.
Because at $110 billion, that makes them the third largest
spender on defense.
They will have the third largest defense budget,
surpassing Russia.
Russia spends about $67 billion, somewhere
between $67 and $70 billion.
One of the things that happened, one of the assumably unintended consequences of Putin's
move was putting Europe on alert.
Now under the pretext that Russia has offered it having to do with NATO, it would be counterproductive
to have all of the NATO countries increase their defense spending, especially a country
like Germany who can spend what it can spend.
There's no way for Russia to keep up.
Russia can't outspend Germany, much less Germany, the United Kingdom, France, the United States,
and all of the other NATO members.
This move put NATO on a more aggressive stance.
footing has changed. They're gonna start building up because we're going back
into a Cold War style thing. Now it's funny because the best presentation
I've seen of this apparently came from somebody who is a gamer. According
to them, they know nothing about this topic but they put together one of the
best presentations on it I've seen. $67 billion, $70 billion that Russia spends,
it doesn't really produce a lot. You get the breakdowns that you've seen thus far
because they have to spend a lot of money on prestige items. The nukes and the
Navy, that cost a lot of money and that doesn't really do them much good in a
situation like Ukraine. With all of these other countries starting to ramp up
defense spending, if Russia is truly concerned about NATO, they have to try to match it.
That's kind of what contributed to the downfall of the Soviet Union.
Russia has put itself in a bad situation.
The reaction of the West, of NATO, to this invasion is a really bad sign for Putin's
long-term plans, what he said they were.
Now contrary to what I think most Germans may believe about this, your neighbors aren't
going to be worried.
Most countries, especially countries like Poland, will probably be kind of relieved
because in the eyes of NATO for a long time, Germany wasn't meeting its 2% mark, which
which they're supposed to spend, all NATO countries are supposed to spend about 2% of
their gross domestic product on defense.
Germany wasn't doing it.
A lot of countries don't do it.
But for a country like Germany, where 2% is a lot of money, it created a little bit of
tension, especially with the less economically powerful countries who felt like Germany wasn't
pulling their weight.
Putting $110 billion into the defense budget will certainly alter that perception.
Now it will be interesting to see what Germany chooses to spend this money on because when
you're building a military, it's not just about putting troops together.
You have to decide what kind of war you're going to fight, so on and so forth, and you
have to plan ahead for that.
You have to start building the military you want to have when you go to war.
It's going to be very telling to see what that money gets spent on, whether it's tanks
or helicopters, whether they focus on naval power, where they put that money.
Because it will tell you a lot about their doctrine outside of NATO, what they feel they
they can contribute to European defense on their own.
So that's something, if you're interested in this topic, it's definitely something
to watch because it will be able to forecast developments in Europe because Germany is
a country that historically has paid attention to developments.
And the military they choose to build with this massive investment, it's going to be
geared towards their best guess of what coming conflicts in Europe would look like.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}