---
title: Let's talk about Ukraine political parties and the mirror....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aWCgFVmkgOs) |
| Published | 2022/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Zelensky and the National Security Council in Ukraine suspended 11 political parties in Ukraine due to their pro-Russian sympathies.
- The opposition party for life, a significant banned party, has ties to Moscow, including a main figure with connections to Vladimir Putin.
- These banned parties were suspected of collaborating with Russia, possibly sharing information or being potential puppet governments.
- Beau questions the decision to ban the parties, suggesting imprisonment might have been a more suitable action.
- He points out the realities of war in Ukraine, with death and destruction, justifying the need to remove potential threats.
- Beau draws parallels to historical actions in the US during times of threat, challenging notions of freedom of speech versus national security.
- He warns against glorifying violent rhetoric and politicians posing with guns, as seen in Ukraine, and urges self-reflection for Americans.
- Beau stresses the importance of acknowledging the harsh realities of war and avoiding the idealization of violence and conflict.
- He calls for a mirror to be held up to American exceptionalism and a recognition of the potential for similar actions in the US.
- Beau concludes by urging viewers to learn from Ukraine’s situation and recognize the dangers of escalating rhetoric and political tensions.

### Quotes

- "War is bad. It is dirty. It is nasty. It is ugly. It is cruel."
- "If you are calling to restore the Republic for the Glorious Revolution that requires violence, you don't love this country."
- "We acknowledge that we are much closer to looking like that than we may really want to admit."

### Oneliner

Beau examines Ukraine's political crackdown through a lens of war realities, cautioning against violent rhetoric and urging Americans to self-reflect on potential parallels. 

### Audience

Americans

### On-the-ground actions from transcript

- Hold a mirror up to American exceptionalism and violent rhetoric (suggested)
- Refrain from glorifying violence and militaristic politicians (implied)
- Acknowledge the risks of escalating political tensions (implied)

### Whats missing in summary

The detailed nuances and historical comparisons in Beau's analysis can best be grasped by watching the full transcript. 

### Tags

#Ukraine #PoliticalCrackdown #WarRealities #ViolentRhetoric #AmericanExceptionalism


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
Zelensky and the political parties because a whole bunch of people asked
about it after that last video. I would like to point out I don't watch Tucker
Carlson. I had no idea that was at the beginning of the segment. Okay, so we'll
go through it. If you don't know what happened, Zelensky and the National
Security Council in Ukraine suspended 11 political parties in Ukraine. That is
being presented in a bunch of different ways in Western media and we're going to
run through a couple of them. The first take is that he did this so he could
maintain power in the next election, which is possibly the worst political
take I've ever heard in my life. This assumes that there's going to be another
election, which means Ukraine wins. I'm fairly certain the person who led
Ukraine through grinding the Russian Empire to a halt and, you know, preserving
the country is probably kind of a shoe-in for getting re-elected. That's a
horrible take. The next is that it's Zelensky banning all the left-wing
parties. That's just objectively false. That's just not true. That
isn't what happened. This isn't something that can be framed on left and right
because there is no consistency there. If there's no consistency, that's probably
not the motive. It's worth noting that most of these parties are small. The only
real big one is the opposition party for life, right? That's the only big one.
They're not left-wing. So that kind of throws that whole theory out the
window. There's only one unifying metric with all of these parties, and that is
that in the past they have expressed pro-Russian sympathies. That's the
unifying history of them, which is also why Zelensky and the National Security
Council said they were banning them. That kind of matches up. Now it's worth
noting that I know at least one of these parties denounced the Russian invasion
like as soon as it happened, but they do have a pro-Russian history. That big one,
the opposition party for life, the one that actually matters, the main figure in
that is an oligarch who is loyal to Moscow, who has a daughter, who has a
godfather who is named, what's his name, Vladimir Putin. I mean, yeah, that seems
like a pretty significant tie. It's also worth noting that that same political
figure was arrested for treason, I think, right before the war and was on house
arrest, but as soon as the Russian invasion started, disappeared, took off.
Yeah, I mean, that seems like it would be a concern. Now, the next part in this is
the obvious question. This is what Zelensky did. The implication that
has been put out there is that these parties were somehow collaborationist
with Russia. Maybe they're feeding them information, maybe they were
going to agree to be the public government. We don't know. The idea is
that at least elements within these parties were engaging in activities that
were favorable to their opposition forces, and in response, Zelensky banned
those political parties. Do I think that's the right move? No, no, I don't.
Assuming that there's some kind of evidence to back up these assertions,
I don't think that's the right move. I think the right move is imprisoning them,
willing to bet that didn't go where you thought it was going to go. Yeah, let's
not be naive here for a second. They're at war. There is death raining from the
sky. Every day there's footage of people being pulled out from underneath rubble.
If you have a concern about people passing on information, or working with
the opposition, or agreeing to be their puppet government in different cities, if
the Russians take them, then, I mean, yeah, it seems like you would want to get
those people out of circulation so they can't continue to harm your country. I
mean, that seems easy. Seems like common sense. But I know somebody right now is
going, but Beau, what about freedom of speech and the Constitution? Sure, I mean, but
let's go back and think about what the people who wrote the Constitution did to
their political opposition when they had foreign troops in their soil. They drug
them out of their homes naked, burned their houses down, and covered them with tar.
And I know, that was a long time ago. The United States would certainly not do
that now. What did the US do the last time it was under actual threat of
having foreign troops on its home soil? It interned people. Not based on any
political party that they belonged to. Not based on anything they had a choice
in. Just based on color. But I know, that was a long time ago, too. We wouldn't do that
today. We're more civilized, right? Are we? Or have we just gotten better at it?
Because I'm pretty sure we probably just send undercover agents into mosques, like
we did. Stop being naive. It's a war. It's a war. Wars are bad. Bad things happen. I
had somebody send me a message that said they understand why I'm using this
framing device and bringing it back to the US constantly, but that it's not
working. That I need to be more clear about what I'm saying. So when y'all make
comments down below, I can click on your little picture and I can read the
comments that you've put out in the past. And I've noticed a very interesting
trend. Most, not all, but most of those who are very concerned about some of these
activities in Ukraine, they're the people that two years ago or a year ago were
talking about the glorious revolution, saying they were ready for civil war and
when the time comes and all of that stuff. Take a good hard look at Ukraine,
because that's what it would be like, only worse. Because it's not like there
would be a shortage of arms or ammunition in the United States. Maybe
it's time to drop that rhetoric. We're getting to see it firsthand and because
of the location of this country, we're actually getting a pretty clear picture
of it. War is bad. It is dirty. It is nasty. It is ugly. It is cruel. It's not your
ragtag group of friends marching in step like some 80s movie off to glory. It is
slaughter. It is horrible. Maybe it's a good idea to back off that rhetoric.
Maybe it's a good idea to stop elevating people to political offices who pose
with guns, because they're the type of people who can bring this about. Maybe
it's time to take a step back. You're getting a clear view of it. This is what
it would be like, only worse. Because every country on the planet would be
flooding this country with arms. Because it's not about right and wrong. It's
about power that goes for every country. And there's nothing better for the power
of other nations than for the world's superpower to rip itself apart.
Understand that if you are one of those who is calling to restore the Republic
for the Glorious Revolution or whatever nonsense you're talking about that
requires violence, you don't love this country. Because that's what happens to
it. I don't think it's a good idea to continue the idea of American
exceptionalism and pretend that this stuff is just something that would only
happen over there. Because it wouldn't. Everything that Ukraine has done, the
United States would do in its situation, only worse. Much worse. They have been
pretty restrained in their responses. I think it's important to note that. So yeah,
I think we need to hold a mirror up to ourselves. Every time there's some story
of the day that is trying to suggest that what's happening there is just not
something that would happen anywhere else, and we need to be really concerned
and outraged about that, we need to remember that we would absolutely do it
in almost every case that I've seen so far we already have.
Refusing to face the reality of this. Refusing to acknowledge that this is what
it's like. This isn't an anomaly. This isn't because it's Eastern Europe. It's
not because the Russians are barbaric. This is just what it is. And normally we
don't get to see it. We get a very sanitized view of it back here. But this
is different because it's in Europe. It's different. So we see all of it. We need
to acknowledge it. And we need to understand we don't want to go there. And all
of the tough guy rhetoric, all of the posturing, talking about wanting to
take America back, nothing was taken from you. But if you continue that rhetoric,
something will be, because it's going to look like over there. If there's one good
thing that can come out of what's happening in Ukraine for the United
States, it's that we acknowledge this. And we acknowledge that we are much closer
to looking like that than we may really want to admit. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}