---
title: Let's talk about No-fly zones and other options....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sPzQ1DyGZHs) |
| Published | 2022/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the desire to help and get involved in response to distressing events.
- Explaining the push for a no-fly zone in Ukraine without a comprehensive understanding.
- Urging caution about the consequences of implementing a no-fly zone in Ukraine.
- Describing the no-fly zone as a bluff that could potentially escalate into a force-on-force confrontation.
- Emphasizing the importance of being transparent about intentions, especially when advocating for actions that may lead to war.
- Pointing out the risks of NATO involvement escalating the situation in Ukraine.
- Advocating for covert methods of support to avoid direct confrontation and maintain plausible deniability.
- Suggesting the deployment of international volunteers as a potential solution while avoiding direct military intervention.
- Critiquing the emotional rhetoric and lack of consideration for consequences in advocating for certain actions.
- Warning against actions that could inadvertently harm civilians in Ukraine and provoke a larger conflict.

### Quotes

- "Shallow foreign policy takes tend to lead to shallow graves."
- "You see what's happening to civilians. And you don't like it, because you're human, right?"
- "A force on force engagement between NATO and Russia on Ukrainian soil would be devastating for Ukrainians."
- "We have to make sure that the something we do actually helps, doesn't hurt."
- "For the overwhelming majority of people pushing this, it's not because they care. It's because they want to do something."

### Oneliner

Exploring the risks of advocating for a no-fly zone in Ukraine and the importance of transparent intentions to prevent unintended consequences.

### Audience

Policy advocates and concerned citizens

### On-the-ground actions from transcript

- Coordinate an international force of volunteers to provide support to Ukraine (implied)
- Provide supplies, weapons, and intelligence to Ukraine openly while maintaining plausible deniability for other forms of support (implied)

### Whats missing in summary

Beau's detailed analysis of the risks and implications of advocating for a no-fly zone in Ukraine, as well as the necessity of transparent intentions and thoughtful actions.

### Tags

#Ukraine #NATO #NoFlyZone #InternationalSupport #Advocacy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about helping,
the desire to help, the desire to get involved,
because people are seeing things they don't like to see.
That's going to lead us to talking about Ukraine and NATO
getting involved, or a no-fly zone.
And we're going to go through a bunch of options,
because there's one that's being championed
by a whole bunch of people right now.
And I don't think that they're accurately
explaining what it means.
The first rule of an emergency is
to make sure you don't create another victim.
That's the first thing, right?
Right now, Ukraine, it's a 40-car pileup.
That's what we're using as an analogy right now.
It is bad.
Because of that, people want to rush in and help.
The main thing that is gaining traction right now
is a no-fly zone.
We've used them in the past.
The West has used them in the past,
not against countries like Russia, I would point out.
There's a substantial difference between when
we've used them before and against countries
and when we've used them before and using them now.
So let's talk about what it is.
A force goes into the air and basically ensures
that things don't fly.
No-fly zone.
Pretty simple sounding, right?
The thing is, in that international poker game
where everybody's cheating, the one that we talk about
on this channel all the time, it's a bluff.
It's a bluff until it's enforced.
From the other side, it is always perceived as a bluff.
What happens if Russia calls?
We put our forces up.
NATO puts forces up.
The UN puts forces up.
It doesn't matter.
It's the West.
Let's be honest.
These forces are in the air.
If Russia calls, what do they do?
They knock some planes out of the air.
And they know that that is going to start a force
on force confrontation with NATO.
So we let them choose when to attack.
Give them speed, surprise, violence of action, right?
Sound like a good idea?
Probably not.
Now, there's the possibility that it starts by accident.
There's that, too.
But if you're going to advocate for this,
don't try to backdoor into it.
Say you want to go to war with Russia,
because that's a pretty high likelihood.
And at least that way, you're not
risking American lives, NATO lives, for no reason.
You're being upfront about it.
Because if you put them up there and give the initiative,
the ability to decide when the war starts to Russia,
you're putting them at a disadvantage.
Just go ahead and say you want to go to war.
Forget about the no-fly zone.
Move in and start doing it.
But when that happens, remember why you wanted to do this.
You are upset about the footage.
You see what's happening to civilians.
And you don't like it, because you're human, right?
It will get worse, not better, when NATO moves in.
Because then you have another large force firing
at the ground, another large force moving through towns.
It's not really a great solution.
This will end up a lot like the people who,
in the two years or so prior to the US leaving Afghanistan,
kept cheering for it, because it'll
be better for the civilians, people who cannot stand
to look at what's happening there now.
Shallow foreign policy takes tend to lead to shallow graves.
Now, it is a bluff, though.
There is the chance that NATO or the UN
throws up a no-fly zone and Russia's like, OK, you got me.
I don't know how likely that is.
But it's possible.
I'm not going to say that it's not.
But it's possible.
And it's not a bad thing.
Because it's not.
It is something that can happen.
But once that force on force confrontation occurs,
if it doesn't happen, if Russia doesn't fold immediately
on that bluff, that force on force confrontation happens.
And then what?
NATO stomps them into the ground.
NATO will stomp Russia into the ground.
The worry is that they do it so badly that Russia launches.
That's the key difference between other countries
that we've done this before.
They didn't have a massive strategic arsenal
at their disposal.
That seems to be missing in a lot of the coverage,
the likely pattern of escalation.
Does that mean we can't do anything?
Does that mean that the West can't help?
No.
That isn't what it means.
It means that we can't attempt to win the next war
by doing stuff from the last.
We need plausible deniability.
The West needs plausible deniability.
That's the key thing that they need.
Yeah, sure, they can do things like give them
supplies and weapons and intelligence,
which is going to be way more important than people
are making it out to be.
We can do that pretty openly.
But anything else, it's got to be done a little bit
under the table.
It's got to be a card that is slid to them.
It can't be done openly because it runs the risk of war.
Now, the reality is we know what we're going to do.
Russia knows.
And we know that Russia knows.
And Russia knows that we know that they know.
And it's going to go back and forth.
But it limits our involvement in their eyes.
And we have that plausible deniability.
So what does that mean?
Well, we can't go there and send our fighters.
You're right, we can't.
That would be an act of war.
Flying tigers.
Prior to the US entrance into World War II,
there were some Americans who went to fight the Japanese.
They were contractors.
They were volunteers who went over to help.
Now, it is worth noting that we actually
got sucked into the war before they really got rolling.
But it started.
They paid them three times as much
as they would normally get paid.
But if they were shot down and captured, well,
I'm a volunteer.
I'm not part of the American military.
I'm working with the Chinese military.
Now, what they really need there, from where I sit,
they need force multipliers.
They need special groups on the ground.
That's what they need.
And they need a lot of them.
But we can't put in American troops.
Because if they get caught, well, the war starts.
If only there was some form of plausible deniability
on the way.
16,000 forms of plausible deniability on the way,
if I'm not mistaken.
That international force of volunteers, man,
that's a great cover.
Once they're there, then the United States, the British,
anybody who would be willing could put in troops,
special troops.
But they're not our troops.
They're volunteers.
No, no, no.
I'm not special air service.
I'm a volunteer.
That is pretty likely.
That's probably something that's going to happen.
These are the ways we can help and not risk a nuclear war,
not create another victim in this 40-car pileup.
Many of the suggestions are akin to saying,
OK, we're going to help this 40-car pileup
by crashing a 747 into it.
A force on force engagement between NATO and Russia
on Ukrainian soil would be devastating for Ukrainians.
If your actual desire is to help the civilian there,
you don't want to encourage that.
The no-fly zone, yeah, sure, there
is a chance that Russia faults.
I'm not going to say there's not.
There's also a chance that it puts NATO forces in a situation
where they're sitting ducks.
There's also a chance that it escalates.
If it was me and my choices were go to war or no-fly zone,
I would go to war because at least then NATO
maintains the initiative.
We're not just waiting to get hit.
Now, the pundits who are pushing for this, have any of them
brought any of these other options up?
Have they talked about the realities
of what's going to happen?
Or have they used a bunch of emotional rhetoric
because it's good for ratings?
They do not care.
The majority of the people pushing this
do not actually care because if they did,
they wouldn't be going this route.
You can see the emotional rhetoric
that's being used by some of the stuff that's being said.
And you think about how it ties in.
One of the big ones right now, oh, we're not going to help them
because they're not part of NATO.
You know, Korea and Japan aren't part of NATO either.
Trying to tap in to the people that have strong links
to those countries, right?
And they're right.
Japan and Korea are not part of NATO.
Neither is Australia.
They're not in the North Atlantic.
We have other treaties with them, other defense treaties.
We didn't have one with Ukraine.
And people are saying, does that make a difference?
Yes, it does.
Russia knows that if it hits a NATO country,
it goes to war with all of NATO.
It is Dr. Strangelove.
That movie, it's the doomsday device.
And it doesn't serve a purpose if you keep it secret.
The alliance is the deterrent.
Ukraine wasn't part of it.
Therefore, Russia knew it could move on it.
The alliances with Korea or Japan or any of the other countries
that get used in this way, they're public.
They know that they exist.
Another thing to keep in mind is that a lot of the voices,
not all, but a lot of the voices behind this
are the same people who helped inflame the country that led to,
in a way that led to January 6th.
And I'm not trying to tie the two together.
I'm getting to a point here.
A lot of those people that did that were there that day,
hiding behind chairs, because they didn't think
it was going to impact them.
They didn't understand how much power their words had.
And you're seeing the same thing right now.
They're pushing this because it's good for writings,
because it makes them look tough,
because it gives them this rhetorical edge
to make it seem like they're up on world events.
It's for votes.
It's for image.
For the overwhelming majority of people pushing this,
it's not because they care.
It's because they feel helpless,
and they want to do something.
And I can understand that.
We have to make sure that the something we do
actually helps, doesn't hurt.
If there is one good thing that can come out of this,
it's that those people who are inflaming this situation
unnecessarily, those who are pushing it,
they are certain that it's not going to impact them.
But if it does spiral, yeah, they all live in Atlanta,
DC, New York, LA, they're not getting out.
They probably think they are, because they hear those times.
Oh, it's going to take 22 minutes.
I can get to my helicopter.
Yeah, that's only if you know when it starts.
It's going to take like 18 minutes to confirm.
You're not going to know, and you're not going to get out.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}