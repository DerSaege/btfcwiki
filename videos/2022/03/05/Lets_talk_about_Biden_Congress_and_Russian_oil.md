---
title: Let's talk about Biden, Congress, and Russian oil....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sh-L8k3RHnM) |
| Published | 2022/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congress is considering a bipartisan effort to ban Russian oil, which the White House disagrees with.
- Congress believes they may be able to affect regime change in Russia through economic means.
- There are risks associated with creating chaos in Russia, including uncertainty about who may take over and the security of Russia's arsenal.
- Shutting down Russian oil could lead to increased reliance on OPEC, impacting gas prices and causing economic strain.
- Biden opposes the move to ban Russian oil, as it could lead to higher gas prices for Americans.
- Congress aims to pressure Putin through economic measures, hoping for resistance from the people or oligarchs.
- Biden may face sharp debate with Congress, including members of his own party, over this issue.
- If Congress passes the ban, it could give Biden leverage in negotiations with Russia.
- Economic sanctions can have a significant impact on the target country but come with uncertainties and potential consequences.
- The debate between Congress and the White House revolves around the effectiveness and consequences of banning Russian oil.

### Quotes

- "I do not like sanctions, economic warfare that hits the average person."
- "Biden is already getting hammered for this because there are a lot of people who believe the president of the United States has way more control over gas prices than he actually does."
- "The only good thing that I can see coming of this is that if something like this passes Congress, they're handing Biden a really good negotiation tool."
- "I don't like kicking down. This will have a marked impact on the average Russian."
- "Biden is already getting hammered for this because there are a lot of people who believe the president of the United States has way more control over gas prices than he actually does."

### Oneliner

Congress considers banning Russian oil, but Biden opposes it due to potential economic impacts on Americans and uncertainties about outcomes in Russia.

### Audience

Policy Makers, Congress Members

### On-the-ground actions from transcript

- Engage in informed debate and decision-making on economic measures affecting international relations (implied).
- Advocate for diplomatic negotiations and alternative solutions to address political issues (implied).
- Stay informed about geopolitical events and their potential impacts on global economies (implied).

### Whats missing in summary

Insights into the broader geopolitical implications and potential long-term effects of banning Russian oil.

### Tags

#Congress #Biden #RussianOil #EconomicSanctions #Geopolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Biden and oil and Congress and some differing
opinions, because Congress has a bipartisan effort to basically ban Russian oil.
They want to, they want to shut down Russian oil.
and the White House disagrees with this stance. Before we get into it, I want to talk about what
should be for a second and then we'll talk about what is. I do not like sanctions, economic warfare
that hits the average person. This is that. I don't like kicking down. This will have a marked
impact on the average Russian. The economy is already cratered. I'm not
particularly fond of Congress's plan. So that being said, why do they
want to do it? Because it would be incredibly effective. I believe that
Congress is under the impression that they might actually be able to affect
regime change in Russia using economic means. Creating a situation in which the
people decide that it's time for Putin to go. And they might be right. There
might be a point that they can reach through economic means where that would
happen. However, it's a lot like Senator Graham's wish. One of the things I
didn't mention in that video is that you don't know who's coming next. You don't
know who's going to take over. It might be worse. I would also point out when
you're talking about Russia, if there is that kind of chaotic, popular uprising
that changes the leadership there is a period in time where a very large
strategic arsenal is in flux and that's not a good thing. There are a lot of
risks associated with this. Congress seems to believe that this is the
quickest way to lessen the fighting. I'm not sure of that. I'm not convinced of
that. I do believe that it would put additional economic strain on Russia, and
it would certainly create discomfort among the average people of Russia.
I don't know that it's enough to cause popular resistance, and I
I don't know that it's worth doing that to people with uncertainty.
Biden, at least for the time being, it seems that the White House is against this move.
They don't want to do this to Russian oil.
Is it because Biden is also sympathetic to the plight of the average Russian?
No, Biden's a better politician.
If the U.S. does this, your gas prices go up, you will pay more at the pump.
Biden is already getting hammered for this because there are a lot of people who believe the president of the United
States has way more control over gas prices than he actually does.  That's probably his motivation.
He is probably also more aware of the fact that it doesn't look like OPEC is going to
increase production.
This is something that Congress may not have thought about yet because generally in the
past OPEC has, at least recently anyway, been willing to help out at times.
It doesn't look like they're going to this time.
So shutting down Russian oil kind of makes us more heavily reliant on OPEC, because we
don't have the infrastructure we need to be moving to in the United States, which will
make all of these issues worse, gas prices, stuff like that.
So not just will it impact the average Russian, it will impact the average American, the average
Canadian, it's going to hit everybody. I don't think it's a good idea, however,
I cannot deny that these kinds of measures do cause a lot of economic
issues in the target country, and that seems to be Congress's goal. They seem to
believe that if they hit him hard enough in the wallet that either the
people or the oligarchs will turn against Putin and I can't say that that's
out of the realm of possibility. I just don't know how high a price people are
going to be willing to pay for this and I don't know how long people would have
to suffer for that catalyst to arise.
So there is probably going to be sharp debate between Biden and Congress, including members
of his own party over this.
I don't think Biden wants to, I know Biden doesn't want to do this, and I don't think,
I don't think that he's likely to change his position on it.
would have to be something that Congress goes out and sells it to the American people and
then there's enough political pressure on him to sign it.
The only good thing that I can see coming of this is that if something like this passes
Congress, they're handing Biden a really good negotiation tool because he can talk
to Russia and say hey you know I can push back against this or I can sign this
send it into law and it's gonna give him leverage. That's about the only real
positive I can see of coming of it. Anyway it's just a thought y'all have a
good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}