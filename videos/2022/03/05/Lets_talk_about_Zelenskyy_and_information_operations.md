---
title: Let's talk about Zelenskyy and information operations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2UBOFjtZv7w) |
| Published | 2022/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Information campaigns aim to disrupt unit cohesion in the opposition by making them doubt their cause or the people they are with.
- A story broke about Russians planning to remove Zelensky, but Ukrainian special operations thwarted the attempt, possibly with a tip-off from Russian intelligence.
- The effectiveness of an information campaign lies in its plausibility, not necessarily in whether it actually happened.
- Possible reasons for the leak include allegiances to Ukraine or being a patriot for Russia trying to protect Putin from a bad decision.
- The leak has sidelined those involved in the operation, causing internal security in the FSB to scramble to find the leak.
- Well-trained individuals involved in the operation are now relegated to less sensitive positions to prevent future leaks.
- The success of this information operation lies in its self-reinforcing nature and the doubt it sows within Russian intelligence.
- While the information may not be true, the Russians are compelled to investigate to maintain loyalty and trust within their ranks.
- Information operations during wartime are designed to influence people globally, often requiring the spread of misinformation.
- Consumers of information should be aware of the potential for deception and manipulation in the information they receive.

### Quotes

- "You're not the target. You're collateral in this."
- "During wartime, there are a lot of information operations going on that are designed to influence people all over the world."
- "But in order to get that information out, in order to make it believable, well they have to lie to you too."

### Oneliner

Information campaigns disrupt opposition by sowing doubt; plausibility is key, sidelining and destabilizing, forcing investigations and manipulating beliefs.

### Audience

Global citizens

### On-the-ground actions from transcript

- Verify information sources (implied)
- Stay critical of information received (implied)
- Educate others on the potential for misinformation in information campaigns (implied)

### Whats missing in summary

The full transcript provides a deep dive into the intricacies of information campaigns and the potential consequences of misinformation during wartime.

### Tags

#InformationCampaigns #Misinformation #WartimeTactics #GlobalInfluence #Deception


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
information campaigns and how they work and how some are perfect. This is
something I've wanted to talk about since everything started over there, but
I had to wait until there was one that was one I couldn't mess up by talking
about it. So the goal of an information campaign is, well there could be a bunch
of different goals, but the most successful are those that disrupt unit
cohesion in your opposition. Those that make them doubt their cause or doubt the
people they're with. Those are the best kind because it degrades their
capabilities. If you missed it, news broke, a story came out, reporting
suggests, however you want to phrase it, that the Russians decided, well Zelensky
needed to go, and they put in a team to do this. And then Ukrainian special
operations stopped them. And they were able to do this because they got tipped
off by the FSB, by Russian intelligence. So did that happen? We don't know, but it
also doesn't matter because the story went out, it got widely reported on, which
means the person that oversees looking into stuff like that, they have to act on
it. Is it plausible? Yeah. There's a couple of reasons why somebody on the Russian
side would do this. One is the obvious. They have allegiances of some kind to
Ukraine. Maybe a girlfriend or a wife or a husband or something like that.
Another would be that it's somebody who's a true patriot for Russia, and they
understand that when Putin made this decision he was wrong. He was too
arrogant and didn't understand the consequences if it had been successful.
So this person was basically protecting Putin from himself, making sure that
Putin didn't further undermine his command by making sure this didn't
happen. Sacrificing those who were put in to accomplish the mission for the
greater good of Russia and all of that. That could be a reason. Or it could have
never happened, but it doesn't matter because it's plausible. The people who
are in charge of stopping leaks, they have to investigate. And the Ukrainian
side, they picked the perfect thing to do this with because the people who had
access to this information, the people who had access to a plan that would
remove Zelensky, these are the best and brightest. And now they're in essence on
the sidelines. They're not going to be tasked with anything that sensitive
until it can be demonstrated that they wouldn't do it again. And internal
security within the FSB is going to be just going nuts trying to find this leak
that may or may not exist. But it doesn't matter because it's plausible. That's how
information operations should work, good ones, effective ones. And if they're
perfect like this is, it doesn't even matter if somebody points out that it's
an information operation or that it could be because it's self-reinforcing.
It's plausible. But these people, they probably wouldn't do that. But they're so
well-trained because they're the best and brightest, if they did do it, would
internal security really be able to catch them? And it creates this loop. I
don't know how many people had access to the information regarding this specific
operation, but those people, they're on the sidelines. They're not going to be
tasked with anything new, anything sensitive. They're going to be relegated
to positions that are below their capabilities. So this degraded Russian
intelligence operations just by slipping that little bit of information out. And
while the Russians are probably sitting there thinking, yeah this isn't true, they
certainly cannot risk not investigating. Because if it turns out later to be true,
after it was in the papers, well then you have to question the loyalty of the
people who should have done the investigation. Why didn't they? Maybe they
weren't loyal to Putin either. It's brilliant. It's brilliant. Now the reason
I'm bringing this up is not just to talk about how perfect this is, it's to point
out that I would imagine most people who heard this news, they have an opinion of
it. They believe it and there's probably a small percentage of people who are like,
that's probably not true. They're just trying to trick them. But it doesn't
matter. You're not the target. You're collateral in this. And during wartime,
there are a lot of information operations going on that are designed to
influence people all over the world. But in order to get that information to
them, in order to get that information out, in order to make it believable, well
they have to lie to you too. So when you're consuming information, just always
remember that. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}