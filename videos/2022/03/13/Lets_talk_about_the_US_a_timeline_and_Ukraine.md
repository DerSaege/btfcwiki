---
title: Let's talk about the US, a timeline, and Ukraine...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Z7rE38x4ypE) |
| Published | 2022/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls memories of disinfecting in the kitchen triggering thoughts about the early days of the COVID-19 pandemic when his wife worked in a COVID wing.
- Mentions the decontamination ritual his wife had to undergo every day after work.
- Talks about the initial advice to disinfect packages and leave them outside in the sun due to lack of information about the virus.
- Shares an encounter at a gas station where people express disbelief that a crisis like Ukraine could happen in the US due to its wealth and power.
- Describes American exceptionalism as the belief that the US could handle any crisis because of its financial and political standing.
- Provides a timeline of major events during the COVID-19 pandemic, including milestones like the declaration of a global pandemic by the World Health Organization and vaccine developments.
- Notes the significant number of COVID-19 cases and deaths in the US compared to other countries despite its wealth and power.
- Criticizes American exceptionalism for leading to complacency and inefficient handling of crises.
- Urges Americans to re-evaluate their priorities, focusing on community, policy, and leadership rather than empty patriotism.
- Encourages reflection on the need for genuine leadership to address national and global challenges.

### Quotes

- "Money and power couldn't stop a virus."
- "The United States has succumbed to bumper sticker patriotism."
- "We're still looking around as countries all over the world pass us."
- "We've become apathetic to everything around us."
- "It's probably something that is way past time to address."

### Oneliner

Recalling the early days of COVID-19, Beau criticizes American exceptionalism and calls for reevaluation of priorities and genuine leadership in facing global challenges.

### Audience

Americans

### On-the-ground actions from transcript

- Invest more in policy than in party and personality (implied)
- Care about local communities by actively engaging and supporting them (implied)
- Prioritize genuine leadership over empty patriotism (implied)

### Whats missing in summary

Importance of reflecting on past failures and redefining American priorities for effective crisis management.

### Tags

#COVID-19 #AmericanExceptionalism #Leadership #Policy #Community


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to start off with a story.
Stick with me there as a point.
I was disinfecting something in the kitchen.
And you know, smell is a very strong trigger of memory.
And that smell, all I could think of was the early days.
And for those that don't know, my wife,
when all of that mess started, she ran a COVID wing.
So every day when she came home,
it was a decontamination ritual.
And it just got me thinking about it.
This is back when they were still saying,
to disinfect the packages, leave them outside in the sun,
given the best information they had,
bearing on the side of caution, you know.
And you know, it just got me thinking about
all of that stuff.
And then when I finished, I had to run up
to the gas station to get something.
And when I'm in there, there's two people
in line ahead of me and they're talking.
And they're talking about Ukraine.
And one of them's like, you know,
I'm glad nothing like that can happen here.
And I kind of look at them and I can feel
the cashier who knows me.
Staring at me.
And so I kind of look back and she's giving me
that face that's just, you hush, you know.
Yes, ma'am.
So I didn't say anything.
But the person that was with them kind of challenged it.
And I was like, what do you mean?
Can't happen here.
And they had this conversation because it's the South.
They did it right there in line.
And the general consensus they came away with
was that it couldn't happen here
because America's got too much money.
The US has too much money, too much power.
If something like that started,
well, we'd be able to deal with it.
That American exceptionalism, you know.
And I get my stuff after I leave and I come back home.
And when I walk back in, I can still smell the Lysol.
And it made me realize that COVID
can probably teach Americans something about Ukraine
and probably teach us something about ourselves as well.
So quick timeline for everybody.
December 12th, 2019, the first case was identified.
January 7th, 2020, it was identified
as a novel coronavirus.
And the same day, the CDC set up
an incident management center.
January 17th, the CDC starts to screen at airports,
but it doesn't matter.
It's too late.
It's already in Washington state.
February 23rd of 2020 is when Italy became the hotspot.
February 23rd of 2020 is when Italy became the hotspot.
Remember the singing, all of that.
March 11th is when the World Health Organization
finally was like, hey, we're in a pandemic
and declared that openly.
March 17th, trials began for the first vaccine, Moderna.
April 10th, US surpasses Italy in the number of lost.
By May 9th, US unemployment at 14.7%.
May 28th, US lost hits 100,000.
September 22nd, 200,000.
October 2nd, Trump gets it.
October 7th, New Zealand declares itself COVID free.
December 11th, Pfizer is approved for emergency use.
December 14th, 300,000 lost.
Sandra Lindsay on the same day becomes the first American
outside of a trial to be vaccinated.
2021, January 18th, 400,000.
February 21st, 500,000.
June 1st, Delta variant becomes dominant.
June 15th, 600,000.
October 4th, 700,000.
December 15th, 800,000.
2020, February, 900,000.
We just passed two milestones.
Worldwide, 6 million lost.
In the US, a study concluded that we've had more
than 1 million excess deaths.
And going off of this, yeah, that matches up, right?
Matches up with the other counts.
The US makes up 4.25% roughly of the world population.
All that wealth, all that power, 16.6% of the world's lost.
American exceptionalism, that's lethal in a whole bunch of ways.
The US has sat around screaming, we're number one,
we're number one for so long.
Well, we're not.
A whole lot of countries handled this far better than we did.
6 million worldwide, 1 million in the US.
4.25% of the population.
When you look at this and you think about how it happened
and how you had people didn't believe that it could happen,
and then you look at Ukraine and you hear people say,
well, it couldn't happen here because we have too much money and power.
Money and power couldn't stop a virus.
Money and power are typically things that feed conflict.
The United States has succumbed to bumper sticker patriotism.
Slap that American flag on your car and well,
because we're the greatest country on the planet.
The way we have jumped from calamity to calamity lightly
and done less than well in handling them should probably give Americans pause.
We should probably start to think about what exactly we want
because right now we're so convinced that we're number one.
We're still looking around as countries all over the world pass us
because we've become apathetic to everything around us
because we don't care about our local communities,
because we have invested more in party and personality than we have in policy.
It's probably something that is way past time to address.
We want to claim ourselves to be the world leader.
Funny thing about that is you actually have to lead.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}