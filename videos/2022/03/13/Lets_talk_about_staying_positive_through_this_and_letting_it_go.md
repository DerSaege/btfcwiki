---
title: Let's talk about staying positive through this and letting it go....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wmEgl6TfX74) |
| Published | 2022/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the struggle of staying positive amidst doomscrolling and feeling helpless.
- Acknowledges the importance of celebrating small wins in the midst of chaos.
- Shares a heartwarming example of a little girl singing in a bomb shelter bringing warmth and hope.
- Encourages people to find and acknowledge moments of positivity in their lives.
- Advises to not compare activism efforts with others; it's not a competition.
- Advocates for doing what you can, where you can, for as long as you can.
- Emphasizes the importance of taking breaks and not letting awareness consume you.

### Quotes

- "Acknowledge the wins."
- "It's not a competition."
- "There's nothing wrong with feeling sad or angry."
- "There's nothing wrong with acknowledging the wins, even if they're small."
- "Y'all have a good day."

### Oneliner

Beau addresses staying positive amidst chaos, celebrating small wins, and avoiding comparison in activism efforts.

### Audience

Individuals seeking guidance on staying positive amidst challenging times.

### On-the-ground actions from transcript

- Acknowledge the wins in your life, no matter how small (implied).
- Celebrate moments of positivity and hope (implied).
- Take breaks when needed to prevent feeling overwhelmed (implied).

### Whats missing in summary

The importance of finding moments of joy and hope amidst difficult circumstances.

### Tags

#StayingPositive #CelebratingWins #Activism #SelfCare #CommunitySupport


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about how to stay positive.
Because I've had quite a few messages come in asking for
advice on how to do that.
I chose one of them to read, but there's a bunch of them
with this tone.
You've tweeted about doomscrolling.
I do that and it takes a toll on me.
I see you at this, what seems like 24 hours a day.
How does it not take a toll on you?
Do you have any tricks for people to avoid the sadness
or anger or feeling of helplessness?
Who said it doesn't make me sad or angry?
Who said it doesn't take a toll?
People get an impression from these videos,
But I get to choose when to record.
It's easy to be calm and collected and convey calm
when you get to select the time you speak.
But that being said, yeah, I actually
do have one really important one that I think everybody
needs to take into account at all times,
It's not just during stuff like this.
Acknowledge the wins.
Acknowledge the small wins.
If you watch, you'll see people there in the midst of chaos doing that.
That's what gives them the hope.
That's what gives them the will to push on.
There's a video of a little girl in a bomb shelter, and she sings Let It Go in Ukrainian.
Outside of that shelter, there is lead and steel forged into weapons of war that are
colder than anything else I ever created. But in that moment when she was singing
the warmth inside that shelter you could see it through the video. Everybody
listening. People had their phones out recording it, smiling. They were
acknowledging that win. That moment when everything was okay. And those things
aren't gonna be the same for everybody. Different people are gonna see
different things as a win. For them, that moment in time, that was a win. That was a
moment when they could breathe, relax. For me, I played that with my kids around.
They recognized the tune. They didn't recognize the words, but they recognized the tune and
they sang along. All I wanted for Christmas was a Russian artillery officer.
So you have those moments in your life, and you have to acknowledge them.
Maybe two days after that, I think, news had broke, I saw a tweet.
That little girl was in Poland.
In the grand scope of things, that's one child.
For me, that was a win.
That was something I could acknowledge and be like, ha, there is good in the world.
You have to find those, and you have to acknowledge those moments when they come, and it may not
even have anything to do with what's going on, what you're doom-scrolling, what is making
you feel bad.
It could just be a quiet meal with your partner.
It could be Netflix putting a new season of a show you like on.
those. What often happens is people see fire raining from the sky and they
understand how horrible it is on some level and then they feel as though they
can't feel good about the things in their life. Now you've got to. You have to
take those moments to acknowledge it or you will become paralyzed. You won't be
able to do anything, you will freeze.
And then aside from that, you do what you can, when you can, where you can, for as long
as you can.
That's it.
The other part of this was that, well you do it this long, this is not a competition.
It's not a competition.
Never look at it.
compare your activism, for lack of a better word, to other people.
It's not a competition.
Right now, helpless is a good word to describe it.
There's not much you can do.
It might be easier for me because there is something I can do.
I can gather information and put it out in a video.
Makes me feel like I am contributing in some way, hoping I'm deepening understanding,
and that at some point later, that may have a positive outcome.
Right now, the decisions being made, the policy decisions that are going into effect, those
are way above our pay grade.
not involved in them. You could donate. You can do a lot of little things to help, but
as far as ending it, there's nothing you can really do. The only thing you can do is be
aware, but you can't be so aware that that's all you think about, because you end up in
casualty, just a different kind. There's nothing wrong with taking a break.
There's nothing wrong with feeling sad or angry. There's also nothing wrong with
acknowledging the wins, even if they're small, even if in the grand scheme you
You know that it isn't a huge one.
That it's one out of millions.
But you can take that moment.
If the people there living through it can take that moment, you certainly can.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}