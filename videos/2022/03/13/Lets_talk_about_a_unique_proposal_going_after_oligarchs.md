---
title: Let's talk about a unique proposal going after oligarchs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RN4vP74kZhY) |
| Published | 2022/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discovers legislation allowing President Biden to issue letters of mark and reprisal to go after Russian oligarchs.
- Explains letters of mark as documents authorizing private citizens to act as privateers with legal backing.
- Mentions potential effectiveness of the bill in prompting oligarchs to move their assets out of foreign countries.
- Points out the constitutional basis for issuing letters of mark under Article 1, section 8, clause 11 of the US Constitution.
- Connects the bill's concept to historical practices and the use of private contractors.
- Suggests amending the bill to target financial assets like cryptocurrencies for increased effectiveness.

### Quotes

- "Is this constitutional? Surprisingly, yeah."
- "Those that are sanctioned will remove their assets."
- "It might be a really good idea to amend this to include the ability to go after financial assets in, say, I don't know, cryptocurrencies."

### Oneliner

Beau explains legislation allowing President Biden to issue letters of mark to target Russian oligarchs, proposing effectiveness and a potential amendment to include cryptocurrencies.

### Audience

Legislative enthusiasts

### On-the-ground actions from transcript

- Track the progress of bill 6869 (suggested)
- Stay informed about the implications of the bill on targeting oligarchs (suggested)
- Advocate for potential amendments to include cryptocurrencies in the legislation (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of a unique legislative proposal and its potential impact on targeting Russian oligarchs, suggesting room for further effectiveness through amendments.

### Tags

#Legislation #LettersOfMark #RussianOligarchs #Constitution #Cryptocurrencies


## Transcript
Well ahoy there internet people, it's Beau again.
So today we're going to talk about some legislation that I was unaware of.
I had been keeping up with events overseas and hadn't really been following my legislative
tracker as much as I should.
In the House, the bill is 6869.
It's a Republican sponsored bill and it allows President Biden to issue letters of mark and
reprisal.
Yeah.
If you don't know what those are, don't feel bad because they haven't been used in quite
some time.
These are the documents that Captain Jack Sparrow wanted.
They are their authorization from a state for private citizens to act as privateers,
pirates with legal backing.
The bill is designed to allow Biden to issue letters of mark to go after Russian oligarchs,
go after their yachts, their aircraft, their belongings that exist outside of the territorial
confines of the United States.
There are obvious questions.
Is this constitutional?
Surprisingly, yeah.
Article 1, section 8, clause 11 of the US Constitution does in fact provide a mechanism
to issue letters of mark.
That's wild.
I think it might actually be effective in a really bizarre way.
Mainly, the first time somebody does it, once these are issued, the first time somebody
does it under that letter, oligarchs will pull all of their assets out of foreign countries.
They will.
Those that are sanctioned will remove their assets.
They will find some way to get them back home where they are safe or get them to a country
that wouldn't allow it.
I know it is an incredibly bizarre move, but it's not as wild as it sounds.
This was standing policy for a really long time for a lot of countries.
In the age of private contractors, this seems like the next logical step.
Given Russia's use of Wagner in Ukraine, I don't think they really have a lot of room
to complain.
Yeah, so keep track of this bill, especially if you ever thought that a pirate's life was
for you.
Because yeah, it exists.
I thought it was a joke when I first saw it, but looked at it, and yes, it's there.
Yes, there's a constitutional mechanism for it.
I honestly don't know what to say beyond it would be a very unorthodox method of limiting
the ability of oligarchs to operate.
It might be a really good idea to amend this to include the ability to go after financial
assets in, say, I don't know, cryptocurrencies.
That might be a really good idea.
It might be incredibly effective.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}