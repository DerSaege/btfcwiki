---
title: Let's talk about a follow up on major powers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fCNntReQWjk) |
| Published | 2022/03/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responds to a viewer's message about NATO and whether major powers are inherently good.
- Compares Russia and the US in terms of imperialism and maintaining empire.
- Explains how the US maintains its empire primarily through economic power.
- Talks about China's strategy of state-to-state deals for stability and economic gain.
- Emphasizes that foreign policy is about power, not right or wrong.
- Stresses the need to move past nationalism for a better world.
- Advocates for a broader perspective that transcends borders and focuses on commonalities among people.

### Quotes

- "No major power is good."
- "Foreign policy isn't about right and wrong, good and evil. It's about power."
- "Nationalism is politics for basic people."
- "We have more in common with the soldiers in Ukraine than with our own representative in DC."
- "We have to start thinking broader than a border."

### Oneliner

Beau responds to a viewer's message, comparing major powers' imperialism, explaining US economic empire maintenance, and advocating for a broader perspective to transcend nationalism for a better world.

### Audience

Global citizens

### On-the-ground actions from transcript

- Communicate with people globally to foster understanding and unity (implied)
- Shift focus from nationalism to global commonalities for international political change (implied)

### Whats missing in summary

Beau's detailed analysis and insights on major powers, imperialism, and the need to transcend nationalism for a better world.

### Tags

#ForeignPolicy #Empire #Nationalism #GlobalUnity #PowerRelations


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to do a follow-up.
I got another message from the person who asked about NATO being honest and we're going
to go over it because it's, this is a process. Getting to this point is a process. Okay,
didn't expect a video, but appreciate it. I read the comments and see other people having
trouble, so can I ask for a follow-up? I've watched enough of your videos to know comparing
Russia to the US is a criticism, not a compliment. It's not saying it's okay for Russia to
act the same way and my professor gave me a list of times when Russia lied or was the
aggressor. She watched your video and said you were right in that we can't trust any
power that pursues an elective war. Isn't this a bit cynical? No major power is good?
All powerful countries are exactly the same and engage in imperialism. If this is the
way it's always been, do we just accept it? If you and her are right, it seems impossible
to hope for that better world. Lots of questions and we'll go one by one. Okay, isn't this
a bit cynical? No, it's just reality. It's the way it is and on this channel when we're
talking about foreign policy, we tend to talk about the way it is, not the way it should
be because we can't get to where we should be until people understand what it is. A lot
of people don't even know what's broke yet. No major power is good. We'll come back to
that. All powerful countries are exactly the same and engage in imperialism. I mean, they
all maintain empires, but they're not all exactly the same. They do it differently.
Russia's old school. Russia is old school. Russia changes maps. Russia changes maps.
That's the huge difference there. They either just take the territory and say, this is Russia
now, or they establish a new country that is a total client. It's a puppet government
in the truest sense. And that's not a modern way of maintaining an empire. Countries don't
do that anymore, not really, because it's not incredibly effective. The United States
maintains its empire mainly, and this is going to surprise a lot of people, through economic
power, not military power. Military is the backup. The US empire is maintained primarily
through money. Corporate logos, banks, Wall Street. That's really the front line when
it comes to American empire. Because the US tends to get a country reliant on supplying
the US, trading with the US. So they do it, they're asked, or they lose that money. And
those people at the top, well they really want it because they're getting the lion's
share of it. So it tends to keep countries in line. The US also uses a lot of cultural
power, which is hard to quantify, but you're talking about, well, everything that you see.
Logos, movies, the general attitude that reaches all over the world. American culture is thick
everywhere. Because of that, the United States tries to install democracies, right? But I
think most people know that it doesn't really care if it's a democracy. It's just another
tool. Because of that cultural prevalence, if you can get a democracy, you can get the
average people. If they get a voice of some kind, it's easier for the United States to
maintain control via economic power. But it's not a requirement that a country is a democracy.
Look at Saudi Arabia. It doesn't have to be, but it's one of those things that the US can
use if things go sideways in a country. Well, it's time to bring democracy to it and give
the people a voice, and hopefully that cultural power will win out. And then if all of this
fails, then you have a military that is incredibly strong because of the economic power. The
US maintains its empire differently, and it's huge. The number of countries that fall into
this category of being very reliant on the US for their economic well-being and their
defense is huge. It's just we don't change maps. The US doesn't do it that way. China
is developing their own. China does a lot of state-to-state deals. Like they show up
and they talk to country X and they're like, hey, we want to help make your country more
stable. We'll put in a port and we'll put in some railroads and, you know, that'll increase
economic activity. It'll raise the standard of living in your country, and hopefully it'll
make it more stable. And I think it will in the long term. They're going to have issues
along the way, but overall I think it's a pretty winning strategy. But is China doing
this out of the goodness of its heart, or is it trying to get those power coupons, trying
to get that money, right, by extracting natural resources, by taking its cut of the wealth
in that country? All of the infrastructure that they build, I mean, it all kind of lines
up with making sure that stuff can get to China, so China can process it and turn it
into something else, and then sell it to the rest of the world. It's all about power. I
know you say you watched this channel long enough to know what I'm saying. How many times
have you heard me say foreign policy isn't about right and wrong, good and evil. It's
about power. No major power is good. That's the hard part for people. This is what exists.
It's about power, and that thirst for power causes wars. I don't think that's good, and
I don't care what country does it. I would prefer 19-year-old kids not get shipped off.
And then that last part. If this is the way it's always been, do we just accept it? If
you and her are right, it seems impossible to hope for that better world. Everything's
impossible until it's not. I mean, the idea of decolonization was impossible, and then
it happened. Well, it changed, obviously, based on what we just talked about. That better
world is dependent on enough people understanding that it's broke. The international relations
the way international relations occur and have occurred isn't good. It has to start
there. And a big part of that is understanding that no major power is good. That no country
pursuing an elective war, as far as the way your professor said it, it's all bad. It has
to start there. Nationalism is politics for basic people. If you want that better world,
you have to get past that point. The good news is that it isn't impossible, because
the world is shrinking, because we can communicate with people all over the world instantly.
All of this, over time, will help shift that. It doesn't help right now, but that's where
we're headed. We're headed to the point where people understand that you and I and everybody
else watching this video, we have more in common with the soldiers, Russian or Ukrainian,
in Ukraine, than we have with our own representative in DC. It takes getting past all of that nationalism.
We have to start thinking broader than a border. We have to start thinking of the world. Then
international politics can change. But as long as we're focused on the imaginary lines
on the map that create our country, there's always going to be that tension. There's always
going to be that other, that external threat that can be harnessed to win elections, to
maintain power. We have to get beyond that. We have to realize that we have more in common
with each other, the average people of the world, than we do with those who say they
represent us in any system of government. That's how we get there, but it's going to
take a long time. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}