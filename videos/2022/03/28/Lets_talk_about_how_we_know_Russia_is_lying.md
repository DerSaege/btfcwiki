---
title: Let's talk about how we know Russia is lying....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F7xeddeyj3A) |
| Published | 2022/03/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia's goal was to take over the whole country of Ukraine, not just settle for a small area.
- He believes Russia's claim of not intending to take the capital is not truthful.
- Beau explains how Russia's actions in Ukraine clearly indicate an attempt to take over the entire country.
- He references a video he made on February 10th, accurately predicting Russia's military strategy in Ukraine before the war started.
- Beau describes how Ukraine's resistance and strategic moves have thwarted Russia's attempts to take over the country.
- Russia's failed attempts, including a decapitation strike and paratrooper insertion, support the fact that their goal was to take the capital.
- Russia's actions, giving Ukraine a month to prepare and then invading, are seen as a huge error in military strategy.
- Beau points out that Russia's failure to achieve its goals in Ukraine makes them look incompetent in military planning.
- Despite the conflict, Ukraine still maintains conventional forces and has even increased its tank numbers.
- Beau concludes that Russia's actions in Ukraine match the operational template he outlined in a previous video about taking over a whole country.

### Quotes

- "Russia's goal was to take over the whole country of Ukraine."
- "If this was a distraction, they're even worse generals."
- "The only surprising aspect is that Ukraine still has conventional forces."

### Oneliner

Beau explains how Russia's actions in Ukraine clearly indicate an attempt to take over the entire country, despite their claims otherwise, with Ukraine's strategic moves thwarting their plans.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Watch Beau's video from February 10th to understand the accurate prediction of Russia's military strategy in Ukraine (suggested).
- Study military strategy and operational aspects to better comprehend the ongoing conflict (implied).

### Whats missing in summary

Insights on the consequences of Russia's military actions in Ukraine and the implications for future conflicts.

### Tags

#Russia #Ukraine #MilitaryStrategy #ConflictAnalysis #Geopolitics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about how we know
Russia was trying to take the whole country.
Not think, not suspect.
We know that that's what their goal was.
You know, right now they're saying
that that's not what they wanted.
They never meant to take the capital.
They were just gonna settle for this little area.
And I have made no secret of the fact
that I think that that is them being
economical with the truth.
They've made that up.
I've had a lot of people say, well, how do you know that?
You can't possibly know that.
Yeah, yeah, no, I do know that.
The easiest way would be to explain
what it looks like when you're trying to take over a country,
how you would go about taking it over.
But that would require giving like a giant class
in military strategy.
But you could just trust me in the fact
that what they did is what it looks
like when you're trying to take over the whole country.
But it's easy for me to say that standing here today, right?
It'd be better if I had said it before the war,
like on February 10th in a video called
Let's Talk About the Futures of Ukraine and Russia.
In that video, I'm explicit about the fact
that at that time, we didn't know they were trying
to take the whole country.
But I described what it would look
like if they were trying to take the whole country.
That's the whole premise of that video
is describing what it would look like and the possible outcomes.
It looks like short version exactly what happened.
Said it would open with a strong air and artillery game, which
is what happened.
Didn't go so well because Russia didn't actually
secure superiority.
They're a near peer, not a peer.
From there in the video, I say that they would open up
with three lines of advance.
They opened up with four because they were way overconfident
and they paid for it.
I talk about how if Ukraine put up a small fight
and then fell back, that they might be able to stop them
during the advance.
And if they do that, they have a chance of winning.
Incidentally, that's exactly what happened.
They stopped them before they got to the capital.
And were able to start the occupation.
So that's why you're seeing the developments you are today.
I also talk about something that was presented to me
by another analyst, somebody who's really good.
And they're like, there's also the idea
that they could just try a decap strike
and try to take the capital from the air.
And in the video, I'm making fun of it.
I'm like, that's really unlikely.
Basically, because nobody's that bad,
the Russians proved us wrong.
That is, if you tried to take it from the air,
that's paratroopers.
And that's exactly what they tried.
And I made fun of it in the video
because that's just a good way to waste paratroopers.
And that's what happened.
The idea that they weren't trying to take the capital,
it's made up.
It's a lie.
It's them trying to make themselves feel better.
That was absolutely the goal.
If you go back to the video recorded
before the shooting started that describes
what it would look like, that's their exact operational
template, including the stuff that I thought
was just too ridiculous to actually do because they can't
secure the air.
They're not the US.
They don't have that air power.
And that's the other part of it.
They gave their opposition a month to prepare.
They knew it was coming.
And then they went in anyway.
Something else that was brought up in the video.
That's a bad move.
It's such a bad move that when you watch the video,
I'm doubtful that Putin would invade
because that's a huge error.
You don't do that.
They're not the US.
The US can do that.
The US can say, yeah, we're going
to invade on March 2nd at 4 PM.
And there aren't really any countries that can
stop the outcome from that.
Russia isn't the US.
Giving that kind of lead up, that kind of warning,
huge error.
And they're paying for it.
I'll put the video down below.
Go watch it.
Look at the date.
That's what it looks like.
If they wanted to just take certain regions,
they would have just moved into those regions.
They wouldn't run a distraction operation,
wasting millions upon millions of dollars in equipment
and thousands of lives.
If this was a distraction, they're even worse generals.
They're even worse planners.
Because if this was a distraction,
it just means they failed even harder.
To anybody who understands military strategy,
to anybody who understands the operational aspects of this,
saying that that was a distraction just
makes you look worse, not better.
It makes you look completely incompetent.
They tried.
They failed.
Because they gave Ukraine too much warning.
Because they counted on that toe-to-toe battle up front
that Ukraine didn't give them.
Ukraine pulled back.
And they did it well enough.
They fought deep.
And because of that, they actually still
have conventional forces, which that's
the only thing that's surprising.
The only thing that is surprising
about how this has turned out is that Ukraine still
has conventional forces.
They have more tanks now than when they started.
That's the only surprising aspect.
What Russia did is the exact template
that I outlined in that video.
Because that's what it looks like if you're trying
to take the whole country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}