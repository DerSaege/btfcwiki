---
title: Let's talk about possibilities of Ukraine winning conventionally....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hjMWa2rgGD0) |
| Published | 2022/03/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the possibility of Ukraine winning conventionally against Russia.
- Mentions that Western reporting lacks critical information about the situation.
- Points out Ukrainian forces making strategic moves to potentially encircle Russian troops.
- Describes the significance of Ukraine gaining the initiative by encircling Russian troops.
- Notes the importance of Russian logistics and morale in the conflict.
- Emphasizes the unpredictability and unlikelihood of Ukraine's potential conventional victory.
- Urges to monitor the northwestern side of the capital for possible developments.
- Indicates that Russian advance has halted, creating a stalemate.
- Stresses the potential for Ukraine to shift the initiative and put Russia on the defensive.
- Concludes by encouraging viewers to stay updated on the situation.

### Quotes

- "This is nothing short of a military miracle."
- "Once those lines start collapsing, the Russian morale is just going to drop like a rock."
- "They've got a really good chance of greatly altering the way those maps look."

### Oneliner

Beau explains Ukraine's potential to win conventionally against Russia due to strategic moves and shifts in initiative, marking a military miracle in progress.

### Audience

Global citizens, analysts

### On-the-ground actions from transcript

- Monitor and analyze developments on the northwestern side of the capital (suggested)
- Stay informed about the ongoing conflict and potential shifts in initiative (suggested)

### Whats missing in summary

In-depth analysis and historical context.

### Tags

#Ukraine #Russia #Conflict #MilitaryStrategy #WesternReporting


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about the possibility of Ukraine winning this thing conventionally. In a recent
video I said I thought Ukraine could win if they were given the tools and I had a
whole bunch of people send me messages saying name one way that could happen
because of Western reporting. The problem with a lot of newspapers is that you can
fill volumes with what you don't read in them. When it comes to the unconventional
side which would require Russia actually accomplishing its objectives and taking
these cities, the odds are heavily in favor of Ukraine. We've talked about that
at length on this channel. The odds lean to Ukraine once Russia completes the
conventional operation. But there's a possibility of Ukraine being able to
defeat them conventionally, which those are not words I would have said three
weeks ago, two weeks ago, a week ago. When you look at Western reporting you keep
hearing the encircled capital, the besieged capital. That capital is not
encircled. It's not even surrounded by a half circle. And over the last few days
Ukrainian forces have made some very interesting moves along the western side
of those lines. So on the northwest side of the capital there. They have made some
really interesting moves. Moves that could easily be mistaken for a coherent
strategy to encircle a portion of those troops, a portion of the Russian troops.
They pull that off. They take those encircled troops off the field one way
or another. Then they have the initiative. And it is likely that they
press it and attempt to roll the entire Russian line and they might be able to
do it. At that point Ukraine has the initiative. You know when we talked about
Mariupol we talked about how Ukrainians there were holding onto dirt that wasn't
strategically significant anymore because it was bottling up Russian
forces. It was keeping them tied up trying to take it. If the capital isn't
under imminent threat, there's a whole bunch of Ukrainian forces that suddenly
become free. A whole lot of them will go to press the attack along the Russian
forces in the north, but there will be some that will be freed up to go south,
which means those Russian troops near Mariupol will find themselves firing in
two different directions. Those lines will probably collapse as well, in which
case they can try to roll those. They can try to roll those too. There's a lot of
if this then that in this sentence, you know, in this scenario, but none of them,
none of these developments are out of the realm of possibility. Some of them
are super likely. Once those two cities are, once those lines are broke, it's,
Ukraine has all of the advantages at that point because you have to remember
Russian logistics is really bad. The lines are not great. This is something
that wasn't even something that could be considered a few weeks ago, but
now it can be. And understand, I don't know how to, I don't know how to stress
how wild this is. This is something that shouldn't have occurred by
any measure. This isn't something that should be in the cards. It's not
something that should be likely. This is nothing short of a military
miracle. But I would watch what goes on on the northwestern side of the
capital because if there is a massive reversal, it's going to probably start
there at time of filming. This video won't even go out until tomorrow, but that's
with the maps the way they are, with the lines the way they are, that seems
like something that's possible, especially with Russian morale being the
way it is. And once those lines start collapsing, the Russian morale is
just going to drop like a rock. And troops don't fight as well with low
morale. It could be something that really pushes the Russians back. And to be
clear, because I know somebody's going to say that none of this is a secret, if you
are looking at the maps, I don't know how they wouldn't see this coming. The
problem is that I don't think that Russia has the ability to stop that
encirclement right now. It would take them getting really lucky, and then from
there, it's all up to the troops on the ground. There's nothing that, from a
strategic level, there's nothing the generals are going to be able to do
about it. What happens is what's going to happen. But these are possibilities. That
scenario, it's not far-fetched, and it's something that just a couple weeks ago
wasn't even... nobody could think that would be even likely. But it's occurring.
This is the way it's shaping up. The Russian advance has stopped.
They're trying in some areas, trying to push forward, and it's a stalemate, and we
keep saying that, and that makes it sound like they're not actively involved in
fighting, but they are. They haven't stopped fighting, and they're not
moving, which means all it takes now is for Ukraine to catch a break, and then
the initiative shifts, and Russia's on the defensive. So as wild as it seems,
especially given the estimates from everybody, that's not out of the realm of
possibility that they can do it conventionally, which is... it's just wild.
I don't know how to say that any other way. So I would watch the northwestern
side of the capital and see what happens there, because at the time of filming, that is
the most likely spot for there to be a huge reversal, and for Ukraine to gain
the initiative on the battlefield. And if they do, and they press it, they've got a
really good chance of greatly altering the way those maps look. Anyway, it's just
a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}