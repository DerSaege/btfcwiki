---
title: Let's talk about a free library to satisfy your curiosity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EQk-h9xWmpM) |
| Published | 2022/03/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about people being curious and wanting to know more about different subjects.
- Mentions difficulty in finding reliable information on various topics.
- Provides a link to archive.org where hundreds of military field manuals and technical manuals are available for download.
- Points out that the US military is a self-contained society with manuals on various subjects like home improvement, plumbing, electrical work, and carpentry.
- Emphasizes that the texts available for download were developed using taxpayer money.
- Mentions that while most manuals are declassified, companies that reprint them usually focus on those of interest to survivalists.
- Notes that archive.org has a wide range of manuals available, including some from the 70s, with still relevant information on home improvement and auto repair.
- Encourages browsing through the titles even if not interested in war-related topics as they can be downloaded for free.
- Suggests downloading manuals for offline use in case of emergencies.
- Reminds viewers that they have already paid for these resources and should not hesitate to use them.

### Quotes

- "So don't hesitate to use it."
- "You paid for these texts to be developed."
- "It's a great resource, and it's free."

### Oneliner

Beau talks about the importance of curiosity, providing access to free military manuals on archive.org funded by taxpayers.

### Audience

Knowledge Seekers

### On-the-ground actions from transcript

- Visit archive.org and download military field manuals and technical manuals (suggested)
- Browse through the available titles on archive.org for various subjects (suggested)
- Print out manuals for offline use in case of emergencies (suggested)

### Whats missing in summary

The full transcript covers the importance of curiosity, access to free military manuals, and utilizing taxpayer-funded resources effectively. For a more in-depth understanding and additional tips, watching the full video is recommended.

### Tags

#Curiosity #InformationAccess #MilitaryManuals #TaxpayerFundedResources #FreeResources


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about people being curious.
Because I've had a whole bunch of questions come in since this giant mess started
that really boil down to curiosity and people wanting to know more about different subjects,
but having a hard time finding reliable information on those subjects.
Some of them are like, hey, you know, I know you say not to worry about it,
but I'm actually kind of worried about the unthinkable.
I'd like to know a little bit more about it.
Some people saying, hey, I want to know more about military tactics,
but you know, I don't want to fork out 40 bucks for each manual
and the library doesn't actually carry that stuff.
So I'm going to have a link down below that is going to take you to archive.org
and it has hundreds of military field manuals or technical manuals available for download.
Now if you don't want to know anything about war, still stay tuned,
because the US military is a self-contained society.
So if you're into home improvement, understand there's manuals on plumbing,
doing electrical work, carpentry, all of this stuff.
It's down there.
And remember, you paid for this.
You paid for these texts to be developed.
And they're on every subject you can possibly imagine.
Now the problem is most of these things are declassified.
All the ones on archive.org are declassified.
But you could get them.
You could get your hands on them.
But generally the companies that reprint these,
they only reprint those that are of interest to survivalists.
Those are the ones that get reprinted.
The ones available at archive.org, they have everything.
They have everything that they can get their hands on.
And it's information, some of it is dated, some of the field manuals are older,
they're from the 70s or whatever.
But the information is still good when you're talking about the home improvement stuff
or even just building structures.
There's all kinds of stuff, auto repair.
These are texts that if you were to buy them on the civilian market,
they would cost, I don't know, 200 bucks, some of them.
And you can just download them.
If you want to know more about war in particular, these are great references.
If you want to know more about the Russian military and their tactics,
look for the ones that say OPFOR.
It's going to have a lot of information about their stuff in those.
There's probably some stuff down there that's just like Soviet doctrine
or Russian military doctrine, stuff like that.
But to be honest, I wouldn't waste your time reading it
because they're not following it.
But even if you have no interest in any of that stuff whatsoever,
it still might be worth browsing through those titles
because you can download them to your phone, your Kindle, whatever.
You can print them out.
You know, if you are worried about the unthinkable,
maybe your phone doesn't work.
You can print them out and put them in a binder.
I don't know.
But it's a great resource, and it's free.
And you guys, you really did.
You already paid for all of this.
So don't hesitate to use it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}