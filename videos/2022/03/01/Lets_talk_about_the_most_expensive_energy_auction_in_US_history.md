---
title: Let's talk about the most expensive energy auction in US history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hIsbjuCE01k) |
| Published | 2022/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The largest, most expensive energy auction in U.S. history just happened, grossing $4.37 billion over three days involving six companies.
- This auction is how the U.S. government manages federal lands for energy production, allowing companies to bid for access to certain sites.
- Surprisingly, the auction wasn't for oil or natural gas but for wind power sites three miles off the coast of New Jersey and New York.
- This shift towards wind power signifies a significant step in addressing environmental and climate issues in the United States.
- Despite the positive impact on the environment, the focus for many Americans remains on the financial aspect, with $4.37 billion and thousands of jobs being key points of interest.
- Companies are willing to invest heavily in wind power because they see a profitable return on investment due to the abundance and free access to wind as an energy source.
- The lucrative nature of wind power will likely lead to a domino effect where more companies will invest, leading to increased campaign contributions and political interests in green energy.
- While it's unfortunate that profit drives environmental change, the significant investments in wind power are seen as a positive step towards a greener future.
- The scenario underscores the importance of financial interests in driving change, where companies prioritize returns on investments over environmental concerns.
- Beau concludes by noting the necessity of commodifying environmental efforts to ensure efficient and rapid progress towards widespread renewable energy usage.

### Quotes

- "If it doesn't make money, it's not worth anything."
- "It shouldn't take this to get change, but when companies are willing to drop this much money, they're going to make it work."
- "It's sad that it takes commodifying everything to get anything good done."
- "They're not going to be environmentalists; they're going to care about the return on their investment."
- "Once they make their money here, they're going to want what every other company wants, more."

### Oneliner

The U.S. witnesses a record-breaking $4.37 billion wind power auction, signaling a significant shift towards addressing environmental concerns through profitable investments.

### Audience

Environment enthusiasts, policymakers

### On-the-ground actions from transcript

- Advocate for policies that support renewable energy initiatives (implied)
- Support companies investing in green energy by choosing sustainable options (implied)
- Stay informed and engaged in environmental issues to drive positive change (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the recent record-breaking wind power auction in the U.S. and its implications for addressing environmental and climate issues through profitable investments in renewable energy sources.

### Tags

#RenewableEnergy #EnvironmentalIssues #WindPower #ClimateChange #Investments


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the largest, most expensive, highest grossing, let's use
that term, the highest grossing energy auction in U.S. history.
Just happened.
It just happened.
It was a three day auction, six companies, and it grossed $4.37 billion.
If you don't know, this is how the U.S. government manages federal lands, areas it controls.
This is how it kind of divvies stuff up for energy production.
Companies are allowed to bid to get access, to get the rights to use certain sites.
And a big one just happened.
$4.37 billion.
That is wild.
What do you think it was for?
What kind of energy?
Oil?
With that price tag?
Biggest in history?
It's got to be oil, right?
It's not natural gas either.
I'll give you a hint.
The sites are three miles off the coast of New Jersey and New York.
It's wind power.
Wind power.
$4.37 billion.
Now see, the news that should matter here is that the United States is finally starting
to take giant strides to address pressing issues dealing with the environment and the
climate.
These sites, I want to say it's 488,000 acres combined.
That should be the news.
That should be what people care about.
What will people care about $4.37 billion and the thousands of jobs it will create?
That price tag, I think that may be the thing that wakes a lot of people up.
Because for a whole lot of people in the United States, a country that worships the almighty
dollar, if it doesn't make money, it's not worth anything.
It doesn't count if you can't slap a price tag on it.
And they're going to have to wonder why companies are willing to do this.
Why they're willing to pay this much to get access to this.
Because they know it will work.
They know they're going to get the return on their investment.
And they know it's going to be a big one.
Because basically you have the rights to use the area, the cost of putting the things up,
and then what?
The wind's free.
You don't have to put it in barrels.
You don't have to move it.
The wind is free.
So they're going to make their money.
Now what is likely to happen, and it is sad that this is the way it works, but it's probably
how it's going to work.
The companies that got these sites, they're going to make a fortune.
And when that happens, well then other companies are going to realize that it's a way to make
money.
And they do that, those dollars, then they translate into campaign contributions.
And you will find a bunch of politicians who were denying climate change suddenly very
interested in green energy.
That's how it works.
For those of us who want a better future, or a future at all, depending on how bad things
get, this is fantastic news.
The fact that companies were willing to spend this much to get access, this is good news.
It shouldn't take this to get change, but when companies are willing to drop this much
money, they're going to make it work.
They're going to make it work, they're going to make it profitable, and then they're going
to make sure that their business interests are protected.
They're going to get politicians in their pocket.
You know, it's always been that joke for a long time, if only the environment had lobbyists
that had the funding that the oil companies do.
They're not going to be environmentalists.
They're not going to be people who care about the environment, not necessarily.
They're going to care about the return on their investment.
And if they're willing to drop four billion dollars for the rights, they're expecting
a huge return, which means their lobbyists are going to have a lot of money.
So it's sad that it takes commodifying everything to get anything good done, but this is probably
going to be the most effective way, the most efficient way, the fastest way to get something
done like this, to get renewable greener energy in widespread use.
Because once they make their money here, they're going to want what every other company wants,
more.
Anyway, it's just a thought. So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}