---
title: Let's talk about a Russian article from the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WWIYfKWSAxw) |
| Published | 2022/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of understanding intent in foreign policy and how countries don't openly reveal their intentions.
- Mentions a leaked article from Russian state media discussing their victory in Ukraine.
- Indicates that the article was likely pre-written and accidentally published ahead of schedule.
- Describes the content of the article, which boasts about Russia's actions and goals in Ukraine.
- Explains that the ultimate goal is to bring Belarus and Ukraine under Moscow's direct control to elevate Russia's power.
- Criticizes the imperialistic nature of Russia's actions and Putin's desire for a more powerful Russia.
- Suggests that Putin sees this as his legacy and Russian soldiers may be unaware of the true motives behind the invasion.
- Encourages reading the leaked article for insights into Kremlin's messaging and Putin's mindset.
- Points out the racist undertones in the article, aimed at stoking ethnic divisions in the West.
- Condemns the false justifications put forward by Putin and urges those who defended him to reconsider.

### Quotes

- "Countries don't announce what they want to do. They come up with pretexts to explain their actions."
- "It's imperialism, nothing more."
- "For those who have been going to bat for Putin on this, you need to read it the most."
- "It's about power. It's about some old guy wanting more power, his name in a history book."
- "It's always the same thing."

### Oneliner

Understanding the leaked article reveals Russia's imperialistic ambitions in Ukraine, driven by Putin's quest for power and legacy, shedding light on the deceptive nature of foreign policy.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Read the leaked article to understand Kremlin's messaging and Putin's goals (suggested)
- Challenge false justifications and narratives surrounding Russia's actions (implied)

### Whats missing in summary

Deep dive into the deceptive nature of foreign policy and the manipulation of public perception.

### Tags

#ForeignPolicy #Russia #Putin #Imperialism #Kremlin


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about an article
from the future, an article you're not
supposed to have read yet.
We talk about foreign policy on this channel a lot.
And we talk about making estimates, assessments,
predicting what the other side is going to do,
whoever the other side is.
And when we talk about it, I often talk about intent,
because that's the key.
Countries don't announce what they want to do.
They come up with pretexts to explain their actions.
And then if those actions are successful,
then they let you know.
They don't tell you ahead of time what they really want,
because they're at that international poker game,
where everybody's cheating.
Knowing the intent makes your estimates, your predictions,
a whole lot more accurate because, well, I mean,
it's easier to figure out what moves somebody is going to make
if you know what they're trying to do.
The thing is that, generally speaking, countries,
you know, they don't tell you that.
It's not like they go out and publish a public relations
piece explaining their intent. So Russian state media accidentally published an
article talking about their victory in Ukraine. It was obviously not supposed to
be published when it was. It is interesting to note that its time of
publication would have been right after Russia installed the new government in
Ukraine if everything had gone according to plan. So the best guess is that this
article was pre-written, well I mean that part's obvious, but it was probably
written before the operation even started and it was scheduled to go out
once they won because they were very overconfident and when things didn't go
that way, nobody changed the schedule for it to be released. So it got released. It
wasn't out long, but it was out long enough to get archived. I will have the
links down below. So what's it say? It gloats. It gloats. It talks about their
intent. It says exactly why they're doing what they're doing now, because when this
article, when this article was supposed to be published, they were already
successful. So they're broadcasting, they're messaging to the world
about what they had accomplished. Those pretexts, all that stuff that they
brought up as justification for this, it's not in there because it's not what
it's about. It was never about that. It never is. It's always about one thing and
one thing only, power. The article explains that the goal of this operation
is to bring Belarus and Ukraine under direct control of Moscow. The governments
there, they're supposed to be puppets, nothing more. It talks about acting as one
on the geopolitical stage. It also talks about how the West is is is stupid, which
is funny given everything that's going on with us. So the goal here is to enact
Putin's dream of making a more powerful Russia by reasserting claims over, I
I think he called them ancient lands or something like that, that newfound block, Belarus, Ukraine
and Russia, that would form the greater Russian world.
And it would challenge the West and it would try to elevate Russia from being a near pier
to being a peer again, to being a superpower. That's the goal. It's imperialism, nothing
more. And for those that have a problem with me defining it that way, let's be clear. One
country invaded another, not for any of the reasons that it gave, not for any of the pretexts.
It invaded another to take control of that country's government and make it subordinate
to the invading country.
It's imperialism.
That's what this was about, power.
Once they have that, they're able to project more power.
What does this change on the ground for Ukraine?
Not much.
Other than the knowledge that Putin may not be flexible on this.
To him, this is his legacy.
This is what he is leaving the world, a more powerful Russia.
It may mean something to the Russian soldiers though, because as happens a lot, they're
They're on a lie.
They're there to satisfy the ambitions of some Dadushka, some old man who has dreams
of being in a history book.
This little propaganda package that went out, it wasn't supposed to be there.
never supposed to see this until after it was done. It's worth the time to
read it because you get good insight into the messaging that the Kremlin
wanted to push out to the world. You get good insight into Putin's mind and to
what their goals are. It's also really racist too, by the way, which is... I honestly didn't
see that coming, but there is a whole lot of attempts at stoking ethnic divisions within
the West buried in it. Because again, it's gloating. The West was powerless to stop Russia
from taking over Ukraine. It only took four days to do it. I'll have it below. It's definitely
worth a read. And this definitely sums up that whole, your propaganda is bad and you
should feel bad thing. For those who have been going to bat for Putin on this, you need
to read it the most. All of the justifications, everything that he put
out to the world, all of the messaging, was all false. It's not what it's about.
And if you bought it, don't feel bad. He had a whole bunch of foreign policy
experts that bought it, had a whole bunch of American politicians on TV parroting
those points because they thought that's what it was about too. At the end of the
day it is always about the same thing. It's about power. It's about some old
guy wanting more power, his name in a history book, some legacy to leave behind.
And because of that there's a bunch of 19 year old conscripts out there. It's
always the same thing. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}