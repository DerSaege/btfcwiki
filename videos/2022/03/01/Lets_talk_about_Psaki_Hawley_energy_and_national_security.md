---
title: Let's talk about Psaki, Hawley, energy, and national security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HEKAnEfpr64) |
| Published | 2022/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing statements made by Senator Hawley and Press Secretary Psaki regarding national security and energy.
- Senator Hawley criticizes President Biden for shutting down American energy production and greenlighting Russian energy production.
- Beau fact-checks Senator Hawley's claim by pointing out that the US has increased natural gas and oil production under Biden.
- Beau explains that Biden's focus on renewables actually leads to more energy production, not less.
- Beau suggests that Senator Hawley may be referring to Biden's desire to stop federal leases, which was halted by a court order.
- Press Secretary Psaki argues that national security is tied to transitioning to green, independent energy sources produced within the country.
- Beau supports Psaki's statement by citing the Department of Defense's emphasis on the importance of green energy for national security.
- Beau warns that failure to switch to cleaner energy sources could lead to conflicts over energy resources in the future.

### Quotes

- "If you are against protecting the environment, if you are against green energy, you are also against American national security, period, full stop."
- "The Department of Defense tends to know a little bit about national security."
- "Wars will be fought over this."

### Oneliner

Beau fact-checks Senator Hawley's claims on energy production, while supporting Psaki's argument that green energy is vital for national security.

### Audience

Internet viewers

### On-the-ground actions from transcript

- Contact your representatives to support policies that prioritize green energy and national security (implied)

### Whats missing in summary

The detailed breakdown and analysis provided by Beau in the full transcript.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about Senator Hawley
and Press Secretary Psaki and national security and energy.
Because we had two people make a couple of statements
and we're just gonna kind of fact check them,
see if there's accuracy, see who is,
well, telling the American people the truth.
So we'll start with Senator Hawley here.
Now he's talking about President Biden when he says this.
And he says, he comes to office and what does he do?
He shuts down American energy production
and green lights Russian energy production.
Is it any wonder that Vladimir Putin feels emboldened
to do whatever he wants to do?
That is quite the statement.
And by that, I mean, it has words in it.
Is it accurate?
In the context of the current situation in Ukraine,
you would have to kind of assume
it's about natural gas, right?
But see, that doesn't make any sense.
Number one, the federal government, Joe Biden,
only controls like 13% of American natural gas production
via federal leases on federal land.
So, I mean, that seems odd.
Aside from that, in 2021,
the United States produced more natural gas
than it did in 2020 under Trump.
And it's slated to produce more in 2022
and even more in 2023.
So it can't be about that.
So what about oil production?
I had to look this up, I didn't know this.
It's pretty much the same thing.
It's flapped from 2020 to 2021
and slated to go up in 2022 and 2023.
Weird, because that's more energy production.
It's really hard to shut American energy production down
when you're creating more of it.
And then on top of this,
Biden has put out all the investment
and all of the plans for windmills
and all of the renewable stuff,
which would be more energy production,
not shutting it down.
See, my guess is that Holly's talking about Biden's desire
to stop federal leases.
And that would make sense,
except for the fact that there was a court order
stopping him from doing what he wanted to do.
So none of that ever happened.
So the end result is not just do we have the shift
to get renewables,
we still have all the fossil fuel stuff going on as well,
which again is more energy production, not less.
So that's just not true.
That's just not a true statement.
It's made up, it's not reality.
So what'd the press secretary say?
She kind of indicated that national security
depends on going green, new independent energy,
energy that can be produced within the country,
renewable stuff, doesn't rely on other countries.
Does that make sense?
Yes.
And who's that backed up by?
The Department of Defense.
They tend to know a little bit about national security.
This isn't me trying to cover for Biden.
I have been screaming about this on this channel for years.
The army has paper after paper after paper.
If we do not get a handle on the environment,
if we do not switch to cleaner energy
that can be produced at home, there will be wars over it.
If you are against protecting the environment,
if you are against green energy,
you are also against American national security,
period, full stop.
This isn't new.
These papers, they've been around a while.
It's not a liberal plot.
It's DOD saying, hey, if we don't fix this,
we're gonna be in trouble.
Wars will be fought over this.
They've been saying it for a while.
The defense agencies in Europe have been saying it too.
Maybe take a look at Europe.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}