---
title: Let's talk about Hannity, Fox, and ethics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ggQht-I2AZo) |
| Published | 2022/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions journalistic ethics regarding Sean Hannity and his advising of the president.
- Points out the lack of accountability for Hannity's unethical behavior.
- Expresses confusion over why Fox News isn't holding Hannity accountable.
- States that Hannity is not a journalist but a commentator.
- Views Fox News as a propaganda machine rather than a journalistic outlet.
- Mentions the importance of personal ethics in disclosing biases.
- Suggests Hannity may face consequences for not disclosing information to viewers.
- Explains why he isn't leading a charge against Hannity for violating journalistic ethics.
- Comments on the misleading nature of Fox News using "news" in its name.
- Emphasizes the danger of government regulation on who can be considered a journalist.

### Quotes

- "Why isn't Sean Hannity being held to account for his unethical behavior?"
- "Because he's not a journalist."
- "Fox News is not a journalistic outlet. It's a propaganda machine."
- "Nobody's expecting him to be held to journalistic ethics because he's not a journalist."
- "Why isn't anybody riding that horse? Because it's a chicken."

### Oneliner

Beau questions Sean Hannity's journalistic ethics, labels Fox News as a propaganda machine, and explains why Hannity isn't held to journalistic standards.

### Audience

Media consumers

### On-the-ground actions from transcript

- Challenge media outlets promoting propaganda (implied)
- Advocate for ethical journalism standards (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of journalistic ethics, accountability in media, and the distinction between journalism and commentary in news outlets.

### Tags

#JournalisticEthics #SeanHannity #FoxNews #PropagandaMachine #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk a little bit about Sean Hannity and journalistic ethics,
because I kind of question, and judging by conversations that are occurring on social media,
a lot of people are looking at it through this lens.
And I think it's a case of missing the obvious.
So we're just kind of going to roll through it real quick.
Okay, so the message.
Explain something to me.
Why isn't Sean Hannity being held to account for his unethical behavior?
Isn't advising the president a violation of journalistic ethics?
And why aren't you leading the charge on this?
You put boring, boring disclosure notices on videos about wolves,
and he said nothing about a coup.
Am I missing something here?
Why is nobody demanding Fox holds him accountable for violating these ethics?
I get it. I get it.
He's on Fox News, and on the other end of the spectrum,
you just had somebody on another outlet get fired for advising a family member.
Seems like he should be held to that same standard, right?
I get it.
But what's the obvious answer here?
Why am I not leading a charge to hold Sean Hannity accountable
for violating journalistic ethics?
Because he's not a journalist.
He's not a journalist.
And in my estimation, Fox News is not a journalistic outlet.
I don't expect them to abide by those ethics.
It's really that simple.
It's the same reason I'm not leading a charge to hold him accountable
for violating the Hippocratic Oath.
He's not a doctor.
He's not a journalist.
And that's not necessarily an insult.
He is one of the few that has actually been pretty open,
and he himself has said, I'm not a journalist.
I think at other times he's said that he's an advocacy journalist
or an opinion journalist, something like that.
He's a commentator.
He's not a journalist.
That outlet, I don't view it as a journalistic outlet.
It's a propaganda machine.
It's all it is.
I don't expect them to behave ethically.
And there's nothing wrong with him just being a commentator either.
I would point out, nothing inherently wrong with it,
I'd point out that I haven't done anything I would consider real journalism
since before the pandemic.
Yeah, I put disclosure notices when somebody asks for factual information,
like in the case with the wolves.
Somebody was asking for factual information about something,
and that is a cause that I'm involved in.
So I felt the need to say, hey, I'm biased on this,
but I'll try to answer as best I can.
I don't know that I did that out of some professional obligation, though.
I think that was more personal ethics.
I don't foresee Sean Hannity being held to account in any way
for violating journalistic ethics because he's not a journalist,
and he doesn't work for an outlet that really seems to pride itself
on abiding by those ethics.
I'm willing to bet he is probably going to get in more trouble
over not disclosing what he knew to his viewers,
not out of an ethical sense, but because that's millions that Fox lost.
Why he didn't frame a show around what he knew,
that's probably the question his bosses are asking.
Not, they're not asking about ethical concerns.
I'm willing to bet the people that are looking at it through this lens,
if somebody walked up to them and said,
Fox News is my trusted source of journalism,
they would laugh and say, it's not journalism.
That's why.
People are overlooking the obvious here.
Yeah, they have news in their name.
That's a, I don't think that that's truth in advertising,
but I also don't think it would be a good idea for the government
to regulate who can say, who can use the word news,
because that gets real close to the government deciding
who's a journalist and who's not.
And that's dangerous. That's bad.
I think there's your, there's your answer.
Nobody's expecting him to be held to journalistic ethics
because he's not a journalist.
Why isn't anybody riding that horse? Because it's a chicken.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}