---
title: Let's talk about Ted Cruz, Russia, and pipelines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K23p-vjVMRo) |
| Published | 2022/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Ted Cruz has legislation in the Senate aiming to force sanctions on a pipeline carrying natural gas from Russia to Germany, worth billions of dollars.
- Democrats may be changing their stance on supporting sanctions, preferring to leave it to the Biden administration due to diplomatic engagements with Russia.
- Cruz is frustrated by this change and wants to impose sanctions immediately to utilize leverage.
- Beau warns against expending leverage too early, drawing parallels to issues faced in Afghanistan due to premature actions.
- Cruz initially secured a vote on his legislation by releasing holds on ambassador appointments, contradicting his supposed concern for American foreign policy.
- Beau suggests that imposing sanctions could have significant implications on peace and potential invasion in Ukraine.
- He advises Cruz to step back and let the State Department handle foreign policy decisions, considering the current delicate global situation.
- The decision on imposing sanctions holds critical importance as the Biden administration navigates challenges with near-peer competitors.

### Quotes

- "Expend that leverage."
- "Removing that leverage is a national security issue."
- "It provides the Biden administration with more leverage."

### Oneliner

Senator Cruz pushes for pipeline sanctions, risking foreign policy leverage, while Democrats reconsider, potentially impacting national security and peace.

### Audience

Policymakers

### On-the-ground actions from transcript

- Contact your representatives to voice support or opposition to imposing sanctions (implied).
- Stay informed and engaged in foreign policy decisions impacting national security (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of imposing sanctions on a pipeline from Russia to Germany, urging caution and strategic decision-making in foreign policy.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about pipelines
and Senator Ted Cruz and deals in Congress
and Russia and Germany and foreign policy
and all kinds of stuff,
because Senator Cruz is like Superman right now.
Senator Cruz has legislation in the Senate
and it's probably coming up for a vote next week.
And the purpose of the legislation
is to force sanctions on a pipeline
that is coming out of Russia and into Germany,
carrying natural gas,
that is worth billions upon billions of dollars.
It's a big deal.
Cruz would like to put sanctions on this,
and he's a little upset right now because Democrats,
well, they may be changing their tune.
Maybe they don't want to force the sanctions.
Maybe they want to leave that up to the Biden administration,
given the fact that he is now engaging in diplomacy with Russia
and that provides him some leverage.
In the past, perhaps under an administration like Trump's
that didn't actually plan on countering Russia,
just caving to it,
the Democrats supported the idea of forcing the sanctions.
The situation has changed.
Cruz is upset by this,
and he wants to put the sanctions out now,
expend that leverage.
See, the thing is, this kind of thinking,
this grandstanding at home,
while there are big foreign policy things going on,
this is kind of what led to the problems we had in Afghanistan.
If you go back and watch the videos from that period,
you'll see that Trump's negotiators,
they didn't have a chance.
Even if they were capable of pulling off a deal,
they couldn't because he kept messing it up,
because he kept saying back here
and just ranting about how he wanted to leave Afghanistan.
The leverage that his negotiators had that we would stay,
he expended it, he used it.
So therefore, the opposition in that country,
they knew all they had to do was wait,
and while they were waiting, they could, I don't know,
move troops around the country, get them into the Capitol,
and have them ready for the eventual withdrawal,
which is what happened.
Ted Cruz is trying to make the same mistake again,
expending the leverage too early.
And it's funny because if you really want to be honest about it,
why is he so angry?
How did he get this legislation up for a vote?
See, Senator Cruz had a bunch of holds on ambassadors
Biden appointments, right?
He had holds on them, you know,
as he pretends he really cares about American foreign policy.
Please remember he's holding up ambassador appointment anyway.
Well, in order to get this vote, he released his holds on them.
He used that leverage, right?
And now Democrats are kind of shifting their position.
What are you going to do about it, Senator?
You already used the leverage, right?
You have to find something else now.
I'm sure there are more tools in the toolbox, but that,
well, that's done.
You already used it.
You could probably learn something from that
and apply that to foreign policy.
If you apply the sanctions,
you can't say that you're going to apply the sanctions.
And right now, with everything going on,
as you rant about the national security of the United States
and our allies, please understand
that those sanctions might be the difference between peace
and the invasion of Ukraine.
We have a decent State Department right now.
Maybe Senator Cruz should go on vacation,
go to Cancun, take a break, chill out, you know,
and let State Department do their job.
Whether or not those sanctions get imposed,
that's a foreign policy decision.
And right now, as the Biden administration
is moving into two near-peer contests,
they need all the leverage they can get.
Removing that leverage is, dare I say it,
a national security issue.
It would make sense for Democrats to vote against this
because it provides the Biden administration,
who actually is trying to counter Russia, with more leverage.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}