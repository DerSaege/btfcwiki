---
title: Let's talk about Trump going all in this weekend....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YrpIisESY5k) |
| Published | 2022/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump has broken from his typical playbook by indicating he might grant pardons to those involved in the January 6th actions if he were to be re-elected.
- Trump's recent actions, including hinting at potential pardons and suggesting he hopes for nationwide protests if criminal charges are brought against him, are attempts to cast investigations into him as political rather than legal.
- Despite indications from Trump, there is bipartisan support to update the Electoral Act of 1887, not change election results.
- Trump's statements about Vice President Mike Pence's role in the election results read like a confession to some, as he suggests Pence had the power to overturn the election.
- Trump's deviation from the typical playbook of wannabe dictators, by openly admitting to wanting to change election results, indicates potential criminal liability and a slipping standing with his base.

### Quotes

- "If you know there's a con man in the room but you don't know who the mark is, you're the mark."
- "Trump is not a planner, so when he's not following somebody else's plan, he's probably really likely to make some mistakes."
- "He may even incriminate himself when it comes to some of the things that he might have criminal liability for."

### Oneliner

Former President Trump breaks from playbook, hinting at pardons and admitting desire to change election results, potentially indicating criminal liability and slipping base support.

### Audience

Political observers, concerned citizens

### On-the-ground actions from transcript

- Contact representatives to support updating the Electoral Act of 1887 (suggested)
- Stay informed about political developments and hold leaders accountable (exemplified)

### Whats missing in summary

Analysis of potential future implications and consequences of Trump's deviation from the typical playbook.

### Tags

#FormerPresidentTrump #ElectionResults #Pardons #CriminalLiability #PoliticalStrategy


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
former President Trump's weekend because he's gone all in. Three separate things that we
kind of need to talk about and he's breaking from the pattern. He's breaking from the playbook
and that's kind of a good thing. We'll get to that. Okay, so the first thing he did was
he indicated he might be willing to grant pardons to those who participated in the actions
of the 6th if he was elected to office again. I do want to point something out. If Trump
wanted to pardon them, he could have done it on the 7th or the 8th or the 9th or the
10th or the 11th or the 12th. He had no intention of doing it. It wasn't politically advantageous
for him to do it, so he didn't. He doesn't actually care about those people. You know,
there's that old saying, if you know there's a con man in the room but you don't know who
the mark is, you're the mark. If you believe this, you're the mark. He has no intention
of doing it. It's politically expedient for him to say he might. It also helps with the
other thing, one of the other things that he did. He has kind of indicated that he hopes
that if criminal charges are brought against him, that there are nationwide protests. He
is attempting to cast the various investigations into him as political. This is the same thing
that he did with the election. Before the results were known, he started kind of pushing
the idea that, well, maybe there's fraud, you know, with no evidence. It's what he's
going to do here. He's going to try to get the perception built that this is all political,
that it's a political prosecution rather than a prosecution because he may have attempted
a self-coup. The dangle of the pardons is probably an attempt to encourage those protests.
It's kind of maybe seen as encouraging a wilder response. I would just point out that he didn't
pardon those people. Point that out again. And then after this, the big one, this little
press release, you know, he can't be on Twitter, so he issues little statements that then get
tweeted out in image form by other people. This says, if the vice president, Mike Pence,
as if we didn't know who he was, had absolutely no right to change the presidential election
results in the Senate, despite fraud that there has still been no evidence of, and many
other irregularities that there's been no evidence of, how come the Democrats and Rhino
Republicans like wacky Susan Collins are desperately trying to pass legislation that will not allow
the vice president to change the results of the election? Actually, what they are saying
is that Mike Pence did have the right to change the outcome, and they now want to take that
right away. Unfortunately, he didn't exercise that power. He could have overturned the election.
That's mask off, isn't it? Now what he's talking about is there is bipartisan support to update
the language in the Electoral Act of 1887. I have a video on it. And said that he was
going to make this claim like three weeks ago. They are not changing it. They're updating
the language in it. Basically, the law is old. It was written in 1887. What they're
doing is putting a warning label on it. You know, that you buy a blender and it has that
warning label, do not put your hand in blender when it's on, and you wonder who that's for.
It's for Trump. The law is very clear. The vice president is there to open envelopes.
It's a ceremonial gig. He doesn't have the power. She doesn't have the power to overturn
the election. Now, some things to note in here. Change the presidential election results.
He's admitting that Biden won. Change the results of the election. Overturn the election.
Many people have commented that this reads like a confession, and I have to admit it
kind of does to me. And that's where he's deviating from the plan. That's the playbook
that exists for this sort of person is to continue to make the claims, even after they've
been proven false, because their core base would believe it. But by admitting that the
desire was to change the results, he's deviating. The claim that they're just closing a loophole
for something that existed, that's part of the plan. That's part of the playbook that
wannabe dictators have used throughout history. But admitting that it was an attempt to undermine
the current system of government, that's new. Normally, they deny that and claim that it
was stolen. Now, you can read this and say that, well, maybe he just meant that's how
the other side is framing it. And that's probably how he'll try to spin this. But a plain reading
of it, to me, seems like he is saying multiple times that he wanted Pence to change the outcome
of the election, to overturn the election. I think that he's starting to realize, A,
he's probably got some criminal liability, that there is a possibility that he could
be held criminally liable for some of these actions. Lawyers have probably informed him
of that, so he's a little nervous about that. He also sees his stature, his standing with
the base slipping, as people who once used to use him to get ahead are now more popular
than him. I think we may see Trump become more and more unhinged. And the further down
that route he goes, the more likely he is to make a lot of mistakes and deviate from
the historical playbook for people like him. That's actually good news. It's going to present
a lot of uncomfortable moments when he says stuff like he would pardon the people from
the 6th or encouraging nationwide protests, that kind of stuff. But the deviation from
the normal playbook for wannabe dictators is good. Trump is not a planner, so when he's
not following somebody else's plan, he's probably really likely to make some mistakes. So while
I would expect to hear more things from him that are very uncomfortable to hear, I'm going
to say it's a good thing. I'm going to say that a lot of those people who may be on the
edge of leaving his base, that this kind of rhetoric and this open admission, these statements
and deviating from that plan, that may be the thing that pushes them out, pushes them
out of his base and kind of breaks the sway that he's held over them. And in his desperate
attempt to cling to that base, the tighter he squeezes, the more he deviates from that
historical playbook, the more of him he's going to lose. So again, I would expect a
lot of rhetoric from him. I would expect to hear some uncomfortable things, but I think
it's good news that we're going to start hearing it because he's going to make mistakes. He's
going to drive his base away. He may even incriminate himself when it comes to some
of the things that he might have criminal liability for. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}