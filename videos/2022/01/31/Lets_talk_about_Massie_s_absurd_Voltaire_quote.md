---
title: Let's talk about Massie's absurd Voltaire quote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xfQdI6ZXBL8) |
| Published | 2022/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Thomas Massey of Kentucky mistakenly tweeted a misattributed quote that has been widely circulated.
- The misattributed quote implied that criticizing a certain individual indicates who is ruling over you.
- Massey's attempt to criticize Fauci using this quote doesn't hold up logically because he openly criticizes Fauci, like many others.
- Beau points out that the true way to identify those trying to rule over you is by recognizing those who spread fear, blame, powerlessness, and victimhood.
- Beau suggests looking out for those who try to make you scapegoat others and direct your anger towards them as a means of control.
- The quote wrongly attributed to Voltaire about criticizing rulers doesn't make sense in the context of identifying true rulers.
- Beau stresses the importance of not falling for absurd rhetoric that leads to atrocities and the manipulation of fear to gain control.
- Beau concludes by encouraging critical thinking and awareness in recognizing true attempts at ruling over individuals.
- The core message revolves around identifying manipulative tactics of fear and blame in order to understand who is truly attempting to rule over you.

### Quotes

- "Those who can make you believe absurdities can make you commit atrocities."
- "Watch for people who are trying to make you feel like a victim when you're not."
- "Watch for those who are trying to scapegoat individuals or groups of people."
- "That's how you find out who is trying to rule over you."
- "Y'all have a good day."

### Oneliner

Beau explains the misattributed quote by Massey, urging to look out for those manipulating fear and blame to identify true rulers.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Identify and challenge fear-mongering tactics in your community (implied).
- Refrain from falling into scapegoating behaviors and encourage others to do the same (implied).

### Whats missing in summary

The full transcript provides a comprehensive breakdown of the misattributed quote by Representative Massey and offers insight into the manipulation tactics used by those attempting to rule over individuals.

### Tags

#MisattributedQuote #ManipulationTactics #CriticalThinking #FearAndBlame #CommunityLeadership


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Representative Thomas Massey of Kentucky
and his quote that he got wrong.
It wasn't a quote. It was misattributed, and we're going to talk about that.
That's what everybody's focusing on, the fact that he tweeted out this misattributed quote.
I think there's something more important to talk about, but we'll go through the whole thing.
If you have no idea what I'm talking about,
Massey tweeted out an image that had a quote on it.
The quote was something to the effect of,
if you want to know who rules over you, figure out who you can't criticize, da da da.
And it was attributed to Voltaire.
As has been reported all over the media, that is not from Voltaire.
That is from somebody who we hold in less regard,
somebody who it is widely seen as a generally uncool quote.
And again, that's what people are looking at.
He got that wrong.
He tweeted out this quote by this person, hopefully by accident.
The thing is, I can kind of give him a pass on that, on getting the attribution wrong.
Because one, I have seen that quote misattributed for years.
And two, I don't look or listen to Massey and think,
hey, this guy is like really schooled in classic philosophy.
I can see him getting it wrong.
I don't think that that should be the story.
Let's talk about the quote and what he was trying to do.
The context when he tweeted this thing out was he was criticizing Fauci
and trying to suggest that Fauci ruled over us.
However, if the quote was accurate, well, Massey wouldn't be able to criticize him.
It doesn't make sense.
It's one of those double think things that never adds up
when you're talking about this kind of rhetoric.
He's openly criticizing Fauci.
There are entire websites dedicated to criticizing Fauci.
But by using this quote, the intent was to suggest that you can't do that.
If you want to know who is trying to rule over you,
don't try to figure out who you can't criticize.
That's not how you do it.
Because think about it like this.
Right now, if you came out and trashed Betty White,
I'm pretty sure you'd get canceled, right?
Can't criticize her.
Did she rule over us?
The quote's garbage.
Always has been.
It doesn't add up.
It doesn't make any sense.
That's not how you figure out who is ruling over you,
who is attempting to rule over you.
You want to figure out who is attempting to rule over you,
look for those people who are trying to scare you and give you somebody to blame.
Somebody who's trying to make you feel powerless and say,
those people or that person, they're the ones who are to blame for it.
Those people trying to make you feel like a victim and scare you.
And use that fear and direct it at somebody so they can have control.
That's who's trying to rule over you.
That's how you figure it out.
The premise that Massey is suggesting is absurd.
That's not how it works.
Those who can make you believe absurdities can make you commit atrocities.
That actually is volterer.
Watch for people who are trying to make you feel like a victim when you're not.
Watch for those who are trying to scapegoat individuals or groups of people
who are trying to other them in a way that directs your anger
and places them as your leader.
That's how you find out who is trying to rule over you.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}