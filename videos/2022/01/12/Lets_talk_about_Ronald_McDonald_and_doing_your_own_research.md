---
title: Let's talk about Ronald McDonald and doing your own research....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1sMZXZHoNmE) |
| Published | 2022/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ronald McDonald House in Canada requires vaccinations for stay.
- Some in anti-science community criticize the decision.
- Beau questions those opposing vaccination requirement.
- Ronald McDonald House provides accommodations for immunocompromised individuals.
- Beau criticizes those prioritizing personal beliefs over public health.
- Ronald McDonald House assists individuals finding other accommodations.
- Beau advocates for taking every possible precaution around immunocompromised individuals.
- He mentions the importance of understanding Ronald McDonald House's purpose.
- Beau encourages individuals to research and not rely on misinformation.
- He stresses the significance of prioritizing the well-being of sick children.

### Quotes

- "Nobody cares what you think when it comes to people who refuse to take every possible precaution to keep a bunch of sick kids from getting sicker."
- "Your moral compass is broke."
- "Nobody cares about your moral determination because your moral compass is broke."
- "And I think that's a very good thing."
- "It's just a thought."

### Oneliner

Ronald McDonald House requires vaccinations, sparking criticism from anti-science groups, but Beau stresses the importance of prioritizing public health over personal beliefs and moral judgments.

### Audience

Public health advocates

### On-the-ground actions from transcript

- Support Ronald McDonald House or similar organizations assisting families of sick children (implied).
- Research and prioritize scientifically-backed information over misinformation (implied).
- Take necessary precautions to protect immunocompromised individuals in communal settings (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of the controversy surrounding Ronald McDonald House's vaccination requirement and the importance of prioritizing public health over personal beliefs and moral judgments.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about Ronald McDonald House
in Canada, doing your own research, moral judgments,
and a certain subset of the anti-science community.
And I want to specify that it is a certain subset,
because even some relatively prominent figures
in this community are remaining really quiet about this,
because even they can see that if there
is a place that should do this, it's Ronald McDonald House.
If you don't know what I'm talking about, Ronald McDonald
House in Canada, and perhaps everywhere, I'm not sure,
has made the determination that if you
want to stay in their facility, you need to be vaccinated.
This was met with people talking about tyranny
and making a whole bunch of moral judgments
and wanting a boycott of McDonald's and Ronald McDonald
House.
That last part doesn't make any sense.
I don't think anybody actually wants
to use Ronald McDonald House.
And I would suggest that if you're
part of a group that talks about natural immunity
and your normal health, taking care of things,
maybe you shouldn't be in McDonald's anyway.
I'm just saying.
But all of that aside, it's the moral judgments
that are actually just driving me wild.
It's the most bizarre thing I've seen.
Let me save everybody a whole lot of time on this one.
If you would be around a bunch of immunocompromised kids
and their caregiver families without taking
every possible precaution, nobody cares what you think.
Nobody cares about your moral determination
because your moral compass is broke.
Nobody cares.
If you don't know what Ronald McDonald House does,
it's a facility where the caregiver families of kids
who are incredibly ill can stay while their kid is
being treated.
And there are a lot of common areas in these facilities.
Yeah, it makes complete sense for them
to have something like this.
I am actually shocked that they didn't have it before.
Now, there is one thing that appears to be missing from this,
from this little conversation.
And that's that Ronald McDonald House isn't throwing these
people out in the street.
According to them, their team is going
to find them other accommodations.
They just can't stay in the facility.
Good.
I mean, that's very nice of them.
And I know there are people right now who may be saying,
well, wait, that's not what I thought.
I even donated to a GoFundMe.
Yeah, well, that's what happens when you do your own research.
And by do your own research, what you really mean
is you let a whole bunch of people who
profit from you being ill-informed misinform you
and you just repeating whatever they say.
And I think that's a very good thing.
And you just repeating whatever they say and allowing
them to drive your emotions.
Nobody cares what you think when it
comes to people who would refuse to take
every possible precaution to keep a bunch of sick kids
from getting sicker.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}