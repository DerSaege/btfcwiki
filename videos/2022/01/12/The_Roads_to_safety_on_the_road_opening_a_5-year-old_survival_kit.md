---
title: The Roads to safety on the road (opening a 5-year-old survival kit)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JASVnNrCKmQ) |
| Published | 2022/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of emergency preparedness on the roads following an incident where many people were stranded on the interstate.
- Demonstrates an emergency preparedness kit that he put together years ago for a friend who had to leave in a rush and rented a car.
- Emphasizes the necessity of checking and updating emergency kits annually due to expired and non-functional items.
- Lists the key components of a basic emergency kit: food, water, fire, shelter, first aid kit, and a knife.
- Shows the contents of the emergency kit, including items like a first aid kit, Mylar blanket, siphon, N95 mask, gloves, batteries, compass, chewing gum, flints, fishing line, hand sanitizer, duct tape, flashlights, can opener, chemlights, multi-tool, candle, hatchet, survival card, meds, paracord bracelets, tampons, coffee, biofuel, dog food, rice, and canned food.
- Points out the importance of having duplicate items in the kit and multiple ways to start a fire.
- Mentions the significance of adapting the emergency kit based on climate, terrain, and individual skill set.
- Stresses the need for regular maintenance and updating of emergency kits to ensure functionality during emergencies.
- Encourages viewers to understand that emergency preparedness is an ongoing process and not a one-time task.
- Concludes by reminding everyone to keep their emergency kits updated and wishes them a good day.

### Quotes

- "You always have to keep it updated."
- "Most of the stuff you need doesn't work."
- "Emergency preparedness is an ongoing process."

### Oneliner

Beau stresses the importance of regularly updating emergency kits to ensure functionality during crises, showcasing items in an outdated kit as a reminder of the need for preparedness.

### Audience

Emergency Preparedness Enthusiasts

### On-the-ground actions from transcript

- Inventory and update your emergency kit annually to ensure all items are functional and up-to-date (exemplified).
- Customize your emergency kit based on your location, climate, and individual skill set (implied).

### Whats missing in summary

The full transcript provides a detailed walkthrough of an outdated emergency kit to underline the importance of regular maintenance and updates for functional readiness during emergencies. Viewers can glean valuable insights on assembling their emergency kits effectively. 

### Tags

#EmergencyPreparedness #KitMaintenance #UpdateRegularly #Customize #BePrepared


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the roads
to emergency preparedness on the roads.
Recently, there was an incident on the interstate
where a whole bunch of people got stranded
for an extended period of time.
After that happened, a whole bunch of questions
came in talking about what people
could do to be prepared.
Now, I have videos that are about survival and emergency
preparedness in general on this channel and on the other.
But when you're talking about putting something in a car,
there are some other things that you need to keep in mind.
This box sitting in front of me is a little emergency
preparedness kit that we put together years ago.
A friend of mine had flown out to visit.
Something happened.
They had to leave in a rush, and they rented a car.
We went to my garage and put one together very, very quickly.
Threw it in the back of the rental car.
They went home.
When they got there, this case got switched into their car.
About a year later, they came to visit again
and brought it back to me.
Now, in that first year, I don't know if this was opened.
Since then, it hasn't been.
So we're going to go through this as an example of what
you can put in a kit.
And it's also going to teach us something else about why you
should check your stuff every year.
Because while I don't know what's going to be in this,
I totally know what's going to be in this.
A lot of the stuff that's in here
isn't going to work because it hasn't been checked.
Now, when you're putting together a kit in general,
you need to make sure you have a certain set of things.
You need to be able to have food, water, fire, shelter,
a first aid kit, and a knife.
That's what you need in any kit you're making.
Other than that, it's going to be heavily influenced
on your climate, your terrain, what it's for,
so on and so forth.
But those things you always have to have.
OK.
So let's open this up and see what's in it.
OK.
So right up top, as it should be, is a first aid kit.
Now, odds are, yeah, the bandages, all this stuff
is expired.
And I don't really want to get into that,
but if you're talking about sterile stuff, yes, it can expire.
But you make do with what you can in situations like that.
There's also a Mylar blanket.
So you have a first aid kit and shelter right here.
And most of this would still be usable to some degree.
This is a siphon to get gas out of the kit.
This is a siphon to get gas in an emergency.
And I already see corroded batteries.
OK.
So we have an N95 and gloves.
Now, this was put together before all of the world
stopped because of the whole public health thing.
But it's always good to have stuff like this in your kit.
And it's not just for public health.
When you're talking about other hazards that
could exist on a road, it might be a good idea
to have a mask available.
And it is certainly always a good idea
to have gloves available if there's an accident,
you need a barrier against fluid.
So we have our first pack of batteries
that opened up when I touched it.
The whole box just fell apart.
OK.
That is what hot, cold, hot, cold can do to a battery.
They're completely worthless.
They're ruined.
They've corroded.
Gross.
OK.
What else do we have in here?
We have another pack of batteries,
which are also corroded, but not quite as bad.
These are still unusable, though.
And then yet another pack of batteries.
Now, the reason it's good to have a bunch of batteries
is because in addition to it powering stuff,
you can also use this to start a fire.
And all of that stuff that we talked about,
these actually look like they would still function.
This pack doesn't look so bad.
All that stuff we talked about, you
want to have multiple ways to do anything.
So you want to have multiple ways to start a fire.
This combined with the foil from a cigarette pack
can actually be used to start a fire.
OK.
We have a compass.
This should still work.
Should still work.
It'll, it's just a little slow.
Some kind of chewing gum that is definitely gross, expired.
We have flints for a Zippo.
Fishing line.
So fishing line can obviously be used for fishing,
but it can also be used for tying stuff together,
making snares, so on and so forth,
if you get stranded in a very isolated area
for an extended period.
Here we have a little bit of a stuff
that we can use for a fire.
Here we have hand sanitizer that expired two years ago.
Toilet paper.
That would still work.
Duct tape, because you'll always need that.
This is something you can be used to manufacture shelter.
It can, it's duct tape.
It can be used for anything.
We have a flashlight that is definitely corroded.
But oddly enough, still works.
So we have one working flashlight.
That's cool.
We have a can opener.
We have another flashlight, because if you have one,
you have none.
You want to be able to generate light.
This is completely corroded.
It's horrible.
If you have one, you have none.
And you want to be able to have light outside of your fire,
and you want to have something that's portable.
So that does not function.
We have a backup for that.
Also does not function.
OK.
Another method of creating light.
We have a bag of chemlights, glow sticks.
Chemlights, glow sticks.
Hey, look at that.
Still works.
We have a multi-tool that surprisingly is not rusted.
Now if I didn't say this earlier,
we threw this together in my garage.
We were just throwing stuff in.
So there's probably duplicates of everything,
like to a degree more than you would normally need.
We have a candle that, due to being stored
in the trunk of a vehicle, has melted and kind of taken
the form of the water bottles around it.
But that would actually still work.
We have a hatchet.
Now this can be used in the event of a car accident
to get somebody out or to get firewood if you're
stranded for an extended period.
We have a little survival card.
Has a bunch of tools on it.
It's another multi-tool.
Also has string for snares.
A mechanical pencil.
I'm not really sure why that's there.
OK.
We have some meds.
Benadryl, generic Benadryl.
So antihistamine.
And it expired two years ago.
More bandages.
Antacids.
Expired two years ago and molded.
That looks tasty.
We have these silly little paracord bracelets.
Now the paracord is useful.
Obviously you can use it to help pass from the shelter.
You can untie these bracelets.
And then you just have the rope.
And then many of them, like this one,
has a striker right here and a scraper there.
So you can use it to create a fire.
There's a demonstration of this over on another video
on this channel, like the roads to survival
or something like that.
There's another one of those.
We have an inexpensive knife and gig with paracord, again.
And then another inexpensive knife.
This is a pocket knife, but this was probably thrown in here
because it's utensils.
It has a fork, spoon, knife, and can opener on it.
Another flashlight covered in corrosion and sticky stuff.
Does not work.
Now if you know how to do this, I
recommend putting one of these in your kit.
This is what's used to plug a tire.
So if you get a hole, run over a nail or a screw,
this can be used to fix it.
OK, we have a folding stove.
And this actually has the candle thing in there.
This actually looks like it would still work.
OK, we have a pocket knife.
Another flashlight that does not work.
This one is corroded and covered in rust.
That's about it.
We have a blanket.
Now this would work where I'm at.
And generally in the south, if you're
going to get up north, you probably
want something a little bit more heavy duty.
We have a lighter that does not work.
It's not functional.
So far, basically anything that could go bad has.
So this definitely shows you why it's important to check
this stuff out every once in a while.
OK, so here we have said to be waterproof matches.
They're not.
They're water resistant at best.
Striker is on the bottom.
And no, these are definitely not going to work.
OK, we have two tampons without applicators.
So there's a lot of people who carry these
and they give various reasons for carrying them.
These are really good kindling.
So you can open these up, fluff them out,
and use that fire striker.
And this is something that is really good for starting fires.
There are a lot of urban legends about the uses of tampons.
OK, we have coffee.
True survival need right here.
It's stuck to the side of the container and has molded
and is a solid piece of mold inside.
Probably wouldn't want to use that now.
OK, we have biofuel.
Now, this can be used to heat.
It's used for putting under dishes, like at buffets
and stuff like that.
But this does put off a good amount of heat
and it burns for a while.
I don't know if this stuff expires or not, to be honest.
And I don't see an expiration date on it anywhere.
I'll actually have to try that and see if it works.
We have dog food.
Oh, that's right.
They had a dog with them.
It's expired, of course.
We have more toilet paper, more utensils, a striker.
So another method of creating fire.
A lighter.
Hey, look at that.
That one works.
We have a couple of bags of rice.
And this, it's best by date is five years ago.
And we have a can of pork and beans
that expired four years ago.
And then we have some bottles of water in here,
which are probably still all right.
OK, so all of the contents, everything
that you need for a kit like this, it was in here.
You had food, water, fire, shelter, knife, first aid kit.
It's all there.
But I'd say more than half of it wasn't functional.
So while you can use this as a guide
to figure out what you want to put in your kit
and what you want to keep in your car,
so if you do get stranded, you have some of the stuff you need,
just remember that you always have to keep it updated.
This was something that once it came back,
it got moved from the garage to the shop.
I think this actually used to be sitting in the background
at one point and never opened again.
If you were to have to rely on this to keep you safe,
most of the stuff you need doesn't work.
Now, in the other videos that I have on this topic,
it goes more in-depth into what you
can use all of this stuff for, and why you need it,
and why it's a good idea to have it.
But in general, this kind of gives you
an overview of what you need or what
would be a good idea to have.
You can adjust this however you want, and you should.
You should make it fit more to your climate.
I'm down here in the South, and where this person was traveling
was also more or less in the South.
So water is something you can find everywhere down here.
If you were in the southwestern United States,
you would probably want a lot more.
You need to make sure you adjust it for the area that you're in,
and make sure you adjust it for your skill set.
So the idea, there's fishing wire,
and there's string for snares in here.
If you don't know how to do that,
it doesn't do you any good to have it.
Make sure that you have stuff that suits your skill set.
I'm really glad that people after seeing that were like,
hey, that could happen to me.
How do I get prepared?
That's a good mindset to have.
Just make sure you understand it's not a one-time event.
You don't just throw the stuff into a container,
and forget about it.
Because if you do that when you need it,
most of the stuff isn't going to work.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}