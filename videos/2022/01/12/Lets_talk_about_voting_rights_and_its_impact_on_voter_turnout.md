---
title: Let's talk about voting rights and its impact on voter turnout....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IQ9uYPi953I) |
| Published | 2022/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democrats are pushing for voting rights legislation, with heavy hitters in the party in favor of it.
- The debate over voting rights legislation could end up benefiting Democrats in terms of voter turnout.
- Republicans may struggle to block the legislation, leading to potential consequences.
- Voting is viewed as a fundamental aspect of American democracy, cherished by many.
- Opposing voting rights legislation could paint Republicans as anti-American.
- Republican attempts at gerrymandering and the January 6th committee's revelations may further this perception.
- Republicans will need to justify their position on blocking the legislation, potentially risking a negative image.
- The polarization in the country could significantly impact voter turnout.
- Democrats may show up in larger numbers if Republicans continue to oppose voting rights legislation.
- Republicans could inadvertently drive Democratic voter turnout by standing against voting rights.

### Quotes

- "Democrats are pushing for voting rights legislation, with heavy hitters in the party in favor of it."
- "Voting is viewed as a fundamental aspect of American democracy, cherished by many."
- "Opposing voting rights legislation could paint Republicans as anti-American."
- "Republicans will need to justify their position on blocking the legislation, potentially risking a negative image."
- "Republicans could inadvertently drive Democratic voter turnout by standing against voting rights."

### Oneliner

Democrats are pushing for voting rights legislation, but Republican opposition could inadvertently drive Democratic voter turnout.

### Audience

Voters

### On-the-ground actions from transcript

- Contact your representatives to express support for voting rights legislation (implied).
- Join organizations working to protect voting rights (implied).
- Organize events to raise awareness about the importance of voting (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential impacts of the ongoing debate over voting rights legislation, which can be best understood by watching the full video. 

### Tags

#VotingRights #DemocraticParty #RepublicanParty #Polarization #Gerrymandering


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today we are going to talk about voting rights legislation and the
impacts of the debate over it.
Um, you know, it's in the news a lot right now, but I don't think
people are looking ahead.
It seems that the democratic party may have stumbled their way into a win-win
situation and the Republican party may end up doing all of the heavy
lifting to assist in high Democratic voter turnout.
Okay, so in the discussion over this, Democrats are through the door.
They've said they're going to do it.
All of the heavy hitters in the Democratic Party have now come out in favor of it.
They have to make the attempt.
So what happens?
Either they get it through somehow, and looking at the way things are lined up at the moment,
I am skeptical of that occurring.
It doesn't seem likely.
They're talking about, well, we're going to carve out an exception in the filibuster.
They don't even have the votes to do that.
So I'm skeptical.
They can find the votes, but at time of filming, that seems kind of unlikely.
The other option is that Republicans are able to block it, okay, well then what?
What happens then?
Voting is seen by Americans as a key element of American democracy, and whether you support
representative democracy or not, you know that for most Americans, it is sacred.
It's something, it's a closely held belief, it is something that is close to their heart.
And this is really something that should apply to Republicans, Democrats, independents, everybody.
Those people who are invested in the American system are supporters of democracy and voting
rights are foundational to it, right?
All of this should go without saying.
Republicans opposing voting rights legislation paints them as anti-American.
It paints them as being against foundational American beliefs.
Now under normal circumstances, things are so polarized, well that doesn't really matter
because Republicans are just going to listen to Republican talking points.
So it wouldn't penetrate that base of people.
Democrats would listen to Democrats, but they're going to vote for Democratic candidates anyway.
But see, right now, it's not just the voting rights rhetoric.
It's not just going to be that debate.
There's more that's going to be in the news.
You have Republican attempts at gerrymandering across the country.
Those maps, most of them, are so absurd they will be tested in court.
Most of the current ones are going to lose.
They're going to be sent back.
Every time that happens, there are going to be newspaper articles about it.
It's going to get coverage, which will further the idea that Republicans are against voting,
Republicans are against a foundational element of American democracy. On top of
that you have the January 6th committee and there are going to be revelations
coming out of that for the next, I don't know, year. And every one of them is going to
tie back to the idea that Republicans tried to undermine democracy. They tried
to overturn the election. And it's not just going to be the text messages. It'll
be the fake electors that sent those documents in from the various states. All
of that is going to get news coverage. All of it is going to paint Republicans
as being against voting, being against a foundational element of American
democracy, right? And it's going to go on and on and on. Then what happens?
Republicans themselves, they have to find a way to, well, block the legislation,
right? And it's not going to be as simple as just not allowing it to go through
through, because Democrats are going to be making noise.
They've made that clear.
So Republicans are going to have to craft sound bites to justify their position, to
explain why they're not going to allow it through.
And every time they do that, with a lot of the talking points and the rhetoric they could
use from Republicans today anyway, they'll paint themselves as being against voting.
And given recent history, it's just going to add to the events of January 6th and the
attempt to overturn the election.
Republicans need to think about what happened when there was just the perception of Democrats
telling the Republican base to wear masks and get vaccinated.
Now I'm not doing it.
I'm not going to take basic precautions.
Their base literally died because we are that polarized as a country.
They put themselves and their families at risk over talking points because the perception
was that the other team, well, they wanted them to do it, so they weren't going to.
Americans are a stubborn people.
This occurred in numbers big enough to swing elections in and of itself.
Those lost disproportionately skew Republican because of this.
Republicans need to ask themselves if they believe Democrats are any different when it
comes to being stubborn Americans.
If the perception is that Republicans don't want Democrats to vote, what do you think
will occur?
I think it will drive voter turnout.
I think Democrats will show up in more numbers than they typically do in midterms.
I don't really see another way it could shape up, to be honest.
Republicans stand their ground on this and try to block voting rights
legislation, they will successfully paint themselves as being against voting because
it's not just going to be the normal political debate because we have all
these other news stories and all of these other news stories reinforce the
idea that Republicans are against voting. Republicans may do all of the work to
drive Democratic voter turnout. If Republicans were smart, and that's a big
if lately, if the leadership of the Republican Party, they would probably
have those senators who are retiring soon or those senators who are in very
secure areas where you know they're going to get re-elected, side with Democrats, and
let this legislation go through.
Because if they don't, they will continue the narrative of January 6th, and they will
drive Democratic voter turnout, and that doesn't seem like a win for them.
legislation going through isn't a win for them either, but it's not something
that is quite as detrimental to their political futures. Either way, if you are
a Democrat, as a whole lot of the people watching this channel are, just remember
Republicans don't want you to vote. I'm willing to bet you'll be at the polls.
holes.
It's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}