---
title: Let's talk about Trump and the Georgia special grand jury....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RNgZWzvSxe8) |
| Published | 2022/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fulton County in Georgia is impaneling a special grand jury to look into disruptions after the election, including Trump's actions.
- The special grand jury in Georgia won't indict Trump but can make recommendations to the prosecutor.
- In Southern politics, there is a tradition of not pointing fingers at fellow party members, even if they broke the law.
- A special grand jury in Georgia has subpoena power and can demand records from government agencies.
- The grand jury will likely provide political cover for those who want to help with the investigation without risking their careers.
- The prosecutor may have already decided to try to get Trump indicted, and the grand jury will help gather evidence for this.
- The process is seen as more of an investigative tool rather than a means to directly indict Trump.
- Beau believes that crimes were committed based on tape evidence, but the grand jury will determine the legal implications.
- The grand jury convenes at the beginning of May and is good for a year, but Beau doesn't think it will take that long.

### Quotes

- "A special grand jury in Georgia is not a normal grand jury."
- "If you walk in and you put your hand on that Bible and you swear before God that you're going to tell the truth, the whole truth and nothing but the truth, you had better do that."
- "It has subpoena power. It has the ability to demand records from government agencies, stuff like that."
- "So if you are sworn to tell the whole truth, well, that overrides everything."
- "Y'all have a good day."

### Oneliner

Fulton County in Georgia convenes a special grand jury to look into post-election disruptions, providing political cover and investigative tools without directly indicting Trump.

### Audience

Georgia residents

### On-the-ground actions from transcript

- Contact local organizations or legal aid to understand how you can support the process (suggested)
- Attend community meetings or events to stay informed about the investigation (suggested)

### Whats missing in summary

Legal nuances and specific details about the potential outcome of the investigation.

### Tags

#Georgia #FultonCounty #SpecialGrandJury #Trump #Investigation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Georgia,
because last night Georgia was on a lot of people's minds
and my inbox filled up.
If you don't know, you missed the news,
Fulton County, they are going to impanel
a special grand jury to look into disruptions
in the process after the election,
meaning they're going to be looking into Trump.
They're going to be looking into Trump,
his perfect phone call, asking to find the votes,
and I think something about a prosecutor who resigned.
So they're going to be looking into that.
Now my inbox filled up asking me how long
I thought it would be until this grand jury
indicts Trump.
They won't.
They won't.
So I'm sure lawyers in Georgia are laughing right now.
A special grand jury in Georgia is not a normal grand jury.
In fact, I don't even know that they can indict.
I think they can only make recommendations
back to the prosecutor who wanted the special grand jury.
Okay, so if it can't indict, what good is it, right?
You're about to get a crash course in Southern politics.
Let's say, hypothetically speaking,
that there's a complex case,
probably how it's going to be described,
in which the real complexity is that the people
who have the evidence, well,
if they provide it,
they're ruining their political careers.
Because by Southern tradition,
if you're part of the same club as somebody else,
the same party, even if you know they broke the law,
you don't walk in and talk to the law and point the finger.
That's not something you do, right?
And if you did that, you might suffer,
well, you might be looked down upon
by people in your party, in your club.
However, that all changes if you get a subpoena.
If you walk in and you put your hand on that Bible
and you swear before God that you're going to tell the truth,
the whole truth and nothing but the truth,
you had better do that.
I think in this case,
the special grand jury is, for a lot of people,
it's just going to be political cover.
I would imagine that a lot of the people
who are about to get a subpoena
already have like a file
of what they're going to bring to the grand jury,
but they won't do it without a subpoena,
that they need that political cover.
A special grand jury in Georgia
is more of an investigative tool.
It has subpoena power.
It has the ability to demand records
from government agencies, stuff like that.
And that's what it's going to be used for.
As far as how long this is going to take,
I don't actually know.
I want to say it convenes at the beginning of May
and it's good for a year.
I don't think it will take the whole year, though.
I think that's just an arbitrary time frame they put on it.
But don't expect an indictment
because there won't be one from this grand jury.
They'll make a recommendation back to the prosecutor.
And to be honest, my guess is that the prosecutors
already made the decision to try to get Trump indicted.
Otherwise, we wouldn't be at this step.
They just need to gather the evidence that they know exists
and the subpoena power of the special grand jury
will allow them to do that.
And it will provide political cover
for those who might want to help
but would prefer not to ruin their political careers over it.
But it is the Bible Belt.
So if you are sworn to tell the whole truth,
well, that overrides everything.
So I don't have a timeline,
but don't expect an indictment from this grand jury.
Basically, I would imagine that a couple of months,
the prosecutor will walk out
and just announce the decision on how that's going to go.
My guess, my very non-lawyer,
basic understanding of Georgia law,
I'm fairly certain that there were crimes committed on tape.
But again, I'm not a lawyer.
That's what the grand jury's for.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}