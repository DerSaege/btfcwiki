---
title: Let's talk about Doocy, Biden, and hard-hitting journalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vvZHdUs3BqQ) |
| Published | 2022/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A Fox News reporter shouted a question at President Biden as he was leaving, asking if high inflation is a political liability for the incumbent party.
- President Biden responded, saying high inflation is a great asset, followed by a derogatory comment towards the reporter.
- Beau criticizes the question as not being hard-hitting journalism but a "gotcha" question designed for a soundbite.
- Beau believes journalism should provide new information and uncover things rather than aim for sensational soundbites.
- He suggests that the purpose of journalism is to inform and not to incite.
- Beau states that journalists should be educators, but many viewers prefer sensationalism over informative content.
- He points out that both sides of the aisle have criticized the reporter, indicating a bipartisan agreement on the nature of the question.
- While Beau admits finding humor in Biden's response, he acknowledges that it was not fitting behavior for the president.
- Beau concludes by stating that news outlets that do not inform are actually inflaming their audience and serving their own agenda rather than providing a service.

### Quotes

- "Questions like this don't inform, which should be the purpose. They inflame."
- "Journalists should be educators."
- "If your news outlet is not informing you, it's inflaming you."

### Oneliner

Beau criticizes a "gotcha" question posed to President Biden, advocating for journalism that informs rather than incites.

### Audience

Journalists, News Viewers

### On-the-ground actions from transcript

- Hold news outlets accountable for providing informative content (implied)
- Support journalism that educates and informs the public (implied)

### Whats missing in summary

The full transcript provides a deeper analysis of the role of journalism in informing the public and the dangers of sensationalism in news reporting.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So I guess today we're going to talk about Ducey and Biden and what was said.
And we're going to talk about hard hitting questions and journalism and where this
country is headed.
If you don't know what happened, which I imagine most people do, a reporter for Fox
News shouted a question to President Biden as the reporter was leaving the room.
The question was something to the effect of, is it a political liability for the
incumbent party to have high inflation?
Something along those lines.
To which President Biden responded by saying something to the effect of, no, it's a
great asset, you stupid son of a...
So that's what happened.
I got this message.
You just said journalists should ask hard hitting questions.
I'm wondering if you'll defend Ducey for following your advice.
I was just thinking that.
I was just thinking how asking if high inflation is a political liability for the
incumbent party, I was thinking about how that's hard hitting journalism.
How that's asking the tough questions.
That's holding those in power responsible.
You know, I mean, that ranks right up there with asking, you know, is it a
political liability if a candidate is caught in an extramarital activity?
Is it a political liability if there is high unemployment?
Is it a political liability if the other side gets more votes?
This isn't actually hard hitting journalism.
This isn't a tough question.
This is a gotcha question.
It's designed to elicit a soundbite.
It did.
I mean, he got what he wanted.
I'm going to suggest, though, that journalism is designed to uncover something.
To provide new information.
I think that perhaps if you are unaware that high inflation is bad for the
incumbent party, maybe your news organization made a mistake with the
assignments. Maybe you shouldn't be in the White House press pool.
Maybe you should, I don't know, be covering high school debates or something.
Like that's something more, you know, more linked to your understanding.
It wasn't hard hitting.
It was a gotcha question designed to elicit a response.
And he got one.
He does it every day.
He sits in that room and asks some just ridiculous question.
And the press secretary and everybody else in the room, right.
What's in the room rolls their eyes.
It's not hard hitting journalism.
It's tabloid stuff.
It's the byproduct of the oh no you didn't generation.
It's looking for that soundbite because most of the consumers of that channel do
not think beyond soundbites.
It's not actually hard hitting journalism.
It's not even valuable.
Questions like this don't inform, which should be the purpose.
They inflame.
The problem is that many Americans have fallen into the idea that this is hard
hitting, that getting that salacious soundbite is somehow actually what they're
supposed to be doing rather than providing their viewers, their audience with
information.
Journalists should be educators.
The problem is that if your core audience villainizes education at every turn and
believes that everything they know is right, regardless of the outcome, then
regardless of the evidence that comes out, being an educator doesn't pay.
It doesn't get you ratings.
So you have to ask a question that everybody in the room knows the answer to
and hope that the person you're asking the question of gets irritated.
I would point out that Ducey has been called dumb or stupid by people on both
sides of the aisle. Viewing his questions as ridiculous is a bipartisan thing.
Now, should Biden have said that?
No.
No, even though I will admit I laughed, because to be honest, I don't view people
who ask questions like that as journalists, in my opinion.
That being said, President Biden is, in fact, the president.
Not exactly what he should have done.
Personally, I wouldn't mind more of it, but it is not fitting when it comes to the
office. I will admit that part.
It's easier for me to admit that than say that Ducey's a journalist.
If your news outlet is not informing you, it's inflaming you.
You aren't providing, you're not getting a service from that news outlet.
The news outlet is getting a service from you.
They're using you to further their own agenda as they drive you and manipulate
you rather than inform you.
No, it's not hard hitting journalism.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}