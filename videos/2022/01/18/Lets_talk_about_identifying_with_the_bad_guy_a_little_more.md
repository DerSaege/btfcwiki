---
title: Let's talk about identifying with the bad guy a little more....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H-h5LK5FnNA) |
| Published | 2022/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the choice of identifying with the bad guy throughout history.
- Responds to a message challenging the identification of founding fathers as racist due to owning slaves.
- Points out that owning slaves based on race is a clear sign of racism.
- Expresses dislike for the term "founding fathers," suggesting they were not actively involved in parenting the nation.
- Introduces Roger Sherman as a significant figure who played a key role in various foundational documents and never owned slaves.
- Questions why figures like Roger Sherman, who opposed slavery, are not more prominently discussed in history.
- Suggests that the narrative of all founding fathers being slave owners is a deliberate fiction to maintain a certain image.
- Lists examples of founding fathers like John Adams, John Quincy Adams, Thomas Paine, and Benjamin Franklin who did not own slaves or later opposed slavery.
- Dispels the myth that all founding fathers were slave owners and argues that it serves a purpose to maintain a false narrative.
- Addresses the issue of systemic racism, explaining how it can be pervasive and invisible to those benefiting from it.
- Encourages the audience to look for white heroes to identify with and challenges the notion of needing a white hero.
- Raises the question of whether race or moral values should dictate hero identification.

### Quotes

- "If you owned slaves based on race, you are in fact a racist."
- "Systemic racism is something that is so pervasive you don't even realize it's occurring."
- "Why are you looking for a white hero? Is it impossible to identify with a black person?"
- "Pigment more influential in determining who you can identify with, then right or wrong."
- "And if that's the case, you chose to identify with the bad guy."

### Oneliner

Beau challenges the narrative around founding fathers, systemic racism, and hero identification, questioning the choice to identify with the bad guy.

### Audience

History enthusiasts, anti-racism advocates

### On-the-ground actions from transcript

- Research and share information about historical figures like Roger Sherman who opposed slavery (suggested).
- Educate others about the diverse views and actions of founding fathers regarding slavery (suggested).
- Challenge traditional narratives and seek out lesser-known historical figures who stood against injustice (suggested).

### Whats missing in summary

Exploration of the complex narrative surrounding historical figures and the impact of systemic racism on society today.

### Tags

#History #SystemicRacism #HeroIdentification #FoundingFathers #Racism


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk a little bit more about choosing
to identify with the bad guy.
In a recent video, I was talking about how a lot of people
look back through history.
And for whatever reason, they choose
to identify with the person doing wrong,
with the structure that was wrong
and was proven to be wrong.
They choose to identify with the bad guy.
We're going to talk about it a little bit more today because I
got a message.
I was with you up until you said I had chosen to identify
with the bad guy by the standard they use,
they being people who would like to look at history
with a critical eye towards race.
By the standard they use, all of our founding fathers were racist just because they owned
slaves.
So I'm sure you'll pull out an example of somebody who didn't own slaves, but don't
just give me one possible option, give me a few white heroes to identify with, or are
they all bad?
This is all an excuse to further divide the country.
about systemic racism is where you always lose me. This is the one issue
where you always lose me. I know racism existed, I know it was bad, but does it
really still exist today?" Okay, so we're gonna talk about all of this. First, I
just want to point out, well actually first I want to point out this
whole, they were racist just because they owned slaves. I mean, yeah, again, let's just start
there. If you owned slaves based on race, you are in fact a racist. Just throwing that out there.
But also, when it comes to founding fathers, I really dislike that term. I have grown to despise it.
it. Mainly because if this was a parental relationship, they were pretty absentee. At
best, they had shared custody. Most founders, most people involved with the founding of
this country, they weren't around the whole time. They showed up, did a little bit, and
then disappeared. In fact, there's only one person who signed the Articles of Association.
That's what terminated trade with the king.
Signed the Articles of Association, the Declaration of Independence, the Articles of Confederation,
and the U.S. Constitution.
There's only one of our founders that was around for the whole thing, for all four documents.
It's worth noting this person also signed the 1774 petition to the king.
think you'd know who this is, especially given the fact that our Congress looks the way it does,
in large part because of him. Two houses, you know, bicameral, one done by population,
the other divided by the states. It wouldn't look that way without him. His name's Roger Sherman.
There's going to be a whole bunch of people who that's probably the first time they've ever heard
his name. I wonder why. I wonder why we haven't talked about this person. Why this isn't front
and center in a whole lot of history books. It seems like somebody that played that big of a part
in it should be mentioned. There is one unique thing about Roger Sherman. Never owned slaves.
referred to slavery as inequitous, absolutely refused to be involved in passing a tax on slaves
because you tax property, not human beings.
It's weird that you don't hear about him, right?
But maybe that's by design.
maybe there were actually quite a few of the founders who were opposed to slavery.
And you don't hear about them because they want, it was important for the country to maintain the
idea that all of the founding fathers were racist because they owned slaves.
The idea that they all owned slaves. They didn't. That's not true.
You said you wanted more than one. John Adams, John Quincy Adams, Thomas Paine.
There's actually a bunch. You know, people want to quote Thomas Jefferson and look to him,
which to me, I always find odd because Thomas Paine was objectively the better person and
objectively had better quotes. But there's quite a few. These are just the ones that I can come up
off the top of my head, Benjamin Franklin owned slaves early in his life. And as he got older,
he became an abolitionist. The Constitution was drafted in 1787, ratified in 88, became effective
in 89, and in 90, Benjamin Franklin introduced a bill to abolish slavery. The idea that it's all
of them. That's just not true. But it is a useful fiction to maintain that system, to maintain the
idea that, well, that's just the way it was back then. Our founders, they didn't do this bad thing.
That's just the times. Nobody knew any better. It's not true. Even Thomas Jefferson referred to
slavery as a hideous blot knew it was wrong knew it was wrong. So there's your
answer to that there's a bunch and you can actually you could probably type in
founding fathers who didn't own slaves and get a list but then there's this
other part this is all too all an excuse to further divide the country talking
systemic racism is where you always lose me, this is the one issue where you always lose me, I know
racism existed, I know it was bad, but does it still exist today? Systemic racism. Systemic racism
is something that is so pervasive you don't even realize it's occurring a lot of times if you're
benefiting from it. If you are on the bottom, it's probably pretty pronounced and very easy to see.
But if you're benefiting from those structures, you probably have to look for it, and you
may participate in it or be affected by it without even knowing.
Don't just give me one possible option.
Give me a few white heroes to identify with.
Go back and watch the other video.
There at the end, when I'm talking about this, I said, you chose to identify with the
the slaver, rather than those helping the Underground Railroad.
I was thinking of Harriet Tubman.
Why are you looking for a white hero?
Is it impossible to identify with a black person?
Is it impossible to view them as a hero?
pigment more influential in determining who you can identify with, then right or wrong.
And if that's the case, you chose to identify with the bad guy.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}