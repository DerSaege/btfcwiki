---
title: Let's talk about my last day teaching in Virginia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b51vrmzdKHo) |
| Published | 2022/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Last day teaching in Virginia, moving to a different state.
- State-mandated curriculum requires teaching a debate between Abraham Lincoln and Frederick Douglass that never happened.
- Contemplated a fake debate but realized the critical nature of Douglass towards race wouldn't be suitable.
- Planned to show an epic rap battle between Frederick Douglass and Thomas Jefferson but found it critical of slavery and the founding systems.
- Constrained by restrictions and guidelines, making it impossible to teach history in Virginia.
- Leaving with a lesson from Thomas Paine, stressing the importance of knowledge and duty.
- United States is meant to be a government of informed people, requiring knowledge for self-governance.
- Criticizes new laws from state politicians making knowledge a crime and ignorance a duty.
- Blames parents for surrendering education to profit-driven individuals who benefit from keeping people ill-informed.
- Parents neglecting their duty to be informed and knowledgeable, allowing politicians to criminalize knowledge and perpetuate ignorance.
- Calls attention to a bill in Virginia mandating the teaching of a debate that never happened, questioning the competence of those behind it.
- Urges the people of Virginia to recognize manipulation and reclaim their duty to be informed.

### Quotes

- "Your duty is to get as much knowledge as you can because, in essence, you are supposed to be leading this country."
- "Your parents surrendered your education to a bunch of people who profit from you being ill-informed."
- "The people of Virginia should probably take a long, hard look at how they were manipulated."

### Oneliner

Beau addresses the absurdity of teaching a non-existent debate, criticizes the legislation making knowledge a crime, and urges Virginians to reclaim their duty to be informed.

### Audience

Students, Educators, Virginians

### On-the-ground actions from transcript

- Question the curriculum requirements and advocate for accurate and meaningful education (suggested).
- Challenge legislation that restricts knowledge and supports ignorance (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the challenges faced in education and the importance of critical thinking and informed citizenship.


## Transcript
Well, howdy there class, internet people.
As some of you know, today is my last day teaching.
I will be leaving Virginia. I'm going to go to a different state to teach.
And I chose this is my last day for a reason.
Today is the day that the state mandated curriculum tells me that I have to teach you
about the debate between Abraham Lincoln and Frederick Douglass.
And I had some trouble trying to figure out how to do that because that
debate never occurred. It's in the law that I have to teach that.
But it never happened. There was a debate that was the
Lincoln-Douglass debate, but it had nothing to do with Frederick Douglass.
So I've been trying to figure out how to comply with the law.
And I thought about maybe like doing a fake debate
between Abraham Lincoln and Frederick Douglass, but then I realized
Douglass was kind of like a big abolitionist.
And if him and Abraham Lincoln were talking,
they would probably talk about, I don't know, race in a pretty critical manner.
And you might get some theories from it, but I can't teach that here either.
And then I was just kind of just going to wing it for the day
and just let y'all watch a video. You know, everybody likes to just watch a movie.
And I was going to put on the epic rap battle between Frederick Douglass and Thomas Jefferson.
And then upon reviewing it, I realized that even in that,
it points out that Thomas Jefferson, who owned slaves, referred to slavery as a hideous blot.
He was pretty critical of the founding systems of this country as well.
Given the restrictions and the guidelines that are coming down from these politicians,
there's no way to teach history in Virginia. Not really.
So rather than run afoul of the law, I'm going to go somewhere else.
But today I'm going to leave you with a lesson from a different founder.
From one that doesn't get talked about a lot. From Thomas Paine,
the spiritual and philosophical founder of the revolution and therefore the United States.
He said that where knowledge is duty, ignorance is a crime.
The United States is supposed to be a government of the people.
You're supposed to self-govern. And if you're going to govern, you have to be knowledgeable.
It's a participatory system. Your duty is to be informed.
Your duty is to get as much knowledge as you can, because in essence,
you are supposed to be leading this country. At least in theory, that's how it's supposed to work.
However, the new laws coming out from our state politicians are making knowledge a crime
and ignorance a duty. You can't learn about this. You can't teach about this.
And the reason it's happening is because your parents gave up.
Your parents surrendered your education to a bunch of people who would profit
from you being ill-informed. They get to remain in power as long as you aren't educated.
Your parents surrendered. They gave up on their duty to be informed, to be knowledgeable.
They committed the crime of ignorance. But rather than admit that,
and rather than take the responsibility seriously and become informed,
they're going to allow the politicians to make it a crime for you to be knowledgeable,
for you to make a better system, for you to change the world.
That's going to be illegal, and your duty will be to remain ignorant.
So if you don't know, I'm not really a teacher in Virginia, but this is real.
There's a bill in Virginia right now that would require teachers to teach something that never occurred.
People in the state government there have put forth a bill requiring teachers to teach a debate that never happened.
And you can say it's a typo, but think about how many people have seen this. It's not a typo.
They don't know. They have no clue what they're talking about,
and they do not know what it takes to educate.
The people of Virginia should probably take a long, hard look at how they were manipulated,
and how you were scared into abandoning your duty to be informed.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}