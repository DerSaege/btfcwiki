---
title: Let's talk about Tucker, the lost and found, and the way it is....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8XyOZDvpKBE) |
| Published | 2022/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the lost and found, Tucker Carlson, excuses, and silly things worldwide.
- People involved in the Capitol incident on January 6 tried to recover items they dropped.
- Tucker Carlson spun their actions as evidence of innocence, claiming they didn't know they were breaking laws.
- Beau disagrees with this excuse, mentioning a similar incident from 1993 involving a rented truck.
- Individuals at the Capitol on January 6 were misled by rhetoric about patriotism and history.
- They were unaware of the gravity of their actions, thinking they were fighting for their country.
- Beau questions who fed them this rhetoric and left them ill-informed.
- There was a disconnect between their actions and the consequences, as seen by interactions with law enforcement.
- Beau challenges the notion that they were innocent due to lack of understanding.
- Concludes by urging to question those who influenced and misled them.

### Quotes

- "Tucker Carlson spun their actions as evidence of innocence."
- "There was a disconnect between their actions and the consequences."
- "Who fed them that rhetoric? Who left them ill-informed?"

### Oneliner

Beau exposes the dangerous influence of misinformation and false patriotism leading individuals astray in the Capitol incident aftermath.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Challenge misinformation sources and hold them accountable (implied)
- Educate others on the dangers of false narratives and rhetoric (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the consequences of misinformation and blind patriotism, urging accountability and education.

### Tags

#Misinformation #Patriotism #CapitolIncident #Accountability #Rhetoric


## Transcript
Well, howdy there internet people. It's Beau again.
So today, we are going to talk about the lost and found.
We're gonna talk about the lost and found.
And Tucker Carlson.
And excuses.
And doing silly things.
And the way things are all over the world.
And the way they've been for a very, very long time.
If you don't know, some of the people involved in the recent unpleasantness at the Capitol
on the 6th.
Afterward, they called the lost and found and tried to recover items that they might
have dropped during their visit.
Of course, this kind of got turned into evidence and in some cases, I guess, led to their arrest.
Tucker Carlson got on his show and tried to spin that into evidence that they didn't think
they had done anything wrong.
They obviously were unaware of what was happening.
They didn't know they had broken any laws.
Otherwise, they wouldn't have done that.
No, no, I don't think he should get off that easy, to be honest.
I do not think that Tucker Carlson should get off that easy.
See, here's the thing.
This excuse, this idea that, oh, this person did this silly thing right after an incident.
That kind of shows that they didn't think they did anything wrong.
I've heard that before.
I heard it once about an incident that occurred way back in 1993.
A guy, he rented a rider truck and he did something horrible with that truck.
In the process, that truck got blown up.
Then he went back and tried to get his security deposit back for that truck.
Using Tucker's logic, that guy didn't think he broke the law.
He didn't think he'd done anything wrong, right?
What's the reality?
The reality is that he didn't understand the way things worked.
He didn't understand the gravity of the situation.
Because while he didn't have a clear understanding of anything because he was ill-informed, he
did have a bunch of rhetoric that had been fed to him his whole life, telling him that
he was in a struggle, that this is what he needed to do to protect his way of life, that
this is how he could be a real patriot, that this is how he could earn his place in history.
Right?
Didn't work out so well for him, by the way.
There are a lot of people who were there on the 6th who did not understand the gravity
of the situation, who didn't really understand what they were involved in, didn't understand
that they were really just cover for something else.
They didn't get any of that.
They didn't understand the way things worked.
But they did have people fill them with a lot of rhetoric, telling them that they were
in a struggle for their very way of life, for their country, that this is how they could
be a patriot, just like 1776, that this is how they could earn their place in history.
So the same thing all over the world.
A bunch of rich people using those who they could con and sending them in the breach to
do their dirty work for them.
All over the world it works that way.
It's worked that way forever.
There are people who participated in the 6th, actually had physical altercations with law
enforcement, got pepper sprayed, and then when they were walking away, they like shouted
their name to live streamers.
Because there was such a disconnect.
Because of the rhetoric, because of what they had been fed.
They didn't even seem to think that if you have an incident with law enforcement in which
they pepper spray you, that they might come looking for you later.
They were uninformed.
Where do they get their news, Tucker?
I do not think that it's enough to just sit there and say, oh, well, they didn't understand
that they were completely innocent.
And I get it.
I get wanting to try to help them.
Because I think on some level, there are a whole lot of people in the media who know
that those people were there because of the stuff they said for writings.
But no, just because they did something silly afterward doesn't mean they don't understand
what they did at the time.
Doesn't mean that they weren't aware they were breaking the law.
I don't think that just simply saying that they were too dumb to know what they had done
is going to clear the slate.
The reality is, we have to ask who fed them that rhetoric?
Who left them that ill-informed?
Who told them that they needed to be patriots and earn their place in history and that their
way of life was being taken away and that they needed to get involved with the struggle?
And if you speak Arabic, yeah, I'm using that word on purpose.
Because it's the same thing.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}