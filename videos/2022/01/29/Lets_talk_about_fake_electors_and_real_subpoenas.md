---
title: Let's talk about fake electors and real subpoenas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kTOzrNzSO1M) |
| Published | 2022/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The committee investigating the sixth has issued 14 subpoenas in seven states for individuals identified as alternate electors.
- Politicians may not fully comprehend the laws they have passed, making statements asserting their actions were legal in support of Trump's endeavors.
- Beau provides an analogy involving Bob and Jim to illustrate how seemingly legal actions, when done in furtherance of illegal activities, can lead to criminal liability.
- There is no specific statute for attempting a self-coup, but criminal charges could be brought based on related concepts.
- Individuals who claimed their actions were legal in supporting Trump's election overturn might face legal consequences.
- Beau acknowledges his layman status and uncertainty, offering his perspective as a thought rather than legal advice.

### Quotes

- "I'm starting to think that some of these politicians, they may not understand some of the laws that they have passed."
- "And I only did this so Trump could do what he's doing."
- "What I did was legal. And I did it to support the president who was trying to overturn the election."
- "That may have not been a sound legal strategy."
- "I could be wrong about this. It's just a thought."

### Oneliner

The committee issues subpoenas for alternate electors as politicians assert legality in supporting Trump's actions, potentially facing legal consequences.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Contact legal experts for clarification on potential legal liabilities (suggested)
- Join organizations advocating for legal accountability in political actions (implied)

### Whats missing in summary

Insights on potential legal implications and accountability for individuals involved in supporting actions to overturn elections.

### Tags

#Subpoenas #LegalConsequences #PoliticalActions #Trump #ElectionFraud


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about fake electors
and real subpoenas.
The committee, looking into the sixth,
has issued 14 subpoenas for people in seven states.
They are the top two people who submitted,
well, let's just call them alternate electors.
We'll be polite for the moment.
Now, this seems to be yet another step in what appears to be the committee trying to
build a very, very large case.
We can assume that the Department of Justice is running a parallel investigation, probably
along very similar lines.
I'm starting to think that some of these politicians,
they may not understand some of the laws
that they have passed because there are some weird statements
being said, things like, well, what I did was legal.
I did this act, and that's a legal act.
And I did this so Trump could do his thing.
And that's their statement.
And that sounds good and all, but let's
Let's just put that into a different context for a second.
Let's say Bob, Bob, well, we're going to be nice with Bob too.
We'll say that Bob wanted to start a, well, let's call it a secretive gardening business.
He wanted to grow something and sell something that may not should be grown and sold in that
particular jurisdiction.
But see, the local cops, they know Bob and they're always watching Bob.
So he doesn't want to go rent a house in his name and let people know what he's doing.
So he reaches out to his friend named Jim, and he's like, hey Jim, look, you do me a
favor now, I'll do you one later.
Can you rent this house for me because I want to do my little secretive gardening club?
And Jim says yes.
Now all Jim does is go and rent a house.
That's a legal act.
There's nothing illegal about renting a house.
just filling out some paperwork. But if you rent that house in furtherance of a
secretive gardening club, I would imagine that the way the laws are structured,
you might have some criminal liability. And there are a whole lot of Einsteins
right now going on TV to say, hey I did this this completely legal act, don't
look at me. And I only did this so Trump could do what he's doing. And it does
appear that what Trump was attempting to do was affect a self-coup. Now there's
not a statute for that, but there are a whole bunch of criminal charges that
could be leveled around that concept. And if you committed a legal act in
In furtherance of that, you might also have some criminal liability.
I think that may be something we see play out.
A whole lot of people that are going to be really upset that they got on TV and said,
hey, I did this.
What I did was legal.
And I did it to support the president who was trying to overturn the election.
That may have not been a sound legal strategy.
But all that being said, I'm a layman.
I'm not a lawyer.
I don't know.
I could be wrong about this.
It's just a thought.
Anyway, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}