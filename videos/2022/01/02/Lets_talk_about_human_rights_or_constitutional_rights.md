---
title: Let's talk about human rights or constitutional rights....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zkxKYLqC2CI) |
| Published | 2022/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Settling a debate between a father and daughter over constitutional rights and human rights.
- Daughter believes in Universal Declaration of Human Rights, father believes in US founding documents.
- Father argues that Constitution gives authority to sign the declaration, daughter thinks Constitution is outdated.
- Beau appreciates the beautiful language in the US founding documents but acknowledges flaws.
- Points out the similarities between the Universal Declaration of Human Rights and the US Constitution.
- Notes that both documents acknowledge existing rights rather than granting them.
- Suggests that if people read each other's preferred document, the debate might not exist.
- Advocates for choosing the document that provides the most rights in any given situation.
- Emphasizes that both human rights and constitutional rights aim to protect individuals from government actions.
- Encourages the family to have a night to compare and understand both documents.

### Quotes

- "At the end of the day, human rights are constitutional rights. There's not a difference."
- "I don't believe that either one of you have read the others preferred document."
- "Both of these documents are intended to shield people from the actions of their governments."
- "I don't really think this should be an argument, to be honest."
- "Y'all should plan a family night and sit down with both documents and see if you can figure out where they overlap."

### Oneliner

Settling a debate on constitutional vs. human rights, Beau finds common ground and encourages understanding.

### Audience

Father and daughter

### On-the-ground actions from transcript

- Plan a family night to sit down with both documents and compare where they overlap (suggested)

### Whats missing in summary

The full transcript provides a detailed comparison between the Universal Declaration of Human Rights and the US Constitution, urging for understanding and common ground.

### Tags

#Debate #HumanRights #ConstitutionalRights #Comparison #Understanding


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we're going to settle a discussion
that is taking place between a father and daughter.
Not really settle it, but provide a little bit of context
that might be needed that will hopefully
settle the discussion.
And in the process of that, we will
talk about which is more important, which
should take precedence, constitutional rights
human rights because we got this message. My daughter and I have been
arguing since Christmas. We need an arbiter. She believes the Universal
Declaration of Human Rights should take precedence in granting our rights and I
believe the US founding documents should take precedence in granting our rights.
My position is that the US Constitution provides the authority to sign the
declaration so it must come first and that I don't believe we should go beyond
the rights granted there. Her position is that the Constitution is dated and
written by a bunch of old white slave owners, and that we need a new
document because times have changed. She's read this to ensure I don't
misrepresent her position. There is one thing in this message that we definitely
want to talk about but generally this is just a wildly interesting debate that is
ultimately completely pointless. One thing I'm certain from this message is
that I don't believe that either one of you have read the others preferred
document and if you have I'm willing to bet you didn't compare them to your
preferred document. Me, you know, I love the language in the U.S. founding
documents. I really do. It's gorgeous, beautifully written. You know, all men are
created equal, endowed with certain inalienable rights, you know, rights that
are self-evident just the way it's written. It's beautiful. I mean, it would
be great if it was true when they wrote it, but the way it's written is great. But
But maybe she's right.
I mean, it was, in fact, written by a bunch of old white slave owners.
So maybe it could use an update.
If you were to update it, what might it sound like?
All human beings are born free and equal in dignity and rights.
They are endowed with reason and conscience and should act towards one another in a spirit
of brotherhood.
from the Universal Declaration of Human Rights.
I'm not going to go through every article,
but I'm going to do enough to prove the point.
Everyone has the right to life, liberty, and Americans right
now are saying pursue happiness, security of person.
But if you're secure in your person,
you can pursue happiness.
No one should be held in slavery or servitude.
No one shall be subjected to torture or to cruel, inhuman,
or degrading treatment or punishment.
I think in our documents, it just
says cruel and unusual punishment.
No one shall be subjected to arbitrary arrest, detention,
or exile because we need due process.
These aren't competing documents.
They're complementary. I'd be willing.
I mean, I've never actually worked out a percentage,
But I'm going to say 80 to 90% of what is in the Universal Declaration of Human Rights
is explicitly stated in the US Constitution.
And even the stuff that isn't explicitly stated
is still in the US Constitution through the 9th Amendment, which
says the enumeration of certain rights doesn't disparage other rights
held by the people, basically saying that, hey, just because we didn't
list a right here doesn't mean it doesn't exist. Because, and this is the
thing that definitely should be addressed, neither of these documents
grant rights. They don't say, hey these are your rights. The rights
are self-evident. These documents just acknowledge that they exist. That's how
they're supposed to work. I'd be willing to bet that if y'all sat down and read
each other's document, I don't think that this discussion will be taking place.
They're pretty much the same.
It's just updated language, different format, but they're very mirror.
I don't know that this should be a real debate.
Now if you're talking about legally, which one should take precedence?
I mean, sure, the Constitution being the thing that chartered the United States would be
the thing that takes precedence, but from a moral standpoint, from a standpoint of wanting
the maximum amount of freedom for the maximum amount of people, when it comes down to any
individual case, I would just side with whichever one provides the most rights, whichever one
shields it the best. I don't know that I would take a position that limits my own
rights in favor of a government, in favor of a state. I want people to
have the maximum amount of freedom they can have. Both of these documents are
are intended to shield people from the actions of their governments.
So I would just roll with whichever one suits me at the moment.
Not really a great legal decision there, but that's what I would do.
At the end of the day, human rights are constitutional rights.
There's not a difference.
These are rights that existed.
Now there are some in the Universal Declaration of Human Rights that have to do with quality
of life stuff, and people are going to say, that's not in the Constitution.
Yeah, the idea was, though, promoting the general welfare.
I don't really think this should be an argument, to be honest.
I think y'all should plan a family night and sit down with both documents and see if you
can figure out where they overlap.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}