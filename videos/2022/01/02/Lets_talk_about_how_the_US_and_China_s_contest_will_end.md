---
title: Let's talk about how the US and China's contest will end....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qFxiPDEUYSE) |
| Published | 2022/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the emerging near-peer contest between the United States and China in foreign policy.
- He outlines two main strategies for how such contests tend to resolve: fall in line or fall apart.
- Under the fall in line strategy, the U.S. aims to encourage China to accept second place through diplomacy and containment.
- The fall apart strategy involves the U.S. containing China tightly until internal pressures lead to the regime falling apart.
- Beau criticizes foreign policy pundits for presupposing a U.S. win without considering the Chinese response.
- He points out that U.S. foreign policy actions will have counteractions from China in this international "poker game."
- Beau expresses concerns about the U.S.' standing due to past actions like withdrawing from allies, impacting its ability to counter Chinese influence.
- He notes the internal divisions within the U.S., especially within the Republican Party, which may affect its approach to international leadership.
- Beau warns against assuming a U.S. win in this contest and stresses the need for a nuanced and cautious approach in dealing with China.
- He suggests that moving slowly and rebuilding trust globally is key for the U.S., but this approach may be at risk if certain factions within the Republican Party gain power.

### Quotes

- "Fall in line or fall apart."
- "U.S. foreign policy does not occur in a vacuum. Every move will have a counter move."
- "Moving slowly also assumes that the Republican Party won't obtain power anytime soon."
- "It's easy to make the assumption that we'll win because in near-peer contests the United States typically does."
- "They're assuming a U.S. win, and I don't know that there's grounds to make that assumption."

### Oneliner

Beau outlines the strategies of falling in line or falling apart in the emerging near-peer contest between the U.S. and China, cautioning against assuming a U.S. win and stressing the need for a nuanced approach.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Keep abreast of foreign policy developments and advocate for nuanced approaches (implied).
- Engage in critical analysis of international relations and question assumptions (implied).

### Whats missing in summary

Deeper analysis on the potential implications of the U.S.-China contest and the importance of adapting strategies based on current realities.

### Tags

#ForeignPolicy #US #China #InternationalRelations #Strategy #RepublicanParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk a little foreign policy again.
Semi-recently, we'd been discussing the developing near
peer contest that is emerging between the United States and
China.
Since then, a lot of people have been asking, how does
that end?
We can see that it's starting.
It's not starting.
has started. So people are wondering what happens next? What is the overall
strategy? How do near-peer contests tend to resolve themselves? There are two main
strategies, two main methods of conclusion, and the easiest way to sum it
up is fall in line or fall apart. Under the fall in line strategy, the United
States would look to China and try to encourage it to accept second place.
That's really what it boils down to.
It would use a lot of diplomacy, economic might, soft power to contain China and keep
it out of things that are historically U.S. spheres of influence.
If you're looking for a historical example, think containment, domino theory from the
first Cold War.
The idea then was to keep the Soviets out of other countries.
It's the same thing.
They would want to keep Chinese influence out of other countries.
The other method, so that's fall in line, fall apart is when the United States decides,
hey, China is not going to accept second place.
They're not going to fall into the traditional U.S.-led international order.
So we got to break them.
And this is where the U.S. takes all of its might, all of its diplomatic power, economic
power, soft power, and tries to box China in tightly rather than loosely and keep it
boxed in until internal pressures fracture it.
Things going on inside the country lead the regime to fall apart.
Think the end of the first Cold War.
That's what that looks like.
When you listen to foreign policy pundits in the United States, they talk about this.
However, something that I've noticed is that they presuppose a US win because the
United States has been the dominant power in the world since the end of World War II.
They're not, they don't really seem to be entertaining the Chinese response.
U.S. foreign policy does not occur in a vacuum.
Every move will have a counter move.
Remember, poker game, international poker game, and everybody's cheating.
The Chinese government isn't going to sit there and just allow the United States to
do these, to engage in this behavior.
They're going to respond.
And that's one of the more concerning things about it.
Because when you're talking about the fall in line aspect, because of the previous administration
withdrawing on our allies, we don't really have the best standing.
So the US saying, oh, don't side with the Chinese, don't worry, we'll protect you,
that track record shot.
The Chinese will be able to successfully counter that.
And then when it comes to the fall apart scenario, which country is on better footing domestically?
It's not the United States.
China doesn't have an opposition party that is questioning the very system of governance.
That's not a thing.
The Republican Party in the United States is obviously questioning the validity of democracy
itself in a lot of ways, but aside from that, if they were to obtain power, at least the
America first Trumpism side of things, they want to withdraw.
They don't want to be the international leader anymore.
They don't want to be on the foreign policy stage.
It's America first.
In a weird way that I don't want to say makes them allies because that makes it sound like
there's some kind of collusion and I have no evidence to suggest that.
But the interests of the Republican Party and the American first ideology aligns with
Chinese foreign policy.
mesh very well together.
The U.S. doesn't have an advantage like that when it comes to dealing with China.
The pundits that you hear discussing this, they seem to forget that the United States
had a really bad few years, and that bad few years really manifested itself overseas.
We sold out a lot of allies.
We didn't keep a lot of promises.
We pulled out of a lot of treaties.
It's going to take a long time to regain the world's trust there.
And then when it comes to the fall apart aspect, it's not China who has the internal divisions
that can be easily exploited, it's the United States.
The idea of presupposing a U.S. win, that's one of those things that could lead to catastrophe.
It's easy to make the assumption that we'll win because in near-peer contests the United
States typically does, historically speaking.
But you don't win the next contest by preparing for the last and doing the same thing that
you did last time, especially when the situations are dramatically different.
If the United States is smart, it wouldn't have started this, to be honest, but since
it has started, if they're going to play their cards right at that international poker
table, the best move is to move very, very slowly, because the United States has a lot
of making up to do all over the world. But moving slowly also assumes that the
Republican Party won't obtain power anytime soon. Because if the faction of
the Republican Party that supports America first, that Trumpists mentality,
But if it obtains power, especially in the executive branch, the Chinese government is
going to have free reign because the United States will be pulling back.
So to answer the direct questions, it ends with either fall in line or fall apart.
If you're listening to a lot of pundits talking about foreign policy today, just understand
that it is very noticeable that almost all of them are assuming before the conflict contest
??? I hope it doesn't turn into a conflict ??? before the contest even gets started,
they're assuming a U.S. win.
And I don't know that there's grounds to make that assumption.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}