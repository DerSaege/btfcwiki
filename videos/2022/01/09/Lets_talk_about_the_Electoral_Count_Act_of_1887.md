---
title: Let's talk about the Electoral Count Act of 1887....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=s-UYheM5bgY) |
| Published | 2022/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Bipartisan support for updating the Electoral Count Act of 1887 to modernize and clarify terms.
- Aim is to close perceived loopholes to prevent any legal cover for a potential self-coup.
- Not voting rights legislation, just updating a law to prevent misinterpretation.
- Anticipates wild theories emerging post-update, potentially feeding into conspiracy theories.
- Warns against allowing Republicans to frame it as voting rights legislation or Democrats to claim unrelated victories.
- Encouraged by bipartisan support indicating a rejection of attempted coups.
- Vice President's role seen as ceremonial, aiming to deter future coup attempts.

### Quotes

- "This is not voting rights legislation."
- "Don't allow Republicans to frame it as voting rights legislation and don't let Democrats use it as a way of saying, look, we did something completely unrelated."
- "They're updating old language in an old law, closing perceived loopholes."

### Oneliner

Bipartisan move to update Electoral Count Act aims to prevent legal cover for self-coups, not voting rights legislation.

### Audience

Legislators, activists, voters.

### On-the-ground actions from transcript

- Educate others on the distinction between updating laws and voting rights legislation (implied).
- Stay informed on the progress of the Electoral Count Act updates and advocate for transparency (generated).

### Whats missing in summary

The full transcript provides a nuanced understanding of the proposed update to the Electoral Count Act and warns against potential misinterpretations that may arise post-modernization.

### Tags

#ElectoralCountAct #BipartisanSupport #Modernization #LegalReforms #ConspiracyTheories


## Transcript
Well, howdy there, internet people, it's Bill again.
So today, we're going to talk a little bit about the Electoral Count Act of 1887.
Yeah, okay, so it appears that there is bipartisan support for taking this law,
updating the language in it, clarifying what some of the terms mean,
and just basically modernizing it.
closing perceived loopholes.
It looks like it has support from both sides of the aisle.
The idea is to get rid of any legal cover
that a self-coup might have in the future.
This is good.
There's nothing inherently wrong with this.
However, there are two things that we
have to be aware of because they are two byproducts that might come from this.
The first is to acknowledge right now, before this is even drafted, before they have really
done any work on it whatsoever, this is not voting rights legislation.
It's not the same thing.
It has to do with voting, but it is not voting rights legislation.
Has nothing to do with stopping voter suppression.
is updating a bill. That's all it is. It's updating a law, bringing it into modern
language so people don't look at it and see loopholes where nonexists. And that's
the other outcome. The other byproduct is just go ahead and brace yourself for the
wild theories to come, because it's going to feed into that.
It's going to make people say, oh, see, look,
they had to update this law.
If only Vice President Pence had stood
in the middle of the Senate floor in a circle of salt,
stood on one foot, and hopped around,
and said the name of each state backwards,
well, then those votes wouldn't have counted.
You know it's coming.
So go ahead and prepare yourself for that and acknowledge now this is not voting rights
legislation.
Don't allow Republicans to frame it as voting rights legislation and don't let Democrats
use it as a way of saying, look, we did something completely unrelated.
They're not the same thing.
They don't have the same outcomes.
They have nothing to do with each other.
It is slightly encouraging that this does appear to have support from both sides of
the aisle.
It does appear that once drafted, this will pass.
That means that at least there are some, a few, members of the Republican Party who aren't
cool with attempted coups, which is is good. I mean, I have had my doubts over
last year. So when people start talking about this, just understand what it is.
They're updating old language in an old law, closing perceived loopholes. They'll
make it very clear in the new legislation. I'm sure that the vice
president is there in a capacity that is mostly ceremonial and it's just to
deter future would-be Trumps.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}