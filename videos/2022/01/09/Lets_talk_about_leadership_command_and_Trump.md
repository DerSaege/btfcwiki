---
title: Let's talk about leadership, command, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L1xaaXNbviQ) |
| Published | 2022/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Leadership and command are distinct; leadership is found on the left while command is prevalent on the right due to hierarchy and perceived authority.
- Command is built little by little through small, seemingly insignificant orders that establish compliance and build towards following larger orders.
- The essence of command lies in obeying perceived authority rather than the action itself.
- A key rule of command is to never issue an order that won't be obeyed, as it undermines authority.
- Former President Trump's commands are increasingly being ignored, with some even leading to him being booed.
- Trump's recent command for followers to leave Twitter and Facebook due to being "radical left" is likely to be disregarded.
- If Trump continues down this path, he risks rendering himself irrelevant, becoming just an elderly person shouting at the internet.
- The question arises of who will assume command after Trump and capitalize on the movement that surrounded him.
- Beau suggests watching for prominent Republican figures, potentially governors, who can effectively give and have commands followed as potential successors to Trump.

### Quotes

- "It doesn't matter because it's not the action that matters. It's obeying that perceived authority."
- "Former President Trump has been issuing a lot of commands lately and a lot of them are being ignored."
- "If former President Trump continues on his current path, he will order himself into an irrelevant position."

### Oneliner

Leadership vs. command: Beau dissects the distinction, Trump's waning authority, and potential successors in the political landscape.

### Audience

Political Observers

### On-the-ground actions from transcript

- Watch for prominent Republican figures giving and having commands followed as potential successors to Trump (implied).

### Whats missing in summary

Further insights on the implications of shifting command dynamics and the impact on political movements.

### Tags

#Leadership #Command #Hierarchy #PerceivedAuthority #PoliticalMovements


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
leadership and command and former President Trump. You know the terms
leadership and command, they get used interchangeably a lot. They don't mean
the same thing and you don't find them in the same places. On the left you don't
find command, not really. You find leadership but you don't find command
because it runs counter to the ideology. On the left things are supposed to be
egalitarian. Everybody's supposed to be equal. Command comes from a perceived
authority, comes from hierarchy. So you don't find it there very much. Leadership
well it can come from the bottom so that's okay. On the right, well it's
pretty much all command because it's hierarchical. That hierarchy, that
perceived authority, those people on the top telling those people on the bottom
what to do and the people on the bottom doing it. How do you build your command?
How do you get that perception of authority? Little by little. If you talk to somebody who
was in the military, most of them are going to tell you about a commander who showed up and
made a whole bunch of small changes when they arrived. Little things, silly things,
ridiculous things that didn't make any sense but at the same time, well it was
pretty easy to comply with the orders. If you comply with the smaller orders, you do
what you're told there, you'll follow the bigger orders later. That's how a lot of new
commanders try to build their commands. You know it doesn't take a lot to make
sure all the mats by the door, the grain is going the same way, or the fire
extinguishers get checked once a week or painted once a month, don't walk on the
grass, whatever. It doesn't matter because it's not the action that matters. It's
obeying that perceived authority. See the thing is the opposite is true too. The
first rule of command is to never give an order that isn't going to be obeyed.
You never give an order that won't be or can't be followed because it undermines
your command. Because just like if you start following orders from somebody,
you're likely to continue. If you start ignoring them, well their authority, you
realize it's just a perception. Former President Trump has been issuing a lot
of commands lately and a lot of them are being ignored. Some of them are leading
to him being booed. His latest command was to tell his followers that they
needed to get off of Twitter and Facebook because Twitter and Facebook
are radical left. You know, I'm sure that when you think radical left you think
giant multi-billion dollar capitalist corporations. You know, aside from that
being a really weird statement, his followers aren't going to obey that
command. That hierarchy that they need, that's where they get it. They love
social media more than they love him. They'll ignore this. They're not going to
give up social media and they probably won't move to the one that he prefers.
If former President Trump continues on his current path, he will order himself
into an irrelevant position. He will become an old man yelling at the
internet. But that opens up another question. Who takes over the mantle of
command after him? Once his perceived authority is gone, who's going to try to
take over? Who's next in line? I have a couple of ideas on who it would be. I would
start watching for prominent Republican figures, perhaps maybe a governor, to
start giving small commands and see how people respond to them. See how the right
responds to them. Those politicians who are capable of giving those small
commands up front and having them followed, those are the ones you need to
look at as the replacement for Trump, as the next Trump, the next person that's
going to try to tap into the movement that arose around him, that he was able
to capitalize on, because former President Trump is losing his command.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}