---
title: Let's talk about the DHS cybersecurity bulletin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kQhENIpexzw) |
| Published | 2022/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Homeland Security issued a bulletin warning about the possibility of Russian cyber attacks on the United States if tensions in Ukraine escalate.
- The bulletin suggests that a cyber attack could disrupt civilian infrastructure in the US.
- Beau confirms that such a scenario is indeed possible given the current state of technology.
- Hardening systems may not be effective as cyber attacks often exploit human vulnerabilities rather than technological weaknesses.
- International treaties could be a way to address cyber threats, but smaller countries see cyber capabilities as a cost-effective deterrent against larger powers.
- Beau criticizes the US's reluctance to lead in international treaties due to a perceived threat to sovereignty.
- He suggests classifying cyber attacks on civilian infrastructure as war crimes to deter state actors from engaging in such activities.
- While major powers like Russia may not initiate devastating cyber attacks to avoid strong US responses, disruptions to critical infrastructure are still possible.
- Beau acknowledges the unprecedented nature of this cyber warfare era compared to the Cold War but believes doomsday scenarios are unlikely.
- He ends by stressing the importance of recognizing the interconnected nature of the world to effectively address cyber threats.

### Quotes

- "You know, if things kind of heat up to any real measurable level in Ukraine, it is kind of likely that the Russians launch a cyber attack on the United States that would disrupt civilian infrastructure."
- "There's a human component that can always be defeated. It doesn't matter how good everything else is."
- "The real solution here is to classify it by international convention as an act of war. And if it targets civilian infrastructure, it is a war crime."
- "But as far as disrupting pipelines or wastewater treatment, something like that, for a limited time and limited scope, oh yeah, that could happen."
- "Until then, this is a risk."

### Oneliner

Homeland Security warns of potential Russian cyber attacks on the US, stressing the human vulnerability factor and the need for international cooperation to classify such attacks as war crimes.

### Audience

Security analysts, policymakers

### On-the-ground actions from transcript

- Advocate for international conventions to classify cyber attacks on civilian infrastructure as war crimes (implied)
- Push for stronger cybersecurity measures in critical infrastructure (implied)

### Whats missing in summary

The full transcript expands on the implications of cyber warfare and the challenges in addressing this modern threat landscape.

### Tags

#CyberSecurity #HomelandSecurity #InternationalRelations #CyberWarfare #PolicyResponse


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we will talk about the bulletin, what it means, what Homeland Security is concerned
about, and whether or not it's a real possibility.
So today we will be talking about cyber attacks.
If you missed the news, Homeland Security put out a bulletin that basically said, hey,
you know, if things kind of heat up to any real measurable level in Ukraine, it is a
it's kind of likely that the Russians launch a cyber attack on the United States that would
disrupt civilian infrastructure.
Is that true?
Yeah.
Oh, yeah.
And there's not much that in our current state that could be done about it.
There are a lot of people right now asking, well, why do they have this capability?
We have the same one.
There aren't many ways to deal with this, because when it comes to the actual technology,
one route would be to try to harden our systems and they will try to find a way around it.
If we go back and forth, cat and mouse, and it will become a cyber arms race type of thing,
which in and of itself isn't really going to be that effective, because if you talk
to anybody that has dealt with physical security, they can tell you that, you know, that building,
it may have a four million dollar camera system, but you don't have to defeat the four million
dollar camera system.
You have to defeat the person getting paid thirteen dollars an hour to watch it.
Cybersecurity is kind of the same way.
There's a human component that can always be defeated.
It doesn't matter how good everything else is.
So this risk will exist.
Now as far as getting rid of it, sure, you could do it via treaty rather than arms race.
However, there are a couple of issues with that.
One for smaller countries.
This is a super cost effective way to maintain a deterrent.
This isn't something that is incredibly hard.
It's not like procuring, you know, starting a nuclear program or anything like that.
This is relatively cheap and relatively easy to do, to build this type of capability.
And it gives them a deterrent against larger powers.
So they're not going to be in favor of it right out of the gate.
The United States is not in a position to lead because of our national psychology.
You know, we have kind of adopted the view as a country that any international treaty
is somehow an affront to our sovereignty.
I am 100% certain that if the Geneva Conventions were proposed today, we would have people
waving Gadsden flags in Lafayette Park out there in front of the White House talking
about how we need to preserve our right to commit war crimes.
Until we realize that we do live in an interconnected world and we have to start acting as such,
this is going to remain a real threat to the United States.
The real solution here is to classify it by international convention as an act of war.
And if it targets civilian infrastructure, it is a war crime.
That's where the safety, some element of safety, can come from as far as from major powers,
from state actors.
Until then, this is a risk.
Now just like with anything else, when you're talking about near peers, when you're talking
about great powers, Russia isn't going to want to, they're not going to want to do anything
too devastating because that would prompt a response.
So while it's a possibility, the doomsday scenarios that you're hearing, those are kind
of unlikely.
But as far as disrupting pipelines or wastewater treatment, something like that, for a limited
time and limited scope, oh yeah, that could happen.
That could definitely happen.
But again, that has to be tempered with how far are they willing to go based on the likelihood
of a U.S. response.
So this is a new front.
There's not anything like this historically in the first Cold War to compare it to.
But the same principles apply.
So it's possible, and this is kind of likely really, but it's probably not going to go
to the more wild scenarios and wild levels that are being discussed in some of the media
right now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}