---
title: Let's talk about how George Floyd may transform the country again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Qkq7drX4gnI) |
| Published | 2022/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a federal case currently ongoing involving three officers with Derek Chauvin.
- Mentions the defense's strategy of claiming they were following Chauvin's orders.
- Criticizes the defense's reliance on the "just following orders" excuse.
- Points out that officers should not prioritize their careers over justice.
- Predicts that this defense may be used by higher-ranking individuals involved in the events of January 6th.
- Suggests that if a jury doesn't hold these officers accountable, there may be a backlash.
- Speculates on the potential transformative impact of George Floyd's death on future trials.
- Raises the possibility that the outcome of this trial could influence the future of the United States as a democracy.

### Quotes

- "If you're willing to allow what happened to Floyd to happen for the sake of your career, you probably shouldn't be a cop."
- "My guess is that there's going to be a backlash."
- "It's wild to think that his death may be even more transformative for this country."
- "Putting a lot on one person there."
- "Y'all have a good day."

### Oneliner

Beau explains how the defense strategy in George Floyd-related trials may impact the future of accountability and democracy in the United States.

### Audience

Jury members, activists, citizens.

### On-the-ground actions from transcript

- Hold law enforcement accountable (exemplified).
- Advocate for police reform (implied).
- Support systemic changes (implied).

### Whats missing in summary

Full context and depth of Beau's analysis on the impact of George Floyd's case on the ongoing federal trial and future implications. 

### Tags

#GeorgeFloyd #Accountability #PoliceReform #Justice #Democracy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a federal case that's currently ongoing
and how George Floyd may prove to be even more influential.
How the case surrounding him may change the country even further.
So if you don't know, three officers who were with Chauvin that day,
they're going through trial right now.
A federal case saying that y'all violated his rights.
That's what the feds are proving, attempting to prove.
The defense is attempting to basically use a defense that is leaning heavily
on the idea that Chauvin called the shots and he had a whole lot of control
over their future because he was the most senior person on the scene.
Now there's a whole lot of commentary to be made on this defense.
Generally speaking, we view, I was just following orders,
as not always a proper defense.
And aside from that, there's the commentary that they were concerned
about their future in the department.
And if you're willing to allow what happened to Floyd to happen
for the sake of your career, you probably shouldn't be a cop.
So there's plenty of commentary even beyond that to be made in depth.
But I want to point something else out.
This defense, what we're seeing play out in this case,
it's going to be the defense used by the upper echelons of those people
responsible for the events of the 6th.
That's what they're going to say.
Their defense will be that the senior person on the scene told them to do it.
And they were just following orders.
And this person had all the power and they were in control of their future.
That's going to be the defense.
In many cases, it's going to be the only defense they have,
assuming it gets to that stage.
So what we're seeing in this trial is kind of a preview of that.
And we're going to see how a jury responds.
What do you think will happen if a jury doesn't hold these officers
accountable after using this defense?
My guess is that there's going to be a backlash.
There would be some kind of backlash.
And that may mean that that defense won't work in the future.
In a very bizarre way, I can envision a scenario where the death of George Floyd
ends up influencing the outcome of trials related to the 6th.
It's wild to think that his death may be even more transformative for this country.
Because for a lot of people, that was the moment when they couldn't deny it anymore.
That was the moment when they couldn't say, oh, this doesn't happen.
Oh, it's not really that big of a deal.
It doesn't occur that often because it shoved it in everybody's face.
It is just, it's wild to think that the outcome of this trial
and how it plays out may significantly impact trials that, in my opinion,
will probably determine how long the United States remains a representative democracy.
Putting a lot on one person there.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}