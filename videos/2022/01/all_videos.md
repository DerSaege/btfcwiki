# All videos from January, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-01-31: Let's talk about Trump going all in this weekend.... (<a href="https://youtube.com/watch?v=YrpIisESY5k">watch</a> || <a href="/videos/2022/01/31/Lets_talk_about_Trump_going_all_in_this_weekend">transcript &amp; editable summary</a>)

Former President Trump breaks from playbook, hinting at pardons and admitting desire to change election results, potentially indicating criminal liability and slipping base support.

</summary>

"If you know there's a con man in the room but you don't know who the mark is, you're the mark."
"Trump is not a planner, so when he's not following somebody else's plan, he's probably really likely to make some mistakes."
"He may even incriminate himself when it comes to some of the things that he might have criminal liability for."

### AI summary (High error rate! Edit errors on video page)

Former President Trump has broken from his typical playbook by indicating he might grant pardons to those involved in the January 6th actions if he were to be re-elected.
Trump's recent actions, including hinting at potential pardons and suggesting he hopes for nationwide protests if criminal charges are brought against him, are attempts to cast investigations into him as political rather than legal.
Despite indications from Trump, there is bipartisan support to update the Electoral Act of 1887, not change election results.
Trump's statements about Vice President Mike Pence's role in the election results read like a confession to some, as he suggests Pence had the power to overturn the election.
Trump's deviation from the typical playbook of wannabe dictators, by openly admitting to wanting to change election results, indicates potential criminal liability and a slipping standing with his base.

Actions:

for political observers, concerned citizens,
Contact representatives to support updating the Electoral Act of 1887 (suggested)
Stay informed about political developments and hold leaders accountable (exemplified)
</details>
<details>
<summary>
2022-01-31: Let's talk about Massie's absurd Voltaire quote.... (<a href="https://youtube.com/watch?v=xfQdI6ZXBL8">watch</a> || <a href="/videos/2022/01/31/Lets_talk_about_Massie_s_absurd_Voltaire_quote">transcript &amp; editable summary</a>)

Beau explains the misattributed quote by Massey, urging to look out for those manipulating fear and blame to identify true rulers.

</summary>

"Those who can make you believe absurdities can make you commit atrocities."
"Watch for people who are trying to make you feel like a victim when you're not."
"Watch for those who are trying to scapegoat individuals or groups of people."
"That's how you find out who is trying to rule over you."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Representative Thomas Massey of Kentucky mistakenly tweeted a misattributed quote that has been widely circulated.
The misattributed quote implied that criticizing a certain individual indicates who is ruling over you.
Massey's attempt to criticize Fauci using this quote doesn't hold up logically because he openly criticizes Fauci, like many others.
Beau points out that the true way to identify those trying to rule over you is by recognizing those who spread fear, blame, powerlessness, and victimhood.
Beau suggests looking out for those who try to make you scapegoat others and direct your anger towards them as a means of control.
The quote wrongly attributed to Voltaire about criticizing rulers doesn't make sense in the context of identifying true rulers.
Beau stresses the importance of not falling for absurd rhetoric that leads to atrocities and the manipulation of fear to gain control.
Beau concludes by encouraging critical thinking and awareness in recognizing true attempts at ruling over individuals.
The core message revolves around identifying manipulative tactics of fear and blame in order to understand who is truly attempting to rule over you.

Actions:

for critical thinkers,
Identify and challenge fear-mongering tactics in your community (implied).
Refrain from falling into scapegoating behaviors and encourage others to do the same (implied).
</details>
<details>
<summary>
2022-01-30: Let's talk about individualism, masculinity, and Spartans.... (<a href="https://youtube.com/watch?v=gcmNYvCh5ts">watch</a> || <a href="/videos/2022/01/30/Lets_talk_about_individualism_masculinity_and_Spartans">transcript &amp; editable summary</a>)

Beau addresses misconceptions about individualism and masculinity, advocating for a balance between strength as an individual and collective action while challenging traditional gender roles.

</summary>

"You're only as strong as your weakest link."
"The strength of the individual lends itself well to collective action."
"The general idea of separating things up between the masculine and feminine [...] it's going to be a failure."
"Just because the social contracts of today are shifting, it doesn't negate the fact that all roles have to be fulfilled."
"If you want to be a strong individual, embrace all aspects of it."

### AI summary (High error rate! Edit errors on video page)

Responds to a message about individualism, masculinity, femininity, and historical references.
Explains the misconception of individualism as a masculine trait.
Clarifies that the reference to "rule 303" is not related to Irish heritage.
Talks about the Spartans at Thermopylae and the balance between individualism and collective action.
Emphasizes the importance of collective networks and bargaining.
Challenges the notion of fighters being solely individualists.
Stresses the power of collective action over individual strength.
Argues against promoting aggression towards political enemies.
Advocates for inclusivity and embracing all aspects of society for progress.

Actions:

for community members, activists,
Build community networks and support collective bargaining (implied)
Embrace inclusivity and embrace all aspects of society for progress (implied)
</details>
<details>
<summary>
2022-01-30: Let's talk about Biden's executive order and Vanessa Guillen.... (<a href="https://youtube.com/watch?v=HSIX7sqq9N8">watch</a> || <a href="/videos/2022/01/30/Lets_talk_about_Biden_s_executive_order_and_Vanessa_Guillen">transcript &amp; editable summary</a>)

Biden's executive order in response to Vanessa Guillen case makes sexual harassment a crime under UCMJ, changing how cases are handled and investigated, a significant step in combatting military sexual harassment.

</summary>

"It is going to matter."
"This is something that will have a positive impact."
"While this doesn't actually bring justice to Vanessa Guillen, it may make the military a little bit more just."

### AI summary (High error rate! Edit errors on video page)

Biden signed an executive order in response to the Vanessa Guillen case at Fort Hood.
The executive order makes sexual harassment a crime under the UCMJ, changing the handling of such cases.
Previously, sexual harassment was handled within the chain of command, leading to issues like cover-ups and retaliation.
With the new order, if someone covers up harassment, they can end up in cuffs, and cases can now be pursued outside the chain of command.
Cases will now be investigated by CID instead of through administrative proceedings.
The executive order will immediately change how cases are handled and investigated.
It will likely be a significant tool in combatting sexual harassment in the military.
Tracking of allegations of retaliation will also be implemented, likely referred to CID.
CID has decided to increase the ratio of civilian investigators, which could lead to more effective investigations.
The pressure applied after the Vanessa Guillen case led to these changes.

Actions:

for advocates, military personnel,
Reach out to Senate Armed Services and DOD directly to keep pressure for positive changes (implied).
Support civilian investigators in handling cases effectively (implied).
</details>
<details>
<summary>
2022-01-29: Let's talk about the hunt for blue lightning.... (<a href="https://youtube.com/watch?v=KTX_7pTK7sw">watch</a> || <a href="/videos/2022/01/29/Lets_talk_about_the_hunt_for_blue_lightning">transcript &amp; editable summary</a>)

An F-35 lightning crashes into the South China Sea, sparking a high-stakes race to retrieve it before China gains a military edge, revealing strategic risks in near-peer contests.

</summary>

"This may prove to be an incredibly costly mistake for the US Navy."
"The United States is currently rushing to try to get this aircraft back off the ocean floor."
"The Chinese say they have no interest in that aircraft. They have said this publicly and not to put too fine a point on it, they are lying."
"Well, I mean not dry, it's underwater, but you know what I mean."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

An F-35 lightning crashed on the USS Coral Vinson in the South China Sea, falling off the carrier deck into the water and sinking.
The United States is urgently trying to recover the aircraft from the ocean floor to prevent the Chinese from reverse-engineering it for a significant military advantage.
Chinese denial of interest in the aircraft is doubted, with assumptions that they are likely seeking it covertly.
The Navy's closest salvage ship will take eight days to reach the crash site, by which time the black box's battery giving the aircraft's location signal will have expired.
The incident underlines the high-stakes competition between world powers, especially in near-peer contests, where such advanced technology is coveted.
The proximity of the crash to China amidst heightened tensions amplifies the significance of the situation.
The US Navy's delay in recovering the aircraft may have severe consequences, potentially being a costly mistake.
The F-35 is a sophisticated aircraft integrated with various systems, making it desirable for adversaries to study its capabilities and vulnerabilities.
The scenario sheds light on the strategic implications and risks associated with military accidents in sensitive geopolitical contexts.
Beau concludes with a contemplative remark on the potential repercussions of this incident and wishes the audience a good day.

Actions:

for military analysts, policymakers,
Monitor developments in military technology and international relations for implications (implied)
Support initiatives promoting transparency and accountability in military operations (implied)
</details>
<details>
<summary>
2022-01-29: Let's talk about fake electors and real subpoenas.... (<a href="https://youtube.com/watch?v=kTOzrNzSO1M">watch</a> || <a href="/videos/2022/01/29/Lets_talk_about_fake_electors_and_real_subpoenas">transcript &amp; editable summary</a>)

The committee issues subpoenas for alternate electors as politicians assert legality in supporting Trump's actions, potentially facing legal consequences.

</summary>

"I'm starting to think that some of these politicians, they may not understand some of the laws that they have passed."
"And I only did this so Trump could do what he's doing."
"What I did was legal. And I did it to support the president who was trying to overturn the election."
"That may have not been a sound legal strategy."
"I could be wrong about this. It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The committee investigating the sixth has issued 14 subpoenas in seven states for individuals identified as alternate electors.
Politicians may not fully comprehend the laws they have passed, making statements asserting their actions were legal in support of Trump's endeavors.
Beau provides an analogy involving Bob and Jim to illustrate how seemingly legal actions, when done in furtherance of illegal activities, can lead to criminal liability.
There is no specific statute for attempting a self-coup, but criminal charges could be brought based on related concepts.
Individuals who claimed their actions were legal in supporting Trump's election overturn might face legal consequences.
Beau acknowledges his layman status and uncertainty, offering his perspective as a thought rather than legal advice.

Actions:

for legal analysts,
Contact legal experts for clarification on potential legal liabilities (suggested)
Join organizations advocating for legal accountability in political actions (implied)
</details>
<details>
<summary>
2022-01-28: Let's talk about transplants, trust, and vaccines.... (<a href="https://youtube.com/watch?v=SfWkrqMGCD8">watch</a> || <a href="/videos/2022/01/28/Lets_talk_about_transplants_trust_and_vaccines">transcript &amp; editable summary</a>)

Renewed coverage of organ transplant vaccination requirements post-surgery is about trust in medical professionals and ensuring successful outcomes through post-transplant vaccinations.

</summary>

"Trust them when it comes to the shot."
"It's not punishment for not being vaccinated."
"After the transplant, you have to get the vaccine."
"Trust them with the aftercare as well."
"It's not news. It's been like this a long, long time."

### AI summary (High error rate! Edit errors on video page)

Renewed coverage of organ transplant vaccination requirement due to slow news week.
Vaccinations required post-transplant to increase success chances.
Immunosuppression post-transplant necessitates vaccines for natural immunity replacement.
Organ transplant scarcity leads to prioritizing recipients with higher success chances.
Trust in doctors for transplant surgery extends to post-transplant care and vaccination.
Vaccination post-transplant is not punishment but a standard procedure for successful outcomes.
Trust in medical professionals for surgery includes trust in vaccination recommendations.
Failure to vaccinate post-transplant could result in wasted surgery and missed opportunities for better-prepared recipients.
Vaccination post-transplant ensures the effectiveness of immunosuppressive drugs and the transplant's success.
Trusting doctors for complex medical procedures should extend to following vaccination recommendations.

Actions:

for medical patients,
Trust medical professionals' expertise in post-transplant care (implied).
Ensure compliance with vaccination recommendations post-transplant surgery (implied).
</details>
<details>
<summary>
2022-01-28: Let's talk about the Biden-Zelensky call and context on Ukraine.... (<a href="https://youtube.com/watch?v=QybEHXTLLtI">watch</a> || <a href="/videos/2022/01/28/Lets_talk_about_the_Biden-Zelensky_call_and_context_on_Ukraine">transcript &amp; editable summary</a>)

Beau explains the Biden-Zelensky call, criticizes false equivalencies, praises Zelensky's leadership, and warns about the implications of sensationalized media.

</summary>

"Keep calm and carry on."
"Zelensky has displayed more leadership than all of the other world leaders combined."
"It has been my contention for quite some time that Putin would like to take Ukraine without a shot."
"A lot of people's quest for ratings may quite literally shift the balance of power in Europe."
"No, the transcripts do not need to be released."

### AI summary (High error rate! Edit errors on video page)

Explains the Biden-Zelensky phone call and the differing intelligence estimates between heads of state.
Criticizes the right-wing outrage for drawing false equivalencies with Trump's Ukraine call.
Describes Zelensky's leadership in handling the situation with Russia poised on Ukraine's border.
Points out the damaging effects of discussing potential war on Ukraine's economy and resistance efforts.
Talks about the posturing between Russia, NATO, and Ukraine, and the importance of posture in geopolitical contests.
Raises concerns about US intelligence missing the bigger picture and the potential for Ukraine to become a buffer zone.
Suggests that Zelensky's willingness for bilateral talks with Russia may indicate a pragmatic approach to protect Ukraine.
Stresses the need for NATO commitment to stop Russian advances and criticizes sensationalized media coverage for its real-world implications.

Actions:

for foreign policy analysts,
Monitor and analyze geopolitical developments for accurate understanding and informed action (suggested).
Support efforts for peaceful resolutions and diplomatic negotiations (implied).
Advocate for responsible media coverage and avoid sensationalism in reporting (implied).
</details>
<details>
<summary>
2022-01-28: Let's talk about Deep Goat and releasing the transcript.... (<a href="https://youtube.com/watch?v=s6EvGgFPH3A">watch</a> || <a href="/videos/2022/01/28/Lets_talk_about_Deep_Goat_and_releasing_the_transcript">transcript &amp; editable summary</a>)

Deep Goat discreetly addresses the significance of understanding intent in intelligence work and subtly criticizes the risk of publicizing classified transcripts.

</summary>

"Intent is what is important. That's what matters in intelligence work."
"A detailed intent of two world leaders who were on the same side, valuable to the opposition intelligence service."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Deep Goat remains anonymous and discreet about their location, hinting at involvement in a sensitive topic.
Deep Goat is asked to create a video discussing the actions of different sides, particularly the Russians in relation to the United States and Ukraine.
The focus is on the importance of understanding intent in intelligence work, rather than just knowing the plans or developments.
Russian intelligence is suggested to be interested in knowing the intentions of the United States and Ukraine, especially post-high-level phone calls.
Possibilities for Russian intelligence to gather information include intercepting signals, having a human source, or manufacturing situations to obtain sensitive transcripts.
Deep Goat subtly criticizes the idea of public release of classified transcripts due to the risk it poses in revealing valuable intent information to foreign intelligence services.
The transcript ends with Deep Goat leaving the audience with a thought on the consequences of releasing detailed intent information.

Actions:

for intellectuals, analysts, policymakers,
Analyze intent and potential actions of various sides (implied)
Maintain discretion and confidentiality in sensitive matters (implied)
</details>
<details>
<summary>
2022-01-27: The roads to Career Day about starting a channel.... (<a href="https://youtube.com/watch?v=TQWvx0eHel4">watch</a> || <a href="/videos/2022/01/27/The_roads_to_Career_Day_about_starting_a_channel">transcript &amp; editable summary</a>)

Beau shares insights on starting a YouTube channel, stressing luck, content quality, and the need for a backup profession for financial security.

</summary>

"Luck. Luck. It's luck."
"Content is king. Always has been."
"YouTube probably isn't the route for you."
"I make the videos. I've tried not to overthink it."
"Do it as a hobby and let it turn into something."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a virtual career day where he answers student questions and tackles Twitter inquiries related to being a YouTuber.
Initially pushed into YouTubing, Beau shares how he hid his accent until it became a source of humor, leading him to start making videos.
Beau started his channel with a broken cell phone and a $110 Chromebook, stressing content over production quality at the beginning.
While some YouTubers make a living at 100k subscribers, ad rates vary wildly, and success often hinges on luck.
Luck is deemed the most critical factor in a successful YouTube channel, overshadowing advice on algorithm manipulation or content quality.
Beau doesn't get to choose pre or post-roll ads on his videos; he only selects sponsors for integrated segments.
Beau addresses the unpredictability of YouTube revenue, where some creators earn significantly more than others based on ad rates and content.
The key to a successful YouTube channel, according to Beau, is high-quality content that resonates with viewers, surpassing algorithms and production quality.
Beau shares his thick-skinned approach to handling toxic comments and the importance of moderating his comment section to foster inclusivity.
Beau estimates working 60 hours a week on his channel but stresses the need for a backup profession for financial security.

Actions:

for aspiring youtubers,
Start your YouTube channel as a hobby and let it grow organically (suggested).
Focus on creating high-quality content that resonates with your target audience (suggested).
Moderate your comment section to foster inclusivity and remove toxic comments (exemplified).
Have a backup profession for financial security (exemplified).
</details>
<details>
<summary>
2022-01-27: Let's talk about Maus, Tennessee, and failing a test.... (<a href="https://youtube.com/watch?v=4iR_Fk760ZE">watch</a> || <a href="/videos/2022/01/27/Lets_talk_about_Maus_Tennessee_and_failing_a_test">transcript &amp; editable summary</a>)

Beau sheds light on the true motives behind banning "Mouse" graphic novel, revealing a dangerous attempt to keep students ignorant of authoritarianism signs.

</summary>

"Banned books are the best books."
"Please do not take these arguments in good faith."

### AI summary (High error rate! Edit errors on video page)

School board in Tennessee decided to ban the graphic novel "Mouse," set in a dark period of history and told through animals, mice.
Author attempted to address objections in good faith, considering each argument.
Some arguments against the novel, like a mouse with no clothes, are not made in good faith.
Conservative circles' reactions to animated characters without clothes show the hypocrisy in these arguments.
The objections are not about the novel's content or theme; it's about something else.
Beau suggests that educators are hiding the failure to recognize rising authoritarianism in the country by banning such books.
The decision to ban the book is driven by a desire to preserve closely held beliefs and avoid embarrassment for failing to recognize authoritarian signs.
Beau believes the ban aims to keep students ignorant and incapable of recognizing authoritarianism.
Banning books like "Mouse" creates a generation blind to the signs of authoritarianism, leading to long-term impacts.
The real danger lies in creating a generation unable to identify and resist authoritarian rhetoric and actions.

Actions:

for students, educators, community members,
Challenge book bans in schools (suggested)
Advocate for diverse literature in education (suggested)
</details>
<details>
<summary>
2022-01-27: Let's talk about American dogs and Chinese yaks.... (<a href="https://youtube.com/watch?v=vtk7dbLsgJw">watch</a> || <a href="/videos/2022/01/27/Lets_talk_about_American_dogs_and_Chinese_yaks">transcript &amp; editable summary</a>)

Beau examines the development of weaponized robots, warning against fear-mongering and the escalation of military interventions driven by advancements in ground warfare technology.

</summary>

"We cannot allow there to be a robot gap."
"This is the future of ground warfare."
"Are there going to be boots on the ground? The answer will always be no. The reality is there will always be boots on the ground."
"Country doesn't do what you say. Well, you let slip the dogs and yaks of war."
"And with little risk to American or Chinese lives, the likelihood of using them increases a whole lot."

### AI summary (High error rate! Edit errors on video page)

Exploring the development of robotic technology between China and the United States.
Chinese state media released footage of testing a robot called "yak" designed for transporting supplies to troops in isolated areas.
The robot is likely to be weaponized, capable of carrying machine guns, grenade launchers, or anti-tank rockets.
Beau points out fear-mongering over the robot development and the potential for a "robot gap" between countries.
Compares the Chinese robot to the American Big Dog, noting differences in payload and speed.
Emphasizes that Chinese technology is about 17 years behind American technology in this aspect.
Beau warns against allowing fear to drive increased defense spending or justify military intervention using robotic technology.
Considers the implications of robotic warfare on global power dynamics, with major powers gaining more leverage over smaller or poorer nations.
Predicts that ground warfare is evolving towards a future resembling Star Wars with the introduction of weaponized robots.
Raises concerns about the political consequences of using robots in warfare and the detachment from human casualties in decision-making.

Actions:

for policy makers, activists, military personnel,
Advocate for international treaties or agreements to regulate the development and use of weaponized robots (implied).
Support organizations working towards demilitarization and peace-building efforts (implied).
</details>
<details>
<summary>
2022-01-26: Let's talk about the DHS cybersecurity bulletin.... (<a href="https://youtube.com/watch?v=kQhENIpexzw">watch</a> || <a href="/videos/2022/01/26/Lets_talk_about_the_DHS_cybersecurity_bulletin">transcript &amp; editable summary</a>)

Homeland Security warns of potential Russian cyber attacks on the US, stressing the human vulnerability factor and the need for international cooperation to classify such attacks as war crimes.

</summary>

"You know, if things kind of heat up to any real measurable level in Ukraine, it is kind of likely that the Russians launch a cyber attack on the United States that would disrupt civilian infrastructure."
"There's a human component that can always be defeated. It doesn't matter how good everything else is."
"The real solution here is to classify it by international convention as an act of war. And if it targets civilian infrastructure, it is a war crime."
"But as far as disrupting pipelines or wastewater treatment, something like that, for a limited time and limited scope, oh yeah, that could happen."
"Until then, this is a risk."

### AI summary (High error rate! Edit errors on video page)

Homeland Security issued a bulletin warning about the possibility of Russian cyber attacks on the United States if tensions in Ukraine escalate.
The bulletin suggests that a cyber attack could disrupt civilian infrastructure in the US.
Beau confirms that such a scenario is indeed possible given the current state of technology.
Hardening systems may not be effective as cyber attacks often exploit human vulnerabilities rather than technological weaknesses.
International treaties could be a way to address cyber threats, but smaller countries see cyber capabilities as a cost-effective deterrent against larger powers.
Beau criticizes the US's reluctance to lead in international treaties due to a perceived threat to sovereignty.
He suggests classifying cyber attacks on civilian infrastructure as war crimes to deter state actors from engaging in such activities.
While major powers like Russia may not initiate devastating cyber attacks to avoid strong US responses, disruptions to critical infrastructure are still possible.
Beau acknowledges the unprecedented nature of this cyber warfare era compared to the Cold War but believes doomsday scenarios are unlikely.
He ends by stressing the importance of recognizing the interconnected nature of the world to effectively address cyber threats.

Actions:

for security analysts, policymakers,
Advocate for international conventions to classify cyber attacks on civilian infrastructure as war crimes (implied)
Push for stronger cybersecurity measures in critical infrastructure (implied)
</details>
<details>
<summary>
2022-01-26: Let's talk about how George Floyd may transform the country again.... (<a href="https://youtube.com/watch?v=Qkq7drX4gnI">watch</a> || <a href="/videos/2022/01/26/Lets_talk_about_how_George_Floyd_may_transform_the_country_again">transcript &amp; editable summary</a>)

Beau explains how the defense strategy in George Floyd-related trials may impact the future of accountability and democracy in the United States.

</summary>

"If you're willing to allow what happened to Floyd to happen for the sake of your career, you probably shouldn't be a cop."
"My guess is that there's going to be a backlash."
"It's wild to think that his death may be even more transformative for this country."
"Putting a lot on one person there."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains a federal case currently ongoing involving three officers with Derek Chauvin.
Mentions the defense's strategy of claiming they were following Chauvin's orders.
Criticizes the defense's reliance on the "just following orders" excuse.
Points out that officers should not prioritize their careers over justice.
Predicts that this defense may be used by higher-ranking individuals involved in the events of January 6th.
Suggests that if a jury doesn't hold these officers accountable, there may be a backlash.
Speculates on the potential transformative impact of George Floyd's death on future trials.
Raises the possibility that the outcome of this trial could influence the future of the United States as a democracy.

Actions:

for jury members, activists, citizens.,
Hold law enforcement accountable (exemplified).
Advocate for police reform (implied).
Support systemic changes (implied).
</details>
<details>
<summary>
2022-01-25: Let's talk about Trump and the Georgia special grand jury.... (<a href="https://youtube.com/watch?v=RNgZWzvSxe8">watch</a> || <a href="/videos/2022/01/25/Lets_talk_about_Trump_and_the_Georgia_special_grand_jury">transcript &amp; editable summary</a>)

Fulton County in Georgia convenes a special grand jury to look into post-election disruptions, providing political cover and investigative tools without directly indicting Trump.

</summary>

"A special grand jury in Georgia is not a normal grand jury."
"If you walk in and you put your hand on that Bible and you swear before God that you're going to tell the truth, the whole truth and nothing but the truth, you had better do that."
"It has subpoena power. It has the ability to demand records from government agencies, stuff like that."
"So if you are sworn to tell the whole truth, well, that overrides everything."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Fulton County in Georgia is impaneling a special grand jury to look into disruptions after the election, including Trump's actions.
The special grand jury in Georgia won't indict Trump but can make recommendations to the prosecutor.
In Southern politics, there is a tradition of not pointing fingers at fellow party members, even if they broke the law.
A special grand jury in Georgia has subpoena power and can demand records from government agencies.
The grand jury will likely provide political cover for those who want to help with the investigation without risking their careers.
The prosecutor may have already decided to try to get Trump indicted, and the grand jury will help gather evidence for this.
The process is seen as more of an investigative tool rather than a means to directly indict Trump.
Beau believes that crimes were committed based on tape evidence, but the grand jury will determine the legal implications.
The grand jury convenes at the beginning of May and is good for a year, but Beau doesn't think it will take that long.

Actions:

for georgia residents,
Contact local organizations or legal aid to understand how you can support the process (suggested)
Attend community meetings or events to stay informed about the investigation (suggested)
</details>
<details>
<summary>
2022-01-25: Let's talk about Doocy, Biden, and hard-hitting journalism.... (<a href="https://youtube.com/watch?v=vvZHdUs3BqQ">watch</a> || <a href="/videos/2022/01/25/Lets_talk_about_Doocy_Biden_and_hard-hitting_journalism">transcript &amp; editable summary</a>)

Beau criticizes a "gotcha" question posed to President Biden, advocating for journalism that informs rather than incites.

</summary>

"Questions like this don't inform, which should be the purpose. They inflame."
"Journalists should be educators."
"If your news outlet is not informing you, it's inflaming you."

### AI summary (High error rate! Edit errors on video page)

A Fox News reporter shouted a question at President Biden as he was leaving, asking if high inflation is a political liability for the incumbent party.
President Biden responded, saying high inflation is a great asset, followed by a derogatory comment towards the reporter.
Beau criticizes the question as not being hard-hitting journalism but a "gotcha" question designed for a soundbite.
Beau believes journalism should provide new information and uncover things rather than aim for sensational soundbites.
He suggests that the purpose of journalism is to inform and not to incite.
Beau states that journalists should be educators, but many viewers prefer sensationalism over informative content.
He points out that both sides of the aisle have criticized the reporter, indicating a bipartisan agreement on the nature of the question.
While Beau admits finding humor in Biden's response, he acknowledges that it was not fitting behavior for the president.
Beau concludes by stating that news outlets that do not inform are actually inflaming their audience and serving their own agenda rather than providing a service.

Actions:

for journalists, news viewers,
Hold news outlets accountable for providing informative content (implied)
Support journalism that educates and informs the public (implied)
</details>
<details>
<summary>
2022-01-24: Let's talk about what the media is getting wrong about the 6th.... (<a href="https://youtube.com/watch?v=-WXj9j58D_k">watch</a> || <a href="/videos/2022/01/24/Lets_talk_about_what_the_media_is_getting_wrong_about_the_6th">transcript &amp; editable summary</a>)

Beau criticizes US journalism for failing its obligation to pursue truth, urging journalists to ask tough questions and hold people accountable.

</summary>

"Your job as a journalist is to open the window and look outside."
"Democracy doesn't die in the darkness."
"It's an obligation of journalism to pursue this, the whole idea of the public trust and all of that."
"These questions need to be asked."
"People need to be put on record about where they stood then and where they stand now."

### AI summary (High error rate! Edit errors on video page)

Talks about journalism in the United States failing in its obligation to uncover the truth.
Mentions the importance of journalists looking outside the window to see if it's raining or sunny, not just printing conflicting quotes.
Points out the current documentation revealing an attempt to overturn a free and fair election and an apparent coup.
Criticizes the lack of journalists asking political figures about their support for the former president's coup attempt.
Raises concerns about the media's focus on trivial details rather than pressing questions related to serious issues.
Emphasizes the danger of misinformation thriving in an information vacuum.
Calls for journalists to hold people accountable by asking tough questions and seeking the truth.
Questions why journalists are waiting for government press releases instead of actively pursuing answers.
Urges journalists to fulfill their obligation to pursue the truth and hold people accountable.
Expresses the need for individuals to be transparent about their stance during critical events.

Actions:

for journalists, news outlets,
Challenge journalists to ask tough questions and pursue the truth (implied).
Encourage news outlets to prioritize accountability over ratings and circulation (implied).
Advocate for transparency from individuals regarding their stance on critical events (implied).
</details>
<details>
<summary>
2022-01-24: Let's talk about the USS Pueblo, Ukraine, and the Cold War.... (<a href="https://youtube.com/watch?v=yBbtV3tgSBk">watch</a> || <a href="/videos/2022/01/24/Lets_talk_about_the_USS_Pueblo_Ukraine_and_the_Cold_War">transcript &amp; editable summary</a>)

Beau explains the Cold War dynamics, cautioning against dramatic narratives and discussing the USS Pueblo incident and Gary Powers, while comparing historical events to current tensions in Ukraine.

</summary>

"Great powers don't like to go to war."
"The sides trying to avoid war while gaining the upper hand."
"It's unlikely, but it's not impossible."
"Your dad and other old people are right."
"It's going to happen a lot in Ukraine."

### AI summary (High error rate! Edit errors on video page)

Explains the inaccurate narrative in teaching history in the US and focuses on specific points like the Cuban Missile Crisis.
Describes the Cold War as a period where major powers avoided direct war while jockeying for position.
Mentions the concept of mutually assured destruction (MAD) and how great powers try to avoid conventional conflict due to massive destruction.
Details the USS Pueblo incident in 1968, where North Korea seized a US Navy ship and held the crew captive for almost a year.
Talks about Gary Powers, who was captured by the Soviets in a U-2 spy plane incident.
Emphasizes that great powers like the US and Russia prefer to avoid direct conflicts.
Compares the current situation with Ukraine to historical Cold War dynamics, pointing out the front line shift to Kiev.
Clarifies that the media coverage may exaggerate troop movements near Ukraine for ratings.
Suggests that backing Ukrainian partisans in conflict with Russia is more likely than a direct US-Russia conflict.
Speculates on Putin's intentions, leaning towards the idea that he may be seeking a way to deescalate tensions in Ukraine.

Actions:

for history enthusiasts, policymakers, concerned citizens,
Brush up on history from 1945 to 1990 to understand the predominant theme of avoiding direct war while gaining advantages (suggested)
</details>
<details>
<summary>
2022-01-23: Let's talk about agents, influence, and cultural opinion makers.... (<a href="https://youtube.com/watch?v=VHVjEFZ0eG8">watch</a> || <a href="/videos/2022/01/23/Lets_talk_about_agents_influence_and_cultural_opinion_makers">transcript &amp; editable summary</a>)

Beau delves into the world of intelligence, discussing agents of influence and their significant impact on shaping public opinion and foreign policy.

</summary>

"An agent of influence is somebody who is a cultural opinion maker."
"Throughout this person's career, well, they just run the standard patriotic line the entire time."
"Once recruited, can end up creating more."
"They're often the most effective."
"They can shift foreign policy more than anything else."

### AI summary (High error rate! Edit errors on video page)

Delves into the world of intelligence, clarifying the terminology between "CIA agent" and "CIA officer," explaining the difference between an agent and an officer.
Mentions the overlooked yet valuable type of agent called an "agent of influence," individuals who can shift thought and influence public opinion.
Describes agents of influence in various fields like movies, art, academia, government, journalism, and daytime TV talk shows.
Explains that agents of influence can be recruited from individuals who may be disaffected or dissatisfied with their current status.
Points out the difficulty in identifying and catching agents of influence once recruited, as they know their role well.
Raises concerns about the heavy reliance on technical means of gathering intelligence, leading to a potential presence of agents of influence in the United States.
Suggests the possibility of non-state actors using similar tactics to influence politics within a country.
Emphasizes the significant impact agents of influence can have on shaping foreign policy, even though they may not be as glamorous as portrayed in movies.

Actions:

for intelligence enthusiasts,
Identify potential disaffected individuals who may be vulnerable to recruitment as agents of influence (implied).
Stay informed about the tactics used by agents of influence in different fields to recognize potential manipulation (implied).
</details>
<details>
<summary>
2022-01-23: Let's talk about Sinema, dollars, and sense.... (<a href="https://youtube.com/watch?v=EodwFF1SxJk">watch</a> || <a href="/videos/2022/01/23/Lets_talk_about_Sinema_dollars_and_sense">transcript &amp; editable summary</a>)

Senator Sinema faces backlash for opposing voting rights legislation and may need to reconsider her position due to pressure from donors and progressives organizing a primary challenge.

</summary>

"Sinema and her staff seek higher political office."
"Progressives in Arizona are organizing a primary challenge against Sinema."
"The Democratic Party is a coalition party with diverse ideologies."
"The pressure from donors might lead to a shift in Sinema's stance."
"Support from big donors is vital for winning elections."

### AI summary (High error rate! Edit errors on video page)

Senator Sinema has opposed creating an exemption in the filibuster for voting rights legislation, obstructing President Biden's agenda.
70 donors to her campaign, including many who gave the maximum allowable contribution, will withdraw support if she doesn't change her position.
Progressives in Arizona are organizing a primary challenge against Sinema.
Sinema and her staff aim for higher political office, potentially prioritizing funding for her future ambitions over voting rights legislation.
Sinema may view herself as a Democratic equivalent to Donald Trump, someone challenging the party's norms.
Democrats are a coalition party with diverse ideologies, making it challenging to position oneself as an outsider.
Sinema's strategy, heavily reliant on big donors, is likely to fail without their support.
The pressure from donors and other forms of backlash might lead to a shift in Sinema's stance.
Financial support is vital for winning elections, making it necessary for Sinema to reconsider her position.
Overall, Sinema faces significant challenges due to her stance on voting rights and opposition to party norms.

Actions:

for progressive activists,
Organize and support the primary challenge against Senator Sinema (exemplified)
Mobilize to pressure Senator Sinema to change her position on voting rights legislation (implied)
Donate to progressive candidates challenging incumbents who oppose voting rights (exemplified)
</details>
<details>
<summary>
2022-01-22: Let's talk about how the media covers foreign policy.... (<a href="https://youtube.com/watch?v=RqVS8yUCbY8">watch</a> || <a href="/videos/2022/01/22/Lets_talk_about_how_the_media_covers_foreign_policy">transcript &amp; editable summary</a>)

Beau presents a critical view of foreign policy, focusing on power dynamics and the absence of good versus bad actors, stressing the importance of supporting truth over narratives.

</summary>

"Foreign policy is a giant international poker game where everybody's cheating."
"It's not about good guys and bad guys. It's not about morality. It's not about right and wrong. It's about power."
"My default is to be against war. You have to convince me that it is absolutely necessary."
"If you want to support the troops, whichever side of whichever conflict, you have to support the truth first."
"It's just framing. It's about power, and anything that distracts from that is the actual motive."

### AI summary (High error rate! Edit errors on video page)

Received messages about different perspectives on Russian and U.S. postures in foreign policy.
Avoids injecting personal feelings into coverage to provide unbiased information.
Media tends to cheerlead and establish narratives rather than inform objectively.
Tucker Carlson's misunderstanding of NATO's purpose and history.
NATO's existence justified by fear-mongering and the need for mutual defense.
Foreign policy is not about good guys versus bad guys but about power and interests.
Ideology and morality take a backseat to power in international relations.
Consistently against all wars unless necessary for civilian life preservation.
Power structures determine war decisions, not necessarily morality.
When consuming media, be aware of framing and focus on understanding power dynamics.

Actions:

for media consumers,
Support truth over narratives (implied)
Stay informed and critical when consuming media (implied)
</details>
<details>
<summary>
2022-01-22: Let's talk about Mitch McConnell's tribute to Malcolm X.... (<a href="https://youtube.com/watch?v=oMLhXsD__co">watch</a> || <a href="/videos/2022/01/22/Lets_talk_about_Mitch_McConnell_s_tribute_to_Malcolm_X">transcript &amp; editable summary</a>)

Beau delves into the disconnect between the elite and the struggles of the common folk, echoing Malcolm X's words and questioning the lack of patriotism in an America turning into a nightmare.

</summary>

"I'm speaking as a victim of this American system."
"They don't live in a world where what they notice is the rich getting richer and the poor getting poorer."
"If they want people to respect the country, they have to create a nation worth loving."
"If this government cannot secure voting rights, you're not going to find a lot of patriotism."
"But if there was a single person who could sum up what Malcolm X was talking about, it's probably Mitch McConnell."

### AI summary (High error rate! Edit errors on video page)

Exploring Mitch McConnell's tribute to Malcolm X and the confusion in society's upper echelons.
McConnell's dismissive comment about African American voting rates and Malcolm X's powerful words on Americanism.
The disconnect between the establishment and the struggles of the common folk.
The failure of the elite to understand the diminishing patriotism and discontent in America.
The detachment of the rich from the realities of socioeconomic struggles.
Lack of access to healthcare, exploitation of soldiers, and voter suppression go unnoticed by the elite.
Ignorance towards voting rights legislation, college costs, and the propaganda around education.
Elite's disconnect from the American dream turning into a nightmare for many.
Commonality among those at the bottom, irrespective of demographics, in facing the American nightmare.
The elite's focus on legislating patriotism, pushing college narratives, and restricting education to maintain ignorance.

Actions:

for activists, progressives, voters,
Advocate for voting rights legislation (implied)
Support initiatives to make college education more affordable (implied)
Fight against voter suppression tactics (implied)
</details>
<details>
<summary>
2022-01-21: Let's talk about why people are burning Carhartt.... (<a href="https://youtube.com/watch?v=8Z2tcXjAef8">watch</a> || <a href="/videos/2022/01/21/Lets_talk_about_why_people_are_burning_Carhartt">transcript &amp; editable summary</a>)

Beau questions the symbolic burning of Carhartt products in protest, urging reflection on separating symbols from their actual meaning.

</summary>

"You're not attacking Carhartt. You're not attacking the employees. In your mind, you're trying to make it better for them, right?"
"It's a symbol. And it's a way that you are expressing your displeasure."
"Perhaps you've grown a little too attached to those symbols as well."
"Just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Carhartt expressed support for vaccines, sparking backlash on social media.
Some individuals responded by setting Carhartt products on fire to show their disapproval.
Beau questions the reasoning behind this extreme response.
He suggests that those burning the products are not targeting employees but rather expressing dissatisfaction with leadership decisions.
Beau acknowledges the right to protest and burn items as a form of expression.
He reminds people that such actions don't directly impact the company but serve as symbolic protests.
Burning a symbol is meant to convey discontent with those in charge, not harm the company financially.
Beau urges individuals to understand the symbolic nature of their actions and their intended message.
He encourages reflection on attachment to symbols and the need to separate them from their actual meaning.
Beau concludes by leaving the audience with a thought to ponder on the importance of symbols and their representation.

Actions:

for online activists,
Contact Carhartt to express views on their public statements (suggested)
Engage in respectful dialogues with others about the significance of symbols (exemplified)
</details>
<details>
<summary>
2022-01-21: Let's talk about executive orders, doubt, and machinery.... (<a href="https://youtube.com/watch?v=A1KspsrGIyE">watch</a> || <a href="/videos/2022/01/21/Lets_talk_about_executive_orders_doubt_and_machinery">transcript &amp; editable summary</a>)

Beau warns of a dangerous coup attempt, stresses the importance of accountability, and urges swift action to protect democracy.

</summary>

"They're baseless claims."
"This was an attempt to drive a stake into the heart of the Federal Republic."
"Without accountability, it's not failed, it's practice."
"It's time to show the American people what almost happened to their country."
"The evidence that is being released piecemeal is backed up by more."

### AI summary (High error rate! Edit errors on video page)

Talks about a draft executive order that proposed the seizure of voting machines in the 2020 election based on baseless claims.
Points out the lack of evidence to support these claims and how they were fabricated.
Describes the attempt to have Pence discard legitimate votes and electors in favor of an alternate slate to retain power.
Criticizes those who supported these efforts and questions their understanding and motives.
Urges for accountability and a swift investigation into the events surrounding the attempt to overturn the election.
Warns that without consequences, failed attempts become practice and set a dangerous precedent.
Labels the events of January 6th as a coup attempt, acknowledging the level of organization behind it.
Emphasizes the importance of revealing the truth to the American people about the threat their democracy faced.
Expresses concern that without accountability, future attempts to undermine democracy may succeed.
Stresses the need to act quickly and decisively to prevent similar events in the future.

Actions:

for american citizens, activists,
Demand accountability from elected officials (suggested)
Support and push for a thorough investigation into the events of January 6th (exemplified)
Educate others about the dangers of undermining democratic processes (exemplified)
</details>
<details>
<summary>
2022-01-21: Let's talk about Ivanka Trump and the heart of the Republic.... (<a href="https://youtube.com/watch?v=zatQa66Fz0E">watch</a> || <a href="/videos/2022/01/21/Lets_talk_about_Ivanka_Trump_and_the_heart_of_the_Republic">transcript &amp; editable summary</a>)

Beau delves into the committee's developments, revealing multi-faceted power retention plans and warning of potential impacts on the Federal Republic.

</summary>

"Drive a stake into the heart of the Federal Republic."
"There were people in that caucus who realized the long-term effects of what was going to occur."
"Understanding the mechanics and understanding that it wasn't just a singular event."

### AI summary (High error rate! Edit errors on video page)

Recap of developments from the committee, focusing on Ivanka Trump's invitation.
Understanding the mechanics of how power retention was planned.
Multiple plans coordinated to pressure states and brute force change election results.
Reporting reveals Rudy Giuliani's involvement in organizing alternate electors.
Events of January 6 could pressure Pence to make desired decision or invoke Insurrection Act.
Planning for the coup attempt likely started before the election.
Mechanics of the self-coup attempt becoming clearer, showing pre-election planning.
Mention of a message warning about the impact on the Federal Republic if plans went through.
The seriousness and long-term effects of the events emphasized.
Importance of understanding the broader implications of the developments.

Actions:

for political observers,
Contact representatives to express concerns about potential threats to democracy (implied).
Stay informed and engaged with developments in governance (suggested).
</details>
<details>
<summary>
2022-01-20: Let's talk about Clyde Bellecourt.... (<a href="https://youtube.com/watch?v=GU7A54mbklo">watch</a> || <a href="/videos/2022/01/20/Lets_talk_about_Clyde_Bellecourt">transcript &amp; editable summary</a>)

Beau introduces Clyde Bellacourt's impactful legacy, detailing his continuous efforts in fighting for change and establishing community programs, setting a precedent for future movements.

</summary>

"Clyde Bellacourt, he helped co-found AIM in 1968, and he did participate in the occupation at Wounded Knee."
"He's Clyde Bellacourt, and that's how you'll find him."
"A man that spent decades in the fight trying to make things better, not just for his demographic, but for everybody as a whole."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Clyde Bellacourt and his significant impact.
Expresses disappointment in the media coverage of Clyde Bellacourt.
Emphasizes Clyde Bellacourt's continuous involvement in fighting for change.
Details Clyde Bellacourt's contributions, including co-founding AIM and participating in the occupation at Wounded Knee.
Mentions Clyde Bellacourt's efforts in setting up patrols to monitor law enforcement and curb police brutality.
Talks about Clyde Bellacourt's initiatives in job training, medical care, and establishing funds for healthcare.
Notes the resourcefulness of Clyde Bellacourt in achieving these initiatives without modern technology.
Describes Clyde Bellacourt's establishment of aid networks, schools, summer programs, and fresh produce networks.
Mentions Clyde Bellacourt's unique approaches in addressing substance issues with the youth.
Acknowledges Clyde Bellacourt's legacy and his impact on future movements like Standing Rock.

Actions:

for activists, history buffs, community leaders,
Research Clyde Bellacourt's life and legacy to understand his impact (suggested).
Support community programs and aid networks in your area (exemplified).
Advocate for culturally relevant education programs and preservation of native languages (implied).
</details>
<details>
<summary>
2022-01-20: Let's talk about Biden's statements on Ukraine.... (<a href="https://youtube.com/watch?v=w28DlpFCOJw">watch</a> || <a href="/videos/2022/01/20/Lets_talk_about_Biden_s_statements_on_Ukraine">transcript &amp; editable summary</a>)

President Biden's misinterpreted comments on Ukraine and NATO have raised concerns of escalating tensions, potentially leading to a significant foreign policy blunder with implications for future conflicts.

</summary>

"Biden's made a mistake."
"Never remove the leverage from your negotiating team."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

President Biden's recent comments on Ukraine and NATO led to confusion and sparked concerns of escalating tensions.
Biden's mention of a "minor incursion" was misinterpreted, creating the perception that a small invasion might be acceptable.
The administration later clarified Biden's statement and undertook a significant damage control effort.
The mistake in Biden's statement is seen as a significant foreign policy blunder during his term.
Biden's attempt to differentiate between invasion and incursion was likely a political move to protect himself from criticism.
The risk of misinterpretation by Russia could lead to miscalculations and potential conflict.
Both the US and Russia do not desire a war, but mistakes on either side could escalate tensions.
The situation in Ukraine is viewed as a critical point in the competition between the US and Russia.
The likelihood of conflict depends on Russia's interpretation of NATO's stance, influenced by Biden's initial comments.
Russian intelligence and Putin's decisions play key roles in determining the outcome and potential conflict.

Actions:

for foreign policy analysts,
Stay informed on international relations and foreign policy developments (implied)
Advocate for clear communication and diplomacy in handling international disputes (implied)
</details>
<details>
<summary>
2022-01-19: Let's talk about Republicans learning about Streisand.... (<a href="https://youtube.com/watch?v=m9ml3FHGwFA">watch</a> || <a href="/videos/2022/01/19/Lets_talk_about_Republicans_learning_about_Streisand">transcript &amp; editable summary</a>)

The Republican Party demonizes education, but students fight back with banned book clubs, inspiring activism and community involvement among the youth.

</summary>

"The response from students has been more pronounced than the response from a lot of people in the Democratic Party."
"I would like to thank the Republican Party for creating the next generation of activists."
"Band books are the best books."
"Those are people that want to make the world a better place and are willing to invest the time to do so."
"Welcome to the information age and the Streisand effect."

### AI summary (High error rate! Edit errors on video page)

The Republican Party in the United States demonizes education, trying to make ignorance a duty and knowledge a crime.
They have created lists of objectionable books and want to teach mythology instead of history.
Students' response has been strong, with essays and banned book clubs forming in protest.
An eighth grader in Pennsylvania started a banned book club, inspiring activism in the younger generation.
Beau addresses a common question from high school students on how to start community networks.
He praises the idea of meeting to read banned books as a litmus test for potential activists.
Reading and discussing banned books can lead to real-world activism and community involvement.
Beau encourages high school students to find like-minded peers through book clubs as a safe way to get involved.
He contrasts adults afraid of education with youth advocating for knowledge and learning.
The youth are pushing back against attempts to limit education and information access.

Actions:

for high school students,
Start a banned book club at your school to encourage activism and community involvement (suggested)
Read and openly discuss objectionable books to challenge censorship and inspire others to join in activism (suggested)
</details>
<details>
<summary>
2022-01-19: Let's talk about Carhartt and rural patient education.... (<a href="https://youtube.com/watch?v=JHT4PVjF-X0">watch</a> || <a href="/videos/2022/01/19/Lets_talk_about_Carhartt_and_rural_patient_education">transcript &amp; editable summary</a>)

Beau explains the rural vaccination divide and the importance of self-reliance in medical care, offering insights on patient education challenges and unconventional methods to influence vaccination decisions.

</summary>

"The strongest predictor of your vaccination status is your political affiliation."
"People rely on themselves for medical stuff. They take more preventative measures."
"Maybe appeal to that survivalist-ish stereotype."
"Stubborn doesn't really cover it, does it?"
"I don't really know how to use that to increase patient education, though."

### AI summary (High error rate! Edit errors on video page)

Explains the situation with Carhartt, a clothing company known for its rugged workwear, and their vaccine policy causing controversy among customers.
Shares a message from a healthcare worker who noticed a vaccination trend between urban and rural areas and seeks advice on patient education.
Notes that political affiliation is a significant factor in vaccination rates, with Democrats more likely to be vaccinated than Republicans.
Describes the rural areas as predominantly Republican where people tend to rely on themselves for medical care due to the distance from big medical infrastructure.
Points out the importance of individuals in rural areas having their own medical supplies and taking preventative measures seriously.
Mentions the difficulty in convincing people in smaller towns to shift their views and the uphill battle faced by healthcare workers in these areas.
Suggests appealing to the survivalist stereotype to encourage vaccination and mentions the challenge of changing deeply set beliefs.
Recommends finding unofficial methods, like speaking to family members, to influence high-risk individuals who are hesitant about getting vaccinated.

Actions:

for healthcare professionals, educators,
Speak to high-risk individuals' family members to influence their vaccination decisions (implied).
</details>
<details>
<summary>
2022-01-18: Let's talk about what Flint can teach us about the 6th.... (<a href="https://youtube.com/watch?v=cynrhRdx9mU">watch</a> || <a href="/videos/2022/01/18/Lets_talk_about_what_Flint_can_teach_us_about_the_6th">transcript &amp; editable summary</a>)

Beau warns Congress against delaying and politicizing investigations, stressing the need for swift action and transparency to avoid potential fallout.

</summary>

"The longer they wait, the longer they drag this out, while it may seem good politically, The worst it is for everything else."
"This isn't something they can play political games with."
"Let's get it done and go from there."

### AI summary (High error rate! Edit errors on video page)

Talks about two unrelated events involving government officials and draws parallels.
Mentions the new reporting on what happened in Flint, Michigan, with prosecutors building a RICO case implicating government officials.
Explains how the initial team of prosecutors was dismantled after an election, leading to a new team that brought fewer charges.
Warns Congress about the risk of delays and political manipulation in investigations, using the events of January 6 as an example.
Emphasizes the importance of completing investigations and releasing reports before elections to avoid potential fallout.
Urges Congress to act swiftly and not politicize the investigation into the events of January 6 at the Capitol.
Stresses the need for transparency and informing the American people promptly about the wide-ranging implications of the events on January 6.
Advises against playing political games with the investigation and underscores the significant consequences that may arise.
Encourages swift action and completion of the investigation to avoid negative repercussions.
Urges for prioritizing duty and quick resolution in handling the investigation into the events of January 6.

Actions:

for congress members,
Pursue investigations swiftly and transparently (implied)
Inform the American people promptly about the wide-ranging implications (implied)
</details>
<details>
<summary>
2022-01-18: Let's talk about identifying with the bad guy a little more.... (<a href="https://youtube.com/watch?v=H-h5LK5FnNA">watch</a> || <a href="/videos/2022/01/18/Lets_talk_about_identifying_with_the_bad_guy_a_little_more">transcript &amp; editable summary</a>)

Beau challenges the narrative around founding fathers, systemic racism, and hero identification, questioning the choice to identify with the bad guy.

</summary>

"If you owned slaves based on race, you are in fact a racist."
"Systemic racism is something that is so pervasive you don't even realize it's occurring."
"Why are you looking for a white hero? Is it impossible to identify with a black person?"
"Pigment more influential in determining who you can identify with, then right or wrong."
"And if that's the case, you chose to identify with the bad guy."

### AI summary (High error rate! Edit errors on video page)

Talks about the choice of identifying with the bad guy throughout history.
Responds to a message challenging the identification of founding fathers as racist due to owning slaves.
Points out that owning slaves based on race is a clear sign of racism.
Expresses dislike for the term "founding fathers," suggesting they were not actively involved in parenting the nation.
Introduces Roger Sherman as a significant figure who played a key role in various foundational documents and never owned slaves.
Questions why figures like Roger Sherman, who opposed slavery, are not more prominently discussed in history.
Suggests that the narrative of all founding fathers being slave owners is a deliberate fiction to maintain a certain image.
Lists examples of founding fathers like John Adams, John Quincy Adams, Thomas Paine, and Benjamin Franklin who did not own slaves or later opposed slavery.
Dispels the myth that all founding fathers were slave owners and argues that it serves a purpose to maintain a false narrative.
Addresses the issue of systemic racism, explaining how it can be pervasive and invisible to those benefiting from it.
Encourages the audience to look for white heroes to identify with and challenges the notion of needing a white hero.
Raises the question of whether race or moral values should dictate hero identification.

Actions:

for history enthusiasts, anti-racism advocates,
Research and share information about historical figures like Roger Sherman who opposed slavery (suggested).
Educate others about the diverse views and actions of founding fathers regarding slavery (suggested).
Challenge traditional narratives and seek out lesser-known historical figures who stood against injustice (suggested).
</details>
<details>
<summary>
2022-01-17: Let's talk about the ratchet effect and Democracy.... (<a href="https://youtube.com/watch?v=mHbwJgZt-tM">watch</a> || <a href="/videos/2022/01/17/Lets_talk_about_the_ratchet_effect_and_Democracy">transcript &amp; editable summary</a>)

Beau explains the ratchet effect, critiquing the belief that there's no difference between Republicans and Democrats, while stressing the individual's role in leading progressive change.

</summary>

"The Democrats are supposed to lead you. No, you're supposed to lead you."
"It's not their job. It's your job."
"If your starting place is further over than me, good, good."
"You're supposed to lead the country left."
"You're supposed to institute those reforms."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the ratchet effect, where the Republican Party moves the country right and Democrats prevent it from moving left.
Addresses a message from a college student questioning the effectiveness of Democrats and the ratchet effect.
Points out the difference between real leftists and American leftists in political discourse.
Critiques the idea of the ratchet effect leading people to believe there is no difference between Republicans and Democrats, discouraging political involvement.
Argues that there is a difference between the two parties, not just in left-right terms but in quality of life and social democracy issues.
Affirms that democracy is under attack, particularly by the Trumpist faction of the Republican Party.
Emphasizes the Republican efforts to undermine democracy through tactics like gerrymandering and voter suppression.
Acknowledges that Democrats are working to maintain a semblance of representative democracy in contrast to authoritarian tendencies.
Encourages individuals, like Erica, who challenge the status quo and push for more radical ideas, especially in a university setting.
Stresses the importance of individuals taking the lead in pushing for progressive changes, rather than relying solely on political parties.

Actions:

for college students, political activists,
Connect with student political groups on campus to understand the differences between political ideologies (implied)
Engage in political discourse and activism on quality of life issues and social democracy (implied)
Stay informed about threats to democracy and participate in efforts to safeguard democratic principles (implied)
Challenge traditional political narratives and push for more radical ideas for societal change (implied)
</details>
<details>
<summary>
2022-01-17: Let's talk about history and an oppressor narrative.... (<a href="https://youtube.com/watch?v=b9xl5uvqtQg">watch</a> || <a href="/videos/2022/01/17/Lets_talk_about_history_and_an_oppressor_narrative">transcript &amp; editable summary</a>)

Beau explains the accurate depiction of history as a narrative of oppressed versus oppressor and challenges readers to identify with those seeking positive change.

</summary>

"History can teach lessons, but it should never be brought back as a tool of division and accusation."
"If you're neutral in that struggle that has existed throughout the human experience, then you have chosen the side of the oppressor."
"If you are reading something that claims to be a history book, and it doesn't make you uncomfortable, you're not reading a history book."
"If you read these history books, and you identify with the slavers, rather than those who helped the Underground Railroad, that says something about you."
"It's not manufactured, point to a time in American history where that dynamic wasn't at play."

### AI summary (High error rate! Edit errors on video page)

Expresses support for a theory misunderstood by the right wing as a historical theory rather than legal studies.
Receives message calling the theory racist garbage that divides people along race lines.
Points out the historical struggle between oppressor and oppressed classes throughout history.
Emphasizes that history is not a story but what actually happened.
Asserts that history must be accurate to teach lessons and draw parallels to the present.
States that the narrative of oppressed versus oppressor is accurate and not wrong to teach.
Explains that being neutral in the struggle between oppressor and oppressed sides is choosing the side of the oppressor.
Argues that history books must accurately depict the dynamic between oppressed and oppressor.
Challenges the discomfort felt when reading history books and the identification with oppressors rather than those seeking change.
Encourages reflection on why one identifies with the oppressors in history.

Actions:

for history learners and critical thinkers.,
Analyze history books for accuracy and discomfort (implied).
Identify with those seeking positive change in history (implied).
</details>
<details>
<summary>
2022-01-16: Let's talk about baseless claims about a guy named Ray.... (<a href="https://youtube.com/watch?v=xb1dAEbrr28">watch</a> || <a href="/videos/2022/01/16/Lets_talk_about_baseless_claims_about_a_guy_named_Ray">transcript &amp; editable summary</a>)

Addressing a baseless claim about Ray Epps and questioning the accountability of political leaders for incitement without physical participation on January 6th.

</summary>

"You think he should be arrested?"
"What did he do that warrants arrest?"
"They are making the case to charge Trump."
"Isn't that what Trump did?"
"If Epps warrants arrest, there's a whole bunch of people in the Republican Party that also warrant arrest."

### AI summary (High error rate! Edit errors on video page)

Addresses a baseless claim surrounding a person named Ray that has gained traction among conservatives.
Explains the lack of evidence supporting the claim that Ray was working for the government and responsible for the events on January 6th.
Suggests a way to address conservatives asking about Ray Epps by questioning their stance on his arrest and actions.
Compares the situation with Ray Epps to holding political leaders accountable for encouraging but not physically participating in events.
Encourages challenging the belief that a single person without political influence could incite a crowd, raising questions about the power of influential figures' words.
Notes the absence of concrete evidence to support the claim about Ray Epps, making it challenging to debunk.
Raises the point that if Ray Epps warrants arrest, then many others in the Republican Party could also be implicated.
Advocates for probing deeper into the logic behind calling for Ray Epps' arrest and drawing parallels with other political figures' actions.

Actions:

for those addressing baseless claims.,
Ask individuals questioning baseless claims to justify their beliefs (suggested).
Challenge misconceptions by drawing parallels with other political figures' actions (exemplified).
</details>
<details>
<summary>
2022-01-16: Let's talk about Ukraine.... (<a href="https://youtube.com/watch?v=_V-3Ta-ADV8">watch</a> || <a href="/videos/2022/01/16/Lets_talk_about_Ukraine">transcript &amp; editable summary</a>)

NATO expansion, intelligence pretexts, and Ukrainian resistance tactics heighten tensions between Russia and the United States.

</summary>

"Governments manufacture pretexts to go to war. They do that. The United States does it. Russia does it too."
"Ukraine's doctrine shift is a brilliant move. Whoever came up with this idea needs to be promoted."
"Ukraine may be the front line for the near-peer contest between Russia and the United States."

### AI summary (High error rate! Edit errors on video page)

NATO's expansion towards Russia's borders is causing tension due to Russia's national security concerns and desire to reassert power.
The United States has intelligence suggesting Russia planned a pretext for war by hitting its own people in Ukraine.
The United States possibly manufactured a psychological campaign to sow distrust between the Russian military and their proxies.
Russia may send troops to Cuba and Venezuela, but the U.S. should not get involved as it doesn't pose a threat.
Ukraine is shifting its doctrine to resist Russia by training civilians in first aid and resistance tactics.
Ukraine's strategy mirrors Iran and North Korea's technique of putting up resistance and disappearing.
Ukraine's doctrine change could potentially deter conflict if they can quickly field thousands of partisans.
Conflict starting immediately in Ukraine seems unlikely, but it could escalate in the future due to various factors.
Both the U.S. and Russia misreading each other simultaneously could lead to a conflict involving them.

Actions:

for policy analysts, diplomats, activists,
Support Ukraine's civilian training initiatives to resist potential conflict. (suggested)
Advocate for diplomatic solutions and non-interference in sovereign affairs. (implied)
Stay informed about developments in Ukraine to raise awareness. (implied)
</details>
<details>
<summary>
2022-01-15: Let's talk about my last day teaching in Virginia.... (<a href="https://youtube.com/watch?v=b51vrmzdKHo">watch</a> || <a href="/videos/2022/01/15/Lets_talk_about_my_last_day_teaching_in_Virginia">transcript &amp; editable summary</a>)

Beau addresses the absurdity of teaching a non-existent debate, criticizes the legislation making knowledge a crime, and urges Virginians to reclaim their duty to be informed.

</summary>

"Your duty is to get as much knowledge as you can because, in essence, you are supposed to be leading this country."
"Your parents surrendered your education to a bunch of people who profit from you being ill-informed."
"The people of Virginia should probably take a long, hard look at how they were manipulated."

### AI summary (High error rate! Edit errors on video page)

Last day teaching in Virginia, moving to a different state.
State-mandated curriculum requires teaching a debate between Abraham Lincoln and Frederick Douglass that never happened.
Contemplated a fake debate but realized the critical nature of Douglass towards race wouldn't be suitable.
Planned to show an epic rap battle between Frederick Douglass and Thomas Jefferson but found it critical of slavery and the founding systems.
Constrained by restrictions and guidelines, making it impossible to teach history in Virginia.
Leaving with a lesson from Thomas Paine, stressing the importance of knowledge and duty.
United States is meant to be a government of informed people, requiring knowledge for self-governance.
Criticizes new laws from state politicians making knowledge a crime and ignorance a duty.
Blames parents for surrendering education to profit-driven individuals who benefit from keeping people ill-informed.
Parents neglecting their duty to be informed and knowledgeable, allowing politicians to criminalize knowledge and perpetuate ignorance.
Calls attention to a bill in Virginia mandating the teaching of a debate that never happened, questioning the competence of those behind it.
Urges the people of Virginia to recognize manipulation and reclaim their duty to be informed.

Actions:

for students, educators, virginians,
Question the curriculum requirements and advocate for accurate and meaningful education (suggested).
Challenge legislation that restricts knowledge and supports ignorance (implied).
</details>
<details>
<summary>
2022-01-15: Let's talk about Tucker, the lost and found, and the way it is.... (<a href="https://youtube.com/watch?v=8XyOZDvpKBE">watch</a> || <a href="/videos/2022/01/15/Lets_talk_about_Tucker_the_lost_and_found_and_the_way_it_is">transcript &amp; editable summary</a>)

Beau exposes the dangerous influence of misinformation and false patriotism leading individuals astray in the Capitol incident aftermath.

</summary>

"Tucker Carlson spun their actions as evidence of innocence."
"There was a disconnect between their actions and the consequences."
"Who fed them that rhetoric? Who left them ill-informed?"

### AI summary (High error rate! Edit errors on video page)

Talking about the lost and found, Tucker Carlson, excuses, and silly things worldwide.
People involved in the Capitol incident on January 6 tried to recover items they dropped.
Tucker Carlson spun their actions as evidence of innocence, claiming they didn't know they were breaking laws.
Beau disagrees with this excuse, mentioning a similar incident from 1993 involving a rented truck.
Individuals at the Capitol on January 6 were misled by rhetoric about patriotism and history.
They were unaware of the gravity of their actions, thinking they were fighting for their country.
Beau questions who fed them this rhetoric and left them ill-informed.
There was a disconnect between their actions and the consequences, as seen by interactions with law enforcement.
Beau challenges the notion that they were innocent due to lack of understanding.
Concludes by urging to question those who influenced and misled them.

Actions:

for media consumers,
Challenge misinformation sources and hold them accountable (implied)
Educate others on the dangers of false narratives and rhetoric (implied)
</details>
<details>
<summary>
2022-01-14: Let's talk about optimism and public health.... (<a href="https://youtube.com/watch?v=w1XjtqILYts">watch</a> || <a href="/videos/2022/01/14/Lets_talk_about_optimism_and_public_health">transcript &amp; editable summary</a>)

Beau cautiously addresses the optimism around the potential end of the pandemic, stressing the need for continued precautions and medical confirmation before celebrating.

</summary>

"Don't be that guy."
"I'm not going to declare victory until the MDs are saying it."
"Do not be that person who gets caught up in it right before it's over."

### AI summary (High error rate! Edit errors on video page)

Addressing the optimism surrounding the potential end of the pandemic and transition into an endemic period.
Cautiously optimistic about moving through the period of transitioning out of the pandemic.
Mention of a growing body suggesting the current transition phase.
Emphasizing the need for confirmation from medical doctors before celebrating the end.
Warning against premature declarations of victory as another variant could emerge.
Stressing the importance of maintaining precautions like hand-washing, mask-wearing, and social distancing.
Advising to continue with effective measures to avoid being caught off guard.
Urging to stay vigilant and get vaccinated even if nearing the end of the pandemic phase.
Noting that transitioning into the endemic phase means the virus will still be present but less severe.
Encouraging everyone to stay proactive and not let their guard down.

Actions:

for individuals and communities,
Keep practicing preventive measures (implied)
Get vaccinated (implied)
</details>
<details>
<summary>
2022-01-14: Let's talk about DOJ's new special unit... (<a href="https://youtube.com/watch?v=AKCRfu7vnoU">watch</a> || <a href="/videos/2022/01/14/Lets_talk_about_DOJ_s_new_special_unit">transcript &amp; editable summary</a>)

Department of Justice's new unit for domestic terror won't be a game changer due to limitations on targeting groups and potential abuse of power.

</summary>

"This isn't going to be a game changer."
"That kind of power, under the control of the executive branch, is a really bad idea."
"But it might be a useful tool for people who are trying to stop the targeting of the civilians."

### AI summary (High error rate! Edit errors on video page)

Department of Justice announced creating a new unit for domestic terror, sparking questions on its effectiveness and structure.
New unit won't be a game changer, likely a small group of experts for coordination and information sharing.
In the US, targeting domestic groups is limited due to freedom of assembly and speech.
Concerns raised about the potential abuse of power if the government could designate groups.
Setting up the new unit seems more academic and for coordination rather than a significant tool.
Beau cautions against pushing for special teams with designation powers due to potential negative consequences.
FBI and DOJ already respond to domestic incidents swiftly, so additional specialized teams may not be necessary.
Beau believes the announcement is mostly for PR purposes, lacking a substantial reason.
The new unit might be useful in preventing civilian targeting but won't eradicate movements in the US.
Overall, Beau doesn't see this announcement as a significant development.

Actions:

for policy analysts, activists,
Advocate for community-based solutions to address domestic terror (implied)
Engage in dialogues and workshops on civil rights and freedoms (implied)
</details>
<details>
<summary>
2022-01-13: Let's talk about political and military wings again.... (<a href="https://youtube.com/watch?v=8cjLPgFxfzo">watch</a> || <a href="/videos/2022/01/13/Lets_talk_about_political_and_military_wings_again">transcript &amp; editable summary</a>)

Beau clarifies basic information on wings, contrasts left and right-wing groups, and suggests legal maneuvers over traditional strategies for certain groups.

</summary>

"All of them have something in common though. What is it? They're all left wing."
"It's really hard to operate under a strategy that requires a lot of autonomy when your entire ideology is about hierarchy and not having autonomy."
"I think in the future what you would really need to be looking for is more of a series of legal maneuvers to affect a self-coup."

### AI summary (High error rate! Edit errors on video page)

Addressing concerns and questions about discussing wings and structures.
Clarifying that the information shared is basic and not secretive.
Mentioning examples from popular culture where similar structures are depicted.
Noting a lack of successful right-wing groups adopting similar structures.
Explaining the ideological differences between left and right-wing groups.
Describing the challenges right-wing groups face in adopting decentralized structures.
Pointing out the failure of right-wing groups in using certain strategies in the United States.
Suggesting a focus on legal maneuvers rather than traditional tactics for right-wing groups.
Emphasizing the importance of understanding the dynamics of different ideological groups.
Concluding with a thought about potential future strategies for certain groups.

Actions:

for activists, analysts, researchers,
Analyze and understand the dynamics of different ideological groups (implied)
</details>
<details>
<summary>
2022-01-13: Let's talk about Ahmaud and Houston County cops.... (<a href="https://youtube.com/watch?v=V-AeS2gBXlo">watch</a> || <a href="/videos/2022/01/13/Lets_talk_about_Ahmaud_and_Houston_County_cops">transcript &amp; editable summary</a>)

Houston County Sheriff's Department faces scrutiny over deputy's public endorsement of extra judicial execution, prompting a call for reform and reevaluation of departmental culture.

</summary>

"When people tell you who they are, you need to believe them."
"Failure to address the underlying culture may lead to more severe problems and loss of lives."
"Sheriff needs to go through that department with a fine tooth comb."

### AI summary (High error rate! Edit errors on video page)

Case of Houston County Sheriff's Department resurfaces, involving a deputy's controversial comment about Ahmaud Arbery's killers.
Deputy's statement, publicly endorsing extra judicial execution, was condemned but sheds light on the department's culture.
Sheriff swiftly fired the deputy, but the incident reveals deeper issues within the department.
Sheriff must recognize the culture within his department and take serious actions to reform and retrain.
Failure to address the underlying culture may lead to more severe problems and loss of lives.
Public statement by the deputy hints at potentially widespread problematic beliefs within the department.
Sheriff needs to thoroughly inspect and address the department's culture to prevent further harm.
Deputy's public statement indicates a concerning mindset that could have serious consequences when not under public scrutiny.

Actions:

for law enforcement officers,
Inspect, retrain, and reform the department to address the underlying culture (implied).
</details>
<details>
<summary>
2022-01-12: The Roads to safety on the road (opening a 5-year-old survival kit).... (<a href="https://youtube.com/watch?v=JASVnNrCKmQ">watch</a> || <a href="/videos/2022/01/12/The_Roads_to_safety_on_the_road_opening_a_5-year-old_survival_kit">transcript &amp; editable summary</a>)

Beau stresses the importance of regularly updating emergency kits to ensure functionality during crises, showcasing items in an outdated kit as a reminder of the need for preparedness.

</summary>

"You always have to keep it updated."
"Most of the stuff you need doesn't work."
"Emergency preparedness is an ongoing process."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of emergency preparedness on the roads following an incident where many people were stranded on the interstate.
Demonstrates an emergency preparedness kit that he put together years ago for a friend who had to leave in a rush and rented a car.
Emphasizes the necessity of checking and updating emergency kits annually due to expired and non-functional items.
Lists the key components of a basic emergency kit: food, water, fire, shelter, first aid kit, and a knife.
Shows the contents of the emergency kit, including items like a first aid kit, Mylar blanket, siphon, N95 mask, gloves, batteries, compass, chewing gum, flints, fishing line, hand sanitizer, duct tape, flashlights, can opener, chemlights, multi-tool, candle, hatchet, survival card, meds, paracord bracelets, tampons, coffee, biofuel, dog food, rice, and canned food.
Points out the importance of having duplicate items in the kit and multiple ways to start a fire.
Mentions the significance of adapting the emergency kit based on climate, terrain, and individual skill set.
Stresses the need for regular maintenance and updating of emergency kits to ensure functionality during emergencies.
Encourages viewers to understand that emergency preparedness is an ongoing process and not a one-time task.
Concludes by reminding everyone to keep their emergency kits updated and wishes them a good day.

Actions:

for emergency preparedness enthusiasts,
Inventory and update your emergency kit annually to ensure all items are functional and up-to-date (exemplified).
Customize your emergency kit based on your location, climate, and individual skill set (implied).
</details>
<details>
<summary>
2022-01-12: Let's talk about voting rights and its impact on voter turnout.... (<a href="https://youtube.com/watch?v=IQ9uYPi953I">watch</a> || <a href="/videos/2022/01/12/Lets_talk_about_voting_rights_and_its_impact_on_voter_turnout">transcript &amp; editable summary</a>)

Democrats are pushing for voting rights legislation, but Republican opposition could inadvertently drive Democratic voter turnout.

</summary>

"Democrats are pushing for voting rights legislation, with heavy hitters in the party in favor of it."
"Voting is viewed as a fundamental aspect of American democracy, cherished by many."
"Opposing voting rights legislation could paint Republicans as anti-American."
"Republicans will need to justify their position on blocking the legislation, potentially risking a negative image."
"Republicans could inadvertently drive Democratic voter turnout by standing against voting rights."

### AI summary (High error rate! Edit errors on video page)

Democrats are pushing for voting rights legislation, with heavy hitters in the party in favor of it.
The debate over voting rights legislation could end up benefiting Democrats in terms of voter turnout.
Republicans may struggle to block the legislation, leading to potential consequences.
Voting is viewed as a fundamental aspect of American democracy, cherished by many.
Opposing voting rights legislation could paint Republicans as anti-American.
Republican attempts at gerrymandering and the January 6th committee's revelations may further this perception.
Republicans will need to justify their position on blocking the legislation, potentially risking a negative image.
The polarization in the country could significantly impact voter turnout.
Democrats may show up in larger numbers if Republicans continue to oppose voting rights legislation.
Republicans could inadvertently drive Democratic voter turnout by standing against voting rights.

Actions:

for voters,
Contact your representatives to express support for voting rights legislation (implied).
Join organizations working to protect voting rights (implied).
Organize events to raise awareness about the importance of voting (implied).
</details>
<details>
<summary>
2022-01-12: Let's talk about Ronald McDonald and doing your own research.... (<a href="https://youtube.com/watch?v=1sMZXZHoNmE">watch</a> || <a href="/videos/2022/01/12/Lets_talk_about_Ronald_McDonald_and_doing_your_own_research">transcript &amp; editable summary</a>)

Ronald McDonald House requires vaccinations, sparking criticism from anti-science groups, but Beau stresses the importance of prioritizing public health over personal beliefs and moral judgments.

</summary>

"Nobody cares what you think when it comes to people who refuse to take every possible precaution to keep a bunch of sick kids from getting sicker."
"Your moral compass is broke."
"Nobody cares about your moral determination because your moral compass is broke."
"And I think that's a very good thing."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Ronald McDonald House in Canada requires vaccinations for stay.
Some in anti-science community criticize the decision.
Beau questions those opposing vaccination requirement.
Ronald McDonald House provides accommodations for immunocompromised individuals.
Beau criticizes those prioritizing personal beliefs over public health.
Ronald McDonald House assists individuals finding other accommodations.
Beau advocates for taking every possible precaution around immunocompromised individuals.
He mentions the importance of understanding Ronald McDonald House's purpose.
Beau encourages individuals to research and not rely on misinformation.
He stresses the significance of prioritizing the well-being of sick children.

Actions:

for public health advocates,
Support Ronald McDonald House or similar organizations assisting families of sick children (implied).
Research and prioritize scientifically-backed information over misinformation (implied).
Take necessary precautions to protect immunocompromised individuals in communal settings (implied).
</details>
<details>
<summary>
2022-01-11: Let's talk about why they aren't talking to the committee.... (<a href="https://youtube.com/watch?v=VMlUCBYZIVM">watch</a> || <a href="/videos/2022/01/11/Lets_talk_about_why_they_aren_t_talking_to_the_committee">transcript &amp; editable summary</a>)

Exploring the structure of movements and the importance of maintaining division between political and militant wings to ensure plausible deniability and safety.

</summary>

"Plausible deniability is key."
"The division between the political and militant wings is vital."
"Failure to maintain the division could lead to exposure."
"Communication between wings is usually indirect."
"I have a feeling we are going to find out."

### AI summary (High error rate! Edit errors on video page)

Exploring the structure of movements, particularly the division between the political wing and militant wing within movements seeking to change governance by force.
The militant wing uses violence or the threat of violence to influence and achieve their goals, while the political wing provides cover and condemns the actions while advocating for the goals.
Plausible deniability is key for the political wing to distance themselves from the militant wing, making it easier for them to be reelected.
Communication between the political and militant wings is usually indirect, through dead drops or cutouts, to maintain separation and plausible deniability.
The division between the political and militant wings is vital for the functioning and safety of the movement.
Failure to maintain the division could lead to exposure and legal consequences, which may explain why certain individuals are not coming forward to make their case.
Beau suggests that there may have been communication links between members of Congress and militant movements involved in the events of January 6th.

Actions:

for observers, activists, politicians,
Investigate potential communication links between members of Congress and militant movements (implied)
</details>
<details>
<summary>
2022-01-11: Let's talk about the motives, intent, and the 6th.... (<a href="https://youtube.com/watch?v=6zwtzLcRtes">watch</a> || <a href="/videos/2022/01/11/Lets_talk_about_the_motives_intent_and_the_6th">transcript &amp; editable summary</a>)

Beau explains why he doesn't speculate on individual motives and focuses on accuracy and broader context in understanding events.

</summary>

"Because I like to be right."
"I like to be right and I think that doing it, if you can't be accurate, is dangerous."
"We need to get down to the nuts and bolts of what occurred."
"People's motives for doing things, especially things that are like this, are often very private."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses a common question about why he doesn't provide individual motives in his breakdowns.
Explains that determining motive accurately is difficult without talking to the person directly.
Warns against speculating on motives as it can lead to manipulation and simplistic answers.
Points out the dangers of assigning intent and motive, especially in media, to protect against manipulation.
Emphasizes the importance of providing accurate information over guesswork.
Acknowledges that people have multiple motives for their actions, making it hard to pinpoint a single primary one.
Prefers to focus on group dynamics, events, and historical context rather than individual motives in urgent national issues.
Suggests that examining individual motives can happen later once the immediate concerns are addressed.
Stresses the need to understand how events unfold historically to prevent their recurrence.
Concludes by encouraging a focus on the broader picture rather than diving deep into individual motives for better accuracy and understanding.

Actions:

for creators, analysts, journalists,
Analyze group dynamics and historical context to understand events better (implied).
</details>
<details>
<summary>
2022-01-10: Let's talk about Yellowstone and wolves.... (<a href="https://youtube.com/watch?v=fEK2mrsuOMY">watch</a> || <a href="/videos/2022/01/10/Lets_talk_about_Yellowstone_and_wolves">transcript &amp; editable summary</a>)

Yellowstone wolves face increased threats as states fail in their conservation duties, urging for federal intervention to protect the species.

</summary>

"Losing 20 wolves in a single year, that's the most that has happened since reintroduction efforts began."
"It's time for the feds to relist the wolf."

### AI summary (High error rate! Edit errors on video page)

Yellowstone wolves faced threats after the federal government delisted gray wolves, shifting protection responsibilities to states.
Yellowstone lost 20 wolves recently, impacting a pack significantly.
States can now hunt, trap, and bait wolves outside of federal land, potentially leading to increased threats.
Losing 20 wolves in a year is the highest since reintroduction efforts began.
Montana's governor, one of the major offenders, failed to follow regulations and shot a radio-collared wolf near the park.
Beau urges for wolves to be relisted under federal protection due to states' inadequate safeguarding efforts.
States show little interest in protecting wolves, necessitating federal intervention for conservation.

Actions:

for conservationists, wildlife advocates,
Contact local representatives to advocate for relisting wolves under federal protection (exemplified)
Support organizations working towards wolf conservation efforts (exemplified)
</details>
<details>
<summary>
2022-01-10: Let's talk about Trump, Pence, and updates on the committee.... (<a href="https://youtube.com/watch?v=0uCjz8bV_b8">watch</a> || <a href="/videos/2022/01/10/Lets_talk_about_Trump_Pence_and_updates_on_the_committee">transcript &amp; editable summary</a>)

Beau provides insights into the committee's investigations into the events of the 6th, detailing pressure on Pence and the importance of individuals maintaining their moral compass.

</summary>

"You don't understand, Mike. You can do this. I don't want to be your friend anymore if you don't do this."
"They tend to not notice the help. They tend to not watch what they say in front of them."
"A lot of this was luck."

### AI summary (High error rate! Edit errors on video page)

Update on committee activities investigating the events of the 6th.
Confirmation of suspected details regarding the events.
Americans likely not shocked by findings, with most understanding what occurred on that day.
Description of the events on the 6th as a self-coup.
Details of Trump pressuring Pence to overturn the election.
Pence's refusal to comply with Trump's demands.
Former press secretary Grisham's cooperation with the committee and providing valuable information.
Committee engaging with lower-level individuals who are cooperating.
Subpoena of the pillow salesman's phone records by the committee.
Public questioning of the Department of Justice's actions in response to the committee's investigations.
Positive note on how the investigation is being conducted without leaks or a play-by-play.
Interest in Vice President Pence as a witness and potential cooperation.
Acknowledgment of luck and individuals' moral compasses in preventing certain outcomes.
Reflection on the importance of individuals standing their ground in critical positions.

Actions:

for committee members and concerned citizens.,
Contact your representatives to express support for thorough investigations (suggested).
Stay informed about the developments surrounding the events of the 6th (implied).
</details>
<details>
<summary>
2022-01-09: Let's talk about the Electoral Count Act of 1887.... (<a href="https://youtube.com/watch?v=s-UYheM5bgY">watch</a> || <a href="/videos/2022/01/09/Lets_talk_about_the_Electoral_Count_Act_of_1887">transcript &amp; editable summary</a>)

Bipartisan move to update Electoral Count Act aims to prevent legal cover for self-coups, not voting rights legislation.

</summary>

"This is not voting rights legislation."
"Don't allow Republicans to frame it as voting rights legislation and don't let Democrats use it as a way of saying, look, we did something completely unrelated."
"They're updating old language in an old law, closing perceived loopholes."

### AI summary (High error rate! Edit errors on video page)

Bipartisan support for updating the Electoral Count Act of 1887 to modernize and clarify terms.
Aim is to close perceived loopholes to prevent any legal cover for a potential self-coup.
Not voting rights legislation, just updating a law to prevent misinterpretation.
Anticipates wild theories emerging post-update, potentially feeding into conspiracy theories.
Warns against allowing Republicans to frame it as voting rights legislation or Democrats to claim unrelated victories.
Encouraged by bipartisan support indicating a rejection of attempted coups.
Vice President's role seen as ceremonial, aiming to deter future coup attempts.

Actions:

for legislators, activists, voters.,
Educate others on the distinction between updating laws and voting rights legislation (implied).
Stay informed on the progress of the Electoral Count Act updates and advocate for transparency (generated).
</details>
<details>
<summary>
2022-01-09: Let's talk about leadership, command, and Trump.... (<a href="https://youtube.com/watch?v=L1xaaXNbviQ">watch</a> || <a href="/videos/2022/01/09/Lets_talk_about_leadership_command_and_Trump">transcript &amp; editable summary</a>)

Leadership vs. command: Beau dissects the distinction, Trump's waning authority, and potential successors in the political landscape.

</summary>

"It doesn't matter because it's not the action that matters. It's obeying that perceived authority."
"Former President Trump has been issuing a lot of commands lately and a lot of them are being ignored."
"If former President Trump continues on his current path, he will order himself into an irrelevant position."

### AI summary (High error rate! Edit errors on video page)

Leadership and command are distinct; leadership is found on the left while command is prevalent on the right due to hierarchy and perceived authority.
Command is built little by little through small, seemingly insignificant orders that establish compliance and build towards following larger orders.
The essence of command lies in obeying perceived authority rather than the action itself.
A key rule of command is to never issue an order that won't be obeyed, as it undermines authority.
Former President Trump's commands are increasingly being ignored, with some even leading to him being booed.
Trump's recent command for followers to leave Twitter and Facebook due to being "radical left" is likely to be disregarded.
If Trump continues down this path, he risks rendering himself irrelevant, becoming just an elderly person shouting at the internet.
The question arises of who will assume command after Trump and capitalize on the movement that surrounded him.
Beau suggests watching for prominent Republican figures, potentially governors, who can effectively give and have commands followed as potential successors to Trump.

Actions:

for political observers,
Watch for prominent Republican figures giving and having commands followed as potential successors to Trump (implied).
</details>
<details>
<summary>
2022-01-08: Let's talk about what Canada can do to get ready.... (<a href="https://youtube.com/watch?v=ZjjtEiUIvXs">watch</a> || <a href="/videos/2022/01/08/Lets_talk_about_what_Canada_can_do_to_get_ready">transcript &amp; editable summary</a>)

Canadians brace for potential U.S. conflict fallout, considering asylum seekers and NATO staging implications.

</summary>

"The scenario presented involves two groups emerging from a contested election: the legitimate government and the rebels."
"NATO, including Canada, is likely to support the legitimate government due to control over the U.S.' strategic arsenal."
"Suggestions include relaxing immigration restrictions, preparing asylum qualifications, and expediting processing for different groups based on resources."

### AI summary (High error rate! Edit errors on video page)

Canadians are concerned about the potential impact of a civil conflict in the United States on their country due to their proximity and membership in NATO.
Speculation arises about what Canada should do if the U.S. experiences a collapse or civil conflict.
The scenario presented involves two groups emerging from a contested election: the legitimate government and the rebels.
NATO, including Canada, is likely to support the legitimate government due to control over the U.S.' strategic arsenal.
Other nations like China and Russia may support the rebel group, complicating the situation further.
Canada might become a staging area for NATO if conflict spreads in the U.S., given their shared border.
Concerns for Canada include increased border security to prevent spillover of conflict and handling asylum seekers from the U.S.
Suggestions include relaxing immigration restrictions, preparing asylum qualifications, and expediting processing for different groups based on resources.
Documentation and banking issues for Americans seeking asylum in Canada are also addressed.
Handling millions of firearms from asylum seekers fleeing a civil conflict is noted as a significant challenge for Canada.

Actions:

for canadians, policymakers,
Prepare for potential influx of asylum seekers from the U.S. by easing immigration restrictions and expediting processing (suggested).
Enhance border security to prevent spillover of conflict into Canada (implied).
Set up accommodations and processing centers for asylum seekers, including handling firearms (implied).
Make it easy for Americans seeking asylum to handle banking and documentation issues (implied).
</details>
<details>
<summary>
2022-01-08: Let's talk about must-win elections for Dems.... (<a href="https://youtube.com/watch?v=FIycjPOSRIA">watch</a> || <a href="/videos/2022/01/08/Lets_talk_about_must-win_elections_for_Dems">transcript &amp; editable summary</a>)

NPR's list reveals Secretary of State candidates spreading misinformation about elections, urging Democratic Party action in must-win elections.

</summary>

"These are must-win elections."
"If you live in one of those states, and you have ever desired to become active in electoral politics, now is your time."

### AI summary (High error rate! Edit errors on video page)

NPR compiled a list of candidates running for Secretary of State who spread misinformation or believed baseless claims about the 2020 election.
These candidates hold the power to count votes and potentially alter election outcomes.
Candidates who believed in misinformation may intervene due to a perceived moral duty.
Those running for Secretary of State positions must have good judgment and understanding of election processes.
The Democratic Party must prioritize winning these Secretary of State elections to secure fair elections.
Historically, Secretary of State elections have not received much attention but are critical for election integrity.
Beau urges viewers to read the article for a list of candidates and states they are running in.
Residents in states with concerning candidates should get involved in electoral politics to make a difference.
Beau stresses the long-term importance of these elections in maintaining election integrity.

Actions:

for democratic party members,
Get involved in electoral politics in your state (implied)
Prioritize winning Secretary of State elections for fair and secure elections (implied)
</details>
<details>
<summary>
2022-01-07: Let's talk about Ted Cruz, Russia, and pipelines.... (<a href="https://youtube.com/watch?v=K23p-vjVMRo">watch</a> || <a href="/videos/2022/01/07/Lets_talk_about_Ted_Cruz_Russia_and_pipelines">transcript &amp; editable summary</a>)

Senator Cruz pushes for pipeline sanctions, risking foreign policy leverage, while Democrats reconsider, potentially impacting national security and peace.

</summary>

"Expend that leverage."
"Removing that leverage is a national security issue."
"It provides the Biden administration with more leverage."

### AI summary (High error rate! Edit errors on video page)

Senator Ted Cruz has legislation in the Senate aiming to force sanctions on a pipeline carrying natural gas from Russia to Germany, worth billions of dollars.
Democrats may be changing their stance on supporting sanctions, preferring to leave it to the Biden administration due to diplomatic engagements with Russia.
Cruz is frustrated by this change and wants to impose sanctions immediately to utilize leverage.
Beau warns against expending leverage too early, drawing parallels to issues faced in Afghanistan due to premature actions.
Cruz initially secured a vote on his legislation by releasing holds on ambassador appointments, contradicting his supposed concern for American foreign policy.
Beau suggests that imposing sanctions could have significant implications on peace and potential invasion in Ukraine.
He advises Cruz to step back and let the State Department handle foreign policy decisions, considering the current delicate global situation.
The decision on imposing sanctions holds critical importance as the Biden administration navigates challenges with near-peer competitors.

Actions:

for policymakers,
Contact your representatives to voice support or opposition to imposing sanctions (implied).
Stay informed and engaged in foreign policy decisions impacting national security (implied).
</details>
<details>
<summary>
2022-01-07: Let's talk about Hannity, Fox, and ethics.... (<a href="https://youtube.com/watch?v=ggQht-I2AZo">watch</a> || <a href="/videos/2022/01/07/Lets_talk_about_Hannity_Fox_and_ethics">transcript &amp; editable summary</a>)

Beau questions Sean Hannity's journalistic ethics, labels Fox News as a propaganda machine, and explains why Hannity isn't held to journalistic standards.

</summary>

"Why isn't Sean Hannity being held to account for his unethical behavior?"
"Because he's not a journalist."
"Fox News is not a journalistic outlet. It's a propaganda machine."
"Nobody's expecting him to be held to journalistic ethics because he's not a journalist."
"Why isn't anybody riding that horse? Because it's a chicken."

### AI summary (High error rate! Edit errors on video page)

Questions journalistic ethics regarding Sean Hannity and his advising of the president.
Points out the lack of accountability for Hannity's unethical behavior.
Expresses confusion over why Fox News isn't holding Hannity accountable.
States that Hannity is not a journalist but a commentator.
Views Fox News as a propaganda machine rather than a journalistic outlet.
Mentions the importance of personal ethics in disclosing biases.
Suggests Hannity may face consequences for not disclosing information to viewers.
Explains why he isn't leading a charge against Hannity for violating journalistic ethics.
Comments on the misleading nature of Fox News using "news" in its name.
Emphasizes the danger of government regulation on who can be considered a journalist.

Actions:

for media consumers,
Challenge media outlets promoting propaganda (implied)
Advocate for ethical journalism standards (implied)
</details>
<details>
<summary>
2022-01-06: Let's talk about whether it was really a coup attempt.... (<a href="https://youtube.com/watch?v=Q6zjigTxfQ4">watch</a> || <a href="/videos/2022/01/06/Lets_talk_about_whether_it_was_really_a_coup_attempt">transcript &amp; editable summary</a>)

Beau analyzes the Capitol event, clarifies coup types, and criticizes misperceptions of political violence fueled by fictional portrayals, urging for informed discourse and rejecting advocacy based on ignorance.

</summary>

"There's not actually any debate in the circles that study political violence over whether or not this was a coup attempt."
"If you understood anything about the topic, you'd know that."
"You have no business advocating for something like that if you don't know what happened on the 6th."
"Because people refuse to actually learn what this [political violence] would be like."
"It's not Red Dawn."

### AI summary (High error rate! Edit errors on video page)

Analyzing whether a specific term is appropriate for a certain event that took place at the Capitol, leading to ongoing debates even a year later.
Describing different types of coups and suggesting that the event might have been an attempted self-coup rather than a dissident coup.
Mentioning the Klein Center for Advanced Social Research's classification of the event as an attempted dissident coup, which Beau disagrees with.
Emphasizing that there is no debate within political violence study circles about whether the event was a coup attempt; the debate revolves around what type of coup it was.
Criticizing the lack of understanding and misperceptions about political violence, attributing it to reliance on TV and movies for information.
Arguing that advocating for civil conflict without a proper understanding of political violence is irresponsible and dangerous.
Expressing concern over the impact of misleading rhetoric on the country's stability and urging people to seek education beyond fictional portrayals of violence.

Actions:

for educators, activists, researchers,
Research different types of coups and political violence to understand the nuances and implications (suggested).
Engage in critical media literacy to differentiate between fictional representations and reality in political events (implied).
Advocate for accurate education about political violence and discourage misinformation spread through popular media (exemplified).
</details>
<details>
<summary>
2022-01-06: Let's talk about Trump's ex-press secretary planning action.... (<a href="https://youtube.com/watch?v=KHvdM7qGyMs">watch</a> || <a href="/videos/2022/01/06/Lets_talk_about_Trump_s_ex-press_secretary_planning_action">transcript &amp; editable summary</a>)

Former Trump officials, including Grisham, plan formal opposition to prevent Trump's return, while lower-level staff testimonies may lead to significant revelations.

</summary>

"A woman named Stephanie Grisham is the former Trump press secretary."
"My guess is that it's going to be some formal political thing."
"Those who worked for the Trump administration are regretting that decision."
"The people in the room, they know what happened."
"It's something that I can see turning into big news."

### AI summary (High error rate! Edit errors on video page)

Stephanie Grisham, former Trump press secretary, mentioned a meeting with other former Trump officials to stop Trump from running again.
Grisham had openly cooperated with the January 6th committee.
The group of former Trump officials may be trying to establish formal opposition to Trump returning to power.
It's unclear what "formal" opposition means—could be political action committees or providing information to prosecutors.
Grisham's actions post-Trump administration have been limited to putting out a book.
There is speculation that the meeting could be a way to garner attention for Grisham's book.
Some former officials are serious about ensuring Trump doesn't return to power.
The January 6th committee has been interviewing lower-level staff, potentially leading to damaging revelations.
The committee is focusing on individuals who were present, overheard things, and were asked to do minor tasks.
The information gathered by lower-level staff could be more damaging than anticipated.

Actions:

for political observers,
Keep a close watch on the developments regarding former Trump officials' plans (implied).
Stay informed about the testimonies of lower-level staff members to the January 6th committee (implied).
</details>
<details>
<summary>
2022-01-05: Let's talk about Hannity's texts and a golden moment... (<a href="https://youtube.com/watch?v=kpNKuOpbEto">watch</a> || <a href="/videos/2022/01/05/Lets_talk_about_Hannity_s_texts_and_a_golden_moment">transcript &amp; editable summary</a>)

The committee offers Hannity a chance to cooperate, shaping a narrative that could save his career while prompting others to speak up.

</summary>

"He's being presented with an opportunity to save his skin and paint that narrative however he'd like."
"Hannity, more so than the others, probably understands that that's kind of all they need."
"What the American public doesn't know, well, that's what makes us the American public."

### AI summary (High error rate! Edit errors on video page)

Committee released excerpts from Sean Hannity's text messages, sparking questions about his involvement and knowledge.
Texts can be interpreted differently, either painting Hannity as complicit or as a concerned individual trying to advise against certain actions.
Committee may be offering Hannity an opportunity to cooperate and potentially save his career by sharing what he knows.
Many high-profile individuals are obstructing the investigation, but the committee notes that nine out of ten people they've spoken to have cooperated.
People cooperating with the committee are likely those behind the scenes and not in the public eye, such as assistants, legal advisors, security professionals, etc.
Hannity, with his experience in the industry, understands the dynamics of these behind-the-scenes individuals talking to the committee.
Hannity has the chance to shape the narrative by cooperating and sharing his knowledge.
Cooperation from Hannity might influence others to do the same or trigger memories of those involved.
Hannity's connections and inside information could be valuable to the committee.
Without context, it's hard to determine the full extent of Hannity's involvement or concerns from the excerpts.

Actions:

for political commentators, investigators,
Contact the committee if you have relevant information to share (suggested)
Cooperate with investigative bodies if you have insights into significant events (implied)
</details>
<details>
<summary>
2022-01-05: Let's talk about Garland's speech.... (<a href="https://youtube.com/watch?v=mVms5KEF9_I">watch</a> || <a href="/videos/2022/01/05/Lets_talk_about_Garland_s_speech">transcript &amp; editable summary</a>)

Cautious optimism about accountability in the investigation into Capitol events, stressing the need for tangible actions over speeches to safeguard democracy.

</summary>

"Until then, I'm very cautious in getting hopeful."
"If they were involved, they were going to go after them."
"That's accountability on that level. Sure, it would help, but that doesn't actually solve the problem."
"Nothing will until I see action at those levels."
"I just hope they see the institution of democracy as worth protecting as well."

### AI summary (High error rate! Edit errors on video page)

Cautious optimism about the investigation into the events at the Capitol, due to historical protection of the presidency.
Hope is present in the investigation process starting with lower-level individuals and moving up.
Real optimism hinges on indictments of significant individuals linked directly to the Capitol events.
Emphasizes the importance of real accountability to safeguard democracy.
Warns against blind faith in the Department of Justice (DOJ) to solve all issues.
Acknowledges that DOJ's actions alone won't address the broader societal acceptance of harmful actions.
Stresses the necessity of seeing tangible accountability actions before becoming optimistic.
Raises concerns about the long-standing tradition of shielding the presidency from scandal.
Expresses the value of both the presidency and democracy, underscoring that any erosion of democratic values exacerbates existing problems.
Despite recognizing the speech as good, Beau remains skeptical until concrete actions demonstrate accountability at higher levels.

Actions:

for citizens, activists, voters,
Monitor and demand accountability actions at all levels (implied)
Advocate for transparency and thorough investigations into events of January 6th (implied)
Participate in civic engagement to uphold democratic values and hold leaders accountable (implied)
</details>
<details>
<summary>
2022-01-04: Let's talk about how the government is handling the cases.... (<a href="https://youtube.com/watch?v=urTSUSVTMpA">watch</a> || <a href="/videos/2022/01/04/Lets_talk_about_how_the_government_is_handling_the_cases">transcript &amp; editable summary</a>)

Beau navigates the moral dilemmas and pragmatic consequences of the government's response to January 6th, advocating against extreme measures that could backfire.

</summary>

"I understand the anger and I understand that initial urge to say, let me tell you how you fix this. But if you think about it for any length of time, it's not really a fix."
"Generally speaking, I am for the preservation of life."
"It's a bad idea to seek that extreme level of sentencing for what happened that day."

### AI summary (High error rate! Edit errors on video page)

Explores the moral and pragmatic aspects of the government's response on the day of January 6th and the response today.
Considers the debate around seeking capital punishment for the individuals involved in the incident.
Shares his personal stance on capital punishment, leaning towards the preservation of life and against revenge.
Points out that many defendants had cognitive issues and were manipulated into participating in the events.
Believes that capital punishment is not a deterrent and questions its effectiveness in addressing the situation.
Warns against creating martyrs by employing extreme measures, citing historical examples.
Argues that seeking extreme sentencing could embolden the perpetrators and fuel further resistance.
Mentions the disparity in sentencing between the January 6th cases and other criminal cases.
Advocates for reducing sentences rather than increasing penalties for everyone.
Expresses gratitude for the government's restrained response on January 6th, believing an all-out reaction could have escalated the situation.

Actions:

for activists, policymakers, citizens,
Advocate for fair and just sentencing in cases related to the January 6th events (suggested)
Support initiatives that aim to address cognitive issues and prevent manipulation in vulnerable individuals (exemplified)
</details>
<details>
<summary>
2022-01-04: Let's talk about Schumer and the filibuster.... (<a href="https://youtube.com/watch?v=sj2fvictjxU">watch</a> || <a href="/videos/2022/01/04/Lets_talk_about_Schumer_and_the_filibuster">transcript &amp; editable summary</a>)

Schumer considers changes to the filibuster, facing dilemmas on safeguarding voting rights while avoiding creating tools for opponents to exploit.

</summary>

"It's about power. It's about their party. It's not about the country."
"Changing the filibuster in and of itself sets the precedent that Republicans can change it next time they're in power."
"The exemptions, sure, it's easier for this issue, but make no mistake about it, they will carve out exemptions for whatever issue they want next."
"At this point in history, the filibuster isn't about debate, because no debate is taking place."
"It's a talking filibuster, no lies, and it has to be about the legislation."

### AI summary (High error rate! Edit errors on video page)

Explains the possible changes to the filibuster being discussed by Schumer due to Republicans not going along with voting rights legislation.
Notes that the filibuster is a Senate rule, not part of the Constitution, originally intended for ample debate time.
Schumer is open to changes in the filibuster but not getting rid of it entirely.
Two options are on the table: creating an exemption for voting rights legislation or implementing a talking filibuster where senators have to actively speak.
Democrats are hesitant to eliminate the filibuster as they see it as a tool beneficial when not in power.
Concerns that if exemptions are made, Republicans will do the same for their priorities, ultimately leading to removing the filibuster altogether.
Beau suggests a talking filibuster with strict rules on staying on-topic about legislation and penalizing falsehoods with an immediate end to the filibuster.
Changing the filibuster sets a precedent for future alterations by whichever party holds power.
Points out that the fear of providing Republicans with a tool disappeared once public discourse on changing the filibuster began.
Beau argues for pursuing the talking filibuster with specific rules since it allows for potential filibustering in critical situations while removing unnecessary roadblocks.

Actions:

for legislators, political activists,
Advocate for a talking filibuster with strict rules to ensure transparency and accountability (suggested).
Push for changes in the filibuster system that prioritize legislative debate over partisan power struggles (suggested).
Engage in public discourse and advocacy to support measures that safeguard voting rights while maintaining Senate functionality (implied).
</details>
<details>
<summary>
2022-01-03: Let's talk about small towns, education, and change.... (<a href="https://youtube.com/watch?v=3L5m6jiETkE">watch</a> || <a href="/videos/2022/01/03/Lets_talk_about_small_towns_education_and_change">transcript &amp; editable summary</a>)

A young woman faces a life-changing scholarship offer while her boyfriend raises concerns about change, but Beau urges her to embrace growth and education.

</summary>

"The odds of your relationship with your boyfriend surviving you going off to college are slim to none."
"You are so good at something that you love, somebody is willing to basically fork out a hundred grand for you to go to school so you can do it."
"Of course take it. You are so good at something that you love, somebody is willing to basically fork out a hundred grand for you to go to school so you can do it."
"You're probably gonna meet somebody. You know, small-town dating. Not always a choice. Sometimes a lack of options."
"How long would it take you to pack? because we both know you would have gone."

### AI summary (High error rate! Edit errors on video page)

A young woman from a small town has the chance for a full scholarship at a better school six hours away, with the potential for a master's degree if she maintains good grades and an extracurricular activity.
Despite this amazing offer, her boyfriend of many years doesn't want her to go because he's afraid it will change her.
Beau shares his own experience of leaving his small town for education and how it changed him and his perspective on life.
He encourages the young woman to take the scholarship, acknowledging that the experience will indeed change her, but that's the whole point of growth.
Beau predicts that the relationship with her boyfriend may not survive her leaving for college, but new opportunities and connections will arise.
He advises her to seize the incredible chance for education and personal growth, especially since someone is willing to invest over a hundred thousand dollars in her potential.
Beau subtly questions whether the boyfriend, if given a similar career advancement, wouldn't hesitate to leave for better prospects.
He leaves the decision to the young woman, acknowledging that the comments section may have valuable insights on the situation.

Actions:

for young adults,
Pursue the educational opportunities available to you, even if they involve significant changes in your life (suggested).
Embrace personal growth and new experiences for a chance at a better future (suggested).
Seek advice and perspectives from others, especially if facing a difficult decision (implied).
</details>
<details>
<summary>
2022-01-03: Let's talk about newly disclosed information about the 6th.... (<a href="https://youtube.com/watch?v=PecFLDrmya8">watch</a> || <a href="/videos/2022/01/03/Lets_talk_about_newly_disclosed_information_about_the_6th">transcript &amp; editable summary</a>)

Disclosing year-old news slowly to lessen surprise during hearings about elite military forces involved in responding to a coup attempt.

</summary>

"Laws become very flexible. They become very gray."
"The most elite units of this country's military wouldn't stand idly by during a coup attempt."
"The echo chambers that exist online can be dangerous."

### AI summary (High error rate! Edit errors on video page)

Disclosing year-old news slowly to lessen surprise during hearings about a group taking over a building in D.C.
During a coup attempt, Beau live tweeted and provided context and background information.
Reports of specific helicopters being used by elite forces to retake the building.
Questions about the legality of using active duty military against American citizens.
Elite forces were seconded to the Department of Justice, not the National Guard.
Military operators under the Attorney General's control operated under unknown contingency plans.
The National Mission Force, including FBI's hostage rescue and render safe teams, was present.
Military operators were ready to respond but not for crowd control.
In emergencies, laws become flexible and gray.
Elite military units wouldn't stand idly by during a coup attempt.

Actions:

for activists, advocates, researchers,
Read the Newsweek article titled "Secret Commandos Will Shoot to Kill Authority Were at the Capitol" (suggested)
Educate others on the flexibility of laws in emergencies (exemplified)
</details>
<details>
<summary>
2022-01-02: Let's talk about human rights or constitutional rights.... (<a href="https://youtube.com/watch?v=zkxKYLqC2CI">watch</a> || <a href="/videos/2022/01/02/Lets_talk_about_human_rights_or_constitutional_rights">transcript &amp; editable summary</a>)

Settling a debate on constitutional vs. human rights, Beau finds common ground and encourages understanding.

</summary>

"At the end of the day, human rights are constitutional rights. There's not a difference."
"I don't believe that either one of you have read the others preferred document."
"Both of these documents are intended to shield people from the actions of their governments."
"I don't really think this should be an argument, to be honest."
"Y'all should plan a family night and sit down with both documents and see if you can figure out where they overlap."

### AI summary (High error rate! Edit errors on video page)

Settling a debate between a father and daughter over constitutional rights and human rights.
Daughter believes in Universal Declaration of Human Rights, father believes in US founding documents.
Father argues that Constitution gives authority to sign the declaration, daughter thinks Constitution is outdated.
Beau appreciates the beautiful language in the US founding documents but acknowledges flaws.
Points out the similarities between the Universal Declaration of Human Rights and the US Constitution.
Notes that both documents acknowledge existing rights rather than granting them.
Suggests that if people read each other's preferred document, the debate might not exist.
Advocates for choosing the document that provides the most rights in any given situation.
Emphasizes that both human rights and constitutional rights aim to protect individuals from government actions.
Encourages the family to have a night to compare and understand both documents.

Actions:

for father and daughter,
Plan a family night to sit down with both documents and compare where they overlap (suggested)
</details>
<details>
<summary>
2022-01-02: Let's talk about how the US and China's contest will end.... (<a href="https://youtube.com/watch?v=qFxiPDEUYSE">watch</a> || <a href="/videos/2022/01/02/Lets_talk_about_how_the_US_and_China_s_contest_will_end">transcript &amp; editable summary</a>)

Beau outlines the strategies of falling in line or falling apart in the emerging near-peer contest between the U.S. and China, cautioning against assuming a U.S. win and stressing the need for a nuanced approach.

</summary>

"Fall in line or fall apart."
"U.S. foreign policy does not occur in a vacuum. Every move will have a counter move."
"Moving slowly also assumes that the Republican Party won't obtain power anytime soon."
"It's easy to make the assumption that we'll win because in near-peer contests the United States typically does."
"They're assuming a U.S. win, and I don't know that there's grounds to make that assumption."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the emerging near-peer contest between the United States and China in foreign policy.
He outlines two main strategies for how such contests tend to resolve: fall in line or fall apart.
Under the fall in line strategy, the U.S. aims to encourage China to accept second place through diplomacy and containment.
The fall apart strategy involves the U.S. containing China tightly until internal pressures lead to the regime falling apart.
Beau criticizes foreign policy pundits for presupposing a U.S. win without considering the Chinese response.
He points out that U.S. foreign policy actions will have counteractions from China in this international "poker game."
Beau expresses concerns about the U.S.' standing due to past actions like withdrawing from allies, impacting its ability to counter Chinese influence.
He notes the internal divisions within the U.S., especially within the Republican Party, which may affect its approach to international leadership.
Beau warns against assuming a U.S. win in this contest and stresses the need for a nuanced and cautious approach in dealing with China.
He suggests that moving slowly and rebuilding trust globally is key for the U.S., but this approach may be at risk if certain factions within the Republican Party gain power.

Actions:

for foreign policy analysts,
Keep abreast of foreign policy developments and advocate for nuanced approaches (implied).
Engage in critical analysis of international relations and question assumptions (implied).
</details>
<details>
<summary>
2022-01-01: Let's talk about the future of 2022.... (<a href="https://youtube.com/watch?v=2eA02s3X0JY">watch</a> || <a href="/videos/2022/01/01/Lets_talk_about_the_future_of_2022">transcript &amp; editable summary</a>)

Beau contrasts two visions of the future in 2022, urging immediate action to prevent a dystopian reality and work towards a brighter, more sustainable world.

</summary>

"We're running out of time to figure out what kind of world we're going to have."
"If we don't make serious changes, it's definitely going to look more like Soylent Green than the Jetsons."
"All we have to do really is set a course for some idyllic future and work towards it."
"We're going to have to adjust and adapt."
"This could be the year it starts to turn around."

### AI summary (High error rate! Edit errors on video page)

Beau welcomes viewers to 2022 and muses about fictional stories set in the current year.
He contrasts two visions of the future: the Jetsons' semi-idyllic world and the grim reality portrayed in a story about a cop investigating a company.
Despite advancements like smart watches, the world is plagued by issues like climate catastrophe, overpopulation, and extreme wealth inequality.
Beau references "Soylent Green," a story from the same era as the Jetsons, depicting vastly different futures.
Urging for immediate action, Beau warns that without addressing global inequities and environmental concerns, the future may resemble "Soylent Green" more than the Jetsons.
He stresses the importance of choosing a course towards a better future and being prepared to adapt to inevitable changes.
Beau encourages viewers to make 2022 a year of commitment towards a brighter future akin to the Jetsons' world.
He leaves viewers with a thought-provoking message about the potential for positive change in the current year.

Actions:

for general public,
Commit to making a better future by taking concrete steps towards sustainability and equity (suggested)
Advocate for policies that address climate change and wealth inequality (implied)
</details>
<details>
<summary>
2022-01-01: Let's talk about learning from the Jetsons and pollution... (<a href="https://youtube.com/watch?v=quLe88pi9w8">watch</a> || <a href="/videos/2022/01/01/Lets_talk_about_learning_from_the_Jetsons_and_pollution">transcript &amp; editable summary</a>)

Exploring the history of the Jetsons reveals the impact of added social commentary in reboots and the lasting influence on societal views.

</summary>

"If you believe the Jetsons live in the sky because of pollution, you are probably a little bit more in tune to environmental issues."
"Decades later, people still remember those messages. They become part of popular culture, and they shift views."
"Changes and social commentary in franchises can work and influence views over time."

### AI summary (High error rate! Edit errors on video page)

Exploring the history of the Jetsons and the idea of an ideal future in the 60s.
Received messages correcting the misconception that the Jetsons lived in the sky due to pollution.
Some viewers opposed reboots becoming "woke" with added social commentary.
The Jetsons initially aired in the 60s, with no pollution-related storyline.
Pollution and social commentary were added in reboots to raise awareness.
People may be less accepting of new ideas as they age, influenced by tradition and peer pressure.
Introducing social commentary in franchises can have long-lasting impacts and raise awareness.
New generations may not be aware of the original story and see added commentary as part of the plot.
Social commentary serves as a warning for potential societal consequences.
Criticism arises from those resistant to change and those who view added commentary as performative.
Despite criticism, social commentary in reboots can be impactful and shape popular culture.
Changes and social commentary in franchises can work and influence views over time.

Actions:

for viewers, franchise fans,
Analyze the social commentary introduced in entertainment and its impact on shaping views (implied).
</details>
