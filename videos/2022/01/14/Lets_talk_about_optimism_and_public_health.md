---
title: Let's talk about optimism and public health....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w1XjtqILYts) |
| Published | 2022/01/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the optimism surrounding the potential end of the pandemic and transition into an endemic period.
- Cautiously optimistic about moving through the period of transitioning out of the pandemic.
- Mention of a growing body suggesting the current transition phase.
- Emphasizing the need for confirmation from medical doctors before celebrating the end.
- Warning against premature declarations of victory as another variant could emerge.
- Stressing the importance of maintaining precautions like hand-washing, mask-wearing, and social distancing.
- Advising to continue with effective measures to avoid being caught off guard.
- Urging to stay vigilant and get vaccinated even if nearing the end of the pandemic phase.
- Noting that transitioning into the endemic phase means the virus will still be present but less severe.
- Encouraging everyone to stay proactive and not let their guard down.

### Quotes

- "Don't be that guy."
- "I'm not going to declare victory until the MDs are saying it."
- "Do not be that person who gets caught up in it right before it's over."

### Oneliner

Beau cautiously addresses the optimism around the potential end of the pandemic, stressing the need for continued precautions and medical confirmation before celebrating. 

### Audience

Individuals and communities

### On-the-ground actions from transcript

- Keep practicing preventive measures (implied)
- Get vaccinated (implied)

### Whats missing in summary

Importance of staying vigilant and not becoming complacent during the transition phase.

### Tags

#PublicHealth #Pandemic #Endemic #Precautions #Vaccination


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about whether or not
we're there yet.
Talk about optimism and our public health issue,
because I got a message.
It says, besides your first video,
which I have come to realize I am never going to live down,
you've been really good at breaking down
everything that's been happening with the public health
issue, in quotation marks.
I've been waiting for an update.
Do you think we're at the end, like a lot of people are
saying?
I am cautiously optimistic.
If you don't know, there is a growing body
that suggests that right now, we are
moving through the period in which we are transitioning out
of the pandemic.
There are people who are saying that when we look at it,
when we look back on it, in hindsight,
that this is going to be the dividing line.
And that's great news.
What I want to point out is while that sentiment is coming
out pretty strong from a lot of people,
there are people who, to my knowledge,
are looking at statistics and modeling.
And I've looked at the same thing,
and I see what they're saying.
They are PhDs.
I'm not popping the champagne until I hear it from the MDs.
While it is possible that this is that transition period,
it's also possible that there's going to be another variant.
That's another possible future outcome.
We don't know yet.
And the other part of that sentence,
we're transitioning out of the pandemic
and into the endemic period, which is it's still around.
It's just not causing massive amounts of loss.
I'm not going to declare victory until the MDs are saying it,
until the medical doctors are saying it.
But I am optimistic.
Now, another word of caution here is don't be that guy.
Do not be that guy.
Keep your posture up.
Whatever you've been doing that has worked for you thus far,
continue doing it.
Wash your hands.
Don't touch your face.
Wear a mask.
Wear the N95.
Wear the good mask if you have one.
Stay at home.
Social distance.
Do everything that you're supposed to.
If this is that period where we're almost done,
you don't want to mess up now.
So keep your posture up.
Keep all of your precautions that
have worked for you so far.
If we are at the end of it, nearing the end of it,
you don't want to be one of the last ones.
And get your vaccines.
Get your shots.
If you haven't done it yet, now is a good time to do it.
Because again, even if the docs say
we're leaving the pandemic period,
that doesn't mean it's gone away.
It just means it's harder.
It's harder to get it because it's not
transmitting as easily.
And I guess the modeling suggests
that between the vaccines and people who have got it,
it's going to get harder and harder for it to spread.
But again, I'm not going to say that until I hear it
from the medical doctors.
I see what the people who are looking
at the statistics and the modeling, I see what they see.
But there are a lot of other possible outcomes.
So we don't want to declare victory too soon.
And you don't want to be that person who gets caught up
in it right before it's over.
So keep your posture up.
Keep doing whatever has worked for you so far.
Don't get lax now.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}