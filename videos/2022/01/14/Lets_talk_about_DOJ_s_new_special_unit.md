---
title: Let's talk about DOJ's new special unit...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AKCRfu7vnoU) |
| Published | 2022/01/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice announced creating a new unit for domestic terror, sparking questions on its effectiveness and structure.
- New unit won't be a game changer, likely a small group of experts for coordination and information sharing.
- In the US, targeting domestic groups is limited due to freedom of assembly and speech.
- Concerns raised about the potential abuse of power if the government could designate groups.
- Setting up the new unit seems more academic and for coordination rather than a significant tool.
- Beau cautions against pushing for special teams with designation powers due to potential negative consequences.
- FBI and DOJ already respond to domestic incidents swiftly, so additional specialized teams may not be necessary.
- Beau believes the announcement is mostly for PR purposes, lacking a substantial reason.
- The new unit might be useful in preventing civilian targeting but won't eradicate movements in the US.
- Overall, Beau doesn't see this announcement as a significant development.

### Quotes

- "This isn't going to be a game changer."
- "That kind of power, under the control of the executive branch, is a really bad idea."
- "But it might be a useful tool for people who are trying to stop the targeting of the civilians."

### Oneliner

Department of Justice's new unit for domestic terror won't be a game changer due to limitations on targeting groups and potential abuse of power.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Advocate for community-based solutions to address domestic terror (implied)
- Engage in dialogues and workshops on civil rights and freedoms (implied)

### Whats missing in summary

Detailed examples and elaboration on the potential consequences of giving the government power to designate groups.

### Tags

#DOJ #domesticterror #abuseofpower #freedomofspeech #communitypolicing


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the announcement
from the Department of Justice.
DOJ announced that they were going
to create a new unit, task force, working group.
I don't know what they're calling it.
And as soon as that announcement went out,
questions started coming in.
One of the big ones is, why don't we already have this?
Another is, how big will it be?
How effective will it be?
What will it look like?
So on and so forth.
The structure wasn't really laid out in the announcement.
But we'll kind of go over, and I can give you my best guess on it.
If you don't know what I'm talking about,
the Department of Justice announced
that they're going to create a new unit that
is going to specialize in domestic terror.
And I guess the biggest question is, how effective will it be?
It won't be.
I mean, not really.
This isn't going to be a game changer.
I think people are picturing like the Untouchables,
but for terror groups, that's not what it's going to be.
More than likely, this is going to be a group of, I don't know,
a dozen subject matter experts.
And they'll be in a working group, a little unit attached to,
I think, the National Security Division.
And it'll be a clearinghouse for information.
It'll help coordinate cases.
It'll be a phone number that agents in the field
can call if they have questions.
But it's not going to be what I think a lot of people
are envisioning.
The question that came in the most
is, why didn't we already have this?
For the same reason, this isn't going to be super effective.
In the United States, you have the freedom of assembly.
You have the freedom of speech.
The federal government can't point to a group or a belief
and say, you can't believe that.
You can't belong to that group.
They can't designate domestic groups.
Therefore, units that are targeting domestic groups,
well, they're kind of not supposed to be there.
And they don't have a lot of ability to do anything.
They have to wait until crimes are being committed.
Or they have to try to disrupt them before they start.
It's harder than it sounds.
I know right now, because of the events of the 6th
and everything that has gone on, you
may be sitting there saying, well,
why don't we have that power?
Why can't they do that?
Maybe that's a good idea.
And sure, right now, that might seem like a good idea.
However, what about two years ago?
Would you think it was a good idea then?
How long do you think it would have taken former President
Obama to designate BLM?
That kind of power, with the way the United States is structured,
that kind of power, under the control of the executive branch,
is a really bad idea.
It will be abused.
What they're setting up now appears to be mostly academic.
It appears to be mostly a place for coordination and advice.
And sure, there's probably some value in that.
But it's not going to be a game changer.
Now, when it comes to domestic incidents,
understand the FBI shows up.
DOJ is on scene pretty quick.
It's kind of a given that that's going to happen.
The desire to have a special team to go after him,
I understand it.
But I would be very cautious of asking for that,
of supporting that idea.
Because in order for it to be effective,
they have to have the power to designate groups.
And once you open that door in this country,
it's going to go sideways really, really fast.
So this isn't something that's a game changer.
I don't foresee this being something
that helps stamp out movements or anything
like that in the US.
But it might be a useful tool for people
who are trying to stop the targeting of the civilians.
So you have that.
But mostly this announcement's kind of PR.
There's not really a reason to even make this announcement.
They could have just put it together.
It's not going to be a game changer.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}