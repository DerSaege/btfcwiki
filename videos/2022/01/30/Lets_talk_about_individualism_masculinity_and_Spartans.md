---
title: Let's talk about individualism, masculinity, and Spartans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gcmNYvCh5ts) |
| Published | 2022/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responds to a message about individualism, masculinity, femininity, and historical references.
- Explains the misconception of individualism as a masculine trait.
- Clarifies that the reference to "rule 303" is not related to Irish heritage.
- Talks about the Spartans at Thermopylae and the balance between individualism and collective action.
- Emphasizes the importance of collective networks and bargaining.
- Challenges the notion of fighters being solely individualists.
- Stresses the power of collective action over individual strength.
- Argues against promoting aggression towards political enemies.
- Advocates for inclusivity and embracing all aspects of society for progress.

### Quotes

- "You're only as strong as your weakest link."
- "The strength of the individual lends itself well to collective action."
- "The general idea of separating things up between the masculine and feminine [...] it's going to be a failure."
- "Just because the social contracts of today are shifting, it doesn't negate the fact that all roles have to be fulfilled."
- "If you want to be a strong individual, embrace all aspects of it."

### Oneliner

Beau addresses misconceptions about individualism and masculinity, advocating for a balance between strength as an individual and collective action while challenging traditional gender roles.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Build community networks and support collective bargaining (implied)
- Embrace inclusivity and embrace all aspects of society for progress (implied)

### Whats missing in summary

Beau's nuanced perspective on individualism, masculinity, femininity, and collective action can be best understood by watching the full transcript. 

### Tags

#Individualism #Masculinity #Femininity #CollectiveAction #GenderRoles


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about individualism
and masculinity and femininity and song lyrics
and some historical facts and ancient combat,
all kinds of stuff, because I got a message that came in
that is pretty unique.
So I'm gonna read the whole thing
and then we'll go through it.
And in sections, I have a question about individualism.
You seem to promote individualism
and masculine strengthening of self
with every chance you get.
I have to assume because of your Irish heritage,
rule 303 is a reference to a nation once again.
I'm drawing references and imagery of the Spartans
at Thermopylae, hints to a desire
to promote the masculine individualism
and taking control when needed.
How do you also promote collective ideas
like collective bargaining and community networks?
This seems ridiculously incoherent.
A major part of your branding is tapping into the men
who individually face down the agogi,
but then you promote this feminine cooperative.
I love your political takes, but your philosophy needs work.
If you're going to promote metaphorical fighting politically
you need to acknowledge the great fighters
were always individualists.
You need to promote that figurative aggression
towards the political enemy.
First, I do wanna thank you for saying metaphorical
and figurative, acknowledging the nonviolent approach there.
So this is really unique on a whole bunch
of different levels.
So now we're gonna run through it.
Now that you know where it's going,
you seem to promote individualism
and masculine strengthening of self
with every chance you get.
Okay, I don't view individualism or strengthening of self,
the idea of being a strong individual person
as a masculine trait.
That's not the way I see that at all.
I have to assume because of your Irish heritage,
rule 303 is a reference to a nation once again.
It's not, but cool.
That's kind of cool.
For those that don't know,
a nation once again is a song from Ireland
and it references a nation once again
and it's a song about a nation once again.
It's a song from Ireland and it references 300 men
and three men, 303.
I mean, it tracks, it makes sense.
I do wanna point out just for the sake of clarity
that that's referencing two separate incidents,
300 at Thermopylae as mentioned in the message,
three at a battle in the early days of Rome
against the Etruscans.
It's two separate things.
But no, that isn't where that reference comes from,
but it doesn't change your overall point.
So we're just gonna kind of roll with it.
Drawing references and imagery of the Spartans at Thermopylae
hints to a desire to promote the masculine individualism
and taking control when needed.
How do you also promote collective ideas
like collective bargaining and community networks?
This seems ridiculously incoherent.
A major part of your branding is tapping into the men
who individually face down the agogi,
but then you promote this feminine cooperative.
And that's where you lose me.
Okay, so yeah, the Spartans,
they were widely viewed as some of the best
individual warriors on the planet at the time.
Absolutely.
And they did go through a lot of training
that built them as individuals,
that molded them into incredibly strong individual people.
Granted, absolutely.
But what happened the second they stepped on a field?
They didn't act individually.
Not at all.
The formation that they chose
actually specifically limits individual action,
and it makes them function as a unit.
It's why such a small number of troops
could hold off larger forces,
because it leveraged everybody's individual abilities
into a collective force.
And I think the easiest way to explain this
is that if you and I were in a shield wall
next to each other,
and I've got my shield up, right,
and you have yours up,
and you drop yours, I'm likely to get hit.
I'm likely to be the one that gets hurt,
because the shields cover the person next to you.
That feminine cooperative, I guess.
The reality is, if you're going to use
that type of militaristic vibe in your political imagery,
you have to be realistic about it.
Sure, SEALs today are seen widely
as some of the world's best individual fighters.
Absolutely.
But what are they called, the teams?
Collective action.
The strength of the individual
lends itself well to collective action.
I like collective action to work,
so I want strong individual links in that chain.
So I do promote individualism.
I do promote being the best that you can be
and striving to better yourself in every way,
not just physical strength,
because if you do that, and then you become involved,
and you become part of a community network,
or you engage in collective bargaining,
you make it that much stronger.
You're only as strong as your weakest link type of thing.
That's where it comes from.
And historically, that's been the most effective.
Now, as far as the greatest fighters being individualists,
no, and this goes back all the way to back then.
When you go through that kind of training,
you get stripped of your identity.
You're definitely not an individualist.
You are broken down and remolded.
You become part of something, in theory, greater than yourself.
Your individual attributes become nothing more
than items that are at the disposal of the larger.
So yeah, your individualism is important,
and it drives everything forward,
but it's that collective power that can actually get things done.
And then the part that I really disagree with is this last part.
If you're going to promote metaphorical fighting politically,
you need to acknowledge the greatest fighters were always individualists.
You need to promote that figurative aggression towards the political enemy.
Oh, absolutely not.
I'm fairly certain that the best fighters did not fight
because they hated what was in front of them.
That they fought because they loved what was behind them.
The general idea of separating things up between the masculine and feminine,
on some levels, yeah, it makes sense from a point of view of,
this is how society has operated.
But when you are trying to move towards a more egalitarian society,
a lot of that kind of slips away.
And it's also worth noting that up until very, very recently,
the roles, they weren't as open as they are now.
So trying to frame things in a way that appeals solely to the masculine
or solely to the feminine is going to be a failure.
It's not going to work.
And I would imagine that if you have this kind of take on something,
you would say that society was better off when men were men and women were women,
or something along those lines, whatever it is.
The idea that those roles both had to be filled, that's true no matter what.
No matter what, that has to happen.
Just because the social contracts of today and the roles,
the gender roles that have exist and the way people look at things are shifting,
it doesn't negate the fact that all of that has to be fulfilled for a functioning society.
And if you're trying to achieve something that is more fair, that is more equal,
you need everybody and you need all of those attitudes.
And if you want to be a strong individual,
it might be a good idea to be able to embrace all aspects of it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}