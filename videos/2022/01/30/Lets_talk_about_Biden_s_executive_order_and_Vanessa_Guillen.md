---
title: Let's talk about Biden's executive order and Vanessa Guillen....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HSIX7sqq9N8) |
| Published | 2022/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden signed an executive order in response to the Vanessa Guillen case at Fort Hood.
- The executive order makes sexual harassment a crime under the UCMJ, changing the handling of such cases.
- Previously, sexual harassment was handled within the chain of command, leading to issues like cover-ups and retaliation.
- With the new order, if someone covers up harassment, they can end up in cuffs, and cases can now be pursued outside the chain of command.
- Cases will now be investigated by CID instead of through administrative proceedings.
- The executive order will immediately change how cases are handled and investigated.
- It will likely be a significant tool in combatting sexual harassment in the military.
- Tracking of allegations of retaliation will also be implemented, likely referred to CID.
- CID has decided to increase the ratio of civilian investigators, which could lead to more effective investigations.
- The pressure applied after the Vanessa Guillen case led to these changes.

### Quotes

- "It is going to matter."
- "This is something that will have a positive impact."
- "While this doesn't actually bring justice to Vanessa Guillen, it may make the military a little bit more just."

### Oneliner

Biden's executive order in response to Vanessa Guillen case makes sexual harassment a crime under UCMJ, changing how cases are handled and investigated, a significant step in combatting military sexual harassment.

### Audience

Advocates, military personnel

### On-the-ground actions from transcript

- Reach out to Senate Armed Services and DOD directly to keep pressure for positive changes (implied).
- Support civilian investigators in handling cases effectively (implied).

### Whats missing in summary

The emotional impact and perseverance shown by those who pushed for change after the Vanessa Guillen case.

### Tags

#Military #SexualHarassment #VanessaGuillen #UCMJ #CID


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the executive order that Biden signed
and kind of talk about whether or not it matters.
You know, I love it when something good comes from something bad,
but it isn't often on this channel where we get to cover something that is horrible
and a year, more than a year later, there's a positive outcome.
And that's kind of what happened here.
This executive order, all of this, is in response to the Guillen case at Fort Hood,
to the Vanessa Guillen case.
Okay, so a lot of people asking, is this executive order, is it worth it?
Does it mean anything? Yeah, yeah, it's a game changer, to be honest.
So this executive order was called for by the NDAA of 2022.
And what this does is it makes sexual harassment a crime under the UCMJ.
Now, prior to this, this kind of harassment,
it was handled within the chain of command, really.
There's a longstanding tradition in the armed services of doing stuff that way.
The problem is, in this case, if the person responsible is Major Bob
and his best friend is Colonel Smith, well, your complaint may not go anywhere
and you may be retaliated against.
Switching it to a crime under UCMJ means a couple of things.
One, if Colonel Smith covers it up for Major Bob, Colonel Smith's going to end up in cuffs.
Two, is now it can be pursued outside of the chain of command.
And three, these cases will now be investigated not through an administrative proceeding,
but by CID, by the Criminal Investigative Division.
Yeah, it matters. It's a big deal.
This will dramatically and immediately change cases and how things are done.
When it comes to sexual harassment in the military,
this is probably going to be the tool that really, really changes it.
A criminal investigation being opened is very different than an administrative one.
It is a game changer.
My understanding is that part of the executive order also says
that they will start tracking any allegations of retaliation as well.
My guess would be the tracking ends up also being referred to CID.
And then it's also worth noting that aside from this executive order,
CID already decided to up the ratio of civilian investigators.
Now, their reasoning, I'm sure, was to bring in more experienced people or something like that.
The reality is having civilians run investigations like this would be incredibly beneficial,
because it does not matter what rank that person is wearing.
They don't care.
So, yeah, it's a game changer. It is a game changer.
It is going to matter.
This is something that will have a positive impact.
And it is because of the pressure that was applied after the Vanessa Guillen case,
because people reached out to Senate Armed Services.
People reached out to DOD directly.
And they didn't stop. They didn't give up.
And while this doesn't actually bring justice to Vanessa Guillen,
it may make the military a little bit more just.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}