---
title: Let's talk about Deep Goat and releasing the transcript....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=s6EvGgFPH3A) |
| Published | 2022/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Deep Goat says:

- Deep Goat remains anonymous and discreet about their location, hinting at involvement in a sensitive topic.
- Deep Goat is asked to create a video discussing the actions of different sides, particularly the Russians in relation to the United States and Ukraine.
- The focus is on the importance of understanding intent in intelligence work, rather than just knowing the plans or developments.
- Russian intelligence is suggested to be interested in knowing the intentions of the United States and Ukraine, especially post-high-level phone calls.
- Possibilities for Russian intelligence to gather information include intercepting signals, having a human source, or manufacturing situations to obtain sensitive transcripts.
- Deep Goat subtly criticizes the idea of public release of classified transcripts due to the risk it poses in revealing valuable intent information to foreign intelligence services.
- The transcript ends with Deep Goat leaving the audience with a thought on the consequences of releasing detailed intent information.

### Quotes

- "Intent is what is important. That's what matters in intelligence work."
- "A detailed intent of two world leaders who were on the same side, valuable to the opposition intelligence service."
- "It's just a thought. Y'all have a good day."

### Oneliner

Deep Goat discreetly addresses the significance of understanding intent in intelligence work and subtly criticizes the risk of publicizing classified transcripts.

### Audience

Intellectuals, analysts, policymakers

### On-the-ground actions from transcript

- Analyze intent and potential actions of various sides (implied)
- Maintain discretion and confidentiality in sensitive matters (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the importance of intent in intelligence work and the risks associated with publicizing classified information.

### Tags

#Intelligence #Intent #Russia #UnitedStates #Ukraine


## Transcript
Well, howdy there internet people. It's Deep Goat again.
So, can't tell you where I'm at today.
You know, normally I have an interesting story, but can't really talk about it right now
because sometimes there are things you just don't talk about.
But, somebody asked me to make a video
explaining what the various sides might be doing now.
And by various sides, I mean in a place that I'm certainly not at right now
pretending to be a journalist so I can blend in.
So, I guess we should just start with the Russians
and figure out what they were doing.
You know, we've talked about it before and I know other people have mentioned it.
The key thing in intelligence work is intent.
It's figuring out what the other side plans to do.
That's really what it's about.
The plans to the new jet fighter, they're not nearly as important
as knowing what the opposition plans to do with that new jet fighter.
That's what really matters. Intent.
So, if you're the Russians right now, you want to know what the United States and Ukraine intend to do.
You want to know what they're planning, right?
Now, given the fact that there were just high-level phone calls
between the United States and Ukraine,
I mean, the best thing would be for Russian intelligence to try to get a signal intercept,
but that's pretty unlikely.
World leader phone calls are pretty well secured.
So, the next best thing would be to have a human source in one of those offices.
Now, if you couldn't do that, you'd have to find some other way of figuring out intent.
So, in this case, it would be really difficult to ascertain
because the public statements and the private statements from world leaders typically differ.
I mean, the only way you'd really be able to get access to it,
if you were the Russians, would be to, I don't know, manufacture some outrage of the day
and, I don't know, create a thing.
Maybe they disagreed on intelligence or something like that
and play up that argument and then get a bunch of people
who don't understand how any of this works
to try to advocate to get the transcript released
so everybody can look and see what the United States and the Ukrainian president said to each other,
including the Russians.
I mean, the good news about that is that certainly nobody in the United States
would be so ignorant as to fall for that, right?
I mean, that wouldn't happen.
I mean, it's not like these people have never seen a spy movie in their entire life.
Certainly they might understand that high-level talks between leaders
just before a possible conflict would be classified
and be incredibly valuable to a foreign intelligence service.
So there's no way that they could somehow get released the transcript
to start trending on Twitter.
I mean, that wouldn't happen.
Nobody's that bad at this.
Nobody can be tricked that easily, right?
Right?
Intent is what is important.
That's what matters in intelligence work.
A conversation that detailed the intent of two world leaders
who were on the same side would be incredibly valuable to the opposition intelligence service.
That might be something they would go out of their way to try to get
because it might contain intelligence estimates.
It might contain their view of what might happen,
which means the foreign intelligence service, the opposition intelligence service,
if their plans match that, well they could change it.
So it would be a really bad idea to release a transcript like that.
Almost as bad as asking for one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}