---
title: Let's talk about the Biden-Zelensky call and context on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QybEHXTLLtI) |
| Published | 2022/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Biden-Zelensky phone call and the differing intelligence estimates between heads of state.
- Criticizes the right-wing outrage for drawing false equivalencies with Trump's Ukraine call.
- Describes Zelensky's leadership in handling the situation with Russia poised on Ukraine's border.
- Points out the damaging effects of discussing potential war on Ukraine's economy and resistance efforts.
- Talks about the posturing between Russia, NATO, and Ukraine, and the importance of posture in geopolitical contests.
- Raises concerns about US intelligence missing the bigger picture and the potential for Ukraine to become a buffer zone.
- Suggests that Zelensky's willingness for bilateral talks with Russia may indicate a pragmatic approach to protect Ukraine.
- Stresses the need for NATO commitment to stop Russian advances and criticizes sensationalized media coverage for its real-world implications.

### Quotes

- "Keep calm and carry on."
- "Zelensky has displayed more leadership than all of the other world leaders combined."
- "It has been my contention for quite some time that Putin would like to take Ukraine without a shot."
- "A lot of people's quest for ratings may quite literally shift the balance of power in Europe."
- "No, the transcripts do not need to be released."

### Oneliner

Beau explains the Biden-Zelensky call, criticizes false equivalencies, praises Zelensky's leadership, and warns about the implications of sensationalized media.

### Audience

Foreign Policy Analysts

### On-the-ground actions from transcript

- Monitor and analyze geopolitical developments for accurate understanding and informed action (suggested).
- Support efforts for peaceful resolutions and diplomatic negotiations (implied).
- Advocate for responsible media coverage and avoid sensationalism in reporting (implied).

### Whats missing in summary

Deeper insights on the nuances of the geopolitical situation and the importance of pragmatic leadership in times of crisis.

### Tags

#Biden-Zelensky #Geopolitics #Leadership #Sensationalism #NATO #Russia #Ukraine


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk
about the Biden-Zalinsky phone call,
a phone call that has captured
so many people's imaginations and conversations.
So we're gonna go over it real quick.
I know that Deep Goat released a video about it,
but we're gonna provide a little bit more context.
Okay, so the general tone of this conversation
this conversation is two heads of state having different intelligence estimates
and going back and forth about it. That's what happened. The media has
described it as not going well, they've described it as frank and honest, a whole
bunch of different things. None of the descriptions, as different as they are,
are contradictory. When heads of state have differing opinions and differing
intelligence estimates, they tend to butt heads a little bit. I'm only aware
of one president who believed a foreign intelligence service over his own.
Now the right-wing outrage of the day machine has decided that this conversation is somehow
comparable to Trump's Ukraine conversation.
the one that had the whistleblower and the quid pro quo stuff and all of that.
They're trying to draw a false equivalency, therefore they are asking for a release of
the transcripts.
The only thing in this conversation that has been reported that wasn't already public is
the actual intelligence estimates.
They're portraying it as some kind of bombshell.
Oh, they disagreed on this, it's super surprising, be shocked.
They've been disagreeing over this in public.
This isn't news, actually.
This isn't really a surprise.
Zelensky has openly criticized NATO countries for pulling their top diplomats.
He has talked about how it's like, what did he describe it as, captains leaving a sinking
ship.
But then he goes on to say, but Ukraine is definitely not the Titanic because he is trying
to convey calm.
Now he has also gone on to say, hey, you know, it'd be great if NATO countries could stop
saying the war was going to start any day because it's damaging the economy.
Zelensky is definitely right about that.
That needs to stop.
He has openly said that it is a clear and imminent threat, but he has always tried to
convey calm.
What has happened is that the right-wing media machine has conditioned people so much and
gotten them so accustomed to being motivated by fear that the right-wing audience doesn't
what real leadership looks like anymore. The reality of the situation is that
Zelensky is the head of state of a country that has a hundred thousand
troops poised on its border. Its military has zero chance of stopping them, none. If
the Russians move in, they're gonna move in. All they can do is make it costly.
They can't actually stop it. Zelensky knows this. If he didn't believe it was an
imminent threat, if it was a clear threat, a constant threat, all words that he's
used in public, he probably wouldn't have said those things and he definitely
wouldn't be training the civilian populace to mount a resistance. There's
not actually debate over the severity of the threat. There is debate over whether
or not, it should be portrayed that way publicly.
Zelensky is out there saying, hey, be prepared, but don't panic.
Keep calm and carry on.
That's what he's doing.
In this entire fiasco that's gone on over there, Zelensky has displayed more leadership
than all of the other world leaders combined.
And it's been very consistent, and he has generally been correct when it comes to what
needs to be put out into the public sphere.
As far as them taking the economy by constantly talking about how war is going to happen,
he's right.
That shouldn't be discussed because it will disrupt the economy, and if the economy gets
disrupted, well, then Russian intelligence services and the Russian military are going
to find it a whole lot easier to recruit assets, to get local forces, stuff like that.
And if the economy is in shambles, it makes it that much harder to mount a resistance.
He is definitely correct there.
Now at the end of the day, what we have right now is the U.S. intelligence service saying,
We believe this threat is imminent, you know, you're going to be invaded, it's going to happen, and now they're saying,
well, it could happen next month.  U.S. intelligence services historically tend to overestimate threats
and under-respond.
Ukraine's intelligence service is... new, let's just leave it at that.
And because of their close geographic proximity to Russia, there is an incredibly high likelihood
that a lot of Ukrainian intelligence is compromised.
Now does that mean their estimate is wrong and that it's not that Putin doesn't plan
to invade?
I don't know, I don't think that it has anything to do with anything like that.
I think Putin is testing NATO, and if NATO is just going to roll over, he will move in.
If NATO looks like they're going to respond, well, maybe he doesn't.
Now there is posturing that is going on.
That's what this is about.
It's a near-peer geopolitical contest.
need to get used to it, I would point out that the video that was made nine months ago
on this topic, when there was the first build up, everything that was talked about in that
video has happened.
It's just the way this stuff works, even down to the right wing media, sensationalist
media causing issues.
yes, the only people that are really excited about that transcript being released would be Russian
intelligence. They would be super happy. Now, it is worth noting that Zelensky's intelligence has
confirmed that Russian special operations are already in the country working. I think somebody
said something about that and said that was incredibly likely and that that's why Biden
said minor incursion rather than just any troops at all because the troops were already
there.
Now all of this is just context and explaining that conversation.
Where are we actually at?
The same place we were nine months ago.
It's posturing at the moment.
Both sides have to make a mistake at the same time.
Russia does not want to go to war with NATO.
NATO does not want to go to war with Russia.
What happens to Ukraine depends on the posture of both of those countries.
Now, while US intelligence is just so transfixed on this buildup, I think they're missing
something even more important.
I think Zelensky has realized that his nation is going to be a buffer zone for somebody.
He has indicated his willingness to engage in bilateral conversations with Russia, meaning
just them.
If he does understand that his nation is going to be a buffer zone, that may be a sign that
But he's like, okay, well, let's find out who's going to give us the best deal.
That is probably something you as intelligent should be looking at, just throwing that out
there because it has been my contention for quite some time that Putin would like to take
Ukraine without a shot.
And if given the previous administration and its level of corruption is how it might be
seen from Ukraine, the Trump administration and their level of corruption through Ukrainian
eyes, and Biden's less than tough stance when it comes to committing to protect Ukraine,
If Putin walks in and says, you know what, we will make sure that your country is safe.
That might go a long way.
It might go a lot further than US intelligence seems to be suggesting.
I don't know that, but that statement about bilateral talks coming out during a period
when NATO is less than unified, that seems pretty telling to me.
Ukraine has to rely on NATO for defense.
They cannot defend themselves.
They do not have the strength to stop that advance.
NATO has been in the position of having to talk about how there's a possible invasion
to try to manufacture consent to send in special operations and what they're, I guess, now
calling lethal aid.
But the assistance that NATO has granted, it is not enough.
That also will not stop Russia.
It will take commitment of NATO forces to stop the Russian advance.
NATO doesn't want that, and Russia doesn't actually want to fight a NATO response.
This is why posturing is so important, and this is why, as discussed nine months ago,
that type of sensationalized coverage is really, really bad.
It has real-world implications, and they're not good ones.
A lot of people's quest for ratings may quite literally shift the balance of power in Europe.
Given Zelensky's pragmatic approach throughout all of this and his leadership and his primary
concern really does seem to be the welfare of the Ukrainian people, I mean, he's a politician,
you know what I'm saying, definitely has faults, but in this case and in this instance,
his concern has been about his country and keeping it safe.
He wants to become part of the Western European Bloc.
That's his preference.
But I don't know how much his preference is going to matter if NATO seems incapable
of keeping his country safe, especially if Putin extends a hand right now.
So there's an overview of the whole thing.
No, the transcripts do not need to be released.
There's nothing in it that's surprising.
It's only surprising to people who don't actually follow foreign policy and only look
at the headlines and only look at sensationalist commentators.
None of it is surprising.
The discrepancy and the debate between Biden and Zelensky, this isn't new.
It's not just in this conversation.
manufactured attempt by Republicans to draw a false equivalency. That's all it
is. And I honestly, any politician who tweeted from their verified account that
that should happen, I honestly think they should lose their security clearance. I
don't believe that they should be allowed to see anything even remotely
classified because that shows that they have no clue how to handle it. Anyway,
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}