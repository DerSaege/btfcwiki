---
title: Let's talk about transplants, trust, and vaccines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SfWkrqMGCD8) |
| Published | 2022/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Renewed coverage of organ transplant vaccination requirement due to slow news week.
- Vaccinations required post-transplant to increase success chances.
- Immunosuppression post-transplant necessitates vaccines for natural immunity replacement.
- Organ transplant scarcity leads to prioritizing recipients with higher success chances.
- Trust in doctors for transplant surgery extends to post-transplant care and vaccination.
- Vaccination post-transplant is not punishment but a standard procedure for successful outcomes.
- Trust in medical professionals for surgery includes trust in vaccination recommendations.
- Failure to vaccinate post-transplant could result in wasted surgery and missed opportunities for better-prepared recipients.
- Vaccination post-transplant ensures the effectiveness of immunosuppressive drugs and the transplant's success.
- Trusting doctors for complex medical procedures should extend to following vaccination recommendations.

### Quotes

- "Trust them when it comes to the shot."
- "It's not punishment for not being vaccinated."
- "After the transplant, you have to get the vaccine."
- "Trust them with the aftercare as well."
- "It's not news. It's been like this a long, long time."

### Oneliner

Renewed coverage of organ transplant vaccination requirements post-surgery is about trust in medical professionals and ensuring successful outcomes through post-transplant vaccinations.

### Audience

Medical patients

### On-the-ground actions from transcript

- Trust medical professionals' expertise in post-transplant care (implied).
- Ensure compliance with vaccination recommendations post-transplant surgery (implied).

### Whats missing in summary

The importance of following vaccination recommendations post-transplant to ensure the success of the surgery and prevent wasted opportunities for other recipients. 

### Tags

#OrganTransplant #Vaccination #TrustInDoctors #PostSurgeryCare #MedicalAdvice


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about transplants,
and vaccines, and trust.
We're going to do this because there is renewed coverage
of the fact that if you want an organ transplant,
you need to be vaccinated.
I say renewed coverage because this isn't actually news.
This isn't a new development.
It has been like this for months and months.
This isn't news.
It's just being covered again because it's
kind of been a slow news week, to be honest.
That's really what it's about.
And people are wanting to reignite this debate.
So let's run through it.
Why?
Why do they want people to have vaccinations?
And it's not just the COVID vaccination.
It's a bunch of vaccinations because after a transplant,
you become immunosuppressed.
That's why.
Your natural immunity, which you are counting on right now,
it's not there after a transplant.
So they want to increase the probability of success
because what is being transplanted is in short supply.
There's giant wait lists.
So they want to give that organ to the person that has
the greatest chance of success.
And if you are not going to get the vaccine,
your chance of success is lower.
It's that simple.
This isn't new.
It's not news.
It's been like this a long, long time.
So I do want to point something else out, though.
When you're talking about this, when you're
talking about a heart transplant,
you trust this doc to open you up, cut out
your still beating heart, separate it from your system,
remove it from your body, replace it with a dead heart,
connect it to your system, and then Dr. Frankenstein that thing
back to life.
You trust them to do that.
Perhaps trust them with the aftercare as well.
And that's really what this is.
They're not saying, you know, we're not
going to give it to you because you didn't get the vaccine
because we want everybody to get the vaccine.
It's aftercare.
Because after the transplant, you
have to get the vaccine.
And that's what they're saying is aftercare.
Because after the transplant, I want
to say it's drugs that they give you that actually make you
immunosuppressed.
And they don't want to waste that heart.
And that's what it boils down to.
If they provide it to somebody who is immunosuppressed
and every possible precaution, it
could have been a wasted surgery.
And it could have gone to somebody
who took the precautions, who stood a better chance.
That's what it's about.
It's not punishment for not being vaccinated.
It's the way it's always been.
If you are willing to trust somebody to open you up
and replace parts inside of you, trust them
when it comes to the shot.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}