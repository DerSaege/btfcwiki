---
title: Let's talk about the USS Pueblo, Ukraine, and the Cold War....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yBbtV3tgSBk) |
| Published | 2022/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the inaccurate narrative in teaching history in the US and focuses on specific points like the Cuban Missile Crisis.
- Describes the Cold War as a period where major powers avoided direct war while jockeying for position.
- Mentions the concept of mutually assured destruction (MAD) and how great powers try to avoid conventional conflict due to massive destruction.
- Details the USS Pueblo incident in 1968, where North Korea seized a US Navy ship and held the crew captive for almost a year.
- Talks about Gary Powers, who was captured by the Soviets in a U-2 spy plane incident.
- Emphasizes that great powers like the US and Russia prefer to avoid direct conflicts.
- Compares the current situation with Ukraine to historical Cold War dynamics, pointing out the front line shift to Kiev.
- Clarifies that the media coverage may exaggerate troop movements near Ukraine for ratings.
- Suggests that backing Ukrainian partisans in conflict with Russia is more likely than a direct US-Russia conflict.
- Speculates on Putin's intentions, leaning towards the idea that he may be seeking a way to deescalate tensions in Ukraine.

### Quotes

- "Great powers don't like to go to war."
- "The sides trying to avoid war while gaining the upper hand."
- "It's unlikely, but it's not impossible."
- "Your dad and other old people are right."
- "It's going to happen a lot in Ukraine."

### Oneliner

Beau explains the Cold War dynamics, cautioning against dramatic narratives and discussing the USS Pueblo incident and Gary Powers, while comparing historical events to current tensions in Ukraine.

### Audience

History enthusiasts, policymakers, concerned citizens

### On-the-ground actions from transcript

- Brush up on history from 1945 to 1990 to understand the predominant theme of avoiding direct war while gaining advantages (suggested)

### Whats missing in summary

In-depth analysis of the potential geopolitical dynamics and historical comparisons are best understood by watching the full transcript.

### Tags

#History #ColdWar #USForeignPolicy #Geopolitics #Ukraine #Putin #USRussiaConflict


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about how we teach history
in the United States and why the way we focus on certain points
may leave people with a less than accurate picture
of history.
And we're going to talk about the USS Pueblo
and a guy named Gary Powers.
We're going to do this because somebody is incredibly concerned
and sent a message.
My boyfriend joined the army last year.
I'm worried he'll have to go to war in Ukraine.
My dad said not to worry and that it was Cold War II.
He had the same attitude you do and said
it was probably saber rattling.
Looking up the Cold War, I find Vietnam and the Bay of Pigs
and the Cuban Missile Crisis.
This doesn't seem minor, but all of the old people
that talk about this don't seem concerned.
Explain, please.
OK.
Great powers, near peers in today's language,
they don't like to go to war.
It's called the Cold War because it didn't get hot.
The list you have there, the only part of that
is that is really representative of the Cold War
is the Cuban Missile Crisis.
The way we teach history is we tend
to focus on the more dramatic parts, which can really
lead to a bad narrative.
The Cold War can be defined by two major powers, really three,
but we'll keep it simple, two major powers,
jockeying for position while attempting
to avoid going to war with each other directly.
Cuban Missile Crisis, the Cuban Missile Crisis fits that.
It was tension.
It didn't get hot.
The near peer contest that we're entering, it's the same thing.
The concept is mutually assured destruction, mad.
And when you say that, people immediately
think nukes because that's what's in your history book.
And that's true.
But even in a conventional conflict,
between great powers, the destruction is massive.
It's the destruction of the economy.
It's the destruction of the conventional forces.
It's the removal of a generation of people.
It's a big deal.
Both sides try to avoid it.
For it to go hot, both sides have
to make a mistake at the same time.
Something that is far more representative of the Cold War
is the USS Pueblo.
It was a ship in 1968, I think.
I could be wrong on that.
Fact check that.
It was winter, January of 1968.
It's a spy ship hanging out off the coast of North Korea,
just doing spy stuff.
The North Koreans had launched a clandestine commando raid
across the border south, and it didn't go well.
So I guess, I don't know, maybe to preserve reputation
on the international scene, they decided
to move on this spy ship.
Now, the American position is that it
was in international waters.
The North Koreans say it was in North Korean waters.
The reality, as near as I can tell,
is that it was in international waters,
but really close to North Korean waters.
The North Koreans took the ship.
They seized a US Navy ship in the US's eyes
in international waters.
They kept the crew.
They kept them prisoner for almost a year,
I want to say 11 months.
They didn't treat them well.
They forced them to take propaganda photos.
And the sailors convinced the North Koreans
that the Hawaiian symbol for good luck
was extending your middle finger to the camera, because Navy.
And this was a way of letting the people back home
know that the photos were coerced.
Held them for almost a year.
Didn't go to war over it.
That ship, the USS Pueblo, it's still in North Korea.
They give like guided tours on it.
The reason the US didn't want to go to war over an act of war,
seizing a ship like that, is because North Korea
was part of that block and had those allies.
And fighting them meant fighting great powers.
We don't want to do that.
Another example is Gary Powers.
He was flying a U-2 spy plane.
And he was definitely in Soviet airspace.
And they shot him down.
They captured him, interrogated him,
convicted him of espionage, and sent him to prison.
We didn't go to war over the Soviets actually shooting down
a US plane.
And they didn't go to war for us invading their airspace.
Great powers don't like to go to war.
In that case, we traded, I want to say, a KGB colonel for him.
But he was held for an extended period as well.
The great powers don't like to fight each other directly.
That's why it's called the Cold War.
Your dad and other old people are right.
We're running back into that.
If you want to know what's going to happen,
look back to the Cold War.
And don't just read about the dramatic parts,
because that's actually, those are the mistakes.
Look back to it and just compare Ukraine to Germany.
In this version of this contest, it's
not going to be our man in Berlin.
It's going to be our man in Kiev.
That's going to be the front line.
But because a lot of people aren't familiar with this,
and the media loves the ratings, they're
going to play up every troop buildup.
We're simply going to play up every troop buildup.
We're sending troops to Ukraine.
That's what the coverage says, but we're actually not.
We're sending troops to countries near Ukraine
to act as a deterrent, mutually assured destruction.
There may be some special operations that
are actually in Ukraine, but it's
going to be pretty limited.
That's going to be the front line in this.
I don't want to say that it's not possible, because it is.
If both sides make a mistake at the same time,
it is possible for a US versus Russia direct conflict
to occur, but that's very unlikely
because both sides are going to try to avoid that.
Now, as far as the US backing Ukrainian partisans
while they're fighting the Russians,
that's a whole lot more likely.
But at this point, I'm kind of leaning towards the idea
that Putin's looking for a way out,
because if he was going to advance,
he probably would have done it by now.
I think at this point, he is probably
looking for a way to kind of hit the safety valve,
release some of the tension, let things come back to normal,
and then he'll try to jockey for position again,
because this isn't going to end any time soon.
It's going to be a cycle.
In fact, I have a video from like nine months ago
where pretty much the same thing happened.
This will occur over and over again,
and it's going to happen a lot in Ukraine.
It will happen a lot in other countries as well.
But judging by the geopolitical realities,
Ukraine's probably going to be the front line,
and it is going to be very comparable to Germany
during the Cold War.
This is especially true if Putin does advance
and carve up the country more,
that the dynamic will be even more similar.
But at this point, I wouldn't panic every time
there's an article about it,
because there's going to be a lot of them.
And I know they withdrew from the embassy.
That's actually not that rare.
They're making a big deal out of it right now
because of what happened in Afghanistan.
The Biden administration wants to make sure
that everybody knows they're saying to get out.
But going to a smaller crew at the embassy
does not necessarily mean that war is imminent.
I think that Putin would like to take large segments of Ukraine
without firing a shot,
and it doesn't look like he'll be able to pull that off
this time.
So it seems as though he might want to deescalate a little bit.
But we don't know. He could make a mistake.
And if the Biden administration makes a mistake
at the same time, then a hot conflict is possible.
But both sides are trying to avoid that
because it's bad for everybody.
So your dad's right.
It's unlikely, but it's not impossible.
So it's probably time for everybody to brush up on,
you know, history from, say, 1945 to 1990,
and don't just look at the dramatic events.
Look at the smaller stuff that went along with it,
because that's the predominant theme,
is going to be the sides trying to avoid war
while gaining the upper hand,
at least direct war with each other.
Proxy conflicts are going to happen a lot.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}