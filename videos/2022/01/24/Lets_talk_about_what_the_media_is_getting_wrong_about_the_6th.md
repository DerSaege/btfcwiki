---
title: Let's talk about what the media is getting wrong about the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-WXj9j58D_k) |
| Published | 2022/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about journalism in the United States failing in its obligation to uncover the truth.
- Mentions the importance of journalists looking outside the window to see if it's raining or sunny, not just printing conflicting quotes.
- Points out the current documentation revealing an attempt to overturn a free and fair election and an apparent coup.
- Criticizes the lack of journalists asking political figures about their support for the former president's coup attempt.
- Raises concerns about the media's focus on trivial details rather than pressing questions related to serious issues.
- Emphasizes the danger of misinformation thriving in an information vacuum.
- Calls for journalists to hold people accountable by asking tough questions and seeking the truth.
- Questions why journalists are waiting for government press releases instead of actively pursuing answers.
- Urges journalists to fulfill their obligation to pursue the truth and hold people accountable.
- Expresses the need for individuals to be transparent about their stance during critical events.

### Quotes

- "Your job as a journalist is to open the window and look outside."
- "Democracy doesn't die in the darkness."
- "It's an obligation of journalism to pursue this, the whole idea of the public trust and all of that."
- "These questions need to be asked."
- "People need to be put on record about where they stood then and where they stand now."

### Oneliner

Beau criticizes US journalism for failing its obligation to pursue truth, urging journalists to ask tough questions and hold people accountable.

### Audience

Journalists, News Outlets

### On-the-ground actions from transcript

- Challenge journalists to ask tough questions and pursue the truth (implied).
- Encourage news outlets to prioritize accountability over ratings and circulation (implied).
- Advocate for transparency from individuals regarding their stance on critical events (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the shortcomings in US journalism and the urgent need for journalists to fulfill their obligation to seek the truth and hold individuals accountable. Watching the full transcript will offer a comprehensive understanding of Beau's insightful perspectives on the current state of journalism in the country.

### Tags

#Journalism #US #Accountability #Truth #Democracy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about journalism in the United States and
how it is failing in one of its biggest obligations.
One of my favorite concepts when it comes to being a journalist is the idea that
if one person tells you it's raining and one person tells you it's sunny,
your job as a journalist is not to print both quotes.
Your job as a journalist is to open the window and look outside.
That's what you're supposed to do.
If the news media in the United States were to do that today,
they would find that it is raining documentation detailing an attempt
to overturn a free and fair election in the United States.
They would find documentation detailing a pretty concerted effort in
an apparent attempted coup.
That's what they would find.
But questions related to that, that's not really what's driving the news,
not the overall theme, just each little piece of information,
because we have a 24-hour news cycle.
I'm not hearing those in political office be asked, did you then or
do you now support the former president's apparent coup attempt?
That seems like a question that should be asked of a whole lot of people.
You have the fake electors.
You have the draft executive orders.
You have the text messages.
You have the public statements from a whole lot of high profile people
that are very close in time to when a lot of stuff was happening behind the scenes.
This seems like something journalists should be asking.
They should be kind of pursuing this line of questioning, because they know the what,
they know the why, they know the where, they know the when.
Now you gotta figure out the who.
But rather than that, we have the New York Times running articles
with the headline of, for many, Jan 6th was just the beginning.
Apparently completely unaware of the danger of that.
It details how many who showed up on the 6th but didn't enter the building and
didn't get arrested, that for them, well, this is a badge of honor.
This is giving them social credibility.
This is propelling them forward and giving them status, which means for
other people in that movement, they will see similar actions as a way to socially advance.
That seems like a bad idea.
We're not seeing those questions.
You have the media railing constantly about disinformation and misinformation.
Disinformation and misinformation, well, it flourishes in an information vacuum.
If you're not asking the questions, if you're not putting people on the spot and
asking those questions, that type of misinformation will flourish.
If this was a story about a takeover at a Fortune 500 company,
every board member would have been questioned about this by now.
They would have been asked where they stood then and where they stand now.
It seems like these questions should be asked.
Democracy doesn't die in the darkness,
despite a tagline from a pretty popular paper in the United States.
Democracy dies when somebody, quote,
attempts to drive a stake into the heart of the Federal Republic,
and that isn't driving questions for the next month.
That's when democracy dies.
When journalists are so just overcome with the desire for
ratings and circulation that they attempt to
both sides issues that do not have two sides.
These questions should be asked.
I understand that many journalists are waiting for the January 6th committee.
I get it.
Is there any other time when you would do nothing and
not ask these questions while you were waiting for a government press release?
Are you journalists or do you get your copy from press releases?
What's your job?
Where are you at?
You have access.
You can ask these questions, but you're wanting to both sides it.
The documentation is there.
It's an obligation of journalism to pursue this,
the whole idea of the public trust and all of that.
These questions need to be asked.
People need to be put on record about where they stood then and
where they stand now.
Because we're going to find out that many of the people who are remaining silent and
not commenting, they're doing so because they're waiting to find out if
their names are in the documents.
Wouldn't it be good to have statements from them beforehand,
directly related to what's happening?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}