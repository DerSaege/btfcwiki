---
title: Let's talk about Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_V-3Ta-ADV8) |
| Published | 2022/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NATO's expansion towards Russia's borders is causing tension due to Russia's national security concerns and desire to reassert power.
- The United States has intelligence suggesting Russia planned a pretext for war by hitting its own people in Ukraine.
- The United States possibly manufactured a psychological campaign to sow distrust between the Russian military and their proxies.
- Russia may send troops to Cuba and Venezuela, but the U.S. should not get involved as it doesn't pose a threat.
- Ukraine is shifting its doctrine to resist Russia by training civilians in first aid and resistance tactics.
- Ukraine's strategy mirrors Iran and North Korea's technique of putting up resistance and disappearing.
- Ukraine's doctrine change could potentially deter conflict if they can quickly field thousands of partisans.
- Conflict starting immediately in Ukraine seems unlikely, but it could escalate in the future due to various factors.
- Both the U.S. and Russia misreading each other simultaneously could lead to a conflict involving them.

### Quotes

- "Governments manufacture pretexts to go to war. They do that. The United States does it. Russia does it too."
- "Ukraine's doctrine shift is a brilliant move. Whoever came up with this idea needs to be promoted."
- "Ukraine may be the front line for the near-peer contest between Russia and the United States."

### Oneliner

NATO expansion, intelligence pretexts, and Ukrainian resistance tactics heighten tensions between Russia and the United States.

### Audience

Policy analysts, diplomats, activists

### On-the-ground actions from transcript

- Support Ukraine's civilian training initiatives to resist potential conflict. (suggested)
- Advocate for diplomatic solutions and non-interference in sovereign affairs. (implied)
- Stay informed about developments in Ukraine to raise awareness. (implied)

### Whats missing in summary

Analysis of potential long-term impacts of the Ukraine-Russia tensions.

### Tags

#Ukraine #Russia #NATO #ForeignPolicy #Conflict #Tensions #Intelligence #Diplomacy #Resistance #Sovereignty


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about Ukraine.
We're going to talk about the recent developments. We're going to talk about a doctrinal shift.
We're going to talk about the overall foreign policy scene and developments, and we're going
to give a brief explanation of why this is occurring. Okay, so to start off with,
we're just going to kind of do an overview of what's going on.
Super basic. There's a lot more details to it than this.
Basically, NATO is expanding closer and closer to Russia's borders. Russia, Putin, does not like this.
It represents what they see as a national security concern. Putin, there is the idea that he would
like to reassert Russia as a much larger power and kind of bring us back in the USSR, re-establish
that kind of presence. Ukraine would be important to that. So those are the developments that are
kind of leading to the tension. Aside from that, you have the rekindling of the near-peer contest
between the United States and Russia. Okay, so that's what's led to the tension. Now, there are
a whole bunch of pretexts that go into this, but that's the big picture from the top. Okay,
now on to the developments. The first is the United States is saying that it has intelligence
suggesting that Russia planned to kind of hit its own people secretly, hit their proxy forces in
Ukraine to give them a pretext to go to war. Is this possible? I mean, I don't think I need to
explain to this audience or Americans in general that yes, governments manufacture pretexts to go
to war. They do that. The United States does it. Russia does it too. So that is possible.
There's also something else from my eldest son, and when I say that, you're probably thinking of
my oldest child. That's not what I'm talking about. I'm talking about Project Eldest Son.
During Vietnam, the situation was kind of similar. You had proxy forces,
the Viet Cong, and they were being supplied by a major power, in this case China.
In order to sow seeds of distrust, in order to complicate that relationship, the United States
waged a lot of psychological campaigns. A small piece of one of those campaigns
was to take captured ammunition that was manufactured in China and, well, sabotage it.
Make it to where when you fired your rifle or you dropped that mortar, that instead of it
shooting and doing what it was supposed to, it would just explode. They took this doctored ammo
and they would insert one round into, you know, 5,000 rounds of ammo, and they would basically
find a way to give it back to the Viet Cong. The purpose was to, A, make people second guess
pulling the trigger or dropping a mortar, and B, make the Viet Cong distrustful of their Chinese allies.
It is also very possible that the United States has manufactured this
to sow seeds of distrust between the conventional Russian military and their proxies. That's
very possible. I would imagine those who would be hit under this plan are not going to be happy
with the Russian government. Which one is it? Don't know. And to be honest, we probably will
never know, at least not for the next, you know, 30 years. So could the intelligence be real? Yes.
Could it also be completely fabricated as part of a psychological campaign? Also yes.
We're not going to know which. Now on the foreign policy scene, another development is that
apparently the Russians have indicated that they may send troops to Cuba and Venezuela.
The question that has come across a few times is what should the United States do about that?
Nothing. Nothing. And for two reasons. The first is that that would be an arrangement between two
sovereign countries that has nothing to do with us and we shouldn't get involved. That would be
one reason. More to a reason that matters to American foreign policy, which is not about
morality, is not about right and wrong, it's about power, is the fact that it doesn't matter.
In some ways it's actually good. So if Russia was to send troops to these two areas, does it
actually present a threat to the United States? No. No. They're not going to send enough troops
to actually pose a threat. But what they will do is either send normal infantry people or they will
send their equivalent to U.S. Special Forces, their trainers, so they can train them. And
they will send their equivalent to U.S. Special Forces, their trainers, so they can train the
locals. If they send normal infantry, that's just reducing infantry troops in theaters that matter
to put them somewhere that they don't matter. Or they send the trainers, trainers that would be
the most important thing right now, and they put them somewhere that is kind of irrelevant in the
overall foreign policy scheme. So what should the U.S. do? Nothing. If the U.S. feels like it has
to respond, my move would be to encourage it. Be like, you know what, that's a great idea. We're
very proud of the Russian government for joining the international community and wanting to
help these countries and help them out. If they need any assistance, we are there. We'll help.
You want food, you want medicine, just let us know. We know you're not going to be able to do
this on your own. And please, if you're going to promise them help, make sure that you follow
through with it. Don't just let this be some foreign policy tool. Send them the help they
need. Make sure you send enough people. That's what I would do. What will the State Department
do? I have no idea. Okay, so that's the posturing that's going on right now.
What actually matters is something that isn't being talked about. Ukraine has indicated a
shift in doctrine. Rather than attempting to go toe-to-toe with Russia, which they would lose,
they plan to put up a fight initially and then disappear into the hills and mount a resistance
effort. And they are training civilians to do this. They're sending them to crash courses
where basically they're learning first aid and they are learning how to make things go boom
and how to hit people and run away. That is a brilliant move. Whoever came up with this idea
in the Ukrainian government needs to be promoted. That's smart. This doctrine of putting up stiff
resistance and then disappearing, there are two other countries that use it, Iran and North Korea.
Two countries on the U.S. hit list that we've never invaded, and this is why.
Nobody wants to get involved in a conflict like that. Putin, because he was around during the end
of the Soviet Union, is going to remember this kind of conflict from Afghanistan.
Hopefully, this will be a deterrent. It may be a larger deterrent than anything the United States
can do. It depends on how quickly the Ukrainians can get this up and running. If they can field
thousands of partisans quickly like that, that might be enough to make Putin change his plans.
Now, the big question. Do I think that this is going to start a conflict right now?
Probably not. It's possible. I don't want to say no, but it seems unlikely at this moment. However,
I would definitely get used to having Ukraine in the headlines. So, I'm going to leave it at that.
I'm going to leave it at that. I'm going to leave it at having Ukraine in the headlines. That looks
like it's going to be the front line for the near-peer contest between Russia and the United
States. Could conflict flare there later? Yes. I don't believe that either side really wants a war
between the United States and Russia. I don't believe that Russia wants to create a buffer
between their traditional territory and the West. And Ukraine would be that buffer.
Some major shift, like Ukrainian doctrine shift, that might cause them to second guess.
But we'll have to wait and see. I don't foresee conflict happening soon, but it's possible.
And the further you get out from right now, there are more ways for it to occur.
There are more variables at play. And what this is going to take for it to turn into a conflict,
especially a conflict involving the United States, is for both the United States and Russia
to misread the other side at the same time. So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}