---
title: Let's talk about baseless claims about a guy named Ray....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xb1dAEbrr28) |
| Published | 2022/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a baseless claim surrounding a person named Ray that has gained traction among conservatives.
- Explains the lack of evidence supporting the claim that Ray was working for the government and responsible for the events on January 6th.
- Suggests a way to address conservatives asking about Ray Epps by questioning their stance on his arrest and actions.
- Compares the situation with Ray Epps to holding political leaders accountable for encouraging but not physically participating in events.
- Encourages challenging the belief that a single person without political influence could incite a crowd, raising questions about the power of influential figures' words.
- Notes the absence of concrete evidence to support the claim about Ray Epps, making it challenging to debunk.
- Raises the point that if Ray Epps warrants arrest, then many others in the Republican Party could also be implicated.
- Advocates for probing deeper into the logic behind calling for Ray Epps' arrest and drawing parallels with other political figures' actions.

### Quotes

- "You think he should be arrested?"
- "What did he do that warrants arrest?"
- "They are making the case to charge Trump."
- "Isn't that what Trump did?"
- "If Epps warrants arrest, there's a whole bunch of people in the Republican Party that also warrant arrest."

### Oneliner

Addressing a baseless claim about Ray Epps and questioning the accountability of political leaders for incitement without physical participation on January 6th.

### Audience

Those addressing baseless claims.

### On-the-ground actions from transcript

- Ask individuals questioning baseless claims to justify their beliefs (suggested).
- Challenge misconceptions by drawing parallels with other political figures' actions (exemplified).

### Whats missing in summary

The full transcript provides detailed guidance on addressing baseless claims effectively and engaging in critical discourse with individuals questioning such claims.

### Tags

#BaselessClaims #Accountability #CriticalThinking #PoliticalLeaders #January6th


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a baseless claim.
And we're gonna talk about a person
who there's been a lot of coverage over,
and it's kind of become a talking point
among conservatives.
And we're gonna talk about how to deal with that
when it comes up.
Because I've had this conversation
a few times myself already,
and I got a message asking about it.
This baseless claim is a little bit different
because normally when there's something like this,
I can track back and find something
that can be misinterpreted to support
the theory that's being presented.
In this case, I can find nothing.
There are two main pieces of evidence
are that he didn't get arrested
and that he was removed from a list. The FBI was like, hey, we want to talk to this person,
and he got removed from that list after they talked to him. I mean, that kind of makes sense.
And he has not been charged as of yet. The counter-evidence to this is that he never
entered the building on the 6th. As far as I know, and as far as anything anybody's told me,
He's never entered the building.
The other part is that he's already talked to the January 6th committee and said that
he wasn't working for the government.
Now if you're not familiar with the claim, there's a guy named Ray.
And for whatever reason, a lot of right-wingers believe that he was working for the government
and that somehow he's responsible for what occurred on the 6th.
This is the question.
tips on how to get a conservative, in quotation marks, to stop asking, what about Ray Epps?
Asking for everyone with a Fox News junkie in their life.
Okay, so you could try to present him with the information I just gave you, right?
And try to be like, what evidence do you have to suggest that he was working for the government?
Or that he was capable of inciting a thousand people to do what they did, right?
You could go that route, but it's probably not going to be effective and it will definitely
not be as fun.
What I have done when this comes up is just start by asking, oh yeah, I heard about that
guy.
You think he should be arrested?
And so far, everybody has said yes.
Then ask him what he did.
Ask him what he did that warrants arrest.
Because basically what we've seen is that he stood outside and said, hey, we should
go in the building.
And stuff like that.
That he encouraged it but never entered himself.
When they make this claim, when they say that they should arrest this guy, or they claim
that one dude could somehow incite what occurred, they are making the case to
charge Trump. Somebody who encouraged it but never entered. I would go that route.
I would try to get them to understand that what they are suggesting, if they
want this guy charged, they are making the case to charge a lot of their
political leaders, people who encouraged it but didn't enter, who didn't
participate but were were fond of the idea. Now as far as him not being
charged that there could be a whole bunch of reasons for that but at the
moment as far as I've been able to find there is literally no evidence to
support this claim? None. There is nothing to suggest he was working for the government.
So that's the route I would go. Do you think he should be arrested? What did he do that warrants
arrest? Isn't that what Trump did? Isn't that what, pick a politician, encouraged it? I would
question whether or not they think a single person with no political clout
could incite what occurred, and if they believe that, what do they think somebody
with a lot of political power could do? Just with their words. Are they
responsible for what occurred that day? That's the route I would go. There's the
evidence route, but it's really hard in this case because there isn't any real
evidence. It's just a belief. Normally when I'm dealing with these kind of
theories, these baseless claims, I go to the evidence and I just rip it apart. But
in this case there's really not any to support it. I mean there's not even
anything that can really be misinterpreted as evidence to support it
if you're being honest. So if you have a Fox News junkie in your life, that's the
would go about it because make no mistake about it if Epps, if he warrants
arrest there's a whole bunch of people in the Republican Party that also warrant
rest. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}