---
title: Let's talk about Garland's speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mVms5KEF9_I) |
| Published | 2022/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Cautious optimism about the investigation into the events at the Capitol, due to historical protection of the presidency.
- Hope is present in the investigation process starting with lower-level individuals and moving up.
- Real optimism hinges on indictments of significant individuals linked directly to the Capitol events.
- Emphasizes the importance of real accountability to safeguard democracy.
- Warns against blind faith in the Department of Justice (DOJ) to solve all issues.
- Acknowledges that DOJ's actions alone won't address the broader societal acceptance of harmful actions.
- Stresses the necessity of seeing tangible accountability actions before becoming optimistic.
- Raises concerns about the long-standing tradition of shielding the presidency from scandal.
- Expresses the value of both the presidency and democracy, underscoring that any erosion of democratic values exacerbates existing problems.
- Despite recognizing the speech as good, Beau remains skeptical until concrete actions demonstrate accountability at higher levels.

### Quotes

- "Until then, I'm very cautious in getting hopeful."
- "If they were involved, they were going to go after them."
- "That's accountability on that level. Sure, it would help, but that doesn't actually solve the problem."
- "Nothing will until I see action at those levels."
- "I just hope they see the institution of democracy as worth protecting as well."

### Oneliner

Cautious optimism about accountability in the investigation into Capitol events, stressing the need for tangible actions over speeches to safeguard democracy.

### Audience

Citizens, activists, voters

### On-the-ground actions from transcript

- Monitor and demand accountability actions at all levels (implied)
- Advocate for transparency and thorough investigations into events of January 6th (implied)
- Participate in civic engagement to uphold democratic values and hold leaders accountable (implied)

### Whats missing in summary

Beau's deep skepticism and cautious hopefulness can best be understood by watching the full transcript.

### Tags

#Accountability #Democracy #Presidency #DOJ #CapitolEvents


## Transcript
Well howdy there internet people, it's Beau again.
So we're going to talk about Garland's speech today.
You know, since this whole thing started and the investigation began,
I have been very cautious in being optimistic about it
because there is a long-running tradition in this country
of protecting the institution of the presidency, of insulating it.
There is a cause for hope in the way the investigation is being conducted so far,
starting with the little fish, medium-sized fish,
then theoretically they would move to the bigger fish
and then the catch of the day to complete it.
His speech indicated that that is what they were doing.
That they would hold people accountable at any level,
whether or not they were at the Capitol or not.
If they were involved, they were going to go after them.
And that's good, but it is a speech.
I won't become truly optimistic
until we see big fish catch indictments
that are actually related to the events of that day.
Not just for obstructing the process of the investigation or something like that,
but those that are actually related to those events.
Or some just massive conspiracy charge being laid out
that has room to move up.
Those would be my signs that the institution of democracy
has won out over the institution of the presidency.
Until then, I'm very cautious in getting hopeful
when it comes to seeing real accountability,
which is rather pessimistic and very cynical of me.
But it's important in the sense that if there isn't real accountability,
it makes pulling back from the edge that much harder.
So I don't think it's a good idea for people to just assume
that DOJ is going to take care of everything.
Because even if ideal circumstances, they find the evidence
and the facts lead them to the conclusion that many people believe,
and there is real accountability for those in the administration,
there's still a lot of work to be done.
This country is close to bad things.
And DOJ isn't the cure-all.
That's accountability on that level.
Sure, it would help, but that doesn't actually solve the problem
of that many Americans being okay with the actions of that day.
Because those actions take us down a road that's really bad.
So I don't want to get optimistic.
I won't become optimistic until I actually see it happening.
Now, if his speech is 100% on the level and that's the intent, good.
And to be clear, his speech lines up with how this should work,
with how it should happen.
What he described as far as following the evidence in the way he said it.
And the actions that have been taken thus far do line up
with an attempt to get real accountability at the highest levels.
But we don't know how far up they're actually going to go.
And I'm not going to set myself up for the assumption that
they're going to take care of it.
Because the tradition of insulating the institution of the presidency
from real scandal in this country, it's very long-running.
It is very long-running.
I would hope that people understand, people who are making the decisions,
understand that the institution of the presidency
is kind of worthless without the institution of democracy
in this country as we have it.
And people can say and list out all of the problems
that we have with democracy in this country,
with the way this republic functions.
And yeah, that's true.
But understand, any chipping away of that makes those problems worse, not better.
So it was a good speech.
If it had been a politician making it, it would have been a good speech.
That's something that would swing polls a point or two.
Does it provide me any comfort?
No.
But honestly, nothing will.
Nothing will until I see action at those levels.
Because until then, we have that tradition that's out there,
that desire to shield the Oval Office, to protect the image.
And until I see that overcome, I won't get my hopes up.
I would love if I was proven to be too cynical here.
But it's not something I'll hope for.
It's not something I'm going to base any plans or strategy around.
Because history says the institution of the presidency is worth protecting.
I just hope they see the institution of democracy as worth protecting as well.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}