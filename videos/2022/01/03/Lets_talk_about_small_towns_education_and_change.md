---
title: Let's talk about small towns, education, and change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3L5m6jiETkE) |
| Published | 2022/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A young woman from a small town has the chance for a full scholarship at a better school six hours away, with the potential for a master's degree if she maintains good grades and an extracurricular activity.
- Despite this amazing offer, her boyfriend of many years doesn't want her to go because he's afraid it will change her.
- Beau shares his own experience of leaving his small town for education and how it changed him and his perspective on life.
- He encourages the young woman to take the scholarship, acknowledging that the experience will indeed change her, but that's the whole point of growth.
- Beau predicts that the relationship with her boyfriend may not survive her leaving for college, but new opportunities and connections will arise.
- He advises her to seize the incredible chance for education and personal growth, especially since someone is willing to invest over a hundred thousand dollars in her potential.
- Beau subtly questions whether the boyfriend, if given a similar career advancement, wouldn't hesitate to leave for better prospects.
- He leaves the decision to the young woman, acknowledging that the comments section may have valuable insights on the situation.

### Quotes

- "The odds of your relationship with your boyfriend surviving you going off to college are slim to none."
- "You are so good at something that you love, somebody is willing to basically fork out a hundred grand for you to go to school so you can do it."
- "Of course take it. You are so good at something that you love, somebody is willing to basically fork out a hundred grand for you to go to school so you can do it."
- "You're probably gonna meet somebody. You know, small-town dating. Not always a choice. Sometimes a lack of options."
- "How long would it take you to pack? because we both know you would have gone."

### Oneliner

A young woman faces a life-changing scholarship offer while her boyfriend raises concerns about change, but Beau urges her to embrace growth and education.

### Audience

Young adults

### On-the-ground actions from transcript

- Pursue the educational opportunities available to you, even if they involve significant changes in your life (suggested).
- Embrace personal growth and new experiences for a chance at a better future (suggested).
- Seek advice and perspectives from others, especially if facing a difficult decision (implied).

### What's missing in summary

The full transcript provides detailed insights and personal anecdotes that add depth to Beau's advice on seizing educational opportunities and embracing change.


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today we're going to talk about small towns, education,
opportunities, change.
And even though I said this channel wasn't going to turn
into Dear Abby, I guess I'm giving some
dating advice, too.
I got a message from somebody.
It's very detailed and very long in the beginning, so I'm
going to kind of summarize. She has completed one year of college in a local school. She lives in
a very small town and she has been offered a free ride at a much better school. And by free ride I
don't mean just tuition. Dorm, books, everything. Totally free ride. It's about six hours away from
where she lives and all she has to do to keep that scholarship for the full three
years is keep her grades up and engage in an extracurricular activity she
enjoys. If she does that it's very possible that they will pay for an
additional two years so she can get a master's out of this. And then, here's the rub.
All of this is great, right? I'm not writing just to celebrate. My boyfriend
and I have been together since I was 16. He told me he didn't want me to go
because he's afraid it'll change me. I asked him to come with me. He said he
wanted to stay at his job at a resort. This led to a pretty big argument, and he
said I was throwing him away, and I'm left wondering if he's right. Will it
change me? Should I not go? Like A-I-T-A here. If you don't know what A-I-T-A is,
don't feel bad. I had to look it up too. Okay, so let's start with what's on the
table. Tuition, dorm, and books at that school for three years, it's like seventy
five grand. Easy. And that's just all you had to do to get that is keep your grades
up and do something you love. And it's a possibility that they'll pay for two
more years. So there's theoretically more than a hundred thousand dollars on the
table. And you're wondering if it'll change you because of what your
boyfriend said. I mean, yeah, it will. I mean, it wouldn't be much good if it
didn't, right?
Now, I don't know if you know this,
and this is why you sent me the message,
or if it's just a weird coincidence,
or you just know I'm from the general area.
If you leave your town and drive 15 miles south,
that's where I went to school.
That's where I went to high school.
Two stoplights, right?
That's where I went to school.
That's where I went to high school.
And I left for years.
And incidentally, while I was gone,
I lived where you're about to move to.
And then I came back.
And admittedly, it was longer than three years.
And I met some of my friends from high school
who didn't leave the area.
And the first night, we went to a restaurant out in Destin.
And they thought that was really weird.
And then the next night, we hung out at the same bar
that we hung out at when we were in high school,
because they didn't card us, and played pool
at the same pool table that we played at in high school.
And it was at that moment that they
chose as a group to inform me that I had changed a lot.
And I'm sure right now you're sitting there thinking,
all of your friends telling you that you
had changed in a negative way, that's
got to be really bad, right?
Honestly, when they said you've changed, all I could think
was you didn't after all that time.
Then I got older and realized there's nothing inherently wrong with that.
Some of the happiest people I know will die in the town they were born in, and that's
okay, but some people want something different.
I'm willing to bet you're one of those people.
If you weren't, you wouldn't be entertaining this.
You wouldn't have had the argument, and you wouldn't send me the message.
I mean, let's be honest, you knew what I was going to say before you sent this.
I mean, two of my big things are that travel is the greatest thing in the world and no
education is wasted.
And you sent me a message asking if you should travel for education, and of course you should.
You already knew that.
You just need somebody to say it, I guess.
But yeah, it's going to change you, guaranteed, no doubts, but it's supposed to.
That's the point.
The odds of your relationship with your boyfriend surviving, you going off to college are slim
to none, slim to none.
What is probably going to happen is sometime after you leave, you're going to meet some
guy who grew up in a small town who also wanted something different and you were
going to hit it off. And I say that as somebody who did exactly what you're
about to do and who is married to a woman who grew up in a small town in the
Midwest who wanted something different. You'll have a lot of the same interests
and be able to kind of bond over those things.
There are more options for dating where you are headed.
The student body of the school you're headed to is literally nine times the size of your
entire town.
are, you're probably gonna meet somebody. You know, small-town dating. Not always a
choice. Sometimes a lack of options. So yeah, you want my advice? Of course take
it. Of course take it. You are so good at something that you love, somebody is
willing to basically fork out a hundred grand for you to go to school so you can
do it. I don't know why you wouldn't take the opportunity. Now, as far as whether or
not you're the bad person here, because throwing him away or whatever, I'm not
going to get too far into that. I'm going to let the women in the comments
section respond to this general idea.
But I would ask if the situation was reversed,
and that resort he works at was going
to give him a big promotion and send him up to Virginia,
to that ski resort they own.
How long would it take you to pack?
because we both know you would have gone.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}