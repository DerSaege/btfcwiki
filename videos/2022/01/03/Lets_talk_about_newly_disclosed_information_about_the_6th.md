---
title: Let's talk about newly disclosed information about the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PecFLDrmya8) |
| Published | 2022/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Disclosing year-old news slowly to lessen surprise during hearings about a group taking over a building in D.C.
- During a coup attempt, Beau live tweeted and provided context and background information.
- Reports of specific helicopters being used by elite forces to retake the building.
- Questions about the legality of using active duty military against American citizens.
- Elite forces were seconded to the Department of Justice, not the National Guard.
- Military operators under the Attorney General's control operated under unknown contingency plans.
- The National Mission Force, including FBI's hostage rescue and render safe teams, was present.
- Military operators were ready to respond but not for crowd control.
- In emergencies, laws become flexible and gray.
- Elite military units wouldn't stand idly by during a coup attempt.

### Quotes

- "Laws become very flexible. They become very gray."
- "The most elite units of this country's military wouldn't stand idly by during a coup attempt."
- "The echo chambers that exist online can be dangerous."

### Oneliner

Disclosing year-old news slowly to lessen surprise during hearings about elite military forces involved in responding to a coup attempt.

### Audience

Activists, Advocates, Researchers

### On-the-ground actions from transcript

- Read the Newsweek article titled "Secret Commandos Will Shoot to Kill Authority Were at the Capitol" (suggested)
- Educate others on the flexibility of laws in emergencies (exemplified)

### Whats missing in summary

The full transcript provides a deep dive into the involvement of elite military forces during a coup attempt, shedding light on the flexibility of laws in emergencies and dispelling misconceptions about military support based on political affiliations.

### Tags

#CoupAttempt #EliteForces #MilitaryInvolvement #FlexibilityOfLaws #EchoChambers


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about some news that is almost a year old,
but it's just now being disclosed.
I have a feeling they're disclosing it now,
kind of letting this information out slowly now,
so it isn't as much of a surprise
when the information comes out during the hearings.
And the House's explanation of what occurred on that day.
You might remember on the 6th of January last year,
group of people who fashioned themselves operators
took over this building in D.C.
It was on the news, you know.
During the coup attempt, I live tweeted,
and I tried to explain what I could
and provide context and background information
and explain the possibilities of what might happen next.
During this, reports came in
about some pretty specific helicopters being used.
Helicopters that are really only used by one group of people.
So I talked about the likelihood of some of America's most elite forces
being used to retake that building.
At the time, I had a whole lot of questions that came in about it,
because it's illegal.
You're not supposed to use active duty military
against American citizens.
And I talked about how that law is flexible at times,
and how people could be transferred to the National Guard,
put on National Guard patches, retake the building,
take those patches off, disappear,
and say the National Guard did it.
That was described at the time
as the most terrifying sentence I'd ever written,
because it kind of cast doubt
on the way laws we believe are inflexible can be gotten around.
Now, the reality is I was wrong about that.
I was wrong about that.
They weren't seconded to the National Guard.
They were seconded to the Department of Justice.
The reporting coming out of Newsweek,
the presence of these extraordinary forces
under the control of the Attorney General,
and mostly operating under contingency plans
that Congress and the US Capitol Police were not privy to,
added an additional layer of highly armed responders.
The role that the military played in this highly classified operation
is still unknown, though FBI sources tell Newsweek
that military operators seconded to the FBI,
and those on alert as part of the National Mission Force were present.
The National Mission Force in and of itself, those are heavy hitters.
That's the FBI's hostage rescue team,
as well as their render safe people.
The render safe teams, those are people responsible
for dealing with the worst scenarios imaginable.
Chemical, biological, radiological, nuclear.
These are not people who are playful.
There are other elements to the National Mission Force as well,
but that's not in the reporting, so we're not going to go into that.
So this is loosely confirming that military operators were on scene,
were ready to respond.
Now there's probably questions about why didn't they?
These aren't people for crowd control.
These aren't people for what we were witnessing.
These are people who, when they clear a building,
the people are still inside the building when they're done.
They're just laying down.
They're not trying to get them to move.
Now again, this isn't just an I told you so video.
There's the important element here is for people to understand
that in emergencies like this, laws become very flexible.
They become very gray.
And the people who are in the most elite units that America has to offer,
they're not likely to stand by while somebody attempts a coup.
In the weeks after I tweeted about this, I had a lot of people say it was irresponsible
to suggest that active duty military would be involved because it's illegal,
because it's impossible.
And the real reason I'm making this video is because a whole bunch of people
suggested that they wouldn't get involved because they supported Trump.
It's not how it works.
The most elite units of this country's military would not stand idly by during a coup attempt.
A whole bunch of people who fashioned themselves operators
were very close to seeing operators in real life.
And had that option been exercised, it would have been the last thing they saw.
The echo chambers that exist online that create the idea that some actions
are more popular than they really are, or they reinforce a bias that leads you
to believe people who aren't on your side are, can be dangerous.
They can be bad for your health.
I don't know exactly what the line was to have released this particular group of people.
My guess would be if the inner doors were breached.
Had that happened, this would have been an entirely different scenario
because their entire mission would have been to secure the vice president,
Congress, and them, and nothing would have stood in their way.
This little part about the military being involved, it is literally the last paragraph
in an article on Newsweek, and the title is
Secret Commandos Will Shoot to Kill Authority Were at the Capitol.
People might want to read that and might want to really understand
that this isn't a video game.
And the ideas that you have from people who were involved with certain units
and are now civilians, who are profiting from a certain image being cast,
that's not how it works.
That's not what would have happened.
People in that building were a whole lot closer to a really bad outcome
than I think they're aware.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}