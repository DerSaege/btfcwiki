---
title: Let's talk about must-win elections for Dems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FIycjPOSRIA) |
| Published | 2022/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NPR compiled a list of candidates running for Secretary of State who spread misinformation or believed baseless claims about the 2020 election.
- These candidates hold the power to count votes and potentially alter election outcomes.
- Candidates who believed in misinformation may intervene due to a perceived moral duty.
- Those running for Secretary of State positions must have good judgment and understanding of election processes.
- The Democratic Party must prioritize winning these Secretary of State elections to secure fair elections.
- Historically, Secretary of State elections have not received much attention but are critical for election integrity.
- Beau urges viewers to read the article for a list of candidates and states they are running in.
- Residents in states with concerning candidates should get involved in electoral politics to make a difference.
- Beau stresses the long-term importance of these elections in maintaining election integrity.

### Quotes

- "These are must-win elections."
- "If you live in one of those states, and you have ever desired to become active in electoral politics, now is your time."

### Oneliner

NPR's list reveals Secretary of State candidates spreading misinformation about elections, urging Democratic Party action in must-win elections.

### Audience

Democratic Party members

### On-the-ground actions from transcript

- Get involved in electoral politics in your state (implied)
- Prioritize winning Secretary of State elections for fair and secure elections (implied)

### Whats missing in summary

The urgency and importance of active participation in upcoming elections can best be grasped by watching the full transcript. 

### Tags

#SecretaryOfState #ElectionIntegrity #DemocraticParty #GetInvolved #Misinformation


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about some people that are running for Secretary of State at
the state level.
We're going to do this because NPR put together a list.
They put together a list.
The list will be down below.
For those who are long time viewers of the channel, you know I don't link articles.
It's because you need to read this one.
It details candidates who are running for that position who spread misinformation or
lied, manipulated, or worse, actually believed the baseless claims about the 2020 election
and they are running for Secretary of State.
These are the people who will be counting the votes.
These are people that would be in a position to alter the outcome, to apply significant
amounts of pressure to subvert an election.
The reason I say, or worse, they actually believed it, is because those people who just
spread misinformation for fun and profit, those who lied and manipulated to obtain power
for power's sake, yeah all of that is bad.
But those people who actually believed it, well they may feel a moral duty to intervene.
These are people who have demonstrated that they have poor judgment, that they don't understand
how it works, and they are running for the position that is responsible for making sure
the elections work.
These should be Democratic Party must-defeat candidates.
You know generally speaking, the Secretary of State positions, these aren't hot elections.
They're not ones that get a lot of coverage because until recently there was the assumption
that whoever ran the office would exercise their duties.
The Democratic Party needs to win these.
They need to win these.
They need to devote the resources necessary to win them.
Consider it expending resources, investing in the 2024 election, because if you lose
these it doesn't matter how much you spend in 2024, and it may not matter what the votes
are.
These are must-win elections.
I will have the link down below.
I'll have the link down below.
It's a relatively short read, and most importantly, it details the names and the states of where
they're running.
If you live in one of those states, and you have ever desired to become active in electoral
politics, now is your time.
These elections will matter.
They will matter long-term.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}