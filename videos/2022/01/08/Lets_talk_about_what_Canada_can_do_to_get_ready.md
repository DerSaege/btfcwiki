---
title: Let's talk about what Canada can do to get ready....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZjjtEiUIvXs) |
| Published | 2022/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Canadians are concerned about the potential impact of a civil conflict in the United States on their country due to their proximity and membership in NATO.
- Speculation arises about what Canada should do if the U.S. experiences a collapse or civil conflict.
- The scenario presented involves two groups emerging from a contested election: the legitimate government and the rebels.
- NATO, including Canada, is likely to support the legitimate government due to control over the U.S.' strategic arsenal.
- Other nations like China and Russia may support the rebel group, complicating the situation further.
- Canada might become a staging area for NATO if conflict spreads in the U.S., given their shared border.
- Concerns for Canada include increased border security to prevent spillover of conflict and handling asylum seekers from the U.S.
- Suggestions include relaxing immigration restrictions, preparing asylum qualifications, and expediting processing for different groups based on resources.
- Documentation and banking issues for Americans seeking asylum in Canada are also addressed.
- Handling millions of firearms from asylum seekers fleeing a civil conflict is noted as a significant challenge for Canada.

### Quotes

- "The scenario presented involves two groups emerging from a contested election: the legitimate government and the rebels."
- "NATO, including Canada, is likely to support the legitimate government due to control over the U.S.' strategic arsenal."
- "Suggestions include relaxing immigration restrictions, preparing asylum qualifications, and expediting processing for different groups based on resources."

### Oneliner

Canadians brace for potential U.S. conflict fallout, considering asylum seekers and NATO staging implications.

### Audience

Canadians, policymakers

### On-the-ground actions from transcript

- Prepare for potential influx of asylum seekers from the U.S. by easing immigration restrictions and expediting processing (suggested).
- Enhance border security to prevent spillover of conflict into Canada (implied).
- Set up accommodations and processing centers for asylum seekers, including handling firearms (implied).
- Make it easy for Americans seeking asylum to handle banking and documentation issues (implied).

### Whats missing in summary

Detailed insights on the implications of potential U.S. conflict on Canada and practical preparations for managing asylum seekers.

### Tags

#Canada #USConflict #NATO #AsylumSeekers #BorderSecurity


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about Canada.
We're going to talk about Canada because I've gotten more messages from Canadians in the last
three or four days than I have in the entire time I've been doing this, and they mostly relate to
just a couple of articles. Those in the United States may not know this because we tend to not
focus on foreign outlets, foreign news outlets, but news outlets all over the world have been running
pieces speculating about what their country should do in the event of a U.S. collapse
if the U.S. was to devolve into civil conflict. Now I want to start off by saying
I don't think it's really at the point where we have to start wargaming this internationally yet.
I do see a lot of roads away from this thing that is kind of now being treated,
at least at the moment, as something that's almost inevitable.
But that being said, I get really mad when people say, oh well it can't happen here,
and yes it absolutely can. The United States is not special. It absolutely can happen here.
So I don't think there's any harm in kind of going through this, which is what most of the
messages were asking about, like how it would impact Canada. And Canadians are more concerned
with it because they're in a special situation. They are members of NATO and they share a giant
border with the United States. Okay, so the scenario that gets presented in these articles
is basically, go figure, a contested election leads to the formation of two groups. One would
be the legitimate government, right, the government that is recognized by the international community.
The other would be the rebels. Okay, and that's how it would start and yeah sure likely
it would start that way. It would fracture pretty quickly. It would break up into even more groups
pretty quickly in my estimation. So one of the things when Americans talk about this
that tends to be overlooked is the entire rest of the world. A lot of Americans feel
kind of like this would happen in a vacuum. It absolutely would not. So I said Canada was part
of NATO and that matters because NATO would enter the conflict on the side of the legitimate
government, whoever that is. Okay, why? Because the legitimate government, the recognized government,
that would be the one that controls the United States' strategic arsenal. Most of NATO
relies on the U.S. and that arsenal for their own nuclear deterrent. They will come in
on the side of whatever that recognized government is.
The rebel side, whoever they are, they would be getting help from near peer opposition nations,
China and Russia. Any nation that wanted to see the United States fail would support the rebel group.
This places Canada in a unique situation because NATO would probably want to use it as a staging area.
If the conflict is widespread in the United States, they probably wouldn't want to use
military installations that are under the control of the legitimate government. They would rather
stage somewhere else and move in on their own, which would make sense. Think about the way
the Allies staged in the United Kingdom prior to moving into continental Europe during World War II.
It would be kind of similar. So you have that aspect of it, and then you have the border,
which is going to create the actual issue for Canadians. Okay, so what would Canada want to do?
The first thing is they probably want increased border security because if a civil conflict
does pop off, it would be super bad for it to spill onto the other side of the fence
and cross over into Canada. So the border would probably be more secured. By the time it was over,
it would probably be militarized because of NATO wanting to use it as a staging area.
Then once NATO forces moved south or moved to other installations via air or whatever,
then Canada's left with its main issue, and that's going to be asylum seekers,
people from the United States wanting to get out. So if it appears like this is something that's
going to happen, Canada might want to start easing that ahead of time because we're talking about
millions upon millions of people. It wouldn't be a small number. So they might want to relax
immigration restrictions from the United States so those who could come early, you know, do.
So it alleviates that problem as it worsens, helps reduce the numbers that are going to come
all in one big surge. So want to do that, and one of the things that Canada might want to do,
I'm not 100% certain, but I think Canada doesn't allow anybody with a felony to immigrate.
Understand United States felonies and Canadian felonies are very different things.
So you might want to take a look at that and more gear it towards stopping people with certain
crimes rather than just a felony in general. You'd probably want to figure out what qualifies
for asylum now ahead of time, you know, before it's needed because once it's needed, it's going
to be a lot of people, a whole lot of people. So it would make sense to put in express lanes.
Now one is going to exist because it always exists, the super rich. The super rich, they're
just going to fly in, you know, fly in, rent a suite, and wait it out. Then you have the middle
class, those people that have some kind of savings, the savings that might let them provide for
themselves for a few months. Those people need to be expedited through. They need to have an
express lane. And it's not because they have money. I mean it is, but not because you want to
treat them better because they're rich. It's because you don't have to expend resources
for them. You can process them through, give them a map, and say go find a hotel, here's your work
authorization, welcome to Canada. Those people who do not have the resources to fend for themselves
for a month or two, you got to find accommodations for them, and that gets hard. So if you can
alleviate that by allowing those people that have the resources to fund themselves to do so,
that's probably a really good idea. Now the other thing Canada's going to run into that
is, it's always surprising to people from outside the United States, documentation. Most Americans
do not have a passport. Good news is with Canada, y'all allow Americans to drive there,
right, on a U.S. driver's license. I would say as part of the processing of that,
the initial processing, take their American driver's license, give them a Canadian one.
That way you have them in the Canadian system right away. If it's good enough to allow them
to drive when they're visiting, just give them the license. That way you have documentation
right from the beginning because there won't be passports. Then you're going to have banking
issues. I would think it's a good idea to make it very easy for Americans to get their currency
out. You're going to have a whole bunch of people showing up. Might as well have them bring their
money too, right. It might even be a good idea if it does begin to look like this is inevitable,
it might be a good idea to set up a counter to that. So if you're going to have a lot of people
coming in, you're going to have to set up accounts where Americans could, I don't know,
open a savings account in Canada and do it easily so they already have money there waiting for them.
Then you run into the big issue, which is securing people that are wanting to come.
You're not going to want a giant crowd of people right outside the border because
eventually one of two things happens. Something triggers them to overrun the border,
such as a giant group of people being perceived as a target and that just forcing them to cross,
or something happens and then conflict ensues between the would-be asylum seekers
and border security. So it would make sense to take border crossings and put long, long fences
deep into Canadian territory, miles, so there could be lines of cars that are already in Canada
but haven't gone through processing yet. So they're not capable of just going wherever,
they have to go through processing first. If you have the fencing set up so they're already
on Canadian soil but haven't gone through processing, it lessens the likelihood of somebody
seeing them as an easy target. And then you're going to need a bunch of connexes,
shipping containers, because if Americans are fleeing a civil conflict, they're going to be
armed. They're Americans. So as they go through processing, you're going to want to take their
weapons and then you've got to figure out what to do with millions of firearms as well,
on top of the people. That would be the largest concern that I see, would be the massive influx
of asylum seekers, because my guess is if that was to happen, and again I don't know that that is
as likely as a lot of people are painting it. It's not out of the question, but I think it's
out of the question, but I don't know that we're at the point where we should really be
talking about this. I don't because I think it's thought exercise.
I think that's going to be the major concern for Canada, is dealing with the asylum seekers.
And then it will end up being a staging area for NATO, because NATO is not going to allow
the United States' strategic arsenal to fall into anybody's hands. Not just is it
bad in the sense of you don't want those types of items to be loose.
NATO relies on the U.S. strategic arsenal. Countries aren't going to allow their own
national security to be put at risk. They're not going to lose their own deterrent because
somebody doesn't want to admit they lost an election. That seems pretty unlikely.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}