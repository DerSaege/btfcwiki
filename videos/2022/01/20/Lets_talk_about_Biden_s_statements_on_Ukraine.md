---
title: Let's talk about Biden's statements on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w28DlpFCOJw) |
| Published | 2022/01/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's recent comments on Ukraine and NATO led to confusion and sparked concerns of escalating tensions.
- Biden's mention of a "minor incursion" was misinterpreted, creating the perception that a small invasion might be acceptable.
- The administration later clarified Biden's statement and undertook a significant damage control effort.
- The mistake in Biden's statement is seen as a significant foreign policy blunder during his term.
- Biden's attempt to differentiate between invasion and incursion was likely a political move to protect himself from criticism.
- The risk of misinterpretation by Russia could lead to miscalculations and potential conflict.
- Both the US and Russia do not desire a war, but mistakes on either side could escalate tensions.
- The situation in Ukraine is viewed as a critical point in the competition between the US and Russia.
- The likelihood of conflict depends on Russia's interpretation of NATO's stance, influenced by Biden's initial comments.
- Russian intelligence and Putin's decisions play key roles in determining the outcome and potential conflict.

### Quotes

- "Biden's made a mistake."
- "Never remove the leverage from your negotiating team."
- "Y'all have a good day."

### Oneliner

President Biden's misinterpreted comments on Ukraine and NATO have raised concerns of escalating tensions, potentially leading to a significant foreign policy blunder with implications for future conflicts.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Stay informed on international relations and foreign policy developments (implied)
- Advocate for clear communication and diplomacy in handling international disputes (implied)

### Whats missing in summary

Detailed analysis of the historical context and implications of Biden's statement on US-Russia relations.

### Tags

#ForeignPolicy #Biden #Ukraine #NATO #Russia


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden's blunder on the foreign policy scene.
We're going to talk about what he said.
Um, we're going to talk about what he was probably trying to say, why
he was trying to say it.
We're going to talk about how the Russian government might read those statements.
We're going to talk about whether or not this is something that might see the
tensions there escalate to the point where there is actually a hot conflict.
Okay, so what happened? President Biden was talking about Ukraine. He's talking
about NATO's posture and how they might respond to Russian aggression and then
he drew a line and he talked about a minor incursion and how the response
might be different for that. The way that came across was that a small invasion
might be okay. It's not what he meant and it was clarified later and then there
was a massive cleanup operation from the administration and there was a speech
given in Berlin today to kind of finalize on the fact that the perception
of what was said isn't reality. So why would he even say this? Because we've
talked about it when Trump was president. You know, heads of state, they always had
to take the hard line. We talked about it specifically with Afghanistan. The
opposition there knew they were gonna win. They knew all they had to do was
wait. As soon as he started saying, we want to leave, we want to leave, we want
leave, removes the leverage from the negotiating team. They don't have it
anymore. It's easy for the negotiator to say, well, maybe we can calm our
head of state down, but it's really hard for the negotiator to say, well, we're
going to convince him to take a stiffer posture if you don't do what we want. The
head of state, when talking about stuff like this, always has to be hard-line or
or say nothing at all.
That middle ground that got presented,
that was just all bad.
This is probably the worst foreign policy mistake
that Biden has made since he's been in office.
So why would he say that?
Why would he try to differentiate
between an invasion and an incursion, a minor incursion?
Incursion can mean a lot of things, some of which
are already happening and we know they're happening,
some of which are probably happening already.
A minor incursion could be a cyber attack.
It could be aerial surveillance that crosses over
into Ukrainian territory.
It could be Russian special operations meeting
with their proxies inside Ukrainian territory.
All of these things, it's kind of likely that they're
happening right now.
So he probably was attempting to couch himself politically.
So if two weeks from now, after he says,
any troops across the border means a full response from NATO,
and then some Russian special operations guys get caught,
if he doesn't provide a full response from NATO
and encourage that politically at home.
The right-wing pundits, well, they're going to tear him up.
Well, he went soft.
See, he's not doing it.
More than likely, that's probably
what he was trying to protect himself against.
But that's not the way it came out.
That's not what was projected by that first comet.
It got cleared up later, but that first comment, that soundbite already exists.
So what does this mean?
Biden's made a mistake.
Now the good news is that this isn't Afghanistan.
This isn't something that's asymmetrical with a large power in opposition to a smaller
power.
This is near peer.
We've talked about it before.
For there to be a conflict between Russia and the United States, both sides have to
make a mistake at the same time.
Neither side actually wants a war with the other.
That's bad.
So the danger arises if Russia makes a mistake right now too.
If when their intelligence agencies are looking at all of these statements and they're making
their assessments, if they decide that Biden's first comment is the actual posture of NATO,
it might encourage them to move forward.
If they do that, that's going to be a mistake because I don't believe that that's actually
the posture of NATO.
But them making that mistake, while Biden made his, if the Russians make one right now,
there is a likelihood of conflict.
Up until right now, if you look at these videos, I've kind of viewed Ukraine as the front line
in the near-peer contest that's going on.
And I think I've said that I didn't expect a conflict on this round of tensions.
It became a little more likely.
It all depends on whether or not Russia makes a mistake right now.
The good news is that Russia's intelligence service, well, it's pretty good.
So it's unlikely that they're going to mess up.
They're probably going to look at the totality of the statements and the overall historical
posture of NATO and base their actions on that.
But Putin is a nostalgic old man and that's dangerous because if he sees an opportunity,
might move forward, even against a good assessment.
This was a bad move.
This was a bad statement from the Biden administration.
And right now, whether or not it becomes a horrible statement depends entirely on how
good Russian intelligence is.
If they can come up with a proper assessment, then it probably isn't going to matter.
And if they make a mistake too, conflict may be a lot more likely.
So hopefully that kind of clears that up as to why he said it, why it was wrong to say
it, and the possible future impacts from that statement.
If you're a head of state, you always have to be hardline or you say nothing at all.
Never, never remove the leverage from your negotiating team and never put the country
in a situation where there has to be a clarification operation like has gone on over the last 24
hours.
Now as a side note, something that isn't enough to be a full video, but it's worth
noting, I had a question come in and it was basically, hey, do you think that the Russian
government is trying to divide the United States from its allies the way the US did
with Project eldest son?
Of course, of course.
They're absolutely trying to do that.
However, they don't need to go to a lot of extremes because we have Fox News.
Fox News benefits from American foreign policy failures right now.
So it is in their interest to play up that first comment and not talk about any of the
clarifications.
It's in their interest to disrupt American foreign policy.
So they don't, the Russian intelligence services don't have to go quite as far in trying to
sow discord among allies.
Fox News is going to do it for them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}