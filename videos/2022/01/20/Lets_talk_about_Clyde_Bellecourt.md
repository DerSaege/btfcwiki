---
title: Let's talk about Clyde Bellecourt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GU7A54mbklo) |
| Published | 2022/01/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the topic of Clyde Bellacourt and his significant impact.
- Expresses disappointment in the media coverage of Clyde Bellacourt.
- Emphasizes Clyde Bellacourt's continuous involvement in fighting for change.
- Details Clyde Bellacourt's contributions, including co-founding AIM and participating in the occupation at Wounded Knee.
- Mentions Clyde Bellacourt's efforts in setting up patrols to monitor law enforcement and curb police brutality.
- Talks about Clyde Bellacourt's initiatives in job training, medical care, and establishing funds for healthcare.
- Notes the resourcefulness of Clyde Bellacourt in achieving these initiatives without modern technology.
- Describes Clyde Bellacourt's establishment of aid networks, schools, summer programs, and fresh produce networks.
- Mentions Clyde Bellacourt's unique approaches in addressing substance issues with the youth.
- Acknowledges Clyde Bellacourt's legacy and his impact on future movements like Standing Rock.

### Quotes

- "Clyde Bellacourt, he helped co-found AIM in 1968, and he did participate in the occupation at Wounded Knee."
- "He's Clyde Bellacourt, and that's how you'll find him."
- "A man that spent decades in the fight trying to make things better, not just for his demographic, but for everybody as a whole."

### Oneliner

Beau introduces Clyde Bellacourt's impactful legacy, detailing his continuous efforts in fighting for change and establishing community programs, setting a precedent for future movements.

### Audience

Activists, History Buffs, Community Leaders

### On-the-ground actions from transcript

- Research Clyde Bellacourt's life and legacy to understand his impact (suggested).
- Support community programs and aid networks in your area (exemplified).
- Advocate for culturally relevant education programs and preservation of native languages (implied).

### Whats missing in summary

The full transcript provides a deeper insight into Clyde Bellacourt's life, legacy, and the importance of recognizing his continuous fight for change and community empowerment.

### Tags

#ClydeBellacourt #AIM #CommunityEmpowerment #Activism #Legacy


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about a man named Clyde Bellacourt.
I had somebody ask me to talk about him, but I kind of figured that he was going to be in the news
and that the media would provide a clear picture.
And I was mistaken. I was mistaken.
Most of the coverage that has come out about him basically kind of sums it up as he co-founded
this group called AIM and participated in the occupation at Wounded Knee.
That makes it sound like he kind of dropped out of the fight decades ago and that's just
not the case.
I think it's important to kind of recap some of the stuff that he did because a lot of
times we forget, if you are active, if you're politically active, you're trying to create
change, we forget how long of a fight it is.
We look at some of the things that we do today and we think that we are innovating, we are
coming up with new stuff.
Yeah, Clyde Bellacourt, he helped co-found AIM in 1968, and he did participate in the
occupation at Wounded Knee. He also set up patrols to help monitor law
enforcement to curb police brutality and misconduct. They didn't have cell phone
cameras back then. It was a little harder. They organized to do that. He organized
programs to to provide job training. You know, medical care established funds to
make sure people could get the medical care that they needed.
And this was in the days before GoFundMe.
A lot of the stuff that we do today, he did it with a whole lot less resources,
with a lot less technology.
He set up aid networks like the community networks we talk about.
He set up schools to reach people in ways that were culturally relevant to them and
to provide an impact and to preserve native languages.
Lots of stuff.
He established summer programs so kids could come out and work on farms and stuff like
that and used that to address food deserts, things we don't really, we think of these
as new concepts.
They're not.
They're not.
He established networks to provide fresh produce to people in need in cities years and years
and years ago.
He came up with some pretty unique ways to address substance issues with the youth, involving
an anipi, a sweat lodge.
He's Clyde Bellacourt, and that's how you'll find him.
Most people within that community called him uncle.
man that spent decades in the fight trying to make things better, not just for his demographic,
but for everybody as a whole.
And his legacy, it extends far beyond just these programs, because beyond this stuff,
the movement that seems so ancient now, when you're talking about the founding of the American
Indian movement and what happened to a wounded knee. It seems like it was so long ago, but
without that, there wouldn't be Standing Rock. It's a chain. It's all connected. We're all one.
Clyde died on January 11th, 2022. Just somebody that, when you have time,
Certainly a life worth looking into and I would say in many ways a life worth
trying to model years after. Somebody who never walked away from the table never
gave up and continued to fight.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}