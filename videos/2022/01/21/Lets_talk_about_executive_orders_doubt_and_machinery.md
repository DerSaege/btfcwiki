---
title: Let's talk about executive orders, doubt, and machinery....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A1KspsrGIyE) |
| Published | 2022/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a draft executive order that proposed the seizure of voting machines in the 2020 election based on baseless claims.
- Points out the lack of evidence to support these claims and how they were fabricated.
- Describes the attempt to have Pence discard legitimate votes and electors in favor of an alternate slate to retain power.
- Criticizes those who supported these efforts and questions their understanding and motives.
- Urges for accountability and a swift investigation into the events surrounding the attempt to overturn the election.
- Warns that without consequences, failed attempts become practice and set a dangerous precedent.
- Labels the events of January 6th as a coup attempt, acknowledging the level of organization behind it.
- Emphasizes the importance of revealing the truth to the American people about the threat their democracy faced.
- Expresses concern that without accountability, future attempts to undermine democracy may succeed.
- Stresses the need to act quickly and decisively to prevent similar events in the future.

### Quotes

- "They're baseless claims."
- "This was an attempt to drive a stake into the heart of the Federal Republic."
- "Without accountability, it's not failed, it's practice."
- "It's time to show the American people what almost happened to their country."
- "The evidence that is being released piecemeal is backed up by more."

### Oneliner

Beau warns of a dangerous coup attempt, stresses the importance of accountability, and urges swift action to protect democracy.

### Audience

American citizens, Activists

### On-the-ground actions from transcript

- Demand accountability from elected officials (suggested)
- Support and push for a thorough investigation into the events of January 6th (exemplified)
- Educate others about the dangers of undermining democratic processes (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the attempted coup, stressing the need for accountability and swift action to protect democracy.

### Tags

#CoupAttempt #Accountability #Democracy #AmericanPeople #Investigation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk a little bit about a draft executive order
and what that might mean and how that fits in with some of the other mechanics
that we have learned about.
And it should prompt a lot of questions from a lot of people who may have
been giving the benefit of the doubt to a lot of people who probably don't
necessarily deserve it.
There was a draft executive order for Trump to sign that would have allowed
the seizure of the voting machines in the 2020 election, citing these baseless claims.
And you can say that, you know, maybe they weren't baseless.
They were. It's been more than a year.
There's no evidence to support them. None.
They made it up. They lied.
It's not true.
After all this time, no evidence has come forward because it doesn't exist.
They're baseless claims.
And they were going to seize the voting machines.
Why? Why?
What good would that do?
Let's look at the mechanics of one of the other potential avenues that they
explored, maybe attempted, in pursuit of retaining power against the will of the
American people.
Look to the events of the 6th.
The idea was to have Pence take the legitimate electors, the legitimate votes,
and toss them out, take those and seize them, and say, no, we're not going to use these.
We're going to use this alternate slate of electors, this bogus slate over here.
We're going to use those.
We're going to replace the votes that were made with this stuff over here.
That's wild. In and of itself, that's wild.
Now pair that with the idea that they wanted to seize the voting machines.
This was an attempt to drive a stake into the heart of the Federal Republic.
The more we learn about the various avenues they were exploring, planning, perhaps
attempting, the more concerted it seems, the more connected they all seem.
Those people who supported this man, those people who supported this effort, a year
later, you have to ask yourself, where's the evidence?
You gave them the benefit of the doubt.
Where's the evidence now?
It doesn't exist because there isn't any, because they made it up.
Just like every other wannabe tyrant throughout history, they made it up.
And then you look at those who followed along, those who provided political cover
because he happened to have an R after his name.
What about them?
Do they get the benefit of the doubt?
I find it unlikely that people did not understand or did not know about what he was entertaining.
When you have messages from the House Freedom Caucus, from that crew of people warning that
these attempts would drive a stake into the heart of the Federal Republic, who else knew?
Who else knew?
And why didn't they come forward and say, hey, this is what he's planning?
Was partisan loyalty more important than loyalty to the country, loyalty to the base principles
of a free society?
How much benefit of the doubt do they get?
What's going to happen during the investigation?
At this point, as we learn about the mechanics, it's important for the speed to accelerate,
for the committee, for whatever DOJ is doing, that they've got to speed up.
This was a clear attempt.
And without accountability, it's not failed, it's practice.
That's not just some kind of talking point.
It's not a slogan.
It's the way it works throughout all of history.
Tossing electors, having additional alternate electors ready, seizing voting machines, pressing
secretaries of state, and then the actual events of the sixth.
This very, very early on, while it was still happening, I said it was a coup attempt.
I kind of mocked it, to be honest, because it was incredibly unorganized.
Far more organized than I initially thought.
They had the mechanisms.
They had the machinery thought out.
They just didn't have the logistics to do it.
Rest assured that if there isn't some form of accountability, the next group of people
will have the logistics.
They've laid out a template on how to do it.
And had Trump been a little bit more polished in his rhetoric, had he been a little bit
less inflammatory, had he not kept the entire nation on guard for this eventuality for four
years, it probably would have worked.
This isn't the time for politics.
This isn't the time to drag this out to try to get it closer to the midterms.
The evidence that is being released piecemeal is backed up by more.
It's time to show the American people what almost happened to their country.
How close that stake got to the heart of this country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}