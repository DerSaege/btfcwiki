---
title: Let's talk about why people are burning Carhartt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8Z2tcXjAef8) |
| Published | 2022/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Carhartt expressed support for vaccines, sparking backlash on social media.
- Some individuals responded by setting Carhartt products on fire to show their disapproval.
- Beau questions the reasoning behind this extreme response.
- He suggests that those burning the products are not targeting employees but rather expressing dissatisfaction with leadership decisions.
- Beau acknowledges the right to protest and burn items as a form of expression.
- He reminds people that such actions don't directly impact the company but serve as symbolic protests.
- Burning a symbol is meant to convey discontent with those in charge, not harm the company financially.
- Beau urges individuals to understand the symbolic nature of their actions and their intended message.
- He encourages reflection on attachment to symbols and the need to separate them from their actual meaning.
- Beau concludes by leaving the audience with a thought to ponder on the importance of symbols and their representation.

### Quotes

- "You're not attacking Carhartt. You're not attacking the employees. In your mind, you're trying to make it better for them, right?"
- "It's a symbol. And it's a way that you are expressing your displeasure."
- "Perhaps you've grown a little too attached to those symbols as well."
- "Just a thought."
- "Y'all have a good day."

### Oneliner

Beau questions the symbolic burning of Carhartt products in protest, urging reflection on separating symbols from their actual meaning.

### Audience

Online activists

### On-the-ground actions from transcript

- Contact Carhartt to express views on their public statements (suggested)
- Engage in respectful dialogues with others about the significance of symbols (exemplified)

### Whats missing in summary

The full transcript provides additional insights into the dynamics of symbolic protests and the importance of understanding the intended message behind such actions.

### Tags

#Protest #Symbolism #Expression #CorporateResponsibility #SocialMedia


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about Carhartt
and why there is some wild stuff going on
on social media today.
If you missed it, Carhartt decided to say,
hey, vaccines are good.
A lot of people are upset with this
and some have decided to show their displeasure
with the company by setting the Carhartt's ablaze
and taking photos of it and posting it to social media.
Which, I mean, sure, I get it.
I understand, you know, basic witchcraft
and all that stuff, exercising the demons and whatever.
Why are you doing it?
Are you mad at the employees that make this stuff?
No.
Are you mad at the employees in the stores that sell it?
No.
And in fact, in your narrow scope of reality,
if you're doing this,
you're doing it because you're trying to protect them.
You wanna make Carhartt better, I guess.
You wanna protect those employees.
You don't feel like they should have
this mandate pushed upon them, right?
You're burning this product because it's a symbol
and it's a way to express your displeasure
with those at the top, those making the decisions.
That's why you're doing it, right?
I don't think I'm wrong about that.
It's a form of protest.
And while I happen to think vaccines are wonderful
and a good thing, I get it.
And you have a first amendment right to do that.
You have a right to torch your hat if you want to.
No problems there.
I mean, have fun with that.
Be safe though.
The only thing that I would ask is that
the next time you see somebody with a flag doing it,
you remember this.
The next time somebody kneels during a song,
you remember this.
You're not attacking Carhartt.
You're not attacking the employees.
In your mind, you're trying to make it better for them,
right?
You're expressing your displeasure with those at the top
by torching a hat, by burning a symbol.
It doesn't actually hurt the company, right?
You already bought it.
It hurts your wallet, not theirs.
It's a symbol.
And it's a way that you are expressing your displeasure.
Just remember that.
Perhaps some of the other people
that perhaps some of the other brands
that you have bought into have,
maybe you've grown a little too attached
to those symbols as well.
And you can't separate the symbol from the actual item,
the thing it's supposed to symbolize.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}