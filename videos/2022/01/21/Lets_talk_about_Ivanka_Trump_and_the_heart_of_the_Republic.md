---
title: Let's talk about Ivanka Trump and the heart of the Republic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zatQa66Fz0E) |
| Published | 2022/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recap of developments from the committee, focusing on Ivanka Trump's invitation.
- Understanding the mechanics of how power retention was planned.
- Multiple plans coordinated to pressure states and brute force change election results.
- Reporting reveals Rudy Giuliani's involvement in organizing alternate electors.
- Events of January 6 could pressure Pence to make desired decision or invoke Insurrection Act.
- Planning for the coup attempt likely started before the election.
- Mechanics of the self-coup attempt becoming clearer, showing pre-election planning.
- Mention of a message warning about the impact on the Federal Republic if plans went through.
- The seriousness and long-term effects of the events emphasized.
- Importance of understanding the broader implications of the developments.

### Quotes

- "Drive a stake into the heart of the Federal Republic."
- "There were people in that caucus who realized the long-term effects of what was going to occur."
- "Understanding the mechanics and understanding that it wasn't just a singular event."

### Oneliner

Beau delves into the committee's developments, revealing multi-faceted power retention plans and warning of potential impacts on the Federal Republic.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact representatives to express concerns about potential threats to democracy (implied).
- Stay informed and engaged with developments in governance (suggested).

### Whats missing in summary

Detailed analysis of the potential long-term consequences and historical context.

### Tags

#CommitteeDevelopments #PowerRetentionPlans #CoupAttempt #FederalRepublic #DemocracyProtection


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to go over some of the developments
from the committee.
Most people are focusing on the invitation
the committee has extended to Ivanka Trump.
And I get it, I understand that.
It's symbolic in a way, finally getting somebody
with the last name of Trump to be invited to come talk.
That invitation will probably be declined,
it will eventually be turned into a subpoena would be my guess. And I understand why the
focus is on that. She was the fair-haired Goldilocks who was supposed to keep him in
check. I get it. But I think there were far more important developments because we're
starting to understand the mechanics of how they plan to retain power. Because we've
We've kind of been operating under the assumption that there was a plan.
It seems like there were multiple plans that were to work in concert and individually.
We have the attempt to pressure the states directly through perfect phone calls to secretaries
of states asking them to find the votes, stuff like that.
He had that plan, trying to just brute force it through and change the results.
We're getting reporting now that the, let's just be polite, the alternate electors, the
bogus electors, that that was organized by Rudy Giuliani, somebody inside Trump's inner
circle.
The reporting is saying that Trump's inner circle was involved in getting these made
and that these were to just kind of stand by.
So if Pence threw out the legitimate electors, those would be right there and he could just
substitute them.
And then you have the actual events of the 6th, which could be used to pressure Pence
to make the decision Trump wanted him to make, and we now have reporting of phone calls that
went to Pence from Trump trying to get him to do it.
It could be used for that or it could be used as a pretext to invoke the Insurrection Act.
The thing is, I am fairly certain that when the final history of this is written, we'll
find out that the planning phases of this, the initial beginnings of this little coup
attempt. We're gonna find out that it started before the election even took
place. Understanding the mechanics and understanding that it wasn't just a
singular event, there's no way with everything else that's been uncovered
that they can portray it as, oh Trump just misspoke and those people went wild.
We have, okay, well, what about this stuff over here?
What about these documents that were ready to be switched out?
We're beginning to understand the mechanics of how they were going to attempt to the self-coup.
And given some of the logistics involved in this and some of the early floating of the
is, I think we'll find out that the plans originated before people even voted because
they had that little respect for the institutions, for the republic, for the country that he
said he wanted to make great again.
Then the one line that I think people should really, really note is a message that went
from a member of the House Freedom Caucus, if you don't know, those are the right wingers
of the right wing to the White House.
The message said that if Trump went through with what he was planning on the 6th, it would
Drive a stake into the heart of the Federal Republic.
Drive a stake into the heart of the Federal Republic.
It would end the United States.
It would create a change of governance.
If you were somebody who was on the fence about the seriousness of it, if somebody in
And the House Freedom Caucus, these are the people that should have been aligned with
him completely, sending a message warning that that's going to be the impact.
I think that that is probably the most important part of this.
There were people in that caucus who realized the long-term effects of what was going to
occur.
They warned the White House, but it didn't seem to matter.
That's the part that people really need to pay attention to.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}