---
title: Let's talk about agents, influence, and cultural opinion makers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VHVjEFZ0eG8) |
| Published | 2022/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Delves into the world of intelligence, clarifying the terminology between "CIA agent" and "CIA officer," explaining the difference between an agent and an officer.
- Mentions the overlooked yet valuable type of agent called an "agent of influence," individuals who can shift thought and influence public opinion.
- Describes agents of influence in various fields like movies, art, academia, government, journalism, and daytime TV talk shows.
- Explains that agents of influence can be recruited from individuals who may be disaffected or dissatisfied with their current status.
- Points out the difficulty in identifying and catching agents of influence once recruited, as they know their role well.
- Raises concerns about the heavy reliance on technical means of gathering intelligence, leading to a potential presence of agents of influence in the United States.
- Suggests the possibility of non-state actors using similar tactics to influence politics within a country.
- Emphasizes the significant impact agents of influence can have on shaping foreign policy, even though they may not be as glamorous as portrayed in movies.

### Quotes

- "An agent of influence is somebody who is a cultural opinion maker."
- "Throughout this person's career, well, they just run the standard patriotic line the entire time."
- "Once recruited, can end up creating more."
- "They're often the most effective."
- "They can shift foreign policy more than anything else."

### Oneliner

Beau delves into the world of intelligence, discussing agents of influence and their significant impact on shaping public opinion and foreign policy.

### Audience

Intelligence enthusiasts

### On-the-ground actions from transcript

- Identify potential disaffected individuals who may be vulnerable to recruitment as agents of influence (implied).
- Stay informed about the tactics used by agents of influence in different fields to recognize potential manipulation (implied).

### Whats missing in summary

The full transcript provides a detailed insight into the role of agents of influence and their potential impact on public opinion and foreign policy decisions.

### Tags

#Intelligence #AgentsOfInfluence #ForeignPolicy #PublicOpinion #Recruitment


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
influence and we're going to talk about agents. We're going to delve a little bit
into the world of intelligence today. First we have to clear up some terminology
because you hear something a lot and it's using vocabulary incorrectly.
And this is in a whole lot of movies but we have to clear this up to move forward.
How many times have you heard the term CIA agent? Probably a lot, right? More than
likely that's a CIA officer. An agent is the person the officer is running. An
agent is typically somebody who was turned or recruited. Sometimes they were
inserted but that's rare. And there are different kinds of agents. Now one of
the most overlooked and also one of the most valuable is a type of agent called
an agent of influence. An agent of influence is somebody who is a cultural
opinion maker. They can shift thought. They can change the way a target
demographic views things. They're not talked about movies a lot because
it's not like James Bond. You know, there's no casinos, there's no
parachutes. These are people who have normal jobs. They could be producers or
directors in movies. In the art world there are a lot of opportunities to get
agents of influence. You know, a producer or director could put out a series of
films that highlights a particular nation as the bad guy or as the good guy
or however. They can shift thought that way. You see agents of influence being
recruited in the academic world as well to put out think tank pieces that are
favorable to the officer's home country, to the intelligence service that is
running the agent. Another place you see them is actually in government. That's
rare, a little bit more rare, but in elected office. Those types of
agents, those are like crown jewels. It doesn't happen very often, but when it
does, that's a huge win for the intelligence service that's doing it.
Another place where it's really common is journalism or daytime TV like
talk shows. People who can shift public opinion. Imagine you had somebody who was
seen as a true patriot and was a cultural opinion maker within that
demographic. They looked to this person for answers. See, throughout this person's
career, well, they just run the standard patriotic line the entire time
they're having their show or writing their articles or whatever the case may
be. But anytime the subject turns to the country that is running that agent of
influence, well, they find a way to make it your patriotic duty to support this
other nation, to support their position. And see, the best part about it is that
once the agent's been recruited, it doesn't take much. You don't have to meet
him. There's not a lot of dead drops. There's not a lot of face-to-face. They
know what they're supposed to do and they do it. You just have to find the
people who would be up for something like that. A good place to look would be
people who felt that they deserved more. Perhaps they applied to work for their
own intelligence service in their own country and didn't make it for whatever
reason. That person would be primed for recruitment. Or maybe they felt that they
were in a position that was below their stature. Maybe they were...
they viewed themselves as somebody who should be in elected office, who
should really be up there high. And here they are writing articles for some
magazine. They're disaffected in some way. Those people would be primed for
recruitment. And then once the recruitment takes place, there's not
really a whole lot to do after that. They know their job. They know what they're
supposed to do. So it's really hard to catch them. It's really hard to identify
them. Like let's say the Russians, as an example, because of the current political
climate. Let's say they establish some agents of influence in the United States.
Today, because of everything that's going on with Ukraine, these people would
probably be pushing the position that we don't need to have anything to do with
Ukraine. We need to have nothing to do with that at all. Because that's really
Russia's area. They have control over that. Even though it's a sovereign
nation, they might try to deflect the attention to another nation and
make them seem as though they're the greatest threat. And this is how they
shift opinion. The United States for a long time became heavily reliant, in my
opinion over-reliant, on technical means of gathering intelligence. Signals
intelligence, electronic intelligence, satellites, stuff like that. They didn't
do a lot of human intelligence. It got cut back. Most other intelligence
agencies did not make this mistake. It would not surprise me if there were
agents of influence within the United States operating right now on behalf of
any number of countries. Another avenue for this, something else that may come up
in the future, is seeing these tactics used by non-state actors. Seeing this
type of tactic used for normal politics, quote, within a country. Perhaps an
intelligence officer turning a politician to get them to support this
other politician. Using the same tactics, the same techniques, to turn them into an
agent of influence, for a new doctrine, for a way to supplant the current
ideology of a political party and replace it with something new. That's
something else that we might see happen. Or maybe we already did. It's one of
those things that this is a term and a concept I think people need to become
familiar with. Because as we learn more and more information about certain
events, I have a feeling that we're going to find out that this tactic, this type
of operation where you convert somebody into being your mouthpiece, I think we're
going to find out that it has happened at least once or twice in the United
States. Now whether or not it was a non-state actor within the United States
or somebody operating at the behest of a foreign intelligence service, we'll
probably never know. Because this type of stuff is really hard to track. The other
part about it is that one agent of influence, once recruited, can end up
creating more. We know how the news cycle works. Let's say hypothetically Rush
Limbaugh, back in the day, was turned and he became an agent of influence for a
foreign nation. Remember how all the radio shows based their show, all the
conservative talk radio shows, they based their show off of his? So if they turned
Rush, all of the other shows, all the other cultural opinion makers would
follow suit. And in this way, while they're not the most glamorous, these
aren't the type of agents that have movies made about them, they're often the
most effective. They're often the type that can shift foreign policy of the
target nation more than anything else. Anyway, it's just a thought. Y'all have a
good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}