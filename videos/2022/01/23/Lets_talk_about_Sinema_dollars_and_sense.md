---
title: Let's talk about Sinema, dollars, and sense....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EodwFF1SxJk) |
| Published | 2022/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Sinema has opposed creating an exemption in the filibuster for voting rights legislation, obstructing President Biden's agenda.
- 70 donors to her campaign, including many who gave the maximum allowable contribution, will withdraw support if she doesn't change her position.
- Progressives in Arizona are organizing a primary challenge against Sinema.
- Sinema and her staff aim for higher political office, potentially prioritizing funding for her future ambitions over voting rights legislation.
- Sinema may view herself as a Democratic equivalent to Donald Trump, someone challenging the party's norms.
- Democrats are a coalition party with diverse ideologies, making it challenging to position oneself as an outsider.
- Sinema's strategy, heavily reliant on big donors, is likely to fail without their support.
- The pressure from donors and other forms of backlash might lead to a shift in Sinema's stance.
- Financial support is vital for winning elections, making it necessary for Sinema to reconsider her position.
- Overall, Sinema faces significant challenges due to her stance on voting rights and opposition to party norms.

### Quotes

- "Sinema and her staff seek higher political office."
- "Progressives in Arizona are organizing a primary challenge against Sinema."
- "The Democratic Party is a coalition party with diverse ideologies."
- "The pressure from donors might lead to a shift in Sinema's stance."
- "Support from big donors is vital for winning elections."

### Oneliner

Senator Sinema faces backlash for opposing voting rights legislation and may need to reconsider her position due to pressure from donors and progressives organizing a primary challenge.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Organize and support the primary challenge against Senator Sinema (exemplified)
- Mobilize to pressure Senator Sinema to change her position on voting rights legislation (implied)
- Donate to progressive candidates challenging incumbents who oppose voting rights (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of Senator Sinema's political challenges, including the role of big donors, party dynamics, and the potential impact on future elections.


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about Senator Sinema and some of the some of the troubles she's likely to
face in the near future.
Sinema has been one of two key Democratic senators that have opposed
carving out an exemption in the filibuster for voting rights legislation.
She has also in many ways obstructed President Biden's agenda. A letter was
sent from 70 donors to her campaign, many of whom gave the maximum
contribution allowable by law, saying that they will sever all support if she
does not change her position. This comes at a time when progressives in the state
of Arizona are already organizing a primary challenge to her. Sinema has
indicated and her staff has indicated that she seeks higher political office.
While she may not fully grasp the importance of voting rights legislation,
she may not grasp the importance of the Democratic Party racking up a bunch of
wins in the run-up to the midterm, she might understand the importance of that
funding to her own future political ambitions. This is probably going to be
more important and weigh more heavily on her decision-making than some of the
more unique methods people have tried to get her attention. In many ways I think
she sees herself as a Democratic version of Donald Trump, somebody who can fly in
the face of the party's idea. The difference between the Republican Party
and the Democratic Party is that the Republican Party is total status quo,
total status quo and moving back, nostalgia. So as long as you're
generally moving in that direction, it's okay, everybody's on the same team. The
Democratic Party is not like that. There is no unity like that. It is a coalition
party between centrists, liberals, left-leaning liberals, and actual leftists.
There isn't a lot of room to try to cast yourself as the outsider because
everybody's an outsider. Everybody is playing for their own sub-demographic
within the coalition that makes up the Democratic Party. The strategy that she
has adopted is one that I'm fairly certain will fail and without the
support of big donors it's guaranteed to. Now is this going to be enough in and of
itself to shift her vote? I'm not sure, but this combined with the rest of the
backlash she has been getting, we may see a reversal in the future. If she intends
on pursuing higher political office, she has to because without money you're not
going to win an election. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}