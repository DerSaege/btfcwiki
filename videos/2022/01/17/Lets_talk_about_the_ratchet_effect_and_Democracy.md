---
title: Let's talk about the ratchet effect and Democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mHbwJgZt-tM) |
| Published | 2022/01/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the ratchet effect, where the Republican Party moves the country right and Democrats prevent it from moving left.
- Addresses a message from a college student questioning the effectiveness of Democrats and the ratchet effect.
- Points out the difference between real leftists and American leftists in political discourse.
- Critiques the idea of the ratchet effect leading people to believe there is no difference between Republicans and Democrats, discouraging political involvement.
- Argues that there is a difference between the two parties, not just in left-right terms but in quality of life and social democracy issues.
- Affirms that democracy is under attack, particularly by the Trumpist faction of the Republican Party.
- Emphasizes the Republican efforts to undermine democracy through tactics like gerrymandering and voter suppression.
- Acknowledges that Democrats are working to maintain a semblance of representative democracy in contrast to authoritarian tendencies.
- Encourages individuals, like Erica, who challenge the status quo and push for more radical ideas, especially in a university setting.
- Stresses the importance of individuals taking the lead in pushing for progressive changes, rather than relying solely on political parties.

### Quotes

- "The Democrats are supposed to lead you. No, you're supposed to lead you."
- "It's not their job. It's your job."
- "If your starting place is further over than me, good, good."
- "You're supposed to lead the country left."
- "You're supposed to institute those reforms."

### Oneliner

Beau explains the ratchet effect, critiquing the belief that there's no difference between Republicans and Democrats, while stressing the individual's role in leading progressive change.

### Audience

College students, political activists

### On-the-ground actions from transcript

- Connect with student political groups on campus to understand the differences between political ideologies (implied)
- Engage in political discourse and activism on quality of life issues and social democracy (implied)
- Stay informed about threats to democracy and participate in efforts to safeguard democratic principles (implied)
- Challenge traditional political narratives and push for more radical ideas for societal change (implied)

### Whats missing in summary

Exploration of the importance of individuals driving political change rather than solely relying on political parties.

### Tags

#Democracy #PoliticalActivism #RatchetEffect #Difference #IndividualLeadership


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about the ratchet effect.
And we're going to answer some questions from somebody who's
in college.
And we're going to talk about whether the Democrats are
worth anything at all, whether they do any good at all.
OK, so here's the message.
I've been watching you since I was in high school.
You're the reason I'm politically active at all.
I've watched every single video you've made.
Now I'm at blank university.
I'm involved in a lot of political groups on campus
who say Democrats aren't really left, which you've said,
but they say there's no difference
between Republicans and Democrats
and talk about the ratchet effect.
You seem to think there is.
Can you expand on this?
And can you say whether democracy is really under attack
as Democrats seem to be framing it?
And then, do you have any response to my people who say, you're barely even left.
It would be great if you could address this part to Erica.
Sure.
Okay.
All right, so if you're not familiar with the ratchet effect, it's a meme.
It's the idea that the Republican Party moves us right, moves the country right, and the
Democrats stop us from moving left.
Now this is a person who has a lot of things on their profile, such as flags and flowers
in their screen name.
When they say left, they mean real left, international left, not American left.
So they're talking, they're moving over from where we're at now from the status quo to
rejecting capitalism.
That's the dividing line.
They're talking about real leftists here, not just the American left.
And when I was looking at the profile, there were a couple of places that I recognized
because they are politically active on campus, and that becomes important here in a minute.
So I have issues with the idea of the ratchet effect.
Not that it's inherently wrong, because it's kind of not.
The Democratic Party is very ineffectual at moving to the real left, to processing leftist
ideas, because most of them aren't actually leftists.
They're American left, so they don't drive that direction very often.
The other thing that I have an issue with when it comes to the ratchet effect is that
that it leads people to believe, much like this message, that there is no difference
at all, that there's no difference at all, and that it's pointless to be involved.
Because if that's the case, if the people that are the main opposition are really just
holding the line for the true authoritarians, the true right-wing people, then what good
is there?
I don't like this framing.
I think it leads to conspiratorial thinking and a whole bunch of bad stuff.
But I understand where the idea comes from.
So first, is there any real difference?
One of the places that you have a photo of you in is a library that has a whole bunch
of different student political groups there all the time.
over and talk to the women from now, from the National Organization for Women, right?
Go talk to them.
Ask them if they would be fighting these same fights they're fighting now with the same
intensity they have to fight them if Democrats had been in power the preceding four years.
There is a difference.
But it's not really a difference that can be summed up on left-right, because most Democrats
aren't actually leftist, by the way you're thinking, most Democrats.
There aren't any major Democrats that are really leftists.
It's more quality of life stuff, it's more social democracy stuff, moving towards leftism
but not quite going over the line yet.
stuff like that or basic civil liberties. Now, the next part of it is, so we've
established there is in fact a difference, then the idea is is it worth
it and in this case is democracy really under attack? Yeah, yeah. Make no mistake
about it. If the Trumpist faction of the Republican Party had control of the
federal government for eight years, we wouldn't be worrying about the next election because
we wouldn't be having one.
It is that authoritarian.
It is that antithetical to democracy and the system of representative governance that we
have in the United States.
If you want to know if they're actually trying to undermine democracy, just look at the headlines.
the gerrymandering, the voter suppression, all of that stuff, it's out in the open.
It is out in the open.
They're talking about the quality of the votes rather than the quantity of the votes.
These are quotes that they're saying publicly.
Not just are they attacking the fundamental nature of representative democracy, they're
doing it in the open.
are openly doing this. Yes, that battle is real. And yes, Democrats actually do
appear to be interested in holding the line on this one. Maybe it's just so they
can retain power, but at this point it does appear that they are trying to
maintain some semblance of a representative democracy. To me, that's a
huge win over the authoritarian state that would exist under Trumpist
Republicans. So it does matter. And yeah, that framing is accurate. Now, do you have
any response to my people who say you're barely even left? It would be great if
you could address this part to Erica. So my guess is that Erica is somebody
probably from that library, who basically looks at me and says, well, he doesn't go
far enough.
And these are the sorts of people that you're hanging out with in college.
People that are more radical than me.
Good.
They're supposed to.
They're supposed to say that.
And I would hope that eventually you think that.
The idea of when you're at college, when you're at a university, you're supposed to be embracing
all of the new ideas.
You're supposed to be figuring all of this out.
And that should start with pushing it as far as you can and embracing all of the ideas.
Because while we talk about the ratchet effect, the idea that's present in that is that the
Democrats are supposed to lead you.
No, you're supposed to lead you.
You're supposed to lead you.
You're supposed to lead the country left.
You're supposed to institute those reforms
and bring about all of those better changes
that you want to see.
Not the Democrats.
It's not their job.
It's your job.
And if your starting place is further over than me,
good, good.
that's that's perfectly okay. I like Erica. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}