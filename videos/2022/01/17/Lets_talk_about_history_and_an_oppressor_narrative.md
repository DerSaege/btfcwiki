---
title: Let's talk about history and an oppressor narrative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b9xl5uvqtQg) |
| Published | 2022/01/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses support for a theory misunderstood by the right wing as a historical theory rather than legal studies.
- Receives message calling the theory racist garbage that divides people along race lines.
- Points out the historical struggle between oppressor and oppressed classes throughout history.
- Emphasizes that history is not a story but what actually happened.
- Asserts that history must be accurate to teach lessons and draw parallels to the present.
- States that the narrative of oppressed versus oppressor is accurate and not wrong to teach.
- Explains that being neutral in the struggle between oppressor and oppressed sides is choosing the side of the oppressor.
- Argues that history books must accurately depict the dynamic between oppressed and oppressor.
- Challenges the discomfort felt when reading history books and the identification with oppressors rather than those seeking change.
- Encourages reflection on why one identifies with the oppressors in history.

### Quotes

- "History can teach lessons, but it should never be brought back as a tool of division and accusation."
- "If you're neutral in that struggle that has existed throughout the human experience, then you have chosen the side of the oppressor."
- "If you are reading something that claims to be a history book, and it doesn't make you uncomfortable, you're not reading a history book."
- "If you read these history books, and you identify with the slavers, rather than those who helped the Underground Railroad, that says something about you."
- "It's not manufactured, point to a time in American history where that dynamic wasn't at play."

### Oneliner

Beau explains the accurate depiction of history as a narrative of oppressed versus oppressor and challenges readers to identify with those seeking positive change.

### Audience

History learners and critical thinkers.

### On-the-ground actions from transcript

- Analyze history books for accuracy and discomfort (implied).
- Identify with those seeking positive change in history (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of historical narratives and challenges readers to critically analyze their perspectives on historical events.

### Tags

#History #Narratives #Oppression #CriticalThinking #Education


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a theory.
We're gonna talk about history.
We're gonna talk about historical narratives
and how those narratives shape history.
And they provide us the lessons that we can learn from it.
Recently, I expressed my support for a certain theory.
And it's... the thing is, what it's being called is not actually what it is.
But when the right wing got a hold of this talking point,
they shaped a narrative around it.
And they turned it into a historical theory
rather than something that has to do with legal studies.
But whatever.
After expressing my support for it, I got this message.
It's racist garbage that only seeks to divide people by creating
an oppressed versus oppressor narrative along the lines of race.
History can teach lessons, but it should never be brought back
as a tool of division and accusation.
Okay, see, one of the interesting things about the right wing
latching onto CRT as a term,
it's led people to kind of just acknowledge that CRT is history.
The two things have become pretty much entwined,
even though it's not accurate in the popular perception of things.
That's the way people are beginning to see it.
And the general idea now is that it's creating a narrative
of oppressed versus oppressor.
And that somehow that's A, inaccurate and B, bad.
I would like you to point to a period in history in this hemisphere
since the arrival of Europeans that could not be defined
by a struggle between an oppressor class and the oppressed class.
I hate to be the one to break it to you, but that's the human experience.
That is the human experience.
As long as there are those who seek power,
there will be those who seek to divide and divide along race,
ethnicity, religion, a whole bunch of different things,
anything they can use to other people.
And that creates that oppressed versus oppressor narrative
that you're looking for.
The word is dynamic.
It's not a story.
It's what happened.
That's history.
That's what occurred.
History can only teach lessons if it's accurate.
If you can look at it and say, hey, this is what occurred,
and then you can draw a parallel to what's occurring now
because while history doesn't repeat, it does rhyme.
The only way to draw that benefit is if you discuss it accurately.
Yes, there is a narrative of oppressed versus oppressor
because that's history.
That's the human experience.
That's what's occurred.
Go back.
You can go back to ancient times, and you will find this narrative,
it's dynamic because it's how it works.
If you go all the way back then, you will find people denying
that that exists or justifying it in some way.
Oh, well, we treat them really well here.
So, A, it's accurate.
Now, B, that it's somehow wrong to teach that. It's not.
The reason the sharpness of that dynamic has declined is because we have taught it.
We have explained the dynamic between oppressed and oppressor,
and we decided that if you're neutral in that,
if you're neutral in that struggle that has existed throughout the human experience,
then you have chosen the side of the oppressor.
That is American history.
It's world history.
If you want to get rid of that dynamic from your history books,
you can't call them history books anymore.
They're a fairy tale, a story made to make grumpy middle-aged men feel better about themselves.
That's not reality.
The world is a scary place.
A lot of horrible things happen.
If you are reading something that claims to be a history book,
and it doesn't make you uncomfortable,
and you don't look at some of it and go,
why would that happen, why would they do that?
You're not reading a history book.
You're reading a propaganda text.
That dynamic between the oppressed class and the oppressor,
it exists, it's real, it's not manufactured,
point to a time in American history where that dynamic wasn't at play.
You can't.
CRT, as is commonly viewed, that's just history.
Now, it may make you feel uncomfortable.
You may feel like you're being subjected to an accusation.
Look at me. It doesn't get much whiter than me.
I don't feel like that.
Why do you?
Why do you identify with the bad guys in the story?
Why don't you identify with those who sought to make the world better,
who sought to bring about that change?
If you read these history books,
and you identify with the slavers,
rather than those who helped the Underground Railroad,
that doesn't say anything about the text.
It says something about you.
Anyway, it's just a thought. Bye-bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}