---
title: Let's talk about Ahmaud and Houston County cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=V-AeS2gBXlo) |
| Published | 2022/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Case of Houston County Sheriff's Department resurfaces, involving a deputy's controversial comment about Ahmaud Arbery's killers.
- Deputy's statement, publicly endorsing extra judicial execution, was condemned but sheds light on the department's culture.
- Sheriff swiftly fired the deputy, but the incident reveals deeper issues within the department.
- Sheriff must recognize the culture within his department and take serious actions to reform and retrain.
- Failure to address the underlying culture may lead to more severe problems and loss of lives.
- Public statement by the deputy hints at potentially widespread problematic beliefs within the department.
- Sheriff needs to thoroughly inspect and address the department's culture to prevent further harm.
- Deputy's public statement indicates a concerning mindset that could have serious consequences when not under public scrutiny.

### Quotes

- "When people tell you who they are, you need to believe them."
- "Failure to address the underlying culture may lead to more severe problems and loss of lives."
- "Sheriff needs to go through that department with a fine tooth comb."

### Oneliner

Houston County Sheriff's Department faces scrutiny over deputy's public endorsement of extra judicial execution, prompting a call for reform and reevaluation of departmental culture.

### Audience

Law enforcement officers

### On-the-ground actions from transcript

- Inspect, retrain, and reform the department to address the underlying culture (implied).

### Whats missing in summary

The full transcript provides detailed insights into the culture within the Houston County Sheriff's Department following a deputy's controversial endorsement of extra judicial execution, urging immediate reform to prevent further harm and loss of lives.

### Tags

#LawEnforcement #Culture #Reform #CommunityPolicing #Accountability


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a case that I thought was over.
Case I didn't think was going to be making any more headlines, yet here we are.
And we're going to talk about the Houston County Sheriff's Department, and
we're going to talk about the culture in law enforcement.
And how cultures vary from department to department.
If you don't know what happened,
a news article went out detailing the sentences
that were handed down to those who killed Ahmaud Arbery, life sentences.
Two of them without parole.
There was a comment under the article on social media, and
that comment says, that criminal Aubrey still got the death penalty though.
The comment was made by a Houston County Sheriff's deputy.
The comment got deleted, but screenshots had already been taken and
they were sent to the department.
Now the sheriff, I'm willing to give him the benefit of the doubt on some of this.
Simply because of the speed, it went from he said what to he's fired.
However, there's some things that that sheriff really needs to understand.
Because in the write up, he's like this is unbecoming,
the way it reflects on the department, so on and so forth.
The general tone is that the sheriff does not believe that this action,
this statement is representative of the department.
It is, it is.
I'm willing to give the sheriff the benefit of the doubt and
say that he honestly doesn't believe it is, but he's wrong.
This is an extreme statement.
This is a statement that's kind of co-signing, extra judicial,
extra legal execution.
There is no department in the country where this thought process
should even be floated, but this was a statement that was made publicly.
One he thought people were going to see, he knew people were going to see this, and
he thought it would give him credibility.
He thought it would give him that social credibility.
He thought other deputies in that department would co-sign it with him.
Whether or not they did so publicly,
he believed that this statement was in line with the culture of that department.
That's a real bad sign for the department.
Because the sheriff made the right call,
I do give him the benefit of the doubt.
But the reality is, this needs to be the sign for the sheriff.
He needs to go through that department.
He needs to retrain.
He needs to fire.
He needs to reform.
Because if nothing is done, this is still the culture.
These statements, they're still in line.
And if the statements are in line, the actions are, or they're not far behind.
If the sheriff doesn't do anything, there will be more problems.
And by more problems, I mean there will be more people lost.
And that sheriff will look back at this moment and say, man,
could I have done something to avoid this?
And the answer is yes.
You need to go through that department with a fine tooth comb,
because this is the culture in that department.
When people tell you who they are, you need to believe them.
And the rank and file deputies just told you who they are.
This statement was made publicly.
This deputy didn't see anything wrong with saying this publicly.
I assure you, there are other deputies who feel the exact same way.
And if they're willing to co-sign a statement like that,
it was made publicly, imagine what they will do when those body cameras are turned off.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}