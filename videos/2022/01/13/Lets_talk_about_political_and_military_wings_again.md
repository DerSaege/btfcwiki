---
title: Let's talk about political and military wings again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8cjLPgFxfzo) |
| Published | 2022/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing concerns and questions about discussing wings and structures.
- Clarifying that the information shared is basic and not secretive.
- Mentioning examples from popular culture where similar structures are depicted.
- Noting a lack of successful right-wing groups adopting similar structures.
- Explaining the ideological differences between left and right-wing groups.
- Describing the challenges right-wing groups face in adopting decentralized structures.
- Pointing out the failure of right-wing groups in using certain strategies in the United States.
- Suggesting a focus on legal maneuvers rather than traditional tactics for right-wing groups.
- Emphasizing the importance of understanding the dynamics of different ideological groups.
- Concluding with a thought about potential future strategies for certain groups.

### Quotes

- "All of them have something in common though. What is it? They're all left wing."
- "It's really hard to operate under a strategy that requires a lot of autonomy when your entire ideology is about hierarchy and not having autonomy."
- "I think in the future what you would really need to be looking for is more of a series of legal maneuvers to affect a self-coup."

### Oneliner

Beau clarifies basic information on wings, contrasts left and right-wing groups, and suggests legal maneuvers over traditional strategies for certain groups.

### Audience

Activists, analysts, researchers

### On-the-ground actions from transcript

- Analyze and understand the dynamics of different ideological groups (implied)

### Whats missing in summary

Beau's detailed analysis and insights on the challenges faced by right-wing groups in adopting certain organizational structures and strategies.

### Tags

#Wings #Structures #Ideology #LeftWing #RightWing


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk a little bit more about wings
and structures like that, because after that last video,
there were people who had some concerns
and some questions came in, basically wondering
if I felt that talking about it was giving them tips
and would make them better at doing it in the future.
And the answer to that is definitely not.
And there are a couple of reasons for that.
First, the stuff that I went over, it's basic.
It's basic.
It's not even like a 101 course.
That's the introduction to the introduction.
None of that is secretive.
It's in the manual.
It's not high speed stuff.
You could pick up most of that by binge watching a couple of
seasons of the Americans. The political wing would be the people at the embassy.
The handler, well that's the cutout. And the militant wing would be the
protagonists. They actually use Dead Drops a lot in the series and get into
stuff that is way more advanced than what I talked about. This isn't secretive
stuff. Most of this you can pick up in a Tom Clancy novel to be honest. And then
there's the the real reason I'm not concerned about it. If you go back to
that video and you look in the comments, there's a whole bunch of people that are
like, hey this group did that. They used that structure. They had a political wing
and a militant wing. And there's probably a dozen examples in the comment section.
And everyone that I saw, yes, they definitely adopted that structure.
All of them have something in common though.
What is it?
They're all left wing.
All of them.
Name a right wing group that successfully adopted this structure.
And if you don't know anything about this topic, just know that right now at home, a
A whole bunch of people who know a lot about that topic are sitting there going,
because it's really hard to find right-wing groups that were successful at using this.
There's actually like academic debate over why right-wing groups are so horrible at this.
My theory is that it has to do with the ideology themselves.
It has to do with the ideology itself.
In the United States, we focus on left and right when we talk about stuff, but there's
another axis.
You have the left-right axis, but you also have the up and down, the vertical axis, and
at the top is authoritarian, at the bottom is anti-authoritarian.
If you're in Europe, libertarian, but in the United States, don't say that, or it's just
going to open up this whole other topic.
That word doesn't mean the same thing here.
When you're talking about groups that would be seeking to change the system of governance,
these are groups that are on the fringes.
These are extreme groups, typically groups that are far left are also down.
They're anti-authoritarian.
They believe in egalitarian ideologies, things where everybody's equal, everybody has autonomy.
It lends itself well to having a structure that is decentralized.
Right-wing groups that are extreme are the polar opposite.
They move up.
They rely heavily on hierarchy, and that disrupts the ability to adopt any decentralized structure.
It's really hard to operate under a strategy that requires a lot of autonomy when your
entire ideology is about hierarchy and not having autonomy.
The way right-wing groups typically try to compensate for this is just by sealing themselves
off, but then they don't have a support network, so they tend to fail.
It would be super beneficial, in my opinion, if in the future they continued to try to
use this strategy because almost assuredly they would mess it up.
It just doesn't lend itself well to right-wing groups.
That structure doesn't work for them.
I can assure you that at some point in the last year, there was some guy sitting in the
headquarters looking at photos of members of one of these groups and he's
sitting there looking and he's like hey these people have rank thanks for the
organization chart this is going to be easy it just it doesn't work for them
and then I think the reason they tried to use this strategy is because the
tactics and strategy and organizational structure that typically works for right-wing groups
really can't be used very well in the United States at this time because it's very clear
what it is.
When they use the strategies that tend to work for right-wing groups, it's marching
in the street and it looks exactly like what it is.
In the United States, people don't like that.
I would point out that the reliance on command and hierarchy and authority kind of cast doubt
their battle cry of freedom.
I wouldn't worry about the strategies that are used in this sense.
I think in the future what you would really need to be looking for is more of a series
of legal maneuvers to affect a self-coup.
I don't know that they're going to rely on shock troops again.
It's probably going to be more in the halls of courthouses
than out in the streets.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}