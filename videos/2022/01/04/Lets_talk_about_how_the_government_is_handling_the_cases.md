---
title: Let's talk about how the government is handling the cases....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=urTSUSVTMpA) |
| Published | 2022/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explores the moral and pragmatic aspects of the government's response on the day of January 6th and the response today.
- Considers the debate around seeking capital punishment for the individuals involved in the incident.
- Shares his personal stance on capital punishment, leaning towards the preservation of life and against revenge.
- Points out that many defendants had cognitive issues and were manipulated into participating in the events.
- Believes that capital punishment is not a deterrent and questions its effectiveness in addressing the situation.
- Warns against creating martyrs by employing extreme measures, citing historical examples.
- Argues that seeking extreme sentencing could embolden the perpetrators and fuel further resistance.
- Mentions the disparity in sentencing between the January 6th cases and other criminal cases.
- Advocates for reducing sentences rather than increasing penalties for everyone.
- Expresses gratitude for the government's restrained response on January 6th, believing an all-out reaction could have escalated the situation.

### Quotes

- "I understand the anger and I understand that initial urge to say, let me tell you how you fix this. But if you think about it for any length of time, it's not really a fix."
- "Generally speaking, I am for the preservation of life."
- "It's a bad idea to seek that extreme level of sentencing for what happened that day."

### Oneliner

Beau navigates the moral dilemmas and pragmatic consequences of the government's response to January 6th, advocating against extreme measures that could backfire.

### Audience

Activists, policymakers, citizens

### On-the-ground actions from transcript

- Advocate for fair and just sentencing in cases related to the January 6th events (suggested)
- Support initiatives that aim to address cognitive issues and prevent manipulation in vulnerable individuals (exemplified)

### Whats missing in summary

The full transcript provides a nuanced perspective on the moral and pragmatic considerations surrounding the government's response to the January 6th events, offering insights into the potential ramifications of different approaches.

### Tags

#GovernmentResponse #CapitalPunishment #MoralDilemma #PragmaticConsequences #FairSentencing


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we're going to talk about the moral
and the pragmatic aspects of something.
We're going to talk about whether the government's
response was right the day of the 6th,
and whether or not the response today is right.
Because shortly after that last video about the 6th,
I had people start asking, would it have been better if the government had just taken the
gloves off that day, left a bunch of people laying in the hallway, wouldn't that have
sent a better message?
Then following that up, quite a few people saying, well, should we be seeking capital
punishment in this case?
When you're talking about something like this, you have the moral aspects, generally,
the moral aspects specific to that particular incident, and then you have the pragmatic
aspects.
Generally speaking, I don't support capital punishment.
There are incredibly few exceptions.
I was still raised where I was raised at the time I was raised there.
So yeah, there are moments when I read a newspaper article and my initial reaction is, yeah,
you know what the solution to this is, but I know morally that's not the right response.
That generally speaking, the government shouldn't be in the business of revenge, right?
And then you have the moral aspects specific to this case.
If you look into a lot of these defendants that have diagnosed psychological or cognitive
issues, most of them were manipulated into being there.
You can say, yeah, well, some of the manipulators were in the crowd, and sure, but I sure wouldn't
want to be the person having to decide who was who.
I don't believe that capital punishment is a deterrent really.
I don't think that that matters in that sense.
I don't think that factors into the argument.
I think that a lot of these people were manipulated, and sure, some of them had truly evil intent.
They had bad designs.
They wanted to do something really bad.
I don't know that the response to that, I don't know that how you stop that is by doing
something bad yourself, rather, either way, whether at the time or afterward.
Generally speaking, I am for the preservation of life.
But all that, all that's moral.
That's my opinion.
That's the way I look at the world.
What about the pragmatic?
Because there are some who will say, well, you know what?
They started this.
We're just going to finish it.
So you have to look at it from the pragmatic side and take all of that out of it, right?
Generally speaking, when you are talking about movements like this, it's a super bad idea
to give them martyrs.
It's a really bad idea.
And you can look to pretty much anywhere in history, in just about any country.
If the government had taken the gloves off that day, what would they be out there saying?
It's the Boston Massacre all over again.
They would use it to inflame.
Right now, because they're going to trial and then going to sentencing, we get to hear
them say that they were manipulated, that they regret it, that they feel duped by Trump,
that they were lied to.
We get to hear them say that.
That's on the record.
If they're not here to make a statement, believe me, people are going to put words in their
mouths.
And then as far as seeking that kind of punishment after the fact, I think the best example that
Americans might be familiar with and know the outcome, or at least a little bit of the
outcome, is Easter 1916 in Ireland.
There was a movement for independence in Ireland, and they occupied a building, a little bit
more force was used, and they were eventually driven from that building, and then the British
to send a message, put a whole bunch of them on the wall, took them out.
It did not, it did not quell that movement.
To the contrary, it created a widespread spirit of resistance.
Songs were written about the people shot that day.
They're still played pragmatically.
It's a bad idea to seek that extreme level of sentencing for what happened that day.
It will embolden them.
It will give them something to rally around.
Right now, they get to sit there and say, oh, it's a comical failure.
That wasn't even real.
Don't even call it that.
And maybe they're less likely to participate in the next comical failure.
But if those people, those are patriots who paid the ultimate price, that's an entirely
different thing.
I don't think it's a good idea, pragmatically speaking.
So I don't agree with it morally, and I know it's not a good idea from the pragmatic aspect.
Now you have people who are also talking about how light some of the sentencing is.
Right after they started arresting people, I went through how federal prosecutions work.
All the beginning ones are little fish, and it works its way up.
That's what's happening, if you notice the sentences are getting longer.
So far, I mean, they've been more or less, you know, there have been a lot of them.
But overall, they've seemed pretty appropriate to me.
You know, you're talking about those people who just went inside, they're basically getting
a slap on the wrist, don't do this again, can't believe you did something like this,
bad.
Those people who hurt people, their sentences are in the ears.
that makes sense to me. And there is a disparity between this sentencing and
what happens to normal people in normal situations. And yeah, that disparity is real.
And it definitely needs to be noticed and acknowledged. But to me, I don't think
And the normal sentencing is appropriate.
I don't think locking somebody up for five years because they had a plant they shouldn't
have, I don't think that's appropriate.
So to me, the solution is to bring their sentence down, not increase everybody else's.
But on topic, I'm pretty glad that the gloves didn't come off that day, because it would
have altered the entire dynamic.
Not just for those there, but for everybody else dealing with the fallout, because the
political talking points would be drastically different.
And it also could have spiraled out of control and prompted Trump to use it as an excuse
to retain power.
Look at the violence that occurred this day.
There is no good way for it to finish if it starts like that.
The government that day was incredibly restrained.
Me, I'm glad they were.
I don't believe that an all-out response was the right move.
I understand why it was there, I'm glad that it was held in reserve.
But I'm glad it was held and not used.
And then as far as after the fact, pragmatically that's a really bad idea.
Setting moral considerations aside, that's just not good.
Nothing good can come of that for anybody.
I understand the anger and I understand that initial urge to say, let me tell you how you
fix this.
But if you think about it for any length of time, it's not really a fix.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}