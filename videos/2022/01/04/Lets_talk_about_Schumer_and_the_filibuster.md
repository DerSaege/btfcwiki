---
title: Let's talk about Schumer and the filibuster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sj2fvictjxU) |
| Published | 2022/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the possible changes to the filibuster being discussed by Schumer due to Republicans not going along with voting rights legislation.
- Notes that the filibuster is a Senate rule, not part of the Constitution, originally intended for ample debate time.
- Schumer is open to changes in the filibuster but not getting rid of it entirely.
- Two options are on the table: creating an exemption for voting rights legislation or implementing a talking filibuster where senators have to actively speak.
- Democrats are hesitant to eliminate the filibuster as they see it as a tool beneficial when not in power.
- Concerns that if exemptions are made, Republicans will do the same for their priorities, ultimately leading to removing the filibuster altogether.
- Beau suggests a talking filibuster with strict rules on staying on-topic about legislation and penalizing falsehoods with an immediate end to the filibuster.
- Changing the filibuster sets a precedent for future alterations by whichever party holds power.
- Points out that the fear of providing Republicans with a tool disappeared once public discourse on changing the filibuster began.
- Beau argues for pursuing the talking filibuster with specific rules since it allows for potential filibustering in critical situations while removing unnecessary roadblocks.

### Quotes

- "It's about power. It's about their party. It's not about the country."
- "Changing the filibuster in and of itself sets the precedent that Republicans can change it next time they're in power."
- "The exemptions, sure, it's easier for this issue, but make no mistake about it, they will carve out exemptions for whatever issue they want next."
- "At this point in history, the filibuster isn't about debate, because no debate is taking place."
- "It's a talking filibuster, no lies, and it has to be about the legislation."

### Oneliner

Schumer considers changes to the filibuster, facing dilemmas on safeguarding voting rights while avoiding creating tools for opponents to exploit.

### Audience

Legislators, political activists

### On-the-ground actions from transcript

- Advocate for a talking filibuster with strict rules to ensure transparency and accountability (suggested).
- Push for changes in the filibuster system that prioritize legislative debate over partisan power struggles (suggested).
- Engage in public discourse and advocacy to support measures that safeguard voting rights while maintaining Senate functionality (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the current filibuster system and potential changes, discussing the implications for future political power dynamics and the importance of transparent legislative processes.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about possible changes to the filibuster
because Schumer has indicated that they're very open to it now.
So quick recap, the filibuster is not part of the Constitution.
It's just a rule that the Senate has had.
The stated reason for the filibuster is to provide ample time for debate.
That's really why it is supposed to exist.
That's not really how it gets used, though.
What it has turned into is basically requiring an additional ten votes to get anything passed.
That's really what it is boiled down to.
Now Schumer has indicated that if Republicans aren't going to go along with voting rights
legislation, which they won't, he's open to changing it.
Now they're not talking about getting rid of it.
That's something that's important to note.
This isn't going to do away with the filibuster.
The reason the Democratic Party is reluctant to do that is because they know that it benefits
them when they aren't the party in power.
So they don't want to get rid of the filibuster.
What Schumer is talking about is creating changes that can kind of result one of two
ways.
One is to carve out a specific exemption for voting rights legislation, allowing that to
be passed by simple majority.
The other is to establish a talking filibuster, whereas right now they can just say, well,
we filibuster this.
We're not going to allow the vote to proceed.
If they want to do it under a talking filibuster, they would have to get up there and talk.
Those are the two options that are being entertained.
Something that's important to note is that Republicans are not afraid of exercising power
the way Democrats are.
If Democrats carve out an exemption for voting rights legislation, Republicans will carve
out exemptions for anything that they want.
It's that simple.
The whole purpose or the whole reason that Democrats are reluctant to do this is because
they don't want to hand Republicans a tool and say, well, you did it, so we're going
to do it too.
If you are going to provide exemptions, just get rid of it.
Just get rid of it, because that's going to be the end result.
Get rid of it and ram through everything you can right now.
That's how to exercise power.
You get rid of it, you ram through everything that you can, and hope that the impacts from
that legislation are enough to swing votes and keep you in power, because that's what
it's about.
As far as the talking filibuster, to me that's a much better idea, as long as there are some
rules that go along with it.
First, is that what's being discussed has to actually be about the legislation.
Don't just hand Republicans a forum to create sound bites for themselves, so they can get
up there and say, oh, this bill that allows early voting, that's just Marxist Socialist
CRTism, because their base isn't really concerned about substance.
They'll buy it.
It'll give them points.
So it has to be about the legislation.
I would also suggest that any falsehood uttered by any member of any party ends the filibuster
for that party.
So if somebody gets up there and lies, that's the end of the filibuster.
Don't just hand them a tool to get themselves on Fox News.
They will use it for that.
There won't be any incentive to avoid the filibuster, because they'll get up there and
talk about whatever it is they have manufactured as the outrage of the day.
It's got to be on topic about the legislation and no lies.
Anything that is objectively false ends the filibuster immediately.
Now again, the reason they've been reluctant to do this is because they don't want to hand
Republicans a tool when they take power again.
Changing the filibuster in and of itself sets the precedent that Republicans can change
it next time they're in power.
But what I would also point out is that at this point, there have been enough people
posture and talk about changing the filibuster, they already have the sound bites in the footage
for the attack ads.
They've already got the sound bites to say, well, you were going to do it, so we're going
to do it now.
That reason to avoid changing the filibuster, that disappeared the second everybody started
talking about it publicly.
The sound bites already exist to prove that if you oppose them changing it, well, you're
a hypocrite.
You're in this situation.
Democrats are in this situation.
They need to just pursue it.
To me, the talking filibuster with those rules makes the most sense.
Because if there is something that comes along later, you can still attempt a filibuster
of it.
If there is something that is truly damaging to the country, you still have that tool available.
The exemptions, sure, it's easier for this issue, but make no mistake about it, they
will carve out exemptions for whatever issue they want next.
That's not a safeguard against them further altering the filibuster.
It just gives them more of a reason to do it.
At this point in history, the filibuster isn't about debate, because no debate is taking
place.
It's not about good faith discussion and deciding what's best for the country, because everything
is so partisan and polarized.
Nobody is discussing about the country.
That's not what the debates are about.
It's about power.
It's about their party.
It's not about the country.
It's a talking filibuster, no lies, and it has to be about the legislation.
Get rid of the roadblocks.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}