---
title: Let's talk about Yellowstone and wolves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fEK2mrsuOMY) |
| Published | 2022/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Yellowstone wolves faced threats after the federal government delisted gray wolves, shifting protection responsibilities to states.
- Yellowstone lost 20 wolves recently, impacting a pack significantly.
- States can now hunt, trap, and bait wolves outside of federal land, potentially leading to increased threats.
- Losing 20 wolves in a year is the highest since reintroduction efforts began.
- Montana's governor, one of the major offenders, failed to follow regulations and shot a radio-collared wolf near the park.
- Beau urges for wolves to be relisted under federal protection due to states' inadequate safeguarding efforts.
- States show little interest in protecting wolves, necessitating federal intervention for conservation.

### Quotes

- "Losing 20 wolves in a single year, that's the most that has happened since reintroduction efforts began."
- "It's time for the feds to relist the wolf."

### Oneliner

Yellowstone wolves face increased threats as states fail in their conservation duties, urging for federal intervention to protect the species.

### Audience

Conservationists, Wildlife Advocates

### On-the-ground actions from transcript
- Contact local representatives to advocate for relisting wolves under federal protection (exemplified)
- Support organizations working towards wolf conservation efforts (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of the challenges faced by Yellowstone wolves post-delisting and the urgent need for federal intervention to ensure their protection and conservation.

### Tags

#Yellowstone #Wolves #Conservation #FederalProtection #StateResponsibility


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about Yellowstone,
the park, not the TV show, and wolves. Provide a little update on a story we have been following
on this channel for quite some time. To catch everybody up, not too long ago the federal
government decided it was time to delist the wolf, to delist the gray wolf, and it turned
out to be the state's responsibility for protecting this animal back over to the states.
At the time I expressed my concerns with this decision. The fight to bring this animal back
from the brink of extinction has been a long one, and to my way of thinking when this decision was
made, we were right there. We were right there. Right at the point where just five years more,
and this would have been one of the great American conservation success stories. However,
if the feds turned it back over to the states and the states didn't do their part, it could set us
back decades. Over the weekend, news broke that 20 of Yellowstone's wolves had been taken out.
The park expressed concerns about this happening back in September,
concerns that apparently nobody cared about.
Taking out 20 leaves 94 at the park, it does look like it completely eliminated or at least
disrupted one of the packs in its entirety. It's a big deal. It is a big deal. Now the wolves are
protected on Yellowstone, but because the states have the responsibility for protecting it outside
of federal land now, just outside of Yellowstone, well, they can hunt and trap, and most concerning,
they can bait. Baiting is something that could be used to lure a wolf off the park, away from where
it's protected, because it's worth pointing out, I guess, that the wolves are in fact wolves.
They don't have maps. They don't have GPS. They don't know when they leave the park
and quite literally become fair game.
Losing 20 wolves in a single year, that's the most that has happened since reintroduction efforts
began. It's a big deal. The wolves at Yellowstone are probably the most monitored wolves in the
United States. They're the most tracked wolves in the United States. If this is what we're
seeing in the United States, if this is what is happening to them, you can take an educated guess
as to what's happening to wolves that have less surveillance aimed at them. The responsibility
for protecting these animals now falls on the states, which means it falls on the governors.
It's worth noting that one of the biggest offenders here is the state of Montana.
The governor of Montana is reported to have received a warning from a game warden,
because the governor hadn't taken the mandatory courses and had trapped and shot a radio collared
wolf less than 10 miles from the park. This is the person who has the duty to protect the state
to protect the animals. You'll forgive me for not believing that they're up to the task,
that perhaps they're unwilling, perhaps they're putting some fantasy image of being a mountain
man or the economic concerns above the species. At the end of the day, this is really easy.
They tried it. The feds tried it. It's time to relist the wolf. It's that simple. This isn't
going to work. The longer states have control and have the responsibility to protect these animals,
the further back we are going to be set, the more damage is going to be done. It is clear
at this point the states have zero interest in actually protecting the animal. None.
It's time for the feds to relist the wolf. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}