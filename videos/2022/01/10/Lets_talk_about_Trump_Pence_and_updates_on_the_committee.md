---
title: Let's talk about Trump, Pence, and updates on the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0uCjz8bV_b8) |
| Published | 2022/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Update on committee activities investigating the events of the 6th.
- Confirmation of suspected details regarding the events.
- Americans likely not shocked by findings, with most understanding what occurred on that day.
- Description of the events on the 6th as a self-coup.
- Details of Trump pressuring Pence to overturn the election.
- Pence's refusal to comply with Trump's demands.
- Former press secretary Grisham's cooperation with the committee and providing valuable information.
- Committee engaging with lower-level individuals who are cooperating.
- Subpoena of the pillow salesman's phone records by the committee.
- Public questioning of the Department of Justice's actions in response to the committee's investigations.
- Positive note on how the investigation is being conducted without leaks or a play-by-play.
- Interest in Vice President Pence as a witness and potential cooperation.
- Acknowledgment of luck and individuals' moral compasses in preventing certain outcomes.
- Reflection on the importance of individuals standing their ground in critical positions.

### Quotes

- "You don't understand, Mike. You can do this. I don't want to be your friend anymore if you don't do this."
- "They tend to not notice the help. They tend to not watch what they say in front of them."
- "A lot of this was luck."

### Oneliner

Beau provides insights into the committee's investigations into the events of the 6th, detailing pressure on Pence and the importance of individuals maintaining their moral compass.

### Audience

Committee members and concerned citizens.

### On-the-ground actions from transcript

- Contact your representatives to express support for thorough investigations (suggested).
- Stay informed about the developments surrounding the events of the 6th (implied).

### Whats missing in summary

Insights on potential future revelations and implications from ongoing investigations.

### Tags

#CommitteeInvestigations #TrumpAdministration #Pence #JusticeDepartment #MoralCompass


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to kind of provide an update
on the activities of the committee
that are looking into the events of the 6th.
Just kind of piece some stuff together.
Some of the stuff that we suspected has now been confirmed
and we're just gonna kind of lay it all out
and see where they're at and what's going on.
Representative Jamie Raskin said that Americans
would be shocked and surprised to find out
what the committee has learned.
I don't know that I believe that.
I don't know that that's true.
I think most Americans know what they saw that day.
Most Americans have a pretty good understanding
of what they witnessed.
And even if they're not 100% certain,
it has certainly entered their minds.
I do not know that they will be shocked or surprised
to find out what was behind the event
that Raskin himself described as
the unleashing of mob violence
to intimidate the vice president and Congress,
overwhelm and stop the counting of votes,
and provide a pretext and context
for Trump to potentially intervene with military force
under the Insurrection Act to put down the uprising
he himself had helped to organize.
For those at home,
that's a really good description of a self-coup.
We also have the reporting from Woodward,
which talks about a conversation that took place
between Trump and Pence.
And Trump is really pressing him,
trying to get him to overturn the election,
trying to get him to agree to be part of the team here.
And Pence just isn't having it.
By the reporting, Pence is like,
yeah, this isn't happening.
You need to figure out how to talk about losing it.
I'm just there to open envelopes.
I'm paraphrasing, of course.
In response to that, Trump,
and I'm not paraphrasing this,
Trump replied with, no, no, no.
You don't understand, Mike.
You can do this.
I don't want to be your friend anymore
if you don't do this.
The former president of the United States,
the president at the time, whining like a toddler,
trying to get his way.
Pence didn't budge.
Pence maintained his position.
It has also been confirmed that Grisham,
the press secretary, the former press secretary for Trump
that we talked about last week, she did.
She walked into the committee hearing and was like,
yeah, you need to talk to this person,
this person, this person, and this person.
You need to talk to this person about this thing.
Gave them all kinds of names
and gave them lines of inquiry.
This is the former press secretary
who indicated that former Trump officials
are currently working on some kind of maneuver
to ensure that former president Trump
never gets his hands on the levers of power again.
It does seem like those statements
that she made were genuine.
They were backed up by what she has told the committee.
We also found out that the committee was,
in fact, talking to lower-echelon people.
I think they described them as second-
and third-tier or something like that.
This is going to be incredibly damaging to the major players.
People who are powerful and who believe
they can get away with something like this,
they're incredibly arrogant.
They tend to not notice the help.
They tend to not watch what they say in front of them.
Those who are lower-level,
they appear to have, they're cooperating with the committee.
They're telling them what they know,
and these are people who were asked to do
functionary things in furtherance of the plan
that was apparently in place.
We also found out that the committee subpoenaed
the pillow salesman's phone records.
At least that's the claim.
And this is something that kind of leads into something else.
While that in and of itself isn't surprising,
it adds to another conversation that's going on.
There are a lot of people who are kind of looking at DOJ like,
what are y'all doing? What are you waiting for?
Right now, the committee provides a lot of cover for DOJ.
They can get records. They can do all sorts of things
without raising the alarm
and without causing people to lawyer up,
because at the moment, it's a political thing.
And a lot of the people involved are, well, they are arrogant,
and they may believe that this is something
that they can just talk their way out of
in front of the committee.
If it stays that way for as long as possible,
it gives DOJ a lot of leeway.
It's a possibility that that's what's going on.
We don't know that, though.
It's also worth noting that in an investigation like this,
what's going on is how it should look.
We're just not used to seeing that.
We're not accustomed to seeing investigations
run the way they should at this level.
There's not supposed to be leaks.
You're not supposed to find out everything that's going on.
There shouldn't be a play-by-play.
It appears that everything is going according
to the way it should, which is surprising.
Actually, it makes me a little uneasy, to be honest.
And the committee has definitely expressed interest
in talking to Vice President Pence,
talking to Pence directly.
And it appears that the committee believes
he's going to be a pretty cooperative
and favorable witness.
Raskin said that he has a lot to be proud of
and that he was a constitutional patriot.
He stood up for the Constitution.
Talking about Pence.
And I think, honestly, my gut tells me
that there is more to that statement
than just the quotes we have from Woodward.
I don't think it's just Pence telling him no that night.
I think there might have been more behind that quote.
Yeah, sure, just doing what the Constitution
kind of outlines the vice president to do on that day.
Yeah, but that should be what's expected.
You know, I don't know that that alone would rise
to calling somebody a constitutional patriot
and saying they have a lot to be proud of.
That's kind of, it should be, the expected bare minimum,
even with the amount of pressure that he was under.
I think there's more to that.
And I, for one, cannot wait to hear from Pence.
As we get more and more information,
it doesn't actually appear that the systems
that are designed to thwart this type of behavior
from somebody in the executive branch
is really what stopped it.
It seems that we got lucky.
And in certain positions, we had people who were inflexible,
people who were going to stand their ground.
And they were in positions that could alter the outcome
of perhaps using the military
or refuse to provide political cover to a maneuver
that is not in the spirit or the letter of the Constitution.
I think it's important to acknowledge
that a lot of this was luck.
A lot of this was luck.
It wasn't that the systems themselves defeated this attempt.
There were individuals in the right spot
who maintained their moral compass.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}