---
title: Let's talk about Trump's ex-press secretary planning action....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KHvdM7qGyMs) |
| Published | 2022/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Stephanie Grisham, former Trump press secretary, mentioned a meeting with other former Trump officials to stop Trump from running again.
- Grisham had openly cooperated with the January 6th committee.
- The group of former Trump officials may be trying to establish formal opposition to Trump returning to power.
- It's unclear what "formal" opposition means—could be political action committees or providing information to prosecutors.
- Grisham's actions post-Trump administration have been limited to putting out a book.
- There is speculation that the meeting could be a way to garner attention for Grisham's book.
- Some former officials are serious about ensuring Trump doesn't return to power.
- The January 6th committee has been interviewing lower-level staff, potentially leading to damaging revelations.
- The committee is focusing on individuals who were present, overheard things, and were asked to do minor tasks.
- The information gathered by lower-level staff could be more damaging than anticipated.

### Quotes

- "A woman named Stephanie Grisham is the former Trump press secretary."
- "My guess is that it's going to be some formal political thing."
- "Those who worked for the Trump administration are regretting that decision."
- "The people in the room, they know what happened."
- "It's something that I can see turning into big news."

### Oneliner

Former Trump officials, including Grisham, plan formal opposition to prevent Trump's return, while lower-level staff testimonies may lead to significant revelations.

### Audience

Political Observers

### On-the-ground actions from transcript

- Keep a close watch on the developments regarding former Trump officials' plans (implied).
- Stay informed about the testimonies of lower-level staff members to the January 6th committee (implied).

### Whats missing in summary

Insights on the potential repercussions of the actions taken by former Trump officials and the testimonies of lower-level staff members.

### Tags

#StephanieGrisham #FormerTrumpOfficials #January6Committee #PoliticalAction #Revelations


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about some news
that might get overshadowed.
With everything going on with the talk
of what happened last year,
an announcement, an offhand comment
may not get the coverage that I think it should.
So we're gonna talk about that.
A woman named Stephanie Grisham
is the former Trump press secretary.
Now, she's not the one that you're probably familiar with
because she never gave a press briefing.
And for the most part,
she hasn't been wrapped up in everything.
She did put out a book called,
I wanna say it's something like,
I'll take your questions now,
which I find funny because she didn't take questions then.
But in an interview,
she kind of offhandedly said,
hey, yeah, next week,
myself and about a dozen other former Trump officials
are meeting to find a way to stop Trump.
A little bit of prodding.
And they found out that some of the officials
are senior to her, some are junior to her.
So you have a group of former Trump officials
who are trying to establish some form of formal,
that word was there,
opposition to Trump running again
or to Trump getting his hands on the levers of power again.
Formal is interesting because it could mean a lot of things.
It could mean a political action committee.
It could mean going to prosecutors
and telling them what they know.
But from what I understand,
Grisham has already openly cooperated
with the January 6th committee.
So I'm curious to find out what it is they're planning.
Now, it may be nothing because she hasn't done a whole lot
to move forward from her working for Trump,
other than putting out a book,
which I mean, they're all doing that.
But there hasn't been a lot of active opposition
up until now.
She said things, but we haven't really seen her get involved.
So this could just be a way to promote her book,
get people to say it like I did.
Or she could be serious
because there are definitely more than a dozen
former Trump officials who would like to make sure
he never gets his hands on the levers of power again.
So this is something we need to watch for.
In about a week, we have to find out what actually happened,
what their plan is, how they're gonna move forward.
My guess is that it's going to be
some formal political thing.
But it's literally just a guess,
and that's entirely based on the fact
that she's a press secretary.
It could be something more than that,
but it's definitely something that we need to watch.
It is worth noting that as time goes on,
those who worked for the Trump administration
are regretting that decision.
It is also worth noting that,
as suggested in a previous video,
the January 6th committee has been interviewing
what they describe as second and third tier,
lower-level staff, people who were present,
who had information, who were asked to do minor things,
who may have been in the room and overheard stuff.
This was something I suspected,
but it has now been confirmed.
That is going to be far more damaging
than I think the major figures realize.
The people in the room, they know what happened.
So this is one of those things,
not really big news yet,
but it's something that I can see turning into big news.
So just something to be aware of.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}