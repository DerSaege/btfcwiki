---
title: Let's talk about whether it was really a coup attempt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q6zjigTxfQ4) |
| Published | 2022/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing whether a specific term is appropriate for a certain event that took place at the Capitol, leading to ongoing debates even a year later.
- Describing different types of coups and suggesting that the event might have been an attempted self-coup rather than a dissident coup.
- Mentioning the Klein Center for Advanced Social Research's classification of the event as an attempted dissident coup, which Beau disagrees with.
- Emphasizing that there is no debate within political violence study circles about whether the event was a coup attempt; the debate revolves around what type of coup it was.
- Criticizing the lack of understanding and misperceptions about political violence, attributing it to reliance on TV and movies for information.
- Arguing that advocating for civil conflict without a proper understanding of political violence is irresponsible and dangerous.
- Expressing concern over the impact of misleading rhetoric on the country's stability and urging people to seek education beyond fictional portrayals of violence.

### Quotes

- "There's not actually any debate in the circles that study political violence over whether or not this was a coup attempt."
- "If you understood anything about the topic, you'd know that."
- "You have no business advocating for something like that if you don't know what happened on the 6th."
- "Because people refuse to actually learn what this [political violence] would be like."
- "It's not Red Dawn."

### Oneliner

Beau analyzes the Capitol event, clarifies coup types, and criticizes misperceptions of political violence fueled by fictional portrayals, urging for informed discourse and rejecting advocacy based on ignorance.

### Audience

Educators, Activists, Researchers

### On-the-ground actions from transcript

- Research different types of coups and political violence to understand the nuances and implications (suggested).
- Engage in critical media literacy to differentiate between fictional representations and reality in political events (implied).
- Advocate for accurate education about political violence and discourage misinformation spread through popular media (exemplified).

### Whats missing in summary

The full transcript provides a detailed breakdown of different coup classifications, challenges misperceptions of political violence influenced by media, and calls for informed discourse to prevent advocating for civil conflict based on fictional portrayals. Viewing the full transcript offers a comprehensive understanding of these critical issues.

### Tags

#Coup #PoliticalViolence #Education #Misperceptions #Advocacy


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk
about whether or not a certain term is appropriate. Whether or not that's really
what it was or perhaps it was something else. Maybe there's a better term that
fits because after a year there is still discussion over this. People are still
saying, well, it really wasn't that. It was something else. It wasn't quite that big
of a deal. A year ago when everything started happening at the Capitol, my
first tweet on the topic was, you're witnessing an attempted coup. To this day,
a year later, I still get tags from people. Is that really what it was? You
know, it really wasn't that violent. I suggest you google palace coup or forced
resignation coup. They don't have to be violent at all. There weren't tanks on
the streets. Yeah, that's a military coup. That's a military coup. And that form of
it is becoming more and more rare. It's becoming so rare, in fact, that you can go
to foreign policy, the outlet foreign policy, and they have an article that I
think is titled coups ain't what they used to be. And the lead-in is something
to the effect of, if you want to seize control of a state, you don't have to put
tanks on the streets anymore. That's a military coup. And that's where people
get their image because that's what gets used in movies and on TV. A military coup.
There are a whole bunch of different types of coup. Not all of them get
depicted in blockbusters. The ultimate funny ha ha part about it is that a
whole lot of military coups aren't really technically military coups. They're
veto coups or they are counter coups. There's a lot to this. There's not just
one type. That would be silly. When you really get into it, if you are studying
it in depth, you will find out that there are dozens of different
classifications that could be used to describe a coup. It would be super cool
if there was some outfit whose whole job was to, I don't know, catalog coups
and kind of put it into common language and make it accessible to the general
public so you didn't have to spend years studying it. That would be a really great
project. If you were going to open something like that, you should probably call it the
Klein Center. The Klein Center for Advanced Social Research and have a coup
d'etat project. It exists. It exists. That's their whole purpose for being for that
project. They have their own list and it's general. It's basic. There are more.
So if I use a term that isn't in this list, that's why. They have military coup,
dissident coup, rebel coup, palace coup, foreign-backed coup, auto coup. That's a
self-coup. Forced resignation, popular revolt, counter coup, and other. They have
other because this is general. It's basic. It's designed to be easily consumable.
Oh, you know what? Since there is an outfit whose whole job is to catalog coups, I
wonder what they say about the sixth. Now, to be honest, I vehemently disagree with
their conclusion. And it's not because they said it wasn't a coup and that
makes me look bad. They totally said it was a coup, but they classified it in a
way that I don't agree with. Specifically, at the time of this writing, we classify
it as an attempted dissident coup. I can't disagree with that more. It was an
attempted self-coup. It was an attempted self-coup or auto coup to use their
terminology. The person in power used extra legal or illegal means to attempt
to render another branch of government too weak to execute a change of
leadership. It was a self-coup. But in the Kline Center's defense, I do want to point out
that part where it says, at the time of writing. The reason that's important is
because they released this on January 27th of last year. January 27th. Pretty
quick, because it was very obvious what it was. They were putting out a formal
official statement, so they had to go off of only what they could prove. Only what
they could absolutely say without debate. I'm not bound by that. This is
pretty informal. I can say early on, no, this is an attempted self-coup,
because we know that figures within the administration were talking about using
extra legal means to stay in power. And then this occurred. All signs point to
a yes sort of thing, right? I can go off of that. And as time went by, yeah, that's how
the evidence shaped up. So while I disagree with it, I understand why it was
phrased that way at the time. And this is the important thing to note about this.
There's not actually any debate in the circles that study political violence
over whether or not this was a coup attempt. That's not a discussion that's being
had. Some people may call it an unrealized coup rather than a coup attempt.
But that's not the topic. The discussion isn't whether or not that's
what it was. It's down to the specifics. What kind of coup it was. And that's just
the nature of academics. If you've ever been around them, you know you can take
three of them and put them at a table and give them a topic, come back an hour
later and get five opinions. Academics love to debate the intricacies of this
type of stuff. But there's no debate over whether or not it was a coup attempt. So
it's a year later and you still have people out there saying that. There
weren't tanks on the street. It wasn't that violent. It had to have been
something else. Didn't look like what I expected it to look like because your
image comes from TV and movies. That's why. That's why. It's a symptom of the
embrace of being against education in this country. There are a whole bunch of
people who can't identify what they saw live on TV. That's a problem. And you're
going to say, well some of them are just saying that because of, you know, it's good for
them politically. And yeah, that's true. Undoubtedly that is true. That's a
statement of fact. But those at the bottom, those who you find on social
media saying it wasn't a coup attempt, that's not them. They just don't know
because they never took the time to learn about it. They have an image that
they got from TV and they think that's reality because they're so divorced from
reality, from actual education, from the desire to learn about a topic. They still
view the dynamics that are so dated. There are already articles talking about
how dated they are. And they view that as the only way it happens when that's not
the case. That's never really been the case. So there's two key takeaways here.
One, there's not really a discussion, a debate, over whether or not it was a coup
attempt. Just what kind. The other thing that is really important to note for a
lot of people, your perception of political violence is so off that you
couldn't identify something that happened in front of your eyes, live, on
TV, after a year. If that's the case, maybe you're getting a lot of your other
perceptions and estimations about political violence from movies and TV as
well. And maybe they are also just as wrong. Perhaps if you cannot identify a
coup that happened in front of your face, you have no business calling for civil
conflict because your perception of that is just as wrong. You are just as off
about that as you are about what happened on the 6th. Because you base it
on movies, on TV, on fiction. Maybe civil conflict is like super bad and nobody
should want it. Nobody should be advocating for it. There's got to be a
better way, right? If you understood anything about the topic, you'd know that.
You have no business advocating for something like that if you don't know
what happened on the 6th. And if you do, if you do understand political violence,
especially that kind, you wouldn't advocate for it because you understand how
horrible it is. It's been a year and the rhetoric of some people still has such a
hold on people in this country that we aren't really that much better off when
it comes to how far away we are from the edge. Because people refuse to educate
themselves. They refuse to actually learn what this would be like. They just look
at movies. It's not Red Dawn. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}