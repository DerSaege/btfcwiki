---
title: Let's talk about Carhartt and rural patient education....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JHT4PVjF-X0) |
| Published | 2022/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation with Carhartt, a clothing company known for its rugged workwear, and their vaccine policy causing controversy among customers.
- Shares a message from a healthcare worker who noticed a vaccination trend between urban and rural areas and seeks advice on patient education.
- Notes that political affiliation is a significant factor in vaccination rates, with Democrats more likely to be vaccinated than Republicans.
- Describes the rural areas as predominantly Republican where people tend to rely on themselves for medical care due to the distance from big medical infrastructure.
- Points out the importance of individuals in rural areas having their own medical supplies and taking preventative measures seriously.
- Mentions the difficulty in convincing people in smaller towns to shift their views and the uphill battle faced by healthcare workers in these areas.
- Suggests appealing to the survivalist stereotype to encourage vaccination and mentions the challenge of changing deeply set beliefs.
- Recommends finding unofficial methods, like speaking to family members, to influence high-risk individuals who are hesitant about getting vaccinated.

### Quotes

- "The strongest predictor of your vaccination status is your political affiliation."
- "People rely on themselves for medical stuff. They take more preventative measures."
- "Maybe appeal to that survivalist-ish stereotype."
- "Stubborn doesn't really cover it, does it?"
- "I don't really know how to use that to increase patient education, though."

### Oneliner

Beau explains the rural vaccination divide and the importance of self-reliance in medical care, offering insights on patient education challenges and unconventional methods to influence vaccination decisions.

### Audience

Healthcare professionals, educators

### On-the-ground actions from transcript

- Speak to high-risk individuals' family members to influence their vaccination decisions (implied).

### Whats missing in summary

Insights on the impact of political affiliation on vaccination rates and the unique challenges faced in rural healthcare settings.

### Tags

#Carhartt #Vaccination #RuralHealthcare #PatientEducation #SelfReliance


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Carhartt
and patient education in rural areas.
I'm not sure I can answer all of the question
about patient education,
but I can explain why this occurs.
And I know there's enough medical people
that watch this channel
that maybe y'all will be able to do something
with the information,
or y'all can provide an answer in the comments
to the question.
But before we get into that,
I have to explain the Carhartt thing.
If you don't know, Carhartt is a clothing company
that makes clothing primarily for me.
I'm their target demographic.
News came out that they have a vaccine policy.
And a whole bunch of people
who like to wear their clothing
because they like to appear
as though they work way out in the boonies,
they got really mad about it.
And then you have a whole bunch of people
who are actually out in the boonies
wearing their clothing and wearing masks
and all of this stuff showing support,
normal Twitter antics.
But it brought up a question.
Beau, I worked in an Atlanta hospital
and burned out due to the pandemic
and moved to a small town clinic in Georgia.
Your tweet about Carhartt
got me thinking about something
I've noticed since coming here.
In Atlanta, the people in the city
were mostly vaccinated
and those just outside weren't.
Here in the small town,
those in town aren't vaccinated
and those out in the country are.
A big part of my job is patient education.
Can you explain why this happens
and give any tips on how I can use it
to further educate patients?
Okay.
All of this,
I don't know that there's any studies on this,
but your anecdotal observations,
they match my world.
They match everything that I see as well.
The strongest predictor of your vaccination status
is your political affiliation.
Democrats are more likely to be vaccinated
than Republicans.
So in Atlanta, it makes sense
that people in the city
are more likely to be Democrats,
therefore they're more likely to be vaccinated
than those on the edge
who are probably more Republican leaning.
So the vaccination difference is there.
It's gonna be obvious.
When you get out into rural areas,
they're all red, right?
Like 30% of the town
is affiliated with the Democratic Party.
So the overwhelming majority of people
are Republican,
meaning they're less likely to be vaccinated in town.
And then when you get outside of town,
this weird anomaly happens
where you have people
with like giant Trump banners on their truck
who were vaccinated.
Okay, so while I don't know how this will help,
I can explain that.
That green bag right there,
the green bag in the corner,
that's a trauma kit.
That's basically your small town clinic in that bag.
There's one there,
there is one in my truck,
and there is one in the house.
Because if you get far enough outside of town,
I don't know what the mile marker is,
but there comes a point where you realize
medical help is pretty far away.
So you start relying on your own abilities.
If I have a piece of rebar go through my chest,
if the people there with me at the time
cannot stabilize me,
I won't make it.
Paramedics will not get there in time, period.
A small clinic,
probably much like the one you work at,
is probably 20 minutes away
at 70, 80 miles an hour.
A real hospital,
like they could handle something like that,
hour and a half.
So once you get outside
and you get into the real rural areas,
people tend to rely on themselves
for medical stuff.
And they tend to have their own bags,
their own equipment.
And they tend to take preventative stuff
a little bit more seriously.
Because if somebody has respiratory distress,
they may not make it
to some place that can actually help them.
That's why it occurs.
That's the reason.
Is once you get far enough away
from big medical infrastructure,
people rely on themselves.
They take more preventative measures.
So, you know,
they're more likely to get the vaccine
regardless of political affiliation.
I mean, I actually think I'm the only person around me
that doesn't own an AED.
It's pretty common if you're that far out.
So that's why.
Now, how you can use that
to convince people to get vaccinated,
I don't know.
Maybe play up into the fact
that many people, especially in the suburbs,
they want to be seen as super rural.
And maybe lean into that.
Maybe appeal to that,
I don't know, survivalist-ish stereotype.
You know, I feel for you.
You're fighting an uphill battle.
I'm sure you thought it would be easier
coming out of Atlanta and moving to a smaller area.
But yeah, it's going to be hard.
And also keep in mind,
when you get into the smaller towns,
people are pretty set in their ways.
It's going to take a lot of education
to get them to shift.
I think your best route might be talking to their mom.
I know you're not allowed to do that.
But maybe find an unofficial method
of getting the message to them.
If you know somebody that's really at high risk,
and maybe there's a family member
who you can get them to allow you to speak to
that might be able to sway them.
Because stubborn doesn't really cover it, does it?
Again, don't know that I actually have an answer,
but it's something that I noticed,
but I just I didn't know that
that was something that was common everywhere.
If your anecdotal observations match that,
it probably is true in a lot of ways.
And if it's something that can be used
to get people vaccinated, I'm all for it.
I don't really know how to use that
to increase patient education, though.
Hopefully somebody in the comments will.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}