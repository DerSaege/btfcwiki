---
title: Let's talk about Republicans learning about Streisand....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m9ml3FHGwFA) |
| Published | 2022/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party in the United States demonizes education, trying to make ignorance a duty and knowledge a crime.
- They have created lists of objectionable books and want to teach mythology instead of history.
- Students' response has been strong, with essays and banned book clubs forming in protest.
- An eighth grader in Pennsylvania started a banned book club, inspiring activism in the younger generation.
- Beau addresses a common question from high school students on how to start community networks.
- He praises the idea of meeting to read banned books as a litmus test for potential activists.
- Reading and discussing banned books can lead to real-world activism and community involvement.
- Beau encourages high school students to find like-minded peers through book clubs as a safe way to get involved.
- He contrasts adults afraid of education with youth advocating for knowledge and learning.
- The youth are pushing back against attempts to limit education and information access.

### Quotes

- "The response from students has been more pronounced than the response from a lot of people in the Democratic Party."
- "I would like to thank the Republican Party for creating the next generation of activists."
- "Band books are the best books."
- "Those are people that want to make the world a better place and are willing to invest the time to do so."
- "Welcome to the information age and the Streisand effect."

### Oneliner

The Republican Party demonizes education, but students fight back with banned book clubs, inspiring activism and community involvement among the youth.

### Audience

High school students

### On-the-ground actions from transcript

- Start a banned book club at your school to encourage activism and community involvement (suggested)
- Read and openly discuss objectionable books to challenge censorship and inspire others to join in activism (suggested)

### Whats missing in summary

The full transcript provides more context on the importance of education, activism, and community involvement, along with a reflection on the contrast between adults' fear of education and youth's advocacy for knowledge.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some students who refuse to shirk their duty of knowledge
and we're going to answer a question that I've never had an answer to before that I've
been asked for quite some time.
The Republican Party in the United States has spent a lot of effort demonizing education
of various sorts, trying to make ignorance a duty and knowledge a crime.
They have put out lists of books that they don't want students reading, that are questionable,
objectionable.
They have pushed to revamp the way history is taught and would rather mythology be taught
in place of history.
The response from students has been more pronounced than the response from a lot of people in
the Democratic Party.
You have essays, op-eds, being published in major outlets that are basically telling parents
to quote chill out.
You have the formation of banned book clubs, one that caught a lot of press up in Pennsylvania,
founded by an eighth grader, and they're meeting at a local bookstore and they're reading all
the books that the Republicans have deemed objectionable.
That is fantastic.
That's wonderful.
I would like to thank the Republican Party for creating the next generation of activists.
It's beautiful to see.
The kids are definitely alright.
Now a question I've gotten over the years when I talk about community networks is coming
from people who are in high school.
How do I start one?
How do I sift through people who would be interested in this?
I've never had an answer before because I'm not one to tell people in high school, hey,
go meet strangers off the internet or try to arrange your entire social life around
this.
You know, it has to be something that's organic, but finding something that would appeal to
people in high school that would also serve as a litmus test for whether or not people
would actually be active, that's been hard.
I've never really been able to come up with anything.
I would like to thank an eighth grader in Pennsylvania.
Band books are the best books.
And students in high school who would devote some free time to reading books like that
and discussing them, those are people that will be active in your community.
Those are people that want to make the world a better place and are willing to invest the
time to do so.
So if you've ever asked me that question, there's your answer.
That would be a wonderful gateway into getting involved in real world activism.
Meeting in a place like that, very public, people showing up and coming as they see fit.
It doesn't require contact outside.
It can be arranged just to be from your high school or middle school or wherever, I guess
eighth grade's middle school there.
It seems like a relatively safe way to establish that circle of friends that you could become
involved in your community with and that would be willing to put forth the effort.
On one side, I do want to point out, we have a bunch of adults terrified of books, of education,
wanting to outlaw, wanting to make sure that those topics, well, they're just not discussed,
that their kids are left without an education.
And on the other side, you have the youth of this country screaming very loudly that
that's just not going to do.
Welcome to the information age and the Streisand effect.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}