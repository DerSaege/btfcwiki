---
title: Let's talk about Mitch McConnell's tribute to Malcolm X....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oMLhXsD__co) |
| Published | 2022/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring Mitch McConnell's tribute to Malcolm X and the confusion in society's upper echelons.
- McConnell's dismissive comment about African American voting rates and Malcolm X's powerful words on Americanism.
- The disconnect between the establishment and the struggles of the common folk.
- The failure of the elite to understand the diminishing patriotism and discontent in America.
- The detachment of the rich from the realities of socioeconomic struggles.
- Lack of access to healthcare, exploitation of soldiers, and voter suppression go unnoticed by the elite.
- Ignorance towards voting rights legislation, college costs, and the propaganda around education.
- Elite's disconnect from the American dream turning into a nightmare for many.
- Commonality among those at the bottom, irrespective of demographics, in facing the American nightmare.
- The elite's focus on legislating patriotism, pushing college narratives, and restricting education to maintain ignorance.

### Quotes

- "I'm speaking as a victim of this American system."
- "They don't live in a world where what they notice is the rich getting richer and the poor getting poorer."
- "If they want people to respect the country, they have to create a nation worth loving."
- "If this government cannot secure voting rights, you're not going to find a lot of patriotism."
- "But if there was a single person who could sum up what Malcolm X was talking about, it's probably Mitch McConnell."

### Oneliner

Beau delves into the disconnect between the elite and the struggles of the common folk, echoing Malcolm X's words and questioning the lack of patriotism in an America turning into a nightmare.

### Audience

Activists, Progressives, Voters

### On-the-ground actions from transcript

- Advocate for voting rights legislation (implied)
- Support initiatives to make college education more affordable (implied)
- Fight against voter suppression tactics (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the disconnect between the elite and the struggles faced by many in society, offering a critical perspective on patriotism and the American dream.

### Tags

#MitchMcConnell #MalcolmX #AmericanDream #Patriotism #VotingRights


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Mitch McConnell paying tribute to Malcolm X.
And why there's confusion among many in the upper echelons of our society,
our bettors, why they don't understand where the discontent is coming from.
You know, Mitch McConnell, he recently made a comment that was something along the lines
of, oh we don't need to worry about it because African Americans are voting at almost the
same rate as Americans.
Wow, I mean that's quite the statement there Mitch.
But I want to read you something.
No I'm not an American.
I'm one of the 22 million black people who are the victims of Americanism.
One of the victims of democracy.
Nothing but disguised hypocrisy.
So I'm not standing here speaking to you as an American or a patriot or a flag saluter
or a flag waver.
No not I.
I'm speaking as a victim of this American system.
I don't see any American dream.
I see an American nightmare.
Malcolm X.
Now I'm fairly certain that Mitch McConnell's never actually listened or read any Malcolm
X, but I couldn't help but think of that when I heard what he said.
Many in the establishment today, many in the upper echelons are bettors.
They look down at us common folk and they wonder where the patriotism went.
Why is it so hard to cast that image of America, the land of the free, the home of the brave,
the greatest country on the earth?
And it's because they're so out of touch.
They don't see what those on the lower rungs of the socioeconomic ladder see.
They don't live in a world where what they notice is the rich getting richer and the
poor getting poorer.
They don't live in a world with a lack of adequate access to health care, being able
to take care of yourself when you get sick.
They don't see their brothers, their sisters, their friends deployed all around the world
as cannon fodder for adventures that will make the establishment richer.
They don't witness that.
They don't see the establishment try to undermine the vote and remove what little voice Americans
still have in how this country's run.
They don't see that either.
So they don't see the need for voting rights legislation.
They don't see how expensive college is, the debt.
They didn't experience all of the propaganda, all the advertising saying, hey, if you want
a successful life, if you want to have a great career, you need to go to college.
Then a few years later, get out and see ads that say master's degree required, $14 an
hour.
They don't see that.
They're so disconnected from it.
They're like those students at that elite school that assumed the average American makes
$800,000 a year or whatever it was.
They're that disconnected.
They don't see that those on the bottom don't have the American dream and they've woken
up to the American nightmare.
And it's not restricted to one demographic.
It's across the board.
Those on the lower rungs, well, they have realized they have far more in common with
somebody who has a little bit of a different skin tone than they are ever going to have
in common with those people in DC, telling them, well, if you just worked harder, it'd
be all right.
They want to legislate patriotism.
You know, they sit there and still push the college narrative.
Not understanding the costs, not wanting to do anything about them.
And then they try to undermine public education by getting them to restrict what can be taught
to keep the people on the bottom as ignorant as possible.
And they have the nerve to wonder why Americans aren't cheering them on.
Why they don't have that patriotic fervor that once existed.
Those in DC have forgotten that if they want people to respect the country, they want people
to respect the flag and speak of love of nation, they have to create a nation worth loving.
They have to create a nation that is trying to fulfill all of those promises and all of
those documents that they like to quote.
If this government cannot secure voting rights, the bare minimum, you're not going to find
a lot of patriotism.
You'll find more and more people looking at the American dream and seeing that nightmare.
I know Mitch McConnell was not trying to pay tribute to Malcolm X.
But if there was a single person who could sum up what Malcolm X was talking about, it's
probably Mitch McConnell.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}