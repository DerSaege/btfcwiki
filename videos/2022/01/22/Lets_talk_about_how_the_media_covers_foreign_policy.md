---
title: Let's talk about how the media covers foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RqVS8yUCbY8) |
| Published | 2022/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received messages about different perspectives on Russian and U.S. postures in foreign policy.
- Avoids injecting personal feelings into coverage to provide unbiased information.
- Media tends to cheerlead and establish narratives rather than inform objectively.
- Tucker Carlson's misunderstanding of NATO's purpose and history.
- NATO's existence justified by fear-mongering and the need for mutual defense.
- Foreign policy is not about good guys versus bad guys but about power and interests.
- Ideology and morality take a backseat to power in international relations.
- Consistently against all wars unless necessary for civilian life preservation.
- Power structures determine war decisions, not necessarily morality.
- When consuming media, be aware of framing and focus on understanding power dynamics.

### Quotes

- "Foreign policy is a giant international poker game where everybody's cheating."
- "It's not about good guys and bad guys. It's not about morality. It's not about right and wrong. It's about power."
- "My default is to be against war. You have to convince me that it is absolutely necessary."
- "If you want to support the troops, whichever side of whichever conflict, you have to support the truth first."
- "It's just framing. It's about power, and anything that distracts from that is the actual motive."

### Oneliner

Beau presents a critical view of foreign policy, focusing on power dynamics and the absence of good versus bad actors, stressing the importance of supporting truth over narratives.

### Audience

Media consumers

### On-the-ground actions from transcript

- Support truth over narratives (implied)
- Stay informed and critical when consuming media (implied)

### Whats missing in summary

The detailed breakdown of various perspectives on foreign policy, NATO's purpose, and the focus on power dynamics rather than morality in international relations.

### Tags

#ForeignPolicy #MediaAnalysis #PowerDynamics #NATO #WarOpposition


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk a little bit more
about foreign policy, and how the media covers it,
and how I cover it, and Tucker Carlson,
and the key element you really need to take to heart
when it comes to foreign policy.
Last night, I received a bunch of messages about something.
three different kinds. One upset that I laid out the Russian case as to why they are assuming
the posture they're assuming. Another mad that I seem to be supporting the U.S. case
and the U.S. posture. And yet another group mad that I didn't say what I believed. That
The last one's right.
I don't try to inject my personal feelings into coverage of foreign policy because pretty
much every pundit, every commentator out there does that.
And they tend to inflame more than inform.
And it's also worth noting that my opinion on this, on how it should be done, doesn't
really matter because it doesn't weigh in what's going to be done.
When the media covers it, they tend to cheerlead.
They tend to establish a narrative and say, this is how it should work.
It establishes expectations for the administration and the powers involved as to how they should
behave.
I don't necessarily think that's a good idea.
I got a message from somebody last night asking me about something Tucker Carlson said because
apparently he informed his audience last night that nobody knows why NATO exists.
was built to counter the Soviets during the Cold War, but not a single person can tell
you why it exists today, which I find hilarious.
It's worth noting that the only time Article 5 has been invoked with NATO was after the
end of the Cold War.
It was a day in September in 2001.
If Tucker wants an idea of what NATO might be used for, he could go to YouTube and type
in his own name and the word China and take a look at all of that fear-mongering.
That super-masculine army that's going to come beat us up.
Because that idea is put out there, that fear-mongering is out there, it justifies NATO's existence.
It justifies permanent military alliances.
That's why NATO exists.
It's a mutual defense organization.
That's its purpose.
So when you're talking about foreign policy and you're listening to pundits, if they're
not super clear about when they are inserting their own opinion, they're probably doing
you to service, because they're establishing expectations that they want to happen, how
they feel it should happen, none of that matters, because it doesn't actually, it doesn't carry
any weight with what's going to happen.
One of the big issues for people that are trying to understand foreign policy, especially
now that we have moved into near-peer contests is they're looking for good
guys and bad guys. I've had people ask in relation to Ukraine, what's the
anti-imperialist stance here? There's not one. There's not one. Russia wants to
literally take and hold territory, old-school imperialism. The United States
would like to see NATO expand and expand and expand through that new form of
soft imperialism. We're not taking over your country, but I mean, we can always
invoke Article 5 and you're gonna help us out. Same end result, just different
strategies. Even the idea that, well, it's right up against their border. That's an
imperialist stance too, spheres of influence. Ukraine is a country by itself, so farming
out its sovereignty to Russia simply because it is geographically close, that's, that
is what it is. People are looking for good, good guys and bad guys. They don't exist.
in this. You know, during the Cold War there was that window dressing, that
framing, that it was democracy versus communism, you know, and that's how it got
portrayed. We don't have that framing right now. We don't have that window
dressing. The United States is attempting to dress it up as democracy versus
authoritarianism. That's the framing they're trying to get to stick at the
moment, but it's not there yet and the game's already being played. So if you
want to know if it's really window dressing or really what it's about, it's
already started before the window dressing was applied. It's not about that.
Foreign policy is a giant international poker game where everybody's cheating.
It's not about good guys and bad guys. It's not about morality. It's not about
right and wrong. It's about power. Most times, ideology doesn't even come into it.
It's about power. Nothing else. And understanding that is critical to being
able to understand what's happening. The ideological arguments, they don't matter
because it's not about ideology. It's not about morality. It's about power.
and that's it. Now that I've said all this, you know, I feel like, I feel like
this should be known for people who watch the channel a while. I'm pretty
consistent in this. I'm already against the war. Which one? All of them. The next
one too, the one after Ukraine. My default is to be against war. You have to
convince me that it is absolutely necessary. Absolutely necessary for what?
Power? Ideology? No. Preservation of civilian life. That's how I measure it, you
know. If I lived in Ukraine, I might have a very different opinion of that, you
You know, it's easy to be way over here and be against, you know, using whatever means
is available.
But at the end of the day, because of the power structures involved, Ukraine doesn't
even get to make the decision.
Russia does.
Russia gets to decide whether or not there's a war.
Understand the United States, NATO, is not threatening to invade.
Russia. Is that, does that somehow make NATO the good guy? Not really. Not really.
Because it's still just about power. It's not about right and wrong. When it comes
to foreign policy, when we're talking about foreign policy issues, I really
don't try to insert my views until the cost starts weighing on civilians.
That's when you hear me start to talk about it or when I know that that's what's going
to happen.
So when you're consuming media, remember that all of the framing that's being put out there,
it's just that.
It's framing.
It's about power, and anything that distracts from that is the actual motive.
It's just extra stuff that doesn't need to be there.
We're in that unique spot where the struggle has already started.
The contest has already begun, but the framing, well, that hasn't been established yet.
10 years, yeah, it will probably be framed as democracy versus authoritarianism, assuming
the United States is still a democracy.
If you want to support the troops, whichever side of whichever conflict, you have to support
the truth first.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}