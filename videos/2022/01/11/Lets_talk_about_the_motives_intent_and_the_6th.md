---
title: Let's talk about the motives, intent, and the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6zwtzLcRtes) |
| Published | 2022/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a common question about why he doesn't provide individual motives in his breakdowns.
- Explains that determining motive accurately is difficult without talking to the person directly.
- Warns against speculating on motives as it can lead to manipulation and simplistic answers.
- Points out the dangers of assigning intent and motive, especially in media, to protect against manipulation.
- Emphasizes the importance of providing accurate information over guesswork.
- Acknowledges that people have multiple motives for their actions, making it hard to pinpoint a single primary one.
- Prefers to focus on group dynamics, events, and historical context rather than individual motives in urgent national issues.
- Suggests that examining individual motives can happen later once the immediate concerns are addressed.
- Stresses the need to understand how events unfold historically to prevent their recurrence.
- Concludes by encouraging a focus on the broader picture rather than diving deep into individual motives for better accuracy and understanding.

### Quotes

- "Because I like to be right."
- "I like to be right and I think that doing it, if you can't be accurate, is dangerous."
- "We need to get down to the nuts and bolts of what occurred."
- "People's motives for doing things, especially things that are like this, are often very private."
- "Have a good day."

### Oneliner

Beau explains why he doesn't speculate on individual motives and focuses on accuracy and broader context in understanding events.

### Audience

Creators, Analysts, Journalists

### On-the-ground actions from transcript

- Analyze group dynamics and historical context to understand events better (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of why Beau chooses not to speculate on individual motives and instead focuses on accuracy and broader context in understanding events.

### Tags

#Analysis #Media #Accuracy #GroupDynamics #HistoricalContext


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about a
complaint, not really a complaint, but a question that I got that is kind of
pretty prominent throughout the channel. If you're somebody who is looking for
this, odds are you won't find it here. It'll be pretty rare. The person reads
mystery novels a lot, enjoys mystery novels, and they basically said, hey you
know I really like the way you break down how something happened, what
happened, the dynamics of what happened, why it happened in a general sense, but
one of the things that I've noticed is that you never tell us the individual
motives of the people that are involved, and I feel like I'm missing part of the
story. So the question is why don't I provide motive for people that I'm
talking about? And there's a short snarky answer and then there's a
long answer. We're going to give them both. Why don't I speculate as to motive? Because I
like to be right. And determining motive, determining intent, is very hard unless
you are talking to the person. It's not simple, and if you try to just wing it
and guess, you end up speculating and you get slogan answers. Why did these people
do this on the 6th? Because they're traitors. I mean sure, but that's
not the... that doesn't help. That doesn't actually provide the real motive, the real
intent. And when you start doing that, if you start providing motive that you're
not sure about, you can manipulate people. If you go to the other channel, there's
actually a long video titled The Roads to Misinformation, and one of the things
that I caution people about, and the whole video is just ways to protect
yourself against manipulation in the media, one of the things I caution about
is people who assign intent, who provide motive. It's hard. It's hard to do that
accurately. So because I like being right, if I can provide all of the information
other than that, and say this is what happened, this is the dynamics of what
occurred, this is why it happened in the way that it did, I don't feel the need to
get into the intent of the individuals. And then the other part that's a
little bit longer of an answer is that people want a motive. It's even in the
message, I should have read it. It's a motive. Don't provide a motive for the
people involved. People don't have a motive. People have motives, more than one.
You know, as far as what happened on the 6th, and the politicians that provided
cover for what occurred, some of them probably did so because they truly want
some authoritarian strongman, fashy leader, right? Some didn't understand the
danger and were just, politics is normal, and then things got out of hand. Some
were doing it because they were making a lot of money off that rhetoric. Some were
probably not smart enough to understand how they themselves were being
manipulated. Some didn't have the courage to stand up to larger figures within the
party. Some were probably at some point approached by someone who was like, hey,
you know that thing you did? It'd be a real shame if people found out about
that. These are a whole bunch of different motives that could apply to
different individuals. But the thing is, odds are that most people who
participated in it, that they had more than one of those. And even if you were
able to isolate all the possible motives for a single person, you have no idea
which one was the primary one. And sometimes from the outside, the one that
appears to be the primary motive, it's not. It's just the most visible. I take a
lot of pride, I guess, in providing the most accurate information that I can. I
don't like trying to speculate about motive because I'm not going to be
accurate. I won't be right and it'll leave you with my best guess. Sometimes I'll
provide a general motive, like the way I just did. These things right here could
be the reasons for it. But when you get to an individual person and their
actions, it's really hard to pin down all of their motives and then to
select the one that was primary in their actions. So the simple answer is, I
don't do it because I like to be right. The long answer is, I like to be right and
I think that doing it, if you can't be accurate, is dangerous because it
increases that slogan bumper sticker mentality. And it further polarizes
things when we probably don't need that right now. We need to get down to the
nuts and bolts of what occurred, figure out how to make sure it doesn't occur
again, and we can examine the motives of people later, the individual motives.
Right now, because this is a national issue that has a lot of urgency, I think
it's more important to talk about the group dynamics and the actual events and
the order they occurred and how things occur historically. I think that's far
more important than delving into Representative X did this because, you
know, he was insulted by this other person five years ago. People's motives
for doing things, especially things that are like this, are often very private.
So speculating on them is hard if you care about accuracy. Anyway, it's just a
thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}