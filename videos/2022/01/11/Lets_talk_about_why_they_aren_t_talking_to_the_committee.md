---
title: Let's talk about why they aren't talking to the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VMlUCBYZIVM) |
| Published | 2022/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the structure of movements, particularly the division between the political wing and militant wing within movements seeking to change governance by force.
- The militant wing uses violence or the threat of violence to influence and achieve their goals, while the political wing provides cover and condemns the actions while advocating for the goals.
- Plausible deniability is key for the political wing to distance themselves from the militant wing, making it easier for them to be reelected.
- Communication between the political and militant wings is usually indirect, through dead drops or cutouts, to maintain separation and plausible deniability.
- The division between the political and militant wings is vital for the functioning and safety of the movement.
- Failure to maintain the division could lead to exposure and legal consequences, which may explain why certain individuals are not coming forward to make their case.
- Beau suggests that there may have been communication links between members of Congress and militant movements involved in the events of January 6th.

### Quotes

- "Plausible deniability is key."
- "The division between the political and militant wings is vital."
- "Failure to maintain the division could lead to exposure."
- "Communication between wings is usually indirect."
- "I have a feeling we are going to find out."

### Oneliner

Exploring the structure of movements and the importance of maintaining division between political and militant wings to ensure plausible deniability and safety.

### Audience

Observers, Activists, Politicians

### On-the-ground actions from transcript

- Investigate potential communication links between members of Congress and militant movements (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the structure and dynamics of movements, shedding light on the importance of maintaining separation between different wings for the movement's integrity and success.

### Tags

#Movements #PoliticalWing #MilitantWing #Communication #PlausibleDeniability


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to conduct a little thought exercise.
And we're going to talk about the structure of movements.
Movements like the one that has sprung up in the United States over the last few years.
And why the movement in the U.S. seems to be deviating
from a very established norm and a pattern that exists all over the world.
And we're going to talk about what that might mean.
Why that change is occurring and what it may mean long-term.
Okay, generally speaking, when you're talking about movements
that seek to change the system of governance by force,
they tend to divide up into two wings.
You have the political wing and the militant wing.
The militant wing is the one everybody knows about.
Those are the people out there getting headlines.
Those are the people that might, I don't know, rush into a building
while something was going on in an attempt to stop it.
Something like that, right?
These are the people who are using violence or the threat of violence
to influence those beyond the immediate area of attack
to achieve a political, religious, ideological, or monetary goal.
Those people. That's the militant wing.
The political wing are the people who are providing cover for the militant wing.
They're putting a kinder, gentler face on the movement.
They're typically people in elected office, right?
And what they normally do is they condemn the action, right?
So that thing that that other wing did, that was bad.
They shouldn't have done that.
But why did they do it?
Didn't they really just have the purest of intentions?
Aren't they just true patriots?
And maybe we should think about what they were trying to achieve
and maybe we should just do that.
The idea is to advocate for the goals while condemning the action, right?
And the militant wing doesn't get mad about this
because they know they're going to do it because it's all a giant game.
It's a show for the public, right?
That's how it's supposed to work.
So the people in the political wing should jump at the chance to,
I don't know, talk to a committee and make the case
because then they can come out and grandstand.
They can say, this is what we talked about.
This is what I said.
This is why we really need to address this issue.
What does it mean if they don't do that?
If they suddenly go silent and nobody wants to make the case?
What happens when the political wing stops doing that?
And why would they stop?
See, I think people understand that there are two wings,
two movements like this.
I'm not sure that they understand why.
This structure is to give the political wing plausible deniability.
It's to give them the ability to say, no, I'm not part of that group.
That's the reason.
They do that because if you're in elected office, generally speaking,
it's kind of hard to get reelected if you've been linked to a group like that
in most places.
Aside from that, in most countries, it's kind of illegal
to try to change the system of governance by force.
Most nations tend to look down on that sort of thing.
So the people who are out there speaking for that movement,
well, they can't be linked to it.
The political wing and the militant wing,
they don't talk to each other, not openly, ever.
They only communicate via the press through like prepared statements
or through dead drops or cutouts.
Dead drops are, think about any spy movie you've ever seen
where somebody writes a message and then hides it
in like a hollowed out log and then leaves and somebody gets it later.
That's a dead drop.
The two people never meet face to face.
A cutout is a trusted intermediary.
Now, this is somebody who in movies is generally
depicted as a guy wearing like khaki cargo pants and a pea coat
and a watch cap.
And he picks up like the envelope and goes and delivers it.
That's not how it works in real life.
Most times, more often than not, it's like a bartender
or maybe a pretty woman like Julia Roberts.
Because if it's something like that, it gives the person meeting them
a reason to be tail conscious.
So if they're under surveillance, it doesn't look weird
that they were looking over their shoulder the whole time
when they arrive at their destination.
The people pulling surveillance on them are like,
oh, well, yeah, he didn't want to get caught doing that.
And it doesn't look like it has anything to do with some plot.
A good cutout generally would be somebody who is nondescript.
It would be somebody who has no public facing allegiance
to either the political group, the political wing,
or the militant wing.
They would be seen as uninvolved with it.
They would be relatively unknown.
So that division between the political wing and the militant wing
exists for a reason.
It exists to provide plausible deniability.
And in order for that to work, the two sides can't ever talk.
They can't have communications going back and forth.
I'm not sure that the movement in the United States knew that.
I don't think they understood that aspect of it.
A really horrible person to use as a cutout
would be, hypothetically speaking, I don't know,
a incredibly well-known political commentator with a podcast.
That would be a really bad person to use.
Somebody who has public facing allegiances to both the political wing
and the militant wings.
That would be a really bad idea.
You would have to be really, really horrible with this
to use somebody like that as your conduit for communication
because you would almost certainly get caught.
Now, if you engaged in a major activity
and were unaware of the fact that that division was supposed to exist,
that that compartmentalization between the political wing
and the militant wing was supposed to be there,
and it didn't when some major event occurred,
well, it would be very advisable
for the political wing to not talk about it.
It would be even more so
if the questions were going to be about their communications.
If members of a would-be political wing
were in that situation,
they would probably be worried
about a parallel criminal investigation going on.
So they wouldn't want to say anything under oath about their communications.
They could be used against them later,
probably because they might know
that politician X called politician Y,
who then called somebody outside of politics,
who then had direct contact with somebody in a militant group.
And all of those communications are logged.
That might explain why we don't see the perceived political wing
here in the United States attempting to get out there and make the case,
why they're not rushing to talk to the committee.
That would make sense.
And to be honest, it's the only thing I can think of.
Because even those who may be just completely unaware
of any type of organized activity,
it would seem that they would also want to talk to the committee
because it's just good politics for them right now.
But there are a lot of people not wanting to do that.
You have to wonder why.
And the only reason that I can come up with
is that they didn't understand why there was supposed to be a division
between the political wing and the militant wing.
Why that compartmentalization exists in the first place.
It's not just to divide up the pacifists from the other people.
It's not why it happens.
And I don't think they understood that.
I have a feeling that by the time this is all said and done,
we are going to find out that there were communication links
between people who are currently sitting in Congress
and the leadership of militant movements
that were involved in what happened on the 6th.
I could be wrong, but I don't think I am.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}