---
title: Let's talk about American dogs and Chinese yaks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vtk7dbLsgJw) |
| Published | 2022/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the development of robotic technology between China and the United States.
- Chinese state media released footage of testing a robot called "yak" designed for transporting supplies to troops in isolated areas.
- The robot is likely to be weaponized, capable of carrying machine guns, grenade launchers, or anti-tank rockets.
- Beau points out fear-mongering over the robot development and the potential for a "robot gap" between countries.
- Compares the Chinese robot to the American Big Dog, noting differences in payload and speed.
- Emphasizes that Chinese technology is about 17 years behind American technology in this aspect.
- Beau warns against allowing fear to drive increased defense spending or justify military intervention using robotic technology.
- Considers the implications of robotic warfare on global power dynamics, with major powers gaining more leverage over smaller or poorer nations.
- Predicts that ground warfare is evolving towards a future resembling Star Wars with the introduction of weaponized robots.
- Raises concerns about the political consequences of using robots in warfare and the detachment from human casualties in decision-making.

### Quotes

- "We cannot allow there to be a robot gap."
- "This is the future of ground warfare."
- "Are there going to be boots on the ground? The answer will always be no. The reality is there will always be boots on the ground."
- "Country doesn't do what you say. Well, you let slip the dogs and yaks of war."
- "And with little risk to American or Chinese lives, the likelihood of using them increases a whole lot."

### Oneliner

Beau examines the development of weaponized robots, warning against fear-mongering and the escalation of military interventions driven by advancements in ground warfare technology.

### Audience

Policy makers, Activists, Military personnel

### On-the-ground actions from transcript

- Advocate for international treaties or agreements to regulate the development and use of weaponized robots (implied).
- Support organizations working towards demilitarization and peace-building efforts (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of robotic warfare on global politics and military interventions, offering a cautionary perspective on the future of ground warfare.

### Tags

#Robotics #Weaponization #GlobalPolitics #MilitaryIntervention #FutureWarfare


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to,
we're gonna talk about American dogs and Chinese yaks,
robots, we're gonna talk about robots
and what those robots mean for the future.
Chinese media, Chinese state media,
released footage of them testing out their robot,
which they call the yak.
It looks exactly like you would expect
given recent developments in robotics.
It's four legs.
It looks just like the ones you've seen
in science fiction movies
or dancing around on the internet and on social media.
Okay, so the specs on the thing say
that it can carry 350 pounds
and it moves at about six miles per hour.
The Chinese government is saying
that this instrument will be used to transport supplies
to troops in isolated areas.
That's what you or I,
you know, outside of the world of diplomacy,
that's what you or I would call a lie.
That's just not true.
I don't know a military commander in the world
that would look at a helicopter and be like,
is there anything that you have?
I mean, this is good and all,
but do you have anything that's like a whole lot slower
and can carry less equipment at a time?
Bring it to me.
Maybe make it more vulnerable too.
It's not what it's for.
These are designed to be weaponized.
Okay, the Chinese ones, the American ones,
they're all designed to be weaponized.
350 pounds that can carry a machine gun
and an automatic grenade launcher
or a bank of HJ-12s, anti-tank rockets.
That's what it's for.
Because of that and because of the current political climate
it's incredibly likely that people will fear monger
over this.
You know, we cannot allow there to be a robot gap, I guess.
So let's see how it stacks up to American technology.
Okay.
Big dog is roughly comparable.
Big dog has a payload of 340 pounds
and moves at four miles per hour.
So the Chinese one carries just a wee bit more
and moves a little bit faster.
The thing is, Big Dog came out in 2005.
Chinese tech is about 17 years behind American tech
on this one.
And Boston Dynamics has made a lot of improvements
in those 17 years.
There is no reason to allow fear mongering over this.
And it is certainly not justification
for an even larger defense budget.
That's one aspect of it.
And the other aspect of it
is that this is the future of ground warfare.
Like it or not, this is where we're headed.
We are headed to Star Wars.
There are walkers on the horizon.
That's where we're going.
And that's really bad.
And that's really bad.
Major powers, world powers, will have access to this.
Smaller countries, poorer countries, they will not.
And it's going to allow the world powers
to operate with even more impunity.
When the United States is considering an intervention
in a country, what's the question that always gets asked?
Are there going to be boots on the ground?
Right?
And the reason the politicians always ask that
is because being a wartime politician is good,
but those casualties, those affect poll numbers.
So if there's too many that could be lost,
well, they may not get reelected.
It's not that they actually care about the lives lost.
It's they care about the political outcomes.
The casualties impact public opinion of the war
and therefore public opinion of the politicians.
That's why they care.
When these things become operational,
are there going to be any boots on the ground?
The answer will always be no from the American perspective
or from the Chinese perspective.
There'll always be no.
The reality is there will always be boots on the ground.
Those boots will be filled by the people
these things are going after
and the people that are caught in between them.
This technology is going to increase
American military adventurism.
It is going to increase our willingness to intervene
and politically realign nations that don't do what we say.
And any world power that gets access to this,
they'll be doing the same thing.
This is the future of American foreign policy.
Country doesn't do what you say.
Well, you let slip the dogs and yaks of war.
And with little risk to American or Chinese lives,
the likelihood of using them increases a whole lot,
which means the likelihood of small conflicts increases a whole lot.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}