---
title: The roads to Career Day about starting a channel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TQWvx0eHel4) |
| Published | 2022/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a virtual career day where he answers student questions and tackles Twitter inquiries related to being a YouTuber.
- Initially pushed into YouTubing, Beau shares how he hid his accent until it became a source of humor, leading him to start making videos.
- Beau started his channel with a broken cell phone and a $110 Chromebook, stressing content over production quality at the beginning.
- While some YouTubers make a living at 100k subscribers, ad rates vary wildly, and success often hinges on luck.
- Luck is deemed the most critical factor in a successful YouTube channel, overshadowing advice on algorithm manipulation or content quality.
- Beau doesn't get to choose pre or post-roll ads on his videos; he only selects sponsors for integrated segments.
- Beau addresses the unpredictability of YouTube revenue, where some creators earn significantly more than others based on ad rates and content.
- The key to a successful YouTube channel, according to Beau, is high-quality content that resonates with viewers, surpassing algorithms and production quality.
- Beau shares his thick-skinned approach to handling toxic comments and the importance of moderating his comment section to foster inclusivity.
- Beau estimates working 60 hours a week on his channel but stresses the need for a backup profession for financial security.

### Quotes

- "Luck. Luck. It's luck."
- "Content is king. Always has been."
- "YouTube probably isn't the route for you."
- "I make the videos. I've tried not to overthink it."
- "Do it as a hobby and let it turn into something."

### Oneliner

Beau shares insights on starting a YouTube channel, stressing luck, content quality, and the need for a backup profession for financial security.

### Audience

Aspiring YouTubers

### On-the-ground actions from transcript

- Start your YouTube channel as a hobby and let it grow organically (suggested).
- Focus on creating high-quality content that resonates with your target audience (suggested).
- Moderate your comment section to foster inclusivity and remove toxic comments (exemplified).
- Have a backup profession for financial security (exemplified).

### Whats missing in summary

The full transcript provides in-depth insights into Beau's journey as a YouTuber, including the importance of luck, content quality, and financial planning. Viewing the full transcript can offer a comprehensive understanding of the challenges and strategies involved in building a successful YouTube channel.

### Tags

#YouTube #ContentCreation #FinancialPlanning #Luck #Inclusivity


## Transcript
Well, howdy there, internet people. It's Beau again. Today is going to be incredibly different.
Today is a virtual career day. I have a bunch of questions from students, and that's going to be
the first part. The second part is going to be answering questions from Twitter, and they're
all related to doing this as a job. If you don't know, and I didn't until very recently,
I want to be on YouTube is the new I want to be an astronaut. So I was asked to answer some
questions to help people who may want to pursue it. So I haven't read the ones from Twitter at
all, and I only read the first three from the students because the third one was really funny,
and I was like, you know what, y'all should probably get live reactions to these. So
okay, let's get started. How did you know you wanted to be a YouTuber? I didn't. I didn't know,
and to be honest, I had to be pushed into putting these videos on YouTube to start with. I
I hid my accent most of my life, and when people finally found out I had one, it became a joke,
and I started making videos making fun of myself, and then a friend was like, you really need to
put these on YouTube, and I'm like, yeah, it's not that important. It's not that good, blah, blah,
blah, and eventually got to the point between him and my wife, you know, that I felt like I had to.
They really did push me into it, and I'm really glad they did.
How much do the cameras and computers cost to start? This channel started with a broken cell
phone that only produced audio out of one side of the speakers. It was only coming out of one side,
and like a $110 Chromebook. You don't need much to start. As time goes on, you will get more
expensive equipment, cameras, computers, and all that stuff. So I'm not going to get into that.
I'm going to get into the expensive equipment, cameras, computers, lighting, mics, all kinds of
stuff. But to begin with, don't worry about the production quality. Don't worry about that stuff.
Worry about the content. If it's something that people, if it's something that will resonate with
people, they'll watch it even if they can only hear it out of the left speaker. So don't worry
about that. The next question. I don't know who you are. Do you know Mr. Beast? I have DM'd with
Mr. Beast on Twitter once, and it was like a five reply conversation. So no, no, I do not know Mr.
Beast. Not really. How much do YouTubers really make? Most don't make anything. There's a lot that
goes into the finances of them, because it's all based on ad rates. And the ad rates, they vary
just wildly depending on who the person is that's watching, what ad they're being shown, what time
they're being shown it, what country they're in. Generally speaking, you can make a living on
YouTube at a hundred thousand subscribers, somewhere in there. But that really varies wildly.
I know a lot of people who put out really good content and put a lot of time and effort into it
that don't make a whole lot. And I know people who get on YouTube and play video games, and they
make a fortune. I know one person that makes more a week than I make a year. It just depends on the
ads that are shown and how much advertisers are willing to pay for those ads. There is no
straight answer to that question. It's not like a normal job where there's a set pay rate. It
doesn't work like that. It's very unpredictable. I've watched a lot of videos that tell you how
to grow a successful YouTube channel. What do you think the most important part is? Luck. Luck.
It's luck. All of the stuff that are in those videos, and I've watched them, it's good advice.
But what really matters is having that video that goes out at the exact time a whole bunch of people
want to see it, and it resonates, and it clicks, and it starts to get pushed out to other people
in the recommendations, and those people subscribe. And then once that happens, you have to have
follow-up content that will keep them engaged. Without a little bit of luck, none of that other
stuff matters. There's no way to predict what people are going to want to watch ahead of time.
It's pure luck on that part. All of the other stuff matters, but you have to have the luck first.
Aside from luck, I would say content. I would say that the quality of the content is more important
than any games you're going to try to play with the algorithm, any production quality,
anything like that. Content is king. Always has been.
Do you get to pick your advertisers? No. Well, for those people who do sponsored videos, like you're
watching a video and in the middle of it, they're like, this video brought to you by whatever. You
can pick those, but those that are shown before or after or during your content, no. You don't
really pick those. Do you get free stuff from companies to review on your channel? I don't,
because that's not what I do, but there are channels that do that, and those actually,
those tend to make a lot of money, oddly enough. Advertisers are what drives the revenue, right?
So if you're, let's say, reviewing laptop computers, the companies that make laptops know that the
people who are watching that are about to buy, so they're willing to put more into advertising to
them. I don't, but there are people who do. Do you know Julie Nolke? No, but she's hilarious, isn't
she? How does the algorithm work? Yeah, that's a question for the ages. You know, people try to get
into the intricacies and figure out every little detail. What really matters and the part that will
help you understand everything else that goes on is figuring out what the algorithm is supposed to
do for YouTube. YouTube's algorithm is designed to keep you on YouTube as long as possible, so
ads can be shown because they get money for the ads too. Remember that. That's what it's there
for, to keep you watching as long as possible. The other stuff, it's all based on what videos
you've watched in comparison to what videos other people have watched and how it drives engagement.
That's what nobody really knows. It's magic. It's magic. My brother said YouTube is dead and Twitch
is the new YouTube. So Twitch is alive. It's a very different dynamic. I know some people on Twitch
and most of them seem to like it. I would imagine this is about video games, but it's a different
type of content. It's live. It's interactive on Twitch. YouTube is for more prepared content.
I don't know that they're, I know they're seen as competitors, but I don't know that they really
are. Let's see. How many views do you get per day and how many subscribers do you have? I have
a little more than 600,000 subscribers and views range between, I don't know,
200,000 and 350,000 a day, somewhere in there.
How long do you spend on post-production? What's post-production? My channel, my main channel,
I record the video, the card comes out, I put it in the computer and upload it. The second channel
does have post-production. How long that takes? I don't know. I don't actually do it. I have a
friend that takes care of that. I have no idea how long it takes to edit these videos. A while
is what it seems. Let's see. How do you deal with toxic comments? I'm pretty thick-skinned.
From a creator standpoint, from this side of the camera, what matters is not so much how it makes
you feel, but how it makes the other people in your comment section feel. That's what you really
have to guard against. There are downsides in the sense that the internet can be vicious, you know?
So you can get just piled on by people. Just remember what it is that you're talking about.
You can get just piled on by people. Just remember what it is. Understand that most of them don't
really see you as a human. You're a character on a TV screen. I wouldn't take a whole lot of it
personally, easier said than done, but I do moderate my comment sections. There are certain
you know overt racism stuff like that that I don't even worry about replying. I just get rid of the
commenter. I think it's incredibly important to keep your comment section inclusive. If you do
that, you end up getting rid of a lot of the other stuff along the way. How many hours per week do
you work? I have no idea. A lot. A lot. There's, you know, for a lot of people this seems like a
job that is, that doesn't require a lot. It does. This is one of those things where you know people
are saying, I'm tired of working a nine to five. I'm going to start my own business. Well now you're
working nine to nine. I probably work eight to ten hours a day, seven days a week. So do you
live stream? Is it harder than recording videos? I personally think live streaming is way harder
than recording videos like this. I know people who feel the opposite though. I think that has
a lot to do with your personality. How do you come up with your ideas for videos? My channel is about
news for the most part. So I get my ideas from the news cycle and try to tie them to historical
ideas or philosophical concepts and go from there. Okay, so those are the questions from the
students. We'll now move on to the ones from Twitter. Again, these I haven't looked at, so
maybe screen this before you show it to the students. Okay, how many other politically
oriented YouTube content creators have you interacted with privately? Way more than you
would imagine. Any comments about any of these interactions? No. Normally if somebody says
something to me in private, it stays private. I think generally what people are like what we're
talking about, most times it's something like, hey, here's this story, here's this angle on this
story, it doesn't fit my channel for whatever reason. You take it. Something like that. How do
you deal with having to listen to your own voice? I'm referring to the well-known phenomenon where
you sound differently to you than you do to anyone else, not implying you are anything but melodious.
So I normally just watch the video once after I make it to make sure that I didn't mess something
up. So I don't listen to myself often. Recently I had a friend here who does the video editing
and they were staying here and they were working on something and I kept hearing it and yeah,
they wound up having to like work on that in their room rather than in their room.
I couldn't listen to myself. It also is weird because I can tell what mic was used
by the way I sound. So how much of an absolute accident was all of this for you? A huge accident.
None of this was intentional. It started as a joke and then when I realized I had a platform
that I could do some good with, I used it. Even accounting for the absolute accident,
do you think that would have happened if you were clean shaven? No, no I don't. The videos
that kind of propelled this channel, there's a contrast between what you expect to hear from
somebody who looks like me and what's in the video. And that contrast, that subverting of
expectations is what I think drove the channel initially. I think that's what
really got the ball rolling. Compensation is a big thing to consider. It might be helpful to note
how that is balanced so you are not forced into the position of needing to do sponsored videos.
Yeah, when it comes to money on YouTube, it's sporadic. Mine at this point, it's kind of
leveled off because I have a very consistent viewership and a consistent output of videos.
I have no idea those people who like to watch my videos, who like to watch my videos,
who like to watch my videos. I have no idea how they manage their finances. If that video's bad
and it doesn't get views, or whatever, they don't have more content for a week.
That really disrupts what they can expect to make in a month, which is what I do.
really disrupts what they can expect to make in a month,
I literally have no clue how they do it.
With me, I put out at least 14 videos a week,
so if one of them is bad, well, one of them's bad.
You know, it's not a huge deal,
but if you are looking for financial security and stability,
YouTube probably isn't the route for you.
A lot of people will go through and do sponsored videos,
which is funny, because I've been joking about this lately.
It doesn't fit with my channel, my format,
so I don't do them.
But if you looked back over the years,
what milestone took the longest to hit
as far as subscribers?
A million, because I'm not there yet.
So that's actually my goal, is to get to a million.
But really, my channel,
it went from zero to like 60,000,
and then another video popped and it went over 100,000,
and then it was kind of a steady growth up into 500,000,
and then it kind of slowed down.
So I haven't hit the milestone
that's taking the longest to hit yet.
How do you deal with the haters?
I don't care.
I don't, I try not to put too much stock in it.
I try not to let it get to me.
I wonder if the students will be curious about George.
I don't know.
It doesn't seem like a lot of them know who I am.
So I love your ever-changing array of T-shirts
and how they frequently seem germane
to the topic you're discussing.
Is this purposeful or a happy coincidence?
It's on purpose.
I go out of my way to kind of match them up.
Again, it started as a joke,
and these were just little things that I put in
to make it funny,
and then it became a tradition on the channel.
Let's see.
How much time goes into each video?
Average prep time, research, practice, take, set up,
video post-production, wow.
I have no idea, because the last part to that
says post-production, posting, and monitoring engagement.
No clue.
Generally, if things go well,
I can record one of these videos in one take.
And I really just kind of sit around,
think about the subject that I'm going to talk about,
try to come up with a good angle,
and then kind of get a good idea
of what I'm going to say in my head.
This takes about 15 minutes, sometimes an hour,
and then I record it.
Monitoring engagement is hard to judge.
I do that a lot all the time,
but it's all of the videos at once.
I don't know how to break it down to a per video thing,
but hours of my day are spent kind of monitoring engagement
and talking to y'all.
How has the Target demo changed
since your work model changed in the last year?
Do the algorithms still send you the same number of views?
Where do you get most of new views from these days?
And what do you do to grow that?
I focus on the content.
You know, my Target demographic has always been
people who are not necessarily aligned with me.
That's who I try to reach.
So I tailor my content to try to get it in their world.
That's why my titles are weird sometimes.
And that's why I'll avoid certain terminology
and stuff like that,
because I'm trying to get people away
from something that I see as dangerous.
As far as the views, that hasn't changed.
The algorithms still put things out.
And if you don't know what this question is about,
for a long time, you had no idea how many videos
you were going to get per day
or when they were gonna be published.
I was publishing videos at like midnight.
I would cover the news as it broke.
Now I have kind of settled into a semi-routine.
I say this as I broke with it yesterday.
But for the most part,
I put out a video in the morning and a video in the afternoon.
But sometimes that changes.
Is there a difference in the reception and reaction
to your channel overseas compared to the US?
Oh yeah, huge.
If so, how does it differ?
Depends on the location.
You know, I get a lot of views in Japan,
Korea, Germany, the United Kingdom.
Those places all have US military installations.
And then I get a lot of views
from outside the United States from Canada.
I get a lot of them from Western Europe in general,
a lot in Australia.
I think for many in Europe,
there's the, again, there's that contrast.
When you look at popular media,
if you've never been to the Southern United States,
and if you're just looking at what you see in movies
or whatever, if you see somebody who looks like me,
you're expecting dueling banjos.
So the content that goes along with my appearance
probably drives a lot of that.
What education, occupations, personal experiences
led to the breadth of knowledge you possess now,
which you draw upon for your videos?
What do you do to stay educated, informed now?
What motivated you to share your knowledge perspective
on YouTube and when did it take off?
Okay, so my,
I had a lot of jobs,
but most of them boiled down to taking information
and distilling it and presenting it to people,
which is what I do now.
I stay educated.
I stay informed by reading constantly.
I'm pretty much all, if I'm not recording,
I'm not doing family stuff,
I'm pretty much always reading.
So what motivated me?
Again, yeah, it started as a joke.
Initially, there was no grand plan.
It was just me making fun of myself.
Let's see, and when did it take off?
It, early on in my channel, I would say within,
by the time I had put out the fifth video,
I had 60,000 subscribers.
And that was really, that was,
most of that was based off of one that was a joke.
And then it blew up later with other videos.
Do you have more videos than shirts yet?
Do you use the same mental system
to keep them all straight?
Periodically, I'll see you reference past vid
and vaguely remember it,
but sometimes finding the specific one is a pain.
Yeah, so a lot of my titles are geared
to get that video into my target demographic.
So that makes them hard to find later.
I have the same issue.
I don't have a good system for it.
My wife remembers like most of them.
And yeah, I'm at the point now
where I think I have close to 2,000 videos.
There's at least one instance
where I had made a video like a year and a half ago,
and then I made pretty much the exact same video again
because I was like, hey, that's a good idea,
and didn't remember making it.
So it, yeah, it's confusing for me too.
I don't have a good system for that yet.
How do you see Bow as a brand?
Yeah, okay.
Strictly as a business,
do you see the videos as the draw
that brings them to the real revenue streams?
Have you ever thought of supplementing it with writing?
If other revenue streams took off,
what percentage would continue to be just videos?
The videos are the main revenue stream for me,
or for the brand, I guess.
That's so weird.
I don't actually see it as a business.
I see this as more of personal activism
that is self-funding.
I don't, I know that's weird.
It runs as a business, but I don't,
I've never viewed it as something where,
like, I honestly have no idea how much we make.
That's not what I focused on.
So we, you know,
my goal with this was never about money.
You know, from the beginning it was a joke,
and then I had the platform, so I wanted to use it.
The fact that it got to the point where we can have
a group of people who work on this is just amazing.
Have you ever thought of supplementing it with writing?
Yes, I'm actually working on that now,
but it's the project going on in the background
when I have time.
But yeah, there will be books, plural, coming out.
If other revenue streams took off,
what percent would continue to be?
I will do this forever.
I love this.
As long as I feel like it's making an impact,
I'll keep doing it.
So, will the,
how much time average do you put in researching the facts,
and how do you know they are facts in a Googled world?
Normally, it's not actually hard
to discern fact from fiction,
as long as you set your biases aside.
That's what matters.
When it comes to in-depth topics
that are related to science,
go with the latest peer-reviewed research.
Go with that, and then talk to people
who really understand that topic.
You know, the videos that I have on economics,
understand those videos aren't really my opinion.
I have three people that I know who are economics whizzes.
I talk to them and combine their opinion.
I don't know much about that.
So, I rely on people who made that their field of study.
Let's see.
What color are your shoes?
Not wearing any is not an answer.
I have a black pair, a brown pair.
I've got a pair of cowboy boots.
I don't really know how to answer that.
Let's see, should address how best
to handle commenters and trolls.
Yeah, I mean, commenters,
people who are commenting, engage,
and beware of the idea of just,
of only responding to negative comments,
because that's something that I've seen a lot of people do.
Respond to the people who enjoy your stuff, too.
And as far as trolls, people acting in bad faith,
just get rid of them.
You don't need them.
When to turn off comments on a video.
I've never done that.
So, I don't know.
But mine is, again, my content is supposed
to provoke thought and discussion.
So, I don't, if people are arguing with it,
I don't necessarily mind.
Students should ask themselves,
do you want to put that much of yourself out there?
Will you be a solo act, or will you be better off
teaming up with friends?
Yeah, that's an important thing for people to decide,
is just to understand that once you start this,
for many people, you become public property.
And there is a lot of downside that goes with it.
I'm not saying don't do it.
I'm just saying make sure you're aware
of what you're walking into.
Think about the way you view people in the media,
and you have this image of how they are,
and you've probably never met them.
And you may base your interactions with their social media
on this perception of how they are.
And that may not really be them.
Please say something about accounting taxes, expenses.
Yeah, okay.
So if you do actually start making money,
make sure that you take a third of whatever you're making
and putting it aside.
Just put it aside in an account,
because the IRS is gonna want that.
They're gonna want their cut.
And then when it comes to the things you buy,
a lot of it is deductible from the taxes.
A lot of it is tax deductible.
Just be realistic about it.
You know, your cameras, sure.
If you make videos dealing with video games,
maybe a new console might actually be tax deductible.
However, your new Mustang
that you're gonna use in a video once, yeah, no.
And don't try to do that,
because the IRS will get really mad.
Definitely be aware of that,
and be aware that you can get in a lot of trouble
if you don't take care of the tax aspect of it,
because it's not deducted from your paycheck
like at a normal job.
Wow, that's now constituting a career.
Yeah, so yeah, YouTube is now apparently a career.
And I guess it is.
I mean, I never, yeah.
So there's that.
Just for the students, if y'all are still watching this,
this is surprising.
It's a surprising development, I think, for a lot of people.
Let's see.
Is being established enough on social media
to allow your work, putting out a message,
just as you see fit, to be self-sustainable,
if not profitable, a more mind-blowingly meaningful,
fun, and fulfilling form of career
than you could possibly have imagined beforehand?
Yes, yeah.
I mean, the reality is I had no intention
of putting videos on YouTube and had to be pushed into it.
Now, I will never stop.
It is great.
It's amazing.
It is incredibly fulfilling,
but from the career aspect of it,
it's probably a lot more work than most people imagine.
So what equipment software
would you recommend getting started?
Now, I will go with Canon cameras,
but that's a personal preference.
As far as software, I don't know anything about it.
They use Premiere, I think.
I think they use Premiere.
I could be wrong about that, though.
Any tips or tricks for dealing
with the dreaded YouTube algorithm?
Well, I haven't had that much issue with it
because of my content, because I keep it relatively clean,
because I don't engage in a lot of hyperbole.
I don't have issues with it.
My content doesn't really seem to get hit in any negative way,
but I've also read the rules.
That's another thing.
YouTube has these rules,
and they have the rules for the community,
and then they have the rules for the advertisers,
the advertiser-friendly guidelines.
Read them. Read them.
There are a whole lot of people who get on YouTube
and they get mad when what they did
was plainly against the guidelines that they put out.
They're pretty clear in most cases,
and it doesn't take a whole lot to stay within them.
So how much practice and preparation
and editing goes into one video?
So yeah, there's normally that time where I'm figuring it out
and then I start recording.
Normally, I get through it in one take,
but there are times when I'll get tongue-tied or whatever,
and that ends up...
It's basically one take,
or I'm not even getting through the intro,
and I've had to restart, and there's 20 takes.
So that varies wildly.
When doing research for your topics,
how do you vet your sources?
So what I tend to do is look at
whatever the item of information is
and see if you can duplicate that information
elsewhere on the Internet.
If you can find it somewhere else
that isn't related to this initial topic, you know...
I don't know, let's just say it's a jobs report or whatever.
This is what this outlet has interpreted it as.
Then you can go and you can look at the original,
and then you go look at other interpretations of the report
and see if they match up.
Try to get perspectives from all sides,
and then boil it down.
You know, it's not hard to find that
mutually agreed-upon fact in most cases.
There are some topics where people just deny reality,
but I mean, that's part of why I do it.
Was there one particular point in time
when your videos took off,
or was it a gradual increase in viewership?
I started following you when a politically opposite relative
posted a video of yours.
I skeptically watched it and was hooked.
So, the first video did pretty well,
and that got me, I want to say 60,000 subscribers,
something like that, over that first month or so.
Could be wrong about the timing.
And then I had, I put out a video.
Let's talk about what it's like to be black in America.
And it got tons of views.
I think in large part because,
look at me and look at the title.
You know, there were a lot of people
who clicked on it to watch it to be like,
yeah, let's see what this hillbilly's about to say.
And that is what really made the channel pop.
You know, again, that subverting of expectations,
that contrast had a lot to do with it.
I didn't know that at the time,
but looking back, that plays a whole lot into it.
And then from there, after that, it was pretty gradual.
Let's see.
This doesn't seem like a typical nine to five
with holidays and weekends.
What's the actual amount of time you spend per week on this?
Yeah.
Yeah, I don't know, 60 hours a week probably.
I don't know.
Even when you're not working though,
a lot of times it's in the back of your head.
You know, I could be at the ranch doing something
and I'm still thinking about it
while I'm building a hay feeder or whatever.
So it's always there because it's 24 hours a day.
You know, your channel is up and open 24 hours a day.
So it's always on your mind.
What's the startup cost?
You don't have to go.
You don't have to spend a lot of money initially,
as long as the content is good.
How does the revenue stream work?
So you have your normal videos, the ads,
and the advertisers pay per 1,000 impressions.
So 1,000 views, the advertisers pay for that.
YouTube takes a cut, you get a cut.
The live streams work the same way.
You know, people do super chats.
YouTube takes a cut, you get a cut.
Everything is worked on percentages.
So it is hard to figure out.
Now, once you get YouTube and you get a presence,
then you can do the merchandising, the Patreon,
and the other stuff that goes along with it.
But again, all of that stuff fluctuates very wildly
with the exception of Patreon.
The people who do the Patreon on this channel keep it running.
They make sure that it functions,
that all of the people who have signed up
to work on this channel and do the little stuff as far as,
you know, making sure that it gets to Facebook
and making sure it gets turned into an audio file
and put on Spotify and all of this,
they make sure that those people get paid.
So before you start expanding and bringing on employees,
I would definitely get a Patreon in order.
And then you can get it to the point where your job
as the person in front of the camera,
all you do is make content.
And then you don't worry about anything else,
and you can focus on making the content well.
Again, content is key.
That's what matters.
Is there any cheap free editing software?
I don't know.
I know that there's actually even an editor in YouTube
that you can use.
I don't know how good it is.
Let's see.
Napkin math on average, how many hours a day do you work?
Yeah, I'd say eight to 10.
I would say eight to 10 probably.
How far in advance do you plan videos?
Do you have a whiteboard filled with topics to cover?
I do.
My videos normally come out in arcs
where you'll have three or four videos about education.
You'll have three or four videos about foreign policy.
You'll have, and they all tie in together.
And that is planned.
That is very much planned.
And normally, if you're paying attention to the news cycle
every day, and you're reading the stuff beyond the headlines,
the in-depth articles, you can kind of guess
where the news cycle is going.
And you can prepare ahead of time and get that arc set up.
Is it hypothetically worth starting a channel just
to make a few smallish videos that say things
that you want to say, even if it never goes anywhere financially?
Yes, if you're going to do this, start as a hobby.
Start as a hobby.
And maybe it grows.
I would actually recommend that that's
how people view it, as a hobby that could potentially
turn into a job.
Going into this with the idea of this is going to be my job
is going to be really hard.
So how many messages do you tend to get from viewers?
How do you go about answering them all?
About 400 a day right now.
And how do I go about answering them all?
I don't.
I can't get to them now.
I used to be very good answering every single message.
Now I read them, but I don't necessarily
have time to respond to all of them.
And I don't know.
I've experimented with different ways
on how to try to get through it.
And I don't know how.
Well, we're still trying to figure that out,
some kind of workflow for that.
Is this what you wanted to be doing for a living,
or did you back into it?
At 12 years old, what did you want to be doing?
At 12 years old, I wanted to either be a journalist
or a medic in fifth group.
Now that's, so yeah, and this is where I'm at.
Have you ever met an unemployed plumber or electrician?
That's a great question for this.
If you're going to do this, make sure you have a trade.
Make sure you have something that you can fall back into,
some kind of profession outside of this.
One, it will help inform your videos and make you better.
Two, if you don't make the money on YouTube,
you have something to fall back on,
and you can still use this as a hobby.
Do you hear from people with whom you went to high school?
Sometimes, but I mean, I'm from a small town.
And I don't live too far from that small town.
I see some of them sometimes.
Do you ever watch the reply videos that people make
about you and your content?
Sometimes.
It depends on, like I'll normally click on one,
and within the first 30 seconds or so,
I know whether or not I'm going to watch it.
Or then sometimes just based on the thumbnail.
You know, I don't really have time
to watch reviews because I'm already
working on the next video.
Do you collaborate with others for developing your messaging?
Messaging, no.
Like as far as my ideal take on something, no.
I'm actually fiercely independent
when it comes to that.
Even people on the channel who work behind the scenes,
they know that it's like, hey, here are some ideas for videos.
And I may use them.
But as far as this is the message that should go along
with this idea, no.
Yeah, that doesn't happen.
My messaging is mine.
Now, what does happen at times is
if there's a universal message that a lot of people
can get behind, we may talk to each other and be like, hey,
we're all going to put out videos this week on this.
Or put everybody in one video with the idea
that brunch is canceled.
How does an independent YouTuber compare
to traditional journalism?
Well, I love not having deadlines.
There are some, but they're self-imposed.
I enjoy the ability to say in one piece, here are the facts,
what I think, and have my opinion separate.
That's not something that you should traditionally do.
I enjoy being able to do that.
I enjoy being able to frame things with my messaging
and hopefully spark discussion.
More so than with traditional journalism,
where you're just, if you're doing it right,
nobody should know what your opinion is.
And then again, when this started as it began,
I started to view it more as gonzo journalism, Hunter S.
Thompson type stuff.
And as it developed, people asked me, well,
where's the line between this and gonzo?
I'm like, I have no idea.
I don't know anymore.
I make the videos.
I've tried not to overthink it, because it's a new media.
It's a new form.
And there are a lot of rules from older media
that don't apply.
And it's just very different.
And it's constantly evolving.
How much of your work involves figuring out
what the algorithm wants to further expose
your videos to more people, or at least
avoiding being de-emphasized?
I, because this channel's clean, because the channel's clean,
because I don't try to inflame people,
I don't have problems with this.
It's one of those things where there are normal cycles.
I promise you that by the end of this month,
there will be people that are saying YouTube
is like pushing them down.
No, it's January.
It's January.
What happens in December?
A bunch of companies are running ads,
because it's in the lead up to Christmas.
January, it always historically tapers off.
It drops.
Your ad rates are higher in December
than they are in January.
And because of all of this combining at once,
almost every January, there's people like,
YouTube has deprioritized me.
And like, no, it's just the normal drop that occurs here.
So will you do more TikToks?
Yes.
We actually have a TikTok channel.
I played with it, experimented with it,
decided we're going to do it.
But we're going to get the second channel up
running well before we add another thing to the plate.
Will you also do a question and answer with your wife?
At some point.
We've talked about that a lot lately.
So that's probably something that's coming.
How much of personal information is safe to reveal?
Does it make you more relatable?
I don't know.
I do not know the answer to that question.
I think it will be different for every single person.
You know, there are, when you put yourself out in public,
there are a lot of considerations
like that that you may not think about initially.
But you should definitely, you should.
You should be thinking about it.
You should decide how much of your life, your private life,
are you going to allow into the public.
So it's a decision that is going to be
different for each person.
My kid said she wanted to be a YouTuber.
I said, no, you want a few hundred nice people to like
your videos.
That's not the same.
Yeah, I get what you're saying here.
There becomes a lot, there's a line where it's all fun.
It's all fun and games.
It's just you and that first group of people
that find you on YouTube.
And it's all happy.
And then there's that moment, and I don't know the number,
but at some point, you cross over
from it just being fun to the point
where you become a target for those people who
don't like your content.
And the thing is, if you're looking
to do this as a job, that phase where it's just all fun,
that isn't profitable enough to be a job.
Do you ever have days that you don't want to make videos?
If yes, how do you get over it?
No, not really.
There are days when I have stuff I have to do other
than make videos.
So I have videos that are evergreen,
that don't relate to the news that I
can release on those days.
But I really do love doing this.
I've never been like, oh, I don't want to go to work today.
I think in the last two years, I may have had,
I don't know, five days off, as far as points where there
weren't videos going out.
It doesn't happen often.
Given how touchy the platform can be,
do you have backup plans in case things go
sour with YouTube in any way?
I know for a lot of people, that's a priority,
setting up that parachute.
It's not for me.
I don't believe I've drawn their ire in any way.
But I follow the rules.
And when you're looking at a lot of the stuff that
becomes subjective, the lens that I
think people are missing is, is this content
likely to hurt someone?
And that tends to be the defining line.
Is it likely to incite somebody?
Is it likely to convince somebody
to do something that might get themselves hurt?
Is the information so bad that it may impact their health?
Those seem to be the concerns that really get people hit.
And when you think about it and you take it out
of the framing that normally gets used of free speech
and all of this stuff, look at it from a company standpoint.
If they allow this content that they view,
can they be held liable?
And I think in a lot of ways, they're
trying to shield themselves from that.
And it's not just legal liability.
It's the reputation.
It's the brand itself that could be damaged.
This person said this thing on this platform,
and then these horrible things happened?
That looks bad on the platform.
I think a lot of what is getting construed
as them taking a political stance
is really just them protecting the platform.
But that's a big debate.
And to me, most times, not always,
there have been some where I've looked at it and been like,
I have no idea why they did this.
But most times, if you look at the platform itself
and you look at their guidelines and then
you look at the content that was put out by the person,
most times you can see it.
Most times.
There are exceptions to that.
But most times, you can be like, yeah, that's kind of messed up.
This might get somebody hurt.
And to me, that's what I've noticed
the line seems to be in addition to the normal community
guidelines.
Let's see.
OK, so that looks like everything.
All right, so if you're considering this for a job,
don't do it for money.
Don't do it for money.
Do it as a hobby and let it turn into something.
And be aware of the pitfalls.
It's a lot of work.
There are a lot of things that people
don't think about when it comes to being in public.
There are the legal aspects of the taxes and all of that stuff
that people generally don't consider until it's too late,
until that first year.
And then they go in to do their taxes.
And they're like, I owe them how much?
Make sure that you have all of this stuff taken away.
And the key part is to growing the channel,
making sure the content is what the people who
are going to watch your channel, your target demographic,
the content is what they want.
And then you got to get lucky.
You have to get lucky.
I'm the first one to say that it was luck.
There was a lot of luck involved with it.
Sure, I put in a lot of time.
Yes, I put out a lot of videos.
Yeah, I work hard and all of that stuff.
But there's a lot of luck involved with it too.
So just don't think if you don't take off immediately
that it's because you're doing something wrong.
You may be doing everything right.
And you just haven't got that magic video yet.
You haven't gotten lucky yet.
And I've seen people who were putting out great content, who
didn't take off fast enough, who then changed their content
and tried to make it more sensationalist or whatever.
And it basically ensured that they would never take off.
If you have your idea and you know what your channel is
going to be like and you've figured it out,
roll with that plan.
Roll with that plan and see what happens.
Don't give up too easily.
And yes, just as a reminder, I've
never met an unemployed electrician or plumber.
Make sure that you don't put all of your future ideas in this.
Have a backup plan.
Have two backup plans.
And that's good for just life in general.
If you have one, you have none.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}