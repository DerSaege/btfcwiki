---
title: Let's talk about Maus, Tennessee, and failing a test....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4iR_Fk760ZE) |
| Published | 2022/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- School board in Tennessee decided to ban the graphic novel "Mouse," set in a dark period of history and told through animals, mice.
- Author attempted to address objections in good faith, considering each argument.
- Some arguments against the novel, like a mouse with no clothes, are not made in good faith.
- Conservative circles' reactions to animated characters without clothes show the hypocrisy in these arguments.
- The objections are not about the novel's content or theme; it's about something else.
- Beau suggests that educators are hiding the failure to recognize rising authoritarianism in the country by banning such books.
- The decision to ban the book is driven by a desire to preserve closely held beliefs and avoid embarrassment for failing to recognize authoritarian signs.
- Beau believes the ban aims to keep students ignorant and incapable of recognizing authoritarianism.
- Banning books like "Mouse" creates a generation blind to the signs of authoritarianism, leading to long-term impacts.
- The real danger lies in creating a generation unable to identify and resist authoritarian rhetoric and actions.

### Quotes

- "Banned books are the best books."
- "Please do not take these arguments in good faith."

### Oneliner

Beau sheds light on the true motives behind banning "Mouse" graphic novel, revealing a dangerous attempt to keep students ignorant of authoritarianism signs.

### Audience

Students, educators, community members

### On-the-ground actions from transcript

- Challenge book bans in schools (suggested)
- Advocate for diverse literature in education (suggested)

### Whats missing in summary

The emotional impact and depth of analysis in discussing the dangers of banning books like "Mouse" and the implications for future generations.

### Tags

#BookBans #Education #Authoritarianism #CriticalThinking #Literature


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about what the author of
Mouse might be getting wrong.
And I say might be because I'm not 100% sure.
I saw an interview, and we'll get to all that.
If you don't know what's going on, a school board in
Tennessee, people who are, in theory, supposed to be
educators, they have decided to remove Mouse, to ban it.
Can't be taught.
If you don't know what Mouse is, it's a graphic novel,
critically acclaimed.
It's a comic book.
It is a comic book that is set in the most horrible period of
the 20th century.
And it tells the story through animals, mice.
The author gave an interview and seemed
to be trying to entertain the school board's objections
in good faith, trying to take each individual argument
and kind of mull it over in his head.
And I don't know if he was doing this simply
because he's in shock because of how it appears,
or he was doing it as a rhetorical device,
because that's an effective rhetorical device.
If you can take a bad faith argument
and destroy it while treating it as a good faith argument,
it works.
Some people do it so often that they
have a catchphrase for when they start to do it.
And if you don't believe me, let's just
pretend that it's true for a minute. It works. But I couldn't tell if that's what
he was doing. With the absolute most respect possible, if you are actually
entertaining these arguments in good faith, do not do that because they're not
being made in good faith and I can prove it. One of the arguments is that there's
There's a mouse with no clothes on, and that's a reason to get rid of this.
Sure, let's start a movement to ban Donald Duck, little weirdo running around with no
pants on.
Can you imagine the conservative pearl clutching?
They would be on fainting couches all week, and it's important to note that last week,
The talking point in conservative circles was that the animated M&M, the candy, well,
it wasn't hot enough because of the change of the shoes.
It's not real.
That's not a real argument.
It's not being made in good faith.
And then the language and the overall theme, no, it's not real either, I'm not buying
that.
Do a survey of students in that school district.
Ask them what language they hear at home.
Ask them what content their parents allow them to consume at home.
Ask them if they've seen Braveheart or The Patriot or any other three-hour movie that
Mel Gibson's been involved in.
I'm willing to bet they've seen them.
It's not about that either.
Those are excuses.
And it's not really about a reluctance to teach about that period in history either.
That's not what it's about either.
I don't think so anyway.
I think it has to do with something else.
See if students start learning about that period in Germany, they learn about what happened,
they want to know how it happened.
They want to know how an entire country went off the rails like that.
They want to know what moved the population to think this was okay or to stand by.
And when students start to learn about how, what do they find?
They find rhetoric that says it's okay to lock up your political enemies.
They find rhetoric that demonizes and others, people from different religions, different
ethnicities, different countries of origin, that they find that people wore an article
of red clothing to symbolize their loyalty to some fake strongman leader.
Yeah, if I was somebody who was in the conservative movement today, I wouldn't want that taught
either. When they read these books today, when they're reviewing them, they're
holding a mirror and they don't like what they see. And because they're
educators, they're doing what every student that ever failed did, hiding the
grade. They couldn't recognize the signs of authoritarianism rising in this
country. So they're just hiding the fact that they failed that test. They're not
hiding it from their parents, they're hiding it from their kids. That's what
it's about. I don't think it's a conscious decision on their part to say
hey we're going down this road, let's make sure the kids don't know. I think
they're embarrassed because they fail for it, because they didn't study it,
because they didn't pay attention and now they want to hide that fact. This
isn't about the past, it's about the future. It's about denying these students
the ability to recognize the signs of rising authoritarianism in this country
and they're doing it because they don't want to be embarrassed. That's my theory
anyway. Don't treat these arguments in good faith because they're not being
made in good faith. They don't care about any of this. They are not upset that the
students might be exposed to the concept of hanging. If that was the case, they'd
be wanting to ban the news from being shown in schools because I'm fairly
certain there were a whole bunch of people chanting about that recently.
Pretty embarrassing. It's pretty embarrassing. And that's what it's about.
It's about trying to preserve these closely held beliefs that they've
developed. And anything that runs counter to that, well you got to get rid of that.
We can't let our kids know that we didn't study enough. That we failed the
test that we couldn't recognize what was happening.
So don't teach it.
Keep them ignorant.
So there will be another generation incapable of seeing
what is all over the news and all over Twitter.
That's what it's about.
It is far more dangerous than just close-minded people because the impacts of it are long-term.
It creates a generation incapable of recognizing authoritarianism when it is plain and it is
out in the open because they don't see the rhetoric and they don't know the danger because
they won't teach them what happens if that rhetoric takes hold, if people get
moved like that, if a country goes off the rails like that. That's what it's
about. Please do not take these arguments in good faith. And if you're a student,
please remember, banned books are the best books. Anyways, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}