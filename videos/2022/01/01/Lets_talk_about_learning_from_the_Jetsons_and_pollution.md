---
title: Let's talk about learning from the Jetsons and pollution...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=quLe88pi9w8) |
| Published | 2022/01/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the history of the Jetsons and the idea of an ideal future in the 60s.
- Received messages correcting the misconception that the Jetsons lived in the sky due to pollution.
- Some viewers opposed reboots becoming "woke" with added social commentary.
- The Jetsons initially aired in the 60s, with no pollution-related storyline.
- Pollution and social commentary were added in reboots to raise awareness.
- People may be less accepting of new ideas as they age, influenced by tradition and peer pressure.
- Introducing social commentary in franchises can have long-lasting impacts and raise awareness.
- New generations may not be aware of the original story and see added commentary as part of the plot.
- Social commentary serves as a warning for potential societal consequences.
- Criticism arises from those resistant to change and those who view added commentary as performative.
- Despite criticism, social commentary in reboots can be impactful and shape popular culture.
- Changes and social commentary in franchises can work and influence views over time.

### Quotes

- "If you believe the Jetsons live in the sky because of pollution, you are probably a little bit more in tune to environmental issues."
- "Decades later, people still remember those messages. They become part of popular culture, and they shift views."
- "Changes and social commentary in franchises can work and influence views over time."

### Oneliner

Exploring the history of the Jetsons reveals the impact of added social commentary in reboots and the lasting influence on societal views.

### Audience

Viewers, Franchise Fans

### On-the-ground actions from transcript

- Analyze the social commentary introduced in entertainment and its impact on shaping views (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of the influence of social commentary in entertainment and how it shapes societal perspectives over time.

### Tags

#Jetsons #SocialCommentary #Franchises #Impact #EnvironmentalAwareness


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about the Jetsons.
Didn't expect that.
My 2022 bingo card is already missing.
The video that went out this morning,
I mentioned in it that the Jetsons came out in the 60s
and that was the ideal future that people saw.
That was the best that they could hope for.
My inbox quickly filled up with people telling me
I was wrong, that that wasn't the case.
The reason the Jetsons lived in the sky was pollution,
smog, a comet in one of them,
and then something about the Flintstones living below them
that I really didn't understand, to be honest.
A whole bunch, one even saying that,
I guess in one show that the Jetsons said
that they had never even seen the ground.
They'd never seen grass.
The one that really caught my attention though
was from somebody who had sent a message in the past
saying that they didn't like it when reboots became woke,
when they introduced social commentary,
when they changed the source material,
they should stick to canon,
it needs to be respected, so on and so forth.
I found all of this really interesting.
So, a little bit of history about the Jetsons.
The Jetsons first started being produced in 1962.
It was produced for about a year.
In 1963, they stopped making episodes.
It was still on air, reruns, you know,
and then it came back in 1985.
In the original versions, in the 60s,
there's nothing wrong with the ground, nothing.
I know this because there are multiple episodes
where they're on the ground.
You see the ground in multiple episodes.
There's nothing wrong with it.
The pollution, the smog, all of that,
it got added in reboots later.
Social commentary, they got introduced
to raise awareness of an issue.
The Jetsons became woke.
That's what happened.
Now, here's the thing,
is that we can actually learn something
pretty important from this.
The first is that there may be people
who complain about reboots becoming woke today
who were totally okay with it in the past.
And that probably has a lot to do with the fact
that generally speaking, people are more accepting
of new ideas when they're younger.
As we get older, you know, tradition
and peer pressure from dead people,
that kind of takes over.
So generally, people are less accepting of new ideas.
There are obvious exceptions to this.
So you have that.
And the other thing that you have
is a clear demonstration that it works.
If you believe the Jetsons live in the sky
because of pollution, you are probably
a little bit more in tune to environmental issues.
I mean, that makes sense.
You might be somebody who would watch a channel like this
that talks about it a lot.
It shows that when franchises introduce social commentary,
it can have impacts decades later.
People, a lot of people cared enough about this
to send a message saying, hey, no,
that they lived in the sky because of pollution.
It wasn't ideal.
Well, because a lot of times when people
are introduced to a reboot,
they're not aware of the story before.
That social commentary that gets weaved into the plot,
that's just the story to them.
They're not aware of what occurred before.
So it serves as a warning.
It shows very clearly, hey, you know,
if we don't do something, things could go really bad,
drastically alter our society, so on and so forth.
This probably had a pretty profound effect
on a lot of people.
I mean, it did in some ways.
We could show that because years later,
they care enough to message about it.
So when franchises do this,
understand that they're gonna get criticism
from the people who don't want anything to ever change.
But a lot of times they also get criticism
from people who want things to change,
but they look at these little changes,
this little social commentary, this little addition,
and they're like, performative.
It's not doing any good.
It does.
It works.
It works.
Decades later, people still remember those messages.
They become part of popular culture, and they shift views.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}