---
title: Let's talk about the future of 2022....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2eA02s3X0JY) |
| Published | 2022/01/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau welcomes viewers to 2022 and muses about fictional stories set in the current year.
- He contrasts two visions of the future: the Jetsons' semi-idyllic world and the grim reality portrayed in a story about a cop investigating a company.
- Despite advancements like smart watches, the world is plagued by issues like climate catastrophe, overpopulation, and extreme wealth inequality.
- Beau references "Soylent Green," a story from the same era as the Jetsons, depicting vastly different futures.
- Urging for immediate action, Beau warns that without addressing global inequities and environmental concerns, the future may resemble "Soylent Green" more than the Jetsons.
- He stresses the importance of choosing a course towards a better future and being prepared to adapt to inevitable changes.
- Beau encourages viewers to make 2022 a year of commitment towards a brighter future akin to the Jetsons' world.
- He leaves viewers with a thought-provoking message about the potential for positive change in the current year.

### Quotes

- "We're running out of time to figure out what kind of world we're going to have."
- "If we don't make serious changes, it's definitely going to look more like Soylent Green than the Jetsons."
- "All we have to do really is set a course for some idyllic future and work towards it."
- "We're going to have to adjust and adapt."
- "This could be the year it starts to turn around."

### Oneliner

Beau contrasts two visions of the future in 2022, urging immediate action to prevent a dystopian reality and work towards a brighter, more sustainable world.

### Audience

General public

### On-the-ground actions from transcript

- Commit to making a better future by taking concrete steps towards sustainability and equity (suggested)
- Advocate for policies that address climate change and wealth inequality (implied)

### Whats missing in summary

The full transcript provides a reflective look at past predictions of the future and challenges viewers to actively shape a more positive reality by addressing global issues and embracing change.

### Tags

#Future #2022 #ClimateChange #Inequality #Adaptation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So welcome to the future.
It is now 2022, right?
You know, as we move further into the future,
I always like to kind of look at fictional stories
and find ones that are set in the year we're in.
It's normally entertaining.
This year's kind of different because it gives us, well,
it gives us two competing options.
Do you know who was born in 2022?
George Jetson.
George Jetson.
In the 60s, people thought that's
where we'd be at by now.
This semi-idyllic world, cars that
fly and fold up into briefcases, working three hours a day,
three days a week, because everything else is automated.
That image of the Jetsons, that was the best
that they could imagine.
And oddly enough, a lot of that stuff has happened.
Smart watches, anyway.
That's what people were hoping for back then.
You know what else is set this year, 2022?
Story about a cop investigating this thing that
happened involving this company.
The world is not idyllic.
The oceans are dying.
There's climate catastrophe, overpopulation.
It's hyper-capitalist, the gap between those on the bottom
and those on the top, well, it has never been more pronounced.
Those at the top, well, I mean, they're
paying $150 for a jar of strawberry jam.
Those at the bottom, well, they're eating what they can.
And Soylent Green is people.
Two very conflicting visions of the future.
Oddly enough, the book that Soylent Green was based on
was actually written around the same time as the Jetsons.
Two very, very different futures.
And now we're there, and it looks nothing like either one.
The thing is, we're running out of time
to figure out what kind of world we're going to have.
Because if we continue down the path we're on,
it's definitely going to look more like Soylent Green
than the Jetsons if we don't make serious changes,
if we don't start to address some
of the inequities in the world, the whole world, not just
the US.
If we don't start being a little bit more conscious of this rock
that we're on, we don't have a choice.
We won't end up being able to choose.
It's just going to devolve.
You know, all we have to do really
is set a course for some idyllic future and work towards it
and accept the fact that things are going to change.
Things that we have known will change.
Systems that we have known will change.
We're going to have to adjust and adapt.
Or we don't, and we just hope we're
the ones on the top when the dust settles, right?
You know, 2020, for a lot of people, pretty bad year.
It's probably, historically speaking,
that that's going to go down.
It's a really bad year.
Now we're in 2022.
This could be the year it starts to turn around.
This could be the year that we commit
to making a better future, to charting that course
and getting somewhere that, well, gets us to George Jetson.
Or this can be 2022.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}