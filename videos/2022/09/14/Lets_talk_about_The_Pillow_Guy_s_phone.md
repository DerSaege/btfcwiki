---
title: Let's talk about The Pillow Guy's phone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AG2nt1Cb3Bs) |
| Published | 2022/09/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mike Lindell, noted pillow industrialist, faced a federal search warrant at Hardee's leading to sleepless nights despite his superior pillow products.
- The warrant seeks records on Lindell's phone related to violations including identity theft, intentional damage to a protected computer, and conspiracy involving several individuals.
- The listed violations involve individuals like Tina Peters, the MyPillow guy, and others known and unknown to the government.
- The investigation, starting at the state level, has now expanded to national figures like Lindell and Peters, who have been at the White House.
- Beau advises that once your name is listed in an investigation like this, contacting a good lawyer and learning how to cook with ramen might be wise steps.
- When federal authorities reach this stage, they likely have enough information to indict, showcasing a high level of certainty in their case.
- The evidence gathered seems to connect various levels of government and individuals from New York City to the White House.
- Despite seeming like a side investigation, all roads seem to lead to the same place, hinting at future relevance of the material.
- Beau suggests that viewers can take the evidence into their own hands and get an idea of its impact.
- The transcript concludes with Beau urging everyone to have a good day.

### Quotes

- "Despite his superior pillow products, he won't be sleeping easily any time soon."
- "If you see your name in a list like this, step one, contact a really good lawyer."
- "Generally speaking, once the feds have gotten to this point, they kind of already have the information needed to indict."
- "All roads lead to the same place."
- "You can take this evidence in your own hands."

### Oneliner

Mike Lindell faces federal investigation and advice on dealing with legal troubles; all roads lead to the same place.

### Audience

Legal observers, concerned citizens.

### On-the-ground actions from transcript

- Contact a really good lawyer (implied).
- Learn how to cook in a microwave using ramen (implied).
- Take the evidence into your own hands (implied).

### Whats missing in summary

Insights on the potential consequences of being involved in such investigations.

### Tags

#MikeLindell #FederalInvestigation #LegalAdvice #NationalProminence #WhiteHouse


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we will be talking about how sweet dreams are in fact not made of these,
and hardies, and searches, and an expansion of an investigation that we knew was occurring,
and a familiar cast of characters, and what it means for the future.
If you missed it, noted pillow industrialist Mike Lindell was at Hardee's when the federal
government decided to execute a search warrant for his phone. There is a reported copy of the
warrant floating around, and suffice it to say the information in it leads me to believe that
despite his superior pillow products, he won't be sleeping easily any time soon.
It says, all records and information on the Lindell cell phone that constitute fruits,
evidence, or instrumentalities of violations of, and it goes through and lists the laws,
identity theft, intentional damage to a protected computer, a conspiracy involving that, so on and
so forth. Those violations involving Tina Peters, Conan James Hayes, Belinda Neasley, I could be
saying that name wrong, Sandra Brown, Sharona Bishop, Michael Lindell, and or Douglas Frank,
among other co-conspirators known and unknown to the government. Since November 1st, 2020,
that's what they're looking for. That's pretty expansive. Tina Peters is somebody who we have
talked about on the channel before. She is a unique figure out in Colorado. The MyPillow guy
is somebody who was at the White House and at one point emerged holding a look like maybe notes
that detailed conspiracy theories, and Lindell has become a prominent figure in the election
denial industry. So a number of things about this. First, obviously, this is an expansion
of an investigation that began at the state level and now it appears to be reaching
figures who are at the national level. They're of national prominence who were at the White House.
And the other thing to note is, you know, this is just free advice for you. I am not a lawyer,
but if you ever see your name listed in the fashion that this was, if you see your name
in a list like this, step one, contact a really good lawyer. Step two, it's probably a good idea
to learn how to cook in a microwave using ramen. Generally speaking, once the feds have gotten to
this point, they kind of already have the information needed to indict. They are very
certain of their case once they start doing stuff like this. If you're a lawyer, you know,
I expect that you would feel pretty good that you've got a really strong opinion in this.
Along with this roughly data, and I do want to show that you can take this evidence in
your own hands, you can get a really good idea. In an alternative nascent form,
you can hear their delivery of the evidence for each public allegation from New York City and
at the state level to people in the White House. So while this may seem kind
of an unrelated side investigation to everything else that's going on, all
roads lead to the same place. So you will see this material again, I'm sure. Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}