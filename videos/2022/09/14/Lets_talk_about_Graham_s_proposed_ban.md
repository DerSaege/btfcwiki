---
title: Let's talk about Graham's proposed ban....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Pe4IpI3ysrI) |
| Published | 2022/09/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Lindsey Graham's proposal of a 15-week abortion ban is seen as surprising in light of current political trends.
- National politicians like Lindsey Graham are distancing themselves from extreme state bans with this proposal as a compromise.
- The 15-week ban is positioned as a way to appeal to moderates and independents while still supporting abortion restrictions.
- Graham's proposal, although framed as a compromise, is criticized for compromising on women's bodily autonomy.
- The goal behind the proposal is to allow Republican senators to cater to pro-ban supporters while maintaining an image of reasonability.
- Beau does not view this compromise on women's rights as acceptable or politically viable.
- He sees the proposal as a token gesture to find middle ground, which he believes is untenable.
- Beau criticizes the Republicans for being out of touch with the average American on this issue.

### Quotes

- "You showed us who you were. We believe you. There's no compromise to be had here."
- "We're not going to compromise or negotiate on women's bodily autonomy."
- "I don't see any compromise to be had here."
- "It just illustrates exactly how out of touch Republicans are with the average American."
- "I don't see this move as politically tenable."

### Oneliner

Lindsey Graham's 15-week abortion ban proposal is seen as a calculated compromise that compromises women's rights, illustrating the GOP's disconnect with Americans.

### Audience

Advocates for women's rights

### On-the-ground actions from transcript
- Advocate for women's bodily autonomy and reproductive rights (implied)
- Support organizations that work towards protecting women's rights (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Lindsey Graham's proposal and its implications on women's rights and political positioning.

### Tags

#LindseyGraham #AbortionBan #ReproductiveRights #GOP #WomenRights


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about Lindsey Graham
and his proposal and why his proposal
has come to be seen as surprising.
The general consensus, conventional wisdom,
has said that the Republican Party realized
they made a mistake with this whole thing.
And many candidates are trying to walk back their position
and distance themselves from that Supreme Court ruling,
and more importantly, distance themselves
from the decisions that state legislators are making
if they're on the national scene.
Those national politicians, they don't
want to be involved in that.
And then Lindsey Graham comes out like Leroy Jenkins
and introduces or proposes a 15-week ban, ban on everything
before 15 weeks.
And people are wondering why.
And the talking point is that the Senate
is just confused by it.
Nah, nah, I think this is pretty calculated.
The idea is that this is the compromise.
They see the extreme bans being put in place at the state level.
And this is the national politician's method of saying, look, I'm on your side.
I'm just not extreme, still trying to catch those moderates and those independents or
even the normal Republicans.
So what they're doing is they're proposing 15 weeks as the compromise.
We're not going to have a total ban.
We're just going to stop it at 15 weeks, you know, that's what we're going to do.
Now the reality is I think the majority of them occur before 15 weeks.
So I don't think that it's going to placate their more enthusiastic supporters of these
kinds of measures.
And as far as the independents and the moderates, you're trying to compromise on the rights
of half the population.
not a compromise on this. You showed who you were. You told us who you are. We believe
you. There's no compromise to be had here. That's the goal. The goal is to allow Republican
senators to vote for something and get their street cred among the people who want those
bands while still maintaining the image that they're reasonable. It's not
reasonable. You're taking away half the country's rights. We're not going to
compromise or negotiate on women's bodily autonomy. That's not going to be a
winning position. That's what I think it is. I think it's token. It's a way to
try to find the middle ground. Well, frankly, I don't want just a little bit
of authoritarian gunnery to placate a tiny, tiny group of people. I don't think
that we can sign away people's rights because of the personal beliefs of a
few. I don't see this compromise as something that is tenable. I don't see
this move as politically tenable. I think that it just illustrates exactly
how out of touch Republicans are with the average American. They saw what
happened in Kansas, they understand that things aren't the way they perceived, but
they still haven't realized how far away they are from what the average American
once. I don't see any compromise to be had here. Anyway, it's just a thought. Y'all
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}