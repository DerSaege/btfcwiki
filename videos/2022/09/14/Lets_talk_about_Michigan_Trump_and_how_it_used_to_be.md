---
title: Let's talk about Michigan, Trump, and how it used to be....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TsJjESPXixI) |
| Published | 2022/09/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about a tragic incident in Walled Lake, Michigan involving a man who fell into a conspiracy theory and opened fire on his family.
- The man's daughter managed to escape and shared how her father deteriorated and became obsessed with election conspiracy theories.
- Former President of the United States shared a photoshopped image of himself with symbols associated with the conspiracy theory after the incident.
- Beau points out that this incident is not the first of its kind but is particularly close to a tragic event.
- Beau criticizes the Republican Party for either following along or turning a blind eye to the dangerous dissemination of conspiracy theories.
- This incident should be a wake-up call for the Republican Party to realize how far they have gone.
- Beau urges the Republican Party to do some soul-searching and address the dangerous influence of conspiracy theories within their ranks.

### Quotes

- "This isn't the first time that this has happened, but this was just in such close proximity."
- "There should probably be a moment in which the Republican Party realizes how far it's gone."
- "The Republican Party has some soul-searching to do, and they need to do it quick."

### Oneliner

Former President's promotion of a conspiracy theory after a family tragedy prompts Beau to urge the Republican Party to confront their dangerous path.

### Audience

Republicans, political observers

### On-the-ground actions from transcript

- Call for the Republican Party to self-reflect and address the impact of conspiracy theories within their circles (implied).

### Whats missing in summary

The emotional impact of the tragic incident and the urgent need for accountability within political parties.

### Tags

#Michigan #ConspiracyTheories #RepublicanParty #PoliticalResponsibility


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about the way things used to be
and something that happened in Michigan
and something that the former president of the United States
decided to share on his little social media network.
and what it says about the Republican Party.
So in Walled Lake, Michigan, a 53-year-old man,
according to his daughter, became obsessed.
Fell down a rabbit hole, an information silo.
Became obsessed with a conspiracy theory
that's symbolized by a letter.
He had an argument and opened fire on his family.
His wife, the dog, did not make it.
The daughter managed to crawl out the front door.
He then engaged law enforcement and he did not make it.
Another daughter was reportedly at a birthday party.
She's made a statement basically talking about how it happened
and how he deteriorated and how he became obsessed with the election
and the wild claims that went along with it.
Since that happened, the former president of the United States
shared an image on social media of himself.
The image had been photoshopped,
so he is wearing a lapel pin of the letter Q,
and it has the slogans associated with that theory.
After that happened, the story broke.
The news is everywhere about what happened in Michigan,
and the former president of the United States
shared a photoshopped image of himself reinforcing that theory
after it led to disaster.
Again, this isn't the first time that this has happened,
but this was just in such close proximity.
At any other point in American political history, this would be it,
and there have been so many moments like this.
This would be it.
This would be a career-ending scandal,
and this doesn't even touch on everything else
associated with Trump and Trumpism,
but the Republican Party is either following along
or looking the other way.
People who were upset about a tan suit not too long ago
are looking the other way at something like this,
reinforcing that theory, putting those slogans out there.
There should probably be a moment
in which the Republican Party realizes how far it's gone.
This would be a good one.
This would be a moment to recognize
that the leader of the Republican Party,
the person exerting the control in the aftermath of the event in Michigan,
put out a image that had been Photoshopped of himself
that furthers the message that the daughter believes is responsible.
The Republican Party has some soul-searching to do,
and they need to do it quick.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}