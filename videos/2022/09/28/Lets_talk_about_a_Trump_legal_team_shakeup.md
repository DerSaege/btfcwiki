---
title: Let's talk about a Trump legal team shakeup....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oXEZTWc4Sc4) |
| Published | 2022/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's legal team saw a shakeup recently, with a high-end Florida lawyer named Kyes reportedly being paid $3 million in advance.
- Initially thought to be on the Espionage Act case, Kyes has now been benched according to CNN, leading to speculation about his new role.
- Potential cases for Kyes include the January 6th grand jury in DC or the New York case, but using a Florida heavy hitter in New York seems odd.
- Speculation arises about Kyes' ethical stance, with insinuations that he may not be willing to participate in Trump's legal tactics.
- CNN reports Kyes has been taken off the lead on the Espionage Act case, while Team Trump insists his role remains unchanged.
- Team Trump refutes CNN's reporting, hinting at a discrepancy in perspectives.
- The uncertainty around Kyes' role in Trump's legal team fuels speculation and leaves the actual situation unclear.
- The outcome of this legal team shakeup remains to be seen, with implications that this development may resurface later on.
- Beau concludes by leaving the situation open-ended, suggesting that time will reveal the truth behind the reported changes.
- The shakeup in Trump's legal team and Kyes' altered role spark curiosity and debate among observers.

### Quotes

- "Trump's legal team saw a shakeup recently, with a high-end Florida lawyer named Kyes reportedly being paid $3 million in advance."
- "The uncertainty around Kyes' role in Trump's legal team fuels speculation and leaves the actual situation unclear."
- "Either way, this is probably one of those things that you're going to see this material again."

### Oneliner

Trump's legal team sees a shakeup as high-end lawyer Kyes is reportedly paid $3 million but then benched, sparking speculation and uncertainty.

### Audience

Political analysts, legal enthusiasts

### On-the-ground actions from transcript

- Monitor developments in legal teams (implied)
- Stay informed about political and legal updates (implied)

### Whats missing in summary

Insights into the potential implications of this shakeup and its impact on Trump's legal defense strategy. 

### Tags

#Trump #LegalTeam #Shakeup #Speculation #Kyes #CNNReporting


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about a shakeup
on Trump's legal team, because CNN is reporting
that there have been some changes
and it is fueling a lot of speculation.
And it is pretty interesting if you really think about it.
Okay, so not too long ago,
Trump obtained the services of a high-end Florida lawyer
named Kyes.
He was reportedly paid 3 million in advance.
Conventional wisdom was that he would be
on the Espionage Act case.
When he was brought on,
I asked a friend of mine who's a lawyer about him.
And this was a few weeks ago.
I don't remember the exact wording,
but he used a boxing analogy,
something along the lines of, you know,
he's somebody you want to go up against
because you want that win under your belt,
but also you don't want to go up against them
because they're really good.
Very complimentary take.
CNN is reporting that he's been benched,
that he is not leading that case anymore.
Now the question is, what's he doing, right?
Now, because it is Trump,
there are a lot of opportunities for employment for a lawyer.
But the most likely cases for somebody like him
would be the January 6th grand jury up in DC,
which we haven't seen him heavily involved in,
or the New York case.
But it doesn't really make a whole lot of sense
to use a Florida heavy hitter in New York.
So that brings up the speculation.
To obtain a very complimentary take from the people
that he's getting them from,
it seems unlikely that Kais would be willing to participate
in some of the legal shenanigans that Trump enjoys.
You know, insinuations rather than assertions.
It could be that the lawyer is just too ethical
to be on Trump's team.
Again, it's speculation though.
Nobody really knows what's happened.
Other than CNN is reporting
that he has been kind of taken off the lead
on the Espionage Act case.
Now, for the Team Trump take on this,
it is worth noting that they say,
Kais' role as an important member
of President Trump's legal team remains unchanged.
And any suggestion otherwise is untrue.
I mean, okay.
So Trump's spokespeople are refuting the reporting of CNN.
I guess we'll just wait and see what happens in court.
Either way, this is probably one of those things
that you're going to see this material again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}