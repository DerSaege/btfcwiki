---
title: Let's talk about who hit Nordstream....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=T49_4Z1L3-4) |
| Published | 2022/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nord Stream 1 and 2 pipelines have faced intentional difficulties, with officials from Norway, Sweden, and Denmark suggesting deliberate sabotage.
- Motives for the pipeline interference include Russia testing capabilities and leveraging sanctions, countries seeking to limit Russia's influence, and entities looking to benefit from the disruption.
- Various entities, including countries in the region and external actors, could be responsible, but the true perpetrator remains unknown.
- Initial reporting indicates intentional disruption, but Beau advises against firm conclusions until concrete evidence emerges.
- Beau humorously suggests whimsical theories involving fictional characters like the Little Mermaid or Cobra, underscoring the lack of definitive information.
- The mystery surrounding the pipeline attack requires patience and open-mindedness to avoid prematurely adopting unverified theories.

### Quotes

- "Date these opinions right now. Date these theories."
- "Don't set yourself up to go ahead and start believing one until there's real evidence."
- "The answer to the question is, we don't know yet."

### Oneliner

Nord Stream pipeline sabotage triggers global speculation on motives, urging cautious consideration of diverse theories until concrete evidence emerges.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor developments in the Nord Stream pipeline situation (implied)

### Whats missing in summary

The transcript underlines the importance of critical thinking and patience in the face of uncertain geopolitical events.

### Tags

#NordStream #PipelineSabotage #Geopolitics #GlobalCitizens #CriticalThinking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, I guess we're going to talk
about who hit that pipeline.
I mean, that seems to be the question on everybody's mind,
at least according to my inbox.
And I get it.
It's interesting.
It's a international mystery.
Everybody wants to know who done it.
And it's good because it can kind of lead us to something.
Now, if you don't know, if you missed it,
Nord Stream 1 and 2 have suffered
technical difficulties.
These are pipelines moving gas from Russia to Europe.
That's what they're there for.
Those pipelines have been less than active recently because
of the situation over there.
Norway, Sweden, and Denmark have all had officials describe
the difficulties as deliberate or sabotage.
Russia said that no option can be ruled out.
Initial reporting suggests that it was intentional,
that these pipelines were intentionally disabled.
So who did it?
Treat it like any other crime, who has motive?
And when you're talking about that international poker game
where everybody's cheating, the answer is, well,
pretty much everybody.
Pretty much everybody has a motive for this to happen,
if you're being honest, because you have to look at all
of the various angles.
Let's run through some of them real quick.
Russia.
Russia did it to themselves.
Why would they do that?
Proof of concept to show that they could because a new
pipeline just opened up.
At the same time, it puts them in a situation where come
this winter, they could be like, hey, we'd love to help you,
but sanctions.
They're in the way.
Maybe y'all should ease up on those.
Any country involved with a new pipeline has a motive,
because if the other pipelines are just not there,
probably be a whole lot easier to get investment for more
or to expand your own.
The US or any other country in Europe
that wants to make sure that Russia doesn't
have leverage over Europe.
Everybody has a motive.
If this is one of those mysteries where we're all
hanging out in the library, the only person that gets to leave
is Germany.
They're the only people, the only player
that doesn't have a motive.
And that's just countries in the region.
There's nothing to say that it couldn't be an outside country
that maybe is looking to send gas to Europe from the south.
There's also nothing to say that it's not a corporate entity,
because right now, we don't know.
We know nothing.
I would also point out that it is initial reporting.
Generally speaking, and I have videos about this,
I don't trust initial reporting, because it's often wrong.
I mean, that being said, I have looked at some of the stuff.
And yeah, I mean, that looks pretty intentional.
But the reality is, right now, we don't know.
We don't know.
You have people all over the world
we don't know, you have people all over social media
expressing their opinions, their theories.
And all of that's fine, as long as you
don't get married to one.
Date these opinions right now.
Date these theories.
Don't set yourself up to go ahead and start believing one
until there's real evidence.
Now, as far as I'm concerned, who do I think's behind it?
My best guess, and I think the most obvious answer,
is that it's either the Little Mermaid upset
that people are still freaking out about her new movie,
or it was Cobra, who has a long history of attacking energy
resources, probably hired the dreadnoughts.
And as you laugh, I'd like to point out
that those theories have exactly the same amount of evidence
as any other.
The answer to the question is, we don't know yet.
So we're going to have to wait.
And we'll just see how it plays out,
and we'll continue to cover it.
But don't set yourself up to be 100% behind any theory yet.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}