---
title: Let's talk about Putin, Russia, and mobilization....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BZu4q6Cwg2c) |
| Published | 2022/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Supreme Leader Putin promoted himself to theater commander, leading to potential strategic mistakes in the war effort.
- Units near Kherson were told not to withdraw, indicating a possibly untenable position for the Russian military.
- An attack on a recruiting station in Russia is linked to resistance against conscription.
- There is a debate in Western countries about accepting people wanting to leave Russia.
- Suggestions to allow individuals to leave Russia are met with some resistance, but exemptions could be made.
- Keeping potential troops off the battlefield is a smart move, both strategically and economically.
- Most individuals with means are unlikely to be conscripted due to historical patterns seen in imperial powers.
- Encouraging those wanting to flee mobilization could decrease morale and destabilize Putin's position.
- The goal of Western sanctions and pressure on Russia was to destabilize Putin as a leader.
- The departure of individuals trying to flee could have economic repercussions, contributing to the intended destabilization.

### Quotes

- "Sending a bunch of incredibly spoiled, entitled people who will probably not be on the front means that they're going to be around the Ukrainians they're occupying."
- "It's bad for the economy. It creates brain drain. It creates upset mamichkas."
- "I understand the desire to say, no, y'all caused this. Y'all supported it. You have to deal with it."

### Oneliner

Supreme Leader Putin's strategic missteps, resistance to conscription, and Western debates on accepting fleeing individuals contribute to potential destabilization in Russia.

### Audience

Global citizens

### On-the-ground actions from transcript
- Facilitate the departure of individuals wanting to flee mobilization (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the internal situation in Russia, focusing on Putin's decisions, resistance to conscription, Western responses, and the potential economic and destabilizing effects of fleeing individuals.

### Tags

#Russia #Putin #Conscription #WesternCountries #Sanctions #EconomicImpact


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Russia,
mainly the internal situation and people wanting to leave
and what Western countries should do with that,
considering the long-term objectives
and what they have been trying to do.
We'll kind of go through that.
But before we get into that,
we'll touch on a couple of other things.
The first is that Supreme Leader Putin
has decided to promote himself to theater commander,
and he has taken a more hands-on approach
to strategic decisions involving the war effort.
Yeah, cue downfall memes now.
This generally fails, pretty much always.
It is reported that Putin told units near Kherson
that they couldn't withdraw.
Incidentally, that means that the Russian military
in that area wanted to retreat,
which means their position is probably not tenable.
This is probably going to lead to more mistakes
with being overly aggressive
and perhaps Putin overestimating
the capabilities of the Russian military again.
There was an attack on a recruiting station in Russia.
The conventional wisdom says it has to do
with the mobilization effort, and yeah, that makes sense.
People tend to not like, you know,
being drafted, conscripted.
So I would be surprised if that was the last one.
You have a bunch of people trying to leave Russia,
and there is a discussion in the West about what to do.
Some countries don't want to take them.
Some do.
There's a lot of discussion about this on social media.
Yes, let them leave.
Help them leave.
Absolutely.
Yes.
Now, there are several responses to that,
people who don't want to allow them to leave.
Most of them don't really hold up to scrutiny.
One does, but it's easily resolved.
The one that does is the fact that the countries
closest to Russia shouldn't have to shoulder
the burden of this alone.
But in Europe, basically, the first country
you come to to claim asylum, that's
the one you have to claim asylum in.
However, that's not like a law of physics.
That's something that can be changed.
An exemption could be carved out.
I mean, me personally, I think this is bad all the way around,
and it should just go away.
But it's not my decision.
But an exemption for this particular situation
seems to make sense.
It's just words on paper.
They can be changed.
Another one of the responses is from people
who want to keep them there because it's not
like they opposed the war.
They just don't want to fight in it themselves.
They didn't have a problem with what was happening
until they might have to go fight.
I mean, yeah, that's most imperial countries.
That's really common.
So here's the thing about that.
It's always the right move to get potential troops
off the battlefield.
That's always the smart move.
You want them not there fighting.
And if you can get them somewhere else, that's smart.
And it's certainly way cheaper to deal with them
as asylum seekers than it is to deal with them
on the battlefield.
And that's not even including the whole cost
in lives thing, which should factor into this
by my way of thinking.
But that kind of stems from a desire
to want to kind of punish the Russian populace for this.
And I get it.
I understand it.
But again, foreign policy, don't think
about morality or justice or anything like that right now.
That doesn't have anything to do with it.
Aside from that, the reality is most of these people
aren't going to be conscripted.
Most of these people will not be the ones who get drafted.
I think the easiest way to illustrate
this is let's say that conscription in your country
starts right now.
How long does it take for you to get the money together to flee?
Probably not immediate, right?
Probably not 24 hours, 48 hours.
Yeah.
These people are people with means.
They're people with money.
They're people with power coupons.
Those power coupons will get them out of the mobilization.
They're just scared.
And they have reason to be scared,
because the odds are, yeah, if they get called up,
given some very limited training and substandard equipment,
and then get sent to Ukraine, yeah,
the Ukrainian army is going to turn a whole lot of them
into mist very, very quickly.
They have reason to be scared.
But realistically, they won't be called up,
because Russia is like any other imperial power.
It's those with means get exempted
from a lot of the burden of it.
Think to the United States in Vietnam.
Was it black people, poor white folk?
They got those deferments?
Same thing over there.
That's why you're going to see, well, you are seeing,
the most dramatic protests are in Dagestan, Chechnya,
places like that.
The outer ring of the imperial state,
where they will pull people from and ship them off.
So realistically, you're not really
keeping people off the battlefield,
because the rich folk, they're not going to end up going.
However, if they were to leave the country, well, they're gone.
They're not there.
What do rich 20 and 30-year-old people typically have?
What about Popichka and Momichka?
They probably got money too.
And Momichka knows that she's not
going to see her little boy until that war's over.
That means you have people with money
who have a reason for the war to end quickly.
People with money who would become
more opposed to Putin's war.
That seems like a good thing.
And then there's the idea that they need to stay
and oust Putin.
I don't think that the people terrified of combat
and the people who would stage a coup,
I don't think that's the same demographic.
I do not think that that Venn diagram is a circle.
Those who are running are probably not
the people who would go toe to toe with Russian security
services.
So I don't see that as working either.
Now, here's the political reality for the West.
The purpose of the sanctions, the purpose
of all of the political pressure,
the purpose of targeting the oligarchs
was to destabilize Putin as a leader.
That was the purpose of all of that.
Don't get cold feet now that it's happening.
This will be destabilizing.
Even though these people probably
won't be troops, those that are trying to leave,
even though they probably wouldn't be conscripted,
they still have jobs.
They have jobs that either they have money
through their family or they have jobs
that produce a lot of money.
Them leaving, that production, whatever
is occurring because of their job,
well, it's going to slow down.
It's bad for the economy.
It creates brain drain.
It creates upset mamichkas.
It causes internal issues, which if I'm not mistaken,
that's been the goal of Western policy since Russia invaded.
Don't get cold feet now.
It's working.
I understand the desire to say, no, y'all caused this.
Y'all supported it.
You have to deal with it.
Go fight.
And I get that on an emotional level.
Sending a bunch of incredibly spoiled,
entitled people who will probably not be on the front
means that they're going to be around the Ukrainians
they're occupying.
It's probably not good for them either.
I understand it on an emotional level.
But from a strategic point of view,
this is what people have been trying to cause.
All of the sanctions were designed
to make it uncomfortable to put pressure on Putin.
That's happening now to the point where people are scared
and people with money are trying to get out.
I would encourage facilitating.
Anybody who wanted to flee mobilization, I would help them.
It will probably decrease morale,
will probably further destabilize Putin.
All of the stuff that got put into motion
when Putin invaded Ukraine, those policies are working.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}