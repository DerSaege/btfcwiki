---
title: Let's talk about credit cards, codes, and guns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7UjP2KlEpWE) |
| Published | 2022/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The gun industry will now have its own merchant code when purchasing with credit cards from Visa, MasterCard, and American Express.
- Pro-gun advocates fear mass surveillance, tracking, and politically motivated monitoring with this new merchant code system.
- The gun industry not having its own merchant code until now is what's unusual, considering the level of detail in merchant codes.
- There is no new information being tracked with the introduction of the gun industry's merchant code; the information was already available with credit card purchases.
- Gun control advocates believe the merchant code could help identify suspicious gun purchases, but Beau argues that typical purchases like a rifle, magazines, and ammo may lead to false alarms.
- Beau believes that law enforcement action based on flagged gun purchases is unlikely to result in anything significant, as legal gun ownership is not a crime.
- The limited potential benefit of the new merchant code system lies in tracking purchases between areas with differing gun laws to prevent illegal sales across jurisdictions.
- Overall, Beau sees the new merchant code as more of a talking point and a political move rather than a practical tool for tracking gun purchases effectively.
- The new merchant code is not expected to be a useful tool for law enforcement or a form of mass surveillance, contrary to some fears surrounding its implementation.

### Quotes

- "There's no new information that's really being gathered."
- "The fact that guns didn't exist in one of these already is what's weird."
- "It's not going to be a super useful tool."
- "It's a talking point coming out before elections."
- "It's not going to be Big Brother watching you."

### Oneliner

Beau breaks down the new gun industry merchant code, debunking fears of surveillance and tracking while questioning its actual utility for law enforcement and gun control efforts.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Educate policymakers on the limited practical use of the new gun industry merchant code (implied).

### Whats missing in summary

Further details on the implications and limitations of the new gun industry merchant code can be better understood by watching the full transcript.

### Tags

#GunIndustry #MerchantCodes #CreditCardPurchases #GunControl #LawEnforcement


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about credit cards
and merchant codes and categories and tracking
and a conversation that has kind of popped up from this
that has two opposing opinions on what's about to happen.
And the reality is they're both wrong.
So we're gonna go through this kind of step-by-step
and talk about the fact that now guns
will have their own merchant code.
Okay, so the short version is the gun industry,
when you use Visa, MasterCard, American Express now,
making these purchases,
they will be kind of sectioned into their own
four-digit category.
Now, the pro-gun side is like screaming 1984.
Mass surveillance, they're gonna be tracking us.
It's a way to quote,
track politically disfavored industries.
And going from there,
just to kind of show how weird this is,
5441 is the four-digit code
for the politically disfavored candy stores.
It's not weird that guns are getting
their own merchant code.
It's weird that they've gone this long without them.
It's a massive industry and these things exist
to show you how detailed and specific they get.
7841 is the merchant code for videotape rental stores.
They're really specific.
The fact that guns didn't exist in one of these already
is what's weird.
Aside from that,
please understand that there's not really
any new information that's being tracked here.
Make no mistake about it,
if you go to Jim Bob's Gun Store
and you make a purchase with a credit card,
the information's there.
That's not a secret.
It's available.
So there's that aspect of it.
There's no new information that's really being gathered.
At the end of the day, this doesn't change anything.
Now, to the gun control side,
the reason the pro-gun side thinks this
is because a lot of people think
that's how this is gonna be used.
The reality is even if they were to try,
it's not gonna be effective.
I think a lot of it is based on the perception
and the media presentation
of what bad events look like beforehand.
The theory from the gun control side is,
well, now that these are in their own category,
the credit card companies can look more closely
at these purchases.
So if somebody goes in and buys a rifle
and six magazines and a thousand rounds,
well, that's obviously a clear sign,
except it's not.
That's pretty normal.
When somebody buys a new rifle,
they tend to buy all of the accessories that go with it,
such as magazines, and they also buy a lot of ammo
because it's a new weapon
and they either want to zero it or practice with it
or with a whole lot of people play with it.
So a thousand rounds, a bunch of mags and a rifle,
if that's what's gonna get flagged,
there's going to be probably 99% false positives on this,
which means the cops won't go and check them out anymore.
Now, aside from that, the other way it could be used is,
let's assume you have a credit card company
that is just totally on the ball
and they realize somebody has made a purchase
and it's like $5,000 worth of guns and ammo
and stuff like that, and they're really young
and it's their first purchase
and there's just tons of red flags everywhere.
And they actually do make contact with law enforcement
over this.
From there, law enforcement has to actually act on it.
Okay, they actually have to do something
with this information.
That in and of itself, in my mind, is a stretch,
but let's pretend that they do.
So they show up at the person's house
and they knock on their door and they say,
hey, you bought a bunch of guns and ammo.
And they say, yes, I've taken up shooting as a hobby.
And that's the end of the conversation.
That's not illegal.
So there's not a whole lot for law enforcement
to do with this information.
In fact, the only way that I can actually see this
being used in any way that's really beneficial,
it's very limited in scope.
It's for places that have strict gun laws.
And then just outside that jurisdiction,
there are lax gun laws.
So there are people who will buy guns in the lax area,
drive into the strict area and sell them.
And this information, doing it this way,
would save a cop who, like, let's say a cop
rolls up on somebody and they have a firearm
and they ask them about it and they're like,
yeah, I bought it secondhand.
Normally, that cop is going to kind of dig into it a little,
maybe, if it goes to a detective.
So with this new coding system, it
would save them one out of three 15-minute phone calls.
That's about it.
It would make it slightly easier to track down
which purchase, assuming they're working from that side of it,
which purchase was done at a store
rather than having to get a list of the places
the card was used.
Or the more likely scenario is they work from the other side
and they end up at the gun store before they even have
the credit card information.
It's not going to be a super useful tool.
And since it's not going to be a super useful tool,
it's also not going to be Big Brother watching you.
It's a talking point coming out before elections.
It's a feel-good measure for one side that doesn't even
make me feel good.
And on the other side, it's a talking point
to scare and fear monger about nothing
because they think the pro-gun crowd is dumb.
That's those representing the pro-gun side
believe their supporters aren't smart enough
to realize that if you use a credit card at a gun store,
there's a record of it.
Or to realize that the merchant codes are
a really common thing.
Florists have their own.
It's not a tracking thing.
At the same time, if you're pro-gun control,
just understand it's not going to function the way you think,
if you do believe it's going to be a good tool for tracking
or intervening.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}