---
title: Let's talk about an update on Trump's special master....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=T1gmB3-OoUY) |
| Published | 2022/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on the legal battle concerning Trump documents and the special master process.
- Department of Justice (DOJ) requested permission to bypass the special master for documents with classification stamps.
- The judge denied the DOJ's request, prompting them to appeal to the 11th Circuit.
- The 11th Circuit gave Team Trump until Tuesday to respond to the appeal, indicating a fast-track process.
- Trump's team seeks exemption for documents with classification stamps from the special master process to use them immediately.
- The 11th Circuit consists of 11 judges, with six being Trump appointees, but three will be randomly selected for the case.
- The issue is about speed rather than the final outcome.
- Beau doubts that the documents will not eventually be handed over to the DOJ.
- Even if the documents were declassified, they remain property of the U.S. government.
- Regardless of the court's decision, it is likely that the DOJ will obtain and use the documents.
- The focus now is on how long the process will take rather than the ultimate outcome.
- The 11th Circuit's decision will determine the timeline for accessing the documents for the criminal investigation.
- Beau believes that the DOJ's access to the documents is almost certain, just a matter of procedural hurdles.
- The 11th Circuit appears to be pushing for a quick resolution based on the deadline given to Trump's team.

### Quotes

- "Regardless of what the court decides, the end result will be that the Department of Justice ends up getting to use these documents."
- "I can't envision a scenario in which these documents don't ultimately end up in the hands of the Department of Justice to use in this case."
- "What they're really deciding on is how long this drags out."
- "The final outcome here as far as DOJ being able to use these documents, I think that's pretty much set in stone."
- "It's just how many procedural hoops are they going to have to jump through before they get access to them for the criminal investigation side of things."

### Oneliner

DOJ likely to gain access to Trump documents despite legal battles; focus now on procedural timeline with 11th Circuit involvement.

### Audience

Legal observers and those interested in transparency and accountability.

### On-the-ground actions from transcript

- Prepare to stay informed about the developments in the legal battle over Trump documents (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the ongoing legal battle over access to Trump documents and the involvement of the 11th Circuit, offering insights into the process and potential outcomes.

### Tags

#Trump #LegalBattle #DOJ #Transparency #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to provide a little
bit of an update on what's going on with the Trump documents case and the legal battle that's going
on with this special master and all of that stuff and how that's been playing out because there have
been some interesting developments. If you remember, the Department of Justice asked the judge
to basically stay part of their ruling and say, hey, you know, these documents that have
classification stamps, DOJ can go ahead and go through those. Those documents don't need to go
through the special master process. That's what DOJ was asking for. Judge said no. On Friday,
the Department of Justice took that request to the 11th Circuit. Sometime between Friday when they
they filed and Sunday morning, the 11th Circuit told Team Trump that they have until tomorrow,
Tuesday, to respond. So it does appear as though they will put this request on the fast track.
So what they're asking for, in essence, is the documents that bear classification stamps,
for those to be exempted from the special master process. Basically, they want to use them now
rather than later. And that's the entire request. It's not a huge thing. Now, the 11th Circuit is
made up of 11 judges and I think six are Trump appointees. A lot of people are making a big deal
out of this. I don't think that's going to be a huge deal for a couple of reasons. One, three of
them are going to be selected at random to decide this case. And two, just because Trump appointed
the judge doesn't mean that they're going to decide in Trump's favor. On top of that, again,
this is a question of speed, not outcome. I can't envision a scenario in which a special master
decides documents produced by the U.S. government bearing classification stamps are Trump's property,
number one, and number two, that they are even in any way privileged that they should be exempted.
I don't see that outcome. It's a delay. Regardless of what the court decides, the end result will be
that the Department of Justice ends up getting to use these documents. I cannot envision a scenario
in which that doesn't happen. There's not, and I've tried, I've tried to come up with an argument
that would make it just as a thought exercise. I cannot imagine a scenario in which these documents
don't ultimately end up in the hands of the Department of Justice to use in this case.
By everything that we know about them, they were produced by the U.S. government. They were
classified by the U.S. government. Even if they were magically declassified, they would still
remain property of the U.S. government. So it, I don't even really get their argument here. I don't
understand what they're trying to say these records are. So in short, Trump's team has until tomorrow
at noon, I think, to respond. And from there, the 11th Circuit will make their determination.
What they're really, what they're really deciding on is how long this drags out. The, I think that
the final outcome here as far as DOJ being able to use these documents, I think that's pretty much
set in stone. I can't imagine anything that would change that. It's just how many procedural hoops
are they going to have to jump through before they get access to them for the criminal investigation
side of things. But given the fact that it certainly appears that the 11th Circuit said you
have until Tuesday to respond over the weekend, I think that they're looking to move quickly on this.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}