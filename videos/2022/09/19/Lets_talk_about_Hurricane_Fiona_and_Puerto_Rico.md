---
title: Let's talk about Hurricane Fiona and Puerto Rico....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=USr_FZdZjm4) |
| Published | 2022/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hurricane Fiona is causing flooding in Puerto Rico and the Dominican Republic, with rainfall being the main issue.
- The excessive rain, ranging from 12 to 30 inches in some areas, is leading to major flooding, including urban areas and rivers.
- The flooding has triggered landslides, mudslides, and power outages across the island.
- Puerto Rico's power grid is still struggling to recover from Hurricane Maria, with nearly one and a half million homes losing power during Fiona.
- President Biden has declared a state of emergency, but the island's past experiences with inadequate federal responses are causing skepticism among residents.
- Efforts are being made to restore power and establish command functions, but concerns remain about the effectiveness of the response.
- It's uncertain how the situation will unfold, and there may be a need for widespread assistance in Puerto Rico.
- Monitoring the situation in the coming days will provide a clearer picture of the extent of the damage and opportunities for individuals to help.
- Beau commits to keeping everyone informed about ways to support the affected communities.
- The aftermath of Hurricane Fiona serves as a reminder of the ongoing challenges faced by Puerto Rico and the importance of proactive assistance from both government and individuals.

### Quotes

- "It's not so much the wind. It's the rain."
- "The flooding has caused landslides, mudslides, power outages."
- "There are probably a lot of people in Puerto Rico right now that are less than optimistic about the federal government's response."
- "Sometimes past performance does predict future results."
- "We'll know what we as individuals can do to help out."

### Oneliner

Hurricane Fiona triggers major flooding in Puerto Rico and the Dominican Republic, raising concerns about the effectiveness of federal response efforts amidst past inadequacies.

### Audience

Disaster relief organizations

### On-the-ground actions from transcript

- Support local relief efforts in Puerto Rico (implied)
- Stay informed about the situation to understand how to provide assistance (implied)
- Volunteer with organizations working to aid those affected in Puerto Rico (implied)

### Whats missing in summary

The emotional impact on individuals and communities affected by the flooding and power outages in Puerto Rico. 

### Tags

#HurricaneFiona #PuertoRico #DisasterRelief #FederalResponse #CommunitySupport


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Hurricane Fiona and Puerto Rico.
Now, it's worth noting that I'm going to specifically talk about Puerto Rico in this,
but the Dominican Republic is also being hit.
Okay, so in this instance, it's not so much the wind.
It isn't so much the wind. It's the rain.
12 to 18 inches on average, some places up to 30 inches.
This is causing flooding, and some of it is major.
There's flooding in urban areas, and there is flooding with rivers.
I want to say one of the rivers topped 20 feet over where it should be
and was going up at the time I saw it.
This has caused landslides and mudslides, power outages.
At one point, it appeared as though the entire island was without power.
I want to say that's one, almost one and a half million homes.
The grid there has still not really recovered from Maria.
Now, Biden has issued the declaration.
He did that Sunday morning, but given the fact that it's right around five years since Maria
and there are still problems left over from that,
there are probably a lot of people in Puerto Rico right now that are less than optimistic
about the federal government's response because it was less than adequate last time.
We don't know how it's going to play out this time.
It does appear that there are people there to help set up like command and control functions
and get the power up and running.
It appears they're already there, but sometimes past performance does predict future results.
So this may be something we need to watch in case there needs to be a widespread effort
to try to help out down there because we don't know what's going to happen
and there hasn't been a great track record.
So as everything clears up over the next couple of days and we get a picture of the scope of what's happened,
we'll know a little bit more and we'll know what we as individuals can do to help out.
And I will try to keep everybody posted on that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}