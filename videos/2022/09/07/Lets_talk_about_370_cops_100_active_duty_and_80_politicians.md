---
title: Let's talk about 370 cops, 100 active duty, and 80 politicians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Jwp-TeJOPSI) |
| Published | 2022/09/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the numbers of 370 current cops, 100 active service members, and 80 public officials identified in the roster of the group, the oath keepers.
- The group had 38,000 names on its roster, so the percentages are not as high as they may seem.
- Questions if the numbers should be considered high, given the target demographic of the group.
- Considers that the numbers are low, even if you apply standard math to estimate a higher count.
- Raises the importance of analyzing former law enforcement and military members to get a clearer picture.
- Expresses surprise that the numbers weren't higher, considering the target demographic and recruitment efforts.
- Mentions recent news about the arrest of a longtime attorney for the group and efforts to delay trials.
- Suggests that perceived capability of such groups may not match their actual influence.
- Calls for perspective on the numbers and the need for lower figures.
- Concludes with a reminder to keep these factors in mind.

### Quotes

- "These numbers are low. Even if you were to take the standard math when it comes to active versus sympathizer and all of that stuff and multiply it, 3,700 cops nationwide, it's actually not as influential an organization as a lot of people may think."
- "The recruitment efforts of this group were specifically catered to reach a certain group of people. Going by what we see here that recruitment effort didn't succeed."
- "Perceived capability is being substituted for an accurate analysis of actual capability."

### Oneliner

Analyzing the numbers of identified individuals in the oath keepers' roster reveals lower percentages than expected, prompting a call for perspective and accurate analysis over perceived capability.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Analyze the demographics and percentages of former law enforcement and military members related to groups like the oath keepers (suggested).
- Maintain awareness and vigilance regarding recruitment efforts by extremist groups (implied).

### Whats missing in summary

The full transcript provides detailed insights into the analysis of identified individuals in extremist groups, shedding light on the importance of accurate assessments over perceived threats.

### Tags

#Analysis #Extremism #LawEnforcement #Military #Recruitment


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about some numbers
and an analysis that has made it into the news cycle.
And whether or not the way those numbers are being received
is the way they should be received.
So, 370 current cops,
A hundred active service members, people in the military and eighty public officials are
believed to have been identified on the roster of the group, the oath keepers.
Now in the press people are looking at this and saying these numbers are high.
Are they though?
When you really consider the group and what the group was attempting to do, are these
numbers high. This roster had 38,000 names on it. 38,000 names. Their target demographic,
as the name suggests, are people who took an oath to support and defend the Constitution
of the United States. Yes, I understand the irony of that, given everything that's happened,
that's their target demographic. 370 out of 38,000? That's not high. That's not a
high percentage. And when you're talking about the the service members we don't
know how many of those are reservists or National Guard who were just looking
for an identity or just thought it was just another veterans group and had
nothing to do with it. These numbers are low. Even if you were to take the
standard math when it comes to active versus sympathizer and all of that stuff
and multiply it, 3,700 cops nationwide, it's actually not as influential an
organization as a lot of people may think. I would love to see this same
analysis done of former, former law enforcement, former military, and see what
the percentages are out of that 38,000. I think that that information might do a
lot of good because there may be people who join groups like this believing
that they're going to be around a certain group of people because they're
looking for an identity of a sort, that group of people may not be part of this
group. So, and again, I'm not trying to downplay these numbers. I just think that
there needs to be perspective on it. These numbers should be much lower.
You're never gonna have numbers that are zero, but they should be lower. Yeah, I get
that, but I honestly expected them to be much higher. I really did. Now, since we're
talking about the group, we'll go ahead and hit some other news that has occurred
over the last few days. Longtime attorney for the group has been arrested on a
conspiracy charge related to the 6th and it appears that Rhodes has fired a
couple of lawyers and is attempting to get a continuance trying to delay the
trial for an additional 90 days or something like that. It looks like I
believe he was set to go to trial the 28th, 29th, the end of the month. How about
that, end of September, and is currently trying to get that extended. When you're
talking about these numbers, sure, I mean that's more than it should be, but when
you're talking about these are the nationwide numbers, they're nowhere near
as high as I would have expected. The recruitment efforts of this group were
specifically catered to to reach a certain group of people. Going by what we
see here that recruitment effort didn't succeed. So while again these numbers
aren't great, they're also not terrifying. They're nowhere near as high as one
might expect given the the influence that this group and groups like this are
are given, it may be a case where perceived capability is
being substituted for an accurate analysis
of actual capability.
And it's just something to keep in mind.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}