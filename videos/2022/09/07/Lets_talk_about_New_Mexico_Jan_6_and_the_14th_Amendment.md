---
title: Let's talk about New Mexico, Jan 6, and the 14th Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wQJbU9frJ-4) |
| Published | 2022/09/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Story from Otero County, New Mexico involving a county commissioner and the 14th Amendment to the U.S. Constitution.
- Commissioner Coy Griffin being removed from office for actions on January 6th at the Capitol.
- Griffin, founder of Cowboys for Trump, involved in refusing to certify election results.
- 14th Amendment prohibits holding office if engaged in insurrection against the U.S. Constitution.
- Judge ruled Griffin incited violence on January 6th, contributing to delaying election certification.
- Courts will need to decide if January 6th events constitute insurrection or rebellion under the Constitution.
- Likely appeal in Griffin's case, potentially impacting future similar cases.
- Griffin's charges currently minor, with a jail sentence of around 14 days served.
- Case could set a precedent for future cases and raise political attention.
- Possibility of courts ruling rhetoric leading to violence as aiding and comforting.

### Quotes

- "No person shall be a Senator or Representative in Congress, or Elector of President and Vice President, or hold any office, civil or military, under the United States or under any State, who having previously taken an oath as a Member of Congress, or as an Officer of the United States, or as a Member of any State Legislature, or as an Executive or Judicial Officer of any state to support the Constitution of the United States shall have engaged in insurrection or rebellion against the same, or given aid or comfort to enemies thereof."
- "Griffin incited, encouraged, and helped to normalize the violence on January 6th."
- "This case could set the tone for other cases later and it's definitely something we need to keep an eye on."

### Oneliner

A county commissioner faces removal under the 14th Amendment for his actions on January 6th, potentially setting a precedent for future cases and attracting political attention.

### Audience

Legal enthusiasts, political analysts

### On-the-ground actions from transcript

- Keep an eye on the legal proceedings and outcomes surrounding cases related to the January 6th events (implied).

### Whats missing in summary

Deeper analysis on the potential implications of this case on future interpretations of the 14th Amendment and its application in similar scenarios.

### Tags

#OteroCounty #14thAmendment #CoyGriffin #PoliticalJustice #LegalSystem


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about a story coming out of Otero County, New Mexico, dealing
with a county commissioner in the 14th Amendment to the United States Constitution.
A district court judge, Francis Matthew, ruled that Coy Griffin, a county commissioner there,
should be removed and cannot hold office again because of his actions on January 6th at the
Capitol.
Griffin is known as the founder of Cowboys for Trump and was recently involved in a situation
in which they were refusing to certify election results.
The 14th Amendment to the Constitution says,
No person shall be a Senator or Representative in Congress, or Elector of President and Vice
President, or hold any office, civil or military, under the United States or under any State,
who having previously taken an oath as a Member of Congress, or as an Officer of the United
States, or as a Member of any State Legislature, or as an Executive or Judicial Officer of
any state to support the Constitution of the United States shall have engaged in insurrection
or rebellion against the same, or given aid or comfort to enemies thereof.
But Congress may buy a vote of two-thirds of each house to remove such disability."
So the general idea here is that because Griffin had already swore an oath of allegiance to
the U.S. Constitution, the actions there create this disability and stop him from being able
to hold office.
The judge said that Griffin incited, encouraged, and helped to normalize the violence on January
6th, and that by joining the mob and trespassing on restricted capital grounds, Mr. Griven
contributed to delaying Congress's election certification proceedings.
Now there are other cases where this has been brought up using the 14th Amendment in this
manner in relation to what happened on the 6th.
The courts are going to have to kind of uniformly decide whether or not what happened on the
sixth counts as insurrection or rebellion in for the purposes of the Constitution.
I would imagine that this is going to be appealed. Griffin does not seem the type to just kind of
throw in the towel on this, so this story probably isn't over. But either way, it is
likely to catch the eye and ear of a lot of politicians because it is entirely possible
that courts rule that the rhetoric that might lead to the inciting of such an event counts
is aid and comfort. So this case, while right now minor and Griffin, the charges
are relatively minor. I want to say he got like 14 days in jail time served. This
is going to set the tone for other cases later and it's definitely something we
to keep an eye on and see how it plays out if the assumed pills occur. Anyway,
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}