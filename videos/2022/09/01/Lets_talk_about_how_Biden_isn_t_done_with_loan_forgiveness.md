---
title: Let's talk about how Biden isn't done with loan forgiveness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=79NWEnEpy08) |
| Published | 2022/09/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the topic of student loan forgiveness and addresses some misconceptions in the media.
- Talks about overlooked borrowers, specifically those with federal family education loans held commercially.
- Mentions that the Biden administration might extend forgiveness to these loans through executive order.
- Emphasizes that this potential extension is not just beneficial for the directly impacted borrowers but everyone.
- Points out that Biden administration is still working on revamping education costs post the initial forgiveness announcement.
- Mentions the uncertainty around whether the forgiven amount will be taxed and lists states that are undecided on taxing it.
- States that New York has clarified they won't tax the forgiven amount and might amend the law accordingly.
- Advises caution on assuming definite taxation on forgiven loans, as some states on the list might not tax it.
- Suggests keeping an eye on further developments regarding taxation and the administration's education cost plans post-midterms.
- Beau ends by reiterating that the Biden administration is not done and advises against premature budgeting based on potential tax implications.

### Quotes

- "The Biden administration isn't finished."
- "They're not finished."
- "The Biden administration isn't finished."
- "The reporting about the taxes is kind of up in the air."
- "I hope you all have a good day."

### Oneliner

Beau explains student loan forgiveness, overlooked borrowers, potential taxes on forgiven loans, and ongoing efforts by the Biden administration.

### Audience

Students, Loan Borrowers

### On-the-ground actions from transcript

- Keep informed on updates regarding student loan forgiveness and potential tax implications (implied).
- Stay engaged in understanding how policy changes may impact student loans (implied).

### Whats missing in summary

Further insights on specific state actions regarding taxing forgiven loans can be gained from watching the full transcript.

### Tags

#StudentLoans #Forgiveness #BidenAdministration #EducationCosts #Taxes


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're gonna talk a little bit more
about student loan forgiveness.
And a couple of aspects about it
that are kind of showing up in the media
and maybe need some clarification.
The first topic is the topic of overlooked borrowers.
And the second is whether or not you're gonna be taxed
on the forgiveness as income.
If you are concerned about this, stick around
I will tell you there is some bad reporting out there on this. Okay overlooked
borrowers first. There's about 5 million people in the United States that have
loans that are under the umbrella of federal family education loans. These
loans were part of a program that was terminated I want to say back in 2010
and the loans were guaranteed by the feds but they were held commercially. Now
Now through bank bailouts and students defaulting and the feds ending up with the paper and
all of that, about half of them have wound up being owned by the feds.
But because these loans are held commercially, the forgiveness doesn't apply to them.
Now if you're interested in your particular loan, you can go to studentaid.gov and type
in your FSA ID number and you can get a little bit more information.
The interesting part is that the Biden administration appears to be trying to find some way through
executive order, some policy directive, whatever, to extend the forgiveness to these loans or
maybe just the half that are owned by the feds.
We don't know yet.
But while that's really good for the five million or two and a half million people that
are directly impacted, it's also good for everybody else.
It shows that the Biden administration isn't done.
When it comes to revamping education costs, the Biden administration isn't finished.
They just had a huge win, right?
All over the papers, millions of people really, really happy, but they're still working quietly
behind the scenes to increase it. They're not finished. Right now they have to do
things that they can manage just within the administration and without Congress.
Maybe after the midterms the makeup of the Senate and House will look a little
bit differently and they'll be able to enact some wider ranging changes because
as it stands, the Republicans have made it very clear they have no interest in
helping students or those with student debt. Okay, onto the second thing. Are you
going to be taxed on this money? And there has been a lot of reporting out
there on it. Now, the states that are either undecided or you will be taxed on.
Arkansas, Hawaii, Idaho, Kentucky, Massachusetts, Minnesota, Mississippi, Pennsylvania, South
Carolina, Virginia, West Virginia, and Wisconsin.
Somebody right now is going to say New York, no.
The state of New York has said that they're not taxing on it, and it looks like they're
actually going to clarify the language in the law to make sure that that doesn't happen.
The other states, it's possible that you're taxed.
It's not a guarantee.
A lot of the reporting includes New York,
and a lot of the reporting makes it
seem like it is definitely going to happen.
Nobody knows that yet.
Certainly, some of these states will tax on it.
But the list that's out there are those states that could,
not those states that will.
So hang tight and keep watching and seeing what's going on.
So big takeaway here, the Biden administration isn't finished.
They might be able to get more done
if they had the House and the Senate.
And the reporting about the taxes is kind of up in the air.
I would wait before you start budgeting on that.
So anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}