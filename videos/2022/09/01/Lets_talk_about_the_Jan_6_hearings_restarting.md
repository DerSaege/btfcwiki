---
title: Let's talk about the Jan 6 hearings restarting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HgR1yuUCQ8Q) |
| Published | 2022/09/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The upcoming January 6th hearings will focus on "The Cash and the Cover-Up," delving into the finances and potential cover-up aspects of the events surrounding January 6th.
- The committee aims to look into fundraising activities, the misuse of funds, and financing for the event itself or individuals promoting the "stop the steal" narrative.
- Expect scrutiny on missing documents, Secret Service involvement, and attempts to deflect blame as part of the cover-up investigation.
- The hearings are likely to structure similarly to previous ones, focusing on specific aspects and tying everything together for a broader understanding.
- Previous hearings reached millions of Americans effectively, and it's expected that the committee will continue with this successful approach.
- It's uncertain whether the committee will push for charges or if the hearings are purely informational; the Department of Justice is not obligated to follow their suggestions.
- Stay tuned for updates on new subpoenas and how different parties will respond—cooperation versus resistance has become a common theme in the process.

### Quotes

- "The Cash and the Cover-Up."
- "Tell them what you're going to tell them, tell them, tell them what you told them."
- "Millions of people watching it, it worked."

### Oneliner

The upcoming January 6th hearings will focus on finances and cover-up, structured to inform and reach millions, with potential for charges. 

### Audience

Politically Engaged Citizens

### On-the-ground actions from transcript

- Monitor updates on the January 6th hearings and stay informed on the developments (implied).
- Share information from the hearings with others to increase awareness and understanding of the events (implied).
- Stay engaged with the process and be prepared to take action based on the outcomes of the hearings (generated).

### Whats missing in summary

Insights on specific findings and implications from the hearings beyond the general focus on finances and cover-up. 

### Tags

#January6th #Hearings #CoverUp #Finances #DOJ #Accountability #Transparency


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the upcoming hearings,
the January 6th hearings.
They will be restarting this month.
And we've already gotten a little glimpse
of some of the main focus of at least part of it.
And it is aptly titled, The Cash and the Cover-Up.
appears as though the committee wants to delve into the finances of the events
surrounding the sixth. Now this is probably going to have a whole lot to do
with the fundraising activities, the stop the steal thing, and how a lot of that
money never actually went to any endeavor designed to alter the outcome.
It may also delve into the financing for the event itself or financing for those
who were carrying talking points for this endeavor. Now the cover-up aspect
of it. I think when they're talking about it they're very careful to use the term
potential. You're probably going to hear about the Secret Service, missing text
messages, missing documents, stuff like that. All under the umbrella of trying to
mask what occurred or deflect blame. I would imagine that in this phase they
would talk about the aftermath of the Sixth and how many people close to the
former president put out information that was less than accurate in an
attempt to deflect responsibility. Now as far as the hearings themselves, I would
imagine that they're going to try to structure them the same way. At least I
would hope so. They did a pretty good job last time. Tell them what you're going to
tell them, tell them, tell them what you told them, you know, and keep each
individual hearing focused on one particular aspect and then tie it all
together. They did a really good job with this and it reached the American
people, the message was received, you're talking about millions of people
watching it, it worked. I don't see them deviating from that. I'd be really
surprised if they did. So that's a glimpse of what we can expect over the
course of the rest of the hearings here. Now we still don't have a any real sign
as to whether or not the committee plans on trying to push for charges or this is
just informational. They can, but DOJ doesn't have to do what they suggest.
There's a lot to it, and we'll also get to find out about any new subpoenas soon
and who is going to cooperate and who is going to fight it, which has just become
part of the process now. So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}