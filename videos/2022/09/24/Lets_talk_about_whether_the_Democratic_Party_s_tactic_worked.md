---
title: Let's talk about whether the Democratic Party's tactic worked....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EuWTZ17kH-I) |
| Published | 2022/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Democratic Party used an unusual tactic during the Republican primaries by spending tens of millions of dollars to support far-right candidates to improve the chances of the Democratic nominee in the general election.
- In Maryland, the Democratic Party supported a candidate named Cox, described as a "Q whack job," against Democratic nominee Wes Moore, a veteran and Rhodes Scholar.
- Despite initial skepticism, Moore is currently leading by more than 20 points in Maryland, showing the success of the strategy.
- Similar tactics were used in New Hampshire and Illinois, resulting in Democratic candidates being ahead in the polls by double digits.
- Michigan and Nevada are also part of this strategy, with Democrats having the edge, although the races are more competitive.
- Republicans planned to support candidates who were more polished and less openly authoritarian, hoping to push them forward with voter support.
- The Republicans' plan was disrupted by the Democratic Party's tactic of boosting far-right candidates in the primaries.
- The success of the Democratic strategy has left Republicans unhappy with the outcome.
- Overall, the tactic of supporting far-right candidates in Republican primaries to benefit Democratic nominees has proven effective in several states.

### Quotes

- "It has definitely paid off there."
- "Their plan was to ignore the more openly authoritarian, the Trumpists and support candidates that were a little bit more polished."
- "They probably would have got away with it too, if it weren't for those meddling dims."

### Oneliner

The Democratic Party's tactic of boosting far-right candidates in Republican primaries proved successful, leading to significant leads for Democratic nominees in multiple states.

### Audience

Political strategists

### On-the-ground actions from transcript

- Support and get involved in local Democratic Party efforts to strategize and support candidates in primary elections (suggested)
- Stay informed about political strategies and tactics being used in primary elections (suggested)

### Whats missing in summary

The full transcript provides more detailed insights into the specific races and outcomes in various states, giving a comprehensive view of the Democratic Party's strategic approach during primary elections.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit more
about the unusual tactic that the Democratic Party used
during the Republican primaries.
We've talked about it a little bit on the channel,
but to go over it real quick,
the Democratic Party spent tens of millions of dollars to,
to, well, kind of help out Trumpists.
They helped far-right candidates in their primary, so the Democratic candidate would
be in a better position to win the general election.
That was a tactic that met with a lot of skepticism.
The one that I highlighted was Maryland, because it was just such a pronounced difference when
it came to the way you would expect the election to go afterward.
The Democratic Party elevated a man named Cox, who has been described as, quote, a Q
whack job.
The Democratic nominee is Wes Moore.
He's a veteran and a literal road scholar.
I said at the time that I didn't like early predictions,
but I was fairly certain that Moore was going to do all right.
He is currently not just up by double digits.
He is up by more than 20 points.
It has definitely paid off there.
That strategy, that tactic worked there.
They did it in a few other places.
In New Hampshire, there were two congressional candidates
one senatorial candidate. It looks like it worked there as well. In Illinois, it was
done with the governor's race, and the incumbent Democrat is now up by double digits there.
There are two more locations, Michigan and Nevada, that are a little bit more competitive.
It looks like they have the edge, but they're not as pronounced as the other races. The
polling isn't as clear. Now, Republicans are obviously not going to be happy with
how this worked out. Their plan was to ignore the more openly authoritarian, the
Trumpists and promote candidates that were a little bit more polished. They knew the
right things to say and more importantly what not to say and they would still support that
same authoritarian agenda and they were hoping that they could get those candidates past
the voters.
They probably would have got away with it too, if it weren't for those meddling dims.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}