---
title: Let's talk about Trump by the numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zVnyDa_OoyY) |
| Published | 2022/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an overview of Trump's current standing within the Republican Party and among voters.
- Shares polling data showing low favorability ratings for Trump among registered voters.
- Points out the dilemma faced by the Republican Party in the upcoming midterms due to Trump's influence.
- Explains that Trump's presence is still felt within the Republican Party, despite not being on the ballot.
- Analyzes how Trumpist candidates might impact Republican turnout in the elections.
- Emphasizes the challenge faced by Republican leadership for failing to distance themselves from Trump.
- Criticizes the Republican leadership for not taking the necessary steps to address the Trumpist influence within the party.
- Notes the potential consequences of the Republican Party's association with Trump in the general election.
- Warns about the possible negative impact of Trump's influence on Republican candidates endorsed by him.
- Concludes by suggesting that the Republican Party's failure to address Trump's influence could lead to significant problems in the upcoming elections.

### Quotes

- "Trump is the Republican Party."
- "The Republican Party may have a problem with turnout."
- "They had the numbers to shake Trump off. They just didn't have the courage."
- "They're linked to Trumpism. And lots of Republicans are waking up to how bad that really is."
- "The Republican Party, they're in trouble."

### Oneliner

Trump's influence continues to loom large over the Republican Party, potentially impacting their performance in the upcoming midterms.

### Audience

Voters, Republican leadership

### On-the-ground actions from transcript

- Mobilize voters to make informed decisions in upcoming elections (implied)
- Encourage Republican leadership to address Trumpist influence within the party (implied)
- Educate voters on the implications of candidates endorsed by Trump (implied)

### Whats missing in summary

Deeper insights into the potential consequences of the Republican Party's association with Trump and the need for strategic decision-making to navigate this challenge effectively.

### Tags

#Trump #RepublicanParty #Midterms #Leadership #Elections


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Trump by numbers.
We're going to go over some numbers that give us a good look
at what the Republican Party is going to be facing come the midterms,
because there's some interesting developments that aren't really surprising.
It's just cool to see numbers to back them up.
Okay, so an NBC poll of registered voters, not likely voters, registered voters,
said that only 20% had a very positive view of Trump.
And 46% had a very negative view of Trump.
When it comes to a more general view of it, less enthusiasm,
people who just view Trump in a positive light, the number jumps up to 34%.
But it also jumps up to 54% when you're just talking about those who view him in a general negative light.
Those are bad numbers.
The Quinnipiac poll shows that Trump is viewed favorably 34%, unfavorably 57%.
And then there's this one that just is really interesting to me.
NBC poll of Republicans.
33% of registered Republicans view themselves as a supporter of Donald Trump,
more so than just a supporter of the Republican Party.
58% view themselves as a supporter of the Republican Party,
more than they view themselves as a supporter of Donald Trump.
So what's the problem for the Republican leadership?
Trump's not on the ballot, except he kind of is.
Trump is the Republican Party.
Because the Republican leadership didn't get out there in the primaries
and energize the base to support candidates that weren't Trumpist,
they're fielding a candidate of Trump clones,
of people who went out of their way to court the Trump vote.
But the Trump vote isn't actually super popular with the Republican base.
It's just popular among the most energized of the Republican base,
those who view themselves as a supporter of Donald Trump, 33%.
So those 33% showed up in high enough numbers to outweigh the rest of the Republican Party,
which is the majority.
So you have a bunch of Trumpist candidates running.
Now, that doesn't mean that those that are supporters of the party,
doesn't mean they're going to go vote Democrat.
But they might decide to go fishing instead,
because he is viewed in a very negative or unfavorable light by most people.
The Republican Party may have a problem with turnout,
because they produced candidates through the primary process that were Trumpist.
And the thing is, they can try to walk back that association,
but they're not going to be able to.
Because the Democratic Party, they're certainly not going to let go of that.
These were Trump endorsed candidates.
More importantly, Trump isn't going to let them.
If they start to walk away from Trump,
if they start to not toe his line,
he will be out there making fun of them,
talking about how they were doing so bad in the polls.
And then they called him and he came in to rescue them,
got them ahead in the polls, they won the primary,
and then they betrayed him and you know he'll do it.
The Republican Party has set themselves up for a lot of problems.
And it is 100% the fault of the Republican leadership,
because they refused to lead.
They mistook the most vocal portion of their base as representative of the whole.
33% view themselves as a supporter of Donald Trump over the party.
58% view themselves as a supporter of the party over Trump.
They had the numbers.
They had the numbers to shake Trump off.
They just didn't have the courage.
They didn't have the will to actually lead.
So those candidates that they have out there,
they're not going to be able to energize large portions of the Republican base
because they're linked to Trumpism.
And lots of Republicans are waking up to how bad that really is.
This is within the Republican Party.
Forget about the independents.
The independents are going to know that these candidates,
they were Trump endorsed.
And they might look the other way simply based on that fact alone.
It was something we said throughout the entire primary season.
We pointed out you can't win a primary without Trump,
but you may not be able to win a general with him.
And that's where they're at.
The Republican Party, they're in trouble.
And how much remains to be seen.
But if the Democratic Party is capable of equating Trump with the Republican Party,
and they can keep that framing, which let's be honest, it's pretty accurate.
They don't have to spend a lot to paint the Republican Party
as just Trump's party with him still in control.
They don't have to do a lot of work to make that image stick.
If they do that, the independents, all of those people who view him in a very negative light,
that's 46% of registered voters, they might be energized to show up.
The Republican Party has made an error because they refused to actually get rid of Trump
when the opportunity presented itself.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}