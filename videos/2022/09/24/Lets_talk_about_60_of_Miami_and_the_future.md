---
title: Let's talk about 60% of Miami and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sl__MoKgnvw) |
| Published | 2022/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the future over the next 40 years and the impact of climate change on internal displacement in the US.
- Mentions an example of an island off the coast of Louisiana disappearing due to climate change, leading to people being relocated.
- Shares a shocking statistic that by 2060, 60% of Miami is expected to be underwater, causing 1.6 million internally displaced people just from that area.
- Expresses concern about the lack of real action and deep systemic change needed to address the impending crisis.
- Observes the mistaken belief of showmanship for leadership, pointing out the need for genuine solutions.
- Stresses the urgency of the situation and the need to prioritize climate change in elections due to running out of time.
- Talks about the reluctance to accept internally displaced Americans as refugees, indicating a need for a shift in perspective on a global scale.
- Points out that ignoring the problem will only exacerbate it and that action is necessary to address the looming crisis.

### Quotes

- "We have real problems on the horizon. They're big and it's going to take more than talking points."
- "Ignoring it will make it worse because we won't make the changes that are necessary."
- "This problem isn't going to be solved by ignoring it."
- "There's a lot of problems on the horizon. They won't be solved by talking points."
- "We know what's happening. We know what's causing it."

### Oneliner

Beau talks about the urgent need for deep systemic change to address the impending climate change crisis, with 60% of Miami expected to be underwater by 2060, leading to 1.6 million internally displaced people.

### Audience

Climate activists, policymakers, citizens

### On-the-ground actions from transcript

- Contact local representatives to prioritize climate change action in policies and elections (suggested)
- Join or support organizations advocating for climate change mitigation and support for internally displaced people (suggested)

### Whats missing in summary

The full transcript provides a detailed insight into the impending crisis of internal displacement due to climate change in the US and the urgent need for systemic change and action to address this growing issue.

### Tags

#ClimateChange #InternalDisplacement #SystemicChange #Miami #Urgency


## Transcript
Well, howdy there internet people.
It's Beau again.
So today we are going to talk a little bit about the future.
What's going to happen over the next 40 years or so.
And answer a question that a whole lot of people had
and that I didn't have an answer to after a recent video.
Recently we talked about how there was an island
off the coast of Louisiana that, well, it was going away
and the people were relocated.
They were given new homes and as far as I know,
that's the first example of internally displaced people
dealing with climate change
that were like officially sponsored.
Okay.
A whole lot of people asked about like, you know,
how many people are there going to be like that?
And I didn't have an answer to that.
I really didn't.
And I looked and there weren't any really good examples.
So what I did find though,
was that within the next 38 years,
there will be 1.6 million internally displaced people
from climate change in the US
that are going to have to move all around the US
from where they're at now.
And that's not a total figure.
That is just from Miami date.
By 2060, 60% of Miami date is expected to be underwater.
That is according to research by the University of Miami.
1.6 million from one place.
You know, recently I was driving down along the coast.
You know, people know where I live generally,
but like I'm closer to Alabama than I am the coast.
And I was down there doing, you know, the old person thing
and looking at all of the developments.
I'm looking at hundreds of millions of dollars
in development that I know won't be there in 30 years.
There's a whole lot of people
that are mistaking showmanship for leadership.
Those things are not the same.
We have real problems on the horizon.
They're big and it's going to take more than talking points.
It's going to take more than performative action.
We need some deep systemic change and we need it quick.
1.6 million people, that's a lot.
And that is just from Miami date.
That is 60% of the population there.
This problem isn't going to be solved by ignoring it.
Ignoring it will make it worse
because we won't make the changes that are necessary.
We know what's happening.
We know what's causing it.
We have the ability to mitigate a whole lot of it.
But every election cycle that passes,
it's not really a deciding factor for voters.
It probably needs to be because we are running out of time.
People talk about refugees.
People talk about those who are displaced
by all sorts of things,
disasters that are natural or man-made.
And there is a reluctance among a lot of Americans
to accept them.
I don't know that that will be different
when the refugees are Americans
and they're internally displaced,
not without a big shift in how we look at the world
because this is a global issue.
There's a lot of problems on the horizon.
They won't be solved by talking points.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}