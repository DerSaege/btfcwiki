---
title: Let's talk about the new bail law in Illinois....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jf2r5OFTJpY) |
| Published | 2022/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the change to the system in Illinois, specifically Chicago, addressing the memes and reactions surrounding it.
- Clarifies that the point of the change is to restructure the system so cash isn't the sole determinant for release.
- Criticizes conservatives and Republicans for wanting excessive bail, keeping poor people in jail, and letting rich individuals out.
- Reminds viewers about the Eighth Amendment, which prohibits excessive bail, fines, and cruel punishments.
- Argues that the new system aims to prevent poor individuals from being penalized due to financial constraints.
- Emphasizes that hearings will still be held to determine threats or flight risks, moving away from the cash-based standard.
- Urges viewers to view the change in line with the spirit of the U.S. Constitution and ensuring fairness in the legal system.
- Stresses the goal of not letting individuals, especially those charged with minor offenses, languish in jail solely because of poverty.

### Quotes

- "Excessive bail shall not be required."
- "The whole point of this is to make sure that poor people aren't punished for a lack of power coupons."
- "It's just moving away from the standard of requiring cash."
- "That's what you want."
- "It's getting in line with the principles of the U.S. Constitution."

### Oneliner

Beau clarifies Illinois system change to reduce cash standard for release, urging adherence to the Constitution’s spirit and fairness, aiming to prevent poor individuals from unjust detention.

### Audience

Advocates for justice reform

### On-the-ground actions from transcript

- Advocate for justice reform in your community (implied).
- Educate others on the importance of reducing cash-based determinants for release (implied).
- Support initiatives that aim to prevent unjust detention of poor individuals (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the rationale behind the Illinois system change, urging viewers to reconsider their views on bail and incarceration practices.

### Tags

#JusticeReform #Illinois #Chicago #BailSystem #Constitution #Fairness


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Illinois
and specifically Chicago
is where it is mostly being referenced
and the change to the system up there
and all of the memes and everything
that has come out about it.
If you are a conservative, if you are a Republican,
please at least watch the first minute
and 30 seconds of this.
The way it's being presented is that now,
you know, people are just gonna get out of jail.
There's no bail and, you know,
people who are dangerous are gonna be getting out.
No, the point is to restructure it
so the standard isn't cash.
So it's no longer just cash that gets you out.
There's a pretrial detention hearing.
The point is to make sure that excessive bail
isn't required and so people can get out.
And I know right now is where the conservatives
and the Republicans, they're saying,
no, I want that excessive bail.
I want those people in jail.
Yeah, we know you don't support the constitution.
You don't have to keep reminding us.
Every time y'all put out one of these memes,
every time you fight about this type of stuff,
you're just reminding us that you don't care
about the founding documents of this country.
Excessive bail shall not be required.
Shall not be required, nor excessive fines imposed,
nor cruel and unusual punishments inflicted.
It's the eighth amendment.
We get it.
You don't like the constitution.
That's fine.
But let me present it to you in another way.
Rather than trying to cast this as the purge law,
think about it in the sense of what it actually accomplishes.
Right now, the way the current system is,
if somebody is truly dangerous or a flight risk,
as long as they can put up the cash, they're getting out.
That's not how it works under the new system.
Your argument is to maintain a system
that keeps poor people in, even if they're not dangerous,
and lets rich people out, even if they are.
That's what you want.
We got it.
The whole point of this is to get in line
with the spirit of the constitution.
The whole point of this is to make sure
that poor people aren't punished for a lack of power coupons.
That's what this is.
The doors to jail cells are not going to fly open
the moment this goes into effect.
There will be hearings and things will be restructured
the way things always work in the legal system.
That's the general idea here,
is to make sure that people who get busted
for some just minor crime,
that they don't sit in jail simply because they're poor.
Judge hits them with a $500 bail and they can't get out.
So they just sit there.
And then, because they're not working,
because they can't pay their rent,
they end up in more debt.
And it places an undue burden on poor people.
That's what this is.
Judges can still order people detained.
There just has to be a clear threat or a flight risk
or something like that.
It's just moving away from the standard of requiring cash.
It's getting in line with the principles
of the U.S. Constitution.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}