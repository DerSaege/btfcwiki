---
title: Let's talk about the GOP needing more time for marriage equality....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6yDVBtXf0bE) |
| Published | 2022/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans want to stall on bringing the vote on marriage equality because they perceive it as complex.
- Democrats aim to bring the vote on marriage equality to the floor on Monday.
- Republicans are bothered by this and claim they need more time to figure things out.
- Beau questions how much time is needed to decide on people's basic rights.
- Republicans may be stalling until after the midterms to vote against marriage equality.
- Republicans want to vote it down but are hesitant when voters have a voice.
- Some Republicans argue they need more time to work out finer details in the text before voting.
- Beau suggests that Senators should put in extra effort considering the importance of the rights of 20 million Americans.
- The issue boils down to Republicans trying to figure out who is vulnerable to appeal to their base.
- Beau urges Democrats to hold the vote on Monday, forcing Republicans to decide on granting rights to 20 million Americans.

### Quotes

- "Exactly how much time do you need to decide whether or not people should have basic rights?"
- "If a senator is afraid of making a vote, of going on the record about something this simple just prior to an election, it's because they know the decision they would make, the vote that they cast, is unpopular."
- "Either they vote in favor of giving rights to 20 million Americans or they vote against it."

### Oneliner

Republicans stall on marriage equality vote, aiming to push it past midterms; Democrats urged to hold the vote, forcing a decision on basic rights for 20 million Americans.

### Audience

Democrats, Activists, Voters

### On-the-ground actions from transcript

- Hold Senators accountable for their stalling tactics and urge them to vote on marriage equality promptly (suggested).
- Advocate for timely decision-making on significant issues that affect millions of Americans (implied).

### Whats missing in summary

Nuances of Beau's passionate delivery and the urgency for swift action on granting basic rights to all Americans.

### Tags

#MarriageEquality #DemocraticParty #RepublicanParty #BasicRights #Voting #Accountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Republicans needing just a little bit more time
because they want to stall.
They want to take a little bit more time to figure something out
because I guess they perceive it as a complex issue.
Schumer has indicated that the Democratic Party would like to bring the vote on marriage
equality to the floor on Monday.
And Republicans are bothered by this, again, saying that it's complex.
They need time.
I'm curious.
Exactly how much time do you need to decide whether or not people should have basic rights?
I mean, can we get a timeline on that?
How long is it going to take for the Republican Party to figure that out?
This isn't a complex issue.
The reality is this is about the midterms.
If given the opportunity, Republicans will stall this until after the midterms
because they want to vote against it.
They want to shoot it down.
But they're afraid to do it when the voters have a voice.
It's that simple.
They want to vote it down.
One of their talking points on this is that there's still some finer details being worked
out in the text, and they won't get to see it until like Thursday,
which gives them the entire weekend to read the text.
I mean, I understand that y'all are supposed to be off, but given the fact that the Senate
only works like what, 165 days a year on average since 2001?
Maybe y'all could put in some overtime considering you're talking about the rights of 20 million
Americans.
That seems like something that might be worth putting in a little bit of extra effort for.
But it's not that.
There are a whole lot of people in the Senate, in the Republican Party, who if a vote is
taken before the midterms, well, they'll probably vote in favor of it.
But if it was taken afterward, when they feel safe, well, they'd vote against it.
That's the math.
That's the complex issue.
They have to figure out who's vulnerable.
Who is willing to sacrifice the rights of 20 million Americans to appeal to the base
that they have cultivated.
That's not really how a representative democracy is supposed to work, to be clear.
If a senator is afraid of making a vote, of going on the record about something this simple
just prior to an election, it's because they know the decision they would make, the vote
that they would cast, is unpopular.
Meaning they don't want to represent.
They want to rule.
They want to cast orders from up on high and be unaccountable.
No.
I think that y'all could read it over the weekend, since apparently it's going to take
more than, you know, Friday to read something that isn't really going to be that long.
I'm willing to bet I could read the whole thing in the course of one video.
I think that Democrats really should hold them to this.
Put the vote out there on Monday.
Either they vote in favor of giving rights to 20 million Americans or they vote against
it.
It's that simple.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}