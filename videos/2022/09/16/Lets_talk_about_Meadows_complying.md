---
title: Let's talk about Meadows complying....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pODbCwsRGM8) |
| Published | 2022/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on the Department of Justice's criminal investigation into January 6th.
- Meadows received a subpoena from the Department of Justice and complied by turning over similar documentation as provided to the committee.
- Meadows previously stopped complying after providing the committee with thousands of messages and emails.
- He had withheld documents citing executive privilege, a claim not pushed by the committee due to its intention to allow privacy from Congress.
- The Department of Justice, conducting a criminal investigation, may challenge Meadows' executive privilege claims.
- Trump's inner circle advised him to distance himself from Meadows, fearing Meadows may cooperate with the Department of Justice.
- Meadows, due to his position in the White House at the time, holds valuable information and may be pressured to cooperate.
- Meadows may face a choice between cooperating with the Department of Justice or facing potential charges.
- His cooperation or lack thereof may significantly impact the direction of the investigation and potential indictments.
- The lack of legal resistance from Meadows in complying with the subpoena indicates his understanding of the situation and potential consequences.

### Quotes

- "Meadows may be somebody that the Department of Justice is looking at as somebody that's a high priority to flip."
- "He knows a lot, he has access to a lot, and he would be one of the few people who can flip and provide insight into what Trump himself was doing."

### Oneliner

Beau provides insight into Meadows' compliance with the Department of Justice's investigation and the potential implications of his cooperation or lack thereof on the case.

### Audience

Legal analysts, concerned citizens

### On-the-ground actions from transcript

- Monitor updates on the Department of Justice's investigation to stay informed (implied).
- Support transparency and accountability in legal proceedings by following related news and developments (implied).

### Whats missing in summary

The full transcript provides detailed analysis and context on Meadows' compliance with the Department of Justice's investigation into January 6th, offering insights into potential legal battles and implications for the case.

### Tags

#DepartmentOfJustice #CriminalInvestigation #ExecutivePrivilege #Compliance #LegalProceedings


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to provide a little update
on the investigation into January 6th
being conducted by the Department of Justice.
This is the parallel investigation
to what the committee was talking about.
At the federal level, this is in fact
a criminal investigation, just to keep them all straight.
The Department of Justice gave a subpoena to Meadows.
That's what the reporting suggests now.
The reporting also suggests that he complied and turned over basically the same documentation
that he turned over to the committee.
Now I know a lot of people remember the fact that he stopped complying.
It's worth noting that before he stopped complying, he provided the committee with literally thousands
of messages and emails, all kinds of stuff.
And the reporting is saying that he gave the same stuff to the Department of Justice.
It's also worth remembering that he held a whole bunch of documents back under the claim
of executive privilege.
Now the committee really didn't push that too hard because the whole idea of executive
privilege is kind of to allow some privacy from Congress.
So it really is applicable there.
You can definitely make an argument that in that case it wasn't, but at least that's
in the spirit of it.
However, the Department of Justice is not Congress.
They're part of the executive branch, and they're conducting a criminal investigation.
I don't know if the Department of Justice is going to take the same view that the committee
had.
I don't know if they're just going to let that slide.
If they try to push for more, which I personally think they will, it will set up a legal battle
over executive privilege in this investigation as well. Remember, this is separate from the
documents investigation. Now Meadows is in a unique situation because Trump's current inner
circle has reportedly told Trump to cut ties with him. Now Trump, according to reporting,
hasn't, but the relationship has definitely changed. They keep in contact, but they're
not buddy buddy anymore. The reason Trump's inner circle told him this is because given Meadow's
position and the likelihood that he is totally unwilling to go down for Trump he may be somebody
that the Department of Justice is looking at as somebody that's a high priority to flip.
That may actually be the one piece of good advice that has been reportedly given to Trump from his
inner circle. I don't know how much good it would do at this point, because they've kind of
already talked about it and everything's already been done. But Meadow's position
in the White House during the time in question basically means he kind of
knows everything. So DOJ is going to be leaning on him very hard. He is
definitely going to be somebody who is going to be faced with a choice of
cooperate or, you know, go have a seat in the Defendants Chair,
assuming that the Department of Justice moves forward with charges.
If there is criminal activity to be found,
that is the position that Meadows is likely to find himself in.
He knows a lot, he has access to a lot, and he would be one of the few people
who can flip and provide insight into what Trump himself was
doing.
So the lack of legal maneuvering in trying
to get out of complying at all with the subpoena,
that leads me to believe that Meadows knows the score.
And I don't know how much of a fight Meadows would put up
when it comes to the Department of Justice challenging
the executive privilege claims.
Because I'm certain that Meadows understands
the situation he's in.
So this is another one of those.
You will see this material again.
If the grand jury does decide to indict, Meadow's position, whether or not he cooperates, is
going to be one of the things that shapes how prosecutors proceed.
So anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}