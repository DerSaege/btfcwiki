---
title: Let's talk about Trump's options and choices for leadership....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d1523D8Sdkk) |
| Published | 2022/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Trump's potential indictment and his choices as a former president.
- Trump's reckless statements and the need for leadership from him.
- The power of Trump's voice and ability to influence his supporters positively.
- The importance of Trump calming his base and diffusing tensions.
- Trump's legacy being shaped by his post-presidency behavior.
- Urging Trump to show leadership to avoid being seen as a mistake by American voters.
- Emphasizing Trump's ability to prevent bad outcomes by choosing to lead and calm his supporters.

### Quotes

- "His option is to lead, to diffuse the situation, to calm things down."
- "His legacy is being cemented by his post-presidency behavior."
- "His moment to finally be of value to this country."

### Oneliner

Beau addresses Trump's potential indictment, urging him to show leadership and calm his base to prevent negative outcomes and shape a positive legacy.

### Audience

Former Trump supporters

### On-the-ground actions from transcript

- Contact Trump supporters to urge calm and positive behavior (suggested)
- Advocate for peaceful actions and leadership in the community (implied)

### Whats missing in summary

Beau's detailed analysis and call for Trump to take responsibility and lead effectively can be best understood by watching the full transcript.

### Tags

#Trump #Leadership #Legacy #Responsibility #Community #PositiveBehavior


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk a little bit more about Trump
and what he said might happen if he was eventually indicted.
And we're going to talk about options and choices
that the former president has.
We're going to do this because I got a message.
And the question deserves an answer
because it's a question I think a lot of people might be asking.
In your recent video, you said you
thought what Trump was saying about what could happen
if he was indicted was reckless.
Seconds later, you said you didn't really
think he was wrong about what he was saying,
except maybe you disagree on the size.
If you think his followers might do that, why can't he say it?
What other choice does he have?
Off the top of my head, he could lead.
He could choose to lead.
He could choose this moment in his life
to show some leadership.
We know he has the ability to build a base.
But what we haven't seen is his ability
to guide them towards something positive.
It would be very easy for him to mitigate or eliminate
the possibility of bad things happening.
Have you noticed that when he talks about this and says,
oh, these bad things might happen,
he does nothing to cool the waters?
He doesn't try to calm his base at all?
He just says, this bad stuff would happen.
And he normalizes it.
This is a man that knows the power of his voice.
He knows the power of his words.
He got to watch it live on TV on the 6th
while they were putting themselves at risk.
His choice, his option, he could lead.
He could come out and say, I don't want my supporters
to do anything bad.
I don't want them to put themselves, their families,
their communities, and their country at risk.
If this happens, we're going to take it to court
and we're going to win.
And yeah, that may not be true.
That may not be true.
But I don't understand how a man who
has used the legal system his entire life
to get what he wants suddenly thinks
that working class Americans should go out and put
themselves at risk to help him.
His option is to lead, to diffuse the situation,
to calm things down.
And he has the ability.
If he can rile them up as he has, he can certainly calm them.
Patriots don't want to rip their country apart.
His legacy is being cemented not by what
he did during his presidency.
His legacy is being cemented by his post-presidency behavior.
If he wants a legacy that doesn't list him
as the greatest mistake the American voters ever made,
he has to show some leadership and he has to show it now.
He can calm things down, but he's choosing not to.
He has other options.
He could try to calm those who put their trust in him.
Those people who still believe in what he says,
he could calm them down.
Those are the people that might cause something bad to happen.
He has other options.
He has other choices.
And he's not exercising them.
The other thing that he might want to keep in mind
is his behavior when it comes to this
and his rhetoric, this type of stuff, it's not controllable.
It gets out of hand real quick.
He has other options.
He has the ability to stop this before it starts,
but he doesn't.
He feeds into it.
He normalizes it.
There are a whole bunch of people who, for whatever reason,
trust that man.
And with just a few words, he could stop bad things
from happening.
It's not that he can't say it.
It's that when he does, he doesn't condemn it.
He doesn't show any leadership whatsoever.
This is his moment to finally be of value to this country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}