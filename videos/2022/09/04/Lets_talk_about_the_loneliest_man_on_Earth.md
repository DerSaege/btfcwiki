---
title: Let's talk about the loneliest man on Earth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gHAoHK4C_2s) |
| Published | 2022/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of the man known as "the man of the hole" or the "loneliest man in the world."
- Describes how the man lived in the Amazon alone for 25 years after his community was attacked by cattle ranchers.
- Mentions the last attack on his community in 1995, when he became the sole survivor and eventually died.
- Points out the lack of knowledge about the man's community and customs due to failed contact attempts.
- Quotes Fiona Watson from Survival International about the deliberate genocide of the man's people by cattle ranchers for land and wealth.
- Emphasizes the violence and cruelty inflicted on indigenous peoples globally in the name of colonization.
- Talks about how the man resisted contact and just wanted to be left alone.
- Mentions Survival International's advocacy for uncontacted or recently contacted indigenous groups worldwide.
- Points out that similar situations exist globally, not just in the past.
- Addresses ongoing cultural assimilation and inequitable treatment of indigenous communities in the US and worldwide.

### Quotes

- "The deliberate wiping out of an entire people by cattle ranchers hungry for land and wealth."
- "Resistance. We can only imagine what horrors he had witnessed in his life and the loneliness of his existence after the rest of his tribe were killed."
- "It's happening now."
- "The loss to humanity of these cultures, it's still something that's occurring."
- "This type of fight, it's not over and it won't be over for a very very long time."

### Oneliner

Beau talks about the loneliest man in the world, a victim of genocide by cattle ranchers, shedding light on ongoing global struggles faced by indigenous communities.

### Audience

Advocates, activists, community members

### On-the-ground actions from transcript

- Support organizations like Survival International that advocate for uncontacted or recently contacted indigenous groups (suggested)
- Educate yourself and others about the ongoing struggles faced by indigenous communities globally (implied)

### Whats missing in summary

The emotional depth and impact of the man's solitary existence and the continued fight for indigenous rights globally.

### Tags

#IndigenousRights #CulturalGenocide #SurvivalInternational #Amazon #GlobalAdvocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about somebody who the media has described as
the man of the hole.
That's the term that they've latched onto in this coverage.
Another nickname that he'd been given was the loneliest man in the world.
And I think that's probably more appropriate.
He lived in the Amazon for a quarter of a century by himself.
He was the last remaining member of his community, part of an indigenous group
that had run into some issues with cattle ranchers who wanted their land.
This led to attacks in the 1970s and it culminated in I want to say 95 when he
was the only one left, the last of his group. He died. Now they're saying that he
had made preparations. There were colored feathers around him as if it was some rite.
But there's very little known about his community and their customs and
traditions because when they did have contact it didn't go well.
I want to read this. No outsider knew this man's name or even very much about
his tribe and with his death the genocide of his people is complete. For
this was indeed a genocide, the deliberate wiping out of an entire
people by cattle ranchers hungry for land and wealth. He symbolized both the
appalling violence and cruelty inflicted on indigenous peoples worldwide in the
name of colonization and profit, but also their resistance. We can only imagine
what horrors he had witnessed in his life and the loneliness of his existence
after the rest of his tribe were killed. But he determinedly resisted all
attempts at contact and made it clear that he just wanted to be left alone.
That is Fiona Watson. She's Survival International's research and advocacy
director. Survival International is a nonprofit global group that advocates
for uncontacted or recently contacted indigenous groups. You know in the United
States this isn't something that we think about anymore, but there are groups
like this, communities like this all over the world. And this group, this organization
that refuses to take government money, they attempt to establish protocols, make
sure that the communities have property rights over the lands that they have
inhabited in some cases for thousands of years. Something like this, you know, it
seems as though it would be something from history. And we can say that even in
this case it really stopped back in 95. Yeah, but it's going on in other places
and the impacts, they're still being felt. And the loss to humanity of these
cultures, it's still something that's occurring. You know in the US it
seems as though it's history. Even though it's not. The habit of attempting to
assimilate cultures that were here before the dominant culture now arrived,
it's still alive and well. The less than equitable treatment is still policy in a
lot of ways. This type of fight, it's not over and it won't be over for a very
very long time. And this is just one reminder of the fact that it isn't
ancient history. It's happening now. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}