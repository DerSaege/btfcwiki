---
title: Let's talk about the US military running low....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cX4PzAxYtiw) |
| Published | 2022/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the question of whether the US military is running out of supplies, specifically ammo.
- Clarifies that the US military is not running out of ammo but is running low on a specific type, 155 millimeter shells used in M777 howitzers.
- Explains that the US has provided over 800,000 rounds of this specific ammunition to Ukraine, causing the inventory to dip below the preferred amount.
- Compares the situation to managing inventory at a job, where running low prompts the need to order more.
- Emphasizes that this shortage is not a significant issue and that the US military will order more to replenish supplies.
- States that despite the low inventory, the US military's readiness is maintained and there is no system-wide problem.
- Assures that the United States will never completely run out of ammo and that this shortage is specific to one type used frequently in training or operations.

### Quotes

- "Our version of low is more than most countries have."
- "This isn't a system-wide thing where the US is running out of ammo."
- "It's time to order more."
- "The United States will never run out of ammo."
- "Y'all have a good day."

### Oneliner

Beau clarifies that while the US military is low on a specific type of ammo used in Ukraine, it is not running out overall, maintaining readiness and planning to restock soon.

### Audience

Military personnel, supporters, concerned citizens

### On-the-ground actions from transcript

- Contact military support organizations for ways to contribute (exemplified)
- Stay informed about military supply situations (implied)

### Whats missing in summary

Beau provides a clear and reassuring explanation about the US military's low ammo inventory, ensuring understanding and dispelling alarm surrounding the situation.

### Tags

#USMilitary #AmmoShortage #Readiness #InventoryManagement #UkraineSupply


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a question I never
thought I'd get, whether the US military is
running out of supplies.
There has been some reporting on this,
and it has been mostly accurate, but it's also
been framed in a way that has made people nervous.
And I had a question come in, so we're just going
to talk about it real quick.
My brother told me that the US Army is running out of ammo
because we're supplying Ukraine.
Is this true?
I mean, yes, but no.
These are, it's a relative concept.
OK, the US military is not running out of ammo.
US military is, the term that was used
was uncomfortably low when it comes to a specific type,
155 millimeter shells.
This is what's used in an M777.
It's howitzer ammo, artillery.
The United States has provided a little bit more
than 800,000 rounds of this stuff to Ukraine.
So it's low, uncomfortably low.
But we're the United States.
Our version of low is more than most countries have.
DOD conducts monthly inventories of this stuff.
So what's happened is the US has supplied Ukraine
with this particular type of ammunition.
And it's also used in training or used in other places
by the United States.
It dipped below the amount they like to have.
Think of it in terms of any job you've ever
had where there was inventory.
You probably had a PAR sheet or, well, today it's probably
done on a spreadsheet.
But there was probably a set number of item X
you're supposed to have on hand.
Let's say it's a restaurant.
You're supposed to have three boxes of hamburgers.
You know by Saturday night, you'll be at like 2 and 1
1.
What does that mean?
It's time to order more.
That's where they're at.
The framing of this has been a little alarmist.
This is not a huge deal.
They'll order more.
And then that stuff will sit there forever, too,
until it's used in training or whatever.
Now, a direct quote says, we're able to provide
what we have provided and still maintain our readiness
as a military force.
So what that tells me is that they have just barely dipped
below that magic number where they need to order more.
This isn't a system-wide thing where the US is running out
of ammo.
The United States will never run out of ammo.
It's specific to one type of ammunition.
And it's just something that has been used a lot over there.
And it's time to order more.
It's not indicative of any readiness failure
or any system-wide issue.
It'll be back up probably two months.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}