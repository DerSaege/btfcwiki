---
title: Let's talk about climate refugees in the US today....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=orkoucmkteY) |
| Published | 2022/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the impacts of climate change that are already affecting communities.
- Mentions Isle de Jean Charles, an island off the coast of Louisiana inhabited by a native group.
- Describes how the residents of Isle de Jean Charles are being relocated to a new development called New Isle due to their island disappearing underwater.
- Explains that New Isle came into being through a $48 million grant from HUD in 2016.
- Notes that Louisiana loses an area the size of Manhattan every year due to coastal erosion.
- Points out that the residents of Isle de Jean Charles are now considered climate change refugees.
- Emphasizes that this won't be the last story of people being forced to move due to climate change impacts.
- Urges for attention and action on climate change to prevent such situations from worsening.
- Concludes by stressing the importance of keeping climate change issues at the forefront of our concerns.

### Quotes

- "We now have official within the United States climate change refugees that were assisted in their move by the federal government."
- "This won't be the last coverage about a group of people who had to move because their home became uninhabitable."
- "This has to be on the ballot because it's gonna get worse."

### Oneliner

Beau warns about the real impacts of climate change through the relocation of Isle de Jean Charles residents, now climate change refugees, and calls for urgent attention and action as these situations worsen.

### Audience

Climate activists, policymakers, community organizers

### On-the-ground actions from transcript

- Advocate for policies addressing climate change impacts (implied)
- Support and amplify voices of communities affected by climate change (implied)
- Take action to reduce carbon footprint and mitigate climate change effects (implied)

### Whats missing in summary

The emotional toll on displaced communities and the need for global cooperation on climate action.

### Tags

#ClimateChange #EnvironmentalJustice #CommunityImpact #ClimateRefugees #Louisiana


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the climate
and impacts that we can see now,
that are starting now, that will get worse over time.
Off the coast of Louisiana,
there's a place, Isle de Jean Charles.
And it's an island that's home to a native group.
And I've read a lot about it,
and there's some conflicting information,
but they have lived there an incredibly long time.
And one of the things I read said
that there was a consolidation that occurred,
people trying to get away from the Indian Removal Act.
Very long time they've lived there.
But they're not gonna live there anymore.
They're going to live in a place called New Isle.
12 homes were just handed over to the residents.
Now they get the homes and lots for free.
They have to pay the taxes and insurance.
They stay there for five years.
They own the houses outright.
New Isle is a 515 acre location.
It's gonna be a development.
It came into being through a $48 million grant
back in 2016 from HUD.
Used to be a sugar farm.
This is happening because their island is going away.
It's going underwater.
Louisiana's coastline loses an area
about the size of Manhattan every year.
This is a real thing.
We now have official within the United States
climate change refugees that were assisted
in their move by the federal government.
This is gonna continue to happen.
This situation isn't going to go away.
It is going to get worse.
This movement that was described as bittersweet
and I'm sure losing a home,
an area that they could call their own
and restarting it.
I mean, the fact that there is a place for them to go,
that's cool, but losing it to the federal government
is a real thing.
For them to go, that's cool, but losing it to begin with,
kinda not, you know.
This will not be the last story like this.
This won't be the last coverage about a group of people
who had to move because their home became uninhabitable
because of rising sea levels or increasing temperatures
or a lack of water.
It's coming.
It's going to happen.
And with everything else that's going on,
it's something that we have to pay attention to
and we have to continue to keep on the forefront.
This has to be on the ballot because it's gonna get worse.
Anyway, it's just a thought.
Y'all have a good day.
Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}