---
title: Let's talk about the DISCLOSE Act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fCNjeW4XmNc) |
| Published | 2022/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of knowing where money in politics is coming from to help voters see through funding sources.
- Talks about the DISCLOSE Act, which aims to shine a light on dark money in politics.
- Mentions that the DISCLOSE Act has bipartisan support historically but is currently mainly supported by Democrats.
- Outlines provisions of the DISCLOSE Act, such as disclosing donations over $10,000 to Super PACs and banning foreign entities from contributing.
- Notes that the top five donors must be listed at the end of TV ads and donors influencing judicial nominees must be disclosed.
- Raises the question of whether it's worth investing time and effort into this Act, considering $2.6 billion was spent this way in 2020.
- Mentions that the Senate may vote on the DISCLOSE Act soon to restore faith in electoral politics and the judicial system.
- Expresses uncertainty about how effective the Act will be, especially in reshaping politics and influencing voters.
- Recommends the website Open Secrets as a tool to track who funds elected representatives and how their votes may be influenced by donations.
- Emphasizes that democracy requires advanced citizenship, where citizens need to pay attention and make informed choices.

### Quotes

- "Is this really worth putting some time and effort into?"
- "Democracy, having a representative democracy, a republic like we're supposed to have, it's advanced citizenship."
- "You know, that may have some impact."
- "The reason we got here is part corruption and part because a whole lot of people don't pay attention."
- "It's just a thought."

### Oneliner

Beau explains the significance of the DISCLOSE Act in shedding light on political funding to empower voters and restore faith in democracy and the judicial system, but questions its effectiveness in reshaping politics.

### Audience

American voters

### On-the-ground actions from transcript

- Contact your senators to express support for the DISCLOSE Act (implied)
- Use Open Secrets website to track donations to elected representatives (implied)

### Whats missing in summary

Full context and depth of Beau's analysis on the impact of the DISCLOSE Act and political funding transparency.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about politics and money
in politics and not really getting money out of politics,
but at least knowing where the money is coming from
in hopes that the American voter is smart enough
to see through who's funding it.
We're going to talk about the DISCLOSE Act.
This particular act has been supported throughout the years
by Republicans and Democrats.
Right now, it is being supported mainly by Democrats,
but there has been a call to kind of make it bipartisan.
We'll see how that goes.
The goal of it is not really to eliminate dark money,
but to at least shine a light on it.
The idea is that if somebody donates to a Super PAC
and it's more than $10,000, well, that gets disclosed.
Foreign entities are banned.
The top five donors get listed at the end of a TV ad,
which I find that funny.
And disclosure of donors for things
that are influencing judicial nominees.
This is the short version of what it hopes to accomplish.
Is this really worth putting some time and effort into?
$2.6 billion, with a B, were spent this way in 2020.
Yeah, it's probably worth knowing
where that money's coming from.
The Senate may vote on this as early as this week.
That's the plan.
Now, the goal is to help restore a little bit of faith
in electoral politics in general in this country,
because right now, most people, can't imagine why,
believe that the system is rigged,
in the sense that those with the money,
those with the power coupons, well, they have the power.
They're also hoping that it will restore
a little bit of faith in the judicial system,
because people will know where the influence is coming from.
I don't know how effective it's going to be.
It's putting a lot of faith in the average voter
paying attention more than the average voter
tends to pay attention.
You know, I talk about the website's open secrets.
I talk about that website a lot,
because it's a wonderful tool to find out
who owns your elected representative,
who actually put up the money to get them into office.
And you can see that a lot of times,
their votes reflect those donations.
That website's been around a long time.
It can't hurt, but I don't know how effective
it's going to be at actually reshaping politics.
The thing about the donors at the end of the TV ads,
that might do something, because let's say it is an ad
against a politician who wants windmills and solar energy,
and the ad is just ripping them for that.
And then at the end, it's paid for by massive oil conglomerate.
You know, that may have some impact.
But beyond that, beyond those types of incidents,
I don't know how much it's going to help,
but it's a tool that would be available
for those who wanted to make a more informed choice
and use electoral politics as best they can
and be as informed as they can.
You know, democracy, having a representative democracy,
a republic like we're supposed to have,
it's advanced citizenship.
You have to pay attention.
The reason we got here is part corruption
and part because a whole lot of people don't pay attention.
So we'll have to see what effect it actually has,
assuming it actually gets through.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}