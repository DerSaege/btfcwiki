---
title: Let's talk about Trump, the 11th Circuit, and documents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DkikDVVL_pw) |
| Published | 2022/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Late-breaking news regarding the 11th Circuit's decision to side with the Department of Justice on using documents with classification markings.
- Decision is about the search at Mar-a-Lago, not cases in Georgia, New York, or D.C.
- The 3-0 decision by the 11th Circuit was unanimous, with two of the three judges being Trump appointees.
- The judges' decision was critical, questioning why former President Trump needed the 100 documents with classification markings.
- The argument that declassifying the documents wouldn't change their content or make them personal is emphasized.
- The focus is on why Trump wanted to retain these documents despite clear indications of issues.
- Department of Justice is now allowed to proceed with the documents, likely to move quickly to avoid delays.
- Other documents will go through special master process, but those with classification markings are seen as a more serious issue.
- The seriousness of the situation will depend on why Trump had these documents and his intentions.
- Former President's legal strategy appears to be delaying tactics in hopes of Republican return to power to influence the judicial branch.

### Quotes

- "The desire to keep them after there was a clear indication that there was an issue, that is what this is really going to focus on."
- "The classification markings really don't matter because that's not what the laws hinge on."
- "It seems as though the only legal strategy they have here is to delay as much as possible."

### Oneliner

Late-breaking news on 11th Circuit's decision favors DOJ in using classified documents, raising questions on Trump's intentions and legal strategy.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Monitor and analyze the ongoing legal proceedings (implied)
- Stay informed about developments in the case (implied)

### Whats missing in summary

Detailed analysis and implications of the legal proceedings and decisions.

### Tags

#11thCircuit #DepartmentOfJustice #Trump #LegalStrategy #DocumentClassification


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about some late-breaking news from last night,
and it is news that certainly did not make the former president's day any better.
So we will talk about the 11th Circuit, documents, and decisions,
because the 11th Circuit decided to side with the Department of Justice
in the decision over whether or not they can use the documents
that are bearing classification markings immediately.
So to be clear, this is about the case dealing with the search at Mar-a-Lago,
not the case in Georgia or New York or D.C.
The decision was 3-0, it was unanimous.
And before anybody starts down in the comments section talking about the deep state or whatever,
I would like to point out that two of the three judges were, quote, his judges.
They were Trump appointees.
The decision itself was pretty scathing.
For our part, we cannot discern why former President Trump would have an individual interest in
or need for any of the 100 documents with classification markings.
Goes on, in any event, at least for these purposes, the classification argument is a red herring,
because declassifying an official document would not change its content or render it personal.
I feel like there have been people saying this since he first said that he blinked and nodded and declassified them.
So even if we assume that former President Trump did declassify some or all of the documents,
that would not explain why he has a personal interest in them.
And that's what this really is going to boil down to.
Why did he have them? Why did he want to keep them?
That's the big question. You know, they can try to frame this as a document storage issue all they want.
But the desire to keep them after there was a clear indication that there was an issue,
that is what this is really going to focus on.
So that allows the Department of Justice, this decision allows the Department of Justice to continue.
And my guess is they're going to go through this very quickly.
It's 100 or so documents and they'll be done as fast as humanly possible to avoid any other delay tactics.
Now, there are still a whole bunch of other documents that will go through the special master process.
But to be honest, and I don't mean to downplay it when I say this, but it's just reality.
Those documents, they're a speeding ticket.
The documents with the classification markings, well, that's something wrapped up in a tarp and duct tape in the trunk.
One is way more serious of an issue than the other.
And it's going to hinge on why he had them, what he wanted to do with them, why he wanted to retain them.
That question of why is going to determine how serious this gets.
Again, the classification markings really don't matter because that's not what the laws hinge on.
They hinge on national defense information.
And the content, like the judges said, wasn't changed even if he did declassify them.
So this was really bad news for the former president, who at this point,
it seems as though the only legal strategy they have here is to delay as much as possible in hopes of getting,
in hopes of the Republicans returning to power and then therefore exerting pressure on the judicial branch.
It seems to be the current strategy.
Can't say that for sure, but that's certainly what it appears to be.
Because the arguments that are being put forward, they're not...
they aren't arguments that would actually defeat a charge.
So we'll have to wait and see how it plays out over the rest of the week.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}