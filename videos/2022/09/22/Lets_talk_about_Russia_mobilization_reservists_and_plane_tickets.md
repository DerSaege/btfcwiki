---
title: Let's talk about Russia, mobilization, reservists, and plane tickets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2Rz9UFjcqjk) |
| Published | 2022/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin ordered a partial mobilization of around 300,000 Russian reservists, who are not equivalent to American reservists but amateurs forced into military service.
- Russian reservists are being sent to face combat-hardened Ukrainian military, which is likely to end poorly due to their lack of training, equipment, and morale.
- Demonstrations and protests have broken out across Russia against Putin's decision, showing cracks in the facade of domestic tranquility.
- Efforts to insulate the civilian population and maintain economic stability in Russia are failing, leading to domestic turmoil.
- The price of plane tickets out of Russia has skyrocketed as people try to flee being forced into a conflict they have no desire to partake in.
- The best chance for a Russian reservist is to break away from their unit and turn themselves into the Ukrainians upon arrival.
- The war in Ukraine is deemed lost, with the political objectives failing and the conflict being about imperialism and ego rather than ideals worth fighting for.
- Continued protests, desertion among reservists, and low morale are expected consequences of Putin's decision for a partial mobilization.
- A full mobilization may not significantly change things on the battlefield and may result in unnecessary waste of lives and resources.
- The Ukrainian military's ability to disrupt movements behind the lines may prevent many reservists from even reaching the front lines.

### Quotes

- "This is not something that is likely to turn the tide there, despite the numbers involved, because it's quantity and quality type of thing."
- "At the end of this, what you have is another rich man sending a whole bunch of poor boys off to fight his war."
- "The war is lost. The political objectives of the war were lost as soon as other countries decided it was better for them to join NATO."
- "The cracks in the facade are showing."
- "The greatest chance of making it for a Russian reservist is to immediately, upon arrival, break away from their unit and turn themselves into the Ukrainians."

### Oneliner

Putin's order for a partial mobilization of Russian reservists reveals cracks in the facade of domestic tranquility and leads to protests, economic instability, and a rush for plane tickets out of the country.

### Audience

Activists, Peace Advocates, Humanitarians

### On-the-ground actions from transcript

- Join protests against Putin's decision for a partial mobilization (exemplified).
- Support efforts to aid Russian reservists who may seek to desert and avoid forced combat (exemplified).
- Provide assistance to those trying to leave Russia to avoid involvement in the conflict (exemplified).

### Whats missing in summary

Insights on the potential long-term effects of Putin's decisions on Russia's domestic situation.

### Tags

#Russia #Putin #Reservists #Ukraine #Protests #Imperialism


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the news coming out of Russia, and
there's a lot of it.
So we'll go through it piece by piece and try to put it all into context.
So today we will talk about Russia, reserves,
facades, the price of plane tickets, and a whole bunch of other things.
If you missed it, Putin, in his infinite wisdom,
has decided to order a partial mobilization of around 300,000 reservists.
That seems like a good place to start, the military aspects of this.
First and foremost, Russian reservists are not like American reservists.
They're not the same.
They are not equivalent in any way, shape, or form.
American reservists are still pretty much on par.
Russian reservists are people who did a couple years in the military a decade ago
and then returned to normal working class life.
They are not warriors.
They're amateurs.
And they're being forced against their will to go through a basic refresher,
and then they're gonna be sent into the grinder to face off against
combat-hardened people.
This isn't going to go well.
If the Ukrainian military was capable of defeating and
stalling Russian active duty, they are also capable
of defeating and stalling Russian reservists, people who will have worse
equipment, worse training, and worse morale.
This is not something that is likely to turn the tide there,
despite the numbers involved, because it's quantity and quality type of thing.
They're not gonna know what they're doing.
The Russian people know what I just said.
Because of that, demonstrations and protests have broken out across the country,
to include in Moscow and St. Petersburg, two locations that Putin has gone
out of his way to insulate from the effects of his little misadventure.
There have been hundreds of arrests.
The morale and support for
this engagement is fleeting at best.
At the end of this, what you have is
another rich man sending a whole bunch of poor boys off to fight his war.
And it's over ego.
The demonstrations are showing cracks in the facade
of the domestic tranquility that has been portrayed.
When the conflict started, there were a lot of people who were surprised
by the unexpectedly poor performance of the Russian military.
There were a lot of people who realized at that point that it was a facade and
that the cracks started to show immediately.
Even those of us who were aware that the Russian military wasn't quite
as good as it was portrayed and believed the Ukrainian military was going to do
better than expected, I mean, most of us were surprised by how big those cracks got.
The same thing is now happening domestically.
All of the efforts to insulate
the civilian population from the conflict are failing.
The efforts at cooking the books to keep the economic picture rosy are failing.
This is the start of a lot of domestic turmoil for Russia.
How bad it gets?
Well, we don't know.
What we do know is that the price of plane tickets to leave the country
has skyrocketed, and by some reports, the tickets sold out.
Because people want to get out before they are forced against their will
to go into a combat zone that they have no business being in.
The greatest chance of making it for
a Russian reservist is to immediately, upon arrival, break away from their unit
and turn themselves into the Ukrainians.
That is probably their best chance of returning home vertically.
This is a horrible idea for the people who are going.
And again, at this point, please remember, the war is lost.
The political objectives of the war were lost
as soon as other countries decided it was better for them to join NATO.
Remember, war isn't about the battle.
War is a continuation of politics by other means.
Those political objectives have failed.
At this point, it's just about dirt.
It's about imperialism.
It's about nothing that any of these people want to fight for.
These are normal, working class people now.
These aren't warriors.
And it's just going to lead to a bunch of waste.
The cracks in the facade are showing.
Putin's position is growing more and more precarious.
The likelihood of continued protests is pretty high.
The likelihood of desertion among the reservists is high.
The morale is going to be low.
This isn't the right move for Russia.
And this is in response to a partial mobilization.
Putin has toyed with the idea of a full mobilization.
Now, is this going to change things on the battlefield?
It could, theoretically, if they treat the reservists like prisoners
and do not give them a chance to leave,
or they use them in a wave fashion.
Possibly.
At the same time, the Ukrainian military has shown an ability
to disrupt movements behind the lines,
which means that I'm guessing that a lot of the reservists,
they may never actually see the front lines.
They may be returning home before then, or they'll be left there.
This is going to amount to a bunch of waste that doesn't need to happen.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}