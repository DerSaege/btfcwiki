---
title: Let's talk about Memphis and how it can change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RTXgX23RmnE) |
| Published | 2022/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a familiar story that plays out too often, leading to questions and statements that result in little change.
- Raises the possibility of a different outcome by utilizing existing technology after an incident in Memphis.
- Suggests sending a text to the entire city to alert them of what was happening, potentially altering the course of events.
- Points out that a bill addressing this technology was proposed in the House but faced pushback.
- Criticizes individuals who may express regret and send thoughts and prayers but resist practical solutions when presented.
- Notes that the bill might still be pending in the Senate Judiciary Committee.
- Emphasizes that simple actions, like utilizing technology for alerts, could significantly impact outcomes but are not being implemented.
- Criticizes politicians for prioritizing rhetoric over implementing tools that could save lives.
- Anticipates future similar incidents until necessary actions, like sending texts for alerts, are put in place.
- Concludes with a reflection on the government's tendency to delay action until exhausting other options.

### Quotes

- "What if after that first or that second incident yesterday, a text went out to the entire city telling them what was happening?"
- "Those tasked with governing seem incapable of applying the simplest things to help the action."
- "There will be another one. And probably another one."
- "It will do the right thing after it tries everything else first and ignores the problem for years."

### Oneliner

Beau suggests utilizing existing technology, like sending texts for alerts, to potentially alter outcomes in recurring incidents, criticizing the lack of action and prioritization of rhetoric over practical solutions.

### Audience

Advocates for Change

### On-the-ground actions from transcript

- Advocate for the implementation of alert systems utilizing existing technology (suggested)

### Whats missing in summary

The full transcript provides a deeper insight into the recurring cycle of incidents and the potential for simple technological solutions to prevent harm and save lives.

### Tags

#Technology #AlertSystems #GovernmentInaction #PolicyChange #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about yesterday.
It's a familiar story.
Plays out all too often.
And there's normally the same cycle
after each time this story occurs,
each time it plays out.
The same questions get asked, the same things get said,
and nothing really changes.
So we're going to talk about how it could be different.
I think most people know what happened in Memphis yesterday.
It's not like it's a new occurrence.
But what if something simple could have changed it?
What happened could have been limited.
Through a technology we already have, we use it all the time.
And it was floated as an idea.
In fact, there was a bill that went through the House on it.
What if after that first or that second incident yesterday,
a text went out to the entire city telling them
what was happening?
Think people, I don't know, might have locked doors
at businesses, got off the street?
Think it would have altered the outcome?
Imagine it would have, a little bit.
The very least, it would have made people more aware.
Simple thing.
We have the technology, we use it for other stuff all the time.
But when the idea came up to use it for this, a text,
it got pushback because people are so ingrained
in their positions that they don't want
to look for actual solutions.
The same people who today are probably
going to express their regret and send their thoughts
and prayers, they might have voted against it.
If I'm not mistaken, I think the bill's actually still alive.
It's just sitting in the Senate Judiciary Committee.
Could be wrong.
I didn't look it up.
But I think that's what's happened with it so far.
That technology, just a text, could have altered the outcome
of this. It could have made it less horrible.
But we're not acting.
Those tasked with governing seem incapable of applying
the simplest things to help the action.
To help the average American.
I mean, how long would it take to set this up?
If resources were actually applied to it, what, a week?
It already exists.
We have Amber Alerts.
We have all kinds of things.
It could have made a difference here.
But politicians looking at a midterm election,
they wanted that rhetoric.
That was more important than actually fixing something.
Actually providing a tool that could save lives.
There's going to be another one.
Just, I mean, I know people who watch this channel,
you know this.
There will be another one.
And probably another one.
And they'll keep happening.
And my guess is that we'll go through at least a couple more
before this technology really gets implemented.
Before those texts start going out.
Because if there is one thing we have learned about the US government,
is that it will do the right thing after it tries everything else first
and ignores the problem for years.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}