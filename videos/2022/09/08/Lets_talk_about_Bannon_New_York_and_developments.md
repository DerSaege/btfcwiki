---
title: Let's talk about Bannon, New York, and developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wAj9uMnejrM) |
| Published | 2022/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Bannon is expected to surrender to face a new indictment related to the build the wall fundraising efforts, but the details of the indictment are sealed.
- Steve Bannon is framing the indictment as a political hit job, fueling the far right's views on a politicized criminal justice system.
- Bannon's arrest is likely to energize the far right, viewing it as an attack on their people through the justice system.
- The reaction and statements from Trump regarding Bannon's case will significantly influence how the far right base responds.
- Republican strategists hope that Trump remains quiet as his reaction could either motivate or harm the far right base, impacting the midterm strategy.
- Bannon is also facing two counts of contempt, adding to the legal issues he is dealing with.
- The unfolding details of Bannon's case are expected to dominate news cycles in the coming days as more information emerges.

### Quotes

- "It's a weaponized criminal justice system."
- "His arrest is likely to energize the far right."
- "This is a moment where Trump might come out and have a reaction to this that really hurts Republicans."
- "His reaction is going to definitely steer the far right reaction."
- "But it's likely that this may end up becoming the dominant story over the next few days."

### Oneliner

Beau outlines Bannon's new indictment, his framing of it as a political hit job, and the potential impact on the far right base depending on Trump's reaction.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Stay informed about the unfolding details of Bannon's case (implied).

### Whats missing in summary

Insight into the potential broader implications of Bannon's legal troubles on political landscapes and movements.

### Tags

#Bannon #SteveBannon #Indictment #FarRight #Trump #RepublicanStrategists


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Bannon
and the state of New York and the developments there,
what we know, what we don't, and what to be on the watch for.
Now, at time of filming, everything
is kind of up in the air.
If Bannon's lawyer is correct, about an hour
before this video gets published,
Bannon will have turned himself in to the state of New York,
will have surrendered himself to face a new indictment.
What is that indictment?
Well, we don't know yet.
It's sealed.
The conventional wisdom says that it
has to do with the build the wall fundraising efforts,
a $25 million or so fundraising effort.
The allegations are that some of that money, well,
it didn't exactly go where it was supposed to.
Now, if some of the donors were in New York, that makes sense.
There's also an expectation that this case is going
to delve into other things.
But that's rumor.
We don't know any of this yet because the indictment is still
sealed right now.
Should find out more tomorrow.
Well, today, when you're watching this.
How is Bannon dealing with it?
Bannon is framing it as a political hit job.
It's a weaponized criminal justice system.
That's the public line that's going out.
That's how he is viewing it.
Now, as odd as this may seem to a whole lot of people
who watch this channel, Steve Bannon is a far right thought
leader.
He's important to them.
His arrest is likely to energize the far right.
They will view this as another attempt
to get one of their people through a politicized
criminal justice measure.
That's how it's going to be presented.
What really matters, though, in the scheme of how the far right
reacts is how Trump reacts and what Trump
says in regards to this case.
Now, according to the Republican strategy for the midterms,
Trump should basically be in hiding at the moment.
He shouldn't be out making waves.
We know that he is, but that's not part of the strategy.
This is a moment where Trump might come out and have
a reaction to this that really hurts Republicans.
It all depends on how he views it and how he frames it,
because it's either going to motivate that far right base
in a good way, a bad way.
It has a lot to do with Trump himself.
I am certain that Republican strategists
hope that he just stays in his room and doesn't come out,
and somebody takes his phone, and he
doesn't get on his social media network and say anything.
But that seems kind of unlikely.
So his reaction is going to definitely
steer the far right reaction.
Now, this case is on top of the two counts of contempt
that Bannon is going to be sentenced for soon.
So there will be more information
about this coming up in the next few weeks.
This coming out, my guess is today, later today,
as you all watch this.
But it's likely that this may end up
becoming the dominant story over the next few days,
as more and more is learned.
So anyway, it's just a thought.
And I have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}