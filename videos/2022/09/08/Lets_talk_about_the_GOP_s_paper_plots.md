---
title: Let's talk about the GOP's paper plots....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZE6pGS3ZgfI) |
| Published | 2022/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the idea of the Republican Party having a plot to subvert the US Constitution through a constitutional convention.
- Explains that even if the amendments were pushed through, they still need to be ratified by 38 states, which is unlikely.
- Points out that the Republicans are resorting to this scheme because they lack the power and support to achieve their legislative goals through normal means.
- Emphasizes that the fixation on quasi-legal theories shows the weakness of the Republican Party.
- Mentions that if the Republicans manage to subvert the Constitution and push through amendments, it won't hold as they lack the consent of the governed.
- Draws parallels with Prohibition as an example of an unpopular amendment that eventually went away.
- States that progressives shouldn't fear this Republican tactic but should be prepared to counter it, as it indicates that progressives are winning.
- Notes that the Republicans' attempts to skirt the Constitution alienate more people and reveal their true intentions of wanting to rule rather than represent.
- Concludes by stating that this situation is a sign of progressives winning and that it's not something to fear but rather a display of the Republicans' disconnect from the majority of Americans.

### Quotes

- "They call themselves patriots while trying to find some way to skirt the constitution, which undoubtedly is their profile picture."
- "It's a sign that progressives are winning."
- "It's not a scary thing."

### Oneliner

Beau explains the Republican Party's scheme to subvert the Constitution, showcasing their weakness and lack of support, a sign that progressives are winning.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Be aware of the Republican Party's tactics and be prepared to counter them (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's motivations and actions, offering insights into their weaknesses and the implications of their schemes.

### Tags

#RepublicanParty #USConstitution #Progressives #PoliticalAnalysis #PowerStruggles


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about an idea
that has kind of floated up
and entered public consciousness in a way
that for a lot of people is probably unnerving.
And it's the idea that the Republican Party
has another paper plot underway,
another scheme with which to subvert the US Constitution,
try to use a loophole, a technicality,
to get away from what it really meant,
and force amendments to the Constitution.
Now, the idea was to use a bunch of states
to call for a constitutional convention,
and then from there push the amendments through.
Okay, sure, let's say they do it.
Then what?
Well, I mean, if you actually believe in the Constitution
and you're not just some authoritarian goon out for power,
well, then the states have to ratify it,
three quarters of them.
So 38 states would have to ratify these amendments.
That's not gonna happen.
That's not realistic.
So they would have to scheme
and find some way to subvert the Constitution there
and get around that process.
So they can force their will on the people, right?
Okay, so what does this really tell us?
Better yet, a question.
Why don't they try to achieve these legislative goals
through normal legislation?
They can't.
They don't have the power.
They're too weak.
That's why they don't do it that way.
So they're trying to use some technicality
because they know they don't have the power.
They don't have the support.
And this is a way to try to cling to power
rather than change their positions.
That's what it's telling you.
The Republican Party fixating
on all of these quasi-legal theories,
these ideas of trying to exercise power
by skirting the Constitution,
it shows that they're weak
and that they know they're weak
and that they know they're not getting stronger.
Because if they actually had support,
they'd be able to do it through normal legislation.
But they don't have it.
So let's say they do it.
They subvert the Constitution.
They change the way things are ratified.
They push this stuff through,
through some quasi-legal means,
but they don't have the support.
What happens?
Nothing.
Nothing.
Nothing happens.
They don't have consent of the governed.
It's not like this concept
hasn't been tried before in the United States.
I'm pretty sure there was like this period called Prohibition
that was brought about by an amendment
that didn't have public support.
What occurred?
It went away.
That's what would happen here.
This push shouldn't be something
that scares progressives.
I mean, be aware of it, be ready to counter it,
but it shouldn't be scary
because it's a sign that you're winning.
They know they don't have the support
to push through legislation through normal means.
So they have to try to wiggle their way through.
And they don't have any hope of gaining that power back
because they're that out of touch
with the majority of Americans.
They don't understand where the country has gone
and they're confused.
So they're lashing out.
They call themselves patriots
while trying to find some way to skirt the constitution,
which undoubtedly is their profile picture.
It's not a scary prospect.
It's a sign that progressives are winning.
Every time they try something like this,
they alienate more and more people
who see them for what they are.
People who don't want to represent,
but people who want to rule,
who want to force their ideological,
their political, and their religious beliefs
on the rest of the country,
even though they don't have consent of the governed.
It's not a scary thing.
It's a sign that progressives are winning.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}