---
title: Let's talk about a Cuban referendum and party and policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qYTOR5XmlJU) |
| Published | 2022/09/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines how party allegiance can overshadow supporting policies in one's own country.
- Cuba had a referendum on a new family code, including same-sex marriage and adoption rights.
- 74.1% voter turnout with two-thirds in favor of the new code.
- Those opposed to the government claimed only 47% of eligible voters voted yes.
- Government supported expanding protections, while anti-government groups opposed it out of habit.
- The opposition movement framed themselves as against rights for vulnerable groups.
- Emphasizes the issue of putting party over policy to cast oneself as pro-freedom.
- Notes the dynamics of social conservatism and evangelical involvement in Cuba's opposition.
- Draws parallels with the U.S., where parties claim to support freedom but may undermine rights.
- Urges individuals to think independently about their beliefs and not blindly follow party lines.

### Quotes

- "It was that habit of putting party over policy."
- "Don't listen to the talking heads. Think about the world you want to leave behind."
- "Maybe it's not really the pro-freedom party."

### Oneliner

Examining how party allegiance can overshadow supporting policies, using Cuba's referendum as a case study, Beau urges individuals to think independently about their beliefs rather than following party lines blindly.

### Audience

Voters, Political Activists, Community Members

### On-the-ground actions from transcript

- Question your beliefs and ideologies, independent of party influence (suggested)
- Advocate for policies that support marginalized groups in your community (implied)

### Whats missing in summary

The full transcript provides deeper insights into the dynamics of party allegiance versus policy support, urging individuals to critically analyze their beliefs and political stances.

### Tags

#PartyAllegiance #PolicySupport #Independence #CubaReferendum #PoliticalBeliefs


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about what a
referendum in Cuba can teach us about party and policy. We often talk on this channel about how
a lot of people put party over the policies they actually believe in and claim to support.
However, when you're talking about your own country it gets hard to see at times because
no matter how independent you are you lean one way or the other. So you may succumb to the idea
of an allegiance to a party even if just subconsciously. And sometimes you may look
away when that party supports a policy or opposes a policy that you feel strongly about.
Okay, so what's this have to do with Cuba? Cuba had a referendum on a new family code.
This code basically provided greater protections for women, kids, the elderly,
grandparents, seeing parents, and the thing that made the most news, same-sex marriage,
and their ability to adopt. 74.1% voter turnout. Two to one in favor. Okay.
Now, that's a clear win. By U.S. standards that is a clear win. Two to one in favor of this.
74% voter turnout. These are decent numbers, right? Now down there, very loosely, you have
two main sides. People who are supportive of the current government and people opposed to it,
right? Those in opposition to the government are now saying that only 47% of voters voted yes.
I mean, it was two to one in favor of it when you're talking about those who voted,
but they're using eligible voters. And they're saying that the 74% voter turnout was due to
them saying that people should boycott this. You don't participate in this system.
I have questions about that. I think there might be other things that contributed to
a lower than normal voter turnout, such as the storm. That might have had something to do with
it, just saying. But so what happened was the government very heavily came out and said,
yes, we need to do this. We need to expand these protections. We need to provide these rights.
The anti-socialist groups, those that are against the current government,
took the bait. They came out and said, no, no, don't participate. Don't agree with this
because they're so accustomed to being in opposition to the government as a whole.
So the movement, the opposition movement in Cuba that wants to be seen as the pro-freedom party
has come out and said, no, let's not extend these protections. Let's not give these people rights.
They took the bait. So they have successfully framed themselves as people who don't care about
kids, women, the elderly, or gay people. And they have framed the socialist government
as the revolution that is continuing to look out for those people.
This wasn't the government that did this. The government just said, hey, this is a good thing.
Let's do it. It was that habit of putting party over policy. If you are looking to
cast yourself as the people trying to bring more freedom to a country and you actively
campaign against something that brings more freedom to the people of that country,
there's probably an issue. But it's reflexive. The opposing party says, hey, let's do this.
No. Even if it's something that they might support. Now, in all honesty, when you get
way deeper into the dynamics, and please understand, I am way over simplifying Cuba.
But if you get deeper into the dynamics, it has to do with a lot of people being socially
conservative and the evangelicals being involved. And that's why there was some opposition.
But it was generally championed by the government and opposed by those who want to see the government
there fundamentally changed. This happens in the U.S. a lot. You have a party in the United States
screaming that they are the pro-freedom party while constantly trying to undermine workers' rights,
women's rights, LGBTQ rights, without end. Because the other side has taken the position
of being supportive of those things. It might be a good idea for everybody to kind of
take stock of where they sit ideologically and what they really believe in when it comes to
these policies. Don't listen to the talking heads. Don't listen to your party's thought leaders.
Think about the world you want to leave behind. I would be willing to bet that there are a whole
lot of people who may be just ardent supporters of a particular party who find out that that party,
well, maybe it's not really the pro-freedom party. Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}