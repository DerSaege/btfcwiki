---
title: Let's talk about Texas and Paxton out for a little run....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Tqd3go5tPc4) |
| Published | 2022/09/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Attorney General Ken Paxton of Texas, facing legal troubles, ran away from a process server trying to serve him a subpoena. 
- The subpoena was related to a federal court hearing involving nonprofits suing over Texans receiving family planning services outside the state.
- Paxton's wife assisted in his avoidance of the process server by driving him away in a truck.
- Despite multiple attempts by the process server to deliver the documents, Paxton evaded receiving them.
- Paxton later defended his actions on Twitter, citing concerns for his family's safety.
- The incident has raised concerns within the Republican Party regarding legal actions taken against its members.
- The behavior exhibited by Paxton, including multiple instances of running away, is seen as dramatic and unusual for an attorney general.
- There are doubts about Paxton's reasons for avoiding the process server and his perceived level of threat.
- The federal judge overseeing the case is likely to be troubled by Paxton's avoidance of the legal proceedings.
- The situation portrays a high-ranking official trying to evade accountability in a legal matter.

### Quotes

- "Attorney General Ken Paxton of Texas literally ran away from a process server trying to serve him a subpoena."
- "The incident involving Paxton has raised concerns within the Republican Party regarding legal actions taken against its members."
- "The federal judge overseeing the case is likely to be troubled by Paxton's avoidance of the legal proceedings."

### Oneliner

Attorney General Ken Paxton ran from a process server, raising concerns within the Republican Party about legal accountability.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Contact local representatives to express concerns about public officials evading legal accountability. (implied)
- Support organizations advocating for transparent and accountable leadership in government. (implied)

### Whats missing in summary

Further details on the specific legal implications for Paxton and the potential consequences of his actions.

### Tags

#KenPaxton #Texas #LegalAccountability #RepublicanParty #Transparency


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about a
story out of Texas that is just wild but also kind of unsurprising involving the attorney general
out there, Ken Paxton. If you are not familiar with attorney general Paxton, he is a controversial
figure. He has been under indictment for securities fraud for years and is subject to some other
legal action as well. He decided it was time to get out of Dodge, at least temporarily, because
according to an affidavit filed in federal court, Paxton literally ran away from a process server
who was trying to serve him a subpoena and fled the area in a truck driven by Paxton's wife.
So the subpoena itself is for a federal court hearing dealing with nonprofits that have filed
a lawsuit and it deals with basically them wanting to pay for Texans to get family planning services
outside of the state. So that's what this is about. It's about nonprofits suing normal kind of
attorney general stuff. So the server, the process server, went to Paxton's house, knocked on the
door. A woman answered the door and the process server was like, hey, I need to see Paxton. I have
some documents for him. And she said, well, he's on the phone. And the process server's like, I'll
wait because that's kind of their job, you know. About an hour later, a Tahoe pulled in the driveway
and Paxton exited the house and the process server called his name, started walking towards him,
and Paxton literally ran back inside. And from there, Paxton's wife came out and got in a truck
and opened the doors on the truck. Then Paxton came out of the house again. And this guy's calling
his name, trying to get his attention to provide him these documents. Paxton's wife came out of the
house again, trying to get his attention to provide him these documents. Paxton maybe didn't hear him,
but ran to the truck and got in. The man, you know, said that he was there to provide the documents
and put them on the ground near the trucks. And the vehicles left without them getting the documents.
So, I mean, that is entertaining. And then Paxton kind of put out a statement via Twitter
that the media was blowing it out of proportion because he was worried about a stranger,
you know, hanging around his house, and he was worried about the safety of his family.
If the affidavit is true and that order of events is accurate, I mean, I have some questions about
that. But the general takeaway here is that members of the Republican Party are now
concerned when it comes to legal action being taken against them. In this case,
this is something that is fairly routine for an attorney general to deal with.
The multiple instances of running seems a bit too much.
Multiple instances of running seems a bit dramatic. But we also don't know Paxton's frame of mind or
who he thought this person was. But either way, I'm fairly certain that the federal judge is going
to be concerned about Paxton's unavailability for the proceedings. Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}