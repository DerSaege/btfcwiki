---
title: Let's talk about women's rights, republicans, polls, and reality....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TbrNh5s3DoQ) |
| Published | 2022/09/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into women's rights, freedoms, polling insights, and the implications for the Democratic Party.
- A CBS News poll revealed that 68% believe rights and freedoms are at stake in the midterms.
- When asked about the impact of Republican control on women's rights, 43% said fewer rights and freedoms, 39% said no change, and 18% said more rights and freedoms.
- Beau questions the 18% who believe women will have more rights under a Republican-controlled Congress, deeming it a denial of reality.
- He stresses the importance of focusing on the 39% who believe things won't change, as they are potentially persuadable by the Democratic Party.
- Beau criticizes the Republican Party's stance on women's rights, noting their patriarchal culture and limitations on women's freedoms.
- He suggests that Democratic efforts should target the 39% who don't expect a change in women's rights and freedoms under Republican control.
- Beau asserts that there is no scenario where a Republican-controlled Congress enhances women's rights, given their historical positions and rhetoric.
- He advocates for using poll insights to shape policy decisions and refine Democratic messaging to appeal to the persuadable 39%.
- Beau concludes by urging the Democratic Party to focus on reaching out to the undecided 39% to influence their voting decisions.

### Quotes

- "You are never going to reach these people."
- "There is no objective reality in which a Republican-controlled Congress leads to more rights and freedoms for women."
- "39% of people are up for grabs."

### Oneliner

Beau delves into polling insights on women's rights, urging the Democratic Party to focus on reaching the persuadable 39% instead of those denying reality.

### Audience

Democratic activists and strategists

### On-the-ground actions from transcript

- Reach out to the persuadable 39% with tailored messaging and policy proposals (implied)
- Advocate for women's rights and freedoms within local communities (implied)

### Whats missing in summary

The full transcript provides additional context on the importance of polling insights in shaping policy decisions and messaging strategies for political parties.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about women
and rights and freedoms and polling
and what polling can teach us about objective reality
and where to focus your efforts.
CBS News conducted a poll.
And there were a couple of interesting pieces in it.
Two of them we're going to focus on right now.
One of the questions was, what's at stake in the midterms?
And 68% of people said your rights and freedoms.
And I think that that's probably a very accurate depiction.
Then there was another question.
And it said, if Republicans control Congress,
women will have.
And then it had three options, fewer rights and freedoms,
more rights and freedoms, or things won't change.
43% said fewer rights and freedoms.
39% said no change.
And 18% said more rights and freedoms.
So this is why, when we talk about people both sides
and things that don't have two issues,
this is one of those moments.
This 18% of people, they have chosen, one way or another,
to disregard reality.
You are never going to reach these people.
This 18% of people, they have literally,
I reject that reality and substitute my own.
If you are a Republican and you believe
that women will have more rights and freedoms,
I will have a pinned comment down below on YouTube.
Please list them.
I can think of a couple of things
that you might be able to try to frame that way.
But there's no way that those small examples would
overshadow everything else.
These people have just rejected reality.
But because it makes up 18%, people
will try to both sides this.
You'll see it in the media because of this poll, probably.
But what does this tell the Democratic Party?
While 68% of people believe that rights and freedoms
are at stake, only 43% believe that women
will have fewer rights and freedoms
under a Republican-controlled Congress.
That 39% that think things won't change,
that should be the group of people
that the Democratic Party is reaching out to and saying,
hey, yeah, there's the obvious family planning thing
that everybody's talking about.
But then you also have normal birth control.
You have family planning designed to increase births.
You have candidates for the Republican Party
suggesting that allowing women to vote was a mistake.
The general culture tone and the rhetoric
supports the furtherance of a patriarchal society.
That 39% of people can be reached.
And that's where the Democratic Party
needs to be focusing their efforts.
There is no objective reality in which
a Republican-controlled Congress leads to more rights
and freedoms for women.
That's not a thing.
There's nothing in their platforms
that suggests that's even something they want to attempt.
In fact, most of the rhetoric,
most of their overtly stated positions
suggest they want to limit women's rights and freedoms,
with the possible exception of having the right and freedom
to do what you're told to by some mediocre man
who thinks that by the virtue of his birth,
he should have a say-so.
Polls like this, where they actually ask questions
that relate to objective reality,
can be really important and should guide policy
and should guide Democratic messaging.
39% of people are up for grabs.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}