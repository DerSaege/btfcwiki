---
title: Let's talk about understanding the other side....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GCA0a4tygfM) |
| Published | 2022/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about trying to understand and relate to the other side's perspective, even if it's conflicting.
- Mentions how people in Russia only mobilized against the war when they faced the risk of being drafted.
- Acknowledges that the same behavior can be seen in the United States, where people may not protest against injustices until it directly affects them.
- Expresses sympathy for regular people being forced into war due to the actions of those in power.
- Points out the increased likelihood of atrocities when scared and untrained individuals are forced into conflict.
- Encourages empathy by putting oneself in others' shoes and recognizing one's own past actions or inactions in similar situations.

### Quotes

- "People care about the pebble in their shoe, not the rock slide 1,000 miles away."
- "I don't want anybody else to be thrown into a cauldron of war and strife for some rich guy's ego."
- "This move, it's a bad move. It's bad all the way around."
- "But it also doesn't mean it's uncommon."
- "Y'all have a good day."

### Oneliner

Beau talks about understanding conflicting perspectives, mobilization against war, and the human tendency to act only when directly impacted, urging empathy and reflection on past behaviors.

### Audience

Global citizens

### On-the-ground actions from transcript

- Put yourself in others' shoes and empathize with their perspectives (implied).
- Recognize past behaviors or inactions and strive to be better (implied).

### Whats missing in summary

The full transcript provides a deeper reflection on human nature, empathy, and the impact of conflicts on ordinary people, urging individuals to learn from past behaviors and strive for better understanding and action.

### Tags

#Empathy #Conflict #HumanNature #GlobalCitizens #Reflection


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the other side,
trying to relate to where they're at,
trying to relate to where they're coming from,
maybe even having a little bit of sympathy
for the position they're in.
Because there are a lot of people who are having,
let's just say, mixed emotions, conflicting emotions,
when it comes to what's happening in Russia right now.
And this really kind of sums it up.
You seemed really upset about the Russians getting drafted.
They could have protested when all of those horrible things
were happening to Ukrainian civilians.
They didn't.
They're not against the war.
They're just against dying in it.
They were fine when they were safe.
But now it's a problem because they're going to be there too.
No lies detected.
Everything in that's true.
Those are all 100% true statements about them
and about us.
It's human nature.
People care about the pebble in their shoe,
not the rock slide 1,000 miles away.
Yeah, there were a bunch of moments
that could have brought forth a strong movement against the war
inside of Russia.
There were a bunch of things that
happened that could have encouraged people to get out
there in the streets.
And it was the mobilization, it was
the risk of them themselves having
to go that acted as that catalyst
to get them out in the streets.
True enough.
It's fact.
But we're not any different.
I mean, we're the United States.
Believe me, there's a giant list of things
that we could justifiably just go on strike for and shut
the entire country down to express our displeasure
with what's happening or what has happened.
But we don't.
We didn't.
I understand what you're saying.
I also understand the drive to not die.
I mean, that seems pretty basic.
Self-preservation is kind of the first law of nature.
So it's not surprising.
And yeah, it does upset me.
I don't want anybody else to be thrown
into a cauldron of war and strife for some rich guy's ego.
These are normal people who are going to be forced to go.
I do sympathize with them.
And I don't think that we're that different.
I think there are many people who would do the exact same
thing here, who maybe have done the exact same thing here.
At the same time, I also want to point something else out.
When you're talking about the waste
that this is going to create, yeah,
the focus is on the amateur army that Russia
is going to be building.
But all of those horrible things that happened,
they are a whole lot more likely to happen again and again.
When you are talking about a group of scared, untrained
people who don't want to be there,
they're much more likely to engage in horrible acts.
This move, it's a bad move.
It's bad all the way around.
It's bad for everybody.
And when you're in a situation and you're
trying to figure out why somebody else is
acting in a certain way, try to put yourself in their shoes.
Because odds are, at some point, you've
been in a situation where you've been in a situation
because odds are, at some point, you've been in a similar
situation where you kind of looked the other way
until it got a little too close to home.
Doesn't make it right that we should all
strive to be better than that.
But it also doesn't mean it's uncommon.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}