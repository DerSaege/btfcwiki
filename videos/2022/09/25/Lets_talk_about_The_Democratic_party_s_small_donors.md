---
title: Let's talk about The Democratic party's small donors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9L6zS1zuAhc) |
| Published | 2022/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the Democratic Party's finances and the impact of small donors.
- Raises the question of supporting Democrats in heavily red areas.
- Mentions the importance of advertising and messaging in such areas.
- Suggests that donations to Democrats in tough areas aren't wasted, as they can influence policies and candidate moderation.
- Criticizes the Democratic Party for lacking a cohesive national strategy.
- Advocates for national ad buys to spread Democratic ideas and policies.
- Emphasizes the long-term impact of planting seeds of Democratic ideas in red areas.
- Advises supporting candidates polling within the margin of error close to election time.
- Points out the dynamic nature of political situations and the need to adapt support accordingly.
- Expresses concerns about the Democratic Party's messaging and strategy.


### Quotes

- "Your donation isn't wasted, assuming it goes to advertising."
- "I think that getting those ideas out and planting those seeds, even in areas that are heavily red, I think it's a good idea."
- "The fight doesn't stop after Election Day."
- "Support the one that's in the margin of error."
- "I think that's the route to a strong, left-leaning Democratic Party."


### Oneliner

Beau tackles Democratic Party finances, advocating for strategic support in heavily red areas and a cohesive national strategy to further progressive policies.


### Audience

Political activists and donors


### On-the-ground actions from transcript

- Support Democratic candidates in heavily red areas by donating to fund advertising (implied).
- Advocate for a cohesive national strategy within the Democratic Party to further progressive policies (implied).
- Get involved in local campaigns to support candidates within the margin of error close to election time (implied).


### Whats missing in summary

Beau's nuanced perspective on supporting Democrats in challenging areas and the importance of a cohesive national strategy within the party.


### Tags

#DemocraticParty #SmallDonors #CampaignStrategy #ProgressivePolicies #PoliticalActivism


## Transcript
Well howdy there internet people. It's Beau again. So today we are going to talk about
the Democratic Party's finances and what small donors can accomplish and what they can't.
And races that aren't really competitive and whether or not it's worth supporting them.
Basically we're going to talk about how to play the game because I got this message.
What do you think about financially supporting Democrats in heavily red areas?
I've seen somebody I respect and somebody you've spoken highly of say it's a bad idea and just a waste of money.
What do you think? Somebody I respect or somebody you respect and I've spoken highly of.
But no name. I don't think there's a yes or no answer to that.
I don't think it's clear cut. I think there's a whole lot of nuance in that.
So, heavily red areas. There are areas like the one I live in.
The chance of a Democratic candidate winning here, it's pretty slim. It really is.
It's not impossible, but it's pretty slim.
However, the advertising that they put out, the policies that they put out, the messaging that they put out,
they can't overhear it, they can't unhear it. So the advertising isn't wasted.
Therefore, your donation isn't wasted, assuming it goes to advertising.
It may not win this race. It may not win the next race. It may not ever win a race.
But they might introduce those policies and make the Republican candidate a little bit more moderate.
That's a possibility. One of the problems with the Democratic Party is that they don't really have a cohesive strategy.
They focus race by race and they don't engage in a lot of messaging that is national,
which is something they should probably do.
If it was me, I would be doing national ad buys and just not to influence the outcome of any specific race,
but to further the ideas that the Democratic Party supports, further their policies,
and get those policies into areas that right now a Democratic candidate doesn't stand a chance in.
So I don't think it's a waste of money. I think that getting those ideas out and planting those seeds,
even in areas that are heavily read, I think it's a good idea.
Focusing on the Republican Party's authoritarian streak, focusing on labor rights,
focusing on how the subtext of a lot of Republican positions right now is that the government should raise your kids,
despite their rhetoric. These are things that would resonate and a long-term campaign aimed at pointing that out
could probably influence elections 8, 12, or 16 years from now.
But the fight doesn't stop after Election Day, right?
So I don't think it's wasted.
Now, all of that being said, as it comes close to election time,
if there is a candidate that is polling within the margin of error, and a candidate that's 15 points down,
support the one that's in the margin of error. Support the one that has a chance of winning today.
It's not a flat answer, because it's a dynamic situation and it changes over time.
In general, no, I don't think it's a waste of money.
At the same time, there are moments when you want those dollars to be as effective as possible, right?
You have to make that decision on your own.
But I have a lot of issues with the way the Democratic Party messages and the lack of a cohesive strategy,
because they do. They play each race individually, rather than trying to build a movement
that can further policy that helps people, even those who didn't vote for them.
I think that's the route to a strong, left-leaning Democratic Party.
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}