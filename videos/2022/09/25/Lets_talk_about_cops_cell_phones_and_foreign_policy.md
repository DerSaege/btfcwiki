---
title: Let's talk about cops, cell phones, and foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rIL18lKQ-8o) |
| Published | 2022/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he separates videos on what's happening from how it could be better in foreign policy.
- Stresses the importance of understanding the cold reality of foreign policy without notions of justice or morality.
- Compares the need for change in foreign policy to the drive for police accountability before cell phones were widespread.
- Draws parallels between the impact of cell phone footage on police accountability and the potential for real-time footage to change perceptions of war and foreign policy.
- Emphasizes that the availability of real-time footage from conflicts like Ukraine is shaping public perception and eliminating glorified views of war.
- Argues that major global events like climate change necessitate a shift towards more cooperative foreign policies.
- Points out that most countries engaging in international affairs exhibit similar behavior to the US.
- Clarifies that he is not always right, acknowledging his fallibility and urging against blind faith in his analysis.

### Quotes

- "Don't bring silly notions like justice or morality into this, because that doesn't have anything to do what we're talking about."
- "We're getting our cell phones. Those people who want a different foreign policy, we're getting that tool."
- "I am not always right. Human, flawed, make mistakes."

### Oneliner

Beau explains why he separates analysis of foreign policy realities from ideal solutions, stressing the need for a wake-up call on the cold truth, devoid of notions of justice or morality, while drawing parallels between the impact of cell phone footage on police accountability and the potential for real-time footage to reshape perceptions of war and foreign policy.

### Audience

Activists, Policy Makers

### On-the-ground actions from transcript

- Watch and share real-time footage from conflicts to raise awareness and shape perceptions (implied).
- Advocate for more cooperative foreign policies in light of global challenges (implied).

### Whats missing in summary

In-depth analysis and examples of the effects of real-time footage on public perception and its potential to drive policy change.

### Tags

#ForeignPolicy #RealTimeFootage #PerceptionChange #CooperativePolicies #GlobalChallenges


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about foreign policy and cell phones
and how a cell phone equivalent is going to change foreign policy
and why I do my foreign policy videos the way I do.
And we're going to do this because I got a message directly asking a question,
and it's a good question.
The first sentence of it, we're going to skip and we're going to come back to.
We'll do that at the end.
But my question is why you separate videos talking about what is happening
from videos talking about how it could be better.
Shouldn't they be done together?
You could say, this is bad.
We could do this instead.
It might make people want a different foreign policy.
I get what you're saying here, the idea of presenting them together.
Right?
The reality is most people don't realize there's a problem yet.
Most people do not understand American foreign policy to the point
where they could realize that that suggestion of how it could be better,
that that's not even on the table.
And if you put them together, it makes it seem like that was an option
that the powers that be considered and for whatever reason rejected.
Not that it just wasn't one that was even considered.
I think it is important for people to understand the cold, hard reality
of foreign policy and how it works.
I think they need to hear.
Don't bring silly notions like justice or morality into this,
because that doesn't have anything to do what we're talking about.
It's about power.
You've heard me say stuff like that over and over again
in almost every one of those videos.
That's about how it is.
They need that wake up call.
I think people need that because most people don't
understand there's a problem yet.
If you were to compare it to another fight to make something better,
compare it to the drive for police accountability,
we don't have cell phones yet.
When everybody started carrying cell phones,
there was this moment where people were like,
the cops have gotten out of control.
No, they've been like that.
They just weren't on tape.
They just weren't filmed.
If you go back 10 years before then and you watch or listen
to stuff produced by black creators,
whether it be music or movies,
they're telling you this is what's happening.
But that evidence isn't there.
They need to hear it and then they'll see it,
because we are getting the foreign policy equivalent
of cell phones right now in Ukraine.
When you think about American foreign policy
and you think about the American view of armed foreign policy,
or it shifted in Vietnam, right?
Because the footage was broadcast into the living rooms.
It was no longer approved stuff given to news outlets by DOD.
It was journalists there broadcasting that footage.
It seemed more real.
It seemed more real because you got to see the 19-year-old kid
from two towns over get loaded onto a Huey.
It made it more tangible.
It wasn't all this idealistic, glory-filled version of war.
Go get them, boys.
That's not what it was like.
It became more real.
Ukraine is the next level of that.
There aren't many engagements that have occurred in that war
that you can't get footage of.
If you wanted, you could watch it.
I don't suggest you do, but you could.
It's shaping our perception of it,
and it's getting rid of that image that it's all glory.
You know, people, they could say, you know,
I don't want to go to war.
If I go, though, at least I'll go down fighting.
Yeah, then they see the footage.
They see that's not really how it works.
A lot of those people, they didn't even know it happened.
One second they were here, the next second they were gone.
They were carrying rations from the truck to the tent,
and that's the second the drone flew over.
There's no glory in that.
And that being available is altering our views,
and it might make people more receptive to the idea
of a foreign policy that is less competitive
and more cooperative, which is where we're going to have to go.
We're going to have to go there anyway
because of the major events that are on the horizon,
climate change, stuff like that.
There's no way to do that in a nationalist framework.
It's going to have to become more cooperative.
We don't have a choice as a species.
And understand, when I say our foreign policy,
I'm talking about humanity, not just the US.
Yeah, the US is bad.
There's ample evidence of that.
But this is true of pretty much any country that engages
in moves on the international stage.
Any major player does the same stuff.
So I think having them divided so it doesn't appear
as though the options for cooperation
were on the table and just discounted,
I think having them divided is more effective.
I think it points out that this is how we view it, period,
full stop.
We don't even consider this other stuff.
So that's why I do it that way.
Now, to the first sentence in this,
you're always right about international and foreign
policy stuff.
No, I am not.
Don't do that.
Don't do that.
No, I'm not.
More accurate than most.
Sure, I'll take that.
But I would point out the reason I'm more accurate than most
is because I don't put as much faith as you
put in me in that sentence in anybody.
Don't do that.
Don't get to the point where you think I'm always right,
because that's the point I'm definitely going to be wrong.
If you ever need a reminder of how wrong I can be,
go back and watch that first video about the pandemic.
That should give you a clear sign.
I am not always right.
Human, flawed, make mistakes.
So just bear that in mind.
But we're getting our cell phones.
Those people who want a different foreign policy,
we're getting that tool.
People are seeing it in real time, real footage, uncensored.
It's probably going to change the perception of war
being glorious and foreign policy being something
that has to be competitive.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}