---
title: Let's talk about a philosophical look at drafts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=y0SrTA4SDb4) |
| Published | 2022/09/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a philosophical question related to foreign policy and conscription in Russia.
- Criticizes the idea of a draft on principle and philosophy.
- Raises concerns about entitled rich people sending poor individuals to war without consent.
- Contemplates the consequences of allowing Russians trying to leave to face the aftermath.
- Questions whether forcing individuals to deal with the consequences may change their perspectives.
- Examines the strategic value versus philosophical implications of letting Russians leave.
- Analyzes how a draft is enforced through penalties to ensure compliance.
- Considers the impact of social and criminal penalties in enforcing a draft.
- Advocates for allowing individuals to leave from a philosophical standpoint.
- Suggests helping those without means leave rather than forcing them to suffer or fight.

### Quotes

- "It's unconscionable for somebody nowhere near the fighting to say, you pick up a gun and go fight for the state."
- "If things get bad enough, won't they reach a point where they'll fight against Putin?"
- "Allow them to leave."
- "Support of the upper middle class in Moscow and St. Petersburg is integral to the war effort."
- "The right move is to help those who want to avoid conscription."

### Oneliner

Beau addresses the philosophical and ethical implications of foreign policy and conscription in Russia, advocating for the right move to help those seeking to avoid conscription.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Assist individuals trying to leave Russia to avoid conscription (exemplified)
- Support initiatives that provide resources for those without means to leave conflict zones (exemplified)

### Whats missing in summary

The full transcript provides a detailed exploration of the ethical considerations surrounding drafts, foreign policy, and individual autonomy.

### Tags

#ForeignPolicy #Conscription #Ethics #Russia #Philosophy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a philosophical question,
one that came in, that also deals with foreign policy
and the way things feel like they should be
and whether or not that's really the way they are if you apply
a philosophical filter to it.
This has to do with people wanting to leave Russia, people trying to get out of conscription
over there.
I know from past videos you don't support a draft on principle and philosophy.
I too think it's unconscionable for somebody nowhere near the fighting to say, you pick
up a gun and go fight for the state. Entitled rich people, not the term here,
sending poor people off without their consent to war for their personal
betterment is horrific. But I can't agree with just letting the Russians leave.
Some of these people had Z decals on their cars when they tried to leave. If
If they're forced to stay and deal with the consequences, they'll have to change their
views.
If things get bad enough, even if they're not people who would fight today, won't they
reach a point where they've suffered enough, then they'll fight against Putin.
I listened to what she said about the strategic value of letting them leave and admit it makes
sense, but philosophically, shouldn't they have to clean up their own mess?
There's a lot in that.
So let's start with this.
How is a draft enforced?
How do they make people go along with a draft?
There's a penalty, right?
And whatever that penalty is, it's
worse than the possibilities of what
could happen to you in a war.
That's why a draft functions, because there is a penalty.
Sometimes it's a social penalty.
Sometimes it's a criminal one.
Sometimes it's both.
But there's a penalty.
So you either do what they say, or you deal
with the consequences of it, right?
Your message here, I agree with a whole lot of it.
it's unconscionable for somebody nowhere near the fighting to say,
you pick up a gun and go fight for the state.
Does it become more consumable if it's you pick up a gun and go fight for what I
think you should fight for?
It doesn't change a whole lot, just the mechanism of whether or not it's somebody's personal
belief or a government entity.
That's all that's changing.
If things get bad enough, even if they're not people who would fight today, won't they
reach a point where they've suffered enough, then they'll fight against Putin.
It's a draft from the other side, isn't it?
From a purely philosophical point of view, I would still say allow them to leave.
And I mean, from a purely philosophical point of view, I think we should be trying to help
those without means, leave and get away from it.
Not put them in a situation where they have to suffer
or fight.
Because that's the same mechanism
that pushes people
to go to war when they don't want to.
Again, this is
This is one of those moments where you're right.
The support of the upper middle class in Moscow and St. Petersburg, yeah, it's integral to
the war effort.
It really is, and they did cause it.
Their support for it, their consent to it.
They allowed Putin to make those moves on some level, I agree, I get it.
At the same time, that's true in any country that engages in something like this.
What do you think would have happened in 2003 in the United States to all of those yellow
ribbons that were on trucks if suddenly they were going to draft people and send
them to Iraq. I bet a lot of them would have gotten pulled off. It's not that
different and the the reality is them leaving is them withdrawing their
support, and it will be something that helps end internal support for the war,
which does help clean up the mess. They may not be doing it for those reasons,
they're probably not. It's probably all very self-serving, but when you're talking
about people's lives and not just theirs, I mean understand, yes, the Ukrainian
military is a battle hardened and Russian conscripts are not going to do
well against them but they're also not going to not fight back. I mean there
there are Ukrainian lives at stake with this as well. I would suggest that both
philosophically, through a foreign policy lens, through every lens except for
vengeance. The right move is to help those who want to avoid conscription
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}