---
title: Let's talk about Hurricane Ian and helping from where you are....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nFs0xPo9lQQ) |
| Published | 2022/09/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the need for aid for those impacted by Hurricane Ian.
- Sharing lessons learned from previous relief efforts after Hurricane Michael.
- Listing items that were hard to obtain but incredibly useful during distribution.
- Chainsaws, generators, CPAP machines, phone batteries, and sleeping bags among the needed items.
- The importance of easy-to-prepare food like cereal, Chef Boyardee, and oatmeal.
- Mentioning the economic hardship faced by people post-disaster due to job loss.
- The usefulness of gift cards to offset living expenses.
- Specific items like over-the-counter meds, masks, gloves, cleaning supplies, and specialized baby formula were hard to find.
- Battery-powered fans are necessary as electricity may take time to be restored.
- Uncertainty about where to send aid as local networks are not set up yet.
- Plans to get involved in relief efforts through the channel and accepting donations for future live streams.
- Encouraging mutual aid networks, church groups, and offices to send needed items.

### Quotes

- "Chainsaws and generators."
- "Food of all kinds."
- "Gift cards are incredibly useful."
- "Battery-powered fans."

### Oneliner

Beau addresses the need for aid following Hurricane Ian, sharing lessons learned and listing hard-to-find but vital items for distribution, while also discussing future relief efforts through the channel.

### Audience

Communities, aid organizations.

### On-the-ground actions from transcript

- Organize a collection drive for chainsaws, generators, CPAP machines, phone batteries, sleeping bags, and other listed items (suggested).
- Prepare and send easy-to-prepare food like cereal, Chef Boyardee, and oatmeal (suggested).
- Gather gift cards to help offset living expenses post-disaster (suggested).
- Collect over-the-counter meds, masks, gloves, cleaning supplies, specialized baby formula, and battery-powered fans for donation (suggested).
- Await updates from local networks on where to send aid (implied).

### Whats missing in summary

The full transcript provides detailed insights into the specific items needed for hurricane relief efforts and guidance on how individuals and groups can contribute effectively.

### Tags

#HurricaneRelief #CommunityAid #DisasterResponse #MutualAid #Donations


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about those
who want to help those who've been impacted by Hurricane Ian.
Getting a whole lot of questions from local organizations
who would like to provide aid.
And the main question is, what do we send?
We have some resources.
How is it going to be most useful down there?
Now, at time of filming, we don't
know how bad the damage is going to be.
We don't know any of the real specifics.
However, we have lessons learned from Michael here.
If you don't know, after Michael,
the people involved with this channel,
we did a lot of relief work.
So we can kind of use that as a guide,
because it's probably going to be really similar.
So what I'm going to go over are items
that were incredibly hard to get our hands on
when we were trying to distribute stuff,
and stuff that was incredibly useful, but difficult
to obtain.
OK, so first, if you have a lot of resources,
chainsaws and generators.
Because basically, if you didn't have one before,
you're not getting one now.
And there are a lot of people who are going to need them.
CPAP machines were incredible, and all
of the different components to it,
those were incredibly hard to get our hands on,
and were really needed by people.
Phone batteries, the things you charge,
and then you plug it into your phone, and it charges your phone,
those were really useful, because we
could charge them in our vehicles
while we were making deliveries, and then distribute them
to key people in different neighborhoods
who were kind of organizing.
And it kept their phones up and running.
Those are relatively cheap, and super effective,
and they were hard to find.
And they're like $6 to $9, but we just
couldn't get our hands on them.
Sleeping bags, cots, and air mattresses.
If a house is damaged to the point
where people shouldn't be there, they
will seek alternate living arrangements.
Sometimes they will be in a living room of a friend's house
or a relative's house.
There may not be enough beds.
So those were really, really useful.
Tarps, doesn't seem like something
that would be hard to find.
But as repair started getting underway,
it became more and more difficult to locate them.
And they're used to cover holes in roofs,
or in windows that are broken, or walls
that a tree had gone through.
And they were hard to find.
Food of all kinds.
But specifically, in the beginning, easy to prepare
food.
Think cereal.
Think Chef Boyardee, ramen, oatmeal.
Stuff that doesn't take a lot to prepare.
Because the National Guard's going to come in and provide
MREs.
They pretty much always do, and they're normally
really fast about it.
However, when you're talking about a large population
of people, there are some children and older folks
who just will not eat it.
So keep that in mind.
But food of any kind, because even
after the storm and the immediate impacts are dealt
with and everybody's safe, there's
going to be a whole bunch of people who don't have jobs,
because their place of employment no longer exists.
Or it's damaged to the point where nobody's working.
So anything you can do, send, that
would offset normal cost of living expenses
once everything kind of starts to get rolling again,
but isn't quite there.
Causes a lot of economic hardship
for those who are just keeping their heads above water
right now.
So gift cards are incredibly useful for that reason.
Over-the-counter meds were really hard to find,
specifically Benadryl or anything
that had to do with allergies.
That was hard to find.
Masks, gloves, and cleaning supplies,
those were difficult in the beginning.
And then we got this giant truck full of pre-made barrels
to distribute.
Specialized baby formula.
The big baby formula companies are normally
pretty good about getting stuff here,
getting stuff to the impact zones pretty quickly.
However, if you see the stuff that
it's for babies that have allergies,
or the purple can of infant milk that is for babies that I guess
they get more gas than normal, that was highly sought after
and difficult to find.
And then battery-powered fans, because it's
going to be a while for the electric to get back up.
And then on top of that, the AC units
tend to sit outside, trees.
A lot of people are going to be in a lot of heat.
So those were the items that were
most difficult to find in the immediate area that are
probably pretty common elsewhere.
Now, as far as where to send this stuff, right now,
I don't know.
The local networks haven't set up yet.
So they're still waiting for the storm to go through.
Now, also had a lot of questions about this channel
and what we're going to do.
Odds are we will get involved in some way.
I mean, that's pretty much a guarantee.
And we've had people ask about how to send money now,
because we're probably not going to have time to do a live stream.
This channel keeps cash back to deal with disasters like this.
So we will do a live stream after the fact this time.
Normally, we try to do it beforehand
so we have an idea of a budget.
But this time, we'll just go with it
and see where we land afterward.
So those are the items.
If you have a mutual aid network,
if you have a church group, if your office
wants to put stuff together and send it down,
these are the items that we can safely
assume are going to be in need and hard to find here.
So I hope that helps.
And I am loving the fact that I literally have
50 messages asking this question.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}