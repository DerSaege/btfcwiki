---
title: Let's talk about the Senate's Electoral Count Act bill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zqsgVUh1q_0) |
| Published | 2022/09/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Electoral Count Act update in the Senate to clarify the vice president's role and prevent misinterpretation.
- Notes Ted Cruz as the sole Republican on the committee to vote against the Act, while Mitch McConnell endorsed it to prevent chaos.
- Mentions the Act having 11 Republican co-sponsors, potentially overcoming filibuster challenges.
- Anticipates resolution after the election due to timing constraints, likely by the lame duck Senate.
- Encourages people to know where their senator stands on the issue and suggests voting based on their position.

### Quotes

- "Because of timing, this probably won't be resolved until after the election."
- "If they don't actually support you having a voice, they probably shouldn't be your representative."
- "This is a fundamental piece of American democracy, so maybe it should be a deciding factor when it comes to your vote."

### Oneliner

Beau explains the Electoral Count Act update in the Senate, notes key players' stances, and urges people to vote based on their senator's position post-election.

### Audience

American voters

### On-the-ground actions from transcript

- Know where your senator stands on the Electoral Count Act (suggested).
- Vote based on your senator's position on the Act (suggested).

### Whats missing in summary

Importance of informed voting for American democracy.

### Tags

#ElectoralCountAct #Senate #TedCruz #MitchMcConnell #AmericanDemocracy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk a little bit
about the Electoral Count Act in the Senate.
We talked about it recently in the House,
and now we're going to talk about
where it stands in the Senate.
This Act is designed to, or the update to the Act,
is designed to clarify the language,
to prevent a willful misreading of its intent,
and just say, hey, the vice president is actually there
to kind of act in a ceremonial role.
He can't actually just throw away the electors' votes
and declare whoever he wants president.
It increases the threshold to object
to certifying the electors, stuff like that.
It's minor stuff designed to prevent this perceived loophole
from being used to stage a coup.
Ted Cruz was the only Republican, the only person
on the committee to vote against it.
Mitch McConnell has endorsed it, saying
that it was important to make sure
that we didn't have the same kind of chaos,
his word, that occurred on the 6th.
So that bodes very well for its chances of really making it
through the Senate. That coming from McConnell is probably also a low-key shot at Trump.
The bill has, I want to say, 11 Republican co-sponsors, so it is already over the threshold
to deal with the filibuster and all of that stuff. The only real grumblings are coming
from the people you would expect, you know, the members of the Sedition Caucus, for lack
of a better term, that crew that was active in this misreading.
But even some of them voted in favor of it on the committee.
Now for the bad news.
Because of timing, this probably won't be resolved until after the election, after this
election.
It will probably be dealt with by the lame duck Senate.
There's just not enough time.
Odds are they're not going to come back in October, then the election, and you go from
there.
So that's where it's at.
It would be worth noting where your particular senator stands on this issue.
I would suggest that anybody who cares so little about the votes of the people of their
state that they wouldn't support this probably doesn't deserve your vote.
I mean that just kind of stands to reason, right?
If they don't actually support you having a voice, they probably shouldn't be your
representative because they're obviously not going to represent you.
If they would like this perceived loophole to remain, they have to have a reason.
And I'm not buying the claims about it securing election integrity because that is obviously
not how it was used.
So if you plan on voting and there is a senatorial race that you'll be voting in, I would just
check to see where they stand on it.
This is a fundamental piece of American democracy, so maybe it should be a deciding factor when
it comes to your vote.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}