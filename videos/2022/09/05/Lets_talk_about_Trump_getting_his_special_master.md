---
title: Let's talk about Trump getting his special master....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Jl2OaxDJd2g) |
| Published | 2022/09/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's request for a special master has been granted, where a third party will sort out privileged material, likely to be appealed by the Department of Justice.
- The impact of this decision won't be substantial on the case, just a delay tactic.
- Beau questions the argument of executive privilege when some of the documents being sought are presidential records.
- The Presidential Records Act lacks enforcement mechanisms, defining presidential records as property of the U.S. government.
- Beau mentions the consequences for concealing or destroying records filed with the court or a public office.
- He hopes no presidential records belonging to the people are found by the special master.
- The delay tactic doesn't seem wise to Beau, who acknowledges he's not a lawyer.
- The intelligence and counterintelligence review is ongoing and unaffected by this development.
- Most intelligence agencies lack the mandate for domestic investigations, a task usually handled by the FBI.
- Beau believes the delay caused by this decision may not go beyond the midterms, which could be Trump's goal.
- Trump's desire for a special master might stem from wanting to appear influential like his friends who had similar setups in their cases.
- The Department of Justice likely has sufficient evidence for indictment, potentially leading to pretrial confinement, possibly home confinement for Trump as a former president.
- Trump's delaying tactics could elongate any potential pretrial confinement period.
- Beau anticipates appeals back and forth, with the Trump team aiming for extensive delays.

### Quotes

- "I mean, you can make of that what you will."
- "This is a delay tactic."
- "It's just a thought."
- "It's going to be made out to be a big thing because it involves Trump and his legal case, but all it does is delay it."
- "To me, this doesn't really make any sense."

### Oneliner

Trump's request for a special master leads to a delay tactic with minimal impact on the case, raising questions about executive privilege and potential consequences for concealing records.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed on legal developments and implications (implied)
- Support transparency and accountability in legal proceedings (implied)

### Whats missing in summary

Insight into the potential political motivations behind the delay tactics and the broader context of legal strategies in high-profile cases.

### Tags

#LegalAnalysis #Trump #SpecialMaster #DelayTactic #PresidentialRecords #Concealment


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Trump's request for a special master and what that
means now that it's been granted.
So a judge down there has decided that a third party will review the documents and sort out
any privileged material.
The Department of Justice is likely to appeal this, but at the end of it, it doesn't really
matter.
This isn't going to have a substantial impact on the case.
It's just a delay.
Now it's a unique position to take.
If you go back and look at the previous video where I was talking about the special master,
I tried to go out of my way to not use the term executive privilege because that doesn't
make a whole lot of sense to me to argue that some of the documents would be covered by
executive privilege when a portion of what they're looking for is presidential records.
Because that's kind of, I mean, it almost seems like an admission that there are presidential
records in there.
Now I think one of the reasons that that aspect of it was kind of overlooked is the fact that
the Presidential Records Act really doesn't have a mechanism for enforcement.
This is what you're supposed to do, but there's no huge penalty if you don't do it.
However, it does kind of say that presidential records are property of the U.S. government.
Okay, so concealment removal.
Whoever willfully and unlawfully conceals, removes, mutilates, obliterates, or destroys
or attempts to do so or with intent to do so, takes and carries away any record, proceeding
map, book, paper, document, or other thing filed or deposited with any clerk or officer
of any court of the United States or in any public office or with any judicial or public
officer of the United States shall be fined under this title or imprisoned not more than
three years or both.
I mean, I would hope that the special master doesn't find any presidential records that
belong to the people.
This is a delay tactic.
I don't think it's going to matter a whole lot in the grand scheme of things.
I also don't think it was incredibly wise, but again, I'm not a lawyer.
The intelligence and counterintelligence review is continuing anyway.
Doesn't matter.
There's not much the judge could do about that anyway.
It's worth noting that the intelligence community doesn't, most intelligence agencies don't
have a mandate to conduct investigations domestically.
That task tends to fall to the FBI.
This isn't a huge thing.
It's going to be made out to be a big thing because it involves Trump and his legal case,
but all it does is delay it.
I doubt it would delay it beyond the midterms, which is probably Trump's goal.
To me, this doesn't really make any sense.
I think even beyond a delay tactic, the most likely reason Trump wanted this was because
he had friends that had special masters in their cases and he wanted to show that he's
a big shot too.
At the end of this, the Department of Justice, it seems to me that they already have the
evidence necessary to indict on a couple of charges.
If they were to do so, I have a feeling that all of Trump's groundwork, all the groundwork
that Trump's team has put together to delay is going to come back to bite him because
in a case involving defense information and the possibility that it might be transmitted
to other people, it's kind of standard for the Department of Justice to request some
kind of pretrial confinement.
Now given the fact that he's a former president, it would probably be home confinement, but
the delays that he's creating now would extend that period.
But we don't know.
We'll have to wait and see how all of this plays out.
My guess is that this gets appealed, but then again, the Trump team might appeal it afterward
because they do seem intent on creating as long a delay as humanly possible, which I
mean you can make of that what you will.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}