---
title: Let's talk about Labor Day's origins....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yxt6oOZyBgI) |
| Published | 2022/09/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Labor Day origins in September 1882, national holiday in 1894 by Grover Cleveland.
- Connected to leftist worker-oriented thought during the Second Industrial Revolution.
- Labor Day in September to avoid association with May Day and leftist ideologies.
- Debate on who created Labor Day - Peter J. McGuire or Matthew McGuire.
- Imagery associated with Labor Day: industrial jobs, blue-collar workers, and Second Industrial Revolution.
- Questioning the purpose of Labor Day: honoring labor workers or the labor movement?
- Honoring the labor movement includes figures like Blair Mountain, Eugene Debs, Cesar Chavez, and more.
- Labor movement achievements include the weekend, end to child labor, 40-hour work week, and more.
- Labor Day is for every worker in the United States, not just industrial workers.
- Importance of celebrating the worker of today and being part of the ongoing labor movement.

### Quotes

- "Labor Day is for you. It's for every worker in the United States."
- "The labor movement is still alive and well."
- "It's not history. It's not a moment. It's a movement."

### Oneliner

Labor Day origins, labor movement celebration, and ongoing worker empowerment emphasized by Beau.

### Audience

American workers

### On-the-ground actions from transcript

- Join a union for collective bargaining (implied)
- Advocate for better working conditions and standard of living (implied)
- Educate others on the importance of the labor movement (implied)

### Whats missing in summary

Importance of continued advocacy for workers' rights and collective empowerment.

### Tags

#LaborDay #LaborMovement #WorkerEmpowerment #CollectiveBargaining #AmericanWorkers


## Transcript
Well, howdy there, internet people.
It's Beau again.
So I hope you're having a good Labor Day.
Because of that, we're going to talk about Labor Day
because some questions have come in over on the other channel
about it, and it's worth diving into.
One of the big questions is, who are we
supposed to be celebrating?
And we'll get to that.
OK, so the origins.
The first Labor Day was September 5, 1882, in New York.
I know some people are going to argue that.
That's from the Department of Labor website.
Take it up with them.
It became a national holiday in 1894 when Grover Cleveland
declared it.
That's when it started.
Now, this happened at a time when you're looking at around
the era of the Second Industrial Revolution.
There's a lot of leftist, worker-oriented thought
occurring and becoming popular, which also answers
another question.
Why do we have Labor Day in September,
and why the US really doesn't do a whole lot for May Day?
And the answer to that, for a lot of people,
is Grover Cleveland.
He understood what a lot of leftist thought entailed,
and he understood the connection to May 1 and May Day.
So he thought it would be better to go with the September
Day as the national holiday, because he believed
if they made May Day a national holiday with the labor roots,
that it would basically just constantly turn into a thing
of celebrating leftist thought, which
wasn't something he wanted.
Now, as far as who really created it,
there is a debate there, too.
There are some people who say it is Peter J. McGuire,
and some people who say it is Matthew McGuire.
You can argue that as well.
Now, the fact that it occurred around the period
of the Second Industrial Revolution,
and that's what kind of led into that,
that leads to a lot of the imagery
that we have that's associated with Labor Day.
That industrial job, the soot-covered workers,
and the blue-collar stuff.
That's where that comes from, because of the time period
in which it originated.
And that imagery has stayed true throughout it.
And that leads to that question.
What's a Labor Day for?
Is it for labor, the worker?
Or is it for the labor movement?
And if it's for the labor movement,
who are we supposed to honor?
And if you're looking for a way to honor the labor movement,
yeah, you could look to Blair Mountain.
You could look to Eugene Debs, Addie Wyatt,
Cesar Chavez, Mother Jones, or you.
And there are a lot of people who, when you say that,
they hear it and they're like, well, I don't really know.
You know, I'm not part of a union.
Yeah, unions are the backbone of the American labor movement.
But when they achieve something, does it stay there?
The labor movement in the United States gave us the weekend,
an end to child labor, the 40-hour work week,
unemployment, workers' comp insurance,
the eight-hour workday, protective equipment
at your job, the minimum wage.
All of these things, sure, they originated in the labor
movement, but they benefit all workers, right?
I also don't like the idea of trying
to honor the labor movement of the past
without celebrating the worker of today,
because then it turns it into a moment in history
rather than a movement that is very much alive today.
Labor Day is for you.
It's for every worker in the United States.
In the labor movement, it's alive,
and you can be a part of it, and you don't
have to be an industrial worker.
There's that image because of the time
when all of this came about.
And that's what gets associated with unions.
That's what gets associated with labor in general.
However, today, there's teachers' unions.
There's nurses' unions.
And yes, there are unions for baristas
because it's for all workers.
The idea of the labor movement is for the worker
to band together so they can bargain as one
and have the same power, or at least comparable power,
to the people signing the checks.
It's that old saying, divided we beg, united we bargain.
That's what's being celebrated.
The labor movement as a whole, which includes, yeah,
all of the leaders of old, all of those previous wins,
but also the worker of today and the movement of today.
The labor movement is still alive and well.
If you're somebody who's watched this channel
or the other one, you know about the Warrior Met strike.
That's going on still right now as we speak.
That's happening.
It's not history.
It's not a moment.
It's a movement.
It's something that will continue to grow
as long as you're there, as long as you,
the American worker, continue to push for better conditions,
for a better standard of living, and you
do it in a united fashion.
That's American labor.
And it's one of the few things that kind of levels
the playing field for you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}