---
title: Let's talk about numbers to give the GOP chills....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d-9zfxxWdLk) |
| Published | 2022/09/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of document-based journalism in providing context and revealing information.
- Mentions a Politico article containing interesting numbers that should concern the Republican Party.
- Compares the number of online donors for the Republican and Democratic parties from the last half of 2022 to the first half of 2022.
- Republican Party had a loss of 43,000 online donors from 2021 to 2022.
- Democratic Party saw a substantial increase in online donors during the same period.
- Democratic Party's enthusiasm supposedly increased significantly after a specific Supreme Court decision in June.
- The surge in Democratic online donors might include unlikely voters, potentially affecting polling accuracy.
- Indicates the need for political strategists to pay attention to these trends.

### Quotes
- "These additional 600,000 online donors for the Democratic Party, those very well may be those unlikely voters that we've talked about as far as things that might cause polling to be a little off."
- "That Supreme court decision, that's what people see as a motivating factor."
  
### Oneliner
Beau breaks down online donor numbers revealing Democratic Party's surge and Republican Party's loss; a Supreme Court decision seems to be a significant motivator.

### Audience
Political analysts, strategists

### On-the-ground actions from transcript
- Analyze political trends and donor data for strategic insights (implied)

### Whats missing in summary
Analysis of the potential impact of online donor numbers on political strategies and future elections.

### Tags
#Document-based Journalism #Online Donors #Republican Party #Democratic Party #Political Trends #Supreme Court Decision


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about some good,
old-fashioned paperwork journalism.
Something I talk about on the channel a lot
are outlets that go through documents
and put the numbers together to provide context,
because it can be very, very revealing.
Normally, that comes from just one of a few outlets.
This is from Politico,
and I will put the article down below
because I'm just gonna go over a tiny fraction
of the numbers and there's a lot
of other interesting stuff in it.
What I'm gonna go over should give
the Republican Party chills.
The set of numbers I found the most interesting
was comparing the number of online donors
for each party from the last half of 2021
the first half of 2022. In the last half of 2021, the Republican Party had 956,000 online donors,
first half of 2022, 913,000, a loss of 43,000 online donors. Heading into a midterm. That's
That's unnerving.
What about the Democratic Party?
And that was through WinRed, by the way, the GOP's online fundraising platform.
The Democratic Party, which uses ActBlue, the last half of 2021, 1.9 million online
donors, the first half of 2022, 2.5 million online donors.
That's a substantial increase.
But wait, there's more.
People have been talking about how the Democratic Party has become more enthusiastic, become
more driven because certain things have caused them to be more engaged.
first half of 2022. January, February, March, April, May, June. When did the enthusiasm
supposedly start? June 24th. That's when that ruling came down. These numbers don't
even include any of that. That should give the Republican Party chills. That Supreme
court decision, that's what people see as a motivating factor.
But what this shows is that it was happening before then.
That was just the thing that pushed it over the top.
These additional 600,000 online donors for the Democratic Party, those very well may
be those unlikely voters that we've talked about as far as things that might cause polling
to be a little off.
So this is something that I'm sure political strategists are going to start paying attention
to.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}