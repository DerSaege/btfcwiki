---
title: Let's talk about the Little Mermaid and relatability....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ubE-arwpjk0) |
| Published | 2022/09/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The new version of The Little Mermaid features a Black mermaid, causing a stir.
- The original Little Mermaid story differs drastically from the Disney version.
- Beau grew up with the Disney version and feels attached to the red-haired Ariel.
- Changing Ariel's race makes her more relatable to a wider audience.
- Beau questions the purpose of changing Ariel's race in the movie, considering it's integral to the plot.
- The message of the story could be interpreted as "race doesn't matter."
- Beau encourages viewers to see beyond skin tone and embrace the message of love conquering all.
- The film introduces The Little Mermaid's story to a new audience, offering them the same lessons and inspiration.
- Love conquering all, including race, is a central theme of the movie.
- Beau suggests that the focus should be on the broader message rather than skin tone.

### Quotes

- "Changing Ariel's race makes her more relatable to a wider audience."
- "The message of the story could be interpreted as 'race doesn't matter.'"
- "Love conquering all, including race, is a central theme of the movie."

### Oneliner

The new Black Ariel in The Little Mermaid brings relatability and love conquering all themes to a wider audience.

### Audience

Film enthusiasts, Disney fans

### On-the-ground actions from transcript

- Watch and support movies that showcase diverse representation (exemplified)
- Engage in constructive dialogues about diversity and representation in media (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of how representation in media impacts relatability and the interpretation of themes like love and race.

### Tags

#TheLittleMermaid #Diversity #Representation #LoveConquersAll #FilmRepresentation


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
the Little Mermaid again. If you don't know, the new version of the Little Mermaid, the
Mermaid Will Be Black. And a trailer for the movie recently came out and it has caused
a stir and it prompted a message. I kind of want to go through it. The first video I ever
watched from you was the one about the Little Mermaid. I watched a few others since then,
but I saw you tweet about the Little Mermaid and thought I'd ask a question. Your first
video talked about the original story of the Little Mermaid and how that's so different
from the Disney version that people can't be upset about them changing Ariel from white
to black. Now if you don't know, pause this real quick, if you don't know, the original
story of the Little Mermaid is messed up. Ariel does not get a happy ending. It is drastically
different from the Disney version. But I didn't grow up with the old story. I grew up with
the Disney version, and it didn't really matter. Ariel is addicted to this character.
She was born with a mask on and a superly dark personality. To those interesting enough
to read the storytelling that the Little Mermaid was talking about, Ariel was an architect
who had a happened to be a friend of Ariel's. But by the time she animated the film, the
animator had changed things directly for Ariel in her play. Because of the deception she
like me is red-headed, now that's not going to be the iconic thing about Ariel.
Maybe I'm being selfish because I'm attached to the red-haired Ariel, but why
change her race?
So the relatability thing, to me that's always been weird. I don't know why a
character has to look like you to be relatable, but you're not an anomaly
there. There are a lot of people who feel that way. And because of that
relatability, you took away that curiosity, that desire to be brave, and
you know, being more open. All of that stuff, right? And it's a good thing. It's
a good lesson to pull from The Little Mermaid. And now that story is relatable
to even more people. Those lessons, those things that you found so formative that
you named your daughter after the character, now other people can find that
relatable in that way. I would also point out as far as your daughter's concerned,
it's not like the cartoon version, the old version is like going to get deleted.
It's still going to be around. That version of Ariel will still exist. It's
not being erased. But there's another thing here. I just don't see the
purpose in changing her race in the movie. Now I know what you really mean
here. I know what you're getting at when you say this, but I'm going to answer
that literally. I don't see the purpose in changing her race in the movie,
because it's the entire plot. Without that, her sense of wonder, her sense of
curiosity, it all goes unfulfilled. She doesn't give up her world for love. Why
do it with this particular movie, with this particular character? It's the
perfect character to do it with. It's in the story. She gives up her race, mer
people, whatever it is, to turn human. I mean, if you're going to try to
interpret it and pull out a larger moral from it, it would kind of be that race
doesn't matter. I mean, that seems like one that would show up in it. I
have found it very funny that a lot of people who have found this film so
formative seem to skip right over that part. It's a major piece of the
show, of the movie. It's part of the story that you can't get away of.
It's even in the original, when you go way back. I think that people are
getting wound up unnecessarily. This takes this show, this film, that you love,
and it introduces it and makes it relatable in the same way that it was
relatable to you, to a whole new group of people, a massive new audience. And they
can get those same lessons and that same inspiration that you got. And hopefully
you can apply the more, to me, the more overt message, but maybe the more subtle
message to you that race is not really the biggest part when it comes to love.
If you loved this film, you loved this character as a white redhead, there's no
reason you couldn't love her as a black mermaid. That seems to be like
integral to the plot, is that love conquers all, including race, to the
extent of literally different, I guess, different species. You're talking about
mermaids and humans in the film, and what people are complaining about is skin
tone. It seems like it should be easier to get over the skin tone. Just saying.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}