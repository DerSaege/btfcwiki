---
title: "Let's talk about Karla Hern\xE1ndez-Mats, Crist's running mate...."
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oOprnHGbrjo) |
| Published | 2022/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of the Florida governor's race dynamics, comparing DeSantis and Christ as wild authoritarian versus centrist.
- Introduction of Chris' running mate, Carla Hernandez, president of United Teachers of Dade, signaling a focus on family planning, education, and labor.
- Hernandez's background as a teacher and union leader with immigrant parents from Honduras.
- Speculation on the potential line of attack against Hernandez by the Republican Party.
- Mention of a tweet involving Castro that the Republican Party tried to use to paint Hernandez as a communist.
- Reference to Hernandez's humanitarian work overseas with limited details available.
- The possibility of Hernandez swinging votes in Florida, traditionally viewed as a red state.

### Quotes

- "The successful line of attack against DeSantis as being family planning, education, and labor."
- "She's a teacher. She's very energetic."
- "She has a strong background if that's what they're going to run on."
- "It's an interesting choice."
- "Y'all have a good day."

### Oneliner

In the Florida governor's race, Chris' running mate, Carla Hernandez, a teacher and union leader, may shift the focus to family planning, education, and labor, potentially swinging votes in the traditionally viewed red state.

### Audience

Florida voters

### On-the-ground actions from transcript

- Contact education organizations and unions to support Carla Hernandez (suggested)
- Stay informed about the Florida governor's race and the candidates' platforms (implied)

### Whats missing in summary

Detailed analysis of DeSantis' and Christ's positions and policies.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk a little bit more
about the Florida governor's race
because Chris has picked his running mate.
Now, when we talked about this last time,
described it as a replay of the 2020 election
for president in the United States.
You have the wild authoritarian DeSantis
And you have the centrist, kind of boring, Christ,
guy for the Democratic Party.
Christ's running mate was an interesting choice,
Carla Hernandez.
Now, we don't have a lot on her political positions,
as far as stated, because she's never
held public office before.
But that doesn't mean that she's a stranger to Florida politics,
because she's president of United Teachers of Dade.
That's the teacher's union, Miami.
That's huge.
And that is a political powerhouse in Florida.
Prior to that, she taught middle school in Hialeah,
special needs.
And that's her background.
She's a teacher.
And then she became a union leader for teachers.
I want to say her degree came from Florida International.
And she's the child of immigrants from Honduras.
So what does this tell us?
tells us that the Democratic Party crest sees the successful line of attack
against DeSantis as being family planning, education, and labor. That's what it
signals. Yeah, I mean that that that could work. We don't know that it will, but that
could work. The addition of Hernandez is, it's interesting because she doesn't
have a political background. So it's kind of a clean slate there. She's a very
energetic person. She's a teacher. Now as far as baggage goes, doesn't really seem
to be any that the Republican Party could use. There's one thing they might go after but it
wouldn't make sense because it would likely backfire because where they could say, oh she
knew this person who did this bad thing, there are members of the Republican Party in Florida accused
of doing it themselves. So that doesn't seem like a solid line of attack. The only baggage that they
seem to have been able to come up with, as far as opposition research on her, is a tweet.
The tweet says, a political figure dies at 90, most in Miami rejoice, many in Cuba mourn.
It's obviously about Castro.
The Republican Party has already tried to use this tweet to paint her as a communist.
That seems a stretch, that seems a stretch for Florida, I would imagine that given where
she talked and where she lived, there's going to be plenty of people who would attest to
her political affiliations and her views towards Castro and what they are, one way or another.
She said that she was actually out with her Cuban neighbors, you know, like banging pots
and celebrating in the street.
Don't know.
There's no way to confirm that at this point, and it's also worth noting that this is a
statement made by somebody whose heritage is from Honduras.
It might work because of Castro's reputation in Miami.
That might be a good play for the Republican party in general, however, she's from down
there.
That's probably not going to work as well as they think.
Other than that, the only other interesting thing I could dig up said that she did humanitarian
work overseas.
However, that's all I could find.
I couldn't find out where or what it was.
It was just a little blurb at the end of one of the bios
on a union page.
In addition to the teachers union there,
she is linked up with a number of other education
organizations and unions.
So she has a strong background if that's
what they're going to run on.
And if she can maintain the energy that she had when the announcement was made, she can
probably swing some votes.
And while Florida is traditionally viewed as a red state, it's really kind of not when
you look at the registrations.
So we're going to have to wait and see how it plays out, but it's an interesting choice.
And I would imagine that it probably kind of caught DeSantis off guard.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}