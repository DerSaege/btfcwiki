---
title: Let's talk about preparing for the next 2 months....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SAmOn63QjoE) |
| Published | 2022/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The speaker addresses the overwhelming feeling that comes from following the news, especially with the fast-paced nature of current events.
- Beau gives tips on how to manage the anxiety that stems from constant news updates by preparing for what is likely to happen.
- Providing examples of upcoming events like political scandals, Trump's reactions, hearings, and climate news, Beau encourages viewers to anticipate these plot points to reduce surprise and stress.
- By combining current news consumption with historical context, Beau suggests that individuals can gain a better understanding of what to expect and reduce the stress of unpredictable events.
- Beau ends by reminding the audience that having a historical perspective can act as a fuzzy crystal ball, making staying informed a less stressful experience.

### Quotes

- "History doesn't repeat but it rhymes."
- "Establish a frame of what you expect to happen."
- "Combine consuming news with reading history."
- "You really do end up with a very fuzzy crystal ball."
- "That little bit of foresight makes staying informed a little less stressful."

### Oneliner

Beau provides strategies to manage news-induced anxiety by anticipating upcoming events and combining current news consumption with historical context to gain a clearer perspective and reduce stress levels.

### Audience

News consumers

### On-the-ground actions from transcript

- Prepare for upcoming events by staying informed about historical context (implied).
- Anticipate potential plot points in current events to reduce surprise and stress (implied).
- Combine news consumption with reading history for a clearer perspective (implied).

### Whats missing in summary

The full transcript provides a detailed guide on how to manage anxiety related to consuming news by preparing for upcoming events, utilizing historical context, and viewing current events as predictable plot points, ultimately reducing stress levels.

### Tags

#NewsConsumption #AnxietyManagement #HistoricalContext #PredictableEvents #StressReduction


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about how to consume the news
without letting it consume you, because we're headed into
what is likely to be a really rough and active couple of
months as far as the news is concerned.
And somebody sent me a message, and I definitely
think their last line here probably holds a lot of
truth. Bo, I'm 19. I started following the news during COVID, because what else did I have to do?
I'm better informed and like the feeling of being able to understand what's happening and know it's
important to stay informed. But it's filling me with a constant state of dread, waiting for the
next new development. The news just moves so fast. Is there any tips you have for managing the
anxiety that comes from following the news. I don't think I'm the only one who
feels this way." The news is moving so fast. Yeah, tell me about it. I'm recording
this on August 29th. Let's see when it can actually be published. Okay, so tips
on how to avoid the dread. Why does the dread come? Because you don't know what's
gonna happen, right? Because it's just constantly something new popping up in
your face. Ever watch a scary movie? The first time it was scary, right? Was it
scary the second time, because you knew what was coming.
One of the best parts about being informed is that
crystal ball, the ability to use context clues to kind of
guess at what's coming.
And if you ready yourself for it, you're no longer
surprised when it happens. Now over the next two months it's going to be pretty
wild, but there are things we know are going to happen, right? We have the
campaigns heading into the midterms. They're going to fire up and everything
that goes along with it. So there's going to be some scandals, you know, probably
some information come out about political figures that you may like or
dislike and that's going to create a news story. You have the revelations
that are going to come out of all of the various investigations dealing with
Trump. Those have the potential to spark other stories. You have Trump himself and
and how he reacts to all of the news that is coming out and what he decides to do, how
he decides to steer the MAGA faithful.
You have politicians who are going to make statements trying to catch air time and trying
to get a pat on the head from dear leader.
statements, they may cause unrest. They may cause acts of violence. So you know
that that's a plot point that might happen. So be aware of that. Be ready for
that. You have the hearings coming up and any revelations from there, combined
with an already charged environment from the campaign and from
everything with Trump, you have Ukraine and tides appearing as though they may
start to turn. There's no guarantee in that, but that seems like what's
happening right now. If Russia starts getting pushed back, there's probably
going to be some wild rhetoric. But remember, both sides have to make a
mistake at the same time for things to go bad. So when that happens, just, you
know, keep that part in mind. There's going to be more climate news. There will
probably be some bad storms that are going to hit, well probably me, this area,
the southeastern United States. And then in order to try to energize their base,
you're going to have the Republican Party continue to other people,
particularly those of different identities or orientations because
that's the scapegoat that's who they're they're training their base to kick down
at. Over the next 60 days these stories are going to happen. It's going to occur.
So if you can establish a frame of what you expect to happen and it always helps
me to kind of frame it in the worst case. You're in a better position and you have
a clear view of what's going to occur so it's easier to manage the day-to-day
events because you see it as a build-up to a plot point rather than something
that is unexpected, that is catching you by surprise. History doesn't repeat but
it rhymes. If you combine consuming news with reading history, you really do end
up with a very fuzzy crystal ball. And that little bit of foresight makes
staying informed a little less stressful. Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}