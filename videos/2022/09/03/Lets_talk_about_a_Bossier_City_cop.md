---
title: Let's talk about a Bossier City cop....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rCSj-oqU9q8) |
| Published | 2022/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Story from Bozier City, Louisiana, involving a cop who is also the president of the police union, taken into custody by the feds.
- Initial reports suggested the investigation was related to the union and fundraising, but the affidavit reveals it's about controlled substances, specifically painkillers.
- Sergeant Harold B.J. Sanford has not been indicted yet, charged via complaint based on allegations in the affidavit.
- Transcripts of text messages in the affidavit indicate someone was obtaining and selling pills to the cop, potentially tied to using union funds to support an alleged habit rather than widespread corruption.
- No evidence in the current information points to a larger corruption scheme within the union; it seems more like a typical addiction scenario involving a cop in a position of authority.
- Beau hints at the possibility of undisclosed angles in the case, considering the lack of transparency in legal documents.
- Despite ongoing monitoring of the situation, it doesn't seem likely that there will be a significant investigation into the union or police department.
- Beau concludes with the observation that people are most interested in whether there will be a deep dive into the union itself or the police department.

### Quotes

- "It looks just like a very typical story when it comes to this type of thing."
- "And I think that's really what people want to know."

### Oneliner

A cop's arrest involving controlled substances sparks interest, but initial allegations hint at personal addiction rather than widespread corruption within the union.

### Audience

Community members, curious individuals

### On-the-ground actions from transcript

- Stay informed on the developments of the case and any potential investigations (suggested)
- Support efforts for transparency and accountability within law enforcement (implied)

### Whats missing in summary

Insights on the implications of addressing addiction and personal behavior within positions of authority.

### Tags

#Louisiana #PoliceUnion #Addiction #Transparency #LawEnforcement


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about a story out
of Bozier City, Louisiana, because a whole lot of people
expressed interest in it and asked me to kind of dig into it.
It involves a cop that was taken into custody by the feds,
I want to say last Sunday.
Now, I think the reason it sparked so much curiosity
is because this cop is also the president of the police union.
And initially, there were some reports
suggesting that it had to do with something
to do with the union and perhaps fundraising.
However, when I looked at the affidavit,
that's not what it's about at all, really.
It looks like it has to do with controlled substances,
painkillers. And at this point, Sergeant Harold B.J. Sanford, as far as I can tell,
hasn't even been indicted. So looks like charged via complaint and that's where
it's at. So all of this is allegations at this point. However, when you look in the
affidavit there are some transcripts of text messages and yeah that's I mean it
looks as though he had somebody going out and getting pills for him and then
selling it to him. You know, going to get in the prescription. If there is a tie to
the police union I think it might have something to do more with him using
funds to support his habit, alleged habit, more than any widespread corruption. I
couldn't find anything that indicated that it was part of some bigger scheme
at this point. It looked more like, I mean honestly, it just looks like a pretty
typical addiction story, just this time involving a cop who also happened to be
the president of the police union. I'm not sure if there's perhaps another angle
that isn't disclosed in the affidavit. As everybody's learning from the the Trump
case, it's the documents are sometimes less than forthcoming about their
intentions for obvious reasons. If there is a widespread corruption issue within
the union, the FBI is not going to want to tip their hand. But based on what's
there, that there isn't a huge tie, there's no indication that they're
looking for some massive amount of corruption or anything like that. It
It looks just like a very typical story when it comes to this type of thing.
I will continue following it and see what happens as the case progresses, but at this
time, it doesn't look like there's going to be a massive investigation of the union
itself or of the police department.
And I think that's really what people want to know.
So anyway, it's just a thought.
I have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}