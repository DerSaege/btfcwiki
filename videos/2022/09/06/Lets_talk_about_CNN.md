---
title: Let's talk about CNN....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ctR1QJsCqFM) |
| Published | 2022/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- CNN is undergoing changes in terms of personalities and content, but the exact end goal is unknown.
- There are speculations that CNN might be trying to correct a perception of bias or appeal to moderates and conservatives.
- One option is for CNN to become like AP for TV, focusing on just the facts reporting.
- Another option is to have political commentary from across the spectrum, but this may not be financially successful.
- Shifting right to appeal to moderates and conservatives could indicate a belief that the political landscape in the US has shifted right.
- Beau expresses skepticism about CNN's potential shift to the right and its long-term impact on the network's credibility.
- Trying to capture a conservative demographic may not be successful and could harm CNN in the long run.
- Beau believes that transitioning towards unbiased reporting like AP for TV could be a positive move.
- Beau warns against presenting both sides of issues that do not have two sides, as it can distort the narrative.
- The future direction of CNN remains uncertain, and there are concerns about the potential changes.

### Quotes

- "There's been some messaging indicating that they are concerned about a perception of bias on the outlet."
- "So a shift right is not going to help that perception."
- "If they're trying to appeal to moderates and conservatives, by definition, they're not unbiased."
- "CNN was in the middle. If they shift right, they are likely to lose more than they gain."
- "CNN is going through some changes. What the final result will be, we don't know yet."

### Oneliner

CNN's uncertain changes spark concerns about potential shifts towards bias and audience appeals, with skepticism on the network's future strategies.

### Audience

Media analysts

### On-the-ground actions from transcript

- Monitor CNN's changes and analyze the impact on their reporting and audience perception (implied).
- Share insights and concerns about media bias and political leanings with peers and online communities (implied).

### Whats missing in summary

Insights on the potential consequences of CNN's shifts and the importance of unbiased reporting in media coverage.

### Tags

#CNN #MediaBias #UnbiasedReporting #AudienceAppeal #PoliticalSpectrum


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about CNN
and what's happening there
and what a lot of other people are talking about
when they talk about CNN.
The perceptions, what we know and what we don't,
and the options that CNN may be pursuing.
If you have no idea what I'm talking about,
CNN is making some changes.
Personality changes, content changes, we know that much.
But that's kind of all we really know.
There's been a lot of commentary
that is based on perception, on what's public facing.
We don't know what their end goal is yet.
And there could be a number of things.
There's been some messaging indicating
that they are concerned about a perception of bias
on the outlet.
And they like to bill themselves as being unbiased.
So they may be attempting to correct that.
There's also been some messaging suggesting
they want to appeal to moderates and conservatives.
And we'll get there.
So what are the options?
One is that they are actually concerned
about the perception of bias.
And they are going to try to become AP for TV.
Just the facts reporting.
That's an option.
Is that what we're doing?
We don't know.
We don't know what they're doing.
If that's the route they're going, more power to them.
It's needed.
But I also don't think that would be incredibly profitable.
And I don't see CNN going that route.
Another option would be that they
are trying to create a network that
has political commentary from across the political spectrum.
Interesting.
Again, I don't know how that would work out financially.
You won't build a base when you have conflicting evaluations.
It shouldn't necessarily cause a problem.
But in the United States, that's probably not
going to be a great network, financially speaking.
The idea that they are shifting right.
That they're actually going to try to appeal
to moderates and conservatives.
That means that a profit-seeking entity
believes the Overton window in the United States
has shifted right and that it will remain there
for at least the foreseeable future.
Those are the options I see.
And again, we don't know.
With all the commentary on this, there's
a whole lot that's unknown about what they're attempting to do.
If they are trying to shift, if they're
doing anything other than trying to become AP for TV,
who, what, when, where reporting,
I personally think it's a horrible idea.
First, if they're trying to appeal
to moderates and conservatives, by definition,
they're not on bias.
If you're creating a product to appeal to a demographic,
you're not doing just the facts reporting.
You're creating a framing that would appeal
to a certain group of people.
So a shift right is not going to help that perception.
I also think that the Overton window has shifted right
in the US, but I think it's already
started shifting back left.
So if this is that move, and they're
trying to appeal to that because of that shift,
they're late to the game, so late that by the time
they make the changes, they're going to be outside
of where they should be.
So that's not great either.
Aside from that, if they're trying
to capture that conservative demographic,
they're not going to.
It's really that simple.
There are a lot of people pointing to how
CNN took a hit under Trump.
Republicans became more distrustful of the network.
The thing is, I can remember it being called the Clinton News
Network in the early 90s.
This isn't a new thing, just more pronounced under Trump.
So the shift to the right, I don't
see it accomplishing any of the goals
that a profit-seeking entity would want to accomplish.
Long term, it might damage CNN severely.
Now, if they are trying to slowly shift towards AP for TV,
good.
I mean, really, seriously, that's a good thing.
We need that.
At the same time, they're not going
to get there both sides in things
that don't have two sides.
That type of coverage, even if it's just in a transition
as they try to get to AP for TV, it's
going to damage them beyond anything else.
CNN is in a situation where they have
tried to remain centrist, really.
MSNBC kind of took the left, what
is considered the left in the US.
And Fox on the right, CNN was in the middle.
If they shift right, they are likely to lose more
than they gain.
If they go to just the facts reporting, that's great.
It really is.
It probably isn't going to increase their market share,
though, which may be what they're trying to go for.
Any moves that are made to appeal to a conservative base,
I think are going to do more harm to the network than good.
I don't think that they're going to try to turn into Fox Lite.
I don't think that's where they're headed.
I think what they're the most you may get
is a lot of both sides of things that
don't have two sides, which in today's climate
may be just as damaging because it pulls a false narrative out
of an extreme and brings it to the forefront.
So CNN is going through some changes.
What the final result will be, we don't know yet.
There is reason for concern.
So those who are making commentary that are basically
saying they're shifting right and it's going to be horrible,
they might be right.
I'm not ready to say that because I don't
know what they're doing yet.
But the scenario is that they're going
to be doing it, but the scenario that is being painted
by a lot of commentary, it's not impossible.
It's not even unlikely.
It's just from where I'm at, I don't know that yet.
I can't make that claim until I see a little bit more
about what they're going to do.
But CNN is definitely in a situation
where it has a very limited amount of time
to make whatever shift they're planning on making.
And early signs indicate they're making
a shift that is probably not good for the network
or the country.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}