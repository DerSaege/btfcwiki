---
title: The roads to a Twitter Q&A (September 2022)
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P1MgOHVcZnQ) |
| Published | 2022/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau answers Twitter questions, providing live responses and insights on various topics ranging from nuclear documents to history book recommendations.
- He shares his thoughts on a potential Constitutional Convention, imitating a train, high school history classes, and favorite anime.
- Beau offers advice on studying history, suggests positive news channels, and addresses ethical questions about pet ownership.
- He talks about Liz Truss, VOSH, community organizing, defeating anxiety attacks, and advocating for unionizing.
- Beau touches on automation, defeating fascism, NATO's charter, Batman's ethics, and dealing with political fallacies.
- He shares insights on democracy, civil war avoidance, media leanings, and the importance of electoral engagement.
- Beau comments on sleep habits, favorite snacks, t-shirts, and political analysis strategies.
- He offers advice on navigating societal conflicts, engaging with right-wing media, and countering fascism.

### Quotes

- "Find what you're good at, what you're passionate about, and use that because you're going to be better at that than anything else."
- "Panic is the inability to affect self-help. You never want to be in that situation."
- "Nobody's born and wants to have their identity controlled by the state and wants everything that they get to think, do, or say determined by a massive national government."

### Oneliner

Be engaged, find passion, and resist fascism with Beau's insightful Q&A responses on various topics.

### Audience

Active citizens

### On-the-ground actions from transcript

- Join community organizations to advocate for unionizing (implied)
- Engage in disaster relief efforts or support those who are actively involved in it (implied)
- Support positive news channels like PLN to stay informed and spread good news (implied)

### Whats missing in summary

Insights on engaging with right-wing media for balanced perspectives and effective activism.

### Tags

#Q&A #CommunityEngagement #Activism #PoliticalInsights #Fascism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to do a Q&A from Twitter.
These questions came off of Twitter.
This is a way for people who can't make it to live streams
because of time zones or whatever
to be able to ask questions and get a live response, kind of.
I haven't read the questions in advance.
If I skip it, it's probably because I already
have a video coming out on that topic or plan to do one.
OK, so let's see what's here.
So here's a question I've had.
Are the nuclear documents in question regarding the raid
the ordinance kind?
I've seen a TV expert say it could have been the foo.
Now, I mean, I am 99.9% sure that this is not
football-related stuff.
If it was, and the FBI waited an entire year to retrieve them, we have a whole new set
of problems.
I think that is incredibly unlikely.
Are you concerned about the secret Republican plan for a two-thirds states called Constitutional
Convention?
I have a video coming out on this, but short version, not really.
The paper plots that they have going on, that really just shows their weakness, and in that,
the fact that they have to go that route demonstrates that it won't work long term.
Even if they were to attempt to subvert the Constitution, change the way ratification
is done and try to weasel around that and all of the different things that they've
planned, it still doesn't work out in the end the way they want.
it might just be uncomfortable for a little bit.
But we'll talk more about that later.
Asking the important questions, when imitating a train,
what is the optimal chugga to chew ratio?
2 to 1, obviously.
Chugga, chugga, chugga, chugga, chew, chew.
OK, my high school history classes
never went past 1865 because it could get political after that.
It hurts my soul.
And I've been trying to play catch-up since.
Any topics, books, whatever you recommend,
I know it's a broad question,
but my Florida education really kneecapped me here
and I feel embarrassed.
No reason to feel embarrassed about something like this.
There's a text, it's called The American Pageant.
It's a normal high school history book,
it's probably available at your library.
I'd read that and for me,
it's also very useful to use timelines.
put together timelines of events,
and then ask the eternal question, why?
Why did one thing lead to the next?
And as you dive into it, you find little bits
that you're interested about, which
will lead you to other stuff.
And it becomes like a web of learning.
And it's fun.
Since you obviously watch YouTube,
there's a thing on YouTube called oversimplified history
or something along those lines.
That's pretty good.
Mayo on the top slice or the bottom?
I don't use a lot of mayo.
Will you post more blooper reels?
Occasionally, some end up on Twitter.
What do you know about Liz Truss?
She's a British politician for Americans.
I don't have a firm opinion, because the only thing
that I have been exposed to are her blunders when talking about foreign policy or suggesting
that a certain Irish political party is doing what it's always set out to do, and that being
a surprise.
Because that's my only exposure to her, I don't know if she's just not great or just
not great at speaking and conveying her ideas.
Let's see.
If you do, I would suggest Halloween night.
Seems appropriate.
I don't know what that is.
Oh, that's probably for the timing for this.
No, Halloween is a night that I typically actually take off.
We need a top 10 good news, news stories, wins, example, California now provides breakfast
and lunch free to all students, no questions asked, no means testing.
So there is a channel on YouTube called PLN, it's like Positive Leftist News.
They put out, I think, just one video a month to the beginning of the month.
That's what that is.
As far as me doing that, it doesn't really fit well with my format.
But I'm going to try to start including some positive stuff when I find it.
Can you do a video about the ethics of keeping pets?
I got my pet's spay and feel guilty because she didn't or couldn't consent.
Yeah, I'm sure there are people more qualified than myself to provide an answer to that.
So I'm going to pass on that one.
Would you ever do something with voosh?
So I'll actually end up talking about him
in an upcoming video.
For those who watched the video in question,
I was talking about a person who got pulled out of the far
right by a streamer.
And I didn't want to name him, because at that point
I hadn't talked to him and found out if it was OK with them.
StreamerX in that video is VOSH.
So that will come up soon.
Follow-up question, would you consider
doing a panel with VOSH, Aaron from Re-Education,
Mexie, and other leftists where you're
given real-world problems and you debate the best
practical solution for them?
No theory, just here's the problem,
Here's the solution.
I generally don't see debates as being super effective,
even though I just got finished talking about a story in which
it absolutely did work.
And as far as me personally, I have two modes.
It's the very mellow mode that y'all are used to.
And you're a short-sighted fool, and this is why.
Like, I don't have a middle gear on that.
And my mellow presentation doesn't really
go well with debates.
It becomes very boring.
And the other one isn't great, because while I don't really
know Vosch, like Aaron and Maxi, I like them.
And I don't want to argue with them
in that kind of framing, in that dynamic.
And it's funny, there's a reply from Aaron here, no thanks.
But in short, housing takeovers, free stores, community fridges, and building other dual
power structures is the only viable solution.
Everything else is just incriminalist pacification, one of the reasons I like Aaron.
That community networking is what he's talking about.
What framing of the Mar-a-Lago story have you seen have the greatest effect as far as
making Trump supporters question their support for him?
As far as mass communications, I don't know.
It hasn't been around long enough for me to get a whole lot of feedback on what actually
works.
In person, what I have seen work is explaining what SCI documents are and the type of information
that's on them, and then the consequences of that information being obtained by opposition
groups explaining that it may just be a sentence that says, this information was obtained from
a human source.
The Russian government, they have compartmentalization as well.
Only eight people knew that information.
So the Russians then zoom in, narrow it down, find that person, and they fall off a building.
It's that type of stuff.
And then explain that once that happens to an asset, it's harder for the US to recruit,
it undermines US national security.
And when you're talking to Trump supporters, it undermines US power because that's what
they care about.
What would you consider a just hierarchy?
OK, so let's say there's a worker co-op.
And one person in the group of people making decisions
has a PhD in marketing.
And the question at hand has something to do with marketing.
That person's opinion should probably
weigh a little bit more.
Generally speaking, just hierarchies
are naturally forming, and they're temporary.
Because if the next question the co-op is looking at
is something about manufacturing,
well, the person that actually knows
how to manufacture stuff, their opinion
should matter more than the person with a PhD in marketing.
Unjust hierarchies tend to extend
beyond their actual use.
you know if you're looking for advice on how to make boots you go to the boot
maker let's see what's your favorite anime and why is it one piece I'm not I
don't have a comment on that do you see centrist moderate Republicans moving to
support Trumpists in order to avoid an internal party split? No. Conservatives
are intractable. They don't like change. They like stability. If they start to see
Trumpism as a threat to their party, they will push Trumpism out as far as when
you're talking about these centrist moderates, not those who have fully
bought into Trumpism.
Examples of nations that have gotten to the US's current level of untenability, crumbling
infrastructure increased fasciness, regressive policies, oligarchy, et cetera, and improved
without imploding first.
Think about any country that was a monarchy that then became a democratic republic.
There are countries that make those transitions peacefully and make them well.
In the U.S., we have swung back to the right, but I actually think that we've already started
swinging back left, but we just haven't seen it yet.
Let's see, I've had to explain to people who think NATO was the aggressor against Russia.
many other things, NATO wasn't just a foil against the Soviet block, but also to
stabilize Europe. I'm wondering if you could give a more
detailed rundown on NATO's charter and why it's needed. I actually have a video
that goes through the whole thing, article by article. A lot of people who
are looking at the situation in Ukraine and trying to pin the aggression on NATO
are doing so out of habit. For a whole lot of people, their entire lives, the
United States has been the only country out there really making moves,
especially those that lead to war. So it's kind of a default for a lot of
people to assume that the US and by extension NATO is to blame.
Most of these people are also people who consider themselves anti-imperialists.
Ask them to explain how NATO is at fault without granting Russia final say over Ukrainian sovereignty.
Because most times the argument is NATO was expanding close to Russia's borders, therefore
Russia had to do this.
The problem with that is that that is granting Russia control over Ukraine.
you think of the idea of selfishness being the fifth horseman? I mean, it's a
cool concept, expand on that. If one is a low man lacking the ability to be
elected or unqualified to hold office, what should such a man be doing with his
time to help fight fascism? When you're talking about any cause, find what you're
good at and what you're passionate about. This is a question I get a lot, you know,
So what can I do to help?
Find what you're good at, what you're passionate about, and find a way to apply that skill
set to your cause.
There's a lot of people who try to say, this is the right way to activist and you have
to do it in such a such a way.
That's not true.
You support the cause in the way that you can the best, and it may not be something
that's typical. It may not be something that people even really acknowledge as
being part of the cause, whatever the cause is, and it may be thankless. There's
a person I follow on Twitter and they make flyers, they make templates for
flyers, and they have a very unique style. They're very easy to pick out when you
see them, and I see them on Twitter all the time, shared as memes, but I also saw
one in the wild on the side of a building in Pensacola.
And I have no idea if that person even knows that their work actually does get out there
like that.
You find what you're good at, what you're passionate about, and use that because you're
going to be better at that than anything else.
Brownies 9x13 pan or 9x9?
by 9.
I'm going to say 9 by 13, because I
don't think I have a square pan.
Where do you think the contents of the over 40
empty classified folders are, in general, of course?
Riyadh, Moscow, the sewer?
We don't know.
We don't know.
That's not something I'm going to speculate on.
That accusation that those documents were actually
transferred to a third party, that's huge.
That is huge.
I don't know, but rest assured, there
is a counterintelligence investigation going on
right now to find out what the answer to that is.
And odds are, in most situations, once they know,
we'll know.
Best methods for defeating anxiety attacks
that manage to break through watching Bob Ross.
Do the three, three, three thing.
What is it?
You name three sounds, you can hear three things you can see
and move three parts of your body, something like that.
And that typically refocuses you.
And then there's obviously breathing
and all of the other stuff.
How do I bring up advocate for the unionizing
in a business that treats employees pretty well?
at least relative to other businesses, that's hard.
That's incredibly hard.
The thing to acknowledge is that most times,
when that's happening, it's because the employees already
have some kind of de facto representation with management.
And that's why they're treated well.
It is more difficult. Now, if there are other places in,
If there are other businesses in your industry where employees
are treated worse, it might be a good approach to say,
we can unionize here, not because we
have a problem at this place, but to act in solidarity
with this other group.
But that's always an uphill fight.
When you have a group of people who
have a management system that is responsive,
and therefore the employees are treated decently,
It's hard to get them to organize.
Robots and AI put masses of people out of work.
Do you, A, expand social security,
B, guarantee them SNAP and government micro-apartments,
or take turns cutting Jeff Bezos' hair?
I don't fear automation.
I think automation is one of the things that
is going to force the next change in societal order.
When all of that happens, I mean,
that's the first step to fully automated.
That's the first step to Star Trek.
When you get to that point where it
isn't labor that is required at that level
to produce things to be a consumer,
Things shift from the acquisition of material wealth and hopefully shift towards making
the world better.
It seems that instead of fixing our issues, we vote short term toward the least of the
worst.
How do we fix the U.S. long-term and get out of the two-party system that we have?
If you're going to use electoral politics, you have to believe in electoral politics,
which means you've got to run.
You've got to get out there and be a candidate.
You have to back candidates.
You have to make sure that candidates who aren't just the least bad option win the
primary so you can actually get people that you could stand behind and that want the same
things you do.
Democracy requires advanced citizenship.
Have you ever put any thought into fictional governments and how you'd improve them?
Not really.
That's not something I've really ever, I've never really considered that a thought exercise
but it might be fun.
An honest, independent, nonpartisan news source is the Associated Press.
How many t-shirts do you own and how do you keep track of them?
A lot, I don't know, and it is funny that people think I keep track of them.
Tons of questions about favorite books and authors.
All of them, I think, I never have a good answer to that.
Scientifically for every foot that the ocean rises, the water comes in about 10 feet.
No question there, but I mean there's some information.
Cornbread with or without cracklings.
You're trying to start a war.
So one of my grandmothers made it one way, the other made it the other.
And since they're both gone, I can give you my honest opinion.
If you're eating barbecue without, if it's pretty much any other meal with, and now there's
a whole bunch of people not from the South, like Googling this and finding out that it's
like pork rinds is what they say or something like that.
If Mexico had a stronger centralized government, would that be better, worse, or indifferent
for controlling cartel influence in the region?
That influence is based on corruption.
Centralization just makes it easier to corrupt.
I'm a huge fan of decentralization in almost every way.
This is a situation where the product is wanted, there's a bunch of money in it, it is going
to exist, whether it be in this fashion or through a process of legalizing that industry.
DeSantis barely won in 2018.
Has he gathered more Florida voters or alienated more Florida voters since then?
Would losing his re-election bid stop, slow, or not impact his presidential aspirations?
So, I don't know about whether or not he's alienated more than he's gained.
My circle, my area, he has alienated a lot of people, but I don't know if that's statewide.
As far as him losing, yeah, if he loses Florida, his presidential dreams are over.
Because if he can't carry Florida as a governor, he can't carry it as a presidential candidate,
And the Republicans have to win Florida to win.
I think what's going to decide that is voter turnout.
And we don't know if DeSantis creates the same amount of negative voter turnout that
Trump does.
People showing up to vote against DeSantis, not showing up to vote for Christ.
side is our military on, the U.S. constitutions. The United States military is a political
and there are a lot of safeguards to keep it that way. Any thoughts on CNN's sharp turn?
I have a video coming out about that soon. What is one topic you've always been curious
about and wish you had more time to study in-depth economics economics given the rise
of fascism the impending doom of climate change and all the other things keeping us up at
night what do you think the carolina panthers are looking like with an improved defense
and mayfield at qb yeah those are words i don't know what they mean i i actually don't
a lot of sports and I have zero opinions like I am aware when certain teams play
because you know of like local loyalty to them but I'm not a huge fan of any
sport really. Is it likely there will be an armed conflict in the US in the next
I don't think so, not in the way that most people are picturing, which is like all-out civil war, you know.
Low-intensity stuff? Maybe.
Likelihood of Dems winning a supermajority and ending the filibuster?
I think that there's going to be a lot of polling errors this time, because I think a lot of people who aren't typical
voters,
voters and certainly wouldn't be typical midterm voters are going to show up. So
we don't know. I don't know and I wouldn't want to make a guess on that.
Let's see. Do you think we might actually see a United Ireland in our times, in
our lifetimes? Yes. When should we panic? Never. Never. You can be concerned. You
You can be so concerned that you're even driven to act, but never panic.
Panic is the inability to affect self-help.
You never want to be in that situation.
Are you surprised at what you're doing now?
Is this where you thought you'd be five years ago?
Five years ago, I was just starting to entertain the idea of doing something like this.
So the idea that it actually worked, I mean, it was a vague thought, not really an expectation.
Let's see.
Does your extended family watch your content and what do they think of what you do?
A lot of them do.
A lot of them don't.
who don't watch are aware of what I do and they're also aware of my views and that's
why they don't watch.
And most people don't really, most people I know, especially my older family members,
they have no idea how this works as far as what I do.
Because to them, I was a journalist, that's like a real job, and now I'm a dude on YouTube.
So, it's strange at times for them, I'm sure.
How do you keep a straight face when you are owning political fallacies with sarcasm?
That's just long practice at being very diplomatic when saying things and expressing your displeasure
with things in a way that is appropriate in an office environment.
Do you think that the former guy's base has grown or decreased in light of the current
events?
Is there hope that we will move past all this?
Yeah, we will move past all this.
There's no doubt about that.
I think it's decreased.
I think that his support in the military has dropped even further than it was in 2020.
You know, he started losing the support of the military shortly after being elected.
This, for a whole lot of people, the treatment of those documents is going to be a huge thing.
In fact, I know one person who was very much an ardent Trump supporter who after that happened,
they won't admit that they don't like Trump.
They won't admit that Trump played them, but they don't want him to run anymore because
they felt that that was just too irresponsible for anybody.
Do you think we'll be able to avoid civil war?
Yes, I do.
I really do.
I think there might be some light.
Some light violence may occur, but I
think that it is still very likely that we can get past it
without things getting way out of hand.
Not a question, just a comment.
I love how you care about animals.
Any ideas on countering the rightward lean of major media outlets recently CNN?
Man, there's a lot of questions about that.
I have a video coming out about it.
But cable news is going away.
Their effects are going to be limited to the next 10, 15 years.
After that, it's going to be more what happens here on the internet and through streaming
services.
you ever do an uncut more adult version of your videos? Unlikely. I like to keep
my stuff very accessible and so that has a lot to do with it. Let's see...
What comics do you read? I don't read any anymore but when I was growing up GI Joe
and the Punisher. That's not just a total cliche. Do you hit thrift stores to
find these awesome t-shirts that always
hint at the underlying message.
So it's more like every time I go into a big box store,
I hit the clearance racks.
Or stores like Five Below, or sometimes you
go into a specialty shop of some kind,
like a Halloween-themed store.
I got a whole bunch of Halloween-themed shirts
that will later be used in various ways.
Let's see.
If DIMMs do pick up two seats in the Senate,
do they actually carve out the filibuster
and pass legislation, or do they do nothing?
That's going to depend on who those senators are.
Georgia may turn blue during the 2020 election,
but Herschel Walker is leading in the polls.
What does that say about MAGA's hold in Georgia
and other southern states, I think there are going to be a lot of polling errors.
I typically put a lot of faith in polls, but right now they are very much focused on the
same thing they're always focused on, which is likely voters.
Likely voters changed the second the Supreme Court made some decisions.
So I think they're going to be polling errors.
Let's see.
Is your coffin, I mean bed, air conditioned, or are you a day walker?
Just kidding.
We know you don't sleep.
Yeah, I don't sleep a lot.
Growing any garlic in the garden?
No, I'm not.
What's the best kind of firewood?
Dry.
I like the smell of cedar.
Western Europe asking how much of the US polarization in the last decade is due to FSB bot wars in
your view?
Interesting.
What significance do you attribute to it in the upcoming months during the Ukrainian-Russian
war and our Western societies are still not yet brought up to be cyber aware?
Russian information operations are really good and we know that they have tried some
in the United States to stoke unrest, and how effective they've been, it's hard to
tell because what they tend to do is pick something organic, something that's already
happening and then amplify it.
They don't try to create an issue out of whole cloth, they find something that already
exists and use that to create dissension and cause disruptions. So it's very hard
to measure its effectiveness. What's your view on universal health care? For it,
what are the five greatest wrappers of all time? Snoop, Dre, DMX, M&M, and Snow,
the product. I know somebody's gonna say something about that last one. Yeah, you
you do it that fast in two different languages. Will you be releasing a blooper video? I guess
y'all really liked that. I don't know, maybe at some point. I think it'd probably be more
fun to do it for the on the road stuff because those are more entertaining. Is a hot dog
a sandwich? Yes. Let's see. Have you heard about the Kaiser strike? Supposedly the longest
ever mental health professional strike ever. I just heard about it myself. I'll look into
it and see what's going on. You've mentioned you watch a lot of right-wing news. What outlets
do you usually use and why? All of the ones that you would expect and then a bunch of
them that are really out there. A big part of why I do this is to try to break through
echo chambers, I have to know where the walls are. So I consume a lot of right-wing media.
It's not something I would advise, especially some of the outlets. Unless you're really
inoculated against that kind of rhetoric, it can probably be pretty damaging to your
mental health.
What is your favorite late-night snack? Ramen noodles. Let's see. Destin or Fort Walton.
It depends.
How much money do I have?
Let's see.
If states like Arizona have sold their water rights,
are they legally obligated to uphold these terms
in these times of severe worldwide drought?
When it comes to international contracts like that,
When things get bad enough, laws fall silent.
The United States is not going to allow export of water
if things are really bad.
Batman, good or bad?
Bad.
I could do an entire video on this.
Those kind of resources, there should be a lot more funding
by Bruce Wayne into solving the problems before they start,
rather than just beating up the bad guys.
And then those who are just truly irredeemable people,
and these characters that keep coming back,
and he takes them and he locks them up, turns them over,
they get locked up, knowing that they're going to escape again,
and they're going to hurt more innocent people.
But he doesn't want to permanently solve
the situation, because if you kill one murderer,
there's still the same number of murderers in the world. Yeah that's true
but not if you do it twice. So I have a lot of problems with Batman as the way
he's framed as a good guy. Let's see what's your favorite Union or Union story
Blair Mountain. How can the government make nuclear power a more economical more
economically viable investment. I do not know. I don't know enough about it. Would you ever consider
another facial hair change? No, I like my beard. It might get shorter, but generally speaking,
this is what I have. Do you see any relationship between the timing of the execution of the warrant
and the establishment of the golf tournament hosted by Trump and backed by the Saudis?
Correlation. I need evidence to say causation.
Is there any chance that Biden's speech about fascism can backfire on him with
mainstream Republicans? I don't think so. I think that was a good move on his part
because it makes the midterms about Trump, which is the one thing the Republicans didn't want.
How did the documents physically leave?
Yeah, that's a question.
That's a question that a whole lot of people are going to have to answer and get in trouble
for at some point.
Let's see.
What got you into being against fascism?
I think that's the default state.
Nobody's born and wants to have their identity controlled by the state and wants everything
that they get to think, do, or say determined by a massive national government that also controls
the economy and uses a whole lot of coercive means to do it. That's not a natural state for
the human condition. You have to be indoctrinated into that. So that's, yeah, I just, I assume that
that that is the default. Let's see, what is your t-shirt filing system? Chaos
theory. Did you ever change a Trumpist to merely a libertarian? When I'm doing this
personally I tend to like go and appeal to working-class ethics and by the time
it's done they're actually far left. I don't... when I'm doing it in person and
trying to reach people it is very much a an all-or-nothing thing. When you say
inbox does that mean DM? Yeah, DM or on any social media platform or a comment
that I happen to see. It's just a way of saying, hey, this is something that came to me.
How are the wife and kids? Good. And the horses? Good. Did you get a tiny horse yet? No.
If a civil war breaks out, what would be your advice to a single woman on how to act? Leave. Leave.
And this isn't a single woman thing.
This is for anybody.
If it comes to that, and you aren't of the mindset where you're going to fight, get out.
Get out and get out early.
All right, let's see.
Your method of analysis and evaluation by training or intrinsic ability.
little of both. Somebody saw the trite and encouraged it. Let's see. What scares
you, true believers? The most dangerous people on the planet. Do you think
changing demographics will be enough to bring electoral change quickly enough to
save us? Yeah, I mean I don't think it's gonna be just that, but I do think there
are a lot of options still open, aside from things going really bad.
When do you sleep?
Not much.
And there's no real schedule.
Let's see.
Favorite beverage to have with cookies?
Milk.
Oreos, you know, stab the side, dunk them.
Let's see.
As an activist who can no longer do the thing he was best at because I am now a public figure,
what do I do now?
Oh, I feel this.
I was actually just venting about this recently to somebody.
Is there a way that you can force multiply?
Is there a way because of whatever platform you have been granted, is there a way you
can use that to create more use?
For me, I was complaining because I like doing disaster relief.
It's something that I'm good at and I enjoy doing it.
I haven't been able to do it in a long time, not because there weren't opportunities or
places where I could be useful, but because I had to do other things.
But I hope, and in some cases know, that people who watched this channel were there.
Maybe you can create a situation like that where you may not be able to do whatever it
is that you're really good at, but you can help other people do it.
Is it possible or likely that the top secret document that Trump had was reported by one
of the Secret Service agents and not an inside leak?
I would suggest it's almost a certainty that there was a corroborating Secret Service agent
corroborated it. My guess would be during a sweep, a security sweep, they
happened to see something and immediately called the counterintelligence
people. Never made it to a live stream because of the obvious time zone reasons.
Do you have a photographic memory? I have a really good memory. I'm one of those
people if I can read something twice I will probably remember it, like forever.
Let's see, casserole, townhouse, Ritz, or tater tots.
Tater tots, townhouse, Ritz, in that order.
If a former president is in prison,
did the security detail go with them?
I think the most likely answer there
would be the Secret Service would administer
home confinement.
Do you own guns?
What is your favorite?
If so, I do not anymore.
This is funny.
Somebody actually answered this correctly.
Not anymore in the MP5, and those are right.
That's the right answer.
I don't own anymore.
When I did, I was a big fan of the MP5.
Let's see.
Sorry, there, there are some, there are some conversations going on in this thread as well.
Is there any chance that Trump has the football?
No, I don't, I do not believe that that is a possibility, uh, in any
realistic sense, how might Biden have modified his message to better challenge
the media and sane Republicans and generate less crazy pushback about being too broad brush.
The assumption there is that the goal wasn't to create the pushback.
I think it was.
I have no criticisms of that speech, of the tactics used in that speech.
Sure, words might be changed, but the overall strategy seems to have been to elicit the
exact response that it got, and I think it was a good move.
Let's see.
Spaces?
I don't know what that is.
OK.
Looks like everything else is just conversation.
Let's see.
What is out of the loop?
What does it mean when your patch is upside down?
It means that short version means watch the whole video.
Most times it means satire, but also it
could be one where I'm just, there's a plot twist
at the end of the video as well.
It may not necessarily be satire.
OK, so that looks like all of them,
unless I missed anything.
And so if you can, tune into the next live stream.
If not, we'll do something like this again sometime soon.
I hope y'all had a good Labor Day.
Anyway, it's just a thought.
Y'all have a good day. Thanks for watching!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}