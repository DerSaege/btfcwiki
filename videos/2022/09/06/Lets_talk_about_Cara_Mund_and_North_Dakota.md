---
title: Let's talk about Cara Mund and North Dakota....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pIC9kjKN8-c) |
| Published | 2022/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Follow-up on a U.S. House of Representatives race in North Dakota, where an independent, Mund, entered the race, changing the dynamics.
- The Democratic nominee suspended their campaign, leaving Mund to potentially attract disaffected Republicans.
- Mund could appeal to Republicans seeking choice and those tired of recent Republican Party actions.
- With name recognition, celebrity status, and news of the Democratic nominee's campaign suspension, Mund has an advantage.
- North Dakota, a red state, still has residents interested in progressive ideas and options.
- Approximately 17,000 people in North Dakota watched Beau's channel in the last 28 days.
- Mund positioned herself to provide a wider array of options for voters.
- Although a long shot, Mund has a chance to win the House seat as an independent.
- Republican Party should be concerned about potentially losing the race in North Dakota due to changing dynamics.
- Mund's underestimation may work to her advantage in the election.

### Quotes

- "You can't buy press like that."
- "It is not impossible."
- "There are people there who want a wider array of options."
- "It's probably going to be a whole lot easier to vote for somebody with an I after their name."
- "This is a case where people underestimating her may work out to her advantage in a big way."

### Oneliner

Mund's entry as an independent in the North Dakota House race challenges the Republican stronghold, appealing to disaffected voters seeking choice and change.

### Audience

Voters in North Dakota

### On-the-ground actions from transcript

- Support Mund's campaign by volunteering or donating (suggested)
- Spread awareness about Mund and her platform within your community (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the changing dynamics in the North Dakota House race and the potential shift towards an independent candidate, Mund, challenging the Republican nominee.

### Tags

#NorthDakota #HouseRace #IndependentCandidate #ChangingDynamics #RepublicanParty #Voters


## Transcript
Well, howdy there, internet people, it's Bo again. So today we're going to do a little follow-up,
provide an update on a story, a topic we talked about three or four weeks ago. A U.S. House of
Representatives race in North Dakota. Not a race a lot of people were following because it's a
very red state and the outcome was predetermined, right? So a few weeks ago, an independent
entered the race. Caramand. Caramand is a former Miss America and has a very interesting biography.
If you have a stereotypical image of the term former Miss America, go watch the other video,
get her biography. When that happened, I said that the dynamics of that race were
probably going to change.
The dynamics of that race have changed.
So Mund is a fan of choice.
The Democratic nominee, Haugen, is not.
Haugen has suspended their campaign.
Democratic nominee has suspended their campaign.
That means Amund is going to take all of the votes from the Democratic Party.
Now all she has to do is peel off disaffected Republicans.
Those Republicans who may want their children to have access, to have choice.
Or those Republicans that are just tired of the nonsense that has been coming out of the
Republican Party as of late. Now, Armstrong in 2020 got like 69% of the vote, but a lot
of that was riding on Trump's coattails. You go back to the midterm in 2018, well, he got
60% of the vote. 60%, that's hard to overcome, but it is not impossible, especially when
you have name recognition, celebrity status, and there's about to be a whole bunch of news
articles about the Democratic nominee suspending their campaign.
You can't buy press like that.
When you combine the new dynamics with what happened in Kansas, with what happened in
Alaska, the Republican Party probably should have a little bit of concern because this
This is a race that may not go the way people are anticipating.
The Supreme Court decision that stripped half the country of its rights, it's going to have
a lot of fallout for the Republican Party.
Mund successfully positioned herself to basically be able to pick up those disaffected Republicans
while having the support of the Democratic Party, but not having the liability of being
a Democrat.
When a Republican walks into that voting booth and they want their kid to have choice, it's
probably going to be a whole lot easier to vote for somebody with an I after their name,
an independent, than it would be to switch parties.
image of North Dakota, a lot like the image of former Miss America, is also a
little bit misleading. Yeah, it's a red state, but that doesn't mean that people
don't want progressive ideas, that they don't seek them out. Just out of
curiosity, I went and looked. A little bit more than 17,000 people in North
Dakota watched this channel in the last 28 days.
There are people there who want a wider array of options.
Mundt positioned herself to be the one to provide them.
It's still a long shot, but it's not impossible.
It is not impossible.
And if you're the Republican Party and you now find yourself in a situation where you
have to worry about winning a U.S. House of Representatives race in North Dakota, that's
probably the point where you need to acknowledge that there's a problem, that the rhetoric,
that the policies, that the desire to control people is really chipping away at your base.
has a chance be a long fight, but I think that this is a case where people underestimating
her may work out to her advantage in a big way.
Armstrong probably feels very secure because of that 69% vote that he got in 2020.
I don't know that he should feel that secure.
What happened in Kansas, what happened in Alaska, can happen in other places.
And this is a situation where these stars might align to put an independent in the North
Dakota House of Representatives seat.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}