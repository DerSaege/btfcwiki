---
title: Let's talk about what we can learn from Arkansas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AwOmGSWelG0) |
| Published | 2022/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Get Loud Arkansas, led by state senator Joyce Elliott, is working to ensure up to 104,000 people who may have been improperly removed from voter rolls can vote in the upcoming election.
- Voter purging occurs frequently, especially in red states, where voters are removed for various reasons.
- It's vital for individuals participating in electoral politics to verify their voter registration status to prevent disenfranchisement.
- Not every state has an organization like Get Loud Arkansas actively ensuring voter registration is up to date.
- Individuals from demographics that ruling parties may not want to vote or those in areas that typically vote against the predominant party should be particularly vigilant about their voter registration.
- The rhetoric surrounding elections has been used to justify actions that undermine people's voices.
- In states, especially in the South, it's advisable to verify your voter registration status before the upcoming election.
- Ensure you have enough time for any necessary registration processing before the election.
- Familiarize yourself with the voter registration laws in your state to understand the requirements.
- It's critical to safeguard your right to vote by confirming your voter registration status.

### Quotes

- "It's probably a good idea to check on your voter registration."
- "Not every state has an organization like Get Loud Arkansas that is going to go out and harass you into making sure that your voter registration is up to date."
- "If you plan on voting, this is something you might want to just make sure that everything's still in order."
- "The rhetoric surrounding the last election has given cover for a lot of people to, in the name of election integrity, subvert people's voice."
- "Just stop by and do it with enough time to make sure that your new registration, if you need one, has time to process."

### Oneliner

Get Loud Arkansas ensures 104,000 potentially disenfranchised voters in Arkansas can participate, stressing the importance of verifying voter registration nationwide.

### Audience

Voters

### On-the-ground actions from transcript

- Verify your voter registration status before the upcoming election (implied).
- Familiarize yourself with the voter registration laws in your state (implied).

### Whats missing in summary

Importance of vigilance in safeguarding voting rights and preventing disenfranchisement.

### Tags

#VoterRights #VoterRegistration #Disenfranchisement #ElectionIntegrity #GetLoudArkansas


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about what, well, you,
and pretty much anybody else who is going to participate
in electoral politics, can learn
from a situation happening in Arkansas.
There's a group in Arkansas called Get Loud Arkansas,
and it is ran by state senator Joyce Elliott.
They will be calling, sending emails, snail mail,
going door to door, and trying to make sure
that people are registered, but not in the normal way.
They're going to be focusing on up to 104,000 people
who might have been improperly removed from the rolls.
So come election time, they won't be able to vote.
This is a thing that happens in a lot of states.
It occurs, at least seems to occur more often,
in red states, where voters get taken off the rolls
for various reasons.
If you plan on participating in the electoral system,
if you plan on voting in those midterms,
if you think you might be one of those unlikely voters who,
in some cases we already know, is going to be the deciding
factor in who wins, it's probably a good idea
to check on your voter registration,
because not every state has an organization like Get Loud
Arkansas that is going to go out and harass you
into making sure that your voter registration is up to date.
This would be especially true if you are part of a demographic
that the ruling party of your state wouldn't want to vote,
or you happen to live in an area that typically
votes against the predominant party.
Not saying that it's intentional in any way,
because that might actually be a violation of the law.
But coincidences do happen.
So if you plan on voting, this is
something you might want to just make sure
that everything's still in order, because with a lot
of the rhetoric surrounding the last election,
it has given cover for a lot of people to,
in the name of election integrity,
subvert people's voice.
So if you're in a state, especially in the South,
you might want to check up on that.
Just stop by and do it with enough time
to make sure that your new registration, if you need one,
has time to process.
Check the registration laws in your state.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}