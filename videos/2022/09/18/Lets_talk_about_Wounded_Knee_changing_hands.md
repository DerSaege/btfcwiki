---
title: Let's talk about Wounded Knee changing hands....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HqfNoUiLI50) |
| Published | 2022/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Oglala and Cheyenne River Sioux are purchasing 40 acres of land around the national historical landmark for wounded knee.
- The land purchase is significant because in 1890, almost 300 Lakota people were massacred at this location.
- In 1973, there was a 71-day standoff between the American Indian movement and the government at the same location.
- The purchase allows them to preserve the land and potentially turn it into an educational center.
- Manny Ironhawk expresses the significance of the ghost dance to their people and the duty they carry on at Wounded Knee.
- The misunderstanding of the ghost dance contributed to the tragic events in 1890.
- The land, previously occupied by a trading post, will now be owned by the Sioux tribes.
- It is hoped that the land will serve as a place for people to learn about the history from the perspective of the Lakota people.

### Quotes

- "The ghost dance, the misunderstanding of what they were seeing is one of the contributing factors to what happened in 1890."
- "Today, we are the new ghost dancers, and we carry on a duty that came to us to do what we can for our relatives there at Wounded Knee."

### Oneliner

The Oglala and Cheyenne River Sioux purchase land at Wounded Knee, aiming to preserve history and create an educational center, continuing their duty as "new ghost dancers." 

### Audience

History enthusiasts, Indigenous rights advocates

### On-the-ground actions from transcript

- Support educational initiatives on Native American history and culture (implied)
- Learn about and spread awareness of the significance of Wounded Knee (implied)
- Respect and honor Native American perspectives on historical events (implied)

### Whats missing in summary

The emotional depth and cultural significance conveyed by Beau in discussing the historical importance of the land purchase at Wounded Knee. 

### Tags

#IndigenousRights #History #Education #NativeAmerican #LandPurchase


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about something that has been
a long time coming, something that's been a long time
coming, and a native win.
So the Oglala and Cheyenne River Sioux are engaging in a
real estate deal.
They're purchasing 40 acres.
And this 40 acres sits around a national historical landmark
for wounded knee.
They're paying $500,000 for it, $255,000 coming from the
Oglala, $245,000 coming from Cheyenne River.
And in 1890, almost 300 Lakota people from the Oglala,
Cheyenne River, Rosebud and Standing Rock were massacred. Then in the same location
in 1973 there was a 71-day standoff between the American Indian movement and
the feds and it kind of caused the birth of a widespread movement for Native
Americans. They have been trying to get this land for a really long time and now
they'll own it and they'll be able to do with it as they see fit which means
preserve a lot of the stuff that is still in the ground and a lot of people
hope for it to be an educational center so people can learn about what happened
and they can learn about it from people who are Lakota.
Manny Ironhawk said, the ghost dance
was a beautiful dream for our people.
It wasn't a dream of death.
It was a dream of life.
Today, we are the new ghost dancers,
and we carry on a duty that came to us
to do what we can for our relatives
there at Wounded Knee.
The ghost dance, the misunderstanding of what they were seeing is one of the contributing
factors to what happened in 1890.
It's a pretty cool quote.
For a long time, this land was occupied by like a trading post.
it should be turned into something a little bit more fitting and it will be
held in trust and it's theirs now and I I don't see them giving it up anyway it's
It's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}