---
title: Let's talk about the next set of hearings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b90lolwFBMQ) |
| Published | 2022/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the upcoming hearings related to the events of January 6th and what to expect during those hearings.
- Mentions the general template of telling what you're going to tell them, telling them, and then summarizing what you told them.
- Focuses on the cash and cover-up aspects, with new developments revealing inquiries into possible financial crimes related to money raised for the election fight.
- Emphasizes the significance of Secret Service text messages, thousands of which have been turned over and are being reviewed for insights into Trump's activities and mindset.
- Indicates that the transcripts from these messages will likely take center stage due to their potential to reveal intriguing information that makes for good TV.
- Acknowledges the challenge of presenting information in an accessible and accurate manner to keep the public engaged, especially when dealing with known plots.
- Points out that the dollar amounts raised and where they went will be heavily focused on to maintain public interest.
- Notes that the next hearing is scheduled for September 28, though this may change due to recent information shifting the focus towards money raised and Secret Service findings.
- Suggests that drawing out information on the raised money and its public awareness can aid in future DOJ talks and potentially prepare the public for a former president's possible indictment.

### Quotes

- "They're trying to get this information out to the American people in a way that is going to be a part of the story."
- "It'll make for good TV."
- "Preparing the American people for the possible future indictment of a former president."

### Oneliner

Beau outlines the upcoming January 6th hearings, particularly focusing on financial aspects and Secret Service text messages, aiming to keep the public informed and engaged while preparing for potential future indictments.

### Audience

Citizens, activists, journalists

### On-the-ground actions from transcript

- Stay informed about the upcoming hearings and their developments (suggested)
- Share information about the financial aspects and Secret Service findings with your community (suggested)
- Follow reliable sources to stay updated on related news (suggested)

### Whats missing in summary

The detailed nuances and explanations behind the upcoming hearings and the importance of transparency and public awareness in potential legal proceedings. 

### Tags

#January6th #Hearings #Transparency #FinancialCrimes #PublicAwareness


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about the
next set of hearings related to the events of January 6th and what to expect during those
hearings. We talked about it a couple of weeks ago, but some new developments and new information
have come out. The general template is probably going to be the same. Tell them what you're going
to tell them, tell them, tell them what you told them. And they plan on focusing still on the cash
and the cover-up, their terminology. However, when we talked about it a couple weeks ago,
one of the things I said it was likely focus on would be the money that was raised, the cash
that was raised for the fight against the election and all that stuff. We have since learned that
the Department of Justice and the federal grand jury have been asking questions about that. It's
a line of inquiry that they now have that may lead to possible financial crimes. So that's going to
take on a new level of importance as far as getting that information out to the public,
preferably before the grand jury does anything. Aside from that, there's been a lot of focus
on the Secret Service text messages. Well, there have been thousands now that have been turned over
from the period in question, along with radio traffic. Now at this point, the people on the
committee have their staffers going through them. But that's what they're telling the public. They
probably already know what they need to know. Whether or not it's in there, they're just,
well, they don't want to provide any spoilers for season two is what it boils down to.
So that information is going to shed a lot of light on Trump's activities, movements,
maybe even mindset, depending on what they texted each other. Now that focus is probably going to
take center stage, especially if the transcripts are revealing, because it'll be interesting. It'll
make for good TV. And that's one of the things, like it or not, that is a part of the hearings.
They're trying to get this information out to the American people in a way that is going to
be a part of the story. And that is both accessible and accurate. And that's harder than you might
imagine when you're dealing with something that is, I mean, kind of boring. And especially given
the fact that people already kind of know what happened. And you're filling in holes in a story
that they kind of know the plot of. So they have to keep people's attention. I would imagine that
the transcripts will help them do that, as well as the dollar amounts of the money raised and
where it went. That's probably something that they're going to focus on heavily, because it
gets those sound bites, keeps people engaged. Now, at time of filming, the next hearing is
going to be on September 28. That may change, though. But at this point, the new information
just kind of shifted the focus. These are all the things that we said they were going to talk about
a couple of weeks ago, but now they're probably going to be a lot more intent on the money raised
and on the information obtained from the Secret Service, whatever it may be. Because if they can
draw out information about that money and they can get it into the public's mindset,
when DOJ begins talking about it, it's going to make a lot more sense. And one of the things that
the committee has kind of been forced into the role of is preparing the American people
for the possible future indictment of a former president. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}