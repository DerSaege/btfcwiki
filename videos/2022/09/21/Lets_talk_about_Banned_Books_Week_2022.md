---
title: Let's talk about Banned Books Week 2022....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H_IpG0UyYdA) |
| Published | 2022/09/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Banned Books Week, focusing on books that some in power don't want people to read.
- Mentions the PEN America report covering bans from July 2021 to June 2022.
- Reveals that during this period, there were 2,532 instances of book bans impacting 5,049 schools and over 4 million students.
- Points out Texas, Florida, and Tennessee as the worst offenders in book bans.
- Notes that these bans are often linked to a few conservative groups, impacting the free speech of many authors and illustrators.
- Shows that authors of color and LGBTQ authors are often targeted in these bans.
- Questions why certain groups are trying to silence these voices, suggesting it's due to an inability to defend their ideology.
- Believes that promoting curiosity through books challenges parents who prefer ignorance for their children.
- Suggests that the root cause of banning these books is to uphold an ideology that can't withstand questioning.
- Encourages reading the report and reminds that while reading, "bad books are the best books."

### Quotes

- "Just remember while you're reading that, that bad books are the best books."
- "Those books, those books they're trying to remove, they want to avoid the questions."
- "Parents are making a choice to keep their children ignorant."
- "The worst thing you can do for an idea you support is to present it and defend it poorly."
- "If your idea is indefensible, well, it's easy for them to poke holes in it and that ideology collapses."

### Oneliner

Beau talks about Banned Books Week, revealing the impact of book bans on authors and students while questioning the motives behind silencing certain voices.

### Audience

Book enthusiasts, activists

### On-the-ground actions from transcript

- Read the PEN America report on book bans (suggested)
- Celebrate Banned Books Week by reading a banned book (implied)

### Whats missing in summary

Deeper insights into the contents of the PEN America report and specific examples of banned books.

### Tags

#BannedBooksWeek #Censorship #FreeSpeech #Ideas #Reading


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
books because it's one of my favorite times of year. It's Banned Books Week.
This is a week set aside to highlight and encourage the reading of books that, well,
some people in power don't want you to read. So we're going to talk about books. We're going to
talk about PEN America and their report. We are going to talk about defending ideas and why.
My eternal question, why? Okay, so a glimpse at the report. And the reason I like this one is
it runs from July of 2021 to June of 2022. So basically it covers a whole school year rather
than dividing it up by the calendar year. Okay, so during this period there were 2,532 instances
of book bans impacting 1,648 individual titles. These bans hit 5,049 schools and affected
more than 4 million students. The worst offenders were Texas, Florida, and Tennessee.
A lot of these bans can be tracked back to a small number of conservative groups.
You know, the Republican Party, the group of people who always sang free speech and all of that.
Incidentally, these bans that are government enforced impacted the free speech of 1,261 authors,
290 illustrators, and 18 translators. What's the demographic makeup of them? Yeah, you can guess.
You can guess. I will have the link. There's a whole bunch of other information and I will have
it down below and it goes into this in depth. But the demographics are exactly what you would expect.
The bans are targeting authors of color, authors that are LGBTQ. That's who they go after,
or books that have main characters of those demographics.
The question is why, right? And when you first say that, there's the knee-jerk apparent response,
right? Because the people doing it are old bigots. Like, that's the easy answer to that question.
Why are they trying to silence these voices? But see, I think it's something else.
I'm of the firm belief that the worst thing you can do for an idea is present it and defend it poorly.
That is the worst thing you can do for an idea you support, is to present it and defend it poorly.
Because people who are just hearing that idea, they're going to want to challenge it.
So they can poke holes in it. And if you can't defend it, if your idea is indefensible,
well, it's easy for them to poke holes in it and that idea or that ideology collapses.
The ideology of the book is that it's about the idea of the book.
The ideology that would lead them to specifically target LGBTQ and authors of color, it's indefensible.
Those parents, they want their kids to have that family value.
But they can't defend it because there's not a defense for it.
And those books, those books promote that curiosity.
Those books promote asking questions, questions that the parents don't want to answer because they can't.
So rather than their children having a better ideology, one that could be defended,
they want to remove the books promoting the questions.
Parents are making a choice to keep their children ignorant.
I think that has a whole lot to do with it. I don't think it's that well thought out.
I don't think they necessarily understand that. But I think that's the root cause.
I think they're hoping that if they can just get these books out of these libraries,
nobody talk about it, then that ideology will stand.
Because they know it won't stand if it's questioned.
I will have the report down below. It is definitely worth reading.
Just remember while you're reading that, that bad books are the best books.
You might want to read one of those this week.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}