---
title: Let's talk about Trump, New York, and $250 million....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ctAJVubDoso) |
| Published | 2022/09/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Attorney General James of New York filed a lawsuit against Trump and others, seeking $250 million in relief and requesting sanctions including barring certain individuals from being directors and canceling the Trump Organization's corporate certificate.
- The lawsuit alleges a 10-year pattern of false financial statements aiming to boost Trump's net worth annually, mentioning a couple hundred false or misleading statements.
- Examples include valuing Trump's Wall Street building at $524 million in 2010, a huge jump from $200 million, and overstating the size of Trump's apartment to inflate its value to $327 million.
- These misrepresentations were not minor errors but intentional, according to the allegations, affecting documents used for financial purposes by lenders and insurers.
- While this is a civil case, referrals have been made to the IRS and Department of Justice, potentially leading to further investigation.
- Despite Trump's tendency to exaggerate, this case involving financial documents presents a more serious issue that might result in significant consequences for him.
- Beau believes that although this case won't bring Trump down completely, it will likely lead to significant financial penalties.
- The ongoing legal issues add to Trump's troubles and could potentially escalate into a criminal case in New York.

### Quotes

- "If that happens, they don't work in New York anymore. It's kind of a big deal."
- "There's a word for that."
- "This is going to be a giant, giant pain for Trump."

### Oneliner

Attorney General of New York files a lawsuit against Trump and others for a 10-year pattern of false financial statements, potentially leading to significant consequences for Trump.

### Audience

Activists, Legal Experts

### On-the-ground actions from transcript

- Contact local authorities or legal organizations to stay informed about the developments in this case (suggested).
- Join advocacy groups working on transparency and accountability in financial matters (implied).

### Whats missing in summary

More insights on the potential impacts of the case on Trump's future legal and financial standing.

### Tags

#Trump #NewYork #lawsuit #financialmisrepresentation #legalconsequences


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump and the state of New York, and
what's going on up there.
We're going to kind of break it all down, go over bits and pieces of it, and talk
about where it is likely to go from here.
If you missed it, Attorney General James of the state of New York has filed suit and the
request for relief is in the amount of $250 million, also requesting that some of the
people be barred from ever being a director in an entity up there again and
to cancel Trump Organization's corporate certificate. That sounds, that last one, it
sounds like a little thing in comparison to everything else. That's actually huge.
If that happens, they don't work in New York anymore. It's kind of a big deal.
Okay, so we'll go over the allegations and keep in mind at this point they are
allegations that have not been tested in court. The people named in the suit are
Trump, Trump Jr., Ivanka, Weisselberg, and then there's another executive,
Mcconey, fact check that, and those are the individuals named. The allegations
lay out a 10-year pattern of false or misleading statements, and the suit
identifies a couple hundred of them, alleges a couple hundred of them. The
The general premise is that Trump wanted his net worth to go up every year.
On paper, at least.
And the team, well, they made that happen.
Even if there wasn't a reason for the net worth to go up.
These documents were created, and they were basically summaries of financial health of
the company.
There were valuations, stuff like that in it.
One of them that I think is most notable is dealing with the building on Wall Street,
because the man of the people obviously owns a building on Wall Street.
But in 2010, there was an appraisal that they had for $200 million.
In 2011, they said it was worth $524 million.
That is a huge jump.
Now in theory, you could say, well, obviously something happened to drive the property value
up.
The problem is that according to the allegations in 2012, there's another appraisal for $220
million, but it doesn't stop there.
Wait, there's more.
In 2012, they listed that property as being worth $527 million, and then in 2013, they
listed it as being worth $530 million.
These aren't simple cases of being a little lazy and rounding something or minor mistakes.
These are huge.
These are huge misrepresentations, if that is in fact what happened.
Another one deals with Trump's apartment, which was said to be 30,000 square feet.
Problem is in real life, it's 11,000.
But based on that square footage size, they valued the apartment at $327 million.
It appears that no apartment in New York has ever sold for anywhere near that amount.
This is kind of the examples that will be used.
Now the obvious question is, I mean this is just Trump, like I mean for real, this is,
you can't know anything about Trump and not expect something like this.
When it comes to inflating the size of things or the value, that's like his thing, you know?
We kind of all expect that.
So what's the difference between this and him talking about crowd size or whatever else?
These forms were used by lenders and insurers.
They were presented to be used in that way and that's what the allegation hinges on.
To take it out of the billionaire elite class and bring it down here to us commoners, this
would be pretty similar to you and your partner making 80 grand a year, but when you go to
buy a house, you have forms that say you make $240,000 a year.
There's a word for that.
When it comes to these types of documents being used in a bank, being used for financial
purposes like this, there's an expectation of accuracy that by what we've seen wasn't
met and by the allegations, it was intentional.
differences are not something that could be explained by a simple mistake, especially
given the pattern and the length of time that it went on.
Now this is civil, this is civil, but there have apparently been referrals made to the
IRS and the Department of Justice over this.
So on what's here, yeah, they're probably going to take a look at them.
They will probably take a look at them.
So rather than kind of summing up one case on the whiteboard, it adds two more.
I have a feeling that the feds will follow up on this.
So is this going to be the case that brings Trump down?
No.
If what has been presented thus far is accurate, which I have no reason to believe it's not,
and the Trump team doesn't have a really good reason for all of this to have occurred,
Trump's probably going to end up paying on this one a lot.
There's an expectation when it comes to Trump now that most people have, most people know
that he exaggerates, embellishes.
There's a lot of nice words that you could use for that.
That's not the word that's going to be used in court for this.
The fact that these wound up on financial documents and were used as a basis, that's
kind of a big deal.
Based on what we've seen, I'm kind of surprised that the criminal case out of New York hasn't
moved forward.
I know they say that it's still ongoing, but it seems like that would have come out before
this.
So that is a brief summary of this.
This is going to be a giant, giant pain for Trump.
More legal troubles at a time when he probably could use a break from them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}