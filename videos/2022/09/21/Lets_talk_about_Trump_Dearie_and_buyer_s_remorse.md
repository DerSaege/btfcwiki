---
title: Let's talk about Trump, Dearie, and buyer's remorse....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DO4BiM0S4xk) |
| Published | 2022/09/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's case is now in a courtroom where the judge is a former FISA court judge.
- The FISA court deals with intelligence matters and has a reputation for giving the federal government leeway.
- Trump's team doesn't want to provide evidence of declassification, possibly to preserve their defense in case of indictment.
- The judge wants evidence that the documents were declassified, but Trump's team is hesitant to provide a sworn affidavit.
- Declassification is not a critical fact for the case, but it does slow things down.
- Trump's team seems to be looking at the situation through both political and legal lenses.
- Successfully arguing that Trump intentionally held onto declassified documents could imply willful retention of national defense information.
- Trump's legal team may have buyer's remorse with the judge they suggested, as he is indicating that Trump can't have his cake and eat it too.

### Quotes

- "Declassification is not a critical fact."
- "Trump's legal team may have buyer's remorse with the judge they suggested."
- "The judge said you can't have your cake you can eat it too."

### Oneliner

Trump's case in a courtroom with a former FISA judge, reluctance on evidence, and buyer's remorse with suggested judge.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Follow the legal proceedings closely to understand the implications (implied)

### Whats missing in summary

Insights on the potential consequences of the legal strategy and the impact on Trump's defense.

### Tags

#Trump #LegalProceedings #Declassification #FISA #NationalDefense


## Transcript
Well, howdy there internet people, it's Beau again. So today we're going to talk about Trump
and the proceedings of today, and what they mean long term. If you missed it, Trump
left the world of nonsense, a world where everything will be what it isn't.
He has now found his case proceeding in a courtroom where the judge is a former FISA court judge
that Trump's team offered up as an option. For those that don't know, Deary
was on the FISA court and if you don't know what that is, a FISA court is a
court that, like its whole gig, is dealing with intelligence matters.
That's what it does. So Deary knows a lot about secrets and probably more so than
most judges. FISA courts also have a reputation as being a rubber stamp for
the intelligence community. That's not entirely accurate, but at the same time
there's a reason that reputation exists. They do tend to give the federal
government a lot of leeway. I don't know. Trump picked him. Okay, so the proceedings
today, they mainly hinged on declassification. The judge, being a judge, would like some
kind of evidence that the documents were declassified, that Trump, you know, blinked and nodded or
wiggled his nose or whatever, and magically declassified all these documents.
Trump's team doesn't want to provide that.
Their stated reason is that they want to preserve their defense in case he's eventually indicted.
There are a number of ways in which Trump's team could provide this kind of evidence.
The easiest would be a sworn affidavit.
That is apparently something they don't want to do.
I'm not going to speculate on why.
The judge has basically kind of indicated that, hey, if the documents have classification
stamps and I have absolutely no evidence to suggest that they were declassified, well,
that's the end of it.
kind of telegraphing his decision that those documents are going to be
available to the federal government very quickly. Now one of the other things
here is that Trump's team keeps referring to declassification as a
critical fact. I'd like to remind everybody it's not. It's not. It isn't a critical
fact, as far as the actual case, none of the laws require the information to be classified.
They require it to be national defense information.
And by all accounts, that's there.
Whether or not it's classified really doesn't matter in the scheme of a potential criminal
case. Nothing hinges on that, but the classification does slow things down.
Now, one thing that I see happening, and it appears that there are two different lenses
that Team Trump is looking through, one is the political and one is the legal, I would
point out that if Team Trump does successfully argue that Trump declassified
these documents and was intentionally holding on to them, remember the
classification doesn't matter, they're kind of saying he was willfully
retaining national defense information. That's a big deal. But because of Trump's
statements on his little wannabe Twitter thing, I think maybe they're
being pressured into keeping this as part of the argument because his base
existing in a closed information ecosystem, they may not understand that
it doesn't matter if they're declassified or not. They may not
understand that key part. If they were to successfully argue that he
declassified these documents and retained them, they would have to show
really good reason why and I don't think that reason exists. I think Trump's
legal team is actually correct in not wanting to nail themselves down to this
particular defense, because it's not really a good defense if there was a
potential indictment. That makes sense. Continuing to argue it though, really
doesn't. We don't exactly know what's going to happen, but based on these
proceedings, I'm willing to bet that Team Trump is having some buyer's remorse
with the judge that they suggested. The judge is very much indicating that Trump
can't have his cake and eat it too. And the judge said you can't have your cake
you can eat it too.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}