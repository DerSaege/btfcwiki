---
title: Let's talk about today in space....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-yOtFEORC0I) |
| Published | 2022/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA is conducting the Double Asteroid Redirection Test today, where a spacecraft will be slammed into an asteroid moving at 15,000 miles per hour to adjust its orbit as a proof of concept for a planetary defense system.
- The impact is scheduled for 7:14 PM Eastern, with NASA starting broadcasting on NASA TV at 6 PM.
- This test is purely a proof of concept, not in response to an immediate threat, to ensure effectiveness in the future if needed.
- The spacecraft will deploy a smaller camera bird before impact to film the event, flying within 25-50 miles of the asteroid.
- The impact and any resulting changes in the asteroid's orbit will be observable from Earth.
- Despite having about 100 years before any potential asteroid threat, not all objects are cataloged, leaving room for unknown objects in space.
- Beau mentions sending birthday wishes to one of his favorite rabbis and cheers for the Gators.
- Viewers can watch the test on NASA TV, with the impact scheduled for 7:14 PM Eastern.

### Quotes

- "Today is the big day. The Double Asteroid Redirection Test."
- "You can watch it, because prior to impact, it will deploy a smaller camera bird."
- "This is just to see if they can do it."

### Oneliner

NASA conducts the Double Asteroid Redirection Test, slamming a spacecraft into an asteroid at 15,000 mph to adjust its orbit as a planetary defense proof of concept, viewable on NASA TV at 7:14 PM Eastern.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Watch the Double Asteroid Redirection Test on NASA TV at 7:14 PM Eastern (suggested).

### Whats missing in summary

Details on the potential impact of the test and the significance of planetary defense systems.

### Tags

#Space #NASA #Asteroid #PlanetaryDefense #NASA_TV


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today is the big day.
Today, in just a few hours, actually, the DART test
will be conducted, the Double Asteroid Redirection Test.
This is the space battering ram thing.
NASA will be slamming a spacecraft
into an asteroid that is orbiting another asteroid.
The spacecraft will be moving at around 15,000 miles per hour.
And the goal is to just adjust the orbit of the asteroid
a tiny bit as a proof of concept for a planetary defense system.
So if there is an asteroid, we don't
have to rely on oil well drillers.
You can watch it, because prior to impact,
it will deploy a smaller camera bird that
will film the whole thing.
And the impact is supposed to occur at 7 14 PM Eastern.
And I want to say NASA is going to start broadcasting
on NASA TV at 6 PM.
Now, again, this is a proof of concept.
There is no threat from this asteroid.
This is just to see if they can do it.
So if the situation ever arises where they need to,
they know it will work.
This is something you want to test out ahead of time.
Theoretically, we have at least 100 years.
There's not an asteroid that is headed
towards Earth in that period.
However, it's also worth noting that they really only
have a small fraction of those objects cataloged.
So there could be stuff out there we don't know about.
The camera bird, which I want to say is of Italian manufacture,
will be flying within 25, 50 miles of the asteroid.
So you'll be able to see the impact and probably
like the plume.
And then later, they will observe from Earth
and see if the orbit of that asteroid has changed.
On top of all of that, I would like
to wish one of my favorite rabbis a happy birthday.
So go Gators.
Anyway, you can view it on NASA TV.
I'll try to find a link and put it down below.
It's at 714 PM Eastern.
It should be interesting to watch and give everybody
a little bit of a break from all of this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}