---
title: Let's talk about good news on Climate and why we got it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sZ09IhD3hIk) |
| Published | 2022/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Senate passed an amendment to the Montreal Protocols, aiming to reduce the use of HFC refrigerants.
- This amendment could potentially eliminate half a degree Celsius of warming over the next century.
- The agreement requires an 85% reduction in HFC usage over the next 15 years.
- Despite the lengthy timeline, the transition is expected to happen quicker due to economic incentives.
- The economic benefits of transitioning away from HFCs played a significant role in getting the amendment passed.
- The alignment of environmental and economic interests was key in driving this decision.
- The U.S. is poised to lead in producing climate-friendly refrigerants, driving the urgency for action.
- Beau stresses that many climate mitigation efforts involve economic gains for someone.
- In a capitalist society, profitability often drives environmental decisions.
- Understanding which industries stand to profit can be vital in pushing legislation forward.

### Quotes

- "It's kind of a big deal that this is being phased down."
- "Most climate mitigation efforts, there's money in it."
- "Industry, business interests, hopped on board because they knew it would make money for them."
- "That's how you get this stuff through."
- "They have the power coupons that environmentally conscious people generally don't have."

### Oneliner

The Senate passed an amendment to reduce HFC usage, driven by economic interests and climate concerns, showing the power of profit in environmental decisions.

### Audience

Legislators, environmentalists, activists

### On-the-ground actions from transcript

- Identify industries benefitting from environmental initiatives and reach out to collaborate (suggested)
- Advocate for legislation by partnering with businesses that stand to gain economically (implied)

### Whats missing in summary

The full transcript provides further insights into the intersection of economic interests and environmental policy, stressing the importance of industry collaboration in driving legislative change.

### Tags

#ClimateChange #Legislation #EconomicIncentives #EnvironmentalPolicy #HFCs


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about refrigerants
and an agreement in the Senate and how and why it happened.
And we have some good news on the climate front,
which is a sentence I don't get to say very often.
The Senate voted 69 to 27 to push through an amendment
to the Montreal Protocols.
Now, the agreement itself was hammered out
like back in 2016 or something, and we're way behind.
But it's getting through.
So what does it do?
It scales back use of a certain refrigerant,
commonly called HFCs.
It's a refrigerant that is widely used.
It's kind of a big deal that this is being phased down.
I don't want to say phased out.
Because this amendment, theoretically,
should eliminate about half a degree Celsius of warming
over the next 100 years.
It's kind of a big deal.
It's not phasing it out.
It's phasing it down.
They have to reduce use of it by 85% over the next 15 years.
But here's the interesting thing.
It's not going to take that long.
It won't take that long.
It won't take 15 years to get this done.
And the reason it won't take 15 years
also the reason it made it through. It's the reason it passed. The transition away
from HFCs is expected to stimulate literally billions of dollars in
economic investment in this country. That's why. Money. It's one of those
instances where environmental and economic interests align. That's why it
got through. Make no mistake about it, if there wasn't money on the line, it wouldn't
have made it through. But since that money was there, that money was there to be made,
people threw their weight behind it and got the senators to vote in favor of it. That's
what happened. Because the U.S. is just, it is already geared up to provide the next generation
of refrigerants that are more climate friendly.
So they want to finally jump on board and get this moving.
Environmentalists and people who are concerned about climate change, especially those who
are focused on getting legislation through, take note.
Most climate mitigation efforts, there's money in it.
is going to turn a buck. Somebody's going to make cash. We are in a hyper
capitalist society right now and somebody is going to turn a profit on
this. Whatever it is. Find out who it is that's going to make money. Find out if
their pockets are deep enough on one side to have a senator in the other one.
Because this shows that's how you get this stuff through. No, it's not ideal.
but it works. This is what happened here. The reason this got through is because
industry, business interests, hopped on board because they knew it would make
money for them in the long run. That's how we wound up here. That's how we
finally got this through. If you are one of those people who are involved at that
level when you're talking about legislation or campaigning for
legislation, it might be important to find out what industry, what business is
going to benefit from it and reach out to them because they have the power
coupons that environmentally conscious people generally don't have. They have
the access, they have the senators, and they can push stuff like this through.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}