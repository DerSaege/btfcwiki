---
title: Let's talk about the call coming from inside the White House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eqqTjan9Ses) |
| Published | 2022/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An advisor for the committee, Riggleman, gave an interview with 60 Minutes, revealing his analysis of communications and connections between different groups related to the events of January 6th.
- Riggleman, a former Republican congressperson endorsed by Trump, had good credentials for this work but left the Republican Party in June.
- He analyzed communications, created com graphs, and identified groups like Team Trump, state legislatures, alternate electors, rally goers, and more to establish coordination.
- Texts provided by Meadows were referred to as a roadmap to a coup attempt, resembling foreign opposition groups' communication methods.
- During the events of January 6th, someone from the White House called a person at the Capitol, identified as a rioter, indicating potential coordination.
- The committee might not have been happy about Riggleman's interview and book release, as they likely planned to disclose this information during the next hearing.
- The interview serves as a preview to the upcoming hearing, where more context and examination will be provided on the data analysis.
- The committee aims to generate public interest by releasing bits and pieces of information before the hearing, potentially implicating other individuals like a Supreme Court Justice's spouse.
- The call from the White House switchboard to a person on scene raises significant concerns and suggests possible involvement by high-ranking members of the identified groups.
- Riggleman's analysis indicates a high level of coordination among various groups related to the events of January 6th.

### Quotes

- "The rumor mill says the committee is unhappy about this interview and unhappy that he wrote a book."
- "The call from the switchboard to somebody on scene, that's pretty big."
- "There aren't a lot of ways to explain that."

### Oneliner

An advisor's analysis reveals concerning levels of coordination among various groups related to the January 6th events, potentially implicating high-ranking individuals and prompting anticipation for the upcoming hearing.

### Audience

Committee members

### On-the-ground actions from transcript

- Contact committee members to express support for thorough investigations and transparency (implied).

### Whats missing in summary

The emotional impact and tension surrounding the revelations, along with the potential consequences for those implicated, can be better understood by watching the full transcript. 

### Tags

#CommitteeInvestigation #CoordinationRevealed #January6thEvents #Transparency #PublicInterest


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're talking about the big news.
Big news.
The call is coming from inside the house kind of news.
And what it means for the committee
and where it goes from here,
and we're gonna kind of go over what was discussed.
If you missed it,
some news broke last night.
An advisor for the committee,
somebody who did some investigative work for them,
gave an interview with 60 Minutes,
person named Riggleman.
Okay, so before we get into it,
who is this person?
Ex-intelligence with an apparent specialization
in countering non-state actors.
Was also reportedly a contractor for the NSA.
His official title when working for the committee
was Senior Technical Advisor for the committee.
That is entertaining because in most communities,
the term advisor means somebody who provides advice.
In the contracting consulting world,
the term advisor means somebody who's running a team,
does all of the work,
makes everything run,
then provides a product,
and then provides advice on what to do next,
which appears to be exactly what he did.
But he's not just some ex-spook either.
He was also a Republican congressperson
who was endorsed by Trump.
So he's not ideologically opposed
to a lot of Trump's policies.
In fact, voted with him most of the time,
but there was a line that was crossed,
and that happened with a lot of people.
It's worth noting he left the Republican Party in June.
After leaving the committee in April, I think,
he reportedly did some work
with the recent unpleasantness in Ukraine and helping them.
So that's the background.
Very good credentials for this kind of work.
So what did he do?
He analyzed, and his team analyzed the communications,
broke it down, created com graphs,
and all of that stuff.
So picture it like a chart
that shows who called who and when,
but more importantly, trying to establish
whether or not there was coordination.
His team identified a few groups
that they were watching in particular.
There was Team Trump, the Trump family,
the state legislatures,
and the alternate electors,
the rally goers, people charged by DOJ,
and then the more militarized groups.
And they showed the connections between them.
Where did the information come from?
Probably a lot of places,
but the texts provided by Meadows,
he referred to as a roadmap to a coup attempt.
Didn't put a fine point on a lot of this stuff.
He said that when reading this,
it sounded a lot like foreign opposition groups
and the way they communicate.
On top of this, at some point on the 6th,
while it was happening,
somebody from the White House
called somebody who was at the Capitol,
a person being identified in the interview as a rioter.
Interesting, but what's more interesting
is how that information probably was observed.
My guess would be that this is somebody
who was linked to one of those groups described,
who they were looking at already,
and then noticed the call from the White House,
from the switchboard.
So whoever it is was probably already on their screens.
Probably somebody in the chain of command there.
So those are the key points,
but what this shows, and what the graph shows,
is, to use his term, a lot of coordination.
And yeah, that's what it looked like.
The rumor mill says the committee
is unhappy about this interview
and unhappy that he wrote a book.
And that makes sense.
My guess is they wanted to disclose all this
on Wednesday during the next hearing.
Because my understanding of this is,
when he left, he was like,
you need to follow up on these things.
And they probably did.
So I wouldn't view this as a spoiler
to the Wednesday hearing,
more of a trailer to it.
More of a little preview.
I would imagine that there's going to be
a lot of context added,
a lot of examination.
There's more that he went into
in the interview on 60 Minutes.
But most of it is going to be gone over
during the hearing.
The key parts, though, is the data analysis
and what it showed.
And what prompted this was
a couple of people asking,
is this what these kind of products look like?
Yeah, that's exactly what they look like.
It looks like what was flashed on the screen
looks exactly like what he described he did.
I have no reason to doubt this information.
So, that is where we're at.
There's definitely going to be more.
And I would imagine that there will probably be
some coming out, little bits and pieces,
on Monday and Tuesday,
because the committee has a habit of trying to
prime the public, pique curiosity,
and get them to tune in to the hearing.
With this going out,
they're probably going to talk about this
or some of the other people that were named
in the interview,
like a certain Supreme Court Justice's spouse.
And they'll use that to advertise
and get people interested in something
that it certainly seems like
they should be interested in.
The call from the switchboard
to somebody on scene,
that's pretty big.
There aren't a lot of ways to explain that.
So, we'll have to see and get the rest of the context,
find out who it is.
If it is somebody in the leadership
of any of the groups that they kind of identified,
that's going to be a very large issue
for Team Trump.
And it's going to be hard to overcome.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}