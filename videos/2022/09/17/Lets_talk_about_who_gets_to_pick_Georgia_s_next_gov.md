---
title: Let's talk about who gets to pick Georgia's next gov....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NpvIX_aiGIs) |
| Published | 2022/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Polls rely on likely voters, determined by pollsters, but motivating factors may drive unlikely voters to the polls in Georgia.
- The latest polling in Georgia suggests unlikely voters will decide the next governor, as Kemp and Abrams are neck and neck.
- With changing demographics in Georgia, unlikely voters who typically stay home may now have a significant impact on election outcomes.
- The potential for unlikely voters to sway the election is heightened by the fact that some are willing to cross party lines.
- If unlikely voters lean blue and combine with those crossing party lines, Georgia may turn blue.
- Abrams, known for mobilizing voters, could win if these unlikely voters turn out and support her campaign.

### Quotes

- "It is the unlikely voter who decides who runs that state."
- "Georgia's turning blue."
- "If the vote gets out, if those unlikely voters skew blue, she wins."

### Oneliner

Unlikely voters could decide Georgia's next governor as demographics shift, potentially turning the state blue.

### Audience

Georgia residents

### On-the-ground actions from transcript

- Mobilize unlikely voters to vote (implied)
- Support voter turnout efforts (implied)
- Encourage voter engagement and education (implied)

### Whats missing in summary

The importance of voter mobilization efforts and engaging unlikely voters in Georgia's election process.

### Tags

#Georgia #Election #Voting #Demographics #Abrams


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Georgia
and unlikely voters
and who gets to pick Georgia's next governor.
Recently, we talked about how polls
are based on likely voters.
And that's those numbers that you see most often,
that's what they're measuring.
And that the pollsters themselves
determine who the likely voters are.
They're normally pretty good.
I have a feeling that they may be off this time
simply because there are a lot of motivating factors
that are going to drive people
who are unlikely voters to the polls.
And in Georgia, the latest polling suggests
that it is really going to matter.
In fact, it says that the unlikely voters
will be choosing the next governor.
The latest Quinnipiac poll says that Kemp
has 50% of the vote
and Abrams has 48% of the vote.
Now that sounds like Kemp's ahead.
The margin of error is 2.7.
So this is, right now it's a tie.
They're running neck and neck,
which means it's the unlikely voter who is going to decide.
Those people who are going to show up
who aren't represented by this poll,
that's who gets to pick the next governor.
These numbers hold until the election.
It is the unlikely voter who decides who runs that state.
Now, there are a whole lot of people in Georgia
who are very accustomed to having their voice,
well, not mean a whole lot
because they've been outnumbered for so long.
It's changing.
The demographics of Georgia are changing.
It is becoming more liberal.
The unlikely voters in that state,
those people who typically stay home,
because it doesn't matter,
they will be the people who decide
who sits in the governor's house.
Now, one of the interesting things about this
is that with Warnock and Walker,
Walker's down six points,
six points in the same poll.
So what that shows is that there are a number of people
who are willing to cross party lines on this one.
If that is combined with those unlikely voters,
Georgia's turning blue.
So for Abrams, a person who's kind of known
for their ability to get the vote out,
if the vote gets out,
if those unlikely voters skew blue, she wins.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}