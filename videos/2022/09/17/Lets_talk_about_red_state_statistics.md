---
title: Let's talk about red state statistics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NipiCyojaGk) |
| Published | 2022/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring narratives and information consumption in response to a video on a particular topic.
- Addressing the challenge of breaking free from established narratives, even when faced with contrary evidence.
- Critiquing the misconception about Democrats allowing violent criminals on the streets.
- Pointing out the high murder rates in states that voted for Trump in 2020.
- Providing statistics that challenge the narrative of Democrat-run cities being hubs of violence and murder.
- Mentioning specific cities and states with high crime rates that voted Republican.
- Emphasizing how certain narratives are perpetuated by media outlets to influence beliefs.
- Describing how people become emotionally invested in false narratives and resist conflicting information.
- Criticizing the tendency to hold on to misinformation despite it being easily debunked.
- Concluding with a reminder not to believe everything presented by certain outlets and to think critically about information consumption.

### Quotes

- "Democrat run cities are cesspools of violence and murder."
- "It's a narrative. They repeat it often enough. You believe it."
- "You have an emotional reaction about it. You get angry because now that piece of information doesn't fit with your narrative."

### Oneliner

Beau challenges misconceptions about crime rates in Democrat-run cities and urges critical thinking in information consumption.

### Audience

Information consumers

### On-the-ground actions from transcript

- Fact-check information from media outlets (suggested)
- Challenge narratives with data and statistics (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of how narratives are constructed and perpetuated, urging viewers to critically analyze the information they consume.

### Tags

#Narratives #InformationConsumption #CrimeRates #MediaBias #CriticalThinking


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
narratives and information consumption and a response to a recent video that I got,
the one about the whole Bell thing, and how once a narrative is established, how it's hard
to break free from it, even if you are confronted with some pretty hard evidence that what they've
told you is untrue. Okay, so here's the message. You're, misspelled, such a hack. You act like
we're stupid for thinking Dems would let violent criminals out on the street, all caps. Democrat
run cities are cesspools of violence and murder. It's your soft on crime approach that causes this.
Mine, I guess. Don't quote the Constitution to me. If the founders knew what you'd do
to their country, they never would have put that in there. Wrong there. I just want to point out,
this person has a shall not be infringed banner on Twitter. Do you take the same view of the
Second Amendment as you do of the Eighth? Probably not, huh? Okay, so I'm fairly certain the founders
would have put that in there because they did, and it was kind of like a huge deal for them.
Just saying. But most importantly, let's talk about this Democrat run cities thing,
and them being cesspools of violence and murder. Is that true? I mean, I know you think it is.
I know you believe that narrative, but who told it to you? The same outlets that didn't explain
the bail law to you, right? You think they're a good source? You obviously feel some kind of way
about having been misled and then being corrected. So, I've talked about this before, but we can go
through it again. I don't want to be a hack about it. In 2020, the per capita murder rates were
40% higher in states that voted for Trump. It's a fact. You can look it up. I'll put it down below.
Eight out of ten of the highest per capita murder rates in 2020 voted Republican
in every election this century. The thing I'll link below actually will break it down and go
into cities and talk about how Kevin McCarthy's Bakersfield, I think is the name of it, is
actually worse off than Nancy Pelosi's San Francisco, which is one of those Democrat
run cities that always comes up, right? The top ten. Mississippi, Louisiana,
Kentucky, Alabama, Missouri, South Carolina, New Mexico. Finally got one that's blue.
Georgia. You know, Georgia, the bastion of the Democratic Party. Arkansas and Tennessee.
You know why they frame it the way they do?
Because it's easy. It's easy to, you know, walk outside their affiliate offices and film.
Everything you see in a major city when they show you that footage, is it any different than what
you see in the lower income areas, in rural areas? It's not. It's not.
The crime rates, the violent crime rates in particular, especially the murder rate, much higher.
It's a narrative. They repeat it often enough. You believe it. You believe it.
You believe it so much that you are willing to accept information like the idea that a state enacted a purge law
because you have come to believe this lie so much. It has become a closely held belief of yours.
So when something comes along that reinforces it, you accept it. And if somebody, even if they're a hack,
comes along and debunks that new piece of information, you have an emotional reaction about it.
You get angry because now that piece of information doesn't fit with your narrative.
The same outlets that told you that Illinois enacted that law and that everybody was going to be let out,
and all of that stuff, the same outlets that told you that are the same outlets that fed you this information.
And it is easily, easily debunked. But you'll keep believing it, right?
Because you've heard it over and over again. They repeated it to you.
I'm not the one that treats you like you're stupid.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}