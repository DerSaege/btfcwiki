---
title: Let's talk about who advised Trump's lawyer....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kX6ZFYirs3s) |
| Published | 2022/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the recent release of information from the documents case involving Trump.
- Mentions how this information was initially redacted.
- Points out that documents from the White House were stored in a location within Mar-a-Lago.
- Raises questions about who advised the lawyer regarding the location of the records.
- Speculates on the implications if Trump misled his lawyers about the location of the documents.
- Suggests that this could limit potential individuals who could provide information to the Department of Justice.
- Emphasizes the significance of the lawyer being advised by someone else.
- Notes Trump's likely efforts to identify who provided the advisement to the lawyers.
- Indicates that this information could shift blame towards Trump.
- Points out the critical nature of a document containing a photo of files spread on the floor.

### Quotes

- "This information shifts the blame more to Trump than anything else that we've seen."
- "That document that contained that photo with all of those files spread all over the floor that the Republicans found so funny, that's suddenly kind of critical evidence."

### Oneliner

Beau addresses the implications of recently released information regarding Trump's documents, pointing out potential misrepresentations and shifting blame towards Trump more prominently.

### Audience

Political observers, investigators, activists

### On-the-ground actions from transcript

- Investigate further into the documents case to understand the full implications (suggested)
- Stay informed about developments related to this story (suggested)

### Whats missing in summary

The full context and depth of analysis provided by Beau in the complete transcript.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about something that
this story actually broke a couple days ago, but it's been kind of missed with
everything that's going on, and it's really kind of important,
but because of all of the other developments that are happening,
people didn't pay as much attention to this as maybe they should have.
It's a recently released piece of information coming out of the
documents case with Trump. This was initially redacted.
F. POTUS Council 1 stated he was advised all the records that came from the
White House were stored in one location within Mar-a-Lago, the storage room, and
the boxes of records in the storage room were the remaining repository of records
from the White House. F. POTUS Council 1 further stated he was not advised there
were any records in any private office space or other location in Mar-a-Lago.
Who advised the lawyer? That's the question now.
See, initially there was a lot of speculation because the attorneys said
that they'd handed everything back. Now the information is coming out that
they were told one thing that may have been less than accurate.
The photo with all of those documents spread all over the floor,
you know, the one the Republicans kept making fun of, acting like it wasn't important.
If I'm not mistaken, that was from inside Trump's office.
If he told his lawyers to convey this message to DOJ that the only place these
documents were at was in that storage room while he had some in his office
or he moved them there after that statement was made,
that certainly helps the Department of Justice's case a lot.
It limits the number of fall guys that Trump has.
There's not a lot of people who could in theory provide information to the lawyers
that they would then repeat to the Department of Justice.
This little tidbit of information, it doesn't seem that important.
It's just that the lawyer said, hey, they were all there and didn't know about anything else.
But it's the fact that the lawyer was advised by somebody else.
Trump right now is probably trying to figure out who gave the lawyers that advisement.
And the Department of Justice probably already has a theory as to who it was.
This information shifts the blame more to Trump than anything else that we've seen.
And that document that contained that photo with all of those files spread all over the floor
that the Republicans found so funny, that's suddenly kind of critical evidence.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}