---
title: Let's talk about a possible railroad strike....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8k4JiwK7q0s) |
| Published | 2022/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- There is a potential railroad strike brewing, with a possible start on Friday, but an immediate work stoppage seems unlikely.
- Freight train strikes have a massive impact on the supply chain, much more than a UPS strike.
- The management of railroad companies may want the federal government to intervene to prevent the strike.
- Biden cannot stop the strike; it will require Congress to act.
- Some unions are pointing to staffing issues, requiring workers to be on call seven days a week, as a primary concern.
- Unions are adamant about changes in staffing and scheduling, even more so than pay.
- Beau believes that people involved in critical services like freight trains should be taken care of if shutdowns occur.
- The potential strike's impact extends beyond freight trains, affecting services like Amtrak.
- Biden and the Secretary of Labor are engaging with unions and management, but progress is slow.
- Without a resolution, engineers and conductors going on strike over staffing and scheduling could halt train operations.

### Quotes

- "If you're talking about something, and if it shuts down, the whole country stops, I mean, those people should probably be taken care of, is just my opinion."
- "I think we'll see some movement from the management side. But we're going to have to wait and see."
- "So anyway, it's just a thought. Y'all have a good day."

### Oneliner

There's a looming railroad strike with potential impacts on the supply chain, requiring Congress to act, especially on staffing and scheduling issues beyond just pay.

### Audience

Supply chain stakeholders

### On-the-ground actions from transcript

- Contact local representatives to urge them to address staffing and scheduling concerns in the railroad industry (implied)
- Stay informed about the situation and its potential impacts on transportation and supply chains (implied)

### Whats missing in summary

Insights on the potential long-term consequences of failing to address staffing and scheduling issues in the railroad industry.

### Tags

#RailroadStrike #SupplyChain #CongressAction #StaffingIssues #TransportationConcerns


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are once again going
to talk about the supply chains and what
happens if the train stops because there
is a potential railroad strike brewing.
It could start as soon as Friday,
but that seems a little unlikely.
We have talked in the past about what
happens if the trucks stop.
We talked recently about the possibility
of a UPS strike and how much that impacts the supply chain.
A freight train strike is that times 10.
It's huge.
It's a really big deal.
So the narrative is shaping up to say there's
going to be a strike on Friday.
And that is possible, but an immediate work stoppage
seems unlikely.
Now, the railroads, the management,
and because there are so many players involved here,
so many multiple unions and railroad companies,
we're just going to simplify this.
Unions and management.
Management meaning the boss, the railroad companies.
The railroad companies, they probably
would like the federal government to step in.
Now, recently, at the beginning of this week,
they kind of came out and were like, yeah,
we're already telling people who are
going to ship hazardous material or stuff that
needs to be secured, we're going to go ahead and tell them
to stop.
The unions believe that this is them putting the shippers,
the consumers, and the supply chain at risk in an effort
to pressure the federal government to step in
and make sure there isn't a strike.
Now, Biden can't stop the strike.
There's a thing called a cooling off period,
but he already used it.
Friday is the end of that period.
So it's going to take Congress to act.
I think that's what management might be kind of banking on
right before a midterm in a period where labor sentiment is
running high.
I'm not so sure that Congress is going to push that through.
Certainly not by Friday.
Now, once again, as we talked about before,
it's not always pay that is triggering this.
In this case, a couple of the unions
are pointing to staffing issues that basically
require their people to essentially be
on call seven days a week, even if they're not
scheduled to work.
Now, it's not exactly that, but that
from the worker's point of view.
That's how it's playing out for them.
So that's something that they're kind of intractable on.
They're not going to move on that issue.
The pay and stuff like that, that's one thing.
The staffing and the scheduling, that
seems to be something that they're pretty set on,
that this needs to change.
And I mean, as far as I'm concerned,
this is a lot like the UPS thing.
If you're talking about something,
and if it shuts down, the whole country stops,
I mean, those people should probably be taken care of,
is just my opinion.
Now, this is going to have impacts
beyond just freight trains.
Amtrak is already suspending some services, particularly
those that are long, the longer routes, because Amtrak,
I want to say out of the like 22,000 miles they use of track,
they only own like 700 of it.
So a strike could definitely impact them as well.
Now, Biden is talking directly to the unions
and to management, trying to get something done.
The Secretary of Labor is stepping in.
But at this point in time, there hasn't been a lot of movement.
Some of the unions have a tentative agreement.
But the engineers and conductors,
if they go on strike over staffing and scheduling issues,
the trains stop.
So that's the situation as it is.
I would like to think that a deal will be reached right
at the last moment.
With something like this, this is what normally happens.
I would think that management would understand
that if freight becomes unreliable because of staffing
issues or scheduling issues or strikes,
that shippers may find other ways of doing it.
So we're in a period of time where
we're in a period of innovation.
Older industries that have been around a long time
that rely heavily on technologies that could be,
that shippers could find a different way of doing things,
they probably want to keep their stuff up and running.
So I think we'll see some movement
from the management side.
But we're going to have to wait and see.
They may really be counting on their allies in Congress
to step in.
I don't know how likely that is, though.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}