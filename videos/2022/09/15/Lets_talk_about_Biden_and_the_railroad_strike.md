---
title: Let's talk about Biden and the railroad strike....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A0LavDRrIAI) |
| Published | 2022/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration helped avert a potential railroad strike by brokering talks between unions and railroad companies.
- Republicans in Congress wanted to use their authority to force workers to continue working to avoid the strike.
- A tentative agreement has been reached, but details are not public yet.
- Biden stated that rail workers will receive better pay, improved working conditions, and peace of mind regarding healthcare costs.
- The agreement will benefit both rail workers and companies by retaining and recruiting more workers.
- The term "tentative" indicates that the agreement will go to union membership for ratification through a vote.
- If the vote fails, there will be a cooling-off period before another vote, potentially after the midterms.
- The announcement of the agreement came late at night or early in the morning.
- The Biden administration seems proud of the agreement, which may have caught Republicans advocating to force workers off guard.
- The statement suggests that workers will be happy and railways not upset, but the details of the agreement are still unknown.

### Quotes

- "These rail workers will get better pay, improved working conditions, and peace of mind around their health care costs, all hard earned."
- "It goes on to say that the railroad companies will be able to retain and recruit more workers for an industry that will continue to be part of the backbone of the American economy for decades to come."

### Oneliner

Biden administration brokers talks to avert railroad strike, reaching a tentative agreement benefiting workers and companies, with details pending union vote.

### Audience

Workers, policymakers

### On-the-ground actions from transcript

- Support workers' rights through advocacy and engagement (implied)

### Whats missing in summary

The impact of the agreement on railroad workers and companies, and potential future developments.

### Tags

#Biden #RailroadStrike #UnionNegotiations #WorkerRights #RepublicanSenate


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Biden and railroads and the art of the deal.
The potential railroad strike has apparently been averted.
The Republicans in Congress, in the Senate in particular, were pushing to use the
laws that they have authority to use to avert a strike, basically by forcing workers to continue
working. Meanwhile, the Biden administration was engaging in incredibly long talks, brokering talks
between the unions and the railroad companies. Biden himself is said to have gotten on the calls.
A tentative agreement has been reached, and we'll get to what that means in a second.
At time of filming, details on the actual deal, they don't exist yet.
They're not public yet.
But this is the statement from Biden.
These rail workers will get better pay, improved working conditions, and peace of mind around
their health care costs, all hard earned.
It goes on to say that the railroad companies will be able to retain and recruit more workers
for an industry that will continue to be part of the backbone of the American economy for
decades to come.
It certainly sounds like the railroads will be hiring more people, which would alleviate
the scheduling concerns. But again, that's what it sounds like, and this is in fact a statement
from a politician, whether you like him or not. That is what it is. Now as far as the term tentative,
that's normal language. Once the union leaders and the companies agree on it, it goes to the
the membership for ratification. So there's going to be a vote on it. Now one
of the things that has also been released is that if the vote happens to
fail, another cooling off period will take place and that that would be
several weeks. Read that as after the midterms. I don't know who who would have
put that in there. So at least for the time being, this has been averted by the
incredibly late or early, depending on how you look at it, announcement of this.
I'm going to suggest that it's something that is going to definitely make the
workers happy and also not upset the railways too much. That's the way I'm
reading the statement, but again right now we don't actually know the details
of the of the agreement yet. But the the fact that this statement is being made
in what amounts to the middle of the night, it seems like the Biden
administration is pretty proud of it. It also seems like they may have suckered
Republicans in the Senate into coming out and advocating to force workers to
to stay on their jobs. That almost seems like it might have been intentional.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}