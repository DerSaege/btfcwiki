---
title: Let's talk about what Trump said about possible indictment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=peBvBT-Catk) |
| Published | 2022/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump might face indictment for the documents case at the bare minimum.
- The only reason Trump isn't in jail right now is because he's the former president.
- Trump's lawyers have probably explained the possibility of a future indictment to him.
- Trump anticipates a future indictment, suggesting it could cause unrest in the country.
- There are concerns about Trump intentionally stoking civil unrest or believing he is above the law.
- Trump's behavior post-election may cement him as a historical mistake by American voters.
- The importance of the committee's work in explaining what happened is emphasized.
- Media plays a significant role in informing the public without bias.
- Voter turnout, especially in the midterms, is seen as a way to diminish Trump's influence.
- Trump's legacy and influence are tied to the perception of winning.
- The rhetoric used by Trump and some Republicans is considered dangerous and could incite violence.
- Statements made by the Republican Party are criticized for normalizing potentially harmful outcomes.
- Normalizing these outcomes could backfire if Trump is indicted, leading to harsher consequences.
- Bad advice given to Trump has negative consequences for the country.
- There is a warning of bumpy months ahead due to the current political climate.

### Quotes

- "If this was anybody else and anybody else had those documents for that long, stored in that way, they'd already be in cuffs."
- "I honestly think at this point Trump is cementing himself as the worst mistake the American voters ever made."
- "People are going to get hurt and it needs to stop."
- "His brand, the thing that brought people in, is this idea of winning."
- "Trump's getting a lot of bad advice, and as has been the case a lot lately, the bad advice he's getting is carrying negative consequences for the rest of the country."

### Oneliner

Trump's possible indictment raises concerns of civil unrest, with emphasis on the importance of informing the public and voter turnout to mitigate potential negative outcomes.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Support voter education and turnout in upcoming elections to diminish Trump's influence (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's potential indictment and its implications on public safety, urging for informed voter action to counter negative consequences.

### Tags

#Trump #Indictment #CivilUnrest #VoterTurnout #PublicSafety


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Trump talking about his possible indictment.
Because it's a topic that came up recently.
And I would imagine that it's probably been on his mind a lot.
Because by this point, I would assume that his lawyers have advised him
that he very well might face indictment for the documents case at the bare minimum.
I've said it before, the only reason he isn't in jail right now
is because he's the former president of the United States.
If this was anybody else and anybody else had those documents for that long,
stored in that way, they'd already be in cuffs.
They'd already be in jail.
I feel like his lawyers have probably explained that to him.
And I think in some ways he's probably anticipating a future indictment.
He was asked about it.
And he said, I think if it happened, I think you'd have problems in this country,
the likes of which perhaps we've never seen before.
I want you to think about that statement.
Think about what this country has seen before and what he's saying.
This country has had some pretty bad things happen in its history.
But Trump being indicted, it would trigger something worse than that.
I don't think the people of the United States would stand for it.
I think they'd have big problems, big problems.
I just don't think they'd stand for it.
They will not sit still and stand for this ultimate of hoaxes,
except it's not a hoax.
The documents are there.
The documents were there.
This isn't even in debate.
There's conversations about it from Trump's team.
The reality is that this isn't a hoax.
The actual elements of what makes this a crime, they're not really in dispute.
It really boils down to Trump believing that those rules don't apply to him.
That's what it boils down to.
Now, I've had people ask about this and do basically asking,
do I think that he's intentionally trying to plant the seed of really bad things happening,
of civil unrest?
I don't know.
I can't speak to that.
He very well might be that arrogant.
He just might believe that the country will rip itself apart because he's so beloved
and people will be willing to do extreme things so he can keep sensitive defense information
in his office.
I don't know.
He might be that arrogant.
I can't speak to his intent on why he's saying these things or why other Republicans are
saying these things.
What I can say is that best case scenario, it's reckless.
It's reckless.
And it very well might drive somebody to do something bad.
I honestly think at this point Trump is cementing himself as the worst mistake the American
voters ever made.
That's how he'll go down in history, in large part because of his behavior after he lost,
after the election.
What this does, because they're also not wrong.
There are people who are under Trump's influence to the degree that they really might do something
bad.
What this does is it puts more importance on the work of the committee in explaining
what exactly happened.
It puts more importance on the work of the media in getting the information out there
and not both sides in it.
And just saying, yeah, if this was anybody else, they'd already be in jail.
They're being super nice to him.
And the work of you, the work of the voter.
Because the reality is the likelihood of things going really bad decreases if Trump-backed
candidates lose big in the midterms.
It really does.
His brand, the thing that brought people in, is this idea of winning.
You can hear it in the way they talk.
They will support things that they disagree with just to be perceived as winning over
Democrats.
If they lose big at the polls, it undermines his influence because people break free of
the idea that he is some huge winner.
I don't think that Trump understands what's happening to his legacy, so to speak.
I don't think other Republicans understand how history is going to remember them for
enabling this.
This rhetoric, it's dangerous.
People are going to get hurt and it needs to stop.
We're probably in for a bumpy few months.
I don't think that the statements that are being made are well thought out by the Republican
Party because they're viewing it as a deterrent.
If indicted, this bad thing will happen.
That's not something that's going to hold a lot of sway over DOJ.
And while they're saying this, they're normalizing the idea of those bad things for their followers.
And then if he is indicted and those bad things happen, it certainly increases the likelihood
that he's convicted.
It certainly increases the likelihood that they pursue it in the biggest way possible
and seek the maximum penalties.
Trump's getting a lot of bad advice, and as has been the case a lot lately, the bad advice
he's getting is carrying negative consequences for the rest of the country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}