---
title: Let's talk about Greenland's ice and sea levels....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DuC8uGtrq9s) |
| Published | 2022/09/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a study on Greenland's ice that is essentially "zombie ice" - ice that won't stay ice because it's cut off from the process that maintains it.
- The study predicts a conservative estimate of a 10-inch rise in sea levels solely from Greenland's ice, regardless of current actions taken to combat climate change.
- Attributes the inevitability of this sea level rise to the rise in average temperature in Greenland over the past 40 years, about 1.5 degrees per decade.
- Compares the study's prediction to Noah's estimate of a 10 to 12-inch rise in sea levels over the next 30 years.
- Explains that a 10-inch rise doesn't equate to a uniform increase everywhere, as it can lead to broader flooding in some areas while causing water levels to recede in others.
- Emphasizes the importance of understanding that we are now beyond stopping or avoiding climate change; instead, the focus is on mitigating its effects due to past inaction.
- Points out that the longer action is delayed, the more inevitable and severe the effects of climate change become, as the impacts take time to manifest.
- Suggests that studies like the one on Greenland's ice can raise awareness about the irreversible aspects of climate change and potentially encourage more people to take action to mitigate it.
- Uses the analogy of a train without brakes to illustrate the imminent melting of Greenland's ice and subsequent sea level rise.
- Encourages further research and studies conducted in a similar vein to bolster efforts in combating climate change.

### Quotes

- "There's more than 100 trillion tons of ice that won't stay ice."
- "We are past that point. We're at mitigate climate change."
- "We see that train coming but we also know it doesn't have any brakes."
- "This ice is still there at the moment but it is going to melt."
- "If more research is conducted in this fashion, we might be able to encourage more people to join the fight to mitigate it."

### Oneliner

Greenland's "zombie ice" will inevitably raise sea levels by 10 inches, showing the urgency to mitigate climate change effects as stopping it is now unrealistic.

### Audience

Climate activists and concerned citizens

### On-the-ground actions from transcript

- Join the fight to mitigate climate change by supporting research and studies that raise awareness (suggested)
- Take concrete actions in your community to reduce emissions and adapt to climate change (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the inevitability of sea level rise due to Greenland's ice melt and stresses the importance of immediate action to mitigate climate change effects.

### Tags

#ClimateChange #SeaLevelRise #GreenlandIce #Mitigation #Research


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
Greenland and ice and sea levels and how some things are just inevitable. They are going
to occur no matter what actions we take. Because there's a really interesting study and it
had a cool name in the concept so it's gotten a lot of attention. And it's about ice that
is in Greenland that is basically cut off from the process that keeps it ice. Even though
right now it's still ice, there's more than 100 trillion tons of ice that won't stay ice.
This is what they are calling zombie ice. This study was designed to provide a conservative
estimate of how much sea level rise we will see just from the ice in Greenland. And the
answer is 10 inches. 10 inches no matter what. No matter what actions are taken right now
today, no matter how we cut our emissions right now, we're going to see this. Now there's
no timeline for it, but this is going to definitely occur. And the reason it's going to occur
is because this area has risen in average temperature, I want to say 1.5 degrees per
decade for like the last 40 years. This has already been set in motion. And when you look
at it and you compare it to Noah's estimate as an example, Noah is saying 10 to 12 inches
over the next 30 years. We're going to see it. It's going to occur. Now one thing that
is really important when it comes to talking about sea level rise is understanding that
10 inches doesn't mean 10 inches. You can't walk out to the coast where you're at and
say okay in 30 years it'll be a foot higher than this because that's not how it works.
In some areas if it goes up by a foot, well at 6 inches it's cresting over into a new
area that opens up a wider area that will be flooded. So that area may become much wider,
may become a bay. And somewhere on the other side of the world you may even see water levels
recede. Now one thing that's important to note about this study is that it's new. There
hasn't been a lot of debate about it. But that's not why I'm bringing it up. The exact
prediction of 10 inches no matter what, that's going to be debated, no doubt. But the reason
I think this is notable is because it's talking about the inevitability of some of this. Because
you know we hear stop climate change, avoid climate change. That's not happening anymore.
We are past that point. We're at mitigate climate change. We're trying to lessen the
effects not avoid it altogether because that's not possible anymore. And the longer we wait
the more effects are going to be locked in. The more effects are going to become inevitable
because this stuff doesn't happen immediately. It takes time for the effects to become apparent.
And that's one of the reasons I think that this is important to note. This study is important
to note and studies in a similar fashion. Because it's talking about stuff that we can
see it. We see that train coming but we also know it doesn't have any brakes. This ice
is still there at the moment but it is going to melt. And it is going to raise our sea
levels. That's what makes this notable. And I think that if more research is conducted
in this fashion, there are more studies and those studies and that research gets publicity,
we might be able to encourage more people to join the fight to mitigate it. Anyway,
that's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}