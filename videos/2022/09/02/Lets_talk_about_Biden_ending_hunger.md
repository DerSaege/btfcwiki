---
title: Let's talk about Biden ending hunger....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Kde3fY3il4E) |
| Published | 2022/09/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration set a goal to end hunger in the United States by 2030, with a conference scheduled for September 28th.
- The inspiration for this goal comes from a conference in 1969 that laid the groundwork for systems currently in place to address hunger.
- There is pushback about the timeline, with some people feeling rushed to come up with better ideas.
- Beau encourages seizing the moment and taking advantage of the unique opportunities presented by this initiative.
- The focus is not just on feeding people but on providing healthy food to combat diet-related diseases.
- Beau points out the prevalence of cheap, unhealthy food options and stresses the importance of updating systems to prioritize nutritious options.
- Biden mentioned the impact of diet-related diseases like heart disease and diabetes on families and communities.
- The lack of updates and overhauls to existing systems for a long time suggests a need for change in how we address hunger.
- Beau views this initiative not only as a humanitarian effort but also as a strategic move by the Democratic Party, addressing a critical kitchen table issue.
- The goal to end hunger in the United States is significant and deserves more attention and support.

### Quotes

- "This is your shot. Take it."
- "It's probably time to do so."
- "This is quite literally a kitchen table issue."

### Oneliner

The Biden administration aims to end hunger in the US by 2030, drawing inspiration from a 1969 conference, with a focus on providing healthy food and updating outdated systems, seen as both a humanitarian effort and a strategic move by the Democratic Party.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Support initiatives addressing hunger (implied)
- Advocate for access to healthy food options (implied)
- Stay informed and engaged with efforts to combat hunger (implied)

### Whats missing in summary

The full transcript provides more context on the historical background and current challenges related to hunger in the United States. Viewing the entire video can offer a deeper understanding of the significance of updating systems to address food insecurity.

### Tags

#Hunger #BidenAdministration #HealthyFood #UpdateSystems #PoliticalMove


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about
one of the Biden administration's loftier goals,
something that they're actually setting as a goal.
And I don't think it's getting the coverage that it should
because it's kind of a big deal.
On September 28th, there's going to be a conference
hosted by the Biden administration.
The goal of this conference is to end hunger
in the United States by 2030.
Eight years.
That's big.
That's big.
And it doesn't seem to be getting the coverage it should.
Now, this was announced back in May,
and it draws its inspiration from a conference
that occurred back in 1969.
Three-day conference.
And at that conference, a lot of the systems
that you're familiar with today that
are used to limit hunger in the United States,
I mean, that's where they kind of originated,
at least in the federal government's view.
That's where they came from.
Another way of saying that is that a lot of the systems
in use today are a half century old.
Might be time to update them.
And that's what the Biden administration
seems to be doing.
Now, there is a little bit of pushback
because of the timeline.
A lot of people are pointing out that it seems rushed
and that they feel like they need more time to come up
with better ideas.
They had more lead up time to the 1969 conference.
OK, fine.
You had more lead up time back then.
They also didn't have instant communications.
This is a big deal.
This is an opportunity.
Get on the ball.
This is your shot.
Take it.
Now, one of the interesting things to me about it
is that it's not just about feeding people.
It's about feeding people healthy food.
You know, how many times have you seen a post on Facebook
or on Twitter that says something to the effect of,
you know, I was behind somebody with an EVT card
and all they did was buy junk food?
Yeah, because it's cheap.
That's why they bought all those frozen pizzas,
because it's cheap.
It's also not healthy.
In the talking points about this,
they're talking about getting healthy food.
Biden mentioned empty chairs at tables and kitchens
because of people who were lost to heart disease, diabetes,
or diet-oriented diseases, his term.
I mean, I think that'd be a great change.
We haven't updated these systems.
We haven't overhauled them in a very, very long time,
and it's probably time to do so.
Now, as far as looking at this through a political lens
rather than, hey, this is really good for the United States,
I think it's probably a smart move by the Democratic Party.
This is quite literally a kitchen table issue.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}