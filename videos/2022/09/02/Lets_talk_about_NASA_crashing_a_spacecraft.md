---
title: Let's talk about NASA crashing a spacecraft....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mAenSVO3E8Y) |
| Published | 2022/09/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA has a spacecraft on course to collide with an asteroid as part of the Double Asteroid Redirection Test (DART).
- The test aims to develop the first step in a planetary defense system to alter the course of asteroids heading towards Earth.
- The current mission is a proof of concept, taking place 6.8 million miles from Earth.
- The spacecraft will impact an asteroid orbiting another asteroid, with the target asteroid being 520 feet long.
- The spacecraft, moving at 15,000 miles per hour, acts as a space battering ram.
- This test is a precautionary measure in case an asteroid threat arises in the future.
- Although there is no immediate threat, it's a proactive step for planetary defense.
- The live broadcast of the test release an observation craft on September 26 at 7.14 PM Eastern.
- Viewers can watch the event live on NASA's website, YouTube, Facebook, and Twitter.
- Beau anticipates various theories and speculations arising from the test.

### Quotes

- "I cannot wait to see something smash into an asteroid at 15,000 miles an hour."
- "This is a test. We've talked about it before on the channel."

### Oneliner

NASA's spacecraft is set to collide with an asteroid as part of a planetary defense test, showcasing a proactive approach to potential future threats.

### Audience

Space enthusiasts, science lovers.

### On-the-ground actions from transcript

- Watch the live broadcast of the spacecraft impacting the asteroid on September 26 at 7.14 PM Eastern (suggested).

### Whats missing in summary

The full transcript provides more context and details about NASA's Double Asteroid Redirection Test and the significance of this test in developing a planetary defense system.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about how
NASA has a spacecraft perfectly on course
to smash into an asteroid.
But that's the point.
It's OK.
It's part of DART, the Double Asteroid Redirection Test.
If this sounds vaguely familiar, it's
because about nine months ago, I made a video talking about it.
Said it was going to take forever to get there.
It's almost been forever.
So what's happening?
The idea is to develop the first step in a planetary defense
system.
So if there ever is a situation in which an asteroid is coming
towards Earth and Bruce Willis isn't available,
they can use what is, in essence, a space battering
ram to alter the course of the asteroid
so it doesn't hit Earth.
At the current time, there is no threat.
This is a proof of concept.
This is just to kind of test it out, see if this would work,
and see if they can move the asteroid.
This test will be taking place about 6.8 million miles
from Earth.
And it is designed to hit one asteroid that
is orbiting a second asteroid.
The asteroid that is going to be impacted
is about 520 feet long, which is definitely
big enough to cause a problem if it was to hit Earth.
The spacecraft, the battering ram,
is going to be moving at around 15,000 miles per hour.
So this is the first step.
This is one of those things that you definitely
want to try out before you have to actually use it.
And while there has been no evidence whatsoever,
no statement, anything like that,
suggesting that an asteroid is coming towards Earth
at any point in the future, relatively close,
at some point in time, it will happen.
It's on a long enough timeline that is
something that will occur.
It's happened before.
It will happen again.
This is them testing out, getting proof of concept
before they actually need it.
Now, one of the really interesting things about this
is that if you want to, you can watch it live.
On September 26 at 7.14 PM Eastern,
it will be broadcast.
The battering ram will kind of release an observation craft
that will send a signal back so we can all see what happens.
You'll be able to watch it on NASA's website, on YouTube,
Facebook, Twitter, pretty much all of the social media
except for truth.
So when this comes up, this is what's going on.
I'm certain that at some point, this
will end up in an information silo
and generate all kind of theories.
This is a test.
We've talked about it before on the channel.
And honestly, I cannot wait to see something
smash into an asteroid at 15,000 miles an hour.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}