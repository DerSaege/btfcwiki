---
title: Let's talk about Ian and being overtaken by events....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HS9bPiswYQ8) |
| Published | 2022/09/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hurricane Ian was headed towards Florida with an uncertain forecast track.
- The hurricane's predicted path shifted eastward, likely passing over the peninsula.
- Beau decided to go to South Florida to help with relief efforts or organize supplies.
- Due to the hurricane risk, Beau scheduled videos until Sunday as a precaution.
- Beau may not have access to upload videos if there is breaking news.
- Content will continue for the week, but updates may be delayed.
- People in South Florida may not be able to watch updates but should know help is coming.
- Beau encourages everyone to have contingency plans and supplies ready for emergencies.
- Even 12 hours before filming, those hit by the hurricane thought they were safe.
- Beau reminds viewers to always be prepared for unexpected events.

### Quotes

- "Y'all just hang in there because based on what I have seen so far, it's probably not going to be a little one."
- "Make sure you have your contingency plans in place. Make sure you have supplies and you're ready because something like this can happen at any time."

### Oneliner

Beau reminds viewers of Hurricane Ian's uncertain path towards Florida, urging preparedness and offering help to South Florida.

### Audience

Florida Residents

### On-the-ground actions from transcript

- Organize supplies for hurricane relief efforts in South Florida (implied)
- Ensure contingency plans are in place and necessary supplies are ready for emergencies (implied)

### Whats missing in summary

The emotional impact of being overtaken by events and the importance of community support during emergencies.

### Tags

#HurricaneIan #Florida #EmergencyPreparedness #CommunitySupport #ReliefEfforts


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about Hurricane Ian
and contingency plans and being overtaken by events. As I'm sure a lot of you know,
there is or was, depending on the time you all see this, a hurricane headed towards Florida.
The forecast track for the hurricane was less than specific about where it was going to go.
So up in the panhandle, a lot of us got ready. At time of filming, the track, the predicted
path it was going to take, has shifted further and further and further east. At this point,
it looks like it's going to go over the peninsula. And because it's not following the path it would
to come here, it's probably going to be pretty strong. So if you are watching this, I have been
overtaken by events. I have decided to go down to South Florida and help with relief efforts,
or I'm organizing supplies from here, or I'm driving them back and forth. Something has occurred
that will keep me from my normal updates and my normal routine. Because we were anticipating
the hurricane coming here, or at least considered it a possibility and figured we'd be out of power
for a while, there are videos scheduled out until Sunday, I think. I think like two a day.
Now I have no idea when Sunday is in relation to when you're watching this, but you have videos
until then. I will take a camera and some equipment with me wherever I go to do whatever it is I'm
doing. But there's no guarantee that I will have access to anything to upload it. So short version
is if there is some breaking news from this point forward that I don't cover, it's not that I'm
ignoring it, it's that, well, I'm busy. I'm busy doing something else, or I can't upload the video
that I made. Any of that's possible. So you have some content coming, at least for the rest of the
week. I will try to update y'all somehow as things progress. If you are down there in South Florida,
you probably can't watch this, but hopefully you at least get the feeling that there are people who
are going to come. Y'all just hang in there because based on what I have seen so far, it's probably
not going to be a, it's probably not going to be a little one, but there will be people working
to get y'all what y'all need. So y'all hang in there. For those outside of this area, this is one
of those reminders. Make sure you have your contingency plans in place. Make sure you have
supplies and you're ready because something like this can happen at any time. I would say as
recently as 12 hours before filming this, the people who got hit right now, they thought they
were in the clear. So make sure you have the stuff you need. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}