# All videos from September, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-09-30: Let's talk about the GOP's grand midterm student debt strategy.... (<a href="https://youtube.com/watch?v=6GkFnZBN-sQ">watch</a> || <a href="/videos/2022/09/30/Lets_talk_about_the_GOP_s_grand_midterm_student_debt_strategy">transcript &amp; editable summary</a>)

The Republican Party plans to challenge student debt relief weeks before the midterms, potentially impacting millions of Americans' access to financial relief.

</summary>

"They're literally going to try to stop tens of millions of Americans from getting $10,000 or $20,000 worth of student debt relief right before the election."
"I have a lot of questions about who the Democratic Party paid inside the Republican Party to come up with this plan."

### AI summary (High error rate! Edit errors on video page)

The Republican Party plans to challenge the Biden administration's program for student debt relief by using the court system.
They aim to disrupt the Biden administration's policy of providing up to $20,000 in student debt relief to 40 million Americans.
The GOP's strategy is to reach into the pockets of millions of Americans just weeks before the midterms.
Beau questions the GOP's decision to potentially reduce voter turnout among Democrats by challenging student debt relief.
He points out that Republicans may be operating in a closed information ecosystem, leading to misconceptions about the popularity of their move.
Beau anticipates that voters might blame Republicans if they sue to block the Biden administration's policy on student debt relief.
He expresses disbelief at the GOP's plan to hinder millions of Americans from receiving substantial debt relief right before the election.

Actions:

for voters, democrats, republicans,
Contact your representatives to express support for student debt relief (implied).
Stay informed about political strategies impacting student debt relief (implied).
</details>
<details>
<summary>
2022-09-30: Let's talk about midterm races to watch for a civics teacher.... (<a href="https://youtube.com/watch?v=7fJSrS8TaHY">watch</a> || <a href="/videos/2022/09/30/Lets_talk_about_midterm_races_to_watch_for_a_civics_teacher">transcript &amp; editable summary</a>)

Beau provides a structured approach for civics teachers to teach students about midterm races, focusing on key House, gubernatorial, and Senate races to offer diverse learning opportunities and insights into election dynamics.

</summary>

"For the Senate, I'd give them a bunch."
"It gives you different views of the different tactics."
"It should be a fun learning experience."
"Anyway, it's just a thought."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Providing insights on civics and key races to monitor during the midterms.
Suggesting focusing on two House, two gubernatorial, and multiple Senate races for a comprehensive learning experience.
Recommending North Dakota and Colorado House races for unique teaching opportunities.
Advising looking into Florida and Maryland gubernatorial races for their significance.
Listing a range of Senate races to analyze the potential control shifts in the Senate.
Breaking down the paths to victory for both the Republican and Democratic Parties in the Senate races.
Pointing out the simplicity in tracking Senate control compared to the House.
Mentioning the idea of examining gerrymandered districts without upsetting elected officials.
Encouraging observation of races with early voting dynamics to teach about evolving election results.
Suggesting engaging students by discussing various issues and possibly holding mock votes on selected races.

Actions:

for civics teachers,
Analyze and track key House, gubernatorial, and Senate races for educational purposes (suggested).
Engage students in discussing various issues related to elections and encourage them to follow selected races closely (suggested).
</details>
<details>
<summary>
2022-09-30: Let's talk about Ian and being overtaken by events.... (<a href="https://youtube.com/watch?v=HS9bPiswYQ8">watch</a> || <a href="/videos/2022/09/30/Lets_talk_about_Ian_and_being_overtaken_by_events">transcript &amp; editable summary</a>)

Beau reminds viewers of Hurricane Ian's uncertain path towards Florida, urging preparedness and offering help to South Florida.

</summary>

"Y'all just hang in there because based on what I have seen so far, it's probably not going to be a little one."
"Make sure you have your contingency plans in place. Make sure you have supplies and you're ready because something like this can happen at any time."

### AI summary (High error rate! Edit errors on video page)

Hurricane Ian was headed towards Florida with an uncertain forecast track.
The hurricane's predicted path shifted eastward, likely passing over the peninsula.
Beau decided to go to South Florida to help with relief efforts or organize supplies.
Due to the hurricane risk, Beau scheduled videos until Sunday as a precaution.
Beau may not have access to upload videos if there is breaking news.
Content will continue for the week, but updates may be delayed.
People in South Florida may not be able to watch updates but should know help is coming.
Beau encourages everyone to have contingency plans and supplies ready for emergencies.
Even 12 hours before filming, those hit by the hurricane thought they were safe.
Beau reminds viewers to always be prepared for unexpected events.

Actions:

for florida residents,
Organize supplies for hurricane relief efforts in South Florida (implied)
Ensure contingency plans are in place and necessary supplies are ready for emergencies (implied)
</details>
<details>
<summary>
2022-09-29: Let's talk about the Senate's Electoral Count Act bill.... (<a href="https://youtube.com/watch?v=zqsgVUh1q_0">watch</a> || <a href="/videos/2022/09/29/Lets_talk_about_the_Senate_s_Electoral_Count_Act_bill">transcript &amp; editable summary</a>)

Beau explains the Electoral Count Act update in the Senate, notes key players' stances, and urges people to vote based on their senator's position post-election.

</summary>

"Because of timing, this probably won't be resolved until after the election."
"If they don't actually support you having a voice, they probably shouldn't be your representative."
"This is a fundamental piece of American democracy, so maybe it should be a deciding factor when it comes to your vote."

### AI summary (High error rate! Edit errors on video page)

Explains the Electoral Count Act update in the Senate to clarify the vice president's role and prevent misinterpretation.
Notes Ted Cruz as the sole Republican on the committee to vote against the Act, while Mitch McConnell endorsed it to prevent chaos.
Mentions the Act having 11 Republican co-sponsors, potentially overcoming filibuster challenges.
Anticipates resolution after the election due to timing constraints, likely by the lame duck Senate.
Encourages people to know where their senator stands on the issue and suggests voting based on their position.

Actions:

for american voters,
Know where your senator stands on the Electoral Count Act (suggested).
Vote based on your senator's position on the Act (suggested).
</details>
<details>
<summary>
2022-09-29: Let's talk about a philosophical look at drafts.... (<a href="https://youtube.com/watch?v=y0SrTA4SDb4">watch</a> || <a href="/videos/2022/09/29/Lets_talk_about_a_philosophical_look_at_drafts">transcript &amp; editable summary</a>)

Beau addresses the philosophical and ethical implications of foreign policy and conscription in Russia, advocating for the right move to help those seeking to avoid conscription.

</summary>

"It's unconscionable for somebody nowhere near the fighting to say, you pick up a gun and go fight for the state."
"If things get bad enough, won't they reach a point where they'll fight against Putin?"
"Allow them to leave."
"Support of the upper middle class in Moscow and St. Petersburg is integral to the war effort."
"The right move is to help those who want to avoid conscription."

### AI summary (High error rate! Edit errors on video page)

Addresses a philosophical question related to foreign policy and conscription in Russia.
Criticizes the idea of a draft on principle and philosophy.
Raises concerns about entitled rich people sending poor individuals to war without consent.
Contemplates the consequences of allowing Russians trying to leave to face the aftermath.
Questions whether forcing individuals to deal with the consequences may change their perspectives.
Examines the strategic value versus philosophical implications of letting Russians leave.
Analyzes how a draft is enforced through penalties to ensure compliance.
Considers the impact of social and criminal penalties in enforcing a draft.
Advocates for allowing individuals to leave from a philosophical standpoint.
Suggests helping those without means leave rather than forcing them to suffer or fight.

Actions:

for policy analysts, activists,
Assist individuals trying to leave Russia to avoid conscription (exemplified)
Support initiatives that provide resources for those without means to leave conflict zones (exemplified)
</details>
<details>
<summary>
2022-09-29: Let's talk about Hurricane Ian and helping from where you are.... (<a href="https://youtube.com/watch?v=nFs0xPo9lQQ">watch</a> || <a href="/videos/2022/09/29/Lets_talk_about_Hurricane_Ian_and_helping_from_where_you_are">transcript &amp; editable summary</a>)

Beau addresses the need for aid following Hurricane Ian, sharing lessons learned and listing hard-to-find but vital items for distribution, while also discussing future relief efforts through the channel.

</summary>

"Chainsaws and generators."
"Food of all kinds."
"Gift cards are incredibly useful."
"Battery-powered fans."

### AI summary (High error rate! Edit errors on video page)

Addressing the need for aid for those impacted by Hurricane Ian.
Sharing lessons learned from previous relief efforts after Hurricane Michael.
Listing items that were hard to obtain but incredibly useful during distribution.
Chainsaws, generators, CPAP machines, phone batteries, and sleeping bags among the needed items.
The importance of easy-to-prepare food like cereal, Chef Boyardee, and oatmeal.
Mentioning the economic hardship faced by people post-disaster due to job loss.
The usefulness of gift cards to offset living expenses.
Specific items like over-the-counter meds, masks, gloves, cleaning supplies, and specialized baby formula were hard to find.
Battery-powered fans are necessary as electricity may take time to be restored.
Uncertainty about where to send aid as local networks are not set up yet.
Plans to get involved in relief efforts through the channel and accepting donations for future live streams.
Encouraging mutual aid networks, church groups, and offices to send needed items.

Actions:

for communities, aid organizations.,
Organize a collection drive for chainsaws, generators, CPAP machines, phone batteries, sleeping bags, and other listed items (suggested).
Prepare and send easy-to-prepare food like cereal, Chef Boyardee, and oatmeal (suggested).
Gather gift cards to help offset living expenses post-disaster (suggested).
Collect over-the-counter meds, masks, gloves, cleaning supplies, specialized baby formula, and battery-powered fans for donation (suggested).
Await updates from local networks on where to send aid (implied).
</details>
<details>
<summary>
2022-09-28: Let's talk about who hit Nordstream.... (<a href="https://youtube.com/watch?v=T49_4Z1L3-4">watch</a> || <a href="/videos/2022/09/28/Lets_talk_about_who_hit_Nordstream">transcript &amp; editable summary</a>)

Nord Stream pipeline sabotage triggers global speculation on motives, urging cautious consideration of diverse theories until concrete evidence emerges.

</summary>

"Date these opinions right now. Date these theories."
"Don't set yourself up to go ahead and start believing one until there's real evidence."
"The answer to the question is, we don't know yet."

### AI summary (High error rate! Edit errors on video page)

Nord Stream 1 and 2 pipelines have faced intentional difficulties, with officials from Norway, Sweden, and Denmark suggesting deliberate sabotage.
Motives for the pipeline interference include Russia testing capabilities and leveraging sanctions, countries seeking to limit Russia's influence, and entities looking to benefit from the disruption.
Various entities, including countries in the region and external actors, could be responsible, but the true perpetrator remains unknown.
Initial reporting indicates intentional disruption, but Beau advises against firm conclusions until concrete evidence emerges.
Beau humorously suggests whimsical theories involving fictional characters like the Little Mermaid or Cobra, underscoring the lack of definitive information.
The mystery surrounding the pipeline attack requires patience and open-mindedness to avoid prematurely adopting unverified theories.

Actions:

for global citizens,
Monitor developments in the Nord Stream pipeline situation (implied)
</details>
<details>
<summary>
2022-09-28: Let's talk about students in Virginia.... (<a href="https://youtube.com/watch?v=XnH0LaXwmpA">watch</a> || <a href="/videos/2022/09/28/Lets_talk_about_students_in_Virginia">transcript &amp; editable summary</a>)

Students in Virginia walk out in protest against LGBTQ targeting, challenging Republican scapegoating and demonstrating civic engagement.

</summary>

"They're setting the example for the rest of the country."
"For them, those are their friends. Those are people they see every single day."
"Those students that walked out, those aren't small crowds."
"May God grant you the serenity to accept the things you cannot change because on a long enough timeline, we win."
"School was not the place for such activities."

### AI summary (High error rate! Edit errors on video page)

Students in Virginia walked out of class in protest due to the governor's targeting of LGBTQ individuals within the student body.
A message prompted Beau to address the student walkouts, criticizing the notion that school should only focus on traditional activities like studying and conforming.
Republican strategists are called out for scapegoating and targeting LGBTQ individuals, viewing them as mythical creatures to pander to a bigoted base.
Beau points out that for the students, LGBTQ individuals are not mythical but friends, with a significant portion of Gen Z being LGBTQ.
The students' demonstration of civic engagement through walkouts is praised, with Beau noting that about half of them will be eligible to vote in 2024.
Beau challenges the Republican Party's tactics, stating that the students fighting for their friends will ultimately prevail.
He stresses the importance of not penalizing students for engaging in advocacy and setting an example for the rest of the country.

Actions:

for students, lgbtq community, activists,
Support LGBTQ students and friends by standing up against discrimination and bigotry (implied).
Encourage and participate in civic engagement activities and demonstrations in your community (implied).
Educate others on the importance of standing up for marginalized groups and promoting equality (implied).
</details>
<details>
<summary>
2022-09-28: Let's talk about a Trump legal team shakeup.... (<a href="https://youtube.com/watch?v=oXEZTWc4Sc4">watch</a> || <a href="/videos/2022/09/28/Lets_talk_about_a_Trump_legal_team_shakeup">transcript &amp; editable summary</a>)

Trump's legal team sees a shakeup as high-end lawyer Kyes is reportedly paid $3 million but then benched, sparking speculation and uncertainty.

</summary>

"Trump's legal team saw a shakeup recently, with a high-end Florida lawyer named Kyes reportedly being paid $3 million in advance."
"The uncertainty around Kyes' role in Trump's legal team fuels speculation and leaves the actual situation unclear."
"Either way, this is probably one of those things that you're going to see this material again."

### AI summary (High error rate! Edit errors on video page)

Trump's legal team saw a shakeup recently, with a high-end Florida lawyer named Kyes reportedly being paid $3 million in advance.
Initially thought to be on the Espionage Act case, Kyes has now been benched according to CNN, leading to speculation about his new role.
Potential cases for Kyes include the January 6th grand jury in DC or the New York case, but using a Florida heavy hitter in New York seems odd.
Speculation arises about Kyes' ethical stance, with insinuations that he may not be willing to participate in Trump's legal tactics.
CNN reports Kyes has been taken off the lead on the Espionage Act case, while Team Trump insists his role remains unchanged.
Team Trump refutes CNN's reporting, hinting at a discrepancy in perspectives.
The uncertainty around Kyes' role in Trump's legal team fuels speculation and leaves the actual situation unclear.
The outcome of this legal team shakeup remains to be seen, with implications that this development may resurface later on.
Beau concludes by leaving the situation open-ended, suggesting that time will reveal the truth behind the reported changes.
The shakeup in Trump's legal team and Kyes' altered role spark curiosity and debate among observers.

Actions:

for political analysts, legal enthusiasts,
Monitor developments in legal teams (implied)
Stay informed about political and legal updates (implied)
</details>
<details>
<summary>
2022-09-28: Let's talk about Putin, Russia, and mobilization.... (<a href="https://youtube.com/watch?v=BZu4q6Cwg2c">watch</a> || <a href="/videos/2022/09/28/Lets_talk_about_Putin_Russia_and_mobilization">transcript &amp; editable summary</a>)

Supreme Leader Putin's strategic missteps, resistance to conscription, and Western debates on accepting fleeing individuals contribute to potential destabilization in Russia.

</summary>

"Sending a bunch of incredibly spoiled, entitled people who will probably not be on the front means that they're going to be around the Ukrainians they're occupying."
"It's bad for the economy. It creates brain drain. It creates upset mamichkas."
"I understand the desire to say, no, y'all caused this. Y'all supported it. You have to deal with it."

### AI summary (High error rate! Edit errors on video page)

Supreme Leader Putin promoted himself to theater commander, leading to potential strategic mistakes in the war effort.
Units near Kherson were told not to withdraw, indicating a possibly untenable position for the Russian military.
An attack on a recruiting station in Russia is linked to resistance against conscription.
There is a debate in Western countries about accepting people wanting to leave Russia.
Suggestions to allow individuals to leave Russia are met with some resistance, but exemptions could be made.
Keeping potential troops off the battlefield is a smart move, both strategically and economically.
Most individuals with means are unlikely to be conscripted due to historical patterns seen in imperial powers.
Encouraging those wanting to flee mobilization could decrease morale and destabilize Putin's position.
The goal of Western sanctions and pressure on Russia was to destabilize Putin as a leader.
The departure of individuals trying to flee could have economic repercussions, contributing to the intended destabilization.

Actions:

for global citizens,
Facilitate the departure of individuals wanting to flee mobilization (implied)
</details>
<details>
<summary>
2022-09-27: Let's talk about women's rights, republicans, polls, and reality.... (<a href="https://youtube.com/watch?v=TbrNh5s3DoQ">watch</a> || <a href="/videos/2022/09/27/Lets_talk_about_women_s_rights_republicans_polls_and_reality">transcript &amp; editable summary</a>)

Beau delves into polling insights on women's rights, urging the Democratic Party to focus on reaching the persuadable 39% instead of those denying reality.

</summary>

"You are never going to reach these people."
"There is no objective reality in which a Republican-controlled Congress leads to more rights and freedoms for women."
"39% of people are up for grabs."

### AI summary (High error rate! Edit errors on video page)

Beau delves into women's rights, freedoms, polling insights, and the implications for the Democratic Party.
A CBS News poll revealed that 68% believe rights and freedoms are at stake in the midterms.
When asked about the impact of Republican control on women's rights, 43% said fewer rights and freedoms, 39% said no change, and 18% said more rights and freedoms.
Beau questions the 18% who believe women will have more rights under a Republican-controlled Congress, deeming it a denial of reality.
He stresses the importance of focusing on the 39% who believe things won't change, as they are potentially persuadable by the Democratic Party.
Beau criticizes the Republican Party's stance on women's rights, noting their patriarchal culture and limitations on women's freedoms.
He suggests that Democratic efforts should target the 39% who don't expect a change in women's rights and freedoms under Republican control.
Beau asserts that there is no scenario where a Republican-controlled Congress enhances women's rights, given their historical positions and rhetoric.
He advocates for using poll insights to shape policy decisions and refine Democratic messaging to appeal to the persuadable 39%.
Beau concludes by urging the Democratic Party to focus on reaching out to the undecided 39% to influence their voting decisions.

Actions:

for democratic activists and strategists,
Reach out to the persuadable 39% with tailored messaging and policy proposals (implied)
Advocate for women's rights and freedoms within local communities (implied)
</details>
<details>
<summary>
2022-09-27: Let's talk about a Cuban referendum and party and policy.... (<a href="https://youtube.com/watch?v=qYTOR5XmlJU">watch</a> || <a href="/videos/2022/09/27/Lets_talk_about_a_Cuban_referendum_and_party_and_policy">transcript &amp; editable summary</a>)

Examining how party allegiance can overshadow supporting policies, using Cuba's referendum as a case study, Beau urges individuals to think independently about their beliefs rather than following party lines blindly.

</summary>

"It was that habit of putting party over policy."
"Don't listen to the talking heads. Think about the world you want to leave behind."
"Maybe it's not really the pro-freedom party."

### AI summary (High error rate! Edit errors on video page)

Examines how party allegiance can overshadow supporting policies in one's own country.
Cuba had a referendum on a new family code, including same-sex marriage and adoption rights.
74.1% voter turnout with two-thirds in favor of the new code.
Those opposed to the government claimed only 47% of eligible voters voted yes.
Government supported expanding protections, while anti-government groups opposed it out of habit.
The opposition movement framed themselves as against rights for vulnerable groups.
Emphasizes the issue of putting party over policy to cast oneself as pro-freedom.
Notes the dynamics of social conservatism and evangelical involvement in Cuba's opposition.
Draws parallels with the U.S., where parties claim to support freedom but may undermine rights.
Urges individuals to think independently about their beliefs and not blindly follow party lines.

Actions:

for voters, political activists, community members,
Question your beliefs and ideologies, independent of party influence (suggested)
Advocate for policies that support marginalized groups in your community (implied)
</details>
<details>
<summary>
2022-09-27: Let's talk about Texas and Paxton out for a little run.... (<a href="https://youtube.com/watch?v=Tqd3go5tPc4">watch</a> || <a href="/videos/2022/09/27/Lets_talk_about_Texas_and_Paxton_out_for_a_little_run">transcript &amp; editable summary</a>)

Attorney General Ken Paxton ran from a process server, raising concerns within the Republican Party about legal accountability.

</summary>

"Attorney General Ken Paxton of Texas literally ran away from a process server trying to serve him a subpoena."
"The incident involving Paxton has raised concerns within the Republican Party regarding legal actions taken against its members."
"The federal judge overseeing the case is likely to be troubled by Paxton's avoidance of the legal proceedings."

### AI summary (High error rate! Edit errors on video page)

Attorney General Ken Paxton of Texas, facing legal troubles, ran away from a process server trying to serve him a subpoena.
The subpoena was related to a federal court hearing involving nonprofits suing over Texans receiving family planning services outside the state.
Paxton's wife assisted in his avoidance of the process server by driving him away in a truck.
Despite multiple attempts by the process server to deliver the documents, Paxton evaded receiving them.
Paxton later defended his actions on Twitter, citing concerns for his family's safety.
The incident has raised concerns within the Republican Party regarding legal actions taken against its members.
The behavior exhibited by Paxton, including multiple instances of running away, is seen as dramatic and unusual for an attorney general.
There are doubts about Paxton's reasons for avoiding the process server and his perceived level of threat.
The federal judge overseeing the case is likely to be troubled by Paxton's avoidance of the legal proceedings.
The situation portrays a high-ranking official trying to evade accountability in a legal matter.

Actions:

for legal observers, political analysts,
Contact local representatives to express concerns about public officials evading legal accountability. (implied)
Support organizations advocating for transparent and accountable leadership in government. (implied)
</details>
<details>
<summary>
2022-09-26: Let's talk about today in space.... (<a href="https://youtube.com/watch?v=-yOtFEORC0I">watch</a> || <a href="/videos/2022/09/26/Lets_talk_about_today_in_space">transcript &amp; editable summary</a>)

NASA conducts the Double Asteroid Redirection Test, slamming a spacecraft into an asteroid at 15,000 mph to adjust its orbit as a planetary defense proof of concept, viewable on NASA TV at 7:14 PM Eastern.

</summary>

"Today is the big day. The Double Asteroid Redirection Test."
"You can watch it, because prior to impact, it will deploy a smaller camera bird."
"This is just to see if they can do it."

### AI summary (High error rate! Edit errors on video page)

NASA is conducting the Double Asteroid Redirection Test today, where a spacecraft will be slammed into an asteroid moving at 15,000 miles per hour to adjust its orbit as a proof of concept for a planetary defense system.
The impact is scheduled for 7:14 PM Eastern, with NASA starting broadcasting on NASA TV at 6 PM.
This test is purely a proof of concept, not in response to an immediate threat, to ensure effectiveness in the future if needed.
The spacecraft will deploy a smaller camera bird before impact to film the event, flying within 25-50 miles of the asteroid.
The impact and any resulting changes in the asteroid's orbit will be observable from Earth.
Despite having about 100 years before any potential asteroid threat, not all objects are cataloged, leaving room for unknown objects in space.
Beau mentions sending birthday wishes to one of his favorite rabbis and cheers for the Gators.
Viewers can watch the test on NASA TV, with the impact scheduled for 7:14 PM Eastern.

Actions:

for space enthusiasts,
Watch the Double Asteroid Redirection Test on NASA TV at 7:14 PM Eastern (suggested).
</details>
<details>
<summary>
2022-09-26: Let's talk about the call coming from inside the White House.... (<a href="https://youtube.com/watch?v=eqqTjan9Ses">watch</a> || <a href="/videos/2022/09/26/Lets_talk_about_the_call_coming_from_inside_the_White_House">transcript &amp; editable summary</a>)

An advisor's analysis reveals concerning levels of coordination among various groups related to the January 6th events, potentially implicating high-ranking individuals and prompting anticipation for the upcoming hearing.

</summary>

"The rumor mill says the committee is unhappy about this interview and unhappy that he wrote a book."
"The call from the switchboard to somebody on scene, that's pretty big."
"There aren't a lot of ways to explain that."

### AI summary (High error rate! Edit errors on video page)

An advisor for the committee, Riggleman, gave an interview with 60 Minutes, revealing his analysis of communications and connections between different groups related to the events of January 6th.
Riggleman, a former Republican congressperson endorsed by Trump, had good credentials for this work but left the Republican Party in June.
He analyzed communications, created com graphs, and identified groups like Team Trump, state legislatures, alternate electors, rally goers, and more to establish coordination.
Texts provided by Meadows were referred to as a roadmap to a coup attempt, resembling foreign opposition groups' communication methods.
During the events of January 6th, someone from the White House called a person at the Capitol, identified as a rioter, indicating potential coordination.
The committee might not have been happy about Riggleman's interview and book release, as they likely planned to disclose this information during the next hearing.
The interview serves as a preview to the upcoming hearing, where more context and examination will be provided on the data analysis.
The committee aims to generate public interest by releasing bits and pieces of information before the hearing, potentially implicating other individuals like a Supreme Court Justice's spouse.
The call from the White House switchboard to a person on scene raises significant concerns and suggests possible involvement by high-ranking members of the identified groups.
Riggleman's analysis indicates a high level of coordination among various groups related to the events of January 6th.

Actions:

for committee members,
Contact committee members to express support for thorough investigations and transparency (implied).
</details>
<details>
<summary>
2022-09-26: Let's talk about good news on Climate and why we got it.... (<a href="https://youtube.com/watch?v=sZ09IhD3hIk">watch</a> || <a href="/videos/2022/09/26/Lets_talk_about_good_news_on_Climate_and_why_we_got_it">transcript &amp; editable summary</a>)

The Senate passed an amendment to reduce HFC usage, driven by economic interests and climate concerns, showing the power of profit in environmental decisions.

</summary>

"It's kind of a big deal that this is being phased down."
"Most climate mitigation efforts, there's money in it."
"Industry, business interests, hopped on board because they knew it would make money for them."
"That's how you get this stuff through."
"They have the power coupons that environmentally conscious people generally don't have."

### AI summary (High error rate! Edit errors on video page)

The Senate passed an amendment to the Montreal Protocols, aiming to reduce the use of HFC refrigerants.
This amendment could potentially eliminate half a degree Celsius of warming over the next century.
The agreement requires an 85% reduction in HFC usage over the next 15 years.
Despite the lengthy timeline, the transition is expected to happen quicker due to economic incentives.
The economic benefits of transitioning away from HFCs played a significant role in getting the amendment passed.
The alignment of environmental and economic interests was key in driving this decision.
The U.S. is poised to lead in producing climate-friendly refrigerants, driving the urgency for action.
Beau stresses that many climate mitigation efforts involve economic gains for someone.
In a capitalist society, profitability often drives environmental decisions.
Understanding which industries stand to profit can be vital in pushing legislation forward.

Actions:

for legislators, environmentalists, activists,
Identify industries benefitting from environmental initiatives and reach out to collaborate (suggested)
Advocate for legislation by partnering with businesses that stand to gain economically (implied)
</details>
<details>
<summary>
2022-09-25: Let's talk about understanding the other side.... (<a href="https://youtube.com/watch?v=GCA0a4tygfM">watch</a> || <a href="/videos/2022/09/25/Lets_talk_about_understanding_the_other_side">transcript &amp; editable summary</a>)

Beau talks about understanding conflicting perspectives, mobilization against war, and the human tendency to act only when directly impacted, urging empathy and reflection on past behaviors.

</summary>

"People care about the pebble in their shoe, not the rock slide 1,000 miles away."
"I don't want anybody else to be thrown into a cauldron of war and strife for some rich guy's ego."
"This move, it's a bad move. It's bad all the way around."
"But it also doesn't mean it's uncommon."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talks about trying to understand and relate to the other side's perspective, even if it's conflicting.
Mentions how people in Russia only mobilized against the war when they faced the risk of being drafted.
Acknowledges that the same behavior can be seen in the United States, where people may not protest against injustices until it directly affects them.
Expresses sympathy for regular people being forced into war due to the actions of those in power.
Points out the increased likelihood of atrocities when scared and untrained individuals are forced into conflict.
Encourages empathy by putting oneself in others' shoes and recognizing one's own past actions or inactions in similar situations.

Actions:

for global citizens,
Put yourself in others' shoes and empathize with their perspectives (implied).
Recognize past behaviors or inactions and strive to be better (implied).
</details>
<details>
<summary>
2022-09-25: Let's talk about cops, cell phones, and foreign policy.... (<a href="https://youtube.com/watch?v=rIL18lKQ-8o">watch</a> || <a href="/videos/2022/09/25/Lets_talk_about_cops_cell_phones_and_foreign_policy">transcript &amp; editable summary</a>)

Beau explains why he separates analysis of foreign policy realities from ideal solutions, stressing the need for a wake-up call on the cold truth, devoid of notions of justice or morality, while drawing parallels between the impact of cell phone footage on police accountability and the potential for real-time footage to reshape perceptions of war and foreign policy.

</summary>

"Don't bring silly notions like justice or morality into this, because that doesn't have anything to do what we're talking about."
"We're getting our cell phones. Those people who want a different foreign policy, we're getting that tool."
"I am not always right. Human, flawed, make mistakes."

### AI summary (High error rate! Edit errors on video page)

Explains why he separates videos on what's happening from how it could be better in foreign policy.
Stresses the importance of understanding the cold reality of foreign policy without notions of justice or morality.
Compares the need for change in foreign policy to the drive for police accountability before cell phones were widespread.
Draws parallels between the impact of cell phone footage on police accountability and the potential for real-time footage to change perceptions of war and foreign policy.
Emphasizes that the availability of real-time footage from conflicts like Ukraine is shaping public perception and eliminating glorified views of war.
Argues that major global events like climate change necessitate a shift towards more cooperative foreign policies.
Points out that most countries engaging in international affairs exhibit similar behavior to the US.
Clarifies that he is not always right, acknowledging his fallibility and urging against blind faith in his analysis.

Actions:

for activists, policy makers,
Watch and share real-time footage from conflicts to raise awareness and shape perceptions (implied).
Advocate for more cooperative foreign policies in light of global challenges (implied).
</details>
<details>
<summary>
2022-09-25: Let's talk about The Democratic party's small donors.... (<a href="https://youtube.com/watch?v=9L6zS1zuAhc">watch</a> || <a href="/videos/2022/09/25/Lets_talk_about_The_Democratic_party_s_small_donors">transcript &amp; editable summary</a>)

Beau tackles Democratic Party finances, advocating for strategic support in heavily red areas and a cohesive national strategy to further progressive policies.

</summary>

"Your donation isn't wasted, assuming it goes to advertising."
"I think that getting those ideas out and planting those seeds, even in areas that are heavily red, I think it's a good idea."
"The fight doesn't stop after Election Day."
"Support the one that's in the margin of error."
"I think that's the route to a strong, left-leaning Democratic Party."

### AI summary (High error rate! Edit errors on video page)

Talks about the Democratic Party's finances and the impact of small donors.
Raises the question of supporting Democrats in heavily red areas.
Mentions the importance of advertising and messaging in such areas.
Suggests that donations to Democrats in tough areas aren't wasted, as they can influence policies and candidate moderation.
Criticizes the Democratic Party for lacking a cohesive national strategy.
Advocates for national ad buys to spread Democratic ideas and policies.
Emphasizes the long-term impact of planting seeds of Democratic ideas in red areas.
Advises supporting candidates polling within the margin of error close to election time.
Points out the dynamic nature of political situations and the need to adapt support accordingly.
Expresses concerns about the Democratic Party's messaging and strategy.

Actions:

for political activists and donors,
Support Democratic candidates in heavily red areas by donating to fund advertising (implied).
Advocate for a cohesive national strategy within the Democratic Party to further progressive policies (implied).
Get involved in local campaigns to support candidates within the margin of error close to election time (implied).
</details>
<details>
<summary>
2022-09-24: Let's talk about whether the Democratic Party's tactic worked.... (<a href="https://youtube.com/watch?v=EuWTZ17kH-I">watch</a> || <a href="/videos/2022/09/24/Lets_talk_about_whether_the_Democratic_Party_s_tactic_worked">transcript &amp; editable summary</a>)

The Democratic Party's tactic of boosting far-right candidates in Republican primaries proved successful, leading to significant leads for Democratic nominees in multiple states.

</summary>

"It has definitely paid off there."
"Their plan was to ignore the more openly authoritarian, the Trumpists and support candidates that were a little bit more polished."
"They probably would have got away with it too, if it weren't for those meddling dims."

### AI summary (High error rate! Edit errors on video page)

The Democratic Party used an unusual tactic during the Republican primaries by spending tens of millions of dollars to support far-right candidates to improve the chances of the Democratic nominee in the general election.
In Maryland, the Democratic Party supported a candidate named Cox, described as a "Q whack job," against Democratic nominee Wes Moore, a veteran and Rhodes Scholar.
Despite initial skepticism, Moore is currently leading by more than 20 points in Maryland, showing the success of the strategy.
Similar tactics were used in New Hampshire and Illinois, resulting in Democratic candidates being ahead in the polls by double digits.
Michigan and Nevada are also part of this strategy, with Democrats having the edge, although the races are more competitive.
Republicans planned to support candidates who were more polished and less openly authoritarian, hoping to push them forward with voter support.
The Republicans' plan was disrupted by the Democratic Party's tactic of boosting far-right candidates in the primaries.
The success of the Democratic strategy has left Republicans unhappy with the outcome.
Overall, the tactic of supporting far-right candidates in Republican primaries to benefit Democratic nominees has proven effective in several states.

Actions:

for political strategists,
Support and get involved in local Democratic Party efforts to strategize and support candidates in primary elections (suggested)
Stay informed about political strategies and tactics being used in primary elections (suggested)
</details>
<details>
<summary>
2022-09-24: Let's talk about Trump by the numbers.... (<a href="https://youtube.com/watch?v=zVnyDa_OoyY">watch</a> || <a href="/videos/2022/09/24/Lets_talk_about_Trump_by_the_numbers">transcript &amp; editable summary</a>)

Trump's influence continues to loom large over the Republican Party, potentially impacting their performance in the upcoming midterms.

</summary>

"Trump is the Republican Party."
"The Republican Party may have a problem with turnout."
"They had the numbers to shake Trump off. They just didn't have the courage."
"They're linked to Trumpism. And lots of Republicans are waking up to how bad that really is."
"The Republican Party, they're in trouble."

### AI summary (High error rate! Edit errors on video page)

Provides an overview of Trump's current standing within the Republican Party and among voters.
Shares polling data showing low favorability ratings for Trump among registered voters.
Points out the dilemma faced by the Republican Party in the upcoming midterms due to Trump's influence.
Explains that Trump's presence is still felt within the Republican Party, despite not being on the ballot.
Analyzes how Trumpist candidates might impact Republican turnout in the elections.
Emphasizes the challenge faced by Republican leadership for failing to distance themselves from Trump.
Criticizes the Republican leadership for not taking the necessary steps to address the Trumpist influence within the party.
Notes the potential consequences of the Republican Party's association with Trump in the general election.
Warns about the possible negative impact of Trump's influence on Republican candidates endorsed by him.
Concludes by suggesting that the Republican Party's failure to address Trump's influence could lead to significant problems in the upcoming elections.

Actions:

for voters, republican leadership,
Mobilize voters to make informed decisions in upcoming elections (implied)
Encourage Republican leadership to address Trumpist influence within the party (implied)
Educate voters on the implications of candidates endorsed by Trump (implied)
</details>
<details>
<summary>
2022-09-24: Let's talk about 60% of Miami and the future.... (<a href="https://youtube.com/watch?v=sl__MoKgnvw">watch</a> || <a href="/videos/2022/09/24/Lets_talk_about_60_of_Miami_and_the_future">transcript &amp; editable summary</a>)

Beau talks about the urgent need for deep systemic change to address the impending climate change crisis, with 60% of Miami expected to be underwater by 2060, leading to 1.6 million internally displaced people.

</summary>

"We have real problems on the horizon. They're big and it's going to take more than talking points."
"Ignoring it will make it worse because we won't make the changes that are necessary."
"This problem isn't going to be solved by ignoring it."
"There's a lot of problems on the horizon. They won't be solved by talking points."
"We know what's happening. We know what's causing it."

### AI summary (High error rate! Edit errors on video page)

Talks about the future over the next 40 years and the impact of climate change on internal displacement in the US.
Mentions an example of an island off the coast of Louisiana disappearing due to climate change, leading to people being relocated.
Shares a shocking statistic that by 2060, 60% of Miami is expected to be underwater, causing 1.6 million internally displaced people just from that area.
Expresses concern about the lack of real action and deep systemic change needed to address the impending crisis.
Observes the mistaken belief of showmanship for leadership, pointing out the need for genuine solutions.
Stresses the urgency of the situation and the need to prioritize climate change in elections due to running out of time.
Talks about the reluctance to accept internally displaced Americans as refugees, indicating a need for a shift in perspective on a global scale.
Points out that ignoring the problem will only exacerbate it and that action is necessary to address the looming crisis.

Actions:

for climate activists, policymakers, citizens,
Contact local representatives to prioritize climate change action in policies and elections (suggested)
Join or support organizations advocating for climate change mitigation and support for internally displaced people (suggested)
</details>
<details>
<summary>
2022-09-23: Let's talk about the Electoral Count Act of 1887 being clarified.... (<a href="https://youtube.com/watch?v=f3h_oieEzV4">watch</a> || <a href="/videos/2022/09/23/Lets_talk_about_the_Electoral_Count_Act_of_1887_being_clarified">transcript &amp; editable summary</a>)

The Electoral Count Act of 1887 is being revised to prevent potential coup attempts, with clarifications on objections and the ceremonial role of the Vice President, but lack of overwhelming support is disheartening.

</summary>

"The fact that the legislative body of the United States is having to explicitly say the vice president can't just throw out the votes and do what they want, that's appalling."
"People who claim to respect a constitutional republic, a representative democracy, the general ideas that this country was founded on, this should have breezed through with full support."
"There's nothing controversial in this."

### AI summary (High error rate! Edit errors on video page)

The Electoral Count Act of 1887 has come under scrutiny due to a misreading that suggested the vice president could disregard votes.
Both the House and the Senate are taking steps to revise the act to prevent potential coup attempts.
The House has passed a version that requires one-third of House members to sign off on objections to certifying electors, among other clarifications.
The revised act clarifies the ceremonial role of the Vice President and requires governors to transmit electors chosen by popular vote.
Despite being straightforward clarifications, only nine Republicans in the House supported the revision, while others may be reserving the right to support a coup in the future.
The Senate is also working on a comparable bill with support from 10 Republicans, which is likely to pass.
The necessity for such clarifications is concerning, as it should have been common knowledge that the vice president couldn't unilaterally discard votes.
The lack of overwhelming support for this revision from those who claim to respect the country's foundational principles is disheartening.

Actions:

for legislative watchers,
Contact your representatives to express support for clarifying and revising the Electoral Count Act (exemplified)
Stay informed on the progress of the comparable Senate bill and advocate for its passage (exemplified)
</details>
<details>
<summary>
2022-09-23: Let's talk about how Game of Thrones can change the world.... (<a href="https://youtube.com/watch?v=3Lg9QVjU6tE">watch</a> || <a href="/videos/2022/09/23/Lets_talk_about_how_Game_of_Thrones_can_change_the_world">transcript &amp; editable summary</a>)

Beau shares insights on how fiction, including fantasy books like Harry Potter, can inspire and guide individuals towards building a better world through immersive storytelling and world-building experiences.

</summary>

"Fiction is this unique thing because it doesn't matter what the topic is. If the fiction is any good at all, it's going to have an impact on you."
"Sure, these are not books that people perceive as being incredibly deep, so if that's what inspires you, go for it."
"You say information can get you to fact, but fiction can get you to truth."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of fiction and how stories like Lord of the Rings, Game of Thrones, and Harry Potter can teach us about building a better world.
An individual shares their journey of transitioning from reading fantasy books like Harry Potter to feeling the need to catch up on more informative reading material for community development.
The comparison between authors and activists is made, stating they both share the job of envisioning and bringing a world to life through different mediums - pen and paper for authors and actions for activists.
Beau suggests that immersive, world-building books like 1984, Lord of the Rings, Game of Thrones, and Harry Potter could be inspiring for someone interested in community development.
Fiction, regardless of the topic, can have a profound impact on individuals and alter the way they perceive the world, even through small realizations gained in a fantasy setting.

Actions:

for students, aspiring community developers,
Read immersive and world-building books like 1984, Lord of the Rings, Game of Thrones, and Harry Potter to gain inspiration for community development (suggested).
Utilize audiobooks through apps like LibriVox to access a wide range of books amidst a busy schedule (suggested).
</details>
<details>
<summary>
2022-09-23: Let's talk about a Philadelphia cop, Time, Distance, and Cover.... (<a href="https://youtube.com/watch?v=2Fo-ELYK2Pg">watch</a> || <a href="/videos/2022/09/23/Lets_talk_about_a_Philadelphia_cop_Time_Distance_and_Cover">transcript &amp; editable summary</a>)

Beau stresses the importance of police officers following basic principles like time, distance, and cover to prevent unnecessary shootings and save lives.

</summary>

"Time, distance, and cover."
"Those rules, those tactics, those best policies, whatever your department calls them, they're there for a reason. They work."
"One of them, one way, saves lives. One of them ends up with you in a cage."

### AI summary (High error rate! Edit errors on video page)

Recounts a 2017 incident in Philadelphia involving a 25-year-old man, Dennis Plowden, who was shot by Officer Eric Rook.
Rook fired at Plowden within six seconds of arriving on the scene, hitting Plowden in the hand and head. Plowden was unarmed.
Officer Rook was recently found guilty of voluntary manslaughter, becoming the third cop charged with similar offences by District Attorney Larry Krasner.
Rook claimed he was scared because he didn't have cover unlike the other officers who did.
Beau stresses the importance of time, distance, and cover in police tactics to prevent unnecessary shootings.
Beau argues that if these basic principles were followed, Plowden might still be alive.
Officers often receive training on best practices but may disregard them when out on the streets based on the guidance of their training officer.
Beau points out the discrepancy between training and real-world practices that can lead to fatal consequences.
The sentencing for Officer Rook is scheduled for November 17th, with Beau expressing uncertainty about the potential outcome.
Beau concludes by urging for adherence to proper training protocols to avoid tragic incidents like the one involving Dennis Plowden.

Actions:

for police officers, law enforcement.,
Attend or follow the sentencing of Officer Eric Rook on November 17th (suggested).
Advocate for proper training adherence and accountability in law enforcement practices (implied).
</details>
<details>
<summary>
2022-09-22: Let's talk about the DISCLOSE Act.... (<a href="https://youtube.com/watch?v=fCNjeW4XmNc">watch</a> || <a href="/videos/2022/09/22/Lets_talk_about_the_DISCLOSE_Act">transcript &amp; editable summary</a>)

Beau explains the significance of the DISCLOSE Act in shedding light on political funding to empower voters and restore faith in democracy and the judicial system, but questions its effectiveness in reshaping politics.

</summary>

"Is this really worth putting some time and effort into?"
"Democracy, having a representative democracy, a republic like we're supposed to have, it's advanced citizenship."
"You know, that may have some impact."
"The reason we got here is part corruption and part because a whole lot of people don't pay attention."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of knowing where money in politics is coming from to help voters see through funding sources.
Talks about the DISCLOSE Act, which aims to shine a light on dark money in politics.
Mentions that the DISCLOSE Act has bipartisan support historically but is currently mainly supported by Democrats.
Outlines provisions of the DISCLOSE Act, such as disclosing donations over $10,000 to Super PACs and banning foreign entities from contributing.
Notes that the top five donors must be listed at the end of TV ads and donors influencing judicial nominees must be disclosed.
Raises the question of whether it's worth investing time and effort into this Act, considering $2.6 billion was spent this way in 2020.
Mentions that the Senate may vote on the DISCLOSE Act soon to restore faith in electoral politics and the judicial system.
Expresses uncertainty about how effective the Act will be, especially in reshaping politics and influencing voters.
Recommends the website Open Secrets as a tool to track who funds elected representatives and how their votes may be influenced by donations.
Emphasizes that democracy requires advanced citizenship, where citizens need to pay attention and make informed choices.

Actions:

for american voters,
Contact your senators to express support for the DISCLOSE Act (implied)
Use Open Secrets website to track donations to elected representatives (implied)
</details>
<details>
<summary>
2022-09-22: Let's talk about Trump, the 11th Circuit, and documents.... (<a href="https://youtube.com/watch?v=DkikDVVL_pw">watch</a> || <a href="/videos/2022/09/22/Lets_talk_about_Trump_the_11th_Circuit_and_documents">transcript &amp; editable summary</a>)

Late-breaking news on 11th Circuit's decision favors DOJ in using classified documents, raising questions on Trump's intentions and legal strategy.

</summary>

"The desire to keep them after there was a clear indication that there was an issue, that is what this is really going to focus on."
"The classification markings really don't matter because that's not what the laws hinge on."
"It seems as though the only legal strategy they have here is to delay as much as possible."

### AI summary (High error rate! Edit errors on video page)

Late-breaking news regarding the 11th Circuit's decision to side with the Department of Justice on using documents with classification markings.
Decision is about the search at Mar-a-Lago, not cases in Georgia, New York, or D.C.
The 3-0 decision by the 11th Circuit was unanimous, with two of the three judges being Trump appointees.
The judges' decision was critical, questioning why former President Trump needed the 100 documents with classification markings.
The argument that declassifying the documents wouldn't change their content or make them personal is emphasized.
The focus is on why Trump wanted to retain these documents despite clear indications of issues.
Department of Justice is now allowed to proceed with the documents, likely to move quickly to avoid delays.
Other documents will go through special master process, but those with classification markings are seen as a more serious issue.
The seriousness of the situation will depend on why Trump had these documents and his intentions.
Former President's legal strategy appears to be delaying tactics in hopes of Republican return to power to influence the judicial branch.

Actions:

for legal analysts, political commentators,
Monitor and analyze the ongoing legal proceedings (implied)
Stay informed about developments in the case (implied)
</details>
<details>
<summary>
2022-09-22: Let's talk about Russia, mobilization, reservists, and plane tickets.... (<a href="https://youtube.com/watch?v=2Rz9UFjcqjk">watch</a> || <a href="/videos/2022/09/22/Lets_talk_about_Russia_mobilization_reservists_and_plane_tickets">transcript &amp; editable summary</a>)

Putin's order for a partial mobilization of Russian reservists reveals cracks in the facade of domestic tranquility and leads to protests, economic instability, and a rush for plane tickets out of the country.

</summary>

"This is not something that is likely to turn the tide there, despite the numbers involved, because it's quantity and quality type of thing."
"At the end of this, what you have is another rich man sending a whole bunch of poor boys off to fight his war."
"The war is lost. The political objectives of the war were lost as soon as other countries decided it was better for them to join NATO."
"The cracks in the facade are showing."
"The greatest chance of making it for a Russian reservist is to immediately, upon arrival, break away from their unit and turn themselves into the Ukrainians."

### AI summary (High error rate! Edit errors on video page)

Putin ordered a partial mobilization of around 300,000 Russian reservists, who are not equivalent to American reservists but amateurs forced into military service.
Russian reservists are being sent to face combat-hardened Ukrainian military, which is likely to end poorly due to their lack of training, equipment, and morale.
Demonstrations and protests have broken out across Russia against Putin's decision, showing cracks in the facade of domestic tranquility.
Efforts to insulate the civilian population and maintain economic stability in Russia are failing, leading to domestic turmoil.
The price of plane tickets out of Russia has skyrocketed as people try to flee being forced into a conflict they have no desire to partake in.
The best chance for a Russian reservist is to break away from their unit and turn themselves into the Ukrainians upon arrival.
The war in Ukraine is deemed lost, with the political objectives failing and the conflict being about imperialism and ego rather than ideals worth fighting for.
Continued protests, desertion among reservists, and low morale are expected consequences of Putin's decision for a partial mobilization.
A full mobilization may not significantly change things on the battlefield and may result in unnecessary waste of lives and resources.
The Ukrainian military's ability to disrupt movements behind the lines may prevent many reservists from even reaching the front lines.

Actions:

for activists, peace advocates, humanitarians,
Join protests against Putin's decision for a partial mobilization (exemplified).
Support efforts to aid Russian reservists who may seek to desert and avoid forced combat (exemplified).
Provide assistance to those trying to leave Russia to avoid involvement in the conflict (exemplified).
</details>
<details>
<summary>
2022-09-21: Let's talk about what Russia's referendum means in Ukraine.... (<a href="https://youtube.com/watch?v=qIA1o_9KslI">watch</a> || <a href="/videos/2022/09/21/Lets_talk_about_what_Russia_s_referendum_means_in_Ukraine">transcript &amp; editable summary</a>)

Russia's push for a referendum in Ukraine's occupied areas is a propaganda stunt to justify imperialism and sacrifice more Russian lives.

</summary>

"This is propaganda stunt."
"They're attempting to seize the land through force of arms. It's imperialism."
"It is a propaganda stunt designed to allow the leadership of Russia to throw more Russian lives away."

### AI summary (High error rate! Edit errors on video page)

Russia is pushing for a referendum in Ukraine's occupied areas to potentially join Russia, reflecting a belief that they may be pushed out based on the current situation.
The referendum is a propaganda stunt since many participating areas are beyond Ukrainian lines, making the outcome predetermined and invalid.
The goal is to use a successful referendum as propaganda to mobilize Russian people for a forced draft, sending them to fight.
This move by Russia is an attempt at imperialism, seizing land through force and framing it as defending the homeland.
Despite international dismissal of the referendum's legal consequences, domestically in Russia, it creates a facade of defending the homeland and justifies sending young Russians to fight.
The Ukrainian military is still making gains, leading Russia to believe they can't muster a fight against them, though they could potentially hold onto gained territory if they regrouped.
The referendum is perceived as a way for Russian leadership to sacrifice more Russian lives, even though it's unlikely to be accepted by anyone due to lack of control in the areas.

Actions:

for foreign policy analysts,
Mobilize international support against Russia's actions (implied)
Support organizations providing aid to affected individuals in Ukraine (implied)
</details>
<details>
<summary>
2022-09-21: Let's talk about Trump, New York, and $250 million.... (<a href="https://youtube.com/watch?v=ctAJVubDoso">watch</a> || <a href="/videos/2022/09/21/Lets_talk_about_Trump_New_York_and_250_million">transcript &amp; editable summary</a>)

Attorney General of New York files a lawsuit against Trump and others for a 10-year pattern of false financial statements, potentially leading to significant consequences for Trump.

</summary>

"If that happens, they don't work in New York anymore. It's kind of a big deal."
"There's a word for that."
"This is going to be a giant, giant pain for Trump."

### AI summary (High error rate! Edit errors on video page)

Attorney General James of New York filed a lawsuit against Trump and others, seeking $250 million in relief and requesting sanctions including barring certain individuals from being directors and canceling the Trump Organization's corporate certificate.
The lawsuit alleges a 10-year pattern of false financial statements aiming to boost Trump's net worth annually, mentioning a couple hundred false or misleading statements.
Examples include valuing Trump's Wall Street building at $524 million in 2010, a huge jump from $200 million, and overstating the size of Trump's apartment to inflate its value to $327 million.
These misrepresentations were not minor errors but intentional, according to the allegations, affecting documents used for financial purposes by lenders and insurers.
While this is a civil case, referrals have been made to the IRS and Department of Justice, potentially leading to further investigation.
Despite Trump's tendency to exaggerate, this case involving financial documents presents a more serious issue that might result in significant consequences for him.
Beau believes that although this case won't bring Trump down completely, it will likely lead to significant financial penalties.
The ongoing legal issues add to Trump's troubles and could potentially escalate into a criminal case in New York.

Actions:

for activists, legal experts,
Contact local authorities or legal organizations to stay informed about the developments in this case (suggested).
Join advocacy groups working on transparency and accountability in financial matters (implied).
</details>
<details>
<summary>
2022-09-21: Let's talk about Trump, Dearie, and buyer's remorse.... (<a href="https://youtube.com/watch?v=DO4BiM0S4xk">watch</a> || <a href="/videos/2022/09/21/Lets_talk_about_Trump_Dearie_and_buyer_s_remorse">transcript &amp; editable summary</a>)

Trump's case in a courtroom with a former FISA judge, reluctance on evidence, and buyer's remorse with suggested judge.

</summary>

"Declassification is not a critical fact."
"Trump's legal team may have buyer's remorse with the judge they suggested."
"The judge said you can't have your cake you can eat it too."

### AI summary (High error rate! Edit errors on video page)

Trump's case is now in a courtroom where the judge is a former FISA court judge.
The FISA court deals with intelligence matters and has a reputation for giving the federal government leeway.
Trump's team doesn't want to provide evidence of declassification, possibly to preserve their defense in case of indictment.
The judge wants evidence that the documents were declassified, but Trump's team is hesitant to provide a sworn affidavit.
Declassification is not a critical fact for the case, but it does slow things down.
Trump's team seems to be looking at the situation through both political and legal lenses.
Successfully arguing that Trump intentionally held onto declassified documents could imply willful retention of national defense information.
Trump's legal team may have buyer's remorse with the judge they suggested, as he is indicating that Trump can't have his cake and eat it too.

Actions:

for legal analysts, political commentators,
Follow the legal proceedings closely to understand the implications (implied)
</details>
<details>
<summary>
2022-09-21: Let's talk about Banned Books Week 2022.... (<a href="https://youtube.com/watch?v=H_IpG0UyYdA">watch</a> || <a href="/videos/2022/09/21/Lets_talk_about_Banned_Books_Week_2022">transcript &amp; editable summary</a>)

Beau talks about Banned Books Week, revealing the impact of book bans on authors and students while questioning the motives behind silencing certain voices.

</summary>

"Just remember while you're reading that, that bad books are the best books."
"Those books, those books they're trying to remove, they want to avoid the questions."
"Parents are making a choice to keep their children ignorant."
"The worst thing you can do for an idea you support is to present it and defend it poorly."
"If your idea is indefensible, well, it's easy for them to poke holes in it and that ideology collapses."

### AI summary (High error rate! Edit errors on video page)

Introduces Banned Books Week, focusing on books that some in power don't want people to read.
Mentions the PEN America report covering bans from July 2021 to June 2022.
Reveals that during this period, there were 2,532 instances of book bans impacting 5,049 schools and over 4 million students.
Points out Texas, Florida, and Tennessee as the worst offenders in book bans.
Notes that these bans are often linked to a few conservative groups, impacting the free speech of many authors and illustrators.
Shows that authors of color and LGBTQ authors are often targeted in these bans.
Questions why certain groups are trying to silence these voices, suggesting it's due to an inability to defend their ideology.
Believes that promoting curiosity through books challenges parents who prefer ignorance for their children.
Suggests that the root cause of banning these books is to uphold an ideology that can't withstand questioning.
Encourages reading the report and reminds that while reading, "bad books are the best books."

Actions:

for book enthusiasts, activists,
Read the PEN America report on book bans (suggested)
Celebrate Banned Books Week by reading a banned book (implied)
</details>
<details>
<summary>
2022-09-20: Let's talk about a stark warning from an Arizona republican.... (<a href="https://youtube.com/watch?v=e5g2L2pw49M">watch</a> || <a href="/videos/2022/09/20/Lets_talk_about_a_stark_warning_from_an_Arizona_republican">transcript &amp; editable summary</a>)

Beau warns of the Republican Party's push for the independent state legislature theory, risking democracy and constitutional principles, urging vigilance from the American people.

</summary>

"Welcome to fascism."
"It's real. This drive is real."
"There is nothing to actually suggest this is a valid theory under the Constitution."
"It's something the American people need to pay attention to."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the direction of the Republican Party and the independent state legislature theory.
Rusty Bowers, a long-time Republican, spoke out against a bill that aimed to give state legislatures power over electoral votes.
The bill could allow states to override the popular vote and assign electoral votes to a different candidate.
Bowers successfully halted the bill with obstruction techniques, but he warns that it could resurface.
If such legislation lands on the desk of a Republican governor willing to sign it, Bowers warns of descending into fascism.
The independent state legislature theory challenges the core principles of a constitutional republic and representative democracy.
Beau stresses the importance of paying attention to this theory and the internal strife within the Republican Party regarding it.
Despite lacking constitutional validity, a significant faction within the Republican Party supports this theory.
Beau signals that further exploration of this topic will be needed in future videos.
He concludes by urging people to stay vigilant and prepared for the potential consequences.

Actions:

for americans,
Pay attention to legislation proposed by your state legislature and its potential impact (implied)
Stay informed about political developments and theories challenging democratic principles (implied)
Engage in civil discourse and raise awareness about threats to democracy within your community (implied)
</details>
<details>
<summary>
2022-09-20: Let's talk about US information operations being audited.... (<a href="https://youtube.com/watch?v=UT9cBOuaAsI">watch</a> || <a href="/videos/2022/09/20/Lets_talk_about_US_information_operations_being_audited">transcript &amp; editable summary</a>)

Beau clarifies misconceptions about a Department of Defense audit on information operations, focusing on evaluating and enhancing effectiveness rather than ceasing operations.

</summary>

"They're not going to surrender the information space to the opposition."
"The idea that the civilian leadership of the Department of Defense is about to tell them to stop doing this, that's not a thing."
"If what was uncovered is representative of the whole, they're not doing well, and they will need to adjust policy."
"Knowing is half the battle."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the topic of information operations and spy activities, sparked by an audit seeking disclosure of ongoing information operations by the Department of Defense.
The audit was initiated by the Undersecretary of Defense for Policy, requesting a full accounting of all information operations being conducted.
Information operations involve spreading information, whether true or false, to achieve specific objectives, with average people inadvertently becoming impacted by these campaigns.
Concerns arose when two firms identified a U.S.-based information operation network promoting pro-Western narratives in countries like China, Russia, Afghanistan, and Iran.
There is a misconception that this audit will result in halting U.S. information operations, but Beau clarifies that it is more about evaluating and improving their effectiveness rather than discontinuing them entirely.
The audit may lead to the establishment of basic guidelines or guardrails for information operations to enhance their impact and reach.
Beau points out that the primary concern may not be the content of the information operation but rather its lack of success and effectiveness, as evidenced by low follower counts and poor engagement rates.
The U.S. government is unlikely to completely cease information operations but may adjust policies based on the audit's findings.
The audit aims to gauge the effectiveness of information operations and potentially enhance their impact rather than terminate them.
Beau concludes by suggesting that the audit is more about assessing effectiveness and adjusting policies, as knowledge plays a significant role in this realm.

Actions:

for information consumers,
Analyze and critically think about the information you consume, especially on social media platforms (suggested).
Stay informed about information operations and their potential impacts on society (suggested).
</details>
<details>
<summary>
2022-09-20: Let's talk about Trump getting exactly what he asked for.... (<a href="https://youtube.com/watch?v=1HMuLRhxDdE">watch</a> || <a href="/videos/2022/09/20/Lets_talk_about_Trump_getting_exactly_what_he_asked_for">transcript &amp; editable summary</a>)

Trump's legal team faces challenges regarding declassified documents as the judge seeks specifics, potentially leading to immediate disclosure or the need for specific arguments.

</summary>

"Their position is that they want to save that in the event of an indictment."
"It's a Schrodinger's defense, I guess."
"But this is kind of what Trump legal team is trying to say."

### AI summary (High error rate! Edit errors on video page)

Trump's legal team wanted a special master and a specific judge, and they got both.
The judge, Judge Deary, wants the process to be done quickly, likely due to their experience on the FISA court.
The judge is seeking specifics on Trump's declassification efforts, which the legal team is hesitant to provide.
Without evidence of declassification, privileged documents automatically go to the government.
Trump's social media statements contradict his legal team's filings, leading to a potential collision of arguments.
The special master may face challenges in deeming documents privileged without clear evidence.
Laws regarding national defense information do not require classification, making the declassification argument puzzling.
The legal team's defense strategy regarding declassified documents is compared to a "Schrodinger's defense."
Judge Deary may not be likely to accept the defense's arguments for withholding documents.
Ultimately, the situation may result in immediate document disclosure or the legal team providing specific arguments to filter out documents temporarily.

Actions:

for legal analysts, political commentators,
Contact legal experts for insights on the implications of declassification in legal cases (suggested).
Join or support organizations advocating for transparency in legal proceedings (implied).
</details>
<details>
<summary>
2022-09-19: Let's talk about credit cards, codes, and guns.... (<a href="https://youtube.com/watch?v=7UjP2KlEpWE">watch</a> || <a href="/videos/2022/09/19/Lets_talk_about_credit_cards_codes_and_guns">transcript &amp; editable summary</a>)

Beau breaks down the new gun industry merchant code, debunking fears of surveillance and tracking while questioning its actual utility for law enforcement and gun control efforts.

</summary>

"There's no new information that's really being gathered."
"The fact that guns didn't exist in one of these already is what's weird."
"It's not going to be a super useful tool."
"It's a talking point coming out before elections."
"It's not going to be Big Brother watching you."

### AI summary (High error rate! Edit errors on video page)

The gun industry will now have its own merchant code when purchasing with credit cards from Visa, MasterCard, and American Express.
Pro-gun advocates fear mass surveillance, tracking, and politically motivated monitoring with this new merchant code system.
The gun industry not having its own merchant code until now is what's unusual, considering the level of detail in merchant codes.
There is no new information being tracked with the introduction of the gun industry's merchant code; the information was already available with credit card purchases.
Gun control advocates believe the merchant code could help identify suspicious gun purchases, but Beau argues that typical purchases like a rifle, magazines, and ammo may lead to false alarms.
Beau believes that law enforcement action based on flagged gun purchases is unlikely to result in anything significant, as legal gun ownership is not a crime.
The limited potential benefit of the new merchant code system lies in tracking purchases between areas with differing gun laws to prevent illegal sales across jurisdictions.
Overall, Beau sees the new merchant code as more of a talking point and a political move rather than a practical tool for tracking gun purchases effectively.
The new merchant code is not expected to be a useful tool for law enforcement or a form of mass surveillance, contrary to some fears surrounding its implementation.

Actions:

for policy advocates,
Educate policymakers on the limited practical use of the new gun industry merchant code (implied).
</details>
<details>
<summary>
2022-09-19: Let's talk about an update on Trump's special master.... (<a href="https://youtube.com/watch?v=T1gmB3-OoUY">watch</a> || <a href="/videos/2022/09/19/Lets_talk_about_an_update_on_Trump_s_special_master">transcript &amp; editable summary</a>)

DOJ likely to gain access to Trump documents despite legal battles; focus now on procedural timeline with 11th Circuit involvement.

</summary>

"Regardless of what the court decides, the end result will be that the Department of Justice ends up getting to use these documents."
"I can't envision a scenario in which these documents don't ultimately end up in the hands of the Department of Justice to use in this case."
"What they're really deciding on is how long this drags out."
"The final outcome here as far as DOJ being able to use these documents, I think that's pretty much set in stone."
"It's just how many procedural hoops are they going to have to jump through before they get access to them for the criminal investigation side of things."

### AI summary (High error rate! Edit errors on video page)

Providing an update on the legal battle concerning Trump documents and the special master process.
Department of Justice (DOJ) requested permission to bypass the special master for documents with classification stamps.
The judge denied the DOJ's request, prompting them to appeal to the 11th Circuit.
The 11th Circuit gave Team Trump until Tuesday to respond to the appeal, indicating a fast-track process.
Trump's team seeks exemption for documents with classification stamps from the special master process to use them immediately.
The 11th Circuit consists of 11 judges, with six being Trump appointees, but three will be randomly selected for the case.
The issue is about speed rather than the final outcome.
Beau doubts that the documents will not eventually be handed over to the DOJ.
Even if the documents were declassified, they remain property of the U.S. government.
Regardless of the court's decision, it is likely that the DOJ will obtain and use the documents.
The focus now is on how long the process will take rather than the ultimate outcome.
The 11th Circuit's decision will determine the timeline for accessing the documents for the criminal investigation.
Beau believes that the DOJ's access to the documents is almost certain, just a matter of procedural hurdles.
The 11th Circuit appears to be pushing for a quick resolution based on the deadline given to Trump's team.

Actions:

for legal observers and those interested in transparency and accountability.,
Prepare to stay informed about the developments in the legal battle over Trump documents (implied).
</details>
<details>
<summary>
2022-09-19: Let's talk about Hurricane Fiona and Puerto Rico.... (<a href="https://youtube.com/watch?v=USr_FZdZjm4">watch</a> || <a href="/videos/2022/09/19/Lets_talk_about_Hurricane_Fiona_and_Puerto_Rico">transcript &amp; editable summary</a>)

Hurricane Fiona triggers major flooding in Puerto Rico and the Dominican Republic, raising concerns about the effectiveness of federal response efforts amidst past inadequacies.

</summary>

"It's not so much the wind. It's the rain."
"The flooding has caused landslides, mudslides, power outages."
"There are probably a lot of people in Puerto Rico right now that are less than optimistic about the federal government's response."
"Sometimes past performance does predict future results."
"We'll know what we as individuals can do to help out."

### AI summary (High error rate! Edit errors on video page)

Hurricane Fiona is causing flooding in Puerto Rico and the Dominican Republic, with rainfall being the main issue.
The excessive rain, ranging from 12 to 30 inches in some areas, is leading to major flooding, including urban areas and rivers.
The flooding has triggered landslides, mudslides, and power outages across the island.
Puerto Rico's power grid is still struggling to recover from Hurricane Maria, with nearly one and a half million homes losing power during Fiona.
President Biden has declared a state of emergency, but the island's past experiences with inadequate federal responses are causing skepticism among residents.
Efforts are being made to restore power and establish command functions, but concerns remain about the effectiveness of the response.
It's uncertain how the situation will unfold, and there may be a need for widespread assistance in Puerto Rico.
Monitoring the situation in the coming days will provide a clearer picture of the extent of the damage and opportunities for individuals to help.
Beau commits to keeping everyone informed about ways to support the affected communities.
The aftermath of Hurricane Fiona serves as a reminder of the ongoing challenges faced by Puerto Rico and the importance of proactive assistance from both government and individuals.

Actions:

for disaster relief organizations,
Support local relief efforts in Puerto Rico (implied)
Stay informed about the situation to understand how to provide assistance (implied)
Volunteer with organizations working to aid those affected in Puerto Rico (implied)
</details>
<details>
<summary>
2022-09-18: Let's talk about what we can learn from Arkansas.... (<a href="https://youtube.com/watch?v=AwOmGSWelG0">watch</a> || <a href="/videos/2022/09/18/Lets_talk_about_what_we_can_learn_from_Arkansas">transcript &amp; editable summary</a>)

Get Loud Arkansas ensures 104,000 potentially disenfranchised voters in Arkansas can participate, stressing the importance of verifying voter registration nationwide.

</summary>

"It's probably a good idea to check on your voter registration."
"Not every state has an organization like Get Loud Arkansas that is going to go out and harass you into making sure that your voter registration is up to date."
"If you plan on voting, this is something you might want to just make sure that everything's still in order."
"The rhetoric surrounding the last election has given cover for a lot of people to, in the name of election integrity, subvert people's voice."
"Just stop by and do it with enough time to make sure that your new registration, if you need one, has time to process."

### AI summary (High error rate! Edit errors on video page)

Get Loud Arkansas, led by state senator Joyce Elliott, is working to ensure up to 104,000 people who may have been improperly removed from voter rolls can vote in the upcoming election.
Voter purging occurs frequently, especially in red states, where voters are removed for various reasons.
It's vital for individuals participating in electoral politics to verify their voter registration status to prevent disenfranchisement.
Not every state has an organization like Get Loud Arkansas actively ensuring voter registration is up to date.
Individuals from demographics that ruling parties may not want to vote or those in areas that typically vote against the predominant party should be particularly vigilant about their voter registration.
The rhetoric surrounding elections has been used to justify actions that undermine people's voices.
In states, especially in the South, it's advisable to verify your voter registration status before the upcoming election.
Ensure you have enough time for any necessary registration processing before the election.
Familiarize yourself with the voter registration laws in your state to understand the requirements.
It's critical to safeguard your right to vote by confirming your voter registration status.

Actions:

for voters,
Verify your voter registration status before the upcoming election (implied).
Familiarize yourself with the voter registration laws in your state (implied).
</details>
<details>
<summary>
2022-09-18: Let's talk about the next set of hearings.... (<a href="https://youtube.com/watch?v=b90lolwFBMQ">watch</a> || <a href="/videos/2022/09/18/Lets_talk_about_the_next_set_of_hearings">transcript &amp; editable summary</a>)

Beau outlines the upcoming January 6th hearings, particularly focusing on financial aspects and Secret Service text messages, aiming to keep the public informed and engaged while preparing for potential future indictments.

</summary>

"They're trying to get this information out to the American people in a way that is going to be a part of the story."
"It'll make for good TV."
"Preparing the American people for the possible future indictment of a former president."

### AI summary (High error rate! Edit errors on video page)

Talks about the upcoming hearings related to the events of January 6th and what to expect during those hearings.
Mentions the general template of telling what you're going to tell them, telling them, and then summarizing what you told them.
Focuses on the cash and cover-up aspects, with new developments revealing inquiries into possible financial crimes related to money raised for the election fight.
Emphasizes the significance of Secret Service text messages, thousands of which have been turned over and are being reviewed for insights into Trump's activities and mindset.
Indicates that the transcripts from these messages will likely take center stage due to their potential to reveal intriguing information that makes for good TV.
Acknowledges the challenge of presenting information in an accessible and accurate manner to keep the public engaged, especially when dealing with known plots.
Points out that the dollar amounts raised and where they went will be heavily focused on to maintain public interest.
Notes that the next hearing is scheduled for September 28, though this may change due to recent information shifting the focus towards money raised and Secret Service findings.
Suggests that drawing out information on the raised money and its public awareness can aid in future DOJ talks and potentially prepare the public for a former president's possible indictment.

Actions:

for citizens, activists, journalists,
Stay informed about the upcoming hearings and their developments (suggested)
Share information about the financial aspects and Secret Service findings with your community (suggested)
Follow reliable sources to stay updated on related news (suggested)
</details>
<details>
<summary>
2022-09-18: Let's talk about Wounded Knee changing hands.... (<a href="https://youtube.com/watch?v=HqfNoUiLI50">watch</a> || <a href="/videos/2022/09/18/Lets_talk_about_Wounded_Knee_changing_hands">transcript &amp; editable summary</a>)

The Oglala and Cheyenne River Sioux purchase land at Wounded Knee, aiming to preserve history and create an educational center, continuing their duty as "new ghost dancers."

</summary>

"The ghost dance, the misunderstanding of what they were seeing is one of the contributing factors to what happened in 1890."
"Today, we are the new ghost dancers, and we carry on a duty that came to us to do what we can for our relatives there at Wounded Knee."

### AI summary (High error rate! Edit errors on video page)

The Oglala and Cheyenne River Sioux are purchasing 40 acres of land around the national historical landmark for wounded knee.
The land purchase is significant because in 1890, almost 300 Lakota people were massacred at this location.
In 1973, there was a 71-day standoff between the American Indian movement and the government at the same location.
The purchase allows them to preserve the land and potentially turn it into an educational center.
Manny Ironhawk expresses the significance of the ghost dance to their people and the duty they carry on at Wounded Knee.
The misunderstanding of the ghost dance contributed to the tragic events in 1890.
The land, previously occupied by a trading post, will now be owned by the Sioux tribes.
It is hoped that the land will serve as a place for people to learn about the history from the perspective of the Lakota people.

Actions:

for history enthusiasts, indigenous rights advocates,
Support educational initiatives on Native American history and culture (implied)
Learn about and spread awareness of the significance of Wounded Knee (implied)
Respect and honor Native American perspectives on historical events (implied)
</details>
<details>
<summary>
2022-09-17: Let's talk about who gets to pick Georgia's next gov.... (<a href="https://youtube.com/watch?v=NpvIX_aiGIs">watch</a> || <a href="/videos/2022/09/17/Lets_talk_about_who_gets_to_pick_Georgia_s_next_gov">transcript &amp; editable summary</a>)

Unlikely voters could decide Georgia's next governor as demographics shift, potentially turning the state blue.

</summary>

"It is the unlikely voter who decides who runs that state."
"Georgia's turning blue."
"If the vote gets out, if those unlikely voters skew blue, she wins."

### AI summary (High error rate! Edit errors on video page)

Polls rely on likely voters, determined by pollsters, but motivating factors may drive unlikely voters to the polls in Georgia.
The latest polling in Georgia suggests unlikely voters will decide the next governor, as Kemp and Abrams are neck and neck.
With changing demographics in Georgia, unlikely voters who typically stay home may now have a significant impact on election outcomes.
The potential for unlikely voters to sway the election is heightened by the fact that some are willing to cross party lines.
If unlikely voters lean blue and combine with those crossing party lines, Georgia may turn blue.
Abrams, known for mobilizing voters, could win if these unlikely voters turn out and support her campaign.

Actions:

for georgia residents,
Mobilize unlikely voters to vote (implied)
Support voter turnout efforts (implied)
Encourage voter engagement and education (implied)
</details>
<details>
<summary>
2022-09-17: Let's talk about who advised Trump's lawyer.... (<a href="https://youtube.com/watch?v=kX6ZFYirs3s">watch</a> || <a href="/videos/2022/09/17/Lets_talk_about_who_advised_Trump_s_lawyer">transcript &amp; editable summary</a>)

Beau addresses the implications of recently released information regarding Trump's documents, pointing out potential misrepresentations and shifting blame towards Trump more prominently.

</summary>

"This information shifts the blame more to Trump than anything else that we've seen."
"That document that contained that photo with all of those files spread all over the floor that the Republicans found so funny, that's suddenly kind of critical evidence."

### AI summary (High error rate! Edit errors on video page)

Addresses the recent release of information from the documents case involving Trump.
Mentions how this information was initially redacted.
Points out that documents from the White House were stored in a location within Mar-a-Lago.
Raises questions about who advised the lawyer regarding the location of the records.
Speculates on the implications if Trump misled his lawyers about the location of the documents.
Suggests that this could limit potential individuals who could provide information to the Department of Justice.
Emphasizes the significance of the lawyer being advised by someone else.
Notes Trump's likely efforts to identify who provided the advisement to the lawyers.
Indicates that this information could shift blame towards Trump.
Points out the critical nature of a document containing a photo of files spread on the floor.

Actions:

for political observers, investigators, activists,
Investigate further into the documents case to understand the full implications (suggested)
Stay informed about developments related to this story (suggested)
</details>
<details>
<summary>
2022-09-17: Let's talk about red state statistics.... (<a href="https://youtube.com/watch?v=NipiCyojaGk">watch</a> || <a href="/videos/2022/09/17/Lets_talk_about_red_state_statistics">transcript &amp; editable summary</a>)

Beau challenges misconceptions about crime rates in Democrat-run cities and urges critical thinking in information consumption.

</summary>

"Democrat run cities are cesspools of violence and murder."
"It's a narrative. They repeat it often enough. You believe it."
"You have an emotional reaction about it. You get angry because now that piece of information doesn't fit with your narrative."

### AI summary (High error rate! Edit errors on video page)

Exploring narratives and information consumption in response to a video on a particular topic.
Addressing the challenge of breaking free from established narratives, even when faced with contrary evidence.
Critiquing the misconception about Democrats allowing violent criminals on the streets.
Pointing out the high murder rates in states that voted for Trump in 2020.
Providing statistics that challenge the narrative of Democrat-run cities being hubs of violence and murder.
Mentioning specific cities and states with high crime rates that voted Republican.
Emphasizing how certain narratives are perpetuated by media outlets to influence beliefs.
Describing how people become emotionally invested in false narratives and resist conflicting information.
Criticizing the tendency to hold on to misinformation despite it being easily debunked.
Concluding with a reminder not to believe everything presented by certain outlets and to think critically about information consumption.

Actions:

for information consumers,
Fact-check information from media outlets (suggested)
Challenge narratives with data and statistics (exemplified)
</details>
<details>
<summary>
2022-09-16: Let's talk about the new bail law in Illinois.... (<a href="https://youtube.com/watch?v=jf2r5OFTJpY">watch</a> || <a href="/videos/2022/09/16/Lets_talk_about_the_new_bail_law_in_Illinois">transcript &amp; editable summary</a>)

Beau clarifies Illinois system change to reduce cash standard for release, urging adherence to the Constitution’s spirit and fairness, aiming to prevent poor individuals from unjust detention.

</summary>

"Excessive bail shall not be required."
"The whole point of this is to make sure that poor people aren't punished for a lack of power coupons."
"It's just moving away from the standard of requiring cash."
"That's what you want."
"It's getting in line with the principles of the U.S. Constitution."

### AI summary (High error rate! Edit errors on video page)

Explains the change to the system in Illinois, specifically Chicago, addressing the memes and reactions surrounding it.
Clarifies that the point of the change is to restructure the system so cash isn't the sole determinant for release.
Criticizes conservatives and Republicans for wanting excessive bail, keeping poor people in jail, and letting rich individuals out.
Reminds viewers about the Eighth Amendment, which prohibits excessive bail, fines, and cruel punishments.
Argues that the new system aims to prevent poor individuals from being penalized due to financial constraints.
Emphasizes that hearings will still be held to determine threats or flight risks, moving away from the cash-based standard.
Urges viewers to view the change in line with the spirit of the U.S. Constitution and ensuring fairness in the legal system.
Stresses the goal of not letting individuals, especially those charged with minor offenses, languish in jail solely because of poverty.

Actions:

for advocates for justice reform,
Advocate for justice reform in your community (implied).
Educate others on the importance of reducing cash-based determinants for release (implied).
Support initiatives that aim to prevent unjust detention of poor individuals (implied).
</details>
<details>
<summary>
2022-09-16: Let's talk about the GOP needing more time for marriage equality.... (<a href="https://youtube.com/watch?v=6yDVBtXf0bE">watch</a> || <a href="/videos/2022/09/16/Lets_talk_about_the_GOP_needing_more_time_for_marriage_equality">transcript &amp; editable summary</a>)

Republicans stall on marriage equality vote, aiming to push it past midterms; Democrats urged to hold the vote, forcing a decision on basic rights for 20 million Americans.

</summary>

"Exactly how much time do you need to decide whether or not people should have basic rights?"
"If a senator is afraid of making a vote, of going on the record about something this simple just prior to an election, it's because they know the decision they would make, the vote that they cast, is unpopular."
"Either they vote in favor of giving rights to 20 million Americans or they vote against it."

### AI summary (High error rate! Edit errors on video page)

Republicans want to stall on bringing the vote on marriage equality because they perceive it as complex.
Democrats aim to bring the vote on marriage equality to the floor on Monday.
Republicans are bothered by this and claim they need more time to figure things out.
Beau questions how much time is needed to decide on people's basic rights.
Republicans may be stalling until after the midterms to vote against marriage equality.
Republicans want to vote it down but are hesitant when voters have a voice.
Some Republicans argue they need more time to work out finer details in the text before voting.
Beau suggests that Senators should put in extra effort considering the importance of the rights of 20 million Americans.
The issue boils down to Republicans trying to figure out who is vulnerable to appeal to their base.
Beau urges Democrats to hold the vote on Monday, forcing Republicans to decide on granting rights to 20 million Americans.

Actions:

for democrats, activists, voters,
Hold Senators accountable for their stalling tactics and urge them to vote on marriage equality promptly (suggested).
Advocate for timely decision-making on significant issues that affect millions of Americans (implied).
</details>
<details>
<summary>
2022-09-16: Let's talk about Trump's options and choices for leadership.... (<a href="https://youtube.com/watch?v=d1523D8Sdkk">watch</a> || <a href="/videos/2022/09/16/Lets_talk_about_Trump_s_options_and_choices_for_leadership">transcript &amp; editable summary</a>)

Beau addresses Trump's potential indictment, urging him to show leadership and calm his base to prevent negative outcomes and shape a positive legacy.

</summary>

"His option is to lead, to diffuse the situation, to calm things down."
"His legacy is being cemented by his post-presidency behavior."
"His moment to finally be of value to this country."

### AI summary (High error rate! Edit errors on video page)

Addressing Trump's potential indictment and his choices as a former president.
Trump's reckless statements and the need for leadership from him.
The power of Trump's voice and ability to influence his supporters positively.
The importance of Trump calming his base and diffusing tensions.
Trump's legacy being shaped by his post-presidency behavior.
Urging Trump to show leadership to avoid being seen as a mistake by American voters.
Emphasizing Trump's ability to prevent bad outcomes by choosing to lead and calm his supporters.

Actions:

for former trump supporters,
Contact Trump supporters to urge calm and positive behavior (suggested)
Advocate for peaceful actions and leadership in the community (implied)
</details>
<details>
<summary>
2022-09-16: Let's talk about Meadows complying.... (<a href="https://youtube.com/watch?v=pODbCwsRGM8">watch</a> || <a href="/videos/2022/09/16/Lets_talk_about_Meadows_complying">transcript &amp; editable summary</a>)

Beau provides insight into Meadows' compliance with the Department of Justice's investigation and the potential implications of his cooperation or lack thereof on the case.

</summary>

"Meadows may be somebody that the Department of Justice is looking at as somebody that's a high priority to flip."
"He knows a lot, he has access to a lot, and he would be one of the few people who can flip and provide insight into what Trump himself was doing."

### AI summary (High error rate! Edit errors on video page)

Providing an update on the Department of Justice's criminal investigation into January 6th.
Meadows received a subpoena from the Department of Justice and complied by turning over similar documentation as provided to the committee.
Meadows previously stopped complying after providing the committee with thousands of messages and emails.
He had withheld documents citing executive privilege, a claim not pushed by the committee due to its intention to allow privacy from Congress.
The Department of Justice, conducting a criminal investigation, may challenge Meadows' executive privilege claims.
Trump's inner circle advised him to distance himself from Meadows, fearing Meadows may cooperate with the Department of Justice.
Meadows, due to his position in the White House at the time, holds valuable information and may be pressured to cooperate.
Meadows may face a choice between cooperating with the Department of Justice or facing potential charges.
His cooperation or lack thereof may significantly impact the direction of the investigation and potential indictments.
The lack of legal resistance from Meadows in complying with the subpoena indicates his understanding of the situation and potential consequences.

Actions:

for legal analysts, concerned citizens,
Monitor updates on the Department of Justice's investigation to stay informed (implied).
Support transparency and accountability in legal proceedings by following related news and developments (implied).
</details>
<details>
<summary>
2022-09-15: Let's talk about what Trump said about possible indictment.... (<a href="https://youtube.com/watch?v=peBvBT-Catk">watch</a> || <a href="/videos/2022/09/15/Lets_talk_about_what_Trump_said_about_possible_indictment">transcript &amp; editable summary</a>)

Trump's possible indictment raises concerns of civil unrest, with emphasis on the importance of informing the public and voter turnout to mitigate potential negative outcomes.

</summary>

"If this was anybody else and anybody else had those documents for that long, stored in that way, they'd already be in cuffs."
"I honestly think at this point Trump is cementing himself as the worst mistake the American voters ever made."
"People are going to get hurt and it needs to stop."
"His brand, the thing that brought people in, is this idea of winning."
"Trump's getting a lot of bad advice, and as has been the case a lot lately, the bad advice he's getting is carrying negative consequences for the rest of the country."

### AI summary (High error rate! Edit errors on video page)

Trump might face indictment for the documents case at the bare minimum.
The only reason Trump isn't in jail right now is because he's the former president.
Trump's lawyers have probably explained the possibility of a future indictment to him.
Trump anticipates a future indictment, suggesting it could cause unrest in the country.
There are concerns about Trump intentionally stoking civil unrest or believing he is above the law.
Trump's behavior post-election may cement him as a historical mistake by American voters.
The importance of the committee's work in explaining what happened is emphasized.
Media plays a significant role in informing the public without bias.
Voter turnout, especially in the midterms, is seen as a way to diminish Trump's influence.
Trump's legacy and influence are tied to the perception of winning.
The rhetoric used by Trump and some Republicans is considered dangerous and could incite violence.
Statements made by the Republican Party are criticized for normalizing potentially harmful outcomes.
Normalizing these outcomes could backfire if Trump is indicted, leading to harsher consequences.
Bad advice given to Trump has negative consequences for the country.
There is a warning of bumpy months ahead due to the current political climate.

Actions:

for voters, concerned citizens,
Support voter education and turnout in upcoming elections to diminish Trump's influence (implied)
</details>
<details>
<summary>
2022-09-15: Let's talk about a possible railroad strike.... (<a href="https://youtube.com/watch?v=8k4JiwK7q0s">watch</a> || <a href="/videos/2022/09/15/Lets_talk_about_a_possible_railroad_strike">transcript &amp; editable summary</a>)

There's a looming railroad strike with potential impacts on the supply chain, requiring Congress to act, especially on staffing and scheduling issues beyond just pay.

</summary>

"If you're talking about something, and if it shuts down, the whole country stops, I mean, those people should probably be taken care of, is just my opinion."
"I think we'll see some movement from the management side. But we're going to have to wait and see."
"So anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

There is a potential railroad strike brewing, with a possible start on Friday, but an immediate work stoppage seems unlikely.
Freight train strikes have a massive impact on the supply chain, much more than a UPS strike.
The management of railroad companies may want the federal government to intervene to prevent the strike.
Biden cannot stop the strike; it will require Congress to act.
Some unions are pointing to staffing issues, requiring workers to be on call seven days a week, as a primary concern.
Unions are adamant about changes in staffing and scheduling, even more so than pay.
Beau believes that people involved in critical services like freight trains should be taken care of if shutdowns occur.
The potential strike's impact extends beyond freight trains, affecting services like Amtrak.
Biden and the Secretary of Labor are engaging with unions and management, but progress is slow.
Without a resolution, engineers and conductors going on strike over staffing and scheduling could halt train operations.

Actions:

for supply chain stakeholders,
Contact local representatives to urge them to address staffing and scheduling concerns in the railroad industry (implied)
Stay informed about the situation and its potential impacts on transportation and supply chains (implied)
</details>
<details>
<summary>
2022-09-15: Let's talk about Red Wolf Week.... (<a href="https://youtube.com/watch?v=KA52nYtzLCk">watch</a> || <a href="/videos/2022/09/15/Lets_talk_about_Red_Wolf_Week">transcript &amp; editable summary</a>)

Beau talks about Red Wolf Week, successful reintroduction efforts in North Carolina, the importance of continuous action, and supporting wolf conservation efforts at nywolf.org.

</summary>

"People will always devolve if they're not encouraged to protect these animals."
"You can't just count on the government to do what's right for animals that need protection."

### AI summary (High error rate! Edit errors on video page)

Talks about Red Wolf Week, red wolves, reintroduction efforts, and how people can help.
Describes the troubled history of red wolves and their decline to little pockets in Louisiana and Texas by 1970.
Explains Fish and Wildlife's unique idea of catching red wolves for a breeding program to reintroduce them to the wild.
Mentions the successful reintroduction of wolves in North Carolina in 1987, becoming a wildlife conservation role-model.
Criticizes Fish and Wildlife for stopping protection efforts, leading to a decline in numbers due to poaching.
Notes legal battles and federal court intervention prompting Fish and Wildlife to resume protection efforts.
Encourages support for wolf conservation efforts at nywolf.org through symbolic adoptions, donations, or shopping at their gift shop.
Stresses the importance of continuous action and not solely relying on the government for animal protection.

Actions:

for animal lovers, conservation enthusiasts.,
Visit nywolf.org to symbolically adopt a wolf, make a donation, or support by shopping at their gift shop (suggested).
</details>
<details>
<summary>
2022-09-15: Let's talk about Biden and the railroad strike.... (<a href="https://youtube.com/watch?v=A0LavDRrIAI">watch</a> || <a href="/videos/2022/09/15/Lets_talk_about_Biden_and_the_railroad_strike">transcript &amp; editable summary</a>)

Biden administration brokers talks to avert railroad strike, reaching a tentative agreement benefiting workers and companies, with details pending union vote.

</summary>

"These rail workers will get better pay, improved working conditions, and peace of mind around their health care costs, all hard earned."
"It goes on to say that the railroad companies will be able to retain and recruit more workers for an industry that will continue to be part of the backbone of the American economy for decades to come."

### AI summary (High error rate! Edit errors on video page)

Biden administration helped avert a potential railroad strike by brokering talks between unions and railroad companies.
Republicans in Congress wanted to use their authority to force workers to continue working to avoid the strike.
A tentative agreement has been reached, but details are not public yet.
Biden stated that rail workers will receive better pay, improved working conditions, and peace of mind regarding healthcare costs.
The agreement will benefit both rail workers and companies by retaining and recruiting more workers.
The term "tentative" indicates that the agreement will go to union membership for ratification through a vote.
If the vote fails, there will be a cooling-off period before another vote, potentially after the midterms.
The announcement of the agreement came late at night or early in the morning.
The Biden administration seems proud of the agreement, which may have caught Republicans advocating to force workers off guard.
The statement suggests that workers will be happy and railways not upset, but the details of the agreement are still unknown.

Actions:

for workers, policymakers,
Support workers' rights through advocacy and engagement (implied)
</details>
<details>
<summary>
2022-09-14: Let's talk about The Pillow Guy's phone.... (<a href="https://youtube.com/watch?v=AG2nt1Cb3Bs">watch</a> || <a href="/videos/2022/09/14/Lets_talk_about_The_Pillow_Guy_s_phone">transcript &amp; editable summary</a>)

Mike Lindell faces federal investigation and advice on dealing with legal troubles; all roads lead to the same place.

</summary>

"Despite his superior pillow products, he won't be sleeping easily any time soon."
"If you see your name in a list like this, step one, contact a really good lawyer."
"Generally speaking, once the feds have gotten to this point, they kind of already have the information needed to indict."
"All roads lead to the same place."
"You can take this evidence in your own hands."

### AI summary (High error rate! Edit errors on video page)

Mike Lindell, noted pillow industrialist, faced a federal search warrant at Hardee's leading to sleepless nights despite his superior pillow products.
The warrant seeks records on Lindell's phone related to violations including identity theft, intentional damage to a protected computer, and conspiracy involving several individuals.
The listed violations involve individuals like Tina Peters, the MyPillow guy, and others known and unknown to the government.
The investigation, starting at the state level, has now expanded to national figures like Lindell and Peters, who have been at the White House.
Beau advises that once your name is listed in an investigation like this, contacting a good lawyer and learning how to cook with ramen might be wise steps.
When federal authorities reach this stage, they likely have enough information to indict, showcasing a high level of certainty in their case.
The evidence gathered seems to connect various levels of government and individuals from New York City to the White House.
Despite seeming like a side investigation, all roads seem to lead to the same place, hinting at future relevance of the material.
Beau suggests that viewers can take the evidence into their own hands and get an idea of its impact.
The transcript concludes with Beau urging everyone to have a good day.

Actions:

for legal observers, concerned citizens.,
Contact a really good lawyer (implied).
Learn how to cook in a microwave using ramen (implied).
Take the evidence into your own hands (implied).
</details>
<details>
<summary>
2022-09-14: Let's talk about Michigan, Trump, and how it used to be.... (<a href="https://youtube.com/watch?v=TsJjESPXixI">watch</a> || <a href="/videos/2022/09/14/Lets_talk_about_Michigan_Trump_and_how_it_used_to_be">transcript &amp; editable summary</a>)

Former President's promotion of a conspiracy theory after a family tragedy prompts Beau to urge the Republican Party to confront their dangerous path.

</summary>

"This isn't the first time that this has happened, but this was just in such close proximity."
"There should probably be a moment in which the Republican Party realizes how far it's gone."
"The Republican Party has some soul-searching to do, and they need to do it quick."

### AI summary (High error rate! Edit errors on video page)

Talking about a tragic incident in Walled Lake, Michigan involving a man who fell into a conspiracy theory and opened fire on his family.
The man's daughter managed to escape and shared how her father deteriorated and became obsessed with election conspiracy theories.
Former President of the United States shared a photoshopped image of himself with symbols associated with the conspiracy theory after the incident.
Beau points out that this incident is not the first of its kind but is particularly close to a tragic event.
Beau criticizes the Republican Party for either following along or turning a blind eye to the dangerous dissemination of conspiracy theories.
This incident should be a wake-up call for the Republican Party to realize how far they have gone.
Beau urges the Republican Party to do some soul-searching and address the dangerous influence of conspiracy theories within their ranks.

Actions:

for republicans, political observers,
Call for the Republican Party to self-reflect and address the impact of conspiracy theories within their circles (implied).
</details>
<details>
<summary>
2022-09-14: Let's talk about Graham's proposed ban.... (<a href="https://youtube.com/watch?v=Pe4IpI3ysrI">watch</a> || <a href="/videos/2022/09/14/Lets_talk_about_Graham_s_proposed_ban">transcript &amp; editable summary</a>)

Lindsey Graham's 15-week abortion ban proposal is seen as a calculated compromise that compromises women's rights, illustrating the GOP's disconnect with Americans.

</summary>

"You showed us who you were. We believe you. There's no compromise to be had here."
"We're not going to compromise or negotiate on women's bodily autonomy."
"I don't see any compromise to be had here."
"It just illustrates exactly how out of touch Republicans are with the average American."
"I don't see this move as politically tenable."

### AI summary (High error rate! Edit errors on video page)

Lindsey Graham's proposal of a 15-week abortion ban is seen as surprising in light of current political trends.
National politicians like Lindsey Graham are distancing themselves from extreme state bans with this proposal as a compromise.
The 15-week ban is positioned as a way to appeal to moderates and independents while still supporting abortion restrictions.
Graham's proposal, although framed as a compromise, is criticized for compromising on women's bodily autonomy.
The goal behind the proposal is to allow Republican senators to cater to pro-ban supporters while maintaining an image of reasonability.
Beau does not view this compromise on women's rights as acceptable or politically viable.
He sees the proposal as a token gesture to find middle ground, which he believes is untenable.
Beau criticizes the Republicans for being out of touch with the average American on this issue.

Actions:

for advocates for women's rights,
Advocate for women's bodily autonomy and reproductive rights (implied)
Support organizations that work towards protecting women's rights (implied)
</details>
<details>
<summary>
2022-09-13: Let's talk about the hearing, ethics, and Mudge.... (<a href="https://youtube.com/watch?v=aPL093zXv5k">watch</a> || <a href="/videos/2022/09/13/Lets_talk_about_the_hearing_ethics_and_Mudge">transcript &amp; editable summary</a>)

Beau raises awareness about a whistleblower's disclosure on Twitter security collapses, stressing the importance of understanding social media's influence.

</summary>

"He's MUDGE, Google him, M-U-D-G-E."
"I have to take what he says at face value until I was presented with something that was in direct contradiction."
"I definitely suggest paying attention to the coverage of it."

### AI summary (High error rate! Edit errors on video page)

Introduction to discussing a whistleblower and a disclosure about security collapses at Twitter.
Mention of whistleblower Peter Zatko, known as Mudge, with a reputation for being ethical and straightforward.
Beau's personal belief in Mudge's credibility but admits to not reading the disclosure.
Not providing objective commentary due to personal connection with Mudge.
Acknowledgment of hearing about the issue coming up soon and suggests following the coverage.
Emphasizes the importance of understanding social media as critical communications infrastructure.
Raises concerns about social media shaping beliefs and the influence of echo chambers.
Encourages paying attention to the unfolding process and coverage of the issue.

Actions:

for social media users,
Follow the coverage of the whistleblower's disclosure (suggested)
Pay attention to the unfolding process (suggested)
</details>
<details>
<summary>
2022-09-13: Let's talk about all those subpoenas about Trump.... (<a href="https://youtube.com/watch?v=TG6Wi2vsrPs">watch</a> || <a href="/videos/2022/09/13/Lets_talk_about_all_those_subpoenas_about_Trump">transcript &amp; editable summary</a>)

The Department of Justice's wide-reaching subpoenas target post-election actions, potentially implicating Trump allies and political figures, with a focus on obstruction.

</summary>

"There's a seditious underground parking garage that they need to go through next."
"Some of these people, they're staffers, and they are gonna have access to a lot more information than people are gonna give them credit for."
"Taking the phones, that's pretty significant."
"They are kind of building a case that appears now to be actively targeting not just people that are seen as Trump's inner circle, but possibly other political figures."
"There will probably be more information flowing out pretty regularly between now and September 23rd."

### AI summary (High error rate! Edit errors on video page)

Department of Justice sent out 30 to 40 subpoenas last week, targeting a range of individuals.
Subpoenas request broad information on post-election actions of the Trump administration.
Recipients were compelled to appear before a grand jury and had their phones searched.
Information requested includes communications with Trump allies like Giuliani and Powell.
Recipients were asked to provide information on any members of the executive or legislative branches involved in obstructing the election certification.
Senate staffers might be feeling nervous due to the wide-reaching subpoenas.
The investigation keeps expanding due to the complex nature of the case.
Staffers may have access to significant information due to their roles.
Seizing phones indicates the seriousness of the investigation.
The case seems to be targeting not only Trump's inner circle but also other political figures involved in altering election outcomes.

Actions:

for journalists, activists, legal observers,
Stay updated on developments in the investigation and share information with your community (suggested).
Support transparency and accountability in legal proceedings by discussing the implications of these subpoenas with others (suggested).
</details>
<details>
<summary>
2022-09-13: Let's talk about Putin and a Swan Lake moment.... (<a href="https://youtube.com/watch?v=erA9c0mQ_Ik">watch</a> || <a href="/videos/2022/09/13/Lets_talk_about_Putin_and_a_Swan_Lake_moment">transcript &amp; editable summary</a>)

Beau explains Swan Lake moments in Soviet history and draws parallels to Putin's potential downfall amidst Ukraine's successful counteroffensive and altered victory conditions.

</summary>

"Swan Lake is synonymous with political turmoil. It's a bad sign."
"The nationalists within the country are now questioning the war effort."
"Putin will not remain in power through that exchange."
"An unstable or failed Russia is bad for everybody."
"Hope that it is an internal decision based on dissatisfaction with his leadership."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of Swan Lake moments in relation to Soviet history during the Cold War.
Describes the use of Swan Lake ballet on TV to distract and entertain the masses during political transitions in the Soviet Union.
Talks about the failed coup attempt against Gorbachev in 1991 and its connection to Swan Lake being aired on TV.
Details the information vacuum that occurred during the coup attempt due to state control of media.
Links Swan Lake to political turmoil and a symbol of powers behind the scenes during uncertain times.
Analyzes the current situation with Putin in relation to Ukraine's successful counteroffensive.
Mentions the significant losses for Russia in terms of both lives and territory in the ongoing conflict.
Suggests that the nationalist sentiment in Russia is shifting due to perceived failures in the war effort.
Explains Ukraine's altered victory conditions, including seeking reparations from Russia.
Speculates on the potential consequences for Putin if Ukraine demands reparations and how it could lead to a Swan Lake moment.

Actions:

for analysts, policymakers, activists,
Support efforts that aim for a transition of power within Russia (implied)
Advocate for stability and internal decision-making processes in Russia (implied)
</details>
<details>
<summary>
2022-09-13: Let's talk about DOJ looking into Trump's fundraising.... (<a href="https://youtube.com/watch?v=cPT19CkDtdU">watch</a> || <a href="/videos/2022/09/13/Lets_talk_about_DOJ_looking_into_Trump_s_fundraising">transcript &amp; editable summary</a>)

Department of Justice grand jury in DC investigates potential fraud in Trump's post-election fundraising, raising concerns about diversion of funds for personal gain.

</summary>

"It's worth noting that the money that was raised was not spent on what the feds are looking into at this point."
"I literally have a whiteboard now divided up by the various investigations and the various jurisdictions, just so I can figure out which piece of information goes with which case."

### AI summary (High error rate! Edit errors on video page)

Department of Justice grand jury in DC is now looking into potential fraud regarding Trump's fundraising activities post-election.
Subpoenas have been issued to high-profile individuals, with one signed by a federal prosecutor specializing in fraud.
Allegations surfaced during the January 6th hearings regarding Trump fundraising for the Official Election Defense Fund.
Reporting suggests the fund raised around a quarter of a billion dollars, but there are discrepancies in the reported amounts.
Money raised may have gone into Trump's Save America PAC, which poses issues due to the loose regulations surrounding PACs.
Federal investigators are likely examining whether funds raised for a specific purpose were diverted elsewhere for personal gain.
Similar financial discrepancies led to Steve Bannon's arrest, though this case is separate from the New York investigation.
The complex web of investigations surrounding Trump's actions has led to Beau needing a whiteboard to keep track.
Speculation points to the Department of Justice focusing on potential financial crimes based on the subpoenas and previous hearings.
While reports indicate ongoing investigations, definitive details on the specific crimes being scrutinized remain unclear.

Actions:

for legal watchdogs, investigators, concerned citizens,
Monitor updates on the investigation closely (implied)
Stay informed about the potential financial crimes being investigated (implied)
</details>
<details>
<summary>
2022-09-12: Let's talk about the Little Mermaid and relatability.... (<a href="https://youtube.com/watch?v=ubE-arwpjk0">watch</a> || <a href="/videos/2022/09/12/Lets_talk_about_the_Little_Mermaid_and_relatability">transcript &amp; editable summary</a>)

The new Black Ariel in The Little Mermaid brings relatability and love conquering all themes to a wider audience.

</summary>

"Changing Ariel's race makes her more relatable to a wider audience."
"The message of the story could be interpreted as 'race doesn't matter.'"
"Love conquering all, including race, is a central theme of the movie."

### AI summary (High error rate! Edit errors on video page)

The new version of The Little Mermaid features a Black mermaid, causing a stir.
The original Little Mermaid story differs drastically from the Disney version.
Beau grew up with the Disney version and feels attached to the red-haired Ariel.
Changing Ariel's race makes her more relatable to a wider audience.
Beau questions the purpose of changing Ariel's race in the movie, considering it's integral to the plot.
The message of the story could be interpreted as "race doesn't matter."
Beau encourages viewers to see beyond skin tone and embrace the message of love conquering all.
The film introduces The Little Mermaid's story to a new audience, offering them the same lessons and inspiration.
Love conquering all, including race, is a central theme of the movie.
Beau suggests that the focus should be on the broader message rather than skin tone.

Actions:

for film enthusiasts, disney fans,
Watch and support movies that showcase diverse representation (exemplified)
Engage in constructive dialogues about diversity and representation in media (implied)
</details>
<details>
<summary>
2022-09-12: Let's talk about numbers to give the GOP chills.... (<a href="https://youtube.com/watch?v=d-9zfxxWdLk">watch</a> || <a href="/videos/2022/09/12/Lets_talk_about_numbers_to_give_the_GOP_chills">transcript &amp; editable summary</a>)

Beau breaks down online donor numbers revealing Democratic Party's surge and Republican Party's loss; a Supreme Court decision seems to be a significant motivator.

</summary>

"These additional 600,000 online donors for the Democratic Party, those very well may be those unlikely voters that we've talked about as far as things that might cause polling to be a little off."
"That Supreme court decision, that's what people see as a motivating factor."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of document-based journalism in providing context and revealing information.
Mentions a Politico article containing interesting numbers that should concern the Republican Party.
Compares the number of online donors for the Republican and Democratic parties from the last half of 2022 to the first half of 2022.
Republican Party had a loss of 43,000 online donors from 2021 to 2022.
Democratic Party saw a substantial increase in online donors during the same period.
Democratic Party's enthusiasm supposedly increased significantly after a specific Supreme Court decision in June.
The surge in Democratic online donors might include unlikely voters, potentially affecting polling accuracy.
Indicates the need for political strategists to pay attention to these trends.

Actions:

for political analysts, strategists,
Analyze political trends and donor data for strategic insights (implied)
</details>
<details>
<summary>
2022-09-12: Let's talk about Ukraine and expectations.... (<a href="https://youtube.com/watch?v=dE1-8OWbI-U">watch</a> || <a href="/videos/2022/09/12/Lets_talk_about_Ukraine_and_expectations">transcript &amp; editable summary</a>)

Ukrainian forces achieve a successful counteroffensive in Ukraine, but managing expectations is key as the conflict may continue protractedly.

</summary>

"The communications failures are systemic. They're throughout the military."
"Don't oversell this. Huge win at the operational level. This doesn't mean the war's over."
"Putin lost. Militarily, they still may decide that they want to try to hold on to some dirt."
"The soldiers they're fighting are in the same situation soldiers have found themselves in since the beginning of time."
"But it's winning a battle, not winning the war."

### AI summary (High error rate! Edit errors on video page)

Ukrainian forces launched a successful counteroffensive in Ukraine.
Russian forces faced command and communication failures and logistical issues.
Disorganization and disarray were seen both in Russian troops on the battlefield and in Moscow.
State-funded propagandists in Moscow were uncertain and correcting each other on air.
The communications failures in the Russian military do not bode well for the future.
The Ukrainian counteroffensive is a huge operational win and will alter the course of the war.
Politically, the war is over, and Putin lost. But militarily, the fighting can continue.
Managing expectations is vital as the conflict may be protracted.
Ukraine needs Western support for supplies, equipment, and training to defeat Russia militarily.
Overselling the current success may lead to false expectations about the war's end.

Actions:

for international community, activists,
Provide support to Ukraine with supplies, equipment, and training (suggested)
Manage expectations about the conflict's duration and outcomes within your community (implied)
</details>
<details>
<summary>
2022-09-11: Let's talk about midterm strategy and marriage equality.... (<a href="https://youtube.com/watch?v=wtHCrfShCx8">watch</a> || <a href="/videos/2022/09/11/Lets_talk_about_midterm_strategy_and_marriage_equality">transcript &amp; editable summary</a>)

Republicans embrace Trump's rhetoric for midterms, Democrats provoke Trump and put Republicans on the spot with marriage equality vote strategy.

</summary>

"Goad Trump. Get Trump fired up so he says wild things."
"Vote these people out. We've already proven that we're willing to put this legislation forward."
"They're going to have to reject him again."
"They actually get to do some good for a change."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Republicans' midterm strategy: embrace Trump's rhetoric, attack Biden, run a negative campaign.
Trump disrupts Republican plans by not staying on the sidelines, candidates forced to support his rhetoric.
Democratic strategy: provoke Trump to say wild things, make Republicans defend him, look silly to most.
Part of the plan: let Trump speak, put Republicans on the spot regarding policies from MAGA rhetoric.
Schumer's announcement: vote on marriage equality before midterms to energize Democratic-leaning demographic.
Goal for Democrats: make Republicans go on record against marriage equality or pass it to energize voters.
If marriage equality vote fails, Democrats can use it as a talking point to motivate voters for midterms.
Smart move for Democrats to force Republicans to defend policies instead of running a negative campaign.
Democrats aim to show the American people the policies awaiting them if Republicans regain control.
Strategy may work by energizing demographics against authoritarian agenda of the Republican Party.

Actions:

for political activists and voters,
Contact your local representatives to express support for or against marriage equality vote (suggested).
Organize voter education events to inform the community about the policies at stake in the upcoming elections (exemplified).
</details>
<details>
<summary>
2022-09-11: Let's talk about UPS and labor.... (<a href="https://youtube.com/watch?v=VPTz_YTfcu8">watch</a> || <a href="/videos/2022/09/11/Lets_talk_about_UPS_and_labor">transcript &amp; editable summary</a>)

UPS faces potential strike as Teamsters negotiate, showcasing unions' effectiveness beyond pay.

</summary>

"The possibility of a strike is determined by management's stance in negotiations, not the union."
"Unions like the Teamsters strike not just for pay but for better working conditions and representation."
"The high pay of Teamsters is a result of collective bargaining, showcasing the effectiveness of unions."
"Tell me again how unions don't work."
"You get all of these articles talking about the fact that they're the highest paid and therefore they shouldn't strike."

### AI summary (High error rate! Edit errors on video page)

UPS is in the news due to upcoming contract negotiations with the Teamsters union, with speculation of a potential strike.
The possibility of a strike is determined by management's stance in negotiations, not the union.
Recent leadership changes and a $300 million strike fund suggest the Teamsters may indeed go on strike.
A potential strike could start at 12:01 AM on August 1 and could significantly impact the US GDP due to UPS's role in facilitating 6% of it.
The average pay for 350,000 Teamsters is $95,000, but the strike isn't just about pay; it also involves demands like air conditioning in trucks.
Unions like the Teamsters strike not just for pay but for better working conditions and representation.
The high pay of Teamsters is a result of collective bargaining, showcasing the effectiveness of unions in ensuring fair compensation.
The coverage of their high pay should not be used as a reason against their right to strike, as it is the result of strong union representation.

Actions:

for workers, union members, negotiators,
Support Teamsters by staying informed about the negotiations and showing solidarity with their cause (suggested).
Reach out to local unions to understand the importance of collective bargaining and worker representation (implied).
</details>
<details>
<summary>
2022-09-11: Let's talk about California's cautionary tale.... (<a href="https://youtube.com/watch?v=TWyjjVBvjH0">watch</a> || <a href="/videos/2022/09/11/Lets_talk_about_California_s_cautionary_tale">transcript &amp; editable summary</a>)

California's energy crisis serves as a cautionary tale, urging states to accelerate the shift to cleaner energy infrastructure to combat climate change effectively.

</summary>

"This is coming. It's not slowing down. It's not going to stop unless we start to mitigate unless we start to make the changes necessary."
"Your kids will be there soon. This is real."
"The solution is to speed up the installation of the infrastructure to get to that cleaner energy faster."
"It's a lesson in speeding up the transition, not slowing it down or mocking the current situation."
"The urgency lies in making changes now to mitigate the impacts of climate change effectively."

### AI summary (High error rate! Edit errors on video page)

California struggles with energy needs during a heat wave, resorting to using old, dirty infrastructure due to clean energy inadequacies.
The right-wing in the United States uses California's situation to mock clean energy efforts, creating a loop of burning more stuff and generating more emissions.
The heat wave crisis in California serves as a cautionary tale for all states to transition to cleaner energy immediately to avoid a similar fate.
Politicians influenced by the fossil fuel industry slow down infrastructure growth towards clean energy.
The solution is not to laugh or point fingers but to accelerate the installation of cleaner energy infrastructure.
The transition to clean energy must be hastened to prevent future energy crises like California's.
It is a lesson in speeding up the transition, not slowing it down or mocking the current situation.
Children will face the consequences of climate inaction if states do not act swiftly to embrace cleaner energy.
The urgency lies in making changes now to mitigate the impacts of climate change effectively.
The focus should be on expediting the shift to cleaner energy, not delaying it for political gains.

Actions:

for climate activists, concerned citizens,
Accelerate the installation of cleaner energy infrastructure (implied)
Advocate for politicians to prioritize clean energy initiatives (implied)
</details>
<details>
<summary>
2022-09-10: Let's talk about the GOP plan for student debt.... (<a href="https://youtube.com/watch?v=gcF9pI-snG8">watch</a> || <a href="/videos/2022/09/10/Lets_talk_about_the_GOP_plan_for_student_debt">transcript &amp; editable summary</a>)

Beau criticizes Arizona Republicans' plan to stop student loan forgiveness, exposing their lack of interest in helping working-class Americans and their desire to maintain control by keeping people uneducated and in debt.

</summary>

"The Republican Party has absolutely zero interest in helping working class Americans and Americans trying to better themselves."
"They want to represent the people who fund their campaigns."
"They want to make sure that they're setting an example so other people don't get an education."
"They don't want you educated. They don't want you out of debt."
"It's a great idea."

### AI summary (High error rate! Edit errors on video page)

Criticizes a Republican plan from Arizona to file suit against Biden's Student Loan Forgiveness Program.
Mocks the idea that Republicans are trying to help working-class Americans or those seeking education.
Points out the potential negative impact of stopping student loan forgiveness on millions of Americans.
Suggests that the Republican Party wants to keep people in debt and uneducated to maintain control.
Calls out the Republican Party for representing campaign funders rather than the American people.

Actions:

for voters, students, activists,
Contact your representatives to voice support for student loan forgiveness (suggested)
Join advocacy groups pushing for educational opportunities and debt relief (implied)
</details>
<details>
<summary>
2022-09-10: Let's talk about security, Trump, and a talking point.... (<a href="https://youtube.com/watch?v=VZWutbGcaXY">watch</a> || <a href="/videos/2022/09/10/Lets_talk_about_security_Trump_and_a_talking_point">transcript &amp; editable summary</a>)

Addressing excuses to absolve the former president of endangering national security, Beau challenges flawed notions of document security and urges for accountability.

</summary>

"There is literally no excuse. There's no talking point that will make it okay. It never should have happened."
"Secure means different things in different contexts."
"They weren't secure."
"Stop trying to find a way to make this okay. It isn't."
"Now we have to find out how it happened and why it happened."

### AI summary (High error rate! Edit errors on video page)

Addressing the excuses made to absolve the former president of endangering U.S. national security by exposing secrets.
Exploring the flawed notion that Secret Service protection of the former president extends to protecting documents.
Illustrating the illegality and ineffectiveness of attempting to secure classified documents through Secret Service presence.
Challenging the idea of a "secure facility" and the varying meanings of security in different contexts.
Questioning why certain classified documents were not returned and how many individuals were exposed to them.
Emphasizing the lack of excuses or talking points that can justify the mishandling of classified documents.
Urging for accountability and investigation into how and why such a security breach occurred.

Actions:

for citizens, government officials,
Investigate the mishandling of classified documents (implied)
Hold accountable those responsible for the security breach (implied)
</details>
<details>
<summary>
2022-09-10: Let's talk about Trump and RINOs.... (<a href="https://youtube.com/watch?v=xXhSvSCuYZk">watch</a> || <a href="/videos/2022/09/10/Lets_talk_about_Trump_and_RINOs">transcript &amp; editable summary</a>)

Beau explains the "rhino" concept, contrasting Trump's authoritarianism with traditional Republican values represented by figures like Barr, McConnell, Cheney, and Rove.

</summary>

"Trump is the rhino, certainly not Karl Rove."
"Trump's not a Republican. He's an authoritarian."
"He went and shopped the various political parties trying to find the one that was easiest to manipulate."
"The rhino concept has driven a wedge in the Republican Party."
"Their rhetoric was always about individualism and stuff like that."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of a "rhino" and its application to Trump and other prominent Republicans like Barr, McConnell, Cheney, and Rove.
Trump's history of switching political affiliations, starting as an independent in 2012, Democrat in 2009, and Reform Party member in 2001, contrasts with the long-standing Republican backgrounds of the others.
Trump is characterized as an authoritarian rather than a traditional Republican, using the party's platform for his own agenda.
The wedge driven into the Republican Party by the divide between small government conservatives and authoritarians is emphasized.
Beau expresses his disagreement with figures like Barr, Rove, Cheney, and McConnell, acknowledging their Republican identity while criticizing Trump for not truly adhering to Republican principles.
Trump's manipulation of rhetoric and base-building strategies is discussed, pointing out how he deviates from traditional Republican policies.
The contrast between the Republican Party's small government conservatism rhetoric and Trump's authoritarian tendencies is outlined.

Actions:

for political observers,
Analyze political candidates' policies and rhetoric to understand their true intentions (suggested)
Engage in respectful political discourse to bridge ideological divides (implied)
</details>
<details>
<summary>
2022-09-10: Let's talk about North Korea supplying Russia.... (<a href="https://youtube.com/watch?v=d5VjUyt_980">watch</a> || <a href="/videos/2022/09/10/Lets_talk_about_North_Korea_supplying_Russia">transcript &amp; editable summary</a>)

Russia may turn to North Korea for supplies due to low stocks, potentially impacting global dynamics and alliances.

</summary>

"Russia may be turning to North Korea for supplies, including ammo, due to low stocks."
"North Korea's old equipment could be compatible with Russia's, potentially benefiting both sides."
"The potential fallout could lead to unexpected countries supporting Ukraine against Russia."

### AI summary (High error rate! Edit errors on video page)

Russia may be turning to North Korea for supplies, including ammo, due to low stocks.
North Korea's older equipment, initially from the Soviet Union, could be compatible with Russia's.
North Korea has large stockpiles that could benefit Russia in exchange for various goods or looking the other way on sanctions.
There's speculation on whether US intelligence is behind the messaging to sway Russian allies.
The situation makes sense given recent concerns about the US needing to reorder artillery shells.
North Korea's old and potentially degraded supplies could pose a problem for Russia if utilized.
The potential fallout could lead to unexpected countries supporting Ukraine against Russia.
Russia's lack of capability to quickly replenish supplies contrasts with the US's abilities.
The compatibility between North Korean and Russian equipment due to destruction of modern supplies adds up.
The situation is still unfolding, but the possibility of North Korea supplying Russia seems accurate.

Actions:

for policy analysts, geopolitical strategists,
Monitor developments and analyze potential impacts on global alliances and conflicts (implied)
Stay informed about the evolving situation between Russia, North Korea, and Ukraine (implied)
</details>
<details>
<summary>
2022-09-09: Let's talk about midterm polling errors.... (<a href="https://youtube.com/watch?v=R3Jyp6wVbU4">watch</a> || <a href="/videos/2022/09/09/Lets_talk_about_midterm_polling_errors">transcript &amp; editable summary</a>)

Beau warns of potential polling errors in the midterms due to enthusiasm from unlikely voters, especially younger demographics, potentially favoring the Democratic Party.

</summary>

"I think the final product might be wrong as well."
"It gives an edge towards the Democratic Party."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau discussed polling errors in the midterms and his faith in polling historically.
Polling errors could occur in the midterms due to the enthusiasm in certain demographics leading to unlikely voters turning up.
Younger people, especially women energized by a Supreme Court rule, might participate in higher numbers than anticipated.
Errors in determining likely voters, especially with younger individuals and college students, could skew the final polling results.
Beau suggests that discounting certain groups of potential voters might give an edge to the Democratic Party.
Ultimately, Beau's predictions are based on his perception at the current time and may change.

Actions:

for voters,
Stay informed about the upcoming midterms and encourage others to vote (implied).
Ensure you and those around you are registered to vote (implied).
Engage in political discourse and encourage voter participation within your community (implied).
</details>
<details>
<summary>
2022-09-09: Let's talk about how Trump might have more documents.... (<a href="https://youtube.com/watch?v=a1R-FWdEnz0">watch</a> || <a href="/videos/2022/09/09/Lets_talk_about_how_Trump_might_have_more_documents">transcript &amp; editable summary</a>)

Department of Justice pushes for access to classified material in Trump case, raising concerns about national security risks and potential undisclosed documents.

</summary>

"We want the classified material right now."
"They're not attorney-client, they're not executive privilege, and they don't belong to Trump."
"That speaks volumes."
"There might be more to this still coming."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Department of Justice wants classified material now and seeks to change judge's orders.
Classified documents are not attorney-client or executive privilege.
DOJ may take the issue to the 11th Circuit if judge doesn't comply.
Intelligence assessment on risk created by former president is at play.
FBI, part of intelligence community, is vital for counterintelligence.
There's overlap between criminal investigation and intelligence assessment.
DOJ believes former president may still have classified records.
DOJ suggests that impeding use of classified records in criminal investigation poses national security risks.
Feds could have a willful retention case if additional documents are found.
Concerns about potential ongoing searches and undisclosed documents.
Uncertainty around the existence of additional documents.
DOJ hopes judge will grant access to classified material.
Legal justification seems lacking to deny access.
If judge denies, the issue will go to the 11th Circuit.
Uncertainty and potential for more developments in the situation.

Actions:

for legal analysts, concerned citizens,
Monitor updates on the legal proceedings and outcomes (exemplified)
Stay informed about potential national security risks (exemplified)
Advocate for transparency and accountability in handling classified materials (implied)
</details>
<details>
<summary>
2022-09-09: Let's talk about a Michigan ruling and a Republican choice.... (<a href="https://youtube.com/watch?v=Jt4vJPW8EvY">watch</a> || <a href="/videos/2022/09/09/Lets_talk_about_a_Michigan_ruling_and_a_Republican_choice">transcript &amp; editable summary</a>)

Michigan ruling and Attorney General's promise leave Republican Party with a choice: stand up against rights-stripping decision or hide behind the Supreme Court, impacting re-election chances.

</summary>

"It's up to the GOP."
"Whether or not all of that rage that they manufactured, all of that anger, all of that kicking down at people, whether or not they're willing to do it themselves or they just want their constituents to do it."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Michigan ruling combined with promise from Attorney General puts the ball in the Republican Party's court.
Judge in Michigan ruled 1931 law prohibiting abortion violates due process and equal protection.
Attorney General in Michigan promised not to appeal the ruling, so the law will be struck down.
Governor of Michigan asking Supreme Court to rule on lawsuit protecting family planning rights.
Supreme Court of Michigan expected to rule on a constitutional amendment regarding the ballot.
Republican politicians in Michigan have the choice to appeal and fight against the ruling.
Republicans may have to put their names on the decision to take away half the state's rights.
Impact on re-election chances is now a concern for Republican politicians.
Some Republican politicians are starting to backtrack on their rhetoric on this topic.
Question arises whether Republicans are willing to take action themselves or want constituents to do it.

Actions:

for michigan residents, republicans,
Contact Republican politicians in Michigan to urge them to uphold rights (implied)
Stay informed about the developments and decisions regarding the court rulings in Michigan (implied)
</details>
<details>
<summary>
2022-09-08: Let's talk about the GOP's paper plots.... (<a href="https://youtube.com/watch?v=ZE6pGS3ZgfI">watch</a> || <a href="/videos/2022/09/08/Lets_talk_about_the_GOP_s_paper_plots">transcript &amp; editable summary</a>)

Beau explains the Republican Party's scheme to subvert the Constitution, showcasing their weakness and lack of support, a sign that progressives are winning.

</summary>

"They call themselves patriots while trying to find some way to skirt the constitution, which undoubtedly is their profile picture."
"It's a sign that progressives are winning."
"It's not a scary thing."

### AI summary (High error rate! Edit errors on video page)

Talks about the idea of the Republican Party having a plot to subvert the US Constitution through a constitutional convention.
Explains that even if the amendments were pushed through, they still need to be ratified by 38 states, which is unlikely.
Points out that the Republicans are resorting to this scheme because they lack the power and support to achieve their legislative goals through normal means.
Emphasizes that the fixation on quasi-legal theories shows the weakness of the Republican Party.
Mentions that if the Republicans manage to subvert the Constitution and push through amendments, it won't hold as they lack the consent of the governed.
Draws parallels with Prohibition as an example of an unpopular amendment that eventually went away.
States that progressives shouldn't fear this Republican tactic but should be prepared to counter it, as it indicates that progressives are winning.
Notes that the Republicans' attempts to skirt the Constitution alienate more people and reveal their true intentions of wanting to rule rather than represent.
Concludes by stating that this situation is a sign of progressives winning and that it's not something to fear but rather a display of the Republicans' disconnect from the majority of Americans.

Actions:

for progressive activists,
Be aware of the Republican Party's tactics and be prepared to counter them (implied).
</details>
<details>
<summary>
2022-09-08: Let's talk about Memphis and how it can change.... (<a href="https://youtube.com/watch?v=RTXgX23RmnE">watch</a> || <a href="/videos/2022/09/08/Lets_talk_about_Memphis_and_how_it_can_change">transcript &amp; editable summary</a>)

Beau suggests utilizing existing technology, like sending texts for alerts, to potentially alter outcomes in recurring incidents, criticizing the lack of action and prioritization of rhetoric over practical solutions.

</summary>

"What if after that first or that second incident yesterday, a text went out to the entire city telling them what was happening?"
"Those tasked with governing seem incapable of applying the simplest things to help the action."
"There will be another one. And probably another one."
"It will do the right thing after it tries everything else first and ignores the problem for years."

### AI summary (High error rate! Edit errors on video page)

Recounts a familiar story that plays out too often, leading to questions and statements that result in little change.
Raises the possibility of a different outcome by utilizing existing technology after an incident in Memphis.
Suggests sending a text to the entire city to alert them of what was happening, potentially altering the course of events.
Points out that a bill addressing this technology was proposed in the House but faced pushback.
Criticizes individuals who may express regret and send thoughts and prayers but resist practical solutions when presented.
Notes that the bill might still be pending in the Senate Judiciary Committee.
Emphasizes that simple actions, like utilizing technology for alerts, could significantly impact outcomes but are not being implemented.
Criticizes politicians for prioritizing rhetoric over implementing tools that could save lives.
Anticipates future similar incidents until necessary actions, like sending texts for alerts, are put in place.
Concludes with a reflection on the government's tendency to delay action until exhausting other options.

Actions:

for advocates for change,
Advocate for the implementation of alert systems utilizing existing technology (suggested)
</details>
<details>
<summary>
2022-09-08: Let's talk about Bannon, New York, and developments.... (<a href="https://youtube.com/watch?v=wAj9uMnejrM">watch</a> || <a href="/videos/2022/09/08/Lets_talk_about_Bannon_New_York_and_developments">transcript &amp; editable summary</a>)

Beau outlines Bannon's new indictment, his framing of it as a political hit job, and the potential impact on the far right base depending on Trump's reaction.

</summary>

"It's a weaponized criminal justice system."
"His arrest is likely to energize the far right."
"This is a moment where Trump might come out and have a reaction to this that really hurts Republicans."
"His reaction is going to definitely steer the far right reaction."
"But it's likely that this may end up becoming the dominant story over the next few days."

### AI summary (High error rate! Edit errors on video page)

Bannon is expected to surrender to face a new indictment related to the build the wall fundraising efforts, but the details of the indictment are sealed.
Steve Bannon is framing the indictment as a political hit job, fueling the far right's views on a politicized criminal justice system.
Bannon's arrest is likely to energize the far right, viewing it as an attack on their people through the justice system.
The reaction and statements from Trump regarding Bannon's case will significantly influence how the far right base responds.
Republican strategists hope that Trump remains quiet as his reaction could either motivate or harm the far right base, impacting the midterm strategy.
Bannon is also facing two counts of contempt, adding to the legal issues he is dealing with.
The unfolding details of Bannon's case are expected to dominate news cycles in the coming days as more information emerges.

Actions:

for politically engaged individuals,
Stay informed about the unfolding details of Bannon's case (implied).
</details>
<details>
<summary>
2022-09-07: Let's talk about Trump's nuclear documents.... (<a href="https://youtube.com/watch?v=Z7Usf39ngps">watch</a> || <a href="/videos/2022/09/07/Lets_talk_about_Trump_s_nuclear_documents">transcript &amp; editable summary</a>)

Beau warns about the dire consequences of sensitive information on foreign defense capabilities getting into the wrong hands, stressing that there are no good scenarios, just varying degrees of very bad.

</summary>

"There's no good scenario here. It's all bad and it's all very bad."
"Whether or not this information got out is what has to be determined."
"There are no good options. This is all bad."
"There's a reason this information is kept secret and locked off and not shared."
"If you lose a library book, a war doesn't start."

### AI summary (High error rate! Edit errors on video page)

Documents recovered at Trump's place detailed a foreign country's defense capabilities, including nuclear capabilities.
There are no good scenarios, just varying degrees of very bad.
The documents could be about an allied nation, an opposition nation, or a country trying to develop nuclear weapons.
If the information falls into the wrong hands, it could lead to disastrous consequences.
The U.S. prepares estimates about allies, and this information is generally shared intentionally.
If this information gets out, allies may withhold information from the U.S., undermining national security.
If the documents are about an opposition nation, they could alter their intent based on the U.S.' assessment.
Countries attempting to develop nuclear weapons might face war if this sensitive information is exposed.
Losing this information is not like misplacing a library book; it could have severe consequences.
The secrecy surrounding this information is for a critical reason.

Actions:

for national security officials,
Determine if sensitive information has been compromised and take immediate steps to secure it (implied).
Ensure strict protocols for handling and storing classified information to prevent unauthorized access (implied).
Stay informed and advocate for transparency and accountability in national security matters (implied).
</details>
<details>
<summary>
2022-09-07: Let's talk about New Mexico, Jan 6, and the 14th Amendment.... (<a href="https://youtube.com/watch?v=wQJbU9frJ-4">watch</a> || <a href="/videos/2022/09/07/Lets_talk_about_New_Mexico_Jan_6_and_the_14th_Amendment">transcript &amp; editable summary</a>)

A county commissioner faces removal under the 14th Amendment for his actions on January 6th, potentially setting a precedent for future cases and attracting political attention.

</summary>

"No person shall be a Senator or Representative in Congress, or Elector of President and Vice President, or hold any office, civil or military, under the United States or under any State, who having previously taken an oath as a Member of Congress, or as an Officer of the United States, or as a Member of any State Legislature, or as an Executive or Judicial Officer of any state to support the Constitution of the United States shall have engaged in insurrection or rebellion against the same, or given aid or comfort to enemies thereof."
"Griffin incited, encouraged, and helped to normalize the violence on January 6th."
"This case could set the tone for other cases later and it's definitely something we need to keep an eye on."

### AI summary (High error rate! Edit errors on video page)

Story from Otero County, New Mexico involving a county commissioner and the 14th Amendment to the U.S. Constitution.
Commissioner Coy Griffin being removed from office for actions on January 6th at the Capitol.
Griffin, founder of Cowboys for Trump, involved in refusing to certify election results.
14th Amendment prohibits holding office if engaged in insurrection against the U.S. Constitution.
Judge ruled Griffin incited violence on January 6th, contributing to delaying election certification.
Courts will need to decide if January 6th events constitute insurrection or rebellion under the Constitution.
Likely appeal in Griffin's case, potentially impacting future similar cases.
Griffin's charges currently minor, with a jail sentence of around 14 days served.
Case could set a precedent for future cases and raise political attention.
Possibility of courts ruling rhetoric leading to violence as aiding and comforting.

Actions:

for legal enthusiasts, political analysts,
Keep an eye on the legal proceedings and outcomes surrounding cases related to the January 6th events (implied).
</details>
<details>
<summary>
2022-09-07: Let's talk about 370 cops, 100 active duty, and 80 politicians.... (<a href="https://youtube.com/watch?v=Jwp-TeJOPSI">watch</a> || <a href="/videos/2022/09/07/Lets_talk_about_370_cops_100_active_duty_and_80_politicians">transcript &amp; editable summary</a>)

Analyzing the numbers of identified individuals in the oath keepers' roster reveals lower percentages than expected, prompting a call for perspective and accurate analysis over perceived capability.

</summary>

"These numbers are low. Even if you were to take the standard math when it comes to active versus sympathizer and all of that stuff and multiply it, 3,700 cops nationwide, it's actually not as influential an organization as a lot of people may think."
"The recruitment efforts of this group were specifically catered to reach a certain group of people. Going by what we see here that recruitment effort didn't succeed."
"Perceived capability is being substituted for an accurate analysis of actual capability."

### AI summary (High error rate! Edit errors on video page)

Analyzing the numbers of 370 current cops, 100 active service members, and 80 public officials identified in the roster of the group, the oath keepers.
The group had 38,000 names on its roster, so the percentages are not as high as they may seem.
Questions if the numbers should be considered high, given the target demographic of the group.
Considers that the numbers are low, even if you apply standard math to estimate a higher count.
Raises the importance of analyzing former law enforcement and military members to get a clearer picture.
Expresses surprise that the numbers weren't higher, considering the target demographic and recruitment efforts.
Mentions recent news about the arrest of a longtime attorney for the group and efforts to delay trials.
Suggests that perceived capability of such groups may not match their actual influence.
Calls for perspective on the numbers and the need for lower figures.
Concludes with a reminder to keep these factors in mind.

Actions:

for concerned citizens,
Analyze the demographics and percentages of former law enforcement and military members related to groups like the oath keepers (suggested).
Maintain awareness and vigilance regarding recruitment efforts by extremist groups (implied).
</details>
<details>
<summary>
2022-09-06: The roads to a Twitter Q&A (September 2022) (<a href="https://youtube.com/watch?v=P1MgOHVcZnQ">watch</a> || <a href="/videos/2022/09/06/The_roads_to_a_Twitter_Q_A_September_2022">transcript &amp; editable summary</a>)

Be engaged, find passion, and resist fascism with Beau's insightful Q&A responses on various topics.

</summary>

"Find what you're good at, what you're passionate about, and use that because you're going to be better at that than anything else."
"Panic is the inability to affect self-help. You never want to be in that situation."
"Nobody's born and wants to have their identity controlled by the state and wants everything that they get to think, do, or say determined by a massive national government."

### AI summary (High error rate! Edit errors on video page)

Beau answers Twitter questions, providing live responses and insights on various topics ranging from nuclear documents to history book recommendations.
He shares his thoughts on a potential Constitutional Convention, imitating a train, high school history classes, and favorite anime.
Beau offers advice on studying history, suggests positive news channels, and addresses ethical questions about pet ownership.
He talks about Liz Truss, VOSH, community organizing, defeating anxiety attacks, and advocating for unionizing.
Beau touches on automation, defeating fascism, NATO's charter, Batman's ethics, and dealing with political fallacies.
He shares insights on democracy, civil war avoidance, media leanings, and the importance of electoral engagement.
Beau comments on sleep habits, favorite snacks, t-shirts, and political analysis strategies.
He offers advice on navigating societal conflicts, engaging with right-wing media, and countering fascism.

Actions:

for active citizens,
Join community organizations to advocate for unionizing (implied)
Engage in disaster relief efforts or support those who are actively involved in it (implied)
Support positive news channels like PLN to stay informed and spread good news (implied)
</details>
<details>
<summary>
2022-09-06: Let's talk about the other Las Vegas.... (<a href="https://youtube.com/watch?v=iLX3Y8ICLls">watch</a> || <a href="/videos/2022/09/06/Lets_talk_about_the_other_Las_Vegas">transcript &amp; editable summary</a>)

Las Vegas, New Mexico faces water crisis due to drought, fire, and contamination, underscoring the urgent need for upgraded infrastructure across the US.

</summary>

"When you hear politicians push back on infrastructure spending, this is the result."
"Any stress will cause it to fail. We're seeing it all over the country."
"Stresses on the infrastructure will cause it to fail, which is what's happening here."
"Sometimes the biggest hurdle to overcome are the local politicians or the state-level politicians."
"This is going to be an ongoing problem in the United States."

### AI summary (High error rate! Edit errors on video page)

Las Vegas, New Mexico, faces a water crisis with less than three weeks of supply due to drought, fire, and water contamination.
A massive fire and subsequent monsoon contaminated the city's reservoir with a gray sludge, rendering it useless and limiting the water supply.
When trying to disinfect water, the city uses chlorine, which, when mixed with carbon in high quantities, can create a carcinogenic byproduct.
The city is testing a temporary plan to disinfect water from a nearby lake without causing harm but faces challenges in distribution logistics.
Upgraded infrastructure, especially in water distribution, is needed across the United States to prevent similar crises.
Addressing infrastructure issues now can prevent the need for the National Guard to deliver water in emergencies due to failing systems.
Lack of investment in infrastructure by local and state-level politicians leads to crumbling basic systems across the country.
Stress on outdated infrastructure leads to failures, showcasing the urgent need for improvements and investments.
Beau urges citizens to recognize and push for infrastructure investments to avoid widespread crises.
The situation in Las Vegas, New Mexico, is a stark reminder of the consequences of neglecting infrastructure maintenance and upgrades.

Actions:

for local residents, community activists,
Advocate for infrastructure upgrades in your community (implied)
Support politicians who prioritize infrastructure investment (implied)
Stay informed about local infrastructure issues and push for improvements (implied)
</details>
<details>
<summary>
2022-09-06: Let's talk about Cara Mund and North Dakota.... (<a href="https://youtube.com/watch?v=pIC9kjKN8-c">watch</a> || <a href="/videos/2022/09/06/Lets_talk_about_Cara_Mund_and_North_Dakota">transcript &amp; editable summary</a>)

Mund's entry as an independent in the North Dakota House race challenges the Republican stronghold, appealing to disaffected voters seeking choice and change.

</summary>

"You can't buy press like that."
"It is not impossible."
"There are people there who want a wider array of options."
"It's probably going to be a whole lot easier to vote for somebody with an I after their name."
"This is a case where people underestimating her may work out to her advantage in a big way."

### AI summary (High error rate! Edit errors on video page)

Follow-up on a U.S. House of Representatives race in North Dakota, where an independent, Mund, entered the race, changing the dynamics.
The Democratic nominee suspended their campaign, leaving Mund to potentially attract disaffected Republicans.
Mund could appeal to Republicans seeking choice and those tired of recent Republican Party actions.
With name recognition, celebrity status, and news of the Democratic nominee's campaign suspension, Mund has an advantage.
North Dakota, a red state, still has residents interested in progressive ideas and options.
Approximately 17,000 people in North Dakota watched Beau's channel in the last 28 days.
Mund positioned herself to provide a wider array of options for voters.
Although a long shot, Mund has a chance to win the House seat as an independent.
Republican Party should be concerned about potentially losing the race in North Dakota due to changing dynamics.
Mund's underestimation may work to her advantage in the election.

Actions:

for voters in north dakota,
Support Mund's campaign by volunteering or donating (suggested)
Spread awareness about Mund and her platform within your community (implied)
</details>
<details>
<summary>
2022-09-06: Let's talk about CNN.... (<a href="https://youtube.com/watch?v=ctR1QJsCqFM">watch</a> || <a href="/videos/2022/09/06/Lets_talk_about_CNN">transcript &amp; editable summary</a>)

CNN's uncertain changes spark concerns about potential shifts towards bias and audience appeals, with skepticism on the network's future strategies.

</summary>

"There's been some messaging indicating that they are concerned about a perception of bias on the outlet."
"So a shift right is not going to help that perception."
"If they're trying to appeal to moderates and conservatives, by definition, they're not unbiased."
"CNN was in the middle. If they shift right, they are likely to lose more than they gain."
"CNN is going through some changes. What the final result will be, we don't know yet."

### AI summary (High error rate! Edit errors on video page)

CNN is undergoing changes in terms of personalities and content, but the exact end goal is unknown.
There are speculations that CNN might be trying to correct a perception of bias or appeal to moderates and conservatives.
One option is for CNN to become like AP for TV, focusing on just the facts reporting.
Another option is to have political commentary from across the spectrum, but this may not be financially successful.
Shifting right to appeal to moderates and conservatives could indicate a belief that the political landscape in the US has shifted right.
Beau expresses skepticism about CNN's potential shift to the right and its long-term impact on the network's credibility.
Trying to capture a conservative demographic may not be successful and could harm CNN in the long run.
Beau believes that transitioning towards unbiased reporting like AP for TV could be a positive move.
Beau warns against presenting both sides of issues that do not have two sides, as it can distort the narrative.
The future direction of CNN remains uncertain, and there are concerns about the potential changes.

Actions:

for media analysts,
Monitor CNN's changes and analyze the impact on their reporting and audience perception (implied).
Share insights and concerns about media bias and political leanings with peers and online communities (implied).
</details>
<details>
<summary>
2022-09-05: Let's talk about the prisoner's dilemma of Trump and the GOP.... (<a href="https://youtube.com/watch?v=s43npzuSeKQ">watch</a> || <a href="/videos/2022/09/05/Lets_talk_about_the_prisoner_s_dilemma_of_Trump_and_the_GOP">transcript &amp; editable summary</a>)

Donald Trump and the Republican Party face a trust dilemma as they navigate their intertwined fates while balancing conflicting interests.

</summary>

"Each one needs the other to do something but they also have to trust the other person to do it while doing something against their own self-interest."
"The only real way forward for Trump and for the Republican Party is for them to trust each other while distancing from each other."
"It's entertaining on some levels and sad on others."

### AI summary (High error rate! Edit errors on video page)

Donald Trump and the Republican Party are in a prisoner's dilemma where they need each other but also have conflicting interests.
Trump wants public support to shield him from legal issues, hence his desire for rallies to stay in the public eye.
The Republican Party could potentially ease Trump's legal troubles if they gain more power by exerting political pressure.
However, for the Republicans to gain power, Trump needs to trust them to help him, which means he must take a back seat and stop his rallies.
There is a lack of trust between Trump and the Republican Party, with both parties having reasons to doubt each other's intentions.
The GOP appears more like a mafia drama than a political institution, with loyalty dependent on poll numbers and investigations.
Republicans want Trump to fade away, but there's a fear that if asked to step back, Trump might retaliate by starting a third party.
The dilemma requires trust between Trump and the GOP while simultaneously distancing from each other, which seems unlikely to occur.

Actions:

for politically engaged individuals,
Build trust through open communication and transparency between political figures (suggested)
Encourage accountability and ethical behavior within political parties (implied)
</details>
<details>
<summary>
2022-09-05: Let's talk about Trump getting his special master.... (<a href="https://youtube.com/watch?v=Jl2OaxDJd2g">watch</a> || <a href="/videos/2022/09/05/Lets_talk_about_Trump_getting_his_special_master">transcript &amp; editable summary</a>)

Trump's request for a special master leads to a delay tactic with minimal impact on the case, raising questions about executive privilege and potential consequences for concealing records.

</summary>

"I mean, you can make of that what you will."
"This is a delay tactic."
"It's just a thought."
"It's going to be made out to be a big thing because it involves Trump and his legal case, but all it does is delay it."
"To me, this doesn't really make any sense."

### AI summary (High error rate! Edit errors on video page)

Trump's request for a special master has been granted, where a third party will sort out privileged material, likely to be appealed by the Department of Justice.
The impact of this decision won't be substantial on the case, just a delay tactic.
Beau questions the argument of executive privilege when some of the documents being sought are presidential records.
The Presidential Records Act lacks enforcement mechanisms, defining presidential records as property of the U.S. government.
Beau mentions the consequences for concealing or destroying records filed with the court or a public office.
He hopes no presidential records belonging to the people are found by the special master.
The delay tactic doesn't seem wise to Beau, who acknowledges he's not a lawyer.
The intelligence and counterintelligence review is ongoing and unaffected by this development.
Most intelligence agencies lack the mandate for domestic investigations, a task usually handled by the FBI.
Beau believes the delay caused by this decision may not go beyond the midterms, which could be Trump's goal.
Trump's desire for a special master might stem from wanting to appear influential like his friends who had similar setups in their cases.
The Department of Justice likely has sufficient evidence for indictment, potentially leading to pretrial confinement, possibly home confinement for Trump as a former president.
Trump's delaying tactics could elongate any potential pretrial confinement period.
Beau anticipates appeals back and forth, with the Trump team aiming for extensive delays.

Actions:

for legal observers,
Stay informed on legal developments and implications (implied)
Support transparency and accountability in legal proceedings (implied)
</details>
<details>
<summary>
2022-09-05: Let's talk about Labor Day's origins.... (<a href="https://youtube.com/watch?v=Yxt6oOZyBgI">watch</a> || <a href="/videos/2022/09/05/Lets_talk_about_Labor_Day_s_origins">transcript &amp; editable summary</a>)

Labor Day origins, labor movement celebration, and ongoing worker empowerment emphasized by Beau.

</summary>

"Labor Day is for you. It's for every worker in the United States."
"The labor movement is still alive and well."
"It's not history. It's not a moment. It's a movement."

### AI summary (High error rate! Edit errors on video page)

Labor Day origins in September 1882, national holiday in 1894 by Grover Cleveland.
Connected to leftist worker-oriented thought during the Second Industrial Revolution.
Labor Day in September to avoid association with May Day and leftist ideologies.
Debate on who created Labor Day - Peter J. McGuire or Matthew McGuire.
Imagery associated with Labor Day: industrial jobs, blue-collar workers, and Second Industrial Revolution.
Questioning the purpose of Labor Day: honoring labor workers or the labor movement?
Honoring the labor movement includes figures like Blair Mountain, Eugene Debs, Cesar Chavez, and more.
Labor movement achievements include the weekend, end to child labor, 40-hour work week, and more.
Labor Day is for every worker in the United States, not just industrial workers.
Importance of celebrating the worker of today and being part of the ongoing labor movement.

Actions:

for american workers,
Join a union for collective bargaining (implied)
Advocate for better working conditions and standard of living (implied)
Educate others on the importance of the labor movement (implied)
</details>
<details>
<summary>
2022-09-04: Let's talk about the loneliest man on Earth.... (<a href="https://youtube.com/watch?v=gHAoHK4C_2s">watch</a> || <a href="/videos/2022/09/04/Lets_talk_about_the_loneliest_man_on_Earth">transcript &amp; editable summary</a>)

Beau talks about the loneliest man in the world, a victim of genocide by cattle ranchers, shedding light on ongoing global struggles faced by indigenous communities.

</summary>

"The deliberate wiping out of an entire people by cattle ranchers hungry for land and wealth."
"Resistance. We can only imagine what horrors he had witnessed in his life and the loneliness of his existence after the rest of his tribe were killed."
"It's happening now."
"The loss to humanity of these cultures, it's still something that's occurring."
"This type of fight, it's not over and it won't be over for a very very long time."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of the man known as "the man of the hole" or the "loneliest man in the world."
Describes how the man lived in the Amazon alone for 25 years after his community was attacked by cattle ranchers.
Mentions the last attack on his community in 1995, when he became the sole survivor and eventually died.
Points out the lack of knowledge about the man's community and customs due to failed contact attempts.
Quotes Fiona Watson from Survival International about the deliberate genocide of the man's people by cattle ranchers for land and wealth.
Emphasizes the violence and cruelty inflicted on indigenous peoples globally in the name of colonization.
Talks about how the man resisted contact and just wanted to be left alone.
Mentions Survival International's advocacy for uncontacted or recently contacted indigenous groups worldwide.
Points out that similar situations exist globally, not just in the past.
Addresses ongoing cultural assimilation and inequitable treatment of indigenous communities in the US and worldwide.

Actions:

for advocates, activists, community members,
Support organizations like Survival International that advocate for uncontacted or recently contacted indigenous groups (suggested)
Educate yourself and others about the ongoing struggles faced by indigenous communities globally (implied)
</details>
<details>
<summary>
2022-09-04: Let's talk about the US military running low.... (<a href="https://youtube.com/watch?v=cX4PzAxYtiw">watch</a> || <a href="/videos/2022/09/04/Lets_talk_about_the_US_military_running_low">transcript &amp; editable summary</a>)

Beau clarifies that while the US military is low on a specific type of ammo used in Ukraine, it is not running out overall, maintaining readiness and planning to restock soon.

</summary>

"Our version of low is more than most countries have."
"This isn't a system-wide thing where the US is running out of ammo."
"It's time to order more."
"The United States will never run out of ammo."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses the question of whether the US military is running out of supplies, specifically ammo.
Clarifies that the US military is not running out of ammo but is running low on a specific type, 155 millimeter shells used in M777 howitzers.
Explains that the US has provided over 800,000 rounds of this specific ammunition to Ukraine, causing the inventory to dip below the preferred amount.
Compares the situation to managing inventory at a job, where running low prompts the need to order more.
Emphasizes that this shortage is not a significant issue and that the US military will order more to replenish supplies.
States that despite the low inventory, the US military's readiness is maintained and there is no system-wide problem.
Assures that the United States will never completely run out of ammo and that this shortage is specific to one type used frequently in training or operations.

Actions:

for military personnel, supporters, concerned citizens,
Contact military support organizations for ways to contribute (exemplified)
Stay informed about military supply situations (implied)
</details>
<details>
<summary>
2022-09-04: Let's talk about climate refugees in the US today.... (<a href="https://youtube.com/watch?v=orkoucmkteY">watch</a> || <a href="/videos/2022/09/04/Lets_talk_about_climate_refugees_in_the_US_today">transcript &amp; editable summary</a>)

Beau warns about the real impacts of climate change through the relocation of Isle de Jean Charles residents, now climate change refugees, and calls for urgent attention and action as these situations worsen.

</summary>

"We now have official within the United States climate change refugees that were assisted in their move by the federal government."
"This won't be the last coverage about a group of people who had to move because their home became uninhabitable."
"This has to be on the ballot because it's gonna get worse."

### AI summary (High error rate! Edit errors on video page)

Talks about the impacts of climate change that are already affecting communities.
Mentions Isle de Jean Charles, an island off the coast of Louisiana inhabited by a native group.
Describes how the residents of Isle de Jean Charles are being relocated to a new development called New Isle due to their island disappearing underwater.
Explains that New Isle came into being through a $48 million grant from HUD in 2016.
Notes that Louisiana loses an area the size of Manhattan every year due to coastal erosion.
Points out that the residents of Isle de Jean Charles are now considered climate change refugees.
Emphasizes that this won't be the last story of people being forced to move due to climate change impacts.
Urges for attention and action on climate change to prevent such situations from worsening.
Concludes by stressing the importance of keeping climate change issues at the forefront of our concerns.

Actions:

for climate activists, policymakers, community organizers,
Advocate for policies addressing climate change impacts (implied)
Support and amplify voices of communities affected by climate change (implied)
Take action to reduce carbon footprint and mitigate climate change effects (implied)
</details>
<details>
<summary>
2022-09-03: Let's talk about preparing for the next 2 months.... (<a href="https://youtube.com/watch?v=SAmOn63QjoE">watch</a> || <a href="/videos/2022/09/03/Lets_talk_about_preparing_for_the_next_2_months">transcript &amp; editable summary</a>)

Beau provides strategies to manage news-induced anxiety by anticipating upcoming events and combining current news consumption with historical context to gain a clearer perspective and reduce stress levels.

</summary>

"History doesn't repeat but it rhymes."
"Establish a frame of what you expect to happen."
"Combine consuming news with reading history."
"You really do end up with a very fuzzy crystal ball."
"That little bit of foresight makes staying informed a little less stressful."

### AI summary (High error rate! Edit errors on video page)

The speaker addresses the overwhelming feeling that comes from following the news, especially with the fast-paced nature of current events.
Beau gives tips on how to manage the anxiety that stems from constant news updates by preparing for what is likely to happen.
Providing examples of upcoming events like political scandals, Trump's reactions, hearings, and climate news, Beau encourages viewers to anticipate these plot points to reduce surprise and stress.
By combining current news consumption with historical context, Beau suggests that individuals can gain a better understanding of what to expect and reduce the stress of unpredictable events.
Beau ends by reminding the audience that having a historical perspective can act as a fuzzy crystal ball, making staying informed a less stressful experience.

Actions:

for news consumers,
Prepare for upcoming events by staying informed about historical context (implied).
Anticipate potential plot points in current events to reduce surprise and stress (implied).
Combine news consumption with reading history for a clearer perspective (implied).
</details>
<details>
<summary>
2022-09-03: Let's talk about a Bossier City cop.... (<a href="https://youtube.com/watch?v=rCSj-oqU9q8">watch</a> || <a href="/videos/2022/09/03/Lets_talk_about_a_Bossier_City_cop">transcript &amp; editable summary</a>)

A cop's arrest involving controlled substances sparks interest, but initial allegations hint at personal addiction rather than widespread corruption within the union.

</summary>

"It looks just like a very typical story when it comes to this type of thing."
"And I think that's really what people want to know."

### AI summary (High error rate! Edit errors on video page)

Story from Bozier City, Louisiana, involving a cop who is also the president of the police union, taken into custody by the feds.
Initial reports suggested the investigation was related to the union and fundraising, but the affidavit reveals it's about controlled substances, specifically painkillers.
Sergeant Harold B.J. Sanford has not been indicted yet, charged via complaint based on allegations in the affidavit.
Transcripts of text messages in the affidavit indicate someone was obtaining and selling pills to the cop, potentially tied to using union funds to support an alleged habit rather than widespread corruption.
No evidence in the current information points to a larger corruption scheme within the union; it seems more like a typical addiction scenario involving a cop in a position of authority.
Beau hints at the possibility of undisclosed angles in the case, considering the lack of transparency in legal documents.
Despite ongoing monitoring of the situation, it doesn't seem likely that there will be a significant investigation into the union or police department.
Beau concludes with the observation that people are most interested in whether there will be a deep dive into the union itself or the police department.

Actions:

for community members, curious individuals,
Stay informed on the developments of the case and any potential investigations (suggested)
Support efforts for transparency and accountability within law enforcement (implied)
</details>
<details>
<summary>
2022-09-03: Let's talk about Karla Hernández-Mats, Crist's running mate.... (<a href="https://youtube.com/watch?v=oOprnHGbrjo">watch</a> || <a href="/videos/2022/09/03/Lets_talk_about_Karla_Hern_ndez-Mats_Crist_s_running_mate">transcript &amp; editable summary</a>)

In the Florida governor's race, Chris' running mate, Carla Hernandez, a teacher and union leader, may shift the focus to family planning, education, and labor, potentially swinging votes in the traditionally viewed red state.

</summary>

"The successful line of attack against DeSantis as being family planning, education, and labor."
"She's a teacher. She's very energetic."
"She has a strong background if that's what they're going to run on."
"It's an interesting choice."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Overview of the Florida governor's race dynamics, comparing DeSantis and Christ as wild authoritarian versus centrist.
Introduction of Chris' running mate, Carla Hernandez, president of United Teachers of Dade, signaling a focus on family planning, education, and labor.
Hernandez's background as a teacher and union leader with immigrant parents from Honduras.
Speculation on the potential line of attack against Hernandez by the Republican Party.
Mention of a tweet involving Castro that the Republican Party tried to use to paint Hernandez as a communist.
Reference to Hernandez's humanitarian work overseas with limited details available.
The possibility of Hernandez swinging votes in Florida, traditionally viewed as a red state.

Actions:

for florida voters,
Contact education organizations and unions to support Carla Hernandez (suggested)
Stay informed about the Florida governor's race and the candidates' platforms (implied)
</details>
<details>
<summary>
2022-09-02: Let's talk about NASA crashing a spacecraft.... (<a href="https://youtube.com/watch?v=mAenSVO3E8Y">watch</a> || <a href="/videos/2022/09/02/Lets_talk_about_NASA_crashing_a_spacecraft">transcript &amp; editable summary</a>)

NASA's spacecraft is set to collide with an asteroid as part of a planetary defense test, showcasing a proactive approach to potential future threats.

</summary>

"I cannot wait to see something smash into an asteroid at 15,000 miles an hour."
"This is a test. We've talked about it before on the channel."

### AI summary (High error rate! Edit errors on video page)

NASA has a spacecraft on course to collide with an asteroid as part of the Double Asteroid Redirection Test (DART).
The test aims to develop the first step in a planetary defense system to alter the course of asteroids heading towards Earth.
The current mission is a proof of concept, taking place 6.8 million miles from Earth.
The spacecraft will impact an asteroid orbiting another asteroid, with the target asteroid being 520 feet long.
The spacecraft, moving at 15,000 miles per hour, acts as a space battering ram.
This test is a precautionary measure in case an asteroid threat arises in the future.
Although there is no immediate threat, it's a proactive step for planetary defense.
The live broadcast of the test release an observation craft on September 26 at 7.14 PM Eastern.
Viewers can watch the event live on NASA's website, YouTube, Facebook, and Twitter.
Beau anticipates various theories and speculations arising from the test.

Actions:

for space enthusiasts, science lovers.,
Watch the live broadcast of the spacecraft impacting the asteroid on September 26 at 7.14 PM Eastern (suggested).
</details>
<details>
<summary>
2022-09-02: Let's talk about Greenland's ice and sea levels.... (<a href="https://youtube.com/watch?v=DuC8uGtrq9s">watch</a> || <a href="/videos/2022/09/02/Lets_talk_about_Greenland_s_ice_and_sea_levels">transcript &amp; editable summary</a>)

Greenland's "zombie ice" will inevitably raise sea levels by 10 inches, showing the urgency to mitigate climate change effects as stopping it is now unrealistic.

</summary>

"There's more than 100 trillion tons of ice that won't stay ice."
"We are past that point. We're at mitigate climate change."
"We see that train coming but we also know it doesn't have any brakes."
"This ice is still there at the moment but it is going to melt."
"If more research is conducted in this fashion, we might be able to encourage more people to join the fight to mitigate it."

### AI summary (High error rate! Edit errors on video page)

Describes a study on Greenland's ice that is essentially "zombie ice" - ice that won't stay ice because it's cut off from the process that maintains it.
The study predicts a conservative estimate of a 10-inch rise in sea levels solely from Greenland's ice, regardless of current actions taken to combat climate change.
Attributes the inevitability of this sea level rise to the rise in average temperature in Greenland over the past 40 years, about 1.5 degrees per decade.
Compares the study's prediction to Noah's estimate of a 10 to 12-inch rise in sea levels over the next 30 years.
Explains that a 10-inch rise doesn't equate to a uniform increase everywhere, as it can lead to broader flooding in some areas while causing water levels to recede in others.
Emphasizes the importance of understanding that we are now beyond stopping or avoiding climate change; instead, the focus is on mitigating its effects due to past inaction.
Points out that the longer action is delayed, the more inevitable and severe the effects of climate change become, as the impacts take time to manifest.
Suggests that studies like the one on Greenland's ice can raise awareness about the irreversible aspects of climate change and potentially encourage more people to take action to mitigate it.
Uses the analogy of a train without brakes to illustrate the imminent melting of Greenland's ice and subsequent sea level rise.
Encourages further research and studies conducted in a similar vein to bolster efforts in combating climate change.

Actions:

for climate activists and concerned citizens,
Join the fight to mitigate climate change by supporting research and studies that raise awareness (suggested)
Take concrete actions in your community to reduce emissions and adapt to climate change (implied)
</details>
<details>
<summary>
2022-09-02: Let's talk about Biden ending hunger.... (<a href="https://youtube.com/watch?v=Kde3fY3il4E">watch</a> || <a href="/videos/2022/09/02/Lets_talk_about_Biden_ending_hunger">transcript &amp; editable summary</a>)

The Biden administration aims to end hunger in the US by 2030, drawing inspiration from a 1969 conference, with a focus on providing healthy food and updating outdated systems, seen as both a humanitarian effort and a strategic move by the Democratic Party.

</summary>

"This is your shot. Take it."
"It's probably time to do so."
"This is quite literally a kitchen table issue."

### AI summary (High error rate! Edit errors on video page)

The Biden administration set a goal to end hunger in the United States by 2030, with a conference scheduled for September 28th.
The inspiration for this goal comes from a conference in 1969 that laid the groundwork for systems currently in place to address hunger.
There is pushback about the timeline, with some people feeling rushed to come up with better ideas.
Beau encourages seizing the moment and taking advantage of the unique opportunities presented by this initiative.
The focus is not just on feeding people but on providing healthy food to combat diet-related diseases.
Beau points out the prevalence of cheap, unhealthy food options and stresses the importance of updating systems to prioritize nutritious options.
Biden mentioned the impact of diet-related diseases like heart disease and diabetes on families and communities.
The lack of updates and overhauls to existing systems for a long time suggests a need for change in how we address hunger.
Beau views this initiative not only as a humanitarian effort but also as a strategic move by the Democratic Party, addressing a critical kitchen table issue.
The goal to end hunger in the United States is significant and deserves more attention and support.

Actions:

for policy advocates,
Support initiatives addressing hunger (implied)
Advocate for access to healthy food options (implied)
Stay informed and engaged with efforts to combat hunger (implied)
</details>
<details>
<summary>
2022-09-01: Let's talk about the Jan 6 hearings restarting.... (<a href="https://youtube.com/watch?v=HgR1yuUCQ8Q">watch</a> || <a href="/videos/2022/09/01/Lets_talk_about_the_Jan_6_hearings_restarting">transcript &amp; editable summary</a>)

The upcoming January 6th hearings will focus on finances and cover-up, structured to inform and reach millions, with potential for charges.

</summary>

"The Cash and the Cover-Up."
"Tell them what you're going to tell them, tell them, tell them what you told them."
"Millions of people watching it, it worked."

### AI summary (High error rate! Edit errors on video page)

The upcoming January 6th hearings will focus on "The Cash and the Cover-Up," delving into the finances and potential cover-up aspects of the events surrounding January 6th.
The committee aims to look into fundraising activities, the misuse of funds, and financing for the event itself or individuals promoting the "stop the steal" narrative.
Expect scrutiny on missing documents, Secret Service involvement, and attempts to deflect blame as part of the cover-up investigation.
The hearings are likely to structure similarly to previous ones, focusing on specific aspects and tying everything together for a broader understanding.
Previous hearings reached millions of Americans effectively, and it's expected that the committee will continue with this successful approach.
It's uncertain whether the committee will push for charges or if the hearings are purely informational; the Department of Justice is not obligated to follow their suggestions.
Stay tuned for updates on new subpoenas and how different parties will respond—cooperation versus resistance has become a common theme in the process.

Actions:

for politically engaged citizens,
Monitor updates on the January 6th hearings and stay informed on the developments (implied).
Share information from the hearings with others to increase awareness and understanding of the events (implied).
Stay engaged with the process and be prepared to take action based on the outcomes of the hearings (generated).
</details>
<details>
<summary>
2022-09-01: Let's talk about how Biden isn't done with loan forgiveness.... (<a href="https://youtube.com/watch?v=79NWEnEpy08">watch</a> || <a href="/videos/2022/09/01/Lets_talk_about_how_Biden_isn_t_done_with_loan_forgiveness">transcript &amp; editable summary</a>)

Beau explains student loan forgiveness, overlooked borrowers, potential taxes on forgiven loans, and ongoing efforts by the Biden administration.

</summary>

"The Biden administration isn't finished."
"They're not finished."
"The Biden administration isn't finished."
"The reporting about the taxes is kind of up in the air."
"I hope you all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the topic of student loan forgiveness and addresses some misconceptions in the media.
Talks about overlooked borrowers, specifically those with federal family education loans held commercially.
Mentions that the Biden administration might extend forgiveness to these loans through executive order.
Emphasizes that this potential extension is not just beneficial for the directly impacted borrowers but everyone.
Points out that Biden administration is still working on revamping education costs post the initial forgiveness announcement.
Mentions the uncertainty around whether the forgiven amount will be taxed and lists states that are undecided on taxing it.
States that New York has clarified they won't tax the forgiven amount and might amend the law accordingly.
Advises caution on assuming definite taxation on forgiven loans, as some states on the list might not tax it.
Suggests keeping an eye on further developments regarding taxation and the administration's education cost plans post-midterms.
Beau ends by reiterating that the Biden administration is not done and advises against premature budgeting based on potential tax implications.

Actions:

for students, loan borrowers,
Keep informed on updates regarding student loan forgiveness and potential tax implications (implied).
Stay engaged in understanding how policy changes may impact student loans (implied).
</details>
<details>
<summary>
2022-09-01: Let's talk about Ted Cruz, education, slackers, and risk.... (<a href="https://youtube.com/watch?v=Xpd0AnyK4N0">watch</a> || <a href="/videos/2022/09/01/Lets_talk_about_Ted_Cruz_education_slackers_and_risk">transcript &amp; editable summary</a>)

Beau questions Senator Cruz's statement on workers and education, criticizing the attempt to silence voices and devalue labor, using baristas as a prime example.

</summary>

"Talk about saying the quiet part aloud, right?"
"The labor. That barista. That slacker that you're making fun of."
"It wasn't the people in the suits. It was those slacker baristas, the people that did the work."
"What created that value. That's what built that company."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Late to discussing Ted Cruz's statement about workers, education, and baristas.
Cruz's statement had multiple wrong parts packed into a short statement.
Cruz implied that workers like baristas pose a risk if they vote.
Questioning Cruz's labeling of certain education as useless and wasted.
Criticizing Cruz for potentially discouraging education in a country with failing education.
Cruz's statement seems aimed at keeping certain workers in their place and voiceless.
Mention of kicking down at the working class through the term "slacker barista."
Beau points out the deliberate choice of using "barista" to create an "other" group.
Illustrating the value of labor through coffee shops and the role of baristas.
Emphasizing the significant revenue generated by Starbucks due to labor, including baristas.

Actions:

for educators, activists, voters,
Support local coffee shops and their workers by frequenting them regularly (exemplified)
Engage in dialogues about the value of labor and the importance of respecting all workers (implied)
</details>
