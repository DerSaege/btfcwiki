---
title: Let's talk about the hearing, ethics, and Mudge....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aPL093zXv5k) |
| Published | 2022/09/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to discussing a whistleblower and a disclosure about security collapses at Twitter.
- Mention of whistleblower Peter Zatko, known as Mudge, with a reputation for being ethical and straightforward.
- Beau's personal belief in Mudge's credibility but admits to not reading the disclosure.
- Not providing objective commentary due to personal connection with Mudge.
- Acknowledgment of hearing about the issue coming up soon and suggests following the coverage.
- Emphasizes the importance of understanding social media as critical communications infrastructure.
- Raises concerns about social media shaping beliefs and the influence of echo chambers.
- Encourages paying attention to the unfolding process and coverage of the issue.

### Quotes

- "He's MUDGE, Google him, M-U-D-G-E."
- "I have to take what he says at face value until I was presented with something that was in direct contradiction."
- "I definitely suggest paying attention to the coverage of it."

### Oneliner

Beau raises awareness about a whistleblower's disclosure on Twitter security collapses, stressing the importance of understanding social media's influence.

### Audience

Social media users

### On-the-ground actions from transcript

- Follow the coverage of the whistleblower's disclosure (suggested)
- Pay attention to the unfolding process (suggested)

### Whats missing in summary

Insights on the potential impacts of social media influence and echo chambers could be further explored by watching the full transcript.

### Tags

#Whistleblower #Twitter #SocialMedia #Influence #EchoChambers


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about something
that I was kind of trying not to talk about.
It's something that's very much in my will house,
and a lot of questions came in about it.
And my total radio silence on the issue
just prompted more questions.
So today, we'll talk about it a little bit
at the absolute last moment.
OK, so for those who don't know, recently a whistleblower
by the name of Peter Zatko went through the process
and a disclosure was created.
And it had to do with perceived security collapses at Twitter.
Some of the stuff in the disclosure
related to the possibility of information operations,
foreign intelligence, influence, that kind of stuff. Stuff we talk about on this channel a lot. We talked about it this
week.
So, there were questions. Now, the main reason I wasn't talking about it was because of the name.
Peter Zatko. I gotta be honest, I had a real hard time placing it. And the reason is because that's not what I have him
in my phone as.  Mudge. Mudge is his nickname.
I know the whistleblower, I know Mudge, I like Mudge, I talked to Mudge.
Most importantly, if Mudge told me my beard was on fire while we were scuba diving, I
would believe him.
He has a reputation for being incredibly ethical and very straightforward to a fault almost.
Given his reputation and my personal interactions with him, I believe Mudge.
And I haven't read it.
I haven't read the disclosure and I haven't talked to him about any of the details of
any of this.
So I'm not going to be very objective in providing commentary on it.
It's worth noting a couple of things.
One, in my experience with him, he has never been petty or vindictive or anything like
that.
He is not looking for fame.
He's not a newbie trying to make his name.
He's MUDGE, Google him, M-U-D-G-E.
He's been around a long time.
He's at the top of his field.
Given what I know about him, I have to believe that if he went through the pretty tedious
process of becoming a whistleblower through this method, he felt he had to, he
felt he had an ethical responsibility to do so. Now, two other little tidbits about
this. One, the hearing is tomorrow. I would imagine that it would be worth
your time to follow the coverage of it. My guess is that there will be a lot of
discussion that is actually well beyond what I would normally talk about. And
then two, it's important to understand that he's been around a long time and
people who would normally talk about something like this, especially those
who would provide commentary on cyber security, they may know him as well.
Some of them may look to him as a mentor.
If he had any influence over them at all, they're probably also very ethical.
And they might look at providing commentary on something that they have a personal involvement
with in one way or another as unethical.
I wouldn't take people who would normally discuss something like this not talking about
it as them saying that it's not important or that they don't believe him or anything
like that because in all likelihood it's probably demonstrating the exact opposite.
Given my experience, I have to take what he says at face value until I was presented
with something that was in direct contradiction.
That's where I'm at.
So this is why I haven't covered it.
One thing that I think is going to come out of this as this process unfolds is I think
that people are going to have a greater understanding of how social media has become critical infrastructure.
critical communications infrastructure. And it's something that is so complete
and so present in your environment, in everybody's environment, that a lot of
what you believe is shaped through what appears on social media. And those echo
chambers that we talk about on this channel, the information silos that can
be created. That kind of influence, if steered, it could be really bad. And I think that the
process that's going to unfold is probably going to highlight a lot of that. So I definitely
suggest paying attention to the coverage of it. So anyway, it's just a thought. Y'all
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}