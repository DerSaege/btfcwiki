---
title: Let's talk about all those subpoenas about Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TG6Wi2vsrPs) |
| Published | 2022/09/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice sent out 30 to 40 subpoenas last week, targeting a range of individuals.
- Subpoenas request broad information on post-election actions of the Trump administration.
- Recipients were compelled to appear before a grand jury and had their phones searched.
- Information requested includes communications with Trump allies like Giuliani and Powell.
- Recipients were asked to provide information on any members of the executive or legislative branches involved in obstructing the election certification.
- Senate staffers might be feeling nervous due to the wide-reaching subpoenas.
- The investigation keeps expanding due to the complex nature of the case.
- Staffers may have access to significant information due to their roles.
- Seizing phones indicates the seriousness of the investigation.
- The case seems to be targeting not only Trump's inner circle but also other political figures involved in altering election outcomes.

### Quotes

- "There's a seditious underground parking garage that they need to go through next."
- "Some of these people, they're staffers, and they are gonna have access to a lot more information than people are gonna give them credit for."
- "Taking the phones, that's pretty significant."
- "They are kind of building a case that appears now to be actively targeting not just people that are seen as Trump's inner circle, but possibly other political figures."
- "There will probably be more information flowing out pretty regularly between now and September 23rd."

### Oneliner

The Department of Justice's wide-reaching subpoenas target post-election actions, potentially implicating Trump allies and political figures, with a focus on obstruction.

### Audience

Journalists, Activists, Legal Observers

### On-the-ground actions from transcript

- Stay updated on developments in the investigation and share information with your community (suggested).
- Support transparency and accountability in legal proceedings by discussing the implications of these subpoenas with others (suggested).

### Whats missing in summary

Insights into the potential implications on the political landscape and future accountability measures.

### Tags

#DOJ #Subpoenas #TrumpAdministration #ElectionObstruction #LegalProceedings


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're gonna talk about
what the Department of Justice was up to last week
because apparently they had a busy week.
And depending on the reporting,
it deals with 30 or 40 people, somewhere in that range.
So we will talk about what we know and what we don't know
about the 30 to 40 subpoenas
that the Department of Justice sent out.
What we know is that the recipients range
from high profile people to staffers
you never have heard of.
The subpoenas themselves are requesting information
that is pretty broad in scope,
dealing with the post-election behavior
from the Trump administration.
And the subpoenas that have been described
are long enough to be divided into sections and subsections,
requesting information about fundraising activities,
the alternate electors,
delaying the election certification,
the rally, so on and so forth.
We know that at least some of them
compelled the recipient to appear before the grand jury
on September 23rd.
We know that a couple of people, at least two,
had their phones taken so they could be searched.
The subpoenas requested copies of communications
between the recipient of the subpoena
and the Trump allies you would expect,
Giuliani, Powell, Eastman, people like that.
To me, the most interesting part is the suggestion
that the recipient needed to provide any information
they had about members of the executive
or legislative branches that helped kind of plan
anything that might obstruct, influence,
impede, or delay the certification
of the presidential election.
I have a feeling like a whole bunch of Senate staffers
just got super nervous.
And they probably have good reason to.
So why are they casting such a wide net?
Because it's broad.
There's a lot of moving parts.
And the grand jury that is tasked with looking into this,
it seems like every time they think
they've gotten to the bottom of it,
there's a sort of a,
there's a seditious underground parking garage
that they need to go through next.
So it just keeps expanding.
Some of these people, they're staffers,
and they are gonna have access to a lot more information
than people are gonna give them credit for.
The help, as we saw during the hearings,
they often have the best notes
because they're the people that actually run things.
So we can expect there to be
probably some pretty detailed information
that comes out of this.
Taking the phones, that's pretty significant.
A lot of times that's reserved for
incidents when the Department of Justice believes
the person wouldn't provide it voluntarily.
So it's big news, but it's another one of those steps.
This takes us to the next level
where they are kind of building a case
that appears now to be actively targeting
not just people that are seen as Trump's inner circle,
but possibly other political figures
who may have assisted in attempting to
produce a different outcome to the election
than what the votes said.
So that's where we're at.
We're gonna have to wait.
There will probably be more information
flowing out pretty regularly
between now and September 23rd.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}