---
title: Let's talk about DOJ looking into Trump's fundraising....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cPT19CkDtdU) |
| Published | 2022/09/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice grand jury in DC is now looking into potential fraud regarding Trump's fundraising activities post-election.
- Subpoenas have been issued to high-profile individuals, with one signed by a federal prosecutor specializing in fraud.
- Allegations surfaced during the January 6th hearings regarding Trump fundraising for the Official Election Defense Fund.
- Reporting suggests the fund raised around a quarter of a billion dollars, but there are discrepancies in the reported amounts.
- Money raised may have gone into Trump's Save America PAC, which poses issues due to the loose regulations surrounding PACs.
- Federal investigators are likely examining whether funds raised for a specific purpose were diverted elsewhere for personal gain.
- Similar financial discrepancies led to Steve Bannon's arrest, though this case is separate from the New York investigation.
- The complex web of investigations surrounding Trump's actions has led to Beau needing a whiteboard to keep track.
- Speculation points to the Department of Justice focusing on potential financial crimes based on the subpoenas and previous hearings.
- While reports indicate ongoing investigations, definitive details on the specific crimes being scrutinized remain unclear.

### Quotes

- "It's worth noting that the money that was raised was not spent on what the feds are looking into at this point."
- "I literally have a whiteboard now divided up by the various investigations and the various jurisdictions, just so I can figure out which piece of information goes with which case."

### Oneliner

Department of Justice grand jury in DC investigates potential fraud in Trump's post-election fundraising, raising concerns about diversion of funds for personal gain.

### Audience

Legal watchdogs, investigators, concerned citizens

### On-the-ground actions from transcript

- Monitor updates on the investigation closely (implied)
- Stay informed about the potential financial crimes being investigated (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the Department of Justice's inquiry into potential fraud surrounding Trump's fundraising post-election, shedding light on the complex web of financial discrepancies and investigations.

### Tags

#DepartmentOfJustice #Investigation #Trump #Fundraising #FinancialCrimes


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
a new, let's call it a line of inquiry, for the Department of Justice grand jury in DC
what they're looking into, which it's the latest installment in the Trumpy Do mysteries.
So it appears, based on the subpoenas that have gone out and some of the signatures on those
subpoenas, that the grand jury is now looking into the possibility of fraud concerning Trump's
fundraising efforts in the aftermath of the election. There have been a number of high
profile people that have been subpoenaed, and at least one of those subpoenas carried the signature
of a federal prosecutor whose whole thing is fraud. That's what they do, that's what they're good at,
and this is the person that signed off on it. So this is all kind of inference at this point,
but the general belief is that they are looking into the allegations that surfaced during the
January 6th hearings. So one of the things that came up was Trump fundraising for something called
the Official Election Defense Fund, and some of the reporting suggests that it raised a quarter
of a billion dollars. And then there was testimony that suggested there was no such thing. That fund
didn't actually exist, and that the money that was raised theoretically to help, I don't even know,
maybe with the legal fees, that that money wound up going into Trump's Save America PAC.
Now the accounting on that through Open Secrets, I want to say that it shows it raised about 103 mil,
which leaves a pretty wide discrepancy. But beyond that, it going into the Save America PAC is
problematic to begin with because PACs have very loose rules.
So loose, in fact, that the money could be spent on things that could personally enrich Trump or
those around him. Now that's legal because the way election laws work is, well, they're loose to
begin with, but saying the money is for one thing, like explicitly saying, hey, we're going to use it
for this, and then it going somewhere else, that's bad. And that is probably, by conventional wisdom,
what the feds are looking into at this point. It's worth noting that the money that was raised
was not spent on what the feds are looking into at this point. It's worth noting that this is
the same type of thing that just led Steve Bannon to be in cuffs. Now this is not related to the
New York case, though. It's hard to keep track of it all. If you want a summation of Trump's
actions, I think it's best summed up by the fact that I literally have a whiteboard now
divided up by the various investigations and the various jurisdictions,
just so I can figure out which piece of information goes with which case. It's bad.
So it does appear that now, on top of everything else, there is some aspect of something that the
feds are looking into that they want to pursue further that may have to do with financial crimes.
But again, at this point, that's inference. We don't actually know that, despite all of
the reporting saying this is definitely happening. They're educated guesses. It's not like they came
out and said 100% certain that this is what they're looking into and this is the crime.
It's just based on the subpoenas, based on who signed them, based on what was uncovered during
the hearings. Given the fact that when they were kind of previewing what the next batch of hearings
were going to be about, they did mention the cash, it seems likely that that's what's happening.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}