---
title: Let's talk about Putin and a Swan Lake moment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=erA9c0mQ_Ik) |
| Published | 2022/09/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of Swan Lake moments in relation to Soviet history during the Cold War.
- Describes the use of Swan Lake ballet on TV to distract and entertain the masses during political transitions in the Soviet Union.
- Talks about the failed coup attempt against Gorbachev in 1991 and its connection to Swan Lake being aired on TV.
- Details the information vacuum that occurred during the coup attempt due to state control of media.
- Links Swan Lake to political turmoil and a symbol of powers behind the scenes during uncertain times.
- Analyzes the current situation with Putin in relation to Ukraine's successful counteroffensive.
- Mentions the significant losses for Russia in terms of both lives and territory in the ongoing conflict.
- Suggests that the nationalist sentiment in Russia is shifting due to perceived failures in the war effort.
- Explains Ukraine's altered victory conditions, including seeking reparations from Russia.
- Speculates on the potential consequences for Putin if Ukraine demands reparations and how it could lead to a Swan Lake moment.

### Quotes

- "Swan Lake is synonymous with political turmoil. It's a bad sign."
- "The nationalists within the country are now questioning the war effort."
- "Putin will not remain in power through that exchange."
- "An unstable or failed Russia is bad for everybody."
- "Hope that it is an internal decision based on dissatisfaction with his leadership."

### Oneliner

Beau explains Swan Lake moments in Soviet history and draws parallels to Putin's potential downfall amidst Ukraine's successful counteroffensive and altered victory conditions.

### Audience

Analysts, policymakers, activists

### On-the-ground actions from transcript

- Support efforts that aim for a transition of power within Russia (implied)
- Advocate for stability and internal decision-making processes in Russia (implied)

### Whats missing in summary

Deeper insights on the implications of political turmoil and power shifts in Russia and Eastern Europe.

### Tags

#Putin #Ukraine #SwanLake #SovietHistory #PoliticalTurmoil


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about Swan Lake and Swan Lake moments
and whether or not Putin is headed towards a Swan Lake moment.
And we're going to do this because if you were alive during the Cold War
and you were politically aware, you know what this means, more than likely.
If you weren't, it doesn't make any sense.
And I hopped on Google and I tried to find an explanation of it.
And it lays out the facts of these things happened.
But it doesn't put it into context for people who weren't alive then.
There's a big piece of information that's
missing that would make it a whole lot easier to understand.
OK, so that's where we're going to start.
What is a Swan Lake moment?
In 82, 84, and 85, major Soviet leaders, they died.
And during the period in which their replacement was being selected,
Swan Lake was put on loop on the TV to entertain the masses.
And then in 91, when there was a failed coup attempt against Gorbachev,
it happened then as well.
So this is what people are told.
If you're younger, this is the part that makes it make a little bit more sense.
There's no internet.
There is no other source of information.
During the 80s, everything was controlled by the state.
The radio, the newspapers, the magazines, everything
was controlled by the state.
Now by 91, things had started to open up a little bit.
And there was at least one print, it wasn't really
a newspaper or a magazine, kind of a blend of the two.
But anyway, there was one thing that wasn't ran by the state.
However, as soon as the coup attempt started, the KGB showed up
and they were like, yeah, y'all stop working.
Don't do anything until this is over.
So an information vacuum forms.
People don't know what's going on.
They don't get any information.
Rumors flourish.
And Swan Lake is on TV.
It becomes synonymous with political turmoil.
It's a bad sign.
So why is it surfacing now?
Because when you think about it, when the news broadcasters
came back on the TV, you were just
informed of the new reality.
You weren't participating in the system, really.
The powers that be, way up the chain,
they were making those choices.
They were making those decisions.
And when the news broadcast came back on and Swan Lake disappeared,
you were told what was happening.
So it kind of became a symbol of the powers that be,
doing something behind the scenes in hopes
of it being in the best interest of the country.
So what's that have to do with Putin?
So what's that have to do with Putin?
OK.
So I think most people understand that the Ukrainian counteroffensive,
it has gone very well.
It's hard to quantify it.
It's hard to explain it.
But it is very successful.
One of the things that people use to quantify war is the number of lost.
The low end, the low end of the estimates as far as Russian lost
over the last few days is higher than what the United States lost
in Afghanistan the whole time, all 20 years.
And we're talking about just a few days.
It has gone very, very badly.
And that is the low end of the estimates.
I would say, based off of me looking at the estimates
and trying to find an accurate one, I would
say it's probably closer to a little bit less than what the United
States lost in Afghanistan and Iraq.
It hasn't gone well.
On top of that, there's a massive amount of loss of land that Russia took.
The powers that be, the people who would put Swan Lake on the air,
they know that the war is lost politically.
They know that as soon as other countries were like, hey,
I want to join NATO, that the objectives of the war failed.
The only thing they have to salvage this is the ability to retain dirt,
to take land and keep it.
And that's not happening.
That means that the nationalists within the country are now
questioning the war effort.
And this is happening.
Questioning the war effort in a country like Russia right now,
that's uncomfortably close.
That is super close to questioning the leader behind the war effort.
Now, on top of that, Ukraine has altered its victory conditions.
Not just do they want Russians out, they want reparations now,
because they feel that they can probably get them.
The thing about that is they're not going to get them by going to Moscow
and demanding it.
They're not going to take Russian land.
They're going to try to negotiate for them.
The thing is, Putin will not remain in power through that exchange.
If those are the terms that Ukraine seeks, those reparations,
that's a bridge too far.
Putin will no longer be in power, which means, what do you think
may be played on the air?
Swan Lake.
The powers that be may decide he needs to not be there anymore.
And that would lead to a Swan Lake moment.
However, we don't actually want that.
I mean, I know a lot of people reflexively might want that,
but you really don't.
It's probably wise to give Putin an off ramp.
The world doesn't want a failed or unstable Russia.
Nobody wants that.
Not really.
That's not something that's good for anybody, anywhere in the world.
If there's a transition, it needs to be done from within Russia.
And it shouldn't rely on outside pressure, such as reparations.
And don't get me wrong.
I am not saying that Russia shouldn't pay for the damage it's caused.
Remember, at this point, we are now talking about foreign policy.
Don't bring silly notions like morality or justice or anything like that
into it, because it doesn't have anything to do with what we're discussing.
We're discussing power.
That's what foreign policy is about.
And an unstable or failed Russia is bad for everybody
because it creates a power vacuum, and it creates a whole lot
of loose material that can find its way into other corners of the world
and destabilize the power balance there.
So if there is a Swan Lake moment for Putin in the future,
hope that it is an internal decision based on dissatisfaction
with his leadership, not because it's an effective way of getting out
of paying a bill.
But I don't see any way Putin could retain power
after failing in Ukraine and then having to pay Ukraine.
So that's something that hopefully Ukraine has altered the victory
conditions to show their confidence, and they're not actually
hoping to get this, and they're not going to press for it, not too hard anyway.
So I hope that catches everybody up.
That's why the ballerinas, the four ballerinas dancing,
that's why that's showing up everywhere.
It's now even showing up as graffiti in Russia.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}