---
title: Let's talk about midterm polling errors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R3Jyp6wVbU4) |
| Published | 2022/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau discussed polling errors in the midterms and his faith in polling historically.
- Polling errors could occur in the midterms due to the enthusiasm in certain demographics leading to unlikely voters turning up.
- Younger people, especially women energized by a Supreme Court rule, might participate in higher numbers than anticipated.
- Errors in determining likely voters, especially with younger individuals and college students, could skew the final polling results.
- Beau suggests that discounting certain groups of potential voters might give an edge to the Democratic Party.
- Ultimately, Beau's predictions are based on his perception at the current time and may change.

### Quotes

- "I think the final product might be wrong as well."
- "It gives an edge towards the Democratic Party."
- "It's just a thought."

### Oneliner

Beau warns of potential polling errors in the midterms due to enthusiasm from unlikely voters, especially younger demographics, potentially favoring the Democratic Party.

### Audience

Voters

### On-the-ground actions from transcript

- Stay informed about the upcoming midterms and encourage others to vote (implied).
- Ensure you and those around you are registered to vote (implied).
- Engage in political discourse and encourage voter participation within your community (implied).

### Whats missing in summary

More details on the specific demographics that Beau believes could impact the midterm elections.

### Tags

#Polling #Midterms #LikelyVoters #DemocraticParty #VoterParticipation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about polling
when it comes to the midterms, and likely voters,
and why I said something over on the other channel,
on the second channel, it's called The Roads with Beau,
because I know somebody's gonna ask.
I did a Q&A, took questions from Twitter and answered them
in a video over there, and I said that I expected there
to be polling errors in the midterms.
And this prompted a whole lot of questions,
because historically, I have a whole lot of faith in polling.
I have a whole lot of faith in polling,
because it's normally right.
It is normally accurate.
Even the times when people point to it and say,
oh, the polls were off here.
No, I mean, they were actually still
within the margin of error listed down on the bottom.
They're normally pretty accurate.
I don't know that that's going to be the case in the midterms.
And it all hinges on a term that you've probably
heard so often, if you pay attention to polling,
that you may not even realize that that's occurring anymore.
The survey of likely voters said that Jack Johnson
was going to beat John Jackson by two points.
likely voters. The pollsters, they determine who they believe are likely voters and I think they're
going to get that wrong. Based on the enthusiasm that exists in certain demographics, there's going
to be a lot of unlikely voters who show up for the midterms. Based on my perception of things
right now. This could change. But right now there's a lot of enthusiasm in groups that typically
don't show up in high numbers, especially when you're talking about midterms. When you're talking
about the midterm elections, it's generally the most politically engaged who show up.
up.
Those tend to be older people, by the way.
I don't know that that's going to be the case with these midterms.
There are a lot of younger people, specifically younger women, who were energized by a Supreme
Court rule.
I think they're going to show up in higher numbers than has been anticipated.
So because the polls are based on likely voters, those unlikely voters who show up, they stand
a pretty good chance of skewing the results.
Aside from that, there are people who are younger in college or fresh out of college
who due to certain policies, they may be more motivated to vote.
So because of those possible errors in determining who is a likely voter, I think the final product
might be wrong as well.
I will say that every instance where I think there are people who are going to show up
who the pollsters are going to discount, it gives an edge towards the Democratic Party.
But we're going to have to wait and see and see if that's actually what plays out.
I don't have any science or source to base that on.
It's a gut based on this point in time.
It could change.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}