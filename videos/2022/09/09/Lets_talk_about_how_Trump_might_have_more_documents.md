---
title: Let's talk about how Trump might have more documents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a1R-FWdEnz0) |
| Published | 2022/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice wants classified material now and seeks to change judge's orders.
- Classified documents are not attorney-client or executive privilege.
- DOJ may take the issue to the 11th Circuit if judge doesn't comply.
- Intelligence assessment on risk created by former president is at play.
- FBI, part of intelligence community, is vital for counterintelligence.
- There's overlap between criminal investigation and intelligence assessment.
- DOJ believes former president may still have classified records.
- DOJ suggests that impeding use of classified records in criminal investigation poses national security risks.
- Feds could have a willful retention case if additional documents are found.
- Concerns about potential ongoing searches and undisclosed documents.
- Uncertainty around the existence of additional documents.
- DOJ hopes judge will grant access to classified material.
- Legal justification seems lacking to deny access.
- If judge denies, the issue will go to the 11th Circuit.
- Uncertainty and potential for more developments in the situation.

### Quotes

- "We want the classified material right now."
- "They're not attorney-client, they're not executive privilege, and they don't belong to Trump."
- "That speaks volumes."
- "There might be more to this still coming."
- "Y'all have a good day."

### Oneliner

Department of Justice pushes for access to classified material in Trump case, raising concerns about national security risks and potential undisclosed documents.

### Audience

Legal analysts, concerned citizens

### On-the-ground actions from transcript

- Monitor updates on the legal proceedings and outcomes (exemplified)
- Stay informed about potential national security risks (exemplified)
- Advocate for transparency and accountability in handling classified materials (implied)

### Whats missing in summary

Insight into the implications of potential undisclosed classified documents and the ongoing legal battle involving the Department of Justice and the former president.

### Tags

#DepartmentOfJustice #Trump #ClassifiedMaterial #NationalSecurity #LegalProceedings


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
the Department of Justice and their latest move.
And we're also going to talk about
the possibility that Trump might still have
more classified documents.
Before we get into that, we'll go over what DOJ is doing,
what they're trying to do and trying to accomplish.
So the Department of Justice is kind of offering an off-ramp
to the judge who granted Trump the ruling
for the special master.
That ruling, a lot of people have,
well, let's just say they had questions about it.
DOJ is specifically saying,
we want the classified material now.
We want to be able to use that now.
Everything else, fine, give it to the special master,
don't care.
We want the classified material right now.
We want to be able to use it.
And they lay out the fact that the classified documents,
they're not attorney-client,
they're not executive privilege,
and they don't belong to Trump.
So they're looking for something
that is very narrow in scope
to be changed in the judge's orders.
And everything else, they're like, yeah, whatever.
And they also kind of indicate that
if the judge doesn't do that,
then, well, they'll just take it to the 11th Circuit.
And the 11th Circuit almost certainly will.
Now, one of the things that is also at play
is the damage assessment, the intelligence assessment,
dealing with the risk that the former president
may have created.
Now, I've talked about this before,
but intelligence agencies generally don't have authority
to conduct investigations inside the United States.
The FBI is part of the intelligence community,
and they're incredibly important
when they come to counterintelligence.
Now, the first thing that the judge sent down
indicated that the assessment could continue.
When I talked about it, if you go back to the video,
let's talk about Trump getting a special master.
When I'm talking about this, I am smirking.
I'm laughing because I know how this functions.
There will be senior people on the criminal investigation
that are also a part of the intelligence assessment,
literally the same people.
Now, they can't view the documents
as part of the criminal investigation,
but by my reading of what the judge said,
they could view it as part of the intelligence assessment.
So I found that very funny.
By the way I look at it, they're good to go.
I guess Merrick Garland is a stickler
for judicial intent or whatever
and is looking for clarification on that aspect of it.
For those who think that the feds
are just going out of their way
and they're being incredibly unfair, they're not.
This was wide open for them to use this.
I mean, it's so open, I was laughing about it
when I was talking about it.
And they have elected not to do it that way,
and they're asking for clarification.
Now, on to the part in what they sent in
that I think is most important.
In addition, the injunction against using classified records
in the criminal investigation could impede efforts
to identify the existence
of any additional classified records
that are not being properly stored,
which itself presents the potential
for ongoing risk to national security.
The feds believe it is possible that he still has some.
Can you imagine being one of those people
who went out there and went to bat for him
and then finding out that maybe
he is still retaining documents like that?
If that's the case, I would say
that the Department of Justice
certainly has a willful retention case.
Given what happened, how long they were trying
to get him back, and then the search,
if there were additional documents
that he didn't voluntarily say,
oh, you're obviously looking for these,
that speaks volumes.
That speaks volumes.
And the feds are apparently concerned about that.
That, to me, is probably more important
than the legal maneuvering here,
because that may suggest that
there might be more searches.
There might be more to this still coming.
So that may have something to do with the empty folders,
or this may be something that they have uncovered
that we don't know about yet,
or it may just be they're concerned
because they don't know.
We don't actually know yet.
But that being brought up the way it has been,
that's something that we'll probably see
that material again.
So that's what's going on.
I think, I would hope that the judge
is going to give them access to the classified material
and allow them to review it.
There doesn't seem to be a legal justification
to not allow it.
But we're going to have to wait and see
if the judge decides not to.
Next week, it's going to the 11th Circuit.
So that's where we're at on all this.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}