---
title: Let's talk about a Philadelphia cop, Time, Distance, and Cover....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2Fo-ELYK2Pg) |
| Published | 2022/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a 2017 incident in Philadelphia involving a 25-year-old man, Dennis Plowden, who was shot by Officer Eric Rook.
- Rook fired at Plowden within six seconds of arriving on the scene, hitting Plowden in the hand and head. Plowden was unarmed.
- Officer Rook was recently found guilty of voluntary manslaughter, becoming the third cop charged with similar offences by District Attorney Larry Krasner.
- Rook claimed he was scared because he didn't have cover unlike the other officers who did.
- Beau stresses the importance of time, distance, and cover in police tactics to prevent unnecessary shootings.
- Beau argues that if these basic principles were followed, Plowden might still be alive.
- Officers often receive training on best practices but may disregard them when out on the streets based on the guidance of their training officer.
- Beau points out the discrepancy between training and real-world practices that can lead to fatal consequences.
- The sentencing for Officer Rook is scheduled for November 17th, with Beau expressing uncertainty about the potential outcome.
- Beau concludes by urging for adherence to proper training protocols to avoid tragic incidents like the one involving Dennis Plowden.

### Quotes

- "Time, distance, and cover."
- "Those rules, those tactics, those best policies, whatever your department calls them, they're there for a reason. They work."
- "One of them, one way, saves lives. One of them ends up with you in a cage."

### Oneliner

Beau stresses the importance of police officers following basic principles like time, distance, and cover to prevent unnecessary shootings and save lives.

### Audience

Police officers, law enforcement.

### On-the-ground actions from transcript

- Attend or follow the sentencing of Officer Eric Rook on November 17th (suggested).
- Advocate for proper training adherence and accountability in law enforcement practices (implied).

### Whats missing in summary

The full transcript provides detailed insights into a 2017 incident in Philadelphia and the importance of police officers following proper training protocols to prevent fatal shootings and uphold accountability.

### Tags

#PoliceTraining #Accountability #CommunityPolicing #Philadelphia #Justice #LawEnforcement


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about a 2017 incident
in Philadelphia, the current developments with it,
and a callback to something we have talked about
on this channel over and over again.
So in 2017, a 25-year-old man, Dennis Plowden,
was in a pursuit, crashed his vehicle, got out, was sitting on the sidewalk, a bunch
of officers around him, and they're telling him to put his hands up. He puts his left
hand up, his right hand doesn't, he doesn't put his right hand up. Don't actually know
Another officer, Eric Rook, arrives on scene. Within six seconds Rook fires. The
round goes through Plowden's hand and into his head. Plowden is unarmed. Rook
was just found guilty of voluntary man. It is worth noting that this is the third
cop brought up on similar charges by the DA there, Larry Krasner, who is a civil
rights attorney, long-time civil rights attorney, now the district attorney there.
He'll be looking at sentencing on November 17th. What I found interesting
about this is that Rook said the reason he was scared was because he didn't have
cover. The other officers got behind cover. He did not. Time, distance, and cover.
Officers are taught this. This is basic stuff. Time, distance, and cover. When
those things are applied, you very, very, very, very rarely have bad shoots. When
they're not applied, they happen pretty often. Those rules, those tactics, those
best policies, best practices, whatever your department calls them, they're there
for a reason. They work. If time, distance, and cover had been applied in this
situation, I'm willing to bet it never would have happened. I'm willing to bet
Plowden would still be alive. I'm willing to bet that Rook wouldn't be looking at
sentencing, that none of this would have happened if a basic principle had been
followed. Those classes they give you, what happens is a lot of times the
officer hears it, they get the training and then they go out on the street and
their training officer who's with them the first however long tells them, oh
forget all that stuff. And they ask, you know, they ask the instructors, when are
you gonna start training how we do things out on the street? When are y'all
gonna start doing things the way you're trained? One of them, one way, saves lives.
One of them ends up with you in a cage.
We'll have to wait and see how long the officer gets for this. The the range is
pretty wide. I don't even have a guess, to be honest. But we will find out on
November 17th. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}