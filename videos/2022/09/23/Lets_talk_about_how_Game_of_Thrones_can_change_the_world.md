---
title: Let's talk about how Game of Thrones can change the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3Lg9QVjU6tE) |
| Published | 2022/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of fiction and how stories like Lord of the Rings, Game of Thrones, and Harry Potter can teach us about building a better world.
- An individual shares their journey of transitioning from reading fantasy books like Harry Potter to feeling the need to catch up on more informative reading material for community development.
- The comparison between authors and activists is made, stating they both share the job of envisioning and bringing a world to life through different mediums - pen and paper for authors and actions for activists.
- Beau suggests that immersive, world-building books like 1984, Lord of the Rings, Game of Thrones, and Harry Potter could be inspiring for someone interested in community development.
- Fiction, regardless of the topic, can have a profound impact on individuals and alter the way they perceive the world, even through small realizations gained in a fantasy setting.

### Quotes

- "Fiction is this unique thing because it doesn't matter what the topic is. If the fiction is any good at all, it's going to have an impact on you."
- "Sure, these are not books that people perceive as being incredibly deep, so if that's what inspires you, go for it."
- "You say information can get you to fact, but fiction can get you to truth."

### Oneliner

Beau shares insights on how fiction, including fantasy books like Harry Potter, can inspire and guide individuals towards building a better world through immersive storytelling and world-building experiences.

### Audience

Students, aspiring community developers

### On-the-ground actions from transcript

- Read immersive and world-building books like 1984, Lord of the Rings, Game of Thrones, and Harry Potter to gain inspiration for community development (suggested).
- Utilize audiobooks through apps like LibriVox to access a wide range of books amidst a busy schedule (suggested).

### Whats missing in summary

Beau's engaging delivery and encouragement to embrace fiction as a tool for personal growth and societal change can best be experienced by watching the full video. 

### Tags

#Fiction #CommunityDevelopment #BuildingABetterWorld #FantasyBooks #ImmersiveReading


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about fiction
and how Lord of the Rings, Game of Thrones,
and Harry Potter can teach us a little bit
about building a better world.
I've got a message.
I started watching you at the beginning of the pandemic
when I was still in high school.
I remember you talking about Banned Books Week back then,
but I didn't read those kinds of books.
I read fantasy, Lord of the Rings, Game of Thrones,
and Harry Potter.
Then there are some choice words about the author of Harry
Potter that I'm going to skip over here.
Now I'm in college and feel like I wasted my time,
and I'm trying to catch up.
But my courses don't leave a lot of time
for recreational reading.
I want to do something in community development
when I graduate and feel like I'm going to lack depth because I
didn't read anything worthwhile.
You say information can get you to fact, but fiction can get you to truth.
What kind of fiction gets you to truth, or could at least make me better at building
a better world?
Sounds stupid, but what books could be inspiring to me?
Not too long ago, I heard somebody talk about something that was very relevant, and I can't
I don't remember who it was and I'm certain that I'm about to butcher the profound nature
in which they presented it.
But the general idea was that an author and an activist, they really aren't that different.
They kind of have the same job.
They envision a world and they bring it to life.
The author uses pen and paper, the activist uses their actions.
the idea is to create a world that isn't there. I thought it was pretty cool. Pretty cool
concept and it seems to hold up when you think about it. Community development, if I'm not
mistaken, is a holistic approach using techniques that would lead to an equitable and sustainable
community, and the idea is to focus on the lives of the people in it, and how
they interact, and try to make that community more successful. So if
you're going to go into that field, my suggestion would be to read, I don't know,
something like 1984. You know, that's one of those classics that everybody talks
about, and a whole lot of people haven't read even though they're talking about
about it, but that would be one, because it has a whole lot of intense world-building.
You learn how the society functions.
I think some other good examples would be Lord of the Rings, Game of Thrones, Harry
Potter.
Those immersive books that have a lot of world-building in them would probably be pretty inspirational
to somebody going into that field.
In fact, I would suggest that perhaps, if somebody read those types of books, it might
even encourage them to go into that field.
Maybe you've already been inspired.
Maybe that's how you wind up where you're at.
Sure, these are not books that people perceive as being incredibly deep, so if that's what
inspires you, go for it.
Read what you like.
Read the stuff that you like.
I would focus on stuff that is immersive and world-building, because A, it seems like something
you would like, and B, it will transfer.
But I would focus more on reading stuff that you enjoy, especially at this point in your
life when you are a little bit overworked.
And maybe just guide it a little bit so it focuses more on cultures that exist in this
world.
Maybe go that route.
You've got a lot of time to read.
As far as the current course load, LibriVox, app for your phone, it'll read a whole lot
of books to you.
look into audiobooks.
Fiction is this unique thing because it doesn't matter what the topic is.
If the fiction is any good at all, it's going to have an impact on you.
You may not even realize it.
It could drastically alter the way you see the world through little bits and pieces,
baby changes, little realizations that you're free to make in a fantasy world
that doesn't exist. And if you can bring those back to the world that does, well
you might be able to create that better world you want. It goes for books, it goes
for movies, it goes for music. I wouldn't put too much pressure on yourself in
this regard. If at this age you're already this concerned about it you'll
be fine.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}