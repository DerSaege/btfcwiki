---
title: Let's talk about the Electoral Count Act of 1887 being clarified....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f3h_oieEzV4) |
| Published | 2022/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Electoral Count Act of 1887 has come under scrutiny due to a misreading that suggested the vice president could disregard votes.
- Both the House and the Senate are taking steps to revise the act to prevent potential coup attempts.
- The House has passed a version that requires one-third of House members to sign off on objections to certifying electors, among other clarifications.
- The revised act clarifies the ceremonial role of the Vice President and requires governors to transmit electors chosen by popular vote.
- Despite being straightforward clarifications, only nine Republicans in the House supported the revision, while others may be reserving the right to support a coup in the future.
- The Senate is also working on a comparable bill with support from 10 Republicans, which is likely to pass.
- The necessity for such clarifications is concerning, as it should have been common knowledge that the vice president couldn't unilaterally discard votes.
- The lack of overwhelming support for this revision from those who claim to respect the country's foundational principles is disheartening.

### Quotes

- "The fact that the legislative body of the United States is having to explicitly say the vice president can't just throw out the votes and do what they want, that's appalling."
- "People who claim to respect a constitutional republic, a representative democracy, the general ideas that this country was founded on, this should have breezed through with full support."
- "There's nothing controversial in this."

### Oneliner

The Electoral Count Act of 1887 is being revised to prevent potential coup attempts, with clarifications on objections and the ceremonial role of the Vice President, but lack of overwhelming support is disheartening.

### Audience

Legislative watchers

### On-the-ground actions from transcript

- Contact your representatives to express support for clarifying and revising the Electoral Count Act (exemplified)
- Stay informed on the progress of the comparable Senate bill and advocate for its passage (exemplified)

### Whats missing in summary

Importance of staying vigilant to protect democratic processes and prevent potential misuse of power.

### Tags

#ElectoralCountAct #Revision #CoupPrevention #House #Senate


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about
the Electoral Count Act of 1887.
And it being revised.
The reason the Electoral Count Act
came into public discussion
was a willful misreading of it
that led a lot of people to believe
that the vice president could just throw away the votes
and do whatever he wanted.
The House and the Senate have both taken measures to start the process of revising it, to clarify this so people don't
use it to, I don't know, hypothetically speaking, attempt a coup.  The House has passed a version to revise it.
Clarifications include that now one-third of House members will have to sign on to an
objection to certifying any state's electors.
It also narrows the reasons that an objection is valid, and it clarifies that the Vice President
is there in a ceremonial capacity, doesn't actually have the ability to just pick the
ex-president at WIM, you know, because that's clearly what the intention was of the original
act.
It also requires governors to transmit the slate of electors chosen by the popular vote.
These are all real simple things.
These are clarifications.
This is kind of, I mean, with the exception of narrowing the reasons for an objection
And the number of House members that need to sign on to one, this is how it's always
been understood until people started attempting to look for a loophole to stay in power despite
the voice of the people.
This passed in the House.
Nine Republicans signed on, voted yes, to get this through in the House.
It is worth noting that the rest of the Republicans decided that, now I guess they'd rather reserve
the right to coup later.
Now the Senate has a comparable bill.
It's not exactly the same.
I want to say it's one-fifth there, I could be wrong on that.
I think it's one-fifth instead of one-third, or needed to get an objection moving forward.
But, it's relatively similar, and the Republicans there, there are 10 that have signed on that
would get it through the filibuster at this point.
We'll see how it plays out in the future, but at this moment, a comparable Senate bill
is likely to make it through as well.
So it looks as though this is actually going to happen and this revision will occur.
This clarification should be unnecessary.
It really should.
The fact that the legislative body of the United States is having to explicitly say
the vice president can't just throw out the votes and do what they want, that's
appalling. It's even more appalling that it didn't get full support. This should
have breezed through. People who claim to respect a constitutional republic, a
representative democracy, the general ideas that this country was founded on,
this should have breezed through with full support. The only people who should
have voted against it were those who didn't understand it, who literally voted against
it by accident.
There's nothing controversial in this.
But only nine Republicans in the House thought that it would be a good idea to close a potential
loophole that might enable a coup.
That might be worth remembering come election day.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}