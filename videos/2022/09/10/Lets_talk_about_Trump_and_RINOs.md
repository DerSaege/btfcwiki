---
title: Let's talk about Trump and RINOs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xXhSvSCuYZk) |
| Published | 2022/09/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of a "rhino" and its application to Trump and other prominent Republicans like Barr, McConnell, Cheney, and Rove.
- Trump's history of switching political affiliations, starting as an independent in 2012, Democrat in 2009, and Reform Party member in 2001, contrasts with the long-standing Republican backgrounds of the others.
- Trump is characterized as an authoritarian rather than a traditional Republican, using the party's platform for his own agenda.
- The wedge driven into the Republican Party by the divide between small government conservatives and authoritarians is emphasized.
- Beau expresses his disagreement with figures like Barr, Rove, Cheney, and McConnell, acknowledging their Republican identity while criticizing Trump for not truly adhering to Republican principles.
- Trump's manipulation of rhetoric and base-building strategies is discussed, pointing out how he deviates from traditional Republican policies.
- The contrast between the Republican Party's small government conservatism rhetoric and Trump's authoritarian tendencies is outlined.

### Quotes

- "Trump is the rhino, certainly not Karl Rove."
- "Trump's not a Republican. He's an authoritarian."
- "He went and shopped the various political parties trying to find the one that was easiest to manipulate."
- "The rhino concept has driven a wedge in the Republican Party."
- "Their rhetoric was always about individualism and stuff like that."

### Oneliner

Beau explains the "rhino" concept, contrasting Trump's authoritarianism with traditional Republican values represented by figures like Barr, McConnell, Cheney, and Rove.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze political candidates' policies and rhetoric to understand their true intentions (suggested)
- Engage in respectful political discourse to bridge ideological divides (implied)

### Whats missing in summary

Beau's engaging delivery and nuanced analysis can provide deeper insights into the complex dynamics within the Republican Party. 

### Tags

#RepublicanParty #Trump #Authoritarianism #PoliticalAnalysis #SmallGovernment


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about Barr and McConnell
and Cheney and Rove and what we all apparently
have in common and Trump.
Because I got a message, and it's a doozy as a message.
I honestly couldn't tell if it was a joke or not.
And then somebody said, hey, this
is an opportunity to explain something.
Challenge accepted.
OK, so here's the message.
I thought I had found a new person to follow on YouTube.
Turns out, you're just another rhino with TDS,
like McConnell, Cheney, Barr, and Rove.
If you can't see, they're just out to stop him.
You're blind.
And then it goes, the message just gets mean.
You know, lots of caps lock and stuff like that.
I would really like to know if you watch this video.
If you could, please put whatever video
led you to believe I was a Republican.
If you could link it in the comments section,
that would be greatly appreciated.
I'd like to know which video it was so I can delete it.
There's no reason for anybody else to make this mistake.
But it does bring us to the concept of a rhino.
Because while I am not a Republican in name of any kind,
that term has always struck me as funny.
Because here's the reality of it.
Bill Barr has been a Republican at least since the 80s,
probably before.
The dates I'm giving, the years I'm giving,
those are just ones I know by that point
they were a Republican.
Very well could be before that.
So Bill Barr was in the 80s.
Rove was 1973.
The Cheneys have been a Republican dynasty
for like 50 years.
And Mitch McConnell has been a Republican since 1968.
Do you know what Trump was in 2012?
An independent.
Do you know what he was in 2009?
A Democrat.
Do you know what he was in 2001?
Reform Party.
Trump's the rhino.
I hate to break it to you, but he really is.
He went and shopped the various political parties
trying to find the one that was easiest to manipulate.
The one that was easiest to build a base in.
The one that he could stand there, give them permission
to be their worst, and it would motivate them.
Because Trump's not a Republican.
He's an authoritarian.
And the rhino concept has driven a wedge
in the Republican Party between the people
that at one point in time, at least in rhetoric,
were small government conservatives.
And the people who, with the slightest bit of encouragement,
were hoping US troops marched down the streets.
The authoritarians.
I have no love for Barr, or certainly not Rove,
or Cheney, or McConnell, or any of them.
But they're Republicans.
And they've always been Republicans.
They've never hid the fact that they were Republicans.
They're very open about that.
And I don't like it.
But that's what the Republican Party is.
It's the rhetoric of small government conservatism.
That's the plan, right?
That's what they sell people on.
I mean, they've always done tax cuts for the rich
and stuff like that.
And that's really what they are.
But their rhetoric was always about individualism and stuff
like that.
Trump is not about small government.
Trump's never been a Republican.
None of his policies really lined up
with a Republican Party.
He just used that rhetoric to build that base
and found people that would follow him
to the point of casting off their previous beliefs
and accepting whatever dear leader told them to believe.
Trump is the rhino, certainly not Karl Rove.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}