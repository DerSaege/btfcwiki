---
title: Let's talk about North Korea supplying Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d5VjUyt_980) |
| Published | 2022/09/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia may be turning to North Korea for supplies, including ammo, due to low stocks.
- North Korea's older equipment, initially from the Soviet Union, could be compatible with Russia's.
- North Korea has large stockpiles that could benefit Russia in exchange for various goods or looking the other way on sanctions.
- There's speculation on whether US intelligence is behind the messaging to sway Russian allies.
- The situation makes sense given recent concerns about the US needing to reorder artillery shells.
- North Korea's old and potentially degraded supplies could pose a problem for Russia if utilized.
- The potential fallout could lead to unexpected countries supporting Ukraine against Russia.
- Russia's lack of capability to quickly replenish supplies contrasts with the US's abilities.
- The compatibility between North Korean and Russian equipment due to destruction of modern supplies adds up.
- The situation is still unfolding, but the possibility of North Korea supplying Russia seems accurate.

### Quotes

- "Russia may be turning to North Korea for supplies, including ammo, due to low stocks."
- "North Korea's old equipment could be compatible with Russia's, potentially benefiting both sides."
- "The potential fallout could lead to unexpected countries supporting Ukraine against Russia."

### Oneliner

Russia may turn to North Korea for supplies due to low stocks, potentially impacting global dynamics and alliances.

### Audience

Policy analysts, geopolitical strategists

### On-the-ground actions from transcript

- Monitor developments and analyze potential impacts on global alliances and conflicts (implied)
- Stay informed about the evolving situation between Russia, North Korea, and Ukraine (implied)

### Whats missing in summary

Analysis of the potential consequences and geopolitical shifts resulting from Russia seeking supplies from North Korea.

### Tags

#Russia #NorthKorea #geopolitics #globalalliances #militarysupply


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the possibility of Russia being supplied by North Korea.
That is such a strange sentence, but it seems pretty likely.
The reporting is suggesting that Russia's stocks, their supplies, have run so low that
they are starting to look for outside suppliers to provide them with ammo, various kinds.
The reporting is suggesting that North Korea is ready to offer millions of rounds.
So does it make sense?
Yeah, a lot of North Korea's equipment is older.
A lot of it initially came from the Soviet Union.
So a lot of it would be interchangeable,
especially when you're talking about artillery,
small arms, stuff like that.
Yeah, and North Korea maintains pretty large stockpiles.
So this all really does kind of add up
as bizarre as it sounds.
When Russia started using older equipment,
it made the legacy military of North Korea kind of useful.
And North Korea could definitely get something out of it,
as far as chips at the big game, or food, technology, stuff
like that.
Or even just kind of looking the other way
when North Korea violates sanctions.
Now, somebody's asked, is this an information operation?
Is this US intelligence putting this messaging out
to kind of sway possible Russian allies away from them,
saying, hey, they don't have the supplies,
and putting that out there, and are we just collateral damage
in that campaign?
Maybe.
I mean, that's not out of the question.
They would definitely do that if they thought it would work.
At the same time, it tracks.
This information definitely makes sense.
Remember, just a few days ago, we
were talking about how the United States,
a country with massive stockpiles,
is going to have to reorder 155 millimeter artillery shells.
They're going to have to re-up their supply.
So, I mean, it tracks, it makes sense.
The country chosen makes sense.
All of it adds up.
It really does.
Now, Russia may not be happy about this
if they end up having to use this stuff.
A lot of the stores that North Korea has,
they're old, like really old.
And there is a high likelihood
that some of it is degraded, especially when it comes to the artillery rounds.
There was an incident a few years ago where North Korea engaged in a bombardment and the
amount of artillery shells that hit the target was just, it was fractional of what it should
be.
I mean, it was a really poor showing.
There is a high likelihood that that's because it's older, it's degraded, or it's of local
manufacture and the quality controls aren't there.
Either way, this is what Russia may end up with.
So what are the other benefits here?
What are the other things that are at play all around the world?
One is that I would imagine that if this plays out, you will see a lot of interest in helping
Ukraine from countries you might not necessarily imagine.
Countries that view North Korea as opposition.
Because if Ukraine is fighting Russia and North Korea is dipping into their stocks to
supply Russia, it reduces North Korean capability.
So their opposition countries may decide to help Ukraine.
That doesn't bode well for Russia in and of itself, if this is accurate.
Remember when we were talking about the US running low on 155 millimeter, I was like,
Yeah, they're going to order more. They'll have it in two months.
Apparently, Russia doesn't have that capability.
That's not good news for them.
So, is it possible it's an information operation?
Yes, but this seems likely that it's very accurate,
that this is something that is happening. We don't know the scale,
necessarily the legacy army that exists in North Korea, their stockpiles are
useful because a lot of the more modern stuff that Russia has was destroyed. So
they're using older stuff too, making it compatible. It all kind of adds up, but
we'll wait and see. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}