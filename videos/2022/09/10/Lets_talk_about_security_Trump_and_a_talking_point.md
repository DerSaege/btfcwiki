---
title: Let's talk about security, Trump, and a talking point....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VZWutbGcaXY) |
| Published | 2022/09/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the excuses made to absolve the former president of endangering U.S. national security by exposing secrets.
- Exploring the flawed notion that Secret Service protection of the former president extends to protecting documents.
- Illustrating the illegality and ineffectiveness of attempting to secure classified documents through Secret Service presence.
- Challenging the idea of a "secure facility" and the varying meanings of security in different contexts.
- Questioning why certain classified documents were not returned and how many individuals were exposed to them.
- Emphasizing the lack of excuses or talking points that can justify the mishandling of classified documents.
- Urging for accountability and investigation into how and why such a security breach occurred.

### Quotes

- "There is literally no excuse. There's no talking point that will make it okay. It never should have happened."
- "Secure means different things in different contexts."
- "They weren't secure."
- "Stop trying to find a way to make this okay. It isn't."
- "Now we have to find out how it happened and why it happened."

### Oneliner

Addressing excuses to absolve the former president of endangering national security, Beau challenges flawed notions of document security and urges for accountability.

### Audience

Citizens, Government Officials

### On-the-ground actions from transcript

- Investigate the mishandling of classified documents (implied)
- Hold accountable those responsible for the security breach (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the flawed excuses used to justify the mishandling of classified documents by the former president.

### Tags

#Security #NationalSecurity #Accountability #ClassifiedDocuments #SecretService


## Transcript
Well, howdy there, Internet people, it's Bo again. So today we're going to talk about security and the term secure and
The latest talking point, the latest excuse and a long litany of excuses
that is attempting to absolve the former president for his culpability in
in endangering U.S. national security by exposing secrets to, well, anybody who put forth a
little bit of effort, the slightest bit of effort.
This talking point, I've seen it come up a couple of times over the last few days.
This is apparently the new framing.
We're going to go through it, see if it holds up to any scrutiny.
It doesn't.
Here's one example.
a village idiot. That's me, I'm the village idiot. They were, all caps, paper
copies stored in a secure facility guarded by the Secret Service. Okay, now
the implication here is that because the Secret Service is there protecting the
former president, that by default they're also protecting the documents. This isn't
true, but we're gonna pretend that it is for a second to illustrate one thing
right away. Let's just say that this is happening. In fact, let's go further and
say that Trump actually ordered a Secret Service person to stand outside the door
of where he was keeping some of these documents and make sure nobody goes in.
All right, let's go that far. Do you know what that's called? Illegal. It doesn't
change a thing, just to be clear. Put it into a different context. Think of the
characters from the TV show, The Americans. They get a bunch of secret documents that
they're not supposed to have and they hire somebody to stand outside the closet where
they're storing them. It doesn't matter because they're not supposed to have them. Doesn't
change a thing. Aside from that, the implication that the Secret Service is protecting them
is wrong because the Secret Service would have to know they were there.
Now let's go to the idea that it's a secure facility.
Don't use terms if you don't know what they mean.
Secure means different things in different contexts.
For the Secret Service, what is their primary job?
To protect the safety and dignity of the former president down there.
That's what they're supposed to be doing.
What do they try to filter out from people who are coming near him?
What do they try to stop them from having?
What do they want to make sure they don't have on them?
Weapons, right?
Something to commit an act of violence.
That's what they're concerned about.
Do you know what the Secret Service allows in pretty much every case when you're talking
about a public appearance by a president or former president?
Cameras.
Do you think you could walk into a skiff with a camera?
not, right? Different contexts, the terms mean different things. Now, let's take
another step back. So, let's say there's, you know, there are, in fact, a
bunch of Secret Service agents there, and they are securing the perimeter. They're
making sure that only those people allowed can come in and out of certain
areas. That's what they do. Therefore, it's secured. Why do we have skiffs on military
installations again? I'm confused. I mean, the MPs or security forces, they're outside.
They're securing the perimeter. They're making sure that only those people who are supposed
to can come in and out, I mean that's there.
In fact, some of these places, they have like high-speed teams there.
So theoretically, you could say, oh, they're guarded by seventh special forces if the documents
were at Eglin, right?
Yeah, they still have a skiff.
That's not how this works.
If you were to go to CIA headquarters, one of the most secure buildings on the planet,
everything is tracked by that card, cameras everywhere, audio everywhere, do you know
where they handle the SCI documents?
In a skiff.
This is a horrible talking point.
All it does is expose that you should not be allowed anywhere near a classified document.
They weren't secure.
Now by any stretch of the imagination, yeah, they were paper documents.
Why were they there?
Why weren't they returned?
How many people saw them, heard about them, read them, photographed them?
These are questions we need answers to.
Wasn't secure.
Stop trying to find a way to make this okay.
It isn't.
There is no talking point that is going to make this okay.
inexcusable. There isn't something that's going to make this be like, oh, it was
okay for him to have documents that literally never should have left a skiff.
Some of the documents there, you know, the stuff that's secret or classified,
yeah, it shouldn't be there. But the people who are most adamant about the
exceptionally grave danger that this posed to US national security, they're
not talking about those. They're talking about the TS documents. They're talking
about the SCI documents. That's what they're concerned about. There is
literally no excuse. There's no talking point that will make it okay. It never
should have happened. Now we have to find out how it happened and why it
happened. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}