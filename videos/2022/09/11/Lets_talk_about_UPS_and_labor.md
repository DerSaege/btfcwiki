---
title: Let's talk about UPS and labor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VPTz_YTfcu8) |
| Published | 2022/09/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- UPS is in the news due to upcoming contract negotiations with the Teamsters union, with speculation of a potential strike.
- The possibility of a strike is determined by management's stance in negotiations, not the union.
- Recent leadership changes and a $300 million strike fund suggest the Teamsters may indeed go on strike.
- A potential strike could start at 12:01 AM on August 1 and could significantly impact the US GDP due to UPS's role in facilitating 6% of it.
- The average pay for 350,000 Teamsters is $95,000, but the strike isn't just about pay; it also involves demands like air conditioning in trucks.
- Unions like the Teamsters strike not just for pay but for better working conditions and representation.
- The high pay of Teamsters is a result of collective bargaining, showcasing the effectiveness of unions in ensuring fair compensation.
- The coverage of their high pay should not be used as a reason against their right to strike, as it is the result of strong union representation.

### Quotes

- "The possibility of a strike is determined by management's stance in negotiations, not the union."
- "Unions like the Teamsters strike not just for pay but for better working conditions and representation."
- "The high pay of Teamsters is a result of collective bargaining, showcasing the effectiveness of unions."
- "Tell me again how unions don't work."
- "You get all of these articles talking about the fact that they're the highest paid and therefore they shouldn't strike."

### Oneliner

UPS faces potential strike as Teamsters negotiate, showcasing unions' effectiveness beyond pay.

### Audience

Workers, Union Members, Negotiators

### On-the-ground actions from transcript

- Support Teamsters by staying informed about the negotiations and showing solidarity with their cause (suggested).
- Reach out to local unions to understand the importance of collective bargaining and worker representation (implied).

### Whats missing in summary

The full transcript provides additional insights on the importance of unions in advocating for fair working conditions and representation beyond just salary negotiations.

### Tags

#UPS #Teamsters #Strike #Union #Negotiations #WorkerRights


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about UPS.
We're going to talk about UPS because there is a lot of news
coverage about it, and it has prompted a lot of questions.
So what's the coverage about?
UPS is about to enter into contract negotiations
with the Teamsters.
And there's a lot of people who are under the impression
that a strike is kind of inevitable.
And that has led to the coverage,
because if there is a strike, it will
be the largest strike in US history
against a single company.
So what are the main questions?
The first is, are they really going to strike?
And the answer to that is they might.
That's up to management.
That is up to management.
Generally speaking, it isn't the union
that determines a strike is going to happen.
It's management being intractable
in contract negotiations.
Does the union have the ability to go on strike?
They recently elected some new leadership, Sean O'Brien.
And I mean, he certainly seems like the type
that would go on strike.
And they have a $300 million strike fund.
So yeah, I mean, that seems likely.
If there's going to be one, it's probably going to start at 12
01 AM on August 1.
But we'll have to wait and see.
The next question is about the GDP.
Because of the possibility of a strike,
there are a lot of articles talking about the fact
that UPS facilitates 6% of the United States' GDP.
Yeah, that's a lot.
I mean, that's a whole lot.
And yes, if they go on strike, it
is absolutely going to cause supply chain issues.
20 million packages a day.
Yeah, it will be incredibly disruptive
if they go on strike.
Last time they went on strike was back in the 90s.
And it was way before everybody was doing their shopping
online.
It will absolutely be disruptive.
And then the next question is about their pay.
Do they really make $95,000 a year?
What could they possibly have to strike about?
Yeah, that's the average pay, I think, is $95,000 a year
for 350,000 Teamsters who facilitate 6% of the US GDP.
I mean, I don't know how much is that worth to you.
Aside from that, I would point out
that when unions go on strike, it is rarely just about pay.
One of the things that UPS drivers are asking for
is AC, air conditioning, like the whole heat wave
thing that's happening.
A lot of those trucks don't have AC.
And that's something that they won't.
But yes, they absolutely have the ability.
They have the leadership.
They've got a pretty big strike fund built up.
And if management doesn't come across with the negotiations,
then yeah, there's probably going to be a strike.
Yeah, it's probably going to be really disruptive.
That's kind of what strikes are for.
Now, one thing that I want to point out
as people talk about the fact that they're the highest paid,
tell me again how unions don't work.
You get all of these articles talking about the fact
that they're the highest paid and therefore they
shouldn't strike.
Why do you think they're the highest paid?
Because they have the best representation.
Because they're bargaining as a collective
rather than just an individual walking in trying
to strike a deal with a multi-billion dollar
company that doesn't even know their name.
They know who the Teamsters are though.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}