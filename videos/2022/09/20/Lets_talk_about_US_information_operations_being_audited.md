---
title: Let's talk about US information operations being audited....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UT9cBOuaAsI) |
| Published | 2022/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into the topic of information operations and spy activities, sparked by an audit seeking disclosure of ongoing information operations by the Department of Defense.
- The audit was initiated by the Undersecretary of Defense for Policy, requesting a full accounting of all information operations being conducted.
- Information operations involve spreading information, whether true or false, to achieve specific objectives, with average people inadvertently becoming impacted by these campaigns.
- Concerns arose when two firms identified a U.S.-based information operation network promoting pro-Western narratives in countries like China, Russia, Afghanistan, and Iran.
- There is a misconception that this audit will result in halting U.S. information operations, but Beau clarifies that it is more about evaluating and improving their effectiveness rather than discontinuing them entirely.
- The audit may lead to the establishment of basic guidelines or guardrails for information operations to enhance their impact and reach.
- Beau points out that the primary concern may not be the content of the information operation but rather its lack of success and effectiveness, as evidenced by low follower counts and poor engagement rates.
- The U.S. government is unlikely to completely cease information operations but may adjust policies based on the audit's findings.
- The audit aims to gauge the effectiveness of information operations and potentially enhance their impact rather than terminate them.
- Beau concludes by suggesting that the audit is more about assessing effectiveness and adjusting policies, as knowledge plays a significant role in this realm.

### Quotes

- "They're not going to surrender the information space to the opposition."
- "The idea that the civilian leadership of the Department of Defense is about to tell them to stop doing this, that's not a thing."
- "If what was uncovered is representative of the whole, they're not doing well, and they will need to adjust policy."
- "Knowing is half the battle."
- "Y'all have a good day."

### Oneliner

Beau clarifies misconceptions about a Department of Defense audit on information operations, focusing on evaluating and enhancing effectiveness rather than ceasing operations.

### Audience

Information Consumers

### On-the-ground actions from transcript

- Analyze and critically think about the information you consume, especially on social media platforms (suggested).
- Stay informed about information operations and their potential impacts on society (suggested).

### Whats missing in summary

Importance of being vigilant and critical about the information consumed online.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about information consumption
and information operations and spy stuff again,
because there have been some interesting developments.
And it started with coverage of an audit.
Audits aren't normally interesting,
but this one is kind of,
because I think people might be misreading what it means.
The Undersecretary of Defense for Policy
has basically kind of put word out through DOD
that by next month, they want a full accounting.
They want full disclosure of any information operations
that are being run.
Now, we've talked about information operations
on the channel before.
These are campaigns to spread information,
sometimes true, sometimes not, for a specific goal.
We, as average people, end up as just getting caught
in the crossfire of it.
We talked about this a lot when Russia invaded Ukraine.
And we talked about the general premise
of being able to influence events from afar,
increase morale, demoralize the opposition,
create doubt, and so on and so forth.
And the US engages in this.
Now, this is the information
that they've asked for this report.
They've asked for this accounting.
And there have been a lot of concerns
that have been voiced because, I guess, in August,
two firms, Grafica and Stanford Internet Observatory,
they identified what appeared
to be a US information operation, a network.
It was never truly identified
as being from the defense apparatus,
but this collection of accounts,
it promoted pro-Western narratives
in the countries you would expect,
China, Russia, Afghanistan, Iran, places like that.
The assumption is that it was US, maybe UK-based.
There were also some posts put out by this network
that were outside of best practices,
things that were just inflammatory
for the sake of being inflammatory,
not really focusing on any direct goal.
So the conventional wisdom about this audit
is that it's going to be to curtail US operations.
There are some that have suggested
that this is going to mean an end
to US operations like this.
That's wrong.
I can promise you that part's wrong.
The United States government
is not going to cede an entire domain.
They're not going to surrender
the information space to the opposition.
The idea that the civilian leadership
of the Department of Defense
is about to tell them to stop doing this,
that's not a thing.
There is a possibility
that they're going to throw up
some basic guidelines,
but I think there might be something else.
They may be looking into it
because the information operation was bad,
not like in content or anything like that,
although there were certain posts
that were outside of the norms.
It wasn't successful,
and that may be the concern.
The information that's available
says there were 146 Twitter accounts,
39 Facebook accounts,
and 26 Instagram accounts.
It's a pretty big network.
The goal of an information operation
is to have influence.
To have influence,
you need to have reach.
Only one out of five of those accounts
had more than a thousand followers.
This may not be a moment
where the outcome is going to be
some kind of curtailing
of U.S. information operations.
This may be,
why aren't y'all doing better?
The numbers that were available
as far as interactions and stuff like that,
there are people who are on YouTube
who, if their social media person
came to them and said,
hey, this is what we got this week,
they would fire them.
They were really bad numbers.
So that may have something to do with it.
The interesting thing to me
about all of this is, one,
the immediate assumption
that the U.S. is going to try to curtail it.
That's not really a thing.
They may want it to be better.
They may set up some guardrails.
If there was too much overflow
and too much of what went out
filtered back to the United States
and started influencing American citizens
without being effective over there,
that may be a cause for concern.
But the idea that they're going to stop,
no, that's not true.
Basic guidelines is about the most
you can expect there.
But I think that a much larger reason
for wanting to get an audit done
is to see how effective it is.
Because if what was uncovered
is representative of the whole,
they're not doing well,
and they will need to adjust policy.
So I think that's probably
more what this is about.
Because as we all know,
knowing is half the battle.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}