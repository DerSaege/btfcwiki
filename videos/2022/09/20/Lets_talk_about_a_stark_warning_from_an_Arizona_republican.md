---
title: Let's talk about a stark warning from an Arizona republican....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e5g2L2pw49M) |
| Published | 2022/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into the direction of the Republican Party and the independent state legislature theory.
- Rusty Bowers, a long-time Republican, spoke out against a bill that aimed to give state legislatures power over electoral votes.
- The bill could allow states to override the popular vote and assign electoral votes to a different candidate.
- Bowers successfully halted the bill with obstruction techniques, but he warns that it could resurface.
- If such legislation lands on the desk of a Republican governor willing to sign it, Bowers warns of descending into fascism.
- The independent state legislature theory challenges the core principles of a constitutional republic and representative democracy.
- Beau stresses the importance of paying attention to this theory and the internal strife within the Republican Party regarding it.
- Despite lacking constitutional validity, a significant faction within the Republican Party supports this theory.
- Beau signals that further exploration of this topic will be needed in future videos.
- He concludes by urging people to stay vigilant and prepared for the potential consequences.

### Quotes

- "Welcome to fascism."
- "It's real. This drive is real."
- "There is nothing to actually suggest this is a valid theory under the Constitution."
- "It's something the American people need to pay attention to."
- "Y'all have a good day."

### Oneliner

Beau warns of the Republican Party's push for the independent state legislature theory, risking democracy and constitutional principles, urging vigilance from the American people.

### Audience

Americans

### On-the-ground actions from transcript

- Pay attention to legislation proposed by your state legislature and its potential impact (implied)
- Stay informed about political developments and theories challenging democratic principles (implied)
- Engage in civil discourse and raise awareness about threats to democracy within your community (implied)

### Whats missing in summary

In the full transcript, Beau likely provides more in-depth analysis and historical context on the independent state legislature theory and its implications for American democracy. Viewing the complete video may offer additional insights and explanations. 

### Tags

#RepublicanParty #IndependentStateLegislature #Democracy #Fascism #Vigilance


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about where the Republican Party is headed,
the independent state legislature theory, how you're likely to hear it,
and a word that you almost never hear Republicans say.
Rusty Bowers is a Republican.
He's been a Republican for like 30 years.
I want to say he entered the Arizona State House maybe 93, something like that.
Been around a long, long time.
He is now the outgoing Republican Speaker of the State House in Arizona.
And he said that the Republican Party, in some of their legislation,
was attempting to take us back into the dark ages.
There was a bill that in large part, you could say, was derailed because of him.
He used some typical Republican obstruction techniques
to stop it from going anywhere.
And it was the bill that was designed to allow state legislatures to really
be the people in charge of deciding where the electoral votes go.
Now, when you say it like that, it doesn't really sound like horrible
until you realize what that means is that, let's say hypothetically,
this doctrine is in effect in your state.
In your state, votes 70% in favor of candidate A.
The state legislature can say, no, our electoral votes go to candidate B.
And there's nothing you can do about it.
It's derailing the idea of the republic, the constitutional republic,
the representative democracy that the US has.
It's a way to completely destroy it.
And it is being championed by the Republican Party.
Now, you do have some people within the Republican Party fighting against it.
And Bowers did fight against it successfully here.
However, he has a pretty stark warning that basically says they can bring it back.
And the next person may not spread it around the committees to slow it down.
It may get through.
And he says that if it finds itself, that kind of legislation finds itself
on the desk of a Republican governor willing to sign it,
he said, welcome to fascism.
His words, a Republican of 30 years.
It's real.
This drive is real.
And you're going to hear a lot about the independent state legislature
theory in the future.
I've talked about it in the past.
And we will certainly end up talking about it in the future.
There is nothing to actually suggest this is a valid theory
under the Constitution.
And we will get into why in future videos.
But this is being pushed by a significant enough portion
of the Republican Party for it to cause real concern,
for it to be something that Republicans themselves are out there
raising the alarm about.
It's something the American people need to pay attention to
and need to be ready for.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}