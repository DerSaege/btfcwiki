---
title: Let's talk about Trump getting exactly what he asked for....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1HMuLRhxDdE) |
| Published | 2022/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's legal team wanted a special master and a specific judge, and they got both.
- The judge, Judge Deary, wants the process to be done quickly, likely due to their experience on the FISA court.
- The judge is seeking specifics on Trump's declassification efforts, which the legal team is hesitant to provide.
- Without evidence of declassification, privileged documents automatically go to the government.
- Trump's social media statements contradict his legal team's filings, leading to a potential collision of arguments.
- The special master may face challenges in deeming documents privileged without clear evidence.
- Laws regarding national defense information do not require classification, making the declassification argument puzzling.
- The legal team's defense strategy regarding declassified documents is compared to a "Schrodinger's defense."
- Judge Deary may not be likely to accept the defense's arguments for withholding documents.
- Ultimately, the situation may result in immediate document disclosure or the legal team providing specific arguments to filter out documents temporarily.

### Quotes

- "Their position is that they want to save that in the event of an indictment."
- "It's a Schrodinger's defense, I guess."
- "But this is kind of what Trump legal team is trying to say."

### Oneliner

Trump's legal team faces challenges regarding declassified documents as the judge seeks specifics, potentially leading to immediate disclosure or the need for specific arguments.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact legal experts for insights on the implications of declassification in legal cases (suggested).
- Join or support organizations advocating for transparency in legal proceedings (implied).

### Whats missing in summary

Insights on the potential legal ramifications and public implications of the ongoing dispute over declassified documents.

### Tags

#LegalAnalysis #Trump #Declassification #SpecialMaster #JudgeDeary


## Transcript
Well, howdy there, Internet people.
It's Bill again.
So today we are going to talk about being careful what you wish for, what you ask for,
how you can't always get what you want, and how, well, sometimes you do.
Okay, so Trump's legal team, they wanted a special master, and they got a special master.
They also wanted a specific judge.
Based on reporting, they wanted that judge because they believed the judge would be a
little bit more skeptical of the feds because the judge was on the FISA court for seven
years.
They got that judge.
That's Judge Deary.
Trump legal team is getting everything they want.
And now we find out what Judge Deary wants.
The special master wants this done pretty quickly.
It's almost like spending a lot of time on a FISA court looking over stuff related to
national security means that you might understand that time is of the essence in some of these
cases.
The judge also wants to know specifics about Trump's declassification efforts.
The specifics, well, the Trump legal team doesn't want to talk about that, which probably
makes sense.
Their position is that they want to save that in the event of an indictment.
That's a unique position to take.
The whole point of the special master would be to sort out things that were privileged.
Now without any kind of evidence or even statement that certain documents were declassified,
there is literally no way for privilege to apply.
So those would automatically go to the feds.
But if they were to say all of them were declassified and the process for doing that was
less than verifiable, that would look really bad.
It would also look really bad politically.
It would look really bad in the eyes of the court if there was no way to prove that this
declassification took place.
It would also look really bad in the eyes of the public if he declassified all of them
and there was incredibly sensitive information that might be compartmented in this stack
of information that he declassified.
That would also be really bad.
Trump's statements on social media do not match the filings that his legal team is putting
in.
And eventually those two statements, those two sets of arguments are going to collide
and it is going to be a train wreck when it does.
I cannot think of a way for the special master, even if he was incredibly sympathetic to Trump,
I don't know of a way in which he could take documents that are stamped, that bear classification
markings and say that they are privileged and that the government can't use them without
some kind of specifics, some kind of argument suggesting that they're a. no longer government
property and b. aren't relevant or are somehow privileged.
It's worth noting that the whole declassification thing doesn't actually matter.
We've talked about it before in other videos, but just to be clear, these laws hinge on
national defense information.
Doesn't have to be classified.
So I'm not really sure why this statement was made and why they're following, kind of
hinting to it at least, in court filings.
To this point, there hasn't been a real claim that Trump declassified anything in the court
filings.
They've hinted to it, but they haven't actually said it.
And that puts the special master in a unique situation because he can't withhold documents
from the government based on, well, those are mine now.
Put it into any other context, you know, if it was one of us commoners.
Let's say the cops get a warrant for your house and they believe that you knocked over
a bank.
And when they search the house, they find a bag of money.
And that bag of money is stuffed in envelopes from that bank and the serial numbers match
the money that was taken from the bank.
And you say, no, you can't use that as evidence.
That's my money.
I got it legally.
But then won't tell them how.
Now for us commoners, this is the point where the feds are like, hey, do me a favor, turn
around, put your hands on the wall.
And the conversation is kind of over at that point.
But this is kind of what Trump legal team is trying to say.
We're not going to say that we declassified these documents and then therefore have to
make another argument about why they're privileged.
But we're also not okay with the government using them.
It's a Schrodinger's defense, I guess.
I don't think that Judge Derry's going to go for this.
I could be wrong, but it seems incredibly unlikely that the judge is going to be willing
to just be like, okay, sure, you don't want them to use these documents for reasons.
That's fine with me.
That seems really unlikely.
So I think that either the feds are going to get the documents immediately or the Trump
legal team is going to have to provide some kind of specifics and argument to filter out
those documents, at least for some period of time.
Again, I would point out that at the end of this, as far as the laws are concerned, it
doesn't matter if they're classified.
This is all about a delay.
But when you look at the timeline from Derry, it doesn't seem like Derry wants there to
be a delay.
It seems like he may not want to be doing this throughout the holidays and wants to
wrap it up before then.
But anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}