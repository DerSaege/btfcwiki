---
title: Let's talk about the boyfriend loophole....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A50gV2kuVbc) |
| Published | 2022/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Addresses Republican men about a scenario involving domestic violence.
- Questions why it's acceptable to be willing to take someone's life but not their gun.
- Criticizes the Violence Against Women Act for not closing the boyfriend loophole.
- Points out that most law enforcement calls and half of homicides are related to dating partners.
- Expresses confusion over the lack of concern for protecting women from domestic violence.
- Advocates for the Violence Against Women Act as a necessary legislation.
- Questions why Republican senators prioritize other issues over protecting women.
- Emphasizes the importance of the act in reducing violence and protecting women.

### Quotes

- "You're more concerned about a bathroom than somebody actually hurting them."
- "But you're not willing to remove their right to own a firearm."
- "It almost seems like you don't actually care about the women in your lives."
- "They care more about those guns than they do their daughters, their sisters, their mothers."
- "I do not understand the gun culture's fascination with protecting those who provide the excuses to ban guns outright."

### Oneliner

Beau questions Republican men on prioritizing guns over protecting women from domestic violence and advocates for closing the boyfriend loophole in the Violence Against Women Act.

### Audience

Republican men

### On-the-ground actions from transcript

- Advocate for closing the boyfriend loophole in the Violence Against Women Act (suggested)
- Keep fighting for the legislation that protects victims of domestic violence (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the lack of protection for victims of domestic violence under current legislation and calls for action to prioritize women's safety.

### Tags

#DomesticViolence #GunControl #ProtectWomen #Legislation #Advocacy #Equality


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk to the Republicans in the crowd,
the Republican men specifically.
Because I've got a question. Something just happened,
and it specifically applies to them,
and I'm just confused about something.
So I want to lay out a scenario.
Let's say your sister
or your daughter,
let's say her name's Jill,
and she's dating a guy named Jack.
And Jack leaves his house one day
and goes over to Jill's house,
and they get into an argument, and he beats her up.
What are you going to do?
What are you going to say you're going to do anyway? You may not actually do it.
But what's your gut reaction?
Probably a whole bunch of statements about hunting accidents, or about how, you know,
they don't find them if it's not shallow.
Stuff like that, right?
So I need somebody to explain to me how it's totally cool,
accepted, even something that is seen as a virtue,
to be willing to take their life,
to take Jack's life,
but not his gun.
I need some explanation on that, because it doesn't make any sense to me.
The Violence Against Women Act
will once again not close the boyfriend loophole.
If you don't know what this is,
there is current legislation that exists that say,
hey, you get convicted even of a misdemeanor crime of domestic violence,
oh, you're losing your gun.
However, it only applies to certain situations.
The one I outlined, that doesn't apply.
That doesn't apply because they don't live together.
That's dating partner, not spousal.
They're not in cohabitation.
It's this giant loophole that exists.
It's worth noting that 80% of law enforcement calls in this category,
they're dating partner, not spousal.
Half of the homicides.
It's also worth noting that two thirds of the mass incidents that occur
have a DV link.
I don't understand.
What happened to, you know, think of the children, protect your daughters.
You're more concerned about a bathroom than somebody actually hurting them.
I don't understand it.
And before you, if you're new to the channel, before you think,
oh, another gun grabbing liberal, go watch my other videos on guns.
You will find one piece of legislation that I advocate for,
and it's this, because it'll work across a whole bunch of different metrics.
But anytime it comes up, it's the Republican Party standing in the way
of it getting through.
And here it is again.
I do not understand how with the predominant beliefs,
when it comes to the right wing gun owning crowd,
why you're cool with this.
I mean, let's be honest.
If you could handle it outside of the judicial system,
what would the punishment be?
But you're not willing to remove their right to own a firearm.
That seems odd.
It almost seems like you don't actually care about the women in your lives,
that you just are really kind of upset because when he beat her up,
it was an insult to you.
He violated your property rights, and that's why you're mad,
because you obviously don't care about her getting hurt.
The legislation, the Violence Against Women Act,
it's still a good bill, even without this.
It really is.
There's a lot of benefits to it.
But this is the one thing that when it comes to firearms legislation,
it's going to work, no question about it.
It is going to be wildly successful in reducing the loss of life.
And in theory, it should totally fit in with the responsible gun owner mentality.
But they care more about those guns than they do their daughters,
their sisters, their mothers.
It's worth noting, because I'm using this framing and I really don't like it,
but I think it's the only way to get the point across,
that this doesn't just apply to women.
It applies to a whole bunch of different scenarios,
and it would reduce violence across the board.
And it would be based on people's actions and things that they did
that oftentimes predict what they're going to do.
This isn't a blanket thing.
It's pretty specific.
I do not understand the gun culture's fascination with protecting those
who provide the excuses to ban guns outright.
This bill, it'll come up again.
People will keep fighting for it.
People will keep advocating for it.
And eventually, it will get passed.
I'm just wondering why Republican senators,
who are so concerned about what their daughters read,
what they're exposed to, don't care about this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}