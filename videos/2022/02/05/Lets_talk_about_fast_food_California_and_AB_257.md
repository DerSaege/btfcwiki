---
title: Let's talk about fast food, California, and AB 257....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tA9hPmZ7LR0) |
| Published | 2022/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A bill in California, AB257, passed in the state assembly and will impact over half a million fast food workers if it goes through the Senate.
- The bill sets up a council to protect fast food workers, ensuring fair pay, wage standards, training, safety, and more.
- This council will serve as a de facto union representative for the industry.
- Joint liability is created between franchisees and big companies for labor violations at fast food joints.
- Big companies might push back, claiming they lack control over local franchises, but standardization across all locations implies influence.
- The consistency in fast food chains suggests that big companies have more influence than they claim.
- The big companies demand uniformity to create familiarity and bring customers back.
- Money seems to be the primary reason for big companies not ensuring fair treatment of workers.
- Big companies are expected to pour money into defeating the bill, indicating awareness of widespread violations.
- Judging by the effort to defeat it, the bill's worth can be inferred.

### Quotes

- "It's the big company that demands that because they know that familiarity is what brings people back."
- "Sometimes, you can judge the worth of a bill by how much effort goes into defeating it."

### Oneliner

A bill in California aims to protect over half a million fast food workers by establishing a council and joint liability between franchisees and big companies.

### Audience

Californian residents

### On-the-ground actions from transcript

- Review AB257 and reach out to state Senators in support of the bill (suggested)
- Make a call, send an email, or tweet to show support for the bill (suggested)

### Whats missing in summary

The full transcript provides a deeper understanding of the influence big companies have on fast food workers' treatment and the importance of supporting legislation like AB257.

### Tags

#California #FastFoodWorkers #LaborRights #JointLiability #SupportLegislation


## Transcript
Well, howdy there, Internet people. It's Bo again.
So today we are going to talk about fast food in California,
in workers, and really fast food everywhere,
because, I mean, it's kind of the same
all across the country, right?
A bill in the state assembly there in California,
AB257, just passed.
And this bill will impact,
if it makes it through the Senate, the state Senate,
which is where it's headed next,
it'll impact more than half a million workers.
What it does, two things really that matter.
One is that it sets up this council,
and this council's whole job
is to look out for fast food workers,
you know, make sure that they get paid for what they do,
maybe set wage standards, training standards,
safety standards, treatment of employees,
basic labor stuff.
This council is going to be like a de facto union rep
for the whole industry, kind of, and that's cool.
But see, it also does something else.
It creates joint liability
between the franchisee and the big company.
You know, most fast food joints,
the company that owns the name,
that's not really the company that runs it.
It's a local franchise.
But see, it creates this joint liability
where the big company is responsible,
at least in part, for labor violations
that happen at the franchises.
Now, the big companies are undoubtedly
going to push back against this.
They're going to be like, hey, we don't have that kind of control
because that's kind of been
what they've been saying this whole time.
You know, they'll say that it's the local company
that handles all of that stuff,
and they really don't have any way to control it.
And I mean, that makes sense,
unless you think about it or you travel.
You know, last year I drove out to LA,
I drove to California.
And thing is, everywhere we went,
and we ate fast food the whole way,
it was pretty much the same.
I mean, that's why those places do so well,
because it's comforting in a way.
Doesn't matter where you are,
you can walk into one of these places,
you know what's on the menu,
you know what you're going to get,
you probably have a standard order,
a number one with a Coke or whatever it is, right?
And those sandwiches, they all taste the same.
These companies spell out exactly
how much mayonnaise goes on a chicken sandwich.
They have policies for that.
You look at the signage, the marketing,
it's all the same.
Everything is the same in all of these places.
And it's the big company that demands that
because they know that familiarity
is what brings people back.
If they can determine how much mayonnaise
goes on your chicken sandwich,
so there's a consistent amount across locations,
across different franchises,
it seems like they might have a little influence
when it comes to how that company's run,
how those local places are run.
I would think that it might be in their interest
to make sure that their workers are treated well,
that their workers are paid well,
that safety training takes place, all of that stuff.
I mean, part of it is that smiling cashier, right?
That's something that the big company cares about,
that public face.
Why not take care of that public face?
Now, we all know why. Money.
And undoubtedly, the big companies,
the big fast food giants,
they are going to flood California with cash
trying to defeat this.
Which, when you think about it,
also kind of indicates they have a pretty good idea
of the scope of the problem.
Because they wouldn't devote a whole lot of cash
to defeating it if they didn't think
that there were a bunch of violations occurring.
Violations that if this bill passed,
they would at least be partly liable for.
Sometimes, you can judge the worth of a bill
by how much effort goes into defeating it.
So, if you live in California,
AB 257 is the bill that just passed.
It will be headed to the Senate.
Maybe review the bill.
Make a call, send an email, or tweet
if it seems like something you think would benefit
those people who make your food.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}