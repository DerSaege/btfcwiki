---
title: Let's talk about State vs AP on Russia on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OAVS6jmHywk) |
| Published | 2022/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains an exchange between a State Department spokesperson and an Associated Press journalist regarding Russia's alleged plans to invade Ukraine.
- Analyzes the feasibility of the State Department's claims about Russia manufacturing a pretext for invasion.
- Breaks down the different ways the U.S. may have obtained the information and why it's being kept classified.
- Explores the role of journalists like the Associated Press in questioning and investigating government claims.
- Emphasizes the importance of protecting intelligence methods and assets in international relations.
- Notes the evolving dynamics between government spokespeople and the media in near-peer foreign policy contexts.
- Suggests that releasing information strategically can deter conflicts rather than escalate them.
- Acknowledges the necessity for journalists and government spokespersons to maintain a level of skepticism and accountability.
- Concludes that both sides, the State Department and the Associated Press, were fulfilling their respective roles in this exchange.

### Quotes

- "That's State Department's position. State Department isn't wrong in wanting to protect the intelligence community's assets and protect the way they gather information."
- "Journalists and government spokespeople, they're not actually supposed to get along."
- "Both sides were doing their job."

### Oneliner

Beau breaks down the tense exchange between the State Department and the Associated Press, delving into the feasibility of claims and the vital roles of both parties in international relations.

### Audience

Foreign policy observers

### On-the-ground actions from transcript

- Analyze international news sources for multiple perspectives (suggested)
- Support investigative journalism efforts (exemplified)

### Whats missing in summary

Deeper insights into the delicate dance between intelligence sharing, media scrutiny, and conflict prevention in international affairs.

### Tags

#ForeignPolicy #Journalism #StateDepartment #AssociatedPress #Russia #Investigation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about an exchange that
occurred between a State Department spokesperson
and a journalist with the Associated Press about Russia
and what they might be planning.
That exchange got tense.
And people started taking sides and saying, who's right,
who's wrong, going back and forth.
So what we're going to do is we're
going to talk about, first, feasibility.
Is what state department is what they're alleging feasible?
Is it something we should really even consider?
Then we're going to go over state department's position.
And then we're going to go over the Associated Press
position.
And then we will talk about who's right.
OK, so if you have no clue what I'm talking about,
State Department spokesperson was like, hey, we have information suggesting that the Russians
are going to manufacture a pretext to invade Ukraine, and it involves creating this footage
and creating the impression that there was an attack and blah, blah, blah, blah, blah,
blah.
And a journalist with the AP is like, hey, I don't believe you.
I'm paraphrasing here.
I'm like, I don't believe you.
I'm going to need some kind of real evidence.
And State Department spokesperson is like, well, I just told you.
We have this information, we declassified the finding, and here you go.
And the journalist with AP is like, yeah, that's just you saying it.
I'm going to need to see something.
And they go back and forth about it.
So is what State Department alleges, is that feasible?
Yeah, of course.
Of course it is.
The best evidence of this is why is AP suspicious?
Because they covered the last time a world power manufactured a pretext to invade a country.
World powers do this.
Pretty much every major power has done this at some point in time in some way.
So the idea that Russia may be doing this, or maybe they don't even intend on using
it necessarily, but they want to make sure they have everything ready in case they need
to.
The idea that this is being set up and that the wills are in motion to create the impression
that Ukraine attacked them, yeah, I mean, that's feasible.
It's plausible.
I personally would say it's likely, I think it would be a mistake on Putin's part to
have not had this set up just in case.
So now let's go to State Department's position.
If they have evidence, why don't they just release it, right?
I mean if the Russians are doing it, it wouldn't be a surprise, there's nothing that State
Department could have or that the US intelligence community could have that Russia wouldn't
already know, because they're the ones doing it, right?
It goes back to what we have talked about before.
The fact that the US has the plans for the new jet fighter, that's not secret.
It's secret that they have it, but it's not the plans that make it secret.
Russia already has the plans, it's their plane, right?
That's how the US obtained that information.
That's what they're trying to keep secret, means and methods.
In this case, there are three likely ways that the United States could have gathered
this information.
The first would be some form of aerial surveillance.
Maybe it was conducted by a plane the Russians don't even know the US has yet, some kind
of aircraft of some sort, maybe not necessarily a plane.
So that would be one reason to keep it secret.
Another would be, what if this was being manufactured deep inside Russia, and the only way the US
could have got it through that aerial surveillance would be if they were violating Russian airspace?
Yet another reason to keep it secret.
keep it secret how they got the information. So another way the U.S.
could have found out about it would be by intercepting a transmission of some
sort, either a phone call talking about it or an email of the footage, you know.
But what they don't want is to let the Russians know that that method of
communication is compromised. So they can't release it. Another thing, another
way they could've got it is by having a person involved in that special activity, having
an asset in that crew.
Maybe they actually took a video from their cell phone or something.
If they released that footage, what would happen?
The Russian intelligence would look at it and be like, okay, the person was standing
here when they videotaped it, so let's get all the pictures from that day, talk to everybody,
who was standing where. And then they figure out who was standing there. They
put together a little picture, a little chart, and they lay everything out and
they're like, okay Vlad was standing here, next to him was Igor, and Vasily would
have been the person videotaping. Bring me Vasily. Actually just go ahead and
send him to row 13, plot 12. So it would be bad to release the evidence that they have.
That's State Department's position. State Department isn't wrong in wanting to protect
the intelligence community's assets and protect the way they gather information. World powers
do this. This is how the game is played. Does that make AP wrong? No, of course not.
AP is, they're journalists. They're not just supposed to run with every press
release they're handed. They're supposed to investigate. We talked about hard-hitting
questions. Those that were being asked, that's what hard-hitting journalism looks like.
Yeah, I understand what you're saying, but you've given me zero evidence.
I don't believe you, you know?
So the Associated Press, that journalist, not wrong, not wrong at all for questioning that.
They're both correct in their stance.
And this is one of those things where now that we are back in the near-pure world, it's
going to happen more and more often.
But a lot of times you will see the intelligence community and foreign policy of the United
States, you'll see it attempt to communicate with the other side, whoever the other side
is, through the media.
And that's really what's happening right now.
They're attempting to tell the Russians, hey, we already have the footage or whatever that
you guys have manufactured.
So when you release it and you say that this happened, we'll be able to blow your cover.
So maybe don't try that.
They're trying to defuse the situation.
Should the AP play along with it, even if they understand that?
No, that's not their job.
Their job is to do exactly what they did.
department spokesman that job is to do exactly what he did. There has been a lot
of camaraderie between the spokespeople for government agencies and the media.
As we move into near-peer stuff when it comes to foreign policy, yeah that's not
going to happen as much anymore because we're not dealing with countries that
don't have advanced intelligence services. We're dealing with countries
that do. So there has to be a higher standard when it comes to what gets
released. You know, when you see those satellite photos that get released to
the media, you know, it's like, oh look, here's proof that there's there was a
rocket here or whatever. Before those photos are released, the intelligence
community goes through them and blurs them, obscure stuff, and it makes the
image look worse than what they actually have. This is done to make sure that the
opposition nations don't know the United States' true capabilities.
Protecting means and methods goes that far. It goes so far that they will
actually alter information that they're putting out that's accurate. Here's this
photo. We're gonna make it more fuzzy, harder to see, because we don't want the
other side to know how good our stuff is.
We don't want them to have an accurate picture of what we're capable of doing because we
have to protect our means and methods.
That's what that conversation was about.
They're coming at it from two different points of view and they're both right.
They're both doing their job.
Journalists and government spokespeople, they're not actually supposed to get along.
They're not supposed to be the best of friends.
One is supposed to uncover what the other one is trying to hide.
So my personal belief is that Russia is probably doing this, but this isn't a reason for the
United States to go on the offensive, and I don't think they're trying to use it that
way.
think they're releasing this information in an attempt to diffuse the conflict
before it starts. Now, if this same scenario occurred and an intelligence
agency spokesperson was saying, oh, we have the evidence, we just can't show it
to you, but we need your support so we can invade country Y, no, doesn't cut it.
Doesn't cut it. We have to see the evidence then. This isn't offensive, it's
defensive. They can get a little bit more benefit of the doubt. Okay, so let's say
that they're not telling the truth. So Russia doesn't do this. No conflict
starts. They're not proposing a preemptive strike because of this
information. They're putting it out there in hopes of deterring Russia from using
And the State Department spokesperson said that.
Something else that I would point out is that while the Associated Press journalist was
very clear in what they were saying, the spokesperson for State Department was not.
I don't think that the spokesperson expected to be challenged on this information, so was
kind of taken aback by it.
Once that challenge was kind of issued, probably should have regrouped and started from the
beginning, going over the information again and explaining why it was being released.
I know people want to take sides in this.
Both sides were doing their job.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}