---
title: Let's talk about tips, tiktok, and Virginia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WKZ9DdxA-wQ) |
| Published | 2022/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor Yonkin campaigned on giving parents a say in education, only to have the state take full control and declare certain topics off-limits as thought crimes.
- Yonkin aims to eliminate divisive subjects like history and literature to shape young minds by restricting access to information.
- In an effort to bolster state power, citizens are encouraged to report on each other through an email address established by the state.
- TikTokers flooded the state's education email address with misinformation, overwhelming the system and rendering it ineffective.
- This form of activism is a temporary solution to challenge policies aiming to manipulate education and limit historical truths.

### Quotes

- "You know, if you want to have thought crime, you gotta have the thought police."
- "People just have to spend too much effort sifting through the information."
- "We have to hide our history because we can't be proud of it, I guess."
- "If you teach that, students might want to continue to change, and a whole bunch of people at the top who enjoy the status quo, well, they really don't want that, now do they?"

### Oneliner

Governor Yonkin manipulates education, restricting historical truths and encouraging citizens to report on each other, countered by activists flooding the state's email with misinformation.

### Audience

Educators, Activists, Citizens

### On-the-ground actions from transcript

- Flood official channels with information to overwhelm systems (exemplified)

### Whats missing in summary

The emotional impact of restricting education and historical truths, the dangers of manipulating information for political gain.

### Tags

#Education #GovernorYonkin #ThoughtPolice #Activism #Misinformation


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk
about the attempts to restructure the education system in Virginia under
Governor Yonkin. You know, Governor Yonkin, he campaigned on the idea that it
was smart to get parents to believe that they had a say in their child's
education and then have the state just take it over completely and declare a
bunch of subjects off-limits and, you know, basically declare anything that the
state didn't like as a, I don't know, thought crime. And unsurprisingly in today's climate, it
worked. He got elected. And he has attempted to implement this policy of getting
rid of divisive subjects, which is near as I can tell includes, well, mainly
history. I guess there's a little literature thrown in there as well. And
he's pursuing this policy of getting rid of education and establishing a method
in which the state, which we all know is always right, can truly shape and mold
the minds of young people by denying them access to information. In order to
further this effort at creating a, you know, just totally all-powerful state,
everybody knows that if you want that, you also need to make sure that citizens
have been conditioned to inform on each other to the state. You know, report
people to the thought police. If you want to have thought crime, you gotta have the thought police, you know.
So he established an email address. It's helpeducationatgovernor.virginia.gov.
And this was supposed to function as Big Brother. This was going to be the
clearinghouse. You know, if a teacher taught something like American history,
you could report them to this email address and something would happen. I'm
not sure what, you know, drones would show up. I don't know. And the thing is, it's been up a few
days and realistically he probably would have got away with it too if it weren't
for those meddling kids on TikTok. A bunch of TikTokers put out the call to
send, let's just say less than accurate information in large volumes, to this
email address. This is something that has occurred repeatedly recently because a
lot of the policies that state officials are trying to pursue are not really
things that would stand up in court. They've relied heavily on farming out
the enforcement mechanisms to citizens, trying to get them to turn on their
neighbor, you know, to create that all-powerful state. What a lot of people
on TikTok have done, and to great effect so far, is flood these email addresses,
flood these tip lines with information, provide them so much information that
they can't possibly go through it and determine what's real and what's not. And
in the past when they've done this, and they have done it quite a few times, the
end result is that the government ends up taking down the email address and
stop using it because it becomes a burden rather than a tool to make sure
that nobody's committing thought crime. People just have to spend too much
effort sifting through the information. Now this type of activism is obviously a
temporary fix, you know. This isn't going to change the fact that you have
somebody in office in Virginia who thinks it's a good idea to remove large
segments of American history, to make sure that students don't learn anything
too true, because if they do, well, then they might not look favorably on some of
the institutions in the United States. You know, we have to hide our history
because we can't be proud of it, I guess. We can't be proud of what occurred, and
that part makes sense. We could be proud of the fact that we changed, that that
could be something that schools could teach. However, if you teach that, students
might want to continue to change, and a whole bunch of people at the top who
enjoy the status quo, well, they really don't want that, now do they? Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}