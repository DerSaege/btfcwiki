---
title: Let's talk about Russia vs US at the UN....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R63Kee9uszI) |
| Published | 2022/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The recent UN meeting between the United States and Russia wasn't as intense as some may think, considering the historical context of such meetings.
- Both countries accused each other of provocation and war rhetoric, with the US having an advantage due to Russian troops on the Ukrainian border.
- NATO disunity may be a factor delaying resolution, with some NATO countries ready to back Ukraine while others are less inclined.
- The uncertainty lies in NATO's intent and response, with Putin possibly seeking a way out but with the situation still posing a risk of escalation.
- The ultimate outcome depends on how NATO decides to respond, which will impact Russia's actions.
- As a civilian, there may not be much direct impact you can have on the situation, and expectations need to adjust to the unpredictability of potential conflicts.

### Quotes

- "It will come out of nowhere. There will be hotspots that emerge and they will flare up randomly and without a lot of warning most times."
- "The ultimate outcome is going to depend on how NATO finally lines up and what they decide they're going to do."

### Oneliner

Beau provides insights on the recent UN meeting between the US and Russia, discussing accusations, advantages, NATO disunity, and the uncertainty of potential conflict escalation.

### Audience

Global citizens

### On-the-ground actions from transcript

- Stay informed on the situation and be prepared to support peaceful resolutions (implied)
- Advocate for diplomatic solutions and peaceful negotiations (implied)

### Whats missing in summary

More in-depth analysis on the historical context and implications of NATO disunity and its impact on potential conflict escalation.

### Tags

#UNmeeting #US #Russia #NATO #conflict #diplomacy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about the United States and Russia at the UN meeting.
A whole bunch of questions came in about it, and I'm just going to kind of run through
them real quick and go from there.
The overwhelming majority of the commentary on this has been that this was super intense.
It was a mess.
It was a show.
It was something else that this meeting, that was just wild, unprecedented, nah, not really.
We have to adjust our expectations of what these meetings at the UN are going to be like
now, because now it's near peer.
It's near peer now.
Nobody in this meeting got called a stooge or a jerk or a toady of American imperialism.
These are all things that have been said in these before during the Cold War.
Nobody banged their fist on the table or waved a shoe around.
When you're talking about tensions flaring in near-peer contests, this wasn't so bad.
So what actually happened?
The United States and Russia both tried to make their case.
Now, they both accused the other of being responsible for the provocation, of whipping
up the war rhetoric.
Now, the United States came out on top in this conversation, not because they made better
arguments, but because it's Russian troops on the border.
The US was going to come out on top from the beginning.
That's why the Russians voted against having this conversation to begin with.
The Russian position of don't let Ukraine join NATO, that's not something that's going
to fly at the UN because the whole idea of the UN is to respect individual country sovereignty.
So Ukraine, if they want to join NATO, well, that's just something that they can do.
So Russia was at a disadvantage from the beginning.
So they went back and forth, and Ukraine's there in the middle, like, it would be great
if y'all would stop this or take your argument somewhere else.
But it wasn't as extreme as a lot of people may be reading it.
Another question that came in was like, hey, you don't seem as certain that this isn't
going to turn into war as you were nine months ago.
Like, you know, nine months ago, you were adamant that this was just a training exercise
and this time you don't seem as sure.
I'm not.
The scope of this buildup is a lot bigger.
They moved a lot of equipment that you wouldn't normally move in training.
You know, they brought up perishable medical supplies as an example.
That's normally not something you waste.
Now then again, that could be misdirection.
That stuff could actually already have been expired and they just decided to use it for
training, but we don't know.
One of the things that may actually be holding the resolution of this up is NATO's disunity.
We've talked a lot about intent and how intent is important because neither side actually
wants a shooting conflict with the other.
Russia doesn't want to accidentally start a real war with NATO.
Because NATO's intent is all over the place right now, because you have United Kingdom
and Canada, they look like they're ready to go.
They're going to back up Ukraine no matter what.
A lot of the other countries in NATO are like, yeah, whatever, we'll move some troops up,
that they're not posturing to the same degree.
That disunity, that may have the Russians off balance because if they can't get a good
read on what NATO's going to do, they're not going to do anything.
If they knew for a fact that NATO wasn't going to respond or was only going to send weapons,
then they'd probably move in.
But if they don't know, they'll sit tight until they figure it out.
One other thing to keep in mind is that every day that goes by, that Ukrainian resistance
that's being fostered, that grows stronger.
I'm still of the opinion that at this moment, Putin is looking for a way out, but I'm not
certain of that.
There is still a chance that this is real.
It very well could be posturing, but the ultimate outcome is going to depend on how NATO finally
lines up and what they decide they're going to do.
If the US just says, okay, well, we're only going to go this far, then the Russians might
move in. But then again, if the United Kingdom or Canada decides, well, we're
going all the way, we're going to go ahead and put in troops, we're going to
back them up. If the Russians do move in, and they hit British or Canadian troops,
the US is in the war anyway. So there's a lot of variables to this. And when
When we get to this stage, you're talking about near-peer contests with a whole lot
of moving parts.
It's not as easy to predict.
This isn't like a lot of the conflicts we've had over the last 20 years.
There's a whole lot more at play, so it's always going to be a surprise.
As a civilian back home in whatever country you're in, at this stage, you're waiting
to find out.
There's not a lot you're going to be able to do to affect policy.
There's not a lot you're going to be able to do to change the outcome.
There probably needs to be an adjustment of expectation.
For a lot of people today, they have lived through a period where the United States always
just went through this thing and manufactured consent and got those war drums beating.
We knew it was coming.
In fact, most of us got convinced that it was a good thing, right?
That's not how it's going to work now.
It will come out of nowhere.
There will be hotspots that emerge and they will flare up randomly and without a lot of
warning most times.
That's an update on it.
The Ukrainian government is still training to mount a resistance if there is an invasion.
They are still trying to keep everybody calm, keep the rhetoric down so it doesn't take
their economy.
The United States is still blaming Russia, and Russia is still over there going, it's
just an exercise.
What's happening?
find out. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}