---
title: Let's talk about McConnell and Trump going to war....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OEKVRPAgQ5M) |
| Published | 2022/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of the current situation in the Republican Party, focusing on the Trump McConnell power struggle.
- McConnell's active recruitment of candidates to run against those endorsed by Trump, indicating an all-out war within the GOP.
- Emphasizing McConnell's consistent prioritization of his own interests over party dynamics.
- Mentioning that McConnell's moves are likely supported by the big funders in the GOP.
- Pointing out that McConnell's actions do not necessarily stem from a concern for representative democracy but rather from self-serving motives.
- Warning against viewing McConnell as a hero if he is successful in countering Trump.
- Speculating that the establishment Republicans might aim to bring the party back to a George Bush Jr. style of politics.
- Noting that Trump's erratic nature and lack of a clear plan have complicated lobbying and fundraising within the Capitol Hill.
- Suggesting that the establishment Republicans are likely aiming to return to a more conventional style of politics.
- Implying that the move against Trumpism within the GOP may be driven by a desire to resume business as usual without Trump's disruptions.

### Quotes

- "McConnell does what's best for McConnell."
- "He is not going to go against the big funders."
- "Mitch McConnell has joined the resistance, but my guess is he's doing it for very, very self-serving reasons."
- "Don't turn him into a hero if he is successful."
- "Trump is bad for business."

### Oneliner

Beau outlines the Republican Party's internal struggle, driven by McConnell's moves against Trumpism, with a focus on self-serving interests over party dynamics.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your local Republican representatives to understand their stance on the internal party dynamics (suggested).
- Support candidates who prioritize representative democracy over self-serving interests (implied).
  
### Whats missing in summary

Deeper insights into the potential long-term implications of McConnell's actions and the shifting dynamics within the Republican Party.

### Tags

#RepublicanParty #McConnell #Trumpism #PoliticalAnalysis #GOP


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about what's going on in the party of Lincoln.
We're going to talk about what's going on in the Republican party because there
has been some news that's broke.
Not really surprising, if you've been watching this channel, we have been talking
about the Trump McConnell power struggle for an incredibly long time, but now we're
We're seeing how structured that struggle is.
We have seen McConnell make a whole bunch of moves to undermine Trumpism.
We know that he was backing incumbent candidates that Trump didn't like.
We now know, because of the reporting, that he is actively recruiting candidates to run
against Trump-endorsed candidates.
So this isn't just a power struggle, this is an all-out war in the GOP.
The media, the takes that have come out of the media on this, I think they are very short-sighted,
say the least. They're focusing on the fact that some of the people that McConnell has
reached out to have turned him down. The thing is, we wouldn't know about the ones that said
yes because they wouldn't announce that. That's not something that they would come out and
say, oh, I'm doing this to undermine a person that has a hold on a significant portion of
base. So they're basing it on a couple of key losses is the way they're seeing it. We
don't know that. What we do know is that it's McConnell. McConnell does what's best for
McConnell. He is incredibly consistent in this. If McConnell is making moves like this,
It means the people that really have the money in the GOP, that's what they want.
He is not going to go against the big funders.
That's not something McConnell would do.
So the active recruitment of candidates to run against Trumpist candidates says that
Not just are we talking about the old guard politicians, the establishment Republicans,
moderates, so on and so forth.
The people with the money are actively working against Trump.
The people that have the resources.
Because to kind of entice somebody to run, you have to be able to tell them, we got you
funded, don't worry about it.
Because nobody wants to deal with that.
the money to make the campaign successful, that's a precursor to asking somebody if they
want to run.
So all of this is good news.
Now there is one thing I want to caution everybody about.
McConnell does what's best for McConnell.
I do not believe that he is engaging in this activity because he cares about representative
democracy or cares about anything other than himself, to be honest, I believe he's doing
this because it's what's best for him.
And the reason I want to say this is because he's moving to counter Trump and the odds
are that he's going to be more successful than the Democratic Party.
Mitch McConnell has joined the resistance, but my guess is he's doing it for very, very
self-serving reasons.
Don't turn him into a hero if he is successful.
Do not forget who he is and who he has been for a very, very long time.
He is incredibly politically aware.
If he is making this move, it's because the people with the resources want that move made.
This does not bode well for Trump-backed candidates.
We're going to find out whether conventional fundraising and practices that have had their
hold on DC for a very, very long time, will win out over nationalist rhetoric is really
what it boils down to.
That's what's really going to be at play.
The Trumpist side, they're going to focus on saying all of the things that people want
to hear.
They're going to say that they're going to do X, Y, and Z even though they know they
They can't do X, Y, and Z, but that's what people want to hear.
My guess is that the people with the money, the people with the resources, the establishment
Republicans, they're going to try to make a move to bring the Republican Party back
to George Bush Jr.
That's where they're probably aiming to get to.
and return to that style of politics.
See on top of the obvious problems that Trump created for the Republican Party in terms
of party dynamics and all of the internal political stuff, his erratic nature, his lack
of a plan? Well, it complicates a whole lot of other things that go on at Capitol Hill
concerning lobbying and fundraising and a whole bunch of other stuff. Trump's bad for
business. Trump is bad for business. So the Republican Party, those people who have a
of influence up on Capitol Hill. Four years was enough for them. So while it
may seem that Mitch McConnell and company are doing the right thing and
they've seen the light, just remember that it's more than likely just them
wanting to get back to being able to do whatever they wanted without the
interruptions from within their own party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}