---
title: Let's talk about the Superbowl, Star Trek and timelessness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q05SSXgSoJU) |
| Published | 2022/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the Super Bowl halftime show and TV shows in terms of timelessness and societal changes.
- Mentioning the importance of shows that stand the test of time versus those that become problematic.
- Exploring how society evolves over time, leading to shifts in perspectives on past content.
- Noting how certain timeless shows provide commentary on current issues, remaining relevant even decades later.
- Using Star Trek as an example of a show ahead of its time in addressing social issues.
- Pointing out subtle and overt social commentary within Star Trek episodes.
- Describing an episode in Star Trek that mirrors the Dred Scott case, tackling the concept of android rights.
- Referencing a character in Star Trek Deep Space Nine, Jadzia Dax, and discussing themes of identity acceptance and deadnaming.
- Emphasizing the role of timeless shows in offering answers to societal changes and individual growth.
- Encouraging reflection on how both society and individuals evolve over time.

### Quotes

- "Society changes over the course of your life."
- "Timeless shows typically give us the answer."
- "In the course of your life, society has changed that much."
- "Those timeless shows, those shows that you can watch them thirty years later."
- "It's probably worth noting that if society did it, the individuals who make up that society, they did too."

### Oneliner

Analyzing timelessness in TV shows through societal evolution and commentary, using Star Trek as a prime example of ahead-of-its-time social reflection.

### Audience

TV show enthusiasts

### On-the-ground actions from transcript

- Watch timeless TV shows that offer social commentary (implied).
- Analyze and appreciate the social issues addressed in classic TV series (implied).

### Whats missing in summary

Deep dive into societal reflection through timeless TV shows and the evolution of societal norms and values.

### Tags

#TVshows #Timelessness #SocialCommentary #StarTrek #SocietalChange


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Super Bowl
and TV shows,
timelessness,
Star Trek.
You know, the halftime show,
the big topic
that came out of that
was
people on social media talking about what generation it was aimed at.
And when you really think about it, that's an interesting concept.
A halftime show, music,
being geared towards a specific
point in time,
a specific group of people.
And when you think about it, what you find out
is that
that's kind of important
because society changes so much even in the course of one person's life.
A halftime show is
trendy.
It's not going to stand the test of time.
A lot of TV shows are like that.
If you go back and watch some of the shows that you grew up on,
there's going to be a few moments where you just cringe.
There's going to be
things that I guess we would call problematic.
You're going to be like, wow,
I didn't realize how racist the show was.
Stuff like that. It'll happen.
And then there's those true gems
that are timeless. They stand the test of time.
And the reason there are
those shows that have those takes that just make you
cringe, for lack of a better word,
it's because
in the course of your life,
society has changed that much. Something you didn't notice back then.
Now not just do you notice it,
it's unacceptable
because humanity does move forward constantly.
Those timeless shows,
those shows that
you can watch them thirty years later
and you don't really see much of those
true gems.
One thing that you'll find
is that most of them,
well, they
they provided commentary
on
issues
of today.
Not issues of back then.
Issues of today.
They were that far ahead.
They were looking towards the future and they provided
actual commentary, even if it was very subtle, even if you may not have even got it at the time.
Because you...
it may not have been a topic that was in the main
discourse of the country.
A good example of this
is Star Trek.
You know,
there are a lot of people who are conservative who love this show.
I don't know why.
Because it seems like it's
like a
army movie
in space, right?
You have the rank structure,
the hierarchical structure.
They're out there
fighting the bad guys and colonizing.
It seems like a conservative show.
Until you realize that in most cases it's actively mocking
a lot of closely held conservative beliefs.
You're talking about a show that is set
in a period when
the earth doesn't have countries.
You could say it's stateless.
Where everybody
has what they need because it's post-scarcity, right?
So it's classless in a way.
And they don't have money.
There might be an ideology that could
be defined by those three characteristics.
And it...
throughout all of the different versions of that show,
it addressed social issues.
Sometimes subtly, sometimes pretty...
pretty overtly.
A good example
is there's a
an episode with an android.
And
they're trying to decide whether the android has rights
or it's just property
of... it's the Dred Scott case.
It's what it is.
And they worked it in a show.
Unless you're paying attention you may not even catch it.
In one
series
it's...
Deep Space Nine is the name of it.
There's a character
and her name is Jadzia Dax.
And it's cool because it sets up this dynamic from the beginning
because when she shows up at the space station,
well everybody
knows her
as Curzon Dax.
This guy.
This brash warrior.
They know him.
And there's a plot device that allows this to happen.
And so you may not
see the
obvious statements that were being made.
And also this is thirty years ago.
A lot of these topics,
well they weren't exactly
on network TV.
Jadzia, she has all these memories
of when she knew these people
as
this man Curzon.
And she has to deal with
you know a lot of people coming up to her, oh you're Curzon.
Now it's Jadzia now.
Dead naming.
Topic we talk about today.
And it goes through about
accepting that person
as who they are now.
And you watch characters
shift
in their view
of this person.
And here we are thirty years later
trying to decide if it's okay
for somebody to use the name they want.
Timeless shows,
shows that aren't
aimed at a generation,
those shows,
they typically give us the answer.
Society changes
over the course of your life.
I mean,
again, there are those shows that you watch
and you're just like, man, I can't believe that.
And you have that reaction
because
society
as a whole
has moved forward.
It has changed its substance substantially.
It's probably worth noting that if society did it,
the individuals
who make up that society,
they did too.
Or at least they should have.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}