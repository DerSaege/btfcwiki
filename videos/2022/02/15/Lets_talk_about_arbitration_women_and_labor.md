---
title: Let's talk about arbitration, women, and labor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=X-jxqy_aJeI) |
| Published | 2022/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of labor legislation related to arbitration heading to President Biden's desk.
- Mentions the common inclusion of arbitration clauses in new hire packets.
- Describes arbitration as a private means of settling disputes outside the judicial system.
- Notes industries where arbitration makes sense, like those involving trade secrets or intellectual property.
- Points out that arbitration clauses are not suitable for disputes related to sexual harassment or assault.
- Emphasizes how the new legislation renders these clauses unenforceable, particularly benefiting victims of harassment.
- Acknowledges the long process behind this legislation, dating back to around 2017 or 2018.
- Comments on the importance of bringing accountability to those who previously avoided it through arbitration.
- Stresses that the new legislation will provide increased protection, especially for women working under powerful individuals.
- Concludes by wishing everyone a good day.

### Quotes

- "This legislation says those clauses, well, they can't be used anymore."
- "This is going to offer a wider level of protection, particularly for women who are working for rich and powerful men who think they can get away with anything."

### Oneliner

Beau explains the impact of impending labor legislation banning arbitration in cases of sexual harassment, providing vital protection for victims.

### Audience

Employees, advocates, activists

### On-the-ground actions from transcript

- Contact organizations supporting victims of sexual harassment to understand how this legislation can benefit them (suggested)
- Join advocacy groups pushing for similar legislative changes in other areas (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the labor legislation related to arbitration, offering insights into its implications for workplace protections and accountability.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about arbitration
and something that is actually pretty big labor legislation
that is headed to President Biden's desk to be signed.
I don't know how much coverage it's gonna get
because of everything else going on,
but it's kind of big news to be honest.
And more than likely applies to you in some way.
I would be willing to bet when you got hired,
your new hire packet contained a whole bunch of forms
that you signed in half red.
One of them probably had a clause about arbitration in it.
Arbitration simply is settling of things
that would be a lawsuit.
And it's doing it behind closed doors.
It's doing it in private through a means
outside of the judicial system.
There are times when this makes sense.
There are certain industries where this makes sense a lot.
Anything dealing with secrets, trade secrets,
intellectual property, stuff like that.
It makes sense to do it that way.
One category of dispute that it doesn't make sense in
is things dealing with sexual harassment or assault.
This legislation says those clauses,
well, they can't be used anymore.
The reason it matters is the reason it makes sense
in other industries.
You would want something like that
if you were dealing with secrets
because everything happens behind closed doors.
Nobody talks about it and everything is protected.
If you're talking about harassment,
the same thing happens.
It happens behind closed doors,
which often protects the person
that might should be held accountable for their actions.
It leads to multiple instances of one person
harassing different people.
That goes away.
That's gone under this legislation.
These clauses become unenforceable,
which, I mean, this is good news.
And this is a legislation that has been years in the making.
I wanna say, and fact check me on this,
I wanna say it was 2018, 2017, something like that,
when this first started rolling.
And I mean, it's not like it was gonna be signed
under Trump anyway, but here we are now.
It's passed the House, it's passed the Senate.
Biden has signaled that he's gonna sign it.
So this is labor legislation.
This is the kind of legislation that can hold or help hold
people accountable that often were able to do it.
Were able to skate by using this method.
If you keep it behind closed doors, nobody knows.
So the payment gets made, the person leaves,
somebody new takes their position
and ends up suffering the same fate.
And that cycle continues.
This is going to offer a wider level of protection,
particularly for women who are working for rich
and powerful men who think they can get away with anything.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}