---
title: Let's talk about the futures of Ukraine and Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hiB15crTQVM) |
| Published | 2022/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides insights into potential scenarios in Ukraine if Russia decides to move in.
- Russia's initial approach might resemble Desert Storm, focusing on a strong air game and conventional artillery.
- Ukrainian military may not attempt to go toe-to-toe with Russia, opting for guerrilla-style tactics instead.
- Beau mentions the importance of irregular fighters in disrupting major operational plans.
- The possibility of fog of war events, like accidental strikes on civilians, could drastically alter the conflict's outcome.
- Western media and intelligence have a poor track record of predicting the effectiveness of irregular forces.
- Beau speculates that Putin may want to take Ukraine without direct military action.
- The unpredictability of the situation, especially in the first 24 hours, makes it challenging to make accurate predictions.
- The longer the conflict goes on, the more prepared Ukraine can become, potentially making it costly for Russia.
- Beau concludes with the idea that Putin might be seeking a way to take Ukraine without resorting to invasion.

### Quotes

- "Everybody has a plan until they get punched in the face."
- "Professional soldiers are predictable, but the world is full of amateurs."
- "It's too soon to make that kind of prediction."
- "Make no mistake about it. They can't be informed because that first 24 hours, that will shape everything."
- "Putin wants to take Ukraine without a shot."

### Oneliner

Beau provides insights into potential scenarios in Ukraine if Russia decides to move in, including the unpredictability of the conflict and the importance of irregular fighters.

### Audience

Global citizens

### On-the-ground actions from transcript

- Join organizations supporting peace and diplomacy in Ukraine (implied)
- Stay informed about the situation in Ukraine and advocate for peaceful solutions (implied)

### Whats missing in summary

Analyzing the full transcript provides a deeper understanding of the potential outcomes and dynamics in the Ukraine-Russia conflict.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the futures,
plural, of Ukraine.
Have a whole bunch of people asking for predictions,
basically, asking for what's going to go down
in various eventualities.
The most common is people asking what's
going to happen if Russia does decide to go in.
If Russia decides to move in, what's going to happen?
I am personally not arrogant enough
to believe that I can call the outcome of a war
before the first shots are fired.
I understand that a lot of people
are making assessments and estimates
on what's going to occur, how long it's
going to take, the final outcome, and all of that stuff.
I'm not going to do that because I like to be right when
I do stuff like that.
So it's one of those things.
It's like any street fight.
Everybody has a plan until they get punched in the face.
No plan survives first contact.
So I can't tell you what's going to happen,
but I can run you through the most likely scenarios.
Conventional wisdom as to what's going to occur,
it's probably the most likely scenario,
but I don't think it's the foregone conclusion
that Western media is making it out to be.
So we'll go through it.
If Russia decides to move in, what's
it going to look like initially?
Desert storm.
It's going to look like desert storm.
They will most likely try to duplicate that.
That kind of effect.
So it'll open with a strong air game,
but they're near peer nations.
They're not peer nations.
So it's not going to look exactly the way the US does it.
There's going to be a lot of conventional artillery
involved as well.
And along with that, there will be combined arms advances.
I'm going to guess three, three different routes of advance.
Now, this is assuming that they're actually
trying to take the whole country, which
seems to be the assumption that people are operating under.
Keep in mind, that may not be the goal.
They may just want to take certain segments.
But if they are going to attempt to take the whole country,
that would be the most likely opening gamut, right?
And they would drive towards Kiev.
Conventional wisdom, what is being depicted in the media
is that the Ukrainian military falls apart
and Russia is able to accomplish this in days or weeks.
And yeah, I mean, that is a possibility.
That's something that can occur.
But I don't think it is a guarantee.
I wouldn't say standing here today,
I wouldn't say that's what's going to happen.
Because one, the Ukrainian military isn't incompetent.
It's not like they don't know what they're doing.
And they've had a lot of warning.
So they might be planning some surprises,
because there are only so many ways
Russia can accomplish what it wants to accomplish.
So they can find ways to disrupt that.
Now, that's assuming the Ukrainian military decides
to go toe to toe with Russia.
They will lose.
It's a matter of can they make it too costly for Russia
to continue.
They can't hold out forever.
They're outmatched.
And that's what everybody's focusing on is that scenario.
The thing is, A, I think they're underestimating
the Ukrainian military.
B, the Ukrainian military has made it pretty clear
that that's not what they're going to do.
They're not going to attempt to go toe to toe.
That's not their doctrine.
That's not what they've been training for.
That's not what they've been planning for.
They have kind of said that they're
going to put up a stiff fight initially,
and then they're going to disappear,
cut off into the hills, and adopt more guerrilla style
tactics.
Now, from this point, the question
becomes, can they start that campaign, that resistance
campaign, can they get it up and running
while the advance is still going on,
before Russia actually occupies?
If that's the case, I think they got a pretty good shot,
to be honest.
Because in that type of scenario,
you don't need a lot of people who know what they're doing.
You need a few people who know what they're doing
and who have had warning, like they've had,
who are going to sit there and say, there's only so many ways
they can get from point A to point B.
They have to come through here, and they're not going to.
That bridge, we're going to make that super unsafe for them
to go over.
They can find ways to slow the advance.
And if they can do it well enough, it might persuade
Russia to back off.
Aside from that, along with the people
who know what they're doing, there's
going to be a whole bunch of people who don't.
And that's ideal.
I know that doesn't make any sense.
But when you're talking about irregular fighting like that,
that's the best case scenario.
You have a few groups that are really good,
and a whole bunch that have really no clue what they're
doing, but they have a bunch of heart.
The reason for that is that old joke.
Professional soldiers are predictable,
but the world is full of amateurs.
And the subtext of that is, and it's
going to be one of those amateurs that comes out
of nowhere and cleans your clock.
Because if they don't really know what they're doing
militarily, like at a strategic or operational level,
and they just know the tactics, they
could do something that just doesn't make any sense.
But it causes enough damage back down the line
that it disrupts major operational plans
for the opposition.
Those people that just have a lot of heart and not
a lot of skill are incredibly important
in this phase of things.
If they can get something like that going while the advance is
still occurring, I actually think they got a decent shot.
So you have that.
Now, another option is that they don't
get that resistance going until after Russia
gets to where it's going and the occupation starts.
That's the worst case scenario for everybody involved,
because then it becomes protracted
and it becomes really bad.
That's the type of thing that turns this.
It takes it from a conflict that could be a month
and turns it into something that could be decades.
So that's your worst case scenario.
Now, you also have fog of war stuff,
stuff that people don't really count on when
they're making these estimates.
And there's a lot of stuff that is being overlooked
when these estimates are being made.
Please keep in mind that the Western media
and Western intelligence, they have a really bad track
record at predicting the effectiveness
of irregular forces.
See pretty much every conflict that we've been involved
in for the last 50 years.
They're not really good at determining how effective
they're going to be.
And to be honest, there's no way to know until it starts.
So there is that.
Now, the fog of war stuff that could disrupt all estimates
are the wild cards.
You know, during this desert storm phase in the beginning,
Russia's probably going to want to try out
some of their new stuff.
What if it's not super accurate?
Hits a children's hospital, something like that.
What does the West do then, especially
if there's a lot of footage?
Something like that can drastically
alter the outcome of something like this.
It's too soon to make that kind of prediction.
Another one that I don't see people really talking about
is the possibility of flights.
NATO countries have flights running in to Ukraine.
If Russia decides to do its desert storm style approach
during that air game, what if they take out a NATO plane?
A Canadian flight, as an example, bringing in supplies.
And Russia takes it out.
When we went over the treaty, the NATO treaty,
we talked about how vessels and planes
are the same thing as an attack on a territory.
If that occurred and Canada wanted to,
Canada could say an attack on one is an attack on all,
and all of NATO's in that fight.
With the number of variables aside from the most likely
opening gambit, nobody knows.
Anything you're seeing that's on networks
and predicting the outcome, I mean, people really are.
They're calling this war and saying what's going to happen
before it even starts.
I don't think that's a good idea.
It reminds me of don't worry, the troops
will be home by Christmas.
Those kind of predictions, they're never accurate.
There isn't enough information.
Now, another option and something
that somebody I was talking to brought up, and to me,
it would just be ridiculous to do something like this.
But again, not out of the realm of possibility
is that Russia doesn't try to do any of this.
They just try to take Kiev from the air.
I see that as incredibly unlikely.
I see that as unlikely as the Ukrainian military being able
to hold Russia off conventionally.
But it was something that was brought up,
and it's not out of the realm of possibility.
You have a lot of wild cards that
prohibit making any real predictions.
And those people who are making them right now,
they are guessing.
Make no mistake about it.
They can't be informed because that first 24 hours,
that will shape everything.
Now, keep in mind, my opinion of how this is actually
going to play out is that Putin wants
to take Ukraine without a shot.
I am still of the opinion that he's looking for a way out
that isn't embarrassing.
I don't know that he's going to escalate.
I don't know that he's going to invade.
This has been going on a long time.
And again, the longer it goes on,
the more warning Ukraine has, the more prepared they can be.
Putin isn't a, he's not new to this.
He understands that.
It would be a really bad move to move in after this long.
It isn't like Ukraine is without friends.
And it's not like the Ukrainian military stands
zero chance of being able to make it incredibly costly.
They may not win, but they can make it very, very costly.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}