---
title: Let's talk about the Superbowl and trucks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Rt6tSppsO_A) |
| Published | 2022/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses his disinterest in the Super Bowl and trucks, stating he doesn't care about the sports game or who wins.
- He warns against using trucks for demonstrations at the Super Bowl, deeming it a bad idea with potential serious consequences.
- Beau explains the security risks involved with bringing trucks close to the Super Bowl, mentioning the National Special Security Event designation.
- He cautions that individuals pushing for these actions won't be present during any potential arrests or legal consequences.
- Beau illustrates the dangers of attempting to bring unfamiliar trucks close to large crowds by referencing past tragic events.
- He stresses that such actions are unlawful and could lead to severe legal repercussions for participants.
- Beau criticizes the leaders encouraging these actions, accusing them of prioritizing their agendas and monetary gains over the safety and well-being of their followers.
- He concludes by advising against pursuing this course of action, as it will ultimately end poorly for those involved.
- Beau ends with a reminder that this dangerous idea should not be pursued, as it will lead to negative outcomes for participants.

### Quotes

- "Do not do this. This is a horrible idea."
- "They present an unlawful threat of violence."
- "Your leaders who are putting you up to this, they know. They know it's a horrible idea."

### Oneliner

Beau warns against using trucks for demonstrations at the Super Bowl, citing serious security risks and legal repercussions for participants.

### Audience

Community members

### On-the-ground actions from transcript

- Refrain from participating in using trucks for demonstrations at events (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of why using trucks for demonstrations at events like the Super Bowl is a dangerous and ill-advised idea, focusing on the security risks and legal consequences involved. Watching the full video can offer a deeper understanding of Beau's concerns and warnings. 

### Tags

#SuperBowl #TruckDemonstrations #SecurityRisks #LegalConsequences #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the Super Bowl
and trucks.
Before we get started, I want to make some things really clear.
I don't care about your sports ball game.
I don't care who wins.
I don't know who's playing.
I don't care if it gets canceled.
OK?
I do not care.
And as far as these little exercises involving the trucks,
I've been really clear about this.
I think they're silly.
I think you're wrong.
I also think you have the right to have your voice heard.
Do not do it at the Super Bowl.
Period.
Full stop.
This is a really bad idea, and the people putting you up to it
aren't going to be there.
Four letters that you need to understand.
NSSE, National Special Security Event.
That is what the Super Bowl is.
That's not going to be local cops out there running the show.
You will not get your trucks near those people.
Put yourselves in the shoes of the security.
Trucks that you don't know what they're hauling.
How close are you going to allow them to get
to the tens of thousands of lives
you're responsible for protecting?
Do not do this.
This is a horrible idea.
And the thing is, the people putting you up to it,
they're not going to be there that day,
unless maybe they have tickets.
They'll be inside enjoying themselves
while you're getting arrested.
They're not going to be there when you get arrested.
They're not going to be there when you are reading
that letter to the judge talking about how
your life has been ruined, how you didn't understand
how you were tricked and misled, and how you were shamed.
And then the judge is going to sentence you,
because it's not going to matter.
If you don't understand the situation,
rather than think of your semi truck
that you're familiar with, and you're comfortable with,
and you view it as it's your business, right?
You don't see that as menacing.
Picture a rider truck, and think Oklahoma.
You're not getting the trucks near the people.
And if you try, you're going to have a bad day.
If you put yourselves in the security's shoes for a moment,
you're going to realize those trucks only present a threat.
They present an unlawful threat of violence.
And you are attempting to achieve a political goal.
Go ahead and type that into Google and see what pops up.
This is a horrible idea.
And your leaders who are putting you up to this, they know.
They know it's a horrible idea, and they
know what's going to happen to you.
They don't care.
They care about making a statement
and making some money.
And they're going to do it while you are sitting
in an 8 by 10 foot room.
As I understand it, this hasn't been finalized as a thing.
It shouldn't be.
This isn't something that should be pursued.
It will only go bad for the people who
participate and show up.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}