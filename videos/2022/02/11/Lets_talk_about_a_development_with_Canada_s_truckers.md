---
title: Let's talk about a development with Canada's truckers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Pw2CDzAU7EA) |
| Published | 2022/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update and forecast on the situation in Canada involving a group of truckers who have been swearing-in ceremonies to induct people into the Canadian Common Corps of Peace Officers.
- The truckers are claiming the right to arrest and detain people, linked to a movement called Freedom of the Land.
- The movement is designated as an extremist threat by CSIS, the Canadian CIA.
- The movement has been declining since 2010 and historically recruits using issue-based causes.
- Claiming the power to arrest and detain and the authority to use force is viewed negatively by governments.
- Governments losing the monopoly on violence is seen as a sign of a failed state.
- While the movement hasn't been violent by American standards, the Canadian government is likely to respond strongly.
- A marked escalation in response from Canadian officials is expected.
- The situation alters the normal playbook for handling such events and may pose a serious threat if allowed to continue.
- The public swearing-ins by the truckers are seen as a serious miscalculation, likely to elicit a response rather than negotiation from the government.

### Quotes

- "Claiming the power to arrest and detain and the authority to use force is viewed negatively by governments."
- "Governments losing the monopoly on violence is seen as a sign of a failed state."
- "The public swearing-ins by the truckers are seen as a serious miscalculation, likely to elicit a response rather than negotiation from the government."

### Oneliner

Truckers claiming the right to arrest in Canada's trucker situation bring a serious escalation likely to prompt a strong government response.

### Audience

Government officials, Activists, Community Leaders

### On-the-ground actions from transcript

- Monitor the situation closely for updates and developments (implied)
- Stay informed about movements and developments in your community (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the situation involving truckers in Canada, offering insights into the historical context of the movement and potential government responses.

### Tags

#Canada #Truckers #PeaceOfficers #ExtremistThreat #GovernmentResponse


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to provide a little bit of an update
and another forecast, another estimate
is what's going to happen.
For what's going on up in Canada,
there have been some developments that
are probably going to change the government's posture.
And they've occurred over the last 48 hours.
And they, Americans, I'll keep you up to date as we go through
and give you parallels, because it's kind of unique stuff,
to be honest.
OK, the main development is that this group of people,
the truckers, as they've come to be known,
have had swearing-in ceremonies.
And this is to induct people into the Canadian Common
Corps of Peace Officers.
What is that?
It's not a thing.
They are claiming the right to arrest and detain people.
This is important.
This is important as far as the forecast later.
This concept of kind of appointing random people
as peace officers, it goes back about 10, 11 years.
I've been able to find references
to this exact terminology going back that far.
It's linked to a group called, or a movement called,
Freedom of the Land or Freedom on the Land,
something like that.
For Americans, they're sovereigns.
They're sovereigns.
They're very, very similar.
Same pseudo-legal stuff.
The Canadian cousins are less violent.
Good.
That's good.
But it honestly, it appears as though at some point,
a Canadian was exposed to the American movement.
And it was like, hey, copy my homework,
but change it up a little bit.
Very similar stuff.
So it appears that this movement is now
heavily embedded in the trucker situation that's going on
there.
That's going to alter the Canadian government's posture
a lot.
CSIS, which is the Canadian CIA, they
have this movement designated as an extremist threat.
So they're definitely going to kind of up their posture
a little bit.
Because by most estimates I could find,
this crew has been in decline since 2010.
And traditionally, they have gone through and recruited
using issue-based causes like this.
It's unlikely the Canadian government
is going to want a resurgence of this movement.
So they're probably going to act.
The movement itself, you can trace it back all the way
to the 1970s in a way.
But the more modern versions came out
in the late 90s and early 2000s.
So aside from the history of the movement that
has kind of inserted itself here, what else is different?
They claimed the power to arrest and detain.
They have claimed the authority to use force, presumably
under the color of law somehow, using this pseudo-legal stuff,
right?
Generally speaking, governments view that as totally uncool.
A sign of a failed state is losing
the monopoly on violence.
If a government loses that monopoly
and another entity that isn't associated with the government
is seen to have the right to detain and arrest people,
it undermines the government at large.
This almost always elicits a pretty strong response
from the government, from any government anywhere,
that this is a big deal.
It's a big escalation.
And even though, as far as I know at this moment,
they haven't exercised this arrest power
that they have given themselves, just claiming it
in the very public fashion that they did
is probably enough to get the Canadian government's
attention.
I would be incredibly surprised if you
didn't see a marked escalation in response
from the Canadian officials.
This is unlikely to be something they're going to let slide.
Because if they allow something like that to stand and it
grows, it actually becomes a serious threat.
Now, historically, this movement hasn't really
been violent by American standards.
I'm sure by Canadian standards it kind of has been.
But by American standards, in the way
we view these types of groups, they're pretty tame.
But there is a large cultural difference
in what is seen as normal.
I would expect that you start to see more federal response
from the Canadian officials.
And I would expect to see that pretty quickly.
This kind of alters the playbook under normal circumstances.
When you have something like this occurring,
you do exactly what they've been doing.
You kind of, it's like allowing a child to be
scream and kick on the floor when they're throwing a temper
tantrum.
As long as they're not actually harming anyone,
you just kind of let it go.
And you just focus on stopping any violence and vandalism.
That's the playbook.
That's the textbook.
That's what you're supposed to do.
And that's kind of what they've been doing.
This alters that.
Because if they allow it to stand too long,
it's going to be a serious threat.
If they allow it to stand too long,
then they're going to be faced with a real issue.
So this stunt, and that's the only way I can describe it,
these public swearing-ins were probably
a serious miscalculation on the part of the people engaged
in this little adventure.
They're trying to get the government's attention.
And I definitely think they're going to get it with this,
but not in the way they wanted.
This isn't the type of thing that
would bring a government to the table to negotiate or talk.
This is the type of thing that is going to elicit a response.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}