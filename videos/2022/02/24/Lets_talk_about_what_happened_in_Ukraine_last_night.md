---
title: Let's talk about what happened in Ukraine last night....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jHkIrAKYViA) |
| Published | 2022/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an overview of the recent events in Ukraine, mentioning a shift towards a potential unconventional resistance-style campaign.
- Describes the Russian military's swift movement and potential criticisms of the Ukrainian military's lack of resistance.
- Raises the possibility of the Ukrainian military's intentional appearance of collapse to transition into an unconventional campaign.
- Addresses the unlikely scenario of Russia advancing into NATO countries due to potential repercussions.
- Mentions the civilian casualties in the conflict, with varying reports from Russian and Ukrainian sources.
- Predicts that NATO's response may involve aid and sanctions rather than direct military involvement.
- Clarifies the distinction between NATO's Article 4 and Article 5 in terms of response levels to the conflict.
- Suggests that the situation in Ukraine may continue for a protracted period unless it was indeed a military collapse.
- Notes the potential involvement of Russia in Moldova due to its small population size.
- Concludes with an observation on the ongoing nature of the conflict and the uncertainty surrounding its resolution.

### Quotes

- "That's how it's going to be said, I'm sure. Remember, that was the plan."
- "Generally speaking, there are more civilians lost than military. That's just how it goes. That's war. Wars are bad."
- "So, there's your overview. This is what happened while hopefully you got some sleep."

### Oneliner

Beau provides a detailed overview of the recent events in Ukraine, discussing potential scenarios, criticisms, and implications, while addressing questions on civilian losses and NATO's response.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support organizations providing aid to civilians affected by the conflict (suggested)
- Stay informed and advocate for peaceful resolutions to the crisis (implied)

### Whats missing in summary

In-depth analysis and context on the geopolitical implications of the conflict in Ukraine.

### Tags

#Ukraine #Russia #NATO #MilitaryConflict #CivilianCasualties #Geopolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about Ukraine,
because it got loud last night.
So we're going to provide a general overview
of what happened, and kind of catch everybody up
on what happened overnight.
And then we're going to answer a couple of basic questions
that came in.
OK, so the obvious question to start off with
is what did it look like?
the most likely option for something full scale.
It was very, very much a desert storm-style template.
Started off with a strong air and artillery game,
followed up by combined arms, I think, along four routes.
That's four main lines of advance,
but that's murky at the moment.
That's not really confirmed.
Russian military moved very, very quickly. They moved very quickly. There weren't any
real surprises on that side other than some fragility that was displayed in
their economy. From that we can anticipate that NATO's response is going
to be heavily reliant on sanctions, which that's not a surprise, but it may be a
little bit different than what they've messaged so far, and we'll have a whole
follow-up video on that soon. Now from the Ukrainian side, their military is
probably going to be criticized because they didn't put up much of a fight.
That's how it's going to be said, I'm sure. Remember, that was the plan. They
weren't supposed to put up a huge fight. The Ukrainian military had zero chance
of going toe-to-toe with the Russian military and succeeding. The plan was to
put up a small fight and then kind of melt away so they could launch an
unconventional campaign. The best-case scenario would have been for them to get
that up and running while the Russian advance was still going on. Doesn't look
like that happened and it doesn't look like it's going to. So from that, odds are
we're moving to the worst case, which is a protracted, unconventional, resistance-style
campaign.
That's a long-term thing.
Now there is the possibility that what we saw as far as the Ukrainian military kind
of melting away was an actual military collapse.
It's impossible to tell because the plan was for it to look like a military collapse.
So we're not going to know that right away.
And it could be one of those things where it could go either way.
It's up to Ukrainian leadership to kind of galvanize those troops who melted off and
did what they were supposed to.
Now it's up to the leadership to bring them back online.
have to see how that plays out. So that's where we're at. That's what's
occurred at time of filming. This video probably won't be released for another
couple hours though, so there will be more developments in the meantime. One of
the questions that has come up repeatedly is whether or not you Russia
Russia is going to advance beyond Ukraine into other countries, particularly NATO countries.
That's incredibly unlikely for a bunch of reasons.
The first, when you're talking about NATO countries, that would be a mistake.
Russia doesn't want to tangle with all of NATO.
If they move into a NATO nation, an attack on one is an attack on all.
That is incredibly unlikely.
Now as far as non-NATO nations, also unlikely with one exception, and we'll get to that,
but NATO estimates suggest that Russia has at their disposal roughly 190,000 troops.
For comparison, in 2003 when the US-led coalition went into Iraq, it had about 180,000.
The populations of Ukraine and Iraq are pretty similar.
They don't have the manpower for much else.
We've talked on this channel about what it takes to take and hold land as far as personnel.
They don't have the troops to do much else.
The one exception to this is Moldova, because it has a very small population, and there
is a pretext there for them to get involved. However, at the same time, Moldova is already
going to have its own issues dealing with people fleeing. So I'm not sure that Russia
would want to get involved with that right away. But that may be something that they're
looking at as dessert rather than as part of the main course. So, another question that
came in dealt with civilian losses. The Russians are saying there aren't any. The Ukrainians
are saying there are a few hundred. There are a few hundred and that number is going
to go up. They aren't all accounted for. The idea that any nation could stage
something like this without civilian losses is laughable. That's not a thing.
Generally speaking, there are more civilians lost than military.
That's just how it goes. That's war. Wars are bad.
So, this is where we're at as of now.
It does not look like NATO is going to get involved militarily in this directly.
It does look like there's going to be a response, but it will be through aid and it will be
through sanctions.
There is talk about countries invoking Article 4.
clear that up in the NATO treaty, it's Article 5 that you really need to pay
attention to. Article 5 is war. Article 4 is we all need to sit down and talk and
come up with a defensive plan. So don't let the discussion of Article 4 elevate
your blood pressure too much. That was pretty anticipated. So there's
your overview. This is what happened while hopefully you got some sleep. Odds
are that this is going to be pretty protracted at this point. Unless that
actually was a Ukrainian military collapse, this is probably gonna go on
for a while. If it was a collapse, it might already be pretty much over. So, we're
gonna have to wait and see on that. Anyway, it's just a thought. Y'all have a
a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}