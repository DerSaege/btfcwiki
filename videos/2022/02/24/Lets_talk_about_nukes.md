---
title: Let's talk about nukes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WUdFGZuiQd8) |
| Published | 2022/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing concerns about the use of nuclear weapons in the current situation in Ukraine and Europe.
- Explaining why the use of nuclear weapons is unlikely and not a realistic option in the conflict between Ukraine, Russia, and NATO.
- Mentioning the conditions under which the use of nuclear weapons might become a possibility.
- Describing the escalation process that could lead to the consideration of nuclear weapons in a conflict.
- Emphasizing that nuclear weapons are not the first option due to other available technologies and the risk of massive escalation.
- Pointing out the historical reluctance to use nuclear weapons during the Cold War and the devastating consequences of their use.
- Advocating against succumbing to constant fear of nuclear annihilation and suggesting a different approach to strategic weapons.

### Quotes

- "Nobody wins."
- "It's not something anybody wants to do."
- "Once they're used, there will be a response."
- "It isn't incredibly likely."
- "It's just a thought."

### Oneliner

Beau explains the unlikelihood of nuclear weapon use in the conflict between Ukraine, Russia, and NATO, stressing the devastating consequences and historical reluctance towards such actions.

### Audience

Global citizens, policymakers.

### On-the-ground actions from transcript

- Monitor the situation in Ukraine and Eastern Europe closely to understand the dynamics and potential risks (suggested).
- Advocate for diplomatic solutions and de-escalation efforts in international conflicts (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the factors influencing the potential use of nuclear weapons in the Ukraine-Russia-NATO conflict, offering historical context and insights into strategic decision-making.

### Tags

#NuclearWeapons #UkraineConflict #Russia #NATO #Diplomacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the likelihood
of something being used.
There's a lot of concern from people
about a specific item being deployed.
And we're going to run through the likelihood of it.
And we will start with the situation as it is.
And then we'll gradually progress
through various stages before we get to the point
where it would even realistically be considered.
This is something that people seem concerned about.
But there's really not a reason to be too worried about this.
It's not on the table.
And of course, right now, we're talking about nukes.
Lots of people have asked because of what's
going on in Europe, in Ukraine.
At time of filming, hostilities haven't broken out yet,
not direct hostilities.
Russia has ordered troops into eastern Ukraine.
But as far as we know, there's no heavy fighting yet.
However, Ukraine has indicated that the borders
aren't changing, which means they're gearing up for a fight.
So let's just assume that happens.
Ukraine and Russia, they start duking it out.
NATO is assisting Ukraine.
Is the use of nuclear weapons likely?
No.
No, it's not even on the table.
This is more comparable in our near-peer context.
This is more comparable to Vietnam or Afghanistan
with the Soviets there.
There wasn't the constant worry that that
was going to go nuclear because it didn't serve
anybody's purposes to use it.
Same thing here.
Same thing here.
Russia is not going to launch on Ukraine.
They're not going to launch on territory they want.
That doesn't really benefit them in any way.
And they know, because of mutually assured destruction,
that launching is just bad all around.
Probably not on the table.
It's probably not even considered a contingency
for a contingency for a contingency.
It's probably not even mentioned.
The use of strategic forces like nuclear forces like that
is just probably not even in any of the planning.
OK, so let's escalate.
Two mistakes get made.
NATO makes a mistake.
Russia makes a mistake.
Now NATO and Russia are duking it out directly,
not through a proxy, but directly fighting.
Tensions are heightened.
Likelihood, still pretty low, though,
as long as it stays in Ukraine.
As long as it stays in Ukraine or in Eastern Europe.
The theater could expand.
But as long as it stays in Eastern Europe,
probably still not really being thought about.
There would be high level conversations about it.
But it's not going to be in anybody's plan,
because it's like the movie War Games.
Nobody wins.
So still not a real possibility, even in a direct fight
like that.
When does it become a possibility?
Two more mistakes have to be made at the same time.
About 10 months ago, I put out a video,
and I will put it down below, where right wing pundits
started fear mongering over STRATCOM's posture statement,
Strategic Command's posture statement.
These are the people who control our nukes.
And it's odd, because I didn't actually
comment on the content of their statement at the time.
But that's what you need to notice.
And I'll put it down below.
You can listen to it.
And basically what it's saying is the reason STRATCOM
needs to exist is because there may be conditions,
because basically the US is so good at what they do when it
comes to war, especially when you're considering all of NATO
being involved, that it might put an opposition power, Russia,
in the situation where it views the use of nuclear weapons
as their least bad, bad option.
So what does that mean?
It means US and Russia start duking it out.
Because we're talking about all of NATO versus Russia.
Russia begins to lose.
Then NATO advances into Russian territory.
At this point, dynamics start changing.
This isn't a conflict that's external.
This is at home now.
Now, two more mistakes.
Those mistakes have to get made.
One, NATO continues to advance.
And two, Russia doesn't sue for peace.
At that point, it becomes really likely.
Because if the Russians are unwilling to surrender control,
Putin isn't going to give up.
Putin's not going to surrender.
And NATO keeps advancing, something
has to happen to turn the tides.
Russia has to try something.
Then nuclear weapons become an option.
But honestly, they're not going to be the first option.
Nuclear weapons are antiquated in a way.
There are a lot more technologies
that are available today than were available in the Cold War.
Some of which is equally disruptive
without being as destructive and without eliciting
that same massive response.
If Russia launches, NATO launches.
It is that simple.
This is why it didn't happen during the Cold War.
There's not a lot of wiggle room in this.
The whole idea of a limited exchange,
while theoretically possible, if you're
talking about in the midst of a conflict,
it would spiral out of control pretty quickly.
So you're talking about a whole lot of escalation
from the point where we're at to even get to the point
where it would be considered.
And understand when I said it becomes likely then,
I'm talking about it becomes likely that it's considered,
not that it actually gets used.
So you're talking about hostilities breaking out,
open hostilities, then NATO getting sucked in,
then NATO advancing to the point where
Russia feels a true threat of losing control,
then they start to think about it.
Now, I mean, there is the other option
that Russia decides that it's going to take on all of NATO
and by some miracle actually begins
to threaten Western Europe.
The same principle applies.
It becomes the least bad option.
It's not something anybody wants to do,
but we don't want to lose type of thing.
But again, just like with Russia,
there are other technologies available
that could turn the tide with outgoing nuclear.
I know because of Cold War rhetoric
that nukes seem to be the big thing.
And yeah, they are, but they're so big, they're so devastating,
nobody wants to use them because once they're used,
there will be a response.
And generally speaking, the decision makers,
the people who would make the decision to use them first,
they are also the other side's first targets.
It's not, it isn't incredibly likely.
Think about how long the Cold War ran and didn't happen.
It's not to say that it can't,
but I think it would be a mistake for the United States
to succumb to that kind of constant fear
of nuclear annihilation.
We're going back to a near-peer contest, yes,
but we can probably assume a little bit
of a different posture when it comes to strategic weapons,
when it comes to nuclear weapons like that.
So anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}