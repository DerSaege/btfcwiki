---
title: Let's talk about why NATO doesn't want to use troops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t-vWiX08ZIc) |
| Published | 2022/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NATO is exploring non-kinetic options, rather than deploying troops, in response to a confusing situation.
- The United States has superior military capabilities compared to Russia, but the issue lies in understanding doctrine and what countries are willing to do.
- Despite NATO's collective military strength, they are reluctant to militarily depose Putin due to the risk of nuclear war.
- Sanctions are being considered as a strategic option, with the United States leveraging relationships with countries like Saudi Arabia to influence oil prices.
- Strategic arms play a significant role in shaping foreign policy and can act as a deterrent against larger powers attempting political realignment.
- The possession of strategic arms can provide a sense of security for nations on the US hit list, as it deters intervention.
- Foreign policy is driven by power dynamics rather than moral considerations, with strategic arms altering the balance of this power game.
- Cyber capabilities, particularly in the hands of countries like Russia, are a significant factor in modern conflicts and could lead to disruptions for NATO nations.

### Quotes

- "Foreign policy isn't about right and wrong. It's about power."
- "They can't depose Putin militarily. The best they can do is create the conditions in hopes that the Russian people or his political allies depose him from within."
- "It can only go so far. It doesn't actually create change on the geopolitical scene."
- "The possibility of the US doing it through regime change in the way the US typically does it, not so much."
- "It's not as simple as dealing with a power that doesn't have strategic arms."

### Oneliner

NATO's reluctance to militarily depose Putin reveals the complex power dynamics and risks involved, shifting focus towards non-kinetic options and strategic arms in foreign policy.

### Audience

Policy Analysts, Activists

### On-the-ground actions from transcript

- Reach out to policymakers to advocate for diplomatic solutions and support non-kinetic options (suggested).
- Stay informed about international relations and geopolitical dynamics to better understand the implications of strategic arms (implied).

### Whats missing in summary

Deeper insights into the implications of strategic arms and the nuanced decision-making processes in international relations.

### Tags

#NATO #ForeignPolicy #PowerDynamics #StrategicArms #Geopolitics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about why NATO is looking
for non-kinetic options, why they're looking for options
other than putting in troops.
For a lot of people, this is confusing.
And this is especially true if you're
one of those people who may understand
the numbers and comparative capabilities of the militaries,
but you don't understand doctrine.
When it comes to what countries are willing to do.
If you understand the numbers, you're looking at this
and you're trying to figure it out, right?
Because it doesn't really make sense.
When this mythical fight takes place online
and people sandbox it and talk about it,
it's the United States versus Russia.
If you're familiar with the comparative capabilities,
the United States wins.
It's that old joke, the world's largest Air Force
is the United States Air Force.
The world's second largest Air Force
is the United States Navy.
And that's true, by the way.
It's a joke, but it's also accurate.
So why isn't that option being exercised?
Especially considering when you're talking about Europe,
all of NATO's involved.
All of those other militaries are in the fight as well.
NATO would in fact stomp Russia into the ground pretty quickly.
It would be bad.
There would be a lot of lost pretty quickly.
But Russia would be pushed out of Ukraine,
absent really bizarre fog of war stuff.
So why don't they do it?
Because this isn't the type of country
the US normally fights.
This is a near peer.
They can push them out of Ukraine,
but once they hit that border, oh, they hit a roadblock.
And it's not Russian winter.
It's not the Russian people's historical willingness
to put it all on the line.
When we talked about the worst case scenarios,
when we talked about nukes, when do they really
come on the table?
When one nation is worried about actually losing control.
The United States and NATO cannot militarily depose Putin
without running the risk of nuclear war.
That's why.
That's why they're reluctant to do that.
They can go in and push them out.
It would be bad, but they'd be able to do it.
But then nothing changes.
Nothing changes.
Because they won't, if they know, if they're smart,
they won't actually move further.
Because then the risks just, they become astronomical.
So this is why they're looking towards sanctions.
Now, people have asked about that
and how are they going to do it?
And this is where you get to look at some foreign policy
stuff, things that may not really make sense when you're
looking at it through the lens of right and wrong,
good and evil, moral and immoral.
Right now, what, oil's up to $100 a barrel?
Biden's calling Saudi Arabia.
Biden is calling Saudi Arabia.
In fact, we know, I think he reached out last week
and tried to get ahead of this.
The Saudis were unwilling to do so at that time.
Situation might have changed, so he's probably
calling them again.
Now, this explains why the United States has kind of
looked the other way.
Slid them a card every once in a while
at that international poker game where everybody's cheating
to keep this option in reserve.
Tensions have run high between the two countries,
relatively speaking, recently.
So we don't actually know that they're
going to go forward with it.
However, generally speaking, you can trust Saudi Arabia
to do what's best for Saudi Arabia.
And right now, they can offload oil at roughly 30% more
than they normally would.
Now, this is also another moment to learn another thing
about foreign policy.
When you talk about countries looking to obtain strategic
arms, the way it gets sold is always, well, if they get that,
one madman, and they could just go to town,
and they could launch.
With this in mind, knowing that strategic arms really
does stop the ability of a larger power
to politically realign your country.
Perhaps the way that topic gets framed to the world at large
is a little different.
Maybe one of the real reasons is if a country gets this power,
they kind of are guaranteed a pass
because nobody wants to risk it.
So countries that are on the US hit list,
countries that are oppositional to the United States,
all of a sudden, it starts to make sense
why they're looking for this capability,
because if they get it, they know the US, well,
they have that roadblock at the border
because they're not going to want to risk it.
From the other side, when you're talking about foreign policy
and you're the United States, or you're Russia, or China,
if you're any major world power, you
want the ability to politically realign other nations
as you see fit, because foreign policy
isn't about right and wrong.
It's about power.
Those strategic arms, they truly shift that balance.
They make it to where the game can only go so far.
That poker game stops before any player goes bust
if they have nuclear weapons.
That's why.
Now, in a video that's already been recorded
but keeps getting pushed back, we
talk a little bit about cyber capabilities.
That is also something that Russia is really good at.
They used it during Ukraine.
They're really good at that.
And a direct military confrontation
between NATO and Russia, they would let loose.
They would try everything that they could think of.
So there would be disruptions at home
for the civilian populace of NATO.
There's a likelihood that that's going
to happen anyway because of the sanctions to some degree.
But in conflict, it would be all out.
So if you're wondering why NATO isn't just
going in and cleaning house, especially if you really
understand the capabilities of NATO, all of NATO, versus Russia,
this is your answer.
They can only go so far.
They can't depose Putin militarily.
The best they can do is create the conditions in hopes
that the Russian people or his political allies
depose him from within.
The possibility of the US doing it through regime change
in the way the US typically does it, not so much.
Not something that is really likely.
So that should answer that question.
There's too much at stake.
It can only go so far.
It doesn't actually create change
on the geopolitical scene.
It just gets them out of Ukraine.
And that doesn't actually solve the problem
because Russia could attempt to come back in as soon as NATO
withdraws.
And it would be costly.
This is why.
It's not as simple as dealing with a power that
doesn't have strategic arms.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}