---
title: Let's talk about Ukraine and Russia's economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jc6RhyW3KbM) |
| Published | 2022/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the surprise in the Russian economy's instability during the Eastern Europe situation.
- Western powers see this as an opening to use sanctions as a tool rather than just a punishment.
- The Russian economy heavily relies on extracting and selling goods, with a significant portion sold to Europe.
- European powers could severely damage the Russian economy by increasing prices or finding ways to offset costs for citizens.
- NATO views the instability in the Russian economy as an opening and might intensify sanctions to destabilize Russia.
- The goal of sanctions may shift from punishing Russia to destroying its economy to end the war.
- Despite Russia having reserves, the devaluation of the ruble means their money doesn't have the same purchasing power.
- Sanctions are seen as a power move by NATO, focusing on destroying the Russian economy.
- The impact of sanctions on an average person in a country is felt long before achieving the intended goal.
- NATO might deviate from targeting individuals in power and instead opt for broader sanctions due to the Russian economy's instability.

### Quotes

- "The odds are that NATO is going to pursue this route because it's probably going to be effective."
- "We have to keep in mind that average person over there is going to feel this long before Putin."
- "When it comes to wars, I'm always on the same side. The people that don't have anything to do with it."

### Oneliner

Beau outlines how Western powers can use sanctions as a tool to destabilize the Russian economy, impacting civilians before achieving their goal.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Advocate for diplomatic solutions to conflicts (implied)
- Support organizations aiding civilians affected by conflicts (implied)

### Whats missing in summary

The full transcript provides a nuanced understanding of how economic sanctions can impact civilians during conflicts beyond their intended targets.

### Tags

#RussianEconomy #Sanctions #NATO #ForeignPolicy #EasternEurope


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Eastern Europe,
but in particular, we're going to talk about
the Russian economy, because that was the real surprise.
As soon as things got loud,
the Russian economy showed signs of instability,
signs that were big enough to notice.
For the Western powers, that's going to be an opening.
They see that as an opening.
They will view that as a sign that sanctions
are going to be more than just a punishment.
They'll be a tool in the fight.
Now, the Russian economy is heavily centered
on taking stuff out of the ground and selling it.
That's a huge chunk of the Russian economy.
A large portion of what is sold is sold to Europe.
If the European powers stood together and were like,
okay, we're just going to pay more, we're going to pay more,
find a way to offset it,
offset the cost for the average citizen,
that would severely damage the Russian economy.
And the instability that was shown,
that's going to be blood in the water to NATO.
They see an opening, so they're probably going to take it.
I would anticipate the sanctions to probably be even more severe
than they initially let on,
because it's no longer going to be about punishing Russia.
It's going to be about destabilizing it.
It will become a tool,
and the hope would be to destroy the Russian economy.
Because if the economy falters, well, then the war can't go on,
and that's how they're going to look at it.
Russia has some pretty large reserves.
They're sitting on some cash.
The thing is, the ruble, it lost value.
So that money, it doesn't buy as much anymore.
But it would still take time to burn through that.
Now, normally when I talk about foreign policy,
I talk about what is.
I don't get into what should be
or some of the more ethical considerations,
because as we've talked about a whole bunch,
when you're talking about foreign policy, morality, ethics,
none of the stuff plays into it.
It's about power.
In this case, the sanctions are going to be a power move for NATO.
Something to keep in mind is that when you're talking about sanctions
that are intent on destroying an opposition's economy,
it impacts the average person in that country long before
it actually starts to achieve its goal.
It's something that's worth noting,
because the odds are that NATO is going to pursue this route
because it's probably going to be effective
and they're going to use every tool at their disposal.
We have to keep in mind that average person over there
is going to feel this long before Putin.
There's nothing to be done about that,
because realistically, it's a huge opening for NATO,
and I would be really surprised if they didn't take it.
So your calls to whoever,
if you want to stand up for the people of Russia,
they're not going to do any good.
We are past that point.
This is a tool that they have.
I would be shocked if they didn't use it.
The general rule lately has been to try to limit sanctions to those in power
and try to disrupt them as individuals.
Given the instability in the Russian economy, what was demonstrated,
they may break with that recent tradition.
The way of doing it before was just to try to destroy the economy.
NATO has it within their power to do that.
Whether or not they elect to is probably going to be up for debate,
but I would be surprised if that debate went in favor of not using the tool that they discovered.
So I would expect that, and I would expect that soon.
You know, there were a whole bunch of sanctions that were being discussed.
I would anticipate that almost all of them get used,
and probably some that were talked about in news coverage is like something that,
you know, it's on the table, but they wouldn't really do it.
Yeah, they'll probably do it now because that was a weakness that was displayed.
And if NATO is as unified and is as serious about countering this move as they have publicly messaged,
they're probably just, they're going to unload.
You know, I bring up the impact on Russian civilians because when it comes to wars,
I'm always on the same side.
The people that don't have anything to do with it.
And in this case, that is who is going to take the hit first.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}