---
title: Let's talk about Lindsey Graham distancing from Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N1LuQBowO0U) |
| Published | 2022/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lindsey Graham is making strategic moves within the Republican Party, distancing himself from Trump and Trumpism.
- Graham, a long-time politician, is positioning himself away from Trump and those similar to him.
- Other Republicans, including Dan Crenshaw, are also starting to distance themselves from Trump.
- The Republican Party is akin to a mob where loyalty is based on benefits and performance.
- Trump is losing support within the party due to his dwindling numbers and potential legal issues.
- Clones of Trump, aspiring governors, are not politically viable as they lack the polish and may face backlash from Trump himself.
- Graham's actions, such as condemning Trump's statements and supporting Biden, are strategic moves to distance himself.
- Republicans are likely to distance themselves from Trump and his ideologies due to potential future consequences.
- The establishment Republicans have learned not to underestimate Trumpism after being surprised by Trump's initial rise.
- There is a shift happening within the Republican Party away from Trump and towards a different direction.

### Quotes

- "The Republican Party is a lot like the mob. You are only as good as your last envelope."
- "He is definitely starting to distance himself in a lot of different ways. Not just from Trump, but from those like Trump."
- "Former President Trump, well, he is a petty, petty man."
- "They probably understand that the clones of Trump, the various governors who want to be the next Trump, they probably understand that they're not politically viable either."
- "I don't think that the establishment Republicans are going to underestimate Trumpism again."

### Oneliner

Lindsey Graham and other Republicans strategically distance themselves from Trump and Trumpism within the Republican Party, foreseeing potential consequences and shifts in direction.

### Audience

Republican voters

### On-the-ground actions from transcript

- Position yourself strategically within your community or organization (implied).

### Whats missing in summary

Insights on the potential implications of the Republican Party distancing itself from Trump and the impact on future political landscapes.

### Tags

#RepublicanParty #LindseyGraham #Trump #Trumpism #GOP


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about Lindsey Graham
and the new direction he is kind of striking out in. We're going to talk about Trump
and possible follow-ups to Trump
and we're going to talk about what is likely to play out over the next
few months inside the Republican Party.
Because Graham's moves, they are foreshadowing something.
The Republican Party is a lot like the mob. You are only as good as your last envelope.
And right now, Trump isn't kicking up a whole lot.
Not providing a lot of benefits to the organization.
He can't fill those rallies, he can't bring in those numbers.
And he probably has some criminal liability
headed his way to him and his crew.
So you're starting to see some of the strategic thinkers
in the Republican Party start to distance themselves from him.
Lindsey Graham, and I know a lot of people don't like him,
I don't particularly like him, but I will not discount the fact that he's been up there
for a quarter of a century.
He has survived politically for a long time.
He is definitely starting to distance himself in a lot of different ways.
Not just from Trump, but from those like Trump.
Because he can see what's coming down the road.
Crenshaw hadn't been up there long, but he is somebody who is known for
thinking long-term.
They're distancing themselves.
They're positioning themselves to get away from Trump.
From Trumpism. From the Freedom Caucus.
Now Graham, his term doesn't end until I want to say 2027.
So he's safe. He doesn't need Trump's endorsement.
So he can go ahead and start making those moves.
As he does it, it makes it easier for other,
less secure Republicans to make those same moves and start to distance themselves.
Now the other thing is they probably understand that the clones of Trump,
the various governors who want to be the next Trump,
they probably understand that they're not politically viable either.
So they're probably not going to line up with them.
And they're not politically viable for a number of reasons.
One, Trumpism got defeated with Trump.
It's unlikely that a copy of a copy is going to be able to pull it off.
Unless that copy is a lot more polished.
And the governors who are seeking to emulate Trump, they're kind of not.
And they're showing that. They're making a lot of the same mistakes.
The other reason they may not be politically viable is because former President Trump,
well, he is a petty, petty man.
And this is somebody who was, certainly appeared to be ready to torpedo American democracy
because he lost an election.
You don't think that he might torpedo somebody who he helped elevate
if they were to beat him in the primary?
If you're trying to be the next Trump and you don't have Trump on your side,
that portion of the base that he still has a hold over, he'll swing them against him.
So as you look at Graham's actions, you know, kind of condemning some of the stuff that Trump has said,
coming out on Joe Biden's side, on President Biden's side for the black woman on the Supreme Court,
and you're wondering where is it coming from?
This is just him distancing himself.
Whether or not he actually believes any of the stuff that he's saying, we'll never know.
That's how he survived up there for as long as he did.
That's how he's able to survive politically.
But I would expect to see other Republicans start doing it.
Other Republicans start distancing themselves, not just from Trump,
but from the Trump clones and from the Freedom Caucus, from the hardcore Trumpism havens,
because I think they're starting to realize with everything coming down the road,
with his lackluster performance politically, the possible criminal liability that's coming,
and the committee report coming out, they don't want to be tied to him in their own election
when they're coming up for re-election,
and they probably don't want that brand of right-wing ideology at the helm of the Republican Party.
They were surprised by Trump the first time around.
I don't think that the establishment Republicans are going to underestimate Trumpism again.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}