---
title: Let's talk about improv and talking to liberals and centrists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IhHCf6eEGzw) |
| Published | 2022/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how to use British comedy and improv rules to reach centrists and liberals and guide them towards a more progressive position.
- The five rules of improv are discussed: "Yes, and", no open-ended questions, you don't have to be funny, make your partner look good, and tell a story.
- Emphasizes the importance of not making jokes or using in-group humor that the other person may not understand.
- Advocates for being kind, asking guiding questions, and moving slowly to help centrists and liberals shift towards a more progressive stance.
- Provides a practical example using the issue of police reform to demonstrate how to navigate a centrist position towards defunding the police through thoughtful questioning and agreement-building.

### Quotes

- "Yes, and move them."
- "Ask questions that drive them in the direction they want."
- "Nobody has the perfect take about everything."
- "Don't berate them."
- "Talk about it in terms of right and wrong."

### Oneliner

Using British comedy and improv rules, Beau guides how to gently steer centrists and liberals towards more progressive viewpoints through kindness, questions, and agreement-building.

### Audience

Activists, Progressives, Advocates

### On-the-ground actions from transcript

- Ask guiding questions to gently shift perspectives towards progressivism (exemplified).
- Avoid making jokes or using in-group humor that may alienate the listener (exemplified).
- Be kind and patient in guiding individuals towards more progressive viewpoints (exemplified).

### Whats missing in summary

Full understanding of Beau's practical approach to engaging centrists and liberals through kindness, questions, and agreement-building.

### Tags

#BritishComedy #ImprovRules #Progressivism #GuidingQuestions #Centrists


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about British comedy.
We're going to talk about improv, the five rules of improv,
and how to reach centrists and liberals.
Everybody's familiar with whose line is it anyway, right?
That's kind of what we're going to be talking about.
I've watched a lot of your videos on how to argue various points.
They're helpful, but they're all geared
towards reaching the right.
How about some tips for reaching centrists or libs
and getting them to a more progressive position?
People that aren't really wrong, but they're not as far along
as they could be.
To use your bus analogy, how do we
talk them out of getting off at the first stop?
The five rules of improv.
That show, whose line is it anyway?
It's not scripted, but they have these rules that they follow.
And that's why it works.
That's why the jokes happen.
So what are the rules?
Yes, and is number one.
Number two, no open-ended questions.
Number three, you don't have to be funny.
Number four, make your partner look good.
Number five, tell a story.
That's going to make sense in a second.
OK, so yes, and.
As you said, they're not wrong.
They're just not as far along as they could be.
Don't act like they're wrong when
they say whatever they're saying.
Yes, and take it a step further.
And I'll show you how this works in a second.
No open-ended questions.
If you ask somebody something that
requires them to go and explain their entire philosophy,
you're probably going to lose them.
Keep it short.
Keep it simple.
Yes and no questions.
You're pregnant or you're not.
You don't have to be funny.
In this case, don't make fun of them.
Don't use, another one especially on social media,
don't use in-group jokes.
They're not going to get them.
If they were going to get them, they
wouldn't have the opinion they have, right?
I've seen this time and time again.
Conversation's going well, and then the more progressive
person makes a joke.
The person they're talking to is like, well,
how would you handle this?
And they're like, well, in Minecraft, yeah, you've lost
them.
They don't know what that means.
So don't make jokes.
Especially don't make jokes about them or in-group jokes
that they're not going to get.
Try to keep their attention.
Then make your partner look good.
Really just don't make them look bad.
Nobody wants to be berated.
Even if their take is not great in a lot of ways,
if the foundation is there to get them to a better place,
there's no reason to slam them for it.
Just keep moving them along, right?
And then five is tell a story.
In improv, you're trying to keep a cohesive storyline
as you're saying all this random stuff.
In this case, it's just trying to keep it going at all,
trying to keep that conversation moving,
keeping them on the bus as long as possible.
OK, so how does this work in real life?
Yeah, what happened to Floyd?
That was wrong.
And we definitely need to change some things.
But cops have a tough job.
Very centrist position, right?
And normally, how would most people
of a more progressive stance respond to something like that?
They have a tough job.
Not as tough as Floyd, right?
Don't do that.
Don't make them look bad.
Don't make jokes either.
Don't make them look like it's a poke fun of how it's not really
that dangerous or something like that.
Skip over those.
Don't do those.
Those are rules three and four, OK?
So they're saying they want change,
but the cops have a tough job.
Yes, they do.
And how do you think we can eliminate that?
Maybe they'd be better if they didn't
have to take that off their plate.
No open-ended questions, right?
Do you think it'd be better off if they didn't have to respond
to mental health calls?
They're not really trained for that.
You're probably going to get a yes, right?
Yeah, they're not trained for that.
So we're going to take that off their plate
and make their job easier because they
have such a tough job.
So you can get them to agree with that.
And then say, well, would it make sense
to have social workers, people who are actually
trained to deal with that, to do the wellness checks?
Well, yeah, that would make sense.
Well, how are we going to pay for it?
Maybe since they don't have to do those calls anymore,
since the cops don't have to respond anymore,
we can pull some of the funding from there
and use that to fund the social workers.
Three questions from a super centrist position
to defund the police.
Be nice.
It's really what it boils down to.
Ask questions that drive them in the direction they want.
Take their starting position.
Yes, and move them.
Ask questions that will guide them
to the logical conclusion.
One of the things that happens with more progressive people
is that they forget that almost always it
is the morally defensible position.
It's the thing that most people would agree
is right and good and true.
Stick with that.
When you're talking about people who are centrists,
they don't have a theoretical framework for what
you want to talk about.
Just talk about it in terms of right and wrong.
Talk about it in terms of right and wrong.
And move them along.
Yes, and no open-ended questions.
Keep them thinking.
Keep them talking.
Don't make fun of them.
And try to make them look good if you can.
At least don't make them look bad.
Don't berate them.
Nobody wants to be on the bus with somebody yelling at them.
So try to move it slowly.
Nobody has the perfect take about everything.
Acknowledge that.
If it's something that you can work with,
try to work with it as long as it's not harmful.
And then you have to switch up.
If it is something that's harmful,
then yeah, you can't use this.
But if you're talking about people
who are generally headed your direction
and you just want them to go a little bit further,
try to ease them into it.
Yes, and ask some questions.
Don't make fun of them.
Don't berate them.
And keep it going for as long as you can.
Now, if you're sitting there at home right now going,
isn't this Socrates?
Yeah, it kind of is.
Very similar.
And it works.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}