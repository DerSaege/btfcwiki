---
title: Let's talk about Elvis, shirts, and changing the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_13sDGw_5aQ) |
| Published | 2022/02/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There is a group of eight or nine people in Alabama who want to make positive changes in their community, such as putting in wheelchair ramps, helping single moms, and paying off fines for people leaving jail to prevent them from getting stuck in the system.
- They face challenges in fundraising for their activities, especially in an incredibly religious area where helping single moms and criminals might be seen negatively.
- Despite having assets like the ability to make T-shirts and a relative with a stand at a flea market, they struggled to raise funds through progressive-themed merchandise.
- They considered selling right-wing merchandise to fund their left-wing activities, but faced backlash online for compromising their values.
- Beau suggests that if making and selling right-wing merchandise is what it takes to fund their projects and spread progressive ideas, they should go ahead with it.
- He argues that in a war on poverty situation, they should use any tool available to them, even if it means compromising on ideological purity.
- Beau encourages them to prioritize helping people in need over passing an ideological purity test imposed by others online.
- He points out the importance of taking money from opponents legally to do good work, spread progressive messages, and challenge those profiting from selling right-wing merchandise.
- Beau advises not to disclose this fundraising method to those who may have issues with it, focusing instead on the positive impact their projects will have on the community.
- Drawing parallels to Elvis facing resistance in the South, Beau urges the group to focus on their mission of helping those in need, even if it means making uncomfortable compromises.

### Quotes

- "Wars cost money. And if you are, as they are, behind enemy lines, surrounded, use any tool the opposition will give you."
- "People get their fines paid. People get their ramps. Kids get food. Seems like a small price to pay."
- "You're surrounded. You are outnumbered. You're outfinanced."

### Oneliner

A group in Alabama faces fundraising challenges for their community projects, prompting Beau to advocate for utilizing any means necessary, including selling right-wing merchandise, to support their vital initiatives.

### Audience

Community activists

### On-the-ground actions from transcript

- Sell merchandise at flea markets to raise funds (suggested)
- Prioritize community projects over ideological purity tests (implied)

### Whats missing in summary

The importance of prioritizing community impact over ideological purity and utilizing creative means to fund vital projects.

### Tags

#CommunityActivism #FundraisingChallenges #IdeologicalPurity #ProgressiveIdeas #SupportingCommunity


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about what people
who want to change the world can learn from Elvis, kind of.
I had somebody send me a message
and it was a long message and then there,
it was a back and forth, so I'm not gonna read it,
but the situation is this.
There's a group of eight or nine people in Alabama
and they're in the deep South, the old South,
and they have their little network
and they have the ability to do a lot of stuff.
They want to put in wheelchair ramps for people.
They want to help single moms specifically,
and they want to pay off fines for people
after they get out of jail
so they can actually get out of the system.
People who can't afford to pay off the fines,
a lot of times they just get stuck.
These are the three things that they have kind of,
you know, zeroed in on.
All this stuff costs money, right?
And in a incredibly religious area,
you're not gonna be able to fundraise to help single moms.
You're not gonna be able to fundraise to help criminals.
That's the way it's gonna be seen, right?
So they went through their assets,
their inventory of what they could do,
and they have the stuff to do T-shirts, to make T-shirts.
And they have an uncle, one of them has an uncle,
who has a giant stand at a big flea market.
If you're not from the South, just understand,
flea markets sometimes are so big,
they're like destinations.
This is probably two, three hours from me,
and I know people that go there.
So they went through and they made
a bunch of very progressive themed stuff,
and they had the uncle try to sell it, didn't get anywhere.
And they came up with the idea
of selling right-wing merchandise
to fund their incredibly left-wing activities.
And they talked about it in some online chat room.
And people from all around the country,
not where they live, basically told them
that it would be wrong to do that
because you're helping spread the right-wing stuff.
And to use the term that was in the message,
they got purity tested out of it.
That was three months ago.
And it's three months later,
and they still don't have any funding.
They don't know what to do.
So they sent a message asking
how they could fundraise there.
You know, the stuff they want to do,
it's war on poverty stuff.
It is war on poverty stuff.
Wars cost money.
And if you are, as they are,
behind enemy lines, surrounded,
use any tool the opposition will give you.
Make the shirts.
Make the shirts.
If that's what it will take for you to do this,
and in the process,
you're exposing people to more progressive ideas
as you conduct these little assistance projects,
make the shirts.
I've been to the place where your uncle has a shop.
There's no shortage of right-wing merchandise.
It's not like you're putting something out there
that doesn't exist.
And as far as a purity test goes,
how would you feel about saying,
oh, we would totally help you with diapers
and baby formula this month,
but we just couldn't bring ourselves
to sell a thin blue line shirt?
Come on.
People who are online a lot,
and people who discuss theory a lot,
they can afford to be ideologically pure.
You can't.
It's really that simple.
You don't have that luxury.
You have people around you who need help.
And if you can get money from people
who wouldn't normally help them to do it, why not?
I don't see the problem with it, to be honest.
You're in the process of doing this, right?
You are taking money from people who are opposed to you legally,
using it for good purposes,
and hopefully spreading a more progressive message
in an area that desperately needs it.
And also, you're taking money out of the pockets
of people who would want to sell that kind of merchandise.
I don't...
Yeah, you're going to fail an ideological purity test.
Okay, whatever.
People get their fines paid.
People get their ramps.
Kids get food.
Seems like a small price to pay.
And here's a wild idea.
Don't tell them.
The people who would have an issue with this in your chat group,
they're not there.
I noticed that it's been three months
and they didn't offer to help with the funding.
Now, what's this have to do with Elvis?
You know, when Elvis kind of took the nation by storm,
he ran into a lot of resistance in the South.
In fact, there were people who wore buttons that said,
I hate Elvis.
Do you know who sold those buttons?
Elvis's manager.
You're surrounded.
You are outnumbered.
You're outfinanced.
Yeah, I mean, it's not ideal,
but I don't think that I would put off projects
that were that important just to appease
some ideological purity test put to me
by somebody on the other side of the country that I've never met.
You know the people that you're going to be helping.
Seems like they would come first.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}