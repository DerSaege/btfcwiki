---
title: Let's talk about roadblocks in Canada and the manual....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=50X0kus2ePk) |
| Published | 2022/02/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the Canadian response to a roadblock on a bridge, praising their adherence to protocol and minimal use of force.
- Notes that the Canadian response was successful in denying the opposition a propaganda victory due to lack of dramatic footage.
- Mentions that if Canada continues handling similar situations the same way, this incident will likely be forgotten.
- Emphasizes that the goal of the Canadian forces should be to remove all talking points and PR wins from the opposition.
- Addresses the issue of members of a Canadian elite military unit being involved in a controversial movement and the importance of counterintelligence in such cases.
- Suggests that those sympathetic to extreme elements should be removed from units that may protect high-profile individuals like the prime minister.

### Quotes

- "Nobody's gonna get their stripes because they were there."
- "It denies the opposition any victory."
- "They denied them that media win, that public relations win."
- "Handling it the way they're handling it removes all of their talking points."
- "They just look silly now."

### Oneliner

Canadian forces handle a bridge protest textbook-perfect, denying opposition propaganda wins and aiming to fade the incident away through protocol adherence and counterintelligence vigilance.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Conduct more extensive background checks and monitor social media for sympathies in high-risk units (implied)
- Maintain adherence to protocol in handling similar situations to prevent propaganda victories (implied)

### Whats missing in summary

The full transcript provides detailed insights into the Canadian response to a bridge protest, the importance of protocol adherence, and the necessity of counterintelligence measures in elite military units.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Canada a little bit more.
A couple of things about that.
Mainly that little roadblock there on the bridge
that is no more.
If you don't know, the bridge is open.
At least it is at time of filming.
I don't foresee it being retaken.
So how did they do when it comes to following the textbook,
following the playbook, following the manual?
They did everything right.
The Canadian response at the bridge,
it was absolutely textbook.
So short version, you know, they put out the warning,
you need to get out.
Some people left.
Some people stayed behind.
They waited and watched them throughout the night,
let them get tired.
Some left, they moved in
when there was a minimum amount of people.
They used the minimum amount of force.
They followed the textbook to the letter.
Now, I do understand that there are demographics
within Canada who do not get the benefit of the textbook.
And I understand that.
One thing that I do want to point out
is that if they continue to use the manual
and they handle the other places
where stuff like this is going on the same way,
this won't even be a footnote.
This will be forgotten.
Nobody's gonna get their stripes because they were there.
When they use the manual,
and the reason that this playbook exists,
it denies the opposition, in this case, the quote truckers,
it denies them any victory.
They don't get a propaganda victory
because they don't get that footage.
They don't get, you know, the sympathy that's generated
by the state coming in and just, you know,
clubbing people or whatever.
If you watch the takedowns,
I am not exaggerating when I say this,
the worst takedown I saw,
the most physical arrest that I saw in that footage,
while covering demonstrations in the United States
and identified as press,
I have been roughed up more by accident by American cops.
They did a great job.
So they denied them that media win,
that public relations win.
They don't get that.
It was a failure on all fronts.
They didn't secure the bridge.
They didn't keep it,
after they said they were going to,
and then they didn't get any dramatic footage
that they can use to garner sympathy.
If the Canadian forces continue using the manual,
this won't be remembered.
This will be something that is probably forgotten about
within two years.
It won't be a viable recruitment tool.
And for a lot of the groups involved,
remember the mandate isn't the reason, it's the excuse.
And it's not really about that.
It's about getting people associated with the group,
getting them in.
It's a recruitment effort, right?
All the Canadian forces have to do
is keep doing what they're doing,
exactly the way they're doing it.
And this will fade away.
If they deviate, they may end up with an issue.
Now I know some people are going to say,
well, they're only treating them that way
because they agree with them.
Some of the cops may agree with them.
Okay, doesn't matter.
It doesn't change the outcome.
And the outcome is that handling it
the way they're handling it
removes all of their talking points.
It removes all of their PR wins.
They don't get that dramatic footage.
They don't get the image of the state coming in
and just crushing the resistance.
Nobody's going to root for them
because they're the underdog.
They just look silly now.
And that would be the goal.
Now, the other news from Canada
is that they have the same problem the United States has.
It appears that a couple of members
of one of their high-speed teams,
one of their elite units in the military were involved.
My understanding is that they are being processed out
as we speak, which is the right move.
But this shows, hopefully demonstrates
to Canadian counterintelligence
that they've got to look for this.
You know, this is a problem in the US military.
And in this case, again,
you don't know if these people are just against the mandate
or they are aligned with some of the more extreme elements
that were kind of prodding this little movement along.
And that's something that Canadian counterintelligence
is going to have to deal with.
I know there may be some, you know,
wondering why they would take the extreme measure
of booting them.
It works differently there.
This is a unit that could be called upon
to protect the prime minister.
So if they're part of a group or sympathetic to a group
that has called him a traitor
and called for a lot of extreme measures
as far as he's concerned, well, they can't be involved.
So that, as far as I understand,
the move that the Canadian military is taking,
that's the right one.
They got to go.
It should probably be followed up
by some more extensive background checks
and checking out people's social media
and checking out their sympathies.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}