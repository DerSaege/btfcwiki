# All videos from February, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-02-28: Let's talk about peace talks between Ukraine and Russia.... (<a href="https://youtube.com/watch?v=aKGsQdaGg4E">watch</a> || <a href="/videos/2022/02/28/Lets_talk_about_peace_talks_between_Ukraine_and_Russia">transcript &amp; editable summary</a>)

Peace talks between Russia and Ukraine are uncertain amid economic turmoil in Russia, strong Ukrainian resistance, and a window of hope for preventing further escalation.

</summary>

"The resistance is formed. They will be dealing with this for years if they stay."
"He has all of the economic reasons in the world to untangle himself from this mess and not a lot of military reasons to stay."
"It's just a thought."
"This is a window, I'm hoping, that all the parties involved understand that this is the chance to stop this before it gets really bad."
"If negotiators push too hard, he may recommit and just say, fine, we're going to take the losses for the next however long it takes."

### AI summary (High error rate! Edit errors on video page)

Peace talks have started between Russia and Ukraine, but the outcomes are uncertain.
Typically, the first round of peace talks does not lead to lasting peace.
The Russian advance in Ukraine has stalled, with Ukrainian resistance holding up well.
Dynamics have shifted outside Ukraine, with the possibility of Belarus joining the conflict.
Several European countries are offering significant military aid to Ukraine, which may not sit well with Putin.
The economic situation in Russia is deteriorating rapidly, with the ruble losing value and interest rates spiking.
Oligarchs in Russia are losing money and publicly calling for peace, which may impact Putin's power.
The economic turmoil in Russia is more severe than anticipated, with significant impacts on their GDP and economy.
The timing of the economic situation worsening and Russia's willingness to talk peace seems coincidental.
Putin had a plan to reunite Belarus, Ukraine, and Russia, but the economic reasons to back away are strong.
The resistance in Ukraine is strong and formed, indicating a prolonged conflict if Russia continues its military actions.
Negotiators may need to offer Putin a way out to speed up the peace process and prevent further escalation.
There is a window of opportunity for all parties involved to stop the situation from worsening.

Actions:

for world leaders, negotiators,
Offer Putin a way out to speed up the peace process (implied)
Understand the window of opportunity to stop the conflict from escalating further (implied)
</details>
<details>
<summary>
2022-02-28: Let's talk about fallout from the unthinkable.... (<a href="https://youtube.com/watch?v=ox8ZDbCNTCE">watch</a> || <a href="/videos/2022/02/28/Lets_talk_about_fallout_from_the_unthinkable">transcript &amp; editable summary</a>)

Beau simplifies nuclear blast response into two rings, advises on seeking medical attention or finding shelter, and stresses the importance of limiting fallout exposure.

</summary>

"Your key thing is to limit your exposure to fallout for as long as possible."
"Either you're here or you're not."
"It's the Cold War."

### AI summary (High error rate! Edit errors on video page)

Talks about historical realities and the Russian and U.S. Strategic Command being on alert daily.
Mentions Putin's announcement of putting his nuclear arsenal on alert, which is not a significant change in posture.
Emphasizes the importance of knowledge in alleviating concerns, especially in the event of a nuclear blast.
Criticizes traditional training on nuclear blast response for providing too much information that may hinder decision-making.
Simplifies the response to a nuclear blast into two rings: "you felt it" and "you saw it."
Explains actions to take if you are in the "you felt it" ring, including seeking medical attention.
Provides guidance for those in the "you saw it" ring, debunking Hollywood myths about fallout and advising to seek shelter in a sturdy building.
Advises staying away from windows, being in the center of the building, and taking precautions against fallout.
Recommends using dirt to cover windows as a barrier against radiation.
Suggests actions to take if caught outside during fallout, such as getting inside immediately and removing contaminated clothing carefully.
Encourages listening to battery-powered radios for civil defense instructions and staying tuned for guidance.
Assures that most cars should still function post-blast, contrary to movie depictions of EMP effects.
Stresses the importance of limiting exposure to fallout and waiting for further information before taking action.
Mentions the survival rates post-Little Boy bombing and the significance of staying inside to limit exposure.
Recommends accessing archive.org for more information on nuclear blast survival strategies.

Actions:

for emergency responders and civilians,
Seek medical attention if in the "you felt it" ring (implied)
Find a sturdy building and shelter inside if in the "you saw it" ring (implied)
Listen to battery-powered radios for civil defense instructions (implied)
Limit exposure to fallout by staying indoors (implied)
</details>
<details>
<summary>
2022-02-28: Let's talk about Trump at CPAC.... (<a href="https://youtube.com/watch?v=-vcp9xgA_CA">watch</a> || <a href="/videos/2022/02/28/Lets_talk_about_Trump_at_CPAC">transcript &amp; editable summary</a>)

Beau points out the declining support for Trump within his own base, a significant story often overshadowed by his CPAC win and speech.

</summary>

"American leaders are dumb. Everything that's happening in Ukraine is Biden's fault."
"Trump is losing steam much faster than a lot of people want to admit."
"Trump has dropped 11 points among his people, his crew, his fan club."

### AI summary (High error rate! Edit errors on video page)

CPAC, the conservative club for Trump supporters, reveals true conservative beliefs.
Coverage of CPAC focuses on different speeches, notably Trump's.
Trump's speech at CPAC blames Biden for Ukraine, praises Putin, and ignores his own inaction.
Trump won the CPAC straw poll for 2024 with 59%, down from 70% previously.
The drop in Trump's poll numbers within his base is significant but overlooked.
Some Republicans and Democrats have vested interests in Trump's continued influence.
Trump's declining support within his own base indicates a loss of steam.

Actions:

for political analysts, trump supporters,
Analyze political trends within conservative circles (implied)
Stay informed about evolving political dynamics (implied)
</details>
<details>
<summary>
2022-02-27: Let's talk about the Carrington Event.... (<a href="https://youtube.com/watch?v=-6JoX8zVszY">watch</a> || <a href="/videos/2022/02/27/Lets_talk_about_the_Carrington_Event">transcript &amp; editable summary</a>)

Beau explains the impact of a Carrington-sized event today, the playbook to handle it, and the importance of preparedness for inevitable CME occurrences.

</summary>

"A CME will hit Earth. It's happened in the past. It's going to happen again."
"Everything off, this system shouldn't get overloaded."
"Are we going to handle it? Are we going to deal with it?"
"There are people out there that have specializations in different fields and listen to them a little bit more."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of a coronal mass ejection (CME) caused by a geomagnetic storm on the Sun, which can potentially hit Earth and cause disruptions.
Describes the Carrington event of 1859, where a massive CME caused chaos by sending electricity down wires, leading to fires and other problems.
Mentions that auroras, like the Aurora Borealis, can be seen during such events, with examples of occurrences in different years and locations.
Warns about the potential impact of a Carrington-sized event today, where transformers could blow, the power grid could fail, and various technologies like radio and GPS could cease to function.
States that the cost to restore the US infrastructure after such an event could be around two and a half trillion dollars, taking months or even a year to recover.
Talks about the playbook in place for handling such events, involving turning everything off to prevent system overload.
Notes that early warning satellites provide limited time for preparation, usually around 12 hours to a day or two.
Raises concerns about the government's efficiency in implementing plans, despite having preparations in place.
Emphasizes the inevitability of a CME hitting Earth in the future and questions whether society will effectively respond to it.
Encourages people to acknowledge the expertise of specialists in different fields and be prepared for potential disasters like a Carrington event.

Actions:

for preparedness advocates,
Stay informed about space weather alerts and updates (implied)
Prepare an emergency kit with essentials like food, water, and supplies (implied)
</details>
<details>
<summary>
2022-02-27: Let's talk about Pamela Moses and Tennessee.... (<a href="https://youtube.com/watch?v=Vb9Xs_1YDGY">watch</a> || <a href="/videos/2022/02/27/Lets_talk_about_Pamela_Moses_and_Tennessee">transcript &amp; editable summary</a>)

Beau sheds light on Pamela Moses' voting rights battle in Tennessee, questioning the excessive sentence and hinting at potential political motives, urging continued attention to the case.

</summary>

"Six years is probably excessive."
"This battle isn't won yet."
"Let's not let this drop out of our eyesight here."

### AI summary (High error rate! Edit errors on video page)

Overview of Pamela Moses' story in Shelby County, Tennessee regarding voting.
Pamela Moses, an activist, ran for mayor in Memphis.
She believed she was eligible to vote as she was off probation, supported by a certificate from the Department of Corrections.
However, her voting rights were questioned, leading to a trial where she was sentenced to six years in prison for attempting to vote.
The judge ordered the sentence vacated, granting a new trial due to a document not being turned over by the Department of Corrections.
The document's content remains unknown, causing a shift in the case.
The excessive nature of the original sentence is pointed out by Beau, suggesting it is cruel and unusual.
Contrasts Pamela Moses' sentence with shorter sentences for those who attempted to undermine legal votes or used violence.
Beau hints at a possible political motive behind the harsh sentence, implying Tennessee politics may be at play.
Beau believes public awareness played a role in revisiting Pamela Moses' case.
Encourages vigilance and ongoing attention to the case, indicating it's not yet resolved.

Actions:

for activists, voters, community members,
Stay informed on Pamela Moses' case and outcomes (implied).
</details>
<details>
<summary>
2022-02-26: Let's talk about shifting landscapes in Ukraine.... (<a href="https://youtube.com/watch?v=sRs7b1wVKL8">watch</a> || <a href="/videos/2022/02/26/Lets_talk_about_shifting_landscapes_in_Ukraine">transcript &amp; editable summary</a>)

Beau provides an insightful analysis of the rapidly changing landscapes in Ukraine, detailing Russian failures, international responses, and the shifting dynamics of the conflict, indicating a challenging road ahead for Putin.

</summary>

"Only 1,100 lost. Another way to say it is that in three days, Russia has lost a quarter of the forces that the United States lost in Iraq through the whole thing."
"They're already at that point where they're trying to just run up the bill for Russia, and they haven't lost the capital yet."
"Zelensky is proving to be an incredibly charismatic leader."

### AI summary (High error rate! Edit errors on video page)

Talks about the rapidly changing landscapes in relation to Ukraine, elaborating on the resistance and Russian failures.
Mentions the early Russian failures in planning, logistics, and intelligence that have shifted the tides against Russia.
Points out that Russia failed to secure air dominance and is suffering catastrophic losses due to this oversight.
Describes the existence and combat effectiveness of both regular and air forces in Ukraine, which shouldn't be the case at this point in the conflict.
Compares the current conflict dynamics in Ukraine to the Vietnam War, with irregular and regular forces facing a great power.
Notes the foggy estimates of losses but stresses that Russia has suffered significant casualties in a short period.
Explains the shifting political landscape, with European countries now supporting larger sanctions like removing Russia from SWIFT and sending lethal aid to Ukraine.
Talks about Russia's struggle to find allies, including a surprising response from Kazakhstan and Biden's request for billions to assist Ukraine.
Mentions Putin's control of information by shutting down Twitter in Russia and the political challenges he faces domestically.
Concludes by stating that while Ukraine still faces challenges, they have entered the phase of making it costly for Russia and have international support due to their successes.

Actions:

for politically aware individuals,
European countries sending lethal aid to Ukraine (exemplified)
Supporting larger sanctions like removing Russia from SWIFT (exemplified)
</details>
<details>
<summary>
2022-02-26: Let's talk about SCOTUS nominee Ketanji Brown Jackson.... (<a href="https://youtube.com/watch?v=dkizuImYbrk">watch</a> || <a href="/videos/2022/02/26/Lets_talk_about_SCOTUS_nominee_Ketanji_Brown_Jackson">transcript &amp; editable summary</a>)

Beau provides an insightful overview of Ketanji Brown Jackson's background, qualifications, and potential challenges during her Supreme Court nomination process.

</summary>

"Presidents are not kings."
"There is nothing in her background that suggests she's not capable of interpreting the Constitution."
"She has experience across the board in the legal system."
"Some of them are probably going to come up during the hearings."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces Ketanji Brown Jackson as President Biden's nominee for the Supreme Court.
Mentions her educational background, including going to Harvard and writing a thesis on the plea bargaining system.
Notes her personal connections to the criminal justice system, with an uncle sentenced to life for a non-violent substance crime.
Acknowledges her family ties to law enforcement, which might be a point of contention for Republicans.
Describes her past experiences, including work in journalism, clerkships, and roles in private practice and public defense.
Points out her history as a district court judge in DC and later an appellate court judge.
Talks about some of her notable rulings, including cases involving accommodations for a deaf individual, Trump's impeachment subpoenas, and programs for countering teen pregnancy.
Expresses satisfaction with Judge Jackson's rulings and anticipates some of these decisions being discussed during the confirmation hearings.
Emphasizes that her job is to interpret the Constitution and ensure equal application of the law.
Speculates on potential grandstanding during the confirmation process despite her qualifications.

Actions:

for political enthusiasts, supreme court watchers.,
Watch and stay informed about the confirmation hearings (implied).
Advocate for fair and thorough evaluation of Judge Jackson's qualifications (implied).
</details>
<details>
<summary>
2022-02-25: Let's talk about the NATO Response Force and missing context.... (<a href="https://youtube.com/watch?v=JxnnPQMvqiQ">watch</a> || <a href="/videos/2022/02/25/Lets_talk_about_the_NATO_Response_Force_and_missing_context">transcript &amp; editable summary</a>)

The NATO Response Force activation is a strategic defense measure often sensationalized, requiring nuanced understanding to avoid misinterpretation and anxiety over the situation's actual significance.

</summary>

"This thing didn't even get created until after the Soviet Union collapsed."
"So just before people's anxiety gets a little too high with this news, understand it wasn't activated before because it didn't exist."
"It's defensive. They're not moving them into Ukraine."
"That's not quite the news that it's being made out to be."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The NATO Response Force (NRF) is being activated, sparking sensationalized coverage that may not accurately depict the situation.
Headlines are misleading, implying the NRF activation indicates a heightened threat level similar to historical crises like the Berlin Airlift or Cuban Missile Crisis.
Context reveals that the NRF is a relatively new concept, originating in 2002 and becoming operational in 2004.
The current activation, although significant, is not as unprecedented or dramatic as some media outlets suggest.
The NRF has been previously active in various capacities, including securing elections, aiding during natural disasters, and relief efforts after emergencies.
The activation is defensive in nature, aimed at bolstering countries along the line and not escalating tensions by moving troops into Ukraine.
The NRF's activation signifies readiness to defend against potential further aggression from Putin, with 40,000 troops positioned for NATO defenses.
It's vital to understand the NRF's history and purpose to avoid misinterpreting the current activation's significance amid sensationalized headlines.
The activation does not imply imminent conflict but rather a strategic defensive measure in response to existing geopolitical challenges.
Beau urges for a nuanced understanding of the NRF's activation and its implications to prevent undue anxiety and misperceptions.

Actions:

for global citizens,
Stay informed on international developments and geopolitical contexts (implied)
Advocate for peaceful resolutions to conflicts (implied)
</details>
<details>
<summary>
2022-02-25: Let's talk about other ways to level the field.... (<a href="https://youtube.com/watch?v=QgQFdEHVEv4">watch</a> || <a href="/videos/2022/02/25/Lets_talk_about_other_ways_to_level_the_field">transcript &amp; editable summary</a>)

Beau delves into the likelihood and implications of cyber warfare as a primary option before kinetic actions in future conflicts.

</summary>

"Cyber warfare is more likely and less destructive than kinetic options."
"Disrupting infrastructure aims to pressure civilian populace to influence their government to stop a war."
"Cyber options are viewed as the initial step before considering more dramatic measures."

### AI summary (High error rate! Edit errors on video page)

Delving into future technologies and their implications, specifically cyber warfare against near-peer nations like Russia.
Kinetic options are less likely compared to cyber warfare in targeting opposition's command and control.
The Holy Grail of cyber warfare is the ability to turn off and on the opposition's infrastructure at will, but no country has achieved this yet.
China, Russia, and the United States have the capability to disrupt each other's infrastructure without easy reactivation.
Cyber warfare is seen as more likely and less destructive than strategic or nuclear weapons.
Disrupting infrastructure aims to pressure civilian populace to influence their government to stop a war.
Cyber options are viewed as the initial step before considering more dramatic measures.
The disruptive nature of cyber warfare offers a safer alternative than destructive methods if conflicts escalate.
Beau acknowledges the uncertainty in predicting future events but underscores the probability of cyber warfare as a primary option before kinetic actions.
Cyber attacks can serve as signals for diplomatic talks rather than guaranteed escalations in conflict scenarios.

Actions:

for military analysts, policymakers,
Prepare and strengthen cybersecurity measures to protect critical infrastructure (implied)
Advocate for diplomatic solutions to prevent conflict escalation (implied)
</details>
<details>
<summary>
2022-02-25: Let's talk about orders, tradition, and cops.... (<a href="https://youtube.com/watch?v=L1TAen1DfJY">watch</a> || <a href="/videos/2022/02/25/Lets_talk_about_orders_tradition_and_cops">transcript &amp; editable summary</a>)

Beau explains the dangers of blurring the lines between law enforcement and the military, stressing the importance of differentiating between peace officers and warriors.

</summary>

"Law enforcement is not the same as the military."
"There should never be a point where a comparison between peace officers and the military makes sense."
"They're not the same."

### AI summary (High error rate! Edit errors on video page)

Addresses the topic of orders, tradition, and perception, surprised to still be discussing it in this context.
Explains the difference between military and law enforcement in terms of following orders and accountability.
Talks about the warrior cop mentality and the spread of militaristic culture within law enforcement.
Explains the historical origins of following orders from superiors and the lack of accountability in some cases.
Contrasts military objectives with those of law enforcement, pointing out the need for different systems of accountability.
Mentions the importance of training in the military on disobeying unlawful orders and suggests a similar training for law enforcement.
Criticizes the blurred lines between law enforcement and the military, pointing out the dangers of adopting militaristic traditions in policing.
Emphasizes that law enforcement should not be seen as an occupying army and should not adopt a warrior cop culture without the necessary discipline.
Raises concerns about the misconceptions around following orders in law enforcement and the need for clear boundaries between police and military roles.

Actions:

for law enforcement officials,
Implement training for law enforcement on disobeying unlawful orders (suggested)
Advocate for clear boundaries between law enforcement and military roles (implied)
</details>
<details>
<summary>
2022-02-25: Let's talk about if it would've happened with Trump.... (<a href="https://youtube.com/watch?v=4xXgP8XDG0Y">watch</a> || <a href="/videos/2022/02/25/Lets_talk_about_if_it_would_ve_happened_with_Trump">transcript &amp; editable summary</a>)

Trump's absence allowed Russia's actions in Ukraine, with no NATO response under his presidency.

</summary>

"Trump's absence as president contributed to the ongoing conflict between Ukraine and Russia."
"The first time Russia and Ukraine shot at each other directly was under Trump."
"Under Trump, there wouldn't have been a NATO response to Russia's actions."

### AI summary (High error rate! Edit errors on video page)

Trump's absence as president contributed to the ongoing conflict between Ukraine and Russia.
The Kertch Strait incident in November 2018 marked the first time Russia and Ukraine shot at each other directly.
Trump's inaction during his presidency allowed Russia to act without consequences.
Russia used a program called Passports Dissertation during Trump's presidency to justify its actions in contested areas.
Trump's lack of response to Putin's actions indicates he wouldn't have countered Russia's moves.
Under Trump, there wouldn't have been a NATO response to Russia's actions.
Trump consistently undermined NATO during his presidency.

Actions:

for foreign policy analysts,
Contact foreign policy experts for insights on potential impacts of leadership changes (suggested)
Join organizations advocating for strong international alliances (suggested)
Organize events to raise awareness about the importance of upholding international agreements (suggested)
</details>
<details>
<summary>
2022-02-24: Let's talk about why NATO doesn't want to use troops.... (<a href="https://youtube.com/watch?v=t-vWiX08ZIc">watch</a> || <a href="/videos/2022/02/24/Lets_talk_about_why_NATO_doesn_t_want_to_use_troops">transcript &amp; editable summary</a>)

NATO's reluctance to militarily depose Putin reveals the complex power dynamics and risks involved, shifting focus towards non-kinetic options and strategic arms in foreign policy.

</summary>

"Foreign policy isn't about right and wrong. It's about power."
"They can't depose Putin militarily. The best they can do is create the conditions in hopes that the Russian people or his political allies depose him from within."
"It can only go so far. It doesn't actually create change on the geopolitical scene."
"The possibility of the US doing it through regime change in the way the US typically does it, not so much."
"It's not as simple as dealing with a power that doesn't have strategic arms."

### AI summary (High error rate! Edit errors on video page)

NATO is exploring non-kinetic options, rather than deploying troops, in response to a confusing situation.
The United States has superior military capabilities compared to Russia, but the issue lies in understanding doctrine and what countries are willing to do.
Despite NATO's collective military strength, they are reluctant to militarily depose Putin due to the risk of nuclear war.
Sanctions are being considered as a strategic option, with the United States leveraging relationships with countries like Saudi Arabia to influence oil prices.
Strategic arms play a significant role in shaping foreign policy and can act as a deterrent against larger powers attempting political realignment.
The possession of strategic arms can provide a sense of security for nations on the US hit list, as it deters intervention.
Foreign policy is driven by power dynamics rather than moral considerations, with strategic arms altering the balance of this power game.
Cyber capabilities, particularly in the hands of countries like Russia, are a significant factor in modern conflicts and could lead to disruptions for NATO nations.

Actions:

for policy analysts, activists,
Reach out to policymakers to advocate for diplomatic solutions and support non-kinetic options (suggested).
Stay informed about international relations and geopolitical dynamics to better understand the implications of strategic arms (implied).
</details>
<details>
<summary>
2022-02-24: Let's talk about what happened in Ukraine last night.... (<a href="https://youtube.com/watch?v=jHkIrAKYViA">watch</a> || <a href="/videos/2022/02/24/Lets_talk_about_what_happened_in_Ukraine_last_night">transcript &amp; editable summary</a>)

Beau provides a detailed overview of the recent events in Ukraine, discussing potential scenarios, criticisms, and implications, while addressing questions on civilian losses and NATO's response.

</summary>

"That's how it's going to be said, I'm sure. Remember, that was the plan."
"Generally speaking, there are more civilians lost than military. That's just how it goes. That's war. Wars are bad."
"So, there's your overview. This is what happened while hopefully you got some sleep."

### AI summary (High error rate! Edit errors on video page)

Provides an overview of the recent events in Ukraine, mentioning a shift towards a potential unconventional resistance-style campaign.
Describes the Russian military's swift movement and potential criticisms of the Ukrainian military's lack of resistance.
Raises the possibility of the Ukrainian military's intentional appearance of collapse to transition into an unconventional campaign.
Addresses the unlikely scenario of Russia advancing into NATO countries due to potential repercussions.
Mentions the civilian casualties in the conflict, with varying reports from Russian and Ukrainian sources.
Predicts that NATO's response may involve aid and sanctions rather than direct military involvement.
Clarifies the distinction between NATO's Article 4 and Article 5 in terms of response levels to the conflict.
Suggests that the situation in Ukraine may continue for a protracted period unless it was indeed a military collapse.
Notes the potential involvement of Russia in Moldova due to its small population size.
Concludes with an observation on the ongoing nature of the conflict and the uncertainty surrounding its resolution.

Actions:

for global citizens,
Support organizations providing aid to civilians affected by the conflict (suggested)
Stay informed and advocate for peaceful resolutions to the crisis (implied)
</details>
<details>
<summary>
2022-02-24: Let's talk about nukes.... (<a href="https://youtube.com/watch?v=WUdFGZuiQd8">watch</a> || <a href="/videos/2022/02/24/Lets_talk_about_nukes">transcript &amp; editable summary</a>)

Beau explains the unlikelihood of nuclear weapon use in the conflict between Ukraine, Russia, and NATO, stressing the devastating consequences and historical reluctance towards such actions.

</summary>

"Nobody wins."
"It's not something anybody wants to do."
"Once they're used, there will be a response."
"It isn't incredibly likely."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addressing concerns about the use of nuclear weapons in the current situation in Ukraine and Europe.
Explaining why the use of nuclear weapons is unlikely and not a realistic option in the conflict between Ukraine, Russia, and NATO.
Mentioning the conditions under which the use of nuclear weapons might become a possibility.
Describing the escalation process that could lead to the consideration of nuclear weapons in a conflict.
Emphasizing that nuclear weapons are not the first option due to other available technologies and the risk of massive escalation.
Pointing out the historical reluctance to use nuclear weapons during the Cold War and the devastating consequences of their use.
Advocating against succumbing to constant fear of nuclear annihilation and suggesting a different approach to strategic weapons.

Actions:

for global citizens, policymakers.,
Monitor the situation in Ukraine and Eastern Europe closely to understand the dynamics and potential risks (suggested).
Advocate for diplomatic solutions and de-escalation efforts in international conflicts (implied).
</details>
<details>
<summary>
2022-02-24: Let's talk about Ukraine and Russia's economy.... (<a href="https://youtube.com/watch?v=jc6RhyW3KbM">watch</a> || <a href="/videos/2022/02/24/Lets_talk_about_Ukraine_and_Russia_s_economy">transcript &amp; editable summary</a>)

Beau outlines how Western powers can use sanctions as a tool to destabilize the Russian economy, impacting civilians before achieving their goal.

</summary>

"The odds are that NATO is going to pursue this route because it's probably going to be effective."
"We have to keep in mind that average person over there is going to feel this long before Putin."
"When it comes to wars, I'm always on the same side. The people that don't have anything to do with it."

### AI summary (High error rate! Edit errors on video page)

Talks about the surprise in the Russian economy's instability during the Eastern Europe situation.
Western powers see this as an opening to use sanctions as a tool rather than just a punishment.
The Russian economy heavily relies on extracting and selling goods, with a significant portion sold to Europe.
European powers could severely damage the Russian economy by increasing prices or finding ways to offset costs for citizens.
NATO views the instability in the Russian economy as an opening and might intensify sanctions to destabilize Russia.
The goal of sanctions may shift from punishing Russia to destroying its economy to end the war.
Despite Russia having reserves, the devaluation of the ruble means their money doesn't have the same purchasing power.
Sanctions are seen as a power move by NATO, focusing on destroying the Russian economy.
The impact of sanctions on an average person in a country is felt long before achieving the intended goal.
NATO might deviate from targeting individuals in power and instead opt for broader sanctions due to the Russian economy's instability.

Actions:

for foreign policy analysts,
Advocate for diplomatic solutions to conflicts (implied)
Support organizations aiding civilians affected by conflicts (implied)
</details>
<details>
<summary>
2022-02-23: Let's talk about what students can learn from Spartacus.... (<a href="https://youtube.com/watch?v=cPGQXFZMRI8">watch</a> || <a href="/videos/2022/02/23/Lets_talk_about_what_students_can_learn_from_Spartacus">transcript &amp; editable summary</a>)

An 18-year-old seeks advice on supporting their gay friend facing imminent legislation, invoking Spartacus' solidarity as a shield against forced disclosure, emphasized by Beau's call to stand together.

</summary>

"If everybody's gay, nobody is, right?"
"This legislation, it's horrible. It serves no good."
"If you're in that situation, be Spartacus."
"I have no clue what that [being forced out of the closet] would feel like or how to deal with it."
"It doesn't have to be the whole school. It could just be that person's close friends."

### AI summary (High error rate! Edit errors on video page)

An 18-year-old high school student is concerned about a legislation that might pass before they graduate, affecting their gay friend's coming out process.
The student seeks advice on how to support their friend whose parents are unaware of their friend's sexual orientation.
Beau references the movie Spartacus where individuals stand in solidarity to protect each other from harm.
Expressing solidarity and standing up for friends can provide a sense of cover and support for those who are not ready to come out.
Beau criticizes the harmful legislation pushed by authoritarian figures for political gain.
Encourages being like Spartacus and standing together to protect each other.
Beau acknowledges his lack of experience in dealing with being forced out of the closet but suggests preemptive action by close friends and parents to delay any unwanted disclosures.
The goal is to delay the coming out process until the individual is ready, rather than being forced by external pressures.
Beau encourages viewers to take action to support their friends and navigate challenging situations with compassion and solidarity.

Actions:

for high school students,
Organize a group of friends to stand in solidarity with LGBTQ+ individuals facing challenges (exemplified)
Communicate with parents to create a supportive network for friends in need of protection (exemplified)
Advocate against harmful legislation through community support and unity (exemplified)
</details>
<details>
<summary>
2022-02-23: Let's talk about Biden, Ukraine, and fuel prices.... (<a href="https://youtube.com/watch?v=rS5oSmsC4gc">watch</a> || <a href="/videos/2022/02/23/Lets_talk_about_Biden_Ukraine_and_fuel_prices">transcript &amp; editable summary</a>)



</summary>

"Moving to green energy is a national security priority."
"If you want to make America great, you want to make this country strong and ready for war and all of that stuff, well you got to become an environmentalist."
"This isn't some tree-hugging liberal. This is DOD saying that wars are going to be fought over water."
"We don't have a lot of time on this one."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The conflict in Ukraine will impact fuel supplies and raise prices, as seen historically during conflicts.
Europe heavily depends on Russian-controlled fuel supplies, making self-reliance a national security priority.
The solution lies in transitioning to green energy, as emphasized in the U.S. Army's study on climate change implications.
Green energy is vital for national security, reducing vulnerability during conflicts and mitigating environmental impact.
Shifting to renewable technologies is necessary to address climate change predictions, including wars over water by the Department of Defense.
The urgency to transition to green energy is a national security imperative, especially with limited fuel supplies during conflicts.
Regardless of personal beliefs on climate change, the switch to green energy is vital from a national security perspective.
Political platforms need to integrate green energy initiatives urgently, recognizing the time-sensitive nature of the issue.

Actions:

for politicians, environmentalists, citizens,
Advocate for green energy policies in political platforms (implied)
Raise awareness about the importance of transitioning to renewable technologies (implied)
Support and participate in initiatives that focus on environmental sustainability (implied)
</details>
<details>
<summary>
2022-02-23: Let's talk about Arbery, symbolic victories, and ripple effects.... (<a href="https://youtube.com/watch?v=mrinjoHdRKQ">watch</a> || <a href="/videos/2022/02/23/Lets_talk_about_Arbery_symbolic_victories_and_ripple_effects">transcript &amp; editable summary</a>)

The federal case on the killing of Ahmaud Arbery establishes a precedent for holding individuals accountable for past statements, challenging the idea that prior guilty verdicts make further prosecution irrelevant, with potential far-reaching impacts on online racism.

</summary>

"There will be people who will tone down their rhetoric because they understand that they could have criminal enhancements for any actions later."
"The case serves as a counter to figures in authority who have emboldened racist sentiments by showing that actions have consequences."
"I think they're going to be far-reaching ripple effects from this case."

### AI summary (High error rate! Edit errors on video page)

The federal case on the killing of Ahmaud Arbery was concluded with guilty verdicts for hate crimes, revealing impactful details.
Prosecution used racist statements and social media posts from defendants' past to establish a racist motive for their actions.
This case's significance goes beyond being symbolic as it sets a precedent for holding individuals accountable for past statements.
The case challenges the notion that prior guilty verdicts in state court make federal prosecution irrelevant.
By using evidence unrelated to the event, the case may lead to a reduction in online racism due to potential criminal consequences.
Beau predicts that the right wing may spin the case to create a chilling effect on racist speech online.
Despite already facing life sentences, the federal case's outcome can have significant and tangible impacts on future actions and attitudes.
The case serves as a counter to figures in authority who have emboldened racist sentiments by showing that actions have consequences.
Beau anticipates far-reaching ripple effects from this case, extending beyond its immediate legal outcomes.
The case message is clear: past actions and statements can come back to haunt individuals, potentially deterring future racist behavior.

Actions:

for legal reform advocates,
Monitor and report online racist speech to combat its spread and hold individuals accountable (suggested)
Advocate for legal consequences for hate speech and actions to deter online racism (implied)
</details>
<details>
<summary>
2022-02-22: Let's talk about the list in Ukraine.... (<a href="https://youtube.com/watch?v=nncpNOH4jDo">watch</a> || <a href="/videos/2022/02/22/Lets_talk_about_the_list_in_Ukraine">transcript &amp; editable summary</a>)

Beau breaks down the debate on a list in Ukraine, affirming its existence and questioning its portrayal in media, urging critical understanding in a world of messaging.

</summary>

"Does the list exist? Yes, absolutely. 100% I have not seen the list, but I am 100% certain that it exists."
"If the news had said Russia has a high value target list, nobody would have cared."
"Invasions are bad. Wars are bad. Horrible, horrible stuff happens."

### AI summary (High error rate! Edit errors on video page)

Debate sparked over a list in Ukraine, with opinions forming on its existence and purpose.
The United States claimed Russia has a list of people to arrest or eliminate.
Disbelief in the list's existence stems from distrust in US intelligence and modern disbelief in such actions.
The focus of the debate revolved around the list's existence as the US didn't provide it.
Beau affirms the list's existence, citing it as a normal part of war planning.
Media sensationalized the list's purpose, creating inflammatory narratives.
Lack of information on the list leads to uncertainty about its contents and intentions.
The US hints at abnormal conduct in the list by mentioning the presence of a journalist and an ethnic minority.
The secrecy around the list is maintained to protect those on it and prevent tipping off targets.
The release of the list may only happen when hostilities escalate, revealing its true nature.

Actions:

for concerned global citizens,
Stay informed on international events and messaging (implied)
Question media narratives and look for underlying motives (implied)
Advocate for transparency in government actions (implied)
</details>
<details>
<summary>
2022-02-22: Let's talk about a situation update on Ukraine.... (<a href="https://youtube.com/watch?v=xmQvIMFArlc">watch</a> || <a href="/videos/2022/02/22/Lets_talk_about_a_situation_update_on_Ukraine">transcript &amp; editable summary</a>)

Beau provides insight into escalating tensions between Russia and Ukraine, with Putin's flexible strategies and global responses shaping a potentially volatile situation.

</summary>

"Putin claims Russia is sending peacekeepers to stabilize regions held by Ukrainian proxies."
"Ukrainian president views current actions as a violation of territorial integrity."
"U.S. Embassy relocating to Poland signals anticipation of widespread conflict."
"China's statement urges restraint and diplomatic resolution, maintaining neutrality."
"Putin's strategy appears flexible, possibly aiming to advance as much as possible without direct conflict."

### AI summary (High error rate! Edit errors on video page)

Putin claims Russia is sending peacekeepers to stabilize regions held by Ukrainian proxies.
Russian troops are reportedly in contested areas held by proxies, potentially on the brink of conflict.
Ukrainian president views current actions as a violation of territorial integrity.
NATO's response primarily involves expected sanctions and increased material aid to Ukraine.
U.S. Embassy relocating to Poland signals anticipation of widespread conflict.
China's statement urges restraint and diplomatic resolution, maintaining neutrality.
Putin's potential options range from controlling current proxy-held areas to attempting to take the whole country.
U.S. intelligence suggests Putin's ultimate goal may be to take over Ukraine entirely.
Putin's strategy appears flexible, possibly aiming to advance as much as possible without direct conflict.
Uncertainty remains on Putin's definitive course of action amidst escalating tensions.

Actions:

for global observers,
Monitor developments and stay informed on the situation (implied)
Advocate for peaceful resolutions and diplomatic efforts (implied)
</details>
<details>
<summary>
2022-02-22: Let's talk about Republicans in Michigan and the Constitution.... (<a href="https://youtube.com/watch?v=JGuUwel7Y5c">watch</a> || <a href="/videos/2022/02/22/Lets_talk_about_Republicans_in_Michigan_and_the_Constitution">transcript &amp; editable summary</a>)

Three Michigan Attorney General candidates opposing privacy rights reveal a deeper agenda of control and undermining fundamental freedoms.

</summary>

"Republicans don't care about rights or freedom or the Constitution."
"They want things to stay the way they are."
"If you are concerned by the undermining of privacy in this country, this is something you need to pay attention to."
"They want the state to have the right to determine what goes on inside your bedroom."
"The states want to be able to exercise control over you in every facet."

### AI summary (High error rate! Edit errors on video page)

Three Republican candidates for Attorney General in Michigan were asked about Griswold v. Connecticut, known as the birth control case for striking down a law prohibiting married couples from obtaining birth control.
The Supreme Court didn't establish a right to birth control in Griswold v. Connecticut but found a right to marital privacy based on the first, third, fourth, and fifth amendments.
The court's decision on marital privacy was rooted in previous cases like Meyer v. Nebraska and Pierce v. Society of Sisters, which established parental rights.
Republicans who oppose the right to marital privacy are ultimately seeking to control what happens in people's bedrooms as a way to control their lives.
By opposing the right to marital privacy, the Republican candidates for Attorney General in Michigan are going against fundamental rights protected by the Constitution.
Republicans' focus on undermining privacy rights is driven by their desire for control and maintaining the status quo.
Their arguments about states' rights are essentially about giving states the power to restrict freedoms rather than expand them.
The push against privacy rights is not just about birth control but about the broader idea of government interference in personal matters.
Republicans' efforts against privacy rights are seen as a strategy to pave the way for anti-LGBTQ legislation and further control over individuals' lives.
The candidates' stance against privacy rights is a deliberate move that could lead to increased state control over individuals in all aspects of life.

Actions:

for michigan voters,
Pay attention to candidates' stances on privacy rights and fundamental freedoms (suggested).
Advocate for the protection of privacy rights and constitutional freedoms in political discourse (implied).
</details>
<details>
<summary>
2022-02-21: Let's talk about Republicans missing demographic info.... (<a href="https://youtube.com/watch?v=Iz9x5Ste43k">watch</a> || <a href="/videos/2022/02/21/Lets_talk_about_Republicans_missing_demographic_info">transcript &amp; editable summary</a>)

Beau explains the impact of demographic shifts on societal change and political outcomes, warning the Republican Party of potential backlash and urging the Democratic Party to deliver policies that resonate with key demographics.

</summary>

"Once it hits that one in five number, oh, it's over. It is over."
"We have hit that number."
"One out of five, that is a significant percentage."
"There's no way for Republicans to win that vote."
"You have to deliver for these demographics that are part of that coalition."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of societal change and a magic number in relation to generational changes.
Points out the historical shift in attitudes towards interracial marriage over the years.
Emphasizes the importance of changing people's views through culture to drive legislative change.
Notes the increasing acceptance and approval of interracial marriages in American society.
Draws parallels between the acceptance threshold of one in five and cultural shifts leading to legal changes.
Provides statistics on the percentage of different generations identifying as LGBTQ.
Raises concerns about the Republican Party's anti-LGBTQ legislation alienating a significant percentage of new voters.
Stresses the potential impact of this demographic shift on future elections.
Warns that the Republican Party's socially conservative stance may backfire due to changing demographics.
Calls on the Democratic Party to deliver policies that resonate with key demographics instead of relying solely on anti-Republican sentiment.

Actions:

for voters, political activists,
Mobilize young voters to participate in elections by engaging with them and addressing their concerns (implied).
Advocate for policies that represent and support diverse demographics within the community (implied).
</details>
<details>
<summary>
2022-02-21: Let's talk about Putin's options.... (<a href="https://youtube.com/watch?v=iYqeWf3F-qs">watch</a> || <a href="/videos/2022/02/21/Lets_talk_about_Putin_s_options">transcript &amp; editable summary</a>)

Western media noticed Putin's recorded meeting with advisors suggesting recognizing breakaway regions in Ukraine, potentially shaping Russia's foreign policy.

</summary>

"Putin may believe recognizing the republics and not invading Ukraine could be his off-ramp."
"Buffer zones may not matter in modern warfare, but it's still a talking point for Russian state security."
"Uncertainty surrounds Putin's next move, with even his advisors possibly unaware of his plan."

### AI summary (High error rate! Edit errors on video page)

Western media noticed Putin's meeting was recorded before broadcast, but Russians didn't confirm.
Putin's meeting with advisors portrayed him as seeking counsel, where advisors suggested recognizing breakaway regions in Ukraine.
Putin likely made decision before meeting, with video announcing decision possibly already recorded.
Putin may recognize contested areas as independent countries, potentially justifying Russian military presence there.
Putin's strategy could involve creating buffer countries near borders by recognizing breakaway regions.
Recognizing breakaway regions could become part of Russia's foreign policy to create buffer zones.
Buffer zones may not be as strategically relevant in modern warfare but still a talking point for Russian state security.
Options discussed include Putin recognizing breakaway regions and either protecting them or using provocation to invade Ukraine.
Another option is Putin sitting tight and hoping Ukraine doesn't take action against the breakaway regions.
Uncertainty surrounds Putin's next move, with even his advisors possibly unaware of his plan.

Actions:

for foreign policy analysts,
Monitor developments in Russia's foreign policy regarding recognizing breakaway regions. (implied)
Stay informed about potential conflicts in Ukraine and surrounding regions. (implied)
Advocate for diplomatic solutions to regional tensions. (implied)
</details>
<details>
<summary>
2022-02-20: Let's talk about UAVs and Ukraine.... (<a href="https://youtube.com/watch?v=_pE4Atp15gQ">watch</a> || <a href="/videos/2022/02/20/Lets_talk_about_UAVs_and_Ukraine">transcript &amp; editable summary</a>)

Beau expresses concerns about the implications of using unmanned aerial vehicles in military operations and questions the shifting dynamics of warfare with advanced technologies.

</summary>

"In a resistance campaign like this, intelligence, knowing what's going on, the intent of the opposition, where they're going to be, all of this stuff is just invaluable."
"Where is that line? And are we going to allow the advanced technologies that are coming online when it comes to this type of stuff?"
"It's been a worry of mine since all this stuff really started first coming online."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Expresses concern about the use of unmanned aerial vehicles and how policymakers may perceive them differently from ground troops.
Raises questions about the risk and implications of using unmanned aircraft in military operations.
Points out the importance of intelligence and information gathering in a resistance effort.
Considers the potential for advanced technologies to influence policymakers' decisions on armed confrontation.
Expresses fear and concern about conflicts shifting towards unmanned equipment.
Leaves open-ended questions about the Biden administration's stance on these issues.
Emphasizes the evolving nature of warfare due to technological advancements.
Conveys worries about the future of conflicts involving unmanned equipment.
Contemplates the political and ethical considerations surrounding unmanned aircraft in war scenarios.
Wraps up by offering these thoughts as a point of reflection for the audience.

Actions:

for military policymakers,
Stay informed about technological advancements in military equipment and their potential implications for conflict (implied).
Advocate for transparent and ethical decision-making processes regarding the use of unmanned aerial vehicles in warfare (implied).
</details>
<details>
<summary>
2022-02-20: Let's talk about Cruz, Hawley, and Nina Morrison.... (<a href="https://youtube.com/watch?v=VWKO6BtsXmI">watch</a> || <a href="/videos/2022/02/20/Lets_talk_about_Cruz_Hawley_and_Nina_Morrison">transcript &amp; editable summary</a>)

Beau sheds light on Biden nominee Nina Morrison’s dedication to justice and exposes Republican obstructionist tactics in opposing her appointment without valid grounds.

</summary>

"A person who is truly committed to justice rather than just the legal system."
"What you have is normal obstructionist tactics from Republicans in an attempt to manufacture opposition to somebody that there's no real grounds to oppose."

### AI summary (High error rate! Edit errors on video page)

Beau introduces Nina Morrison, a Biden nominee for a lifetime appointment in a US district court, with qualifications from her work with the Innocence Project.
Morrison has freed 30 innocent people, including those on death row, showing a commitment to justice beyond the legal system.
Senators Cruz and Hawley criticize Morrison in unique ways: Cruz links her to rising crime rates in Philadelphia due to advising on conviction integrity, while Hawley ties her to 2020 protests based on an op-ed praising an innocent man's release.
Beau points out the irony in Cruz's argument, noting that freeing innocent people actually helps reduce crime rates.
Hawley's attempt to connect Morrison to the 2020 protests seems baseless, especially considering his own controversial actions during the Capitol riots.
Morrison is highly qualified for the position and dedicated to ensuring justice prevails, despite the obstructionist tactics used by Republicans to oppose her nomination.
Beau criticizes the Republican tactics as a way to appease their inflamed voter base and manufacture opposition without valid grounds.
Beau underscores the importance of supporting nominees like Morrison who prioritize justice and work to prevent innocent people from suffering.
Beau reveals his support for the Innocence Project and provides a link for donations, encouraging viewers to contribute to the cause.

Actions:

for justice advocates,
Support the Innocence Project by donating (suggested)
</details>
<details>
<summary>
2022-02-19: Let's talk about unionizing the military and Texas.... (<a href="https://youtube.com/watch?v=oUUQrMaA_Ro">watch</a> || <a href="/videos/2022/02/19/Lets_talk_about_unionizing_the_military_and_Texas">transcript &amp; editable summary</a>)

Troops in Texas face challenges organizing under an existing union due to ongoing mission issues, raising questions about support from politicians.

</summary>

"Troops can unionize when they are on state active duty."
"Operation Lone Star has been plagued by complaints about living conditions, pay, leadership, mission scope, equipment, pretty much everything."
"It's probably a really good idea for the governor to cancel this little stunt before they get representation."
"What's gonna happen when the people of Texas have to decide whether to support the troops or support the politicians who couldn't even keep their lights on."

### AI summary (High error rate! Edit errors on video page)

Explains the exception to federal troops unionizing and how troops can unionize when on state active duty.
Mentions the hypothetical scenario of National Guard troops in Texas being called up for a political mission called Operation Lone Star.
Notes that Operation Lone Star has faced complaints about various issues like living conditions, pay, leadership, and equipment.
Points out that the Texas State Employees Union Military Caucus is set to meet next week, providing an avenue for National Guard troops to organize.
Describes the ongoing disaster of Operation Lone Star and how the Texas military department has claimed helplessness in addressing the issues.
Suggests that with the troops potentially gaining representation through the union, it might be wise for the governor to cancel the mission to avoid further repercussions.
Mentions that those involved in organizing the union are maintaining a low profile and speaking anonymously to the press due to fears of retaliation from the Texas Military Department.
Raises the question of whether the people of Texas will support the troops or the politicians who have failed to address the situation adequately.

Actions:

for texans, national guard members,
Attend and support the Texas State Employees Union Military Caucus meeting next week (exemplified).
Advocate for better living conditions, pay, leadership, and equipment for National Guard troops (exemplified).
Call for transparency and accountability from the Texas Military Department (exemplified).
</details>
<details>
<summary>
2022-02-19: Let's talk about manufacturing pretexts.... (<a href="https://youtube.com/watch?v=X42ClNHmIaY">watch</a> || <a href="/videos/2022/02/19/Lets_talk_about_manufacturing_pretexts">transcript &amp; editable summary</a>)

Americans believe in inaccurate pretexts for war; manufacturing them is not unique to the U.S., and foreign policy is about power, not morality.

</summary>

"Foreign policy is not about good guys and bad guys."
"Just because one side is engaging in expansionist behavior doesn't mean the other side isn't."
"Sometimes there aren't any good guys. Sometimes it's just a bunch of people with a huge appetite for destruction."

### AI summary (High error rate! Edit errors on video page)

Americans believe in inaccurate pretexts for war.
The manufacturing of pretexts for war is not exclusive to the Iraq incident.
Throughout American history, the U.S. has manufactured pretexts for war or drawn up plans to do so.
The plot of The Princess Bride involves manufacturing a pretext for war.
Other countries have engaged in manufacturing pretexts for war throughout history.
Examples include King Gustav III in 1788, Japan in the Mukden Incident of 1931, Germany in 1939, and Russia in the Russo-Finnish War in 1939.
Foreign policy is not about good guys and bad guys but about power and positioning.
Encouraging the populace to support war involves manufacturing reasons for it.
The belief that only the U.S. engages in manufacturing pretexts for war is untrue; other countries have similar examples.
Understanding foreign policy requires recognizing that it is about power and positioning, not morality or good vs. bad.

Actions:

for foreign policy analysts,
Research historical examples of manufactured pretexts for war (suggested)
Educate others on the realities of foreign policy and manufacturing pretexts (exemplified)
</details>
<details>
<summary>
2022-02-19: Let's talk about 54 cops in California.... (<a href="https://youtube.com/watch?v=7L_kaI3T9ZQ">watch</a> || <a href="/videos/2022/02/19/Lets_talk_about_54_cops_in_California">transcript &amp; editable summary</a>)

Prosecutors uncover 54 cops in California Highway Patrol for various misconduct, prompting Beau to call for a reevaluation of policing practices and funding allocation.

</summary>

"A lot of bad apples."
"It's probably time, past time, to rethink policing in this country."
"This is a moment for law enforcement to change."
"Reallocating some of the funding."
"A whole lot of bad apples to be alleged."

### AI summary (High error rate! Edit errors on video page)

Prosecutors uncovered a group of bad apples within the California Highway Patrol, dating back to 2016-2018.
Allegations include an overtime scheme, falsifying work hours, issuing fake tickets, and involvement in a bribery scheme.
54 cops are set to be arraigned on March 17th and 18th for a total of 302 counts.
The public's trust in law enforcement is eroding due to discrepancies between reports and cell phone footage.
Beau calls for a shift towards consent-based policing and reallocating funds to better-trained professionals.
He suggests establishing a more robust oversight system to ensure accountability for past actions.

Actions:

for law enforcement reform advocates,
Advocate for consent-based policing and reallocation of funds to better-trained professionals (suggested)
Support the establishment of a more robust oversight system for accountability (suggested)
</details>
<details>
<summary>
2022-02-18: Let's talk about failed states and the US.... (<a href="https://youtube.com/watch?v=hUqIgH64PaQ">watch</a> || <a href="/videos/2022/02/18/Lets_talk_about_failed_states_and_the_US">transcript &amp; editable summary</a>)

The United States is showing signs of becoming a failed state through the erosion of key elements, driven by political agendas.

</summary>

"The government is giving away its monopoly on violence."
"A lot of the Republican agenda you're going to find out is guaranteed to destroy [the republic]."
"The United States is definitely on that path."

### AI summary (High error rate! Edit errors on video page)

Explains the elements of a failed state: loss of control of territory, erosion of ability to make collective decisions, inability to provide public services, and inability to interact with other nations.
Points out that losing the monopoly on violence is a significant warning sign of becoming a failed state.
Notes that in the United States, the government is willingly giving away its monopoly on violence for political gains.
Mentions how the government is encouraging non-state actors to enforce punishment, which is detrimental to preserving the republic.
Talks about the erosion of the ability to make collective decisions, citing the active attempts to cast doubt on election results.
References Texas as an example of the inability to provide public services.
States that the United States can still interact with other nations due to its powerful military.
Warns that political failures causing misuse of the military could lead other nations to avoid interacting with the US.
Concludes that while the US is not currently a failed state, elements of becoming one are actively occurring due to certain political agendas.

Actions:

for citizens, policymakers, activists,
Monitor political actions and hold elected officials accountable for decisions that could lead to the erosion of democratic principles (implied).
Advocate for policies that strengthen public services and ensure equal access for all citizens (implied).
Support organizations working to uphold democratic values and principles in the face of political challenges (implied).
</details>
<details>
<summary>
2022-02-18: Let's talk about Ukraine warming up.... (<a href="https://youtube.com/watch?v=XDUhZ_pnR3U">watch</a> || <a href="/videos/2022/02/18/Lets_talk_about_Ukraine_warming_up">transcript &amp; editable summary</a>)

Beau advises potential conflict zone residents to take a break, warns of manufactured pretexts for war, and analyzes Ukraine's defensive strategies against Russia, while clarifying US involvement and public opinion.

</summary>

"When surrounded, attack is a good strategy. However, Ukraine can't move."
"The Ukrainian military cannot go toe-to-toe with the Russian military. It will lose."
"It is incredibly unlikely that the Ukrainian military would be able to defeat an advance militarily."

### AI summary (High error rate! Edit errors on video page)

Urges roughly 3,100 people in the impacted area to take a long weekend away from likely conflict zones in Ukraine.
Warns about Russia potentially manufacturing a pretext for military action in Ukraine.
Explains why Ukraine wouldn't go on the offensive against Russian forces.
Indicates Ukraine's strategy of resistance and the challenges they face against the Russian military.
Mentions the importance of training civilians for potential conflict and the quality of their preparedness.
States that the US is currently only providing aid, logistics, and intelligence support in the Ukraine situation.
Remarks on public opinion polls regarding Biden's handling of the Ukraine situation.
Clarifies the misinformation around Ukrainian military plans for resistance.
Emphasizes the uncertainty of conflict in Ukraine and the possibility of diplomatic solutions.
Conveys the likelihood of Russia moving forward with military action in Ukraine.

Actions:

for concerned citizens,
Take a long weekend away from potential conflict zones in Ukraine (implied)
Stay informed about the situation in Ukraine and be prepared to react accordingly (suggested)
</details>
<details>
<summary>
2022-02-18: Let's talk about 19 cops in Austin, Texas.... (<a href="https://youtube.com/watch?v=4-T7hx-KLCM">watch</a> || <a href="/videos/2022/02/18/Lets_talk_about_19_cops_in_Austin_Texas">transcript &amp; editable summary</a>)

Events in Austin, Texas, leading to 19 officers' indictment reveal shifting public sentiment towards holding law enforcement accountable amidst increased scrutiny and demand for transparency.

</summary>

"Public isn't giving law enforcement the benefit of the doubt anymore because everybody has cell phone cameras."
"For many law enforcement agencies in the United States, the benefit of the doubt, that's kind of stopped."
"The public is beginning to understand that what's on those cameras, that doesn't always match what goes in the report."

### AI summary (High error rate! Edit errors on video page)

Austin, Texas and events from 2020 are under scrutiny, leading to a DA's election.
19 Austin officers have been indicted for aggravated assault with a deadly weapon.
The indictments are linked to the use of beanbags during the Floyd demonstrations.
Public dissatisfaction with the police response during the demonstrations led to the chief of police stepping down.
Multimillion-dollar settlements have been made related to cases involving beanbag use.
Details on the officers and indictments are sparse due to Texas law keeping cop indictments secret.
Austin Police Association President confirmed charges against the officers but lacked details.
District Attorney Garza, who campaigned on indicting police officers, is prosecuting these cases.
Garza's confidence in no officer being convicted raises questions about prior experience or violations.
Public sentiment in Austin seems to be shifting towards holding law enforcement accountable.
The public's growing skepticism towards law enforcement is attributed to increased visibility through cell phone cameras.
Public elected a DA who promised to indict cops, reflecting a desire for change in accountability.
Beau suggests that the public's attitude towards law enforcement in Austin has shifted significantly.
The public's loss of faith in law enforcement's benefit of the doubt is connected to increased video evidence discrepancies.
Beau concludes by prompting reflection on the changing dynamics in Austin regarding law enforcement accountability.

Actions:

for texans, activists, community members,
Contact local community organizers to support accountability measures for law enforcement (suggested)
Attend or organize community forums to address concerns and push for transparency in policing (implied)
</details>
<details>
<summary>
2022-02-17: Let's talk about sea levels.... (<a href="https://youtube.com/watch?v=kH4n-CWf-gU">watch</a> || <a href="/videos/2022/02/17/Lets_talk_about_sea_levels">transcript &amp; editable summary</a>)

A call to action on rising sea levels and climate change impacts, urging immediate attention and political action.

</summary>

"Sea level rise is upon us."
"Once it starts rolling, you can't just stop it when you decide it gets too bad."
"We are seeing the impacts of climate change today, now."
"If we wait for them to get unbearable [...] it's gonna be too late to really stop it."
"This is something that needs to be a campaign issue in the midterms."

### AI summary (High error rate! Edit errors on video page)

Outlines a new report on sea levels released by six US government agencies.
Notes a concerning trend of sea level rise being greater than previously thought.
Mentions the expected increase in sea levels over the next 30 years, surpassing the rise seen in the entire 20th century.
Details the average rise of sea levels around the US as 10 to 12 inches, with some areas facing up to 18 inches.
Projects even higher sea level rises when looking beyond 30 years, particularly by 2060.
Points out specific areas like Galveston and St. Petersburg facing substantial rises in sea levels.
Emphasizes the impact of rising sea levels on buildings, homes, and commerce, especially when combined with storm surges.
Quotes a National Ocean Service representative stating that sea level rise is already happening.
Urges for immediate action to address climate change and its consequences.
Advocates for making climate change a significant campaign issue in upcoming elections.

Actions:

for climate advocates, voters,
Contact local representatives to prioritize climate change in upcoming elections (implied)
</details>
<details>
<summary>
2022-02-17: Let's talk about a sign about the elections from Colorado.... (<a href="https://youtube.com/watch?v=bEfrXuaIkqE">watch</a> || <a href="/videos/2022/02/17/Lets_talk_about_a_sign_about_the_elections_from_Colorado">transcript &amp; editable summary</a>)

Tina Peters' candidacy for Secretary of State of Colorado raises concerns about election security and the spread of falsehoods, echoing a broader trend of Trump loyalists vying for election control positions nationwide.

</summary>

"Colorado needs a Secretary of State to be the Secretary of State."
"Making sure that Trump loyalists do not control the elections at the state level is an imperative."
"Preventing Trump loyalists from controlling elections is key to defeating Trumpism."

### AI summary (High error rate! Edit errors on video page)

Tina Peters, the Mesa County clerk in Colorado, known for echoing Trump's false election claims, is under investigation for alleged election security breaches.
A judge barred Tina Peters from overseeing the election in her county and she was recently arrested on an obstruction charge.
Tina Peters announced her candidacy for Secretary of State of Colorado, potentially putting the entire state's election under her control if she wins.
This situation is not unique; similar instances are happening in other states where Trump loyalists are running for election control positions.
The current Secretary of State of Colorado, Jenna Griswold, labeled Tina Peters as unfit for the role and a danger to Colorado elections.
Jenna Griswold emphasized that Colorado needs a Secretary of State who upholds the will of the people and doesn't embrace conspiracies.
Trump loyalists targeting Secretary of State positions in swing states is part of a strategy to control elections.
It's vital for all citizens, especially in swing states, to pay attention to and prevent Trump loyalists from controlling state-level elections.
Preventing Trump loyalists from controlling elections is key to defeating Trumpism, as altering outcomes through means other than the ballot may be attempted.
Being vigilant and active in preventing Trump loyalists from gaining election control is necessary to safeguard democratic processes.

Actions:

for voters, activists,
Contact local election officials to raise concerns about election security and transparency (implied)
Support candidates who prioritize upholding democratic processes and rejecting conspiracy theories (implied)
Stay informed about candidates running for election control positions in your state and their stances on election integrity (implied)
</details>
<details>
<summary>
2022-02-17: Let's talk about San Francisco, DNA, and the rest of the country.... (<a href="https://youtube.com/watch?v=XcsA2q37p7s">watch</a> || <a href="/videos/2022/02/17/Lets_talk_about_San_Francisco_DNA_and_the_rest_of_the_country">transcript &amp; editable summary</a>)

Beau raises concerns about law enforcement using DNA evidence from sexual assault kits against victims, leading to chilling effects and ethical dilemmas, with potential broader societal implications.

</summary>

"Why would they not use it?"
"It seems to be prioritizing information collection over solving an incredibly serious crime."
"We're going to have to figure out where the lines are, how long information is going to be kept."
"How free of a society can it really be?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Raises concerns about the use of DNA evidence obtained from sexual assault kits against victims years later.
Points out the chilling effect this practice may have on victims coming forward to provide evidence.
Questions law enforcement's prioritization of information collection over solving serious crimes.
Argues that deterring victims from providing evidence through DNA kits may lead to perpetrators committing more crimes.
Calls attention to the ethical implications of using victims' DNA against them without their consent.
Expresses worry that this issue in San Francisco may be happening statewide and potentially nationwide.
Suggests that widespread knowledge of this practice could further discourage victims from seeking justice.
Raises broader societal questions about the collection and storage of personal information by authorities.
Contemplates the implications of a society where the government has total information awareness.
Urges reflection on the impact of such practices on freedom and privacy.

Actions:

for advocates for victims' rights,
Advocate for clear guidelines and consent procedures in the use of DNA evidence from sexual assault kits (implied).
Support organizations working to protect victims' rights in criminal investigations (suggested).
</details>
<details>
<summary>
2022-02-16: Let's talk about why countries don't follow the manual.... (<a href="https://youtube.com/watch?v=uy6U_kkHWLQ">watch</a> || <a href="/videos/2022/02/16/Lets_talk_about_why_countries_don_t_follow_the_manual">transcript &amp; editable summary</a>)

Beau explains why countries deviate from manuals in dealing with civil disturbances, citing race and aggressive tendencies as key influencers, ultimately stressing the importance of endurance over brute force.

</summary>

"It isn't the group that can dish out the most that wins. It's the group that can take the most."
"There are literal manuals about this on civil disturbance, counterinsurgency, stuff like that."
"Race always has something to do with it."
"In Western countries, in the United States especially, there is the idea that to win, you have to be aggressive."
"Most of the major demonstrations you are aware of, that you know of, you know about them because of an overreaction by state forces."

### AI summary (High error rate! Edit errors on video page)

Explains why countries sometimes deviate from the manual in dealing with civil disturbances and demonstrations.
Mentions examples of situations where following or not following the manual leads to expected outcomes.
Addresses a viewer's question about why heavy-handed methods are sometimes used against movements that threaten the status quo.
Talks about the existence of literal manuals on civil disturbance and counterinsurgency.
Points out that race plays a significant role in how different groups are treated by governments.
Mentions the influence of public health experts and advisors in policymaking.
Criticizes the aggressive approach often depicted in movies and how it influences decision-making.
Notes the disconnect between expert advice and political actions, especially in Western countries.
Emphasizes that brute force is not always effective in dealing with civil disturbances.
Stresses the importance of understanding that the ability to endure, not to overpower, is key in such situations.

Actions:

for policy influencers, activists,
Organize peaceful demonstrations to raise awareness and push for policy changes (suggested)
Support marginalized groups facing unequal treatment by advocating for fair and equal enforcement of laws (implied)
</details>
<details>
<summary>
2022-02-16: Let's talk about truckers in America.... (<a href="https://youtube.com/watch?v=rNc9chSqRsg">watch</a> || <a href="/videos/2022/02/16/Lets_talk_about_truckers_in_America">transcript &amp; editable summary</a>)

Organizers and pundits support a US trucker convoy, but skepticism exists around its success and potential risks from certain beliefs.

</summary>

"It does appear that this might run out of gas before it gets anywhere."
"While small, it does appear that this is a group that might have a lot of capacity for doing bad things."
"There is skepticism about the level of support for the convoy."
"This is just all bad."
"At this moment, participation seems limited, that could still change."

### AI summary (High error rate! Edit errors on video page)

Organizers and right-wing pundits are supporting the idea of establishing a trucker convoy in the U.S.
The organization behind the convoy, People's Convoy, is the only group with any real organizational capacity.
The convoy was initially planned to start in March but has been moved up to the end of February due to lack of support.
There is skepticism about the level of support for the convoy, especially in terms of actual trucker involvement.
The convoy is planned to start in Barstow, but the choice of location raises questions about their public relations strategy.
A large portion of the supporters of this convoy are linked to a belief system associated with negative events.
Despite the small number of participants, there is concern about the potential for bad actions due to the devout nature of the supporters.
The group supporting the convoy might be erratic, with plans like stopping at Tyler Perry's house, linked to their belief system.
While the convoy may not gain momentum currently, there is a possibility of right-wing pundits pushing for its success in the future.
Participation in the convoy is currently limited, but that could change due to external influences.

Actions:

for online activists,
Monitor the developments around the proposed trucker convoy (suggested)
Stay informed about any potential risks associated with the convoy (suggested)
Advocate for responsible and peaceful actions within communities (implied)
</details>
<details>
<summary>
2022-02-16: Let's talk about Russia reducing forces near Ukraine.... (<a href="https://youtube.com/watch?v=4UhG3aPJgJE">watch</a> || <a href="/videos/2022/02/16/Lets_talk_about_Russia_reducing_forces_near_Ukraine">transcript &amp; editable summary</a>)

Russia's troop reduction near Ukraine signals hope for de-escalation, but uncertainty looms over whether it's genuine or mere posturing in the ongoing conflict.

</summary>

"This is the front line."
"Even if this does completely de-escalate at this point, just like nine months ago, 10 months ago, this is the front line."
"We don't know if this is a real de-escalation or just posturing."
"Verification is needed to confirm the extent of troop withdrawal."
"This is where tensions are going to continue rising and falling."

### AI summary (High error rate! Edit errors on video page)

Russia is reportedly starting to remove some troops from the Ukrainian border, a positive development.
Beau speculates that Putin's goal may be to take Ukraine without engaging in combat.
The purpose of troop buildups could be to intimidate Ukraine into siding with Russia by showing NATO's lack of support.
There's uncertainty about whether this troop reduction indicates a total de-escalation, as Western sources have not confirmed it.
A false withdrawal to create a false sense of security is a possibility, though it may not be a wise move given today's global climate.
Even if tensions de-escalate now, the Ukraine-Russia conflict is far from over and may lead to future flare-ups.
Ukraine's fate could involve joining NATO, siding with Russia, or becoming a political no man's land.
Resolving the Ukraine-Russia tensions will be an ongoing process, regardless of temporary de-escalations.
Russia may prefer a non-violent takeover of Ukraine, but it's uncertain if that's how events will unfold.
Verification is needed to confirm the extent of troop withdrawal and whether it signifies a genuine de-escalation or merely posturing.
Historical context from the Cold War era is valuable in understanding the gravity of the situation.
The Ukraine-Russia border is likened to Germany during the Cold War, indicating persistent tensions until a resolution is reached.

Actions:

for world citizens,
Monitor international news updates on the Ukraine-Russia situation for accurate information and developments (suggested).
Advocate for diplomatic solutions and peaceful resolutions to the Ukraine-Russia conflict (implied).
</details>
<details>
<summary>
2022-02-15: Let's talk about the Superbowl, Star Trek and timelessness.... (<a href="https://youtube.com/watch?v=Q05SSXgSoJU">watch</a> || <a href="/videos/2022/02/15/Lets_talk_about_the_Superbowl_Star_Trek_and_timelessness">transcript &amp; editable summary</a>)

Analyzing timelessness in TV shows through societal evolution and commentary, using Star Trek as a prime example of ahead-of-its-time social reflection.

</summary>

"Society changes over the course of your life."
"Timeless shows typically give us the answer."
"In the course of your life, society has changed that much."
"Those timeless shows, those shows that you can watch them thirty years later."
"It's probably worth noting that if society did it, the individuals who make up that society, they did too."

### AI summary (High error rate! Edit errors on video page)

Analyzing the Super Bowl halftime show and TV shows in terms of timelessness and societal changes.
Mentioning the importance of shows that stand the test of time versus those that become problematic.
Exploring how society evolves over time, leading to shifts in perspectives on past content.
Noting how certain timeless shows provide commentary on current issues, remaining relevant even decades later.
Using Star Trek as an example of a show ahead of its time in addressing social issues.
Pointing out subtle and overt social commentary within Star Trek episodes.
Describing an episode in Star Trek that mirrors the Dred Scott case, tackling the concept of android rights.
Referencing a character in Star Trek Deep Space Nine, Jadzia Dax, and discussing themes of identity acceptance and deadnaming.
Emphasizing the role of timeless shows in offering answers to societal changes and individual growth.
Encouraging reflection on how both society and individuals evolve over time.

Actions:

for tv show enthusiasts,
Watch timeless TV shows that offer social commentary (implied).
Analyze and appreciate the social issues addressed in classic TV series (implied).
</details>
<details>
<summary>
2022-02-15: Let's talk about arbitration, women, and labor.... (<a href="https://youtube.com/watch?v=X-jxqy_aJeI">watch</a> || <a href="/videos/2022/02/15/Lets_talk_about_arbitration_women_and_labor">transcript &amp; editable summary</a>)

Beau explains the impact of impending labor legislation banning arbitration in cases of sexual harassment, providing vital protection for victims.

</summary>

"This legislation says those clauses, well, they can't be used anymore."
"This is going to offer a wider level of protection, particularly for women who are working for rich and powerful men who think they can get away with anything."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of labor legislation related to arbitration heading to President Biden's desk.
Mentions the common inclusion of arbitration clauses in new hire packets.
Describes arbitration as a private means of settling disputes outside the judicial system.
Notes industries where arbitration makes sense, like those involving trade secrets or intellectual property.
Points out that arbitration clauses are not suitable for disputes related to sexual harassment or assault.
Emphasizes how the new legislation renders these clauses unenforceable, particularly benefiting victims of harassment.
Acknowledges the long process behind this legislation, dating back to around 2017 or 2018.
Comments on the importance of bringing accountability to those who previously avoided it through arbitration.
Stresses that the new legislation will provide increased protection, especially for women working under powerful individuals.
Concludes by wishing everyone a good day.

Actions:

for employees, advocates, activists,
Contact organizations supporting victims of sexual harassment to understand how this legislation can benefit them (suggested)
Join advocacy groups pushing for similar legislative changes in other areas (implied)
</details>
<details>
<summary>
2022-02-15: Let's talk about McConnell and Trump going to war.... (<a href="https://youtube.com/watch?v=OEKVRPAgQ5M">watch</a> || <a href="/videos/2022/02/15/Lets_talk_about_McConnell_and_Trump_going_to_war">transcript &amp; editable summary</a>)

Beau outlines the Republican Party's internal struggle, driven by McConnell's moves against Trumpism, with a focus on self-serving interests over party dynamics.

</summary>

"McConnell does what's best for McConnell."
"He is not going to go against the big funders."
"Mitch McConnell has joined the resistance, but my guess is he's doing it for very, very self-serving reasons."
"Don't turn him into a hero if he is successful."
"Trump is bad for business."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of the current situation in the Republican Party, focusing on the Trump McConnell power struggle.
McConnell's active recruitment of candidates to run against those endorsed by Trump, indicating an all-out war within the GOP.
Emphasizing McConnell's consistent prioritization of his own interests over party dynamics.
Mentioning that McConnell's moves are likely supported by the big funders in the GOP.
Pointing out that McConnell's actions do not necessarily stem from a concern for representative democracy but rather from self-serving motives.
Warning against viewing McConnell as a hero if he is successful in countering Trump.
Speculating that the establishment Republicans might aim to bring the party back to a George Bush Jr. style of politics.
Noting that Trump's erratic nature and lack of a clear plan have complicated lobbying and fundraising within the Capitol Hill.
Suggesting that the establishment Republicans are likely aiming to return to a more conventional style of politics.
Implying that the move against Trumpism within the GOP may be driven by a desire to resume business as usual without Trump's disruptions.

Actions:

for political observers,
Contact your local Republican representatives to understand their stance on the internal party dynamics (suggested).
Support candidates who prioritize representative democracy over self-serving interests (implied).
</details>
<details>
<summary>
2022-02-14: Let's talk about roadblocks in Canada and the manual.... (<a href="https://youtube.com/watch?v=50X0kus2ePk">watch</a> || <a href="/videos/2022/02/14/Lets_talk_about_roadblocks_in_Canada_and_the_manual">transcript &amp; editable summary</a>)

Canadian forces handle a bridge protest textbook-perfect, denying opposition propaganda wins and aiming to fade the incident away through protocol adherence and counterintelligence vigilance.

</summary>

"Nobody's gonna get their stripes because they were there."
"It denies the opposition any victory."
"They denied them that media win, that public relations win."
"Handling it the way they're handling it removes all of their talking points."
"They just look silly now."

### AI summary (High error rate! Edit errors on video page)

Talks about the Canadian response to a roadblock on a bridge, praising their adherence to protocol and minimal use of force.
Notes that the Canadian response was successful in denying the opposition a propaganda victory due to lack of dramatic footage.
Mentions that if Canada continues handling similar situations the same way, this incident will likely be forgotten.
Emphasizes that the goal of the Canadian forces should be to remove all talking points and PR wins from the opposition.
Addresses the issue of members of a Canadian elite military unit being involved in a controversial movement and the importance of counterintelligence in such cases.
Suggests that those sympathetic to extreme elements should be removed from units that may protect high-profile individuals like the prime minister.

Actions:

for community members, activists,
Conduct more extensive background checks and monitor social media for sympathies in high-risk units (implied)
Maintain adherence to protocol in handling similar situations to prevent propaganda victories (implied)
</details>
<details>
<summary>
2022-02-14: Let's talk about Elvis, shirts, and changing the world.... (<a href="https://youtube.com/watch?v=_13sDGw_5aQ">watch</a> || <a href="/videos/2022/02/14/Lets_talk_about_Elvis_shirts_and_changing_the_world">transcript &amp; editable summary</a>)

A group in Alabama faces fundraising challenges for their community projects, prompting Beau to advocate for utilizing any means necessary, including selling right-wing merchandise, to support their vital initiatives.

</summary>

"Wars cost money. And if you are, as they are, behind enemy lines, surrounded, use any tool the opposition will give you."
"People get their fines paid. People get their ramps. Kids get food. Seems like a small price to pay."
"You're surrounded. You are outnumbered. You're outfinanced."

### AI summary (High error rate! Edit errors on video page)

There is a group of eight or nine people in Alabama who want to make positive changes in their community, such as putting in wheelchair ramps, helping single moms, and paying off fines for people leaving jail to prevent them from getting stuck in the system.
They face challenges in fundraising for their activities, especially in an incredibly religious area where helping single moms and criminals might be seen negatively.
Despite having assets like the ability to make T-shirts and a relative with a stand at a flea market, they struggled to raise funds through progressive-themed merchandise.
They considered selling right-wing merchandise to fund their left-wing activities, but faced backlash online for compromising their values.
Beau suggests that if making and selling right-wing merchandise is what it takes to fund their projects and spread progressive ideas, they should go ahead with it.
He argues that in a war on poverty situation, they should use any tool available to them, even if it means compromising on ideological purity.
Beau encourages them to prioritize helping people in need over passing an ideological purity test imposed by others online.
He points out the importance of taking money from opponents legally to do good work, spread progressive messages, and challenge those profiting from selling right-wing merchandise.
Beau advises not to disclose this fundraising method to those who may have issues with it, focusing instead on the positive impact their projects will have on the community.
Drawing parallels to Elvis facing resistance in the South, Beau urges the group to focus on their mission of helping those in need, even if it means making uncomfortable compromises.

Actions:

for community activists,
Sell merchandise at flea markets to raise funds (suggested)
Prioritize community projects over ideological purity tests (implied)
</details>
<details>
<summary>
2022-02-13: Let's talk about the boyfriend loophole.... (<a href="https://youtube.com/watch?v=A50gV2kuVbc">watch</a> || <a href="/videos/2022/02/13/Lets_talk_about_the_boyfriend_loophole">transcript &amp; editable summary</a>)

Beau questions Republican men on prioritizing guns over protecting women from domestic violence and advocates for closing the boyfriend loophole in the Violence Against Women Act.

</summary>

"You're more concerned about a bathroom than somebody actually hurting them."
"But you're not willing to remove their right to own a firearm."
"It almost seems like you don't actually care about the women in your lives."
"They care more about those guns than they do their daughters, their sisters, their mothers."
"I do not understand the gun culture's fascination with protecting those who provide the excuses to ban guns outright."

### AI summary (High error rate! Edit errors on video page)

Addresses Republican men about a scenario involving domestic violence.
Questions why it's acceptable to be willing to take someone's life but not their gun.
Criticizes the Violence Against Women Act for not closing the boyfriend loophole.
Points out that most law enforcement calls and half of homicides are related to dating partners.
Expresses confusion over the lack of concern for protecting women from domestic violence.
Advocates for the Violence Against Women Act as a necessary legislation.
Questions why Republican senators prioritize other issues over protecting women.
Emphasizes the importance of the act in reducing violence and protecting women.

Actions:

for republican men,
Advocate for closing the boyfriend loophole in the Violence Against Women Act (suggested)
Keep fighting for the legislation that protects victims of domestic violence (exemplified)
</details>
<details>
<summary>
2022-02-13: Let's talk about breaking the first two rules.... (<a href="https://youtube.com/watch?v=U-fAlT7VlLE">watch</a> || <a href="/videos/2022/02/13/Lets_talk_about_breaking_the_first_two_rules">transcript &amp; editable summary</a>)

Beau breaks down the misunderstood film "Fight Club," exploring themes and discussing how a minor change alters its message on personal growth and societal systems.

</summary>

"Humanity isn't really designed to live the cubicle life."
"There is such a longing for fulfillment and real meaning in life that it is easy to succumb to a group identity."
"It changes it to a dystopian commentary on how the system is so strong that you cannot evolve, that you cannot change."
"It alters the whole thing. It shifts it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talking about a film from the 90s that is being released in another country with minor changes.
The movie "Fight Club" is widely misunderstood due to intentional ambiguity and vague themes left up to viewer interpretation.
Themes in the film include criticism of the cubicle life, toxic masculinity, and anti-capitalist messages.
The relationship between the narrator and Tyler evolves, with one becoming more masculine and the other retreating.
Conflict arises around intimacy between the characters, leading to extreme antics and a longing for fulfillment.
In the original film, the climax is dramatic, but in the version released in China, the police intervene and prevent the bomb from exploding.
This change alters the film's message, potentially turning it into a commentary on the strength of the system preventing personal evolution.
While some may be upset by the change, it doesn't necessarily make the film bad but definitely different.
The alteration raises questions about the impact of editing on narratives and the importance of context in storytelling.

Actions:

for film enthusiasts, social critics,
Critically analyze films and media for underlying messages (exemplified)
Advocate for preserving original artistic intentions in film adaptations (suggested)
</details>
<details>
<summary>
2022-02-12: Let's talk about the Republican Govs' strange move in Georgia.... (<a href="https://youtube.com/watch?v=KTC2W_Jsae4">watch</a> || <a href="/videos/2022/02/12/Lets_talk_about_the_Republican_Govs_strange_move_in_Georgia">transcript &amp; editable summary</a>)

The Republican establishment is distancing from Trump, supporting opponents, possibly due to foreknowledge of damaging events.

</summary>

"The big donors and the organizations or establishment insiders, the people who've been around a long time, distancing themselves from Trump and Trumpism."
"Somebody somewhere got inside information as to what is coming for Trump and that crew."
"They don't think it will reflect well on the Republican Party as a whole, so they are trying to kind of re-establish the old guard and break away from Trump."

### AI summary (High error rate! Edit errors on video page)

The Republican establishment and big money are distancing themselves from Trump and financially supporting defeating his chosen candidates.
The Republican Governors Association, which usually endorses the incumbent, committed half a million dollars in Georgia to back Governor Kemp against Trump's preferred candidate.
Large associations like this traditionally do not spend money on ads in primaries, making this move unusual.
A growing number of long-time insiders and organizations are visibly moving away from Trump and his allies.
Speculation suggests that they may have inside information about something negative coming for Trump and his crew.
There seems to be an effort to re-establish the old guard and distance from Trump to protect the Republican Party's image.
The significant financial backing behind this distancing indicates a serious effort.
The shift away from Trump is not limited to individuals like Graham and McConnell but involves a broader chorus of institutions and people.
The motivation behind this distancing remains suspicious and suggests foreknowledge of impending events.
The Republican Party's image may be a key factor driving this break from Trump.

Actions:

for political observers,
Support candidates not endorsed by establishments (suggested)
Stay informed about political developments (suggested)
</details>
<details>
<summary>
2022-02-12: Let's talk about the CRT Republicans love.... (<a href="https://youtube.com/watch?v=0B_Iw-Q-msc">watch</a> || <a href="/videos/2022/02/12/Lets_talk_about_the_CRT_Republicans_love">transcript &amp; editable summary</a>)

Republicans have their own theory, Causal Relationship Theory (CRT), that explains how past legislation impacts current society, raising questions about why they oppose teaching it to students.

</summary>

"Previous legislation has an impact today. You can't argue with it. It's just fact."
"If we want to fix it we have to know about it."
"They have to understand that causal relationship."

### AI summary (High error rate! Edit errors on video page)

Explains that Republicans have their own theory called Causal Relationship Theory (CRT) that they use to analyze government policies and their impact on the average American.
Republicans believe that government actions have long-lasting effects, citing examples like welfare policies and historical legislation that marginalized certain groups.
Points out the importance of understanding how past legislation continues to impact society today, especially in areas like race relations.
Questions why Republicans oppose teaching this theory to students, suggesting it may be to maintain systems of inequality and prevent individuals from understanding the effects of past legislation on their lives.
Emphasizes the necessity for future leaders to comprehend the causal relationship between past policies and present outcomes in order to address and correct societal issues.

Actions:

for students, educators, policymakers,
Teach students about the causal relationship between past legislation and present societal issues (implied)
</details>
<details>
<summary>
2022-02-12: Let's talk about a win for wolves.... (<a href="https://youtube.com/watch?v=rXJyraMwUCQ">watch</a> || <a href="/videos/2022/02/12/Lets_talk_about_a_win_for_wolves">transcript &amp; editable summary</a>)

Gray wolves were delisted but recently relisted under the Endangered Species Act, with ongoing efforts needed in states where protections are lacking.

</summary>

"Good news. That's a win."
"There's been a big win, but the fight isn't over."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Gray wolves were delisted from the Endangered Species Act during the Trump administration, shifting protection responsibilities to the states.
A recent court ruling relisted the gray wolf under the Endangered Species Act due to inadequate data collection.
Despite this win, Wyoming, Idaho, and Montana are not covered by the relisting, where significant wolf-related issues persist.
Organizations are working towards an emergency listing of gray wolves in these states with some federal support.
There's skepticism about the three states' willingness to address the wolf protection issue due to economic interests and a certain image they wish to maintain.
Continued support for organizations striving for wolf protection is necessary even after the recent win.

Actions:

for conservationists, wildlife advocates,
Support organizations working on emergency listing of gray wolves in Wyoming, Idaho, and Montana (implied)
Provide assistance and resources to organizations collaborating with Fish and Wildlife and the Department of the Interior for wolf protection (implied)
</details>
<details>
<summary>
2022-02-11: Let's talk about the futures of Ukraine and Russia.... (<a href="https://youtube.com/watch?v=hiB15crTQVM">watch</a> || <a href="/videos/2022/02/11/Lets_talk_about_the_futures_of_Ukraine_and_Russia">transcript &amp; editable summary</a>)

Beau provides insights into potential scenarios in Ukraine if Russia decides to move in, including the unpredictability of the conflict and the importance of irregular fighters.

</summary>

"Everybody has a plan until they get punched in the face."
"Professional soldiers are predictable, but the world is full of amateurs."
"It's too soon to make that kind of prediction."
"Make no mistake about it. They can't be informed because that first 24 hours, that will shape everything."
"Putin wants to take Ukraine without a shot."

### AI summary (High error rate! Edit errors on video page)

Beau provides insights into potential scenarios in Ukraine if Russia decides to move in.
Russia's initial approach might resemble Desert Storm, focusing on a strong air game and conventional artillery.
Ukrainian military may not attempt to go toe-to-toe with Russia, opting for guerrilla-style tactics instead.
Beau mentions the importance of irregular fighters in disrupting major operational plans.
The possibility of fog of war events, like accidental strikes on civilians, could drastically alter the conflict's outcome.
Western media and intelligence have a poor track record of predicting the effectiveness of irregular forces.
Beau speculates that Putin may want to take Ukraine without direct military action.
The unpredictability of the situation, especially in the first 24 hours, makes it challenging to make accurate predictions.
The longer the conflict goes on, the more prepared Ukraine can become, potentially making it costly for Russia.
Beau concludes with the idea that Putin might be seeking a way to take Ukraine without resorting to invasion.

Actions:

for global citizens,
Join organizations supporting peace and diplomacy in Ukraine (implied)
Stay informed about the situation in Ukraine and advocate for peaceful solutions (implied)
</details>
<details>
<summary>
2022-02-11: Let's talk about the Superbowl and trucks.... (<a href="https://youtube.com/watch?v=Rt6tSppsO_A">watch</a> || <a href="/videos/2022/02/11/Lets_talk_about_the_Superbowl_and_trucks">transcript &amp; editable summary</a>)

Beau warns against using trucks for demonstrations at the Super Bowl, citing serious security risks and legal repercussions for participants.

</summary>

"Do not do this. This is a horrible idea."
"They present an unlawful threat of violence."
"Your leaders who are putting you up to this, they know. They know it's a horrible idea."

### AI summary (High error rate! Edit errors on video page)

Beau expresses his disinterest in the Super Bowl and trucks, stating he doesn't care about the sports game or who wins.
He warns against using trucks for demonstrations at the Super Bowl, deeming it a bad idea with potential serious consequences.
Beau explains the security risks involved with bringing trucks close to the Super Bowl, mentioning the National Special Security Event designation.
He cautions that individuals pushing for these actions won't be present during any potential arrests or legal consequences.
Beau illustrates the dangers of attempting to bring unfamiliar trucks close to large crowds by referencing past tragic events.
He stresses that such actions are unlawful and could lead to severe legal repercussions for participants.
Beau criticizes the leaders encouraging these actions, accusing them of prioritizing their agendas and monetary gains over the safety and well-being of their followers.
He concludes by advising against pursuing this course of action, as it will ultimately end poorly for those involved.
Beau ends with a reminder that this dangerous idea should not be pursued, as it will lead to negative outcomes for participants.

Actions:

for community members,
Refrain from participating in using trucks for demonstrations at events (implied)
</details>
<details>
<summary>
2022-02-11: Let's talk about a development with Canada's truckers.... (<a href="https://youtube.com/watch?v=Pw2CDzAU7EA">watch</a> || <a href="/videos/2022/02/11/Lets_talk_about_a_development_with_Canada_s_truckers">transcript &amp; editable summary</a>)

Truckers claiming the right to arrest in Canada's trucker situation bring a serious escalation likely to prompt a strong government response.

</summary>

"Claiming the power to arrest and detain and the authority to use force is viewed negatively by governments."
"Governments losing the monopoly on violence is seen as a sign of a failed state."
"The public swearing-ins by the truckers are seen as a serious miscalculation, likely to elicit a response rather than negotiation from the government."

### AI summary (High error rate! Edit errors on video page)

Providing an update and forecast on the situation in Canada involving a group of truckers who have been swearing-in ceremonies to induct people into the Canadian Common Corps of Peace Officers.
The truckers are claiming the right to arrest and detain people, linked to a movement called Freedom of the Land.
The movement is designated as an extremist threat by CSIS, the Canadian CIA.
The movement has been declining since 2010 and historically recruits using issue-based causes.
Claiming the power to arrest and detain and the authority to use force is viewed negatively by governments.
Governments losing the monopoly on violence is seen as a sign of a failed state.
While the movement hasn't been violent by American standards, the Canadian government is likely to respond strongly.
A marked escalation in response from Canadian officials is expected.
The situation alters the normal playbook for handling such events and may pose a serious threat if allowed to continue.
The public swearing-ins by the truckers are seen as a serious miscalculation, likely to elicit a response rather than negotiation from the government.

Actions:

for government officials, activists, community leaders,
Monitor the situation closely for updates and developments (implied)
Stay informed about movements and developments in your community (implied)
</details>
<details>
<summary>
2022-02-10: Let's talk about Trump's Truth Trouble.... (<a href="https://youtube.com/watch?v=qTGJkkAW50I">watch</a> || <a href="/videos/2022/02/10/Lets_talk_about_Trump_s_Truth_Trouble">transcript &amp; editable summary</a>)

Trump's social media venture, Truth Social, faces delays and challenges, jeopardizing its vision of a censorship-free platform for extreme views.

</summary>

"Trump's plan was to create a censorship-free platform for his followers to express extreme views."
"Without distribution through major platforms, the success of the network is doubtful."
"Being removed from the app store could be the most damaging outcome for Truth Social."

### AI summary (High error rate! Edit errors on video page)

Trump's social media network, Truth Social, is facing roadblocks and delays in its release.
The app's beta version failed to materialize in November, and the release has been pushed back again.
Trump's plan was to create a censorship-free platform for his followers to express extreme views.
However, in order to be distributed via Apple and Google, the app will have to adhere to their guidelines, including moderation.
Without distribution through major platforms, the success of the network is doubtful.
Being removed from the app store could be the most damaging outcome for Truth Social.
Trump has brought in Devin Nunes to address the issues faced by Truth Social.
Nunes has experience with social media networks, particularly on Twitter.
Trump's fans may not get the unrestricted platform they desire due to the need for moderation.
The future of Truth Social remains uncertain amidst these challenges.

Actions:

for social media users,
Monitor the developments of Truth Social and its impact on social media landscape (implied).
</details>
<details>
<summary>
2022-02-10: Let's talk about Starbucks uniting the country.... (<a href="https://youtube.com/watch?v=xx5jBLSkVQI">watch</a> || <a href="/videos/2022/02/10/Lets_talk_about_Starbucks_uniting_the_country">transcript &amp; editable summary</a>)

Starbucks workers organizing for better conditions challenge societal views on labor value and advocate for fair treatment in a wealthy country with widening wealth disparities.

</summary>

"The people who provide that service, they deserve a decent life."
"Every job requires skill of some kind."
"If the gap between the haves and the have-nots is that big, there's probably something wrong with it."
"If you being one of the haves in your own eyes is dependent on somebody who provides you a service being a have-not, you're already a have-not."
"Have you ever been to a new Starbucks, one that just opened up and all the employees are new? Does it run well? No."

### AI summary (High error rate! Edit errors on video page)

Starbucks employees are organizing themselves to start a union to advocate for better pay, benefits, and conditions.
The idea that certain jobs are "lesser" and do not deserve fair compensation stems from societal beliefs about education and labor devaluation.
Beau challenges the notion of "unskilled labor" and argues that every job requires skill of some kind.
He points out the importance of valuing all types of work and ensuring that workers can live decently.
Beau criticizes the mindset that suggests certain jobs do not require fair wages or benefits based on perceived hierarchy.
He illustrates the impact of undervaluing labor on depressing wages and creating disparities between different professions.
Beau questions the concept of unskilled labor, suggesting that it may refer more to uncredentialed work rather than lacking skills.
He underscores the need for collective bargaining to achieve a decent standard of living for many workers in a wealthy country.
Beau points out the concerning wealth gap between the haves and have-nots, indicating underlying issues in society.
He challenges individuals to rethink their perspectives on labor, education, and societal inequalities.

Actions:

for workers, advocates, activists,
Support unionization efforts among workers (exemplified)
Advocate for fair wages and benefits for all types of labor (implied)
</details>
<details>
<summary>
2022-02-10: Let's talk about HB2161 in Arizona... (<a href="https://youtube.com/watch?v=pXljyJIXmzU">watch</a> || <a href="/videos/2022/02/10/Lets_talk_about_HB2161_in_Arizona">transcript &amp; editable summary</a>)

Arizona bill requires teachers to inform parents about their child's identity, sparking concerns of government intrusion and discrimination.

</summary>

"The purpose of the bill is to require people at schools, teachers, so on and so forth, to call in parents if they find out their student, if they find out their child, is expressing a different identity or orientation."
"When asked why he didn't work with real stakeholders, with education groups, with groups that represent educators and teachers, that was his answer."
"If you're proposing legislation and the group that's going to be impacted by it wouldn't support it, that's probably an indicator that it's a bad piece of legislation."
"This is, A, part of the desire to create that informer system in the United States, to bring about that type of system that authoritarians like to use to keep the populace in check."
"Republicans, at this point, are limited to kicking down at LGBTQ kids."

### AI summary (High error rate! Edit errors on video page)

Addressing a quote from Arizona about a new piece of legislation introduced by Steve Kaiser, HB 2161.
The bill aims to require teachers to inform parents if their child expresses a different identity or orientation.
This bill creates an atmosphere where the government monitors and controls personal information about students.
Failure to comply with this requirement could lead to lawsuits, fostering an environment of spying and informing on neighbors.
Many anti-LGBTQ groups assisted in drafting this legislation, revealing its discriminatory nature.
When questioned about the lack of collaboration with education stakeholders, Kaiser's response was dismissive and concerning.
The legislation seems to have been proposed without the support of any education groups, indicating its flawed nature.
This bill is part of a broader agenda to establish an informer system reminiscent of authoritarian regimes.
It serves as a divisive tactic to appeal to a certain base by targeting vulnerable LGBTQ children.
Republicans resort to attacking LGBTQ kids as a way to garner support, lacking genuine leadership.
Individuals in Arizona are encouraged to look into the implications of this bill.

Actions:

for activists, educators, parents,
Contact local education advocacy groups to understand their stance on the bill (suggested)
Stay informed about local legislative actions affecting schools and children (exemplified)
</details>
<details>
<summary>
2022-02-09: Let's talk about the RNC, McConnell, and Trump.... (<a href="https://youtube.com/watch?v=sIEYv4avfCY">watch</a> || <a href="/videos/2022/02/09/Lets_talk_about_the_RNC_McConnell_and_Trump">transcript &amp; editable summary</a>)

Beau speculates on McConnell's surprising defense of Republicans involved in the January 6th investigation, hinting at potential insider knowledge of Trump's legal troubles.

</summary>

"They've tried to spin it."
"He didn't say anything."
"I think they know something."
"There's a lot of splits."
"I think there may be some loose lips."

### AI summary (High error rate! Edit errors on video page)

Speculates on the recent actions of the RNC, McConnell, and Trump in relation to the events of January 6th.
Points out the RNC's censure of Republicans participating in the committee investigating the events of January 6th.
Mentions McConnell's surprising defense of Republicans on the committee and distancing from the RNC's statement.
Questions McConnell's motives in publicly supporting Republicans on the committee despite potential political risks.
Suggests that establishment Republicans are strategically distancing themselves from Trumpism.
Notes ongoing feuds within the Republican Party between establishment figures and those embracing Trumpism.
Speculates on potential information that McConnell and other establishment Republicans might have regarding Trump's legal troubles.
Concludes with a thought on loose lips in Capitol Hill hinting at Trump's legal challenges.

Actions:

for political analysts, republican voters,
Speculate on political motives and insider knowledge within the Republican Party (implied)
</details>
<details>
<summary>
2022-02-09: Let's talk about harm reduction and less than accurate coverage.... (<a href="https://youtube.com/watch?v=dg8DA1LuZFk">watch</a> || <a href="/videos/2022/02/09/Lets_talk_about_harm_reduction_and_less_than_accurate_coverage">transcript &amp; editable summary</a>)

The Biden administration's $30 million grant program for harm reduction in addiction was inaccurately framed as purchasing crack pipes to provoke outrage, but it actually includes various harm reduction strategies representing the future of combating addiction.

</summary>

"Manufactured outrage, because the Republican base now, it has to be angry."
"The war on drugs is an unmitigated failure. We lost. The plants won, right?"
"This is what the future of combating addiction in the country is going to look like."

### AI summary (High error rate! Edit errors on video page)

Biden administration authorized a $30 million grant program for harm reduction in addiction.
Grant program had eight different categories for spending, including safe smoking kits.
Headlines falsely claimed $30 million spent on crack pipes to provoke outrage.
Other approved uses of the grant money include harm reduction vending machines, medications, condoms, and more.
Framing of the coverage was inaccurate and designed to provoke outrage among the Republican base.
The history of othering people with addiction issues and the failure of the war on drugs are discussed.
Beau advocates for harm reduction strategies as the future of combating addiction.
Safe smoking kits do not necessarily mean pipes; they can include protective rubber pieces.
The inaccurate coverage does not represent the future of addiction combatting efforts in the country.

Actions:

for advocates for harm reduction,
Advocate for harm reduction strategies in combating addiction (implied)
Educate others on the importance of harm reduction programs (implied)
</details>
<details>
<summary>
2022-02-09: Let's talk about defending democracy and tool boxes.... (<a href="https://youtube.com/watch?v=VXfhuTgjF7Q">watch</a> || <a href="/videos/2022/02/09/Lets_talk_about_defending_democracy_and_tool_boxes">transcript &amp; editable summary</a>)

Beau questions support for U.S. democracy amidst the desire for radical change, comparing government to different toolboxes and stressing the importance of maintaining the current system to build a better society.

</summary>

"Government should serve the people, not the other way around."
"That's the starting place."
"Politics makes strange bedfellows."
"It's probably happened a lot throughout history."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Questions the support for U.S. democracy by those wanting a radically different world.
Compares different toolboxes as symbolic of different forms of government/society.
Describes his toolbox as representative democracy, with limitations but some abilities.
Warns against far-right government models where people serve the government, not vice versa.
Acknowledges flaws in the current U.S. government and Constitution but sees potential for change.
Stresses the importance of maintaining the current toolbox (representative democracy) to build a better society.
Recognizes individuals who advocate for change while supporting the existing system behind the scenes.
Shares a moment of realization about the precarious state of American democracy.
Notes the efforts of many pushing for progressive change while maintaining the status quo.
Concludes by reflecting on the complex dynamics of politics and alliances throughout history.

Actions:

for political activists, progressives, democracy advocates.,
Maintain support for representative democracy (implied).
Advocate for progressive change within the existing system (implied).
Stay informed about the state of American democracy and actively support positive changes (implied).
</details>
<details>
<summary>
2022-02-08: Let's talk about understanding foreign policy as it is and working on cars.... (<a href="https://youtube.com/watch?v=KNZFOkE6Lxs">watch</a> || <a href="/videos/2022/02/08/Lets_talk_about_understanding_foreign_policy_as_it_is_and_working_on_cars">transcript &amp; editable summary</a>)

Beau explains American foreign policy through a car repair analogy, urging a shift from military-centric approaches towards cooperative diplomacy for effective change.

</summary>

"Most Americans have Steve's view of foreign policy."
"The problem is the engine is out of the car."
"I want to make sure that people understand the parts so they can fix it."

### AI summary (High error rate! Edit errors on video page)

Beau delves into American foreign policy, stressing the importance of understanding it as it is rather than how we wish it to be.
He uses a story about fixing cars to illustrate his point about American foreign policy.
Beau differentiates between three types of people when it comes to understanding cars: those who can do basic repairs, those who can diagnose issues over the phone, and those who struggle to identify anything under the hood.
Drawing a parallel, Beau compares most Americans' understanding of foreign policy to the friend who struggles with car repairs, focusing on symptoms rather than the root cause of issues.
He points out that many Americans see military adventurism and drones as issues with foreign policy, but these are merely symptoms of a deeper problem.
Beau suggests that the over-reliance on the US military for policy execution is one of the major flaws in American foreign policy.
He advocates for a shift towards a more cooperative approach in foreign policy, utilizing economic ties and diplomatic solutions over military intervention.
Beau criticizes common foreign policy takes on platforms like Twitter, likening them to trying to fill a car with gas when the engine is missing.
He mentions a recent movement involving Ted Cruz advocating for sanctions on a pipeline, showcasing how such actions can hinder diplomatic efforts.
Beau stresses the importance of understanding the flaws in foreign policy to work towards improving and refining it for the future.

Actions:

for policy analysts, activists,
Analyze and understand the root causes of issues in foreign policy (implied)
Advocate for a shift towards cooperative diplomacy and economic solutions in foreign affairs (implied)
</details>
<details>
<summary>
2022-02-08: Let's talk about Trump, boxes, Section 2071, and treason... (<a href="https://youtube.com/watch?v=-XXKz4G7YYo">watch</a> || <a href="/videos/2022/02/08/Lets_talk_about_Trump_boxes_Section_2071_and_treason">transcript &amp; editable summary</a>)

Beau explains US Constitution's treason definition, Section 2071 penalties, and presidential qualifications' constitutional restrictions, dampening excitement over potential disqualification.

</summary>

"Treason is a crime outlined in the U.S. Constitution."
"If Congress wants to change that, they need an amendment."
"The excitement over disqualification may not be applicable due to constitutional constraints."
"There is a possible avenue for seeking an indictment and a conviction."
"That penalty that everybody's super excited about, not going to be applied."

### AI summary (High error rate! Edit errors on video page)

Explains Title 18, Section 2071 of the US Code, currently circulating on social media in meme form.
Talks about potential applications of this statute to former President Trump.
Addresses the idea of changing the legal definition of treason and its relationship to the current situation.
Clarifies that treason is defined in the US Constitution and can only be changed through an amendment.
Points out that changing the legal definition of treason is unlikely and Congress may create a new charge instead.
Details the penalties under Title 18, Section 2071 for removing items from public offices without authorization.
Mentions that violating this statute could result in fines, imprisonment up to three years, and disqualification from running for public office.
Explains the qualifications to be President as defined in the Constitution.
Suggests that disqualification due to violating Section 2071 may be unconstitutional since presidential qualifications are set by the Constitution.
Mentions legal cases that establish Congress's limitations in changing qualifications without an amendment.
States that the excitement over disqualification under Section 2071 may not be applicable due to constitutional constraints.
Notes the recovery of documents from a golf course that were supposed to be at the White House or archives.
Raises the possibility of legal liability for the former president regarding the removed documents.
Concludes by mentioning that while legal actions are possible, the anticipated penalty may not be applied.

Actions:

for legal analysts, political enthusiasts.,
Contact legal experts for a detailed understanding of Section 2071 and its implications (suggested).
Engage in legal or political advocacy to address concerns regarding constitutional limitations on changing qualifications (exemplified).
Stay informed on any developments related to the discussed legal matters (implied).
</details>
<details>
<summary>
2022-02-08: Let's talk about Indigenous causes, truckers, and playbooks.... (<a href="https://youtube.com/watch?v=QOHGhn0o_pI">watch</a> || <a href="/videos/2022/02/08/Lets_talk_about_Indigenous_causes_truckers_and_playbooks">transcript &amp; editable summary</a>)

Countries deviate from the playbook based on perceived threats to the status quo, with racism and capitalism playing pivotal roles, impacting responses to movements advocating for real change versus those supporting the system.

</summary>

"The reason it historically gets visited upon marginalized groups is because they're smaller in number."
"Marginalized groups tend to be on the right side of history."
"If you ever needed a reason to show up, that's it."

### AI summary (High error rate! Edit errors on video page)

Explains why countries deviate from the playbook despite knowing it.
Mentions the Canadian truckers' situation and indigenous causes as examples.
Points out that racism and capitalism play a significant role in these deviations.
Illustrates how movements are seen as threats to the status quo.
Emphasizes that the response to movements depends on their perceived impact on the existing system.
Compares the relevance of truckers to the status quo with that of indigenous causes.
Notes the high vaccination rate in Canada and the expected relaxation of restrictions without protests.
Stresses the difference in treatment between movements advocating for the status quo versus those aiming for real change.
Addresses the power dynamics and historical overreactions by governments.
Encourages individuals, especially from the majority group, to show up in support of marginalized groups to counter public opinion manipulation.

Actions:

for activists, allies, advocates,
Show up in support of marginalized groups (implied)
Attend protests and demonstrations to counter public opinion manipulation (implied)
Join movements advocating for real change (implied)
</details>
<details>
<summary>
2022-02-07: Let's talk about nurses and a message to hospital administrators.... (<a href="https://youtube.com/watch?v=NVednwpJEzs">watch</a> || <a href="/videos/2022/02/07/Lets_talk_about_nurses_and_a_message_to_hospital_administrators">transcript &amp; editable summary</a>)

Hospital administrators and lawmakers are criticized for legislation that protects profits over nurses' well-being, potentially leading to a protest on May 12.

</summary>

"There's nothing in it to, I don't know, address the widespread concern from nurses about patient ratios..."
"It's just designed to protect the profits of the large companies at the expense of the little people."
"For nurses, that's just Tuesday. That's just something they do."
"I think they will do it. I disagree."
"Not addressing any of the concerns of the nurses. Not doing anything to solve the nursing shortage, just moving to protect the profits of the large companies."

### AI summary (High error rate! Edit errors on video page)

Hospital administrators, politicians, and executives are being called out for legislation designed to cap what companies can charge for travel nurses.
The legislation could push nurses' wages down, impacting even those who aren't travel nurses.
The focus of the legislation seems to protect profits rather than solve the real issues in the nursing industry.
There is concern about patient ratios, which directly affect patient safety, not being addressed in the legislation.
Beau suggests the need for an RN bill similar to the GI bill to alleviate the nursing shortage.
The nursing shortage is partly due to how hospitals are run, leading to nurses leaving the profession for other jobs.
Beau criticizes the prioritization of profit over the well-being of nurses and patients.
Nurses face emotionally challenging situations regularly, like helping patients say goodbye without adequate support.
The possibility of nurses organizing a protest on May 12, Florence Nightingale's birthday, is mentioned.
Hospital administrators are advised to prepare for potential disruptions during this time.

Actions:

for nurses, healthcare workers,
Prepare for potential disruptions and show support for nurses during a possible protest on May 12 (implied).
</details>
<details>
<summary>
2022-02-07: Let's talk about Canada's response to the truckers.... (<a href="https://youtube.com/watch?v=XJzFo6-xO4Q">watch</a> || <a href="/videos/2022/02/07/Lets_talk_about_Canada_s_response_to_the_truckers">transcript &amp; editable summary</a>)

Beau explains why he hasn't covered the trucker protests in Canada, praises Canadian officials' handling of the situation, warns against cracking down on protesters, and predicts potential arrests to disperse the movement amidst observed hypocrisy.

</summary>

"If you want this to go away, this is the correct response."
"Because rather than a bunch of working class people getting beat by the Mounties, what you have are people out there screaming, it's horrible."
"But because the conservative movement of today has just become a bunch of entitled, whiny, temper tantrum throwing people."

### AI summary (High error rate! Edit errors on video page)

Explains why he hasn't covered the trucker protests in Canada, stating that in relation to what is typically discussed on his channel, it's not a big story.
Points out that Canadian officials are handling the situation well, focusing on law enforcement efforts against vandalism and violence rather than cracking down on the protesters.
Draws parallels to the Trump administration's failures during demonstrations in the Pacific Northwest, where implementing a security clampdown backfired and made the movement grow.
Emphasizes the importance of allowing voices to be heard in a free society, even if the message may not make sense, while concentrating law enforcement efforts on specific issues like vandalism.
Predicts a potential increase in arrests of those supplying individuals engaged in vandalism and hints at the dispersal of the movement.
Comments on the hypocrisy of some individuals supporting the trucker protests while complaining about other border regulations, showcasing entitlement and inconsistency.
Foresees pressure mounting on the Canadian government to take action against the protesters disrupting businesses in Ottawa, urging them to resist until the movement disperses on its own.

Actions:

for social justice activists,
Monitor and support Canadian officials' focus on law enforcement efforts against vandalism and violence (exemplified)
Resist pressure to crack down on peaceful protesters and allow voices to be heard (suggested)
Advocate for a peaceful resolution to the situation and discourage violent responses (implied)
</details>
<details>
<summary>
2022-02-06: The roads with Mariah Parker AKA Linqua Franqa.... (<a href="https://youtube.com/watch?v=YFRTSDY9UIE">watch</a> || <a href="/videos/2022/02/06/The_roads_with_Mariah_Parker_AKA_Linqua_Franqa">transcript &amp; editable summary</a>)

Beau interviews Mariah Parker, a County Commissioner and hip hop artist, discussing her multifaceted roles, advocacy for workers, and plans for a potential freedom school to fight for collective liberation through music and education.

</summary>

"I think using music as a tool to educate."
"I just encourage people to think creatively about how that could be done."
"You can use the master's tools to unmake the master's house."
"I have a feeling that this is gonna take off in the sense that there's gonna be a coordinated push this year for organized labor and collective bargaining."
"I hope that was enlightening and entertaining."

### AI summary (High error rate! Edit errors on video page)

Introduces Mariah Parker, also known as Lingua Franca, a County Commissioner in Athens-Clarke County, Georgia, who is a hip hop artist and nearing completion of her PhD.
Mariah talks about her multifaceted roles as a public servant, artist, and scholar, including her recent re-election, upcoming album release, and advocacy for workers.
She shares the inspiration behind her song "Work," which references the labor movement and calls for unity among workers.
Mariah delves into the themes of her upcoming album, touching on historical and international movements for liberation and personal struggles.
The interview covers Mariah's unique swearing-in ceremony with Malcolm X's autobiography and her priorities as County Commissioner, focusing on labor rights and collective bargaining.
She expresses the need for direct action and community involvement to address issues like evictions and economic exploitation.
Mariah reveals her plans for a potential freedom school and stresses the importance of using music and education for social change.
The discussion extends to Mariah's work on police accountability, her reflections on the prison system, and her call for more creative forms of collective action.
Beau inquires about Mariah's experiences as a new parent and her upcoming projects, including her podcast and album release.

Actions:

for activists, community members,
Support worker organizing efforts in your community by encouraging collective bargaining and the formation of worker-owned cooperatives (implied).
Engage in direct action to protect individuals facing eviction by showing up and preventing their displacement (implied).
Take part in creative forms of collective action, such as blocking eviction proceedings or protesting against harmful development projects, to address urgent social issues (implied).
</details>
<details>
<summary>
2022-02-06: Let's talk about a question on republican lawsuit legislation.... (<a href="https://youtube.com/watch?v=QbUfrPqCcu4">watch</a> || <a href="/videos/2022/02/06/Lets_talk_about_a_question_on_republican_lawsuit_legislation">transcript &amp; editable summary</a>)

Beau criticizes Republican legislation allowing frivolous lawsuits, pointing out flaws in enforcement and advocating for education over coercive measures in changing behavior.

</summary>

"If you have a system that is based on money, the way this legislation proposes, you create two sets of people."
"You cannot make the justice system about money."
"It's not a penalty, it's a fee to do whatever they want and get away with it."
"In an ideal world, changing behavior is done by convincing through education, not coercive means."
"This is a tool to kick down those who don't have the means to fight back."

### AI summary (High error rate! Edit errors on video page)

Critiques the Republican habit of creating legislation allowing people to sue each other in cases where damages wouldn't normally exist.
Provides examples to illustrate the potential consequences of such legislation, such as holding someone accountable for leaving keys in a stolen car that causes harm.
Argues that the civil system already covers damages and penalties, making additional legislation unnecessary.
Points out the flaws in assuming that this legislation will be used wisely, citing past examples where it was not.
Raises concerns about the inequality that monetary penalties create, making justice a privilege for the wealthy.
Warns against making the justice system about money, as it allows the wealthy to circumvent consequences.
Advocates for changing behavior through education rather than coercive measures like monetary penalties.
Emphasizes the danger of using legislation as a tool to target marginalized individuals who lack resources to defend themselves.
Expresses skepticism about the effectiveness of legislative changes in influencing behavior, suggesting that education is a more sustainable approach.

Actions:

for legislators, activists, community organizers,
Challenge legislation that allows frivolous lawsuits (implied)
Advocate for education-based approaches to behavior change (implied)
Support marginalized individuals who may be targeted by unjust laws (implied)
</details>
<details>
<summary>
2022-02-06: Let's talk about NATO, the treaty, and whether it's still needed.... (<a href="https://youtube.com/watch?v=EjnmLRDbRQE">watch</a> || <a href="/videos/2022/02/06/Lets_talk_about_NATO_the_treaty_and_whether_it_s_still_needed">transcript &amp; editable summary</a>)

Beau provides a thorough overview of NATO's history, the key articles in its treaty, and the ongoing debate surrounding its relevance post-Cold War, ultimately concluding its continued importance in current foreign policy.

</summary>

"An attack on one is an attack on all."
"Why does NATO still exist?"
"Organizations like NATO, like Warsaw Pact, any permanent military alliance is generally not a great thing."
"NATO is as important as it was during the Cold War."
"It's probably still pretty necessary."

### AI summary (High error rate! Edit errors on video page)

Explains the origins and purpose of NATO, starting in 1949 with concerns about the Soviet Union and a need for collective defense.
Outlines the 14 articles of the NATO treaty and their key points, including resolving disputes peacefully, economic cooperation, and collective defense.
Emphasizes Article 5, stating that an attack on one NATO member is considered an attack on all, a significant aspect of the treaty.
Addresses the ongoing debate about the relevance of NATO post-Cold War, with questions arising about its necessity despite the disappearance of the Soviet Union.
Notes how historical events, like the invocation of Article 5 after 9/11, have influenced perceptions of NATO's importance over time.
Acknowledges the criticisms of permanent military alliances while recognizing the current strategic importance of NATO in countering near-peer adversaries like Russia and China.

Actions:

for international policy enthusiasts,
Join organizations advocating for diplomatic solutions to international conflicts (suggested)
Coordinate with local policymakers to understand the implications of NATO's role in current foreign policy (implied)
</details>
<details>
<summary>
2022-02-05: Let's talk about fast food, California, and AB 257.... (<a href="https://youtube.com/watch?v=tA9hPmZ7LR0">watch</a> || <a href="/videos/2022/02/05/Lets_talk_about_fast_food_California_and_AB_257">transcript &amp; editable summary</a>)

A bill in California aims to protect over half a million fast food workers by establishing a council and joint liability between franchisees and big companies.

</summary>

"It's the big company that demands that because they know that familiarity is what brings people back."
"Sometimes, you can judge the worth of a bill by how much effort goes into defeating it."

### AI summary (High error rate! Edit errors on video page)

A bill in California, AB257, passed in the state assembly and will impact over half a million fast food workers if it goes through the Senate.
The bill sets up a council to protect fast food workers, ensuring fair pay, wage standards, training, safety, and more.
This council will serve as a de facto union representative for the industry.
Joint liability is created between franchisees and big companies for labor violations at fast food joints.
Big companies might push back, claiming they lack control over local franchises, but standardization across all locations implies influence.
The consistency in fast food chains suggests that big companies have more influence than they claim.
The big companies demand uniformity to create familiarity and bring customers back.
Money seems to be the primary reason for big companies not ensuring fair treatment of workers.
Big companies are expected to pour money into defeating the bill, indicating awareness of widespread violations.
Judging by the effort to defeat it, the bill's worth can be inferred.

Actions:

for californian residents,
Review AB257 and reach out to state Senators in support of the bill (suggested)
Make a call, send an email, or tweet to show support for the bill (suggested)
</details>
<details>
<summary>
2022-02-05: Let's talk about State vs AP on Russia on Ukraine.... (<a href="https://youtube.com/watch?v=OAVS6jmHywk">watch</a> || <a href="/videos/2022/02/05/Lets_talk_about_State_vs_AP_on_Russia_on_Ukraine">transcript &amp; editable summary</a>)

Beau breaks down the tense exchange between the State Department and the Associated Press, delving into the feasibility of claims and the vital roles of both parties in international relations.

</summary>

"That's State Department's position. State Department isn't wrong in wanting to protect the intelligence community's assets and protect the way they gather information."
"Journalists and government spokespeople, they're not actually supposed to get along."
"Both sides were doing their job."

### AI summary (High error rate! Edit errors on video page)

Explains an exchange between a State Department spokesperson and an Associated Press journalist regarding Russia's alleged plans to invade Ukraine.
Analyzes the feasibility of the State Department's claims about Russia manufacturing a pretext for invasion.
Breaks down the different ways the U.S. may have obtained the information and why it's being kept classified.
Explores the role of journalists like the Associated Press in questioning and investigating government claims.
Emphasizes the importance of protecting intelligence methods and assets in international relations.
Notes the evolving dynamics between government spokespeople and the media in near-peer foreign policy contexts.
Suggests that releasing information strategically can deter conflicts rather than escalate them.
Acknowledges the necessity for journalists and government spokespersons to maintain a level of skepticism and accountability.
Concludes that both sides, the State Department and the Associated Press, were fulfilling their respective roles in this exchange.

Actions:

for foreign policy observers,
Analyze international news sources for multiple perspectives (suggested)
Support investigative journalism efforts (exemplified)
</details>
<details>
<summary>
2022-02-04: Let's talk about staying warm without power.... (<a href="https://youtube.com/watch?v=5m2Cq5YztSo">watch</a> || <a href="/videos/2022/02/04/Lets_talk_about_staying_warm_without_power">transcript &amp; editable summary</a>)

Recapping tips for staying warm during a power outage in extreme cold, urging proactive community action and caution with fire hazards.

</summary>

"Sometimes there is justice, and sometimes there is just us."
"The government's not coming to help. Ask the people in Texas."
"Don't wait for a response from them. As soon as it starts, put your plan into action."
"Because getting this set up while your home is still somewhat warm, that's ideal."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Recapping how to stay warm during a power outage in extreme cold, focusing on dressing in layers and heating a small area.
Designating a room away from exterior doors and windows to keep warm and setting up a tent inside with extra insulation layers.
Using a candle for additional heat but being cautious with fire hazards.
Creating a makeshift tent by moving a dining room table into the designated room and insulating it with blankets and clothes.
Heating rocks or smaller pots outside the home to bring warmth inside, being cautious of potential explosions.
Ensuring the designated room is big enough and comfortable for everyone, avoiding sweating from overdressing, and staying hydrated.
Reminding to take medications and checking on neighbors who may need help during outages.
Emphasizing community support and proactive action during prolonged outages, as government assistance may be delayed or insufficient.
Urging immediate action on the prepared plan once an outage starts, regardless of the expected duration or government response.
Advising to practice the preparedness plan even if the power is restored quickly, to be ready for future outages and starting the setup while the home is still warm.

Actions:

for community members,
Designate a warm room away from doors and windows with insulation layers (suggested).
Set up a makeshift tent using blankets and clothes for extra insulation (implied).
Heat rocks or pots outside to bring warmth inside (suggested).
Practice the preparedness plan immediately when an outage starts (suggested).
Stay hydrated and ensure everyone's comfort and safety in the designated warm room (implied).
</details>
<details>
<summary>
2022-02-04: Let's talk about Trump getting convicted.... (<a href="https://youtube.com/watch?v=K3YGHdOXsq4">watch</a> || <a href="/videos/2022/02/04/Lets_talk_about_Trump_getting_convicted">transcript &amp; editable summary</a>)

Beau analyzes the challenges in convicting Trump amidst partisan loyalty and the need for a compelling narrative to sway public opinion.

</summary>

"If they can indict a ham sandwich, odds are they can indict a mango Mussolini."
"They have to be able to recast him from dear leader to the existential threat to the republic that he was."
"They still have to account for that one person, one person on that jury can vote not guilty. Not based on the evidence, but based on partisan loyalty."
"Because you have all of this evidence, you have all the reporting about it, everything that we've seen."
"They have to say, this led to this, that led to this, that led to this."

### AI summary (High error rate! Edit errors on video page)

Explains the message he received from Germany questioning why Trump hasn't been indicted yet, given the number of people in prison.
Mentions the running joke that a federal prosecutor can indict a ham sandwich, implying it shouldn't be difficult to indict Trump.
Raises the question of whether they can convict Trump, especially with a portion of the jury pool still loyal to him.
Comments on the Department of Justice (DOJ) possibly dragging their feet due to the challenges of convicting Trump.
Expresses doubts about the committee's ability to craft a narrative that breaks the spell Trump has on his supporters.
Emphasizes the committee's duty to inform the public and ensure accountability while reshaping Trump's image from "dear leader" to a threat to democracy.
Suggests that the evidence alone may not be enough to secure a conviction due to partisan loyalty influencing jurors.
Proposes potential strategies for DOJ, including slamming Trump with charges or getting prominent right-wingers to testify against him.
Stresses the urgency for the committee to act swiftly in presenting a compelling case before DOJ proceeds.
Concludes by underscoring the importance of constructing a cohesive narrative to sway those still under Trump's influence.

Actions:

for political analysts, activists, voters,
Push the committee to swiftly present a compelling case (implied)
Advocate for prominent figures to assist in crafting a narrative against Trump (implied)
</details>
<details>
<summary>
2022-02-03: Let's talk about the Army's vaccine.... (<a href="https://youtube.com/watch?v=vZDsdcvqRL0">watch</a> || <a href="/videos/2022/02/03/Lets_talk_about_the_Army_s_vaccine">transcript &amp; editable summary</a>)

The US Army is developing a game-changing vaccine that could combat all variants of the current public health issue, with promising results so far, but further testing is needed before potential distribution.

</summary>

"This might be the game changer everybody's been waiting for."
"We're going to need more tools in the toolbox."
"The virus doesn't care."
"We should be responding in a global fashion."
"It's something to put on your radar."

### AI summary (High error rate! Edit errors on video page)

The US Army at Walter Reed has been working on their own vaccine for the current public health issue, using different technology that can work on all variants, known and unknown.
The vaccine has undergone animal and phase one testing, with promising results, but phase two and phase three testing are still needed, making it about nine months away from potential distribution.
The vaccine is referred to as a pan-coronavirus vaccine, although it's likely to work specifically on COVID-19 rather than all coronaviruses.
Beau expresses concern about potential conspiracy theories if the vaccine doesn't work on all coronaviruses in the future.
The vaccine's ability to be stored at room temperature for a month and in a fridge for six months is seen as a significant advantage for distribution, particularly in areas with limited infrastructure.
Beau stresses the importance of responding to the global pandemic in a global manner, even considering countries that the United States doesn't typically focus on.
He believes that this vaccine could be a game-changer in the fight against the current public health issue and suggests keeping an eye on its progress.

Actions:

for health officials, researchers, policymakers,
Monitor the progress of the US Army's vaccine development (suggested)
Advocate for global cooperation in responding to the pandemic (suggested)
Stay informed about advancements in vaccine technology (suggested)
</details>
<details>
<summary>
2022-02-03: Let's talk about Republicans getting sue happy.... (<a href="https://youtube.com/watch?v=ty5cpRWAkkU">watch</a> || <a href="/videos/2022/02/03/Lets_talk_about_Republicans_getting_sue_happy">transcript &amp; editable summary</a>)

New laws incentivizing citizen lawsuits mirror authoritarian tactics, chilling speech and behavior, ultimately turning neighbors against each other.

</summary>

"They can't really do anything about it, not directly. It would get shot down in the courts. But if they just created a mechanism for citizens to sue that teacher, well that would have a chilling effect."
"It's how they chill speech in society where only your betters get to decide what you can do, what you can talk about, what is socially acceptable, and what behaviors you need to engage in."
"Believe me, for $10,000, your neighbor will probably turn you in."

### AI summary (High error rate! Edit errors on video page)

Explains the connection between new laws proposed by Republicans and his piece of the Berlin Wall keychain from East Germany.
Describes the new method Republicans are using to push their agenda through state levels by allowing citizens to sue.
Draws parallels between the authoritarian tactics of the Stasi in East Germany and the incentivized citizen lawsuits in the US.
Warns about the dangerous implications of these laws, which aim to circumvent constitutional frameworks and manipulate citizens into enforcing state desires.
Raises concerns about the chilling effect on freedom of speech and behavior, as citizens could be sued for things deemed offensive or against state preferences.
Predicts a slippery slope where the lawsuit mechanism can be expanded to target various behaviors beyond just teaching subjects like gender.
Compares the incentivized citizens under these laws to those who collaborated with the Stasi in East Germany, betraying neighbors for personal gain or to avoid trouble.
Emphasizes that once such machinery is introduced, it will inevitably be used against those who initially supported it, leading to a more authoritarian society.
Concludes by drawing attention to the historical tactic used by authoritarian regimes to control and suppress dissent by turning citizens against each other for rewards.

Actions:

for citizens, activists,
Organize community education sessions on the importance of protecting freedom of speech and resisting authoritarian measures (suggested).
Join or support organizations advocating for civil liberties and protection against state overreach (exemplified).
Foster a strong community network that resists divisive tactics aimed at turning citizens against each other for personal gain (implied).
</details>
<details>
<summary>
2022-02-02: Let's talk about improv and talking to liberals and centrists.... (<a href="https://youtube.com/watch?v=IhHCf6eEGzw">watch</a> || <a href="/videos/2022/02/02/Lets_talk_about_improv_and_talking_to_liberals_and_centrists">transcript &amp; editable summary</a>)

Using British comedy and improv rules, Beau guides how to gently steer centrists and liberals towards more progressive viewpoints through kindness, questions, and agreement-building.

</summary>

"Yes, and move them."
"Ask questions that drive them in the direction they want."
"Nobody has the perfect take about everything."
"Don't berate them."
"Talk about it in terms of right and wrong."

### AI summary (High error rate! Edit errors on video page)

Explains how to use British comedy and improv rules to reach centrists and liberals and guide them towards a more progressive position.
The five rules of improv are discussed: "Yes, and", no open-ended questions, you don't have to be funny, make your partner look good, and tell a story.
Emphasizes the importance of not making jokes or using in-group humor that the other person may not understand.
Advocates for being kind, asking guiding questions, and moving slowly to help centrists and liberals shift towards a more progressive stance.
Provides a practical example using the issue of police reform to demonstrate how to navigate a centrist position towards defunding the police through thoughtful questioning and agreement-building.

Actions:

for activists, progressives, advocates,
Ask guiding questions to gently shift perspectives towards progressivism (exemplified).
Avoid making jokes or using in-group humor that may alienate the listener (exemplified).
Be kind and patient in guiding individuals towards more progressive viewpoints (exemplified).
</details>
<details>
<summary>
2022-02-02: Let's talk about Lindsey Graham distancing from Trump.... (<a href="https://youtube.com/watch?v=N1LuQBowO0U">watch</a> || <a href="/videos/2022/02/02/Lets_talk_about_Lindsey_Graham_distancing_from_Trump">transcript &amp; editable summary</a>)

Lindsey Graham and other Republicans strategically distance themselves from Trump and Trumpism within the Republican Party, foreseeing potential consequences and shifts in direction.

</summary>

"The Republican Party is a lot like the mob. You are only as good as your last envelope."
"He is definitely starting to distance himself in a lot of different ways. Not just from Trump, but from those like Trump."
"Former President Trump, well, he is a petty, petty man."
"They probably understand that the clones of Trump, the various governors who want to be the next Trump, they probably understand that they're not politically viable either."
"I don't think that the establishment Republicans are going to underestimate Trumpism again."

### AI summary (High error rate! Edit errors on video page)

Lindsey Graham is making strategic moves within the Republican Party, distancing himself from Trump and Trumpism.
Graham, a long-time politician, is positioning himself away from Trump and those similar to him.
Other Republicans, including Dan Crenshaw, are also starting to distance themselves from Trump.
The Republican Party is akin to a mob where loyalty is based on benefits and performance.
Trump is losing support within the party due to his dwindling numbers and potential legal issues.
Clones of Trump, aspiring governors, are not politically viable as they lack the polish and may face backlash from Trump himself.
Graham's actions, such as condemning Trump's statements and supporting Biden, are strategic moves to distance himself.
Republicans are likely to distance themselves from Trump and his ideologies due to potential future consequences.
The establishment Republicans have learned not to underestimate Trumpism after being surprised by Trump's initial rise.
There is a shift happening within the Republican Party away from Trump and towards a different direction.

Actions:

for republican voters,
Position yourself strategically within your community or organization (implied).
</details>
<details>
<summary>
2022-02-01: Let's talk about tips, tiktok, and Virginia.... (<a href="https://youtube.com/watch?v=WKZ9DdxA-wQ">watch</a> || <a href="/videos/2022/02/01/Lets_talk_about_tips_tiktok_and_Virginia">transcript &amp; editable summary</a>)

Governor Yonkin manipulates education, restricting historical truths and encouraging citizens to report on each other, countered by activists flooding the state's email with misinformation.

</summary>

"You know, if you want to have thought crime, you gotta have the thought police."
"People just have to spend too much effort sifting through the information."
"We have to hide our history because we can't be proud of it, I guess."
"If you teach that, students might want to continue to change, and a whole bunch of people at the top who enjoy the status quo, well, they really don't want that, now do they?"

### AI summary (High error rate! Edit errors on video page)

Governor Yonkin campaigned on giving parents a say in education, only to have the state take full control and declare certain topics off-limits as thought crimes.
Yonkin aims to eliminate divisive subjects like history and literature to shape young minds by restricting access to information.
In an effort to bolster state power, citizens are encouraged to report on each other through an email address established by the state.
TikTokers flooded the state's education email address with misinformation, overwhelming the system and rendering it ineffective.
This form of activism is a temporary solution to challenge policies aiming to manipulate education and limit historical truths.

Actions:

for educators, activists, citizens,
Flood official channels with information to overwhelm systems (exemplified)
</details>
<details>
<summary>
2022-02-01: Let's talk about Russia vs US at the UN.... (<a href="https://youtube.com/watch?v=R63Kee9uszI">watch</a> || <a href="/videos/2022/02/01/Lets_talk_about_Russia_vs_US_at_the_UN">transcript &amp; editable summary</a>)

Beau provides insights on the recent UN meeting between the US and Russia, discussing accusations, advantages, NATO disunity, and the uncertainty of potential conflict escalation.

</summary>

"It will come out of nowhere. There will be hotspots that emerge and they will flare up randomly and without a lot of warning most times."
"The ultimate outcome is going to depend on how NATO finally lines up and what they decide they're going to do."

### AI summary (High error rate! Edit errors on video page)

The recent UN meeting between the United States and Russia wasn't as intense as some may think, considering the historical context of such meetings.
Both countries accused each other of provocation and war rhetoric, with the US having an advantage due to Russian troops on the Ukrainian border.
NATO disunity may be a factor delaying resolution, with some NATO countries ready to back Ukraine while others are less inclined.
The uncertainty lies in NATO's intent and response, with Putin possibly seeking a way out but with the situation still posing a risk of escalation.
The ultimate outcome depends on how NATO decides to respond, which will impact Russia's actions.
As a civilian, there may not be much direct impact you can have on the situation, and expectations need to adjust to the unpredictability of potential conflicts.

Actions:

for global citizens,
Stay informed on the situation and be prepared to support peaceful resolutions (implied)
Advocate for diplomatic solutions and peaceful negotiations (implied)
</details>
