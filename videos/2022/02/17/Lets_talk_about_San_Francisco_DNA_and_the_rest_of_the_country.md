---
title: Let's talk about San Francisco, DNA, and the rest of the country....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XcsA2q37p7s) |
| Published | 2022/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises concerns about the use of DNA evidence obtained from sexual assault kits against victims years later.
- Points out the chilling effect this practice may have on victims coming forward to provide evidence.
- Questions law enforcement's prioritization of information collection over solving serious crimes.
- Argues that deterring victims from providing evidence through DNA kits may lead to perpetrators committing more crimes.
- Calls attention to the ethical implications of using victims' DNA against them without their consent.
- Expresses worry that this issue in San Francisco may be happening statewide and potentially nationwide.
- Suggests that widespread knowledge of this practice could further discourage victims from seeking justice.
- Raises broader societal questions about the collection and storage of personal information by authorities.
- Contemplates the implications of a society where the government has total information awareness.
- Urges reflection on the impact of such practices on freedom and privacy.

### Quotes

- "Why would they not use it?"
- "It seems to be prioritizing information collection over solving an incredibly serious crime."
- "We're going to have to figure out where the lines are, how long information is going to be kept."
- "How free of a society can it really be?"
- "Y'all have a good day."

### Oneliner

Beau raises concerns about law enforcement using DNA evidence from sexual assault kits against victims, leading to chilling effects and ethical dilemmas, with potential broader societal implications.

### Audience

Advocates for victims' rights

### On-the-ground actions from transcript

- Advocate for clear guidelines and consent procedures in the use of DNA evidence from sexual assault kits (implied).
- Support organizations working to protect victims' rights in criminal investigations (suggested).

### Whats missing in summary

The full transcript provides a deeper dive into the ethical and privacy concerns surrounding the use of DNA evidence from sexual assault kits and prompts reflection on the broader societal implications of information collection by authorities.

### Tags

#DNAevidence #VictimsRights #LawEnforcement #PrivacyConcerns #InformationAwareness


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about data.
We're going to talk about information, how it's
collected.
And we're going to talk about a case out of San Francisco,
news breaking from there, that while it originates in San
Francisco, it seems as though it's going to impact the
entire state of California.
And it very well might affect other states as well, we don't know yet.
A district attorney in San Francisco was working a property case, and in the process of it,
they came to believe that the police department in San Francisco was using DNA evidence obtained
from a kit that is administered after a sexual assault
against the victim years later.
The kits, they collect all DNA samples.
So the victim's DNA gets put into a database.
It gets cataloged so forensics can say, OK,
this is supposed to be here.
This isn't.
and then they can identify the perpetrator.
And the DA has kind of come to find out, or at least believes,
that that database of victims is being
used to further criminal investigations years
after the fact.
Now, at this point, you have to stop and look at it
from law enforcement's point of view for just a second.
Because from where they're sitting, they have this database, they have this information.
Why would they not use it?
And the simple answer to that is that it might deter people from having that kit administered.
People might not want to provide that information.
They may not want to provide information that could be used against them later.
It could have a chilling effect on the collection of evidence against somebody who you know
committed a crime.
When one of those kids is administered, a crime was committed.
It already happened.
The idea of putting a chilling effect on the collection of that evidence because maybe
at some point in the future, the victim might commit a crime.
That seems like, well, kind of counterproductive.
A crime already happened, and you can gather evidence on that crime, but it seems as though
the police are willing to deter people from providing that evidence because they want
to use that evidence against them later.
There is the idea in this country that you have the right not to incriminate yourself.
I would suggest your DNA is part of that.
The person who elects not to undergo one of those kits out of fear of their DNA being
collected, the person who attacked them, they may not get caught, which means they may go
wanted to commit another crime because they've already shown that they'll do it
once. Whereas the victim in that case, they haven't. This process seems to be
very self-defeating. It seems to be prioritizing information collection over
solving an incredibly serious crime. The DA bringing attention to this is in
my opinion doing the ethical thing. We believe we have the right to be free
from unreasonable search and seizure, to be free from incriminating ourselves, to
be secure in our persons. The DA says that there is no statement when the
patient is administered this kit that says it could be used in this way. Now, all of
this is related to San Francisco. Reporting suggests that this practice of using this
database of victims in this way is statewide. If it is statewide there, it's probably happening
in other states.
This is probably the first case that we're hearing about that's going to start a domino
effect and we're going to find out that police departments all over the country have been
doing this.
And once that information starts to circulate widely, there's even more of an incentive
not to have that kit administered.
It's an incredibly invasive procedure to begin with that is occurring right after something
pretty bad happened.
It's not something that people look forward to already.
If you give them yet another reason to not do it, you're going to lose evidence that
that pertains to a serious crime, not a property crime.
With the ability to store information that has come into being recently, the United States
is going to have to grapple with this.
We're going to have to figure out where the lines are, how long information is going to
be kept.
much information should even be collected. And it doesn't just pertain to
this, it pertains to electronic information and everything else. At some
point we have to ask ourselves whether we really want the powers that be to
have total information awareness. And if that society occurs where the government
is aware of everything and stores that information forever to be used at a
later date. How free of a society can it really be, if you look to most literature
that pictures dystopian futures? A pretty constant thing is that, well, the
government has records on everything.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}