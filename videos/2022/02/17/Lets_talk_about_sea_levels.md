---
title: Let's talk about sea levels....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kH4n-CWf-gU) |
| Published | 2022/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Outlines a new report on sea levels released by six US government agencies.
- Notes a concerning trend of sea level rise being greater than previously thought.
- Mentions the expected increase in sea levels over the next 30 years, surpassing the rise seen in the entire 20th century.
- Details the average rise of sea levels around the US as 10 to 12 inches, with some areas facing up to 18 inches.
- Projects even higher sea level rises when looking beyond 30 years, particularly by 2060.
- Points out specific areas like Galveston and St. Petersburg facing substantial rises in sea levels.
- Emphasizes the impact of rising sea levels on buildings, homes, and commerce, especially when combined with storm surges.
- Quotes a National Ocean Service representative stating that sea level rise is already happening.
- Urges for immediate action to address climate change and its consequences.
- Advocates for making climate change a significant campaign issue in upcoming elections.

### Quotes

- "Sea level rise is upon us."
- "Once it starts rolling, you can't just stop it when you decide it gets too bad."
- "We are seeing the impacts of climate change today, now."
- "If we wait for them to get unbearable [...] it's gonna be too late to really stop it."
- "This is something that needs to be a campaign issue in the midterms."

### Oneliner

A call to action on rising sea levels and climate change impacts, urging immediate attention and political action.

### Audience

Climate advocates, voters

### On-the-ground actions from transcript

- Contact local representatives to prioritize climate change in upcoming elections (implied)

### Whats missing in summary

The urgency and importance of taking immediate action on climate change, using sea level rise as a tangible example.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about sea levels
and a new report that has been released.
It's coming from six US government agencies,
the US Army Corps of Engineers, FEMA, NOAA, NASA,
the EPA, and the US Geological Survey.
And, you know, good news.
For once, we're above average.
I mean, it's sea level rise,
so I guess that's not actually good news.
I didn't know this.
I assumed that sea level rise would be
kind of consistent around the world.
That's not the way it works.
Didn't know that.
What the report says is that over the next 30 years,
we can expect to see as much sea level rise
as we saw in the entirety of the 20th century.
The average around the US is going to be 10 to 12 inches.
There will be parts of Louisiana and Texas
that are closer to 18 inches.
So, significant.
When you add 10 years to that,
and rather than go out to 2050, you go out to 2060,
the rise is, in many areas,
in many areas, more easily measured in feet
rather than inches.
Galveston is more than two feet.
St. Petersburg here in Florida is just under two feet.
These are substantial amounts.
LA is 14 inches, and I think Seattle does the best at nine.
These are significant amounts.
They are significant amounts,
and they're going to matter because this is the standard.
This is the standard level that's going to exist.
In many places, this standard is already going
to be impacting buildings and people's homes
and just commerce in general as well.
And then you have to factor in the surges
that come with hurricanes and stuff like that.
This is significant.
Somebody from the National Ocean Service,
which is part of NOAA, said that sea level rise is upon us.
And it is.
So, what the world has been warned about for a long time
is now rapidly occurring.
This is just one facet to a global problem
when it comes to the environment
and when it comes to our levels of consumption
that cause climate change.
Stuff like this is one of those things
that once it starts rolling,
you can't just stop it when you decide it gets too bad.
You have to stop it before, you know,
semi-trucks are on the news a lot lately.
This is a lot like an 18-wheeler.
When you slam on the brakes, it doesn't stop immediately.
It takes time.
We have the reports.
We are seeing the impacts of climate change today, now.
If we wait for them to get unbearable
or to the point where it's truly disrupting commerce,
which in the United States, that's what matters,
it's gonna be too late to really stop it.
This is something that needs to be a campaign issue
in the midterms, in the next election,
and in every election after that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}