---
title: Let's talk about a sign about the elections from Colorado....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bEfrXuaIkqE) |
| Published | 2022/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tina Peters, the Mesa County clerk in Colorado, known for echoing Trump's false election claims, is under investigation for alleged election security breaches.
- A judge barred Tina Peters from overseeing the election in her county and she was recently arrested on an obstruction charge.
- Tina Peters announced her candidacy for Secretary of State of Colorado, potentially putting the entire state's election under her control if she wins.
- This situation is not unique; similar instances are happening in other states where Trump loyalists are running for election control positions.
- The current Secretary of State of Colorado, Jenna Griswold, labeled Tina Peters as unfit for the role and a danger to Colorado elections.
- Jenna Griswold emphasized that Colorado needs a Secretary of State who upholds the will of the people and doesn't embrace conspiracies.
- Trump loyalists targeting Secretary of State positions in swing states is part of a strategy to control elections.
- It's vital for all citizens, especially in swing states, to pay attention to and prevent Trump loyalists from controlling state-level elections.
- Preventing Trump loyalists from controlling elections is key to defeating Trumpism, as altering outcomes through means other than the ballot may be attempted.
- Being vigilant and active in preventing Trump loyalists from gaining election control is necessary to safeguard democratic processes.

### Quotes

- "Colorado needs a Secretary of State to be the Secretary of State."
- "Making sure that Trump loyalists do not control the elections at the state level is an imperative."
- "Preventing Trump loyalists from controlling elections is key to defeating Trumpism."

### Oneliner

Tina Peters' candidacy for Secretary of State of Colorado raises concerns about election security and the spread of falsehoods, echoing a broader trend of Trump loyalists vying for election control positions nationwide.

### Audience

Voters, Activists

### On-the-ground actions from transcript

- Contact local election officials to raise concerns about election security and transparency (implied)
- Support candidates who prioritize upholding democratic processes and rejecting conspiracy theories (implied)
- Stay informed about candidates running for election control positions in your state and their stances on election integrity (implied)

### Whats missing in summary

The full transcript provides detailed insight into the concerning trend of Trump loyalists seeking election control positions and the importance of safeguarding democratic processes at the state level. Viewing the full transcript can offer a comprehensive understanding of the risks associated with individuals like Tina Peters gaining power in critical election roles.

### Tags

#ElectionSecurity #TrumpLoyalists #SecretaryOfState #Colorado #Democracy


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
Colorado and the Mesa County clerk there, Tina Peters, who is known for appearing with the pillow
guy and is known for repeating Trump's, well, less than accurate claims about the election,
who is also under investigation for alleged election security breaches,
who was also barred by a judge from overseeing the election in her county,
who was also arrested last week for, on an obstruction charge,
who just announced her candidacy for Secretary of State of all of Colorado,
putting the entire state's election under her control if she were to win.
It's worth noting that she made this announcement on Bannon's podcast.
That's quite the resume and this is a symptom of something that is occurring all over the country.
There are people who have echoed Trump's claims about the election, running to control the elections
in Georgia, Michigan, Nevada, and Arizona. You can safely assume that this is going to happen
in any swing state, that they will try to get loyalists into those positions.
Now the current Secretary of State in Colorado released a statement about this development.
It says, Tina Peters is unfit to be Secretary of State and a danger to Colorado elections.
Peters compromised voting equipment to try to prove conspiracies, costing Mesa County taxpayers
nearly $1 million. She works with election deniers, spreads lies about the elections,
was removed from overseeing the 2021 Mesa County election, and is under criminal investigation by
a grand jury. Colorado needs a Secretary of State to be the Secretary of State.
Colorado needs a Secretary of State who will uphold the will of the people, not one who embraces conspiracies
and risks Coloradans' right to vote. That is from Jenna Griswold, current Secretary of State there.
Normally the Secretary of State position is one that it's not really hotly contested.
It isn't viewed as a must-win election. The current strategy of Trumpism requires
those elections be won. If you are in a swing state, really if you're in any state,
if you've never paid attention to this election before, this position now warrants your attention.
Making sure that Trump loyalists do not control the elections at the state level is an imperative.
It is something that will help defeat Trumpism as a whole, because since they know they're not going to win
at the ballot under conventional circumstances, they may attempt to find a way to alter those outcomes.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}