---
title: Let's talk about orders, tradition, and cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L1TAen1DfJY) |
| Published | 2022/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the topic of orders, tradition, and perception, surprised to still be discussing it in this context.
- Explains the difference between military and law enforcement in terms of following orders and accountability.
- Talks about the warrior cop mentality and the spread of militaristic culture within law enforcement.
- Explains the historical origins of following orders from superiors and the lack of accountability in some cases.
- Contrasts military objectives with those of law enforcement, pointing out the need for different systems of accountability.
- Mentions the importance of training in the military on disobeying unlawful orders and suggests a similar training for law enforcement.
- Criticizes the blurred lines between law enforcement and the military, pointing out the dangers of adopting militaristic traditions in policing.
- Emphasizes that law enforcement should not be seen as an occupying army and should not adopt a warrior cop culture without the necessary discipline.
- Raises concerns about the misconceptions around following orders in law enforcement and the need for clear boundaries between police and military roles.

### Quotes

- "Law enforcement is not the same as the military."
- "There should never be a point where a comparison between peace officers and the military makes sense."
- "They're not the same."

### Oneliner

Beau explains the dangers of blurring the lines between law enforcement and the military, stressing the importance of differentiating between peace officers and warriors. 

### Audience

Law Enforcement Officials

### On-the-ground actions from transcript

- Implement training for law enforcement on disobeying unlawful orders (suggested)
- Advocate for clear boundaries between law enforcement and military roles (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the historical origins of following orders in law enforcement and the dangers of adopting militaristic traditions within the police force.

### Tags

#LawEnforcement #Orders #Tradition #Military #Accountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about something
that I kind of thought we were done talking about.
I didn't imagine it coming up in this capacity anyway.
I could see it coming up in different ways later,
but not actually discussing the merits.
I thought we were done with that, but here we are.
So today we're going to talk about orders
and tradition and perception.
Beau, I'm confused about something.
I've been waiting for you to express sympathy
towards the lower ranks.
While I totally understand and support
the case against Chauvin, by tradition,
aren't lower ranks immune if they
are following a direct order?
I mean, yeah, if they were in the military
and they were on a battlefield.
Not if they're cops.
That tradition, that originated in the military.
And I say tradition because it's, we'll get there.
We'll get there.
Law enforcement is not the same as the military.
The problem is that warrior cop mentality has spread so far.
Apparently there are people who think
they are subject to the UCMJ.
They are not.
They're civilian law enforcement.
They are not the military.
Those traditions, those rules do not apply,
because it's a very different game.
At least it should be.
The comparison between police and the military
shouldn't occur, because police are supposed
to be peace officers.
The military is there specifically to not have peace.
I mean, it's there to create war.
Aside from the different objectives,
they're not subject to the same systems that are supposed
to provide accountability.
Now let's get to the tradition.
Yeah, there was a point in time where some major walked up
and was like, corporal privates, go do this horrible thing.
And they went and did it.
And then some colonel finds out about it and is mad.
They're not getting in trouble.
They aren't getting in trouble at all.
The major would get in trouble.
There was a period in time where that's how it worked.
This originated back when if the major walked up and said that
and you didn't, you could be shot.
I'm fairly certain that there's not an employee handbook
for a law enforcement agency in this country that
has that particular punishment in it.
That's where the tradition arose.
You follow the order because it maintains military discipline
and all of that stuff.
But you can't really be held accountable
because it's an act of self-preservation.
Because if you disobey a direct order,
well, it's not going to go well for you.
That's where the tradition arises.
It is worth noting that today in the military,
soldiers are taught how to disobey an unlawful order.
They get training on this.
They are told how to tell that major to take a hike.
That's not how they're told to do it.
But they receive training on how to disobey an unlawful order.
It would make sense for law enforcement
to get something similar, to be instructed on how to say,
hey, that's unconstitutional.
Hey, that's against the law.
Hey, that's going to hurt somebody.
Hey, that's excessive.
This seems like a class that should exist.
It exists in the military for wartime.
But it doesn't exist here.
And it's gotten to the point where
people believe that law enforcement is
OK with anything.
They can do anything they want as long as another higher
ranking cop told them to do it.
No.
I know that for a lot of people, you're
going to find this funny, but law enforcement isn't actually
an occupying army.
It's not supposed to be.
This is symptomatic of something that's wider.
Law enforcement wants to adopt that culture, that warrior cop
image.
They want all of the trappings of it,
but not the discipline of it.
That's a huge part of it.
They shouldn't be comparable.
There should never be a point where
a comparison between peace officers and the military
makes sense.
The fact that the comparison gets made as often as it does,
and the fact that the lines are so blurred that people are now
assuming traditions that originated in the military
now apply to law enforcement, that
should be a sign that there's a really big problem.
They're not the same.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}