---
title: Let's talk about if it would've happened with Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4xXgP8XDG0Y) |
| Published | 2022/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's absence as president contributed to the ongoing conflict between Ukraine and Russia.
- The Kertch Strait incident in November 2018 marked the first time Russia and Ukraine shot at each other directly.
- Trump's inaction during his presidency allowed Russia to act without consequences.
- Russia used a program called Passports Dissertation during Trump's presidency to justify its actions in contested areas.
- Trump's lack of response to Putin's actions indicates he wouldn't have countered Russia's moves.
- Under Trump, there wouldn't have been a NATO response to Russia's actions.
- Trump consistently undermined NATO during his presidency.

### Quotes

- "Trump's absence as president contributed to the ongoing conflict between Ukraine and Russia."
- "The first time Russia and Ukraine shot at each other directly was under Trump."
- "Under Trump, there wouldn't have been a NATO response to Russia's actions."

### Oneliner

Trump's absence allowed Russia's actions in Ukraine, with no NATO response under his presidency.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Contact foreign policy experts for insights on potential impacts of leadership changes (suggested)
- Join organizations advocating for strong international alliances (suggested)
- Organize events to raise awareness about the importance of upholding international agreements (suggested)

### Whats missing in summary

The detailed analysis and context provided by Beau in the full transcript.

### Tags

#Ukraine #Russia #Trump #ForeignPolicy #NATO


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to,
we're gonna talk about how none of this would be happening
if Trump was still president.
You know, that's a talking point that has emerged
and it emerged before, you know, things got wild.
And to be honest, I'll go ahead and admit before we start,
there is an element of truth to that
and we'll get to that at the end.
And I'll just say that's true in the beginning.
See, until very recently,
this wasn't Ukraine versus Russia,
not officially.
It was a state actor versus a non-state actor
or a non-state actor versus a non-state actor
that were backed and so on and so forth.
It wasn't until recently that Ukraine and Russia
actually shot at each other.
Of course, by recently, I don't mean the last 24 hours.
I mean, November 25th, 2018, when Trump was president.
It's called the Kertch Strait incident.
Some Ukrainian ships were selling into an area
where they were allowed to
via a freedom of navigation treaty.
And the FSB, some FSB boats, opened fire on them.
I want to say they hurt three or maybe six.
There were conflicting reports.
You know, opened fire on a ship in shared,
it's an act of war.
And then they captured a little more than 20 Ukrainian sailors
and they didn't get let go
because Russia was afraid of Trump.
That didn't happen.
They wound up going to court.
So the reality is that the first time
Russia and Ukraine actually shot at each other,
Trump was president.
You probably didn't hear a lot about it
because not a lot of people covered it.
I know I didn't because we all knew
Trump wasn't going to do anything about it.
In his entire time in office,
he didn't do anything to upset Russian national interests
with the exception of a few token responses.
That didn't really matter.
The idea that Trump kept Russia at bay is laughable.
I think another good example of that would be
what was the pretext for this again from the Russian side?
They had citizens, right, in two contested areas.
Therefore, they had to move in to protect those citizens.
But were they citizens really?
Or were they handed passports
through a program called Passports Dissertation
that occurred in 2019 while Trump was president?
Handing Russia the pretext to move in
to protect their citizens
because they hold a Russian passport.
Yeah.
Now, there is another piece to this talking point
that doesn't make sense.
The fact that like now during this period
when Trump heard about what Putin was doing,
he kind of cheered him on doesn't sound like the response
that somebody who would attempt to stop it would give.
Doesn't sound like somebody
who would be countering those moves.
So given all of this, why did I say, you know,
it wouldn't be happening?
I mean, it wouldn't be happening
because there wouldn't be a NATO response.
That's why.
He would let Russia do whatever they wanted
the way he did his entire presidency.
Remember, Trump constantly attempted to undermine NATO.
Seems unlikely that there would be a unified NATO response
during the Trump administration.
This talking point is ridiculous
and it's undermined chiefly by Trump's own words now.
As he talks about it.
He wouldn't counter it.
And to say that he would is laughable.
The first time in this entire conflict
that has been going on a very long time
that Ukraine and Russia actually went toe to toe.
That actually it was state versus state forces
was under Trump.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}