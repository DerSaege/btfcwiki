---
title: Let's talk about other ways to level the field....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QgQFdEHVEv4) |
| Published | 2022/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Delving into future technologies and their implications, specifically cyber warfare against near-peer nations like Russia.
- Kinetic options are less likely compared to cyber warfare in targeting opposition's command and control.
- The Holy Grail of cyber warfare is the ability to turn off and on the opposition's infrastructure at will, but no country has achieved this yet.
- China, Russia, and the United States have the capability to disrupt each other's infrastructure without easy reactivation.
- Cyber warfare is seen as more likely and less destructive than strategic or nuclear weapons.
- Disrupting infrastructure aims to pressure civilian populace to influence their government to stop a war.
- Cyber options are viewed as the initial step before considering more dramatic measures.
- The disruptive nature of cyber warfare offers a safer alternative than destructive methods if conflicts escalate.
- Beau acknowledges the uncertainty in predicting future events but underscores the probability of cyber warfare as a primary option before kinetic actions.
- Cyber attacks can serve as signals for diplomatic talks rather than guaranteed escalations in conflict scenarios.

### Quotes

- "Cyber warfare is more likely and less destructive than kinetic options."
- "Disrupting infrastructure aims to pressure civilian populace to influence their government to stop a war."
- "Cyber options are viewed as the initial step before considering more dramatic measures."

### Oneliner

Beau delves into the likelihood and implications of cyber warfare as a primary option before kinetic actions in future conflicts.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Prepare and strengthen cybersecurity measures to protect critical infrastructure (implied)
- Advocate for diplomatic solutions to prevent conflict escalation (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential shift towards cyber warfare in future conflicts, offering insights into the strategic considerations and implications involved.

### Tags

#CyberWarfare #MilitaryStrategy #FutureTechnologies #ConflictResolution #InfrastructureProtection


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to delve into the future a little bit
and different technologies.
The other technologies I mentioned the other day.
In a recent video, we were talking about how
things might progress.
And I said that there were other technologies that
were available that weren't available in the 80s, 70s,
and 60s.
And I said that, and it didn't go into it.
So questions, of course, came in.
So in lieu of going nuclear, there
are other things that near-peer nations can do.
Now, I'm not going to get too far into the kinetic options,
because there's a whole bunch of them.
But even those are less likely than the thing
we're going to kind of dive into, because the ability
to target the opposition's command and control
with non-nuclear weapons, all of the countries, China, Russia,
the United States, they all have that.
The thing is, they're not super likely to use them,
because if you are somebody who can make the decision
to target the opposition's command and control at home,
you're the person they're going to respond against,
because you're part of your country's command and control.
Out of self-preservation, odds are that's
going to be less likely to be used, because if you do that,
there will be reciprocity.
There will be a response.
And it's going to be directed at you as an individual.
So even though there are a whole bunch of other kinetic options,
missiles, they're less likely than what
we're going to dive into, which is cyber warfare.
If Russia felt threatened in an attempt to turn the tide,
it might engage in widespread cyber attacks
on the United States.
Now again, this is to go back to our first scenario.
This is at the point where Russia
is beginning to feel threatened that it
is going to lose control and lose territory or lose
completely.
OK?
And at that point, it might go all out with a cyber attack.
Now to my knowledge, no country has the Holy Grail yet.
The Holy Grail of cyber warfare is the ability
to turn off your opposition's infrastructure
and then turn it back on when you want to.
As far as I know, nobody's achieved that.
That's what most of the programs are trying to get to,
but nobody's really been able to do it yet.
However, all three countries do have the ability
to disrupt the other side.
They can shut it off, but they end up doing it in a way that
doesn't allow them to turn it back on easily.
So that is probably far, far, far more likely
than any kind of strategic weapons, nuclear weapons.
Now what would that look like?
Think about the pipeline that got shut down.
That's what it would look like.
It would be incredibly disruptive,
but not really destructive.
It would be designed more to get the civilian populace
of whatever the country is that's
being hit to put pressure on their government
to stop the war.
That would be the point.
I see that as just way more likely in the chain of events
that would escalate than anything else,
because that's something that doesn't necessarily
require a massive response the way
a lot of the kinetic options do.
This is something that could be done almost as a signal of,
OK, we really need to talk, rather than something that
is guaranteed to escalate it.
So I think that would be your most likely option.
Now there are still tons of kinetic options out there,
but I see them as less likely.
Now this is new ground.
We don't know that for sure.
But just human nature wise, it seems
that before any of the other dramatic stuff
would ever even be considered, there
would be cyber options would be used.
They would be uncomfortable.
It's disruptive.
It's annoying.
But it's nowhere near as destructive.
In some ways, our ability to disrupt infrastructure
without destroying it makes things a little bit safer
for everybody involved.
If things were to be destroyed, we
weren't for everybody involved, if things were to escalate
to that point.
Which again, I don't see that as incredibly likely.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}