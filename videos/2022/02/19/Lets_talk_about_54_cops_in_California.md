---
title: Let's talk about 54 cops in California....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7L_kaI3T9ZQ) |
| Published | 2022/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Prosecutors uncovered a group of bad apples within the California Highway Patrol, dating back to 2016-2018.
- Allegations include an overtime scheme, falsifying work hours, issuing fake tickets, and involvement in a bribery scheme.
- 54 cops are set to be arraigned on March 17th and 18th for a total of 302 counts.
- The public's trust in law enforcement is eroding due to discrepancies between reports and cell phone footage.
- Beau calls for a shift towards consent-based policing and reallocating funds to better-trained professionals.
- He suggests establishing a more robust oversight system to ensure accountability for past actions.

### Quotes

- "A lot of bad apples."
- "It's probably time, past time, to rethink policing in this country."
- "This is a moment for law enforcement to change."
- "Reallocating some of the funding."
- "A whole lot of bad apples to be alleged."

### Oneliner

Prosecutors uncover 54 cops in California Highway Patrol for various misconduct, prompting Beau to call for a reevaluation of policing practices and funding allocation.

### Audience

Law Enforcement Reform Advocates

### On-the-ground actions from transcript

- Advocate for consent-based policing and reallocation of funds to better-trained professionals (suggested)
- Support the establishment of a more robust oversight system for accountability (suggested)

### Whats missing in summary

The emotional impact and detailed examples can be best understood by watching the full video. 

### Tags

#LawEnforcement #PoliceMisconduct #Accountability #FundingReallocation #Oversight


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
cops some more. This time in California. A few bad apples with the California Highway Patrol,
according to prosecutors there. I was unaware of this situation when I made the Austin video,
but it all kind of ties in together. Okay, so the prosecutors there, they allege they have
uncovered a few bad apples. And it originated with an investigation into the East Los Angeles
station and activities that date from 2016 to 2018. Now it has taken this long to kind of come
forward and come to light because the few bad apples, well, they worked with some more bad
apples, according to prosecutors. And the supervisors, who would be the people who would
normally catch the bad apples, prosecutors are alleging that they were also bad apple.
They were also bad apple. 54. There were 54 bad apples, according to prosecutors. They are set to
be arraigned March 17th and 18th. 54 cops, 302 counts, $226,000 I believe. Prosecution is alleging
that there was an overtime scheme going on. They would show up to work a detail, work three or four
hours, and say they worked eight. The prosecution is also alleging that in order to make this seem
a little bit more real, that the bad apples wrote fake warning tickets and filed a report saying
that they gave assistance to drivers. On top of all of this, some of them were also alleged to be
involved in a bribery scheme dealing with gray market exotic cars as well. Prosecution is
alleging that they falsified records for them. That's a lot of bad apples. That's a lot of bad
apples. This kind of just fits right in with our earlier topic, law enforcement losing the benefit
of the doubt because the public no longer believes the reports because the reports rarely match the
cell phone cameras. And in this case, there aren't any cell phone cameras because the state is
alleging that the reports are fictitious. This atmosphere of the public just failing to give
officers the benefit of the doubt is probably going to become a recurrent theme. This is a
moment for law enforcement to change. That's what this is. In the last 24 hours, you have
news breaking of more than 70 cops getting arrested. That's wild. It's probably time,
past time, to rethink policing in this country, move towards consent-based policing, move towards
limiting the stuff that we dump on law enforcement. This is your job, even though they're not trained
to do it. Reallocating some of the funding and putting it towards people that may be better
trained to deal with that. And coming up with a little bit of a better oversight system so we're
not finding out and finally seeing accountability for actions that occurred in 2016 in 2022.
At this point, it is worth noting that these actions are alleged. In fact, they haven't even
been arraigned yet. But that's a whole lot of bad apples to be alleged. Anyway, it's just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}