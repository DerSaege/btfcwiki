---
title: Let's talk about unionizing the military and Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oUUQrMaA_Ro) |
| Published | 2022/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Explains the exception to federal troops unionizing and how troops can unionize when on state active duty.
- Mentions the hypothetical scenario of National Guard troops in Texas being called up for a political mission called Operation Lone Star.
- Notes that Operation Lone Star has faced complaints about various issues like living conditions, pay, leadership, and equipment.
- Points out that the Texas State Employees Union Military Caucus is set to meet next week, providing an avenue for National Guard troops to organize.
- Describes the ongoing disaster of Operation Lone Star and how the Texas military department has claimed helplessness in addressing the issues.
- Suggests that with the troops potentially gaining representation through the union, it might be wise for the governor to cancel the mission to avoid further repercussions.
- Mentions that those involved in organizing the union are maintaining a low profile and speaking anonymously to the press due to fears of retaliation from the Texas Military Department.
- Raises the question of whether the people of Texas will support the troops or the politicians who have failed to address the situation adequately.

### Quotes

- "Troops can unionize when they are on state active duty."
- "Operation Lone Star has been plagued by complaints about living conditions, pay, leadership, mission scope, equipment, pretty much everything."
- "It's probably a really good idea for the governor to cancel this little stunt before they get representation."
- "What's gonna happen when the people of Texas have to decide whether to support the troops or support the politicians who couldn't even keep their lights on."

### Oneliner

Troops in Texas face challenges organizing under an existing union due to ongoing mission issues, raising questions about support from politicians.

### Audience

Texans, National Guard members

### On-the-ground actions from transcript

- Attend and support the Texas State Employees Union Military Caucus meeting next week (exemplified).
- Advocate for better living conditions, pay, leadership, and equipment for National Guard troops (exemplified).
- Call for transparency and accountability from the Texas Military Department (exemplified).

### Whats missing in summary

Full context and nuances of ongoing issues faced by National Guard troops in Texas.

### Tags

#Texas #NationalGuard #Unionizing #StateActiveDuty #MilitaryCaucus


## Transcript
Well, howdy there, internet people, it's Bo Young.
So today, we're gonna talk a little bit
about the Lone Star State.
We're gonna talk about Texas.
And we're gonna talk about unionizing the military.
And a whole bunch of people are laughing right now
because you know federal troops can't unionize.
That's not a thing, you can't do that.
The thing is, there is an exception to that rule.
Troops can unionize when they are on state active duty.
Now, it normally doesn't happen, in fact, to my knowledge, it's never happened, because
when the state calls up National Guard troops, it's normally like, hey, there was an earthquake,
come help us put stuff back together real quick.
They do it and then they go home.
There's not really enough time to organize, but, hypothetically speaking, if you were
a National Guard person in Texas and you were, I don't know, activated and called up, sent
down to go on a political stunt of a mission.
We'll pretend that it's called Operation Lone Star, and Operation Lone Star has been plagued
by complaints about living conditions, pay, leadership, mission scope, equipment, pretty
much everything, and Operation Lone Star is an extended thing, pretending to have something
to do with security.
That length of time, that would give troops enough time to organize, hypothetically speaking.
And by hypothetically speaking, I mean the Texas State Employees Union Military Caucus
meets next week.
That is an existing union that the National Guard is going to organize under.
Now this mission has been a disaster of epic proportions since it began, this little operation.
The Texas military department, which is running the show, they have of course claimed there
was nothing they could do about it, it's just the way it is, this was all avoidable.
And they've been able to say that because the troops really don't have any representation,
but it seems like they're going to.
Which means it's probably a really good idea for the governor to cancel this little stunt
before they get representation.
these people go home. Because while the Texas Military Department has said that
it's unavoidable, we all know that it's avoidable. And if even half of what I have
heard about what goes on is true, this is going to sink pretty much every
everybody that supported it. Now the caucus meets next week at this time the
people involved with the organizing they are keeping they're keeping a low profile
they're talking to the press anonymously because they fear retaliation from the
Texas Military Department because, you know, their morale is really high there.
I'm wondering what's gonna happen when the people of Texas have to decide whether to
support the troops or support the politicians who couldn't even keep their lights on.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}