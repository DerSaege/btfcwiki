---
title: Let's talk about manufacturing pretexts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=X42ClNHmIaY) |
| Published | 2022/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Americans believe in inaccurate pretexts for war.
- The manufacturing of pretexts for war is not exclusive to the Iraq incident.
- Throughout American history, the U.S. has manufactured pretexts for war or drawn up plans to do so.
- The plot of The Princess Bride involves manufacturing a pretext for war.
- Other countries have engaged in manufacturing pretexts for war throughout history.
- Examples include King Gustav III in 1788, Japan in the Mukden Incident of 1931, Germany in 1939, and Russia in the Russo-Finnish War in 1939.
- Foreign policy is not about good guys and bad guys but about power and positioning.
- Encouraging the populace to support war involves manufacturing reasons for it.
- The belief that only the U.S. engages in manufacturing pretexts for war is untrue; other countries have similar examples.
- Understanding foreign policy requires recognizing that it is about power and positioning, not morality or good vs. bad.

### Quotes

- "Foreign policy is not about good guys and bad guys."
- "Just because one side is engaging in expansionist behavior doesn't mean the other side isn't."
- "Sometimes there aren't any good guys. Sometimes it's just a bunch of people with a huge appetite for destruction."

### Oneliner

Americans believe in inaccurate pretexts for war; manufacturing them is not unique to the U.S., and foreign policy is about power, not morality.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Research historical examples of manufactured pretexts for war (suggested)
- Educate others on the realities of foreign policy and manufacturing pretexts (exemplified)

### Whats missing in summary

The full transcript provides historical examples and a critical analysis of manufacturing pretexts for war, offering a nuanced view of foreign policy dynamics beyond good vs. bad narratives.

### Tags

#ForeignPolicy #ManufacturedPretexts #PowerDynamics #War #History


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about something a lot of Americans believe
that may be less than accurate.
And we're going to talk about pretexts and the manufacturing of pretexts.
Because when this topic comes up on the foreign policy scene,
Americans tend to kind of adopt this attitude of,
yeah, we know, sorry, we're not going to fall for it again.
And they think of one particular incident.
You know, they think of one time.
And that's what they kind of latch on to.
The thing is, the manufacturing of a pretext for war,
that did not start with Iraq.
That's not where that began.
If you're up on American history right now,
you're running through your head of all of the other times
the United States either manufactured a pretext for war
or drew up plans to.
We don't even have to go into that.
I don't want to talk about that.
I want to talk about The Princess Bride, that movie.
Everybody's seen that.
The plot is about manufacturing a pretext for war.
It's not inconceivable that other countries
might engage in this behavior.
It's been an idea for a really, really long time.
This isn't really something that's made in America.
But I mean, it's easy for me to say, oh, they do it in movies,
so other countries must do it.
But if you're going to say that, you
need some kind of historical evidence
to back that up, as you wish.
In 1788, King Gustav III kicked off the Russo-Swedish War.
He reached out to, I want to say, an acting troop
and got them to make him some Russian uniforms, which
he distributed to his closely trusted associates, who then,
well, they attacked themselves.
Blamed it on the Russians, kicked off a two-year war.
Didn't go so well for their side.
In 1931, you have the Mukden Incident.
This was Japan wanting to kick stuff off in Manchuria,
but they really didn't have a reason.
So they went after their own railroad.
In 1939, Germany needed a reason to go after Poland.
So the Gestapo, well, they manufactured a raid
on, I want to say, a radio station,
some kind of broadcasting station,
and blamed it on Poland.
Also in 1939, you have the beginning
of the Russo-Finnish War, which today historians basically
chalk up to the NKVD, Russian spies shelling a Russian town
and blaming it on the other side to start the war.
This was all but admitted to by Boris Yeltsin in 1994.
This isn't an American-made thing.
This isn't a thing that is solely the United States.
The US definitely does it.
Make no mistake about that.
And the Iraq example that everybody uses,
that's not the first time.
The US does it, but so does pretty much every country
that's operating on any significant level
on the geopolitical scene.
It's not a thing that only the US does.
The reason people fall into this trap
is because they look at foreign policy,
they look at situations, and they're
like, there's got to be good guys and bad guys,
because that's what we see in movies.
It's foreign policy.
There are no good guys and bad guys.
Good, bad, right, wrong, morality, none of this
has anything to do with foreign policy.
It is about power and nothing else.
And if you want your foreign policy takes to make sense,
you have to acknowledge that fact.
There are no good guys.
There are no bad guys.
It's about power, positioning.
A lot of times, money.
Countries have to engage in manufacturing a pretext for war.
Why?
How do you feel about war?
You like it?
No.
Most people don't.
Most people don't.
So the populace of any given country
has to kind of be encouraged to want to take part in one.
So nations come up with a reason.
It's not just a US thing.
The idea that the United States did this in Iraq,
therefore they're the only country that does it,
it's not true.
You could look into recent Russian history
and find pretty similar examples.
Let's see, in 1999, quite a few different places.
It's something that if this is a belief you hold,
if you are looking at this through the lens of there's
no way Russia would do that, only the US does it,
you might want to look into how Russia has gotten involved
in various conflicts since the year 2000.
Well, actually, start in 1999.
When people examine foreign policy,
they want that moralistic good guys and bad guys storyline.
It doesn't exist most times.
Understand, for those who are concerned
about being anti-imperialist, you cannot support either side
in this conflict, and it'd be an anti-imperialist take.
Just because one side is engaging
in expansionist behavior doesn't mean the other side isn't.
It's not how it works.
Sometimes there aren't any good guys.
Sometimes it's just a bunch of people
with a huge appetite for destruction.
Until you acknowledge that foreign policy is about power
and positioning, your foreign policy takes
will always be shallow.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}