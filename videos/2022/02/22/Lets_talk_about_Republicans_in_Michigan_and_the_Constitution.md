---
title: Let's talk about Republicans in Michigan and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JGuUwel7Y5c) |
| Published | 2022/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Three Republican candidates for Attorney General in Michigan were asked about Griswold v. Connecticut, known as the birth control case for striking down a law prohibiting married couples from obtaining birth control.
- The Supreme Court didn't establish a right to birth control in Griswold v. Connecticut but found a right to marital privacy based on the first, third, fourth, and fifth amendments.
- The court's decision on marital privacy was rooted in previous cases like Meyer v. Nebraska and Pierce v. Society of Sisters, which established parental rights.
- Republicans who oppose the right to marital privacy are ultimately seeking to control what happens in people's bedrooms as a way to control their lives.
- By opposing the right to marital privacy, the Republican candidates for Attorney General in Michigan are going against fundamental rights protected by the Constitution.
- Republicans' focus on undermining privacy rights is driven by their desire for control and maintaining the status quo.
- Their arguments about states' rights are essentially about giving states the power to restrict freedoms rather than expand them.
- The push against privacy rights is not just about birth control but about the broader idea of government interference in personal matters.
- Republicans' efforts against privacy rights are seen as a strategy to pave the way for anti-LGBTQ legislation and further control over individuals' lives.
- The candidates' stance against privacy rights is a deliberate move that could lead to increased state control over individuals in all aspects of life.

### Quotes

- "Republicans don't care about rights or freedom or the Constitution."
- "They want things to stay the way they are."
- "If you are concerned by the undermining of privacy in this country, this is something you need to pay attention to."
- "They want the state to have the right to determine what goes on inside your bedroom."
- "The states want to be able to exercise control over you in every facet."

### Oneliner

Three Michigan Attorney General candidates opposing privacy rights reveal a deeper agenda of control and undermining fundamental freedoms.

### Audience

Michigan Voters

### On-the-ground actions from transcript

- Pay attention to candidates' stances on privacy rights and fundamental freedoms (suggested).
- Advocate for the protection of privacy rights and constitutional freedoms in political discourse (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how political candidates' stances on privacy rights can reveal deeper intentions and threats to individual freedoms. 

### Tags

#Michigan #AttorneyGeneral #PrivacyRights #Constitution #Republicans


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about some candidates for Attorney General in Michigan
and the Constitution and the first, third, fourth, and fifth amendments
and why these candidates got the answer wrong,
because that's how it's being covered.
They were asked a political softball of a question, right,
and all of them gave the obviously wrong answer.
So we're going to talk about why.
Okay, so if you don't know what I'm talking about, three Republican candidates for Attorney General in Michigan,
they were asked how they felt about Griswold v. Connecticut.
Griswold v. Connecticut is widely known as the birth control case
because the Supreme Court struck down a law that said married couples couldn't get birth control.
That's why it's known that way.
The thing is, the court didn't find a right to birth control in this case.
So that's not really what it's about.
The court found, based on the first, third, fourth, and fifth amendments,
that there was a right to marital privacy because even back then,
in a 7-2 decision by the court, the court found, I want to say they used the word repulsive,
they found the idea that the government would be that involved in people's bedrooms as repulsive.
Found a right to marital privacy.
In finding this right in the decision, it talks about how the court has, in the past,
found rights that weren't expressly included in the Constitution.
Now to be clear on this, the Constitution says there are rights that aren't expressly written out in the Constitution.
The court relied primarily on two cases.
Meyer v. Nebraska and Pierce v. Society of Sisters.
The funny part about this is that those two cases established parental rights.
You know, the thing Republicans are running around the country screaming about right now,
using as an excuse to ban books and education in general.
So undermining this decision based on the idea that it's states' rights,
not really what they said, would also undermine the idea of parental rights in the same fashion.
But see, the thing is, it really is about that finding of privacy.
That idea that the government shouldn't be able to control what goes on in people's bedrooms
because Republicans really, really, really want to do that.
They're more concerned about what goes on there than just about anywhere else
because if they can shape your life there, well they can shape it anywhere.
It goes to prove that despite all of the talking points,
Republicans don't care about rights or freedom or the Constitution.
And coming out against this, which all three Republican candidates for Attorney General in Michigan did,
it puts them directly at odds with the first, third, fourth, and fifth amendments.
A pretty big chunk of the rights that Americans view as fundamental.
They don't care about that. They care about control.
They care about maintaining the status quo because that's the essence of conservatism.
They want things to stay the way they are.
Well, I mean, now they want to go backwards like decades.
But the point being, them coming out against this, I don't think it's an accident.
I think they understand that if they want their anti-LGBTQ legislation to stick,
well, they got to get rid of this. They may not care about birth control,
but they want to get rid of that right to privacy.
They want to get rid of the idea that what goes on in your bedroom isn't the government's business.
And they pretty much said that because they made the argument and said that, well, it's a state's rights issue.
And just like pretty much every time they use this argument, going back to slavery,
the conservative position isn't that they want the states to have the right to give you more freedom.
They want the states to have the right to take it away.
That's the position. If you are concerned by the undermining of privacy in this country,
this is something you need to pay attention to.
It seems almost impossible that three candidates would get this question wrong,
which is how it's being framed. It's being framed as if, well, they really just didn't understand it.
No, I think they completely understood it because it goes along with their objectives.
And just to be clear, if they get that legislation against that group that they've othered,
if they get that passed, this shows they're coming for you next.
They've made it clear they want the state to have the right to determine what goes on inside your bedroom.
The right to marital privacy would not exist without this.
And all three Republican candidates came out against it.
I don't think that's coincidence. I think it's the plan.
The states want to be able to exercise control over you in every facet.
And if they can get you complying in your bedroom, well, you'll comply anywhere.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}