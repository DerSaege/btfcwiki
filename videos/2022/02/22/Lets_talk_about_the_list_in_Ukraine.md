---
title: Let's talk about the list in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nncpNOH4jDo) |
| Published | 2022/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Debate sparked over a list in Ukraine, with opinions forming on its existence and purpose.
- The United States claimed Russia has a list of people to arrest or eliminate.
- Disbelief in the list's existence stems from distrust in US intelligence and modern disbelief in such actions.
- The focus of the debate revolved around the list's existence as the US didn't provide it.
- Beau affirms the list's existence, citing it as a normal part of war planning.
- Media sensationalized the list's purpose, creating inflammatory narratives.
- Lack of information on the list leads to uncertainty about its contents and intentions.
- The US hints at abnormal conduct in the list by mentioning the presence of a journalist and an ethnic minority.
- The secrecy around the list is maintained to protect those on it and prevent tipping off targets.
- The release of the list may only happen when hostilities escalate, revealing its true nature.

### Quotes

- "Does the list exist? Yes, absolutely. 100% I have not seen the list, but I am 100% certain that it exists."
- "If the news had said Russia has a high value target list, nobody would have cared."
- "Invasions are bad. Wars are bad. Horrible, horrible stuff happens."

### Oneliner

Beau breaks down the debate on a list in Ukraine, affirming its existence and questioning its portrayal in media, urging critical understanding in a world of messaging.

### Audience

Concerned global citizens

### On-the-ground actions from transcript

- Stay informed on international events and messaging (implied)
- Question media narratives and look for underlying motives (implied)
- Advocate for transparency in government actions (implied)

### Whats missing in summary

The full transcript provides deeper insights into the nature of information dissemination and the impact of messaging on public opinion.

### Tags

#Ukraine #Russia #US #Media #Warfare


## Transcript
Well, howdy there, internet people.
Let's bubble again.
So today, we are going to talk about that list in Ukraine.
As soon as news broke of this list,
people started digging their heels in, forming opinions,
many saying it doesn't exist, many using it
to justify further action.
And the crux of the debate was about whether or not the list even existed, right?
If you don't know what I'm talking about, the United States came out and said, hey,
Russia has this list of people that they're going to arrest and send off to camps or they're
going to take them out.
And for a lot of people, for many, I assume, the disbelief is because it just seems like
something that wouldn't exist in this day and age.
For some, they don't trust US intelligence.
And then for others, they're like, hey, we got to get involved.
They have this list.
But generally, most of the conversation was about whether or not the list even exists
because the US didn't show the list.
They just said that it's out there, right?
Okay, let me just start with this.
Does the list exist?
Yes, absolutely.
100% I have not seen the list,
but I am 100% certain that it exists.
And if it doesn't, Putin would be well within his rights
to fire every single one of his war planners.
Because those lists, that's just a normal part of it.
Those exist.
And this isn't something that's just limited to Russia.
Ask anybody you know who went to Iraq what their playing cards
looked like.
They had pictures of people to kill or capture.
But see, that's not how the media reported it, right?
They reported it as send them off to the camps,
take them out.
Whereas if they had just called it a high value target list,
everybody would have been like, well, yeah.
I mean, of course, that exists.
When the news came out, the way the news came out,
it was designed to be inflammatory.
It was designed to be inflammatory when, in reality,
this is completely normal as far as we know,
because we haven't seen the list.
Now the U.S., they have said things that make it seem like this list is outside of normal
conduct, right?
I think they said there was a journalist on the list.
Okay, so there's a journalist on the list.
Is it a journalist that would be actively involved in a resistance against a Russian
invasion?
Is it a journalist that perhaps has a skill set that could immediately be pressed into
an intelligent role?
There's an ethnic minority on the list.
Does that ethnic minority happen to be a general?
Without seeing the list, without knowing who is on the list, we have no way of knowing
whether or not this is either something that is so standard that the US puts their list
on playing cards or something far outside of normal conduct during an invasion.
We don't know.
We don't have that information and we're not going to get it because let's say the
US has a complete list.
They're not going to release it.
Even if they could release it in a way that would preserve means and methods, the way
they obtained the secret list, even if they could release it and keep how they got it
secret, they still wouldn't.
Because right now, the US is probably working with NATO and Ukraine to get those people
out.
They wouldn't want to put them at more jeopardy, right?
They probably don't know whether or not
they have a complete list.
Now, the Russians, they have the list,
because we know it exists.
But they're not going to release it either,
because they don't want to tip their hand to the people
they're going to go after.
They wouldn't release this information
until hostilities really began, especially
anybody that may not realize that they would be on such a list because there
are people that would be obvious targets. You know, in Iraq there was a
very unified and combined command structure between the political and the
military. So most people that found themselves on that list, they knew they
were high value before it ever started. That may not be the case here. So the
Russians wouldn't release it either.
So we probably will not find out whether this is a standard
thing or something that is far outside normal conduct until
it starts getting used.
But the key part here is to realize that all the articles
that have been written about this, nobody's pointed this
out the way the information was
crafted and released. It was released to create an emotional response. The way it was described,
they're going to send them to camps. They're going to arrest them. Yeah, I mean, I hate
to be the one to break it to you, but invasions are bad. Wars are bad. Horrible, horrible
stuff happens.
But if they had described it as a high-value target list that the United States fills maybe
outside of normal conduct, nobody would care.
This is why it's important to understand that Russia, China, and the United States are going
to be messaging on the international scene constantly.
From now on, we are in a near peer contest.
Those messages will be crafted to influence your opinion of events.
You have to be ready for it.
And this is why it's important to actually understand how this stuff works.
Because I guarantee you, if the news had said Russia has a high value target list, nobody
would have cared.
because that sanitized language wasn't used and they just accurately described
what it was. Killer capture. It was jarring. It's jarring because war is
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}