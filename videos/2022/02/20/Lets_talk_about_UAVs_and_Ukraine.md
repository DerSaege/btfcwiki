---
title: Let's talk about UAVs and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_pE4Atp15gQ) |
| Published | 2022/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses concern about the use of unmanned aerial vehicles and how policymakers may perceive them differently from ground troops.
- Raises questions about the risk and implications of using unmanned aircraft in military operations.
- Points out the importance of intelligence and information gathering in a resistance effort.
- Considers the potential for advanced technologies to influence policymakers' decisions on armed confrontation.
- Expresses fear and concern about conflicts shifting towards unmanned equipment.
- Leaves open-ended questions about the Biden administration's stance on these issues.
- Emphasizes the evolving nature of warfare due to technological advancements.
- Conveys worries about the future of conflicts involving unmanned equipment.
- Contemplates the political and ethical considerations surrounding unmanned aircraft in war scenarios.
- Wraps up by offering these thoughts as a point of reflection for the audience.

### Quotes

- "In a resistance campaign like this, intelligence, knowing what's going on, the intent of the opposition, where they're going to be, all of this stuff is just invaluable."
- "Where is that line? And are we going to allow the advanced technologies that are coming online when it comes to this type of stuff?"
- "It's been a worry of mine since all this stuff really started first coming online."
- "Y'all have a good day."

### Oneliner

Beau expresses concerns about the implications of using unmanned aerial vehicles in military operations and questions the shifting dynamics of warfare with advanced technologies.

### Audience

Military policymakers

### On-the-ground actions from transcript

- Stay informed about technological advancements in military equipment and their potential implications for conflict (implied).
- Advocate for transparent and ethical decision-making processes regarding the use of unmanned aerial vehicles in warfare (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the evolving role of unmanned aerial vehicles in military operations and raises critical questions about the ethical and political considerations surrounding their use.


## Transcript
Well, howdy there, NNH people.
It's Beau again.
So today, we are going to talk about an offhand comment
I made.
It's something I've talked about in other videos
before, specifically on the subject of this technology.
And I've always expressed it as something
that I was concerned with.
This is the first time that I can actually
foresee this conversation taking place in the Pentagon.
In a recent video about Ukraine, we were talking about the government position, the American
government position of not being involved in the actual fighting.
And I said that I believe that when it comes to ground troops, and there's a special caveat
here, when it comes to special operations, those are exempted from those statements because
those can be used anytime, anywhere, any country, peacetime, wartime, that's a different dynamic.
But I said I believed it about ground troops, but not necessarily when it comes to aircraft,
and that prompted a whole lot of questions.
In the past, I've talked about how unmanned vehicles of different kinds may prompt policy
makers to view it as having less of a risk.
there's no American life in jeopardy when those are used. Now normally when we
think about a resistance effort, the unmanned aerial vehicles, well they're in
opposition to that. Historically that's kind of how it's been done. I don't think
those in the Pentagon have overlooked the fact that they would be incredibly
useful as support for a resistance effort. You know when you're talking about
about a resistance against a modern military, mobility is life, which means you don't have
a lot of access to heavier stuff.
You have to be able to move.
So that means that you're short when it comes to heavy fire support, stuff that could be
provided by an unmanned aerial vehicle. In the US, it's going to have to weigh
whether or not it believes Russia will view that involvement as, well, it's just
equipment because there's no person in it, or whether or not it's an act of
war. In addition to what you normally think about when you're thinking about
about military aircraft of this type,
there are also unmanned aircraft
that just gather information.
And when you are talking about a resistance campaign
like this, intelligence, knowing what's going on,
the intent of the opposition, where they're going to be,
all of this stuff is just invaluable.
So the pressure, the desire to use this type of aircraft
to support Ukrainian operations is probably going to be pretty high.
Now whether or not American policymakers see the risk of Russia saying, yeah, we don't
care that there's not a pilot in it, that that was an act of war, that's left to be
seen.
This is something I think in pretty much every video I've talked about, this technology
This has been a concern of mine because when when policymakers make these
decisions, that's why they talk about no boots on the ground because politically
that risk to the American soldier, that's one of those things that people
always want to know about. They want to know what it's going to cost and when
When you're talking about aircraft that are being piloted by people who are in a relatively
safe place, that risk isn't really there.
So it may not enter into the equation the way it should.
In this case, you're talking about another world power that has a lot of capability.
They may not view that as just equipment.
They may view it as a more overt act.
And as we have the dogs that get developed or the yaks, this conversation, it's going
to take place again and again.
Where is that line?
And are we going to allow the advanced technologies that are coming online when it comes to this
type of stuff?
to allow that to shift the way we look at war, and will it make policymakers more likely to engage
in armed confrontation if there aren't human lives at stake? I don't know what the Biden
administration is going to do in this regard. No clue. I was expressing that more as a fear,
a concern than anything else. It's been a worry of mine since all this stuff really
started first coming online, because that's where it's inevitably going to go, will be
conflicts just between unmanned equipment of various kinds. And then we determine whether
or not, that really is something that's worth precipitating a full-blown conflict over.
So, anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}