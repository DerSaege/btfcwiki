---
title: Let's talk about Cruz, Hawley, and Nina Morrison....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VWKO6BtsXmI) |
| Published | 2022/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces Nina Morrison, a Biden nominee for a lifetime appointment in a US district court, with qualifications from her work with the Innocence Project.
- Morrison has freed 30 innocent people, including those on death row, showing a commitment to justice beyond the legal system.
- Senators Cruz and Hawley criticize Morrison in unique ways: Cruz links her to rising crime rates in Philadelphia due to advising on conviction integrity, while Hawley ties her to 2020 protests based on an op-ed praising an innocent man's release.
- Beau points out the irony in Cruz's argument, noting that freeing innocent people actually helps reduce crime rates.
- Hawley's attempt to connect Morrison to the 2020 protests seems baseless, especially considering his own controversial actions during the Capitol riots.
- Morrison is highly qualified for the position and dedicated to ensuring justice prevails, despite the obstructionist tactics used by Republicans to oppose her nomination.
- Beau criticizes the Republican tactics as a way to appease their inflamed voter base and manufacture opposition without valid grounds.
- Beau underscores the importance of supporting nominees like Morrison who prioritize justice and work to prevent innocent people from suffering.
- Beau reveals his support for the Innocence Project and provides a link for donations, encouraging viewers to contribute to the cause.

### Quotes

- "A person who is truly committed to justice rather than just the legal system."
- "What you have is normal obstructionist tactics from Republicans in an attempt to manufacture opposition to somebody that there's no real grounds to oppose."

### Oneliner

Beau sheds light on Biden nominee Nina Morrison’s dedication to justice and exposes Republican obstructionist tactics in opposing her appointment without valid grounds.

### Audience

Justice advocates

### On-the-ground actions from transcript

- Support the Innocence Project by donating (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the qualifications and unjust criticisms faced by Biden nominee Nina Morrison, urging viewers to support nominees committed to justice and condemning baseless opposition tactics by Republicans.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about a Biden pick,
a Biden nomination for a lifetime appointment
in a US district court.
Her name is Nina Morrison.
Now, to me, her qualifications mostly
stem from her work with the Innocence Project, which
is going to come up here in a moment.
So disclosure notice, I have donated to the Innocence
Project.
So just bear that in mind as I provide my little commentary
here.
Through her work there, she has freed 30 innocent people,
people who were wrongly convicted,
people who were exonerated by DNA evidence, some of which
were on death row, a person who is truly committed to justice
rather than just the legal system.
Senators Cruz and Hawley took issue with this in unique ways
that we're going to go over.
Cruz attempted to tie skyrocketing his term,
skyrocketing crime rates in Philadelphia,
to her advising an incoming district attorney
on conviction integrity, incidentally conviction integrity
deals with old cases and whether or not
the convictions were valid or whether the people were innocent.
Somehow, in Cruz's mind, this leads
to skyrocketing crime rates, which
is weird because if you put any thought into it whatsoever,
you would realize that if there was a crime committed
and there was an innocent person serving time for that crime,
That means that the person who actually did it
is still out doing crime stuff.
It would seem to me that it's Cruz's position that
would lead to higher crime rates, just saying.
And then Holly has attempted to tie Morrison
to the protests in 2020, because in 2019,
prior to the protest, Morrison wrote an op-ed
kind of congratulating and praising
Kim Garner for her work in getting Lamar Johnson free.
Now, it's worth noting that this case was so pronounced
that Republicans in the state changed the law
to allow this innocent person to go free.
But somehow, Holley believes that an op-ed written
about that had some contribution to the protests in 2020.
I want to point out that as far as I know,
Holley is still selling a coffee mug of himself,
has a picture of himself with his fist raised,
that famous image of him where he's cheering on the people on January 6th.
You know, the ones who like hurt a bunch of people, beat up, you know, cops with American
flags and stuff.
Guess who never gets to talk about the 2020 protests ever again?
The reality, Morrison is more than qualified for this position.
It's a lifetime appointment, U.S. District Court in New York, I want to say the Eastern
District, I think, could be wrong, more than qualified for it.
This would be an opportunity to get a judge who cares about justice, who cares about making
sure that the system functions as it should, and one who dedicated a decent portion of
their life to making sure that innocent people didn't suffer.
What you have is normal obstructionist tactics from Republicans in an attempt to manufacture
opposition to somebody that there's no real grounds to oppose.
It's just a way to try to catch votes and inflame a base that is already so inflamed
that, well, I mean, we all saw what happened.
Those are the supporters of Holly and Cruz.
Cruz and Holly and these kind of stunts, this is what eggs them on.
is what inflames them. In case I didn't say, I have donated to the Innocence
Project in the past as a disclosure notice, and there will be a link to
donate to the Innocence Project down below. Anyway, it's just a thought. Y'all
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}