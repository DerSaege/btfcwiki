---
title: Let's talk about Biden, Ukraine, and fuel prices....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rS5oSmsC4gc) |
| Published | 2022/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The conflict in Ukraine will impact fuel supplies and raise prices, as seen historically during conflicts.
- Europe heavily depends on Russian-controlled fuel supplies, making self-reliance a national security priority.
- The solution lies in transitioning to green energy, as emphasized in the U.S. Army's study on climate change implications.
- Green energy is vital for national security, reducing vulnerability during conflicts and mitigating environmental impact.
- Shifting to renewable technologies is necessary to address climate change predictions, including wars over water by the Department of Defense.
- The urgency to transition to green energy is a national security imperative, especially with limited fuel supplies during conflicts.
- Regardless of personal beliefs on climate change, the switch to green energy is vital from a national security perspective.
- Political platforms need to integrate green energy initiatives urgently, recognizing the time-sensitive nature of the issue.

### Quotes

- "Moving to green energy is a national security priority."
- "If you want to make America great, you want to make this country strong and ready for war and all of that stuff, well you got to become an environmentalist."
- "This isn't some tree-hugging liberal. This is DOD saying that wars are going to be fought over water."
- "We don't have a lot of time on this one."
- "It's just a thought."

### Oneliner

Beau says transitioning to green energy is a national security priority due to fuel supply vulnerabilities and climate change predictions, urging urgent political action. 

### Audience

Politicians, Environmentalists, Citizens

### On-the-ground actions from transcript

- Advocate for green energy policies in political platforms (implied)
- Raise awareness about the importance of transitioning to renewable technologies (implied)
- Support and participate in initiatives that focus on environmental sustainability (implied)

### Whats missing in summary

More details on the specific steps individuals and communities can take to support the transition to green energy.

### Tags

#Ukraine #GreenEnergy #NationalSecurity #ClimateChange #PoliticalAction


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about what's going on in
Ukraine, how it impacts our fuel supplies, and what the
solution is.
If you've missed it, the United States and especially
Europe, there's going to be a crunch when it comes to fuel
supplies, which means prices are going to go up. This kind of establishes the
fact that any time there's a conflict or even the perception of one in some
cases, fuel prices go up. For Europe, large portions of their fuel supply are
controlled by Russia. So it becomes a national security priority to figure
some way to become more self-reliant, to be able to rely solely on the
production capability when it comes to producing energy of your own nation or
at least your own nation and that of your allies. So what's the solution?
I mean it's really easy. In fact the Army already did a study on it called
implications of climate change for the U.S. Army. We have to go to green energy.
Moving to green energy is a national security priority. The lack of fuel
impacts us during wartime. It impacts national security. The way to alleviate
this concern is to transfer, to become better stewards of the environment, to
quote the Army. It's to become more reliant on renewable greener technologies
and stop using stuff that requires us to be in a position where we have given a
foreign policy opponent leverage. This is what has to happen. If you want to make
America great, you want to make this country strong and ready for war and all
of that stuff, well you got to become an environmentalist. It's that simple. This
is a topic we've talked about before. I have a video I'll put down below. It's
called like AOC versus DOD. I will warn you it's an upside-down patch video, so
So stick with me through the beginning, especially if you've never seen one.
Just understand sometimes I employ some creative tactics to get people to listen to something
they need to.
This contest that is playing out around the globe is a clear demonstration that not just
Do we need to shift to more renewable energies
to deal with the impacts of climate change, which
if you watch that other video, it
goes through the Department of Defense's predictions
on what's going to happen over the next 50 years.
This isn't some tree-hugging liberal.
This is DOD saying that wars are going to be fought over water.
It's a serious thing, and we have to start to counteract it now.
It is a national security priority.
The addition of limited fuel supply
during times of conflict only makes it more pressing
to address this issue.
If you don't care about the impacts of climate change,
if you don't believe the science,
it doesn't matter because from a national security standpoint,
We have to make the switch.
We have to begin to become more reliant.
This needs to be something that gets worked into a party
platform at some point.
This needs to be something that politicians are talking about.
We don't have a lot of time on this one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}