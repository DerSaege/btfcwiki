---
title: Let's talk about Arbery, symbolic victories, and ripple effects....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mrinjoHdRKQ) |
| Published | 2022/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The federal case on the killing of Ahmaud Arbery was concluded with guilty verdicts for hate crimes, revealing impactful details.
- Prosecution used racist statements and social media posts from defendants' past to establish a racist motive for their actions.
- This case's significance goes beyond being symbolic as it sets a precedent for holding individuals accountable for past statements.
- The case challenges the notion that prior guilty verdicts in state court make federal prosecution irrelevant.
- By using evidence unrelated to the event, the case may lead to a reduction in online racism due to potential criminal consequences.
- Beau predicts that the right wing may spin the case to create a chilling effect on racist speech online.
- Despite already facing life sentences, the federal case's outcome can have significant and tangible impacts on future actions and attitudes.
- The case serves as a counter to figures in authority who have emboldened racist sentiments by showing that actions have consequences.
- Beau anticipates far-reaching ripple effects from this case, extending beyond its immediate legal outcomes.
- The case message is clear: past actions and statements can come back to haunt individuals, potentially deterring future racist behavior.

### Quotes

- "There will be people who will tone down their rhetoric because they understand that they could have criminal enhancements for any actions later."
- "The case serves as a counter to figures in authority who have emboldened racist sentiments by showing that actions have consequences."
- "I think they're going to be far-reaching ripple effects from this case."

### Oneliner

The federal case on the killing of Ahmaud Arbery establishes a precedent for holding individuals accountable for past statements, challenging the idea that prior guilty verdicts make further prosecution irrelevant, with potential far-reaching impacts on online racism.

### Audience

Legal reform advocates

### On-the-ground actions from transcript

- Monitor and report online racist speech to combat its spread and hold individuals accountable (suggested)
- Advocate for legal consequences for hate speech and actions to deter online racism (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of the federal case on the killing of Ahmaud Arbery, which may not be fully captured in this summary.

### Tags

#LegalReform #Racism #Accountability #HateCrimes #OnlineSpeech


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about a case that is widely
being characterized as being symbolic, that it didn't
really matter.
It was just a symbol of showing that the federal
government was willing to get involved.
And that's how it's being characterized.
And I emphatically disagree with that.
I think they're going to be far-reaching ripple effects from this case.
And we're going to go into why.
If you don't know what I'm talking about, the second case, the federal case about the
killing of Ahmaud Arbery has concluded the defendants were found guilty of hate crimes.
It's not just that the prosecution won, it's how they won.
They used statements, racist statements from the defendant's past, not at the time of
the event, but in the past, and they used their social media posts to establish their
frame of mind and to show that their actions were motivated by racism.
That's a big deal.
I don't think that that's symbolic at all, because believe me, there are a whole bunch
of racists right now on the internet probably cleaning up their timelines.
Now in the United States, undoubtedly, you will have some people who are defenders of
the First Amendment, you know, free speech purists, who might even take up this case
on appeal.
And I think they might win in regards to some of the evidence, but I don't think they're
going to win overall.
establishes a well-known case that took statements that were previously made
unrelated to the case at hand and use them to further the prosecution of hate
crimes. I don't think that's symbolic. I don't think that's a little thing. One of
One of the reasons we've seen such an upsurge of racist sentiment is because you had a figure
in authority kind of give people permission to be their worst.
This case in many ways counteracts that.
This case says, hey, you can be your worst.
Just understand if you ever act on it in any way, all these social media posts are going
to come up.
It's a big deal.
I truly disagree with the characterization that it doesn't really matter because the
defendants had already been found guilty in state court and already had life sentences.
I don't think that this is irrelevant.
I don't think it's just a symbolic win.
This is something that sent a pretty clear message and might actually have the impact
of reducing racism online to some degree.
Because there will be people who will tone down their rhetoric because they understand
that they could have criminal enhancements for any actions later.
Because by the time the right wing gets a hold of this and starts spinning it, it will
be turned into the idea that if you say something racist and even if you act in
self-defense well they'll find you guilty because they'll feed into that
paranoia and in essence the right wing is going to propagandize this to the
point where it's gonna have a chilling effect on racist speech online. That's
that's what I see happening and I do not think that that's symbolic. I think that
That's a very tangible outcome.
So while I understand the point of view
that they were already sentenced to life,
I don't think that this case should
be discounted because I think it's
going to have some pretty far-reaching impacts, maybe
more so than the original case.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}