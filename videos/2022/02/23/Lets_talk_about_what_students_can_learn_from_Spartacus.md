---
title: Let's talk about what students can learn from Spartacus....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cPGQXFZMRI8) |
| Published | 2022/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An 18-year-old high school student is concerned about a legislation that might pass before they graduate, affecting their gay friend's coming out process.
- The student seeks advice on how to support their friend whose parents are unaware of their friend's sexual orientation.
- Beau references the movie Spartacus where individuals stand in solidarity to protect each other from harm.
- Expressing solidarity and standing up for friends can provide a sense of cover and support for those who are not ready to come out.
- Beau criticizes the harmful legislation pushed by authoritarian figures for political gain.
- Encourages being like Spartacus and standing together to protect each other.
- Beau acknowledges his lack of experience in dealing with being forced out of the closet but suggests preemptive action by close friends and parents to delay any unwanted disclosures.
- The goal is to delay the coming out process until the individual is ready, rather than being forced by external pressures.
- Beau encourages viewers to take action to support their friends and navigate challenging situations with compassion and solidarity.

### Quotes

- "If everybody's gay, nobody is, right?"
- "This legislation, it's horrible. It serves no good."
- "If you're in that situation, be Spartacus."
- "I have no clue what that [being forced out of the closet] would feel like or how to deal with it."
- "It doesn't have to be the whole school. It could just be that person's close friends."

### Oneliner

An 18-year-old seeks advice on supporting their gay friend facing imminent legislation, invoking Spartacus' solidarity as a shield against forced disclosure, emphasized by Beau's call to stand together.

### Audience

High school students

### On-the-ground actions from transcript

- Organize a group of friends to stand in solidarity with LGBTQ+ individuals facing challenges (exemplified)
- Communicate with parents to create a supportive network for friends in need of protection (exemplified)
- Advocate against harmful legislation through community support and unity (exemplified)

### Whats missing in summary

Beau's emotional connection and personal reflection on the challenging situation faced by LGBTQ+ individuals and the power of solidarity in navigating adversity.

### Tags

#HighSchool #Support #Solidarity #ComingOut #Legislation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about...
We're going to talk about a high school student who sent a message.
Eighteen years old, last year in high school,
should be getting ready to go out and see the world,
experience everything. Should be a pretty exciting, fun time for them.
Instead, they're trying to figure out how to help their friend,
because there's legislation in that state that looks like it might pass.
And the concern is that it's going to pass and be implemented
before they get out of high school.
Because while the person who sent the message isn't gay, their friend is.
Teachers at the school know their friend is. Their parents do not.
So, the message is asking for advice on how to help their friend.
When this happens, when this is pushed out into the open,
before they're ready for it to be out in the open.
They're looking for those tools. And I'm going to be honest, I don't know.
I don't have a clue how to do with that. I don't.
But, I would imagine a big part of it would be expressing solidarity.
Letting that person know that they're not alone.
I think that'd be a really big part of it.
There's an old movie called Spartacus.
There's a scene in the movie, without getting too far into it.
There's a bunch of Romans and they've captured some prisoners.
And they're like, hey, y'all are in for a world of trouble.
Y'all have some pretty severe punishments coming your way.
We'll let all that go as long as you single somebody out for us.
As long as you tell us who Spartacus is, then, you know, we'll let everybody else go.
And just that person will be singled out.
Spartacus doesn't want all of his friends to be punished as well.
So he stands up and he says, I'm Spartacus.
Then the person next to him stands up and says, I'm Spartacus.
And then the next, and then the next.
And then they're all standing up saying that they're Spartacus.
Maybe you can draw some inspiration from that.
See, this is one of those things where if there are too many students who are gay,
the school may just decide to kind of blow it off.
They may not make those calls.
And let's say you're not somebody who's incredibly social,
or you can't get other students to be Spartacus.
Even if it's just your circle of friends, if you all say that,
and all of the calls get made, and all of the parents get informed,
well, that kind of gives your friend a little bit of cover.
If that person, if your friend, is not ready to disclose that yet,
they could just say they were expressing solidarity as well.
This legislation, it's horrible.
It serves no good.
It doesn't do any good whatsoever.
It can only cause harm.
But a bunch of authoritarian people think this is the way to get votes,
so they are probably going to pursue it.
If you're in that situation, be Spartacus.
I mean, if everybody's gay, nobody is, right?
As far as the tools to deal with being forced out of the closet,
I don't have any advice on that.
I have no clue what that would feel like or how to deal with it.
I am certain that there are other people on YouTube
who probably have a better take.
But if it was me, I like to solve problems before they start.
And I think you probably have it within your power to do that,
because it doesn't have to be the whole school.
It could just be that person's close friends,
that person's parents know who their kids' friends are.
And if all of them did it, they might get a lecture
about jumping off a bridge or something like that.
But it can put off that conversation until the student is ready to have it,
rather than when a whole bunch of politicians think it should be had.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}