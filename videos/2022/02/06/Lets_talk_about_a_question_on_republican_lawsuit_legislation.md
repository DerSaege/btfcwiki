---
title: Let's talk about a question on republican lawsuit legislation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QbUfrPqCcu4) |
| Published | 2022/02/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the Republican habit of creating legislation allowing people to sue each other in cases where damages wouldn't normally exist.
- Provides examples to illustrate the potential consequences of such legislation, such as holding someone accountable for leaving keys in a stolen car that causes harm.
- Argues that the civil system already covers damages and penalties, making additional legislation unnecessary.
- Points out the flaws in assuming that this legislation will be used wisely, citing past examples where it was not.
- Raises concerns about the inequality that monetary penalties create, making justice a privilege for the wealthy.
- Warns against making the justice system about money, as it allows the wealthy to circumvent consequences.
- Advocates for changing behavior through education rather than coercive measures like monetary penalties.
- Emphasizes the danger of using legislation as a tool to target marginalized individuals who lack resources to defend themselves.
- Expresses skepticism about the effectiveness of legislative changes in influencing behavior, suggesting that education is a more sustainable approach.

### Quotes

- "If you have a system that is based on money, the way this legislation proposes, you create two sets of people."
- "You cannot make the justice system about money."
- "It's not a penalty, it's a fee to do whatever they want and get away with it."
- "In an ideal world, changing behavior is done by convincing through education, not coercive means."
- "This is a tool to kick down those who don't have the means to fight back."

### Oneliner

Beau criticizes Republican legislation allowing frivolous lawsuits, pointing out flaws in enforcement and advocating for education over coercive measures in changing behavior.

### Audience

Legislators, activists, community organizers

### On-the-ground actions from transcript

- Challenge legislation that allows frivolous lawsuits (implied)
- Advocate for education-based approaches to behavior change (implied)
- Support marginalized individuals who may be targeted by unjust laws (implied)

### Whats missing in summary

The full transcript provides in-depth analysis and examples on the dangers of legislation enabling frivolous lawsuits and the implications for justice and equality.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk a little bit more
about the Republican habit of now carving out
legislation that allows people to sue each other in cases
where they wouldn't normally have damages,
so they wouldn't normally be able to sue.
We talked about it recently in a video,
And I got a message about it.
It says, I failed to see an issue.
What's wrong with suing a person over their self-induced
inability to curb their behavior?
What's the problem with issuing fines?
If I leave the keys in my car and go run into the store
and the car is stolen and the thief kills people or damaged
property with that car, why shouldn't I be sued?
Now apply that logic to toxic waste recycling.
Apply it to guns, every gun that isn't sufficiently secured.
Apply that logic to schools, public education, failing
to teach all kids.
Apply that logic to the financial system, MBS scandal.
There is only so many ways to enforce behavior.
You can't jail everyone.
And even if you could, you'd just make them a martyr.
If you lie and say that you've been vaccinated
but become the source of an illness that wipes out
8th grade class, why shouldn't there be a penalty?
Okay, well a lot of this is actually covered because there are damages.
There are real damages that can be assessed.
They're already covered in the civil system.
And I don't think anybody's saying that there shouldn't be a penalty.
It's that we shouldn't carve something out to allow people to sue each other and create
a bounty system.
aren't fines. The money doesn't go to the state. The money goes to whoever says,
oh, that person over there did it. So it's not a fine. Let's get that out of
the way. Now the other thing that's going on here is you're presupposing that the
legislation is going to be used wisely. We have no indication that that's the
case. In fact, we have the exact opposite. The only one of these that there's something
comparable to is the public school thing, when the legislation is actually geared to
making sure that teachers don't teach the kids. It's designed to put a chilling effect
on what they teach. And the idea of toxic waste and all that, that sounds good, but
If there are real damages, that's already part of the civil system.
And if you're just talking about lesser stuff, you would have to convince the legislature
to carve out an exemption to sue big business in this manner, which, considering the donations,
that seems super unlikely.
So you're presupposing that it would be used wisely.
We have no evidence to suggest that's true.
But let's just pretend that it is for a second.
Let's go to the next thing.
Let's say, okay, boom, they have used this legislation wisely.
It's only for positive stuff.
And there's some kind of guard to make sure that it doesn't get used in a negative manner.
So now these cases go to the civil court system, which determines right and wrong, right?
Come on, pretend we live in the real world.
What does the American court system typically determine?
Who has the most resources and the best lawyers?
So even in a case where legislatures use this wisely, you still have the problem with the
way the court systems work.
And let's just say that we fix that.
We fix that.
So the legislatures use it wisely and the court system works.
It works really, really well.
Then what do we have?
We have a system that has created go-fast licenses.
When I was in college, I got pulled over.
I was speeding.
I was speeding a lot.
Got pulled over, and I got a ticket.
It was a couple hundred bucks.
And this was a long time ago.
A couple hundred bucks was a lot of money.
Anyway, I got to class, and this guy that I knew,
he heard me talking about how I'd gotten a ticket.
This guy, his dad, was rich.
And I don't mean like a little rich.
He was a real estate developer.
Dude was loaded.
The only reason this kid was going to the same college I
was going to is because he didn't have the grades to get
in anywhere else.
But something he said stuck with me forever, because he said,
oh, you had a ticket.
Yeah, got to go pay for your GoFast license.
What?
If you have that kind of money, you don't view it as a
penalty.
You view it as the fee to do whatever you want.
It was a fee to drive as fast as you wanted.
That's the way he looked at it.
It was wild.
If you have a system that is based on money, the way this would be, what you do, especially
in a country where there's a whole lot of income inequality, is you create two sets
of people.
Those who have to abide by these rules or end up destitute, and those who can completely
ignore them because they just pay the license.
They don't view it as a penalty.
They view it as the fee to do whatever they want.
As an example, that one in Texas, dealing with family planning, $10,000, yeah, for most
people, that's huge.
I mean, that's something that many people wouldn't be able to afford.
What about the rich politicians that put this in play?
If their mistress comes to them or calls them and says, hey, we've got an issue, you think
they won't just go ahead and do it?
They can afford it, it puts them above the law.
If you have a society like the one that we have, you cannot make the justice system about
money.
If you do, there will be people who are totally above the law, even the way it is now.
You still have that because of the resources that are at wealthier people's disposal, the
ability to run out the clock, the ability to get the best lawyers, all of that stuff.
If this is what is used to influence behavior, they're exempt, it's not a penalty, it's
a fee to do whatever they want and get away with it.
In an ideal world, when you're talking about changing the way people behave, it's probably
done by convincing them through education, not through some kind of coercive means. It's
done by changing thought. If you change thought enough, you don't even need to change the
law. And never forget that when it comes to stuff like this, even if you do have a legislature
in one state that would use it wisely.
Somewhere else, it's pretty likely that, well, you're going to get a $10,000 lawsuit because
you exposed children to something they shouldn't be exposed to with that rainbow flag bumper
sticker on your car.
This is a tool to kick down those who don't have the means to fight back, those who, because
of their life and the way they live it, maybe if they're in an area that isn't really
accepting of that, the type that would pass legislation like that, you think they do well
in court? Probably not. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}