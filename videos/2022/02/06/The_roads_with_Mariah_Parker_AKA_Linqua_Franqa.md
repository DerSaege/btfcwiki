---
title: The roads with Mariah Parker AKA Linqua Franqa....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YFRTSDY9UIE) |
| Published | 2022/02/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Mariah Parker, also known as Lingua Franca, a County Commissioner in Athens-Clarke County, Georgia, who is a hip hop artist and nearing completion of her PhD.
- Mariah talks about her multifaceted roles as a public servant, artist, and scholar, including her recent re-election, upcoming album release, and advocacy for workers.
- She shares the inspiration behind her song "Work," which references the labor movement and calls for unity among workers.
- Mariah delves into the themes of her upcoming album, touching on historical and international movements for liberation and personal struggles.
- The interview covers Mariah's unique swearing-in ceremony with Malcolm X's autobiography and her priorities as County Commissioner, focusing on labor rights and collective bargaining.
- She expresses the need for direct action and community involvement to address issues like evictions and economic exploitation.
- Mariah reveals her plans for a potential freedom school and stresses the importance of using music and education for social change.
- The discussion extends to Mariah's work on police accountability, her reflections on the prison system, and her call for more creative forms of collective action.
- Beau inquires about Mariah's experiences as a new parent and her upcoming projects, including her podcast and album release.

### Quotes

- "I think using music as a tool to educate."
- "I just encourage people to think creatively about how that could be done."
- "You can use the master's tools to unmake the master's house."
- "I have a feeling that this is gonna take off in the sense that there's gonna be a coordinated push this year for organized labor and collective bargaining."
- "I hope that was enlightening and entertaining."

### Oneliner

Beau interviews Mariah Parker, a County Commissioner and hip hop artist, discussing her multifaceted roles, advocacy for workers, and plans for a potential freedom school to fight for collective liberation through music and education.

### Audience

Activists, Community Members

### On-the-ground actions from transcript

- Support worker organizing efforts in your community by encouraging collective bargaining and the formation of worker-owned cooperatives (implied).
- Engage in direct action to protect individuals facing eviction by showing up and preventing their displacement (implied).
- Take part in creative forms of collective action, such as blocking eviction proceedings or protesting against harmful development projects, to address urgent social issues (implied).

### Whats missing in summary

A deeper exploration of Mariah's journey into politics, music, and academia, along with her emphasis on embracing personal struggles to drive meaningful change.

### Tags

#CountyCommissioner #HipHopArtist #LaborRights #CollectiveAction #CommunityOrganizing


## Transcript
Well, howdy there internet people, it's Beau again.
And today, y'all are going to probably hear me
eat a bunch of cough drops,
cause my throat is sore.
And we are going to be talking to Mariah Parker,
AKA Lingua Franca.
Why don't you tell them a little bit about yourself?
Sure, so yeah, my name is Mariah.
If you see me at city hall,
I have been serving as County Commissioner
in Athens-Clarke County, Georgia since 2018.
Y'all might remember me from swearing in
on the autobiography of Malcolm X at the age of 26.
But I got reelected last year,
so I'm still in there doing my thing,
repping for workers, fighting the cops.
And then you may also know me as Lingua Franca.
I've been making hip hop music since about 2016.
I've toured all over the country
and got a new album coming out in April
and released my first single, Work, last week.
Just a call to frontline workers everywhere
to fight the bosses and get what they deserve.
And in addition to that, I'm working on my,
I'm almost done with my PhD in Language and Literacy
Education at the University of Georgia.
So this album I have coming out in April
is actually my dissertation for completion of that degree.
And I have a little five month old baby.
So yeah, that's, I think pretty much all I do.
Oh yeah, I got a podcast, Waiting on Reparations.
But yeah, that's it, like for real, that's it.
That's everything.
That's everything.
You don't have time to fit anything else
in that schedule, huh?
I mean, I don't know.
Except talking to Bo, you know,
you gotta make time for Bo, what's up?
I am.
Okay.
So your song, Work, I saw it, I loved it.
The video is something else.
So we'll have a link to it down below
for those of you watching this.
But it seemed as though I heard quite a few little
subtle references to different points in the labor movement.
So why don't you tell us a little bit
about the lyrics of the song?
Because I love the historical elements in it.
Sure, so I mean, I started out the song
by shouting out my own union,
United Campus Workers of Georgia,
which is a local under the Communication Workers of America.
So I'm repping for the 3265 to get it started.
I mean, a lot of what I'm talking about
is actually very relevant to like the pandemic.
Like, you know, people getting spit on at the hospital
and they're just trying to treat people for COVID.
Essential workers that are like, you know,
pushing the shopping carts across the Walmart parking lot
and, you know, smiling at the cashier, how can I help you?
Even though like a lot of us are like drowning out here
in various ways.
And so ultimately though, the hook is like a,
is, you know, a refrain from a very popular,
just historically popular labor song.
You know, going back to the 1930s,
if you've seen documentaries like Harlan County, USA,
it comes up a lot, you know, just again and again,
it's been a refrain in history for the labor movement.
So I got a shout out to that, as well as, you know,
we got a little chant in the middle,
shout out my Latino brothers and sisters,
you know, calling folks into the fight,
ultimately trying to like, through hip hop,
through refrains from labor movements throughout history,
through, you know, shout outs to Latino folks
trying to bring together like that multiracial
working class movement.
I talked about, I think it was last year,
maybe a year before that, 2021,
it feels like it didn't happen.
Like 2020 is still last year, right?
So it might have been 2020, but, you know,
Amazon, Target, FedEx, Walmart, you know,
they were calling for a boycott.
And so in the song, I'm calling for, you know,
those folks to actually organize and come together.
I'm talking about the amount of people
who like joined like the democratic socialist America
in the aftermath of like the Bernie 2020 campaign.
So I'm talking about like, if you sad about Bernie,
you should get in there and, you know,
organize your workplace and various stuff like that.
And there's probably more I can't think of
off the top of my head.
Cause rapping, it's so much muscle memory.
I like, don't even, I can't even remember to tell you
what the lyrics are.
It's just like fully physical kinetic, but yeah,
I try to bring in a bit of labor movements,
old and new across racial lines.
Cause that's what we need honestly to win.
Oh yeah, no, hearing a reference to Harlan County
in the same song with the Spanish lines of, you know,
the people united.
I mean, that was something else that was really cool.
So, I mean, the song is definitely a call to action.
What's the rest of the album?
Like I haven't, I don't even know if I can get it yet.
Is there more to it?
What's coming?
Yeah, so I have another single coming out in this month.
Oh Jesus.
Yeah, so pretty soon.
So that I'm talking about Latasha Harlins,
who is a misremembered or under remembered
catalyst of the LA riots in 1992.
So kind of talking about, I mean, in inadvertent ways,
the way that black women are often erased from, you know,
the stories of uprisings, you know,
today we see Breonna Taylor or, you know,
in the same era as Trayvon Martin,
there was a little girl named Ayanna Stanley Jones
that had been shot and killed
and that didn't really make the news.
So giving a shout out to a long ago martyr in the movement
that people might not remember
and talking a little bit about that uprising.
I get into international movements for liberation.
So I'm talking about the Haitian uprising.
I'm talking about what's going on in places like Myanmar
or shouting out folks like,
Toussaint Louverture and Che Guevara,
Thomas Sankara, who, you know,
who's the president of Burkina Faso in the 1970s,
Nigeria, how they're fighting, you know, police brutality.
So sort of trying to put the Black Lives Matter movement
in the context of like the global struggle
against prison industrial complexes of various kinds
and other sorts of oppressive forces.
And a lot of it's just autobiographical,
just talking about like my own personal struggle
for liberation, you know, as a person with bipolar disorder,
who's like just gone through a lot of emotional turmoil
in my life, how I personally found my place in the movement
after kind of not even knowing what my place was
on earth period.
So it was a little bit of various movement related things,
as well as how I came to them.
So, yeah.
All right.
So as far as your place in the movement,
one of the things like the, when you were sworn in,
I've got to know what prompted that move?
Because I mean, that was something that,
I mean, that doesn't seem just like a normal political stunt.
You'd already been elected, you know?
Yeah, yeah, yeah.
I'm curious as to what prompted it
and I mean, where that idea came from?
For sure.
So, I mean, as a political novice,
like I had just gotten into politics like a year before,
I didn't have a lot of political connections when I ran.
I like didn't even know what a county commissioner was
like a year before I even decided to run.
I honestly think to a degree,
like my choice in that was somewhat political naivety
in that they were like, oh, you know,
let us know what you want to be sworn in on.
So I was like, oh, people just choose
what they want to be sworn in on all the time.
That's so cool.
Not realizing that most people are more traditional.
They go with the Bible, the constitution,
something like that.
I thought it was like legitimately giving me a choice
such as my ignorance about political processes.
And also in a small town like Athens, Georgia,
we got like 130,000 people.
You know, I picked a text that was personally important
to me, you know, Malcolm X and his struggle
from, you know, a life of crime and drug abuse
when he was younger and then his incarceration
and using that period of solitude to really study
and then going out and being like a fearless,
sometimes brash voice for black liberation,
as well as the transformation he underwent
in his later life where originally he had a lot of hatred
for white people, but then he went to Mecca
and saw Muslims of all different, you know,
shades and national origins and started to think more
about the ways we can all come together to fight racism.
And really all these steps along his path as a person
with a somewhat untraditional background for politics
really resonated with me.
So I just picked that book, cause I was like,
you know, this text means a lot to me.
Not realizing that it was going to cause the calamity
that it did at the time, but it really,
that moment sort of just highlighted a lot of things
about me for others that I didn't really think were special.
You know, being 26 years old at the time,
being, you know, having an Afro, being a rapper,
you know, being the first queer elected official
in the city's history.
So I don't know, a lot of things converged in that moment,
but ultimately I just picked a text that was important to me
just to send a signal to my immediate community that,
you know, the little Malcolm X's out there,
who's like first memories of life or of racial violence,
like I'm gonna rep for them,
no matter what their background is,
if they've been incarcerated,
if they've been through drugs, just like he was.
Love it.
All right.
Okay.
Um, I, yeah, it is definitely the thing that
puts you on more of a national level.
Like even people who may not recognize your name,
Yeah.
They'll know the person who was sworn in
on Malcolm X's autobiography, you know?
Yeah, maybe, yeah, probably.
I don't know.
And I think it's interesting that it was an accident,
that it wasn't like, it wasn't a calculated move.
Absolutely not, yeah.
So as County Commissioner, what are your priorities?
What are you trying to get done other than ratio Kemp?
I saw that the other day.
Yeah, yeah, Brian Kemp, I'm coming for you, man, my guy.
So as County Commissioner, honestly,
I was sort of sitting down at the start of 2022 to like,
like a catalog, all that had transpired last year
to sort of like take stock and feel proud of myself
because I'm super hard on myself all the time.
And I realized that I have accomplished everything
that I ran on in 2018, fair, free public transit.
We've passed a non-discrimination ordinance
to protect people from discrimination in housing
and businesses, criminal justice reforms,
affordable housing, getting sidewalks and bike lanes
and all this kind of stuff,
support for minority owned businesses.
I was like, snap, I got to like really think forward
into like what unforged paths,
like we got to like trailblaze
and chop all the weeds out of.
So, as over the course of the pandemic,
I've been thinking more about labor exploitation,
the way that essential workers have been thrown
under the bus to save the economy.
And folks don't have healthcare, people getting kicked out.
I've had so many constituents get evicted
during a public health crisis for somebody's profit motive.
And thinking more about what I can do
as a County Commissioner, like at a very small local level
to help support mass action,
particularly in the form of collective bargaining
and organized labor.
And so in that regard,
we got this economic development department
where they're always all the time
trying to draw big corporations here.
And big corporations would come to Georgia
because we are a right to work state,
like all this union busting stuff, like in the state laws.
So they love it here, they love it, they make millions.
And so thinking about how we as a local government
can encourage workers to organize
and also just like buy out businesses from their bosses
and just make them work their own co-ops.
So they don't even have to worry about a boss
not paying them enough to survive
or denying them paid sick leave.
And so, a lot of what I started thinking about around this
is like, one thing I didn't finish,
one thing I didn't do from my platform
was raise the minimum wage.
And that's because it's bad, like state law, we can't do it.
You know, we just can't, it's 7.25 an hour.
And I started to realize though,
that if like people in their workplaces came together
and like said like, yo, we walking off the job,
we leaving, we want 15 an hour, we want 20 an hour,
we're not coming back, we shutting your whole thing down
so we get it, folks would actually get a living wage
for themselves faster than I could as an elected official.
And that's true of so many things
that impact people's, you know, livelihood.
You know, we're waiting on Congress to pass a minimum,
you know, $15 an hour minimum wage,
but waiting on Congress to pass like Medicare for all.
But like workers go out here and they tell the bosses like,
yo, we leaving, unless you put us on your insurance plan,
like we leaving unless you bump our pay up,
y'all can win it faster than Congress can.
And so, thinking about how my role then
is to help support people in doing that.
So, supporting worker organizing,
supporting the formation of worker owned co-ops
through like our economic development department.
It's something that like I'm really into right now
and also something that sort of comes across in my new song.
And I could talk on and on and on about local politics,
but you know, that's just one thing
that is relevant to our current conversation.
It is, what, okay, so what's your doctorate?
What's it gonna be?
So, I'm getting my doctorate
in language and literacy education.
So, I'm a linguist in background and in studying linguistics,
I was like, I really wanna share this love
of the science of language with people.
So, I decided to get into the education side of it.
And then ultimately, I feel like that's what I also do
through music is like, yo, look at all this crazy stuff
that you can do with like language,
but also education in the sense of like civic education.
Like, yo, this is like, I'm gonna drop some bars on you
that are gonna like teach you something about metaphor,
about rhyme, but also about organizing a workplace
or also about like what prison abolition is
or something like that.
So, rap music also factors a lot into what I study.
Okay, okay, earlier you said,
you talked about finding your place in the movement.
Yeah.
How did you actually go about that?
And what advice can you give to people
who are trying to do that now?
Because that's, you know, a lot of the people
that are gonna be listening to this,
they're trying to find their spot.
They wanna make change, but they don't really know
how to go about it and where they can fit in best.
For sure, so I mean, with my case,
literally like the dude who was my commissioner,
like he served 25 years, like ran unopposed the whole time.
He stepped down and this dude who was running to replace him
was also running unopposed.
And so it was a moment where I was like,
is anybody gonna do something?
Anybody, nobody?
Well, I guess I have to, I can't just stand here
and like let like democracy crumble before me
and like not giving people a choice of who to vote for.
So I jumped in there, I jumped in.
And so I think a part of it is just seizing moments
where you don't see anyone acting and it just has to be you.
Like no one is coming to save you,
but you is sort of a way that you find your place
in the movement, even if you fail,
if you jump in and it's like, well, that didn't work.
That's a lesson.
So now you can cross it off your list
and keep jumping into new things
until you find something that feels good and works.
Another part of finding your place, I think,
comes with recognizing, for me,
it was a lot of just recognizing that the things I thought
held me back from serving were actually things we needed
in our leadership.
So like, I've struggled with drug addiction.
Like I said before, I got bipolar disorder,
queer person, all this stuff.
And I was like, ah, and I'm a rapper.
I was like, and no one's gonna ever elect me.
I have nothing to offer the world, whatever.
And then realizing like no one has ever legislated
from a place of knowing what it's like,
or very few people have legislated from a place
of knowing what it's like to not be able to afford
your antidepressants or from a place of the necessity
of mental health care and that,
I've been suicidal before.
I know what it's like if you don't know,
you're scared to go see a doctor
because you can't afford it.
All sorts of things that I thought counted me out.
I mean, even I worked in the service industry
before I did all this.
And so understanding what it's like to get paid
under the table, less than a minimum wage,
and then go out drinking at the bar where you work
until three in the morning and just doing that
over and over again to like kill your pain, et cetera.
So no one is making policy from that standpoint
of actually understanding how this impacts working people.
And so I think finding my place also required
just accepting the messiness of like just who I am
and reframing it as like, oh, actually,
like I think people actually dig the fact
that I'm like weird and messy like them.
Like they see themselves in me.
I see myself in them.
And so embracing that and whatever you do,
if you feel like, oh, I can't find my place in the movement
because I've been to jail once or like I'm a sex worker,
or whatever, it's like actually people really need that.
They really need to see leadership modeled
among those kinds of folks.
So actually that's a reason why you should definitely
jump into some different stuff and try to find what works.
Love it, love it.
I can actually see how you got elected.
It's good stuff.
I got reelected last year too, what's up, hey.
So where are you going from here?
I mean, because I mean the music,
I have a feeling that this is gonna take off
in the sense that there's gonna be a coordinated push
this year for organized labor and collective bargaining.
And you kind of released the theme song in January.
Great timing.
But what are your goals?
Where are you headed?
I mean, where are we all headed?
I'm just trying to fight for collective liberation.
I've been thinking a lot about what that means.
If public office is the best way to do that
in that the system that I'm governing in
is made up mostly of rich white people
and was founded by rich white people.
And so I'm not sure if you can use the master's tools
to unmake the master's house,
I think in the words of Audre Lorde.
And so I'm thinking about starting a freedom school
like they used to have down in Mississippi in 1964,
where you train people to organize
and then you materially support them in doing so.
Teach them about all the different ways to engage civically
and like, all right, go forth.
What do you need help with?
What you trying to fight for?
And things like that.
I mean, I think using music as a tool to educate.
You know, on Instagram, I post raps all the time
about collective debt strikes
and rent and mortgage cancellation
and prison industrial complex and all these various things
and like continuing to do that.
You know, I study education,
but I also think of myself as an educator
because I'm out here doing it
and would love to focus more on that.
So I don't know, I would like to like write a book.
I've always wanted to be a writer.
I am a writer and then I'm a rapper,
but like that would be cool one day.
But I don't even know what to say
about everything that's happened in the last five years.
So that's in my horizon at some point,
but ultimately whatever I do,
putting it in service of getting everybody free.
So wherever that takes me, I'm cool with.
I love the idea of a school.
Yeah, like that's one of my things.
You know, one day when we've got the funding,
we're totally setting up something like Highland.
Yeah.
Yeah, I think that that is desperately needed
because there's a lot of people that have a lot of passion,
but sometimes they need some finer points to go with that.
So let's see, we've got the labor movement.
You also do a little bit of police accountability work,
I think, right?
I do, I do.
Yeah, so I was very involved in the uprising in 2020.
And even before that, back in 2018,
I read Angela Davis's, Our Prison Obsolete,
and started thinking about like why we have prisons.
What function do they really serve in society?
And around that same time,
my aunt got murdered by my cousin.
And so he was 19 going to jail,
probably for the prison for the rest of his life,
and thinking a lot about how that didn't,
he wasn't gonna get any better in there.
My family didn't feel any better about it
knowing he's going to prison forever.
He almost got the death penalty,
and we're like, yo,
they're going against the wishes of the family
because that wasn't gonna make him any better
if he was executed, like what?
And so thinking really deeply about all of the dominoes
that had fallen to get him and my aunt to that point,
like he didn't grow up in a rural area,
but they didn't have internet or trash pickup,
let alone good schools or job opportunities
and how that led him to never get help
for his mental illness, get into drugs,
end up having to go to the military
because he couldn't go to college
and how that trained him on how to use a lot of weapons
and da, da, da, da, da, here we are.
And so thinking a lot about like,
man, if we just took care of people,
like things would be so much better.
Like we wouldn't be in the situation
my aunt would be here today.
And so that's really informed in the years,
many years since then,
just thinking about like, how do we take care of the people
and like, and lessen our dependency on like the police
and like the prison industrial complex generally.
All right.
Okay, so something I ask everybody that comes on,
if you could tell people one thing
on like how to make the world better, what would it be?
What piece of advice, I know putting you on the spot.
One thing that the people can do, one thing that people do.
I would like to see more direct action.
People think about direct action
as like going to a protest and marching.
But I think about the folks around here
that are getting evicted.
And what would it mean if folks showed up and said,
no, you are not putting this person in the street.
Or when somebody is tearing down your neighborhood
to build luxury high-rises,
what if you showed up and said, excuse me,
please move aside, I'm gonna chain myself to this bulldozer
and tell y'all, no, you're not gonna do it.
I mean, these are not novel ideas.
We see these actions taken with like water protectors
and land protectors, protecting the environment.
We've seen cases of like folks down here in the South
shutting down eviction proceedings at the magistrate court
by just like, blocking the entrance and mass.
And so I think that people get really like disheartened
by the slow moving machinery of government
and like waiting around for the government to do something.
But there's so many forms of collective action we could take
that like help in the immediate term,
but also raise the issue of like,
we really need to address this problem with urgency.
And so I just encourage people to think creatively
about how that could be done.
All right, and then last question,
just because I'm curious, five months, you said?
Yeah, I got five months old, yeah.
How's that going?
It's going pretty good.
It was hard at first, it still is hard sometimes,
but there's a lot of joy with it.
And I actually have a song about him on my new album.
So on my old album, my first album,
I had a song called Eight Weeks
that was about the tough decision to have an abortion.
And as a feminist, like being shocked by like,
oh man, actually this is way more complicated than I thought.
And then I got a song on this new album
that's about like being excited about being a parent.
Woo, this is so exciting.
And they're like,
actually there's all these obstacles in between.
You might see like the cute ultrasound pictures
on Facebook or whatever, but then you got medical bills,
you not sure about this and that.
And so I got a song on the album about him
and it's really cool.
It's challenging, but it brings a lot of joy
and a lot of more urgency to the fight.
Cause like I gotta leave him a good world
and make him a good person to keep his fight up
when I'm not here, so.
Right, right.
Okay, plug all your stuff.
Tell us what your podcast is, all that stuff.
I forget to do the self-promotion stuff sometimes.
So yeah, you can follow me on Twitter, Instagram,
TikTok at Mariah for Athens.
You can go check out the music at bit.ly
slash W-U-R-K, work, video.
Go check out the music video, go pre-order the album.
The album's coming out April 22nd.
You know, if you follow me,
you might see I'm coming to a city near you, playing soon.
And then also you can check out my podcast,
Waiting on Reparations.
It comes out every Thursday.
We talk about hip hop and politics.
So hope to see you there, hear you there.
You hear me there, you know.
Wonderful.
Okay, all right.
Well, we're gonna wrap it up.
That's it for the day.
Everybody at home,
I hope that was enlightening and entertaining.
A lot of those links are gonna be down below.
Definitely watch the video.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}