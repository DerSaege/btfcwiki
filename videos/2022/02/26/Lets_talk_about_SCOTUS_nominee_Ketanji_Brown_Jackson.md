---
title: Let's talk about SCOTUS nominee Ketanji Brown Jackson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dkizuImYbrk) |
| Published | 2022/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Ketanji Brown Jackson as President Biden's nominee for the Supreme Court.
- Mentions her educational background, including going to Harvard and writing a thesis on the plea bargaining system.
- Notes her personal connections to the criminal justice system, with an uncle sentenced to life for a non-violent substance crime.
- Acknowledges her family ties to law enforcement, which might be a point of contention for Republicans.
- Describes her past experiences, including work in journalism, clerkships, and roles in private practice and public defense.
- Points out her history as a district court judge in DC and later an appellate court judge.
- Talks about some of her notable rulings, including cases involving accommodations for a deaf individual, Trump's impeachment subpoenas, and programs for countering teen pregnancy.
- Expresses satisfaction with Judge Jackson's rulings and anticipates some of these decisions being discussed during the confirmation hearings.
- Emphasizes that her job is to interpret the Constitution and ensure equal application of the law.
- Speculates on potential grandstanding during the confirmation process despite her qualifications.

### Quotes

- "Presidents are not kings."
- "There is nothing in her background that suggests she's not capable of interpreting the Constitution."
- "She has experience across the board in the legal system."
- "Some of them are probably going to come up during the hearings."
- "Y'all have a good day."

### Oneliner

Beau provides an insightful overview of Ketanji Brown Jackson's background, qualifications, and potential challenges during her Supreme Court nomination process.

### Audience

Political enthusiasts, Supreme Court watchers.

### On-the-ground actions from transcript

- Watch and stay informed about the confirmation hearings (implied).
- Advocate for fair and thorough evaluation of Judge Jackson's qualifications (implied).

### Whats missing in summary

Insights into specific rulings or controversies that might arise during Judge Jackson's confirmation hearings.

### Tags

#SupremeCourt #KetanjiBrownJackson #ConfirmationHearings #LegalSystem #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Ketanji Brown Jackson.
She is President Biden's nominee for the Supreme Court.
So what we're gonna do is we're gonna go through her history,
her qualifications, a couple of her rulings,
just so people can get a picture of who she is,
beyond the whole historic thing
that everybody's talking about.
And we'll see if there's anything in this that might come up from Republicans during the hearings.
Okay, let's start with this. She went to Harvard. She went to Harvard. Her thesis was about the plea bargaining system
in the United States,  and I believe the subtitle of it was Coercion of the Criminal Defendant.
Her positions on the criminal side of things are probably heavily influenced by the fact
that her uncle was sentenced to life for a non-violent substance crime while she was
in college.
She also has family in law enforcement.
So I would imagine that that position is probably going to be assailed by Republicans.
They're going to try to talk about that and probably frame it as soft on crime or something
like that.
It is important to note that her job at the Supreme Court is to interpret the Constitution,
sentence defendants. Okay, so while she was in college, she protested against a
Confederate flag that was displayed in a dorm room but was visible through a
window. And she protested against that. That's probably going to come up as
well, given the Republican Party's sudden fascination with the
Confederacy. After college, she became a reporter for the time. She dabbled in
journalism for a little bit. Then she goes back to go to law school, edited
the Harvard Law Review, graduated law school in 96. Then she, I think she did
one clerkship, then spent a year in private practice, did two more
clerkships. One of those, she clerked for Justice Breyer, which is funny because that's who she'll
be replacing. So there's that. After that, she bounced around. Private practice, she was special
counsel for the United States Sentencing Commission, and then she became a public defender.
In 2010, she was appointed as the vice chair of the United States Sentencing Commission.
If you don't know what that is, in the federal system, there are guidelines.
The Sentencing Commission develops those guidelines.
So in 2013, she became a district court judge in DC and stayed there until 2021, then she
She became an appellate court judge.
So as far as history, not a whole lot they're going to be able to go after.
I mean, not really.
They'll try because they are who they are.
But there's nothing there.
This is somebody who has experience across the board in the legal system.
Okay, so as far as rulings, she ruled against the Department of Corrections in a case involving
somebody who was deaf, and the Department of Corrections
didn't make accommodations for that person.
One ruling that I think pretty much everybody watching
this channel might be familiar with, or at least a part of it,
is one that occurred during Trump's impeachment.
Subpoenas were sent out.
Somebody didn't want to comply with a subpoena
because Trump told them not to.
And a ruling came out, got a whole lot of media coverage.
It included the phrase, presidents are not kings.
That's Judge Jackson.
She has ruled against
terminating grants
for programs aimed at countering teen pregnancy.
They were going to be terminated early. She ruled against that.
That may come up
because it's
related to family planning
and that's a hot button thing that they'll want to talk about.
She ruled in favor of unions against Trump.
And she issued an injunction that
would have stopped the fast-track deportations of people.
Now, from where I sit, I'm relatively
happy with these rulings.
Some of them are probably going to come up during the hearings.
The thing is, these rulings are...
this is her job.
Her job would be to interpret the Constitution
and make sure that the laws applied equally.
There is nothing
in her background that I was able to find
that suggests she's not capable of that.
We'll wait to the hearings to talk about qualifications and all of that stuff.
But I am fairly certain that the Republican Party has confirmed
less qualified people to the bench recently.
So while I know conventional wisdom right now says that she's just going to sell through and
she very well might, there's probably enough votes to get her through.
I would imagine that there will be people who try to grandstand during the confirmation process.
And those are going to be some of the things they talk about.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}