---
title: Let's talk about shifting landscapes in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sRs7b1wVKL8) |
| Published | 2022/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the rapidly changing landscapes in relation to Ukraine, elaborating on the resistance and Russian failures.
- Mentions the early Russian failures in planning, logistics, and intelligence that have shifted the tides against Russia.
- Points out that Russia failed to secure air dominance and is suffering catastrophic losses due to this oversight.
- Describes the existence and combat effectiveness of both regular and air forces in Ukraine, which shouldn't be the case at this point in the conflict.
- Compares the current conflict dynamics in Ukraine to the Vietnam War, with irregular and regular forces facing a great power.
- Notes the foggy estimates of losses but stresses that Russia has suffered significant casualties in a short period.
- Explains the shifting political landscape, with European countries now supporting larger sanctions like removing Russia from SWIFT and sending lethal aid to Ukraine.
- Talks about Russia's struggle to find allies, including a surprising response from Kazakhstan and Biden's request for billions to assist Ukraine.
- Mentions Putin's control of information by shutting down Twitter in Russia and the political challenges he faces domestically.
- Concludes by stating that while Ukraine still faces challenges, they have entered the phase of making it costly for Russia and have international support due to their successes.

### Quotes

- "Only 1,100 lost. Another way to say it is that in three days, Russia has lost a quarter of the forces that the United States lost in Iraq through the whole thing."
- "They're already at that point where they're trying to just run up the bill for Russia, and they haven't lost the capital yet."
- "Zelensky is proving to be an incredibly charismatic leader."

### Oneliner

Beau provides an insightful analysis of the rapidly changing landscapes in Ukraine, detailing Russian failures, international responses, and the shifting dynamics of the conflict, indicating a challenging road ahead for Putin.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- European countries sending lethal aid to Ukraine (exemplified)
- Supporting larger sanctions like removing Russia from SWIFT (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the current situation in Ukraine, offering valuable insights into the military and political landscapes, international responses, and the challenges faced by both Russia and Ukraine.

### Tags

#Ukraine #Russia #Geopolitics #InternationalRelations #Putin #Resistance


## Transcript
Well, howdy there, internet people.
It's Bob again.
So today, we're going to talk about changing the landscapes
in relation to Ukraine.
I say landscapes because there are a couple,
and they are shifting rapidly.
So we're going to kind of go over them
and talk about where we're at and what has changed.
I have gotten a few messages today and late last night
that basically were like, hey, it's just like you said.
They got the resistance up and running
before the capital fell while the advance was still going,
and things turned.
I mean, yeah, that's what I said,
but it did not happen the way I anticipated.
There's a huge material difference
between what I was anticipating and what occurred.
and we'll get to that in a minute,
but that needs a lot of focus.
Okay, so how did we get to where we're at?
Where the tides are not heavily in favor of Russia?
A bunch of early Russian failures.
Planning, logistics, intelligence,
stuff that should have been dealt with
before anybody moved, and it wasn't.
On top of that, there was the issue we talked about before any of this started.
In fact, it was one of the reasons that many people, myself included, were skeptical of the idea that Putin would move
out.
Because it's a really bad idea to give the opposition a month to prepare.
Ukraine used that month wisely.
And Russia is paying for it at the moment.
Now, another big mistake is that Russia still hasn't secured the air this far
into it. They don't have air dominance. Not really. What is a larger mistake than
that is even though they haven't secured the air, they're behaving as though they
have, which is leading to catastrophic losses for them at times. Okay, so we have
the regular forces, you know, what we talked about, that resistance. It's up
and running. It is up and running. Now we have the question of the regular forces
and that presents a problem for Russia because they exist. At this point in the
game, as lopsided as it was when it started, they shouldn't, and they do. And
not just do they exist, they're combat effective. Yeah, they're disorganized and
it's messy at times, but they're still out there running and gunning. Ground
forces, regular ground forces, still exist and they're still combat effective.
The Air Force, the Ukrainian Air Force, also still exists and is still combat effective.
These are huge issues.
This is that material difference.
These forces, they shouldn't be around anymore.
Not just are they around, they're operational, they're effective.
In fact, both forces have been, well, let's just call it really lucky.
Seems like they know exactly where to be, and I'm sure we'll hear more about how that
happened later.
The reason that this is a big material difference is because it sets up a dynamic where Russian
forces have to be ready to counter both.
Now I dislike comparing anything to this particular conflict
because every pundit uses it to describe it all the time.
Everything is like this.
It's like this conflict.
But realistically, in this case, it's
the only one to compare it to.
You have an irregular force and a regular force,
both operating at the same time, both combat effective.
And they're squaring off against a great power.
The modern example of this is Vietnam.
The irregular force, the VC, the regular force, the NVA.
Decent conventional military, committed, irregular force.
Putin may have walked his troops into a Vietnam-style conflict.
Now, let's talk about the losses.
Any time there's something like this,
these numbers are foggy at best.
And what tends to happen when there's
a wide range of estimates is that people start
to look at it like score.
And that's a really bad way to look at it
for a whole bunch of reasons.
But the main reason is that people
start to compare the estimates to each other
rather than something more comparable.
A few times today, I have seen the phrase,
only 1,100 lost.
1,100 is the lower end of the estimates, by the way.
Only 1,100.
And sure, that is a way to say it.
It's only 1,100.
Another way to say it is that in three days,
Russia has lost a quarter of the forces
that the United States lost in Iraq through the whole thing.
It is not going well.
It is not going well for Russian forces.
OK.
So that's the military landscape as it
stands at time of filming.
The political landscape has shifted again.
When we talked about sanctions, I talked about larger ones.
I said they may use some that were more than they messaged.
I was referring to SWIFT.
At the time that happened, there were European countries
who were opposed to removing Russia from SWIFT.
And if you don't know, SWIFT is, to greatly oversimplify it,
it is an international trading mechanism.
Bouncing Russia out of SWIFT, it cuts them
out of a lot of markets.
It makes things hard.
There were countries who didn't want to do that initially.
They have changed course, and now they're
on board with doing it.
So that's back on the table.
Other European countries have begun shipping lethal aid,
I believe is the term that people are using now, weapons.
They're shipping weapons to Ukraine.
This includes countries that weren't doing it before.
There's been a shift there.
Russia is having a hard time finding friends.
It reached out to Kazakhstan and was like, hey,
we need some reinforcements.
Why don't you send us some troops?
And Kazakhstan's like, oh, you need troops?
I suggest you open a recruiting station
and didn't give them any.
That was unexpected.
That would be like the British asking the US for troops
the U.S. saying no. On top of this, Biden has asked for billions to assist in various ways
with Ukraine. Back in Russia, the generals can no longer hide how bad it's going from Putin,
Putin, which I'm sure they were initially, Putin has shut down Twitter in Russia.
It is going bad enough that he feels he has to control the flow of information because
what is available on Twitter, well, it's just too much to overcome through normal propaganda
channels, so he has to shut it down.
is also dealing with protests already. It's not going well politically there
either. Now, this obviously pushes the question of, well, does that mean he's
going to withdraw? A logical person might, but he has put himself in a corner,
and I'm not sure how how he's going to find a way out. You know, his rhetoric, his
nostalgia for the past, it's what's led to this. This idea of rebuilding Russia to
former Soviet glory, you know, to being a player on that level again. It's going to
be hard to convince other nations that he can do that when he's having a hard
time with this. It's going to be hard to sell the idea that he's going to build a
country that can tangle with NATO, all of NATO, when he's having a way harder time
than he should with Ukraine.
This is a major embarrassment for Putin.
So as far as what this changes, not a whole lot really.
the hows, but not really the outcomes yet. Main difference is that, you know, it's not
that Russia is going to lose or is guaranteed to lose. They still have a massive edge. A
lot of the errors that they have made are correctable, but it certainly is not hopeless
for Ukraine by any means.
They've got a shot.
Even
beyond that,
they have already entered the make it cost phase.
You know, the goal
of making it
too costly, either in political capital or real capital
or
other metrics that get used in conflicts,
They're already at that point where they're trying to just run up the bill for Russia,
and they haven't lost the capital yet.
The Ukrainian resistance is doing very well, and Russia is doing very poorly.
Russia can correct some of their errors.
Some of them they're going to have to live with for a while.
Because of the early successes on the Ukrainian side, that has shifted international opinion.
It has made other countries more likely to help for Americans.
This is the way it's always been.
Think back to the Schoolhouse Rock song.
Once the Colonials showed that they could win, that's when other countries started
helping. It's kind of the same thing here. The more wins that Ukraine racks up, the
more willing other countries will be to assist in whatever ways they can. And
Zelensky is proving to be an incredibly charismatic leader. That whole... if you're
unaware he was offered an evacuation. He was offered a way out, and he said,
I need ammunition. I don't need a ride. That's the type of thing that inspires
resistance. So Russia still has an edge on paper, but so far they have not been
able to convert that edge into any material effect on the battlefield.
They're not doing well.
It's an embarrassment to Putin if he withdraws.
So it's likely that he's going to throw in more troops.
But Ukraine has been as bad as Russia has been doing.
Ukraine has been doing that well.
They've been doing better than expected, so they're getting more assistance from the West.
There's not a lot of calls to make from this point.
We have to see what they actually decide about Swift, how Putin responds to that, how Putin
responds to the protests, how Putin responds to the situation on the ground and being denied
extra troops from other countries.
There's a lot in flux at this moment, and I don't know that a whole lot of it will be
resolved before Monday, because while all these soldiers are out there in harm's way,
the politicians, well, they've got to get their weekend in.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}