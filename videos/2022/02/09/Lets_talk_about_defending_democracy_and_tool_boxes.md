---
title: Let's talk about defending democracy and tool boxes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VXfhuTgjF7Q) |
| Published | 2022/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the support for U.S. democracy by those wanting a radically different world.
- Compares different toolboxes as symbolic of different forms of government/society.
- Describes his toolbox as representative democracy, with limitations but some abilities.
- Warns against far-right government models where people serve the government, not vice versa.
- Acknowledges flaws in the current U.S. government and Constitution but sees potential for change.
- Stresses the importance of maintaining the current toolbox (representative democracy) to build a better society.
- Recognizes individuals who advocate for change while supporting the existing system behind the scenes.
- Shares a moment of realization about the precarious state of American democracy.
- Notes the efforts of many pushing for progressive change while maintaining the status quo.
- Concludes by reflecting on the complex dynamics of politics and alliances throughout history.

### Quotes

- "Government should serve the people, not the other way around."
- "That's the starting place."
- "Politics makes strange bedfellows."
- "It's probably happened a lot throughout history."
- "It's just a thought."

### Oneliner

Beau questions support for U.S. democracy amidst the desire for radical change, comparing government to different toolboxes and stressing the importance of maintaining the current system to build a better society.

### Audience

Political activists, progressives, democracy advocates.

### On-the-ground actions from transcript

- Maintain support for representative democracy (implied).
- Advocate for progressive change within the existing system (implied).
- Stay informed about the state of American democracy and actively support positive changes (implied).

### Whats missing in summary

Nuances of Beau's storytelling and personal reflections on the state of American democracy and progressive activism.

### Tags

#Democracy #ProgressiveChange #PoliticalActivism #Government #Society


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
representative democracy and the machinery for change and toolboxes. If you don't know,
over on the other channel I interviewed someone named Mariah. I'll have the link to it down
below. And that interview prompted a question. I noticed something strange months ago and saw it
again with in your interview with the county commissioner rapper. I'm sorry I forgot her name.
She's great, I loved everything she said. She has a lot of great ideas for how the country should be.
You too. Why are people like you who want a radically different world supporting U.S.
democracy? She said she couldn't let it die. That's why she said she ran for office,
that she couldn't let democracy die. But to get the country she wants, it'll have to, won't it?
Notice other YouTubers and podcasters who promote big changes do the same thing. And I've asked them,
the way I ask makes them think I'm calling them a sellout, when I'm really just curious about how
they can want a different world and promote the current one at the same time. I have a list of
other people who have done it. And there is an attached list of other people. You know,
you are somebody who wants a radically different world. You want a different society. You want that
egalitarian society where everybody's going to get a fair shake and all of that stuff, right?
You want society, government for lack of a better word, to be at the service of the people.
Government should serve the people, not the other way around. Society should serve the people,
right? For that to be the case, that society or government, it has to be incredibly flexible.
It has to be very adaptable because there's a lot of people and people have different needs.
So it has to be able to solve a lot of problems. And it has to be adaptable. It's not the strongest
that survives. It's the most adaptable to change, right? Now, recently, I told that story about
working on cars. And we're going to do a little flashback to that story. If you missed it, there
are three people in the story. Jason, myself, and Steve. Jason, super mechanic, right? Knows
everything there is to know about cars. Me, I can kind of fix stuff. Steve knows nothing, right?
As you might imagine, we have very different toolboxes. Steve has one of those from Walmart
that has like a hammer, two screws, a level, and then like a little socket set in it.
Okay, I have a mechanics tool chest. It's, if you know about it, it's like 200 pieces. So it's big,
but it's not giant, you know. It can solve a lot of problems. But every once in a while,
I have to go to the store and get, you know, a certain adapter or socket or something, right?
Jason, he has two of those big chests on wheels, and they're both full.
His tool chest, that's that ideal government, that ideal society, that flexible one, that adaptable
one. He can work on anything. He can use those tools, and he can do anything. He can do anything
use those tools to fix a Camry, a Big Rig, or an Abrams. Can solve a lot of problems,
and it can be at the service of whoever comes by, be at the service of the people.
My toolbox, yeah, that's representative democracy.
It has a lot of abilities, but there are some things it just can't do. It's flexible,
and it can be added to, right? So good, but not great. And then you have Steve's,
which really can't do much except for require other people to do the work and take from the
people around him because he needs other things. That's the government that is being offered by
the far right. Rather than society being at the service of the people, rather than government
being at the service of the people, the people are at the service of the government. That's the
difference. Now, the far right is trying to take away the toolbox we have, mine, and replace it
with that one. The problem is if that occurs, we have to rebuild the toolbox we have before we can
ever start working on Jason's, before we can ever start building that more adaptable society.
The US government has a lot of problems. Our form of government has a lot of problems.
The Constitution today is flawed. I know that's a wild statement to hear in the United States.
It is flawed today. It was a lot more flawed when it was ratified, right?
The toolbox we have has the tools for change. It has the machinery for change. It takes way too
long, in my opinion. It allows for way too much injustice. It isn't perfect, but the tools are
there, and we can use them. Eventually, we can build that better society. So, taking that away,
losing that ability, losing those tools, that puts a lot of people who want a much better society on
the defensive. It makes them, at the bare minimum, there are people who are laying down cover fire
for a representative democracy. It's not ideal, but you're not taking it because we need this.
It's a tool. It's not the end. That's the starting place. And as far as the people who
didn't like your question, I know some of the people on your list, yeah,
some of them probably legitimately got their feelings hurt because they are true believers.
They are true believers, and it probably bothers them on some level to have to defend a system of
government that they do want to replace. They want to replace it with something better,
something more adaptable. And then some of them probably just don't want that image because they
built their channel around the idea that they're in constant opposition to the United States
and its system of government. So, for them, it's more about image than actually being upset about
it. The thing is, all the names I know on here, yeah, you're right. Even those that wouldn't want
it publicly known that they have, they have all in some way laid down cover fire to help maintain
the current system as they push for something better. And I think if you want to know the
situation American democracy is in, how perilous the situation is in, understand there are people
who are subtly trying to help that would never want it known. But they feel that they have to
because it's at that much of a risk. And everybody had their own moment, I'm sure,
when they realized, oh no, we're in real trouble. For me, I can tell you around the time those
toddlers got hit with tear gas down on the border, and there wasn't really a huge outcry,
not one that matched what I think should have happened. That was the moment for me
when I realized it was way further than I thought.
For some, there were probably points along the way. I'm sure some people came to the
realization before me, and I'm sure that some people didn't realize it until the 6th.
But with Biden not winning in that landslide, with Trumpism not being rejected en masse,
there's still that risk. So you have a lot of people who want a far more progressive
society and system of government and just world in general, who at the moment are reinforcing those,
trying to hold the line. I don't know that it's strange. It's probably happened a lot
throughout history. It's that old idea that politics makes strange bedfellows.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}