---
title: Let's talk about harm reduction and less than accurate coverage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dg8DA1LuZFk) |
| Published | 2022/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration authorized a $30 million grant program for harm reduction in addiction.
- Grant program had eight different categories for spending, including safe smoking kits.
- Headlines falsely claimed $30 million spent on crack pipes to provoke outrage.
- Other approved uses of the grant money include harm reduction vending machines, medications, condoms, and more.
- Framing of the coverage was inaccurate and designed to provoke outrage among the Republican base.
- The history of othering people with addiction issues and the failure of the war on drugs are discussed.
- Beau advocates for harm reduction strategies as the future of combating addiction.
- Safe smoking kits do not necessarily mean pipes; they can include protective rubber pieces.
- The inaccurate coverage does not represent the future of addiction combatting efforts in the country.

### Quotes

- "Manufactured outrage, because the Republican base now, it has to be angry."
- "The war on drugs is an unmitigated failure. We lost. The plants won, right?"
- "This is what the future of combating addiction in the country is going to look like."

### Oneliner

The Biden administration's $30 million grant program for harm reduction in addiction was inaccurately framed as purchasing crack pipes to provoke outrage, but it actually includes various harm reduction strategies representing the future of combating addiction.

### Audience

Advocates for harm reduction

### On-the-ground actions from transcript

- Advocate for harm reduction strategies in combating addiction (implied)
- Educate others on the importance of harm reduction programs (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the inaccuracies in media coverage surrounding the Biden administration's grant program for harm reduction and advocates for effective harm reduction strategies in combating addiction.

### Tags

#BidenAdministration #HarmReduction #WarOnDrugs #MediaCoverage #AddictionCombat


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk about all of the coverage
that occurred in relation to a grant program
and how that coverage was less than accurate
and why that coverage may have been framed the way it was.
way it was. So the Biden administration, as part of their legislation, authorized a grant program
of, I want to say, 30 million dollars, something like that. And the purpose of the grants was to
increase harm reduction when it comes to addiction, various kinds. And this program had a whole bunch
of different ways the money could be spent. Different categories. There were eight different
categories. One of the categories involved purchasing equipment. And one of the approved
uses in it was safe smoking kits, which we'll get to what that is in a minute because that's also
not exactly what was portrayed in the media.
The safe smoking kits, that's a tiny, tiny, tiny fraction of this grant program.
But what did the headlines say?
The Biden administration buying crack pipes, right?
Because it could produce outrage, that's it.
They wanted to mislead, $30 million spent on crack pipes.
That's the general take that was put out there.
It's not even remotely accurate.
Setting aside the other seven categories,
let's just talk about the equipment
that could be purchased with this grant money.
Harm reduction vending machines, infectious disease kits,
medication lock boxes, FDA approved OD reversal meds,
condoms, safe smoking kits, screening for infectious disease,
Sharp's disposal kits, test kits, syringes, the vaccinations,
wound care.
Wow, that sounds almost like it's a harm reduction grant
program.
Manufactured outrage, because the Republican base now,
it has to be angry.
In order for those who cater to the, quote, news of the far right, they have to keep them
mad.
They're not interested in any actual news, they just want to be angry and have somebody
tell them who to be angry at.
The way this was framed, it wasn't accurate, not in any way, shape, or form.
But that's what's went out.
Now, we need to talk about something else though.
The reason they knew that this could be used
to provoke outrage is because for a long time,
people who had addiction issues, well, they were othered.
Can't help them, they're bad people.
Don't do anything to reduce harm,
just lock them up and throw away the key, right?
War on drugs.
The war on drugs is an unmitigated failure.
It is a failure.
We lost.
The plants won, right?
After all this time and more than a trillion dollars spent,
the plants won.
The idea of locking people up and just throwing away
the key, it doesn't work.
We have decades showing that.
You know what the future is?
How you can reduce the harm?
This.
This type of stuff.
That's what it's going to look like, so get used to it.
If you were somebody who got angry about this,
understand this is the future, because this is what's actually
worked in other countries.
Now, as far as safe smoking kits, understand
that doesn't necessarily mean pipes.
A lot of times, that's just a piece of rubber
that goes over the pipe, so people don't burn themselves,
so people can, I guess, share the pipe,
could not share the germs, that kind of stuff.
The way this was framed was designed to provoke outrage,
and it wasn't accurate in the coverage.
This is what the future of combating addiction
in the country is going to look like.
Anyway, it's just a thought.
Y'all have a good day. Thanks for watching!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}