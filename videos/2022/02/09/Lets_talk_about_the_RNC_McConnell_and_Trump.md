---
title: Let's talk about the RNC, McConnell, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sIEYv4avfCY) |
| Published | 2022/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on the recent actions of the RNC, McConnell, and Trump in relation to the events of January 6th.
- Points out the RNC's censure of Republicans participating in the committee investigating the events of January 6th.
- Mentions McConnell's surprising defense of Republicans on the committee and distancing from the RNC's statement.
- Questions McConnell's motives in publicly supporting Republicans on the committee despite potential political risks.
- Suggests that establishment Republicans are strategically distancing themselves from Trumpism.
- Notes ongoing feuds within the Republican Party between establishment figures and those embracing Trumpism.
- Speculates on potential information that McConnell and other establishment Republicans might have regarding Trump's legal troubles.
- Concludes with a thought on loose lips in Capitol Hill hinting at Trump's legal challenges.

### Quotes

- "They've tried to spin it."
- "He didn't say anything."
- "I think they know something."
- "There's a lot of splits."
- "I think there may be some loose lips."

### Oneliner

Beau speculates on McConnell's surprising defense of Republicans involved in the January 6th investigation, hinting at potential insider knowledge of Trump's legal troubles.

### Audience

Political analysts, Republican voters

### On-the-ground actions from transcript

- Speculate on political motives and insider knowledge within the Republican Party (implied)

### Whats missing in summary

Context on the recent political events and dynamics within the Republican Party.

### Tags

#RepublicanParty #McConnell #Trump #January6th #RNC #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk
about the RNC, McConnell, Trump, and I'm going to do a little bit of speculation
because there was something that could be seen as an unforced error, but I don't
think it was. So if you don't know what's going on, the RNC censured the Republicans
who are participating in the committee looking into the events of the 6th. When
they made the statement, they referred to it as legitimate political discourse. Now
the perception at the time was that they were talking about the events of the
6th, and that is certainly how it appeared. Since then, the RNC has tried to
suggest that they were really talking about other people who weren't involved
in whatever. They've tried to spin it. It was a situation that required no
comment whatsoever from Mitch McConnell. None. He didn't say anything. But he came
out and said of the 6th that it was a violent insurrection with the purpose of
trying to prevent a peaceful transfer of power after a legitimately certified
election. And then he went on to say that it wasn't the RNC's job to single out
Republicans the way they did. Came out pretty hard in favor of the Republicans
on the committee. That seems odd. That would be an unforced error. That would be
a mistake on his part, unless he knows something everybody else doesn't.
McConnell's been up there a really long time. There's a photo of him in the
Oval Office with Reagan. He has been up there a long, long time. I don't like him,
but he's politically savvy. If there's one thing we know about Mitch McConnell
is that he will look out for Mitch McConnell. He had no reason to come
forward and defend the Republicans on the committee. None. By the current
political climate, it could only hurt him. So why would he do it? There's no reason
to make a statement like this and risk alienating some of your voters,
especially when you're safe in your election. You don't have to run for election
for a while. There's no reason to create that kind of animosity and open up to a
primary challenge later or something like that. It doesn't make any sense,
unless he knows something that other people don't. McConnell isn't a person
that is going to come to the defense of people for no reason, unless it benefits
him. These little shenanigans that are coming forward, that are coming out in the
Republican Party, I don't think they are as random as they may seem. The
establishment Republicans, all of a sudden, they're distancing themselves
pretty hard, trying to get away from anybody that is linked with Trumpism. And
they are saying stuff like this, saying stuff like calling it a violent
insurrection, rather than a tourist visit or whatever. I don't think it's an
accident. I don't think it's an error. I think they know what the committee has
seen. I think they have a better understanding of what is going to be
revealed. It's worth noting that there's a lot of feuds going on in the
Republican Party right now. You have Crenshaw versus the Freedom Caucus. You
got McConnell versus McCarthy. Don't get excited, Tucker. You got Trump versus
Pence. You have DeSantis, who doesn't want to take a side between Trump and Pence.
You have Trump versus Cheney. Trump versus everybody who voted against him.
Trump versus DeSantis, because DeSantis might be more popular than him in some
polls, so on and so forth. There's a lot of splits, and most of them are right
along the lines of establishment Republicans versus people on Trump's
side, those who embrace Trumpism. And sure, maybe it's just normal party dynamics,
but I don't think so. Because for the establishment Republicans, those who
really understand how DC works, for them to write it out, all they have to do is
nothing. They don't have to make a statement, but a lot of them are
talking. I think they know something. I don't know what, but between Graham last
week and McConnell now, I think there may be some loose lips up on Capitol
Hill that are informing those in the Senate that Trump's legal net may be
tightening. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}