---
title: Let's talk about Republicans getting sue happy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ty5cpRWAkkU) |
| Published | 2022/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the connection between new laws proposed by Republicans and his piece of the Berlin Wall keychain from East Germany.
- Describes the new method Republicans are using to push their agenda through state levels by allowing citizens to sue.
- Draws parallels between the authoritarian tactics of the Stasi in East Germany and the incentivized citizen lawsuits in the US.
- Warns about the dangerous implications of these laws, which aim to circumvent constitutional frameworks and manipulate citizens into enforcing state desires.
- Raises concerns about the chilling effect on freedom of speech and behavior, as citizens could be sued for things deemed offensive or against state preferences.
- Predicts a slippery slope where the lawsuit mechanism can be expanded to target various behaviors beyond just teaching subjects like gender.
- Compares the incentivized citizens under these laws to those who collaborated with the Stasi in East Germany, betraying neighbors for personal gain or to avoid trouble.
- Emphasizes that once such machinery is introduced, it will inevitably be used against those who initially supported it, leading to a more authoritarian society.
- Concludes by drawing attention to the historical tactic used by authoritarian regimes to control and suppress dissent by turning citizens against each other for rewards.

### Quotes

- "They can't really do anything about it, not directly. It would get shot down in the courts. But if they just created a mechanism for citizens to sue that teacher, well that would have a chilling effect."
- "It's how they chill speech in society where only your betters get to decide what you can do, what you can talk about, what is socially acceptable, and what behaviors you need to engage in."
- "Believe me, for $10,000, your neighbor will probably turn you in."

### Oneliner

New laws incentivizing citizen lawsuits mirror authoritarian tactics, chilling speech and behavior, ultimately turning neighbors against each other.

### Audience

Citizens, Activists

### On-the-ground actions from transcript

- Organize community education sessions on the importance of protecting freedom of speech and resisting authoritarian measures (suggested).
- Join or support organizations advocating for civil liberties and protection against state overreach (exemplified).
- Foster a strong community network that resists divisive tactics aimed at turning citizens against each other for personal gain (implied).

### Whats missing in summary

The full transcript provides a deep dive into the historical context of authoritarian control and the potential consequences of incentivized citizen lawsuits on society. Watching the full video can offer a more comprehensive understanding of these complex issues.

### Tags

#Authoritarianism #FreedomOfSpeech #IncentivizedLawsuits #HistoricalParallels #CommunityResistance


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk a little bit about East Germany
and something that went on there. And we're going to talk about the new method that Republicans are seeking
to get their agenda pushed through various state levels to make sure that people do what they want.
Especially things that they really can't legislate themselves directly.
So they set up that framework where people are allowed to sue.
You can sue the teacher if they teach something that you don't like.
You can sue somebody for engaging in family planning because it offends your sensibility or whatever.
The thing is, every time I hear about one of these new laws being proposed,
I always end up looking at my keychain.
My keychain is a piece of the Berlin Wall.
And on the other side of that wall, in East Germany, there was an organization called the Stasi.
They had about 90,000 employees and they kept that country in the grip of fear.
Everybody making sure they did exactly what Big Brother, what the government wanted them to do at all times.
Because they never knew when somebody was going to show up from the Stasi and ask them what they were up to.
When you show up, they already knew what you were up to.
These laws that are being proposed, the goal is to get around the frameworks in the Constitution.
The basic general ideas that most Americans have held pretty closely for a long time.
And the state is farming out its authoritarian nature and incentivizing citizens to do their dirty work for them.
That teacher, well that's teaching something that the governor doesn't like, that the state legislature doesn't like.
They can't really do anything about it, not directly.
It would get shot down in the courts.
But if they just created a mechanism for citizens to sue that teacher, well that would have a chilling effect.
And then Big Brother gets what he wants.
You see, right now the people who are cheering it on, they cannot imagine this machinery being used against them.
But it will be.
You know, right now it's a $10,000 lawsuit for teaching kids about gender or whatever.
But soon it'll be a $10,000 lawsuit for not recycling.
What about a $10,000 lawsuit for not being vaccinated and going in public?
And I know somebody right now is saying, slippery slope.
Yeah, I know.
I know.
When I talked about this the first time and I said that it would be used for guns, that comment's down below.
I'm wondering, has California introduced that legislation yet or are they just still drawing it up?
Once machinery like this gets introduced, it's going to be used.
Once people get used to it, it's going to be used.
And it'll be used definitely against the people who are cheering it on right now.
Because most of them are the more entitled sort.
So it'll be easier to justify because it is damaging the people as a whole.
So there's the lawsuit.
$10,000 lawsuit for maybe telling an offensive joke within earshot of children or something.
I mean, certainly if teaching about gender is offensive enough to warrant a $10,000 lawsuit,
then an intentionally offensive joke would be too, right?
Only makes sense.
And I know.
What does this have to do with the Stasi?
See, the Stasi only had 90,000 employees.
But they had about twice that number on top of that 90,000 that were just normal citizens,
that were incentivized to help.
Some were in trouble of their own, and this was the way they could work off their debt to society.
Some were getting cash payments.
Kind of like that $10,000 lawsuit.
Conditioned to betray their neighbors to the state.
To rat people out to Big Brother.
It's interesting.
Especially because people here in the U.S. are saying, oh, this is so inventive.
No, it's not.
It's not innovative at all.
This is a tactic that's been used by authoritarian goons since the beginning of history.
Give them their pieces of silver, right?
This is how authoritarian states keep people in line.
It's how they chill speech in debate.
It's how they chill speech in society where only your betters get to decide what you can do,
what you can talk about, what is socially acceptable, and what behaviors you need to engage in.
And if you step outside of that, well, believe me, for $10,000, your neighbor will probably turn you in.
Especially once it becomes commonplace.
And believe me, once people start suing teachers, nobody's safe.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}