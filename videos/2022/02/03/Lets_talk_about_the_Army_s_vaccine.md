---
title: Let's talk about the Army's vaccine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vZDsdcvqRL0) |
| Published | 2022/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US Army at Walter Reed has been working on their own vaccine for the current public health issue, using different technology that can work on all variants, known and unknown.
- The vaccine has undergone animal and phase one testing, with promising results, but phase two and phase three testing are still needed, making it about nine months away from potential distribution.
- The vaccine is referred to as a pan-coronavirus vaccine, although it's likely to work specifically on COVID-19 rather than all coronaviruses.
- Beau expresses concern about potential conspiracy theories if the vaccine doesn't work on all coronaviruses in the future.
- The vaccine's ability to be stored at room temperature for a month and in a fridge for six months is seen as a significant advantage for distribution, particularly in areas with limited infrastructure.
- Beau stresses the importance of responding to the global pandemic in a global manner, even considering countries that the United States doesn't typically focus on.
- He believes that this vaccine could be a game-changer in the fight against the current public health issue and suggests keeping an eye on its progress.

### Quotes

- "This might be the game changer everybody's been waiting for."
- "We're going to need more tools in the toolbox."
- "The virus doesn't care."
- "We should be responding in a global fashion."
- "It's something to put on your radar."

### Oneliner

The US Army is developing a game-changing vaccine that could combat all variants of the current public health issue, with promising results so far, but further testing is needed before potential distribution.

### Audience

Health officials, researchers, policymakers

### On-the-ground actions from transcript

- Monitor the progress of the US Army's vaccine development (suggested)
- Advocate for global cooperation in responding to the pandemic (suggested)
- Stay informed about advancements in vaccine technology (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the unique features and potential impact of the US Army's vaccine development efforts, offering a comprehensive understanding beyond a brief summary.

### Tags

#Vaccine #USArmy #GlobalResponse #Pandemic #Healthcare


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the mighty marvel
of modern medicine made in Maryland, maybe.
So if you don't know, without a lot of fanfare,
the US Army up at Walter Reed,
they've been working on their own little project,
their own product to deal with our current
public health issue.
They've been working on their own vaccine
And it is different than the other ones
that are out there right now.
Uses different technology.
I'm not going to get too far into that.
But if you look it up, you'll hear about soccer balls
and nanotechnology.
It's cool stuff.
The main takeaway difference is that this
would work on all variants, both known and unknown.
It goes about combating the virus in a different way.
cool. Currently it has gone through animal and phase one testing. So you still have phase two
and phase three, so it's still like nine months out. But thus far results are promising. They
are calling it a pan coronavirus vaccine. That's probably bad terminology. Everything that I have
read says it's going to work on COVID-19. I haven't seen anything to suggest it would work
on the other coronaviruses. The reason I'm saying it's bad is because you know
18 months from now when this thing rolls out somebody's gonna come down with a
case of MERS or something else that's related and you know it will just fuel
conspiracy theories. They'll sit there and say look it doesn't work. So we have
this on the horizon and it's something that is desperately needed because
Because there's a lot of fatigue.
People are very tired of this topic, and you hear people saying, I'm done with this.
I'm ready for it to be over.
Yeah, I was ready for it to be over like two weeks into it.
The virus doesn't care.
As much as it seems like we kind of have a handle on it, we really don't.
We're going to need more.
We're going to need more tools in the toolbox.
This is one that is showing a lot of promise and should be widely available because one
of the other things about this is that it can sit at room temperature, I think, for
a month, something like that, can stay in a fridge for six months.
That's a huge bonus.
It's a game changer for getting these vaccines into areas where there's not a lot of infrastructure.
This is a global pandemic.
We should be responding in a global fashion, even in those countries that the United States
doesn't typically think about.
This will definitely help with that.
Anyway, it's something to put on your radar.
something to kind of keep an eye on because this might be the game changer
everybody's been waiting for.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}