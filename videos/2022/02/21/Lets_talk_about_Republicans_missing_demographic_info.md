---
title: Let's talk about Republicans missing demographic info....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Iz9x5Ste43k) |
| Published | 2022/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of societal change and a magic number in relation to generational changes.
- Points out the historical shift in attitudes towards interracial marriage over the years.
- Emphasizes the importance of changing people's views through culture to drive legislative change.
- Notes the increasing acceptance and approval of interracial marriages in American society.
- Draws parallels between the acceptance threshold of one in five and cultural shifts leading to legal changes.
- Provides statistics on the percentage of different generations identifying as LGBTQ.
- Raises concerns about the Republican Party's anti-LGBTQ legislation alienating a significant percentage of new voters.
- Stresses the potential impact of this demographic shift on future elections.
- Warns that the Republican Party's socially conservative stance may backfire due to changing demographics.
- Calls on the Democratic Party to deliver policies that resonate with key demographics instead of relying solely on anti-Republican sentiment.

### Quotes

- "Once it hits that one in five number, oh, it's over. It is over."
- "We have hit that number."
- "One out of five, that is a significant percentage."
- "There's no way for Republicans to win that vote."
- "You have to deliver for these demographics that are part of that coalition."

### Oneliner

Beau explains the impact of demographic shifts on societal change and political outcomes, warning the Republican Party of potential backlash and urging the Democratic Party to deliver policies that resonate with key demographics.

### Audience

Voters, Political Activists

### On-the-ground actions from transcript

- Mobilize young voters to participate in elections by engaging with them and addressing their concerns (implied).
- Advocate for policies that represent and support diverse demographics within the community (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of societal change, demographic shifts, and their implications on political dynamics, offering insights into the importance of cultural influence on legislative change and the electoral consequences of ignoring key demographics.

### Tags

#SocietalChange #DemographicShifts #PoliticalImplications #DemocraticParty #RepublicanParty


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about societal change
and a magic number.
We're going to talk about math.
We're going to talk about generational changes
and how they occur.
And we're going to talk about how the Republican Party is
definitely misreading demographic changes
within the United States.
And they will pay for it during the elections.
If the Democrats do their job right,
get out a particular vote, the debt comes due in 2024.
OK, so before we get into the current stuff,
let's talk about some older stuff.
When we're talking about societal change.
In 1958, only 4% of Americans approved of interracial marriage.
We've talked about how you have to change thought before the law changes.
By the time the law changes, people's attitudes have already started shifting.
Otherwise the law wouldn't have changed.
People focus on getting legislation passed when a lot of times they need to focus on
changing people's views through culture.
the way people think.
In 1967, which is when interracial marriage became legal everywhere, the percentage of
Americans that approved of it was closer to 20%, right around 16-17%.
About one in five get ready to start recognizing that number.
When it became legal, 3% of new Leo heads were interracial marriages.
By 2015, that number had closed to about 1 in 5, 17%.
Today, 94% of Americans approve of interracial marriages.
That one in five number, that plays into things a lot.
Once a practice that we've seen is something that was outside of the norm, something you
shouldn't do, once it hits that one in five number, oh, it's over.
It is over.
That is now part of the culture.
That's when laws change.
That's when it becomes truly accepted.
That's when that happens, right?
OK, baby boomers, if you were to ask them if they identified as LGBTQ, 2.6% would say
yes.
Gen X, 4.2%, Millennials, 10.5%, Gen Z, 20.8%, 1 out of 5.
We have hit that number.
It is worth noting that this was a survey only of adults that were part of that generation,
part of Gen Z.
So what that means is that the Republican Party pushing all of this anti-LGBTQ legislation,
They are telling one out of five of the new voters, the voters that are coming up right
now, they're flat out saying, we don't like you.
We're not going to represent you.
We're going to try to stifle you.
We're going to just try to erase you and act like you don't exist.
One out of five, that is a significant percentage.
That is something that can swing elections.
And again, this is only of the adults today.
Which means a whole lot of them, well, they couldn't vote in 2020, but they'll be able
to vote in 2024.
If the Democratic Party does its job right, if they get out the young vote, this is going
to matter.
When you're talking about the numbers, this demographic shift combined with the legislation
that the Republican Party is pushing everywhere, this is going to matter.
The Republican Party being socially conservative and trying to legislate their morality, it's
going to backfire in a big way.
They have hit that one in five number.
That is giving the Democratic Party a pretty big edge.
Right out of the gate, one out of five.
There's no way for Republicans to win that vote.
All the Democratic Party has to do is give them something to vote for instead of just
relying on them voting against Republicans.
If the Democratic Party does that, if they can deliver for this demographic, it's going
to matter in 2024.
This is one of the problems with the Democratic Party as a whole is there's a wide coalition
of demographics that vote against Republicans.
But if Democrats want them to turn out in numbers, they have to deliver for them.
They have to give them something to vote for.
Not just count on them showing up to vote against Republicans.
Don't count on them voting in self-defense.
You got to give them something to vote for.
You have to deliver for these demographics that are part of that coalition.
And lately, the Democratic Party hasn't been doing a real good job of that.
If they can change that between now and 2024, it's going to matter.
Not just for this demographic, but for a whole bunch of other ones who showed up in 2020.
But so far, the Democratic Party really hasn't delivered for them.
They probably won't go vote Republican, but they may not vote.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}