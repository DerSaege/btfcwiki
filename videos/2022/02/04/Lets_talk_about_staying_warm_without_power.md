---
title: Let's talk about staying warm without power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5m2Cq5YztSo) |
| Published | 2022/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recapping how to stay warm during a power outage in extreme cold, focusing on dressing in layers and heating a small area.
- Designating a room away from exterior doors and windows to keep warm and setting up a tent inside with extra insulation layers.
- Using a candle for additional heat but being cautious with fire hazards.
- Creating a makeshift tent by moving a dining room table into the designated room and insulating it with blankets and clothes.
- Heating rocks or smaller pots outside the home to bring warmth inside, being cautious of potential explosions.
- Ensuring the designated room is big enough and comfortable for everyone, avoiding sweating from overdressing, and staying hydrated.
- Reminding to take medications and checking on neighbors who may need help during outages.
- Emphasizing community support and proactive action during prolonged outages, as government assistance may be delayed or insufficient.
- Urging immediate action on the prepared plan once an outage starts, regardless of the expected duration or government response.
- Advising to practice the preparedness plan even if the power is restored quickly, to be ready for future outages and starting the setup while the home is still warm.

### Quotes

- "Sometimes there is justice, and sometimes there is just us."
- "The government's not coming to help. Ask the people in Texas."
- "Don't wait for a response from them. As soon as it starts, put your plan into action."
- "Because getting this set up while your home is still somewhat warm, that's ideal."
- "Y'all have a good day."

### Oneliner

Recapping tips for staying warm during a power outage in extreme cold, urging proactive community action and caution with fire hazards.

### Audience

Community members

### On-the-ground actions from transcript

- Designate a warm room away from doors and windows with insulation layers (suggested).
- Set up a makeshift tent using blankets and clothes for extra insulation (implied).
- Heat rocks or pots outside to bring warmth inside (suggested).
- Practice the preparedness plan immediately when an outage starts (suggested).
- Stay hydrated and ensure everyone's comfort and safety in the designated warm room (implied).

### Whats missing in summary

Tips on dealing with power outages, community support, and proactive preparation for extreme cold situations.

### Tags

#PowerOutage #CommunitySupport #ColdWeatherSafety #EmergencyPreparedness #ProactiveAction


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to kind of recap something
we went over almost one year ago to the day.
We're going to talk about how to stay warm
in the event of a power outage in extreme cold.
OK.
So even if you heard this last year,
it's always good to hear stuff like this again
so it stays fresh in your mind.
First part to remember, dressing layers.
Second part, it's easier to heat a small area
than it is a large area.
Stands to reason, right?
That's simple, but people tend to forget that.
So it would be wise to designate a room, preferably
away from exterior doors and windows.
And that's going to be the room that you're
going to try to keep warm.
Inside that room, set up a tent.
And then cover that tent with blankets, tarps, towels,
bathroom mats.
It doesn't matter.
You're just trying to create extra layers of insulation.
You put as much on it as the tent will hold.
If you do that and you keep the doors to that room closed,
you'd be surprised what body heat alone inside that tent
can do to keep you warm.
And if body heat isn't enough, you
can always just have a single candle.
Could do a lot of good.
Be careful with fire.
Now, if you don't have a tent, a quick workaround to that
is to take your dining room table
and move it into that room.
You want it out of the dining room, which
is typically a big room.
So you want it in a small room, because the smaller the area,
the easier it is to keep warm.
And even though you're only going to try to keep what's
under the dining room table warm,
the heat that escapes will help heat that small room.
And over time, it has a cumulative effect.
So you take your dining room table in there,
and you turn that into a tent.
Blankets, everything that we just said before.
Along the sides, just pack up clothes, dirty clothes.
It doesn't matter.
You're just trying to create insulation.
Body heat alone will do a lot in that situation.
Single candle.
Again, be careful with fire.
If that's not enough, if temperatures get that low
or it is that prolonged, you can always
heat stuff outside of your home.
Do not build a fire in your home.
I felt like I shouldn't have to say this,
but last year in Texas, we needed to say that.
Don't build a fire in your home.
Don't burn pressure-treated wood.
There's a whole bunch of stuff.
Be safe.
Use common sense when you're doing this.
So you build a fire outside your home,
and you heat stuff up and bring it in to the area
you're trying to keep warm.
What can you heat up?
You can heat up rocks.
You can heat up rocks.
Now, be aware, rocks can sometimes explode under heat.
So while they're in the fire, stay back.
When you dig them out with a pitchfork or tongs,
whatever it is you're going to use, and put them in a pot,
make sure that you cover up that pot with something,
because it would be horrible to be walking inside
and have one pop and a flaming piece of rock
hit you in the face.
That's going to make this whole thing worse.
So that's one thing.
If you don't have rocks, you can heat up smaller pots.
Anything that will hold heat, you can heat it up,
put it in the pot, bring it inside.
And you can keep several batches of stuff going all at once.
Make sure that the room you designate
is big enough for everybody to be in.
You don't want to pick a bathroom if there's eight
people.
Small is good, but also make sure it's
comfortable enough for everybody.
When it comes to dressing in layers and piling on clothes,
that's good.
Just make sure that you don't hit the point where
you start to sweat, because you have so much on,
because that moisture on your skin
is going to get real cold real quick.
And then stay hydrated.
Be mindful of everything else that you need.
Don't forget to take your meds.
Make sure you can check on your neighbors.
You have people around you that may not
be capable of helping themselves.
And just come together as a community,
because it does look like there's
going to be some prolonged outages in maybe
a couple of different areas soon.
And if not, it's information to file away for later.
The infrastructure in the United States needs work, obviously.
If only there was somebody trying really hard
to address that.
It isn't fair that people are going to go through this.
But sometimes there is justice, and sometimes there is just us.
Understand, if it happens, they're not coming.
The government's not coming to help.
Ask the people in Texas.
Don't wait for a response from them.
As soon as it starts, put your plan into action.
If you put it into action and they get the power back on
in two hours, great.
You got some practice at doing it for the next time
when they don't.
Don't wait on this, because getting this set up
while your home is still somewhat warm, that's ideal.
Because you're not starting from a freezing temperature.
You're starting from room temperature.
So it's easier to keep it warm.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}