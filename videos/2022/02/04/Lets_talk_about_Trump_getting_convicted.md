---
title: Let's talk about Trump getting convicted....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K3YGHdOXsq4) |
| Published | 2022/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the message he received from Germany questioning why Trump hasn't been indicted yet, given the number of people in prison.
- Mentions the running joke that a federal prosecutor can indict a ham sandwich, implying it shouldn't be difficult to indict Trump.
- Raises the question of whether they can convict Trump, especially with a portion of the jury pool still loyal to him.
- Comments on the Department of Justice (DOJ) possibly dragging their feet due to the challenges of convicting Trump.
- Expresses doubts about the committee's ability to craft a narrative that breaks the spell Trump has on his supporters.
- Emphasizes the committee's duty to inform the public and ensure accountability while reshaping Trump's image from "dear leader" to a threat to democracy.
- Suggests that the evidence alone may not be enough to secure a conviction due to partisan loyalty influencing jurors.
- Proposes potential strategies for DOJ, including slamming Trump with charges or getting prominent right-wingers to testify against him.
- Stresses the urgency for the committee to act swiftly in presenting a compelling case before DOJ proceeds.
- Concludes by underscoring the importance of constructing a cohesive narrative to sway those still under Trump's influence.

### Quotes

- "If they can indict a ham sandwich, odds are they can indict a mango Mussolini."
- "They have to be able to recast him from dear leader to the existential threat to the republic that he was."
- "They still have to account for that one person, one person on that jury can vote not guilty. Not based on the evidence, but based on partisan loyalty."
- "Because you have all of this evidence, you have all the reporting about it, everything that we've seen."
- "They have to say, this led to this, that led to this, that led to this."

### Oneliner

Beau analyzes the challenges in convicting Trump amidst partisan loyalty and the need for a compelling narrative to sway public opinion.

### Audience

Political analysts, activists, voters

### On-the-ground actions from transcript

- Push the committee to swiftly present a compelling case (implied)
- Advocate for prominent figures to assist in crafting a narrative against Trump (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the obstacles in convicting Trump and underscores the critical need for a persuasive narrative to counter his influence effectively.

### Tags

#ConvictingTrump #DOJ #NarrativeBuilding #PartisanLoyalty #Justice


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about convicting Trump
and ham sandwiches and mangoes and a message I got from Germany.
Because there's a running theme, but the message I got from Germany
was the funniest.
Whole bunch of people asking why he hasn't been indicted yet.
But the message from Germany really put it together in a nice way.
And it's funny because it started off with something to the effect of,
pardon my English.
While it was probably the most grammatically correct message
I'd ever received.
But the general gist of it was, hey, y'all
have so many people in prison there.
It can't be that hard to lock someone up.
Yeah, I mean, that's true.
The running joke is that a federal prosecutor
can indict a ham sandwich.
If they can indict a ham sandwich, odds are they can indict a mango Mussolini.
But that's only part of it.
Can they convict that mango afterward?
And I know right now people are like, well, they definitely have the evidence.
Well, they definitely have the evidence to indict.
But can they convict when a third of the jury pool is still saying,
oh, that's my president, dear leader?
It's going to be really hard.
That might be why DOJ is kind of dragging their feet on this.
I can understand why DOJ is dragging their feet.
I can't understand why the committee is.
Because on top of the committee's actual duty,
which is to get to the bottom of this, make sure that it can't happen again,
inform the American people about what happened, so on and so forth,
they also have to find a way to string together the events
and fashion that story that is going to break the spell he has on people.
They have to be able to recast him from dear leader
to the existential threat to the republic that he was.
That's their duty.
If they can't do that, you can kiss a conviction goodbye.
I don't know that they're up to it, to be honest.
I mean, that's a big ask.
So even assuming that the long-running tradition in the United States
of protecting the institution of the presidency,
even if we assume that protecting the institution of democracy
is more important and they do go forward with it,
it's still an uphill battle.
Because the evidence doesn't matter as much.
Because even though there's piles and piles and piles of evidence
that's showing up, they still have to account for that one person,
one person on that jury can vote not guilty.
Not based on the evidence, but based on partisan loyalty.
They have to get over that.
They have to find a way to make sure that people are viewing it
in an unbiased manner.
And that's a lot harder than it sounds.
If the committee can't put it together and doesn't do it quick,
they have to do it real fast.
Otherwise, it's not going to matter.
If the committee can't do that, DOJ only has a few options.
One would be to just slam him with charges, tons.
And rack it up so the sentence would be incredibly long,
and then offer him a plea deal for almost no time whatsoever.
That would be one way.
Another way would be to get a whole bunch of prominent right-wingers
to agree to testify against him.
Their options are limited when as much of the potential jury pool
is a supporter of the would-be defendant.
The committee has to get on the ball, and they have to move quickly.
Because before DOJ can really get rolling,
the committee has to make its case.
And they have to do it well.
Because you have all of this evidence,
you have all the reporting about it, everything that we've seen.
That is pretty clear.
But there's still that 30%.
They're going to have to find a way to break through that spell he has on them.
And it's going to take, in my opinion, getting a storyline,
turning it into a story rather than just bits of evidence.
They have to get that narrative rolling.
They have to say, this led to this, that led to this, that led to this.
And it would be super helpful if they could get prominent people
from Trump's circle to assist in the storytelling.
I think that's what it's going to take.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}