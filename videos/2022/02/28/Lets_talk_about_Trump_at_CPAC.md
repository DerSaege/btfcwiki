---
title: Let's talk about Trump at CPAC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-vcp9xgA_CA) |
| Published | 2022/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- CPAC, the conservative club for Trump supporters, reveals true conservative beliefs.
- Coverage of CPAC focuses on different speeches, notably Trump's.
- Trump's speech at CPAC blames Biden for Ukraine, praises Putin, and ignores his own inaction.
- Trump won the CPAC straw poll for 2024 with 59%, down from 70% previously.
- The drop in Trump's poll numbers within his base is significant but overlooked.
- Some Republicans and Democrats have vested interests in Trump's continued influence.
- Trump's declining support within his own base indicates a loss of steam.

### Quotes

- "American leaders are dumb. Everything that's happening in Ukraine is Biden's fault."
- "Trump is losing steam much faster than a lot of people want to admit."
- "Trump has dropped 11 points among his people, his crew, his fan club."

### Oneliner

Beau points out the declining support for Trump within his own base, a significant story often overshadowed by his CPAC win and speech.

### Audience

Political analysts, Trump supporters

### On-the-ground actions from transcript

- Analyze political trends within conservative circles (implied)
- Stay informed about evolving political dynamics (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the CPAC event and its implications for Trump's support base.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about CPAC
and how it is being covered by most outlets.
And we're going to go over the things that they've covered.
And then I want to point something out
that I think is being missed that
might be more important than any of the other coverage.
So if you're not familiar with CPAC,
this is the conservatives' conservative club,
is what it boils down to.
These are Trump's people.
This is his fan club.
These are his crew, right?
And so when this little group gets together,
you see a bunch of conservatives really go mask off.
And that's part of it.
You have a whole bunch of articles
about different things that different people said.
And those are interesting, no doubt.
And then there's a group of articles
about what Trump said, his speech, which I'll go ahead
and summarize it for you.
Putin is smart.
American leaders are dumb.
Everything that's happening in Ukraine is Biden's fault.
He completely forgot to mention that the first time
Russian forces fired at Ukrainian forces directly
happened under him, and he didn't do anything.
And that the most dangerous people are within the country,
not the person who he called smart who just kind of
threatened nuclear war.
That was basically his speech.
And there are people, of course, tearing that apart,
rightfully so.
And then there's the predominant story
is that Trump won the straw poll.
Every time they get together, they
have this little fake election, and they
talk about who they want to be president.
And big surprise, Trump's crew, his people,
people who adore him, said that they wanted him
to be president in 2024, 59%.
Next was DeSantis, and he was in the 20s somewhere.
And that's the story, right?
And those are the three things that are really being covered.
But it's the whole thing with the straw poll
that I think might need some more attention,
because people are chalking that up as a Trump win,
because he did win the poll.
But the thing is, they have this little meeting pretty often.
Last one was just six or seven months ago.
And at that one, Trump got 70%.
And at this one, he got 59%.
Trump has dropped 11 points among his people, his crew,
his fan club, the people who should be backing him
no matter what.
He has dropped 11 points.
That seems like a bigger story, as much as people
don't want to admit it.
And this goes to both parties.
There are people in the Republican Party,
rank and file people, who really want
to believe that Trump is still the heart
of the Republican Party.
And there are Democrats who want Trump
to be the heart of the Republican Party,
because he's an easy opposition.
They already beat him once.
They can do it again.
And that's the way they're looking at it.
But 59% in his circle, in his fan club,
that means 41% of people who should be really behind him
wanted somebody else.
And that's a big difference.
Trump is losing steam much faster
than a lot of people want to admit.
I think that's worth noting.
Anyway, it's just a thought.
Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}