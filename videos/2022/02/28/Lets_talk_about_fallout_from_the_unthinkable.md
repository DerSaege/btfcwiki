---
title: Let's talk about fallout from the unthinkable....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ox8ZDbCNTCE) |
| Published | 2022/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about historical realities and the Russian and U.S. Strategic Command being on alert daily.
- Mentions Putin's announcement of putting his nuclear arsenal on alert, which is not a significant change in posture.
- Emphasizes the importance of knowledge in alleviating concerns, especially in the event of a nuclear blast.
- Criticizes traditional training on nuclear blast response for providing too much information that may hinder decision-making.
- Simplifies the response to a nuclear blast into two rings: "you felt it" and "you saw it."
- Explains actions to take if you are in the "you felt it" ring, including seeking medical attention.
- Provides guidance for those in the "you saw it" ring, debunking Hollywood myths about fallout and advising to seek shelter in a sturdy building.
- Advises staying away from windows, being in the center of the building, and taking precautions against fallout.
- Recommends using dirt to cover windows as a barrier against radiation.
- Suggests actions to take if caught outside during fallout, such as getting inside immediately and removing contaminated clothing carefully.
- Encourages listening to battery-powered radios for civil defense instructions and staying tuned for guidance.
- Assures that most cars should still function post-blast, contrary to movie depictions of EMP effects.
- Stresses the importance of limiting exposure to fallout and waiting for further information before taking action.
- Mentions the survival rates post-Little Boy bombing and the significance of staying inside to limit exposure.
- Recommends accessing archive.org for more information on nuclear blast survival strategies.

### Quotes

- "Your key thing is to limit your exposure to fallout for as long as possible."
- "Either you're here or you're not."
- "It's the Cold War."

### Oneliner

Beau simplifies nuclear blast response into two rings, advises on seeking medical attention or finding shelter, and stresses the importance of limiting fallout exposure.

### Audience

Emergency responders and civilians

### On-the-ground actions from transcript

- Seek medical attention if in the "you felt it" ring (implied)
- Find a sturdy building and shelter inside if in the "you saw it" ring (implied)
- Listen to battery-powered radios for civil defense instructions (implied)
- Limit exposure to fallout by staying indoors (implied)

### Whats missing in summary

Detailed information on varying survivability statistics for different rings and further resources available on archive.org.

### Tags

#NuclearBlast #EmergencyResponse #FalloutProtection #CivilDefense #SurvivalStrategies


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about
something I don't think we need to talk about. But I have had dozens of questions come in
about this exact topic over the last 12 hours. So we're going to talk about it. But before
we do, I want to point out some historical realities that I'm not sure everybody's aware
of. I want you to think back to your earliest memory. On that day, whatever day it was,
there were portions of the Russian and the U.S. Strategic Command on alert of some kind
on that day, whenever that day was, and every day since. Putin made his announcement saying
he was putting his nuclear arsenal on alert. That just means more. It's not really a change
in posture. There were going to be some on alert anyway. It's also worth noting that
historically speaking, when the world has come close to the unthinkable, the public,
you and I, we didn't know. But that probably didn't make anybody feel better. So, because
there are concerns, knowledge normally helps alleviate them. So we're going to go over
what to do in the event of a nuclear blast. I have been through training on this a whole
bunch of times, and I have huge complaints about this kind of training, especially when
it's given to people, generally speaking. I think they provide too much information,
so much information that it makes it impossible for people to actually help make a decision
with that information. If you look at basically any, any instructional information on this
topic, they start off by telling you about the rings, right? There's the thermal radiation
ring, there's the shockwave ring, all of this stuff. And the poor person that is trying
to figure out what to do, they're trying to figure out which ring they're going to be
in. But there's no way they can, because they don't know the yield, and they don't know
where it's going to hit. So two kilometers from ground zero means nothing. It doesn't
help. So rather than a bunch of rings, for the purposes of this conversation, there are
two rings. There is, you felt it, and you saw it, you're aware of it. Those are the
rings. That's it. If you are in the felt it ring, that means you were hit by debris, you
had debris hit near you, you have burns, something like that. If you're in that ring, your next
move, you need medical attention. You need medical attention. And it's not just for the
injury that you're aware of. Now, the problem with this is that because most areas where
this would occur are large urban centers, the infrastructure is going to be down. And
medical attention is, it's a ways away. Could be days before it's available. So, unless
you are aware of a route to get yourself to medical attention that you can make, you need
to follow the advice for the you saw it ring and wait. Okay, so you are not in the seen
it ring, or the felt it ring. You're in the saw it ring. We have to dispel some Hollywood
myths here. Because in most movies, not all, some are incredibly accurate, but in most,
the idea is you have to get away from the fallout. Get in your car, run. No, do not
do that. Because fallout doesn't work the way it works in movies, in most movies. If
you are in the you saw it, you're aware of it ring, get inside. Get inside a strong,
sturdy building. Fallout can begin coming down in about 15 minutes. About 15 minutes
after the blast. Now, it may take a little bit longer, and it can be blown a pretty good
distance depending on wind and a whole bunch of other stuff. Which is another reason why
those rings don't really matter as much as people like to pretend they do. You get inside
and you want to get in a hospital, a school, something concrete and secure, right? If it
has a basement, that's even better. A brick house is better than a wood house. You don't
want to be in a mobile home. However, if you are in a wood house or a mobile home and you
can't make it to a more secure structure within 15 minutes, don't go. Because you don't
want to be outside when the fallout first starts to come down. And we'll explain why
in a second. So, unless you can get there within 15 minutes, stay where you're at. The
reason for that is when the fallout comes down, and it can look like rain, it can look
like snow, it can look like ash, it may not necessarily even be really visible. When it
comes down, it's at its strongest. One hour after that, it is 50% as powerful. Just one
hour. So you don't want to be caught when it's at its peak outside. So, there's a 50%
reduction, and these numbers are approximate, obviously. There's a 50% reduction after one
hour, there's an 80% reduction after 24 hours, and there's a 99% reduction after two weeks.
So your goal is to get inside and stay inside. Think of it just like a tornado. You want
to stay away from windows. You want to be in the center of the building. You don't want
to be near the roof, because that's where all the fallout is going to land. You want
to stay away from what's radioactive. That seems like common sense there. When it comes
to windows, if you have time, put stuff up over it. Find something and stuff in there.
Something that works really well is dirt. So if you're a gardener, and you have bags
of potting soil, put them up in the windows, and then use a 2x4 to secure it. You want
to block off the radiation from coming inside. If you are in the felted area, and you aren't
mobile, you know, it's hard, you're not going to be able to get to medical infrastructure
that is functioning. You need to follow this same advice, and wait. Now, if you do happen
to be caught outside, when the fallout comes down, you get inside immediately. And then
once you're inside, you slowly take off your clothes. Slowly. You don't want to do it quickly.
You don't want to disturb anything that's on the outside. Close your eyes, hold your
breath. Your clothes go into a plastic bag, you seal it up, and you put it away from everything
that matters. Take a shower. You can use soap, you can use shampoo, you cannot use conditioner.
The stuff that is in conditioner to make it work will bind the radioactive stuff to you.
And sadly, you don't become Spider-Man. So, that's a brief overview. Now, when you're
in there waiting, what do you do? You wait and you listen. Battery-powered radio, and
you listen. What Passes for Civil Defense in this country has an informational campaign,
and I want to say it is, get inside, stay inside, stay tuned. And instructions will
be coming on where to go, or hey, maybe you're fine where you're at. Because you don't necessarily
know how bad it is, because you don't know which way the wind blew, you don't know the
yield, any of this stuff. And it's great to talk about percentages, but understand that
all depends on what it starts from. If you only have a little bit come down, you might
be fine after an hour. If you have a bunch come down, you could still be at risk after
a 99% reduction. So, you're going to need more information, unless you're one of those
weird people that just happens to have the instrumentation to test the area around you.
Now if they tell you to move, what should you do? Because your car doesn't work, right?
Because almost every movie you've ever seen says the EMP has knocked out your car. Your
car's probably fine. Your car's probably fine. There was a study on this back in 2004, and
they took 50 cars, and they tried to shut them down with an EMP. Of those, three stopped
running. Two of those three started right back up. Some of them had glitches, like the
blinkers were on and they couldn't turn them off, stuff like that. But almost all of them
ran. Now when you talk about this today, people are like, well, you know, there's more computers
in cars today. And that's true, but car manufacturers, a lot of them will test for lightning strikes.
So your car should be fine. Odds are, your vehicle, or one very, very close to you, will
function. And you use that. But you wait until you know that everything's okay. Don't try
to make a move before then. When you're taking off your clothes, be very careful. Don't scratch
yourself. You don't want anything that is radioactive to get into your skin. Aside from
that, I want to talk about Little Boy for a second. Now, Little Boy is one of the first
ones that was ever actually used. And we have this image of what it's like. And yeah, it
leveled five square miles. It caused a lot of problems. And the ones today are much larger.
But it's also worth noting that 71% of people survived. And this is before people knew what
to do. Your key thing is to limit your exposure to fallout for as long as possible. Now, if
you don't hear anything, if you're staying tuned and you're not getting a message, continue
to wait as long as humanly possible. That's it. Stay inside. So I hope that helps. But
again, I do want to stress that this is, it's the Cold War. And a lot of the posturing that's
going on doesn't really equate to reality. It's also worth noting, just as an additional
aside, if I didn't say it, basements are better, subways, stuff like that. Just additional
information. If you are interested in learning more about this, you can go to archive.org.
You can get a whole bunch of material on this topic. And you can learn about the different
rings and how they vary and survivability statistics for each ring and all of that stuff.
For the purposes of this video, it doesn't matter. Either you're here or you're not.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}