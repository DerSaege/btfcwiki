---
title: Let's talk about the Carrington Event....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-6JoX8zVszY) |
| Published | 2022/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of a coronal mass ejection (CME) caused by a geomagnetic storm on the Sun, which can potentially hit Earth and cause disruptions.
- Describes the Carrington event of 1859, where a massive CME caused chaos by sending electricity down wires, leading to fires and other problems.
- Mentions that auroras, like the Aurora Borealis, can be seen during such events, with examples of occurrences in different years and locations.
- Warns about the potential impact of a Carrington-sized event today, where transformers could blow, the power grid could fail, and various technologies like radio and GPS could cease to function.
- States that the cost to restore the US infrastructure after such an event could be around two and a half trillion dollars, taking months or even a year to recover.
- Talks about the playbook in place for handling such events, involving turning everything off to prevent system overload.
- Notes that early warning satellites provide limited time for preparation, usually around 12 hours to a day or two.
- Raises concerns about the government's efficiency in implementing plans, despite having preparations in place.
- Emphasizes the inevitability of a CME hitting Earth in the future and questions whether society will effectively respond to it.
- Encourages people to acknowledge the expertise of specialists in different fields and be prepared for potential disasters like a Carrington event.

### Quotes

- "A CME will hit Earth. It's happened in the past. It's going to happen again."
- "Everything off, this system shouldn't get overloaded."
- "Are we going to handle it? Are we going to deal with it?"
- "There are people out there that have specializations in different fields and listen to them a little bit more."
- "Y'all have a good day."

### Oneliner

Beau explains the impact of a Carrington-sized event today, the playbook to handle it, and the importance of preparedness for inevitable CME occurrences.

### Audience

Preparedness advocates

### On-the-ground actions from transcript

- Stay informed about space weather alerts and updates (implied)
- Prepare an emergency kit with essentials like food, water, and supplies (implied)

### Whats missing in summary

The full transcript provides detailed insights into the potential consequences of a Carrington-sized solar event and underscores the importance of preparedness and response strategies in such scenarios.

### Tags

#CarringtonEvent #SpaceWeather #EmergencyPreparedness #SolarStorms #DisasterResponse


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about the Sun. Here comes the Sun and a Carrington event. What would happen if
one occurred today? Because somebody was reading an article and I guess earlier
in February there was a coronal mass ejection. For the rest of this video
that's a CME. And it sent them down a rabbit hole and they started looking
into it and now they have questions about what would happen if a Carrington
sized event occurred today. And it's cool because I actually know a little bit
about this. Okay, so a CME is caused by a geomagnetic storm, a solar storm, a storm
on the Sun. And to ridiculously oversimplify it, the Sun shoots out a
bunch of magnetized space stuff. And if it hits Earth, well it causes some
disruptions. It isn't something that is guaranteed to hit Earth. These
ejections can go in any direction off of the Sun. So the odds, these things
actually occur relatively frequently, but they don't normally hit Earth. However
they have. The biggest and the most notable was in 1859. It was observed by
a guy named Carrington, hence the name Carrington event. When it happened,
telegrams, people that were working the telegraph, if they were actually working
it and tapping on it, they could get shocked. The paper in the machines burst
in flames. Sparks showered from the wires above, caught towns on fire. It was a big
deal. Basically the CME kind of sent electricity down the wires. Too much to
handle. So it caused a lot of problems. It wasn't all bad news though, because when
these things occur, you do get to see pretty lights. You can see auroras like
the Aurora Borealis. You can see those from like all over the place. During the
1859 one, you could see it almost at the equator. They have occurred since then in
1921 in New York, in 1972 in Vietnam, in 1989 in Quebec. There was also one in
2012 that was a pretty big one and it missed Earth. The one in 1989 took out the
power grid in Quebec. And this is where it gets a little interesting. The one in
1859, it hit Earth and it was a huge one. But we weren't using a lot of electronics
then. So it was more annoying than something out of a disaster flick, you
know. If it happened today, which is the question, and after I say all of this
stay tuned, because there's more to it. If it happened today, transformers
would blow. The power grid would go down. Gas pumps would cease to work. Wouldn't
matter, because you probably can't pay for anything without your credit card. The water
pumps that fill your pipes would cease to function. Radio, TV, GPS, all this stuff
goes down. We haven't had a big one hit since we've entered the digital age and
if it does, it's going to be a pretty big deal. Lloyds did a study on this a few
years ago and I want to say that it was two and a half trillion dollars,
somewhere around that, just for the United States to get the US back up and
running. And it could take months or a year to bring the US back online. That's
a long time without power. These things normally occur during the solar maximum.
The solar maximum for this cycle, I want to say it starts in like nine months and
it peaks in 2025. And now that you know you're totally terrified, let me just go
ahead and throw this out there. There's a playbook. There's a plan for this. We have
early warning satellites dedicated to this exact issue. And the plan is super
simple. We just turn everything off. Like for real. That's really kind of the plan.
And with everything off, this system shouldn't get overloaded. Kind of a, you
know, you go into blackout voluntarily for a few days to avoid being in blackout
for a few months later. It is worth noting that this is a really simple
playbook. You know, there's not a lot of steps in this one. Which is good because
after the last couple of years, if there's anything that we have learned,
it's that the United States government, it spends a whole lot of money drawing
up these plans, but it has a real hard time putting them into action. So this
would be a, you know, everybody hang out in the dark for a few days, four days to
flatten the CME, I guess. Something like that. Now the bad news about this is that
the early warning satellites, they don't really give us a whole lot of warning. It
could be 12 hours or maybe a day or two, which isn't a lot of time, but it's
enough to make some preparations. This is something that is, it's kind of scary. In
the digital age, this is something that's, yeah, it's concerning. But there are plans
that should, in theory, just go into effect. And it's also one of those
things that is, it's inevitable. A CME will hit Earth. It's happened in the
past. It's going to happen again. A Carrington-style event will occur. The
question is, are we going to handle it? Are we going to deal with it? Are we going to
enact the playbook, shut everything down, do what we're supposed to? Or are we
going to ignore reality? I would hope that after the last couple of years,
people would just start to understand that there are people out there that
have specializations in different fields and listen to them a little bit more. But,
yeah, I mean, with everything else going on, here's one more thing to add to your,
you know, doomscrawling, a Carrington event. It's worth looking into, because
there are other unique little stories. In fact, there's kind of a widespread
belief that these happen more than we might believe, but we never noticed them
until 1859. There is the belief that one occurred on, I want to say 1770, and that
Captain Cook saw it. And it's one of those things where we just didn't notice
it, because we didn't have electronics. So, this is one more thing to say, hey, this
could happen, but we have a plan, but we don't know if they'll put it into effect.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}