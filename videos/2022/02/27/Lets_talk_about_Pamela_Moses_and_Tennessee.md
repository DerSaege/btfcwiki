---
title: Let's talk about Pamela Moses and Tennessee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Vb9Xs_1YDGY) |
| Published | 2022/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of Pamela Moses' story in Shelby County, Tennessee regarding voting.
- Pamela Moses, an activist, ran for mayor in Memphis.
- She believed she was eligible to vote as she was off probation, supported by a certificate from the Department of Corrections.
- However, her voting rights were questioned, leading to a trial where she was sentenced to six years in prison for attempting to vote.
- The judge ordered the sentence vacated, granting a new trial due to a document not being turned over by the Department of Corrections.
- The document's content remains unknown, causing a shift in the case.
- The excessive nature of the original sentence is pointed out by Beau, suggesting it is cruel and unusual.
- Contrasts Pamela Moses' sentence with shorter sentences for those who attempted to undermine legal votes or used violence.
- Beau hints at a possible political motive behind the harsh sentence, implying Tennessee politics may be at play.
- Beau believes public awareness played a role in revisiting Pamela Moses' case.
- Encourages vigilance and ongoing attention to the case, indicating it's not yet resolved.

### Quotes

- "Six years is probably excessive."
- "This battle isn't won yet."
- "Let's not let this drop out of our eyesight here."

### Oneliner

Beau sheds light on Pamela Moses' voting rights battle in Tennessee, questioning the excessive sentence and hinting at potential political motives, urging continued attention to the case.

### Audience

Activists, Voters, Community Members

### On-the-ground actions from transcript

- Stay informed on Pamela Moses' case and outcomes (implied).

### Whats missing in summary

The emotional impact and frustration over the unjust system and the importance of public vigilance in holding authorities accountable.

### Tags

#VotingRights #Justice #Activism #CommunityPolicing #Tennessee


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about a story
out of Shelby County, Tennessee, and voting.
A change of events, a change in the course of things.
A woman named Pamela Moses, she's an activist,
ran for mayor in Memphis.
But see, she did this wild thing. She voted. She voted. And she believed that she was eligible
to vote, according to her, because she was off probation. And I guess at some point,
the Department of Corrections had even given her a certificate saying that.
But then, I guess, somehow that got rescinded. So she was charged, went to trial,
was sentenced to six years in prison for attempting to vote. That seems excessive
and this would be a typical story of injustice. However, judge has ordered that
sentence vacated and she gets a whole new trial. It turns out, according to
prosecutors, that the Department of Corrections failed to turn over a
document. I have been unable to find out what that document was, but whatever it
was, it was enough to vacate a sentence and order a new trial. This may shift this
whole thing. Now, regardless of what this document is and whether or not it proves
her claim that she believed she was off probation and therefore she could work
to get rights back and was eligible to vote and all of that stuff.
I believe that most people would agree that six years is probably excessive.
I mean, I would suggest that that's cruel, perhaps even unusual, especially when there
There is a slate of people who may not have attempted to vote illegally, but attempted
to undermine legal votes, and most of their sentences are in the months.
those who used violence, most of those don't come to six years. This is
outrageous and the sentence alone is outrageous to begin with. The fact that
something has occurred in this case to vacate it and grant a new trial makes it
It seemed almost as if there might be people who didn't want her running for mayor.
This seems like it might be a case of good old Tennessee politics, but we're going to
have to wait and find out because we don't have that information at time of filming.
Either way, her name is Pamela Moses, and I have a strong belief that this case was
revisited and documents were found because of public awareness.
This battle isn't won yet, so let's not let this drop out of our eyesight here.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}