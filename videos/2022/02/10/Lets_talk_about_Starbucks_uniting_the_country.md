---
title: Let's talk about Starbucks uniting the country....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xx5jBLSkVQI) |
| Published | 2022/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Starbucks employees are organizing themselves to start a union to advocate for better pay, benefits, and conditions.
- The idea that certain jobs are "lesser" and do not deserve fair compensation stems from societal beliefs about education and labor devaluation.
- Beau challenges the notion of "unskilled labor" and argues that every job requires skill of some kind.
- He points out the importance of valuing all types of work and ensuring that workers can live decently.
- Beau criticizes the mindset that suggests certain jobs do not require fair wages or benefits based on perceived hierarchy.
- He illustrates the impact of undervaluing labor on depressing wages and creating disparities between different professions.
- Beau questions the concept of unskilled labor, suggesting that it may refer more to uncredentialed work rather than lacking skills.
- He underscores the need for collective bargaining to achieve a decent standard of living for many workers in a wealthy country.
- Beau points out the concerning wealth gap between the haves and have-nots, indicating underlying issues in society.
- He challenges individuals to rethink their perspectives on labor, education, and societal inequalities.

### Quotes

- "The people who provide that service, they deserve a decent life."
- "Every job requires skill of some kind."
- "If the gap between the haves and the have-nots is that big, there's probably something wrong with it."
- "If you being one of the haves in your own eyes is dependent on somebody who provides you a service being a have-not, you're already a have-not."
- "Have you ever been to a new Starbucks, one that just opened up and all the employees are new? Does it run well? No."

### Oneliner

Starbucks workers organizing for better conditions challenge societal views on labor value and advocate for fair treatment in a wealthy country with widening wealth disparities.

### Audience

Workers, advocates, activists

### On-the-ground actions from transcript

- Support unionization efforts among workers (exemplified)
- Advocate for fair wages and benefits for all types of labor (implied)

### Whats missing in summary

The emotional depth and personal anecdotes shared by Beau are best experienced by watching the full transcript.

### Tags

#LaborRights #Unionization #SocietalInequality #FairWages #CollectiveBargaining


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Starbucks.
We're going to talk about Starbucks
and how some of their employees have decided
that they want to be united.
If you don't know, there's a rapidly growing group
of employees of this company who have decided
it is time for them to advocate for themselves,
that they want better pay, better benefits, better conditions.
They're going to exercise that collective power
that they possess, and they're going to start a union.
I've been talking about this a lot on Twitter.
And I got a message that basically said,
electricians, plumbers, miners, Starbucks workers,
one of these things is not like the other.
Do we really need unions for unskilled labor?
And I get it.
I get where this idea comes from.
But I am of the opinion that if we as a society
have deemed that a certain job needs to be done,
that service over there, it needs to be provided.
We would like that.
The people who provide that service,
they deserve a decent life.
The idea that some jobs are lesser, that some jobs, well,
they're just not good enough.
And the people who get stuck doing those jobs, well,
we don't have to pay them what they need to get by.
I think that comes from something
that our parents told us, that we heard over and over again
growing up.
Get an education or you'll be a ditch digger
or something like that.
Pick the profession that your parents used.
But you probably heard something like that
if you're around my age.
The idea was to promote education.
I don't think that's what happened.
I don't think it encouraged children to value education.
I think it encouraged children to devalue labor.
Have you ever been in a city with bad drainage
during a big rainstorm?
Ditches are important.
They do a lot of stuff.
We kind of need them.
And since we need them, I think the people who make them
should be able to get by.
They should have a decent life.
It's that idea that some types of work, well,
that's just for those people.
We don't really need to pay them.
They don't need benefits.
They don't need to get by.
They should have done better in school,
finding some way to justify it.
I don't believe that that's a good way to look at things.
Because when you do that, by the way,
you depress your own wages.
You undermine how much you make.
Because it's funny, because you see people argue it in reverse.
Somebody making hamburgers is going to make $15 an hour.
I'm an EMT, and I only make $17.
Yeah, you're grossly underpaid.
You literally save lives.
That's probably worth more than $17 an hour.
So there's that.
And the other part of this that I don't get is,
I don't know what unskilled labor is.
And I know that sounds like some kind of snarky comment,
and in a way it is, but it's also true.
I have no idea what people are talking about when
they say unskilled labor.
Have you ever been to a new Starbucks, one that just opened
up and all the employees are new,
or one that has ran so poorly all the employees have quit
and they've had turnover?
So all the employees are new.
Does it run well?
No.
No, not at all.
But if you come back a month or two later, it does, right?
Almost like the employees have mastered that skill.
And this is true in any job.
I don't know what unskilled labor is.
Every job requires skill of some kind.
It's a bad term.
I think what people mean when they say that
is uncredentialed labor, labor that doesn't require a degree.
It doesn't require a license from the state,
something like that.
I think that's normally what they mean.
But again, it goes back to that whole get an education
or dig ditches type of thing.
It was wrong.
It wasn't a good way to encourage education
because that wasn't the effect.
The effect was that we have a bunch of people
who don't make enough to really live.
And it's at the point where a lot of them
are going to have to engage in collective bargaining just
to get up to a decent standard of living.
We're a pretty wealthy country.
If the gap between the haves and the have-nots is that big,
there's probably something wrong with it.
And to be clear, if you being one
of the haves in your own eyes is dependent on somebody who
provides you a service, being a have-not,
you're already a have-not.
They've just got you tricked into thinking
you're one of the haves.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}