---
title: Let's talk about Trump's Truth Trouble....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qTGJkkAW50I) |
| Published | 2022/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's social media network, Truth Social, is facing roadblocks and delays in its release.
- The app's beta version failed to materialize in November, and the release has been pushed back again.
- Trump's plan was to create a censorship-free platform for his followers to express extreme views.
- However, in order to be distributed via Apple and Google, the app will have to adhere to their guidelines, including moderation.
- Without distribution through major platforms, the success of the network is doubtful.
- Being removed from the app store could be the most damaging outcome for Truth Social.
- Trump has brought in Devin Nunes to address the issues faced by Truth Social.
- Nunes has experience with social media networks, particularly on Twitter.
- Trump's fans may not get the unrestricted platform they desire due to the need for moderation.
- The future of Truth Social remains uncertain amidst these challenges.

### Quotes

- "Trump's plan was to create a censorship-free platform for his followers to express extreme views."
- "Without distribution through major platforms, the success of the network is doubtful."
- "Being removed from the app store could be the most damaging outcome for Truth Social."

### Oneliner

Trump's social media venture, Truth Social, faces delays and challenges, jeopardizing its vision of a censorship-free platform for extreme views.

### Audience

Social media users

### On-the-ground actions from transcript

- Monitor the developments of Truth Social and its impact on social media landscape (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges faced by Trump's Truth Social network and the potential implications of moderation on its success.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Trump's truth tanking again, at least temporarily.
If you don't know, former president Trump launched a venture to create a social media
network, something that will provide a location for his most
devout followers to meet and congregate.
It has run into some roadblocks.
The most recent ones were, I guess, back in November.
I think they were supposed to release a beta version.
That failed to materialize.
And then the release of the app, I guess,
was supposed to happen this month,
that has been pushed back yet again,
causing stocks to lose a little bit of their value.
And all of this is kind of expected.
It is a tech company of sorts, so delays are part of it.
The one thing that I've been waiting for that has also
occurred, I was hoping that nobody
going to point this out until after the app went live, but Reuters decided to go ahead and put out
an analysis on it. Trump's plan here, what he wanted was to produce a censorship-free environment
a location where his most fervent fans could spout the wildest theories they could think of
without problem, because he would have control of that social media network, and he was going
to let him say anything, presumably as long as it was favorable to him.
He set it up to be the anti-Facebook or anti-Twitter, the thing that was going to just stand in
the way of big tech.
So the problem with this is that in order to distribute his app, he is going to have
to stay within the guidelines produced by Apple and Google.
The idea that it is going to be censorship free, that's not going to happen.
It's going to have moderation just like any other site if it plans to be distributed via
those channels.
And without distribution through Apple or Google, it won't really be on a lot of people's
phones and it will be almost certain to fail.
Being tossed from the app store is probably the most devastating thing that could happen
to a social media network.
So what he set out to do isn't something he's going to really be able to deliver on.
fans will not get the experience that they are desiring where they can just say whatever
they want and get away with it and spread whatever kind of information they want and
get away with it because it's going to have to be moderated or it won't be distributed.
Now to solve the myriad of issues that Truth Social is facing, Trump has brought in an
expert from outside, Devin Nunes, who does have a lot of experience in
dealing with social media networks, particularly on Twitter, if you're
familiar. I think he had something to do with like a cow social media network at
one time as well. That's who has been brought in to solve these issues. I
I can't wait to continue to see this play out.
But as much fun as it would be to watch his fans discover the moderation that's going
to occur, I guess it is probably beneficial for them to have some kind of warning so it
doesn't cause them to just lose it.
But yeah, so if you were waiting for this platform, so you could talk about, I don't
know, lizards ruling the world or whatever, it's not going to be what you're imagining.
And if it is, it won't last very long.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}