---
title: Let's talk about HB2161 in Arizona...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pXljyJIXmzU) |
| Published | 2022/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a quote from Arizona about a new piece of legislation introduced by Steve Kaiser, HB 2161.
- The bill aims to require teachers to inform parents if their child expresses a different identity or orientation.
- This bill creates an atmosphere where the government monitors and controls personal information about students.
- Failure to comply with this requirement could lead to lawsuits, fostering an environment of spying and informing on neighbors.
- Many anti-LGBTQ groups assisted in drafting this legislation, revealing its discriminatory nature.
- When questioned about the lack of collaboration with education stakeholders, Kaiser's response was dismissive and concerning.
- The legislation seems to have been proposed without the support of any education groups, indicating its flawed nature.
- This bill is part of a broader agenda to establish an informer system reminiscent of authoritarian regimes.
- It serves as a divisive tactic to appeal to a certain base by targeting vulnerable LGBTQ children.
- Republicans resort to attacking LGBTQ kids as a way to garner support, lacking genuine leadership.
- Individuals in Arizona are encouraged to look into the implications of this bill.

### Quotes

- "The purpose of the bill is to require people at schools, teachers, so on and so forth, to call in parents if they find out their student, if they find out their child, is expressing a different identity or orientation."
- "When asked why he didn't work with real stakeholders, with education groups, with groups that represent educators and teachers, that was his answer."
- "If you're proposing legislation and the group that's going to be impacted by it wouldn't support it, that's probably an indicator that it's a bad piece of legislation."
- "This is, A, part of the desire to create that informer system in the United States, to bring about that type of system that authoritarians like to use to keep the populace in check."
- "Republicans, at this point, are limited to kicking down at LGBTQ kids."

### Oneliner

Arizona bill requires teachers to inform parents about their child's identity, sparking concerns of government intrusion and discrimination.

### Audience

Activists, Educators, Parents

### On-the-ground actions from transcript

- Contact local education advocacy groups to understand their stance on the bill (suggested)
- Stay informed about local legislative actions affecting schools and children (exemplified)

### Whats missing in summary

The full transcript provides additional context on the discriminatory nature of the legislation and the lack of genuine stakeholder involvement.

### Tags

#Arizona #HB2161 #LGBTQ #Discrimination #Education #Legislation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about a quote from Arizona
that I think is really important.
And we're going to talk about a new piece of legislation
from there.
It's the latest installment in the Republican plan
to remake American society, to transform it
into one of those wonderful societies
where the government has say over everything,
and nobody can talk to anybody without fear of being sued.
And, well, the big brother is always there
to call you in and explain the state's displeasure
with what you're doing.
The bill is HB 2161, introduced by Steve Kaiser from Phoenix.
The purpose of the bill is to require people at schools,
teachers, so on and so forth, to call in parents
if they find out their student, if they find out their child,
is expressing a different identity or orientation.
So if a student confides in a teacher,
it's that teacher's responsibility
to call in the parents and talk to them,
you know, and just explain that their child has done this.
And in the process, of course, create that atmosphere
where it's the government saying,
we know what your kid's doing.
Kaiser, you know, by the way, if the teacher doesn't do this,
of course, there can be a lawsuit, you know,
carving out that exemption, creating that informer network
that gets paid to rat out their neighbor, you know.
Kaiser said that he had worked with different stakeholder
groups to put this together.
When pressed, that story kind of fell apart.
Found out that most of the groups that assisted in this
were just anti-LGBTQ groups.
One was the Center for Arizona Policy,
which has a long history in the state
that you can look up if you'd like.
And Kaiser was questioned about this particular piece
of legislation by Representative Hernandez from Tucson.
If you don't know, Hernandez is gay.
And Kaiser got kind of on the defensive and was like,
you know, I understand why you don't like this group,
you know, and there's a lot that people are focusing on.
But I want to point to one thing that Kaiser said that,
to me, is the most important part of it all.
I'm not sure what education group I'd go to
because they'd be against this.
When asked why he didn't work with real stakeholders,
with education groups, with groups that represent educators
and teachers, that was his answer.
He proposed legislation about schools
that he apparently believes not a single group of educators
in the state would back.
If you're proposing legislation and the group
that's going to be impacted by it wouldn't support it,
that's probably an indicator
that it's a bad piece of legislation.
Now, let's be honest about this.
This is, A, part of the desire to create
that informer system in the United States,
to bring about that type of system that authoritarians
like to use to keep the populace in check.
The other part is it's that red meat for the base, you know,
because if there's one thing that can propel
a Republican candidate forward, it's kicking down at somebody.
And most groups have developed
pretty strong defenses against this.
So Republicans, at this point,
are limited to kicking down at LGBTQ kids.
Yeah, real leaders, right?
Makes sense.
If you live in Arizona, this is something
you might want to look into.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}