---
title: Let's talk about Ukraine warming up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XDUhZ_pnR3U) |
| Published | 2022/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges roughly 3,100 people in the impacted area to take a long weekend away from likely conflict zones in Ukraine.
- Warns about Russia potentially manufacturing a pretext for military action in Ukraine.
- Explains why Ukraine wouldn't go on the offensive against Russian forces.
- Indicates Ukraine's strategy of resistance and the challenges they face against the Russian military.
- Mentions the importance of training civilians for potential conflict and the quality of their preparedness.
- States that the US is currently only providing aid, logistics, and intelligence support in the Ukraine situation.
- Remarks on public opinion polls regarding Biden's handling of the Ukraine situation.
- Clarifies the misinformation around Ukrainian military plans for resistance.
- Emphasizes the uncertainty of conflict in Ukraine and the possibility of diplomatic solutions.
- Conveys the likelihood of Russia moving forward with military action in Ukraine.

### Quotes

- "When surrounded, attack is a good strategy. However, Ukraine can't move."
- "The Ukrainian military cannot go toe-to-toe with the Russian military. It will lose."
- "It is incredibly unlikely that the Ukrainian military would be able to defeat an advance militarily."

### Oneliner

Beau advises potential conflict zone residents to take a break, warns of manufactured pretexts for war, and analyzes Ukraine's defensive strategies against Russia, while clarifying US involvement and public opinion.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Take a long weekend away from potential conflict zones in Ukraine (implied)
- Stay informed about the situation in Ukraine and be prepared to react accordingly (suggested)

### Whats missing in summary

Insights on diplomatic efforts and potential global responses to prevent conflict escalation.

### Tags

#Ukraine #Russia #Conflict #Diplomacy #USInvolvement


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Ukraine and how it's warming up. If you are expecting the
second part to the videos about cops, that's going to come out later. This seems a little bit more pressing.
I will answer some of the questions that came in via Twitter.
But before that, to the roughly
3,100 people who have accessed this channel within the last 28 days
days from the impacted area, if it was me, if it was me, I would take a long weekend.
I would take a long weekend and go to a part of the country that is not likely to be impacted
immediately if there was an attack.
I would move and just go take a trip and probably stay gone until Tuesday and see how things
shape up.
If you're wrong, if you take the trip and nothing happens because there is still a possibility
of a diplomatic resolution, see, you took a trip and had some fun.
If you stay and you're wrong, you could be in a really bad way.
Unless you have a reason to be near the areas that will be contested immediately, I would
remove yourself.
And it may be premature, sure, that's always a possibility.
But you don't want to wait until you know for certain.
If you don't know what's going on, don't know why I'm saying that, there have been
a lot of developments.
There has been some shelling and there is a lot of evidence to support the idea that
Russia is attempting to manufacture a pretext to move forward militarily.
I tweeted out that if you believe Ukraine would go on the offensive with 150,000, 200,000
Russian troops on the border, Putin has some WMDs in Iraq to sell you.
Something to that effect.
And that's prompted some questions.
So before we get too far into it, some of the videos that were released today of the
evacuation that is occurring today, the Associated Press is saying that they have verified the
metadata from those videos shows that they were made two days ago.
That heavily supports the idea that Russia is attempting to manufacture a narrative.
Okay, beyond that, if you don't know the likely scenario as far as this is playing out, in
areas that were previously contested, the Russian proxies are kind of saying that Ukrainian
forces are going on the attack, and that is incredibly unlikely for the reasons we're
about to get into.
Okay, why wouldn't Ukraine go on the offensive?
Why wouldn't they attack?
When surrounded, attack.
Isn't that a standard thing?
It is.
When surrounded, attack is a good strategy.
However, that is typically reserved for when you are cut off and you are trying to get
back to friendly territory.
Ukraine can't move.
It can't go anywhere.
There is no friendly territory for it to get back to.
It is the friendly territory.
So it doesn't apply there.
That is also typically reserved for when you stand a chance of breaking through.
The Ukrainian military cannot go toe-to-toe with the Russian military.
It will lose.
It will lose.
They don't stand a chance in a conventional conflict.
I mean, sure, fog of war and all of that stuff, a whole bunch of things could go right for
them and wrong for Russia and it could happen in theory. But modern militaries, especially
major militaries, they put a lot of work into making sure that that string of coincidences
doesn't happen. It is incredibly unlikely that the Ukrainian military would be able
to defeat an advance militarily.
Their best chance is to follow their plan, but catch the advance while it is still moving.
Their plan is to put up a quick fight and then break away and engage in a resistance
style campaign.
If they can get this up and running while the advance is moving, I think they stand
a chance, not of defeating the Russian military, but of making it too costly to continue.
That isn't actually the same thing.
One has to do with political resolve and the other is actually destroying your opposition
on the battlefield.
I do believe that if they were able to get it up and running successfully during the
advance, they stand a chance of breaking the resolve of making it too costly for Russia
to continue.
If they can't do that, then it's probably going to become something pretty protracted
or a total defeat.
Now I know that a lot of effort has gone into training civilians and getting them ready
for it.
And that was a good move.
It is also worth noting that it's not just the numbers of people that were trained, it
was the quality of the training that counts when it comes to something like this.
It's all well and good to say this is what you do when you see those tanks rolling down
the road, it is very different to be staring at those tanks.
Getting that resistance up and running while the advance is still moving is going to be
a challenge and there's nothing they can do about it at this point to alter the outcome.
Either the training they provided and the resolve of the people who would be doing the
fighting is there or it's not.
Not a whole lot is going to change there.
Will there be U.S. involvement?
The only thing, literally the only, the one single fact that every single source I have
can agree upon is that at this moment the United States is sitting on the sidelines
with the exception of aid, logistics, and intelligence.
That's it.
As far as I know and as far as everybody that I've talked to about this has told me, the
US is just going to be assisting.
They're not committing any forces whatsoever to combat.
I mean that could change, but from what I've seen and given the fact that it is the only
consistent thing that I've been told, I don't really expect it to unless there is a major
development that's not foreseen.
What about the polls?
Polls say that Biden only has 34% approval when it comes to his handling of Ukraine.
Yeah, that doesn't matter.
Not to put too fine a point on this, when it comes to foreign policy and stuff like
this, polls are pretty much always wrong as far as what the move is.
It kind of goes back to why countries don't use the manual.
People believe that this is the right move when it may not be.
Keep in mind the invasion of Iraq, the invasion of Afghanistan, both of those pulled high.
The withdrawal from Afghanistan pulled high.
When it comes to stuff like this, polling doesn't really reflect the policies or the
people involved, it reflects the media coverage and how well they have provided context and
how well people actually understand the situation.
The U.S. media is not really well known for providing a lot of context.
So there is that.
And then the last question is, please don't tell them, please don't say the plans, again,
they're going to the mountains, Russians get YouTube as well, and then say they're
going to the mountains and said they were taken to the hills. That is an
American saying. It doesn't have anything to do with hills. It means
they're disappearing. They're disappearing to start a resistance
campaign, and that's all open source. The Ukrainian military has literally
advertised that that's their plan because they know it's their most
effective route to combat Russian forces, so they're hoping that it would act as a deterrent.
At this point, it does not look like that advertising campaign has been successful,
but no, the resistance will not be literally heading up into the hills or the mountains.
I don't even know that there are any in the relevant areas, or in the areas that would
be immediately relevant.
So I hope that answers the questions and any others that you might have.
It can at least give you a little bit more context to it.
It is not guaranteed that it's going to conflict.
There still are diplomatic solutions and options on the table.
However, given the pretext manufacturing, which is definitely how it appears, when you're
You're talking about this kind of stuff you can never know for sure, but that is definitely
how it appears at the moment that Russia is attempting to manufacture a pretext.
Given that and the combination of the cyber initiatives that Russia has deployed, it seems
incredibly likely that they're going to move ahead.
It's not a guarantee, but it does look like the most likely option on the table at the
moment.
The real question is whether or not the world buys the pretext that Russia is attempting
to manufacture.
If they can get enough countries to say, okay, yeah, that's actually what's happening, they'll
They'll probably move ahead.
If there is a lot of stiff resistance from around the world saying, no, look, here's
the metadata, this is exactly what US intelligence and British intelligence said you were going
to do, and now you're doing it verbatim two days later, but the metadata actually matches
the date intelligence said you were going to do it.
There's a lot in flux right now.
The only thing that I feel pretty confident about is that the United States isn't committing
ground troops in Ukraine.
The statement is that we're not going to commit to air combat either.
I'm not entirely sure of that, to be completely honest.
That is the position of the White House, but I don't know about that.
I hope that helps, and I'll try to provide more information as the situation develops.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}