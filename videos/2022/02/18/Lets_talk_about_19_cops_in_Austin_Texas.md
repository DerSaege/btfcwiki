---
title: Let's talk about 19 cops in Austin, Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4-T7hx-KLCM) |
| Published | 2022/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Austin, Texas and events from 2020 are under scrutiny, leading to a DA's election.
- 19 Austin officers have been indicted for aggravated assault with a deadly weapon.
- The indictments are linked to the use of beanbags during the Floyd demonstrations.
- Public dissatisfaction with the police response during the demonstrations led to the chief of police stepping down.
- Multimillion-dollar settlements have been made related to cases involving beanbag use.
- Details on the officers and indictments are sparse due to Texas law keeping cop indictments secret.
- Austin Police Association President confirmed charges against the officers but lacked details.
- District Attorney Garza, who campaigned on indicting police officers, is prosecuting these cases.
- Garza's confidence in no officer being convicted raises questions about prior experience or violations.
- Public sentiment in Austin seems to be shifting towards holding law enforcement accountable.
- The public's growing skepticism towards law enforcement is attributed to increased visibility through cell phone cameras.
- Public elected a DA who promised to indict cops, reflecting a desire for change in accountability.
- Beau suggests that the public's attitude towards law enforcement in Austin has shifted significantly.
- The public's loss of faith in law enforcement's benefit of the doubt is connected to increased video evidence discrepancies.
- Beau concludes by prompting reflection on the changing dynamics in Austin regarding law enforcement accountability.

### Quotes

- "Public isn't giving law enforcement the benefit of the doubt anymore because everybody has cell phone cameras."
- "For many law enforcement agencies in the United States, the benefit of the doubt, that's kind of stopped."
- "The public is beginning to understand that what's on those cameras, that doesn't always match what goes in the report."

### Oneliner

Events in Austin, Texas, leading to 19 officers' indictment reveal shifting public sentiment towards holding law enforcement accountable amidst increased scrutiny and demand for transparency.

### Audience

Texans, Activists, Community Members

### On-the-ground actions from transcript

- Contact local community organizers to support accountability measures for law enforcement (suggested)
- Attend or organize community forums to address concerns and push for transparency in policing (implied)

### Whats missing in summary

Full context and depth of analysis on the evolving relationship between the public and law enforcement in Austin.

### Tags

#Austin #Texas #LawEnforcement #Accountability #Indictment #PoliceReform


## Transcript
Well, howdy there, Internet of People. It's Beau again.
So today, we're going to talk about the Lone Star State,
specifically Austin, Texas.
We're going to talk about some stuff that happened back in 2020
and some stuff that is happening now.
It might explain why a DA got elected.
And we're going to talk about 19 cops.
19 cops.
Now, at the time of filming, reporting is kind of sparse on this,
and we'll explain why here in a moment.
But what we do know is that reporting is saying
that 19 officers in Austin have been indicted.
It looks as though the charge is aggravated assault with a deadly weapon.
Now, in Texas, when this charge is leveled against a public servant,
the maximum sentence is life.
It is not a minor charge.
It appears as though all of this stems from the use of beanbags
during the Floyd demonstrations in 2020.
It is worth noting that the police response to those demonstrations
was viewed by the public as being so poor that public pressure mounted
and the chief of police wound up stepping down.
There have also been multimillion-dollar settlements, plural,
involving the use of beanbags,
related to cases involving the use of beanbags during those demonstrations.
Okay, so Austin Police Association President Ken Cassidy
did confirm charges against 19 officers.
Said he didn't have details, though.
And that makes sense.
The reason the details are so sparse is because in Texas, under state law,
when it comes to cops, the indictments are secret
until they are actually arrested.
So nobody's really supposed to know much of any of this.
The officers' names haven't been released,
specifics about the indictments, so on and so forth.
Okay, now, we have a couple of quotes that seem relevant.
And these are from Ken Cassidy.
The first, D.A. Garza, now this is the district attorney
who is prosecuting these cases.
D.A. Garza ran on a platform to indict police officers
and has not missed the opportunity to ruin lives and careers
simply to fulfill a campaign promise.
I mean, that's a big statement right there.
Also, he called the move devastating for law enforcement in the city,
but also said he's confident that no officer will be convicted,
according to the Associated Press.
How's he so confident?
I mean, I'm not a Texas lawyer.
I'm not even that well-versed in Texas law.
But as I understand it, the indictments are secret.
Maybe there's an exemption for police association presidents,
but I don't know.
If there's not, he would be saying this based on nothing,
except for maybe prior experience of cops getting off,
or I mean, maybe somebody violated state law.
I don't know.
But when you take those statements together, it kind of makes sense.
I mean, when you think about it, if you were the police association president
and a D.A. got elected by running a campaign that says,
I'm going to indict cops,
it should probably prompt a little bit of soul searching.
You know, the campaign itself, well, that's one thing.
People running for office, well, they'll campaign on anything.
The fact that the public elected a D.A. running with that campaign platform,
I mean, that suggests that the public doesn't believe
law enforcement gets held accountable.
I'm pretty confident I know why.
That statement, confident that they won't be convicted.
Based on what?
Just that's the way it's always been?
It seems as though the public in Austin would like the way it's always been to change,
and that's why D.A. Garza got elected.
And now you have 19 cops that were indicted.
We don't know the specifics of these cases.
I mean, they could just be, they could be nothing.
But given the other surrounding factors, the information that we do know
about the public response, about the settlements,
I wouldn't be too confident of anything.
Because it does appear that the public attitude towards law enforcement in Austin
has, well, shifted a little bit.
They did, in fact, elect a D.A. that campaigned on indicting cops.
There's a point when people become so accustomed to their privilege
that they forget that it is a privilege.
It's not something that has to be extended.
For many law enforcement agencies in the United States,
the benefit of the doubt, that's kind of stopped.
The public isn't giving law enforcement the benefit of the doubt anymore
because everybody has cell phone cameras.
And the public is beginning to understand that what's on those cameras,
that doesn't always match what goes in the report.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}