---
title: Let's talk about failed states and the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hUqIgH64PaQ) |
| Published | 2022/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the elements of a failed state: loss of control of territory, erosion of ability to make collective decisions, inability to provide public services, and inability to interact with other nations.
- Points out that losing the monopoly on violence is a significant warning sign of becoming a failed state.
- Notes that in the United States, the government is willingly giving away its monopoly on violence for political gains.
- Mentions how the government is encouraging non-state actors to enforce punishment, which is detrimental to preserving the republic.
- Talks about the erosion of the ability to make collective decisions, citing the active attempts to cast doubt on election results.
- References Texas as an example of the inability to provide public services.
- States that the United States can still interact with other nations due to its powerful military.
- Warns that political failures causing misuse of the military could lead other nations to avoid interacting with the US.
- Concludes that while the US is not currently a failed state, elements of becoming one are actively occurring due to certain political agendas.

### Quotes

- "The government is giving away its monopoly on violence."
- "A lot of the Republican agenda you're going to find out is guaranteed to destroy [the republic]."
- "The United States is definitely on that path."

### Oneliner

The United States is showing signs of becoming a failed state through the erosion of key elements, driven by political agendas.

### Audience

Citizens, policymakers, activists

### On-the-ground actions from transcript

- Monitor political actions and hold elected officials accountable for decisions that could lead to the erosion of democratic principles (implied).
- Advocate for policies that strengthen public services and ensure equal access for all citizens (implied).
- Support organizations working to uphold democratic values and principles in the face of political challenges (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the elements contributing to the potential decline of the United States into a failed state, urging viewers to stay vigilant and engaged in preserving democratic norms.

### Tags

#FailedState #MonopolyOnViolence #PoliticalAgenda #PublicServices #Democracy


## Transcript
Well howdy there internet people. It's Beau again. So today we're going to talk about
failed states, what they are, what the elements of them are, whether the United States is
working on any of those elements and sliding down that road to becoming a failed state.
This is coming up because in a recent video I talked about the reason the Canadian government
may adopt a stronger posture is because of the swearing in ceremonies. Because that legitimized
a different group of people using force, right? And losing the monopoly on violence is a big
part of becoming a failed state. So what is a failed state? It has four elements. One
is loss of control of territory or of the monopoly on violence. Meaning there are other
entities that can be seen as being able to legitimately use force. Or, obviously, your
government's use of force was not enough to maintain territory. The next is the erosion
of the ability to collectively make decisions for the country. The next is the inability
to provide public services. And then the last is the inability to interact with other nations
on the international scene. Now the inability to make decisions for the country, make those
collective decisions, that also has to do with the loss of the monopoly on violence
in many cases. Because most governments rely on violence to achieve control. So losing
that monopoly, it's a huge, huge warning sign. Now in the United States, are we working on
any of these? Are any of these at play here? Loss of control of territory, not really.
Or of the monopoly on violence. In the United States you have this really weird thing happening
where in the pursuit of votes and casting the tough guy image and punishing those that
one political party sees as the enemy, the government is giving away its monopoly on
violence. If you stand in the street during a protest, well they can run you over and
they're passing laws to this effect. This type of thing is the government willingly
ceding its monopoly on violence. It's encouraging people not associated with the state to mete
out punishment. This is actually really bad for those wanting to preserve the republic.
A lot of the Republican agenda you're going to find out is guaranteed to destroy it. Now
on top of the monopoly on violence, something else that's happening is that the legislatures
are ceding normal judicial functions, which are normally backed up by that monopoly. But
now because of the system we're in, they don't have to use a club. They can use a checkbook.
So all of these statutes, these rules that are coming along, these exemptions that are
being carved out to allow people to sue where they don't really have damages as a method
of maintaining control, again it's privatizing that. It's taking it away from the state and
moving it to the private sector in this case. The point is they're losing that monopoly
and in this case it's not the work of a foreign government or some disaffected group within
the country. It's literally a party platform because they don't understand it. The erosion
of the ability to take collective decisions. So the erosion of faith in government. I mean
I don't think that we really have to run through all of that to know that the United States
is definitely on that path. You have a political party actively attempting to cast doubt on
election results, to scream, not my president. This is all part of it. The inability to provide
public service. We can all just turn and look at Texas for a moment and then move on from
here. The inability to interact with other nations on the international scene. This the
United States has a firm hold on and will continue to because we have the most powerful
military in the world. It's kind of that simple on that one. The United States will have this
aspect of it covered as long as the economy and the military are the way they are regardless
of whatever political failures occur. Now the danger becomes when the danger really
shows up when the political failures cause the military to be used in a way that it shouldn't
be and then other nations might decide not to interact with us. So three out of four
are actively occurring in the United States. Now these are all in the beginning steps.
The problem is that they're part of a political party's platform and the platform is to well
make all of these problems worse. So those are the elements of a failed state. Is the
United States a failed state? No. Is the United States actively trying to, at least one political
party, actively trying to turn the United States into a failed state? I mean I don't
know that they know they're doing that but with the baseless claims about the election,
with the seeding of the monopoly on violence and judicial functions, with the desire to
not provide public services in an effort to... I don't even know what the logic is there.
We're on our way. We are on our way. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}