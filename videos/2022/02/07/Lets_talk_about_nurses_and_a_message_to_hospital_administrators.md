---
title: Let's talk about nurses and a message to hospital administrators....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NVednwpJEzs) |
| Published | 2022/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hospital administrators, politicians, and executives are being called out for legislation designed to cap what companies can charge for travel nurses.
- The legislation could push nurses' wages down, impacting even those who aren't travel nurses.
- The focus of the legislation seems to protect profits rather than solve the real issues in the nursing industry.
- There is concern about patient ratios, which directly affect patient safety, not being addressed in the legislation.
- Beau suggests the need for an RN bill similar to the GI bill to alleviate the nursing shortage.
- The nursing shortage is partly due to how hospitals are run, leading to nurses leaving the profession for other jobs.
- Beau criticizes the prioritization of profit over the well-being of nurses and patients.
- Nurses face emotionally challenging situations regularly, like helping patients say goodbye without adequate support.
- The possibility of nurses organizing a protest on May 12, Florence Nightingale's birthday, is mentioned.
- Hospital administrators are advised to prepare for potential disruptions during this time.

### Quotes

- "There's nothing in it to, I don't know, address the widespread concern from nurses about patient ratios..."
- "It's just designed to protect the profits of the large companies at the expense of the little people."
- "For nurses, that's just Tuesday. That's just something they do."
- "I think they will do it. I disagree."
- "Not addressing any of the concerns of the nurses. Not doing anything to solve the nursing shortage, just moving to protect the profits of the large companies."

### Oneliner

Hospital administrators and lawmakers are criticized for legislation that protects profits over nurses' well-being, potentially leading to a protest on May 12.

### Audience

Nurses, healthcare workers

### On-the-ground actions from transcript

- Prepare for potential disruptions and show support for nurses during a possible protest on May 12 (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of the challenges faced by nurses and the urgent need for legislative action to address their concerns effectively.

### Tags

#Nursing #Legislation #PatientSafety #Protest #HealthcareWorkers


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk a little bit
about the world of nursing.
And we have a little message for some hospital administrators,
I think, something they may need to hear,
along with some politicians and some executives.
If you don't know, it appears there's
going to be legislation that is designed, well,
they're saying it's designed to cap what companies can
charge for travel nurses.
These are the nurses who show up when there's
a surge or a hospital, the executives and the people
pushing this kind of stuff, that they've
ran the hospital so poorly that they've
lost all of their nurses.
Or they can't recruit because there's
a nationwide nursing shortage.
It's going to push their wages down.
That's what's going to happen.
And when that occurs, it will push a lot of nurses' wages
down.
Even those who aren't travel nurses
could be impacted by this.
You know, it's weird because none of the legislation
does anything to actually solve the problem,
just protect the profits.
Almost like it was written by donors, is what it seems like.
There's nothing in it to, I don't know,
address the widespread concern from nurses
about patient ratios, which directly impacts patient
safety.
There's nothing in it about capping a CEO's pay,
just those little worker bees who don't matter,
the ones you're calling heroes, clapping for.
Nothing like that.
No RN bill.
You know, like a GI bill.
Hey, come be a CNA for two years and we'll pay for your RN.
Nothing like that.
That seems like something we should have,
because that might actually alleviate the nursing shortage.
But none of that's in it.
It's just designed to protect the profits
of the large companies at the expense of the little people.
That's what it is.
You know, the nursing shortage is in large part
because of the way hospitals are run.
I know a nurse who left nursing and now she
does the tattooed makeup.
I know a CNA who works at a gas station,
because she can make more at the gas station
than she can as a CNA.
These seem like issues.
A lot of the nursing shortage is self-inflicted.
Now, as far as the costs, I want to point out
that the United States, DOD paid contractors
ridiculous amounts for 20 years.
And I can't wait to hear the rhetoric on how
it is somehow different.
Because they're not putting themselves
on the line like that.
How many of that 900,000 gone are nurses?
They're not protecting the country, right?
I disagree.
Not traumatic.
You know, if you ask those people who went over and saw
all kind of horrible stuff, if they
were one of those who had to deliver one of those letters
or rewrite one of those letters, make those phone calls,
make those visits, I would imagine that almost all of them
would say that was the worst part of it.
For nurses, that's Tuesday.
That's just something they do.
It's not a defining moment in a career or in a tour.
It's just part of the work week.
Stand there and hold the phone so a patient
they know they're going to lose can say their goodbyes.
But we don't have the money for them, right?
That seems wrong.
And I have a feeling that this may
be the thing that causes nurses to push back a little bit.
And I'm pretty sure that the RTs and the x-ray techs
and everybody else is going to throw in with them.
Because actively working to depress
the wages of these nurses at a time like this,
well, it's appalling, to be honest.
They are planning, and this is the part
if you're a hospital administrator,
this is the part you probably need to pay attention to.
It looks like they're going to march on May 12, which
is Florence Nightingale's birthday.
And I'm sure that there are a lot of people
in the upper echelons of these hospitals that go,
they're not really going to do that.
They don't have the backbone, the strength for that.
I disagree.
See, they've been working out a lot over the last couple years
carrying your entire hospital.
I think they will do it.
I would go ahead and start making contingency plans
for that week.
Push back electives.
See if you can get any travel.
Oh, you probably won't be able to get any travel nurses.
That's going to be hard.
Maybe go ahead and make sure that your ACLS, your BLS,
all that stuff's up to date.
Go reorient yourself to a floor, because I
got a feeling you're going to be taking some beds that week.
This is wrong.
Any other time in this system where there is high demand
and low supply, well, that's kind of what determines cost,
right?
But not for the workers, just for the people
who donate to politicians, I'm guessing,
is how it seems to be shaping up.
Not addressing any of the concerns of the nurses.
Not doing anything to solve the nursing shortage,
just moving to protect the profits of the large companies.
That's what this is about.
I have a feeling that this is not
going to go the way a lot of people are thinking.
Anyway, it's just a thought.
Y'all have a good day.
Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}