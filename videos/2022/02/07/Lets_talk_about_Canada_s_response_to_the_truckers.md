---
title: Let's talk about Canada's response to the truckers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XJzFo6-xO4Q) |
| Published | 2022/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he hasn't covered the trucker protests in Canada, stating that in relation to what is typically discussed on his channel, it's not a big story.
- Points out that Canadian officials are handling the situation well, focusing on law enforcement efforts against vandalism and violence rather than cracking down on the protesters.
- Draws parallels to the Trump administration's failures during demonstrations in the Pacific Northwest, where implementing a security clampdown backfired and made the movement grow.
- Emphasizes the importance of allowing voices to be heard in a free society, even if the message may not make sense, while concentrating law enforcement efforts on specific issues like vandalism.
- Predicts a potential increase in arrests of those supplying individuals engaged in vandalism and hints at the dispersal of the movement.
- Comments on the hypocrisy of some individuals supporting the trucker protests while complaining about other border regulations, showcasing entitlement and inconsistency.
- Foresees pressure mounting on the Canadian government to take action against the protesters disrupting businesses in Ottawa, urging them to resist until the movement disperses on its own.

### Quotes

- "If you want this to go away, this is the correct response."
- "Because rather than a bunch of working class people getting beat by the Mounties, what you have are people out there screaming, it's horrible."
- "But because the conservative movement of today has just become a bunch of entitled, whiny, temper tantrum throwing people."

### Oneliner

Beau explains why he hasn't covered the trucker protests in Canada, praises Canadian officials' handling of the situation, warns against cracking down on protesters, and predicts potential arrests to disperse the movement amidst observed hypocrisy.

### Audience

Social justice activists

### On-the-ground actions from transcript

- Monitor and support Canadian officials' focus on law enforcement efforts against vandalism and violence (exemplified)
- Resist pressure to crack down on peaceful protesters and allow voices to be heard (suggested)
- Advocate for a peaceful resolution to the situation and discourage violent responses (implied)

### Whats missing in summary

Insights into the potential consequences of mishandling protests and the importance of maintaining a balanced approach for successful resolution.

### Tags

#TruckerProtests #CanadianOfficials #LawEnforcement #Hypocrisy #PeacefulResolution


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about truckers in Canada.
I guess.
I've had a lot of people ask why I haven't really
been covering this story.
The simple answer to this is in relation to the stuff
that we talk about on this channel,
it's not really a big story.
I know if you live in Ottawa, it may seem different.
But in relation to what we typically
discuss when we're covering stuff like this,
it's not a story.
The main reason is that the Canadian officials
are handling it well.
Again, I know that may not be the perception,
because I know a lot of people want them to go in there
and crack heads and clear the streets,
because it would be super gratifying
and all of that stuff.
That's not the right move.
That isn't the right move.
I have a playlist somewhere that's titled
It's in the Manual or something like that.
And the entire playlist is really just kind
of keeping track of all of the Trump administration's
failures in regards to the demonstrations
in the Pacific Northwest.
Because not just did they make a bunch of mistakes,
they made a bunch of mistakes that
are literally in the playbooks saying, hey, don't do this.
And the biggest one was they tried
to implement a security clampdown.
When you do that, you make movements like this grow.
Because then there's imagery of the average person,
that's how it's perceived, fighting against the state.
It makes him the underdog.
It makes people want to pull for him.
So if you're faced with something like this,
the correct answer is to do what you would normally
do in a free society, which is allow
them to have their voices heard, even if they are saying
something that may not make a whole lot of sense.
And then you focus on your law enforcement efforts.
Those get focused on vandalism and violence.
That's what you look for.
And that's what it appears the Canadian officials are doing.
I know it may not seem like it if you live in Ottawa.
But if you want this to go away, this is the correct response.
Because rather than a bunch of working class people getting
beat by the Mounties, what you have
are people out there screaming, it's horrible.
It's as bad as it was in Germany in the 1930s.
It's an authoritarian state.
They're trying to silence us.
I know you've been hearing this for two weeks
and nothing has happened because that's not true.
It completely undercuts their message.
They have to know that they're losing the PR
fight on this at this point.
Now, what I would expect is sometime in the next 72 hours,
if they are following, if they're actually following
the manual that closely, I want to say it's right about now
that they would begin ramping up arrests of those people who
are supplying those who are engaged in vandalism and stuff
like that.
That's when they start going after the facilitators.
And that tends to disperse stuff like this.
But I have no idea.
A, I don't know if the Canadian manual is the same.
And I have no idea if they're following it that closely.
But that's what I would expect to see next soon.
Now, other than that, the only other commentary
is just the obvious hypocrisy that
has just come to embody the freedom for me,
but not for the conservatives of today,
whatever they've morphed into.
The whole thing is, oh, we can't work.
So they shut down a whole bunch of other working class people.
In the United States, this whole thing,
the main trigger of support appears
to be truckers who would have to have a vaccine to go
into Canada.
A bunch of Trump supporters being upset about border
regulations being too tight.
That's not something I saw coming, I'm going to be honest.
But the overwhelming hypocrisy isn't
going to faze them at all.
Because while they're sitting there and supporting this,
and maybe even trying to send money to people who may or may
not even be involved with it, they're still complaining that,
oh, these asylum seekers, they may not be vaccinated.
They're coming from a country where they don't actually have
a lot of access to the vaccine.
But because the conservative movement of today
has just become a bunch of entitled, whiny,
temper tantrum throwing people, they
expect that they should be able to do whatever they want.
They should be able to go across whatever border they want
and not have to worry about any regulations, regulations that
are far more relaxed than anything
they would impose themselves on anybody
who doesn't look like them.
It's just the standard temper tantrum.
I don't really see it as much of a story.
And as long as the Canadian government
continues with its current approach
and doesn't bow to the pressure, because eventually there
will be pressure mounting, especially
from those people in Ottawa who are having
their businesses disrupted, that it's going to be like,
you know what?
Bring them in.
Get these people out of here.
The Canadian government's going to have to resist that
until the movement starts to disperse on its own.
Once that starts, if they want to go in and prove a point
and show that they're the big bad state
and they can do whatever they want, yeah,
they can do that if they wait for it
to start to disperse on its own.
If they do it before then, yeah, you're
probably going to have an influx of people
wanting to come to their aid.
So that's a bad move.
So anyway, this is why I really haven't talked about it.
From my perspective and the stuff that I cover,
it's kind of boring.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}