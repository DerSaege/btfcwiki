---
title: Let's talk about truckers in America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rNc9chSqRsg) |
| Published | 2022/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Organizers and right-wing pundits are supporting the idea of establishing a trucker convoy in the U.S.
- The organization behind the convoy, People's Convoy, is the only group with any real organizational capacity.
- The convoy was initially planned to start in March but has been moved up to the end of February due to lack of support.
- There is skepticism about the level of support for the convoy, especially in terms of actual trucker involvement.
- The convoy is planned to start in Barstow, but the choice of location raises questions about their public relations strategy.
- A large portion of the supporters of this convoy are linked to a belief system associated with negative events.
- Despite the small number of participants, there is concern about the potential for bad actions due to the devout nature of the supporters.
- The group supporting the convoy might be erratic, with plans like stopping at Tyler Perry's house, linked to their belief system.
- While the convoy may not gain momentum currently, there is a possibility of right-wing pundits pushing for its success in the future.
- Participation in the convoy is currently limited, but that could change due to external influences.


### Quotes

- "It does appear that this might run out of gas before it gets anywhere."
- "While small, it does appear that this is a group that might have a lot of capacity for doing bad things."
- "There is skepticism about the level of support for the convoy."
- "This is just all bad."
- "At this moment, participation seems limited, that could still change."


### Oneliner

Organizers and pundits support a US trucker convoy, but skepticism exists around its success and potential risks from certain beliefs.


### Audience

Online Activists


### On-the-ground actions from transcript

- Monitor the developments around the proposed trucker convoy (suggested)
- Stay informed about any potential risks associated with the convoy (suggested)
- Advocate for responsible and peaceful actions within communities (implied)


### Whats missing in summary

Insights on the potential ripple effects of right-wing pundits pushing for the success of the convoy.

### Tags

#TruckerConvoy #RightWingPundits #BeliefSystem #Skepticism #RiskMitigation


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
trucks and convoys and the American edition of all of this. So organizers who feel they can
benefit from it and a lot of right-wing pundits who have built a business model around manufacturing
outrage, they have thrown in behind the idea of establishing a parallel trucker type thing in the
U.S. The good news is that thus far it is laughably unorganized. It does appear that this might run
out of gas before it gets anywhere. The first attempt at organizing was to do something at the
Super Bowl. As we know that didn't occur or if it did it was such a failure nobody even knew it
happened. The next thing that got brought up that got any backing was meeting up in DC in August so
people could bring their kids and Congress would be forced to look at their kids and then the
civics experts involved in organizing this found out that Congress isn't in session in August.
So that got scrapped. Then the only group that appears to have any organizational capacity
whatsoever is called the People's Convoy and they started organizing with the idea of getting
started next month in March and now they are moving up that date to the end of February.
This is being read by most people, myself included, as them realizing that
people are not as supportive of this idea as the echo chambers online suggest.
There's also the issue of getting actual truckers involved rather than people just
taking that mantle because they have pickup trucks. There isn't as much support as people
might be led to believe and if you're doing a convoy that's going to be very apparent
by the number of trucks. This group, I want to say it's the 23rd or 24th, something like that,
they're talking about meeting up in Barstow to start. The selection of that town alone
kind of indicates that maybe they don't have the best PR. It's not that far from the coast.
It seems odd to not be able to claim a coast-to-coast convoy from the public relations standpoint
and while you may say that that really doesn't matter, all of this is PR. So it does.
Doesn't look like it has the support that they were hoping for. So all of this is good.
Bad news. Those who do seem to be supporting it are, at least a large portion of them,
are adherents to a certain set of beliefs involving a letter of the alphabet.
This belief system was heavily influential in the events of the 6th. It has been responsible for
a whole lot of, well, really bad things from events at pizza places to all kinds of other stuff.
This is just all bad. So while the number of participants at this point appears to be small,
there is the possibility that even though it's small in number, they're very devout.
The other thing is this group is likely to be very erratic. One of the proposed stops in this convoy
that luckily, as far as I can tell, didn't get a whole lot of backing was stopping by Tyler Perry's
house because that's where the fake White House is, where Biden is at. If you don't know about that,
I wouldn't waste your time looking into it, but that is a part of this belief system.
So while small, it does appear that this is a group that might have a lot of capacity
for doing bad things. So it's definitely something that we should be aware of.
Now, at this moment, it doesn't appear that this wagon train is really going to get rolling.
However, there are a lot of right-wing pundits that are behind this who, well, they would make money
off of it being successful, off of it gathering steam so they might start to push it.
While at this moment, participation seems limited, that could still change.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}