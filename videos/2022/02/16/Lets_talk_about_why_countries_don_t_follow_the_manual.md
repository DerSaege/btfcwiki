---
title: Let's talk about why countries don't follow the manual....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uy6U_kkHWLQ) |
| Published | 2022/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why countries sometimes deviate from the manual in dealing with civil disturbances and demonstrations.
- Mentions examples of situations where following or not following the manual leads to expected outcomes.
- Addresses a viewer's question about why heavy-handed methods are sometimes used against movements that threaten the status quo.
- Talks about the existence of literal manuals on civil disturbance and counterinsurgency.
- Points out that race plays a significant role in how different groups are treated by governments.
- Mentions the influence of public health experts and advisors in policymaking.
- Criticizes the aggressive approach often depicted in movies and how it influences decision-making.
- Notes the disconnect between expert advice and political actions, especially in Western countries.
- Emphasizes that brute force is not always effective in dealing with civil disturbances.
- Stresses the importance of understanding that the ability to endure, not to overpower, is key in such situations.

### Quotes

- "It isn't the group that can dish out the most that wins. It's the group that can take the most."
- "There are literal manuals about this on civil disturbance, counterinsurgency, stuff like that."
- "Race always has something to do with it."
- "In Western countries, in the United States especially, there is the idea that to win, you have to be aggressive."
- "Most of the major demonstrations you are aware of, that you know of, you know about them because of an overreaction by state forces."

### Oneliner

Beau explains why countries deviate from manuals in dealing with civil disturbances, citing race and aggressive tendencies as key influencers, ultimately stressing the importance of endurance over brute force.

### Audience

Policy influencers, activists

### On-the-ground actions from transcript

- Organize peaceful demonstrations to raise awareness and push for policy changes (suggested)
- Support marginalized groups facing unequal treatment by advocating for fair and equal enforcement of laws (implied)

### Whats missing in summary

Importance of addressing implicit biases and challenging the status quo to create a more just and equitable society.

### Tags

#CivilDisturbances #ManualDeviation #Race #PolicyMaking #ImplicitBias


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about why countries don't
always follow the manual.
This is a topic we've discussed on this channel
for a very long time.
And we have examples of both.
We have examples of, hey, they're
not following the manual.
It's going to go bad, and then it goes bad.
We have examples of, hey, they're following the manual.
It's going to go well, and it goes well.
We have both, and you can see it develop in real time.
The process works.
It's not really something that's debated.
If you don't know what I'm talking about,
there are set ways of dealing with civil disturbance
of demonstrations.
And the number one thing is you don't want to provide a security
clampdown, right?
Because that creates sympathy for the other side, right?
So here's a question that came in.
Hey, Beau, something's been bugging me
ever since you made that video on why countries sometimes
don't follow the playbook.
You say this is because indigenous and black rights
movements threaten the status quo.
However, you've also accurately pointed out
that failing to abide by the playbook
and using heavy-handed methods actually
strengthens the movement that is being targeted.
If a movement threatens the status quo,
wouldn't the playbook be used so that it has a harder time
growing?
By overreacting, it seems like the status quo
is hampering its own interests.
Is it so threatened that it begins to act irrationally?
Short answer, yes.
Long answer, also yes.
And for those who don't know, when
we talk about the playbook or the manual,
that's not figurative.
There are literal manuals about this on civil disturbance,
counterinsurgency, stuff like that.
You can find most of them, at least declassified versions,
on archive.org.
OK, so why does it seem like governments
use the playbook on those groups that really
don't threaten them, those that are protesting
in favor of the current system, like the truckers, right?
And then they go off script when it
comes to groups that actually threaten the status quo,
therefore strengthening them.
The obvious answer here is race.
And when you're talking about unequal enforcement,
race always has something to do with it.
But there's something else here as well.
And it's not just the internal bias that occurs,
although that factors into it.
Rather than think about this, about this topic,
think about the public health issue.
Subject matter experts, weirdos, advisors, these people,
they get brought in.
And they provide their best advice to policymakers.
They don't make policy, though.
Just like with the public health thing,
you can have experts walk in and say, hey,
it would be really great if everybody wore N95s
and have the policymaker make a point of not
wearing a mask in public.
It's the same thing.
In Western countries, in the United States especially,
there is the idea that to win, you have to be aggressive.
And you have to go in there and knock people around and beat
the bad guy up, because that's what's in all the movies.
That's not true when you're dealing
with something like this.
And it's counterproductive.
However, that's what's in all the movies.
That's what the voting base expects.
So politicians will often completely
disregard the advice of people they
have paid ridiculous sums of money to give them advice
and send in the guys with the shields and the batons.
Without fail, it doesn't work, not long term.
Understand, most of the major demonstrations
you are aware of, that you know of,
you know about them because of an overreaction by state forces.
So part of it is they are angry that those people,
those lesser people, challenged them,
that they're challenging the status quo.
And that implicit bias, that internal bias that they have,
well, that just makes it worse, because those people have
already been othered in their eyes.
And then they look at poll numbers
rather than professionals.
They look at those poll numbers, and the poll numbers say,
send in the brute squad.
And that's what they do.
And most of them seem completely unaware of the fact
that when it comes to stuff like this,
it isn't the group that can dish out the most that wins.
It's the group that can take the most.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}