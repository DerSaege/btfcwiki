---
title: Let's talk about Trump, boxes, Section 2071, and treason...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-XXKz4G7YYo) |
| Published | 2022/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Title 18, Section 2071 of the US Code, currently circulating on social media in meme form.
- Talks about potential applications of this statute to former President Trump.
- Addresses the idea of changing the legal definition of treason and its relationship to the current situation.
- Clarifies that treason is defined in the US Constitution and can only be changed through an amendment.
- Points out that changing the legal definition of treason is unlikely and Congress may create a new charge instead.
- Details the penalties under Title 18, Section 2071 for removing items from public offices without authorization.
- Mentions that violating this statute could result in fines, imprisonment up to three years, and disqualification from running for public office.
- Explains the qualifications to be President as defined in the Constitution.
- Suggests that disqualification due to violating Section 2071 may be unconstitutional since presidential qualifications are set by the Constitution.
- Mentions legal cases that establish Congress's limitations in changing qualifications without an amendment.
- States that the excitement over disqualification under Section 2071 may not be applicable due to constitutional constraints.
- Notes the recovery of documents from a golf course that were supposed to be at the White House or archives.
- Raises the possibility of legal liability for the former president regarding the removed documents.
- Concludes by mentioning that while legal actions are possible, the anticipated penalty may not be applied.

### Quotes

- "Treason is a crime outlined in the U.S. Constitution."
- "If Congress wants to change that, they need an amendment."
- "The excitement over disqualification may not be applicable due to constitutional constraints."
- "There is a possible avenue for seeking an indictment and a conviction."
- "That penalty that everybody's super excited about, not going to be applied."

### Oneliner

Beau explains US Constitution's treason definition, Section 2071 penalties, and presidential qualifications' constitutional restrictions, dampening excitement over potential disqualification.

### Audience

Legal analysts, political enthusiasts.

### On-the-ground actions from transcript

- Contact legal experts for a detailed understanding of Section 2071 and its implications (suggested).
- Engage in legal or political advocacy to address concerns regarding constitutional limitations on changing qualifications (exemplified).
- Stay informed on any developments related to the discussed legal matters (implied).

### Whats missing in summary

In-depth analysis of legal precedent and potential implications for similar cases in the future.

### Tags

#USConstitution #Section2071 #Treason #PresidentialQualifications #LegalImplications


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the US Constitution.
We are going to talk about Title 18, Section 2071,
which is a part of the US Code
that is making the rounds on social media right now
in meme format and has a whole lot of people really excited.
We're going to talk about that statute
and its possible applications to former President Trump.
And we're going to talk about changing the legal definition
of treason, because these two topics are closely related,
but probably not for the reason you think.
OK, so we'll go over the treason stuff first,
because that's quick.
I had a question that came in and said, hey,
You know, with everything going on, is there any way Congress can go in and change the
definition of treason to make it a little bit more applicable to everything going on
today?
And the answer to that is kind of, but no, not really.
Treason is a crime outlined in the U.S. Constitution, Article 3, Section 3, Clause 1.
It states, treason against the United States shall consist only in levying war against
them or in adhering to their enemies, giving them aid and comfort. That's what
treason is. Period. Full stop. If Congress wants to change that, they don't need a
bill. They need an amendment. You'd have to amend the Constitution to change the
legal definition of treason. So is it theoretically possible? Yes. Is it going
to happen? No, because you're never going to get an amendment for that. They'll
just pass a bill for a different charge that is kind of similar and use that instead.
They'll create a new statute under a new crime.
Okay, so now on to the part that everybody's probably curious about.
Title 18, Section 2071, it is a statute that deals with the removal of books, maps, papers
from public offices and short version, if you remove stuff you're not supposed to,
let's say hypothetically speaking, boxes upon boxes of it from the White House and take it to
a golf course, you might be in violation of this law. And the reason people are excited is because
the penalties are kind of unique. The first one is a fine. The second one is up to three years in
prison. The third one is that you get disqualified. You can't run for public
office anymore. That's why people are excited. That's what's on the memes and
all of that is true. However, there's something here that we should go over
and this is the point where I burst everybody's bubble. The qualification to
to be president, they are also defined in the U.S. Constitution. As it applies
today, you have to be 35 years old, be a resident of the country for 14 years, and
be a natural-born citizen. Period. Full stop. If Congress wants to change that,
they need an amendment, not a bill, not a statute.
The portion about being disqualified, it's very likely that that is unconstitutional.
That that portion cannot be applied to any office that has its qualifications expressed
in the Constitution.
If it's defined in the Constitution, they can't change it that way.
Because in essence, they're changing the qualifications and now they are, you have to be 35, be a
resident for 14 years, be a natural born citizen and have never been convicted of this crime.
They can't do it.
There are two cases dealing with something similar, Powell versus McCormick and US term
limits incorporated versus Thornton.
Basically, what's been decided is that Congress doesn't even have the power to change Congress's
qualifications.
They can't alter what was expressly defined in the U.S. Constitution without an amendment.
If Congress can't alter their own qualifications, they certainly cannot alter the qualifications
for the presidency.
So the part that has people really excited is probably unconstitutional and probably
isn't going to be applied.
Now if you're looking for a silver lining, there were in fact boxes of stuff that were
supposed to be at the White House or go to the archives that were in fact down at the
golf course.
Now whether or not the former president has any legal liability for that, that's up for
debate, I guess.
But that part is true.
There were documents recovered from down there.
So there is a possible avenue that prosecutors could look at as far as seeking an indictment
and a conviction.
But that penalty that everybody's super excited about, not going to be applied.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}