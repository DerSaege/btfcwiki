---
title: Let's talk about understanding foreign policy as it is and working on cars....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KNZFOkE6Lxs) |
| Published | 2022/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into American foreign policy, stressing the importance of understanding it as it is rather than how we wish it to be.
- He uses a story about fixing cars to illustrate his point about American foreign policy.
- Beau differentiates between three types of people when it comes to understanding cars: those who can do basic repairs, those who can diagnose issues over the phone, and those who struggle to identify anything under the hood.
- Drawing a parallel, Beau compares most Americans' understanding of foreign policy to the friend who struggles with car repairs, focusing on symptoms rather than the root cause of issues.
- He points out that many Americans see military adventurism and drones as issues with foreign policy, but these are merely symptoms of a deeper problem.
- Beau suggests that the over-reliance on the US military for policy execution is one of the major flaws in American foreign policy.
- He advocates for a shift towards a more cooperative approach in foreign policy, utilizing economic ties and diplomatic solutions over military intervention.
- Beau criticizes common foreign policy takes on platforms like Twitter, likening them to trying to fill a car with gas when the engine is missing.
- He mentions a recent movement involving Ted Cruz advocating for sanctions on a pipeline, showcasing how such actions can hinder diplomatic efforts.
- Beau stresses the importance of understanding the flaws in foreign policy to work towards improving and refining it for the future.

### Quotes

- "Most Americans have Steve's view of foreign policy."
- "The problem is the engine is out of the car."
- "I want to make sure that people understand the parts so they can fix it."

### Oneliner

Beau explains American foreign policy through a car repair analogy, urging a shift from military-centric approaches towards cooperative diplomacy for effective change.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Analyze and understand the root causes of issues in foreign policy (implied)
- Advocate for a shift towards cooperative diplomacy and economic solutions in foreign affairs (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of American foreign policy flaws and advocates for a shift towards a more cooperative and less militaristic approach. It includes insightful analogies and examples to enhance understanding.

### Tags

#AmericanForeignPolicy #CooperativeDiplomacy #MilitaryIntervention #PolicyChange #Analogies


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit
about American foreign policy
and why it's important to understand
American foreign policy as it is today
and rather than the way we want it to be.
And we're also going to talk about fixing cars.
I'm gonna tell a story
And remember this story, because I'm
going to build off of it later.
But I got this message.
Why do you do videos on foreign policy explaining, quote,
the way it is instead of focusing more
on, quote, the way it should be?
Something I say a lot, right?
I know you do some, but it seems like you focus more on what is.
Doesn't saying the way it is reinforce the idea
that that's the way it should be?
I sure hope not, because that is definitely not the intent.
I disagree with 95% of American foreign policy.
So I really hope that it doesn't come across as me advocating that the way it is, is the
way it should be.
So years ago, a friend of mine, a guy named Jason, we're going over to a third friend's
house.
Right?
When we turn on the street, we can see our third friend, his
name's Steve.
We can see him standing in the driveway, hood up on his car,
doing that guy thing, looking down into the engine.
And Jason and I, we just start laughing, just burst out
laughing, because there are three types of people when it
comes to understanding cars.
There's people like me.
I can change my oil, change belts, replace an alternator.
I can do basic repairs.
And if I don't understand something,
I don't have to read one of these, right?
Then there's people like Jason.
And I'm not joking when I say this.
I have seen this multiple times from both ends of the phone
call.
He is somebody who, if you have a problem with your car,
you can call him.
And he will literally say, put the car on the phone.
You crank the car up, hold the phone up to the engine,
and he will tell you what's wrong with it.
and be right pretty much all the time.
I've only seen him be wrong once or twice.
And then there's people like Steve.
And the reason it's funny that he had the hood up
is because I'd be willing to bet that he couldn't identify
anything inside the engine compartment other
than the battery.
But he's trying to figure it out.
We pull in the driveway, continue laughing for a few
seconds, because on top of that, there's a personality
thing here as well that's funny.
But we get out of the car, walk up to him.
He is so perplexed by what he's looking at, he doesn't
even realize we've arrived.
And Jason stands next to him, looks into the engine,
working on the car.
And this guy, this is when he realizes we're there.
And he's like, oh, great, these two.
This is just what I need today.
And Jason's like, have you figured out what's wrong with
it, at least?
He says, it won't start.
Jason's like, that's not what's wrong with it.
That's just what you noticed.
Most Americans have Steve's view of foreign policy.
They notice that they don't like what's happening.
They know it's not working the way it should, but they tend to only notice the symptoms.
If you ask most Americans what's wrong with American foreign policy, they're going to
say, military adventurism, drones, destabilizing other countries, stuff like this, and yeah,
all of this isn't good stuff, but these are all symptoms.
It's not actually what's broke.
It's not what's actually causing the problem.
It's the manifestation of the problem.
If you want to fix it, you have to know what's broke.
If I had to say one thing, to keep it simple,
to give you an example, I would say
that an over-reliance on using the US military to exercise
policy is probably the biggest problem we have. A lot of the stuff that we use the
military for, it's not the military's job. You know, when you're talking about
maintaining access to markets or maintaining access to natural resources,
making sure a country in a strategically important area stays politically aligned,
stuff like this, these aren't really jobs for the military. In the system we exist
and most of this should be handled by State Department, or when it comes to economic concerns,
should be handled by private business, again, in this system.
The idea would be to be more competitive using this system, right?
And those strong economic ties would then matter a little bit more.
They would create friendlier relations, therefore we don't have to rely on the military as much.
Now, again, that's the way it is.
The way it should be, from my point of view, is that we should be moving to a much more
cooperative system, however, if I was to start talking about that, it wouldn't even make
sense.
It would read like a lot of the foreign policy takes you find on Twitter, and if you don't
know, if you ever want just a lesson in bad foreign policy takes, you can find a bunch
of them there.
whole bunch of them that I look at, and I just shake my head.
Because going back to the car analogy, the problem is the
engine is out of the car.
There are no tires on the car, and the presented solution is
to fill the car with gas.
They don't normally make sense.
And this isn't limited to your average citizen.
There was a movement very recently, did a video about
But where Ted Cruz was championing the idea of going ahead and slapping sanctions on a
pipeline, and it was a horrible idea because it reduced the diplomatic leverage that Biden
used yesterday, I think depends on when this video goes out, but very recently to push
back on Russia.
Had those sanctions already been put in place, he wouldn't have had that leverage.
would already be expended.
That is pretty typical of American foreign policy takes.
They're based on the idea that we're going to back everything up with military force,
so we can go ahead and just use all the other options up front just to show that we mean
business and not get anything out of it.
The US has a whole bunch of tools when it comes to foreign policy.
I have a video, I want to say it's called Foreign Policy Strip Malls or something like
that, that kind of goes over some of them.
When I talk about the way it is, I'm trying to give you one of these.
I'm not actually saying that that's what I support.
And I hope that given all of the other stuff that I talk about, people know that.
I really hope it isn't reinforcing the idea that the way American foreign policy is, is
the way it should be, because that is not the case.
I want to make sure that people understand the parts so they can fix it, so eventually
we can soup it up and make it better.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}