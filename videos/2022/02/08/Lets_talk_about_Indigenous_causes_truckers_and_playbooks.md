---
title: Let's talk about Indigenous causes, truckers, and playbooks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QOHGhn0o_pI) |
| Published | 2022/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why countries deviate from the playbook despite knowing it.
- Mentions the Canadian truckers' situation and indigenous causes as examples.
- Points out that racism and capitalism play a significant role in these deviations.
- Illustrates how movements are seen as threats to the status quo.
- Emphasizes that the response to movements depends on their perceived impact on the existing system.
- Compares the relevance of truckers to the status quo with that of indigenous causes.
- Notes the high vaccination rate in Canada and the expected relaxation of restrictions without protests.
- Stresses the difference in treatment between movements advocating for the status quo versus those aiming for real change.
- Addresses the power dynamics and historical overreactions by governments.
- Encourages individuals, especially from the majority group, to show up in support of marginalized groups to counter public opinion manipulation.

### Quotes

- "The reason it historically gets visited upon marginalized groups is because they're smaller in number."
- "Marginalized groups tend to be on the right side of history."
- "If you ever needed a reason to show up, that's it."

### Oneliner

Countries deviate from the playbook based on perceived threats to the status quo, with racism and capitalism playing pivotal roles, impacting responses to movements advocating for real change versus those supporting the system.

### Audience

Activists, Allies, Advocates

### On-the-ground actions from transcript

- Show up in support of marginalized groups (implied)
- Attend protests and demonstrations to counter public opinion manipulation (implied)
- Join movements advocating for real change (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how power dynamics, racism, capitalism, and threats to the status quo influence government responses to different movements.

### Tags

#DeviationFromPlaybook #Racism #Capitalism #MarginalizedGroups #Support


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about what causes
countries to deviate from the playbook.
Why sometimes they don't use the manual,
even though they know what the manual says.
After that last video on the truckers,
a whole bunch of people started asking, you know,
well, if they know it and they're using this playbook now,
why didn't they use it in these other situations?
The most commonly cited, coming from people in Canada,
are situations involving indigenous causes, right?
And the answer is simple.
The same reason the United States didn't use the playbook
when dealing with BLM.
The obvious answer, the one that you see right away,
is well, racism. And yeah, that's part of it.
The more developed answer is racism and capitalism.
Big bucks involved, right? It's also true.
And then there's the thing that really kind of cinches
how much force these powers that be
are going to be willing to use.
And that's whether or not that movement is an actual threat
to the status quo.
Let's think about our truckers in Canada for a second.
They are, for lack of a better word, irrelevant.
As far as the status quo is concerned, they're irrelevant.
They don't matter at all.
You're talking about a country that is, what, 80, 85% vaxxed?
Something like that. It's really high.
Canada enjoys a high vaccination rate, right?
And currently, we are on what most expect to be
the downslope of the pandemic, right?
So much so that a whole bunch of jurisdictions
are lifting their restrictions without protests.
Australia opening up its borders.
It's...
The truckers don't matter to the status quo,
to the powers that be. The truckers are irrelevant.
Let's say something happens and they get their way.
Oh no, we open up immediately and there's more capitalism.
There's more money, right?
And that money tends to flow to those at the top.
They don't bother anybody.
They are irrelevant from the status quo's position.
Canada and the United States aren't that different
if the powers that be up there thought that they could get away
with just letting it run wild
and maintaining the economy at full speed.
They would have, but they knew that that would have been just a
little bit too far in the court of public opinion, right?
So in essence, the trucker thing that's going on,
it doesn't matter. These restrictions,
all of this stuff will be eventually be relaxed over time
with or without protest.
It doesn't matter to those in power.
So what about the indigenous causes?
Is that the same?
No, because they're advocating for real change.
They're advocating for things that are not in line
with the status quo.
You know, right now, all the truckers are trying to paint themselves
as like, you know, trying to undermine everything
and fight back against the powers that be.
No, you're doing exactly what they want you to do.
However, with indigenous causes,
let's just take one of the more prominent ones,
defending water from pipelines, right?
Yeah, that's a lot of money.
That's a lot of money that's going to be made by big and powerful companies
that have big and powerful friends in government.
It also would require more of a shift to greener energies.
It disrupts the status quo.
It's an actual threat.
The racism, yeah, that makes it easier
for them to really clamp down
because racism is normally targeted
at groups that don't have institutional power.
They don't have a lot of power and they don't have a lot of numbers.
So as long as they can convince members of the majority group
that it's okay, or to at least look the other way
when that force is used, well, then they can use it.
That's what it's about.
The reason it historically gets visited upon marginalized groups
is because A, they're smaller in number.
Therefore, they're easier to scapegoat.
Therefore, they're easier for the powers that be
to turn the populace against them in the court of public opinion.
They don't have the institutional power to stand back.
And most importantly, to fight back, I'm sorry,
they don't have the institutional power to fight back.
But most importantly,
marginalized groups tend to be on the right side of history.
And they're calling for real change, deep change,
change that would upset the status quo.
That is when you see the powers that be overreact.
That's when you see them decide to crush it
rather than go through the playbook.
It's because they actually want to change the system,
not just protest in favor of the system.
That's the difference.
And if you look back historically to situations
where the government overreacted,
that's what you're going to find.
Generally speaking, you're talking about dynamics
that have a lot of variables.
So there's always going to be exceptions.
But normally, that's the formula that you will find.
You will find it questions making money
and it upsets the status quo.
And the people being targeted are marginalized.
They're easy to turn the populace against.
This is why if you are part of the majority group,
so if you're in the US or Canada, and you are white,
this is why it's important for you to show up.
Because the more of those faces that are in that crowd,
the harder it is to turn public opinion against them.
The harder it is to use that latent racism
that exists in most Western societies.
Because if it's all there,
if you have this collective group of people,
and if you have those white faces in that crowd,
it makes it harder to use that tactic.
It makes it harder to paint it
as these other people are doing it.
They're not like us
because there are people like us, quote, in the crowd.
If you ever needed a reason to show up, that's it.
Because you can actually, just your presence
can help tone down the response from the powers that be.
Anyway, it's just a thought. You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}