---
title: Let's talk about the CRT Republicans love....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0B_Iw-Q-msc) |
| Published | 2022/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains that Republicans have their own theory called Causal Relationship Theory (CRT) that they use to analyze government policies and their impact on the average American.
- Republicans believe that government actions have long-lasting effects, citing examples like welfare policies and historical legislation that marginalized certain groups.
- Points out the importance of understanding how past legislation continues to impact society today, especially in areas like race relations.
- Questions why Republicans oppose teaching this theory to students, suggesting it may be to maintain systems of inequality and prevent individuals from understanding the effects of past legislation on their lives.
- Emphasizes the necessity for future leaders to comprehend the causal relationship between past policies and present outcomes in order to address and correct societal issues.

### Quotes

- "Previous legislation has an impact today. You can't argue with it. It's just fact."
- "If we want to fix it we have to know about it."
- "They have to understand that causal relationship."

### Oneliner

Republicans have their own theory, Causal Relationship Theory (CRT), that explains how past legislation impacts current society, raising questions about why they oppose teaching it to students.

### Audience

Students, educators, policymakers

### On-the-ground actions from transcript

- Teach students about the causal relationship between past legislation and present societal issues (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Republicans' use of Causal Relationship Theory and the implications of understanding the impact of past policies on current society. Viewing the full video can provide a deeper insight into Beau's perspective and arguments. 

### Tags

#Republicans #CausalRelationshipTheory #CRT #GovernmentPolicies #Education #Equality #SocietalImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a different theory.
You know, there is one academic theory
that is catching a whole lot of media right now.
The Republicans are just absolutely opposed to it,
and that's what's been capturing all of the headlines.
But today, I wanna talk about something else,
because something that isn't as well known
is the fact that Republicans have their own theory.
They have their own theory about how things work,
and they use it all the time.
And where it gets weird is that it is also CRT.
Those are the initials.
It's not the same thing.
It's not critical race.
It's causal relationship theory, CRT, just a different one.
We're going to talk about that today.
The general idea is that the government institutes some policy and then that has a causal relationship.
It causes something that impacts the average working American, right?
As an example, a recent one, that whole thing with the harm reduction stuff, right?
They say because of this policy and because of distributing this stuff, well, there's
going to be increased use. Now we're not going to debate these points right now.
These are just examples. This is how they structure their
arguments. They believe that the government does something and it
impacts people. And the fact is, if you're a Democrat, you can't argue with that.
It's true. That's how it works. You can argue about whether or not a specific
government action had a specific effect but the idea that's contained there, it's
100% accurate. There's no room for debate there, it's just fact. Government
actions have effects on the average person and sometimes those effects can
be very very long-lasting. If you look at like Republican talking points
concerning welfare. They're talking about stuff that occurred decades ago and how
it is still impacting stuff today. Again, you can debate it as to whether or not
that link is as valid as they believe it is, but you do know that government
policies have an impact and this goes to foreign policy and domestic stuff.
Another good example would be, say, I don't know, there was decades and decades and decades and decades of legislation
that marginalized a certain group of people, designed to keep them down, I don't know, let's say through segregation and
slavery and then later on some economic policies and perhaps a whole bunch of criminal justice stuff that was aimed at
them.
The causal relationship theory says that those policies would still have impacts today.
I think that's true. I believe that to be accurate. Now my question is why don't
Republicans want this taught? This is how they structure their arguments in
everyday life. Why don't they want, under causal relationship theory, why don't
they want black students to understand the relationship that previous
legislation has had on their life because see I can only think of one
reason I think it's because perhaps those people want to continue to uphold
that system because you know if a bunch of students if they're never told that
the reason a disproportionate number of their peers are in poverty or are low
income or have family members in the criminal justice system. If they're never told that's
because of a causal relationship to previous legislation, well then they'll just think they're
lesser, right? And I think that's the goal. I think it has to do with trying to uphold
that system and perhaps a little bit of embarrassment if maybe their family members
had cheered that legislation on. But even if somebody rejects the the idea of critical
race theory, causal relationship theory, which is how Republicans structure their arguments every
day, says the same thing is true. Previous legislation has an impact today. You can't argue
with it. It's just fact. It applies to all sorts of stuff including a lot of stuff
centering on race in this country. If we want to fix it we have to know about it.
If the students who are going to one day be the leaders of this country are going
to be tasked with correcting the mistakes of the past they have to
understand it. They have to understand that causal relationship. Anyway, it's just
That's a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}