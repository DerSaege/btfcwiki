---
title: Let's talk about a win for wolves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rXJyraMwUCQ) |
| Published | 2022/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gray wolves were delisted from the Endangered Species Act during the Trump administration, shifting protection responsibilities to the states.
- A recent court ruling relisted the gray wolf under the Endangered Species Act due to inadequate data collection.
- Despite this win, Wyoming, Idaho, and Montana are not covered by the relisting, where significant wolf-related issues persist.
- Organizations are working towards an emergency listing of gray wolves in these states with some federal support.
- There's skepticism about the three states' willingness to address the wolf protection issue due to economic interests and a certain image they wish to maintain.
- Continued support for organizations striving for wolf protection is necessary even after the recent win.

### Quotes

- "Good news. That's a win."
- "There's been a big win, but the fight isn't over."
- "Y'all have a good day."

### Oneliner

Gray wolves were delisted but recently relisted under the Endangered Species Act, with ongoing efforts needed in states where protections are lacking.

### Audience

Conservationists, Wildlife Advocates

### On-the-ground actions from transcript

- Support organizations working on emergency listing of gray wolves in Wyoming, Idaho, and Montana (implied)
- Provide assistance and resources to organizations collaborating with Fish and Wildlife and the Department of the Interior for wolf protection (implied)

### Whats missing in summary

More details on specific organizations and resources available for supporting wolf protection efforts.

### Tags

#GrayWolves #Conservation #EndangeredSpecies #WildlifeAdvocacy #EnvironmentalProtection


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about wolves. Gray wolves.
And good news, everyone.
There's been a pretty big win.
However,
there is a missing piece of the puzzle before we can really count it as a win,
so stick with me.
Okay,
so a little bit of background information.
During the Trump administration,
they delisted
the gray wolf.
They took it off the Endangered Species Act. It's no longer protected.
The responsibility for protecting the wolves fell back to the states.
Now, this was challenged in court. The lawsuit
really focused on how the data was collected, and I'm not going to get into all of that.
Short version, and I'm paraphrasing here, obviously,
the judge was kind of like, yeah, you looked at the core populations, but not the species as a whole.
That's not how we do things.
The gray wolf is relisted.
So it is now being protected again by the Endangered Species Act.
Good news. That's a win.
However,
and this is a big however here,
there are three states
that are not
covered by this, because in this area, the wolf was delisted in a different way.
That's Wyoming, Idaho, and Montana.
If you have been following the coverage on this channel,
you know that that is where
most of the truly egregious behavior
is coming from.
That is an area where the protections
by the state are
lacking,
to say the least.
This issue, it's going to be ongoing.
There's a huge win here,
but
we can't let people think that the fight is over,
because if you learned about this issue from this channel,
understand, I think that every,
I think every single
instance and situation that I brought up
was in one of these three states.
The area where the most help is needed
is the area where it isn't covered.
Now, we do have some good news in that fight.
There are organizations that are trying to get an emergency
listing
of the gray wolf in those states,
and
I wish them the best. I hope it works,
and I think it might,
because the Secretary of the Interior
has
kind of been vocal
about this issue,
and
basically the feds were like, we're concerned,
I think is the word they used.
The way it came across to me
was,
yeah, you three states, y'all need to clean up your act or we're going to do it for you.
That's
kind of how it came across.
I'm hoping that's the case,
and I hope they do it quickly, because these states are not going to clean up their act.
Not in my opinion.
They've had ample time. They're aware
of the damage that is being caused. They don't care. They care about the economic
interests
and
about casting the image as some mountain man.
So
there are organizations that are trying to work with Fish and Wildlife and
the Department of the Interior as a whole to make this happen.
I'll have some links down below.
There's been a big win,
but the fight isn't over.
So we don't want to forget about those organizations who have been having
success and who have been getting this done.
They're still going to need support.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}