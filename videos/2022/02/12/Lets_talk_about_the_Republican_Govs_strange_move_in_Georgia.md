---
title: Let's talk about the Republican Govs' strange move in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KTC2W_Jsae4) |
| Published | 2022/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican establishment and big money are distancing themselves from Trump and financially supporting defeating his chosen candidates.
- The Republican Governors Association, which usually endorses the incumbent, committed half a million dollars in Georgia to back Governor Kemp against Trump's preferred candidate.
- Large associations like this traditionally do not spend money on ads in primaries, making this move unusual.
- A growing number of long-time insiders and organizations are visibly moving away from Trump and his allies.
- Speculation suggests that they may have inside information about something negative coming for Trump and his crew.
- There seems to be an effort to re-establish the old guard and distance from Trump to protect the Republican Party's image.
- The significant financial backing behind this distancing indicates a serious effort.
- The shift away from Trump is not limited to individuals like Graham and McConnell but involves a broader chorus of institutions and people.
- The motivation behind this distancing remains suspicious and suggests foreknowledge of impending events.
- The Republican Party's image may be a key factor driving this break from Trump.

### Quotes

- "The big donors and the organizations or establishment insiders, the people who've been around a long time, distancing themselves from Trump and Trumpism."
- "Somebody somewhere got inside information as to what is coming for Trump and that crew."
- "They don't think it will reflect well on the Republican Party as a whole, so they are trying to kind of re-establish the old guard and break away from Trump."

### Oneliner

The Republican establishment is distancing from Trump, supporting opponents, possibly due to foreknowledge of damaging events.

### Audience

Political observers

### On-the-ground actions from transcript

- Support candidates not endorsed by establishments (suggested)
- Stay informed about political developments (suggested)

### Whats missing in summary

Insights on the potential long-term impact of this distancing from Trump on the Republican Party and future elections. 

### Tags

#RepublicanParty #Trump #PoliticalAnalysis #Foreknowledge #ElectionStrategy


## Transcript
Well howdy there, Internet people. It's Beau again. So today we're going to talk about another
development in the long-running string of developments that indicate the Republican
establishment and more importantly the big money behind the Republican Party are done with Trump.
Not just done supporting him, but now starting to distance themselves from him and also
commit financial resources to defeating his chosen lackeys.
The Republican Governors Association is exactly what it sounds like. It's an association of
Republican governors. The whole point is to get Republican governors elected or re-elected.
This association by tradition typically endorses or kind of gives their nod
to the incumbent. What they don't do is waste money on ads in primaries. That's not a thing.
Republicans don't waste money campaigning against each other, not in any significant way.
Especially the large associations that have access to hundreds of thousands of dollars
for a single ad buy. That would be unheard of. It is now heard of. In Georgia,
the Republican Governors Association committed half a million dollars to backing the current
incumbent governor Kemp against whatever whoever it was that Trump said was going to do whatever
he was told. His opponent there, half a million dollars in a primary for a single ad buy.
That is unusual. That's unusual and it is just one more thing where you have the big donors
and the organizations or establishment insiders, the people who've been around a long time,
distancing themselves from Trump and Trumpism. I still don't know why. It is suspicious. To say
the least, my gut tells me they know something's coming. This isn't just Graham. It's not just
McConnell. It's a growing chorus of institutions, organizations, and people who have been around
a really long time that are pushing away Trump and his allies in a very visible way.
The only reason I can think of is that somebody somewhere got inside information
as to what is coming for Trump and that crew.
And they don't think it will reflect well on the Republican Party as a whole, so they are trying to
kind of re-establish the old guard and break away from Trump. How successful it's going to be,
don't know, but they're willing to put some serious bucks behind it. Half a million dollars
for a single ad buy in a primary in Georgia. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}