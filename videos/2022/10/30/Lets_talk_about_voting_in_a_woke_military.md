---
title: Let's talk about voting in a woke military....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0NFEiMFrM7A) |
| Published | 2022/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the effectiveness of voting and civic engagement.
- Responding to a viewer's disagreement on the impact of voting.
- Drawing an analogy involving a military scenario to explain the concept.
- Emphasizing that voting may not bring systemic change but can buy time.
- Touching on the topic of military inclusion and its implications.
- Acknowledging the background of the viewer's boyfriend as former 18F.
- Signing off with well-wishes for the audience.

### Quotes

- "Voting is never going to give you systemic change."
- "You're not going to achieve that greater world that you probably want through voting."
- "It can buy you time. You can have the least bad option."
- "There's another topic going on right now about the they them military."
- "That's about as hardcore as you get right there."

### Oneliner

Exploring the role of voting and civic engagement, Beau addresses disagreement, using a military analogy and stressing the temporary nature of voting's impact while touching on military inclusion concerns.

### Audience

Civic-minded individuals

### On-the-ground actions from transcript

- Engage in local capacity-building initiatives to drive real change (implied)

### Whats missing in summary

Deeper insights on the impact of military inclusion and its implications on society.

### Tags

#Voting #CivicEngagement #MilitaryInclusion #CapacityBuilding


## Transcript
Well, howdy there Internet people, it's Beau again. So today we have a question about voting and
effective forms of civic engagement and
it also kind of leads into something about
you know a lot of
Republican complaints
about the current state of the military
Okay, so here's the message and this is a really long message
So I'm just going to hit the highlights.
My boyfriend loves your channel, but he disagrees with one thing you say.
And he's being a little cute little girl puppy about it.
You say voting is the least effective form of civic engagement.
He says it's ineffective and says capacity building at the local level is the only way
to get real change.
I like the last part of that sentence a lot.
Um, and then it goes on, our rights are on the line and we live in a state
that will be a close this election.
I told him I was going to ask you to convince him.
And he told me to tell you that if you're going to craft some cute
analogy, you should know he's former 18 F.
Well, I will try not to make him mad in the process.
For those who don't speak the language, 18F is a certified American tough guy right there.
That's somebody with a long tab in the Army, funny little green hat, special forces, intelligence sergeant.
So not just, you know, an Oakley wearing door kicker, but also somebody who can do
the collection and the target analysis and all of that stuff as well. The
archetype of the PhD who can win a bar fight, although probably not actually a
PhD. But that's that's who you're talking about here. Kind of the the ideal when it
comes to the the image of American masculinity okay voting is the least
effective form of civic engagement voting is an ineffective form of civic
engagement it's all a matter of perspective in whether you're looking at
the wider campaign least effective sure these people that are going to be sent
to DC or to your state capitol, they're not saviors.
They're not going to solve all of your problems.
Half of them probably don't even want to.
They're not saviors.
But does that mean that they're ineffective?
Here's your cute little analogy.
You and your ODA, you and your team,
you're pinned down in a riverbed.
You got 200 people surrounding you.
Your rifles, they're not going to turn the tide of that.
Pretty ineffective at winning the engagement.
What's the solution?
Air support, right?
Something from above coming in and altering things,
building a greater capacity for violence of action, right?
OK, so the rifles are ineffective
at winning the engagement.
However, can your rifles buy you two or maybe four minutes for air support to arrive?
That's what voting is.
Voting is never going to give you systemic change.
You're not going to achieve that greater world that you probably want through voting.
You're going to have to build the capacity, you're going to have to change society and
slowly these things will filter through. You're not going to be able to vote it out. Agreed.
But it can buy you time. You can have the least bad option. Or maybe even get an option
that doesn't actively try to make it worse. There's your cute little analogy. But is that
cute little analogy the reason I wanted to make this video. No, not at all. There's
another topic going on right now about the they them military and the the
weakness that allowing those people in and what it what it does. I want to point
out that the boyfriend is former 18F. That's about as hardcore as you get right there.
That's the boyfriend. The message is signed from William. Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}