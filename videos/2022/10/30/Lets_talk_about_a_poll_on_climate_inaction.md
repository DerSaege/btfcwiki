---
title: Let's talk about a poll on climate inaction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4ElgDLR6Ga4) |
| Published | 2022/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent poll shows two out of three Americans believe federal government isn't doing enough to fight climate change.
- Inflation Reduction Act, the largest investment to fight climate change in US history, just passed without a single Republican vote.
- Republican Party is blocking action to serve their campaign donors' interests.
- With Democratic Party majorities, there could be real action on climate change.
- Republican Party is stopping progress, not the federal government.
- Need for immediate action to mitigate climate change, as current efforts are not sufficient.
- Republican Party is the political obstacle hindering climate change mitigation efforts.

### Quotes

- "Republican Party is blocking action to do the bidding of their campaign donors."
- "The Republican Party is stopping it. Because, I mean, why spoil their own investments?"
- "The actions we need to take to mitigate climate change should have started like yesterday."

### Oneliner

Recent poll shows dissatisfaction with federal government's action on climate change, but the real obstruction lies with the Republican Party's blocking of initiatives like the Inflation Reduction Act.

### Audience

Advocates for climate action.

### On-the-ground actions from transcript

- Advocate for political candidates who prioritize climate change action (implied).
- Support and vote for candidates who have plans for effective climate change mitigation (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics hindering climate change action in the U.S.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about some polling,
climate change, and a misframing of a question and an attitude.
Recent poll said that two out of three Americans
believe that the federal government isn't doing enough
to fight climate change.
And yeah, I mean, that's true.
But here's the thing.
That's on the ballot.
That is directly on the ballot this midterm.
Absolutely.
Because the phrasing of that is a little bit off.
The Inflation Reduction Act that just passed
is the largest investment to fight climate change
in US history.
It's the biggest that's ever been done.
It's not that the federal government isn't acting.
It's that the Republican Party won't allow it to.
And that sounds just like partisan nonsense, right?
The Inflation Reduction Act, the largest investment
to fight against climate change in US history,
passed without a single Republican vote.
Not one.
In the House or in the Senate, not one.
This is not a both parties thing.
This isn't a government-wide thing.
This is the Republican Party blocking action
to do the bidding of their campaign donors.
That's what's happening.
If the Democratic Party had larger majorities,
you would see action on this.
There aren't a whole lot of things that I'd say,
you know, if you give the Democratic Party majorities,
you'll definitely see action.
This is one.
If the Democratic Party was to maintain control
and actually get control in the Senate, real control,
where they're not being spoiled by a couple of Democrats,
so much more would go through.
This isn't the federal government not doing enough.
This is the Republican Party stopping it.
Because, I mean, why would they spoil their own investments?
I mean, it's not like they actually care about your kids
once they're born.
I don't really think they care about that anyway,
but it's a good talking point, right?
The actions that we need to take to mitigate climate change
should have started like yesterday.
What's included in the Inflation Reduction Act is good,
but it's not enough.
The political apparatus standing in the way
is the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}