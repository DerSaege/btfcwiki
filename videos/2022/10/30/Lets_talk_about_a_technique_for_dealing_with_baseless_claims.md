---
title: Let's talk about a technique for dealing with baseless claims....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4xdiTUl6wJs) |
| Published | 2022/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Someone received a message about a baseless claim of cities in the US starting a civil war.
- MAGA Republicans were advised to prep, pray, and stay out of the way as the military intervenes.
- The message warned of a potential 10-day information blackout with only emergency broadcasts available.
- These fear-mongering messages have been circulating for about 20 years without ever coming true.
- Beau praises a response to such messages that involves asking critical questions.
- By employing the Socratic method and questioning the source of information, doubt can be planted in the believer's mind.
- The goal is to make individuals question the credibility of the sources they trust.
- Beau suggests using the upcoming holidays to respond thoughtfully to relatives spreading false information.
- Encourages throwing a lifeline to those caught up in misinformation silos.
- Proposes prepping individuals for the failure of baseless claims to help them break free from false beliefs.

### Quotes

- "Thanks for the heads up. I have a few follow-up questions given the rather alarming news."
- "Asking these questions and then waiting for it to inevitably not happen is a way that you can hopefully lead that person to begin to question those people who they've fallen under the spell of."

### Oneliner

Beau explains how questioning baseless claims can help break the spell of misinformation, urging critical thinking over fear-mongering.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Challenge baseless claims by asking critical questions (implied).

### Whats missing in summary

The full transcript provides a detailed guide on utilizing the Socratic method to combat misinformation and encourage critical thinking. 

### Tags

#CriticalThinking #Misinformation #SocraticMethod #QuestioningBeliefs #HolidayInteractions


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about somebody
getting a message, an email, about a baseless claim
and how they responded to it.
Because it seems like a technique that over time
would produce results.
Like if you started it when you met with your uncle
on Halloween, maybe by Christmas you
start to get somewhere with it.
So we're just going to kind of go through the message they
receive and how they responded to it.
OK, so it's basically just a heads up
that people in 11 to 12 cities in the US
may try to start a civil war.
In the next few weeks, we MAGA Republicans
have been advised to prep, pray, and stay out of the way
as the military put down the insurrection.
There may be up to 10 days of information blackout
with only the emergency broadcast
system available for info.
And it goes on.
It's the normal constant fear-mongering,
paranoia-keep-people-on-the-edge type of message.
You've seen them before.
We can go all the way back to the military exercise
in the southwestern United States.
You could even trace this type of thing
all the way back to UN tanks in Canada and stuff like that.
These types of alerts have been going out for like 20 years.
And they're never right.
So most people understand this.
But how do you get through to somebody who believes it?
This response I thought was really good.
Thanks for the heads up.
I have a few follow-up questions given the rather alarming news.
Who are the people starting the civil war and why?
Who is their leader?
What is their motive?
How do you know this?
Why isn't this news on any trusted MAGA news channel
like Fox or OANN or any other news outlet?
If this doesn't end up happening,
will it impact your trust in whatever source
provided you this information?
That's wonderful.
You're asking questions using the Socratic method,
just kind of bringing them to the information.
How do they know this is going to occur if they can't answer
any of these other questions?
It's just generic fear-mongering to keep the base on edge,
to keep them uncertain, to keep them in doubt,
to keep them in fear, to keep them tuned in,
to keep them under the thumb of the people providing
the information because you don't want to miss out
because eventually there'll be an information blackout.
Asking these questions and then waiting for it
to inevitably not happen is a way
that you can hopefully lead that person
to begin to question those people
who they've fallen under the spell of.
I think it's a good technique,
and I think on a long enough timeline it would work.
So we're moving into the holidays.
You're going to be around relatives,
I'm sure, that are going to say things
that you're going to want to have a response to.
Just make sure the response is helpful.
Try to throw them a rope down into that information
silo so they can get out because there are still
a whole lot of people caught up believing things
that just aren't true.
And it doesn't matter how many times it's proven false.
They've had these prophecies over and over again,
and they never occur.
But if beforehand you put the idea
to ask those questions out there,
and what happens if it doesn't occur,
and you prime them for the failure
that you know is going to happen,
it might be one of those things, one of those tools
that helps them break free.
And maybe you can get your relative back.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}