---
title: Let's talk about Georgia and the Herschel Walker situation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MWeHkyK8FJY) |
| Published | 2022/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the Senate race in Georgia and Republican nominee Herschel Walker's controversy.
- Walker's stance against certain family planning methods without exceptions.
- Allegations surface about Walker paying for family planning he opposes.
- Republican Party's response is the focus, revealing a significant shift from stated beliefs.
- Dana's comments show prioritizing winning and control over principles.
- Reveals a willingness to overlook alleged behavior for political power.
- Republican Party's actions contradict their supposed values of freedom and individual liberty.
- Choosing political control over supporting neighbors' autonomy.
- Beau questions the true motives behind Republican Party's actions.
- Emphasizes how people are being manipulated for political gain.
- Criticizes the hypocrisy and lack of genuine concern for freedom and liberty.
- Condemns the prioritization of political power over individual rights.
- Calls out the Republican Party for valuing control over democracy and representation.
- Points out the irony in the Party's actions versus their rhetoric.
- Ultimately, Beau stresses the importance of recognizing the manipulation for power in politics.

### Quotes

- "You don't care about freedom. You don't care about individual liberty."
- "You don't care about the Republic, you don't care about representative democracy, you don't want to be represented, you want to be ruled."
- "They have tricked you, they have duped you. They have control of you."
- "They've got you."
- "They don't care about freedom. They don't care about individual liberty."

### Oneliner

Exploring the hypocrisy in the Republican Party's actions, prioritizing political control over values like freedom and individual liberty.

### Audience

Voters

### On-the-ground actions from transcript

- Challenge political representatives on their actions (implied).
- Support policies that prioritize individual freedom and liberty (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's priorities over values in the context of the Georgia Senate race. Viewing the full transcript offers a deeper understanding of political manipulation for power.

### Tags

#GeorgiaSenateRace #RepublicanParty #Hypocrisy #Freedom #PoliticalManipulation


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the Senate race
in Georgia and the Republican nominee there,
Herschel Walker, and the situation
that has developed there.
And we're going to kind of go into it.
I can assure you, this video is not going where you think.
I want to talk about the Republican Party's response
to it.
That's what's really important here because it showcases something that I don't think
most Republicans actually know.
If you missed it, Walker has positioned himself as somebody who is very against certain types
of family planning.
No exceptions, none.
Now allegations have surfaced that in the past he has paid for that exact kind of family
planning. Now, I do want to point out I haven't looked into the allegations, like
at all, because I don't care. I don't care about the inherent hypocrisy that it
would exist because the Republican Party base doesn't care about it either. What I
care about is the Republican response to it. In particular, there were comments
from Dana somebody, I'll put a link down below, and she said that she didn't care.
That she didn't care. That winning was a virtue and that what she really wanted
was control of the Senate. I mean that's very refreshing honesty. It's a mask-off
moment, just one of those things, hey this is how it is. Good for you. Good for you.
Now let's talk about what that means. It showcases how far the Republican Party
has fallen from its stated beliefs and I am not talking about family planning. I'm
talking about the general tenet that the Republican Party tries to rally its
constituents behind, the idea of freedom and individual liberty and all of that
stuff and small government. The response to this shows that that's not true. You
don't want freedom, you want to be ruled. If you hear about this situation and
you're willing to overlook it because it gives you control of the Senate, Walker
has done this thing that is just theoretically unforgivable according to
these allegations. But you'll overlook it to get control of the Senate because
what you care about is being a good government stooge. If you're willing to
overlook it to get control of the Senate, to get control of the government, but
you're not willing to overlook it for your neighbors so they can have control
of their body, you don't care about freedom.
You don't care about individual liberty.
They have played you, they have tricked you, they have duped you.
They have control of you.
But that's what you want because you're willing to overlook it.
You don't care about the Republic, you don't care about representative democracy, you don't
want to be represented, you want to be ruled.
That's what this shows.
Individual liberty is something that you're willing to cast aside so your team can have
control of the government.
That seems kind of counter to all of the rhetoric, doesn't it?
And that's what this shows.
It shows a willingness to overlook a behavior that would be just condemned universally as
long as you get control of the government, but you won't overlook that
same behavior for your neighbor, for your daughter, for your friend, so they can
have control of their body. You do not care about freedom. They've got you. That
to me is what matters here. I know there's a lot of people, especially
liberals who want to draw attention to the hypocrisy of it. I personally can't
condemn Walker for something that I don't think is wrong. You want to talk
about the hypocrisy of it that's fine but it's very evident the Republican
Party doesn't care about the hypocrisy. They care about doing what the
government tells them to do, obey, and they want to make sure that their team
can control the government so they can tell other people what to do because
they don't care about freedom. They don't care about individual liberty. That's the
take away from this.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}