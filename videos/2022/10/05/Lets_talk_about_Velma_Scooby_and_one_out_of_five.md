---
title: Let's talk about Velma, Scooby, and one out of five....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IzyOlV2mbyA) |
| Published | 2022/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the reaction to Velma's LGBTQ+ character development in Scooby-Doo.
- Responding to questions about why orientation needs to be included in children's shows.
- Explaining his perspective on the importance of representation and diversity.
- Pointing out the increasing acceptance of LGBTQ+ individuals, especially among Gen Z.
- Describing Scooby-Doo's counterculture aspect and empowerment of youth.
- Mentioning the atheistic tendencies and anti-capitalist messages within the show.
- Speculating on the future inclusion of LGBTQ+ characters in media.
- Expressing curiosity about Elsa's girlfriend in Frozen.
- Predicting a trend towards more representation of diverse orientations in media.

### Quotes

- "It's just representation. You got it."
- "You can change the world. You can solve the mystery. You can make things better."
- "It's kind of always been pretty woke in those terms."
- "This is the future. This is how things are progressing."
- "Y'all have a good day."

### Oneliner

Beau addresses the LGBTQ+ character development in Scooby-Doo, explains the importance of representation, and predicts a trend towards more diversity in media for the future.

### Audience

Media consumers

### On-the-ground actions from transcript

- Support and celebrate diverse representations in media (implied)
- Advocate for more LGBTQ+ characters in children's shows (implied)

### What's missing in summary

The full transcript provides a detailed analysis of the evolving representation of LGBTQ+ characters in media and the cultural significance of such changes.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Scooby-Doo and Velma
and the reaction to the latest development in Velma's
character, which a whole lot of people kind of already knew,
varying takes on it.
But it's now official.
So we're going to talk a little bit about that,
because I got possibly one of the more entertaining and fun
messages from somebody asking a question like this
that I've ever gotten.
I saw your little Velma thing in the background
and said, that's bait.
But here I am taking it.
Is this really necessary?
Why do they have to change characters like this?
I'm not even opposed to the idea of gay characters on TV.
But why does it need to be injected into children's shows?
Why does orientation need to be injected at all?
I don't hate gay people and have even
come around on the idea of representation to some degree.
It just seems over the line, because it
seems like they're specifically targeting
young people with this stuff.
Any idea why?
I've also seen enough of your videos
to know if I ask, when did Scooby-Doo become woke,
you'll go on a rant about the subtext of the show.
Even if I don't agree with your rants,
I always find them interesting.
So when did Scooby-Doo become woke?
Also, here's something in all caps.
So you can say, written in all caps, written in all caps,
I like continuity in everything I watch.
OK, so if you missed it, Velma from Scooby-Doo
is officially a member of the LGBTQ community.
I think a whole lot of people already
knew that or assumed that.
One of the other questions I got about this was,
does it not bother you at all?
It does on one level, but totally not the way
I think it's bothering most people
who were concerned about it.
I always had her as my, and I thought
it was good to have a character of that orientation that
wasn't hyperactive, because that's how they typically
get portrayed.
So I mean, that's my big complaint right there.
OK, so to the message.
Why does orientation need to be injected at all?
Because it's part of the human condition,
would be a standard answer.
But more importantly, I would ask if you had
any issue with Daphne and Fred.
I'm going to guess you didn't.
It's not the injection of orientation that bothers you.
It's this specific type.
Let's see.
And then it seems like they're specifically
targeting young people with this stuff.
Any idea why?
I have a theory.
A recent poll showed that among Gen Z adults,
so this is the part of Generation Z
that has already become an adult,
that 20.8% of them identify as LGBTQ.
OK, so 20%.
Another way to say that would be one out of five.
And still another way to say that would be Velma out
of Velma, Daphne, Fred, Shaggy, and Scooby.
It's just representation.
It's just representation.
You got it.
OK, so when did Scooby Doo become woke?
All right, so like it always had a counterculture aspect to it,
man, that's my version of Shaggy.
That was a pretty integral part of it from the beginning.
It was also very much about the empowerment of youth.
I would have got away with it if it weren't for those meddling
kids type of thing.
One of the messages was definitely you.
You can change the world.
You can solve the mystery.
You can make things better.
It's part of the show.
The catch line is that empowerment of youth.
It isn't really a surprise that the franchise would
continue to try to do that.
I would also point out there are a lot of people who see it
as having very atheistic tendencies,
because anything that was supernatural in the show,
well, it got demonstrated to be natural.
It took away that element.
It certainly seems like there are strong anti-capitalist
messages in the show itself as well,
because when you think about it, at the end of every episode,
the villain, you pull the mask off,
and it's some rich old man preying
on people's ignorance or superstition or fear
to in some way facilitate his own economic gain or quest
for power.
Yeah, it's kind of always been pretty woke in those terms.
That's a very ill-defined term, but generally speaking,
it means holding any kind of progressive idea now.
So that's what's going on.
That's why you're going to see conservatives
crying about Velma.
And that's just a general overview
of why it probably happened.
You're going to see more and more of it.
You will see more characters adopting this.
I'm still a little irritated that we don't know
who Elsa's girlfriend is, because let me tell you something,
that is also really telegraphed in that.
So it's just one of those things.
It's this is the future.
This is how things are progressing.
It's what's occurring, because as people have become
more accepting of various orientations,
well, more people feel comfortable
coming out.
And as that percentage grew,
and now it looks like it's leveling off around 20%,
well, then they'll probably have one out of five characters
that are representative of that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}