---
title: Let's talk about Trump packing the boxes himself....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JurReaV2w9E) |
| Published | 2022/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump reportedly packed boxes of documents himself that were sent back to the National Archives.
- Commentators are making a big deal because Trump deciding what to pack may be significant for possible criminal charges he might face.
- The feds have to prove that Trump possessed the documents willfully for it to be a crime.
- If Trump packed the boxes himself, it could show that he willfully left out certain information, which is a critical aspect for criminal charges.
- The reporting suggests that Trump, with his own hands, determined the contents of the boxes, impacting the criminal aspects significantly.
- Trump's lawyer stated that all documents were returned, but another lawyer expressed uncertainty about this claim.
- If the Department of Justice wants to build a criminal case and can confirm the reporting's accuracy, it could be a significant development.
- The sources in the Washington Post report are unnamed, which means it can't be treated as 100% fact.
- Providing information to a reporter anonymously differs from saying it under oath in court.
- The reporting helps establish the federal government's case if true and could be a critical piece if charges are filed against Trump.

### Quotes

- "When it comes to possible criminal charges that former President Trump might face, that's a big component."
- "He himself with his own hands packed the boxes."
- "It helps establish the federal government's case if true."

### Oneliner

Former President Trump's personal involvement in packing documents could have significant implications for possible criminal charges, as it may demonstrate willful actions critical to the case.

### Audience

Journalists, Legal Analysts

### On-the-ground actions from transcript

- Investigate further into the reported actions of former President Trump (implied).
- Stay updated on developments regarding the Department of Justice's actions (implied).

### Whats missing in summary

Full details and context behind the implications of Trump's involvement in packing documents.

### Tags

#Trump #NationalArchives #CriminalCharges #WashingtonPost #FederalGovernment


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Washington Post reporting that suggests
former President Trump packed with his own hands, the boxes of documents that
were shipped back to the National Archives.
We're going to kind of go over what this means, and the main reason is that
There's a whole bunch of questions have come in,
and they all kind of boil down to,
why are commentators making such a big deal of this?
We already knew that these boxes went back,
and we knew that after that, he had a lawyer
say that all the documents had been returned.
So the reason that commentators are making such a big deal of it
is the reporting is saying that he, with his own hands, packed those boxes, meaning that
he determined what went into them.
That also means he determined what didn't go into them.
When it comes to possible criminal charges that former President Trump might face, that's
big component. The feds have to prove a number of things. They have to show
stuff like it was national defense information, which that's not really in
dispute. They have to show that Trump possessed them, which his lawyers in a
recent filing actually used that term, and they have to show that he did it
willfully. If you're working in an office and somebody walks by and throws a file
that's classified that has national defense information in it into your
briefcase and you don't know it's there and you go home and you decide you don't
like that briefcase anymore and you stick it in your attic, that's not a crime.
Now they might still arrest you for it because they might not believe you but
that's not a crime because you didn't willfully do it. If he packed the boxes,
that means the stuff that was left out, he decided to leave it out and that
helps demonstrate the willful aspect of it. That's why commentators are making
such a big deal of it. That's why you're probably gonna hear those
phrases he himself with his own hands packed to the boxes over and over again because when
it comes to the criminal aspects of it, that's a huge piece.
And then after that, after deciding not to return everything, he had a lawyer say that
it was all returned.
That's the reporting.
In fact, I think it may be in the same article or it's similar reporting that says there
was one lawyer that was like, yeah, I'm not saying that.
I'm not going to say it was all returned because I don't know that it was.
So it just helps build the federal government's case if they do decide to build a criminal
case.
So if you assume this reporting is accurate, and the Department of Justice actually wants
to build a criminal case, and that they can get the Washington Post's sources to say
the same thing in court, that's a huge deal.
It is worth noting that the sources in the Washington Post are unnamed.
There's a huge difference between telling something to a reporter that is protecting
your identity and saying something under oath in court.
So you can't treat it as if this is 100% fact.
The Washington Post does have a pretty good track record when it comes to scandals involving
presidents though. So that's that's what the the big piece of it is is it helps
establish the federal government's case if true. So I wouldn't expect this little
piece of information to kind of blow over because this is this is a huge
chunk of what the federal government would have to prove if they filed charges and took
him to court.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}