---
title: Let's talk about being Steadfast at Noon in the face of uncertainty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sbvZptREKgc) |
| Published | 2022/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People near military and NATO installations got nervous due to ongoing NATO exercise called Steadfast Noon involving 14 countries and around 60 aircraft, including B-52s.
- The assets seen in movement were related to potential nuclear response capability, but it's all part of the exercise.
- The exercise is to train on providing nuclear capability to non-nuclear NATO members.
- Similar exercises happen annually around this time, but due to heightened geopolitical tensions, people are more on edge this year.
- Russia is aware of these exercises and often runs its own exercises concurrently, mirroring NATO's actions.
- NATO conducts numerous exercises each year, with over 88 in 2020 and more than 100 planned before the pandemic.
- Despite the use of real missiles, they are likely without warheads for practice.
- If people see military activity near NATO installations, like B-52s or vehicles, it's part of the exercise and not a cause for panic.
- The exercises are well-coordinated and routine, designed to prepare for various scenarios.
- The lack of media coverage and understanding can lead to unnecessary concern, but it's all part of a regular training exercise.

### Quotes

- "If you see B-52s move, if you live near a NATO installation and a whole bunch of vehicles roll through town or fly overhead, don't panic."
- "It's called Steadfast Noon, if you want to Google it."

### Oneliner

People near military installations got nervous about a NATO exercise involving B-52s and nuclear response training, but it's all part of routine training.

### Audience

Community members near military installations

### On-the-ground actions from transcript

- Research more about NATO exercises and their purpose (suggested)
- Stay informed about military activities in your area (suggested)

### Whats missing in summary

The full transcript provides a detailed explanation of the ongoing NATO exercise to help alleviate concerns and clarify the purpose behind the military movements.


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about remaining steadfast
when things get a little unnerving.
Last night, a whole bunch of messages came in via Twitter.
Basically, people saw things and it made them a little bit nervous.
All of these people lived near military installations, NATO installations.
Right now, there is a military exercise going on.
It's a NATO exercise.
It's called Steadfast Noon.
It will run until the end of October.
I want to say October 30th.
It includes 14 countries and I want to say something like 60 aircraft.
That number is, let's just say, always in flux.
There may be more than that.
One of the things that really unnerved people was seeing B-52s.
For those that aren't familiar, if we were going to send US pilots on a grand tour of
Russia, B-52s would probably have something to do with it.
These are all of the assets that were in movement, were assets that would be used in the event
of a nuclear response.
It's an exercise.
My understanding is what they are training for right now is how to provide nuclear capability
to non-nuclear NATO members.
Yeah, that probably didn't make anybody, you know, sleep any easier.
But this is an exercise.
They do these pretty much every year and normally around this time.
Odds are last year you saw the exact same activity or maybe the year before because
of the pandemic.
You just didn't really put any thought into it because the geopolitical situation was
different.
So that's what's going on.
It's important to remember these type of exercises happen all the time.
They're very well choreographed.
Russia is aware of it.
In fact, normally when these exercises are occurring, Russia runs one at the same time,
kind of mirroring it and uses NATO's responses to run their own exercise and vice versa.
There are some that are very much annual events using the same name like Cold Response, Atlantic
Resolve.
They happen all the time.
In 2020, NATO ran 88 exercises.
They had more than 100 planned, but there was the whole pandemic thing.
That's on top of more than 170 exercises that were national or multinational involving NATO
members.
It's a constant thing.
It's just right now people's nerves are a little bit elevated.
So you're noticing it more.
And I'm sure that there are people on social media who are going to play into this for
the sake of getting scare clicks.
There's not much to really worry about.
My guess would be they will be using real missiles, but they won't have warheads.
I would hope that's how they practice this.
So if you see B-52s move, if you live near a NATO installation and a whole bunch of vehicles
roll through town or fly overhead, don't panic.
That's part of this exercise that's going on.
And again, you've probably seen this exact same behavior before.
You just didn't really notice it because it's always occurring in the background.
And you would normally just kind of be like, oh, wow, and move on about your day.
But because of all of the talk about nuclear weapons, it's kind of catching people in a
situation where they're paying attention to it and it's causing concern.
So that's what's going on.
It's called Steadfast Noon, if you want to Google it.
NATO did the right thing.
They put out a press release and everything.
It just doesn't appear to have gotten the coverage that it should have in order to alleviate
people's concerns.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}