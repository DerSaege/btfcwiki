---
title: Let's talk about Trump's nonsense with the special master....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uYRsAuudSqU) |
| Published | 2022/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Judge Derry is not pleased with Team Trump's objections and assertions in the criminal investigation of former President Trump.
- Team Trump is arguing that certain documents are both personal records and covered by executive privilege, which Judge Derry finds incongruous.
- The judge does not seem inclined to accept Team Trump's arguments regarding executive privilege.
- Team Trump has until November 12th to present their arguments, but based on the current situation, they are not in a favorable position.
- The special master process is unlikely to be completed before the midterms, as expected, due to appeals and delays.
- Documents containing national defense information have already been dealt with; the remaining thousands may not be significant in a potential criminal case.
- There may not be many developments in the document case before the midterms.

### Quotes

- "Judge Derry is not happy."
- "Team Trump is not in a good position."
- "This is kind of a formality at this point anyway."

### Oneliner

Judge Derry is unsatisfied with Team Trump's assertions in the criminal investigation, putting them in an unfavorable position with a looming deadline.

### Audience

Legal analysts, political watchdogs

### On-the-ground actions from transcript

- Stay informed on the latest developments in the criminal investigation of former President Trump (suggested).
- Monitor updates on the special master process and any legal implications (suggested).

### Whats missing in summary

Insights into potential implications of the ongoing criminal investigation.

### Tags

#Legal #SpecialMaster #CriminalInvestigation #TeamTrump #ExecutivePrivilege


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the latest developments
with the special master
and the criminal investigation of former President Trump.
What's occurring there.
Short version, Judge Derry is not happy.
Certainly appears to be losing patience.
He said that he did not want to deal with nonsense objections
and nonsense assertions.
Nonsense being his word.
That particular phrasing was probably aimed at Team Trump
because they are somehow attempting to argue
that certain documents are both simultaneously personal records
and covered by executive privilege,
which would tend to indicate that they're government documents.
There's a, I believe the term that Derry used was incongruity there.
Generally his tone seems to be that the Trump team's assertions thus far,
they don't really seem to measure up to the standard for executive privilege.
But that's a general tone not dealing with any specific document.
It is worth noting and reminding everybody before the,
before the MAGA faithful start going after Derry and slandering him
and making stuff up about him,
Trump's team offered him up as the option.
They specifically said, hey, let's use this judge.
And DOJ was like, yeah, because he's a former FISA court judge.
I think when we talked about it back then, I was like, yeah,
he's not going to be happy with this choice.
So thus far it is not going well for Team Trump as far as their objections
and their statements about the documents and not wanting DOJ to have access to them.
The judge does not seem super inclined to accept the arguments of Team Trump.
Now, they have until November 12th to kind of make most of their arguments,
which gives them a little bit more time to refine their positions.
But based on what we heard today, Team Trump is not in a good position.
That date also lets us know that this won't be done before the midterms.
I kind of expected the special master process to be completed by then.
I don't know if I ever said that,
but I definitely thought it would be.
Doesn't look like it's going to be because of the appeals and the delays.
The judge down there did extend the deadline to like December 16th or something like that anyway.
I don't know that Derry is going to be inclined to use all of that time.
There was a definite tone to the process lately, and it's not a favorable tone for Team Trump.
It's worth noting that the documents that are most likely to contain national defense information,
those are already done with.
This is the thousands of other documents at play here,
most of which, when it comes to the possibility of an actual criminal case,
they're probably not going to matter much.
So this is kind of a formality at this point anyway.
But that's where that stands.
I doubt we're going to hear a lot more or there's going to be a lot of developments in the document case
prior to the midterms.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}