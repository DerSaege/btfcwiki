---
title: Let's talk about how the US military is a birthday cake in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9G39T4z0eiI) |
| Published | 2022/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Compares the US military to a birthday cake to explain its capabilities.
- Explains that Ukraine, using US weapons and tactics against Russia, lacks the full "birthday cake" capabilities.
- Illustrates the importance of the US military's top layer (aircraft carriers, air power) in conflicts.
- Points out that the US military's dominance lies in its air power, owning the sky and the night.
- States that the US doesn't need to go nuclear in conflicts due to its conventional combat capabilities.
- Emphasizes that the critical piece of information missing when comparing Ukraine to the US is the US air power.
- Notes that the US excels in wars against nation-states but struggles with peace and occupation.
- Concludes by suggesting that measuring Russia versus NATO using Ukraine as a metric is flawed due to missing critical information.

### Quotes

- "Picture the US military as a birthday cake."
- "The United States military owns two things, the sky and the night."
- "The US doesn't lose wars, it loses the peace."
- "Taking out another country's military, it's as easy as cake."
- "If you wanted to use Ukraine versus Russia as a metric for NATO or the US versus Russia, you have to acknowledge that disparity."

### Oneliner

Beau compares the US military to a birthday cake, explaining its capabilities and why comparing it to Ukraine's situation against Russia misses critical information.

### Audience

Military analysts

### On-the-ground actions from transcript

- Contact military experts to understand the nuances of military capabilities (suggested)
- Join defense strategy forums or organizations to learn more about military tactics and capabilities (suggested)

### Whats missing in summary

In-depth analysis of military strategies and capabilities beyond surface-level comparisons.

### Tags

#USmilitary #MilitaryCapabilities #Comparison #DefenseStrategy #AirPower


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about how the US military is
like a birthday cake.
I'm going to try a new analogy today,
because I've realized that the way I normally
talk about something isn't putting all of the pieces
there for people when they apply logic to it.
So we're going to try to get some other pieces out there
to better understand US capabilities.
OK, so here's this message.
In one of your videos, you said the US military doctrine
was to be able to fight the next two biggest
countries at the same time.
You seem to believe they could do this.
Ukraine is using US weapons and tactics against Russia
and isn't really crushing them.
Do you still think the US is capable of doing this?
I just don't see how.
I like the logic.
What's happening is somebody is taking
what's occurring there with Ukraine and the weapons
that they're using and trying to use that as a gauge
to determine how NATO or the US would do against Russia going
toe to toe.
That's good logic.
It makes sense.
However, you're missing a couple of critical pieces
of information.
Picture the US military as a birthday cake,
a finished, complete birthday cake.
Ukraine is running this war with the icing in the bowl,
the icing in the spoon, a little bit of leftover cake mix,
and a couple of pieces of last year's birthday cake.
That's what's happening.
Aside from that, what makes a birthday cake a birthday cake?
What makes it different than just a normal cake?
Candles and the message up top.
So the stuff on top and candles.
That's what makes it a birthday cake.
Right?
They don't have any of that.
They don't have the top layer to the US warfighting birthday
cake.
They don't have any of that.
When I have talked about this in the past,
if you have ever heard me talk about the US doctrine being
able to fight our next two largest
competitors at the same time, you've
heard me say something else.
The problem is when I say it, it sounds so absurd.
People think it's a joke, and it's not.
So we're going to try to do this a little bit of a different way
to get the point across.
If tensions flare, Taiwan, what does the US send?
If there's pirates off the coast of Somalia,
what does the US send?
If there's issues in the Middle East, what does the US send?
So much so that when a war occurs in the Middle East,
it might be called the Gulf War.
We send aircraft carriers, air power, the stuff up top.
Ukraine doesn't have any of that.
Every time I talk about this, I say the largest air force
in the world is the US Air Force.
The second largest air force in the world is the US Navy.
The United States military owns two things,
the sky and the night.
And I mean owns.
Like they're not making payments on it.
They have the note.
They own the sky and the night.
Nobody can touch us.
That's not American exceptionalism.
It is statement of fact.
And you'll hear people try to argue this and say, well,
you know, wannabe superpower X has developed this plane.
Right.
And they have four of them.
It means nothing.
If you wanted to use Ukraine versus Russia
as a metric for NATO or the US versus Russia,
you would have to acknowledge that disparity
by admitting that Ukraine is fighting
with its dominant hand tied behind its back.
Because US military is the dominant hand
with its dominant hand tied behind its back because US air
power is the thing that ties everything else together.
The US, perfect example, when people
were talking about Russia possibly
using tactical devices, there were a whole lot of people
who were like, the US probably wouldn't go nuclear over that.
And it's not out of fear.
It's because the US doesn't have to achieve
that level of combat degradation on the other side.
They don't have to use a nuke.
They can do it conventionally.
What's even more embarrassing is that with most countries,
they can say, we're going to hit you at noon on Tuesday.
And then at 12.01, they start.
Not just can they do it, they can tell you
when and where they're going to do it.
And there isn't anything that can be done to stop it.
I believe that's what the kids call a flex.
So it's not a good metric for measuring Russia versus NATO.
Because the critical piece that ties the US warfighting
capability together, it's not even in play.
That's the piece of information that matters.
Night operations is a big deal too,
but that's more for low intensity stuff.
When it comes to a nation state as opposition,
nobody can come close to the US.
The United States does not lose wars against nation states.
They don't.
In fact, a lot of the problems occur
because they take out the government so quickly,
they're not prepared to deal with the collapse.
The US doesn't lose wars, it loses the peace.
It loses the occupation after they
have defeated the other military.
And that I could rail for hours about, and why it happens,
and how it happens.
That isn't something the US is good at.
Taking out another country's military, it's as easy as cake.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}