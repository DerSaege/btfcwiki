---
title: Let's talk about Arizona and baseless theories....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TSQeUx2mP0Q) |
| Published | 2022/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the current situation in Arizona regarding groups monitoring voting locations, particularly prevalent in Arizona. 
- Mentions the potentially intimidating tactics used by these groups, such as wearing tactical gear, masks, and photographing license plates. 
- Notes the temporary restraining order sought against one group and the involvement of the Secretary of State in Arizona and the Department of Justice.
- Points out that voter intimidation is a significant issue with potential legal consequences for those involved.
- Explores the motivations behind these actions, attributing them to a belief in baseless conspiracy theories.
- Suggests that attention should also be directed towards those who propagate these theories, as they may bear responsibility for deterring people from voting.
- Draws parallels to past cases where disseminating false information led to legal repercussions.
- Raises the possibility of civil proceedings against those spreading baseless election theories in the future.
- Emphasizes the negative impact of spreading false information and inciting actions that harm others.
- Concludes by encouraging reflection on the implications of misinformation and its real-world consequences.

### Quotes

- "Voter intimidation is actually kind of like a big deal."
- "If charges come from this, it's going to be a big deal for those involved."
- "We're talking about people who are intentionally manipulating people and feeding them false information."
- "This might be another case where we end up years from now seeing a civil proceeding against the people who put out the baseless theories."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Beau addresses voter intimidation in Arizona, warning of legal consequences for spreaders of baseless conspiracy theories and possible civil proceedings in the future.

### Audience

Concerned citizens, activists

### On-the-ground actions from transcript

- Contact local election officials to report any instances of voter intimidation (suggested)
- Support organizations working to combat misinformation and protect election integrity (exemplified)

### Whats missing in summary

A detailed analysis of the legal implications and potential consequences of spreading baseless election theories.

### Tags

#Arizona #VoterIntimidation #ConspiracyTheories #LegalConsequences #ElectionSecurity


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about what's going on out in Arizona
and the longer term impacts from it.
Because right now everybody's focusing on the most immediate
and that makes sense.
But there is probably going to be some developments from this down the road
and it might be a good idea just to remind everybody
how this type of thing has been playing out in recent weeks.
Okay, so if you don't know what's going on out in Arizona,
this is happening in other places, but it's particularly prevalent in Arizona.
There are groups of people who are, let's just say, monitoring places
where voting can occur.
And they're doing it, in some cases, in a manner that could be seen as intimidating.
Sometimes they are decked out in tactical gear, wearing masks,
photographing people's license plates, following them, stuff like this.
Now, there has already been a temporary restraining order sought against one group,
the Secretary of State in Arizona has gotten involved,
there's been a referral to the Department of Justice,
there's a sheriff in Phoenix who is stepping up security around voting locations.
Now, voter intimidation is actually kind of like a big deal.
If charges come from this, it's going to be a big deal for those involved.
And you can look at it and say that,
well, it's just these people out there doing this, but why are they doing it?
If you listen to some of the things that the people engaged in this have said,
they're doing it because they believe a baseless theory.
They were sold on a conspiracy theory, and they believe it,
so they're out there acting upon that.
Right now, everybody is focusing on what's going to happen to the people
who are actively doing it.
There might be a reason to start looking at those people
who put the theory out there,
because I would imagine those who were scared or deterred from voting,
they might look to them, the people who put that theory out there,
the people who promoted it, as being ultimately responsible for it.
And when you're talking about a conspiracy theory that is put out,
perhaps for profit, that then encourages people
to do things in the real world that has a negative impact on others.
If there's something that we have learned from the Jones case,
the court proceedings there can result in millions
against the people who put the theory out there.
The Jones case should have been a wake-up call for a whole lot of people
whose business model revolves around putting out inaccurate information,
and many times they know it's inaccurate when they put it out.
We're not talking about good faith mistakes a lot of times.
We're talking about people who are intentionally manipulating people
and feeding them false information,
along with rhetoric encouraging them to act.
This has proven to be something that juries in civil cases find displeasurable.
They don't like this, and the Jones case proved that.
This might be another case where we end up years from now
seeing a civil proceeding against the people who put out
the baseless theories about the election and about election security.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}