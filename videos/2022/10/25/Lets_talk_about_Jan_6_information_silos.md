---
title: Let's talk about Jan 6 information silos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TQi6oE2Ofvk) |
| Published | 2022/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the aftermath of January 6th and the challenges of discussing it with individuals in closed information ecosystems.
- People on the right-wing often argue that January 6th wasn't an insurrection because there were no guns involved, influenced by misinformation from their sources.
- Despite the presence of significant weapons around the Capitol, some individuals deny the insurrection due to the absence of guns among the rioters.
- A specific case is mentioned where a person dropped a firearm during the Capitol breach, received a five-year sentence, and used a baton on a police officer.
- Beau suggests using such examples in dialogues to counter the false narrative that there were no guns present during January 6th.
- Many individuals in the far-right movement were misled and lied to, rather than genuinely believing in its core beliefs.
- Demonstrating objectively how they were misled might open the door for productive discourse and reflection.
- Beau encourages addressing the misinformation spread, particularly during election season, by simplifying the message: "They lied to you."
- Acknowledges that not everyone will be receptive to this information but believes it's still worth the effort to reach those who were misled.
- Emphasizes the importance of confronting individuals who manipulate reality and deceive their audience for personal gain.

### Quotes

- "They lied to you. What they told you wasn't true. Here is the evidence."
- "We're fighting against people who just don't care about objective reality."
- "You don't win every fight like this. But it'll work on a few."

### Oneliner

Beau addresses the misinformation surrounding January 6th, urging individuals to confront deception and manipulation with evidence, even if not everyone will be swayed.

### Audience

Activists, Educators, Allies

### On-the-ground actions from transcript

- Challenge misinformation in your community by using concrete examples like the Capitol breach incident (suggested).
- Engage in outreach efforts to confront deception and manipulation, especially during critical times like election season (suggested).
- Encourage critical thinking and evidence-based discourse when addressing misled individuals (suggested).

### Whats missing in summary

The full transcript provides a deeper insight into how misinformation can influence beliefs and actions, urging individuals to confront deception with evidence for productive discourse and reflection.

### Tags

#Misinformation #January6th #Deception #CommunityEngagement #CriticalThinking


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk a little bit about January 6th
and some of the comments that you're
likely to receive when you're talking to somebody who has
fallen down an information silo, when they're
in a closed information ecosystem,
and they're just getting their news from certain sources.
When you try to talk to people who are on the right wing
about this topic and what happened that day,
one of the things you inevitably run into
is them saying, well, it wasn't really an insurrection
because they didn't have any guns.
And they believe this because their talking heads
have told them this over and over and over again.
It's a common refrain.
Now, you can point to the massive amounts of weapons
that were stationed around the Capitol,
but that doesn't do it for them.
I mean, to me, that shows a propensity for violence.
But that doesn't normally drive people to do anything.
That doesn't normally drive it home for them.
Because you'll say, see, they were peaceful.
They didn't even bring them.
And they actually used that to reinforce the idea
that there weren't any guns there.
But that's not true.
I would also like to point out that that's not actually
a part of it.
You don't have to have firearms to do this.
But there were firearms there, multiple.
In fact, one person, there were so many there
that one person dropped one and left it.
They lost their gun on Capitol grounds, one of their guns.
They had another one.
This person was just sentenced.
They got 60 months, five years.
According to prosecutors, they had two firearms.
They dropped one.
They wound up taking a baton from a cop
and using it on the cop.
So, I mean, they drew a lengthy sentence, five years.
And because of the assault on the cop
and because of the firearm,
they're going to go to a higher security level than most people.
But this is just one of these cases.
It might be a good idea to have one of these in your back pocket
for these conversations.
So when people can say, they sit there
and they say with a straight face,
there weren't any guns there.
Yes, there were.
Here's one.
He admitted it.
Pled guilty.
60 months.
Right here.
And it opens the door.
And you can ask, if they lied to you about this,
what else do you think they lied to you about?
There is a large population of the people
who are aligned with this movement, with the far right,
that didn't get there because they believe the core tenets.
They got there because they were lied to,
because they were misled,
because they were told things that weren't true.
And showing them that might open the door.
Is it going to work for everybody?
No, because some of them actually do believe
those core tenets.
Or some of them got to that place
through an irrational emotional response.
Those people, it's going to be much harder to reach.
But those who were just misled,
when you can show them objectively
that they were misled, it opens that door.
And when that door gets opened,
that's the point to bring up.
You know, the January 6th committee also showed,
they demonstrated pretty clearly that Trump knew he lost,
but he still told everybody he didn't.
All of this happened because people were misled,
just like you were misled.
And it gives you that opening.
I know right now, election season,
there's probably not a whole lot of people doing outreach,
but this might be an important time to do this
and just break it down for them.
Put it into as simple terms as you can.
They lied to you.
What they told you wasn't true.
Here is the evidence.
And again, for some of them, it's not going to matter.
You don't win every fight like this.
But it'll work on a few.
Means it's worth doing.
It's worth the attempt.
In this case, it's easy to find
because of the fact that he lost one.
You can put that in.
60 months, lost gun, capital, and it'll show up.
You know, we're fighting against people
who just don't care about objective reality,
and they will tell their audience anything
as long as they think it will sway them
and manipulate them into doing whatever it is they want.
Demonstrating that to people, it's hard
because people don't want to admit that they were tricked.
But when you have something that they believe like that,
something they hold so closely, there were no guns there,
and you can show that there were, it might help.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}