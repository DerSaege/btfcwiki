---
title: Let's talk about a cancer vaccine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SUn3-arvEWI) |
| Published | 2022/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A pair of individuals who pioneered the COVID-19 vaccine have announced a bold statement that by 2030, a cancer vaccine will be ready for widespread use.
- The mRNA vaccine technology used for COVID-19 was actually initially researched for cancer vaccines.
- The goal is to train the body's immune system to attack cancer cells, a process different from the current status of cancer treatment.
- The research for cancer vaccines benefited from insights gained during the COVID-19 response.
- Progress is already being made in cancer vaccine trials, with the expectation that it can be tailored to different types of cancer and prevent recurrence.
- The announcement brings hope and something positive to look forward to amidst challenging times.
- Despite the promising news, there may be initial resistance fueled by fear-mongering and demonization of the science.
- The research has been ongoing for a long time, and the end game is now starting to become visible.

### Quotes

- "By 2030, there will be ready for widespread use a vaccine for cancer."
- "The real reason they were looking into the mRNA vaccines was for cancer."
- "They're saying that they expect a cure or something that can change the way cancer patients live their lives."
- "Given who is behind it, there's probably going to be resistance to it at first."
- "The research has been going on a long time, and they're just now starting to see the end game."

### Oneliner

A cancer vaccine by 2030, utilizing mRNA technology originally intended for cancer, offers hope amidst potential resistance and ongoing research progress.

### Audience

Healthcare professionals, researchers, cancer patients.

### On-the-ground actions from transcript

- Support cancer vaccine trials by participating (implied)
- Stay informed about advancements in cancer vaccine research (implied)
- Advocate for scientific progress and combat misinformation (implied)

### Whats missing in summary

Detailed explanations of the mRNA technology and its application in cancer vaccines.

### Tags

#CancerVaccine #mRNA #Healthcare #Research #Hope


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about some news
out of the world of health,
because a very bold statement was made,
and it has raised a lot of eyebrows, expectations,
and hope for a whole lot of people.
It's a bold statement, but it's made by a pair of people
who are kind of entitled to make some bold statements,
I guess.
The statement is that by 2030, there
will be ready for widespread use a vaccine for cancer.
The statement is coming from the people who
pioneered the COVID-19 vaccine.
The general idea is to use the same technology.
What a lot of people don't know is that the real purpose and the real reason that they were looking into the mRNA
vaccines was for cancer.  And they were just able to repurpose what they had learned and use it for a COVID vaccine.
vaccine. They're saying that they expect a cure or something that at the very
least can change the way cancer patients live their lives. It's not simple, you
know, it's very individualized, but it's a pretty bold statement. And it's there's
lot to back it up. The general idea is to use the same format and basically train
the body's immune system to actually attack the cancer cells, which that's not
happening right now. And the unique part about it is they did all of this
research for this purpose and then it benefited the COVID-19 response and then
what they learned there provided more insight into how to pioneer the the
cancer vaccines. There's been a little headway in this already. There's some
stuff that is moving forward and going through trials, but in this particular statement,
they're making it seem as though this is something that they can tailor to hit various
kinds and not just be a one-off.
also talking about the likelihood that it it would stop it from coming back, you
know. So it's still early in this, but the expectation has now been set that
they'll have this in like seven years. And you know with everything going on we
could probably use some good news, something to look forward to, and there
there you are. Given who is behind it, there's probably going to be resistance to it at first,
because no doubt there will be people who play on people's fears and try to demonize
the science. But it's starting now. The research has been going on a long time, and they're
just now starting to see the end game.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}