---
title: Let's talk about Lavrov and Russia severing ties.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=i050mt-7OAc) |
| Published | 2022/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Sergei Lavrov, the foreign minister of Russia, hinted at cutting ties with the West during a speech to new diplomats inducted into the Russian foreign service.
- Lavrov suggested that Russia is ready to sever ties with Western states due to Europe's decision to cut off economic cooperation.
- Beau questions the feasibility of Russia completely cutting ties with the West, especially during a time of war where intelligence is critical.
- Lavrov's threat to sever ties is seen as a tactic to scare the West into taking certain actions, but it is likely not a genuine intention.
- Russia's diplomatic efforts to establish peace and buy time for military maneuvers are not yielding the desired results.
- Putin may be pressuring Lavrov, leading to provocative statements and actions from the Russian foreign minister.
- Despite heightened tensions, direct lines of communication between nations usually exist beyond ambassadors and diplomats.
- Beau views Russia's threats as a bluff and a sign of frustration rather than a serious diplomatic strategy.

### Quotes

- "Russia is having a hard time finding its off-ramp."
- "It is a child stomping their feet, trying to get their way."
- "They're not gonna sever ties with the West. That's not even a thing."

### Oneliner

Sergei Lavrov's threats to cut ties with the West are seen as a bluff by Beau, who believes it's more about frustration than actual diplomatic strategy.

### Audience

World leaders

### On-the-ground actions from transcript

- Stay informed about international relations and diplomatic strategies (implied)

### Whats missing in summary

Analysis of the potential long-term consequences of Russia severing ties with the West.

### Tags

#Diplomacy #Russia #WesternRelations #PoliticalAnalysis


## Transcript
Well, howdy there, Internet of People, it's Beau again.
So today we are going to talk about diplomacy and Russia
and cutting ties and toddlers.
The foreign minister of Russia, little Sergei Lavrov,
he was speaking to a group of people
that were being inducted into their foreign service,
their diplomatic corps, I don't know what it's called.
And he kind of indicated that Russia was ready
to just cut ties with the West entirely, just done with it.
And that those new people,
they shouldn't even expect to go there
because the Russian diplomatic corps
has turned into a bunch of toddlers
who are ready to take their diplomats and go home.
He said, there is neither point nor desire
to maintain the previous presence in Western states.
Went on to say, there's no work to do
since Europe decided to shut off from us
and sever economic cooperation.
Okay, little Lavrov, I know it's been a rough week, buddy,
I get it.
So you're gonna take your diplomats and go home.
You're gonna pull your diplomats from all the embassies
and just leave the people with official covers there
so we can spot all your spies easily.
No, you're not gonna do that.
You're gonna take them as well and cut yourself off
from all the intelligence you need in the middle of a war
where you suffered the worst loss since 1943?
I don't think so.
That's not gonna happen.
You're gonna throw a temper tantrum,
you're gonna stomp your feet,
and then you're gonna provide a note of protest,
just like you did back in March.
The purpose of this is to scare people.
Their goal is to say, oh, well, we're gonna sever ties.
And it's supposed to make people in the West think that,
well, they're just done,
and then they might use that tactical thing
that they've been talking about.
That's the point.
They're gonna do a token withdrawal
from a couple of places, and that's it.
They're not actually gonna withdraw their diplomats.
It's not a thing.
They threatened this in March.
They threatened it with a couple of various countries
over Ukraine.
It's all bluster.
It is a toddler.
It is a child stomping their feet,
trying to get their way,
and saying, look what you made me do.
The reality is Russia is having a hard time
finding its off-ramp.
The diplomatic process
that Russia has tried to use is not going well.
Basically, what they're trying to do
is get a peace process going so they can stall,
so they can get more troops in, and nobody's biting.
They're trying to extort and use the leverage they have.
And when I say that, that part of it,
that's just foreign policy.
That's not saying they're doing something wrong.
That's just how it works.
And nobody's really falling for it.
They're not doing well.
My guess is that Putin is putting a lot of pressure
on Lavrov, and he's lashing out and saying things.
This shouldn't worry anybody.
They're not gonna sever ties with the West.
That's not even a thing.
Generally speaking, when tensions raise the way they have,
there's actually more direct lines of communication
than ambassadors and diplomats anyway.
It's a bluff.
It's a bluster.
It's a childlike statement from somebody
who's losing a game and doesn't want to.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}