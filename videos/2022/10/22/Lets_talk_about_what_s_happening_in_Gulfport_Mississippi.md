---
title: Let's talk about what's happening in Gulfport, Mississippi....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1kBuljt71-w) |
| Published | 2022/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the familiar story of a police shooting in Gulfport, Mississippi, involving a 15-year-old.
- Eyewitnesses agree that the kid didn't have anything in his hands except for the police.
- Beau expresses distrust in both eyewitness reports and police reports.
- Emphasizes the importance of video footage in understanding what happened.
- Points out the lack of public footage, including body cam and dash cam footage, in this case.
- Raises questions about why officials are hesitant to release the footage.
- Mentions that the family has hired lawyer Crump and current demonstrations are demanding the footage.
- Suggests that if the department believes they did nothing wrong, they should allow Crump to view the footage to ease tensions.
- Argues that withholding footage when it could confirm the department's actions as justified puts the blame on the department if demonstrations escalate.
- Urges for transparency to avoid assumptions of wrongdoing by the department and to reassure the community.

### Quotes

- "If you hide it, the only thing the public can assume is that you're hiding it for one of the reasons that includes the department doing something wrong."
- "You have the opportunity to diffuse this now. It seems like the smart move."
- "And if it is one of the reasons where the department didn't do what they were supposed to, it's probably better if the public finds out sooner rather than later."

### Oneliner

Beau sheds light on the importance of releasing video footage in a police shooting involving a 15-year-old to prevent community unrest and assumptions of wrongdoing by the department.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Demand the release of video footage from the police department (suggested)
- Support demonstrations calling for transparency and accountability (exemplified)
- Advocate for allowing lawyer Crump to view the footage to ensure transparency (implied)

### Whats missing in summary

The emotional impact and urgency of addressing police transparency and accountability in cases involving minors like Jaheim McMillan.

### Tags

#PoliceShooting #Transparency #CommunityAccountability #ReleaseTheFootage #JusticeForJaheim


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Gulfport, Mississippi
and an incredibly familiar story.
One that we've heard over and over again,
but this time there's a lot of questions.
And the questions exist because
people seem to not want them answered.
So, in this case, you have competing storylines.
What we have is a 15-year-old kid on the ground
after a police shooting.
It seems like all of the eyewitnesses
can agree on one thing.
That kid didn't have anything in his hands.
They seem to all agree on that fact,
as long as they're not a cop.
Cops say he did have something in his hand.
Cops are saying he had a weapon.
If you go back through the dozens of videos
that I have done on topics like this,
you're going to find an incredibly common theme.
They all have footage.
They all have video.
The reason for that is that there's two things I don't trust.
One is eyewitness reports,
because generally speaking, they're pretty unreliable.
The other is police reports,
partially because we write the report,
we know what that means,
and partially because all a police report is
is an eyewitness report being prepared by somebody
who consciously or subconsciously
is trying to justify their actions.
I don't trust either one of these things.
I would trust the footage from a 10-year-old system
from the gas station across the street,
even if it happened in the rain at night.
It's more reliable.
But there's no footage in this case that's public.
That's public.
There should be body cam footage,
dash cam footage, footage from the store,
and that footage would tell us
what happened to Jaheim McMillan.
But for some reason, the officials
don't want to release it.
Now, there are a number of reasons
why a department wouldn't want to release footage.
A lot of them are because the department did something wrong.
There are a few situations
in which the department didn't do anything wrong,
but they still don't want to publicly release the footage,
and that may be at play here.
Here's the thing.
My understanding is that the family
has retained Crump as their lawyer.
You have demonstrations going on right now,
and right now, these demonstrations
are asking for the footage.
That seems like a super reasonable request to me
when you have a 15-year-old laying on the pavement.
Seems like the community should be able to ask for that.
But if this is one of those situations
where the department didn't do anything wrong,
but you don't want to release it,
why not call Crump in, let him see it?
He can tell the mom.
He can tell public.
I mean, he's going to see it.
Crump's not exactly known as a man
who's going to let something like this go.
And if you do that, if the department did that,
they have a chance to diffuse all this.
You have a politically charged environment,
you have demonstrations,
and you have a 15-year-old that was on the pavement.
If the department is in the right
and there's footage confirming it,
which is why body cameras exist,
they're there to provide video evidence
that the officer did the justified thing.
If that video exists
and the department doesn't disclose it
and these demonstrations spin out of control,
it is 100% the fault of the department.
You have the opportunity to diffuse this now.
It seems like the smart move.
You're not going to be able to hide this forever.
A 15-year-old kid,
and you got half a neighborhood standing there
saying they saw it
and he didn't have anything in his hands.
Footage exists.
If you hide it,
the only thing the public can assume
is that you're hiding it for one of the reasons
that includes the department doing something wrong.
It's the only logical thing to assume.
You have an incredibly well-known
and well-respected attorney
that you can call and let him view that footage.
You have the opportunity to put the community at ease.
And if it is one of the reasons
where the department didn't do what they were supposed to,
it's probably better if the public finds out sooner
rather than later.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}