---
title: Let's talk about Vincent van Soup....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5COJyheLHSE) |
| Published | 2022/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Van Gogh soup incident involving climate activists throwing soup on a painting in London.
- Compares the act to another extreme protest where someone lit themselves on fire to bring attention to climate change.
- Argues that the purpose of such demonstrations is to gain publicity, which was achieved.
- Analyzes the cost of the operation, comparing it to the cost of a primetime ad.
- Emphasizes the lack of risk involved in the protest and the absence of property damage or loss of life.
- Suggests that targeting the art world, a realm dominated by the rich, was strategic in drawing attention to climate change.
- Acknowledges that while the act may seem odd, it effectively garnered attention without risking harm to individuals.

### Quotes

- "If it bleeds, it leads, you know?"
- "I don't know what more you want."
- "A little odd, you can critiqu anything."
- "I don't know what more you want from them really."
- "It's just a thought."

### Oneliner

Beau explains the strategic and attention-grabbing nature of the Van Gogh soup incident by climate activists without causing harm or property damage.

### Audience

Climate activists

### On-the-ground actions from transcript

- Organize attention-grabbing demonstrations that prioritize safety and avoid harm (exemplified)
- Target strategic locations or events to draw attention to climate issues (implied)
- Utilize creative tactics to generate buzz and publicity for climate change causes (suggested)

### Whats missing in summary

Context on the potential controversy and debate surrounding the effectiveness and ethics of extreme protests for social causes.

### Tags

#ClimateActivism #AttentionGrabbing #ProtestTactics #StrategicDemonstrations #Publicity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the whole Van Gogh
soup thing that occurred.
Because a whole bunch of questions have come in about it.
And most people seem to understand
what they were trying to achieve.
And most people even seemed incredibly sympathetic.
But they had questions about why they did it that way.
And we're just going to dive into that and talk about it.
And if you don't know what I'm talking about,
some climate change activists, they
walked into the gallery there in London.
And they threw soup, can of soup, on a Van Gogh painting.
The thing is, the why they did it that way,
The perfect take on that already exists.
Conyer, on YouTube, but I saw this on Twitter,
getting asked the same question over and over again from people
and brought up the case of when Bruce, you recognize that name.
Odds are you don't.
You probably don't.
And if you don't, don't feel bad.
Most people don't recognize that name.
But just a few short months ago, went to the Plaza
there in front of the Supreme Court
and in an attempt to bring attention to climate change,
literally lit themselves on fire.
A few short months later, nobody even recognizes the name
if they even remember this happening.
Six months from now, you are definitely
gonna remember the Van Gogh soup thing. That's why. That's why. As a society, we
tend to focus on possessions and high-profile stuff. The purpose of a
demonstration is to get publicity. It worked. That's what they did.
Activists, and this isn't a criticism, I do this, but activists like to use the
language of war. We're in a fight for climate justice. We're at war to save the
planet. What do wars cost? Two things, lives and money. The loss of life,
Well, that's regrettable. Loss of money? That's unavoidable. So let's look at this
through a military lens. What did this cost them? What did this operation cost
the group that did it? A couple tubes of super glue, a couple cans of soup. It bends
to the galleries free, so transportation. They're out like 15 bucks. What do you
think it would cost to buy a 30-second ad, primetime, on every news channel in
the Western world? $250 million? By that metric, this is a pretty big success.
And if you are going to be going toe-to-toe with energy companies that
have billions upon billions of dollars at their disposal, you're gonna want to
beat the market. You're going to want to get a really high return on your
investment, and they did. What about the other one? Loss of life. No risk. A lot of
climate protests, they're dangerous. This isn't. This isn't dangerous. There's no
chance of anybody getting hurt doing this really. No property damage. If you don't
know, all those paintings are encased. They're protected. So no real property
damage, no loss of life, got publicity, nobody got hurt. I don't know what more
you want from them really. I mean, I would prefer that those who would take it to
more extremes, where people get hurt, I would prefer they do something like this.
That's why. Because it worked. It worked. No loss of life, no loss of property, massive, massive
return on investment. And the biggest criticism people have is, well, it seems weird. Why go
after Van Gogh? Conyer actually had this right too. What is the art world? There are a lot of us
commoners engaged in the art world? No, it's a rich man's game, right? That is a
game for rich people. Who is most heavily invested in dirty energy? Rich people. Why
did they do it this way? They walked into a rich people conference, walked up to
the microphone, tapped on and said, hey, we'd like a word. And it worked. Yeah, okay,
It's a little weird, but it worked.
I can't think of anything that had as low a risk of people getting hurt that generated
that much coverage any time recently.
Normally to get that much coverage to generate that kind of buzz, people have to get injured
because that's what it takes in the news industry.
If it bleeds, it leads, you know?
Yeah, it's a little odd.
I mean, you can critique anything.
But when you're looking at it from the lens of we're trying
to draw attention to this cause, I don't know what more
you want.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}