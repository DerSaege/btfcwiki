---
title: Let's talk about what we can learn from Jan 6 defendants....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6UVBPKlxjps) |
| Published | 2022/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a positive lesson from January 6th defendants.
- Encourages people to show up and get involved locally.
- Emphasizes that everyone has something to contribute.
- Mentions the theme of "getting caught up" in the January 6th defendants' statements for leniency.
- Points out that surrounding oneself with motivated people can lead to positive outcomes.
- Compares the motivation gained from positive causes to the negative consequences of getting caught up in destructive actions.
- Suggests that redirecting energy towards positive actions can lead to significant good in the world.

### Quotes

- "Show up, show up."
- "Everybody has something that they can contribute."
- "Surround yourself with people who genuinely want to make the world a better place."
- "You're around people who genuinely want to make the world a better place."
- "Can you imagine the amount of good it would do if that much energy was expended in making things better, in building rather than destroying?"

### Oneliner

Beau encourages showing up locally, surrounding oneself with motivated individuals, and redirecting energy towards positive actions for a better world.

### Audience

Community members ready to get involved.

### On-the-ground actions from transcript

- Attend local community events and meetings to get involved (implied).
- Surround yourself with individuals motivated to make positive change (implied).
- Redirect energy towards building and improving the community (implied).

### Whats missing in summary

The importance of actively seeking out opportunities to contribute and participate in local initiatives for positive change.

### Tags

#CommunityInvolvement #PositiveChange #Motivation #LocalEngagement #Activism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about something positive
we can learn from the January 6th defendants.
I know that doesn't make sense.
Just stick with me.
Because there's a common theme that has occurred,
and I think it's kind of worth examining.
The main reason is because when I go through
a bunch of messages from y'all,
and I go through a bunch of them at once,
at least one out of 10, one out of 15,
is the question, what can I do?
How can I get involved?
Where can I help?
How should I devote my time?
And my answer's pretty much always the same.
Show up, show up.
Whatever's going on in your local area,
show up and start to get involved.
Now, my reason for saying this is,
well, it's the idea that everybody has something
that they can contribute.
And sometimes you have to be there.
You have to be around other people doing it
to see where you're needed.
And the reality is everybody's needed.
Your skill set, no matter what your skill set is,
what you're able to contribute, it is needed.
There's a lot of fights going on out there
to make the world a better place.
And you are undoubtedly critical to one of them.
What you're good at is desperately
needed by one of them.
But there's something else.
There's a theme that I've noticed in the January 6th
defendants that I'd never really considered,
at least not consciously.
In pretty much all of their pleas for leniency,
the statements that they're giving the court at sentencing,
there is a four-word concept that's
pretty much in all of them.
I got caught up.
They surrounded themselves with people
who used that rhetoric, who had those solutions, quote,
who looked at the world that way.
And because they surrounded themselves
with those people, because they were around them,
because they showed up, they became even more motivated.
They got caught up in something that led them to something
less than desirable.
The same thing would happen showing up locally.
You're going to be around people who are motivated.
Sure, they may have a different primary cause than you.
But you're around people who genuinely want
to make the world a better place.
And that's the rhetoric you're exposed to.
Those are the solutions that you see.
And that is what you very well might get caught up in.
Probably lead to a much better place
if more people got caught up doing stuff like that.
When you think about the amount of time and energy that
was devoted to all of the lies, all of the conspiracies,
all of the demonstrations and memes and speeches that
led to the Sixth, can you imagine the amount of good
it would do if that much energy was expended
in making things better, in building rather than destroying?
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}