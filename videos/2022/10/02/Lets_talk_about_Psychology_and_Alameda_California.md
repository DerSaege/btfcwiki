---
title: Let's talk about Psychology and Alameda, California....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YBJgoziUBzU) |
| Published | 2022/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There is a push for psychological testing of officers to determine their suitability for duty.
- An audit in Alameda County Sheriff's Department resulted in 47 deputies having their weapons and arrest powers taken away.
- This amounts to 5% to 10% of the force, showing a significant oversight.
- The audit was prompted by a former deputy allegedly involved in a double shooting.
- Nationwide psychological testing for law enforcement officers is necessary.
- The issue arises with the expectation of retesting within two months, questioning if it's to address issues or beat the test.
- Law enforcement agencies sometimes believe they are above laws and regulations they enforce, indicating a systemic problem.
- Rushing retesting may lead to gaming the system rather than addressing underlying issues.
- There's skepticism about fixing personnel in two months to meet required standards.
- It's clear that nationwide psychological testing is vital to ensure officers' suitability.

### Quotes

- "One out of 20 cops that's out on the street shouldn't be there."
- "That's way more than a few bad apples."

### Oneliner

Psychological testing is vital for law enforcement suitability, but rushing retesting may lead to gaming the system rather than addressing deeper issues, raising concerns about systemic problems and the need for nationwide testing.

### Audience

Law enforcement oversight advocates

### On-the-ground actions from transcript

- Advocate for nationwide psychological testing for law enforcement officers (suggested)
- Raise awareness about the importance of addressing deep-rooted issues in law enforcement (implied)

### Whats missing in summary

The full transcript dives deeper into the potential consequences of rushing retesting and the systemic issues within law enforcement that contribute to these oversights.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about psychology and suitability
in the Alameda County Sheriff's Department out in California
and what those things all have to do with each other.
You know, in the United States today,
when you're talking about law enforcement,
there is a large push to have psychological testing
periodically done of officers and deputies.
The thing it's trying to determine
is whether or not they should be out on the road with a weapon
and the power to deprive someone of their liberty,
if they're suitable for that.
So this Sheriff's Department recently
conducted an audit of psychological profiles
done from 2016 to today.
47 deputies had their weapons and arrest powers
taken away from them.
Now, depending on the reporting, I
guess it's how you count the department's size,
this amounts to 10% to 5% of the force.
That's pretty big.
I mean, it may seem like a small percentage,
but when you're talking about people
that were deemed not suitable to be doing this, who had been out
there doing this, I guess, based on this, maybe back to 2016,
that's a pretty big oversight.
That's a pretty big issue.
They were deemed not suitable.
This audit was prompted because, I guess, a former deputy
and might have also been former Stockton PD
was allegedly involved in a double shooting.
That's what brought this about.
So it seems clear from this that psychological testing
should probably be done.
That's probably something that needs to happen nationwide.
But then we get to the next issue.
What do you think the next steps are?
Reportedly, they are hoping to have them retested
within two months.
Do you think that that two months is going to be used
to address whatever issues exist that make them not suitable?
I mean, because I think most people know that therapy
definitely runs on a timeline.
Or do you think that's just going
to be used to teach them how to beat the test?
The problem is, and this is the systemic part,
is that a lot of law enforcement agencies
don't view laws, rules, regulations
as actually applying to them because they enforce them.
So they're above them.
The idea that there is a timeline that has already
been put out on how to get them retested and get them back on,
that's an issue in and of itself.
Retest them, sure.
But with the expectation of them coming back, that's not good.
That's not good.
It is unlikely that anybody who is a psychologist
is going to say that, oh, yeah, we
can fix this person in two months.
We can address whatever issue this is.
Seems more likely that they're hoping
they can figure out how to game the questions, game the system.
So they won't be held to the standard that's required.
It's the way it appears based on that statement.
Something that is indisputable about this
is that this needs to happen nationwide, even at 5%.
One out of 20 cops that's out on the street shouldn't be there.
That's way more than a few bad apples.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}