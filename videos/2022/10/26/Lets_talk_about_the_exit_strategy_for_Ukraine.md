---
title: Let's talk about the exit strategy for Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Nto-X-QFlYE) |
| Published | 2022/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the term "exit strategy" in relation to Ukraine and US foreign policy.
- Mentions the skewed perspective of Americans on war due to the last 50 years of US foreign policy.
- Explains the defensive nature of the war in Ukraine and how it differs from US elective wars.
- Emphasizes that the US is backing Ukraine, not leading the war, and should not negotiate with Russia directly.
- Stresses that decisions about peace and territorial concessions lie with Ukraine, not the US.
- Warns against imposing conditions on Ukraine for peace, as it could lead to US engaging in imperialism.
- Advocates for letting the Ukrainian people decide the course of their war and not pressuring them to fight or quit.
- Criticizes the idea of backing imperialist thoughts for better TV ratings.
- Calls for respecting the agency of those fighting for liberation in Ukraine.

### Quotes

- "There isn't an exit strategy. It doesn't exist because it's a defensive war."
- "This is something for the Ukrainian people to decide, not pundits on TV."
- "Just because the war isn't getting good enough ratings, I don't know that's a good enough reason."
- "The US shouldn't negotiate with Russia directly. That's 100% wrong."
- "We supply them, we keep their war running as long as they want to fight it."

### Oneliner

Beau explains why the US should not have an exit strategy in Ukraine and why it's up to the Ukrainian people to decide the war's course, not pundits or imperialist influences.

### Audience

Policymakers, Activists

### On-the-ground actions from transcript

- Support Ukraine by providing aid and supplies (suggested).
- Respect the agency of the Ukrainian people in deciding their war's course (implied).

### Whats missing in summary

The full transcript provides deeper insights into the nuances of US involvement in Ukraine and the importance of letting the Ukrainian people determine their future.

### Tags

#Ukraine #USForeignPolicy #DefensiveWar #Imperialism #SupportUkraine


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about exits
and strategy in Ukraine,
because I keep hearing this term pop up.
And initially, I found it a little humorous in a weird way
because it was just commentators.
And I understood why they were asking the question
And I didn't think it was that big of a deal.
But I recently heard a military analyst ask this question.
So now we definitely have to talk about it because it made me realize how much the last
50 years of US foreign policy has skewed the way Americans look at war.
question is what's the exit strategy for Ukraine? I get it. I understand where this
question comes from because for half a century that was a question that should
have been asked about every US military operation and for the last two decades
It's really only been the United States making moves on the international scene,
particularly those that involve a lot of military force. And that's a question
that has to be asked because the US is engaging in adventurism, because it's a
US offensive. This is a defensive war. There is no exit strategy. That's not a
a thing here that doesn't exist. It's a defensive war. You win or you lose. That's
it. Those are the only two options. This wasn't an elective war. It's not a war
where US troops went in and, you know, destabilized a government or replaced it
and now they're trying to figure out how to, you know, extract themselves from the
situation they've they've created. It's nothing like that. This is a defensive
war. Another country, Russia, decided to make a move on the international scene
using military force. They have to have an exit strategy. More importantly, this
This isn't a U.S. war.
The U.S. doesn't get to make this call.
The closest thing you could have to an exit strategy is the moment when Ukraine decides
to start looking at peace options.
The U.S. can't get involved in that.
I know there's a lot of people that are saying the United States should negotiate with Russia
directly.
100% wrong. There is no way the US can do that, not maintain the premise that it set
out with. This isn't a US war. The US is backing up a country that was invaded with supplies.
That's what's happening.
If the United States starts telling Ukraine, well, you know, maybe you'll let them keep
Dombus.
Why don't you just go ahead and draw that comic with the world powers sitting there
carving up the globe?
Because that's what it turns into.
It's a defensive war for Ukraine.
gets to decide when they are looking at peace, whether they're willing to cede
territory, whether they're willing to give up Crimea, these are all questions
for the Ukrainian people, not for people in DC. If it turns into the United States
saying, you have to accept these conditions, these peace conditions.
Everything that the US did here was for nothing. Because right now as it stands
without the United States doing this, Russia is engaging in imperialism and
the United States for once is on the right side of things when it comes to
the ideological aspects of it. If they start putting conditions on the maps
and saying, oh well you're gonna give this other major power, you're gonna give
them this land, the US is now engaged in that same kind of sphere of influence
imperialism. One of the things that has caused so many problems on this planet.
There isn't an exit strategy. It doesn't exist because it's a defensive war. If
you're talking about when it comes time for peace, that decision is made in
Kiev, not in DC. The US shouldn't have anything to do with that. We supply them,
We keep their war running as long as they want to fight it.
We don't press them to fight. We don't press them to quit.
That's it. This is something for the Ukrainian people to decide, not pundits on TV.
Just because the war isn't getting good enough ratings, I don't know that that's a good enough reason
to start backing imperialist thought processes or selling out people who are
fighting trying to liberate their hometown. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}