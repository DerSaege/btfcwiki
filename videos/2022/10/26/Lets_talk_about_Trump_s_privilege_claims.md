---
title: Let's talk about Trump's privilege claims....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eqUkYboe70Y) |
| Published | 2022/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the focus on executive privilege, Trump, and the investigation around the events of January 6th.
- Mentions the grand jury in the DC area and how Pence's inner circle has testified regarding Trump and January 6th.
- Notes the prosecutors breaking through claimed executive privilege and attempting to compel White House counsel's office members to testify.
- Emphasizes that this is a criminal case, not political, and how constitutional scholars believe testimony will be compelled due to the crime.
- States that historically, the Supreme Court has ruled unanimously that executive privilege does not apply in criminal cases.
- Speculates that efforts to block testimony indicate potentially incriminating information and a strategy to delay the legal process.
- Suggests that if Cipollone testifies, it could be a significant blow to Trump's defenses as it pierces his inner circle.
- Anticipates movement in the case soon, as key figures are now involved in the grand jury process.

### Quotes

- "The rings around Trump, they're getting smaller and smaller."
- "If Cipollone has to testify before the grand jury, they are in the room."
- "They're at the point where they have pierced his inner circle."
- "They are in the room where decisions are being made."
- "They have a very clear picture at that point."

### Oneliner

Beau explains the legal developments around executive privilege, Trump, and the January 6th investigation, signaling potential trouble for Trump's defense as key figures are compelled to testify.

### Audience

Legal observers, political analysts.

### On-the-ground actions from transcript

- Contact legal experts for insights on executive privilege and criminal investigations (suggested).
- Stay informed about the developments in the legal case (implied).

### Whats missing in summary

Insights on the potential implications of key testimonies and how they may impact the ongoing legal proceedings.

### Tags

#ExecutivePrivilege #Trump #January6th #LegalCase #GrandJury #CriminalInvestigation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about, uh, executive privilege, Trump, and what's
going on up in, uh, the case that's looking into the events surrounding the capital.
So not the documents case, not the New York case, not the Georgia case, this
is the one, uh, up, up in the DC area.
So, it is now being reported that some of Pence's inner circle have testified before
the grand jury looking into Trump and the January 6th thing.
That is interesting because it shows a kind of a hole that prosecutors have been able
to get into to kind of break through the wall of claimed executive privilege.
We have also found out that they are attempting to get a couple of people from the White House
counsel's office including Cipollone and putting forth the process to get them
compelled to talk to the grand jury. Now keep in mind this is a criminal case.
This isn't a political thing, this isn't the committee hearings, this is a
criminal case. Now you have a number of constitutional scholars who have weighed
in on this and the general consensus is yeah they're gonna be compelled to
because the grand jury is asking for the testimony and it has to do with a
crime that is going to supersede executive privilege and when it has gone
to the Supreme Court when this issue has reached the Supreme Court it has always
been unanimous in saying that when it comes to criminal activity executive
privilege does not apply and that the grand jury can get that testimony. If the
White House counsel is compelled to testify in the grand jury or before the
grand jury, that's gonna be huge. That's gonna be a massive blow to Trump's
defenses. His current strategy appears to be to try to run this out and delay and
delay and delay, which is again a legal strategy he uses a lot.
At the same time, he's claiming a bunch of different types
of privilege in an attempt to stop these people from talking.
The reason he would want to do that and expend this much effort
is because they may have information that
would be incriminating.
That seems like the logical reason
to expend this much effort to stop somebody from talking.
So this is one of those things that is proceeding and it's the rings around Trump, they're
getting smaller and smaller.
If Cipollone has to testify before the grand jury, they are in the room.
They're at the point where they have pierced his inner circle.
They are in the room when decisions are being made.
They are in the room when he is reacting to events.
They have a very clear picture at that point.
theoretically this could be one of the the final developments before the grand
jury makes decisions so we can anticipate that we're going to we'll
probably see some movement on that case soon because they're they're in the room
where it happened.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}