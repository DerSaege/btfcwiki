---
title: Let's talk about Trump's claims about other Presidents and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qUxn-5EVf_Q) |
| Published | 2022/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing former President Donald J. Trump's statements regarding document storage habits of other former presidents.
- Trump alleged that Obama, Clinton, and Bush one and two took documents.
- National Archives has debunked all of Trump's claims.
- Trump had custody of documents, unlike other former presidents whose documents were with the National Archives.
- Trump is attempting to downplay the issue by framing it as a document storage problem.
- Democrats and liberal commentators are urged to focus on documents with National Defense Information, not trivial matters like love letters from dictators.
- Questions need to be asked about the National Defense Information documents' handling and intent.
- Trump's focus on minor issues is diverting attention from potential serious crimes.
- DOJ's role in investigating the retention of National Defense Information and possible transmission to a third party.
- It is vital not to let Trump control the narrative and shift focus from the critical questions at hand.

### Quotes

- "Don't let him control the narrative."
- "What matters are the documents that had National Defense Information."
- "Stop worrying about the Presidential Records Act."

### Oneliner

Beau analyzes Trump's attempt to downplay document storage issues, urging a focus on documents containing National Defense Information and critical questions while cautioning against letting Trump control the narrative.

### Audience

Journalists, Activists, Citizens

### On-the-ground actions from transcript

- Question the handling and intent of documents with National Defense Information (implied)
- Focus on critical questions about the documents (implied)
- Refrain from letting Trump control the narrative (implied)

### Whats missing in summary

Analysis of the potential serious crimes and the importance of focusing on critical questions rather than trivial matters.

### Tags

#DonaldTrump #NationalDefenseInformation #DocumentStorage #PresidentialRecordsAct #Focus


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
some of former President Donald J. Trump's statements
from his little rallies.
He has made a number of statements
about other former presidents
and their document storage habits.
And we're going to kind of go through
and kind of look at those statements real quick.
Okay, so presidents that Trump has in some way alleged
that they took documents include Obama, Clinton,
and Bush one and two.
Two include a really bizarre rant
about a former bowling alley.
That, anyway, so what he's trying to do,
it certainly appears, is draw an equivalency
between his behavior and the behavior
of other former presidents.
The most important thing to note about this
is that the National Archives
has already debunked all of his claims.
The key difference between the situations
that Trump is pointing to and his own situation
is that in all of the other situations,
the National Archives had custody of those documents,
not the president.
And I know it seems like that would be
a really bizarre thing for him to do,
not the misleading his audience part.
That's not bizarre.
He knows they're not going to fact check him.
But pointing out that all of the other presidents
had their documents in the custody of the National Archives,
that would seem counterproductive.
But let me float an idea here.
It seems to me that he is going out of his way to do this
so he can frame it as a document storage issue,
something that he's tried to do.
He's tried to frame it in that light,
that the only difference, the only issue here
is that the documents were in his custody
rather than the National Archives.
And that's not a big deal.
He's trying to paint the whole thing, it seems to me,
in that light to make it seem really minor.
But aside from the custody aspects of it,
there's the whole question of them constantly asking
for them to be returned and him not returning them.
This isn't a document storage issue.
Now for Democrats, for liberal commentators,
hear me out on this.
Stop worrying about the Presidential Records Act.
Just stop.
I understand it's a crime.
It's something that's going to be a big deal.
It's something he shouldn't have done if he did do it,
you know, blah, blah, blah.
I get it, but let's be honest.
Do any of us really care if Trump took his love letters
from dictators?
That's not what matters here.
What matters are the documents that had National Defense
Information, the documents that had classification marks,
the SCI documents.
That's what matters.
And we have some questions that we should be asking,
like who gave them to you?
What were you going to do with them?
Where were you going to send them?
When were you going to send them back
if you weren't going to transmit them to a third party?
And why did you want them to begin with?
Who, what, when, where, why?
Those are the questions that matter.
And it's the documents that contain National Defense
Information that are really important.
I understand that on some level, the remainder of the documents,
that that's an issue, because yes, they
should have been returned.
I get that.
But you're talking about the difference
between a speeding ticket and a really serious crime.
It certainly appears that Trump is
trying to get everybody to focus on the speeding ticket
and ignore the possibility of an incredibly serious crime.
Understand that when it comes to this,
you can't overplay your hand either.
I've seen a reporting about a suggestion
when it comes to what he intended to do with them.
I have questions about that reporting.
But most importantly, it doesn't matter.
That's DOJ's job.
As far as the crime, what matters
is that he gathered National Defense Information
and that he willfully retained National Defense Information.
That is what would constitute a crime.
The possible transmission to a third party, that's, I mean,
yeah, that would make it a more serious crime.
But that's up to DOJ.
We don't know that yet.
And again, we don't know any of this yet, really,
because it hasn't been tested in court.
But the point that matters is not his love letters.
It's not the fact that he was so infatuated with dictators
that he wanted to retain this stuff.
It's not the fact that he had sloppy record-keeping
practices.
That's not what matters.
What matters is those documents that never should have left
under any circumstance.
That's what matters.
Don't let him control the narrative.
Don't let him try to frame this as some clerical issue.
We want to know why those documents were there,
why the documents containing National Defense Information
were at his house.
Who, what, when, where, and why.
Those are the questions that matter.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}