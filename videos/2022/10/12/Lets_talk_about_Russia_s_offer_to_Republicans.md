---
title: Let's talk about Russia's offer to Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PqX1HD5xIpU) |
| Published | 2022/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia offered American states the chance to break away and join Russia, sparking various reactions.
- Mentioned Deputy Tolmachev in the Russian parliament expressing openness to American states joining Russia.
- Raised the scenario of Alaska being annexed by Russia and questioned the US response.
- Criticized those who support Russian intervention in Ukraine but oppose potential Russian interference in the US.
- Pointed out Russia's motive behind the offer as gloating and making fun of the Republican base.
- Discussed how successful Russian influence operations have led American patriots to take positions detrimental to the US.
- Noted that individuals influenced by Russian propaganda may unknowingly act against American interests.
- Urged people to be cautious of pundits echoing Russian talking points and potentially being Russian allies.
- Warned about falling into information silos and unknowingly betraying fellow Americans.
- Suggested reflecting on how one's beliefs and actions may have been influenced by external sources.

### Quotes

- "They got you to sell out your country without you ever even knowing it."
- "You ignored them and de facto allied with them."
- "You're the person I'm talking to."
- "Patriots right?"
- "Everything that you were afraid of, all of those crazy conspiracy theories that y'all hatched, in some ways it kind of occurred."

### Oneliner

Russia offers American states to join, revealing how successful influence operations sow division and weaken American patriotism.

### Audience

American citizens

### On-the-ground actions from transcript

- Question sources of information and be wary of pundits echoing Russian talking points (implied)
- Engage in critical thinking and analysis of political commentary to identify potential foreign influence (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of how foreign influence can exploit divisions within a nation, urging individuals to critically analyze their beliefs and sources of information.

### Tags

#ForeignInfluence #Patriotism #CriticalThinking #RussianPropaganda #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about an offer from Russia to various American states.
Everybody look at my hat real quick.
This isn't satire. Patches on right side up and everything.
OK, so I believe it was Deputy Tolmachev in the Duma, in their parliament.
He said that if there was an American state or states that was interested in breaking
away from the United States and joining Russia, well, they would consider the application.
And this is funny on one level and it's sad on another.
And there are so many different ways to take this video that I'm not really sure where
to go with it.
But let's just start with this.
Let's take Alaska, a state known for being very independent.
And let's say that Russian troops show up tomorrow and they stage a little fake referendum
and the results of that referendum are in favor of joining Russia.
And Russia annexes it.
Does the U.S. just let that go?
If your answer is no, if the idea of Russian troops on American soil bothers you, stop
telling Ukraine to give up their land.
Stop telling Ukrainians to accept a ruler they don't want.
OK, but let's go to the funny but not so funny part about this.
Why is this being said?
Now it was said in response to polls about people wanting to break away from the U.S.
But why is Russia saying it?
Because they're gloating.
They're making fun of the Republican base.
And the Republican base doesn't even realize it.
That's the funny not funny part.
The agents of influence and the information operations that Russia has run have been so
successful that the Republican Party as a whole here, talking in generalities, has taken
positions so detrimental to the United States that Russia is now joking about accepting
them.
Yeah, you're just Russian now.
Come on.
I mean, I remember the shirts when Trump ran that said, you know, better a Russian than
a Democrat or something like that.
I guess some of y'all took that literally.
The point here is that a whole bunch of people who fashion themselves as patriots have taken
positions so antithetical to their fellow American that government officials in other
countries are openly making fun of them, are openly mocking them when it comes to their
desires in the future because they're weakening the United States to such a degree that the
only possible explanation is that they're actually a Russian ally.
Patriots right?
Y'all might want to think about that a little bit.
If you have fallen down one of those information silos and your favorite political commentator
is just constantly echoing Russian talking points.
I'm not talking about one or two.
I'm not talking about somebody who's opposed to US involvement in Ukraine.
I'm talking about somebody who is constantly echoing Russian talking points or holding
Putin up as like a good leader.
You're the person I'm talking to.
If that's your pundit, if that's the commentator that you follow, understand they have got
you to such a degree that they are publicly mocking you.
They got you to sell out your country without you ever even knowing it.
They got you to turn on your fellow American without you ever even knowing it just by offering
you a cheap red hat.
Something you might want to consider.
Everything that you were afraid of, all of those crazy conspiracy theories that y'all
hatched, in some ways it kind of occurred, but it occurred to you because you were so
busy manufacturing opposition that you didn't realize the United States still has opposition
out there.
You ignored them and de facto allied with them.
And now they are publicly mocking you for it.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}