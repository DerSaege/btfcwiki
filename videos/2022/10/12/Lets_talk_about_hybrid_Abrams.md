---
title: Let's talk about hybrid Abrams....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FvkhCPPBORU) |
| Published | 2022/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of fuel consumption and the U.S. military, discussing a recent concept seen that may be revolutionary.
- Talks about the new electric light reconnaissance vehicle for the U.S. military and its potential impact on consumer vehicle choices.
- Mentions the sneak peek of a new hybrid concept for the Abrams tank, a major piece of equipment for the military.
- Describes the staggering fuel consumption of the Abrams tank in terms of starting fuel requirement and the shift from miles per gallon to gallons per mile.
- Emphasizes the significance of the U.S. military's move towards hybrid and electric vehicles in reducing emissions and fuel consumption.
- Points out how this shift can lead to a decrease in the need to maintain access to oil markets through military force.
- Addresses the potential positive impacts on national security by reducing the necessity for wars in regions with oil resources.
- Acknowledges the role of the U.S. military in maintaining access to oil markets and suggests that a transition to electric vehicles could change this dynamic.
- Expresses optimism about the environmental, national security, and human benefits of transitioning the military's vehicles to hybrid and electric models.

### Quotes

- "It's measured in gallons per mile. It's massive."
- "If we don't need oil, we're not going to be in the Middle East."
- "This is something that can literally stop or slow wars before they start."

### Oneliner

Beau introduces the revolutionary potential of the U.S. military's shift towards hybrid and electric vehicles, impacting fuel consumption, emissions, and national security.

### Audience

Climate advocates, military personnel

### On-the-ground actions from transcript

- Support initiatives that advocate for the transition of military vehicles to hybrid and electric models (suggested).
- Encourage companies to invest in developing more affordable electric vehicles (implied).

### Whats missing in summary

The full transcript provides detailed insights on how transitioning military vehicles to hybrid and electric models can have far-reaching impacts on fuel consumption, emissions, national security, and global conflicts. Viewing the full transcript offers a comprehensive understanding of these interconnected issues.

### Tags

#FuelConsumption #USMilitary #HybridVehicles #ElectricVehicles #NationalSecurity


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about fuel consumption and the U.S. military,
and how a sneak peek of a concept that we recently saw
may be way more revolutionary than people are giving it credit for,
because we saw something and it led to a whole bunch of conversations
going in a whole lot of different directions.
So we're going to kind of go through it.
Recently, I put out a video talking about how the new LRV,
the light reconnaissance vehicle for the U.S. military, is going to be electric.
And I was really excited about this because this is something
that will make a whole lot of people who buy,
who currently drive very fuel-intensive personal vehicles,
switch to electric, because they're going to want to be able to say,
I got the same vehicle the Army has, and it'll go from there.
It'll speed transition.
We've seen this type of thing before.
The next day, we got a sneak peek of a new concept, Abrams.
Now, for those who don't know, an Abrams is a tank.
It is the tank for the U.S. military.
It is a monstrosity, and it's going to be a hybrid.
It's going to be a hybrid.
Now, you can go over the numbers for fuel consumption on this,
but this is one of those things that it's so hard to grasp
because it's so different than what we're used to in a personal vehicle
that it doesn't really give you the context you need.
So we're going to do this a different way.
If you have a normal car, like an Elantra, as an example,
and your gas tank is full, if it operated the same way an Abrams did,
when you cranked it up, the gas light would come on.
That's how much fuel these things take.
It takes 10 gallons to start it.
It's not measured in miles per gallon.
It's measured in gallons per mile.
It's massive.
And the new concept is a hybrid.
Now, from here, conversations just went off
in all kinds of different directions.
One is basically the question of, is this just like PR?
No, this is huge.
This isn't just a little stunt.
This is the new concept.
This falls in line with DOD's drive to actually get away
from fossil fuels as much as possible.
And the Abrams, it's a major piece of equipment.
This is the centerpiece.
It's important to remember that the US military is
the largest institutional consumer of fuel in the United States,
and the United States is the largest consumer of fuel.
This is huge.
A switch like this is big.
Not just does it immediately, when the switch occurs,
not just does it cut emissions drastically,
it also encourages people who are in the military
and also encourages people who might be reluctant to embrace
electric to kind of develop an appetite for it, which
means more companies will make them, which
means they will get cheaper, which
means there will be other varying types available.
It's a big deal.
And nobody is going to be able to say, well, you know,
I don't want an electric because I want the power.
Yeah, you do not need the power of an Abrams, buddy.
So it's a big deal in that regard.
And then there were a lot of people, particularly those who
really understood the long-term impacts of this,
who were making jokes.
And some people didn't realize they were jokes.
Somebody said something to the effect of,
now the tank that goes to liberate
the fuel in the Middle East won't need it,
or something like that.
I mean, I don't know.
I didn't make the comment.
But that sounds like a comment coming
from somebody who understands the long-term impacts of this.
I think most people watching this channel
understand that US foreign policy uses the US military
to maintain access to oil markets.
I don't think that that's a controversial statement here.
If the largest institutional user of fuel in the US
makes a switch and people who use fuel
in their personal vehicles make a switch, we need less fuel.
Therefore, we won't need to maintain access
to those markets.
Therefore, not just will the tank not
need the fuel over there, it won't be over there.
Contrary to a lot of the way people look at things
from a surface level, the US military
doesn't just invade countries for no reason.
There's a reason for this sprawling defense empire.
And it has to do with economics.
It has to do with capitalism.
It has to do with maintaining access to markets.
If we don't need oil, we're not going to be in the Middle East.
And those who were railing about it being the woke military
and hurting national security, allow
me to just rephrase that last part.
If a transition like this takes place,
we won't be going to the Middle East.
This is something that can literally stop or slow wars
before they start.
This is something that removes a presence that
is upsetting to the local population that spawns attacks.
There's no downside to this at all.
And if they can do it with an Abrams, as the concept suggests,
they can do with anything.
They can do with anything.
It looks like they are showing a lower end vehicle.
An LRV is pretty simple.
I mean, it's a Humvee.
OK?
That's one level.
The opposite end of the spectrum is something like the Abrams.
It doesn't really get more fuel intensive than that.
If they can run that off of a hybrid
and then move to electric, it's revolutionary.
And it is something that would cut out the need,
the perceived need, to maintain access to those oil
markets through military force, which stops wars,
which saves lives.
This is great.
And this isn't a thing that is just a PR stunt.
The US military has been talking about climate change
and how it's a national security threat for a while.
And they've been making a lot of strides to start breaking free.
This is just kind of a, all of a sudden,
we're seeing a rush of it when it comes to vehicles
that people know and concepts that people are aware of
and how it will function.
It's just becoming more public now.
There is no downside to this.
It's good for the environment.
It's good for national security because we won't
have to fight these wars.
It's good for the people who live
in the areas that have that resource there
because the wars won't be there.
We won't be trying to maintain access to them.
It's good.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}