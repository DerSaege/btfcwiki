---
title: Let's talk about Bolsonaro's loss and what it can teach us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9FueG7nMeqo) |
| Published | 2022/10/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lula defeated Bolsonaro in Brazil's election, marking a shift away from far-right nationalism.
- Bolsonaro, likened to Trump, is refusing to concede the election.
- Nationalism is on the decline globally, as seen in Brazil and the US.
- The world's interconnectedness through technology is eroding nationalist ideologies.
- Nationalism thrives on fear and ignorance, making people vulnerable to manipulation.
- Leaders who exploit nationalism often do not truly believe in it, using it to control their followers.
- Loyalty to national symbols is diminishing as people gain broader perspectives through increased communication.
- Bolsonaro's loss is a positive development for Brazil and the world, signaling a rejection of far-right nationalism.
- The future trends towards decreased nationalism as understanding and communication increase.
- Ultimately, the diminishing power of nationalism is an inevitable consequence of a more connected world.

### Quotes

- "Nationalism is a dying ideology. It's going away. It is going away."
- "Fear in most cases is a lack of understanding. That ignorance. It's a breeding ground for fear, for nationalism."
- "Those symbols becoming less valuable. It is scary for them. It is terrifying for them."
- "This is going to happen on a global scale."
- "Bolsonaro's loss, it's great for Brazil, and it's great for the rest of the world."

### Oneliner

Lula defeating Bolsonaro in Brazil signals the decline of far-right nationalism globally, fueled by increased understanding and interconnectedness.

### Audience

Global citizens

### On-the-ground actions from transcript

- Reach out to local communities to foster understanding and connection (implied).
- Educate oneself and others on the dangers of nationalist manipulation (suggested).
- Support leaders who prioritize unity and inclusivity over divisive nationalist rhetoric (implied).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the global trend away from nationalism and the implications of increased communication and understanding on political ideologies.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Brazil and the election results down there
and what those results can tell us about the future in general,
not just of Brazil, but of the world.
Lula defeated Bolsonaro.
Now, Bolsonaro was the incumbent and is a far-right nationalist.
This is somebody that was referred to as the Trump of the tropics or the Trump of Latin America,
one that makes me cringe. That's not right. Don't use that.
He was a far-right nationalist.
He's a far-right nationalist that was very, very similar in rhetoric, in leadership style to Trump.
So much so, like many other nationalists who lost an election by millions of votes,
he is currently at time of filming refusing to concede.
Good news is that a lot of his allies are admitting the loss.
So since his allies seem to have a little bit more integrity than other nationalist allies,
hopefully this is something that can work itself out pretty quickly and peacefully.
This is really good news for Brazil and it's really good news for the entire world
when it comes to what's going on in the Amazon as far as the environmental situation.
But beyond all of this, there's a little note that we can take away from it.
Nationalism, it's seeing a peak right now, but it's probably one of the last ones.
Nationalism is a dying ideology. It's going away. It is going away.
Yes, there are peaks. There's a resurgence.
We're seeing that far-right nationalism show up in different places all over the world.
But much like the United States and Brazil, it's short-lived.
It is short-lived because people quickly, after experiencing it, reject it.
The reason nationalism can't really continue to exist as an ideology the way it has in the past
is because the world's getting smaller.
Nationalism was a way for those at the top to keep those at the bottom in fear,
to keep them worried about what those people are doing.
Those people from that different culture in that place that they've never been to
that speak a different language that they don't understand.
The advent of a lot of technologies has altered that.
I can sit on my back porch in the middle of nowhere in the United States
and watch Russian commentators live on TV.
I can watch commentators in other countries.
And even if I don't speak the language, I can use a translation app and understand what's being said.
This gets rid of that fear because fear in most cases is a lack of understanding.
That ignorance.
It's a breeding ground for fear, for nationalism,
and it makes people who have attached their self-worth to some geographic lottery
very susceptible to con men.
Because those con men, those people that pretend to be nationalist,
in most cases, the leaders aren't actually believers in the ideology.
They just use it to manipulate those below them.
Because they know they can wave that flag and talk about whatever the country is
and talk about how great they're going to make it.
And because many people were conditioned to support the flag unconditionally,
they know that they'll follow it.
They know that it'll make them easy to manipulate.
It's something we have seen play out in multiple countries across the world.
And we'll see it play out, I would imagine, a few more times.
But this ideology is disappearing and it is scary.
Those people who have attached their entire identity and self-worth
to the nation they were born in, lines on a map and a flag,
those symbols becoming less valuable.
It is scary for them.
It is terrifying for them.
Because if that isn't the end-all be-all of what to really worry about and believe in,
what are they?
That's how they obtain their self-worth.
And those who prey upon that, those who manipulate them, they know that.
So it keeps coming back up.
But it fails quickly.
This isn't the first time.
It's not even the first time in the United States something like this has happened,
where increased contact kind of undermined a form of nationalism.
There was a time in the United States where people typically held their loyalty
to their state, not the US as a whole.
That faded as more and more contact happened, as it became more normal
for trade and communication to occur.
This is going to happen on a global scale.
There's nothing that can be done to stop this, really.
It's just the natural progression of the world.
The more communication there is, the more understanding there is,
the less fear there is, the less effective nationalism is
at manipulating those who choose to remain ignorant.
Bolsonaro's loss, it's great for Brazil, and it's great for the rest of the world.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}