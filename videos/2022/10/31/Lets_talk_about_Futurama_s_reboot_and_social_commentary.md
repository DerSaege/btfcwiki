---
title: Let's talk about Futurama's reboot and social commentary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sRTrlutD3oM) |
| Published | 2022/10/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the premise of the show "Futurama" where a person is frozen in the year 2000 and wakes up in the year 3000, setting the stage for social commentary and satire.
- Addresses concerns about the reboot injecting social commentary clumsily, mentioning that the entire show is based on social commentary and satire.
- Breaks down episodes from the first season, showing how each one carries social commentary themes, such as political figures, consumerism, religion, and societal issues.
- Points out the strong feminist character in the show who belongs to the permanent underclass, residing in the sewers but vital for society's functioning.
- Lists various social issues addressed in "Futurama," including climate change, cancel culture, polyamorous relationships, and exploitation of native cultures.
- Asserts that "Futurama" has always been woke, combining satire with bluntness to deliver its social commentary effectively.

### Quotes

- "Futurama was always woke."
- "If you want a Futurama without social commentary, you're going to have to build your own Futurama with Blackjack."

### Oneliner

Beau breaks down how "Futurama" excels in delivering social commentary through satire, bluntly addressing various societal issues with a woke perspective.

### Audience

Fans of "Futurama"

### On-the-ground actions from transcript

- Watch and support the rebooted episodes of "Futurama" to appreciate and encourage the continuation of its social commentary (implied).

### Whats missing in summary

A deeper appreciation for the intricate and thought-provoking social commentary woven into the fabric of "Futurama."

### Tags

#Futurama #SocialCommentary #Satire #ScienceFiction #Woke #Reboot


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
Futurama and social commentary and satire and science fiction in general. Because we
got a question about it. If you're not familiar with this show, it began in the late 90s and
it ran for quite some time off and on. It's being rebooted now. The general premise of
the show is that a person was frozen in the year 2000 and they awoke in the year 3000
and things just kind of go from there. Here's the question. You've referenced Futurama in
videos. I thought it was weird because it's crude and definitely not PC. They're rebooting
the series. Are you worried about it being clumsily injected with social commentary?
It would kill the show. The entire show is social commentary. It's satirical. It's over
the top at times. It's very, very, very blunt. It's very clumsy. It's not hard to see. Sometimes
people aren't picking up on the satirical aspects of it. Just as a general guide, if
it's science fiction, it contains social commentary of some kind. It's just a thing. In Futurama,
it is incredibly overt. It is right up there. It is thematic to the episodes. So what we're
going to do is go through the first season real quick and then we'll hit some of the
other themes that they talk about throughout the series. Now the first three episodes of
the show in season one are just world building. It's setting it up. It's Rick and Morty from
the 90s. I know Rick and Morty people are going to be mad about that. That's fine. But
if you really think about it, you have the less than intelligent character being guided
through the new futuristic world by a less than ideal set of guides. It's the same show.
It's the same plot device. Okay. So the world building episodes, the first three, there's
social commentary in them, but they're not thematic to the episode itself. Okay. Season
one, episode four, Zap Brannigan is introduced. Who is Zap Brannigan? Trump. Somebody who
tries to portray themself as an alpha male that clearly isn't. And everybody knows he
isn't except for those people who have bought into the image, the self-manufactured image.
And then when people come in contact with him, they realize that's not who he is. That
alpha male thing is a facade. It's a myth. Just like the whole alpha male thing to begin
with. Episode five is about a world where there are fear mongering politicians using
a scapegoated outsider to control their populace. That's literally the point of the episode.
Episode six, an oil tycoon attempting to stop a new source and going to great lengths to
do so. Episode seven, tradition leads to incompetent leaders. Episode eight is basically an episode
just trashing, pardon the pun, our current society when it comes to consumerism and the
amount of waste that we have and all of that stuff. Episode nine, a robot vendor finds
religion after having a substance issue because religion is the opiate of the masses. This
is the show. On top of this, you have commentary on food, the food chain and veganism. You
have a non-binary character who switches everybody's gender. There is constant commentary about
sexism. I would note the most capable character in the entire show is a woman who is also
part of the permanent underclass. There is a permanent underclass and it is literally
clumsily injected as being below them in the sewers. The society of mutants that keeps
everything running and gets no credit. But don't worry, those rich folk, they have set
up the United Mutant College Fund. It is very clumsy. It is way over the top. It's satire.
It's supposed to be. There are multiple episodes about climate change, colony collapse. There's
an entire episode about desecrating the flag and how that's okay. There's commentary on
government stimulus checks. There's commentary on cancel culture, social media, polyamorous
relationships, gay marriage, evolution, cross-species dressers, transhumanism, native cultures being
exploited. These are all things that are integral to that universe. Yeah, I'll go ahead and
say the line, Futurama was always woke. It's satire. It is crude. It isn't soft. And part
of the thing, part of the issue with this is that a lot of the framing around being
woke is that it has to be very sensitive. It doesn't have to be. I mean, you can hit
the subject over the head with a hammer in some cases, which is what Futurama does. No,
I'm not worried about it. I think they'll handle it very well. And given the fact that
I've already seen some of the new episode titles, one of which is, let's see, Rage
Against the Vaccine, and one is Zzap Gets Canceled, I would imagine, yeah, there's going
to be some social commentary in it as has always been. If you want a Futurama without
social commentary, you're going to have to build your own Futurama with Blackjack. And
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}