---
title: Let's talk about Halloween, filters, and Fermi....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BI5JrtRSPRM) |
| Published | 2022/10/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Bob McGowan says:

- Introduces the Fermi paradox and filters, questioning why intelligent life beyond Earth hasn't been found despite the high probability of its existence.
- Explains the great filter theory, suggesting that intelligent civilizations may destroy themselves before becoming space-faring.
- Mentions various filters that could prevent civilizations from advancing, such as disease, nuclear war, climate change, artificial intelligence, religious stagnation, and wealth obsession.
- Challenges the notion that humanity has surpassed all filters and warns against arrogance in assuming invincibility.
- Concludes with a reflection on humanity's role as the most dangerous threat to itself and the determining factor in its survival.

### Quotes

- "The belief that we've got it, that we've got it all figured out, that we'll be able to surmount any obstacle that comes our way, any filter what we're gonna make it through."
- "This Halloween, as you think of all the ghouls and goblins, remember that the biggest predator, the most dangerous animal, the biggest monster walking the earth is us, nobody else."

### Oneliner

Bob McGowan examines the Fermi paradox, warning against human arrogance as the ultimate filter for civilization's survival amid various existential threats.

### Audience

Science enthusiasts

### On-the-ground actions from transcript

- Question assumptions of invincibility and actively work to prevent potential filters from hindering progress (implied).
- Foster a mindset of humility and collaboration in addressing global challenges to ensure a sustainable future (implied).

### Whats missing in summary

Exploration of the potential impact of societal values and beliefs on civilization's advancement and survival.

### Tags

#FermiParadox #GreatFilterTheory #ExistentialThreats #HumanArrogance #CivilizationSurvival


## Transcript
Well, howdy there internet people, it's Bob McGowan and welcome to our Halloween special.
Tonight we are going to talk about the Fermi paradox and filters and why some of the things
that we perceive as something to worry about may not be and why some of the things that
we're thankful for may actually be the things to worry about. One of the big
questions when it comes to looking up at the stars is where is everybody? Given
the number of stars, the number of presumed planets, solar systems, there's
There's intelligent life out there, somewhere.
So why can't we find it?
Why can't we even find a trace of it?
Where is it?
This is what's known as the Fermi paradox.
The almost certainty that intelligence of life exists somewhere else in the universe
and the total lack of evidence that it does.
Statistically, there should be thousands upon thousands of intelligent civilizations out
there.
But we can't find them.
Radio signals, nothing.
Why?
One of the possible solutions to this is a principle called the great filter theory.
And that is that any intelligent life, any life that is intelligent enough to become
star-faring, to get out there into space and reach the final frontier and all of that,
well they're also intelligent enough to develop the means to destroy themselves before they
get that capability.
Some of it is just normal evolutionary process.
Some intelligent life could have been wiped out by disease, and therefore, well, it never
makes it to the stars.
But along the way to getting to the stars, there are other things that get discovered.
Not just disease, but the weaponization of disease, biological warfare.
Not just understanding nuclear power, which probably is going to play a part in traveling
to distant planets, but nuclear war.
These things filter out civilizations.
These are the ones we think of when you think of things that just could automatically destroy
an entire world.
But there's other things.
Along the way to nuclear power, you're probably going to develop combustion.
Combustion would emit gases, and those gases could lead to climate change.
Something that we're kind of battling with right now, right?
at least we're thinking about starting to fight it.
And then there's the concept of artificial intelligence,
creating something to become self-aware
and then realizes that that biological organism,
well, it's just not up to it and getting rid of it.
Then you have more philosophical reasons
that could filter civilizations out, religious stagnation, societies that become so just
bogged down in religious dogma that they never think to look to the stars because they don't
want to offend the gods or God.
You could have a society that becomes just obsessed with its own version of the pursuit
of wealth. And because of that, why go up there? There's nothing there. These are things
that could filter out a civilization and make it something that would never become space
fairing. But don't really lead to the end of this civilization, at least not immediately.
There's a lot of people that, when you talk about this, you'll hear them suggest that
we've pretty much made it through all the filters.
I mean, that seems a wee bit presumptuous to me.
You know, I don't think that people in, I don't know, the 1800s would consider climate
change as something that could filter them out. Before we can cross to other
solar systems, there's probably a whole bunch of new technology that we're going
to discover. Technology is neither innately good or innately bad. It's how
it's used. But these advanced technologies, it's impossible for us to
truly envision some of them, any sufficiently advanced technology, well
that's gonna pretty much seem to be magic to us. That is probably the ultimate
filter, the belief that we've got it, that we've got it all figured out, that
we'll be able to surmount any obstacle that comes our way, any filter what we're
gonna make it through. That arrogance is probably something that is common among
most intelligent forms of life out there. That belief that since we're the
dominant species on this planet, well we'll be the dominant species anywhere
and will always make it. It's that arrogance that is likely to lead to us being filtered
out via nuclear war, combustion, biological warfare, creating technologies and letting
them run amok because, well, I mean, maybe God wants it that way or maybe it's just
best for the bottom line.
This Halloween, as you think of all the ghouls and goblins, remember that the biggest predator,
the most dangerous animal, the biggest monster walking the earth is us, nobody else.
throughout the rest of our time on this planet, we will decide whether or not we ever make
it to another one.
Anyway, it's just a thaw. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}