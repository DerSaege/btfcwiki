---
title: Let's talk about candidates calling on Pence over Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8unldZfSHrI) |
| Published | 2022/10/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A new development within the Republican Party is causing a split and upsetting the former president.
- Republican candidates are now reaching out to Pence, not Trump, to appeal to moderates.
- Candidates who sought Trump's approval are now turning to Pence for support, showcasing a widening split within the party.
- Pence's actions, campaigning for those who criticized him, may negatively impact his reputation with moderate voters.
- The shift towards Pence over Trump is creating animosity within the Republican Party.
- The move towards Pence is seen as self-serving and not ideologically driven.
- Many voters view Pence as not stepping up during critical moments.
- Trump's likely response to Pence gaining attention could reignite news coverage on extreme positions of candidates.
- The focus on power and self-interest among candidates is detrimental to the party's unity.
- The situation is likely to generate long-term problems and animosity within the Republican Party.

### Quotes

- "They're not the MAGA crowd. They're not America first. They're out for themselves and they are playing on the gullible."
- "This is what happens when you have a group of self-serving people who really aren't ideologically aligned except in pursuit of their own power."
- "And Trump is probably going to say something on his little wannabe Twitter about it."
- "I think most [moderate voters] would still view Pence as somebody who did not really step up and talk to the committee the way they probably should have."
- "It's going to cause problems for the Republican Party long term because it is going to generate a lot of animosity within the party and among candidates."

### Oneliner

A split in the Republican Party emerges as candidates shift towards Pence, causing animosity and revealing self-serving motives over ideology.

### Audience

Republican voters

### On-the-ground actions from transcript

- Reach out to moderate voters and build community support for candidates by engaging in local political initiatives (suggested).
- Stand up against self-serving actions within the Republican Party by promoting transparency and accountability (implied).

### Whats missing in summary

Insight into the potential implications of this split on future election outcomes and party dynamics.

### Tags

#RepublicanParty #Pence #Trump #PoliticalSplit #SelfServing #ModerateVoters


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a new development inside the Republican Party
that long-term doesn't bode well for the unity within the Republican Party
or for certain people's political futures.
It's also very likely to be something that is very upsetting to the former president.
A lot of Republican candidates, they're trying to win over voters
who may not really be familiar with their positions.
They're not reaching out to Trump.
They're reaching out to Pence.
They're reaching out to Pence to try to put a more moderate face on their positions,
trying to gain ground with the moderates because, as we've talked about,
can't win a Republican primary without Trump, can't win a general with him.
So all of these people who went out of their way to gain Trump's approval
are now turning their back on him and standing there with Pence.
We've seen it in New Hampshire.
Pence endorsed Masters.
It's a common thing now.
Now, there's a couple of different elements to this.
I mean, the first is the obvious funny one,
that all of the MAGA candidates who spent a lot of time
pretending that the last election had something wrong with it
are now begging Pence for help.
That's entertaining in and of itself.
It's also entertaining that you know that's bothering Trump.
But it also shows a widening split within the Republican Party,
a dynamic that can't be healed easily.
You have a lot of candidates who, during the primary, said,
well, I'm going to do this, and I'm very much a MAGA person,
but then brought in Pence.
The MAGA crowd, they are hyper-partisan.
And I'm fairly certain that they're going to notice
that these candidates can't even hold to their campaign promises
during the campaign, and they have to shift their rhetoric, which means,
well, I believe Trump would call them rhinos.
And they're going to get up there to Capitol Hill
and just like they did on the campaign trail,
they're going to do what's best for them.
They're not going to represent the MAGA subset of the Republican Party.
The flip side to this is Pence, who, had he stepped up during the hearings,
he really could have gone down in history
as one of America's great vice presidents.
Instead, he hops out there on the campaign trail
and starts campaigning for people who said what he did was wrong,
showing that he is willing to allow that rhetoric,
that anti-constitutional republic, anti-representative democracy rhetoric.
He's willing to let that slide
as long as it helps him lay the groundwork for a 2024 run.
I don't think it's going to work,
but that seems to be the move he's making.
And I don't know how this is going to play out
with the moderate voters they're seeking to court either.
I think most would still view Pence
as somebody who did not really step up and talk to the committee
the way they probably should have.
I don't think that they're going to see him
as a big enough step away from Trump to make a difference.
You know, in some areas, being allied with Trump, that's an asset.
You know, there still are some pockets of the country
that are very much MAGA country.
But most of the country is a little bit leery of the former president
and his rhetoric and his games.
And it's an almost certainty that Pence getting the spotlight
is going to bother Trump.
And Trump is probably going to say something on his little wannabe Twitter about it.
And it'll throw it all back up into news coverage.
And then people, despite these candidates' best efforts
to conceal some of their more extreme positions,
well, it's going to be all over the news anyway.
This is what happens when you have a group of self-serving people
who really aren't ideologically aligned except in pursuit of their own power.
This is what occurs.
They're not the MAGA crowd.
They're not America first.
They're out for themselves and they are playing on the gullible,
those who are easily swayed by Mike Pence showing up.
It's going to cause problems for the Republican Party long term
because it is going to generate a lot of animosity within the party
and among candidates.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}