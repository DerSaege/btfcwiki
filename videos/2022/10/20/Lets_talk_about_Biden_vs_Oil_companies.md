---
title: Let's talk about Biden vs Oil companies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RgHsQ1Ee6jE) |
| Published | 2022/10/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration set to meet with oil companies to address rising gas prices.
- Rumor suggests Biden will urge oil companies to lower prices and help American people.
- Massive oil companies like Exxon, Chevron, and Shell posted record profits while consumers suffered at the pump.
- Inflation caused by transportation costs and oil companies maximizing profits.
- President's role in safeguarding national security may involve transitioning away from expensive oil.
- Limited control over oil prices despite public perception linking the president to gas prices.
- Biden administration faces a challenging relationship with the oil industry.
- Outcome of the meeting could lead to oil companies cooperating or facing stronger measures.
- Public perception often associates the president with gas prices.
- Democratic Party concerned about potential political fallout from rising gas prices before the election.

### Quotes

- "Inflation caused by transportation costs and oil companies maximizing profits."
- "President's job is to safeguard the national security of the United States."
- "Most people look at the TV and they associate the head of state with the gas pump."

### Oneliner

Biden administration confronts oil companies on gas prices, facing pressure to alleviate public hardship amid record profits and political implications.

### Audience

Citizens, Voters, Activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for policies that prioritize public affordability over corporate profits (implied).

### Whats missing in summary

The full transcript provides more context on the complex dynamics between the Biden administration and the oil industry, shedding light on the challenges in addressing rising gas prices and public perceptions.

### Tags

#BidenAdministration #OilIndustry #GasPrices #NationalSecurity #PoliticalImplications


## Transcript
Well, howdy there, internet people, it's Bill again.
So today, we are going to talk about
the Biden administration versus the oil industry
and what is expected to happen.
Now, I am filming this early Wednesday morning.
I can already tell by the way news is breaking,
I'm fairly certain this isn't gonna go out
the day I want it to.
The Biden administration is set to meet with the oil companies today.
And the rumor suggests he's going to pull out all the sops.
By the time y'all watch this, I assume that meeting's already taking place, but at time
of filming I don't know what was said.
the indications are that the Biden administration is going to lay out the
facts and be like look you know we've tapped into the strategic reserve and
we're going to continue doing that we're trying to help. You're going to help too.
You're going to lower the price of the pump and help the American people out.
or. That seems like a pretty wild thing, but it's really not. Before anybody
starts feeling too sympathetic for the massive oil companies, I want to point
something out. In the second quarter Exxon posted $17.9 billion in profit.
Chevron 11.6 billion, Shell 11.5 billion. If I'm not mistaken, $17.9 billion in a
single quarter is the highest posted profit of any oil company in history.
Ever. This is the period when all of us were suffering at the pump. This is the
period when gas prices were through the roof and the oil companies are posting
record profits. That's a big part of the problem. A lot of the inflation that is
occurring is because of transportation costs and that inflation is being caused
by the oil company taking everything that they can. Which I understand that
you know we live in a capitalist society and corporations are called unfeeling
machines and their job is to turn a profit for their shareholders. I think
it's important for oil companies to understand that the president's job is
is to safeguard the national security of the United States and if
oil is just too expensive, maybe federal funding should be used to
transition even faster. The president doesn't have a lot of control
over oil prices, despite every
every political commentator trying to tie the two.
They're limited in what they can do.
They have some tools at their disposal, but without doing something incredibly drastic,
it's very much a pressure game to try to get the oil companies to play ball.
The Biden administration's relationship with the oil industry is not great.
So this conversation will either be the oil companies understanding the situation that they're in and understanding that
they may be pushing things too far by trying to squeeze the American public a wee bit too much,
or the Biden administration is going to have to come down really hard and use all of those tools at their disposal in
conjunction.
because reality being what it is, it doesn't matter. Most people look at the
TV and they associate the head of state with the gas pump. We're going to have to
see what happens because at time of filming it's a rumor, it's a well-sourced
rumor, but it's a rumor. It makes sense. And at time of filming, I'm certain that
the Democratic Party is worried about gas prices going up right before the
election, like in the days before the election, and it causing political
fallout that they may not be able to recover from. So the Biden administration
appears to be stepping in early to try to alleviate the price at the pump.
We'll have to wait and see how successful he is or was by the time
y'all watch this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}