---
title: Let's talk about Biden's pardons and student debt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7Eu9_nFXbn4) |
| Published | 2022/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Biden's blanket pardon for simple possession of cannabis at the federal level.
- Clarifies that Biden's authority is limited to federal level pardons, not state level.
- Notes that thousands of people are being pardoned, with nobody being let out because the shift in prosecution has already started.
- Mentions the potential next steps after pardoning, including descheduling cannabis.
- Suggests that Biden's administration may be working out the details before more significant actions.
- Points out that prior to the big announcement on student debt forgiveness, billions of dollars were already forgiven.
- Addresses the legal paperwork trail that may still affect individuals despite receiving a pardon.
- Emphasizes that while pardons are not a clean slate, they represent a step forward in the process.
- Expresses the belief that there is more to come regarding Biden's actions.
- Concludes by hinting at further developments to watch out for.

### Quotes

- "He did in his executive orders kind of encourage them to do so, but he can't make them pardon."
- "That eternal question. Why aren't people currently in custody for this? Because the shift already started."
- "There's a little bit of a difference there in how a prospective employer might look at it."
- "I think there's a lot more that's going to be coming, but we have to wait and see."
- "Just understand you're going to see this material again."

### Oneliner

Beau explains Biden's federal level cannabis pardons, hints at more actions to come, and sheds light on student debt forgiveness.

### Audience

Reform advocates

### On-the-ground actions from transcript

- Monitor and advocate for further actions from the Biden administration (implied)
- Stay informed about developments in pardons and student debt forgiveness (implied)

### Whats missing in summary

Detailed insights on the implications and potential outcomes of Biden's pardons and student debt forgiveness.

### Tags

#Biden #Pardons #Cannabis #StudentDebt #ExecutiveOrders


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about Biden's pardons,
what we can kind of guess is going to happen next,
and what student debt can teach us about them.
I wasn't actually going to do a video on this yet,
and you'll find out why here in a little bit,
but a whole bunch of questions have come in,
so we're just going to kind of run through some of the questions
and talk about what this does and what it doesn't do.
Okay, so if you missed the big news here,
the Biden administration issued a blanket pardon
for those who were convicted of simple possession of cannabis
at the federal level, and that's the first question.
Why only the federal level?
Because that's all the power he has.
He can't do anything at the state level.
He has no authority over that.
That's up to governors.
He did in his executive orders kind of encourage them to do so,
but he can't make them pardon.
There are some people saying they're not really a pardon
because nobody's getting out.
No, thousands of people are being pardoned,
and that is thousands of people at the federal level,
and my understanding is that this includes D.C. as well,
and there's going to be a whole bunch more there.
I would imagine.
I would think that there might even be more there,
but that's gut feeling.
I don't have numbers on that.
So you're talking about thousands of people
who are being pardoned, but nobody's being let out.
The reason nobody's being let out
is because there's nobody in there.
Why?
That eternal question.
Why aren't people currently in custody for this?
Because the shift already started.
They've stopped prosecuting this.
That's step one.
Pardoning those for simple possession,
well, that's step two.
The next step would be descheduling it,
taking it off of the list of substances you can't have.
Biden can't order it descheduled directly,
but he can order a review of it,
and hypothetically speaking,
if he was to order that review at the same time
he pardoned a bunch of people,
that would send a pretty strong signal,
and that's exactly what he did.
The descheduling, I don't want to say it's in the works
because he can't order that, but he can order the review.
The review that would lead to descheduling has been ordered.
So that is step three.
I'm fairly certain there's more to come, and this is why.
Everybody knows about Biden's big student debt thing,
$10,000 or $20,000 forgiven, right?
Go to Google and type in Biden forgives student debt,
but then go to the tools.
You have to do this on the desktop site.
Go to tools and go down to where it says anytime.
Click on that and change it to custom range
and set the end date for August 1st.
You're going to find out that prior to that big announcement,
he forgave billions of dollars in student debt.
It just didn't make huge headlines.
I have a feeling this is the same process.
He, for whatever reason,
this administration does a lot of stuff piecemeal.
Now, my guess, and this is just a guess,
is that they're working out the bugs
before they do bigger stuff, and that may be the case here.
There are other questions about the pardoning.
There's still a legal paperwork trail.
Well, yeah, that's how pardons work.
There is still a paperwork trail,
but let me just ask you this.
If you're walking into a job interview,
would you rather say,
yes, I have this charge and I was pardoned,
or yes, I have this charge?
There's a little bit of a difference there
in how a prospective employer might look at it.
People may actually still end up being barred
from certain things, though.
It's not a clean slate, but it's a step,
and that's how I perceive this,
to the point where I really wasn't going
to do a video about it,
because I don't think that this story is done yet.
I think there's a lot more that's going to be coming,
but we have to wait and see.
But that catches you up to now.
Just understand you're going to see this material again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}