---
title: Let's talk about Biden's new drone policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WRkGmtKRE1c) |
| Published | 2022/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration's new policy on drones formalizes existing practices.
- Policy specifically addresses the use of drones outside areas of active hostilities.
- White House now makes the call on drone strikes, aiming to limit civilian casualties.
- Policy extends to in-person covert operations like raids.
- Public disclosure suggests White House intent to limit drone and covert operation use.
- Previous lax rules on drone strikes were not effective.
- Skepticism remains on whether the new policy will be effectively implemented.
- There is a history of attempts to clean up drone strike practices.
- The implementation of the new policy needs to be closely monitored.
- The policy may make it easier to conduct operations with low risk to U.S. personnel.

### Quotes

- "The White House determines individuals, groups, and countries that drones can be used in or against."
- "There has been a concerted effort to clean this up."
- "While the policy looks good, we need to see the implementation before we start sharing."

### Oneliner

Biden administration formalizes drone policy to limit civilian casualties, but skepticism remains on effective implementation and impact on covert operations.

### Audience

Policy analysts

### On-the-ground actions from transcript

- Monitor the implementation of the new drone policy closely (implied).
- Stay informed on any updates or changes in U.S. drone strike practices (implied).

### Whats missing in summary

The full transcript provides additional context on the historical background and challenges with drone strike policies, which can offer a comprehensive understanding of the topic. 

### Tags

#BidenAdministration #DronePolicy #CivilianCasualties #CovertOperations #Implementation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the Biden administration's new policy,
because that's what the headlines are calling it.
And that's also the first thing we have to clear up.
This isn't new policy, not really.
I would say that most of this has been de facto policy since his first few months in office.
And almost all of it has been de facto policy
since like the end of his first year.
But now it's formalized.
Now it's approved.
This is not a temporary thing.
This is the new policy.
And the policy deals with the US and their use of drones
overseas outside areas of active hostilities.
So I think the only two countries that qualify for that right now are Iraq and Syria.
So this would apply everywhere else.
Okay.
So short version, what's the new policy, the White House makes the call.
During the Trump administration, the use of drones, the rules became lax.
Lower people in the chain were able to make the call.
That has reversed.
The White House determines individuals, groups, and countries that they can be used in,
or against. The goal of all of this is to limit civilian loss. The terminology
being used is that there is an almost near certainty of having none. You will
see how that works out. There has been a concerted effort to kind of clean this
up for those that have been watching the channel a long time. We went over this
and went over the numbers a lot. For the goals drone use is typically trying
to achieve, which is dealing with low-intensity conflicts and irregular
forces, it's bad to have a bunch of civilian loss. Not just for moral reasons
but for strategic ones as well.
The way they were being used was not good by any standard, moral or strategic.
It wasn't working.
There's been an effort to clean that up.
This policy kind of formalizes it.
Now how well they'll stick to it, we have to wait and see.
I'm skeptical because there have been attempts to clean this up before.
We're going to have to wait and see how it plays out.
What is interesting to note is that these same rules as far as the White House making
the call now also apply to the use of in-person means of a covert nature.
So let's just say raids.
Those are now also at the discretion of the White House.
So the overall thing here is that because they're making this public, they're not doing
this behind closed doors, we can kind of infer that the White House is wanting to limit their
use a lot. They wouldn't be putting this out there as this is the new policy. The
buck stops with us and then make mistakes. That's the theory. But this is
also a very a very hazy topic. There's a lot of fog of war stuff here. So while
While the policy looks good, this is one of those things we have to watch.
If everything that's laid out is actually how it's going to work, this is good.
But I have a lot of skepticism of this.
make it too easy to do certain things with low risk to U.S. personnel, and it's very
tempting.
So while the policy looks good, we need to see the implementation before we start sharing.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}