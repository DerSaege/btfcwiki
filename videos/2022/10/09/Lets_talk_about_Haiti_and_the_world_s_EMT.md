---
title: Let's talk about Haiti and the world's EMT....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3cwzzf20W-E) |
| Published | 2022/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Haiti is facing a troubling situation and has requested foreign troops to help restore order.
- Beau advocates for the United States to act as the world's EMT (Emergency Medical Technician) rather than the world's policeman.
- He criticizes the use of the military for peacekeeping roles, stating that the military is meant to win wars, not maintain peace.
- Beau points out that the lack of a follow-on force like an EMT force leads to extended military presence in countries like Iraq.
- He mentions Thomas Barnett as a proponent of a separate force for non-warfighting missions and suggests watching his TED Talk.
- The request for foreign troops in Haiti has been acknowledged by the State Department, who are considering it.
- Beau believes that the US should prioritize developing a separate EMT force to handle failed states rather than deploying troops.
- He stresses the importance of investing in countries like Haiti rather than solely relying on military intervention.
- Beau argues that building the capability to stabilize failed states could do more for world peace than military actions.
- He suggests that with the decline of Russia, the US needs to be prepared for potential power vacuums in other countries.

### Quotes

- "The US military is not a peacekeeping force. It's a force that wins wars."
- "We don't have the world's EMT force built."
- "The military is there to take down governments, not build them."
- "This is a capability the United States needs that could prevent war."
- "Before we start doing it, we have to actually build that force."

### Oneliner

Beau advocates for the US to prioritize building an EMT force for stabilizing failed states over deploying troops to countries like Haiti, stressing the importance of investing in nations rather than relying solely on military intervention.

### Audience

Policymakers, activists, citizens

### On-the-ground actions from transcript

- Prioritize building a separate EMT force for handling failed states (implied)
- Advocate for investing in troubled nations like Haiti rather than relying solely on military intervention (implied)

### Whats missing in summary

The full transcript provides more in-depth analysis and historical context on the role of the US military in peacekeeping missions and the need for a dedicated EMT force to stabilize failed states.

### Tags

#Haiti #USMilitary #WorldPeace #EMTForce #ForeignIntervention


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Haiti
and being the world's policeman and being the world's EMT.
We're going to do this because if you don't know,
the situation in Haiti is not going well.
And the government there has made a request.
And I'm really worried that the US is going to say yes.
OK, so the government in Haiti has requested troops,
requested foreign troops to come in and help restore order.
One of the things that I've been a big proponent of
is that the United States should become the world's EMT,
not the world's policeman.
This would fall under being the world's EMT.
The problem is we've never developed the EMT force.
We've never developed the worldwide ambulance force.
We have the military, and we try to use the military to do that.
We try to force them into that role.
That's not what the military is for.
The US military is not a peacekeeping force.
It's a force that wins wars.
And right now, somebody is going to say, no, the US doesn't win wars.
No, the US wins wars.
It loses the peace.
It doesn't have the follow-on force.
When you think about Iraq as an example,
it did not take long for the United States to win the war.
The problem is without the follow-on force, the EMT force,
to come in and help re-establish the country
and let them have their country back,
the US military just ends up staying and staying and staying and staying
and eventually leaves.
We see it over and over again.
This concept has been talked about for decades now.
There are a lot of people that have advocated for a separate force,
a force that isn't a warfighting force, to deal with stuff like this.
One of the biggest advocates is Thomas Barnett.
He has a TED Talk on YouTube that's worth watching.
Don't agree with everything in the way that he frames stuff,
but as far as getting the concept down,
there's nobody who can explain it better than him.
The United States has a habit of trying to force the military into these roles,
the US military, and that is not what they're for.
You know, and the way that Barnett has described it in the past
is trying to switch back and forth between being a soldier
and being an aid worker.
And you can't train a 19-year-old kid to do both.
Haiti has made the request.
Certainly, there will be countries that volunteer to do this.
The United States probably shouldn't be one.
The UN went to Haiti, spent years there.
It was very troubled.
It didn't go well, as evidenced by the fact
that they need troops again.
What they need is investment.
What they need is a way to process a politically bankrupt country
and bring it out on the other side under the control of the people
of that country and not a client state.
That's not what the US military is good at.
The US military is good at taking down governments, not building them.
Developing this force could come out of DOD's budget.
Developing the secondary force that's there to act as the world's EMT.
The money's already there.
It just has to be prioritized.
I'm of the firm belief that if the United States developed
that capability, the capability to help, for lack of a better word,
failed states, and get them out the other side under their own power,
that would do more for world peace.
It would do more for the political power of the United States
than the military would.
The military is there to take down governments, not build them.
This request, it went out.
State Department says they're considering it.
They've asked Americans to leave the country.
I would hope that in this instance, the US sits this one out.
Or maybe provides financial assistance, but doesn't send troops there.
We're at the point where we have to develop that force.
We have to develop that capability before we can try to use it.
We don't have the world's EMT force built.
What we have are a bunch of cops that, yeah,
they've got a little bit of a first responder training.
And we try to force them into that role.
It doesn't work.
And when it comes to the actual use of the military,
when you're talking about the use of American force through a war,
that follow-on force, that same force, the world's EMT,
is the organization that can actually stabilize the country rather than
leaving the military to do it.
It's two different skill sets.
With the apparent future decline of Russia,
there are going to be power vacuums that open up in various countries.
This is a capability the United States needs
that could prevent war, which, I mean, that seems like it should be the goal.
Right?
But before we start doing it, we have to actually build that force.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}