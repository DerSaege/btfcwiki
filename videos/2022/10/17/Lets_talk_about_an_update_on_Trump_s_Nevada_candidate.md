---
title: Let's talk about an update on Trump's Nevada candidate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JdMGIYYvXYA) |
| Published | 2022/10/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on the governor's race in Nevada involving candidate Lombardo, who is endorsed by Trump.
- Lombardo's response during the gubernatorial debate regarding whether Trump was a great president: he wouldn't use that adjective, calling Trump a sound president instead.
- The potential implications of distancing from Trump in a Republican primary but needing his support to win.
- Lombardo's campaign later released a statement declaring Trump a great president, contradicting his previous stance.
- The tight race between Lombardo and the Democratic Party's interest in exposing inconsistencies in his positions.
- Speculation that Lombardo's internal polling may show a need to distance from Trump to appeal to moderate voters.
- Concerns about Lombardo's credibility and consistency in his messaging to different voter groups.
- The importance of voters being informed about a candidate's true positions and potential political maneuvers.
- The impact of Lombardo's conflicting statements on undecided voters and independents.
- Democratic Party's likely strategy to capitalize on any missteps or inconsistencies in Lombardo's campaign.

### Quotes

- "By all measures, Donald J. Trump was a great president, and his accomplishments are some of the most impactful in American history."
- "I mean, I think that might sway me."
- "That seems like something that might cause a lot of voters to think that Lombardo is being less than forthcoming about his actual positions."

### Oneliner

Candidate's flip-flop on Trump's greatness reveals strategic political maneuvering in a tight Nevada governor's race.

### Audience

Voters

### On-the-ground actions from transcript

- Analyze candidates' statements and actions for consistency and transparency (implied)
- Stay informed about political candidates' positions and potential motivations (implied)

### Whats missing in summary

Further insights on the specific policy stances and issues at play in the Nevada governor's race. 

### Tags

#Nevada #GovernorRace #Lombardo #Trump #PoliticalStrategy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we have to provide a little update
on a story that we talked about last week.
Deals with the governor's race out there in Nevada.
I can't say it like that, I'm sorry.
We talked about it at the time
because during the gubernatorial debate,
a candidate, Lombardo, who is the Trump-endorsed candidate,
was asked if he thought Trump was a great president.
And Lombardo, when speaking to all of those moderate voters,
those undecided voters,
he said, well, I wouldn't use that adjective.
I'd say he was a sound president.
Distancing himself from Trump,
the person who by many accounts
got him through the primary.
But you can't win a Republican primary without Trump,
but you may not be able to win a general with him.
I mentioned how the former president
might have an issue with that,
given the fact that Trump is, well, pretty thin-skinned.
So later that day, the Lombardo campaign
released a statement.
By all measures, Donald J. Trump was a great president,
and his accomplishments are some of the most impactful
in American history.
Wait, I thought he was just sound.
Given the fact that this race is in a dead heat out there,
the Democratic Party is probably trying to figure out
how to get this to the public.
Because if you're talking about the course of one day
and a candidate can't even give a straight answer
about how they feel about a former president,
I wouldn't use that adjective and then use that adjective?
I mean, that seems like something
that the voters out there should know about.
That seems like something that might cause a lot of voters
to think that Lombardo is being less than forthcoming
about his actual positions.
I mean, if he can't decide between sound and great,
specifically when he says, I wouldn't use that adjective,
it seems like something that the moderate voters should know.
Because what it seems like outside looking in
is that Lombardo's internal polling
shows that he can't win if he's tied to Trump.
So he's trying to distance himself
in the eyes of the undecided voter,
in the eyes of the moderates, the independents.
Oh, I'm not one of those Republicans.
But then he puts out a statement
that those Republicans are going to see.
That, I mean, I think that might sway me.
That might give me a little bit of pause
when it comes to believing anything else
from that candidate.
And given how tight that race is,
the Democratic Party's probably looking for any edge
that it can get.
This certainly seems like one.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}