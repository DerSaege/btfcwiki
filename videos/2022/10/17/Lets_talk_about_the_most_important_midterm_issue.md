---
title: Let's talk about the most important midterm issue....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g-P72CMpsmA) |
| Published | 2022/10/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Identifies the most critical issue for the midterms, focusing on respecting election results as the number one question to ask candidates.
- Expresses disappointment that environmental policy cannot be the deciding factor due to the current political climate.
- Emphasizes the importance of candidates respecting election outcomes as a reflection of their commitment to democracy and representation.
- Raises concerns about candidates who undermine the electoral process and disregard the voice of the people.
- Argues that supporting candidates who do not respect election results means accepting rulers rather than representatives.
- Advocates for prioritizing candidates who genuinely represent the people before addressing other critical issues like environmental policy.

### Quotes

- "The number one question, the first question you have to ask, is whether or not that candidate respected the results of the last election."
- "If they didn't respect the results, especially now that we know and we've seen the footage of them talking about doing what they did before the election even happened, what does it say about them?"
- "They're not going to be your representative. They're going to be your ruler."
- "I don't see how you could vote for somebody who is flat out saying that they don't care about your vote, that your voice is irrelevant."
- "Until we have people that actually represent us and respect the results of elections, you're not going to get anywhere."

### Oneliner

Respecting election outcomes is the fundamental criterion for candidates, shaping true representation and democracy before addressing other critical issues like environmental policy.

### Audience

Voters

### On-the-ground actions from transcript

- Verify candidates' stance on respecting election results (suggested)
- Prioritize supporting candidates who value democracy and representation (exemplified)

### Whats missing in summary

Importance of choosing representatives who prioritize democracy and representation as a foundational step towards addressing other critical issues effectively.

### Tags

#Midterms #ElectionResults #RepresentativeDemocracy #CandidateCriteria #Voting


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the most important issue
for the midterms, at least what I think it is.
I've been asked this question a few times
by different people, and I've kind of dodged it, really,
because of how I view these topics in general.
But the framing of this question kind of caught my attention.
So that's what we're going to talk about.
All right, so here's the message.
I'm an independent.
I'm far more conservative than you,
but I'm not MAGA by any means.
I know you don't endorse candidates
or tell people how to vote, but I'd
like to know what you think the number one
issue is for the midterms.
I think you'll say environmental policy,
but then I thought maybe that's just what you talk about more.
What's the first position you'd look at in a candidate?
In a perfect world, this should be an easy question,
but we don't live in a perfect world.
So it's not.
When you really think about it, it's not an easy question,
because the politicians have made it hard.
Yeah, I would love if environmental policy,
specifically environmental policy related to climate
change, could be the most important issue.
Like, that's what the deciding factor was.
But sadly, it can't be.
It can't be right now, because there's something else.
The number one question, the first question you have to ask,
is whether or not that candidate respected the results
of the last election.
That's really the first question you have to ask.
I mean, some of them are still pushing that line, which
that should just automatically be a disqualifier in my eyes.
All this time, all this time, still no evidence,
but still pushing it.
What does that say about how they will make policy decisions?
They'll base it on their feelings.
But more importantly, when it comes
to respecting the results of the election,
what does it say if they didn't?
If they didn't respect the results, especially now
that we know and we've seen the footage of them talking
about doing what they did before the election even happened,
what does it say about them?
It says that they don't support a constitutional republic.
It says they don't support representative democracy,
that they don't care about your voice.
That's what it says.
They're not going to be your representative.
They're going to be your ruler.
If they didn't support the results of the last election,
if they didn't respect what the people decided,
they are not going to represent you.
They don't care about you.
They made that abundantly clear.
That has to be the first question.
I don't see how you could vote for somebody
who is flat out saying that they don't care about your vote,
that your voice is irrelevant, that they will try
to maneuver around your voice.
I don't see how you can vote for that person
and expect them to be your representative,
because they're telling you they're not going to be.
They're going to do whatever they want,
and they're going to rule you.
That's got to be the first question.
That's the first position that I think people should look at.
From there, yeah, I would say that the environment's
probably at the top.
But we can't get to that point until those
that we've entrusted to be representatives represent,
because they're not going to represent the people when
it comes to the environment.
They'll represent the big companies.
Until we have people that actually represent us
and respect the results of elections,
you're not going to get anywhere.
You won't get the country you want,
even if you are more conservative than I am,
because they're not going to do whatever it is
you think is best.
They're going to do what's best for them,
just like they did in the last election.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}