---
title: Let's talk about snow crabs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KcphFNenWZA) |
| Published | 2022/10/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Alaska canceled the snow crab season, citing the population collapse.
- The term "overfished" allows the government to take action, even if fishing wasn't the cause.
- The snow crab population decreased from 8 billion in 2018 to 1 billion in 2021.
- Factors like warming waters, disease, or migration to cooler areas could have caused the decline.
- Coverage mainly focuses on the economic impact on fishermen, not enough on climate change.
- Beau suggests tying climate change to the economic impact to stress the cost of inaction.
- Transitioning to mitigate climate change requires significant investment and preparation.
- Failure to address climate change could lead to more industries collapsing and workers becoming economic refugees.
- Beau warns about the long-term consequences of not taking action to mitigate climate change.
- The focus on money in the US may require framing the cost of inaction to spur more action.
- Generations of fishermen are losing their livelihoods due to the collapse of the snow crab population.
- The disappearance of billions of a species should be a significant concern, not just an economic one.
- Beau urges for a shift in focus towards considering the costs of inaction when discussing climate change.

### Quotes

- "This is a warning."
- "Generations of a species disappearing should factor into this in some way."
- "Have a good day."

### Oneliner

Alaska cancels snow crab season due to population collapse, raising concerns on economic impact and urging a shift towards considering the costs of inaction on climate change.

### Audience

Climate activists, policymakers, seafood industry workers

### On-the-ground actions from transcript

- Advocate for policies that address climate change impacts on seafood industries (implied)
- Support initiatives to protect and conserve marine species affected by climate change (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the economic and environmental impacts of the snow crab population collapse and serves as a reminder to prioritize action on climate change to prevent further consequences.

### Tags

#ClimateChange #EconomicImpact #SeafoodIndustry #EnvironmentalConservation #ActionNeeded


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about snow crabs and Alaska
and economic impacts and the way we do talk about things
and maybe the way we should talk about things.
So if you missed the news, Alaska
has canceled the snow crab season.
It's not happening.
The boats aren't going out.
If you look at the reporting, you're
seeing the term overfished.
That doesn't mean that that's what caused the population
to collapse.
That's a term that allows the government
to spring into action and say, oh, we're not having a season.
It's the first time they've ever done it with snow crab.
In 2018, there were 8 billion crabs.
2021, 1 billion.
It wasn't fishing that caused the population collapse.
Snow crab, as the name implies, they like it cold.
I want to say that they are most prevalent in areas that
are below 2 degrees Celsius.
They like it cold, cold.
And as most people know, the water is getting warmer.
This could have caused it.
It could be disease, or they could have just
moved to somewhere where it is cooler.
That isn't unheard of.
In fact, there have been multiple populations
since the 1980s that have moved 19 miles or more.
But as it stands, there's billions of this species gone.
The focus right now for most coverage
is on the economic impact of the fishermen.
Not really.
They mention climate change.
Probably not as much as they should.
And they should probably tie climate change
to the economic impact of the fishermen.
The economic impact.
Because when we talk about what it's going to take
to mitigate climate change, we often
talk about the cost of mitigating
and how much it's going to take, the investment it's
going to take to transition, to safeguard,
to put the pieces in place to try to slow climate change.
Maybe we should be talking about how much it's
going to cost if we don't.
If we don't mitigate.
This, it's not going to be the last time
something like this happens.
Early estimates are saying it's going to be years
before the population rebounds.
If that's the case, there's a whole bunch
of fishermen who aren't fishermen anymore.
They have to go find new jobs, new industries.
Somebody recently asked about climate refugees
within the United States.
Do economic refugees count?
Because if they do, that's starting now.
We're focused on the money and what
it's going to cost to mitigate.
Since the United States is a country that
tends to focus on money, it might be more persuasive
if we talk about how much it's going to cost
if we don't mitigate.
Because it's a lot.
It is a lot.
You have fishermen.
Families have been doing this for generations.
They may not be doing it anymore.
And this is on top of the fact that we
are talking about billions of a species disappearing,
which should factor into this in some way.
But it's the US and we care about money.
This is a warning.
This is a warning as if we haven't had just constant
warnings over the last 10 years.
This is something that might continue to get coverage.
And when we talk about it, we need
to talk about the economic impact that
occurred because we didn't act.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}