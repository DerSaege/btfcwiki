---
title: Let's talk about Trump's employee talking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HDdoUdsMSA0) |
| Published | 2022/10/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New developments in the Trump document case are discussed, with confirmation of previously believed information.
- A staffer reportedly spoke to the FBI as a cooperating witness, revealing that Trump instructed moving boxes after receiving a subpoena.
- The potential obstruction charge hinted by the feds may relate to not returning items as per the subpoena.
- Trump aims to frame the issue as a document storage problem rather than a national defense information concern.
- Legal jeopardy for Trump increases as a lawyer's involvement complicates the situation.
- Trump appears more focused on political aspects rather than legal implications.
- The staffer's cooperation with the FBI could lead to significant revelations and developments.
- Trump's attempt to downplay the issue as document storage may not be beneficial legally.
- The statements made by Trump are unlikely to assist him in a legal context.
- The situation surrounding Trump's document case continues to evolve, indicating further troubles ahead.

### Quotes

- "There is no crime having to do with the storage of documents at Mar-a-Lago."
- "That's kind of a big no-no if you're going to move stuff and not return it."
- "The whole situation is raising the legal jeopardy for Trump."
- "If there is a staffer who was trusted enough by Trump to have been asked to move the documents after the subpoena was served, that staffer probably has a whole lot of information."
- "The statements that are being made, they're not going to help him legally."

### Oneliner

New developments in the Trump document case reveal potential legal jeopardy for Trump as he downplays the issue by framing it as a document storage problem, while a cooperating witness sheds light on incriminating actions.

### Audience

Political analysts, investigators

### On-the-ground actions from transcript

- Contact legal experts for analysis on the potential legal implications faced by Trump (implied)
- Stay informed about the ongoing developments in the Trump document case (implied)

### Whats missing in summary

Detailed analysis of the legal implications and potential consequences for Trump.

### Tags

#Trump #DocumentCase #LegalJeopardy #ObstructionCharge #CooperatingWitness


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the new developments when it comes to the Trump document case
and what's going on with it.
We'll talk about the new developments and him confirming something that we definitely believed.
Okay, so the big news, the big news is that a staffer down there has reportedly been talking to the FBI
and is a cooperating witness.
According to reporting, this staffer has told the feds that Trump himself told them to move boxes
out of the storage and up to the residence after Trump got the subpoena.
Now, when all of this started, the feds said something along the lines of,
and they may be looking at an obstruction charge as well.
This probably has something to do with that.
That's kind of a big no-no if you're going to move stuff and not return it.
If that's what happened, you know, you have a subpoena saying you have to get stuff back,
and rather than do that, you just move it.
That seems to be what they're kind of hinting at here.
That's, yeah, that's a whole new thing.
So you have that going on, but one of the other kind of new developments
is Trump confirming something we've been saying here.
He's going to try to frame it as a document storage issue,
rather than something dealing with, you know, the Espionage Act,
national defense information, so on and so forth.
He's going to try to make it all about document storage.
This is what he said.
There is no crime having to do with the storage of documents at Mar-a-Lago.
Only in the minds of, and he just rambles on.
But the whole framing now is about storing the documents.
That's how he's trying to paint it politically.
I'm not sure that that's a great idea legally,
because, I mean, gathering national defense information,
storing it, is kind of the problem.
That kind of seems to be one of the allegations he's looking at.
I don't know that this framing is really going to help him,
except in the minds of his base.
The new developments are pretty substantial.
If there is a staffer who was trusted enough by Trump
to have been asked to move the documents after the subpoena was served,
that staffer probably has a whole lot of information.
And if they are cooperating witness with the FBI, as the reporting suggests,
that's probably going to open a lot of doors into various developments down there.
You also have the situation that developed with the lawyers,
where one lawyer signed a document and then kind of said,
well, you need to talk to these other two.
The whole situation is raising the legal jeopardy for Trump.
And it at least appears from the outside that he is more focused
on the political aspects of it than he is the legal aspects.
The statements that are being made, they're not going to help him legally.
So it's another development that we'll probably see this material again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}