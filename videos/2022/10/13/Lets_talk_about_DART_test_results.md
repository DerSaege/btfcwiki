---
title: Let's talk about DART test results....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6m2qfGX7vgA) |
| Published | 2022/10/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA's Double Asteroid Redirection Test (DART) successfully altered the orbit of an asteroid by crashing something into it.
- The test aimed to shorten the asteroid's orbit by 10 minutes, with a previous orbit of 11 hours and 55 minutes.
- After the test, the asteroid's new orbit is 11 hours and 23 minutes, shortening it by 32 minutes.
- This proof of concept shows NASA's capability to defend Earth from asteroids.
- Success in using this technology could justify NASA's budget indefinitely.
- Hollywood producers may need new plot lines as asteroid defense was a common theme.
- Beau suggests using climate change as a disaster flick theme to raise awareness.
- Asteroid collisions have occurred in the past with no defense until now.
- This NASA project eliminates worries about asteroid impacts on Earth.
- The successful test showcases the potential world-saving impact of NASA's work.

### Quotes

- "This is literally world saving."
- "They're going to have to come up with something new."
- "You no longer have to worry about that."

### Oneliner

NASA successfully alters asteroid orbit, potentially justifying its budget forever, leaving Hollywood producers scrambling for new plot lines.

### Audience

Space enthusiasts, environmentalists

### On-the-ground actions from transcript

- Support NASA's space exploration and defense projects (implied)
- Raise awareness about the importance of asteroid defense and space research (implied)

### Whats missing in summary

The full transcript provides more detailed information on NASA's Double Asteroid Redirection Test and its potential implications for Earth's defense against asteroids.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about NASA
and how NASA just caused a bunch of Hollywood producers
to have to come up with new plot lines.
For a while on the channel, we've
been covering a NASA project called the Double Asteroid
Redirection Test, DART.
This was a proof of concept experiment.
And the idea was to see if it was possible to redirect
the path of an asteroid by basically just crashing
something into it.
The test would have been considered a success
if it altered the orbit of the asteroid by 10 minutes.
So it's one asteroid orbiting another.
They crash something into it.
It's a success if it shortens the orbit by 10 minutes.
The old orbit was 11 hours and 55 minutes.
The new orbit after the test is 11 hours and 23 minutes,
a difference of 32 minutes.
There's still more information to come in.
But based on that, this is a success, proof of concept.
This would work.
So NASA has developed, at least in concept,
a method of defending the Earth from an asteroid.
There are a lot of people who talk about the money
that NASA gets and how it's a giant waste.
And I have a bunch of videos explaining, no, you're wrong.
Just the spinoffs are worth NASA's budget.
But this is literally world saving.
This is something that could stop extinction.
If this technology, this concept gets used once,
it justifies all of NASA's budget forever.
Now, I know there are going to be people in Hollywood
that are kind of upset because this is a go-to for a summer
blockbuster.
But they're going to have to come up with something new.
I don't know, maybe climate change.
Use that as a disaster flick for a change,
kind of get people ready for that.
I know there probably aren't a whole lot of people
who spend their time worried about an asteroid colliding
with Earth and taking everything out.
It's one of those things that exists in the background,
right?
And on some level, yeah, asteroids
have hit the Earth in the past.
They're going to in the future.
And we really didn't have any defense against it until now.
So you no longer have to worry about that.
One less thing, which is always nice.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}