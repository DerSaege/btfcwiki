---
title: Let's talk about the problem with coal and advice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e5jYkO1EHPc) |
| Published | 2022/10/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the problem with coal and answering questions raised.
- Expressing the importance of understanding the causes advocated for.
- Providing information on thermal coal being the dirtiest energy source in the U.S.
- Explaining the implications of advocating for coal and supporting workers.
- Clarifying the effects of a strike failure on coal mines.
- Differentiating between thermal and metallurgical coal and their uses.
- Encouraging support for striking workers through a live stream.

### Quotes

- "This is followed by five paragraphs of facts and figures about how dirty coal is as an energy source."
- "I'm pretty sure that most schools of thought, when it comes to the left, support the workers."
- "If the strike fails, the management, the boss, hires new employees at a lower wage with less benefits and worse working conditions."
- "Understand the stuff that is mined at this mine is what makes the type of energy you want."
- "Maybe tune in."

### Oneliner

Beau addresses the problem with coal, advocates for understanding the causes supported, and clarifies misconceptions about coal mining and energy sources.

### Audience

Leftist activists

### On-the-ground actions from transcript

- Tune in to the upcoming live stream to support striking workers (implied).

### Whats missing in summary

Importance of supporting workers in energy production. 

### Tags

#Coal #Energy #LeftistActivism #SupportWorkers #LiveStream


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about coal
and the problem with coal.
And we're going to answer some questions
that have been raised.
And I think it's important to answer these now
because sometime in the next 48 hours,
we're going to have a live stream
and it will help out some coal miners.
And even though there is a definite specific tone
to the message I'm going to read,
other people may have similar questions
and I want to answer them before the live stream.
Okay, so let's just jump right in here.
Have you ever given any thought
to how the causes you advocate for
could be detrimental to the left?
I see you plan on holding a fundraiser for a coal mine.
This is the problem with redneck leftists on YouTube.
People see a charming accent and a folksy wit,
but ignore your total lack of research and understanding.
Maybe go to college.
But after you leave the dirt road,
you might be one of those college leftists
you told people to ignore recently.
Before you become part of the problem, do some research.
For your edification,
thermal coal is the dirtiest energy source
in the United States.
This is followed by five paragraphs of facts and figures
about how dirty coal is as an energy source.
And as far as I could tell, I didn't fact check at all,
but it all looks right.
And then there's one paragraph talking about better sources,
wind, solar, tidal, like a whole bunch of other stuff.
Okay.
If you want to be part of the left,
do some research and think strategically.
For whatever reason, you have a following.
Have you thought that it might be better
for the strike to fail and cause the mine to close?
You put out videos advocating for wind and solar,
and that's good,
but maybe pull your head out of your box, not their term,
and stop advocating for coal.
I'm so tired of ignorant, fake, reared leftists on YouTube.
Okay. So I didn't tell people to ignore college leftists.
I told them to ignore college leftists
who are offering advice about doing,
like, on-the-ground activities,
because sometimes they don't have a clear picture
of what's going on, and they offer bad advice.
The reason I say that is about to become abundantly clear.
As far as wanting to be part of the left,
I know I just live on a dirt road and everything,
but I'm pretty sure that most schools of thought,
when it comes to the left, support the workers.
That's kind of a key element of it.
I'm not aware of any school of thought on the left
that would suggest abandoning them.
This part about, have you ever thought it might be better
for the strike to fail and therefore cause the mine
to close or whatever, that's literally not how that works.
That's not what happens.
If the strike fails, the management, the boss,
hires new employees at a lower wage with less benefits
and worse working conditions.
Therefore, making an already profitable mine more profitable,
therefore giving the owner class more money to use
for lobbying to keep coal going longer.
That's what would happen.
If a strike fails, the business does not close.
That's not a thing.
And then to the most important part.
At the beginning of your lecture on the strike,
at the beginning of your lecture on how dirty coal is,
you used the term thermal coal.
The need to specify thermal coal indicates the presence
of other kinds of coal, one of which would be metallurgical.
Metallurgical coal is used to make coke,
which is used to make steel, which is used to make
all of the alternative energy sources that you want to use,
except for solar.
I don't think they use it much.
Given the choice between thermal and metallurgical,
which one do you think they mine at Warrior Met?
This is why I give people that advice.
From a distance, if you're not involved
in something directly, you may not have a clear picture
of it.
You may have a view of an industry
and you may not really understand how it works.
That's why I offer that advice.
Understand the stuff that is mined at this mine
is what makes the type of energy you want.
Do you know how much steel is in one of those wind turbines?
So, here soon, next 48 hours or so,
we're gonna have a live stream
and it will benefit some striking workers.
And yeah, so if you want to, I don't know,
make sure that the people who are providing
the energy sources you would like to see,
if you want to make sure that they have decent wages,
a good standard of living, all of that stuff,
maybe tune in.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}