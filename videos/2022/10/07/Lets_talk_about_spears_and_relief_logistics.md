---
date: 2024-03-06 07:44:18.571000+00:00
dateCreated: 2023-02-16 05:26:56.334000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about spears and relief logistics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pU2KJij5lmM) |
| Published | 2022/10/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about informal logistics of relief efforts after hurricanes Michael and Ian
- Compares his role in both situations - more hands-on after Michael due to local contacts, more supply-focused after Ian
- Uses a spear analogy to explain different roles in relief work: tip (active combat), shaft (deploy but not daily combat), and staff (logistics, support)
- Emphasizes the importance of the staff/logistics role in relief work, even if it's less visible
- Explains how he distributed supplies to local contacts after Ian, who then directed them to areas of need
- Goal of informal relief is to ease burden on official first responders
- Viewers of the channel indirectly supported relief efforts by enabling Beau's work
- Mutual assistance networks can form from these experiences, strengthening community resilience
- Key steps: know what's needed, have a distribution point, and a few local contacts to start the process
- Initial response is most important, then local volunteers join in

### Quotes


- "The goal of doing this type of relief is to ease the burden of the actual lights and sirens first responders."
- "You provide the tools for self-rescue, and the locals, the tip of the spear, will do it."
- "You get in where you're most used, where you can be of most use. That's the role you need to fill."

### Oneliner

Beau compares his hands-on role after hurricane Michael to his supply-focused support after Ian, emphasizing the importance of logistics and local networks in informal relief work to ease the burden on first responders and strengthen community resilience.

### Audience

Relief volunteers, Community organizers

### On-the-ground actions from transcript

- Bring supplies to local contacts who can direct them to areas of need (exemplified)
- Set up a makeshift supply depot in impacted neighborhoods (exemplified)
- Distribute tools like chainsaws and cleanup kits to enable self-rescue (exemplified)

### Whats missing in summary

Personal anecdotes and details of Beau's specific actions after hurricanes Michael and Ian

### Tags

#HurricaneRelief #CommunityResilience #MutualAid #InformalLogistics #DisasterResponse

## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk a little bit about informal logistics.
We're going to compare what I did after Michael to what I did after Ian.
And we're going to talk about the Spear analogy again and how it relates to relief efforts.
Because a question that was asked kind of opened the door.
And if you have done this type of work before, the answer to the question seems self-evident,
but that's only because you've done it before.
If you've never been involved in something like this, you don't know the answer to it.
So what was the question?
Basically, hey, you're an upper middle class white guy from North Florida.
How could you possibly have connections in the neighborhoods that need the help the most
down in the impact zone in Ian?
It's a good question.
Because the answer is, I don't.
Absolutely not.
My contacts down there are almost exclusively retired military officers or ex-contractors.
These are people who weren't necessarily in a bad way themselves because they have decent
retirements or the ex-contractor, you know, sitting on six or seven figures in a bank.
They didn't need cash.
But they all also now live in like pre-planned landscape neighborhoods.
They may not have the tools they need to help.
Okay, so after Ian, I ran supplies and tools down.
After Michael, it was very different.
If you weren't watching the channel back then, I was down there daily for weeks.
I was cutting people out of trailers, delivering baby food to government housing, clearing
roads just constant for weeks.
But didn't do that with Ian.
Here's an analogy dealing with a spear.
You might have heard the phrase tip of the spear if you've been around military people.
When you think of a soldier, odds are you are thinking of this.
You're thinking of people who pull triggers, people who are actively involved in combat.
The thing is, that's a really small percentage of soldiers in general.
That's the tip of the spear.
This part of the spear, these are people who go downrange.
They deploy, but they're not necessarily involved in combat operations every day.
These people way back here on the staff, they're people who do logistics.
They fix trucks.
They cook food.
They listen to radio intercepts.
They handle your records, stuff like that.
This is what wins wars.
This is what people think of.
But without this, there's no forward movement.
When people start doing relief work or activism in general, everybody wants to be up here.
That may not be the best spot for you.
After Michael, it was for the very reason that this person asked the question.
I had contacts in these neighborhoods.
I knew my way around.
I could go down there and cut people out of houses.
I had that skill set.
With Ian, not so much.
But what I could do is bring them supplies.
After Michael, up here, I distributed hundreds of these orange Home Depot buckets that were
packed with cleanup tools.
I have no idea where they came from, other than somewhere in Tennessee.
That much I got when I picked it up at the supply depot.
But I have no clue who packed them and sent them down.
Somewhere, hopefully, in the impact zone down there with Ian, right now, somebody's running
a chainsaw.
And they have no idea where it came from.
You provide the tools for self-rescue, and the locals, the tip of the spear, will do
it.
The people that we gave supplies to, yeah.
They are people who, well, not all of them, but most of them are people who are very much
okay, more or less, themselves.
But they're also people who had other contacts.
Their network moved further along, so they would know where to direct those supplies
and those resources to be best used.
It's not a mean question or a rude question, I don't remember how you phrased it.
It's an important one, because it sets up the logistical network.
Everybody wants to be tip of the spear.
The reality is you get in where you're most used, where you can be of most use.
That's the role you need to fill.
I could drive back and forth multiple times and bring supplies.
I could put those supplies in the hands of people who could direct them to be better
used than I could driving around and just asking who needs help.
The goal of doing this type of relief is to ease the burden of the actual lights and sirens
first responders.
After something like this, they have a lot on their plate.
They have to get people who are at risk of losing their life.
They have to triage.
Most of their resources are going to be devoted to stuff like that.
As far as comfort, clean up, getting people out of houses.
If you're trapped in your house and you're safe, you're not exactly a high priority.
If you've had something that happened here, trees fall around a trailer.
They're fine inside, but they can't get out.
That's not a huge priority for the fire department.
Not immediately anyway.
Not when they have people who need to get to hospitals.
So you're trying to relieve that burden if you're at the tip of the spear.
If you are here, you're trying to make sure the tip of the spear has the supplies they
need.
If you're back here, you're trying to make sure whoever's bringing it in has what they
need.
And whether y'all realize it or not, as far as Ian, y'all are here.
And actually after Michael, y'all were.
Because the support that y'all provide through this channel allowed me to go and do it, which
then allowed the people actually doing the cutting, actually doing the distributing to
do it.
They have no idea that you even exist.
But that's generally how it works.
And that's how it's most effective.
It's informal.
The idea of mutual assistance like this is to hopefully form, I don't want to say more
permanent networks, but form groups that can assist each other when the need arises.
I would imagine that the person who set up that makeshift supply depot in the neighborhood
that we drop stuff off to, I would imagine that their social circle after this, it may
not be a formal thing, they may not view themselves as a network, but the next time something
happens they're going to know who to contact and they're going to know how to do it.
Because they got baptism by fire here.
In that, that makes the community overall stronger and better.
And you want to facilitate that.
So the question is, you know, how could it do any good?
It's not about the direct good that you're doing.
You're getting the resources in place so somebody else can take that next step.
Just like here, there were people who got the resources in place so we could take the
next step here.
And that's how it functions.
People tend to overthink the logistics of it.
You know what's needed.
You know what people are going to need.
You need a place for it to be distributed from and a few local contacts that you know
that will start it, that will start the distribution process.
After that, once things start getting rolling, there's more and more volunteers, there's
more people from the local community who show up to help.
You just want to kind of kickstart that initial response and make sure that the first people
going out can do what they need to in those first few days when it's most important.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}