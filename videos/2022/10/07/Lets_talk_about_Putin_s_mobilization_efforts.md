---
title: Let's talk about Putin's mobilization efforts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pAsFAeC9oro) |
| Published | 2022/10/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the mobilization in Russia and addresses misleading headlines in the United States.
- Mentions the headline about 200,000 Russian soldiers being drafted, clarifying that 200,000 troops have been conscripted, but it's shy of their goal.
- Points out that the most critical number is 335,000, the Russians who fled the country to avoid conscription.
- Compares the number of Russians fleeing to avoid conscription to those who fled the United States during the Vietnam draft.
- Indicates the lack of desire among Russian people to fight in the war, with more fleeing than volunteering.
- Notes that even if Russia hits its conscription goal, they still won't have enough troops.
- Mentions that half of the troops sent from one region were unfit for service, leading to the firing of the person in charge of conscription.
- Expresses a pessimistic view on Russia's mobilization and the fate of troops entering Ukraine.
- Describes the situation as "pure waste" due to the lack of substantial troop numbers and quality.
- Speculates on Putin's next moves and the unlikelihood of success in Ukraine.

### Quotes

- "More people have fled the country than have been conscripted."
- "The cause is lost. They don't have the troops."
- "Putin may push mobilization even further in hopes of just throwing enough people over there."
- "At some point, somebody in Russia is going to have to make the call and explain to Putin what his options are."
- "It doesn't look like any of the things that would allow him to remain are going to happen."

### Oneliner

Beau clarifies misleading headlines on Russian mobilization, pointing out the lack of troops due to mass avoidance of conscription, painting a grim picture of Putin's imperialist endeavor in Ukraine.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Inform others about the realities of Russian mobilization (suggested)
- Support efforts to provide aid and resources for those fleeing conscription (exemplified)

### Whats missing in summary

Insights on the implications of Putin's actions and potential consequences for Russia.

### Tags

#Russia #Mobilization #Conscription #Putin #War


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the mobilization in Russia, how it's going,
what some of the headlines are here in the United States,
and we're going to add a little bit of context to those headlines because some of them are
accurate but also misleading at the same time. There appears to be an effort to
drum up a little bit of fear, so I think context is important here.
You've probably seen headlines that say something to the effect of
200,000 Russian soldiers drafted, dun-dun-dun. Well, it may not have the dun-dun-dun part,
but the idea is that number, 200,000 troops. They've been conscripted.
That number is based off of what the Russian military is willing to tell the public.
I would point out that they are trying to paint a rosy picture of the mobilization effort.
I would also point out that 200,000 is shy of their goal,
but that's not even the most important number. The most important number is 335,000.
That's the number of Russians that have fled the country to avoid conscription.
More people have fled the country than have been conscripted.
That's not a good sign for the Russian mobilization effort.
It's not a good sign for Putin when it comes to maintaining domestic tranquility.
For comparison and context to that 335,000 number,
the high end of the estimates for the number of people that fled the United States
to avoid the draft during Vietnam, the whole draft, not just a very short period of time,
but the whole draft is 100,000. That's the high end. The low end is 60,000.
The Russian people do not want to fight in this war.
They're fleeing to avoid conscription. They're certainly not going to volunteer.
Russia needs troops and they don't have them.
When we were talking about this in the very beginning,
we talked about the numbers of troops it takes to take and hold land.
They're nowhere near it. They don't have the numbers.
Even if they hit their goal with conscription, they don't have the numbers.
And given the Russian populace's response to conscription, they're not going to get them.
It is also worth noting that in at least one of the regions,
the person over the conscription was fired
because half of the troops that they sent were unfit for service.
It's worth remembering the conscripted troops that we've seen.
Those were deemed fit for service. Those that were unfit for service are worse than that.
I did not have an optimistic view of Russia's mobilization and how it would go
and what would happen to those troops upon entering Ukraine.
My view has gotten even darker.
This is pure waste. This is pure waste.
The troops that they're getting, they're not getting them in substantial number.
And basically at this point, it looks like the people that they're getting
are the people that either lacked the financial means to get out or weren't smart enough to.
The cause is lost. They don't have the troops.
Even with this mobilization, they will not have the necessary number of troops
to hold the land that they claim is now part of their country.
This imperialist endeavor is not going well for them.
Putin has to find an off-ramp, but at this point, I don't know what that would be.
I don't know what he could accept and offer to the Russian people as a victory
that would allow him to remain in power.
It doesn't look like any of the things that would allow him to remain are going to happen.
So that is something to bear in mind.
Putin may push mobilization even further in hopes of just throwing enough people over there
to make a difference.
But given the amount of training they're getting, the quality of the troop to start with,
and the massive amount of resistance from the populace, it seems very unlikely.
At some point, somebody in Russia is going to have to make the call and explain to Putin
what his options are and that it is unlikely that he sees victory in Ukraine.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}