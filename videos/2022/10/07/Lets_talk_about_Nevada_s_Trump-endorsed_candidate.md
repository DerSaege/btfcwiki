---
title: Let's talk about Nevada's Trump-endorsed candidate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ApyZAJ3hSDQ) |
| Published | 2022/10/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Nevada's governor candidate Joe Lombardo, who is endorsed by Trump but trying to distance himself from the former president.
- Lombardo was asked if he thought Trump was a great president during a gubernatorial debate, to which he responded by saying Trump was a "sound" president.
- Lombardo seems to understand the delicate balance between needing Trump's support in a Republican primary and potentially alienating general election voters.
- There is speculation on how Trump will react to one of his chosen candidates not endorsing him as a great president.
- The Democratic Party in Nevada might strategically refer to Lombardo as the "Trump-endorsed candidate" to exploit the distance he is trying to create from Trump.
- Lombardo's ability to separate himself from Trump, who endorsed him, could be pivotal in the tight race.
- Trump's endorsement has become a burden for some candidates, as they navigate between appealing to the general public and Trump's loyal supporters.
- Suggestions are made for the Democratic Party in Nevada to adopt a strategy that capitalizes on this delicate political dynamic.

### Quotes

- "You can't win a Republican primary without Trump, but you may not be able to win a general with him."
- "Trump has become, in a lot of ways, political dead weight."
- "Lombardo's ability to separate himself from the person who endorsed him is probably going to be a deciding factor."

### Oneliner

Nevada's governor candidate endorsed by Trump tries to distance himself, navigating between Trump's base and general voters, while Democrats strategize around this political tightrope.

### Audience

Political observers, Nevada voters

### On-the-ground actions from transcript

- Strategize party messaging to exploit the distance between the candidate and the endorser (suggested)
- Support political candidates who prioritize the interests of the general populace (implied)

### Whats missing in summary

Analysis on the potential impact of Trump's endorsement on the Nevada gubernatorial race. 

### Tags

#Nevada #GovernorCandidate #TrumpEndorsement #PoliticalStrategy #DemocraticParty


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Nevada
and a governor's candidate there, Joe Lombardo, and Trump
and how the Trump-endorsed candidate, Lombardo,
is trying to distance themselves from Trump.
Recently, in the gubernatorial debate there,
Lombardo was asked if he thought Trump was a great president
and he said, I wouldn't use that adjective.
Instead, he opted for the term sound.
He was a sound president.
I'm not sure how Trump is going to take that,
given the fact that Trump's going there to campaign for him
in just a few days.
But my guess is that Lombardo has realized
what we have been talking about on this channel.
You can't win a Republican primary without Trump,
but you may not be able to win a general with him.
So he's trying to put some distance between himself and Trump
while not angering the MAGA faithful.
That's a tall order.
This is a political dynamic that can only exist out there.
I am very curious to see how Trump responds
to one of his hand-picked candidates
saying that he's not great.
If I'm not mistaken, they are referring to a few candidates out there
as the Trump ticket, Lombardo being one of them.
If the Democratic Party out there was smart,
they would probably only refer to Lombardo at this point
as the Trump-endorsed candidate
because it seems obvious that Lombardo's internal polling
is saying that he needs to distance himself from Trump.
So referring to him as the Trump-endorsed candidate
while Lombardo is out there saying that Trump isn't great
is probably a situation the candidate would like to avoid.
This is actually a relatively tight race out there
and Lombardo's ability to separate himself from the person who endorsed him
and got him through the primary
is probably going to be a deciding factor.
Trump has become, in a lot of ways, political dead weight
and his candidates are trying to walk this fine line
between being electable with the general populace
and not angering the more radical Trump supporters.
The Democratic Party would probably do well to adopt a strategy
that didn't allow that line to be walked.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}