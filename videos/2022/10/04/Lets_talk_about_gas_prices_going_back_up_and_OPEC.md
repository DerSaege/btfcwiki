---
title: Let's talk about gas prices going back up and OPEC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NxmHW_bTChY) |
| Published | 2022/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- OPEC Plus will meet in Vienna to announce a production cut, potentially between 500,000 and 1.5 million barrels per day.
- The aim is to raise oil prices to $100 to $105 per barrel within the next 90 to 180 days.
- This increase in oil prices will lead to a corresponding rise in gas prices.
- Calls for increased oil drilling as a solution are shortsighted since the global oil market allows for adjustments by OPEC.
- Transitioning to green and clean energy is imperative for economic, national security, and environmental reasons.
- The United States and many other countries heavily rely on oil consumption, necessitating a shift towards renewable energy.
- Delaying the transition to clean energy leads to increased costs and environmental damage.
- Rising gas prices will likely contribute to inflation, affecting the cost of transportation and all other goods.
- Oil companies maximize profits by creating artificial scarcity in the market, a common practice in the industry.
- The capitalist nature of the oil industry drives companies to charge based on market demand.

### Quotes

- "Green energy, clean energy, is secure energy."
- "The longer we wait, it's just the more money we spend. It's the more damage we do."
- "Oil is a capitalist endeavor. They will charge whatever the market will bear."
- "So not great news, expected to be coming out of Vienna tomorrow, but it's also not surprising news."
- "It's going to have to occur."

### Oneliner

OPEC's potential oil production cut in Vienna may spike gas prices, urging a necessary transition to green energy for economic, security, and environmental reasons.

### Audience

Energy consumers

### On-the-ground actions from transcript

- Advocate for and support the transition to green and clean energy (implied).
- Reduce personal reliance on fossil fuels by exploring alternative transportation methods (implied).

### Whats missing in summary

Importance of supporting policies and initiatives that accelerate the shift towards renewable energy. 

### Tags

#GasPrices #OPEC #GreenEnergy #Transition #OilIndustry


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the likelihood of gas prices going back up over the next
three to six months.
We're going to talk about this because OPEC Plus will be meeting in Vienna tomorrow face-to-face.
The expectation is that they will announce a cut in production somewhere between one
and a half million and 500,000 barrels per day.
Most people seem to believe it'll be around a million that gets announced, but it'll actually
be less than that in reality.
These numbers don't really matter to us.
What matters is that the expectation is they want to get oil back up to around $100 to
$105 per barrel, and that these increases will occur sometime between the next 90 and
180 days.
So we can expect this to occur, which means gas prices will follow.
They will go up.
That also means that we will have a whole bunch of people screaming, drill, baby, drill,
as if that is the solution.
Oil is a global market, so if the United States starts putting a million more barrels of oil
per day out, OPEC can just cut again.
Green energy, clean energy, is secure energy.
We have to go that way.
It's an economic priority.
It's a national security priority.
It's an environmental priority.
Under normal circumstances, any of these three things would be enough to trigger a transition,
but the United States is very ingrained in oil consumption.
A lot of countries are.
This transition is going to have to occur.
It has to happen.
The longer we wait, it's just the more money we spend.
It's the more damage we do.
It's going to have to occur.
So this will probably not help with inflation in the United States, because if gas prices
go up, cost of transportation also go up, which means the prices of everything will
go up along with it.
So not great news, expected to be coming out of Vienna tomorrow, but it's also not surprising
news.
Oil is a capitalist endeavor.
They will charge whatever the market will bear.
They will try to get the most for their product that they can.
And the way they can do that right now is creating an artificial scarcity, which is
something that happens in other industries, but when it comes to oil, it's really out
in the open.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}