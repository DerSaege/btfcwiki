---
title: Let's talk about Trump suing CNN by the numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e45fiyXNRvo) |
| Published | 2022/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Donald Trump filed a lawsuit against CNN seeking $425 million in damages, alleging defamation and claiming the outlet is trying to undermine his possible 2024 run.
- Trump's lawsuit states that CNN has called him names such as racist and Russian lackey to harm his chances in the upcoming election.
- In a defamation case against a large outlet like CNN, the person suing must prove actual malice, which is a challenging standard to meet.
- Recent polling indicates that 44% of Americans believe Trump should be charged with a crime, making it unlikely for them to vote for him.
- Hypothetical match-ups show Biden winning against Trump in 2024, even within the Republican Party where only 47% support Trump as the primary choice.
- A majority of Americans, 51%, think that the allegations against Trump so far disqualify him from running for office again.
- Given the significant opposition to Trump's potential candidacy, it is unlikely that a major outlet like CNN is conspiring against him to prevent his 2024 run.
- Trump's lawsuit seems more like an attempt to gain publicity and re-enter the mainstream due to dwindling support and becoming a fringe candidate.
- Trump's dwindling appeal to independent voters and the GOP's diminishing political value of his candidacy are factors contributing to his uphill battle for the 2024 election.
- The lawsuit against CNN appears to be a strategic move by Trump to regain relevance in the political landscape.

### Quotes

- "It doesn't seem like Trump's 2024 bid is destined to really go anywhere."
- "The majority of Americans believe he should not be allowed to run, and 44% believe he should be charged."
- "This, to me, seems to be another attempt by Trump to try to grab some headlines."

### Oneliner

Former President Trump's lawsuit against CNN alleging defamation and an attempt to undermine his 2024 run faces significant challenges, with polling data indicating strong opposition to his candidacy.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze and stay informed about political developments and strategies (implied)
- Engage in critical thinking and fact-checking regarding political claims and lawsuits (implied)

### Whats missing in summary

Analysis of the potential impact of Trump's legal actions on media coverage and public perception.

### Tags

#DonaldTrump #CNN #defamation #lawsuit #politics


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to,
we're going to talk about former president Donald J. Trump's
lawsuit against CNN.
Trump has filed, is seeking $425 million in damages
due to a claim that the outlet
has basically engaged in a concerted effort
to undermine a possible 2024 run.
Suing for defamation.
It is worth noting, this is the outlet,
CNN is the outlet that Trump referred to as fake news
and an enemy of the people.
Trump's lawsuit says that they have called him things
like a racist or a Russian lackey,
and that this is aimed at undermining his chances in 2024.
Now, when you're talking about defamation,
especially against a large outlet like this,
the person suing has to demonstrate actual malice,
which is a very, very hard standard to overcome.
New York Times versus Sullivan is still the go-to on this.
So it seems unlikely from that standpoint.
But let's also look at the general idea
of what he's alleging they're attempting to do,
undermine that 2024 run.
It is worth noting that recent polling shows
44% of Americans believe he should be charged with a crime.
Almost half believe he should be charged with a crime.
It's relatively safe to say that that 44%
isn't going to vote for him.
When you look at hypothetical match-ups
between Biden and Trump in 2024, Biden wins.
Even within the Republican Party,
only 47% support him as the primary choice.
When you don't even have a majority within your own party,
it doesn't seem likely that a major outlet
is out to get you to stop you from running.
It is also worth noting that a majority of Americans,
51%, believe that just the allegations
that we know about so far and what we know about them
means that Trump shouldn't be allowed to run again,
more than half.
These aren't really numbers
that are going to be easy to overcome.
It doesn't seem like Trump's 2024 bid
is destined to really go anywhere.
The majority of Americans believe
he should not be allowed to run,
and 44% believe he should be charged.
Those are big numbers to have to overcome.
So the general tone, the general idea that CNN is doing this
because they're scared of his 2024 run,
that seems a little silly.
The legal legs of this,
when it comes to winning a defamation case like that,
that's pretty slim, too.
This, to me, seems to be another attempt by Trump
to try to grab some headlines,
try to kind of get back into the mainstream
because he is becoming more and more of a fringe candidate
that can't get those independent votes,
and therefore, I mean, politically,
as far as the GOP is concerned, is worthless.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}