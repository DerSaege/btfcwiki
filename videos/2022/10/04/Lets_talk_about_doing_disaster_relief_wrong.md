---
title: Let's talk about doing disaster relief wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-77kjfjCJsA) |
| Published | 2022/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Someone went to Fort Myers to provide gas and food to people affected by a disaster.
- The person started by helping people in their parents' neighborhood and then expanded to nearby neighborhoods.
- They were criticized by members of a college organization for not being a "real activist" because they started with people they knew.
- Beau argues that the first rule of disaster relief is to not create a second victim.
- He suggests not listening to critics who aren't involved in relief work themselves.
- Beau commends the individual for starting with people they knew and expanding from there.
- He dismisses the idea of "purity testing" in disaster relief efforts.
- Beau explains that disaster relief involves networking and helping those you're connected to.
- He shares a specific example of how helping one person led to assisting others in need.
- Beau stresses the importance of utilizing existing networks and contacts in relief efforts.

### Quotes

- "The first rule of disaster relief is don't create a second victim."
- "You start with the points of contact on the ground that you have."
- "The whole idea is based around the premise that one network of people assists another network of people."
- "People who may not get involved themselves will always have a criticism of the way you did it."
- "Just keep in mind, you did it."

### Oneliner

Someone travels to help disaster victims, faces criticism for starting with known contacts, but Beau defends the importance of networking and mutual assistance in relief efforts.

### Audience

Disaster relief volunteers

### On-the-ground actions from transcript

- Reach out to your existing network to identify needs and provide assistance (exemplified)
- Collaborate with local organizations or supply depots to distribute aid effectively (exemplified)
- Utilize community connections to expand the reach of relief efforts (exemplified)

### Whats missing in summary

Importance of community networking and utilizing existing contacts in disaster relief efforts

### Tags

#DisasterRelief #CommunityNetworking #MutualAssistance #Criticism #Activism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about one of y'all
who traveled down to the impacted area
to help out after Ian,
and then some commentary that they received
when they returned home.
And we're gonna kind of go through that,
talk about that, talk about the right way to do things
and the wrong way to do things,
and basically talk about, I guess, who's a better activist.
Okay, so this is the question.
I have a question.
This week I went to Fort Myers
to bring gas and food to people.
I started at my parents' house
and then spent three days helping people
in their neighborhood,
and then their adult children
in other nearby neighborhoods
because my parents live in a retirement community.
I was feeling pretty good about it all.
Then when I came back to town X,
I was told by members of college organization Y
that it wasn't real activism
because I started with people I knew.
Frankly, it kind of bothered me,
but I'm just wondering how you do it right.
We're purity testing disaster relief now?
Man, this is getting out of hand.
Okay, so let's start with this.
The first rule of disaster relief
is don't create a second victim.
The second rule, and it is a close second,
do not listen to critiques from online leftists
or college leftists
who aren't actually doing any relief work themselves.
You know how I know they're not doing any?
Because that's actually how you do it.
What you did, that's the way you do it.
I mean, I'm sure ideally they want you to show up
and unionize the neighborhood
and start a renter strike or whatever,
but in real life, that's not how it works.
Yeah, you rattle your network.
That you start with people that you know
and you're not going to listen to them.
That you start with people that you know
and work out from there.
I don't know what they expect you to do,
roll into a random neighborhood
and scream out the window who needs help.
You start with the points of contact on the ground
that you have.
You do it exactly the way you did it.
That's how it works.
And if you do it that way,
you might show up at a house
of somebody that you've known for 10 years.
And because they're good people,
when you show up,
you find out that they have an 80 and a 90 year old
in the house with them.
So you get them to propane to run the generator.
So the older people can get fans
or what passes for AC in a time like that.
And then while you're there,
you can fix the generator for the guy next door
who recently moved down from New York with his wife.
And this sounds all super specific
because it actually happened.
That's how you do it.
I would say every single stop that we made
is within three degrees of separation from this channel.
That's how it works.
You rattle your network and those people rattle theirs
and that's how you find out who needs help.
The last stop that we made was
the makeshift supply depot in the neighborhood
where we basically just offloaded everything else
that we had left with them so they could distribute it.
That person, the person running that is in a social circle
with a very frequent viewer of the channel.
That's how it works.
They don't know what they're talking about.
You did it right.
The whole idea is based around the premise
that one network of people assists
another network of people.
And then those people can assist a third or a fourth.
And everybody mutually benefits from this.
That's how it functions.
Honestly, if the rule was in place,
you can't use any contacts you have.
I don't know how you would start.
I mean, I guess you literally could just roll
into a neighborhood that's devastated
and start knocking on doors.
But that's not really time,
that's not effective when it comes to the amount of time
you're gonna spend doing that.
You start with people that you know
and you work your way out from there.
If you do it well enough and you have organized communities,
you can tap into those like the network
in that neighborhood with the makeshift supply depot.
So that's how we ran things in after Michael here.
There was a community, not really a community center.
They do a lot of work with PTSD vets and horses.
They turned their facility into a supply center.
They reached out to get people to run supplies
to Panama City.
Their network bumped into our network
and that's how the stuff got down there.
That's how it works.
People who may not get involved themselves
that ideologically maybe should,
they will always have a criticism of the way you did it.
they will always have a criticism of the way you did it.
Just keep in mind, you did it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}