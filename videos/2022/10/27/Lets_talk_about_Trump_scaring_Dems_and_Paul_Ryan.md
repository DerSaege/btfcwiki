---
title: Let's talk about Trump scaring Dems and Paul Ryan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wTAUKplV95o) |
| Published | 2022/10/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dispels the myth that Democrats are scared of running against Trump in 2024 and conducting investigations out of fear.
- Notes that Democrats beat Trump before all the revelations, election incidents, and January 6th.
- Asserts that Trump is not a threat to the Democratic Party and labels him weak.
- Quotes Paul Ryan saying they're more likely to lose with Trump as he's unpopular with suburban voters.
- Mentions Republicans, including Ryan, acknowledging that Trump cannot win.
- Criticizes Trump's failed promises like building the wall, locking her up, and draining the swamp.
- Acknowledges one decent foreign policy idea but overall considers Trump's presidency a waste of four years.
- Comments on how Trump inherited a booming economy and left it in shambles without any substantial achievements.
- Points out Trump's actions post-2020 election further alienating voters.
- Dismisses the idea of investigations being part of a Democratic plot and asserts they follow where evidence leads.

### Quotes

- "Remind them that even by the MAGA faithful standards, he succeeded at nothing."
- "He accomplished nothing that he had them chanting for."

### Oneliner

Democrats aren't scared of Trump in 2024; even Republicans doubt his ability to win.

### Audience

Voters, political analysts.

### On-the-ground actions from transcript

- Fact-check Trump's claims and hold him accountable for his failed promises (implied).
- Engage in critical thinking and research about political narratives and statements (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's presidency, his failed promises, and the skepticism surrounding his potential success in the 2024 election. It delves into the perspectives of both Democrats and Republicans regarding Trump's popularity and chances of winning.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about an enduring myth about Trump.
We're going to talk about Paul Ryan,
and we're going to talk about what Paul Ryan had to say about Trump.
There is the idea in a lot of Republican circles
that the Democratic Party is really concerned about running against Trump in 2024.
In fact, that's really the reason for all of these investigations,
because they're scared of him.
Yeah, I mean, that is an idea from a land of make-believes
with jelly beans and unicorns and fairy tales.
It doesn't even make sense.
They beat Trump.
They beat him badly.
And that was before all of the revelations.
That was before he pulled what he pulled in regards to the election.
That was before January 6th.
Trump is not scary to the Democratic Party.
I'm willing to bet that if you let Democratic strategists choose
who to run against, they'd probably pick him because he's that weak.
And that's not just a position of somebody like me.
This is Paul Ryan.
We know we're so much more likely to lose with Trump
because of the fact that he is not popular with suburban voters
that we're going to want to win.
He went on.
We lost the House, the Senate, and the White House in the span of two years.
I don't want to repeat that.
I want to win.
And that's why I think we're going to nominate somebody who can win.
Even Republicans know he can't win.
The only people who believe this are the MAGA faithful,
which doesn't make sense because even by their metrics,
what he said he was going to do, he was an abject failure there too.
I mean, really think about it.
What were the things he was really pushing and saying
that he was going to do and had people chanting about?
Build the wall.
Never happened.
Lock her up.
Never happened.
Drain the swamp.
Never happened.
He succeeded at nothing.
If I'm being objective, he had one decent foreign policy idea,
but he couldn't make it work.
Other than that, it was a total waste of four years.
He was handed a booming economy, and he left it in shambles.
He didn't succeed in anything.
And then you have the events after the 2020 election.
After he lost, after America already turned him
into President-reject Trump, he did things
that would alienate even more voters.
Paul Ryan's probably right.
Trump can't win.
And I honestly don't think that anybody
that modeled themselves after Trump,
I don't think they can win either.
But that remains to be seen.
The idea that these investigations are a Democratic
plot is wild.
They're not worried about Trump.
These investigations have to do with where the evidence leads,
not a worry about 2024.
So when you hear this, and inevitably you're going to,
just remind them that even by the MAGA faithful standards,
he succeeded at nothing.
He accomplished nothing that he had them chanting for.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}