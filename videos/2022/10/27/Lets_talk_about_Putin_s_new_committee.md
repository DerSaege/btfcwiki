---
title: Let's talk about Putin's new committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XZxWAtlKydQ) |
| Published | 2022/10/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin formed a new committee to address military issues, showing a rare admission of problems.
- The committee was created due to economic difficulties and lack of supplies for Russian troops.
- Despite propaganda efforts showing well-equipped soldiers, the reality is different, with many troops having outdated and non-functional equipment.
- Putin hopes the committee will help expedite the arrival of necessary supplies like cold weather gear and medical kits.
- Russia faces challenges in obtaining necessary equipment due to sanctions limiting trade.
- The lack of cold weather gear could impact Russian troops' mobility and effectiveness in comparison to Ukrainian forces.
- The supply issue could become a critical factor in Russia's ability to continue the war as winter approaches.

### Quotes

- "You go to war with the military you have, not the one you want."
- "A lack of cold weather gear is a big deal."
- "If they can't find a solution, winter is coming."

### Oneliner

Putin's new committee addresses military issues stemming from economic difficulties and lack of supplies, with potential consequences for Russia's ability to continue the war as winter approaches.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Support organizations providing aid to Ukrainian forces (implied)
- Advocate for humanitarian aid to war-affected areas (implied)

### Whats missing in summary

Insights on the potential humanitarian impact of the supply issues and the broader implications for the conflict. 

### Tags

#Putin #Russia #MilitaryIssues #SupplyChain #Ukraine


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Putin's new committee,
something that he has put together in hopes of alleviating some of the issues that his military is running into.
This move is one of the first real admissions from that level that things aren't going well.
The reasoning per Putin for this committee being formed is the economic difficulties or restrictions,
depending on the translation you want to use, and the lack of supplies that his troops have.
Despite the little propaganda stunt that Putin engaged in where he went to the training facility
and every soldier there had stuff straight out of the package,
this is an admission that he understands that that's propaganda.
That's not real. The reality is that there are a lot of troops, particularly the conscripts,
who are being shipped off with not just obsolete equipment,
but equipment that is obsolete to the point it's not of any use and it was ill-maintained.
So it doesn't really function even at the obsolete level.
They are having trouble finding cold weather gear, medical kits, stuff like this,
stuff you have to have.
Putin is pulling together this committee in hopes of expediting this material showing up.
The reality is you go to war with the military you have, not the one you want.
This is what he has.
I do not know what Russia is going to be able to do to shore up these supplies.
Given the restrictions that have been put on their trade via sanctions,
and the fact that most countries that are avoiding the sanctions,
that aren't participating in those sanctions, they don't really have the equipment he needs.
I'm not really sure how this is going to play out.
Now this could be critical, and this is why Putin is having to put together this committee.
It's why they're suddenly taking it seriously.
A lack of cold weather gear is a big deal.
The Ukrainians, they're going to have it.
And if the Russian troops are preoccupied with staying warm,
they become very static, and it makes it a whole lot easier for Ukrainian forces.
The supply issue that was very evident at the outset of the war now may become a deciding factor
in whether or not Russia can continue to prosecute this war.
Because if they can't find a solution, winter is coming.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}