---
title: Let's talk about hope and lessons learned from Hurricane Ian....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a6y4svQTXXg) |
| Published | 2022/10/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses lessons learned from helping out with EIN after a hurricane, offering practical advice that can be immediately applied.
- Clarifies the relative scale of the hurricane's impact compared to past disasters like Michael or Katrina.
- Mentions areas like Arcadia and Sebring that may not have received as much coverage but are recovering.
- Advises on the importance of being prepared with gas when entering a disaster area, especially for relief efforts.
- Recommends considering generators that use gas, propane, or solar power for post-disaster situations.
- Emphasizes the need for proper footwear and essentials when assisting in disaster areas.
- Suggests stocking up on freeze-dried food to avoid relying solely on National Guard distributions post-disaster.
- Shares experiences with using generators and chainsaws to provide aid and why they are prioritized in relief efforts.
- Acknowledges the positive response of local governments in handling the aftermath of the hurricane.
- Talks about funding relief efforts through viewer contributions and the importance of bringing hope during disasters.
- Shares a personal experience of leaving supplies unsecured overnight in a disaster-stricken area and finding them untouched.

### Quotes

- "Treat it like going to Antarctica. If you didn't bring it, it's not there."
- "Your social media, your public messaging, that's your radio."
- "After every disaster, people start screaming about looters. Don't do that."
- "You're supposed to be conveying calm."
- "If you want to be that leader tough guy, that's what you should be doing, not inspiring paranoia and fear."

### Oneliner

Beau advises on practical disaster relief lessons, from preparedness to providing hope, showcasing effective community response.

### Audience

Disaster relief volunteers.

### On-the-ground actions from transcript

- Stock up on freeze-dried food for emergency situations (suggested).
- Ensure you have enough gas for both entry and exit when entering disaster areas (implied).
- Invest in generators that use multiple power sources like gas, propane, or solar (implied).
- Prioritize providing tools for self-rescue like generators and chainsaws in relief efforts (implied).

### Whats missing in summary

The full transcript provides valuable insights on disaster relief strategies, community responses, and the importance of conveying hope during challenging times.

### Tags

#DisasterRelief #CommunityResponse #Preparedness #Hope #LocalGovernment


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about lessons learned
from going down and helping out with EIN,
stuff that can be immediately applied by a lot of people.
There will be another video over on the other channel that
deals with some of the other stuff.
But this is stuff that can be useful now
and answering some questions that came in
in the immediate aftermath.
The most common question is, how bad is it?
When you're talking about hurricanes,
it's a relative scale.
This is not as bad as Michael or Katrina.
But this is also not one that nobody's
going to remember the name of in a year.
There is damage.
There are some areas that are bad.
But overall, it's not horrible.
Despite the tragedies that have occurred,
it's not a massive area that was impacted like that.
Again, not downplaying the truly bad parts.
A lot of questions about areas a little bit more inland
that aren't getting a lot of coverage, Arcadia, Sebring,
places like that.
As far as I know, I don't know that Sebring really
had anything happen to it major.
Arcadia, I would be surprised if it's not up and running
by the time you all watch this.
We wound up driving through there
due to the, let's just say, chaotic nature of the road
closures.
I don't want to say they were doing a bad job,
because they really might have been doing the best they could,
given the circumstances.
But there were issues with the rerouting.
OK, so immediate lessons learned.
First, if you're going to help, remember
when you're entering a disaster impact area,
treat it like going to Antarctica.
If you didn't bring it, it's not there.
Saw a number of people who were bringing in relief supplies who
were on the side of the road with no gas.
Gasoline distribution in a situation like this
gets disrupted.
So you need to make sure that you have enough gas not only
to get in, but also to get out.
And that's easier said than done.
We'll go in depth on how to do that in the other video.
But it's gas cans.
You bring gas to get out.
On that note, when it comes to generators,
because any time something like this happens,
those people that were a near miss,
they tend to go out and prepare a little bit more.
If you're looking to buy a generator,
make sure you get one that does gas and propane or solar.
Solar generators are a thing now.
The gasoline is always in short supply after a disaster.
Propane is much easier to get.
So look into your propane and propane accessories.
When it comes to solar, I would point out
that there was a neighborhood, and I
want to say 12 miles from the hardest hit area, that
as far as electric, didn't have any issues
because the whole neighborhood's solar powered.
There are a lot of politicians on Twitter
talking about, well, what would you
do if you had an electric car right now?
Well, if you also had solar panels, you would charge it.
And you would be able to get around a whole lot easier
than if you had a gas vehicle.
Make sure that you bring everything
you need to include footwear, the right kind of footwear.
One of the less than intelligent people that I was with
wound up on a roof in cowboy boots.
It's me.
I'm less than intelligent.
People don't do that.
Make sure you bring the stuff that you need.
That was a mistake on my part.
Be better than me.
MREs, one of the things that happens after any disaster,
the National Guard comes in and they distribute MREs.
And they do a great job of it.
It is incredibly important for a lot of people.
However, one thing I would point out
is if you have the ability, if you
have the financial resources, getting a couple weeks
worth of freeze-dried food that's going to last 50 years
and sticking it in the back of a closet or whatever
might be better than relying on the National Guard.
Not because they're not going to bring it,
but because you're not going to know where the distribution
point is.
And you have to have a way to get there.
Or you have to count on people to bring it to you
from the distribution point.
So if you already have that, it eliminates a worry
and eliminates a use of gasoline that is in short supply.
There was some flooding.
I posted a photo.
The snorkel did work.
For those that know about the whole Jeep thing going on,
the snorkel did work.
That prompted questions about, can I just
put a snorkel on my SUV and will it do the same thing?
Probably not.
Jeeps coming off of the lot, stock Jeeps,
have pretty good water-forwarding capabilities,
just standard.
So putting a snorkel on your SUV that
was really designed to transport families and not go off-road,
probably not going to have the same capabilities.
It's not just a matter of putting the snorkel on
so you get combustion.
But for those who contributed to that livestream
and working to outfit the Jeep and all of that stuff,
your investment definitely paid off.
We have some footage that will be in the other video.
Why did we do generators and chainsaws this time primarily?
Because they take up a lot of space.
It seems like we could help more people if we weren't
distributing the generators.
And why did that change from Michael?
The answer of why it changed is simple economics.
When Michael was going on, this channel had, I don't know,
maybe 100,000 subscribers.
We didn't have the financial capability
to distribute items that cost hundreds of dollars each.
Generally speaking, it's, in most cases,
it's better to provide the tools for self-rescue
than continuing a supply of just what they need.
So if you drop a generator in a neighborhood that
doesn't have power and nobody has a generator,
it gives older people in particular, or younger people,
a place to go that might have fans running,
because the heat is a literal killer.
It allows people to charge phones, flashlights,
stuff like that.
It provides a little bit of hope and a little bit of,
kind of takes the edge off.
Most people are not going to starve to death
after a hurricane.
They may need comfort food.
They may need food that isn't an MRE.
And that isn't to be understated,
because in a situation like this,
you're wanting to provide hope.
You're wanting to let people know that things will eventually
get back to normal and all of that.
So providing the generators and the chainsaws,
the chainsaws allow them to start clearing stuff out
and, again, getting back to normal.
As far as the government response,
the local governments were doing actually really well,
which is not something I normally say.
From what I saw, there were some minor hiccups,
but they really seemed to be handling it.
I don't have any major complaints this time.
From what I saw, the local entities, so county level
and down, really seemed to be on the ball.
And then how did the generators and stuff get paid for?
Y'all.
When we do those live streams and I say,
I don't know what we're going to use this for,
it kind of builds up a cushion.
So when something like this happens unexpectedly,
we have the cash on hand to do this.
So while I would love to have the impression of me
being selfless, no, this was, the channel did this.
Y'all did that.
And then the big part, hope.
That's what you're supposed to be bringing, right,
when you're doing this.
So I had generator, chainsaw, gas, food, everything
in the back of that Jeep.
Unsecured.
No locks, nothing.
Overnight in an area with no power and no security.
And do you know what happened when I came out after sleeping,
walked outside?
Do you know what I found in the back of that Jeep?
Everything that was there before.
After every disaster, people start screaming about looters.
Don't do that.
That's not helpful.
That isn't helpful.
You're supposed to be conveying calm.
You're supposed to be giving the impression that things
are going to get better, not that they're
going to get worse.
You don't want to instill paranoia and fear when
it's not really necessary.
That's not, that isn't helpful.
Everybody knows the risks of something like that happening.
But it's not something that should
be messaged as the true worry.
The true worry is that your elderly neighbor can't
get their generator started or is literally
on the verge of having issues because of the heat
or can't get water.
This is the true worry.
That's a much more likely outcome.
When people talk about this, most times
they're trying to display their tough guy cred.
Think about your favorite tough guy movie
where the soldiers are pinned down
and in that impossible situation.
The person on the radio that they're talking to,
are they screaming, oh, you're doomed.
You're going to be overrun?
No, of course not.
They're trying to convey calm.
They're trying to provide hope, which should be the goal.
If you want to be that leader tough guy,
that's what you should be doing, not inspiring paranoia
and fear.
If you talk to any of those idealized tough guys
and you ask them what they're supposed
to convey on the radio, the answer
they're going to give you is calm.
They're not going to tell you to terrify
the person on the other end.
Your social media, your public messaging, that's your radio.
So overall, it went well.
We got a lot of stuff distributed.
Felt like we made a difference, especially
since we were only there a couple of days.
It was good.
Now we will get back to the regular schedule.
Given how well things are going down there
as far as the recovery, we may not need to do anything else.
Like I said, I'm for once, and this
is from somebody who never says this,
I'm pretty impressed with the local response.
They really do seem to be on the ball.
So that could change, and we might be needed again.
But at this point, I feel like they've got it.
Anyway, it's just a thought.
Y'all have a good day.
Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}