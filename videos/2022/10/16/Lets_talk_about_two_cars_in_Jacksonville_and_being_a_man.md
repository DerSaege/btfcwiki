---
title: Let's talk about two cars in Jacksonville and being a man....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MT6NvXvGKcg) |
| Published | 2022/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Jacksonville, Florida, road rage incident escalates between two families in cars.
- Men engaging in reckless behavior, speeding, brake checking in a dangerous game.
- Water bottle thrown, leading to gunfire exchanged between the vehicles.
- Daughters in both cars are shot, one seriously injured.
- Beau criticizes the toxic masculinity rhetoric tied to firearms.
- Emphasizes the tragic consequences of men acting immaturely.
- Points out the online debate over who was right in the situation.
- Beau condemns both men's actions and stresses the need to avoid violence.
- Criticizes the misguided notion of masculinity that leads to such violent outcomes.
- Stresses the importance of adjusting attitudes towards masculinity and family protection.
- Both men involved have been charged with attempted murder in Florida.

### Quotes

- "This isn't masculinity. This is what happens when men act like children."
- "It's the kids who pay the price."
- "People need to adjust their rhetoric and their concept of what it means to be masculine."

### Oneliner

Two families' road rage incident in Jacksonville spirals into gunfire, showing the tragic consequences of toxic masculinity and immaturity on family safety.

### Audience

Men, fathers, families

### On-the-ground actions from transcript

- Adjust your rhetoric and concept of masculinity (implied)
- Promote mature and non-violent conflict resolution (implied)
- Advocate for peaceful solutions in conflicts (implied)

### Whats missing in summary

The emotional impact and detailed analysis of toxic masculinity and its consequences.

### Tags

#ToxicMasculinity #FamilySafety #CommunityPolicing #ConflictResolution #Jacksonville


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about fathers and daughters and families and Jacksonville,
Jacksonville, Florida, and cars and modern masculinity and the rhetoric that surrounds it.
And what happens when that rhetoric is put into action and the effects of that.
Two cars, both alike in dignity, in fair Jacksonville is where we lay our scene.
From recent grudge break to new mutiny and civil blood make civil hands unclean.
Two cars, both driven by men with their families, start engaging in a road rage thing.
Start going back and forth, speeding up, accelerating, brake checking each other.
A witness described it as a game of cat and mouse.
And it is typical of what you would find in Jacksonville or a lot of other places.
And then a water bottle is thrown from one vehicle to the next.
And then the car that had the water bottle thrown at it opened fire
and struck the daughter in the other vehicle.
And then that car opened fire and struck the daughter in the other vehicle.
This is the effect of the rhetoric of modern masculinity and tying it to a firearm.
This is what happens.
Two men whose daughters now have holes in them, one 5, one 14, one with a really serious injury.
This isn't masculinity.
This is what happens when men act like children and is typically the case when men start acting
like children.
It is literal children who pay the price.
And to me, one of the worst parts about this is that I'm almost certain that if you were
to go to certain online communities that are talking about guns, the debate is going to
be over which one was right.
And the answer is they're both wrong.
If you're going to carry your daughter into a gunfight, you're not right.
The right move here, it seems like it should go without saying, is to hit your brakes and
not get involved in a situation like that.
That's the right move.
But I guess that's not alpha enough, right?
And when people talk about that and they tell each other that they need to adopt that attitude
and be more aggressive and all of that, the idea is to protect your family.
How'd that work out?
This is actually what normally happens when people adopt that attitude.
Can't let go of that anger.
Can't control their ego.
It's the kids who pay the price.
This isn't a new theme.
It's been talked about for a really long time.
And it shows no signs of letting up.
People need to adjust their rhetoric and their concept of what it means to be masculine.
What it means to take care of your family.
Incidentally, both men have been charged with attempted murder in Florida.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}