---
title: Let's talk about the Barnes-Johnson debate in Wisconsin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=W5c-lbZCg_g) |
| Published | 2022/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Johnson, a vulnerable Republican up for re-election, failed to come across as friendly during the final debate in Wisconsin.
- Johnson's defensive posture didn't resonate well with the audience, especially when he made a snarky comment about his opponent turning on America.
- The audience booed Johnson when he mentioned the cost of not finishing Trump's wall.
- Barnes, the Democratic candidate, suggested Johnson's involvement in a fake electors scheme, leading to a quip about a "five-second rule for subversion."
- Senator Johnson closed the debate by claiming, "the FBI set me up," a sound bite likely to be used in the future.
- Johnson's attempt to maintain an attack position may not have played out as he intended, risking potential backlash in the polls.
- Beau questions whether Johnson's aggressive approach will impact polling results and suggests that sometimes holding ground is wiser than pushing further.
- The debate showcased Johnson's struggle to balance aggression with likability and the potential consequences of pushing too hard.

### Quotes

- "I don't know why he turned on America."
- "It cost more to not finish Trump's wall than to finish it."
- "The FBI set me up."

### Oneliner

Senator Johnson's failed attempt at aggression over likability in the Wisconsin debate may impact his re-election chances, with potential backlash in the polls.

### Audience

Voters in Wisconsin

### On-the-ground actions from transcript

- Watch out for soundbites like "the FBI set me up" during the election (implied)

### Whats missing in summary

Insights on the potential impact of Senator Johnson's debate performance on the upcoming election in Wisconsin.

### Tags

#Wisconsin #Election #Debate #SenatorJohnson #Campaign


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to
we're going to talk a little bit about Wisconsin and the Barnes-Johnson debate and
How that played out now for those that aren't following all of them
Senator Johnson is seen as one of the more
vulnerable
Republicans that is running for re-election
Currently he's up by two or three points in the polls. Now this would be the point,
this is the final debate, and it would be the point where it would be wise for
Johnson to adopt a little bit more of a defensive posture and try to maybe come
across a little bit more friendly. That's not the rowdy chose, and it did not seem
to land very well with the with the audience there at the debate. At one
point the candidates were asked to say something admirable about each other,
something that they found admirable about their opposition. And Barnes, the
Democratic candidate was like you know he's a family man and that's to be
respected. Johnson said well he had good parents in a good upbringing I just
don't know why he turned on America and the audience booed him. He elicited
laughter when he said that it would cost more to not finish Trump's wall than to
finish it. Barnes did jab back a little bit. You know, there is a suggestion that
Johnson might have been involved in the fake electors scheme, and there's
a statement that his involvement only lasted seconds. Barnes quipped about a
five-second rule for subversion. And then in the final debate, the closing
message going out to the voters, Senator Johnson actually uttered the phrase the FBI set me up.
If you live in Wisconsin, be prepared to hear that a few times because that is a sound bite
that is probably going to be used. You know, walking into this, Johnson was ahead and tried
to, he tried to maintain an attack position. I don't know that it played the
way he wanted it to. I would be very interested in seeing the polling after
this debate and what happens. There are moments when holding what you have is
better than trying to pursue further. Because if you push too far, well, you
you might get booed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}