---
title: Let's talk about Social Security shenanigans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ob5yJc987mg) |
| Published | 2022/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses Social Security and the potential intentions of the Republican Party in 2023.
- In 2023, there's a significant increase of 8.7% in Social Security payments due to inflation.
- Medicare Part B premium is expected to decrease for the first time in a decade.
- The concern arises from Biden's statement about Republicans putting Social Security on the chopping block.
- Rick Scott and Ron Johnson propose making Social Security discretionary rather than mandatory, potentially leading to cuts.
- House Republicans plan to leverage the debt limit deadline to push for changes in entitlements and safety net programs.
- There's a proposal to raise Medicare eligibility to $270 and Medicaid to $67.
- The Republican Party may use leverage to threaten the economy if they don't get their way, akin to steering the country towards economic ruin.
- Despite thinking it's outlandish, Beau acknowledges recent actions by the Republican Party make this theory plausible.
- Beau expresses concern about the future of Social Security and Medicare if the Republican Party gains more control.

### Quotes

- "If you are receiving Social Security, you know, that you paid into for your entire life, you're the fat, by the way."
- "It is worth noting the Republican Study Committee has put out a thing."
- "But then again, we did all just watch them follow Trump, to, you know, subvert the entire election."
- "I find it hard to believe that there is a major political party in the United States that really does think that leverage is, you know, basically steering the entire country into economic ruin."
- "Y'all have a good day."

### Oneliner

Beau warns of potential cuts to Social Security and Medicare by the Republican Party in 2023, using leverage and political maneuvers to push their agenda.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact local representatives to express support for protecting Social Security and Medicare (suggested).
- Join advocacy groups focused on safeguarding entitlement programs (exemplified).
- Organize community events to raise awareness about potential threats to Social Security and Medicare (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential threats to Social Security and Medicare, urging vigilance and proactive engagement to protect these vital programs.

### Tags

#SocialSecurity #Medicare #RepublicanParty #PoliticalThreats #EntitlementPrograms


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about
Social Security shenanigans
and what the Republican Party might be intending,
whether what Biden said is true
about it being on the chopping block
and what's gonna happen in 2023
because there are some changes.
Okay, so first the good news
if Social Security is important to you.
In 2023, 8.7% increase in Social Security payments.
It's a cost of living adjustment.
Why?
It's inflation.
That's why.
It's the largest increase in 40 years.
Medicare Part B will drop.
The premium, I wanna say it's gonna drop
like five bucks a month,
but it's the first time it's gone down in a decade.
That's the good news.
The bad news is the idea that Biden has put out there
that Republicans wanted on the chopping block.
Is that true?
Well, I mean, it certainly seems like it.
You have Rick Scott and Ron Johnson
both saying they want to switch it
from mandatory to discretionary.
They want to have to reauthorize it every year,
every five years, depending on the political candidate.
Now they say it's to make sure the money's there,
which sure, I mean,
that totally matches up with their rhetoric, right?
Not really.
The more likely reason is to eventually not reauthorize it,
but in the meantime, use the reauthorization process
as leverage to cut it, to cut payments, to trim the fat.
If you are receiving Social Security,
you know, that you paid into for your entire life,
you're the fat, by the way.
That seems like the most likely reason to want to do it.
Their rhetoric does not line up
with what they're saying here.
Bloomberg government had an interesting report
saying in part,
Social Security and Medicare eligibility changes,
spending caps, and safety net
work requirements are among the top priorities
for key House Republicans
who want to use next year's debt limit deadline
to extract concessions from Democrats.
So the idea is that the Republican Party
wants to just steer the US towards a cliff
when it comes to the debt limit.
And if the Republican Party doesn't get its way,
if the Democratic Party doesn't allow the Democrats
to do what they're doing,
doesn't allow them to adjust entitlements,
you know, stuff that you paid into your entire life,
or safety net stuff,
well, they'll just steer us right over the cliff.
That's what Bloomberg reporting is suggesting here.
It is worth noting the Republican Study Committee
has put out a thing.
Basically, it's a plan to raise the eligibility
for Medicare to $270,
and for Medicare to $67, I think.
Now, people are kind of acting like
this would never happen
because there's a Democratic president.
It's not necessarily the case.
If the Republican Party has enough leverage
because they have control of the House,
they might be able to push it through.
They might be able to attach it
to something that's must pass.
They might be able to use some leverage
to basically threaten to crash the entire economy
if they don't get their way.
So, I personally think that that,
you know, I would like to say
that no politician would do that,
that it's just bluster.
But then again, we did all just watch them follow Trump,
to, you know, subvert the entire election.
So, maybe not.
It seems to me like this idea is a little bit out there,
but given what we've seen
from the Republican Party lately,
we can't really put it past them either.
So, there is a legitimate worry
for Social Security, Medicare, stuff like that,
if the Republican Party takes control of the House,
or the Senate, more so the House, I think.
But that's where we're at.
I find it hard to believe
that there is a major political party in the United States
that really does think that leverage is, you know,
basically steering the entire country into economic ruin.
But at the same time, I can't say no,
they wouldn't do that,
because their rhetoric and their record lately
doesn't warrant me saying that.
This is a theory that's been put out.
Is it plausible?
Yes.
Is it possible?
Yes.
Do I think they'll do it?
I would hope not, but you never really know.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}