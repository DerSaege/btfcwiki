---
title: Let's talk about Spy Games, China, and the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G1fK2J1C2tc) |
| Published | 2022/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Justice in the United States has charged Chinese nationals with engaging in intelligence operations within the country.
- Big Red Kelp, a commentator on Chinese spying, mentioned a "gentleman's agreement" existing in the spy game world.
- In the gentleman's agreement, the actual spy handlers are typically asked to leave the country quietly, instead of facing legal action.
- Disrespecting this agreement could lead to repercussions, as shown by the recent charges against Chinese spies.
- China's attempts to move away from international agreements, like this gentleman's agreement, may have triggered the recent actions by the US.
- The US may be increasing pressure on China due to Russia being perceived as a near-peer competitor.
- The US may use intelligence operations to secure American interests, often for commercial advantage, but with distinctions from China's alleged direct support to a specific company.
- Counterintelligence officers focus on law enforcement and catching foreign intelligence operations within their jurisdiction.
- Intelligence officers, on the other hand, are likened to trained criminals who conduct espionage and other covert activities.
- The US could be gearing up for a more intense intelligence operation against China in response to recent events.

### Quotes

- "They're cops. Think of them like that. They're law enforcement."
- "There's a different frame of mind between the two."
- "They're not spies. They're spy catchers and they have a very different frame of mind."

### Oneliner

The US ramps up intelligence operations against China amid concerns over Chinese espionage and flouting international norms.

### Audience

Policymakers, Intelligence Analysts

### On-the-ground actions from transcript

- Research and understand the implications of international espionage and the enforcement of related agreements (implied)
- Stay informed on developments in global intelligence operations and their impact on national security (implied)
- Advocate for transparency and accountability in international relations to prevent abuses of power (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of recent actions taken by the US against Chinese intelligence operations and sheds light on the dynamics of international espionage.

### Tags

#China #UnitedStates #Espionage #InternationalRelations #IntelligenceOperations


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about China and the United
States and spy games and what we can infer from the unique
development that occurred.
We're going to do this because we've got a couple of messages
and it provides a very nice framing for this conversation,
the two in conjunction.
So if you don't know what I'm talking about, the United
States, the Department of Justice has basically decided to charge Chinese
nationals with engaging in intelligence operations within the United States. This
is unusual and we'll get to why and we'll kind of get to what it means. So the
first question that I got was, I was wondering what you think of this guy's
take on Chinese spying. He's new and was wondering if he's worth following. Is
there really a gentleman's agreement like this or is he full of it? Okay, this
guy is Big Red Kelp. If you don't know, this is somebody I chat with on
Twitter. Yeah, he knows what he's talking about. I'm vaguely familiar with his
background, he knows what he is talking about. The gentleman's agreement
definitely exists. For people on this channel, you'll probably like him.
He's very mellow, very concise, generally speaking, so it's probably something that
people who enjoy this channel will enjoy. He's also not new. He's new to YouTube,
YouTube, but he's been on other platforms making videos.
OK, so what did he say beyond there's a gentleman's agreement?
He basically said that the United States did this for a
whole bunch of reasons.
But he didn't want to speculate on all of them.
I will here in a minute.
But that a big one was that it seems that China is trying to
step away from international agreements to include this gentleman's agreement.
And that tracks, that makes sense.
The gentleman's agreement works like this.
Generally speaking, it's not the handler, the actual spy that is in the paid
employee of a foreign power that gets arrested or charged.
It's the source.
It's the agent, the person who was turned that is a citizen of
the country that is being spied upon.
That's normally the person who is charged.
And the actual spy, the handler,
most times has kind of handed a letter saying,
hey, you're not welcome here anymore.
Here's a plane ticket.
Make sure you're on it.
And this happens pretty much all over the world.
This is generally how it's done.
Sometimes it's done because there's
a diplomatic cover involved.
But other times, it's just easier, and we'll get to why.
But there's an element of respect that all of those
playing the great game, so to speak,
they don't want to see somebody go to prison for 20 years just
for doing their job.
And it doesn't matter where that person is from, necessarily.
So that plays into it.
And the expectation is, if we treat you like this,
this is how you'll treat us.
It's also just easier logistically,
because let's say the United States goes out and arrests
a Russian handler.
What's Russia do?
They go out and snatch up an American one.
And then you have to work out a trade,
and it's this whole thing.
It's easier just to send them home.
And in many cases, they don't even
do that because it's easier, it's
more effective to know who the other spy is rather than send
them home and know that they're going to send a replacement who
you haven't identified.
OK, so the gentleman's agreement is real.
And this may have something to do with China trying
to get out of that.
And this is the US's way of saying, OK,
you don't want to play by those rules anymore?
Fine.
Turn around and put your hands on the wall.
But that's just one of the reasons, as he said.
Other reasons.
They went a little outside the norm with something.
They tried to influence the judicial system, the legal system.
They tried to get in there.
And it's not unusual for intelligence agencies to do that.
Theirs, ours, everybody's.
But it's unusual to try to do it directly with a bribe.
That's just not how it's done.
It's kind of against the norms.
So that probably had something to do with it.
Then there's the fact that Russia has been discovered
to be nearly a near-peer.
We've talked for a year and a half or so
about how the United States is moving to near-peer contests.
With Russia demonstrating that it's not really a near-peer,
there's probably a desire to kind of turn the heat up
on China, and this may be part of that.
Another thing is that, in this case, DOJ got to run a double.
One of the people that the Chinese spies approached, they made it to U.S.
counterintelligence without being found out, and were really working for U.S.
counterintelligence while the Chinese thought they were working for them.
Those kind of spy games, they don't get to be played very often, so there also
might be an element of the counterintelligence guys at the FBI
just being like this is gonna be fun and then they stumble into a giant case that
may have something to do with it as well so those are those are the main reasons
it's kind of upping the stakes letting China know that if you're not going to
comply with international norms we're not going to either then there's the
aspect of allowing China to understand that we're going to put more pressure on them than
Russia and given the fact that a lot of this had to do with tech, it may also be a signal
to them of you understand that if you start supplying Russia with tech, that we can upset
you as well.
And then there was a follow-up question that provides some unique framing.
press conference when the Department of Justice was talking about this, they kind of made a big
deal about, you know, and China was using state resources and conducting espionage to give a
company a commercial advantage. And I got a whole bunch of people sending me messages like,
don't we do that? Yes. But there is a slight difference here. Normally, when the US does it,
you hear the phrase American interest and it's very general. It's competing for a commercial position
that could be filled by any company that is U.S. based or even allied based. Chinese intelligence
was apparently trying to support a specific company. That's a little outside the norms.
But that also doesn't mean that the people up there were lying when they presented it
as something that just doesn't happen.
They were more likely just inaccurate.
The people up there were counterintelligence officers or people who are on the counterintelligence
side.
They're cops.
Think of them like that.
They're law enforcement.
Their job is to catch foreign intelligence operations within their jurisdiction and disrupt
them, arrest people, that kind of thing.
It's a law enforcement capacity.
They're trying to get people to play by the rules within that jurisdiction.
The commercial advantage stuff, the national interest, American interest, all of that stuff
done. That's done by intelligence officers, not counterintelligence officers.
If counterintelligence is law enforcement, what's an intelligence
officer? Like really, what's their job? If you don't church it up, they're a
criminal. They're a trained criminal. That's the job. You travel to a
foreign country under an assumed name, you engage in burglary, theft, identity
theft, espionage, like on a daily, that's just Tuesday. So there's a different
frame of mind between the two and there's a lot of times when the
counterintelligence side doesn't really, you know, necessarily know some of the
shadier stuff that the intelligence community does or they kind of overlook
it as oh that's just a bad apple or something like that but yes the United
States absolutely uses its intelligence service its military all of these things
to secure American interest which in many cases is a commercial advantage but
normally I'm not going to say it never happens but normally the United States
States doesn't conduct an intelligence operation at the behest of you know
Twitter like that's not normally something that occurs so that's that's
where that came from that's how it is a slight bit different and that is why they
acted the way they did because they're not spies. They're spy catchers and they
have a very different frame of mind. They're cops and intelligence officers
are criminals. So there's a rough overview. What we can infer from this is
that the United States is going to turn up the heat when it comes to Chinese
intelligence operations. They're probably going to be more aggressive in their own
intelligence operations against China and this is just more of the ratcheting
up of the near-peer contest Cold War style stuff and that's that's where
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}