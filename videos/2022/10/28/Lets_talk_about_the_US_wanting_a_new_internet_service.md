---
title: Let's talk about the US wanting a new internet service....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_d9ErNeu1Zk) |
| Published | 2022/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is considering developing a pop-up infrastructure tool to provide internet access to countries with authoritarian governments or disrupted internet infrastructure like Ukraine.
- There is a desire within the defense and foreign policy community to create a system for such situations.
- The current reliance on private corporations for providing vital internet connectivity is concerning from a national security perspective.
- The U.S. government aims to develop tools to bypass censorship by authoritarian regimes abroad.
- Initial reports suggest a budget of $125 million for this project, which Beau believes is insufficient.
- Beau anticipates the budget to increase significantly as the project progresses.
- Investing in tools that help dissidents avoid government surveillance is seen as a necessary step for America to lead the free world.
- Providing information and internet access empowers people in restricted countries for self-determination, potentially reducing direct U.S. involvement.
- Beau views this initiative positively, seeing it as a way to empower individuals and nations through information.
- A significant project is expected to emerge to create a service for internet access in critical situations.

### Quotes

- "Ideas travel faster than bullets."
- "If America is going to lead the free world, we're going to have to be willing to double and triple our investments in the tools that Iranian and Russian dissidents are jumping on to avoid government snooping."

### Oneliner

The United States may develop a tool for providing internet access in authoritarian regimes, aiming to empower individuals and nations through information access and bypass censorship.

### Audience

Tech enthusiasts, policymakers

### On-the-ground actions from transcript

- Develop tools to provide internet access in countries with authoritarian governments (implied)
- Invest in systems that bypass censorship and government surveillance (implied)

### Whats missing in summary

The full transcript delves into the necessity of empowering individuals with information access and the role of the U.S. in leading global efforts towards this goal.


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
pop-up infrastructure and a new tool that the United States would like to develop and have
at the ready in case they ever need to provide a service. There is a strong desire now within
the defense community and the foreign policy community to develop some kind of system
that could be used to provide internet infrastructure to a country that has an
authoritarian government or is perhaps in a situation where their internet infrastructure
has been disrupted or destroyed like Ukraine. And it appears as though the U.S. government is about
to spend some dollars to develop a system like that. Now, I know what you're saying. I know
you're saying that that sounds like a system that already exists. For some reason they don't want
to use it. It doesn't look that way anyway. I can't imagine why. We should not be in this situation
where we're relying purely on the voluntary goodwill of a private corporation to provide
connectivity issues that many here in America view as vital to U.S. national security interests.
I think that's the director of the FCC talking about this. Along with this, there's also a
coinciding interest in developing tools that can circumvent censorship by authoritarian governments
overseas. So it appears that the United States government wants to put this in their toolbox.
They want to be able to provide information access in countries that restrict it and provide spring
up internet service. It's a tall order. There are already some figures floating around in some of
these reports. I think the first one I saw was like $125 million. That's not going to cut it.
That's nowhere near enough. But as anybody who's familiar with how this type of thing develops
can tell you, the numbers always start off small and they grow. Here's another quote for you.
If America is going to lead the free world, we're going to have to be willing to double and triple
our investments in the tools that Iranian and Russian dissidents are jumping on to avoid
government snooping. You know, I like this to be honest. I think this is a good move.
Ideas travel faster than bullets. If you can provide information or you can provide a service
that allows people in countries to access that information, it increases their ability to engage
in self-determination, which decreases the need for the United States to actually show up in person.
That sounds like a win for everybody. So be on the lookout for a massive project to start to
come online that is basically going to create a service that can provide internet access in
extreme situations. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}