---
title: Let's talk about history, context, and format....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t8oO-DmfrdA) |
| Published | 2022/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing historical context to an earlier video.
- Admitting to owning a mistake in not providing historical context in a previous video.
- Acknowledging the importance of historical context in bringing clarity.
- Linking the incident of soup throwers to a similar historical event in London during the early 1900s.
- Explaining the actions of the Women's Social and Political Union, also known as suffragettes.
- Drawing parallels between the suffragettes' actions and modern-day young climate activists.
- Recognizing the frustration of those who do not have a voice in the decision-making process.
- Acknowledging that when people lack a voice, they may resort to lashing out.
- Mentioning the tactic of drawing attention to climate change through disruptive actions.
- Noting the intentional reference made by one of the people involved with the Van Gogh painting to disobedience leading to rights.
- Suggesting looking up surveillance photos of militant suffragettes for more insight into the movement.
- Encouraging putting a face to a movement or idea by researching further.
- Ending with a thought-provoking message.

### Quotes

- "I made a mistake. Now we're going to turn that mistake into a happy little tree."
- "When people don't have a voice, they lash out. Right or wrong, that's what happens."
- "I can't condemn the lashing out without condemning the conditions that cause them to lash out."
- "It's not without historical precedent."
- "Sometimes it's good to put a face to a movement or an idea."

### Oneliner

Beau provides historical context, admits to a mistake, and draws parallels between suffragettes and modern activists, reflecting on the importance of having a voice in societal decisions.

### Audience

History enthusiasts, activists, educators.

### On-the-ground actions from transcript

- Research and learn more about the suffragette movement and their tactics (suggested).
- Look up surveillance photos of militant suffragettes to understand their struggle better (suggested).
  
### Whats missing in summary

The emotional impact of historical precedents on modern activism.

### Tags

#HistoricalContext #Activism #Suffragettes #ClimateChange #Voice


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to provide a little bit
of historical context to an earlier video.
And we're going to talk about me owning a mistake.
I'm going to own a mistake in this video.
If you haven't seen the Vincent van Soep video,
watch that first or you're probably gonna be lost. So why no historical
reference for the soup throwers? You always provide a mini history lesson
when you have a controversial take. The absence tells me there's no precedent
for this. Maybe if you can't check all the boxes in your own format it's because
they're just entitled little girls out on a lark. It's just a thought. I got to be
honest when I first got this, I was going to do the snarkiest video in the world,
but they're right. The person who sent this is right. I broke with my own format.
I always provide historical context because it's super important. It brings
everything into focus and I didn't do it in that video. So I made a mistake. Now
we're going to turn that mistake into a happy little tree. Consider this part two
of that video. In the early 1900s, 1910s, let's say 1912 to 1915 ish in that range
In London, same city, there was a group of entitled little girls who went after
art in the galleries. Only they didn't just throw soup on a protective casing
or mashed potatoes. They smashed the glass and then they actually cut the art.
I think the most famous one they did it to was Venus. The group was the Women's
Social and Political Union. Who are they right? Especially if you're an American
you've never heard of this group by this name. But you might have heard of them
by another name, the suffragettes. If you don't know, the suffragettes adopted more
and more, let's just say direct means of expressing their displeasure as time
went on. If you're not familiar with the progression, it's definitely worth
looking into. So there is historical precedent for it, and the thing is that's
That's not where the similarities end.
With the suffragettes, it's obvious.
They didn't have a voice.
They didn't have a voice, and they were pushing in that direction.
What about young climate activists today?
You think they feel like they have a voice?
Probably not.
sat there, they've read everything, they know the information, but their politicians are
focused on the bottom line rather than what is probably the most important topic of our times.
When people feel like they don't have a voice, they lash out. Right or wrong, that's what happens.
and to paraphrase somebody much smarter than myself, I can't condemn the lashing
out without condemning the conditions that cause them to lash out. They're
trying to get attention. It's working. It's sparking conversation. The reason
they're doing this is to draw attention to climate change. That's what's
happening? Is it the best tactic? I don't know, but it's not without historical precedent.
It's also worth noting that in an interview with one of the people involved with the Van
Gogh painting, I think they intentionally referenced it. I don't know for sure, but
They talked about how disobedience had given them the right to vote.
And I think that was a reference to it.
I mean, it may just be coincidence, but it seemed to me like they might actually be referencing
this tactic being employed by the suffrage ads.
Now, if you're interested in knowing more about them,
sometimes it's good to put a face to a movement or an idea.
So if you were to hop on Google and type in surveillance
photograph of militant suffragettes,
you'd get images.
And because nobody does irony better than the British,
you will find that photograph at the National Portrait Gallery.
It's a composite of a bunch of suffragettes
and their surveillance photos.
And it includes some of those who
went after the artwork in the galleries
that they're now in.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}