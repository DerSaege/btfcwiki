---
title: Let's talk about right wing commentators and Putin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U3dbWaSgDGI) |
| Published | 2022/10/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the disconnect between what political commentators claim to believe and what they are actually saying.
- Right-wing commentators who advocate for America first are not reflecting American talking points in the context of Ukraine.
- Questioning the fear-mongering tactics employed by these commentators regarding the use of nuclear weapons in Ukraine.
- Pointing out the contradiction in their behavior – claiming to be patriots but not acting in the best interest of America.
- Suggesting that these commentators are undermining the United States by supporting actions that weaken the country.
- Implying that these commentators might not truly support a constitutional republic but instead favor authoritarian rule by elite figures.
- Criticizing their admiration for Putin and the potential implications of his failure in the current conflict.
- Challenging the audience to reexamine the rhetoric they are being fed and whether it truly serves American interests.
- Warning that many individuals may have been deceived by these commentators into supporting positions contrary to American values.
- Concluding with a call for reflection on the audience's alignment with ideologies that may not be in the best interest of the country.

### Quotes

- "They're rooting for a country that has openly said it wants to sow discord in the United States and that it wants the US to fail."
- "Maybe they're not really America first. Maybe they don't really support a constitutional republic, as they claim."
- "They're not nationalists. They're not America first."
- "They just have their audience believing that they are while they feed them talking points that undermine their audience's actual beliefs."
- "Maybe that audience should really start to think about the rhetoric that they're being fed."

### Oneliner

Beau questions the patriotism of right-wing commentators who undermine America's interests while claiming to be patriots.

### Audience

Political observers

### On-the-ground actions from transcript

- Reexamine the rhetoric being consumed (suggested)
- Challenge allegiance to commentators who may not have America's best interests at heart (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how certain political commentators may not truly support the values they claim to, urging viewers to critically analyze the narratives they are presented with.

### Tags

#PoliticalCommentators #Patriotism #Nationalism #AmericaFirst #Authoritarianism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about political commentators
and what they're saying and what they should be saying
if they believed their own rhetoric,
at least if they believed what their brand suggests
they believe.
Because there's a recent anomaly that I think we might
should take a look at.
Because there's a point in time where some commentators
could actually do a lot of good for the United States
and stay on brand if they actually
believed their own rhetoric.
You have a lot of right-wing commentators,
people who call themselves patriots,
people who call themselves nationalists,
people who say America first.
But right now, particularly with Ukraine,
they're not carrying American talking points.
They're really not.
They're out there saying, why are we going to risk
nuclear war over Ukraine?
Why do they think Russia is going to use nukes in Ukraine?
Because they used them when they got beaten in Afghanistan?
Is that what happens?
Do nuclear powers normally use nukes
if they start to lose a war?
Is there a record of that somewhere?
Did we do it?
No.
But they have you scared of that.
Why are you scared of that?
What do they show you?
They show you Russian nationalist commentators
saying, well, we may have to use extreme measures.
And they use it to scare you.
But that doesn't make sense, because if they were really
America first, they wouldn't want
America perceived as weak.
If they really were nationalists,
they'd be saying, oh, you're going to use a tactical device?
You better evacuate St. Petersburg first.
And they would be playing the same game, but they're not.
The one time nationalist commentators could actually
do something good for the country,
and they're too busy polishing Putin's boot.
You have to ask yourself why.
Are they really America first if what they're doing
is actively undermining the United States?
Or do they just use that framing to appeal
to an audience they've tricked?
Maybe they're not really America first.
Maybe they don't really support a constitutional republic,
as they claim.
Maybe they support authoritarian rule by the elites.
And that's why they always point to Putin.
That's why they hold him up.
That's why they talk about how great he is.
And if he loses this war, particularly
to a woke they-them military, it undermines
their entire authoritarian philosophy.
That's the problem.
They're not nationalists.
They're not America first.
They just have their audience believing
that they are while they feed them
talking points that undermine their audience's
actual beliefs.
Maybe that audience should really
start to think about the rhetoric
that they're being fed and wonder
if that's really an America first position.
Wonder if that really is nationalist,
if it's supporting their country the way they think it is.
Because a whole lot of them seem to have been duped.
They're rooting for a country that has openly
said it wants to sow discord in the United States
and that it wants the US to fail.
And that's the side people who cast themselves as patriots
have found themselves on.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}