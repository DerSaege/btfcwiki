---
title: Let's talk about polls and who will win the midterm....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3l3GIuNYGFg) |
| Published | 2022/10/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains that the key question is who will win the midterms, and the simple answer is whoever has the most people show up to vote.
- Points out that polling often shows candidates within one or two points, swinging back and forth within the margin of error.
- Emphasizes that likely voters are typically surveyed in polls, but it will be the unlikely voters who determine the election outcome.
- Notes that it will be the voters who don't usually participate in elections that will decide the midterms.
- Suggests that historically, if unlikely voters show up, they tend to support Democratic candidates.
- Mentions the outlier belief that Republicans handle the economy better, despite evidence to the contrary.
- Asserts that the election outcome is unlikely to change before the election and will be decided by the unlikely voters, who may not vote in every election.
- Concludes by stating that the energized bases will show up, but the key factor will be those who usually don't participate in midterms.

### Quotes

- "Who's going to win the midterms? The answer is, whoever has the most people show up in their favor."
- "This election will be decided by the unlikely voters."
- "It's going to be decided by those people who show up who normally don't and who they vote for."

### Oneliner

Beau explains that the midterm election outcome hinges on unlikely voters showing up and voting, potentially favoring the Democratic Party, despite outlier beliefs about the economy.

### Audience

Voters

### On-the-ground actions from transcript

- Contact unlikely voters in your community to encourage them to participate in the upcoming midterms (implied).

### Whats missing in summary

Insight into the potential impact of energized bases and the importance of voter mobilization efforts.

### Tags

#Midterms #Voting #LikelyVoters #DemocraticParty #ElectionOutcome


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about the most common question
I've gotten over the last few days.
It's being asked at least 10 times a day,
and it's a really simple answer.
The question is, who's going to win the midterms?
The answer is, whoever has the most people show up
in their favor.
And that's not really a snarky answer, not to the degree
that I think people might believe it is.
Yeah, that's true of pretty much any election, right?
But this is a little different.
When you look at the polling, when
you look at the generalized polling, what you find
is that most of them, they're one or two points.
You see the swing back and forth.
But generally speaking, they're within the margin of error
of the poll.
This goes back to what we were talking about, likely voters.
Polls are generally conducted of likely voters, not all voters.
So it will be the unlikely voter who decides this election.
It will be the voter who shows up who normally doesn't.
That's who's going to win.
The people who show up that wouldn't normally vote,
that do, whoever they vote for.
That's who wins.
A lot of the polling is pretty close.
I mean, you have the outlier polls,
and those are the ones that various media outlets
like to tout, because it shows their preferred candidate
however many points ahead.
However many points ahead.
But those are outliers.
When you are looking at the generalized ballots,
it's all pretty close.
And most times, it's within that margin or right at it.
So it's going to be the people who normally don't vote
who decide this election.
And I mean, I guess I really don't like this logic,
but you could say they're going to decide it either way,
whether they're going to show up or not.
And that's their decision.
And I know that argument is going to be made a whole bunch
by people.
But that's not really how elections work.
It's going to be decided by those people who show up
who normally don't and who they vote for.
Now, generally speaking, when you
are looking at most issues, what you find
is that the Democratic Party's position is the one
that most people like.
So if you have a bunch of people show up
who normally don't vote, they will probably
cast their vote for the Democratic candidate.
There are exceptions to this.
The real outlier in this regard is the general idea
of the economy and the demonstrably false notion
that Republicans handle the economy better.
So that's where it's at.
Nothing is going to change this.
I don't see anything happening between now
and the election that is going to alter that fact.
This election will be decided by the unlikely voters.
That's who's going to get to make the call.
The bases, they're energized.
So they'll show up.
It's the people who may not vote in every election,
specifically those who don't vote in midterms, who
are really going to be the deciding factor here.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}