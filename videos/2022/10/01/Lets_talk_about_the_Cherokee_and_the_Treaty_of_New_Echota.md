---
title: Let's talk about the Cherokee and the Treaty of New Echota....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ucz9cPD9V0s) |
| Published | 2022/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Cherokee Nation is seeking fulfillment of a 200-year-old promise from the 1835 Treaty of New Echota for a delegate in the House of Representatives to represent their interests.
- Despite the stipulation in the treaty, the promise has not been honored for almost two centuries, reflecting a historical pattern of neglect towards Native treaties and issues in the American government.
- The Treaty of New Echota, signed through debatable means, resulted in the Trail of Tears, but significant parts of the treaty remain unfulfilled.
- If granted, the Cherokee Nation's delegate in the House of Representatives, Kim Teehee, will be a non-voting member but will have the ability to participate in debates, introduce legislation, and potentially serve on committees.
- Beau advocates for honoring the promises made in treaties, addressing historical issues, and making amends for past injustices towards Native communities.

### Quotes

- "It's probably time for the U.S. government to honor the treaties and include all the words."
- "A little too realistic, I guess. A little bit too representative of the whole."
- "It might be time to do that. Or they could just, you know, forget about those few words again."
- "The Cherokee Nation are asking Congress to give them a delegate in the House of Representatives. They really shouldn't be asking, they should be demanding."
- "It's been almost 200 years and it hasn't happened because, as is often the case when it comes to Native treaties or anything that deals with Natives in the American government, well, let's just sort of ignore that part."

### Oneliner

The Cherokee Nation seeks a delegate in the House of Representatives to fulfill a 200-year-old promise, urging the U.S. government to honor treaties and address historical injustices.

### Audience

Advocates for Native rights

### On-the-ground actions from transcript

- Support the Cherokee Nation's request for a delegate in the House of Representatives (suggested).
- Advocate for honoring Native treaties and addressing historical injustices (suggested).

### Whats missing in summary

The full transcript provides historical context on unfulfilled promises to Native communities and advocates for honoring treaties and rectifying past injustices.


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about treaties
and the trail and the Cherokee Nation and a 200-year-old promise that has gone unfulfilled
that the Cherokee Nation would like to be fulfilled. So the Cherokee are asking Congress
to give them a delegate in the House of Representatives. They really shouldn't be
asking, they should be demanding, because according to the 1835 Treaty of New Echota,
they're entitled to one. It was stipulated in the treaty that they would have a delegate
in the House of Representatives to represent their interests. It's been almost 200 years
and it hasn't happened, because as is often the case when it comes to Native treaties or,
well, really anything that deals with Natives in the American government,
well, let's just forget about that part. You know, those couple of words, they don't really mean much.
The treaty itself was obtained through, well, let's just call it debatable means.
It was signed with a group that is not necessarily representative of the whole
and the treaty led to the Trail of Tears. But a large portion of that treaty
is just not going to be honored, even after what happened.
If the request is granted by the House of Representatives,
the Cherokee Nation would have a non-voting member in the House.
There are other places that have non-voting members. D.C., Guam, the U.S. Virgin Islands, there are others.
They can debate on the House floor. They can introduce legislation and if I'm not mistaken,
they could even sit on committees. So it's not an entirely ceremonial place.
They just can't vote. Now the delegate would be Kim Teehee, I think, and this is somebody
who has a long history of representing Native interests. I would suggest that it's probably time
for the U.S. to honor the treaties, those promises that were made and never really kept.
It might be time to do that. Or they could just, you know, forget about those few words again,
which is often the case. That happened with the Trail of Tears too. The origin of that term,
a Choctaw leader was talking to an Alabama newspaper and described it as the Trail of Tears and Death.
Those last two words, they just forgot about that. That doesn't get included when it's talked about.
A little too realistic, I guess. A little bit too representative of the whole.
It's probably time for the U.S. government to honor the treaties and include all the words
and really address a lot of the issues that have happened and try to make amends or at least honor
the bare minimum promises that were made more than a century ago.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}