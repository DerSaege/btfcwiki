---
title: Let's talk about being polite after public figures cease to be....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VHGAtDPpMTU) |
| Published | 2022/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questioning the appropriateness of being polite after the passing of a monarch, billionaire, or politician.
- Politeness is not for the deceased individual but for the people around you.
- Acknowledges that sometimes being rude can effectively make a point.
- Talks about a figure in US politics and plans to address their impact after they pass.
- Considers that being rude or polite depends greatly on the situation and the cultural context.
- Being polite or rude about a person's passing will be received differently based on the region.
- The focus should be on how your actions are perceived by those around you, not the deceased individual.
- Politeness is a consideration but not a hard rule in these situations.
- Suggests that the passing of influential figures could be a time to advocate for change in policies.
- Ultimately, the decision to be rude or polite should be based on the specific circumstances and cultural norms.

### Quotes

- "Politeness is not for the person who is gone; you're being polite for the people around you."
- "Sometimes being rude is a way to make a point."
- "Being polite or rude depends greatly on the situation."
- "You should probably take regional views into account because you're not being polite for the benefit of the person who's gone."
- "Ultimately, the decision to be rude or polite should be based on specific circumstances and cultural norms."

### Oneliner

Questioning the appropriateness of politeness after the passing of influential figures, focusing on the impact on those around you and considering cultural context.

### Audience

Individuals reflecting on social etiquette.

### On-the-ground actions from transcript

- Respect regional views on politeness (implied).
- Advocate for policy changes following influential figures' passing (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on the dynamics between politeness, making points, and cultural considerations when dealing with the passing of influential figures.


## Transcript
Well, howdy there, internet people, it's Beau again. So today we are going to talk about being
polite when maybe you don't feel the need to, and whether or not it's appropriate to be less
than polite after the passing of a monarch, billionaire, or politician, because somebody
asked for, hey, I'd like your take on whether you should be polite after the passing of a
monarch billionaire politician. I think the question you should ask there is who are you
being polite to? It's not for the person who's gone, right? They don't care. They do not care.
Even if you were to look at it through the lens of spiritual beliefs, there are
few sets of beliefs that suggest the way you're remembered on earth really
matters, right? So you're not being polite for the person who is gone, for the
monarch, billionaire, politician. You're being polite for the people around you.
Now, that sounds like I'm saying you should be polite.
Not necessarily, sometimes being rude is a way to make a point.
If there is a point that can be made at that time and perhaps it's more effective at that
time then I guess be rude. Sometimes that is what it takes. I'm sure this is in
reference a specific event recently. I didn't express any opinion about that
whatsoever overtly other than to express my surprise at the number of people in
my local area that had very strong feelings about it.
But that being said, there is a certain figure in US politics, and it's funny because somebody
asked me to do a video about why they were so horrible for American foreign policy recently.
And I didn't do it because I am waiting until they cease to be.
I have the video planned already.
And the reason I'm going to do it at that point is because this person was very integral
to American foreign policy for a very long time.
There are some good things, but overall it was pretty bad.
Because they were such a large figure in American foreign policy, perhaps the time of their
passing is a time to suggest that our foreign policy changes.
So, it's not like I think it's always the wrong move.
But is there a reason to be rude?
Sometimes there is.
So I don't think that this is a hard rule.
This depends greatly on the situation.
Now if we're talking about the passing of the person I think we're talking about, being
Being polite about it would be received very differently.
Whether you're in Europe or, say, India, there's going to be a different view of it, and you
should probably take that into account because, again, you're not being polite for the benefit
of the person who's gone.
They do not care.
being polite for those people around you. And that's what you should consider. So,
I mean, I know that's not really an answer, but that's my take on it.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}