---
title: Let's talk about teachers, Russians, and supplies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qOxSJp-Sg2w) |
| Published | 2022/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparison between supplies for troops in Russia and the United States.
- Russian troops facing challenges with basic equipment like armor and medical supplies.
- Russian troops being ill-equipped and unprepared for sustained or wider scope wars.
- Mention of a video showcasing a bag with everything needed to treat injuries for troops heading into combat.
- American teachers having better equipment, preparedness, and training to deal with gunfire compared to Russian troops.
- Indictment of both countries for the disparity in equipment between American teachers and Russian troops.
- Importance of not normalizing American teachers having the exact equipment needed by Russian troops in combat.
- Mention of tourniquets and chest seals as critical supplies lacking for Russian troops.
- Suggestion to work on ensuring proper supplies for Russian troops.
- Beau leaves viewers with a thought-provoking message about the supply disparities.

### Quotes

- "American teachers are better equipped, better prepared, and better trained to deal with gunfire than troops heading into Russia that were recently conscripted."
- "That is an indictment of both countries."
- "The tourniquets, the chest seals, this is stuff they desperately need over there and they don't have."
- "Probably something we should work on."
- "It's just a thought."

### Oneliner

Beau compares the disparity in supplies between American teachers and Russian troops, urging to address the critical equipment shortage.

### Audience

Global citizens

### On-the-ground actions from transcript

- Ensure proper supplies for troops (suggested)
- Advocate for better equipment for troops (implied)

### Whats missing in summary

The full transcript provides more context on the ongoing supply challenges faced by Russian troops and the need for urgent action to address this disparity.

### Tags

#Supplies #Russia #UnitedStates #Disparities #GlobalCitizens #Advocacy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk a little bit about supplies in Russia, in the United States.
And a less than comforting reality about, well, both countries really.
There's been a lot of coverage lately talking about the troops that Russia has conscripted,
the newly mobilized troops, and the trouble they are having getting proper equipment,
like armor, really basic stuff, medical supplies, stuff like that.
They're basically being told, hey, here's a uniform and a rifle that may or may not
work. Go have fun.
And that says a lot about the state of the Russian war effort.
And there's a lot of commentary that could be made about the improper readiness that
Russia's supermasculine military had and how they're not really in a position to
fight a sustained war or one that is wider in scope.
But it also brought something else up for me.
There was something the whole time as I'm watching this coverage that was just in the
back of my mind, just gnawing at me.
This bag.
Y'all remember this video?
For those that missed it, inside this bag is everything that a troop heading into combat
would need to treat injury.
I took the bag apart, opened it up, and showed how to use, gave a very speedy demonstration
of how some of the stuff works inside on the channel for teachers in the United States.
I have videos on this channel about how to improvise armor to stop rounds for teachers
in the United States.
American teachers are better equipped, better prepared, and better trained to deal with
gunfire than troops heading into Russia that were recently conscripted.
That is an indictment of both countries.
It is not normal.
It shouldn't be treated as normal that American teachers probably have the exact equipment
that Russian troops in combat need.
That's not something that really should be glossed over.
The tourniquets, the chest seals, this is stuff they desperately need over there and
they don't have.
But there's probably some sitting in almost every school in the US.
Probably something we should work on.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}