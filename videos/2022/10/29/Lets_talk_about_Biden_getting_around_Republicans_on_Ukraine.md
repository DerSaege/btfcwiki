---
title: Let's talk about Biden getting around Republicans on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t1hfLv2bhz0) |
| Published | 2022/10/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains McCarthy's statement and the concerns raised by a segment of the Republican Party about cutting off aid to Ukraine.
- Notes the internal conflict within the Republican Party, with Trump loyalists wanting to cut aid while McConnell seeks greater assistance.
- Breaks down the unlikely steps required for the Republicans to actually stop aid to Ukraine, including winning both chambers, passing legislation, and overriding a potential Biden veto.
- Points out the challenges in tricking Biden into signing such legislation and the limitations of Congress versus the commander in chief in providing aid.
- Emphasizes that even in a veto-proof scenario, there are ways for Biden to work around restrictions and still support Ukraine, making the Republican plan unlikely to succeed.

### Quotes

- "It takes a whole lot for this segment of the Republican Party to get from them talking on Fox News to get a pat on the head from an authoritarian and actually being able to implement it."
- "This wouldn't be a major concern of mine if I had family there."

### Oneliner

Beau explains the unrealistic steps for the Republican Party to block aid to Ukraine and why it's unlikely to succeed.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact political representatives to express support for aid to Ukraine (implied)
- Stay informed about political developments related to foreign aid policies (implied)

### Whats missing in summary

Detailed breakdown of the internal dynamics within the Republican Party regarding aid to Ukraine.

### Tags

#RepublicanParty #UkraineAid #PoliticalAnalysis #BidenAdministration #USForeignPolicy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about McCarthy's statement
and what a certain segment of the Republican Party has said
and how it has concerned a whole lot of people
and whether or not they can actually keep their promise.
We've talked about it on the channel already.
The McCarthy side of things,
the Trump loyalist side of the Republican Party
has said that basically they want to cut off aid to Ukraine.
Now, Mitch McConnell has come out and said,
yeah, I'd like to see greater assistance.
So there's infighting there to begin with,
but that statement led to a whole bunch of people
sending messages asking if they could really do that.
And given the fact that most of them had Ukrainian last names,
I feel like this is a question we should take some time
to kind of delve into what it would actually take
for the American first segment of the Republican Party
to sell out American allies and help Russia.
Okay, so first they have to win,
and they have to win both chambers.
They have to win the House and the Senate.
They have to have control of both.
Then they have to push a legislation through
saying that Biden can't help Ukraine.
They have to get that passed to McConnell,
who is the Republican boss in the Senate.
Let's say that happens.
The next step is that it goes to Biden to sign.
So he vetoes it.
Seems really unlikely that they're able
to even get to this point.
I mean, first they have to win control of both chambers.
Then they have to keep a campaign promise,
not something the MAGA Republican crowd is really known for.
Then they have to find some way to trick Biden into signing it.
The thing is, though, even in that situation,
let's say something bizarre happens,
and they end up with a majority that is veto-proof.
Okay, this isn't a realistic scenario in any way, shape, or form,
but let's just say that it happens.
And they push through legislation
that limits Biden providing aid to Ukraine,
giving them arms or something like that.
He's still the commander in chief,
so he doesn't give them arms.
He can't do that.
Congress said he can't.
Congress has the purse strengths.
Fine, but he's the commander in chief.
So he could say that it's really important
for U.S. national security to put, I don't know,
80 tanks in pick a country in Europe,
and American crews there with them
to defend that country in the event of Russian aggression.
Then that country can provide tanks.
There are hundreds of workarounds,
even in the unlikely scenario
that the MAGA Republican side of things
does manage to push this through.
They can make it difficult.
They can make Biden and the administration
work harder to help a U.S. ally,
but that's about it.
They're not going to be successful
in blocking NATO aid to Ukraine.
That's really not something that's very likely
when you look at all of the steps that they have to take.
So that's where we're at on this.
And given the number of messages,
I felt like we should just kind of walk through the steps.
It takes a whole lot for this segment of the Republican Party
to get from them talking on Fox News
to get a pat on the head from an authoritarian
and actually being able to implement it.
This wouldn't be a major concern of mine if I had family there.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}