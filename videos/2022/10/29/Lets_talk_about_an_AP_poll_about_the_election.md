---
title: Let's talk about an AP poll about the election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jgbxtFqOah4) |
| Published | 2022/10/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the impact of a poll by the Associated Press on the election, Republican Party, and long-term effects of false perceptions.
- Roughly half of Americans believe votes will be counted accurately in the midterms, despite no evidence of widespread voter fraud.
- Questions how politicians reconcile pushing voter fraud rhetoric while urging supporters to vote.
- Warns about the long-term consequences of internalizing the belief that voting doesn't matter.
- Predicts partisan impacts on voter turnout, with the Republican Party being more affected than the Democratic Party.
- Criticizes the Republican Party for perpetuating the idea of insecure elections without evidence.
- Argues that this rhetoric damages the Republican Party's chances in elections by eroding trust in the voting system.
- Compares the situation to a lack of response to a public health crisis, where spreading misinformation harms those spreading it the most.
- Emphasizes that there is no evidence to support claims of widespread voter fraud despite two years of rhetoric.

### Quotes

- "It's wild to watch a group of people listen to somebody who got into office because they were voted in say that, you know, the votes aren't accurate."
- "None. It's been two years and they've strung along half the country with, well, my cousin has the evidence, my uncle has the evidence, I'll release it at this expo."
- "Long term, this is going to be a huge issue for the Republican Party."

### Oneliner

Roughly half of Americans believe votes will be counted accurately in the midterms despite no evidence of fraud, risking long-term damage to the Republican Party.

### Audience

Voters

### On-the-ground actions from transcript

- Challenge misinformation within your community (exemplified)
- Encourage fact-checking and critical thinking among peers (exemplified)
- Support candidates who value truth and transparency in elections (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the dangerous consequences of perpetuating false beliefs about election integrity, urging individuals to critically analyze information and support candidates who prioritize honesty and transparency in elections.

### Tags

#ElectionIntegrity #RepublicanParty #VoterTurnout #Misinformation #PoliticalImpact


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about a really
interesting poll by the Associated Press and its possible effects on the election
and the Republican Party and the long-term impacts of a false perception
and how it might drastically alter the outcome of elections four, six, or eight
years down the road. So, I know, what's the poll? Roughly five out of ten
Americans have a high degree of certainty that the votes will be counted
accurately in the midterms. That's actually an improvement, by the way,
because in 2020 it was only four in ten, which means over the last two years it
really hasn't altered that much. We know which party believes this. This is
something that is very, it's very indicative of a certain set of beliefs.
Now, one thing I do want to point out is that while people believe this, there's
no evidence of widespread voter fraud like anywhere. It's not a thing. It's a
perception encouraged by politicians who count on people showing up to vote for
them to stay in office. It's interesting in the sense that I would love to know
how the people who push this rhetoric square that rhetoric with the fact that
they're constantly telling their supporters to get out and vote. If they
actually believed this, they wouldn't be saying that because it doesn't matter,
right? It's just them lying to their base because they know their base is gullible.
At least they believe it is. Based on this poll, they are. But here's the thing.
What happens long-term if this belief persists and people really start to
internalize the idea that, well, it doesn't really matter? Do you think
they're gonna show up? What do you think that's gonna do to voter turnout?
It's going to be pretty partisan. It's going to impact the Republican Party
far more than the Democratic Party because the Democratic Party isn't
trying to manufacture a grievance to prepare to lose. The Republican Party put
this idea out there and it's gonna have to be the Republican Party that fixes it.
Instead, they're nominating candidates and running candidates and funding
candidates who say that the elections aren't secure, despite all the evidence.
They had two years to produce evidence to say something, anything, was really
wrong with it and they couldn't. At that point, you kind of have to wonder why
they believe it if there's no evidence to back it up. But because it has
resonated with a certain group of people, they're gonna continue to spout this.
They're gonna continue to say it. They're going to continue to cast doubt on the
tool that allows them to stay in office. Because eventually, people will get tired
of the antics. You know, right now they're saying, oh, well you need to show up and
watch the polls, safeguard it, and all of that stuff. Yeah, that's only gonna work
for like an election or two. And then people are gonna realize that they're
not catching anything. But if the rhetoric keeps up, I mean, it just kind of
goes to show that it's all pointless, right? That's gonna be the takeaway.
The Republican Party is damaging their chances at elections for years with this.
This gets internalized. People really start to believe this and hold on to this.
They're not going to take the time out of their day.
It's wild to watch a group of people listen to somebody who got into office
because they were voted in say that, you know, the votes aren't accurate.
The cognitive dissonance there is just amazing. And this is going to end up being
like the lack of a response to the public health issue. The people who are
putting out the bad information, it's going to damage them the most.
I want to say one more time, there's no evidence. There is no evidence to back up this idea.
None. It's been two years and they've strung along half the country with,
well, my cousin has the evidence, my uncle has the evidence, I'll release it at this expo.
But it never comes because it doesn't exist. Because they don't have it.
They tricked you. They tried to energize you. They tricked you.
They fell into the rhetoric and now they're stuck with it.
And none of them have the ability to just walk out and tell the truth
because they've conditioned their base to believe it now.
Long term, this is going to be a huge issue for the Republican Party.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}