---
title: Let's talk about Trump endorsed candidates and swing voters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RRgZoOI7Tl4) |
| Published | 2022/10/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses American swing voters, independents, and moderates, discussing their rejection of Trump due to divisiveness, lies, anger, and hate.
- He points out that Trump-endorsed candidates are essentially mini-Trumps, following his orders and talking points directly.
- Referring to a specific incident in Arizona, Beau mentions Trump instructing a candidate to lie more about the 2020 election to appease his radicalized base.
- Beau warns that Trump-endorsed candidates embody hate, anger, ineffectiveness, and division because they simply follow Trump's directives.
- Despite Trump being viewed as a losing candidate, Beau stresses that as long as he controls his endorsed candidates, he maintains power without winning elections.
- Beau questions the false narratives spread before the election about "Biden's America" with riots and chaos, attributing those images to Trump's time in office.
- He urges swing voters to recall why they rejected Trump and to understand that Trump-endorsed candidates will exhibit the same negative traits.
- In summary, Beau cautions against supporting Trump-endorsed candidates as they represent the same divisive and harmful characteristics as Trump himself.

### Quotes

- "They're taking their orders directly from him."
- "That hate, that anger, that ineffectiveness, that division, that's what you can expect from Trump-endorsed candidates."
- "Where did those images come from? They came from Trump's time in office."
- "They're going to be exactly what you rejected."
- "It's the same hate, the same lies, the same division, the same ineffectiveness."

### Oneliner

American swing voters warned against supporting Trump-endorsed candidates embodying hate, lies, and division, mirroring the damaging traits of the former president.

### Audience

American swing voters

### On-the-ground actions from transcript

- Inform fellow swing voters about the dangers of endorsing Trump-backed candidates (implied)

### Whats missing in summary

The emotional impact of rejecting Trump's divisive politics and the importance of remembering his harmful influence on endorsed candidates.

### Tags

#AmericanPolitics #Trump #SwingVoters #Election2022 #Moderates


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about considerations the American swing voter,
the independents, need to keep in mind, especially in regards to Trump-endorsed candidates.
See, that voter, that independent, that moderate, that swing voter,
they rejected Trump outright, by a wide margin.
They're just like, no, can't do it anymore.
Too divisive, too many lies, too much anger, too much hate.
But now you're looking at the midterms and you're seeing a lot of Trump-endorsed candidates
kind of start to rebound from that.
The reality is these candidates are just mini-Trumps.
They're taking their orders directly from him.
They're taking those talking points directly from him.
There's a clip of masters out there in Arizona on the phone with Trump.
And Trump is telling him to, well, basically lie more about the election, about the 2020
election.
That that's how he wins, because he has to stay true to that base,
that radicalized base.
That's who they're catering to.
Trump says, you know, look at Kerry.
They ask her, you know, how's your family?
And she talks about the election, makes false claims about the election.
That hate, that anger, that ineffectiveness, that division, that's what you can expect
from Trump-endorsed candidates, because they're just doing whatever he tells them to.
You know, there's a lot of people looking at American politics who understand Trump
is a losing candidate.
But as long as he has control over those candidates he endorses, he's not out of power.
He can't win an election, but he can control.
One thing that I want to kind of draw everybody's attention to is, you know, right before the
election you had all those memes show up on social media.
This is Biden's America.
This is the America Biden wants.
And you know, it's got flames in the background.
There's riots, all of this stuff, right?
It's the end of 2022.
Where is that?
Weird.
I don't think it didn't happen.
It's strange.
Where did that footage come from?
Where did those images come from?
They came from Trump's time in office.
It's not Biden's America.
That's Trump's America.
And if you put forth a bunch of candidates who are endorsed and controlled by Trump,
that's exactly what you're going to get back.
Because it's the same hate, the same lies, the same division, the same ineffectiveness,
the same amount of fleecing the taxpayer.
The moderates in the independents need to remember why they rejected Trump en masse.
And they need to remember that those Trump endorsed candidates are beholden to behave
the exact same way.
They're going to be exactly what you rejected.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}