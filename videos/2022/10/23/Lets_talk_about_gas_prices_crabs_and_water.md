---
title: Let's talk about gas prices, crabs, and water....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QqdEVoIx5nE) |
| Published | 2022/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Connects crabs, water, and gas prices to urge Americans to pay attention and think long-term.
- Mentions the impact of climate change on crab season in Alaska.
- Expresses how food supply interruption due to climate change is a scarier concept than just a closed crab season.
- Compares the volatility of the global food market to that of oil prices based on supply.
- Warns about the potential future volatility in basic food prices and eventually water.
- Optimistically believes in the possibility of change and mitigation but stresses the need to act quickly.
- Criticizes the influences of big oil companies and commentators in hindering the transition away from fossil fuels.
- Emphasizes that transitioning from gas is in everyone's best interest to avoid worsening market volatility and emissions.
- Reminds that politicians often lie and that those dismissing climate concerns are not acting in the public's best interest.
- Urges viewers to see through the rhetoric and take action towards a sustainable future.

### Quotes

- "The food supply was interrupted because of climate change."
- "Transitioning away from gas is in your best interest."
- "Those who are telling you you don't need to worry about the climate, that we don't need to transition, and in a quick manner, they're lying to you."

### Oneliner

Beau connects climate change impacts on crabs, water, and gas prices, urging immediate action for a sustainable future and criticizing those misleading the public on transitioning away from fossil fuels.

### Audience

Americans

### On-the-ground actions from transcript

- Transition away from gas to support a more stable market and reduce emissions (implied).
- Challenge misinformation and rhetoric from politicians and commentators invested in oil companies (implied).
- Advocate for funding transitions towards sustainability in the country (implied).

### Whats missing in summary

The full video provides more context on the interconnectedness between climate change, food supply, and market volatility, urging viewers to rethink their consumption habits and demand governmental action for a sustainable future.

### Tags

#ClimateChange #FoodSupply #MarketVolatility #TransitionAwayFromGas #Sustainability


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
crabs and gas prices and water and what they all have to do with each other and why the
American people really need to start paying attention and start thinking beyond the immediate
here. Crabs, water, gas prices, these are all things we've talked about on the channel
within the last few days and they're all connected. You know, people talk about the instability
of gas prices and how volatile it is and how hard it makes being a working class American.
Yeah. We've talked about what's going on up there in Alaska with the crab and the conventional
wisdom right now is that that is due to climate change. No crab season. And that's a way to
say it. We're not going to have crabs. No crab season. That is certainly one set of
words that can identify this. Crab season is closed because of climate change. You know
another way to say that? The food supply was interrupted because of climate change. That's
another way to say it. Way scarier when you say it that way though, isn't it? We don't
think of it that way because for most people crab isn't a staple. It's not something that
we find on our tables super often. But it is in fact food and it is being interrupted
because of the climate. Food is a global market much like oil. The reason oil is so volatile,
the reason gas prices go up and down so much is because there's a daily price based on
supply. What are we going to do when food is in that same situation? There are a lot
of people who are so ingrained in yesterday and silly sayings that came from their dad
in the 80s that they're missing the big picture. If we don't transition quickly these problems
are going to get worse. They're going to get worse anyway, but they're going to become
unmanageable if we don't transition quickly. And your concern that you face with gas prices
and the price changing every week, every day, you're going to have that with basic food
stuffs and eventually you'll have it with water. I'm one of those people who's pretty
optimistic about this. I think we can change and I think we can mitigate a whole lot of
it, but we have to start. Every time there's an actual push to really fund transitioning,
making this country more secure, people in our government beholden to big oil, they try
to stop it. Commentators invested in oil companies try to stop it. They feed that rhetoric and
they convince people that it's not in their best interest. Understand this, if you have
ever complained about the price of gas, transitioning away from gas is in your best interest because
the longer we continue to use it, the more volatile that market's going to become. The
more emissions are going to be spewed out and the more volatile other markets are going
to become. You can sit there and let these people in DC, who undoubtedly have their plans
together, tell you in Texas that you don't need to worry about it. But long before America
became so polarized that nobody listened to the other side, there was one common thing
that pretty much every American believed, and that's that politicians would lie to you.
Those who are telling you you don't need to worry about the climate, that we don't need
to transition, and in a quick manner, they're lying to you. They don't care about you. You're
going to be the one who pays the price for it, or your kids. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}