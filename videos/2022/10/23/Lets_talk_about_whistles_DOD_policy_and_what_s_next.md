---
title: Let's talk about whistles, DOD policy, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PewbsfDfaLo) |
| Published | 2022/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Defense implements a new policy allowing service members to travel for health care or family planning.
- Republicans criticize the Biden administration, accusing them of an election season stunt, but the policy has been in effect since June.
- Secretary of Defense prioritizes the health and well-being of service members, civilian workforce, and families.
- The policy ensures seamless access to reproductive health care without burning leave time.
- Decoding messages from the Secretary of Defense is not difficult; readiness is a key priority.
- Anti-LGBTQ legislation and restrictions on women's rights impact military readiness.
- If service members refuse to re-enlist due to discriminatory laws in certain states, it affects military readiness and base existence.
- Politicians must explain to constituents why their livelihood tied to military bases might be at risk due to wedge issues.
- Impact on readiness could lead to closures of bases and unemployment in surrounding communities.
- Red states heavily rely on military presence, but discriminatory laws could drive the military away.

### Quotes

- "Nothing is more important to me or to this department than the health and well-being of our service members."
- "Readiness is one of the most critical things in the military."
- "If they don't re-enlist because of that, it becomes a readiness issue."
- "You're chasing them away because you're impacting readiness."
- "The Republican Party and their wedge issue trying to motivate their base is going to drive their states even deeper into financial turmoil."

### Oneliner

The Department of Defense implements a policy for service members' health care and family planning, while political wedge issues threaten military readiness and base existence.

### Audience

Military personnel, policymakers, community members

### On-the-ground actions from transcript

- Advocate for inclusive policies within the military (exemplified)
- Support service members affected by discriminatory legislation (exemplified)
- Educate communities on the importance of military presence (exemplified)

### Whats missing in summary

The full transcript includes detailed insights on how discriminatory legislation impacts military readiness and community livelihoods.

### Tags

#DepartmentofDefense #MilitaryPolicy #PoliticalImpact #DiscriminatoryLegislation #CommunitySupport


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about a new
policy within the Department of Defense and a message from the Secretary of Defense that seems
to have caught a lot of Republican politicians by surprise. It shouldn't have. It should have been
plainly obvious that this was going to happen, but somehow they're surprised by it and they are
calling it an election season stunt and all of this stuff. And we'll get to that part. But first,
what's the new policy? Well, it establishes travel and transportation allowances for service members
and their dependents if they need to leave the installation they're at and travel to another
state for health care, for family planning. See, the federal government, there's a law,
and agencies within the federal government can't spend money on certain kinds of family planning.
They can't perform them either in a whole lot of cases, in most cases.
But there's nothing stopping them from allowing their employees,
meaning service members of the U.S. military, to travel somewhere else and pay out of pocket.
Now, normally this was done through a leave process, but now there will be administrative
absences. So they don't even have to burn leave time to do this. This has upset some Republicans
who are accusing the Biden administration of doing this last minute as an election season stunt.
Yeah, this has been de facto policy since June. You just didn't know it.
I'm going to read you a message. This is from the Secretary of Defense.
Nothing is more important to me or to this department than the health and well-being
of our service members, the civilian workforce, and DOD families. I am committed to taking care
of our people and ensuring the readiness, remember that word, and resilience of our force. The
department is examining this decision closely and is working with the Department of Defense
closely in evaluating our policies to ensure we continue to provide seamless access to
reproductive health care as permitted by federal law. That continues the important part, because
this workaround with the leave, that's been around for a really, really long time. Now it's been
formalized and they don't even have to burn leave time for it. So it's not an election season stunt.
It's been going on since June and y'all didn't know it. And I know there's a whole lot of people
who may be like, well, that really isn't what that message means. It means something else.
We just, we saw that at the time. It's not that. He was talking about something else.
So that's his message on June 24th. Here's me referencing it on June 25th.
When SecDef dog whistles to the entire force, y'all heard him, make sure commanders approve
the leave that needs to be approved. Context, the military has tons of restrictions about what
they can and can't do on military installations. The concept of at commander's discretion does a
lot of heavy lifting in leave approval to hypothetically travel to another state.
It's exactly what's happening.
So to those politicians who were surprised by this,
we've established that decoding a message like that isn't really that hard.
But I want to tell you what's coming next because that word readiness is coming up again.
When the Republican Party went on its binge at the state level,
passing all of the anti-LGBTQ legislation and legislation restricting women's rights,
I talked about this and I talked about how readiness is one of the most important things in the military.
And that's what he's citing, readiness.
Do you know what happens if people don't want to go to a specific military installation because it's in red state X
and they don't want to be treated like second class citizens?
If they don't re-enlist because of that, it becomes a readiness issue.
Do you know what happens to that installation? It doesn't exist anymore.
There's a whole bunch of politicians who need to figure out how they're going to explain to their constituents,
to those people who live around those bases, who have their livelihood tied to that base,
they're going to have to explain to them why they're going to be out of a job over that politician's wedge issue.
Because if this continues and these laws that are being put into place,
when they start impacting readiness, those bases won't exist.
They'll start with those bases not being selected for expansion and then they'll start moving commands.
And every time that happens, a whole bunch of people outside that base become unemployed.
The Republican Party and their wedge issue trying to motivate their base is going to drive their states
even deeper into financial turmoil.
A lot of red states depend on the military presence.
You're chasing them away because you're impacting readiness.
It's something that politicians probably need to get ready to answer for.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}