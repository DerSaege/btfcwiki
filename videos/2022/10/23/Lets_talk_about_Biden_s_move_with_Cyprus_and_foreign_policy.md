---
title: Let's talk about Biden's move with Cyprus and foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=txqpMJTdqRQ) |
| Published | 2022/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent decision by the Biden administration to lift the arms embargo against Cyprus.
- Cyprus has had an arms embargo since the late 80s, and now they can start acquiring arms again.
- The reason behind this move is not about what Cyprus is going to buy but what they already have.
- Cyprus possesses military hardware like tanks, infantry fighting vehicles, rocket artillery, and air defense from Warsaw Pact manufacture.
- It is likely that Cyprus will seek new American equipment to replace their existing weaponry.
- Beau speculates that the US is providing Cyprus with new arms and, in return, Cyprus will pass on some of their equipment to Ukraine.
- The move is not to provoke Turkey but to assist Ukraine with capable military equipment.
- The US is playing a strategic game in the international arena, making moves that may seem puzzling at first glance.
- Beau suggests that the deal to provide Cyprus with new equipment and pass on the old to Ukraine may have already been established.
- The decision is part of a larger foreign policy strategy, and one must always anticipate the next steps in such moves.

### Quotes

- "The United States is sliding Cyprus a card, and in return, they're going to slide one to Ukraine."
- "It's not that the United States or the Biden administration is trying to alienate Turkey. It's just right now, they're cheating in a different direction."
- "But when you look at foreign policy moves, always wonder what happens next."

### Oneliner

Beau explains the US decision to lift the arms embargo against Cyprus as part of a strategic move to assist Ukraine, not alienate Turkey, in the international poker game.

### Audience

Foreign policy enthusiasts

### On-the-ground actions from transcript

- Contact organizations supporting diplomatic efforts for peace in regions affected by arms trade (implied)
- Support initiatives advocating for transparency in international arms dealings (implied)

### Whats missing in summary

Insights on the potential implications of this arms deal on regional dynamics and future US foreign policy decisions.

### Tags

#ForeignPolicy #US #Cyprus #ArmsEmbargo #Ukraine #Turkey


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk a little bit about foreign policy
and Cyprus and Turkey and the United States
and why the United States did something that
I guess people have questions about
because I got a question about it
and it leads to showing how things work
on the foreign policy scene
and why sometimes things don't appear to make any sense,
but at that international poker game where everybody's cheating,
it makes total sense.
The question was,
why did the Biden administration lift the arms embargo
against Cyprus?
Isn't that going to make Turkey mad?
I mean, yeah, probably it is.
I mean, that much is true.
But the real question is, why'd they do it?
If you don't know, Cyprus has had an arms embargo against it
since the late 80s.
I think there were some adjustments relatively recently,
but for the most part, it's still intact.
So why would the Biden administration do this
like out of nowhere and say,
yeah, you can start getting arms like in the next 30 days
or something like that?
It has to do not with what they're going to buy,
but with what they already have.
Cyprus has a very eclectic collection of military hardware.
And I think what is most likely to be replaced
would be the AK-74Ms, the RPG-7Vs, the T-80Us.
Those are tanks that they have like 80 of.
The BMP-3s, which are infantry fighting vehicles
like armored cars, they have about 40 of those.
They have some grads, rocket artillery
that is of Warsaw Pact manufacture.
They have normal artillery, about 100 pieces,
that is of Warsaw Pact manufacture.
They have air defense that is of Warsaw Pact manufacture.
I would imagine that Cyprus would want all new,
brand new American stuff.
What do you think is going to happen with that?
Incidentally, the Ukrainian military
knows how to use every single piece of that.
The international poker game where everybody is cheating,
the United States is sliding Cyprus a card,
and in return, they're going to slide one to Ukraine.
That's my guess.
I don't know this for a fact.
But I mean, it tracks.
That, to me, is the only reason to want this country
to have better arms and lift the embargo so quickly.
My guess is the deal's already been made.
The United States is going to allow
them to buy this new equipment, and the stuff
that they don't need anymore is going
to be shipped to Ukraine.
And some of this stuff is decent.
It's all capable, but some of it's actually pretty good.
So it's something that will assist Ukrainian forces there.
It's not that the United States or the Biden administration
is trying to alienate Turkey.
It's just right now, they're cheating
in a different direction.
It's, I mean, that's the simple math.
And I think that that's why they would be doing it.
I mean, I can't really imagine another reason,
given the current climate.
But when you look at foreign policy moves,
always wonder what happens next.
What's the next step when the next set of cards gets dealt?
And normally, you'll find the answer.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}