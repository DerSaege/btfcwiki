---
title: Let's talk about the NY AG's move and Trump Org II....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PZPdX4blyHA) |
| Published | 2022/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New York Attorney General James seeks court approval for an independent monitor to oversee Trump Organization assets to prevent disposal.
- Concerns arise over a Delaware company, registered as Trump Organization 2, potentially being used to offload assets out of reach of New York courts.
- Allegations against Trump Organization include overvaluing assets in its portfolio, leading to financial penalties of around a quarter of a billion dollars.
- New York state aims to track and safeguard assets until the trial in October 2023.
- A hearing on the appointment of an independent monitor is scheduled for October 31st.
- The move is not about freezing Trump's assets, but ensuring they are available if the state of New York takes hold of them.
- Monitoring and approval of financial dealings will be in place to prevent Trump from freely engaging in business or offloading assets.
- With significant money at stake, attempts to shield assets are expected.
- New York's Attorney General's office is proactive in trying to prevent potential asset offloading.
- The situation underscores the importance of ensuring accountability in high-stakes financial dealings.

### Quotes

- "New York Attorney General seeks oversight of Trump Organization assets to prevent disposal."
- "Concerns over offloading assets to a Delaware company to avoid New York courts' reach."
- "Allegations of asset overvaluation by Trump Organization lead to hefty financial penalties."
- "Efforts to ensure asset availability until trial in October 2023."
- "Monitoring and approval of financial dealings aim to prevent asset offloading."

### Oneliner

New York Attorney General seeks oversight of Trump Organization assets to prevent disposal and ensure availability until trial, amid allegations of overvaluation and hefty financial penalties.

### Audience

Law enforcement agencies

### On-the-ground actions from transcript

- Attend and support the hearing on October 31st regarding the appointment of an independent monitor (implied).
- Stay informed about updates on the case and the trial set for October 2023 (generated).

### Whats missing in summary

Detailed insights on the specific allegations and schemes involving asset overvaluation by Trump Organization. 

### Tags

#NewYork #AttorneyGeneral #TrumpOrganization #FinancialPenalties #AssetOversight


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the New York Attorney General's Office
and Trump and Trump Organization
and a possible game of hide-and-seek
that the Attorney General's Office doesn't want to play.
So the New York Attorney General James
has asked the courts there to
kind of approve an independent monitor
to keep track of Trump Organization assets
and make sure that they aren't disposed of.
The reason this is occurring is
there's an organization, an LLC, in Delaware
that has registered with New York as Trump Organization 2.
The concern is that Trump Organization will offload assets to this organization
and because it's a Delaware company,
it would be out of the reach of New York courts.
Now keep in mind the Attorney General's Office
is alleging a long-running scheme
where Trump Organization overvalued assets in its portfolio
and the financial penalties that go along with this allegation
reach the tune of like a quarter of a billion dollars.
So the state of New York has a vested interest
in trying to keep track of that
and make sure it doesn't just disappear,
leaving only footprints behind.
Now if this independent monitor is put in place,
it would keep track of and approve financial dealings
until October of 2023,
which I believe is when the trial is set to occur.
A hearing on this is set to happen on October 31st.
Now one of the things to note here
is that this isn't freezing Trump's assets
in the way that it's being framed in some places.
This isn't freezing the assets under the belief
that it's a foregone conclusion
that the state of New York is going to take hold of them
and end up with them.
It's more of a method of making sure
that those assets are available if that does happen.
It wouldn't prevent Trump necessarily
from engaging in business or offloading assets,
just that they would be monitored
and that the court would be very aware
of what's going on and approve them.
So that's what's happening there.
This is kind of unsurprising.
When you're talking about this much money at stake,
they're going to attempt to shield
themselves, so it's not really surprising.
The Attorney General's office does seem to be kind of on the ball
and is attempting to preempt that type of move.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}