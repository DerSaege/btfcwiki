---
title: Let's talk about Elon Musk, Ukraine, and Starlink....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hsFL__tR4DM) |
| Published | 2022/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk has been providing a space-based internet service to Ukraine, which is financially beneficial for him as a great advertisement.
- Musk is now asking the Pentagon to cover the costs of the service as he can't afford to pay for it indefinitely.
- Some critics view Musk's actions as a shakedown, threatening to cut off communication services for Ukrainian troops if not paid.
- Despite emotional reasoning against Musk, the practicality of Starlink as a battlefield tech for Ukrainian troops is paramount.
- The short-term solution proposed is for the Pentagon to pay around $30 million a month for the service, as there is no comparable alternative available.
- Threatening to withdraw communication services in the middle of a war could have long-term consequences for Musk and his companies within the defense industry.
- Musk's actions are seen as profiting from human suffering, and if he wants to play that game, he needs to understand and abide by the rules.
- It is expected that the Pentagon or another entity will eventually cover the costs, but the memory of Musk's actions will linger.
- The situation is perceived as a shakedown where people are asked to pay to prevent dire consequences, but the lack of replacement services necessitates paying for the service.
- Musk may have underestimated the repercussions of his actions within the defense industry.

### Quotes

- "He wanted to play the game. He wanted to get that free advertising. He probably should have learned the rules to the game first."
- "People are viewing it as a shakedown because it very much is. Pay me this money or these horrible things are going to happen to these people."
- "There's not a replacement service yet. So cut the check."

### Oneliner

Elon Musk's Starlink service in Ukraine raises ethical concerns as he seeks Pentagon payment, viewed as a shakedown, while offering vital connectivity for troops.

### Audience

Defense industry stakeholders

### On-the-ground actions from transcript

- Cut the $30 million monthly Pentagon payment for Starlink service (implied)
- Develop alternative communication systems for military use (implied)

### Whats missing in summary

The full transcript provides detailed insights into the ethical implications of Elon Musk's actions concerning providing Starlink service to Ukraine and seeking payment from the Pentagon. Viewing the complete transcript can offer a deeper understanding of the nuances involved in this situation.

### Tags

#ElonMusk #Starlink #Ukraine #Pentagon #DefenseIndustry


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Elon Musk and Starlink in Ukraine and the recent developments
and people's reactions.
So if you don't know what's going on, Elon Musk has been providing a service to Ukraine
and this is, if you're not familiar with the product, it is space-based internet.
He's been providing this service and paying for it.
Why?
Because it's good for him financially.
This is the best advertisement a service like that could ever get.
It was beneficial to him financially.
He's been providing this service for a few months and now he's saying, well, you know,
we can't afford to pay for it forever.
And he's asking the Pentagon to foot the bill.
There are a lot of people who are providing commentary on this who are basing it on emotional
reasoning.
They don't like Elon Musk in general.
They don't like his Twitter deal.
They don't like his recent foreign policy takes, which were really bad.
Or they don't like the fact that this certainly seems like a shakedown.
We have people in harm's way.
They're dependent on my communication system.
Pay me or I'm going to cut it.
Understand if he cuts it, people will cease to be.
It will cause the end of people.
Okay, so all of that emotional reasoning, is it worth anything?
No.
Because this is what actually matters.
Starlink has proven itself as a low-end battlefield tech.
It's worked pretty well, considering this isn't really what it was designed for.
It has worked pretty well.
So the short-term solution, what should the Pentagon do?
Cut the check.
Cut the check.
You're looking at, I want to say it's right around $30 million a month.
Is there an alternative solution for $30 million a month that can provide that level of connectivity
for the Ukrainian troops across all of those different systems in an area without infrastructure?
There's not.
There's nothing really comparable.
So cut the check in the short-term.
And then, Mr. Musk is going to find out that the defense industry has a really long memory.
Threatening to yank comms in the middle of a war, that's not going to be remembered fondly.
They will remember that and his companies will suffer because of it when they come up
for stuff.
That is an almost guarantee.
It wouldn't surprise me if a similar system was developed specifically for the Army outside
of Elon's enterprises.
And I know that may bother some people, but when you're talking about a war and profiting
off of that war, if you're going to join the little club of people making money off of
human suffering, well if you're in for a penny, you're in for a pound.
You can't pull your services in the middle of the war.
That's just not how it's done.
He wanted to play the game.
He wanted to get that free advertising.
He probably should have learned the rules to the game first.
So my guess is that the Pentagon or maybe some other country within NATO, somebody is
going to cut the check for this.
Perhaps some other agency, maybe a civilian one, is going to end up cutting the check.
They'll pay for this service, but they will remember this.
They will remember this.
I don't think I've ever quite seen something like this with somebody dealing with the defense
industry.
This is, people are viewing it as a shakedown because it very much is.
Pay me this money or these horrible things are going to happen to these people.
That's really the essence here.
But there's not a replacement service yet.
So cut the check.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}