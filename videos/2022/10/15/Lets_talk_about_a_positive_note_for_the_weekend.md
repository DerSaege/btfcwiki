---
title: Let's talk about a positive note for the weekend....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=q8ut-MA9UjM) |
| Published | 2022/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Credits the channel for inspiring him to go to law school, mentioning a video about being part of a community network.
- Explains that he excels at school, so he pursued law school, passing the bar exam in his late 40s while working full time.
- Acknowledges the broken justice system and the need for individuals to take action.
- Emphasizes that everyone, regardless of their skills, can contribute to making the world better.
- Encourages involvement at the local level to effect systemic change.
- Stresses the importance of individuals using their abilities to improve society.
- Congratulates Shelby for achieving her goals and hopes he never needs her legal services.

### Quotes

- "It doesn't matter what you're good at, whatever you enjoy, is needed to make the world better."
- "If you want to make those changes, if you want to achieve that deep systemic change that most people watching this channel want, it starts with you."
- "Doing what you can, when you can, where you can, for as long as you can."
- "It doesn't matter what you're good at because the goal is to create a better world, a better society."
- "Every skill set, every talent, everything. It takes the drive to get there."

### Oneliner

Beau inspires taking action to create systemic change by utilizing individual skills for community betterment.

### Audience

Community members

### On-the-ground actions from transcript
- Join local community initiatives (implied)
- Use your skills to contribute to community improvement (implied)

### Whats missing in summary

Importance of community engagement and using individual skills for societal progress.

### Tags

#CommunityEngagement #IndividualSkills #SystemicChange #LocalAction #JusticeSystem


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to kind of start the weekend off
with a positive note.
Bo, I don't know if you'll remember me.
I decided to go to law school in large part due to your channel.
It was the video where you explained to people
that you don't need a specific talent to be
part of a community network.
He said that we should figure out what we're good at and do that thing to the best of our
ability.
I knew I was good at school, so I decided to go to law school.
Today, I found out I passed the bar exam.
Going to law school in my late 40s while working full time hasn't been easy, but I wanted to
do something given the direction our country seems to be going in.
It still doesn't feel real.
I've worked so hard for so long.
hard to believe I'm done, but I am. Congratulations. And you're going to be
needed because say it with me, our justice system is broken. That question,
what can I do? It's one of the most common questions I get on this channel
And this is the right way to do it.
People have this image of what it takes to affect change, to make the world a better
place.
And often times it's based on a perception of activism that's really based on movies.
It doesn't matter what you're good at, it does not matter.
Whatever it is, whether you're a plumber, an artist, somebody who's good at school,
it doesn't matter.
skill, whatever it is, whatever you're good at, whatever you enjoy, is needed to
make the world better. And there are no exceptions to that. If you want to
make those changes, if you want to achieve that deep systemic change that
most people watching this channel want, it starts with you. It starts with you
getting involved at the local level and fixing your community in ways that you
can. Doing what you can, when you can, where you can, for as long as you can. If
you have the means, you have the responsibility type of thing. There are
no exceptions to this. It doesn't matter what you're good at because the goal
is to create a better world, a better society, and that requires everybody in
society. Every skill set, every talent, everything. It takes the drive to get
there. And this is an example of somebody pulling it off. Congratulations, Shelby.
I hope I never need your services. Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}