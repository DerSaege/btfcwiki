---
title: Let's talk about the end to Trump's week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ISw-Tn8vyvI) |
| Published | 2022/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mark Short, a Key Pence aide, compelled to testify before grand jury amidst Trump's claims of executive privilege.
- Kash Patel also present during Short's testimony.
- Speculation on witnesses' willingness to commit perjury for Trump.
- Paul Ryan excludes Trump from list of likely Republican presidential nominees for 2024, calling him "dead weight."
- Trump responds to January 6th committee subpoena with a letter echoing baseless election claims.
- Beau plans to cover New York Attorney General's case and monitoring of Trump organization's assets.
- Trump team's emergency petition to Supreme Court for document classification markings denied.
- Supreme Court rulings against Trump post-appointment, indicating his misunderstanding of lifetime appointments.

### Quotes

- "Trump's name wasn't on it. In fact, he went out of his way to say that Trump is, well, just dead weight."
- "Have him answer that under oath in front of his base, who he has repeatedly misled to the point of putting them in financial and legal jeopardy."
- "They owe you nothing. And there's nothing you can do about it."

### Oneliner

Key developments from Trump's challenging week: Witnesses testifying, Paul Ryan's snub, Trump's response to subpoena, and Supreme Court's denial.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed on the legal proceedings and developments surrounding Trump's various cases (suggested).
- Support transparency and accountability in political processes by advocating for thorough investigations (implied).

### Whats missing in summary

Insight into the potential implications of these legal battles on Trump's political future and the broader political landscape.

### Tags

#Trump #LegalBattles #2024Election #Accountability #SupremeCourt


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the really bad end
to Trump's week and just kind of run through
some of the developments that we didn't cover
and then go back and touch on a few new developments
and things that we did cover.
Okay, so first and maybe most important is Mark Short,
who was a Key Pence aide.
He has been compelled to testify before the grand jury.
That is what reporting suggests.
Now, he has reportedly talked to them before,
but there were some things he wouldn't answer
because Trump had asserted executive privilege.
It appears as though the judge has kind of tossed
Trump's claims of executive privilege.
It is worth noting that at the same time Short was there,
Kash Patel was sitting outside that same grand jury room.
I do not know how willing some of these potential witnesses
are going to be to commit perjury and go to jail for Trump.
This is moving the case right along on the January 6th front,
the federal case up there.
Moving to something less legal,
but just as likely to give Trump a little bit of a headache,
Paul Ryan put out his list of the most likely
nominees for the Republican presidential nomination in 2024.
Trump's name wasn't on it.
In fact, he went out of his way to say that Trump is,
well, just dead weight.
It's unelectable and will become more unelectable
as time goes on.
Might possibly be the first time I've ever agreed
with Paul Ryan about, well, anything.
Okay, so I'm going to go ahead and move on to the next case.
Okay, so now back to the subpoena
that the January 6th committee is sending.
Trump sent a very lengthy, rambling, old man yelling at clouds
letter to the editor response.
As predicted, he's echoing all of the baseless claims about the election
because he views this as a political process.
He kind of gave a roadmap to the committee
in the eventuality that he does testify live on camera.
I would ask if he has any evidence to back up
any claim that he made in his letter
and have him answer that under oath
in front of his base, who he has repeatedly misled
to the point of putting them in financial and legal jeopardy.
We talked about the New York Attorney General's case.
If we didn't, there will be a video coming out about it,
if we haven't already talked about it, if it hasn't already published.
And what's going on there as far as kind of monitoring
Trump organization's assets.
And then we have the Supreme Court case.
In the documents case, not the January 6th case,
not the New York case, not the George case,
but the documents case,
the Trump team filed an emergency petition for relief
or something like that with the Supreme Court
asking them to put the documents that had classification markings
under the purview of the special master.
And the Supreme Court basically told him to kick rocks.
I mean, they didn't really tell him that.
They didn't tell him anything because they didn't even hear it.
I am sure that he is super upset with his judges.
I think Trump had a very different expectation
of how the Supreme Court was going to behave once he appointed them.
This is quite a few rulings against him since they have been appointed.
Maybe Trump should have taken basic civics
and understood that these are lifetime appointments.
As soon as they're appointed, they owe you nothing.
And there's nothing you can do about it.
Anyway, it's just a thought. I hope you have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}