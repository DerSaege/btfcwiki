---
title: Let's talk about Trump's Jan 6 subpoena....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U6xUOBheiJU) |
| Published | 2022/10/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The committee decided to subpoena former President Donald J. Trump, sparking speculation and commentary on the situation.
- Beau suggests that bringing Trump in for questioning could be the moment to reveal evidence and potentially strike a deal.
- He expresses doubt about Trump's intelligence to grasp subtle hints and strategic moves.
- Recent news indicates Trump's desire to testify live, prompting Beau to challenge this bluff.
- Beau recommends having the FBI present during Trump's testimony to prevent interference and address potential perjury.
- He predicts that Trump may deviate from the truth when speaking in a politically charged situation.
- Beau views Trump's desire to testify as a risky move, especially considering his tendency to stray from factual accuracy.
- He anticipates that Trump's lawyers might dissuade him from testifying to avoid legal repercussions.
- Beau humorously suggests a hypothetical scenario where Trump starts a YouTube channel titled "Trump of the Fifth Amendment" if he follows legal advice.
- He expresses skepticism about Trump's honesty during testimony, particularly if he views it through a political lens rather than a legal one.

### Quotes

- "Call that man's bluff."
- "Imagine what DOJ has and try to cut a deal."
- "He will go off. And once he starts, he just deviates."
- "Call that man's bluff."
- "It's just a thought. Y'all have a good day."

### Oneliner

The committee's decision to subpoena Trump prompts speculation on his willingness to testify, with doubts raised about his honesty under oath.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Challenge bluff by calling for live testimony with cameras and oath (suggested).
- Ensure FBI presence during testimony to prevent interference and address perjury (implied).
- Stay informed and engaged with legal developments surrounding Trump's testimony (exemplified).

### Whats missing in summary

Insights on the potential legal ramifications of Trump's testimony and the broader implications for accountability and truth-seeking.

### Tags

#Trump #Subpoena #Testimony #Legal #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about what happened yesterday at the committee
and the developments
that have happened since then. This is actually take two on this
because of the developments.
If you missed it, the committee has decided it will subpoena
former President Donald J. Trump. Now my initial read on that
was that this was the moment. This was the moment
they were going to bring him in, ask him some questions,
lay out what they have, and that's the moment that the institution of the
presidency people, probably led by Mitch McConnell,
walk in and explain, this
is what they have. Imagine what DOJ has
and try to cut a deal. There's two problems with that.
One, I don't think that's the right move.
And two, I don't think he's smart enough to take the hint.
That was basically the first video. Late breaking news,
reporting suggests that the former president would like to testify
as long as he can do it live. Call that man's bluff.
Call that man's bluff and do it right now.
Say sure, come on in. Turn the cameras on,
give him a mic, and put him under oath.
But if it was me,
I'd have the FBI there for two reasons. One, to keep Nancy
I ain't afraid to catch a case Pelosi away from him.
And two, to pick him up for perjury when he's done.
Because if I was a betting man,
I would suggest that if you were to take the former president and put him in a
situation
that he views as political, he's gonna deviate from the truth.
We've been talking about it for two weeks. His public statements,
they're not helping him legally because
he's playing politics. He's weaving his stories.
He's putting his narrative out there. You put him in a situation
where he has those cameras on him and he knows
that his base is going to hear his answers,
he is going to play to his base.
And then all he has to do is get the slightest bit frazzled.
Have Cheney cut his mic once.
He will go off. And
once he starts, he just deviates.
We've seen it over and over again when he is speaking.
I don't
think that he's really thought that through all the way.
Him testifying in this based on what we've seen,
it's like a super bad idea. He wants to do it live?
Okay. Sure.
But put him under oath.
Now my guess is that his lawyers
will talk some sense into him. They will explain the
legal jeopardy that this is going to put him in and he will find a way to
back out of it. He'll blame the committee for something
and say, well I'm not going to do it then.
If he does end up having to talk to him,
I'm willing to bet that he could probably start a
new YouTube channel, Trump of the Fifth Amendment,
if he's listening to his lawyers. If he understands that this is a legal thing,
not a political thing,
I don't have him saying a whole lot.
But if he is under the impression that this is politics
and he starts playing to those cameras,
I am skeptical of the idea that he's going to tell the truth.
And I have a feeling that
like many of his political speeches,
he will deviate from the truth on things
that are easily, easily
provable. Call that man's bluff.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}