---
title: Let's talk about Pirates, Emperors, Russians, and Ukrainians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KoqYlKhe4S8) |
| Published | 2022/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of pirates and emperors to illustrate a core concept.
- Tells the story of Alexander capturing a pirate to explain the distinction between a pirate and an emperor.
- Emphasizes that the core concept is doing the same thing through the same means.
- States that legality and morality are not the same, even though pirates and privateers share the same action.
- Questions the terminology and media response regarding an event on a bridge, pointing out the influence of state ties.
- Suggests that the delivery system does not change the morality of an action.
- Encourages understanding that nations are not exempt from normal morality and that this understanding helps combat propaganda.
- Compares Russia calling Ukraine a pirate to both sides using similar means for different purposes - defense and invasion.
- Stresses that the real difference lies in the moral aspects rather than the legal or illusionary aspects of conflicts.
- Concludes by leaving the audience with food for thought and well-wishes for the day.

### Quotes

- "Legality and morality are not the same."
- "Pirates and emperors."
- "Once you grasp that and really get it, it's much harder to fall for propaganda."
- "The real difference is that one side is defending and the other side is invading."
- "That's what matters."

### Oneliner

Beau introduces the concept of pirates and emperors, discussing the thin line between legality and morality in conflicts, urging a deeper understanding to combat propaganda.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Understand the distinction between legality and morality when analyzing conflicts (suggested)
- Combat propaganda by seeking a deeper understanding of the moral aspects of conflicts (suggested)

### Whats missing in summary

The full transcript provides a deeper exploration of the interconnected themes of legality, morality, propaganda, and conflicts, urging viewers to critically analyze these concepts.

### Tags

#Morality #Legality #Propaganda #ConflictAnalysis #CriticalThinking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about one
of my favorite themes.
It's one I've discussed on the channel before
and will probably discuss again at some point in the future.
I think it's a really important concept to kind of grasp,
because once you do, a whole lot of other stuff
just kind of falls into place.
This concept has been talked about in a whole bunch
different ways over hundreds of years. My favorite way to illustrate it is the
concept of pirates and emperors. And the story says that St. Augustine basically
told the story of Alexander capturing a pirate. And Alexander asks the pirate and
he's like, what are you doing? You're disturbing the peace of the seas. And the
pirate's like, what are you doing? You're disturbing the piece of the entire
planet. But because I do it with one boat, well, I'm a pirate. But you have a fleet,
so you're an emperor. And Alexander was like, you got me, kind of. The core
concept here is that if you're attempting to do the same thing through
the same means, well, pretty much everything else is just window dressing.
The emperor has the illusion of authority.
They can rely on the concept of a state, nationalism, to give them cover.
The pirate doesn't have that.
And if you think that there's something else at play, what's the difference between a
pirate and a privateer, a permission slip from a government. It's that belief that
the power of the state changes morality. It doesn't. It's the same thing. It's the
same action done in the same way. It's the same thing. Legality and morality are
not the same. The reason this topic is coming up is because of that thing on
the bridge. Conventional wisdom says that that was Ukraine. And because of that,
certain terminology is being thrown around. Big armies always have a term
for smaller armies. But let me ask you this, would that term, would it be being
being tossed out in the media right now, if it had been done from a plane using a precision
guided munition, it wouldn't.
It wouldn't.
Nobody would be calling it that.
Why?
Because of the tie to a state.
of the tie to a government that makes it look legal.
So at the end, what people are arguing about is a delivery system.
I don't know that that changes the morality of it.
This concept of understanding that a lot of what we accept from nations is based on an
illusion that somehow they are exempt from normal morality.
Once you grasp that and really get it, it's much harder to fall for propaganda.
You kind of have to try to fall for it at that point.
Pirates and emperors.
Right now Russia is calling Ukraine a pirate.
But both sides in this war have used the same means to achieve the same thing.
The real difference is that one side is defending and the other side is invading.
That's the real difference.
When you get down to it and you're talking about the moral aspects of it, rather than
the legal or the illusion, that's what matters.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}