---
title: Let's talk about Alabama, Trump's rally, and Tuberville....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ykNn5Eq8EN0) |
| Published | 2022/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Tuberville made controversial statements linking the Democratic Party to being pro-crime and wanting reparations for criminals.
- Tuberville also claimed that the U.S. cannot afford food stamps and food security programs, urging people to get back to work.
- Unemployment rates in the U.S. and Alabama are low, yet there are a significant number of people (769,800) benefiting from SNAP.
- The gap between employment numbers and SNAP recipients indicates that minimum wage may be a contributing factor.
- Alabama's minimum wage at $7.25 per hour falls significantly below the living wage threshold, leading many to rely on poverty programs.
- Beau suggests Senator Tuberville should focus more on understanding the real issues in Alabama rather than making controversial statements elsewhere.
- Beau expresses frustration towards the senator's comments and advocates for supporting striking minors in Alabama by providing Christmas gifts.

### Quotes

- "They're pro-crime. They want crime. They want reparations, because they think the people who do the crime are owed that."
- "The United States could not afford food stamps, food security programs, and that people needed to get back to work."
- "Maybe that has something to do with it. Just saying."
- "If your minimum wage is closer to your poverty wage than a living wage, yeah, you're probably going to have a lot of people using your poverty programs."
- "We have to start getting our ducks in a row on this channel so we can help with getting Christmas gifts for the kids of striking minors in Alabama."

### Oneliner

Senator Tuberville's controversial statements on crime and welfare programs reveal a disconnect with Alabama's economic realities, urging for attention towards fair wages and community support.

### Audience

Policy Advocates, Community Activists

### On-the-ground actions from transcript

- Support striking minors in Alabama by providing Christmas gifts for their kids (suggested)

### Whats missing in summary

The full transcript provides a deeper insight into the economic challenges faced by individuals in Alabama due to low minimum wages and the disconnect between Senator Tuberville's statements and the real issues on the ground.

### Tags

#Alabama #MinimumWage #SNAP #CommunitySupport #EconomicJustice #PolicyIssues


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about how things work, Alabama,
people wanting to work, and some things
that Senator Tuberville said, because wow.
We're going to start with the part
that I'm fairly certain is going to be all over the news,
because, well, you'll see.
Speaking of the Democratic Party,
he said, they're not soft on crime.
They're pro-crime.
They want crime.
They want reparations, because they
think the people who do the crime are owed that.
Yeah, that's something else, isn't it?
The people who do the crime are the people
who are owed reparations.
That seems like he used a lot of extra words
to get his point across.
I'm fairly certain he could have achieved the same effect
by walking out and saying one word.
I don't think I need to provide any more information.
I don't think I need to provide a lot of commentary on this,
because I have a feeling this is going to be talked about.
I think the only thing I can say is, I mean, yeah,
that seems pretty racist to me, too.
Wow.
But I want to talk about something else that he said
that is not quite as shocking, but from a policy standpoint,
might should be talked about.
It's probably going to be ignored because of, I mean,
that.
He said that the United States could not
afford food stamps, food security programs,
and that people needed to get back to work.
OK.
So we'll go over some numbers that I guess
the senator isn't familiar with.
Right now, unemployment in the US is at like 3.5%.
And in case he doesn't know, that's super low.
That's really low.
In Alabama, it's 2.6%.
Y'all only have like 60,000 people out.
But there's this weird thing.
There's 769,800 people benefiting
from SNAP in the state, which incidentally has a work
requirement.
But that's a big gap between those numbers.
So it's probably safe to say that it's not
the people on unemployment that are causing this issue.
It might be something else.
I don't know.
Let me take a shot in the dark on this one.
Might have something to do with the fact
that in 2009, the federal government set the minimum wage
at $7.25.
And that's where it still is in Alabama.
And at $7.25 in Alabama, let's see, $7.25, 40 hours a week,
that's $290 a week times 52 weeks a year.
That is $15,080.
Do you know what you need to qualify for food stamps
as a single person?
You need to make less than $17,667.
Maybe that has something to do with it.
Just saying.
MIT has that living wage calculator.
I want to say Alabama is like $15.90 for a living wage.
That's what you would need to make.
A poverty wage in Alabama is $6.19.
If your minimum wage is closer to your poverty wage
than a living wage, yeah, you're probably
going to have a lot of people using your poverty programs.
Go figure.
It's wild.
This suggests that perhaps the senator
might want to spend a little bit more time in Alabama
instead of Nevada playing with Trump.
This whole thing is wild to me.
I mean, these are just the highlights.
This was unreal.
And I know somebody's going to say angry Beau or something.
And yeah, a little bit.
A little bit, because the day before he said this,
somebody in the labor movement there in Alabama reached out.
And it's not that people don't want to work.
It's that they're tired of being taken advantage of.
And because of that conversation,
though, it made me realize that we
have to start getting our ducks in a row on this channel
so we can help with getting Christmas
gifts for the kids of striking minors in Alabama
for the second year in a row.
That might be something that maybe the senator should
be paying more attention to rather than everything
that he said.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}