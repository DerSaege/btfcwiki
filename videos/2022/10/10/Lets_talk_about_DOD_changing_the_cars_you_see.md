---
title: Let's talk about DOD changing the cars you see....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=03ctWe2hzlU) |
| Published | 2022/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a new vehicle that he is excited about because it will change the types of vehicles seen on the road.
- Talks about a specific demographic that purchases vehicles to project a tough, macho image, like civilianized Humvees and giant diesel trucks.
- Mentions that the Department of Defense (DOD) is introducing a new electric light reconnaissance vehicle (e-LRV) which will impact the types of vehicles people buy.
- Notes that DOD's move towards electric vehicles is influenced by viewing climate change as a national security threat and the need to reduce reliance on fossil fuels.
- Predicts that once this demographic sees the military using electric vehicles, they will want them too to maintain their image.
- Believes that once this specific demographic embraces electric vehicles, it will lead to broader acceptance and adoption of electric vehicles.
- Recognizes the positive impact of the military transitioning to electric vehicles on the environment and domestically.
- Points out that the platform used for the new military vehicle will be available in the civilian market, potentially paving the way for off-road electric vehicles for civilians.
- Concludes by expressing his thoughts on the potential positive impacts of the military's shift to electric vehicles.

### Quotes

- "The people who purchase these types of vehicles for these reasons, they'll want one. I know that sounds silly, but they will."
- "Because if they don't, well, they'll be seen as a fud."

### Oneliner

Beau predicts that the Department of Defense's move towards electric vehicles will influence a demographic known for tough guy trucks to shift towards electric, potentially leading to broader acceptance of electric vehicles.

### Audience

Vehicle enthusiasts, climate-conscious consumers.

### On-the-ground actions from transcript

- Look into purchasing electric vehicles to support the transition towards more sustainable transportation options (implied).
- Stay informed about advancements in electric vehicle technology and their availability in the civilian market (implied).

### Whats missing in summary

Beau's detailed insights and reasoning behind why the shift towards electric vehicles by the Department of Defense could impact consumer behavior positively.

### Tags

#ElectricVehicles #DepartmentOfDefense #Sustainability #ClimateChange #MilitaryTransition


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about a new vehicle
that I am super excited about.
I'm excited about it because it's
going to change the types of vehicles we see on the road.
There is a certain demographic of person
who purchases a vehicle to cast a specific image.
We're talking about the people who bought, like,
civilianized Humvees, H2s, H3s, and now, I guess, the go-to.
It's kind of reverted back to buying a giant diesel
truck that has never pulled or moved anything and never will.
Those who buy these types of vehicles
to cast the macho, tough guy image,
they may be looking at something else soon.
And it's thanks to the Department of Defense
because DOD wants a new LRV.
An LRV is a light reconnaissance vehicle.
Historically, these things look, well,
they can look anything like from a Humvee or a Jeep
all the way up to, like, an MRAP-style vehicle.
DOD has decided that the next LRV will be an e-LRV.
It'll be electric.
They've done testing, proof of concept stuff,
working with GM, well, GM Defense specifically.
And they've decided to move forward with it.
Interestingly enough, the concept vehicle
that DOD was apparently provided was pretty much
the new Hummer pickup electric vehicle.
Once DOD starts using this and once it becomes apparent
that this is the way the military is going,
because as we've talked about on the channel,
DOD has said climate change is a national security threat.
And they're trying to move away as much as they possibly
can from being reliant on fossil fuels,
not just to mitigate climate change,
but to be prepared for the almost inevitable upheavals
that are going to happen because of it.
As DOD does this, the people who purchase these types of vehicles
for these reasons, they'll want one.
I know that sounds silly, but they will.
That's how we wound up with civilianized Humvees,
the H2s and the H3s.
That's how that happened.
That desire to mimic the military, to cast that image,
it's going to happen.
These are the big vehicles.
These are the big trucks.
These are the tough guy trucks.
These are the biggest, loudest voices
when it comes to saying, well, I'm never
going to switch to electric.
I bet they do now.
Because if they don't, well, they'll be seen as a fud.
It's really that simple.
Once this particular demographic buys in to electric
and starts to make the switch, you're
going to see a lot more people outside of that demographic
become more comfortable with it.
So the military getting a new vehicle
is not something I'm normally excited about.
But in this case, I'm not.
But in this particular case, I think
it's going to have a lot of positive impacts
down the road domestically and for the environment.
So the interesting thing about it
from a person who likes to go off-road
is that the platform they're using
is a platform that will be available to the civilian
market.
So it appears that they have overcome a lot of issues
when it comes to making off-road electric vehicles, which
is nice.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}