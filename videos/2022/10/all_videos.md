# All videos from October, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-03-06 08:06:36
<details>
<summary>
2022-10-31: Let's talk about the Pelosi reaction.... (<a href="https://youtube.com/watch?v=GCHq3_D1EzY">watch</a> || <a href="/videos/2022/10/31/Lets_talk_about_the_Pelosi_reaction">transcript &amp; editable summary</a>)

Beau explains a harrowing incident at Pelosi's home, delves into the spread of misinformation, and challenges viewers to confront their complicity in downplaying serious issues.

</summary>

"Think about anything else but that."
"Are we the baddies?"
"You're the type of person who is rooting for the villain in horror movies."
"That's what the information silo that you have fallen into has changed you into."
"It's probably worth taking a step back and looking at who benefits from this."

### AI summary (High error rate! Edit errors on video page)

Explains the incident involving Nancy Pelosi's husband at home in California, facing an intruder with restraints and a hammer looking for Nancy Pelosi.
Mr. Pelosi alertly called 911 and communicated with the police indirectly through coded speech.
Despite the accurate reporting, false stories emerged blaming various unrelated factors, diverting attention from the attacker's association with right-wing conspiracy theories.
Addresses the motive behind flooding misinformation and distracting from the real issue of a violent attack on an elderly person.
Criticizes those who participated in spreading false narratives, trying to downplay or divert attention from the severity of the incident.
Draws a parallel between those deflecting from the attack and individuals rooting for villains in horror movies, manipulating perceptions and reactions.
Encourages self-reflection on being influenced by misleading information and becoming complicit in shifting focus away from critical issues.
Points out the danger of being swayed by misinformation that distorts reality and leads individuals to support harmful agendas.
Urges viewers to question their role in perpetuating false narratives and contributing to a toxic information environment.
Concludes by prompting a reevaluation of who truly benefits from spreading misinformation and the underlying motives driving such actions.

Actions:

for online viewers,
Question the information you consume and verify facts before sharing (implied)
Encourage critical thinking and fact-checking within your social circles (implied)
</details>
<details>
<summary>
2022-10-31: Let's talk about Halloween, filters, and Fermi.... (<a href="https://youtube.com/watch?v=BI5JrtRSPRM">watch</a> || <a href="/videos/2022/10/31/Lets_talk_about_Halloween_filters_and_Fermi">transcript &amp; editable summary</a>)

Bob McGowan examines the Fermi paradox, warning against human arrogance as the ultimate filter for civilization's survival amid various existential threats.

</summary>

"The belief that we've got it, that we've got it all figured out, that we'll be able to surmount any obstacle that comes our way, any filter what we're gonna make it through."
"This Halloween, as you think of all the ghouls and goblins, remember that the biggest predator, the most dangerous animal, the biggest monster walking the earth is us, nobody else."

### AI summary (High error rate! Edit errors on video page)

Introduces the Fermi paradox and filters, questioning why intelligent life beyond Earth hasn't been found despite the high probability of its existence.
Explains the great filter theory, suggesting that intelligent civilizations may destroy themselves before becoming space-faring.
Mentions various filters that could prevent civilizations from advancing, such as disease, nuclear war, climate change, artificial intelligence, religious stagnation, and wealth obsession.
Challenges the notion that humanity has surpassed all filters and warns against arrogance in assuming invincibility.
Concludes with a reflection on humanity's role as the most dangerous threat to itself and the determining factor in its survival.

Actions:

for science enthusiasts,
Question assumptions of invincibility and actively work to prevent potential filters from hindering progress (implied).
Foster a mindset of humility and collaboration in addressing global challenges to ensure a sustainable future (implied).
</details>
<details>
<summary>
2022-10-31: Let's talk about Futurama's reboot and social commentary.... (<a href="https://youtube.com/watch?v=sRTrlutD3oM">watch</a> || <a href="/videos/2022/10/31/Lets_talk_about_Futurama_s_reboot_and_social_commentary">transcript &amp; editable summary</a>)

Beau breaks down how "Futurama" excels in delivering social commentary through satire, bluntly addressing various societal issues with a woke perspective.

</summary>

"Futurama was always woke."
"If you want a Futurama without social commentary, you're going to have to build your own Futurama with Blackjack."

### AI summary (High error rate! Edit errors on video page)

Explains the premise of the show "Futurama" where a person is frozen in the year 2000 and wakes up in the year 3000, setting the stage for social commentary and satire.
Addresses concerns about the reboot injecting social commentary clumsily, mentioning that the entire show is based on social commentary and satire.
Breaks down episodes from the first season, showing how each one carries social commentary themes, such as political figures, consumerism, religion, and societal issues.
Points out the strong feminist character in the show who belongs to the permanent underclass, residing in the sewers but vital for society's functioning.
Lists various social issues addressed in "Futurama," including climate change, cancel culture, polyamorous relationships, and exploitation of native cultures.
Asserts that "Futurama" has always been woke, combining satire with bluntness to deliver its social commentary effectively.

Actions:

for fans of "futurama",
Watch and support the rebooted episodes of "Futurama" to appreciate and encourage the continuation of its social commentary (implied).
</details>
<details>
<summary>
2022-10-31: Let's talk about Bolsonaro's loss and what it can teach us.... (<a href="https://youtube.com/watch?v=9FueG7nMeqo">watch</a> || <a href="/videos/2022/10/31/Lets_talk_about_Bolsonaro_s_loss_and_what_it_can_teach_us">transcript &amp; editable summary</a>)

Lula defeating Bolsonaro in Brazil signals the decline of far-right nationalism globally, fueled by increased understanding and interconnectedness.

</summary>

"Nationalism is a dying ideology. It's going away. It is going away."
"Fear in most cases is a lack of understanding. That ignorance. It's a breeding ground for fear, for nationalism."
"Those symbols becoming less valuable. It is scary for them. It is terrifying for them."
"This is going to happen on a global scale."
"Bolsonaro's loss, it's great for Brazil, and it's great for the rest of the world."

### AI summary (High error rate! Edit errors on video page)

Lula defeated Bolsonaro in Brazil's election, marking a shift away from far-right nationalism.
Bolsonaro, likened to Trump, is refusing to concede the election.
Nationalism is on the decline globally, as seen in Brazil and the US.
The world's interconnectedness through technology is eroding nationalist ideologies.
Nationalism thrives on fear and ignorance, making people vulnerable to manipulation.
Leaders who exploit nationalism often do not truly believe in it, using it to control their followers.
Loyalty to national symbols is diminishing as people gain broader perspectives through increased communication.
Bolsonaro's loss is a positive development for Brazil and the world, signaling a rejection of far-right nationalism.
The future trends towards decreased nationalism as understanding and communication increase.
Ultimately, the diminishing power of nationalism is an inevitable consequence of a more connected world.

Actions:

for global citizens,
Reach out to local communities to foster understanding and connection (implied).
Educate oneself and others on the dangers of nationalist manipulation (suggested).
Support leaders who prioritize unity and inclusivity over divisive nationalist rhetoric (implied).
</details>
<details>
<summary>
2022-10-30: Let's talk about voting in a woke military.... (<a href="https://youtube.com/watch?v=0NFEiMFrM7A">watch</a> || <a href="/videos/2022/10/30/Lets_talk_about_voting_in_a_woke_military">transcript &amp; editable summary</a>)

Exploring the role of voting and civic engagement, Beau addresses disagreement, using a military analogy and stressing the temporary nature of voting's impact while touching on military inclusion concerns.

</summary>

"Voting is never going to give you systemic change."
"You're not going to achieve that greater world that you probably want through voting."
"It can buy you time. You can have the least bad option."
"There's another topic going on right now about the they them military."
"That's about as hardcore as you get right there."

### AI summary (High error rate! Edit errors on video page)

Exploring the effectiveness of voting and civic engagement.
Responding to a viewer's disagreement on the impact of voting.
Drawing an analogy involving a military scenario to explain the concept.
Emphasizing that voting may not bring systemic change but can buy time.
Touching on the topic of military inclusion and its implications.
Acknowledging the background of the viewer's boyfriend as former 18F.
Signing off with well-wishes for the audience.

Actions:

for civic-minded individuals,
Engage in local capacity-building initiatives to drive real change (implied)
</details>
<details>
<summary>
2022-10-30: Let's talk about a technique for dealing with baseless claims.... (<a href="https://youtube.com/watch?v=4xdiTUl6wJs">watch</a> || <a href="/videos/2022/10/30/Lets_talk_about_a_technique_for_dealing_with_baseless_claims">transcript &amp; editable summary</a>)

Beau explains how questioning baseless claims can help break the spell of misinformation, urging critical thinking over fear-mongering.

</summary>

"Thanks for the heads up. I have a few follow-up questions given the rather alarming news."
"Asking these questions and then waiting for it to inevitably not happen is a way that you can hopefully lead that person to begin to question those people who they've fallen under the spell of."

### AI summary (High error rate! Edit errors on video page)

Someone received a message about a baseless claim of cities in the US starting a civil war.
MAGA Republicans were advised to prep, pray, and stay out of the way as the military intervenes.
The message warned of a potential 10-day information blackout with only emergency broadcasts available.
These fear-mongering messages have been circulating for about 20 years without ever coming true.
Beau praises a response to such messages that involves asking critical questions.
By employing the Socratic method and questioning the source of information, doubt can be planted in the believer's mind.
The goal is to make individuals question the credibility of the sources they trust.
Beau suggests using the upcoming holidays to respond thoughtfully to relatives spreading false information.
Encourages throwing a lifeline to those caught up in misinformation silos.
Proposes prepping individuals for the failure of baseless claims to help them break free from false beliefs.

Actions:

for critical thinkers,
Challenge baseless claims by asking critical questions (implied).
</details>
<details>
<summary>
2022-10-30: Let's talk about a poll on climate inaction.... (<a href="https://youtube.com/watch?v=4ElgDLR6Ga4">watch</a> || <a href="/videos/2022/10/30/Lets_talk_about_a_poll_on_climate_inaction">transcript &amp; editable summary</a>)

Recent poll shows dissatisfaction with federal government's action on climate change, but the real obstruction lies with the Republican Party's blocking of initiatives like the Inflation Reduction Act.

</summary>

"Republican Party is blocking action to do the bidding of their campaign donors."
"The Republican Party is stopping it. Because, I mean, why spoil their own investments?"
"The actions we need to take to mitigate climate change should have started like yesterday."

### AI summary (High error rate! Edit errors on video page)

Recent poll shows two out of three Americans believe federal government isn't doing enough to fight climate change.
Inflation Reduction Act, the largest investment to fight climate change in US history, just passed without a single Republican vote.
Republican Party is blocking action to serve their campaign donors' interests.
With Democratic Party majorities, there could be real action on climate change.
Republican Party is stopping progress, not the federal government.
Need for immediate action to mitigate climate change, as current efforts are not sufficient.
Republican Party is the political obstacle hindering climate change mitigation efforts.

Actions:

for advocates for climate action.,
Advocate for political candidates who prioritize climate change action (implied).
Support and vote for candidates who have plans for effective climate change mitigation (implied).
</details>
<details>
<summary>
2022-10-29: Let's talk about an AP poll about the election.... (<a href="https://youtube.com/watch?v=jgbxtFqOah4">watch</a> || <a href="/videos/2022/10/29/Lets_talk_about_an_AP_poll_about_the_election">transcript &amp; editable summary</a>)

Roughly half of Americans believe votes will be counted accurately in the midterms despite no evidence of fraud, risking long-term damage to the Republican Party.

</summary>

"It's wild to watch a group of people listen to somebody who got into office because they were voted in say that, you know, the votes aren't accurate."
"None. It's been two years and they've strung along half the country with, well, my cousin has the evidence, my uncle has the evidence, I'll release it at this expo."
"Long term, this is going to be a huge issue for the Republican Party."

### AI summary (High error rate! Edit errors on video page)

Explains the impact of a poll by the Associated Press on the election, Republican Party, and long-term effects of false perceptions.
Roughly half of Americans believe votes will be counted accurately in the midterms, despite no evidence of widespread voter fraud.
Questions how politicians reconcile pushing voter fraud rhetoric while urging supporters to vote.
Warns about the long-term consequences of internalizing the belief that voting doesn't matter.
Predicts partisan impacts on voter turnout, with the Republican Party being more affected than the Democratic Party.
Criticizes the Republican Party for perpetuating the idea of insecure elections without evidence.
Argues that this rhetoric damages the Republican Party's chances in elections by eroding trust in the voting system.
Compares the situation to a lack of response to a public health crisis, where spreading misinformation harms those spreading it the most.
Emphasizes that there is no evidence to support claims of widespread voter fraud despite two years of rhetoric.

Actions:

for voters,
Challenge misinformation within your community (exemplified)
Encourage fact-checking and critical thinking among peers (exemplified)
Support candidates who value truth and transparency in elections (exemplified)
</details>
<details>
<summary>
2022-10-29: Let's talk about Trump endorsed candidates and swing voters.... (<a href="https://youtube.com/watch?v=RRgZoOI7Tl4">watch</a> || <a href="/videos/2022/10/29/Lets_talk_about_Trump_endorsed_candidates_and_swing_voters">transcript &amp; editable summary</a>)

American swing voters warned against supporting Trump-endorsed candidates embodying hate, lies, and division, mirroring the damaging traits of the former president.

</summary>

"They're taking their orders directly from him."
"That hate, that anger, that ineffectiveness, that division, that's what you can expect from Trump-endorsed candidates."
"Where did those images come from? They came from Trump's time in office."
"They're going to be exactly what you rejected."
"It's the same hate, the same lies, the same division, the same ineffectiveness."

### AI summary (High error rate! Edit errors on video page)

Beau addresses American swing voters, independents, and moderates, discussing their rejection of Trump due to divisiveness, lies, anger, and hate.
He points out that Trump-endorsed candidates are essentially mini-Trumps, following his orders and talking points directly.
Referring to a specific incident in Arizona, Beau mentions Trump instructing a candidate to lie more about the 2020 election to appease his radicalized base.
Beau warns that Trump-endorsed candidates embody hate, anger, ineffectiveness, and division because they simply follow Trump's directives.
Despite Trump being viewed as a losing candidate, Beau stresses that as long as he controls his endorsed candidates, he maintains power without winning elections.
Beau questions the false narratives spread before the election about "Biden's America" with riots and chaos, attributing those images to Trump's time in office.
He urges swing voters to recall why they rejected Trump and to understand that Trump-endorsed candidates will exhibit the same negative traits.
In summary, Beau cautions against supporting Trump-endorsed candidates as they represent the same divisive and harmful characteristics as Trump himself.

Actions:

for american swing voters,
Inform fellow swing voters about the dangers of endorsing Trump-backed candidates (implied)
</details>
<details>
<summary>
2022-10-29: Let's talk about Biden getting around Republicans on Ukraine.... (<a href="https://youtube.com/watch?v=t1hfLv2bhz0">watch</a> || <a href="/videos/2022/10/29/Lets_talk_about_Biden_getting_around_Republicans_on_Ukraine">transcript &amp; editable summary</a>)

Beau explains the unrealistic steps for the Republican Party to block aid to Ukraine and why it's unlikely to succeed.

</summary>

"It takes a whole lot for this segment of the Republican Party to get from them talking on Fox News to get a pat on the head from an authoritarian and actually being able to implement it."
"This wouldn't be a major concern of mine if I had family there."

### AI summary (High error rate! Edit errors on video page)

Explains McCarthy's statement and the concerns raised by a segment of the Republican Party about cutting off aid to Ukraine.
Notes the internal conflict within the Republican Party, with Trump loyalists wanting to cut aid while McConnell seeks greater assistance.
Breaks down the unlikely steps required for the Republicans to actually stop aid to Ukraine, including winning both chambers, passing legislation, and overriding a potential Biden veto.
Points out the challenges in tricking Biden into signing such legislation and the limitations of Congress versus the commander in chief in providing aid.
Emphasizes that even in a veto-proof scenario, there are ways for Biden to work around restrictions and still support Ukraine, making the Republican plan unlikely to succeed.

Actions:

for political observers,
Contact political representatives to express support for aid to Ukraine (implied)
Stay informed about political developments related to foreign aid policies (implied)
</details>
<details>
<summary>
2022-10-28: Let's talk about the US wanting a new internet service.... (<a href="https://youtube.com/watch?v=_d9ErNeu1Zk">watch</a> || <a href="/videos/2022/10/28/Lets_talk_about_the_US_wanting_a_new_internet_service">transcript &amp; editable summary</a>)

The United States may develop a tool for providing internet access in authoritarian regimes, aiming to empower individuals and nations through information access and bypass censorship.

</summary>

"Ideas travel faster than bullets."
"If America is going to lead the free world, we're going to have to be willing to double and triple our investments in the tools that Iranian and Russian dissidents are jumping on to avoid government snooping."

### AI summary (High error rate! Edit errors on video page)

The United States is considering developing a pop-up infrastructure tool to provide internet access to countries with authoritarian governments or disrupted internet infrastructure like Ukraine.
There is a desire within the defense and foreign policy community to create a system for such situations.
The current reliance on private corporations for providing vital internet connectivity is concerning from a national security perspective.
The U.S. government aims to develop tools to bypass censorship by authoritarian regimes abroad.
Initial reports suggest a budget of $125 million for this project, which Beau believes is insufficient.
Beau anticipates the budget to increase significantly as the project progresses.
Investing in tools that help dissidents avoid government surveillance is seen as a necessary step for America to lead the free world.
Providing information and internet access empowers people in restricted countries for self-determination, potentially reducing direct U.S. involvement.
Beau views this initiative positively, seeing it as a way to empower individuals and nations through information.
A significant project is expected to emerge to create a service for internet access in critical situations.

Actions:

for tech enthusiasts, policymakers,
Develop tools to provide internet access in countries with authoritarian governments (implied)
Invest in systems that bypass censorship and government surveillance (implied)
</details>
<details>
<summary>
2022-10-28: Let's talk about history, context, and format.... (<a href="https://youtube.com/watch?v=t8oO-DmfrdA">watch</a> || <a href="/videos/2022/10/28/Lets_talk_about_history_context_and_format">transcript &amp; editable summary</a>)

Beau provides historical context, admits to a mistake, and draws parallels between suffragettes and modern activists, reflecting on the importance of having a voice in societal decisions.

</summary>

"I made a mistake. Now we're going to turn that mistake into a happy little tree."
"When people don't have a voice, they lash out. Right or wrong, that's what happens."
"I can't condemn the lashing out without condemning the conditions that cause them to lash out."
"It's not without historical precedent."
"Sometimes it's good to put a face to a movement or an idea."

### AI summary (High error rate! Edit errors on video page)

Providing historical context to an earlier video.
Admitting to owning a mistake in not providing historical context in a previous video.
Acknowledging the importance of historical context in bringing clarity.
Linking the incident of soup throwers to a similar historical event in London during the early 1900s.
Explaining the actions of the Women's Social and Political Union, also known as suffragettes.
Drawing parallels between the suffragettes' actions and modern-day young climate activists.
Recognizing the frustration of those who do not have a voice in the decision-making process.
Acknowledging that when people lack a voice, they may resort to lashing out.
Mentioning the tactic of drawing attention to climate change through disruptive actions.
Noting the intentional reference made by one of the people involved with the Van Gogh painting to disobedience leading to rights.
Suggesting looking up surveillance photos of militant suffragettes for more insight into the movement.
Encouraging putting a face to a movement or idea by researching further.
Ending with a thought-provoking message.

Actions:

for history enthusiasts, activists, educators.,
Research and learn more about the suffragette movement and their tactics (suggested).
Look up surveillance photos of militant suffragettes to understand their struggle better (suggested).
</details>
<details>
<summary>
2022-10-28: Let's talk about Spy Games, China, and the US.... (<a href="https://youtube.com/watch?v=G1fK2J1C2tc">watch</a> || <a href="/videos/2022/10/28/Lets_talk_about_Spy_Games_China_and_the_US">transcript &amp; editable summary</a>)

The US ramps up intelligence operations against China amid concerns over Chinese espionage and flouting international norms.

</summary>

"They're cops. Think of them like that. They're law enforcement."
"There's a different frame of mind between the two."
"They're not spies. They're spy catchers and they have a very different frame of mind."

### AI summary (High error rate! Edit errors on video page)

The Department of Justice in the United States has charged Chinese nationals with engaging in intelligence operations within the country.
Big Red Kelp, a commentator on Chinese spying, mentioned a "gentleman's agreement" existing in the spy game world.
In the gentleman's agreement, the actual spy handlers are typically asked to leave the country quietly, instead of facing legal action.
Disrespecting this agreement could lead to repercussions, as shown by the recent charges against Chinese spies.
China's attempts to move away from international agreements, like this gentleman's agreement, may have triggered the recent actions by the US.
The US may be increasing pressure on China due to Russia being perceived as a near-peer competitor.
The US may use intelligence operations to secure American interests, often for commercial advantage, but with distinctions from China's alleged direct support to a specific company.
Counterintelligence officers focus on law enforcement and catching foreign intelligence operations within their jurisdiction.
Intelligence officers, on the other hand, are likened to trained criminals who conduct espionage and other covert activities.
The US could be gearing up for a more intense intelligence operation against China in response to recent events.

Actions:

for policymakers, intelligence analysts,
Research and understand the implications of international espionage and the enforcement of related agreements (implied)
Stay informed on developments in global intelligence operations and their impact on national security (implied)
Advocate for transparency and accountability in international relations to prevent abuses of power (implied)
</details>
<details>
<summary>
2022-10-27: Let's talk about Trump scaring Dems and Paul Ryan.... (<a href="https://youtube.com/watch?v=wTAUKplV95o">watch</a> || <a href="/videos/2022/10/27/Lets_talk_about_Trump_scaring_Dems_and_Paul_Ryan">transcript &amp; editable summary</a>)

Democrats aren't scared of Trump in 2024; even Republicans doubt his ability to win.

</summary>

"Remind them that even by the MAGA faithful standards, he succeeded at nothing."
"He accomplished nothing that he had them chanting for."

### AI summary (High error rate! Edit errors on video page)

Dispels the myth that Democrats are scared of running against Trump in 2024 and conducting investigations out of fear.
Notes that Democrats beat Trump before all the revelations, election incidents, and January 6th.
Asserts that Trump is not a threat to the Democratic Party and labels him weak.
Quotes Paul Ryan saying they're more likely to lose with Trump as he's unpopular with suburban voters.
Mentions Republicans, including Ryan, acknowledging that Trump cannot win.
Criticizes Trump's failed promises like building the wall, locking her up, and draining the swamp.
Acknowledges one decent foreign policy idea but overall considers Trump's presidency a waste of four years.
Comments on how Trump inherited a booming economy and left it in shambles without any substantial achievements.
Points out Trump's actions post-2020 election further alienating voters.
Dismisses the idea of investigations being part of a Democratic plot and asserts they follow where evidence leads.

Actions:

for voters, political analysts.,
Fact-check Trump's claims and hold him accountable for his failed promises (implied).
Engage in critical thinking and research about political narratives and statements (implied).
</details>
<details>
<summary>
2022-10-27: Let's talk about Russia's new defensive lines.... (<a href="https://youtube.com/watch?v=zanLNVLyr2Y">watch</a> || <a href="/videos/2022/10/27/Lets_talk_about_Russia_s_new_defensive_lines">transcript &amp; editable summary</a>)

Russian military constructs defensive lines near Ukraine border as Ukraine's quick mobility poses a threat, causing rifts in the Kremlin and Putin's frustration.

</summary>

"When your elective offensive war goes so poorly that you are now constructing defensive positions in your own country, yeah, that's a pretty bad sign."
"There is a Russian city, Belgorod, Ukraine has clearly demonstrated that it has the capability to thunder run right into that city."
"The only reason Belgorod doesn't look like a Ukrainian town and it remains intact is because Ukraine allows it to remain that way."
"For those who are still buying the Russian propaganda lines of, oh, they're going to bring out their good troops soon and all of that stuff, that's actually the reality on the ground."
"It's also probably one of the smarter moves Russia has made."

### AI summary (High error rate! Edit errors on video page)

Russian military constructing defensive lines near Ukraine border, some inside Russian territory.
Defensive lines intended to disrupt armor movements and tanks.
Ukrainian military's capability to quickly move over territory is a concern for Russia.
Putin reportedly upset about defensive lines being publicized.
Belgorod, a Russian city, is vulnerable to Ukrainian military's quick advances.
NATO likely advising against Ukrainian troops advancing into Belgorod due to strategic reasons.
Belgorod remains intact because Ukraine allows it to.
Russian propaganda about bringing out "good troops" is debunked by the reality on the ground.
A Russian town is at risk, but Ukraine chooses not to advance into it.
Defensive lines seen as a smart move by Russia, despite humorous comments and cover stories.

Actions:

for international observers,
Monitor the situation and stay informed about developments near the Ukraine-Russia border (implied).
Support diplomatic efforts to de-escalate tensions between Russia and Ukraine (implied).
</details>
<details>
<summary>
2022-10-27: Let's talk about Putin's new committee.... (<a href="https://youtube.com/watch?v=XZxWAtlKydQ">watch</a> || <a href="/videos/2022/10/27/Lets_talk_about_Putin_s_new_committee">transcript &amp; editable summary</a>)

Putin's new committee addresses military issues stemming from economic difficulties and lack of supplies, with potential consequences for Russia's ability to continue the war as winter approaches.

</summary>

"You go to war with the military you have, not the one you want."
"A lack of cold weather gear is a big deal."
"If they can't find a solution, winter is coming."

### AI summary (High error rate! Edit errors on video page)

Putin formed a new committee to address military issues, showing a rare admission of problems.
The committee was created due to economic difficulties and lack of supplies for Russian troops.
Despite propaganda efforts showing well-equipped soldiers, the reality is different, with many troops having outdated and non-functional equipment.
Putin hopes the committee will help expedite the arrival of necessary supplies like cold weather gear and medical kits.
Russia faces challenges in obtaining necessary equipment due to sanctions limiting trade.
The lack of cold weather gear could impact Russian troops' mobility and effectiveness in comparison to Ukrainian forces.
The supply issue could become a critical factor in Russia's ability to continue the war as winter approaches.

Actions:

for military analysts, policymakers,
Support organizations providing aid to Ukrainian forces (implied)
Advocate for humanitarian aid to war-affected areas (implied)
</details>
<details>
<summary>
2022-10-26: Let's talk about the exit strategy for Ukraine.... (<a href="https://youtube.com/watch?v=Nto-X-QFlYE">watch</a> || <a href="/videos/2022/10/26/Lets_talk_about_the_exit_strategy_for_Ukraine">transcript &amp; editable summary</a>)

Beau explains why the US should not have an exit strategy in Ukraine and why it's up to the Ukrainian people to decide the war's course, not pundits or imperialist influences.

</summary>

"There isn't an exit strategy. It doesn't exist because it's a defensive war."
"This is something for the Ukrainian people to decide, not pundits on TV."
"Just because the war isn't getting good enough ratings, I don't know that's a good enough reason."
"The US shouldn't negotiate with Russia directly. That's 100% wrong."
"We supply them, we keep their war running as long as they want to fight it."

### AI summary (High error rate! Edit errors on video page)

Talks about the term "exit strategy" in relation to Ukraine and US foreign policy.
Mentions the skewed perspective of Americans on war due to the last 50 years of US foreign policy.
Explains the defensive nature of the war in Ukraine and how it differs from US elective wars.
Emphasizes that the US is backing Ukraine, not leading the war, and should not negotiate with Russia directly.
Stresses that decisions about peace and territorial concessions lie with Ukraine, not the US.
Warns against imposing conditions on Ukraine for peace, as it could lead to US engaging in imperialism.
Advocates for letting the Ukrainian people decide the course of their war and not pressuring them to fight or quit.
Criticizes the idea of backing imperialist thoughts for better TV ratings.
Calls for respecting the agency of those fighting for liberation in Ukraine.

Actions:

for policymakers, activists,
Support Ukraine by providing aid and supplies (suggested).
Respect the agency of the Ukrainian people in deciding their war's course (implied).
</details>
<details>
<summary>
2022-10-26: Let's talk about expected economic numbers.... (<a href="https://youtube.com/watch?v=qvrNyKDCF38">watch</a> || <a href="/videos/2022/10/26/Lets_talk_about_expected_economic_numbers">transcript &amp; editable summary</a>)

Economists expect an expanding economy, fueled by steady consumer spending, but concerns about recession linger due to interest rate increases.

</summary>

"Despite all of our complaints about prices and inflation, we haven't stopped spending."
"The concerns about a recession are not over."
"Interest rate increases seem to be working for the moment."
"The expanding GDP is a sign of a healthy economy."
"Be ready for the Democratic Party to be jumping up and down and cheering about the fact that the GDP is expanding."

### AI summary (High error rate! Edit errors on video page)

Economists expect the GDP numbers to show an expanding economy between 2 and 3 percent.
Despite complaints about prices, consumer and retail spending remain steady and on the rise.
Strong job market and low unemployment contribute to the expanding economy.
The Democratic Party is expected to champion these numbers hitting right before the election.
Concerns about a recession are not over due to the Fed raising interest rates.
Companies may not borrow as much with interest rate increases, potentially causing a shrink in the economy later on.
The holiday season may help carry the economy through for a bit, but recession concerns persist.
Interest rate increases seem to be working for now but pose a risk later, combined with other economic factors.
The expanding GDP is seen as a sign of a healthy economy, with Democrats likely to focus on this messaging before the election.

Actions:

for economic analysts,
Monitor economic indicators closely for potential shifts (implied).
</details>
<details>
<summary>
2022-10-26: Let's talk about Trump's privilege claims.... (<a href="https://youtube.com/watch?v=eqUkYboe70Y">watch</a> || <a href="/videos/2022/10/26/Lets_talk_about_Trump_s_privilege_claims">transcript &amp; editable summary</a>)

Beau explains the legal developments around executive privilege, Trump, and the January 6th investigation, signaling potential trouble for Trump's defense as key figures are compelled to testify.

</summary>

"The rings around Trump, they're getting smaller and smaller."
"If Cipollone has to testify before the grand jury, they are in the room."
"They're at the point where they have pierced his inner circle."
"They are in the room where decisions are being made."
"They have a very clear picture at that point."

### AI summary (High error rate! Edit errors on video page)

Explains the focus on executive privilege, Trump, and the investigation around the events of January 6th.
Mentions the grand jury in the DC area and how Pence's inner circle has testified regarding Trump and January 6th.
Notes the prosecutors breaking through claimed executive privilege and attempting to compel White House counsel's office members to testify.
Emphasizes that this is a criminal case, not political, and how constitutional scholars believe testimony will be compelled due to the crime.
States that historically, the Supreme Court has ruled unanimously that executive privilege does not apply in criminal cases.
Speculates that efforts to block testimony indicate potentially incriminating information and a strategy to delay the legal process.
Suggests that if Cipollone testifies, it could be a significant blow to Trump's defenses as it pierces his inner circle.
Anticipates movement in the case soon, as key figures are now involved in the grand jury process.

Actions:

for legal observers, political analysts.,
Contact legal experts for insights on executive privilege and criminal investigations (suggested).
Stay informed about the developments in the legal case (implied).
</details>
<details>
<summary>
2022-10-25: Let's talk about a cancer vaccine.... (<a href="https://youtube.com/watch?v=SUn3-arvEWI">watch</a> || <a href="/videos/2022/10/25/Lets_talk_about_a_cancer_vaccine">transcript &amp; editable summary</a>)

A cancer vaccine by 2030, utilizing mRNA technology originally intended for cancer, offers hope amidst potential resistance and ongoing research progress.

</summary>

"By 2030, there will be ready for widespread use a vaccine for cancer."
"The real reason they were looking into the mRNA vaccines was for cancer."
"They're saying that they expect a cure or something that can change the way cancer patients live their lives."
"Given who is behind it, there's probably going to be resistance to it at first."
"The research has been going on a long time, and they're just now starting to see the end game."

### AI summary (High error rate! Edit errors on video page)

A pair of individuals who pioneered the COVID-19 vaccine have announced a bold statement that by 2030, a cancer vaccine will be ready for widespread use.
The mRNA vaccine technology used for COVID-19 was actually initially researched for cancer vaccines.
The goal is to train the body's immune system to attack cancer cells, a process different from the current status of cancer treatment.
The research for cancer vaccines benefited from insights gained during the COVID-19 response.
Progress is already being made in cancer vaccine trials, with the expectation that it can be tailored to different types of cancer and prevent recurrence.
The announcement brings hope and something positive to look forward to amidst challenging times.
Despite the promising news, there may be initial resistance fueled by fear-mongering and demonization of the science.
The research has been ongoing for a long time, and the end game is now starting to become visible.

Actions:

for healthcare professionals, researchers, cancer patients.,
Support cancer vaccine trials by participating (implied)
Stay informed about advancements in cancer vaccine research (implied)
Advocate for scientific progress and combat misinformation (implied)
</details>
<details>
<summary>
2022-10-25: Let's talk about Jan 6 information silos.... (<a href="https://youtube.com/watch?v=TQi6oE2Ofvk">watch</a> || <a href="/videos/2022/10/25/Lets_talk_about_Jan_6_information_silos">transcript &amp; editable summary</a>)

Beau addresses the misinformation surrounding January 6th, urging individuals to confront deception and manipulation with evidence, even if not everyone will be swayed.

</summary>

"They lied to you. What they told you wasn't true. Here is the evidence."
"We're fighting against people who just don't care about objective reality."
"You don't win every fight like this. But it'll work on a few."

### AI summary (High error rate! Edit errors on video page)

Addressing the aftermath of January 6th and the challenges of discussing it with individuals in closed information ecosystems.
People on the right-wing often argue that January 6th wasn't an insurrection because there were no guns involved, influenced by misinformation from their sources.
Despite the presence of significant weapons around the Capitol, some individuals deny the insurrection due to the absence of guns among the rioters.
A specific case is mentioned where a person dropped a firearm during the Capitol breach, received a five-year sentence, and used a baton on a police officer.
Beau suggests using such examples in dialogues to counter the false narrative that there were no guns present during January 6th.
Many individuals in the far-right movement were misled and lied to, rather than genuinely believing in its core beliefs.
Demonstrating objectively how they were misled might open the door for productive discourse and reflection.
Beau encourages addressing the misinformation spread, particularly during election season, by simplifying the message: "They lied to you."
Acknowledges that not everyone will be receptive to this information but believes it's still worth the effort to reach those who were misled.
Emphasizes the importance of confronting individuals who manipulate reality and deceive their audience for personal gain.

Actions:

for activists, educators, allies,
Challenge misinformation in your community by using concrete examples like the Capitol breach incident (suggested).
Engage in outreach efforts to confront deception and manipulation, especially during critical times like election season (suggested).
Encourage critical thinking and evidence-based discourse when addressing misled individuals (suggested).
</details>
<details>
<summary>
2022-10-25: Let's talk about Arizona and baseless theories.... (<a href="https://youtube.com/watch?v=TSQeUx2mP0Q">watch</a> || <a href="/videos/2022/10/25/Lets_talk_about_Arizona_and_baseless_theories">transcript &amp; editable summary</a>)

Beau addresses voter intimidation in Arizona, warning of legal consequences for spreaders of baseless conspiracy theories and possible civil proceedings in the future.

</summary>

"Voter intimidation is actually kind of like a big deal."
"If charges come from this, it's going to be a big deal for those involved."
"We're talking about people who are intentionally manipulating people and feeding them false information."
"This might be another case where we end up years from now seeing a civil proceeding against the people who put out the baseless theories."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses the current situation in Arizona regarding groups monitoring voting locations, particularly prevalent in Arizona.
Mentions the potentially intimidating tactics used by these groups, such as wearing tactical gear, masks, and photographing license plates.
Notes the temporary restraining order sought against one group and the involvement of the Secretary of State in Arizona and the Department of Justice.
Points out that voter intimidation is a significant issue with potential legal consequences for those involved.
Explores the motivations behind these actions, attributing them to a belief in baseless conspiracy theories.
Suggests that attention should also be directed towards those who propagate these theories, as they may bear responsibility for deterring people from voting.
Draws parallels to past cases where disseminating false information led to legal repercussions.
Raises the possibility of civil proceedings against those spreading baseless election theories in the future.
Emphasizes the negative impact of spreading false information and inciting actions that harm others.
Concludes by encouraging reflection on the implications of misinformation and its real-world consequences.

Actions:

for concerned citizens, activists,
Contact local election officials to report any instances of voter intimidation (suggested)
Support organizations working to combat misinformation and protect election integrity (exemplified)
</details>
<details>
<summary>
2022-10-24: Let's talk about the smartest Republican in the room.... (<a href="https://youtube.com/watch?v=gBRi9-L-V6Y">watch</a> || <a href="/videos/2022/10/24/Lets_talk_about_the_smartest_Republican_in_the_room">transcript &amp; editable summary</a>)

Beau explains McConnell's strategic foresight in supporting aid to Ukraine for long-term benefits, contrasting with short-sighted MAGA Republicans, urging reflection on Ukraine's global importance.

</summary>

"If they really don't understand the potential power of Ukraine and it not being an ally to the United States, perhaps even being upset because we yanked assistance in the middle of a war, they probably shouldn't be in office."
"McConnell is thinking further down the road, the same way Biden was, is."
"You have McCarthy and the MAGA Republicans saying that if they take over, if they win the midterms, well, they're going to cut off aid to Ukraine."
"McConnell seems to understand this."
"That's why he's doing it."

### AI summary (High error rate! Edit errors on video page)

Acknowledges Mitch McConnell as the smartest Republican in the room and addresses a public split in the Republican Party regarding aid to Ukraine.
Points out McConnell's political savviness and ability to think long-term beyond Fox News soundbites.
Contrasts McConnell's stance of supporting greater assistance to Ukraine with McCarthy and MAGA Republicans who want to cut off aid if they win the midterms.
Explains the potential consequences of cutting off aid to Ukraine, leading to prolonged conflict and economic hardship.
Criticizes conservative voices for questioning the importance of Ukraine to national security and dismisses their arrogance.
Emphasizes the strategic value of Ukraine as a potential ally in a future world where food scarcity might be a critical issue.
Suggests that McConnell's support for Ukraine is driven by his understanding of global food markets and the country's significance in addressing food insecurity.
Condemns politicians who prioritize short-term gains or foreign approval over understanding Ukraine's long-term importance.
Questions the competency of politicians who fail to grasp the strategic value of Ukraine and its potential impact on global food security.
Beau concludes by encouraging viewers to ponder these insights and wishes them a good day.

Actions:

for political observers,
Contact your representatives to advocate for continued support and aid to Ukraine (implied)
</details>
<details>
<summary>
2022-10-24: Let's talk about right wing commentators and Putin.... (<a href="https://youtube.com/watch?v=U3dbWaSgDGI">watch</a> || <a href="/videos/2022/10/24/Lets_talk_about_right_wing_commentators_and_Putin">transcript &amp; editable summary</a>)

Beau questions the patriotism of right-wing commentators who undermine America's interests while claiming to be patriots.

</summary>

"They're rooting for a country that has openly said it wants to sow discord in the United States and that it wants the US to fail."
"Maybe they're not really America first. Maybe they don't really support a constitutional republic, as they claim."
"They're not nationalists. They're not America first."
"They just have their audience believing that they are while they feed them talking points that undermine their audience's actual beliefs."
"Maybe that audience should really start to think about the rhetoric that they're being fed."

### AI summary (High error rate! Edit errors on video page)

Analyzing the disconnect between what political commentators claim to believe and what they are actually saying.
Right-wing commentators who advocate for America first are not reflecting American talking points in the context of Ukraine.
Questioning the fear-mongering tactics employed by these commentators regarding the use of nuclear weapons in Ukraine.
Pointing out the contradiction in their behavior – claiming to be patriots but not acting in the best interest of America.
Suggesting that these commentators are undermining the United States by supporting actions that weaken the country.
Implying that these commentators might not truly support a constitutional republic but instead favor authoritarian rule by elite figures.
Criticizing their admiration for Putin and the potential implications of his failure in the current conflict.
Challenging the audience to reexamine the rhetoric they are being fed and whether it truly serves American interests.
Warning that many individuals may have been deceived by these commentators into supporting positions contrary to American values.
Concluding with a call for reflection on the audience's alignment with ideologies that may not be in the best interest of the country.

Actions:

for political observers,
Reexamine the rhetoric being consumed (suggested)
Challenge allegiance to commentators who may not have America's best interests at heart (implied)
</details>
<details>
<summary>
2022-10-24: Let's talk about polls and who will win the midterm.... (<a href="https://youtube.com/watch?v=3l3GIuNYGFg">watch</a> || <a href="/videos/2022/10/24/Lets_talk_about_polls_and_who_will_win_the_midterm">transcript &amp; editable summary</a>)

Beau explains that the midterm election outcome hinges on unlikely voters showing up and voting, potentially favoring the Democratic Party, despite outlier beliefs about the economy.

</summary>

"Who's going to win the midterms? The answer is, whoever has the most people show up in their favor."
"This election will be decided by the unlikely voters."
"It's going to be decided by those people who show up who normally don't and who they vote for."

### AI summary (High error rate! Edit errors on video page)

Explains that the key question is who will win the midterms, and the simple answer is whoever has the most people show up to vote.
Points out that polling often shows candidates within one or two points, swinging back and forth within the margin of error.
Emphasizes that likely voters are typically surveyed in polls, but it will be the unlikely voters who determine the election outcome.
Notes that it will be the voters who don't usually participate in elections that will decide the midterms.
Suggests that historically, if unlikely voters show up, they tend to support Democratic candidates.
Mentions the outlier belief that Republicans handle the economy better, despite evidence to the contrary.
Asserts that the election outcome is unlikely to change before the election and will be decided by the unlikely voters, who may not vote in every election.
Concludes by stating that the energized bases will show up, but the key factor will be those who usually don't participate in midterms.

Actions:

for voters,
Contact unlikely voters in your community to encourage them to participate in the upcoming midterms (implied).
</details>
<details>
<summary>
2022-10-23: Let's talk about whistles, DOD policy, and what's next.... (<a href="https://youtube.com/watch?v=PewbsfDfaLo">watch</a> || <a href="/videos/2022/10/23/Lets_talk_about_whistles_DOD_policy_and_what_s_next">transcript &amp; editable summary</a>)

The Department of Defense implements a policy for service members' health care and family planning, while political wedge issues threaten military readiness and base existence.

</summary>

"Nothing is more important to me or to this department than the health and well-being of our service members."
"Readiness is one of the most critical things in the military."
"If they don't re-enlist because of that, it becomes a readiness issue."
"You're chasing them away because you're impacting readiness."
"The Republican Party and their wedge issue trying to motivate their base is going to drive their states even deeper into financial turmoil."

### AI summary (High error rate! Edit errors on video page)

Department of Defense implements a new policy allowing service members to travel for health care or family planning.
Republicans criticize the Biden administration, accusing them of an election season stunt, but the policy has been in effect since June.
Secretary of Defense prioritizes the health and well-being of service members, civilian workforce, and families.
The policy ensures seamless access to reproductive health care without burning leave time.
Decoding messages from the Secretary of Defense is not difficult; readiness is a key priority.
Anti-LGBTQ legislation and restrictions on women's rights impact military readiness.
If service members refuse to re-enlist due to discriminatory laws in certain states, it affects military readiness and base existence.
Politicians must explain to constituents why their livelihood tied to military bases might be at risk due to wedge issues.
Impact on readiness could lead to closures of bases and unemployment in surrounding communities.
Red states heavily rely on military presence, but discriminatory laws could drive the military away.

Actions:

for military personnel, policymakers, community members,
Advocate for inclusive policies within the military (exemplified)
Support service members affected by discriminatory legislation (exemplified)
Educate communities on the importance of military presence (exemplified)
</details>
<details>
<summary>
2022-10-23: Let's talk about gas prices, crabs, and water.... (<a href="https://youtube.com/watch?v=QqdEVoIx5nE">watch</a> || <a href="/videos/2022/10/23/Lets_talk_about_gas_prices_crabs_and_water">transcript &amp; editable summary</a>)

Beau connects climate change impacts on crabs, water, and gas prices, urging immediate action for a sustainable future and criticizing those misleading the public on transitioning away from fossil fuels.

</summary>

"The food supply was interrupted because of climate change."
"Transitioning away from gas is in your best interest."
"Those who are telling you you don't need to worry about the climate, that we don't need to transition, and in a quick manner, they're lying to you."

### AI summary (High error rate! Edit errors on video page)

Connects crabs, water, and gas prices to urge Americans to pay attention and think long-term.
Mentions the impact of climate change on crab season in Alaska.
Expresses how food supply interruption due to climate change is a scarier concept than just a closed crab season.
Compares the volatility of the global food market to that of oil prices based on supply.
Warns about the potential future volatility in basic food prices and eventually water.
Optimistically believes in the possibility of change and mitigation but stresses the need to act quickly.
Criticizes the influences of big oil companies and commentators in hindering the transition away from fossil fuels.
Emphasizes that transitioning from gas is in everyone's best interest to avoid worsening market volatility and emissions.
Reminds that politicians often lie and that those dismissing climate concerns are not acting in the public's best interest.
Urges viewers to see through the rhetoric and take action towards a sustainable future.

Actions:

for americans,
Transition away from gas to support a more stable market and reduce emissions (implied).
Challenge misinformation and rhetoric from politicians and commentators invested in oil companies (implied).
Advocate for funding transitions towards sustainability in the country (implied).
</details>
<details>
<summary>
2022-10-23: Let's talk about Biden's move with Cyprus and foreign policy.... (<a href="https://youtube.com/watch?v=txqpMJTdqRQ">watch</a> || <a href="/videos/2022/10/23/Lets_talk_about_Biden_s_move_with_Cyprus_and_foreign_policy">transcript &amp; editable summary</a>)

Beau explains the US decision to lift the arms embargo against Cyprus as part of a strategic move to assist Ukraine, not alienate Turkey, in the international poker game.

</summary>

"The United States is sliding Cyprus a card, and in return, they're going to slide one to Ukraine."
"It's not that the United States or the Biden administration is trying to alienate Turkey. It's just right now, they're cheating in a different direction."
"But when you look at foreign policy moves, always wonder what happens next."

### AI summary (High error rate! Edit errors on video page)

Explains the recent decision by the Biden administration to lift the arms embargo against Cyprus.
Cyprus has had an arms embargo since the late 80s, and now they can start acquiring arms again.
The reason behind this move is not about what Cyprus is going to buy but what they already have.
Cyprus possesses military hardware like tanks, infantry fighting vehicles, rocket artillery, and air defense from Warsaw Pact manufacture.
It is likely that Cyprus will seek new American equipment to replace their existing weaponry.
Beau speculates that the US is providing Cyprus with new arms and, in return, Cyprus will pass on some of their equipment to Ukraine.
The move is not to provoke Turkey but to assist Ukraine with capable military equipment.
The US is playing a strategic game in the international arena, making moves that may seem puzzling at first glance.
Beau suggests that the deal to provide Cyprus with new equipment and pass on the old to Ukraine may have already been established.
The decision is part of a larger foreign policy strategy, and one must always anticipate the next steps in such moves.

Actions:

for foreign policy enthusiasts,
Contact organizations supporting diplomatic efforts for peace in regions affected by arms trade (implied)
Support initiatives advocating for transparency in international arms dealings (implied)
</details>
<details>
<summary>
2022-10-22: Let's talk about what's happening in Gulfport, Mississippi.... (<a href="https://youtube.com/watch?v=1kBuljt71-w">watch</a> || <a href="/videos/2022/10/22/Lets_talk_about_what_s_happening_in_Gulfport_Mississippi">transcript &amp; editable summary</a>)

Beau sheds light on the importance of releasing video footage in a police shooting involving a 15-year-old to prevent community unrest and assumptions of wrongdoing by the department.

</summary>

"If you hide it, the only thing the public can assume is that you're hiding it for one of the reasons that includes the department doing something wrong."
"You have the opportunity to diffuse this now. It seems like the smart move."
"And if it is one of the reasons where the department didn't do what they were supposed to, it's probably better if the public finds out sooner rather than later."

### AI summary (High error rate! Edit errors on video page)

Introduces the familiar story of a police shooting in Gulfport, Mississippi, involving a 15-year-old.
Eyewitnesses agree that the kid didn't have anything in his hands except for the police.
Beau expresses distrust in both eyewitness reports and police reports.
Emphasizes the importance of video footage in understanding what happened.
Points out the lack of public footage, including body cam and dash cam footage, in this case.
Raises questions about why officials are hesitant to release the footage.
Mentions that the family has hired lawyer Crump and current demonstrations are demanding the footage.
Suggests that if the department believes they did nothing wrong, they should allow Crump to view the footage to ease tensions.
Argues that withholding footage when it could confirm the department's actions as justified puts the blame on the department if demonstrations escalate.
Urges for transparency to avoid assumptions of wrongdoing by the department and to reassure the community.

Actions:

for community members, activists,
Demand the release of video footage from the police department (suggested)
Support demonstrations calling for transparency and accountability (exemplified)
Advocate for allowing lawyer Crump to view the footage to ensure transparency (implied)
</details>
<details>
<summary>
2022-10-22: Let's talk about Vincent van Soup.... (<a href="https://youtube.com/watch?v=5COJyheLHSE">watch</a> || <a href="/videos/2022/10/22/Lets_talk_about_Vincent_van_Soup">transcript &amp; editable summary</a>)

Beau explains the strategic and attention-grabbing nature of the Van Gogh soup incident by climate activists without causing harm or property damage.

</summary>

"If it bleeds, it leads, you know?"
"I don't know what more you want."
"A little odd, you can critiqu anything."
"I don't know what more you want from them really."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the Van Gogh soup incident involving climate activists throwing soup on a painting in London.
Compares the act to another extreme protest where someone lit themselves on fire to bring attention to climate change.
Argues that the purpose of such demonstrations is to gain publicity, which was achieved.
Analyzes the cost of the operation, comparing it to the cost of a primetime ad.
Emphasizes the lack of risk involved in the protest and the absence of property damage or loss of life.
Suggests that targeting the art world, a realm dominated by the rich, was strategic in drawing attention to climate change.
Acknowledges that while the act may seem odd, it effectively garnered attention without risking harm to individuals.

Actions:

for climate activists,
Organize attention-grabbing demonstrations that prioritize safety and avoid harm (exemplified)
Target strategic locations or events to draw attention to climate issues (implied)
Utilize creative tactics to generate buzz and publicity for climate change causes (suggested)
</details>
<details>
<summary>
2022-10-22: Let's talk about Lavrov and Russia severing ties..... (<a href="https://youtube.com/watch?v=i050mt-7OAc">watch</a> || <a href="/videos/2022/10/22/Lets_talk_about_Lavrov_and_Russia_severing_ties">transcript &amp; editable summary</a>)

Sergei Lavrov's threats to cut ties with the West are seen as a bluff by Beau, who believes it's more about frustration than actual diplomatic strategy.

</summary>

"Russia is having a hard time finding its off-ramp."
"It is a child stomping their feet, trying to get their way."
"They're not gonna sever ties with the West. That's not even a thing."

### AI summary (High error rate! Edit errors on video page)

Sergei Lavrov, the foreign minister of Russia, hinted at cutting ties with the West during a speech to new diplomats inducted into the Russian foreign service.
Lavrov suggested that Russia is ready to sever ties with Western states due to Europe's decision to cut off economic cooperation.
Beau questions the feasibility of Russia completely cutting ties with the West, especially during a time of war where intelligence is critical.
Lavrov's threat to sever ties is seen as a tactic to scare the West into taking certain actions, but it is likely not a genuine intention.
Russia's diplomatic efforts to establish peace and buy time for military maneuvers are not yielding the desired results.
Putin may be pressuring Lavrov, leading to provocative statements and actions from the Russian foreign minister.
Despite heightened tensions, direct lines of communication between nations usually exist beyond ambassadors and diplomats.
Beau views Russia's threats as a bluff and a sign of frustration rather than a serious diplomatic strategy.

Actions:

for world leaders,
Stay informed about international relations and diplomatic strategies (implied)
</details>
<details>
<summary>
2022-10-21: Let's talk about which party you want during a recession.... (<a href="https://youtube.com/watch?v=zzFvKxpP_cs">watch</a> || <a href="/videos/2022/10/21/Lets_talk_about_which_party_you_want_during_a_recession">transcript &amp; editable summary</a>)

Top economists predict a recession, raising concerns about layoffs and influencing midterm elections; understanding party positions on social safety nets is key to voting decisions amidst economic uncertainty.

</summary>

"Companies are cold, unfeeling, profit-driven machines."
"If this is your concern, if this is something that you're going to send a message about, it's probably something that is weighing pretty heavily on you and is going to be a motivating factor."
"One party you know won't [help]."

### AI summary (High error rate! Edit errors on video page)

Top economists are predicting a recession within the next year, raising concerns about the economy and its impact on the upcoming midterm elections.
People are questioning how this economic information will benefit them in the context of potential layoffs during a recession.
Companies no longer follow the trend of laying off newer employees first; instead, they often let go of longer-tenured employees due to higher costs.
Government intervention to prevent layoffs is unlikely, as companies prioritize profit over job security.
When considering which party to support in Congress to address layoffs, it's vital to look at their stance on social safety nets and support for those facing financial challenges.
The Republican and Democratic parties have differing approaches to social safety nets, entitlements, and supporting individuals who have been laid off.
Individuals are advised to scrutinize party platforms regarding assistance for those in financial distress, as this may influence their voting decisions.
While the severity of the predicted recession is uncertain, understanding candidates' positions on social safety nets is key to making an informed choice.

Actions:

for voters,
Scrutinize party platforms on social safety nets and support for those facing financial challenges (suggested).
Make an informed decision based on candidates' positions regarding assistance for individuals in financial distress (implied).
</details>
<details>
<summary>
2022-10-21: Let's talk about the Mississippi going dry.... (<a href="https://youtube.com/watch?v=-fY_ZLPhMgQ">watch</a> || <a href="/videos/2022/10/21/Lets_talk_about_the_Mississippi_going_dry">transcript &amp; editable summary</a>)

Beau explains the Mississippi River's low water levels, impacted by drought and climate, urging action to address environmental concerns.

</summary>

"The planet's not bad at messaging. It's super clear about what's going on."
"It's another sign. It's another example of major features in the United States saying, hey, this is an issue."

### AI summary (High error rate! Edit errors on video page)

Explains the current situation with the Mississippi River and addresses comparisons to Lake Mead drought concerns.
Describes the immense size and vulnerability of the Mississippi River due to its length and the various sources flowing into it.
Notes the unusual access to islands in the river, like Tower Rock, due to significantly low water levels.
Mentions record low water levels in Chester, Illinois, and Memphis, impacting shipping and supply chain issues.
Talks about the drought affecting states feeding into the Mississippi River and the lack of precipitation in the Ohio River Valley.
States that experts suggest the situation will improve by spring, with expected rain in the Ohio area helping to raise water levels.
Emphasizes that while the situation with the Mississippi River is concerning, it may not be as dire or prolonged as other water crises in the country.
Points out that environmental changes like these are clear indicators that action needs to be taken to address climate issues.

Actions:

for climate activists, environmentalists, general public,
Monitor and support initiatives addressing climate change impacts on water sources (suggested)
Stay informed about water conservation practices and advocate for sustainable water management (implied)
</details>
<details>
<summary>
2022-10-21: Let's talk about Trump, O'Dea, and Colorado.... (<a href="https://youtube.com/watch?v=0QCf87bdJ5Y">watch</a> || <a href="/videos/2022/10/21/Lets_talk_about_Trump_O_Dea_and_Colorado">transcript &amp; editable summary</a>)

Republican troubles in Colorado as Trump's influence may cost them a critical Senate seat with a moderate nominee.

</summary>

"MAGA doesn't vote for stupid people with big mouths."
"When you look at the Sedition Caucus, this is pretty much what they are."
"If people do vote for him, all it does is show how weak that man is."

### AI summary (High error rate! Edit errors on video page)

Republican troubles in Colorado with their moderate nominee for Senate, O'Day.
O'Day is a moderate supporting same-sex marriage and most family planning, not extreme like other Republican candidates.
Colorado Republicans need to gain unaffiliated voters to have a chance at winning the Senate seat.
Trump instructed MAGA not to vote for O'Day, potentially costing the Republicans the seat.
O'Day is not a Trump supporter and may have his campaign ended due to Trump's influence.
Trump's instruction could lead to the loss of a critical Senate seat for the Republican Party.

Actions:

for colorado voters,
Support O'Day's campaign by volunteering or donating (implied)
</details>
<details>
<summary>
2022-10-20: Let's talk about candidates calling on Pence over Trump.... (<a href="https://youtube.com/watch?v=8unldZfSHrI">watch</a> || <a href="/videos/2022/10/20/Lets_talk_about_candidates_calling_on_Pence_over_Trump">transcript &amp; editable summary</a>)

A split in the Republican Party emerges as candidates shift towards Pence, causing animosity and revealing self-serving motives over ideology.

</summary>

"They're not the MAGA crowd. They're not America first. They're out for themselves and they are playing on the gullible."
"This is what happens when you have a group of self-serving people who really aren't ideologically aligned except in pursuit of their own power."
"And Trump is probably going to say something on his little wannabe Twitter about it."
"I think most [moderate voters] would still view Pence as somebody who did not really step up and talk to the committee the way they probably should have."
"It's going to cause problems for the Republican Party long term because it is going to generate a lot of animosity within the party and among candidates."

### AI summary (High error rate! Edit errors on video page)

A new development within the Republican Party is causing a split and upsetting the former president.
Republican candidates are now reaching out to Pence, not Trump, to appeal to moderates.
Candidates who sought Trump's approval are now turning to Pence for support, showcasing a widening split within the party.
Pence's actions, campaigning for those who criticized him, may negatively impact his reputation with moderate voters.
The shift towards Pence over Trump is creating animosity within the Republican Party.
The move towards Pence is seen as self-serving and not ideologically driven.
Many voters view Pence as not stepping up during critical moments.
Trump's likely response to Pence gaining attention could reignite news coverage on extreme positions of candidates.
The focus on power and self-interest among candidates is detrimental to the party's unity.
The situation is likely to generate long-term problems and animosity within the Republican Party.

Actions:

for republican voters,
Reach out to moderate voters and build community support for candidates by engaging in local political initiatives (suggested).
Stand up against self-serving actions within the Republican Party by promoting transparency and accountability (implied).
</details>
<details>
<summary>
2022-10-20: Let's talk about Georgia's midterm dynamics.... (<a href="https://youtube.com/watch?v=RENjU6i5ZdU">watch</a> || <a href="/videos/2022/10/20/Lets_talk_about_Georgia_s_midterm_dynamics">transcript &amp; editable summary</a>)

Georgia's unique dynamics and the MAGA dilemma create uncertainty in the upcoming election, challenging predictions and potentially leading to surprises.

</summary>

"Georgia's unique dynamics make predicting the election outcome challenging."
"The MAGA faithful in Georgia face a dilemma between Abrams and Kemp."
"The unpredictability in Georgia's election dynamics raises doubts about polling accuracy."

### AI summary (High error rate! Edit errors on video page)

Georgia's unique dynamics make predicting the election outcome challenging.
Unlikely voters and polarized dynamics could impact voting patterns.
The MAGA faithful in Georgia face a dilemma between Abrams and Kemp.
Kemp's defiance against Trump during the election pressure campaign is not respected by some MAGA supporters.
Some voters may opt for third-party candidates due to their reluctance to support Kemp.
The MAGA crowd is known to be vindictive and may not easily forgive perceived betrayals.
Uncertainty looms over how the MAGA faithful will vote in the upcoming election.
Beau questions whether the MAGA Republicans will turn out for Walker as expected.
The unpredictability in Georgia's election dynamics raises doubts about polling accuracy.
Beau anticipates potential surprises in the election results.

Actions:

for georgia voters,
Reach out to unlikely voters to encourage participation (suggested).
Support third-party candidates if dissatisfied with major party options (implied).
</details>
<details>
<summary>
2022-10-20: Let's talk about Biden vs Oil companies.... (<a href="https://youtube.com/watch?v=RgHsQ1Ee6jE">watch</a> || <a href="/videos/2022/10/20/Lets_talk_about_Biden_vs_Oil_companies">transcript &amp; editable summary</a>)

Biden administration confronts oil companies on gas prices, facing pressure to alleviate public hardship amid record profits and political implications.

</summary>

"Inflation caused by transportation costs and oil companies maximizing profits."
"President's job is to safeguard the national security of the United States."
"Most people look at the TV and they associate the head of state with the gas pump."

### AI summary (High error rate! Edit errors on video page)

Biden administration set to meet with oil companies to address rising gas prices.
Rumor suggests Biden will urge oil companies to lower prices and help American people.
Massive oil companies like Exxon, Chevron, and Shell posted record profits while consumers suffered at the pump.
Inflation caused by transportation costs and oil companies maximizing profits.
President's role in safeguarding national security may involve transitioning away from expensive oil.
Limited control over oil prices despite public perception linking the president to gas prices.
Biden administration faces a challenging relationship with the oil industry.
Outcome of the meeting could lead to oil companies cooperating or facing stronger measures.
Public perception often associates the president with gas prices.
Democratic Party concerned about potential political fallout from rising gas prices before the election.

Actions:

for citizens, voters, activists,
Contact local representatives to advocate for policies that prioritize public affordability over corporate profits (implied).
</details>
<details>
<summary>
2022-10-19: Let's talk about how the US military is a birthday cake in Ukraine.... (<a href="https://youtube.com/watch?v=9G39T4z0eiI">watch</a> || <a href="/videos/2022/10/19/Lets_talk_about_how_the_US_military_is_a_birthday_cake_in_Ukraine">transcript &amp; editable summary</a>)

Beau compares the US military to a birthday cake, explaining its capabilities and why comparing it to Ukraine's situation against Russia misses critical information.

</summary>

"Picture the US military as a birthday cake."
"The United States military owns two things, the sky and the night."
"The US doesn't lose wars, it loses the peace."
"Taking out another country's military, it's as easy as cake."
"If you wanted to use Ukraine versus Russia as a metric for NATO or the US versus Russia, you have to acknowledge that disparity."

### AI summary (High error rate! Edit errors on video page)

Compares the US military to a birthday cake to explain its capabilities.
Explains that Ukraine, using US weapons and tactics against Russia, lacks the full "birthday cake" capabilities.
Illustrates the importance of the US military's top layer (aircraft carriers, air power) in conflicts.
Points out that the US military's dominance lies in its air power, owning the sky and the night.
States that the US doesn't need to go nuclear in conflicts due to its conventional combat capabilities.
Emphasizes that the critical piece of information missing when comparing Ukraine to the US is the US air power.
Notes that the US excels in wars against nation-states but struggles with peace and occupation.
Concludes by suggesting that measuring Russia versus NATO using Ukraine as a metric is flawed due to missing critical information.

Actions:

for military analysts,
Contact military experts to understand the nuances of military capabilities (suggested)
Join defense strategy forums or organizations to learn more about military tactics and capabilities (suggested)
</details>
<details>
<summary>
2022-10-19: Let's talk about being Steadfast at Noon in the face of uncertainty.... (<a href="https://youtube.com/watch?v=sbvZptREKgc">watch</a> || <a href="/videos/2022/10/19/Lets_talk_about_being_Steadfast_at_Noon_in_the_face_of_uncertainty">transcript &amp; editable summary</a>)

People near military installations got nervous about a NATO exercise involving B-52s and nuclear response training, but it's all part of routine training.

</summary>

"If you see B-52s move, if you live near a NATO installation and a whole bunch of vehicles roll through town or fly overhead, don't panic."
"It's called Steadfast Noon, if you want to Google it."

### AI summary (High error rate! Edit errors on video page)

People near military and NATO installations got nervous due to ongoing NATO exercise called Steadfast Noon involving 14 countries and around 60 aircraft, including B-52s.
The assets seen in movement were related to potential nuclear response capability, but it's all part of the exercise.
The exercise is to train on providing nuclear capability to non-nuclear NATO members.
Similar exercises happen annually around this time, but due to heightened geopolitical tensions, people are more on edge this year.
Russia is aware of these exercises and often runs its own exercises concurrently, mirroring NATO's actions.
NATO conducts numerous exercises each year, with over 88 in 2020 and more than 100 planned before the pandemic.
Despite the use of real missiles, they are likely without warheads for practice.
If people see military activity near NATO installations, like B-52s or vehicles, it's part of the exercise and not a cause for panic.
The exercises are well-coordinated and routine, designed to prepare for various scenarios.
The lack of media coverage and understanding can lead to unnecessary concern, but it's all part of a regular training exercise.

Actions:

for community members near military installations,
Research more about NATO exercises and their purpose (suggested)
Stay informed about military activities in your area (suggested)
</details>
<details>
<summary>
2022-10-19: Let's talk about Trump's nonsense with the special master.... (<a href="https://youtube.com/watch?v=uYRsAuudSqU">watch</a> || <a href="/videos/2022/10/19/Lets_talk_about_Trump_s_nonsense_with_the_special_master">transcript &amp; editable summary</a>)

Judge Derry is unsatisfied with Team Trump's assertions in the criminal investigation, putting them in an unfavorable position with a looming deadline.

</summary>

"Judge Derry is not happy."
"Team Trump is not in a good position."
"This is kind of a formality at this point anyway."

### AI summary (High error rate! Edit errors on video page)

Judge Derry is not pleased with Team Trump's objections and assertions in the criminal investigation of former President Trump.
Team Trump is arguing that certain documents are both personal records and covered by executive privilege, which Judge Derry finds incongruous.
The judge does not seem inclined to accept Team Trump's arguments regarding executive privilege.
Team Trump has until November 12th to present their arguments, but based on the current situation, they are not in a favorable position.
The special master process is unlikely to be completed before the midterms, as expected, due to appeals and delays.
Documents containing national defense information have already been dealt with; the remaining thousands may not be significant in a potential criminal case.
There may not be many developments in the document case before the midterms.

Actions:

for legal analysts, political watchdogs,
Stay informed on the latest developments in the criminal investigation of former President Trump (suggested).
Monitor updates on the special master process and any legal implications (suggested).
</details>
<details>
<summary>
2022-10-18: The roads to your Patreon questions.... (<a href="https://youtube.com/watch?v=UL_dYzBwIX0">watch</a> || <a href="/videos/2022/10/18/The_roads_to_your_Patreon_questions">transcript &amp; editable summary</a>)

Beau answers Patreon members' questions, debunks theories, encourages community organizing, and shares insights on various topics, all while maintaining a genuine and no-drama style.

</summary>

"Context is king."
"We have progress, but there's an awful lot of pain in the short to midterm."
"Capacity building, community networking, building that local network."
"If you organize and build a network of people loosely committed to the same goal, you can address problems."
"I try to focus on the positive where I can."

### AI summary (High error rate! Edit errors on video page)

Answers Patreon members' questions.
Considers collaborating with Robert Evans.
Debunks the independent state legislature theory.
Mentions an ongoing counterintelligence investigation.
Talks about the lack of security culture in the White House under Trump.
Addresses the issue of missing government documents.
Shares thoughts on certain books and theories.
Talks about climate change and bug-out bag essentials.
Mentions the potential declassification of classified folders.
Encourages organizing at the local level to address problems.
Expresses admiration for the YouTube channel "Crime Pays But Botany Doesn't."
Recounts a story about Kennedy visiting a Special Forces group.
Talks about community policing and disaster relief efforts.
Shares insights on historical figures and foreign policy issues.
Talks about his accent and persona on YouTube.

Actions:

for community members,
Build a local network to address problems ( suggested, exemplified )
Contribute through YouTube's thanks button to support disaster relief efforts ( exemplified )
</details>
<details>
<summary>
2022-10-18: Let's talk about the economy and a democratic problem.... (<a href="https://youtube.com/watch?v=zGOmbmP7m-0">watch</a> || <a href="/videos/2022/10/18/Lets_talk_about_the_economy_and_a_democratic_problem">transcript &amp; editable summary</a>)

Beau talks about the Democratic Party's elitist image and failure to explain the economic downturn that started before the pandemic, focusing on Trump's trade war and supply chain issues.

</summary>

"We're just commoners and we can't understand it."
"The Democratic Party could be out there explaining this."
"It's not something that most Americans can't grasp."
"That's one of the biggest problems with the Democratic Party."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Talks about the perception problem of the Democratic Party being seen as elitist.
Mentions the Democratic Party's failure to make their case to working-class individuals.
Points out how the Democratic Party didn't explain the economic downturn that started before the pandemic.
Indicates that the economic turbulence began in August 2019 due to the trade war initiated by Trump.
Explains how tariffs raised import prices, impacting consumers.
States that the recession was predicted before the pandemic, specifically on August 14th, 2019.
Suggests that the pandemic prolonged the economic instability because the country prioritized the economy over public health.
Emphasizes that the Democratic Party should address and explain these economic issues to the public.
Notes that presidents typically don't control the economy but acknowledges Trump's role in triggering economic challenges through tariffs.
Stresses the importance of understanding the supply chain issues that predated the pandemic and affected the recession timeline.

Actions:

for politically aware individuals,
Share information about the economic issues discussed in the transcript (suggested).
Educate others about the impact of policies on the economy (implied).
Advocate for transparency and clarity in political messaging (implied).
</details>
<details>
<summary>
2022-10-18: Let's talk about mobilization, Putin, and sleeping.... (<a href="https://youtube.com/watch?v=uY-VdXhVawI">watch</a> || <a href="/videos/2022/10/18/Lets_talk_about_mobilization_Putin_and_sleeping">transcript &amp; editable summary</a>)

Beau analyzes Putin's statement on Ukraine mobilization, criticizes Russian military's lack of foresight and unit cohesion issues, contrasting hyper-masculine culture with a need for sensitivity training.

</summary>

"The whole point of being woke is so they don't catch you while you're sleeping."
"Much more unlikely with the U.S."
"That hyper-masculine military exists. It's that steaming pile of failure over there in Russia."
"And one of the Russian officers basically insulted their god. And the Tajik troops lit him up."
"Well, howdy there, Internet people."

### AI summary (High error rate! Edit errors on video page)

Putin claimed mobilization in Ukraine will end in a couple weeks, but it is unlikely given Russian military's inability to foresee nightfall.
Mobilization in Ukraine facing resistance, protests, mass exodus, attacks, and internal conflicts like a Russian officer insulting a Tajik god.
Lack of unit cohesion among Russian and Tajik troops due to differing cultural identities and lack of sensitivity training.
Beau criticizes hyper-masculine military culture in Russia, contrasting it with the U.S. military's likely response to such conflicts.
Sensitivity training could have prevented the clash between Russian and Tajik troops at the mobilization center.

Actions:

for military reform advocates,
Implement sensitivity training for military personnel (implied)
Advocate for diverse and inclusive military culture (implied)
</details>
<details>
<summary>
2022-10-18: Let's talk about Biden's student debt relief going live.... (<a href="https://youtube.com/watch?v=mV-EJCx9g5I">watch</a> || <a href="/videos/2022/10/18/Lets_talk_about_Biden_s_student_debt_relief_going_live">transcript &amp; editable summary</a>)

Beau explains the Biden Student Debt Relief Program deadlines and income-based application process, urging quick action for potential debt relief of $10,000 to $20,000.

</summary>

"8 million people have already applied."
"It's a bureaucracy because it's the government."
"I mean, I don't know. It's a 5 to 10 minute process and you're going to get either $10,000 or $20,000."

### AI summary (High error rate! Edit errors on video page)

Beau provides an overview of the Biden Student Debt Relief Program and addresses the confusion surrounding the application timelines.
The application for student debt relief is now live on studentaid.gov, requiring basic information such as date of birth, social security number, and contact details, and it only takes 5 to 10 minutes to complete.
There are two deadlines being discussed: November 15th and December 31st. November 15th is more urgent for those whose remaining debt is less than the relief amount, as payments restart in January.
Individuals with debt higher than the relief amount have until December 31st to submit their applications.
Beau speculates on the bureaucratic reasons behind requiring individuals to apply for debt relief, even though the government already has their records.
The relief program is income-based, with a cutoff at around $125,000, and applicants may need to provide additional verification like tax returns.
Beau encourages viewers to take the 10 minutes needed to apply for the relief program as it can result in $10,000 to $20,000 worth of debt relief.

Actions:

for students and individuals with student debt,
Apply for the Biden Student Debt Relief Program before the deadlines (suggested).
Share information about the relief program with others who may benefit (implied).
</details>
<details>
<summary>
2022-10-17: Let's talk about the most important midterm issue.... (<a href="https://youtube.com/watch?v=g-P72CMpsmA">watch</a> || <a href="/videos/2022/10/17/Lets_talk_about_the_most_important_midterm_issue">transcript &amp; editable summary</a>)

Respecting election outcomes is the fundamental criterion for candidates, shaping true representation and democracy before addressing other critical issues like environmental policy.

</summary>

"The number one question, the first question you have to ask, is whether or not that candidate respected the results of the last election."
"If they didn't respect the results, especially now that we know and we've seen the footage of them talking about doing what they did before the election even happened, what does it say about them?"
"They're not going to be your representative. They're going to be your ruler."
"I don't see how you could vote for somebody who is flat out saying that they don't care about your vote, that your voice is irrelevant."
"Until we have people that actually represent us and respect the results of elections, you're not going to get anywhere."

### AI summary (High error rate! Edit errors on video page)

Identifies the most critical issue for the midterms, focusing on respecting election results as the number one question to ask candidates.
Expresses disappointment that environmental policy cannot be the deciding factor due to the current political climate.
Emphasizes the importance of candidates respecting election outcomes as a reflection of their commitment to democracy and representation.
Raises concerns about candidates who undermine the electoral process and disregard the voice of the people.
Argues that supporting candidates who do not respect election results means accepting rulers rather than representatives.
Advocates for prioritizing candidates who genuinely represent the people before addressing other critical issues like environmental policy.

Actions:

for voters,
Verify candidates' stance on respecting election results (suggested)
Prioritize supporting candidates who value democracy and representation (exemplified)
</details>
<details>
<summary>
2022-10-17: Let's talk about snow crabs.... (<a href="https://youtube.com/watch?v=KcphFNenWZA">watch</a> || <a href="/videos/2022/10/17/Lets_talk_about_snow_crabs">transcript &amp; editable summary</a>)

Alaska cancels snow crab season due to population collapse, raising concerns on economic impact and urging a shift towards considering the costs of inaction on climate change.

</summary>

"This is a warning."
"Generations of a species disappearing should factor into this in some way."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Alaska canceled the snow crab season, citing the population collapse.
The term "overfished" allows the government to take action, even if fishing wasn't the cause.
The snow crab population decreased from 8 billion in 2018 to 1 billion in 2021.
Factors like warming waters, disease, or migration to cooler areas could have caused the decline.
Coverage mainly focuses on the economic impact on fishermen, not enough on climate change.
Beau suggests tying climate change to the economic impact to stress the cost of inaction.
Transitioning to mitigate climate change requires significant investment and preparation.
Failure to address climate change could lead to more industries collapsing and workers becoming economic refugees.
Beau warns about the long-term consequences of not taking action to mitigate climate change.
The focus on money in the US may require framing the cost of inaction to spur more action.
Generations of fishermen are losing their livelihoods due to the collapse of the snow crab population.
The disappearance of billions of a species should be a significant concern, not just an economic one.
Beau urges for a shift in focus towards considering the costs of inaction when discussing climate change.

Actions:

for climate activists, policymakers, seafood industry workers,
Advocate for policies that address climate change impacts on seafood industries (implied)
Support initiatives to protect and conserve marine species affected by climate change (implied)
</details>
<details>
<summary>
2022-10-17: Let's talk about an update on Trump's Nevada candidate.... (<a href="https://youtube.com/watch?v=JdMGIYYvXYA">watch</a> || <a href="/videos/2022/10/17/Lets_talk_about_an_update_on_Trump_s_Nevada_candidate">transcript &amp; editable summary</a>)

Candidate's flip-flop on Trump's greatness reveals strategic political maneuvering in a tight Nevada governor's race.

</summary>

"By all measures, Donald J. Trump was a great president, and his accomplishments are some of the most impactful in American history."
"I mean, I think that might sway me."
"That seems like something that might cause a lot of voters to think that Lombardo is being less than forthcoming about his actual positions."

### AI summary (High error rate! Edit errors on video page)

Providing an update on the governor's race in Nevada involving candidate Lombardo, who is endorsed by Trump.
Lombardo's response during the gubernatorial debate regarding whether Trump was a great president: he wouldn't use that adjective, calling Trump a sound president instead.
The potential implications of distancing from Trump in a Republican primary but needing his support to win.
Lombardo's campaign later released a statement declaring Trump a great president, contradicting his previous stance.
The tight race between Lombardo and the Democratic Party's interest in exposing inconsistencies in his positions.
Speculation that Lombardo's internal polling may show a need to distance from Trump to appeal to moderate voters.
Concerns about Lombardo's credibility and consistency in his messaging to different voter groups.
The importance of voters being informed about a candidate's true positions and potential political maneuvers.
The impact of Lombardo's conflicting statements on undecided voters and independents.
Democratic Party's likely strategy to capitalize on any missteps or inconsistencies in Lombardo's campaign.

Actions:

for voters,
Analyze candidates' statements and actions for consistency and transparency (implied)
Stay informed about political candidates' positions and potential motivations (implied)
</details>
<details>
<summary>
2022-10-16: Let's talk about two cars in Jacksonville and being a man.... (<a href="https://youtube.com/watch?v=MT6NvXvGKcg">watch</a> || <a href="/videos/2022/10/16/Lets_talk_about_two_cars_in_Jacksonville_and_being_a_man">transcript &amp; editable summary</a>)

Two families' road rage incident in Jacksonville spirals into gunfire, showing the tragic consequences of toxic masculinity and immaturity on family safety.

</summary>

"This isn't masculinity. This is what happens when men act like children."
"It's the kids who pay the price."
"People need to adjust their rhetoric and their concept of what it means to be masculine."

### AI summary (High error rate! Edit errors on video page)

Jacksonville, Florida, road rage incident escalates between two families in cars.
Men engaging in reckless behavior, speeding, brake checking in a dangerous game.
Water bottle thrown, leading to gunfire exchanged between the vehicles.
Daughters in both cars are shot, one seriously injured.
Beau criticizes the toxic masculinity rhetoric tied to firearms.
Emphasizes the tragic consequences of men acting immaturely.
Points out the online debate over who was right in the situation.
Beau condemns both men's actions and stresses the need to avoid violence.
Criticizes the misguided notion of masculinity that leads to such violent outcomes.
Stresses the importance of adjusting attitudes towards masculinity and family protection.
Both men involved have been charged with attempted murder in Florida.

Actions:

for men, fathers, families,
Adjust your rhetoric and concept of masculinity (implied)
Promote mature and non-violent conflict resolution (implied)
Advocate for peaceful solutions in conflicts (implied)
</details>
<details>
<summary>
2022-10-16: Let's talk about the Barnes-Johnson debate in Wisconsin.... (<a href="https://youtube.com/watch?v=W5c-lbZCg_g">watch</a> || <a href="/videos/2022/10/16/Lets_talk_about_the_Barnes-Johnson_debate_in_Wisconsin">transcript &amp; editable summary</a>)

Senator Johnson's failed attempt at aggression over likability in the Wisconsin debate may impact his re-election chances, with potential backlash in the polls.

</summary>

"I don't know why he turned on America."
"It cost more to not finish Trump's wall than to finish it."
"The FBI set me up."

### AI summary (High error rate! Edit errors on video page)

Senator Johnson, a vulnerable Republican up for re-election, failed to come across as friendly during the final debate in Wisconsin.
Johnson's defensive posture didn't resonate well with the audience, especially when he made a snarky comment about his opponent turning on America.
The audience booed Johnson when he mentioned the cost of not finishing Trump's wall.
Barnes, the Democratic candidate, suggested Johnson's involvement in a fake electors scheme, leading to a quip about a "five-second rule for subversion."
Senator Johnson closed the debate by claiming, "the FBI set me up," a sound bite likely to be used in the future.
Johnson's attempt to maintain an attack position may not have played out as he intended, risking potential backlash in the polls.
Beau questions whether Johnson's aggressive approach will impact polling results and suggests that sometimes holding ground is wiser than pushing further.
The debate showcased Johnson's struggle to balance aggression with likability and the potential consequences of pushing too hard.

Actions:

for voters in wisconsin,
Watch out for soundbites like "the FBI set me up" during the election (implied)
</details>
<details>
<summary>
2022-10-16: Let's talk about Social Security shenanigans.... (<a href="https://youtube.com/watch?v=Ob5yJc987mg">watch</a> || <a href="/videos/2022/10/16/Lets_talk_about_Social_Security_shenanigans">transcript &amp; editable summary</a>)

Beau warns of potential cuts to Social Security and Medicare by the Republican Party in 2023, using leverage and political maneuvers to push their agenda.

</summary>

"If you are receiving Social Security, you know, that you paid into for your entire life, you're the fat, by the way."
"It is worth noting the Republican Study Committee has put out a thing."
"But then again, we did all just watch them follow Trump, to, you know, subvert the entire election."
"I find it hard to believe that there is a major political party in the United States that really does think that leverage is, you know, basically steering the entire country into economic ruin."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau addresses Social Security and the potential intentions of the Republican Party in 2023.
In 2023, there's a significant increase of 8.7% in Social Security payments due to inflation.
Medicare Part B premium is expected to decrease for the first time in a decade.
The concern arises from Biden's statement about Republicans putting Social Security on the chopping block.
Rick Scott and Ron Johnson propose making Social Security discretionary rather than mandatory, potentially leading to cuts.
House Republicans plan to leverage the debt limit deadline to push for changes in entitlements and safety net programs.
There's a proposal to raise Medicare eligibility to $270 and Medicaid to $67.
The Republican Party may use leverage to threaten the economy if they don't get their way, akin to steering the country towards economic ruin.
Despite thinking it's outlandish, Beau acknowledges recent actions by the Republican Party make this theory plausible.
Beau expresses concern about the future of Social Security and Medicare if the Republican Party gains more control.

Actions:

for american citizens,
Contact local representatives to express support for protecting Social Security and Medicare (suggested).
Join advocacy groups focused on safeguarding entitlement programs (exemplified).
Organize community events to raise awareness about potential threats to Social Security and Medicare (implied).
</details>
<details>
<summary>
2022-10-15: Let's talk about the end to Trump's week.... (<a href="https://youtube.com/watch?v=ISw-Tn8vyvI">watch</a> || <a href="/videos/2022/10/15/Lets_talk_about_the_end_to_Trump_s_week">transcript &amp; editable summary</a>)

Key developments from Trump's challenging week: Witnesses testifying, Paul Ryan's snub, Trump's response to subpoena, and Supreme Court's denial.

</summary>

"Trump's name wasn't on it. In fact, he went out of his way to say that Trump is, well, just dead weight."
"Have him answer that under oath in front of his base, who he has repeatedly misled to the point of putting them in financial and legal jeopardy."
"They owe you nothing. And there's nothing you can do about it."

### AI summary (High error rate! Edit errors on video page)

Mark Short, a Key Pence aide, compelled to testify before grand jury amidst Trump's claims of executive privilege.
Kash Patel also present during Short's testimony.
Speculation on witnesses' willingness to commit perjury for Trump.
Paul Ryan excludes Trump from list of likely Republican presidential nominees for 2024, calling him "dead weight."
Trump responds to January 6th committee subpoena with a letter echoing baseless election claims.
Beau plans to cover New York Attorney General's case and monitoring of Trump organization's assets.
Trump team's emergency petition to Supreme Court for document classification markings denied.
Supreme Court rulings against Trump post-appointment, indicating his misunderstanding of lifetime appointments.

Actions:

for political observers,
Stay informed on the legal proceedings and developments surrounding Trump's various cases (suggested).
Support transparency and accountability in political processes by advocating for thorough investigations (implied).
</details>
<details>
<summary>
2022-10-15: Let's talk about the NY AG's move and Trump Org II.... (<a href="https://youtube.com/watch?v=PZPdX4blyHA">watch</a> || <a href="/videos/2022/10/15/Lets_talk_about_the_NY_AG_s_move_and_Trump_Org_II">transcript &amp; editable summary</a>)

New York Attorney General seeks oversight of Trump Organization assets to prevent disposal and ensure availability until trial, amid allegations of overvaluation and hefty financial penalties.

</summary>

"New York Attorney General seeks oversight of Trump Organization assets to prevent disposal."
"Concerns over offloading assets to a Delaware company to avoid New York courts' reach."
"Allegations of asset overvaluation by Trump Organization lead to hefty financial penalties."
"Efforts to ensure asset availability until trial in October 2023."
"Monitoring and approval of financial dealings aim to prevent asset offloading."

### AI summary (High error rate! Edit errors on video page)

New York Attorney General James seeks court approval for an independent monitor to oversee Trump Organization assets to prevent disposal.
Concerns arise over a Delaware company, registered as Trump Organization 2, potentially being used to offload assets out of reach of New York courts.
Allegations against Trump Organization include overvaluing assets in its portfolio, leading to financial penalties of around a quarter of a billion dollars.
New York state aims to track and safeguard assets until the trial in October 2023.
A hearing on the appointment of an independent monitor is scheduled for October 31st.
The move is not about freezing Trump's assets, but ensuring they are available if the state of New York takes hold of them.
Monitoring and approval of financial dealings will be in place to prevent Trump from freely engaging in business or offloading assets.
With significant money at stake, attempts to shield assets are expected.
New York's Attorney General's office is proactive in trying to prevent potential asset offloading.
The situation underscores the importance of ensuring accountability in high-stakes financial dealings.

Actions:

for law enforcement agencies,
Attend and support the hearing on October 31st regarding the appointment of an independent monitor (implied).
Stay informed about updates on the case and the trial set for October 2023 (generated).
</details>
<details>
<summary>
2022-10-15: Let's talk about a positive note for the weekend.... (<a href="https://youtube.com/watch?v=q8ut-MA9UjM">watch</a> || <a href="/videos/2022/10/15/Lets_talk_about_a_positive_note_for_the_weekend">transcript &amp; editable summary</a>)

Beau inspires taking action to create systemic change by utilizing individual skills for community betterment.

</summary>

"It doesn't matter what you're good at, whatever you enjoy, is needed to make the world better."
"If you want to make those changes, if you want to achieve that deep systemic change that most people watching this channel want, it starts with you."
"Doing what you can, when you can, where you can, for as long as you can."
"It doesn't matter what you're good at because the goal is to create a better world, a better society."
"Every skill set, every talent, everything. It takes the drive to get there."

### AI summary (High error rate! Edit errors on video page)

Credits the channel for inspiring him to go to law school, mentioning a video about being part of a community network.
Explains that he excels at school, so he pursued law school, passing the bar exam in his late 40s while working full time.
Acknowledges the broken justice system and the need for individuals to take action.
Emphasizes that everyone, regardless of their skills, can contribute to making the world better.
Encourages involvement at the local level to effect systemic change.
Stresses the importance of individuals using their abilities to improve society.
Congratulates Shelby for achieving her goals and hopes he never needs her legal services.

Actions:

for community members,
Join local community initiatives (implied)
Use your skills to contribute to community improvement (implied)
</details>
<details>
<summary>
2022-10-15: Let's talk about Elon Musk, Ukraine, and Starlink.... (<a href="https://youtube.com/watch?v=hsFL__tR4DM">watch</a> || <a href="/videos/2022/10/15/Lets_talk_about_Elon_Musk_Ukraine_and_Starlink">transcript &amp; editable summary</a>)

Elon Musk's Starlink service in Ukraine raises ethical concerns as he seeks Pentagon payment, viewed as a shakedown, while offering vital connectivity for troops.

</summary>

"He wanted to play the game. He wanted to get that free advertising. He probably should have learned the rules to the game first."
"People are viewing it as a shakedown because it very much is. Pay me this money or these horrible things are going to happen to these people."
"There's not a replacement service yet. So cut the check."

### AI summary (High error rate! Edit errors on video page)

Elon Musk has been providing a space-based internet service to Ukraine, which is financially beneficial for him as a great advertisement.
Musk is now asking the Pentagon to cover the costs of the service as he can't afford to pay for it indefinitely.
Some critics view Musk's actions as a shakedown, threatening to cut off communication services for Ukrainian troops if not paid.
Despite emotional reasoning against Musk, the practicality of Starlink as a battlefield tech for Ukrainian troops is paramount.
The short-term solution proposed is for the Pentagon to pay around $30 million a month for the service, as there is no comparable alternative available.
Threatening to withdraw communication services in the middle of a war could have long-term consequences for Musk and his companies within the defense industry.
Musk's actions are seen as profiting from human suffering, and if he wants to play that game, he needs to understand and abide by the rules.
It is expected that the Pentagon or another entity will eventually cover the costs, but the memory of Musk's actions will linger.
The situation is perceived as a shakedown where people are asked to pay to prevent dire consequences, but the lack of replacement services necessitates paying for the service.
Musk may have underestimated the repercussions of his actions within the defense industry.

Actions:

for defense industry stakeholders,
Cut the $30 million monthly Pentagon payment for Starlink service (implied)
Develop alternative communication systems for military use (implied)
</details>
<details>
<summary>
2022-10-14: Let's talk about Trump's Jan 6 subpoena.... (<a href="https://youtube.com/watch?v=U6xUOBheiJU">watch</a> || <a href="/videos/2022/10/14/Lets_talk_about_Trump_s_Jan_6_subpoena">transcript &amp; editable summary</a>)

The committee's decision to subpoena Trump prompts speculation on his willingness to testify, with doubts raised about his honesty under oath.

</summary>

"Call that man's bluff."
"Imagine what DOJ has and try to cut a deal."
"He will go off. And once he starts, he just deviates."
"Call that man's bluff."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The committee decided to subpoena former President Donald J. Trump, sparking speculation and commentary on the situation.
Beau suggests that bringing Trump in for questioning could be the moment to reveal evidence and potentially strike a deal.
He expresses doubt about Trump's intelligence to grasp subtle hints and strategic moves.
Recent news indicates Trump's desire to testify live, prompting Beau to challenge this bluff.
Beau recommends having the FBI present during Trump's testimony to prevent interference and address potential perjury.
He predicts that Trump may deviate from the truth when speaking in a politically charged situation.
Beau views Trump's desire to testify as a risky move, especially considering his tendency to stray from factual accuracy.
He anticipates that Trump's lawyers might dissuade him from testifying to avoid legal repercussions.
Beau humorously suggests a hypothetical scenario where Trump starts a YouTube channel titled "Trump of the Fifth Amendment" if he follows legal advice.
He expresses skepticism about Trump's honesty during testimony, particularly if he views it through a political lens rather than a legal one.

Actions:

for legal analysts,
Challenge bluff by calling for live testimony with cameras and oath (suggested).
Ensure FBI presence during testimony to prevent interference and address perjury (implied).
Stay informed and engaged with legal developments surrounding Trump's testimony (exemplified).
</details>
<details>
<summary>
2022-10-14: Let's talk about Russia's offer in Kherson.... (<a href="https://youtube.com/watch?v=qeAfkurLBDg">watch</a> || <a href="/videos/2022/10/14/Lets_talk_about_Russia_s_offer_in_Kherson">transcript &amp; editable summary</a>)

Beau reveals Russian offer in Hursan signals potential loss and resource strain in Ukraine conflict, urging attention to unfolding events.

</summary>

"This absolutely does not mean that they think they're going to lose Hursan. This absolutely means the Russian government thinks they're going to lose Hursan."
"The types of missiles being used kind of indicate that Russia is running really low on stuff it absolutely need to prosecute this war successfully."

### AI summary (High error rate! Edit errors on video page)

Russian government offers free accommodation in Russia to people in Hursan, indicating a potential belief that they might lose control of the region.
The offer is likely to relocate those who have assisted the Russian government, not a sign of retreat according to Russian officials.
Speculation that the offer could be used as cover to remove partisans loyal to Ukraine.
Russia's use of missiles in Ukraine raises questions about their dwindling resources needed for a successful war prosecution.
Expect increased attention on the Hursan region and potential forthcoming developments.

Actions:

for world observers,
Keep a close eye on developments in the Hursan region and Ukraine (implied)
Stay informed about the situation and share updates with others (implied)
</details>
<details>
<summary>
2022-10-13: Let's talk about the problem with coal and advice.... (<a href="https://youtube.com/watch?v=e5jYkO1EHPc">watch</a> || <a href="/videos/2022/10/13/Lets_talk_about_the_problem_with_coal_and_advice">transcript &amp; editable summary</a>)

Beau addresses the problem with coal, advocates for understanding the causes supported, and clarifies misconceptions about coal mining and energy sources.

</summary>

"This is followed by five paragraphs of facts and figures about how dirty coal is as an energy source."
"I'm pretty sure that most schools of thought, when it comes to the left, support the workers."
"If the strike fails, the management, the boss, hires new employees at a lower wage with less benefits and worse working conditions."
"Understand the stuff that is mined at this mine is what makes the type of energy you want."
"Maybe tune in."

### AI summary (High error rate! Edit errors on video page)

Addressing the problem with coal and answering questions raised.
Expressing the importance of understanding the causes advocated for.
Providing information on thermal coal being the dirtiest energy source in the U.S.
Explaining the implications of advocating for coal and supporting workers.
Clarifying the effects of a strike failure on coal mines.
Differentiating between thermal and metallurgical coal and their uses.
Encouraging support for striking workers through a live stream.

Actions:

for leftist activists,
Tune in to the upcoming live stream to support striking workers (implied).
</details>
<details>
<summary>
2022-10-13: Let's talk about Trump's employee talking.... (<a href="https://youtube.com/watch?v=HDdoUdsMSA0">watch</a> || <a href="/videos/2022/10/13/Lets_talk_about_Trump_s_employee_talking">transcript &amp; editable summary</a>)

New developments in the Trump document case reveal potential legal jeopardy for Trump as he downplays the issue by framing it as a document storage problem, while a cooperating witness sheds light on incriminating actions.

</summary>

"There is no crime having to do with the storage of documents at Mar-a-Lago."
"That's kind of a big no-no if you're going to move stuff and not return it."
"The whole situation is raising the legal jeopardy for Trump."
"If there is a staffer who was trusted enough by Trump to have been asked to move the documents after the subpoena was served, that staffer probably has a whole lot of information."
"The statements that are being made, they're not going to help him legally."

### AI summary (High error rate! Edit errors on video page)

New developments in the Trump document case are discussed, with confirmation of previously believed information.
A staffer reportedly spoke to the FBI as a cooperating witness, revealing that Trump instructed moving boxes after receiving a subpoena.
The potential obstruction charge hinted by the feds may relate to not returning items as per the subpoena.
Trump aims to frame the issue as a document storage problem rather than a national defense information concern.
Legal jeopardy for Trump increases as a lawyer's involvement complicates the situation.
Trump appears more focused on political aspects rather than legal implications.
The staffer's cooperation with the FBI could lead to significant revelations and developments.
Trump's attempt to downplay the issue as document storage may not be beneficial legally.
The statements made by Trump are unlikely to assist him in a legal context.
The situation surrounding Trump's document case continues to evolve, indicating further troubles ahead.

Actions:

for political analysts, investigators,
Contact legal experts for analysis on the potential legal implications faced by Trump (implied)
Stay informed about the ongoing developments in the Trump document case (implied)
</details>
<details>
<summary>
2022-10-13: Let's talk about DART test results.... (<a href="https://youtube.com/watch?v=6m2qfGX7vgA">watch</a> || <a href="/videos/2022/10/13/Lets_talk_about_DART_test_results">transcript &amp; editable summary</a>)

NASA successfully alters asteroid orbit, potentially justifying its budget forever, leaving Hollywood producers scrambling for new plot lines.

</summary>

"This is literally world saving."
"They're going to have to come up with something new."
"You no longer have to worry about that."

### AI summary (High error rate! Edit errors on video page)

NASA's Double Asteroid Redirection Test (DART) successfully altered the orbit of an asteroid by crashing something into it.
The test aimed to shorten the asteroid's orbit by 10 minutes, with a previous orbit of 11 hours and 55 minutes.
After the test, the asteroid's new orbit is 11 hours and 23 minutes, shortening it by 32 minutes.
This proof of concept shows NASA's capability to defend Earth from asteroids.
Success in using this technology could justify NASA's budget indefinitely.
Hollywood producers may need new plot lines as asteroid defense was a common theme.
Beau suggests using climate change as a disaster flick theme to raise awareness.
Asteroid collisions have occurred in the past with no defense until now.
This NASA project eliminates worries about asteroid impacts on Earth.
The successful test showcases the potential world-saving impact of NASA's work.

Actions:

for space enthusiasts, environmentalists,
Support NASA's space exploration and defense projects (implied)
Raise awareness about the importance of asteroid defense and space research (implied)
</details>
<details>
<summary>
2022-10-12: Let's talk about hybrid Abrams.... (<a href="https://youtube.com/watch?v=FvkhCPPBORU">watch</a> || <a href="/videos/2022/10/12/Lets_talk_about_hybrid_Abrams">transcript &amp; editable summary</a>)

Beau introduces the revolutionary potential of the U.S. military's shift towards hybrid and electric vehicles, impacting fuel consumption, emissions, and national security.

</summary>

"It's measured in gallons per mile. It's massive."
"If we don't need oil, we're not going to be in the Middle East."
"This is something that can literally stop or slow wars before they start."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of fuel consumption and the U.S. military, discussing a recent concept seen that may be revolutionary.
Talks about the new electric light reconnaissance vehicle for the U.S. military and its potential impact on consumer vehicle choices.
Mentions the sneak peek of a new hybrid concept for the Abrams tank, a major piece of equipment for the military.
Describes the staggering fuel consumption of the Abrams tank in terms of starting fuel requirement and the shift from miles per gallon to gallons per mile.
Emphasizes the significance of the U.S. military's move towards hybrid and electric vehicles in reducing emissions and fuel consumption.
Points out how this shift can lead to a decrease in the need to maintain access to oil markets through military force.
Addresses the potential positive impacts on national security by reducing the necessity for wars in regions with oil resources.
Acknowledges the role of the U.S. military in maintaining access to oil markets and suggests that a transition to electric vehicles could change this dynamic.
Expresses optimism about the environmental, national security, and human benefits of transitioning the military's vehicles to hybrid and electric models.

Actions:

for climate advocates, military personnel,
Support initiatives that advocate for the transition of military vehicles to hybrid and electric models (suggested).
Encourage companies to invest in developing more affordable electric vehicles (implied).
</details>
<details>
<summary>
2022-10-12: Let's talk about Trump's claims about other Presidents and questions.... (<a href="https://youtube.com/watch?v=qUxn-5EVf_Q">watch</a> || <a href="/videos/2022/10/12/Lets_talk_about_Trump_s_claims_about_other_Presidents_and_questions">transcript &amp; editable summary</a>)

Beau analyzes Trump's attempt to downplay document storage issues, urging a focus on documents containing National Defense Information and critical questions while cautioning against letting Trump control the narrative.

</summary>

"Don't let him control the narrative."
"What matters are the documents that had National Defense Information."
"Stop worrying about the Presidential Records Act."

### AI summary (High error rate! Edit errors on video page)

Analyzing former President Donald J. Trump's statements regarding document storage habits of other former presidents.
Trump alleged that Obama, Clinton, and Bush one and two took documents.
National Archives has debunked all of Trump's claims.
Trump had custody of documents, unlike other former presidents whose documents were with the National Archives.
Trump is attempting to downplay the issue by framing it as a document storage problem.
Democrats and liberal commentators are urged to focus on documents with National Defense Information, not trivial matters like love letters from dictators.
Questions need to be asked about the National Defense Information documents' handling and intent.
Trump's focus on minor issues is diverting attention from potential serious crimes.
DOJ's role in investigating the retention of National Defense Information and possible transmission to a third party.
It is vital not to let Trump control the narrative and shift focus from the critical questions at hand.

Actions:

for journalists, activists, citizens,
Question the handling and intent of documents with National Defense Information (implied)
Focus on critical questions about the documents (implied)
Refrain from letting Trump control the narrative (implied)
</details>
<details>
<summary>
2022-10-12: Let's talk about Russia's offer to Republicans.... (<a href="https://youtube.com/watch?v=PqX1HD5xIpU">watch</a> || <a href="/videos/2022/10/12/Lets_talk_about_Russia_s_offer_to_Republicans">transcript &amp; editable summary</a>)

Russia offers American states to join, revealing how successful influence operations sow division and weaken American patriotism.

</summary>

"They got you to sell out your country without you ever even knowing it."
"You ignored them and de facto allied with them."
"You're the person I'm talking to."
"Patriots right?"
"Everything that you were afraid of, all of those crazy conspiracy theories that y'all hatched, in some ways it kind of occurred."

### AI summary (High error rate! Edit errors on video page)

Russia offered American states the chance to break away and join Russia, sparking various reactions.
Mentioned Deputy Tolmachev in the Russian parliament expressing openness to American states joining Russia.
Raised the scenario of Alaska being annexed by Russia and questioned the US response.
Criticized those who support Russian intervention in Ukraine but oppose potential Russian interference in the US.
Pointed out Russia's motive behind the offer as gloating and making fun of the Republican base.
Discussed how successful Russian influence operations have led American patriots to take positions detrimental to the US.
Noted that individuals influenced by Russian propaganda may unknowingly act against American interests.
Urged people to be cautious of pundits echoing Russian talking points and potentially being Russian allies.
Warned about falling into information silos and unknowingly betraying fellow Americans.
Suggested reflecting on how one's beliefs and actions may have been influenced by external sources.

Actions:

for american citizens,
Question sources of information and be wary of pundits echoing Russian talking points (implied)
Engage in critical thinking and analysis of political commentary to identify potential foreign influence (implied)
</details>
<details>
<summary>
2022-10-11: Let's talk about not talking about Hunter Biden.... (<a href="https://youtube.com/watch?v=ZITQlkj9Gs0">watch</a> || <a href="/videos/2022/10/11/Lets_talk_about_not_talking_about_Hunter_Biden">transcript &amp; editable summary</a>)

Beau Jahn explains why he avoids discussing specific events like Hunter Biden's, focusing on real news and avoiding gossip.

</summary>

"I don't run a gossip column."
"If there was a situation that Hunter Biden found himself in that did relate to an official capacity or the official capacity of Biden, even if it was just the appearance of it, I would talk about it."
"There is enough real news to kind of parse through that searching for scandals about people that are related to public figures, that's just, I don't have time for that."
"They give you something that is emotionally exciting."
"Unless Hunter Biden's activity somehow impacted his father's activities, I literally don't care at all."

### AI summary (High error rate! Edit errors on video page)

Explains why he doesn't talk about specific events like Hunter Biden and his recent troubles in Ukraine, as he doesn't run a gossip column.
Mentions that as a private citizen, Hunter Biden's personal matters are not of interest unless they directly relate to an official capacity.
Points out that he applies the same standard to all public figures, including Trump's kids and the former First Lady.
Emphasizes the importance of focusing on real news instead of searching for scandals involving public figures.
Suggests that media outlets can manipulate people by villainizing certain groups based on connections to others, leading individuals to compromise their beliefs.

Actions:

for media consumers,
Analyze news critically (implied)
Focus on real news and avoid getting drawn into sensationalized scandals (implied)
</details>
<details>
<summary>
2022-10-11: Let's talk about bridges: military or civilian.... (<a href="https://youtube.com/watch?v=f6aTS-aawjk">watch</a> || <a href="/videos/2022/10/11/Lets_talk_about_bridges_military_or_civilian">transcript &amp; editable summary</a>)

Beau breaks down the misconception around bridges, exposing their military significance and debunking the idea of bridges for military use being classified as civilian infrastructure.

</summary>

"Anybody who is trying to tell you that a bridge used to move troops and supplies is civilian infrastructure either knows nothing about war or is deliberately misleading you."
"Taking out bridges has always been a military thing."
"Anybody who is trying to sell you on the idea that a bridge used to move troops and supplies is civilian infrastructure is trying to sell you a bridge."

### AI summary (High error rate! Edit errors on video page)

Addresses the misinformation surrounding bridges and their military importance.
Questions the narrative that bridges used for military purposes are civilian infrastructure.
Uses the example of Ukrainian bridges being hit by Ukrainians to illustrate the military nature of targeting bridges.
Stresses that taking out bridges has always been a military strategy to impede routes and slow advances.
Mentions the importance of bridges in war strategies and the limited ways to move from point A to point B.
Emphasizes that anyone claiming a bridge used for military purposes is civilian infrastructure is trying to deceive.
Concludes with a thought about the significance of bridges in warfare.

Actions:

for military history enthusiasts,
<!-- Skip this section if there aren't physical actions described or suggested. -->
</details>
<details>
<summary>
2022-10-11: Let's talk about Biden, Hannity, and a voicemail.... (<a href="https://youtube.com/watch?v=vLxB8NF4gs4">watch</a> || <a href="/videos/2022/10/11/Lets_talk_about_Biden_Hannity_and_a_voicemail">transcript &amp; editable summary</a>)

Beau reacts to Sean Hannity's airing of President Biden's emotional voicemail, predicting backfire on the GOP and praising the display of true family values.

</summary>

"This is the single most humanizing thing that has happened in a long time in American politics."
"I do not think that this is going to damage Biden in any way."
"Those, what you heard in that voicemail, those are family values."
"They care about a TV family that never existed, a family without problems."
"No matter what you need, I love you."

### AI summary (High error rate! Edit errors on video page)

Reacts to Sean Hannity playing President Biden's voicemail to Hunter Biden from 2018.
Expresses surprise at the timing of discussing Hannity before knowing about the voicemail situation.
Describes making similar emotional calls to loved ones in the past.
Comments on the humanizing effect of Biden's message and how it contrasts with the Republican Party.
Predicts that the voicemail revelation will backfire on the Republican Party.
Believes the voicemail showcases true family values, contrasting with the image-focused ideals of the GOP.
Concludes by expressing his thoughts and wishing everyone a good day.

Actions:

for politically engaged individuals,
Reach out to loved ones in need of help or support (exemplified)
Advocate for genuine family values in political discourse (exemplified)
</details>
<details>
<summary>
2022-10-10: Let's talk about Pirates, Emperors, Russians, and Ukrainians.... (<a href="https://youtube.com/watch?v=KoqYlKhe4S8">watch</a> || <a href="/videos/2022/10/10/Lets_talk_about_Pirates_Emperors_Russians_and_Ukrainians">transcript &amp; editable summary</a>)

Beau introduces the concept of pirates and emperors, discussing the thin line between legality and morality in conflicts, urging a deeper understanding to combat propaganda.

</summary>

"Legality and morality are not the same."
"Pirates and emperors."
"Once you grasp that and really get it, it's much harder to fall for propaganda."
"The real difference is that one side is defending and the other side is invading."
"That's what matters."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of pirates and emperors to illustrate a core concept.
Tells the story of Alexander capturing a pirate to explain the distinction between a pirate and an emperor.
Emphasizes that the core concept is doing the same thing through the same means.
States that legality and morality are not the same, even though pirates and privateers share the same action.
Questions the terminology and media response regarding an event on a bridge, pointing out the influence of state ties.
Suggests that the delivery system does not change the morality of an action.
Encourages understanding that nations are not exempt from normal morality and that this understanding helps combat propaganda.
Compares Russia calling Ukraine a pirate to both sides using similar means for different purposes - defense and invasion.
Stresses that the real difference lies in the moral aspects rather than the legal or illusionary aspects of conflicts.
Concludes by leaving the audience with food for thought and well-wishes for the day.

Actions:

for critical thinkers,
Understand the distinction between legality and morality when analyzing conflicts (suggested)
Combat propaganda by seeking a deeper understanding of the moral aspects of conflicts (suggested)
</details>
<details>
<summary>
2022-10-10: Let's talk about DOD changing the cars you see.... (<a href="https://youtube.com/watch?v=03ctWe2hzlU">watch</a> || <a href="/videos/2022/10/10/Lets_talk_about_DOD_changing_the_cars_you_see">transcript &amp; editable summary</a>)

Beau predicts that the Department of Defense's move towards electric vehicles will influence a demographic known for tough guy trucks to shift towards electric, potentially leading to broader acceptance of electric vehicles.

</summary>

"The people who purchase these types of vehicles for these reasons, they'll want one. I know that sounds silly, but they will."
"Because if they don't, well, they'll be seen as a fud."

### AI summary (High error rate! Edit errors on video page)

Introduces a new vehicle that he is excited about because it will change the types of vehicles seen on the road.
Talks about a specific demographic that purchases vehicles to project a tough, macho image, like civilianized Humvees and giant diesel trucks.
Mentions that the Department of Defense (DOD) is introducing a new electric light reconnaissance vehicle (e-LRV) which will impact the types of vehicles people buy.
Notes that DOD's move towards electric vehicles is influenced by viewing climate change as a national security threat and the need to reduce reliance on fossil fuels.
Predicts that once this demographic sees the military using electric vehicles, they will want them too to maintain their image.
Believes that once this specific demographic embraces electric vehicles, it will lead to broader acceptance and adoption of electric vehicles.
Recognizes the positive impact of the military transitioning to electric vehicles on the environment and domestically.
Points out that the platform used for the new military vehicle will be available in the civilian market, potentially paving the way for off-road electric vehicles for civilians.
Concludes by expressing his thoughts on the potential positive impacts of the military's shift to electric vehicles.

Actions:

for vehicle enthusiasts, climate-conscious consumers.,
Look into purchasing electric vehicles to support the transition towards more sustainable transportation options (implied).
Stay informed about advancements in electric vehicle technology and their availability in the civilian market (implied).
</details>
<details>
<summary>
2022-10-10: Let's talk about Alabama, Trump's rally, and Tuberville.... (<a href="https://youtube.com/watch?v=ykNn5Eq8EN0">watch</a> || <a href="/videos/2022/10/10/Lets_talk_about_Alabama_Trump_s_rally_and_Tuberville">transcript &amp; editable summary</a>)

Senator Tuberville's controversial statements on crime and welfare programs reveal a disconnect with Alabama's economic realities, urging for attention towards fair wages and community support.

</summary>

"They're pro-crime. They want crime. They want reparations, because they think the people who do the crime are owed that."
"The United States could not afford food stamps, food security programs, and that people needed to get back to work."
"Maybe that has something to do with it. Just saying."
"If your minimum wage is closer to your poverty wage than a living wage, yeah, you're probably going to have a lot of people using your poverty programs."
"We have to start getting our ducks in a row on this channel so we can help with getting Christmas gifts for the kids of striking minors in Alabama."

### AI summary (High error rate! Edit errors on video page)

Senator Tuberville made controversial statements linking the Democratic Party to being pro-crime and wanting reparations for criminals.
Tuberville also claimed that the U.S. cannot afford food stamps and food security programs, urging people to get back to work.
Unemployment rates in the U.S. and Alabama are low, yet there are a significant number of people (769,800) benefiting from SNAP.
The gap between employment numbers and SNAP recipients indicates that minimum wage may be a contributing factor.
Alabama's minimum wage at $7.25 per hour falls significantly below the living wage threshold, leading many to rely on poverty programs.
Beau suggests Senator Tuberville should focus more on understanding the real issues in Alabama rather than making controversial statements elsewhere.
Beau expresses frustration towards the senator's comments and advocates for supporting striking minors in Alabama by providing Christmas gifts.

Actions:

for policy advocates, community activists,
Support striking minors in Alabama by providing Christmas gifts for their kids (suggested)
</details>
<details>
<summary>
2022-10-09: Let's talk about Haiti and the world's EMT.... (<a href="https://youtube.com/watch?v=3cwzzf20W-E">watch</a> || <a href="/videos/2022/10/09/Lets_talk_about_Haiti_and_the_world_s_EMT">transcript &amp; editable summary</a>)

Beau advocates for the US to prioritize building an EMT force for stabilizing failed states over deploying troops to countries like Haiti, stressing the importance of investing in nations rather than relying solely on military intervention.

</summary>

"The US military is not a peacekeeping force. It's a force that wins wars."
"We don't have the world's EMT force built."
"The military is there to take down governments, not build them."
"This is a capability the United States needs that could prevent war."
"Before we start doing it, we have to actually build that force."

### AI summary (High error rate! Edit errors on video page)

Haiti is facing a troubling situation and has requested foreign troops to help restore order.
Beau advocates for the United States to act as the world's EMT (Emergency Medical Technician) rather than the world's policeman.
He criticizes the use of the military for peacekeeping roles, stating that the military is meant to win wars, not maintain peace.
Beau points out that the lack of a follow-on force like an EMT force leads to extended military presence in countries like Iraq.
He mentions Thomas Barnett as a proponent of a separate force for non-warfighting missions and suggests watching his TED Talk.
The request for foreign troops in Haiti has been acknowledged by the State Department, who are considering it.
Beau believes that the US should prioritize developing a separate EMT force to handle failed states rather than deploying troops.
He stresses the importance of investing in countries like Haiti rather than solely relying on military intervention.
Beau argues that building the capability to stabilize failed states could do more for world peace than military actions.
He suggests that with the decline of Russia, the US needs to be prepared for potential power vacuums in other countries.

Actions:

for policymakers, activists, citizens,
Prioritize building a separate EMT force for handling failed states (implied)
Advocate for investing in troubled nations like Haiti rather than relying solely on military intervention (implied)
</details>
<details>
<summary>
2022-10-09: Let's talk about Biden's pardons and student debt.... (<a href="https://youtube.com/watch?v=7Eu9_nFXbn4">watch</a> || <a href="/videos/2022/10/09/Lets_talk_about_Biden_s_pardons_and_student_debt">transcript &amp; editable summary</a>)

Beau explains Biden's federal level cannabis pardons, hints at more actions to come, and sheds light on student debt forgiveness.

</summary>

"He did in his executive orders kind of encourage them to do so, but he can't make them pardon."
"That eternal question. Why aren't people currently in custody for this? Because the shift already started."
"There's a little bit of a difference there in how a prospective employer might look at it."
"I think there's a lot more that's going to be coming, but we have to wait and see."
"Just understand you're going to see this material again."

### AI summary (High error rate! Edit errors on video page)

Explains Biden's blanket pardon for simple possession of cannabis at the federal level.
Clarifies that Biden's authority is limited to federal level pardons, not state level.
Notes that thousands of people are being pardoned, with nobody being let out because the shift in prosecution has already started.
Mentions the potential next steps after pardoning, including descheduling cannabis.
Suggests that Biden's administration may be working out the details before more significant actions.
Points out that prior to the big announcement on student debt forgiveness, billions of dollars were already forgiven.
Addresses the legal paperwork trail that may still affect individuals despite receiving a pardon.
Emphasizes that while pardons are not a clean slate, they represent a step forward in the process.
Expresses the belief that there is more to come regarding Biden's actions.
Concludes by hinting at further developments to watch out for.

Actions:

for reform advocates,
Monitor and advocate for further actions from the Biden administration (implied)
Stay informed about developments in pardons and student debt forgiveness (implied)
</details>
<details>
<summary>
2022-10-09: Let's talk about Biden's new drone policy.... (<a href="https://youtube.com/watch?v=WRkGmtKRE1c">watch</a> || <a href="/videos/2022/10/09/Lets_talk_about_Biden_s_new_drone_policy">transcript &amp; editable summary</a>)

Biden administration formalizes drone policy to limit civilian casualties, but skepticism remains on effective implementation and impact on covert operations.

</summary>

"The White House determines individuals, groups, and countries that drones can be used in or against."
"There has been a concerted effort to clean this up."
"While the policy looks good, we need to see the implementation before we start sharing."

### AI summary (High error rate! Edit errors on video page)

Biden administration's new policy on drones formalizes existing practices.
Policy specifically addresses the use of drones outside areas of active hostilities.
White House now makes the call on drone strikes, aiming to limit civilian casualties.
Policy extends to in-person covert operations like raids.
Public disclosure suggests White House intent to limit drone and covert operation use.
Previous lax rules on drone strikes were not effective.
Skepticism remains on whether the new policy will be effectively implemented.
There is a history of attempts to clean up drone strike practices.
The implementation of the new policy needs to be closely monitored.
The policy may make it easier to conduct operations with low risk to U.S. personnel.

Actions:

for policy analysts,
Monitor the implementation of the new drone policy closely (implied).
Stay informed on any updates or changes in U.S. drone strike practices (implied).
</details>
<details>
<summary>
2022-10-08: Let's talk about the next 3rd ranked military.... (<a href="https://youtube.com/watch?v=ZZ1vKbaA9wc">watch</a> || <a href="/videos/2022/10/08/Lets_talk_about_the_next_3rd_ranked_military">transcript &amp; editable summary</a>)

Russia's fall from the top three military powers opens up space for Japan's rise, driven by increased defense spending and a focus on modern capabilities.

</summary>

"The face of war has changed."
"Training, technology, financial resources, and the will, the desire to force project and be a player."
"Japan becomes a lead nation with the backing of the United States rallying other Pacific nations."
"They're going to be a force to be reckoned with."
"They'll want to be able to stand on their own."

### AI summary (High error rate! Edit errors on video page)

Russia lost its top three military power status due to poor performance in Ukraine and degrading capabilities.
The United States holds the number one spot in military power, with China at number two.
The debate is around who will take the third spot in military power rankings.
Traditional methods of predicting military effectiveness, like troop and tank numbers, are outdated.
Modern military power comes from training, technology, financial resources, and the will to project force.
Japan, historically reliant on the US for defense, has recently increased its defense spending and capabilities.
Japan's focus on air and blue water capabilities positions them to be a significant player in the Pacific.
Geography plays a significant role in a country's ability to project military power.
Japan's demonstration of actual military capability will change how they are viewed internationally.
Japan aims to be a partner with the US, leading Pacific nations in the future.

Actions:

for military analysts, policymakers,
Monitor Japan's defense spending and military capabilities (implied)
Stay informed about geopolitical developments in the Pacific region (implied)
</details>
<details>
<summary>
2022-10-08: Let's talk about a question about Finland and Russia.... (<a href="https://youtube.com/watch?v=cWyEvOrCi_8">watch</a> || <a href="/videos/2022/10/08/Lets_talk_about_a_question_about_Finland_and_Russia">transcript &amp; editable summary</a>)

Beau explains the differing perspectives on Russia's military readiness following the Ukraine conflict, with varying opinions on the timeline and quality of troops near Finland's border.

</summary>

"Russia is — there are already people debating now what the third most powerful military is, because Russia's not it anymore."
"Your father is probably coming from the perspective of what they will do, which is send a bunch of untrained, ill-equipped conscripts up there."
"I think three years is optimistic."
"The amount of damage the Russian military has sustained is immense."
"But at the same time, if there's one thing we've learned about the Russian military lately, they don't really care about quality."

### AI summary (High error rate! Edit errors on video page)

Talks about the military readiness of Russia and Finland following events in Ukraine.
Explains the differing perspectives between a government official and the speaker's father regarding Russia's troop deployment near Finland's border.
Mentions the degradation of Russia's military capability due to the Ukraine conflict.
Emphasizes the time it will take for Russia to recover its military strength post-conflict.
Contrasts the training and equipment quality between Finland and Russia's military forces.
Considers the implications for Finland if they join NATO in relation to Russian troop movements.
Notes that despite the degraded state of the Russian military, they may prioritize rapid deployment over quality.
Suggests that the recovery of the Russian military from the conflict will be a lengthy process.
Points out the lack of emphasis on quality in the Russian military's approach.
Concludes by acknowledging the different perspectives presented and leaves the decision to the audience.

Actions:

for military analysts, policymakers,
Monitor international relations updates regarding Russia and Finland (implied).
</details>
<details>
<summary>
2022-10-08: Let's talk about a new parenting approach.... (<a href="https://youtube.com/watch?v=VvM3jUf00Q4">watch</a> || <a href="/videos/2022/10/08/Lets_talk_about_a_new_parenting_approach">transcript &amp; editable summary</a>)

Introducing an unorthodox method of raising children that prioritizes nurturing skills over accountability, potentially impacting political views.

</summary>

"You hold a toddler more accountable for their actions than you do the leader of your political party."
"If you're wondering why your children may have a different view when it comes to politics now..."
"Parents need to have that ability if they want to succeed."
"It's the only way they will ever be able to be the Republican nominee for president."

### AI summary (High error rate! Edit errors on video page)

Introduces a new method of raising children focused on developing skills for the future, popular in conservative circles.
Parents allow kids to get away with deceptive actions to nurture skills like deception and manipulation for later success.
Examples include children hiding a cookie jar, creating fake permission slips, and mocking others without consequences.
Parents prioritize fostering these skills over holding children accountable for their actions.
Suggests that parents holding their children to a higher standard than political leaders may influence children's political views.

Actions:

for conservative parents,
Reassess parenting strategies to balance skill development and accountability (suggested)
Encourage open political discourse with children to understand differing views (suggested)
</details>
<details>
<summary>
2022-10-08: Let's talk about a bridge not too far.... (<a href="https://youtube.com/watch?v=hC213TG_BHU">watch</a> || <a href="/videos/2022/10/08/Lets_talk_about_a_bridge_not_too_far">transcript &amp; editable summary</a>)

Beau talks about the Kerch Bridge incident, its potential causes, strategic implications, and morale impact on Ukrainian and Russian forces.

</summary>

"This is a significant supply route for Russian forces in Crimea."
"No matter the cause, this is going to be a huge boost in morale to the Ukrainian side."
"If it was a Ukrainian operation, they have reason to be confident in their abilities right now."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of the Kerch Bridge connecting Russia to Crimea, a significant supply route for Russian forces.
Describes the Kerch Bridge as a pride and joy project of Putin, potentially the largest bridge Russia has ever built.
Mentions the Kerch Strait incident involving Russian and Ukrainian forces on November 25th, 2018.
Reports an incident on the bridge involving rapidly expanding gas leading to an explosion, damaging the bridge.
Speculates on possible causes of the incident, including Ukrainian operation, lack of Russian safety standards, a gender reveal mishap, or Russian defense tactics.
Points out the strategic importance of the bridge for Russian supply efforts in Crimea.
Raises the idea of a Ukrainian operation as a sign of confidence and compares it to a scene from "A Bronx Tale."
Suggests that regardless of the cause, the incident will boost morale for the Ukrainian side and disrupt Russian supplies.

Actions:

for policy analysts, strategists,
Analyze the strategic implications of the Kerch Bridge incident (implied).
Monitor the developments regarding the bridge's repair and impact on supply routes (implied).
Support efforts to boost morale for affected communities (implied).
</details>
<details>
<summary>
2022-10-07: Let's talk about the possibility of Putin using a tactical.... (<a href="https://youtube.com/watch?v=o5DBJAbmDTM">watch</a> || <a href="/videos/2022/10/07/Lets_talk_about_the_possibility_of_Putin_using_a_tactical">transcript &amp; editable summary</a>)

Beau details the potential US response to Putin deploying a tactical nuke in Ukraine and why it might not be a likely scenario, considering NATO's capabilities and international dynamics.

</summary>

"This isn't something that anybody wants."
"The warfighting capability of NATO is much greater than that of Russia."
"The likelihood of doing something that would provoke a NATO response at this point seems slim."
"It's just still something that people are unaccustomed to, because we haven't heard it in such a long time."
"I wouldn't worry too much about this, but the number of questions kind of obligated me to respond."

### AI summary (High error rate! Edit errors on video page)

Talks about the possibility of Putin deploying a tactical nuke inside Ukraine and the potential US response.
Diplomatic routes could be the first step, with international community turning hostile towards Russia.
Concerns about providing Ukraine with advanced weapon systems due to fears of Ukraine using them to strike inside Russia's borders.
If a tactical nuke was used, the rules and limitations on supplying Ukraine wouldn't matter anymore.
Petraeus mentioned a potential response involving destroying the Black Sea Fleet and hitting targets inside Ukraine.
Believes that if Putin deployed a tactical nuke, targets outside Russia's borders, including contractors and military assets, could become fair game.
NATO and US don't have to respond with nukes; their warfighting capability is greater than Russia's.
US control of the skies gives them significant advantage in any response.
A NATO endeavor to degrade Russia's capabilities could receive international support.
Covert options could involve targeting Putin individually if he ordered such an action.

Actions:

for global citizens, policymakers,
Contact policymakers to advocate for diplomatic solutions and peace talks (suggested)
Support international efforts to de-escalate tensions and prevent nuclear threats (implied)
</details>
<details>
<summary>
2022-10-07: Let's talk about spears and relief logistics.... (<a href="https://youtube.com/watch?v=pU2KJij5lmM">watch</a> || <a href="/videos/2022/10/07/Lets_talk_about_spears_and_relief_logistics">transcript &amp; editable summary</a>)

Beau compares his hands-on role after hurricane Michael to his supply-focused support after Ian, emphasizing the importance of logistics and local networks in informal relief work to ease the burden on first responders and strengthen community resilience.

</summary>

"The goal of doing this type of relief is to ease the burden of the actual lights and sirens first responders."
"You provide the tools for self-rescue, and the locals, the tip of the spear, will do it."
"You get in where you're most used, where you can be of most use. That's the role you need to fill."

### AI summary (High error rate! Edit errors on video page)

Talks about informal logistics of relief efforts after hurricanes Michael and Ian
Compares his role in both situations - more hands-on after Michael due to local contacts, more supply-focused after Ian
Uses a spear analogy to explain different roles in relief work: tip (active combat), shaft (deploy but not daily combat), and staff (logistics, support)
Emphasizes the importance of the staff/logistics role in relief work, even if it's less visible
Explains how he distributed supplies to local contacts after Ian, who then directed them to areas of need
Goal of informal relief is to ease burden on official first responders
Viewers of the channel indirectly supported relief efforts by enabling Beau's work
Mutual assistance networks can form from these experiences, strengthening community resilience
Key steps: know what's needed, have a distribution point, and a few local contacts to start the process
Initial response is most important, then local volunteers join in

Actions:

for relief volunteers, community organizers,
Bring supplies to local contacts who can direct them to areas of need (exemplified)
Set up a makeshift supply depot in impacted neighborhoods (exemplified)
Distribute tools like chainsaws and cleanup kits to enable self-rescue (exemplified)
</details>
<details>
<summary>
2022-10-07: Let's talk about Putin's mobilization efforts.... (<a href="https://youtube.com/watch?v=pAsFAeC9oro">watch</a> || <a href="/videos/2022/10/07/Lets_talk_about_Putin_s_mobilization_efforts">transcript &amp; editable summary</a>)

Beau clarifies misleading headlines on Russian mobilization, pointing out the lack of troops due to mass avoidance of conscription, painting a grim picture of Putin's imperialist endeavor in Ukraine.

</summary>

"More people have fled the country than have been conscripted."
"The cause is lost. They don't have the troops."
"Putin may push mobilization even further in hopes of just throwing enough people over there."
"At some point, somebody in Russia is going to have to make the call and explain to Putin what his options are."
"It doesn't look like any of the things that would allow him to remain are going to happen."

### AI summary (High error rate! Edit errors on video page)

Explains the mobilization in Russia and addresses misleading headlines in the United States.
Mentions the headline about 200,000 Russian soldiers being drafted, clarifying that 200,000 troops have been conscripted, but it's shy of their goal.
Points out that the most critical number is 335,000, the Russians who fled the country to avoid conscription.
Compares the number of Russians fleeing to avoid conscription to those who fled the United States during the Vietnam draft.
Indicates the lack of desire among Russian people to fight in the war, with more fleeing than volunteering.
Notes that even if Russia hits its conscription goal, they still won't have enough troops.
Mentions that half of the troops sent from one region were unfit for service, leading to the firing of the person in charge of conscription.
Expresses a pessimistic view on Russia's mobilization and the fate of troops entering Ukraine.
Describes the situation as "pure waste" due to the lack of substantial troop numbers and quality.
Speculates on Putin's next moves and the unlikelihood of success in Ukraine.

Actions:

for politically aware individuals,
Inform others about the realities of Russian mobilization (suggested)
Support efforts to provide aid and resources for those fleeing conscription (exemplified)
</details>
<details>
<summary>
2022-10-07: Let's talk about Nevada's Trump-endorsed candidate.... (<a href="https://youtube.com/watch?v=ApyZAJ3hSDQ">watch</a> || <a href="/videos/2022/10/07/Lets_talk_about_Nevada_s_Trump-endorsed_candidate">transcript &amp; editable summary</a>)

Nevada's governor candidate endorsed by Trump tries to distance himself, navigating between Trump's base and general voters, while Democrats strategize around this political tightrope.

</summary>

"You can't win a Republican primary without Trump, but you may not be able to win a general with him."
"Trump has become, in a lot of ways, political dead weight."
"Lombardo's ability to separate himself from the person who endorsed him is probably going to be a deciding factor."

### AI summary (High error rate! Edit errors on video page)

Talking about Nevada's governor candidate Joe Lombardo, who is endorsed by Trump but trying to distance himself from the former president.
Lombardo was asked if he thought Trump was a great president during a gubernatorial debate, to which he responded by saying Trump was a "sound" president.
Lombardo seems to understand the delicate balance between needing Trump's support in a Republican primary and potentially alienating general election voters.
There is speculation on how Trump will react to one of his chosen candidates not endorsing him as a great president.
The Democratic Party in Nevada might strategically refer to Lombardo as the "Trump-endorsed candidate" to exploit the distance he is trying to create from Trump.
Lombardo's ability to separate himself from Trump, who endorsed him, could be pivotal in the tight race.
Trump's endorsement has become a burden for some candidates, as they navigate between appealing to the general public and Trump's loyal supporters.
Suggestions are made for the Democratic Party in Nevada to adopt a strategy that capitalizes on this delicate political dynamic.

Actions:

for political observers, nevada voters,
Strategize party messaging to exploit the distance between the candidate and the endorser (suggested)
Support political candidates who prioritize the interests of the general populace (implied)
</details>
<details>
<summary>
2022-10-06: Let's talk about what a Polk County incident can tell us about teachers.... (<a href="https://youtube.com/watch?v=iCRBwh-7n3M">watch</a> || <a href="/videos/2022/10/06/Lets_talk_about_what_a_Polk_County_incident_can_tell_us_about_teachers">transcript &amp; editable summary</a>)

A blue-on-blue incident in Polk County prompts reflections on proper procedures and the limitations of reactive solutions like arming teachers.

</summary>

"Arming teachers isn't a solution. Bullets go through stuff. That isn't a solution. It's a band-aid at best."
"This type of scenario, something similar to this, will play out before people realize that there need to be more proactive measures involved in this."
"It doesn't look like anything like that is occurring. This looks like just fog of combat stuff."
"But I don't think this is going to be a moment where it's very clear that they violated just the basic principles, which is what normally leads to a bad shoot."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A blue on blue moment occurred in Polk County, Florida involving the Polk County Sheriff's Department.
Deputies were looking to apprehend someone with a felony failure to appear.
The deputies conducted a slow and deliberate clear of a trailer where the suspect was located.
The suspect raised a BB gun, leading to officers inside firing and a cop outside being hit and killed.
Despite initial assumptions of shadiness due to the reputation of the Polk County Sheriff's Department, it seems they followed proper procedures.
There are speculations about what may have gone wrong in the incident, but no clear evidence of gross negligence.
Beau contrasts the controlled scenario of this incident with the chaotic nature of arming teachers in schools.
He stresses that arming teachers is not a solution, as controlling rounds in enclosed spaces can be extremely challenging.
Beau points out that this incident underscores the need for more proactive measures rather than reactive band-aid solutions.
Despite initial reporting suggesting correct procedures were followed, further investigation may reveal more details about what went wrong.

Actions:

for law enforcement personnel,
Analyze proper procedures and protocols in law enforcement encounters (implied)
Advocate for proactive measures rather than reactive solutions (implied)
</details>
<details>
<summary>
2022-10-06: Let's talk about Trump and his lawyers making fun of him.... (<a href="https://youtube.com/watch?v=ZxzV85ED86M">watch</a> || <a href="/videos/2022/10/06/Lets_talk_about_Trump_and_his_lawyers_making_fun_of_him">transcript &amp; editable summary</a>)

Beau explains Trump's financial struggles, leaked emails mocking his money woes, and potential fallout on GOP candidates.

</summary>

"These stories are coming out at the same time that the truth social venture is running into issues."
"He described it as an existential threat to his business and his financial well-being."
"So there's probably going to be a whole bunch of messages and emails sent out to the MAGA faithful saying, you know, give me $45."

### AI summary (High error rate! Edit errors on video page)

Explains the recent stories portraying Trump as financially struggling, including court battles and a $250 million lawsuit.
Mentions leaked emails where Trump's lawyers mock his lack of money and financial liquidity issues.
Points out the potential consequences for Trump if he loses legal battles, such as having to sell his buildings at low prices.
Talks about Trump's dependence on his New York skyscrapers for wealth and the threat to his business if he loses them.
Notes that these financial challenges coincide with issues faced by Truth Social and Trump's legal and political setbacks.
Suggests that Trump's self-perception linked to his wealth could lead to erratic behavior.
Anticipates Trump appealing for money from supporters, potentially causing rifts among Republican candidates he endorsed.
Raises the possibility of Trump boasting about his financial status amidst fundraising controversies and limited cash dispersal.
Speculates that this boasting could fuel animosity between GOP candidates and Trump as they head into the general election.
Concludes with the potential for internal GOP conflicts due to Trump's financial circumstances impacting his political influence.

Actions:

for political observers,
Support GOP candidates independent of Trump (implied)
Stay informed about GOP internal dynamics (implied)
</details>
<details>
<summary>
2022-10-06: Let's talk about Russia falling back.... (<a href="https://youtube.com/watch?v=k4QDOzQpyK8">watch</a> || <a href="/videos/2022/10/06/Lets_talk_about_Russia_falling_back">transcript &amp; editable summary</a>)

Beau explains Russian strategy in Ukraine, revealing the offensive nature of the war and the challenges Russia faces in holding territory, leading to losses and domestic dissent.

</summary>

"It's a war of aggression, not something I hide."
"They're losing in a very loud and ugly fashion."
"Every village that is liberated is a huge loss for Russia."
"The war in Ukraine has gone from bad to worse for Russia."
"If you adopt that position, you might be engaging in maximum cope."

### AI summary (High error rate! Edit errors on video page)

Explains the situation in Ukraine and addresses a question about Russian strategy.
Points out the difference between defending deep and offensive war.
Emphasizes that Russia's only metric for victory now is taking and holding territory.
Notes that Russia's actions in Ukraine are seen as imperialism and aggression.
Describes how the Ukrainian army's advancements are causing losses for Russia.
Mentions the impact of liberated villages on Russia's narrative.
Concludes that the war in Ukraine is going from bad to worse for Russia.

Actions:

for international observers,
Support Ukrainian soldiers by providing aid and resources (suggested)
Raise awareness about the situation in Ukraine and Russian aggression (implied)
</details>
<details>
<summary>
2022-10-05: Let's talk about Velma, Scooby, and one out of five.... (<a href="https://youtube.com/watch?v=IzyOlV2mbyA">watch</a> || <a href="/videos/2022/10/05/Lets_talk_about_Velma_Scooby_and_one_out_of_five">transcript &amp; editable summary</a>)

Beau addresses the LGBTQ+ character development in Scooby-Doo, explains the importance of representation, and predicts a trend towards more diversity in media for the future.

</summary>

"It's just representation. You got it."
"You can change the world. You can solve the mystery. You can make things better."
"It's kind of always been pretty woke in those terms."
"This is the future. This is how things are progressing."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing the reaction to Velma's LGBTQ+ character development in Scooby-Doo.
Responding to questions about why orientation needs to be included in children's shows.
Explaining his perspective on the importance of representation and diversity.
Pointing out the increasing acceptance of LGBTQ+ individuals, especially among Gen Z.
Describing Scooby-Doo's counterculture aspect and empowerment of youth.
Mentioning the atheistic tendencies and anti-capitalist messages within the show.
Speculating on the future inclusion of LGBTQ+ characters in media.
Expressing curiosity about Elsa's girlfriend in Frozen.
Predicting a trend towards more representation of diverse orientations in media.

Actions:

for media consumers,
Support and celebrate diverse representations in media (implied)
Advocate for more LGBTQ+ characters in children's shows (implied)
</details>
<details>
<summary>
2022-10-05: Let's talk about Trump's emergency request of the Supreme Court.... (<a href="https://youtube.com/watch?v=3dvUczldoL0">watch</a> || <a href="/videos/2022/10/05/Lets_talk_about_Trump_s_emergency_request_of_the_Supreme_Court">transcript &amp; editable summary</a>)

The former president's emergency request to the Supreme Court is seen as a delay tactic, but mounting pressure suggests he may be realizing the gravity of the allegations, potentially leading to erratic behavior and bizarre legal maneuvers.

</summary>

"I think the possibility of accountability on this might really be shaking him."
"I think he might have had somebody finally really break it down to him and explain the legal situation that he appears to find himself in."
"I think he might be starting to really understand the gravity of the situation and the importance of those documents."

### AI summary (High error rate! Edit errors on video page)

The former president's emergency request to the Supreme Court is seen as a delay tactic in response to the documents case.
There is debate on whether the court will take up the case, with unique claims related to executive privilege raised by Team Trump.
Despite the court potentially ruling on this due to the unique claims, its current perceived partisanship may lead to avoiding a politically sensitive situation.
Given recent court rulings against Trump, it's not necessarily expected that the court will side with him in this case.
The pressure seems to be mounting on the former president, with increasing activity indicating he may be realizing the seriousness of the allegations.
Trump, known as the Teflon Don, might be facing the possibility of accountability, which could be unsettling for him.
There's a hint that someone may have clarified the legal jeopardy he's in, leading to potential erratic public statements and legal maneuvers.
Expect Trump's behavior to become even more erratic and legal maneuvering to become more bizarre in attempts to navigate the situation.
Trump's choice to involve the Supreme Court draws more attention to the documents with severe consequences if he were to face trial and be found guilty.
The situation implies that Trump won't seek a discreet resolution, hinting at a more public and attention-drawing approach.

Actions:

for legal analysts, political commentators,
Analyze and stay informed about the legal developments regarding the former president's emergency request to the Supreme Court (implied).
</details>
<details>
<summary>
2022-10-05: Let's talk about Trump packing the boxes himself.... (<a href="https://youtube.com/watch?v=JurReaV2w9E">watch</a> || <a href="/videos/2022/10/05/Lets_talk_about_Trump_packing_the_boxes_himself">transcript &amp; editable summary</a>)

Former President Trump's personal involvement in packing documents could have significant implications for possible criminal charges, as it may demonstrate willful actions critical to the case.

</summary>

"When it comes to possible criminal charges that former President Trump might face, that's a big component."
"He himself with his own hands packed the boxes."
"It helps establish the federal government's case if true."

### AI summary (High error rate! Edit errors on video page)

Former President Trump reportedly packed boxes of documents himself that were sent back to the National Archives.
Commentators are making a big deal because Trump deciding what to pack may be significant for possible criminal charges he might face.
The feds have to prove that Trump possessed the documents willfully for it to be a crime.
If Trump packed the boxes himself, it could show that he willfully left out certain information, which is a critical aspect for criminal charges.
The reporting suggests that Trump, with his own hands, determined the contents of the boxes, impacting the criminal aspects significantly.
Trump's lawyer stated that all documents were returned, but another lawyer expressed uncertainty about this claim.
If the Department of Justice wants to build a criminal case and can confirm the reporting's accuracy, it could be a significant development.
The sources in the Washington Post report are unnamed, which means it can't be treated as 100% fact.
Providing information to a reporter anonymously differs from saying it under oath in court.
The reporting helps establish the federal government's case if true and could be a critical piece if charges are filed against Trump.

Actions:

for journalists, legal analysts,
Investigate further into the reported actions of former President Trump (implied).
Stay updated on developments regarding the Department of Justice's actions (implied).
</details>
<details>
<summary>
2022-10-05: Let's talk about Georgia and the Herschel Walker situation.... (<a href="https://youtube.com/watch?v=MWeHkyK8FJY">watch</a> || <a href="/videos/2022/10/05/Lets_talk_about_Georgia_and_the_Herschel_Walker_situation">transcript &amp; editable summary</a>)

Exploring the hypocrisy in the Republican Party's actions, prioritizing political control over values like freedom and individual liberty.

</summary>

"You don't care about freedom. You don't care about individual liberty."
"You don't care about the Republic, you don't care about representative democracy, you don't want to be represented, you want to be ruled."
"They have tricked you, they have duped you. They have control of you."
"They've got you."
"They don't care about freedom. They don't care about individual liberty."

### AI summary (High error rate! Edit errors on video page)

Exploring the Senate race in Georgia and Republican nominee Herschel Walker's controversy.
Walker's stance against certain family planning methods without exceptions.
Allegations surface about Walker paying for family planning he opposes.
Republican Party's response is the focus, revealing a significant shift from stated beliefs.
Dana's comments show prioritizing winning and control over principles.
Reveals a willingness to overlook alleged behavior for political power.
Republican Party's actions contradict their supposed values of freedom and individual liberty.
Choosing political control over supporting neighbors' autonomy.
Beau questions the true motives behind Republican Party's actions.
Emphasizes how people are being manipulated for political gain.
Criticizes the hypocrisy and lack of genuine concern for freedom and liberty.
Condemns the prioritization of political power over individual rights.
Calls out the Republican Party for valuing control over democracy and representation.
Points out the irony in the Party's actions versus their rhetoric.
Ultimately, Beau stresses the importance of recognizing the manipulation for power in politics.

Actions:

for voters,
Challenge political representatives on their actions (implied).
Support policies that prioritize individual freedom and liberty (implied).
</details>
<details>
<summary>
2022-10-04: Let's talk about gas prices going back up and OPEC.... (<a href="https://youtube.com/watch?v=NxmHW_bTChY">watch</a> || <a href="/videos/2022/10/04/Lets_talk_about_gas_prices_going_back_up_and_OPEC">transcript &amp; editable summary</a>)

OPEC's potential oil production cut in Vienna may spike gas prices, urging a necessary transition to green energy for economic, security, and environmental reasons.

</summary>

"Green energy, clean energy, is secure energy."
"The longer we wait, it's just the more money we spend. It's the more damage we do."
"Oil is a capitalist endeavor. They will charge whatever the market will bear."
"So not great news, expected to be coming out of Vienna tomorrow, but it's also not surprising news."
"It's going to have to occur."

### AI summary (High error rate! Edit errors on video page)

OPEC Plus will meet in Vienna to announce a production cut, potentially between 500,000 and 1.5 million barrels per day.
The aim is to raise oil prices to $100 to $105 per barrel within the next 90 to 180 days.
This increase in oil prices will lead to a corresponding rise in gas prices.
Calls for increased oil drilling as a solution are shortsighted since the global oil market allows for adjustments by OPEC.
Transitioning to green and clean energy is imperative for economic, national security, and environmental reasons.
The United States and many other countries heavily rely on oil consumption, necessitating a shift towards renewable energy.
Delaying the transition to clean energy leads to increased costs and environmental damage.
Rising gas prices will likely contribute to inflation, affecting the cost of transportation and all other goods.
Oil companies maximize profits by creating artificial scarcity in the market, a common practice in the industry.
The capitalist nature of the oil industry drives companies to charge based on market demand.

Actions:

for energy consumers,
Advocate for and support the transition to green and clean energy (implied).
Reduce personal reliance on fossil fuels by exploring alternative transportation methods (implied).
</details>
<details>
<summary>
2022-10-04: Let's talk about doing disaster relief wrong.... (<a href="https://youtube.com/watch?v=-77kjfjCJsA">watch</a> || <a href="/videos/2022/10/04/Lets_talk_about_doing_disaster_relief_wrong">transcript &amp; editable summary</a>)

Someone travels to help disaster victims, faces criticism for starting with known contacts, but Beau defends the importance of networking and mutual assistance in relief efforts.

</summary>

"The first rule of disaster relief is don't create a second victim."
"You start with the points of contact on the ground that you have."
"The whole idea is based around the premise that one network of people assists another network of people."
"People who may not get involved themselves will always have a criticism of the way you did it."
"Just keep in mind, you did it."

### AI summary (High error rate! Edit errors on video page)

Someone went to Fort Myers to provide gas and food to people affected by a disaster.
The person started by helping people in their parents' neighborhood and then expanded to nearby neighborhoods.
They were criticized by members of a college organization for not being a "real activist" because they started with people they knew.
Beau argues that the first rule of disaster relief is to not create a second victim.
He suggests not listening to critics who aren't involved in relief work themselves.
Beau commends the individual for starting with people they knew and expanding from there.
He dismisses the idea of "purity testing" in disaster relief efforts.
Beau explains that disaster relief involves networking and helping those you're connected to.
He shares a specific example of how helping one person led to assisting others in need.
Beau stresses the importance of utilizing existing networks and contacts in relief efforts.

Actions:

for disaster relief volunteers,
Reach out to your existing network to identify needs and provide assistance (exemplified)
Collaborate with local organizations or supply depots to distribute aid effectively (exemplified)
Utilize community connections to expand the reach of relief efforts (exemplified)
</details>
<details>
<summary>
2022-10-04: Let's talk about Trump suing CNN by the numbers.... (<a href="https://youtube.com/watch?v=e45fiyXNRvo">watch</a> || <a href="/videos/2022/10/04/Lets_talk_about_Trump_suing_CNN_by_the_numbers">transcript &amp; editable summary</a>)

Former President Trump's lawsuit against CNN alleging defamation and an attempt to undermine his 2024 run faces significant challenges, with polling data indicating strong opposition to his candidacy.

</summary>

"It doesn't seem like Trump's 2024 bid is destined to really go anywhere."
"The majority of Americans believe he should not be allowed to run, and 44% believe he should be charged."
"This, to me, seems to be another attempt by Trump to try to grab some headlines."

### AI summary (High error rate! Edit errors on video page)

Former President Donald Trump filed a lawsuit against CNN seeking $425 million in damages, alleging defamation and claiming the outlet is trying to undermine his possible 2024 run.
Trump's lawsuit states that CNN has called him names such as racist and Russian lackey to harm his chances in the upcoming election.
In a defamation case against a large outlet like CNN, the person suing must prove actual malice, which is a challenging standard to meet.
Recent polling indicates that 44% of Americans believe Trump should be charged with a crime, making it unlikely for them to vote for him.
Hypothetical match-ups show Biden winning against Trump in 2024, even within the Republican Party where only 47% support Trump as the primary choice.
A majority of Americans, 51%, think that the allegations against Trump so far disqualify him from running for office again.
Given the significant opposition to Trump's potential candidacy, it is unlikely that a major outlet like CNN is conspiring against him to prevent his 2024 run.
Trump's lawsuit seems more like an attempt to gain publicity and re-enter the mainstream due to dwindling support and becoming a fringe candidate.
Trump's dwindling appeal to independent voters and the GOP's diminishing political value of his candidacy are factors contributing to his uphill battle for the 2024 election.
The lawsuit against CNN appears to be a strategic move by Trump to regain relevance in the political landscape.

Actions:

for political observers,
Analyze and stay informed about political developments and strategies (implied)
Engage in critical thinking and fact-checking regarding political claims and lawsuits (implied)
</details>
<details>
<summary>
2022-10-03: Let's talk about hope and lessons learned from Hurricane Ian.... (<a href="https://youtube.com/watch?v=a6y4svQTXXg">watch</a> || <a href="/videos/2022/10/03/Lets_talk_about_hope_and_lessons_learned_from_Hurricane_Ian">transcript &amp; editable summary</a>)

Beau advises on practical disaster relief lessons, from preparedness to providing hope, showcasing effective community response.

</summary>

"Treat it like going to Antarctica. If you didn't bring it, it's not there."
"Your social media, your public messaging, that's your radio."
"After every disaster, people start screaming about looters. Don't do that."
"You're supposed to be conveying calm."
"If you want to be that leader tough guy, that's what you should be doing, not inspiring paranoia and fear."

### AI summary (High error rate! Edit errors on video page)

Addresses lessons learned from helping out with EIN after a hurricane, offering practical advice that can be immediately applied.
Clarifies the relative scale of the hurricane's impact compared to past disasters like Michael or Katrina.
Mentions areas like Arcadia and Sebring that may not have received as much coverage but are recovering.
Advises on the importance of being prepared with gas when entering a disaster area, especially for relief efforts.
Recommends considering generators that use gas, propane, or solar power for post-disaster situations.
Emphasizes the need for proper footwear and essentials when assisting in disaster areas.
Suggests stocking up on freeze-dried food to avoid relying solely on National Guard distributions post-disaster.
Shares experiences with using generators and chainsaws to provide aid and why they are prioritized in relief efforts.
Acknowledges the positive response of local governments in handling the aftermath of the hurricane.
Talks about funding relief efforts through viewer contributions and the importance of bringing hope during disasters.
Shares a personal experience of leaving supplies unsecured overnight in a disaster-stricken area and finding them untouched.

Actions:

for disaster relief volunteers.,
Stock up on freeze-dried food for emergency situations (suggested).
Ensure you have enough gas for both entry and exit when entering disaster areas (implied).
Invest in generators that use multiple power sources like gas, propane, or solar (implied).
Prioritize providing tools for self-rescue like generators and chainsaws in relief efforts (implied).
</details>
<details>
<summary>
2022-10-02: Let's talk about what we can learn from Jan 6 defendants.... (<a href="https://youtube.com/watch?v=6UVBPKlxjps">watch</a> || <a href="/videos/2022/10/02/Lets_talk_about_what_we_can_learn_from_Jan_6_defendants">transcript &amp; editable summary</a>)

Beau encourages showing up locally, surrounding oneself with motivated individuals, and redirecting energy towards positive actions for a better world.

</summary>

"Show up, show up."
"Everybody has something that they can contribute."
"Surround yourself with people who genuinely want to make the world a better place."
"You're around people who genuinely want to make the world a better place."
"Can you imagine the amount of good it would do if that much energy was expended in making things better, in building rather than destroying?"

### AI summary (High error rate! Edit errors on video page)

Talks about a positive lesson from January 6th defendants.
Encourages people to show up and get involved locally.
Emphasizes that everyone has something to contribute.
Mentions the theme of "getting caught up" in the January 6th defendants' statements for leniency.
Points out that surrounding oneself with motivated people can lead to positive outcomes.
Compares the motivation gained from positive causes to the negative consequences of getting caught up in destructive actions.
Suggests that redirecting energy towards positive actions can lead to significant good in the world.

Actions:

for community members ready to get involved.,
Attend local community events and meetings to get involved (implied).
Surround yourself with individuals motivated to make positive change (implied).
Redirect energy towards building and improving the community (implied).
</details>
<details>
<summary>
2022-10-02: Let's talk about ignorance vs racism.... (<a href="https://youtube.com/watch?v=t46xbJmOzKQ">watch</a> || <a href="/videos/2022/10/02/Lets_talk_about_ignorance_vs_racism">transcript &amp; editable summary</a>)

Beau explains the importance of combating ignorance to prevent it from fueling racism, suggesting calmly educating individuals as a solution.

</summary>

"Ignorance feeds the racism."
"Stamping out the ignorance should be a pretty high priority."
"You could deal with it."
"Those two things aren't mutually exclusive."
"It's on all of us [...] to fix this."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of combating ignorance on a wider scale.
Shares a personal experience where a black acquaintance did not correct a racist comment because he viewed it as ignorance, not malicious racism.
Defines ignorance as a lack of information that often leads to stereotypes, which can be racist.
Suggests reasons why the black acquaintance didn't correct the ignorant comment, including feeling sorry for the person and exhaustion from constantly educating others.
Points out the prevalence of ignorance in the United States regarding race, culture, nationality, and other aspects.
Provides an example from an episode of King of the Hill to illustrate the difference between ignorance and racism.
Encourages addressing ignorance by calmly educating individuals to prevent it from fueling malicious racism.
Emphasizes that combating ignorance is vital as it serves as a tool for real racists to spread their harmful messages.

Actions:

for all individuals,
Educate individuals calmly on the impact of their ignorant comments (suggested).
Step in and address ignorance to prevent it from fueling malicious racism (implied).
</details>
<details>
<summary>
2022-10-02: Let's talk about Psychology and Alameda, California.... (<a href="https://youtube.com/watch?v=YBJgoziUBzU">watch</a> || <a href="/videos/2022/10/02/Lets_talk_about_Psychology_and_Alameda_California">transcript &amp; editable summary</a>)

Psychological testing is vital for law enforcement suitability, but rushing retesting may lead to gaming the system rather than addressing deeper issues, raising concerns about systemic problems and the need for nationwide testing.

</summary>

"One out of 20 cops that's out on the street shouldn't be there."
"That's way more than a few bad apples."

### AI summary (High error rate! Edit errors on video page)

There is a push for psychological testing of officers to determine their suitability for duty.
An audit in Alameda County Sheriff's Department resulted in 47 deputies having their weapons and arrest powers taken away.
This amounts to 5% to 10% of the force, showing a significant oversight.
The audit was prompted by a former deputy allegedly involved in a double shooting.
Nationwide psychological testing for law enforcement officers is necessary.
The issue arises with the expectation of retesting within two months, questioning if it's to address issues or beat the test.
Law enforcement agencies sometimes believe they are above laws and regulations they enforce, indicating a systemic problem.
Rushing retesting may lead to gaming the system rather than addressing underlying issues.
There's skepticism about fixing personnel in two months to meet required standards.
It's clear that nationwide psychological testing is vital to ensure officers' suitability.

Actions:

for law enforcement oversight advocates,
Advocate for nationwide psychological testing for law enforcement officers (suggested)
Raise awareness about the importance of addressing deep-rooted issues in law enforcement (implied)
</details>
<details>
<summary>
2022-10-01: Let's talk about the Cherokee and the Treaty of New Echota.... (<a href="https://youtube.com/watch?v=Ucz9cPD9V0s">watch</a> || <a href="/videos/2022/10/01/Lets_talk_about_the_Cherokee_and_the_Treaty_of_New_Echota">transcript &amp; editable summary</a>)

The Cherokee Nation seeks a delegate in the House of Representatives to fulfill a 200-year-old promise, urging the U.S. government to honor treaties and address historical injustices.

</summary>

"It's probably time for the U.S. government to honor the treaties and include all the words."
"A little too realistic, I guess. A little bit too representative of the whole."
"It might be time to do that. Or they could just, you know, forget about those few words again."
"The Cherokee Nation are asking Congress to give them a delegate in the House of Representatives. They really shouldn't be asking, they should be demanding."
"It's been almost 200 years and it hasn't happened because, as is often the case when it comes to Native treaties or anything that deals with Natives in the American government, well, let's just sort of ignore that part."

### AI summary (High error rate! Edit errors on video page)

The Cherokee Nation is seeking fulfillment of a 200-year-old promise from the 1835 Treaty of New Echota for a delegate in the House of Representatives to represent their interests.
Despite the stipulation in the treaty, the promise has not been honored for almost two centuries, reflecting a historical pattern of neglect towards Native treaties and issues in the American government.
The Treaty of New Echota, signed through debatable means, resulted in the Trail of Tears, but significant parts of the treaty remain unfulfilled.
If granted, the Cherokee Nation's delegate in the House of Representatives, Kim Teehee, will be a non-voting member but will have the ability to participate in debates, introduce legislation, and potentially serve on committees.
Beau advocates for honoring the promises made in treaties, addressing historical issues, and making amends for past injustices towards Native communities.

Actions:

for advocates for native rights,
Support the Cherokee Nation's request for a delegate in the House of Representatives (suggested).
Advocate for honoring Native treaties and addressing historical injustices (suggested).
</details>
<details>
<summary>
2022-10-01: Let's talk about teachers, Russians, and supplies.... (<a href="https://youtube.com/watch?v=qOxSJp-Sg2w">watch</a> || <a href="/videos/2022/10/01/Lets_talk_about_teachers_Russians_and_supplies">transcript &amp; editable summary</a>)

Beau compares the disparity in supplies between American teachers and Russian troops, urging to address the critical equipment shortage.

</summary>

"American teachers are better equipped, better prepared, and better trained to deal with gunfire than troops heading into Russia that were recently conscripted."
"That is an indictment of both countries."
"The tourniquets, the chest seals, this is stuff they desperately need over there and they don't have."
"Probably something we should work on."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Comparison between supplies for troops in Russia and the United States.
Russian troops facing challenges with basic equipment like armor and medical supplies.
Russian troops being ill-equipped and unprepared for sustained or wider scope wars.
Mention of a video showcasing a bag with everything needed to treat injuries for troops heading into combat.
American teachers having better equipment, preparedness, and training to deal with gunfire compared to Russian troops.
Indictment of both countries for the disparity in equipment between American teachers and Russian troops.
Importance of not normalizing American teachers having the exact equipment needed by Russian troops in combat.
Mention of tourniquets and chest seals as critical supplies lacking for Russian troops.
Suggestion to work on ensuring proper supplies for Russian troops.
Beau leaves viewers with a thought-provoking message about the supply disparities.

Actions:

for global citizens,
Ensure proper supplies for troops (suggested)
Advocate for better equipment for troops (implied)
</details>
<details>
<summary>
2022-10-01: Let's talk about being polite after public figures cease to be.... (<a href="https://youtube.com/watch?v=VHGAtDPpMTU">watch</a> || <a href="/videos/2022/10/01/Lets_talk_about_being_polite_after_public_figures_cease_to_be">transcript &amp; editable summary</a>)

Questioning the appropriateness of politeness after the passing of influential figures, focusing on the impact on those around you and considering cultural context.

</summary>

"Politeness is not for the person who is gone; you're being polite for the people around you."
"Sometimes being rude is a way to make a point."
"Being polite or rude depends greatly on the situation."
"You should probably take regional views into account because you're not being polite for the benefit of the person who's gone."
"Ultimately, the decision to be rude or polite should be based on specific circumstances and cultural norms."

### AI summary (High error rate! Edit errors on video page)

Questioning the appropriateness of being polite after the passing of a monarch, billionaire, or politician.
Politeness is not for the deceased individual but for the people around you.
Acknowledges that sometimes being rude can effectively make a point.
Talks about a figure in US politics and plans to address their impact after they pass.
Considers that being rude or polite depends greatly on the situation and the cultural context.
Being polite or rude about a person's passing will be received differently based on the region.
The focus should be on how your actions are perceived by those around you, not the deceased individual.
Politeness is a consideration but not a hard rule in these situations.
Suggests that the passing of influential figures could be a time to advocate for change in policies.
Ultimately, the decision to be rude or polite should be based on the specific circumstances and cultural norms.

Actions:

for individuals reflecting on social etiquette.,
Respect regional views on politeness (implied).
Advocate for policy changes following influential figures' passing (implied).
</details>
