---
title: Let's talk about not talking about Hunter Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZITQlkj9Gs0) |
| Published | 2022/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Jahn says:

- Explains why he doesn't talk about specific events like Hunter Biden and his recent troubles in Ukraine, as he doesn't run a gossip column.
- Mentions that as a private citizen, Hunter Biden's personal matters are not of interest unless they directly relate to an official capacity.
- Points out that he applies the same standard to all public figures, including Trump's kids and the former First Lady.
- Emphasizes the importance of focusing on real news instead of searching for scandals involving public figures.
- Suggests that media outlets can manipulate people by villainizing certain groups based on connections to others, leading individuals to compromise their beliefs.

### Quotes

- "I don't run a gossip column."
- "If there was a situation that Hunter Biden found himself in that did relate to an official capacity or the official capacity of Biden, even if it was just the appearance of it, I would talk about it."
- "There is enough real news to kind of parse through that searching for scandals about people that are related to public figures, that's just, I don't have time for that."
- "They give you something that is emotionally exciting."
- "Unless Hunter Biden's activity somehow impacted his father's activities, I literally don't care at all."

### Oneliner

Beau Jahn explains why he avoids discussing specific events like Hunter Biden's, focusing on real news and avoiding gossip.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Analyze news critically (implied)
- Focus on real news and avoid getting drawn into sensationalized scandals (implied)

### Whats missing in summary

The detailed explanation and reasoning behind Beau Jahn's editorial choices regarding what news topics to cover and why.

### Tags

#MediaConsumption #Gossip #HunterBiden #EditorialChoices #RealNews


## Transcript
Well, howdy there, internet people, it's Bo Jahn.
So today we are going to talk about why we don't talk about Bruno
and why some people who provide coverage don't talk about specific events
that a lot of other outlets might discuss.
And we're going to do this because I was asked a very direct question
and it has a pretty unique offer in it
that I am intrigued by, so we'll go through it and maybe it'll clear some things up, but anyway,
here's the message. Why don't you talk about Hunter Biden, all caps? You're a paid, democratic all
caps, shill. That's why. This is why you don't talk about Hunter Biden, all caps. I'll give you a
thousand dollars, all caps. If you talk about what he did in Ukraine or him being a gun-toting,
all caps, changing out a word here, a person with a substance issue. You won't because the DNC owns
you. Okay, so the real question here is why haven't I talked about Hunter Biden? I'm assuming this
This is dealing with the recent leak that kind of suggests maybe the feds are looking
into him on a gun charge.
I'm assuming that's what this is about.
Well the main reason is that I don't run a gossip column.
The next reason is the same reason I didn't cover the guy who works down the road at the
Texaco who got picked up for the same thing recently.
Who is Hunter Biden?
Private citizen.
If you're interested, this isn't something that is just applied to democratic family
members.
Go back through my videos and look for any instance of me talking about Trump's kids
and any of the multiple scandals that they were involved in that did not directly relate to something in their official
capacity?
The answer is there's none.
Go back and look for me talking about the First Lady, the former First Lady.
Um, that was a very common topic. You will find two references to her.
One is when she was acting as First Lady and wore that jacket, making a political statement.
That's in bounds to me.
The other is about the photos, you know, the ones that everybody was criticizing, that
I defended because that had nothing to do with her official capacity.
That's why I don't do it.
It doesn't matter.
I mean, plain and simple.
But if there was a situation that Hunter Biden found himself in that did relate to an
official capacity or the official capacity of Biden, even if it was just the appearance
of it, I would talk about it.
So it might be titled, let's talk about a Ukraine-Biden timeline in journalism 101.
I would like my thousand dollars, please.
It's been up for a while.
That's why.
It's not something that I don't cover stuff like this.
There is enough real news to kind of parse through that searching for scandals about
people that are related to public figures, that's just, I don't have time for that.
I don't really care.
I would also point out that if an outlet can convince you to start doing this and just
villainize any particular group of people based on connections to other people, then
they can get you to argue against your own beliefs.
Things that you hold to be true, they can get you to say, oh no, well it's okay to
go after somebody for doing something that you believe is okay. Mr. shall not
be infringed wanting somebody charged with a gun crime. This is how they get
you to sell out your beliefs for a red hat. They give you something that is
emotionally exciting. It gets you drawn in. It gets that vindictive nature
you're going. And the next thing you know, you're writing letters half in all caps to get people to
talk about something that on your own timeline you say you don't agree with, because you want
those people, the others, to suffer.
Yeah, unless Hunter Biden's activity somehow impacted his father's activities, I literally
don't care at all.
And if you go back and watch that video, you'll find out that a lot of what you might believe
about Ukraine isn't accurate.
There's more to it than you might imagine.
And it's not that there was some kind of influence from Hunter Biden to his father.
And that's why things happened.
That's what you're going to hear on the news outlets you presumably consume because that's
how they can get you to do whatever they want you to.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}