---
title: 'Let''s talk about bridges: military or civilian....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f6aTS-aawjk) |
| Published | 2022/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the misinformation surrounding bridges and their military importance.
- Questions the narrative that bridges used for military purposes are civilian infrastructure.
- Uses the example of Ukrainian bridges being hit by Ukrainians to illustrate the military nature of targeting bridges.
- Stresses that taking out bridges has always been a military strategy to impede routes and slow advances.
- Mentions the importance of bridges in war strategies and the limited ways to move from point A to point B.
- Emphasizes that anyone claiming a bridge used for military purposes is civilian infrastructure is trying to deceive.
- Concludes with a thought about the significance of bridges in warfare.

### Quotes

- "Anybody who is trying to tell you that a bridge used to move troops and supplies is civilian infrastructure either knows nothing about war or is deliberately misleading you."
- "Taking out bridges has always been a military thing."
- "Anybody who is trying to sell you on the idea that a bridge used to move troops and supplies is civilian infrastructure is trying to sell you a bridge."

### Oneliner

Beau breaks down the misconception around bridges, exposing their military significance and debunking the idea of bridges for military use being classified as civilian infrastructure.

### Audience

Military history enthusiasts

### On-the-ground actions from transcript

<!-- Skip this section if there aren't physical actions described or suggested. -->

### Whats missing in summary

The detailed examples and historical context provided by Beau in the full transcript.

### Tags

#Bridges #Military #Warfare #Misinformation #Ukraine #Infrastructure


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about bridges
and various kinds of bridges
and all of the discussion that's going on about bridges.
Because there are people who are either uninformed
or they are intentionally misframing something.
So we're going to kind of go through it.
I'm picking this just because it's one of a hundred people
talking about it and using this framing.
It's insane to me that this guy, I'm this guy by the way,
is talking about a deliberate attack
on civilian infrastructure.
Civilian infrastructure.
Is it, Beau?
Is a bridge used to move troops and supplies?
Is that civilian infrastructure?
It's a military objective.
It's been a military objective since there were bridges.
In fact, I'm fairly certain that if you could go back to prehistory,
you would find men in loincloths with sharpened sticks
fighting over the shallow part of the river.
Anybody who is trying to tell you
that a bridge used to move troops and supplies
is civilian infrastructure either knows nothing about war
or is deliberately, deliberately misleading you
and misframing this.
Let's talk about something else as a comparison.
Let's talk about all the Ukrainian bridges
that have been hit by Ukrainians.
Everybody's talking about how Russia hit civilian infrastructure.
That's fine.
I mean, you can talk about that if you want to,
but really that's kind of a whataboutism.
It's showing the nature of war.
It's better to point to Ukraine taking out Ukrainian bridges
because that shows it's a military objective.
It's something to cut routes, slow advances.
It's a military thing.
Taking out bridges has always been a military thing.
In fact, if somebody's initial video
talking about their assessment of what's going to happen,
if that didn't include Ukraine dropping their own bridges,
I would doubt their assessments
since I know somebody's going to ask February 10th,
let's talk about the futures of Ukraine and Russia.
That was mine.
It's that common.
In that video, I say something to the effect of,
you know, it doesn't take a lot of people.
You just need a few who know what they're doing,
and there's only so many ways to get from point A to point B.
And that bridge, we're going to make that super unsafe.
It is an integral part of war, cutting routes.
Anybody who is trying to sell you on the idea
that a bridge used to move troops and supplies
is civilian infrastructure is trying to sell you a bridge.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}