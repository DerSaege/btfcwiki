---
title: Let's talk about Biden, Hannity, and a voicemail....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vLxB8NF4gs4) |
| Published | 2022/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reacts to Sean Hannity playing President Biden's voicemail to Hunter Biden from 2018.
- Expresses surprise at the timing of discussing Hannity before knowing about the voicemail situation.
- Describes making similar emotional calls to loved ones in the past.
- Comments on the humanizing effect of Biden's message and how it contrasts with the Republican Party.
- Predicts that the voicemail revelation will backfire on the Republican Party.
- Believes the voicemail showcases true family values, contrasting with the image-focused ideals of the GOP.
- Concludes by expressing his thoughts and wishing everyone a good day.

### Quotes

- "This is the single most humanizing thing that has happened in a long time in American politics."
- "I do not think that this is going to damage Biden in any way."
- "Those, what you heard in that voicemail, those are family values."
- "They care about a TV family that never existed, a family without problems."
- "No matter what you need, I love you."

### Oneliner

Beau reacts to Sean Hannity's airing of President Biden's emotional voicemail, predicting backfire on the GOP and praising the display of true family values.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Reach out to loved ones in need of help or support (exemplified)
- Advocate for genuine family values in political discourse (exemplified)

### Whats missing in summary

The full transcript provides a deeper analysis of how personal moments can impact political narratives and perceptions. 

### Tags

#FamilyValues #SeanHannity #PresidentBiden #Voicemail #GOP


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Sean Hannity
and voicemails and effects.
But first to kind of head something off.
Now when I recorded the last video that went out last night,
I had no idea what Hannity was about to do.
No clue.
That's just bizarre happenstance.
So if you don't know, Sean Hannity
played a voicemail that is reportedly
President Biden calling Hunter Biden back in 2018
when Hunter Biden was having issues.
This is what was said.
It's dad.
I called to tell you I love you.
I love you more than the whole world, pal.
You got to get some help.
I don't know what to do.
I know you don't either.
I'm here no matter what you need.
No matter what you need, I love you.
I've made a lot of videos.
I've made this call to loved ones before.
Made this call to loved ones before
and wound up on voicemail
because you always end up on voicemail.
When you call, you know you're going to end up on voicemail.
That's why you're calling.
You know at that moment they're doing something
they shouldn't be.
And if you've never had to make a call like this,
let me tell you the emotions you are experiencing
as you make the call,
worry, concern, frustration, hopelessness,
anger, disappointment, all at once.
And you have to choose your words carefully.
And you have to make sure that you leave the door open.
You know?
I hope that the people I've called
when they listened to that message,
I hope it came across half as empathetic
as President Biden's message did.
This voicemail, putting this out there,
I do not think this is going to have the effect
the Republican Party hopes it's going to have.
This is the single most humanizing thing
that has happened in a long time in American politics.
I'm fairly certain this is going to backfire in a big way,
and it should.
There's a whole lot of people across this country
who have had to make this phone call to loved ones.
Maybe not a son, but they've had to make this call.
And they've been in that situation,
and they recognize that tone of voice,
all those emotions, that desperation,
hoping you can find the right combination of words
to get that person to seek help.
I do not think that this is going to damage Biden in any way.
I would suggest that there might be a whole lot of people
who just realized that the party of family values
doesn't care about families.
They don't.
They care about an image.
They care about a TV family that never existed,
a family without problems.
That's what they care about.
Big surprise, something fake.
Those, what you heard in that voicemail,
those are family values.
No matter what you need, I love you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}