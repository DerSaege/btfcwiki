---
title: Let's talk about mobilization, Putin, and sleeping....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uY-VdXhVawI) |
| Published | 2022/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin claimed mobilization in Ukraine will end in a couple weeks, but it is unlikely given Russian military's inability to foresee nightfall.
- Mobilization in Ukraine facing resistance, protests, mass exodus, attacks, and internal conflicts like a Russian officer insulting a Tajik god.
- Lack of unit cohesion among Russian and Tajik troops due to differing cultural identities and lack of sensitivity training.
- Beau criticizes hyper-masculine military culture in Russia, contrasting it with the U.S. military's likely response to such conflicts.
- Sensitivity training could have prevented the clash between Russian and Tajik troops at the mobilization center.

### Quotes

- "The whole point of being woke is so they don't catch you while you're sleeping."
- "Much more unlikely with the U.S."
- "That hyper-masculine military exists. It's that steaming pile of failure over there in Russia."
- "And one of the Russian officers basically insulted their god. And the Tajik troops lit him up."
- "Well, howdy there, Internet people."

### Oneliner

Beau analyzes Putin's statement on Ukraine mobilization, criticizes Russian military's lack of foresight and unit cohesion issues, contrasting hyper-masculine culture with a need for sensitivity training.

### Audience

Military reform advocates

### On-the-ground actions from transcript

- Implement sensitivity training for military personnel (implied)
- Advocate for diverse and inclusive military culture (implied)

### Whats missing in summary

Full context and nuances of Beau's analysis and commentary on the current situation in Ukraine and the contrasting military cultures between Russia and the U.S.

### Tags

#MilitaryReform #Putin #UnitCohesion #SensitivityTraining #UkraineMobilization


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the mobilization over there, how it's going.
We're going to talk about Putin's statement because we've got a question about that.
And we will talk about unit integrity and unit cohesion because these are things that
are becoming important over there all of a sudden.
Okay so what was the question?
If you missed it, Putin said that the mobilization would end in a couple weeks.
They have the troop numbers that they think they need.
Just a couple more weeks, don't worry about it.
And I got a question asking me if I thought that was true.
Well do I think it's going to be over in a couple of weeks?
No.
But if you take Putin's whole statement, then it becomes true.
What he said was that it would be over in a couple of weeks and that there weren't further
actions planned for the foreseeable future.
That's a key phrase.
And that I believe is true because if there is one thing that we have learned about Russian
command from this little excursion is that they can't foresee nightfall.
Two weeks out, I mean that's long term strategic planning for them at this point.
So yes, for the foreseeable future, it's probably it.
But the foreseeable future ends before his two week grace period.
No I don't think the mobilization is over.
I think it will continue because I think they're going to continue to run out of troops.
How's the mobilization going overall?
Well, they're meeting a lot of resistance.
There's been the protests.
There's been people fleeing the country by the hundreds of thousands.
There have been the attacks and now you have the situation that has arisen at a mobilization
center.
And if you haven't heard about this, all reporting, with the exception of Russian state media,
is suggesting that there were some Russian troops and some Tajik troops at the same mobilization
center.
And one of the Russian officers basically insulted their god.
And the Tajik troops lit him up.
That is the reporting and that is completely believable.
That's almost like arming a whole bunch of people with different cultural identities,
with different ways of thinking, and throwing them together isn't a good way to build unit
cohesion.
What would have stopped this?
Little bit of sensitivity training.
When you hear Republicans talk about how the military is becoming woke and they want that
hyper-masculine military, that hyper-masculine military exists.
It's that steaming pile of failure over there in Russia.
This type of thing?
Much more unlikely with the U.S.
Why?
Because the second those Tajik sergeants went and washed up after somebody insulted their
god, the American troops would have known what's up.
The whole point of being woke is so they don't catch you while you're sleeping.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}