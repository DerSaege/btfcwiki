---
title: Let's talk about Biden's student debt relief going live....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mV-EJCx9g5I) |
| Published | 2022/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of the Biden Student Debt Relief Program and addresses the confusion surrounding the application timelines.
- The application for student debt relief is now live on studentaid.gov, requiring basic information such as date of birth, social security number, and contact details, and it only takes 5 to 10 minutes to complete.
- There are two deadlines being discussed: November 15th and December 31st. November 15th is more urgent for those whose remaining debt is less than the relief amount, as payments restart in January.
- Individuals with debt higher than the relief amount have until December 31st to submit their applications.
- Beau speculates on the bureaucratic reasons behind requiring individuals to apply for debt relief, even though the government already has their records.
- The relief program is income-based, with a cutoff at around $125,000, and applicants may need to provide additional verification like tax returns.
- Beau encourages viewers to take the 10 minutes needed to apply for the relief program as it can result in $10,000 to $20,000 worth of debt relief.

### Quotes

- "8 million people have already applied."
- "It's a bureaucracy because it's the government."
- "I mean, I don't know. It's a 5 to 10 minute process and you're going to get either $10,000 or $20,000."

### Oneliner

Beau explains the Biden Student Debt Relief Program deadlines and income-based application process, urging quick action for potential debt relief of $10,000 to $20,000.

### Audience

Students and individuals with student debt

### On-the-ground actions from transcript

- Apply for the Biden Student Debt Relief Program before the deadlines (suggested).
- Share information about the relief program with others who may benefit (implied).

### Whats missing in summary

Details on how to access the application for the Biden Student Debt Relief Program and the specific criteria for eligibility.

### Tags

#StudentDebt #BidenProgram #DebtRelief #ApplicationDeadlines #IncomeVerification


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
Biden Student Debt Relief Program.
What's going on with that?
Because the application is now live
and there are already questions.
And it has to do with the timelines.
There are two deadlines that are being floated.
One is December 31st, one is November 15th,
and we will explain what the difference is.
Okay, so first, the basics.
studentaid.gov.
You need your date of birth, your social,
your contact information.
Whole process is supposed to take 5 to 10 minutes.
You can do it in Spanish or English, mobile or desktop.
8 million people have already applied.
And for those who don't remember,
this is for the either $10,000 or $20,000 worth of student debt
relief.
OK, so the two competing deadlines
that are being talked about.
December 31 is your final deadline.
That's the cutoff point, apparently.
At least that's what's being reported.
November 15 is another date that's been thrown out there.
And that has to do with people who, like let's say,
you owe $7,000 still, OK?
When you get your debt relief, you're
not going to owe anything anymore. If what you owe is less than the amount
of relief you're going to get, you want to have yours in by November 15th
because the payments restart in January. So you want them to have enough time to
process it so you don't have to make another payment. Now if you owe
$30,000 and you're getting 10 knocked off, yeah, you have till December.
Because you're going to make that next payment anyway.
It's irrelevant in that sense.
You still want to get it in, but it's not as pressing when it comes to the time.
Another question that has been asked is, you know, they already have these records, why
are they making you apply for it?
I mean, I don't know.
It's a 5 to 10 minute process and you're going to get either $10,000 or $20,000.
I would suggest sometime in the next month or so, you find the 10 minutes to do it.
Why are they making it a thing they have to apply for?
it's a bureaucracy because it's the government. I mean there's a whole bunch
of reasons. Yeah it probably would have made more sense just to knock it off of
everybody's but they're probably going to need like permission to pull your tax
returns or something like that to verify that you make less than I want to say
it's a hundred twenty five thousand dollars because there is a cutoff when
when it comes to the amount of income you can make.
This is means tested.
But the application's live, and you
can get it done pretty quickly from what I understand.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}