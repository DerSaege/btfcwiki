---
title: Let's talk about the economy and a democratic problem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zGOmbmP7m-0) |
| Published | 2022/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the perception problem of the Democratic Party being seen as elitist.
- Mentions the Democratic Party's failure to make their case to working-class individuals.
- Points out how the Democratic Party didn't explain the economic downturn that started before the pandemic.
- Indicates that the economic turbulence began in August 2019 due to the trade war initiated by Trump.
- Explains how tariffs raised import prices, impacting consumers.
- States that the recession was predicted before the pandemic, specifically on August 14th, 2019.
- Suggests that the pandemic prolonged the economic instability because the country prioritized the economy over public health.
- Emphasizes that the Democratic Party should address and explain these economic issues to the public.
- Notes that presidents typically don't control the economy but acknowledges Trump's role in triggering economic challenges through tariffs.
- Stresses the importance of understanding the supply chain issues that predated the pandemic and affected the recession timeline.

### Quotes

- "We're just commoners and we can't understand it."
- "The Democratic Party could be out there explaining this."
- "It's not something that most Americans can't grasp."
- "That's one of the biggest problems with the Democratic Party."
- "It's just a thought."

### Oneliner

Beau talks about the Democratic Party's elitist image and failure to explain the economic downturn that started before the pandemic, focusing on Trump's trade war and supply chain issues.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Share information about the economic issues discussed in the transcript (suggested).
- Educate others about the impact of policies on the economy (implied).
- Advocate for transparency and clarity in political messaging (implied).

### Whats missing in summary

The full transcript provides detailed insights into the economic challenges faced by the U.S., including the impact of Trump's trade war and the Democratic Party's communication issues.

### Tags

#DemocraticParty #Economy #TradeWar #Trump #Recession #SupplyChain


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a snapshot of the U.S. economy
and a image problem when it comes to the Democratic Party.
Something about the way they're perceived.
And the reason they're perceived this way is because at times they work really hard to be perceived this way.
For a lot of people, particularly those who look and sound like me,
they're seen as incredibly just elitist.
And that perception exists because there are a lot of times when the Democratic Party is just like,
they won't understand it. So they don't make the case they should.
They don't try to educate or explain anything to the American people
when you're talking about working class folk.
We're just commoners and we can't understand it.
A prime example of this is, you know, the news.
All of the economists now, they said, well, 100 percent, we are definitely, we're heading into a recession.
And the Democratic Party just throws their hands up and says,
well, that's it, that's it for the midterms.
You know, we're heading into a recession.
And they don't make any attempt to explain what happened when it's really easy to.
And I know there's a whole bunch of Republicans right now that are watching this are going, he's going to blame Trump.
Just wait. One of the interesting things about this channel, because we publish every day,
there's a snapshot every day of the news.
And because of that, there's a record of events.
When did the economic turbulence start?
Did it start when Biden took office?
No, of course not.
Most people watching this are going to say, well, it started because of, you know, the whole public health thing.
And you could be forgiven for saying that, but that's not true either.
That's not what happened.
It started before the pandemic.
Started in August.
There was a trade war.
Remember that whole thing with Trump and the tariffs and all of that.
And what do tariffs do?
They, you know, raise the price on imports.
Who pays that extra cost?
You do.
Things start to cost more.
Sound familiar?
Right?
It started back then.
It started in August.
And we knew we were going into a recession in August of 2019.
Specifically, August 14th of 2019.
We knew it was going to happen.
Then the pandemic hit.
And because the pandemic hit, it gave Trump cover politically.
The economic downturn that was occurring, well, they just blamed it all on the pandemic.
But see, we didn't close up.
We didn't try to shorten the length of the pandemic.
We, as a country, decided to sacrifice our friends and family on the altar of the stock market
to keep the economy going.
But what really happened was the economic activity was depressed
because some people were staying home.
And therefore, it just prolonged how long that economic instability occurred.
Just made it last longer.
It didn't help the economy.
If we had shut down and stopped transmission, we would have been able to get back on track sooner.
All of this is talked about in various videos, but it's that August 14th one that really matters.
Because in that video, we lay out the fact we're going into a recession.
Here are the indicators.
But more importantly, in that video, and I'll have it down below,
not just do we say we're going into a recession,
it's going to take a couple of years to get here, and it's going to be blamed on the next president.
It's in the video from 2019, from before the pandemic.
We're not ignorant folk.
This is something the Democratic Party needs to be talking about.
This is when it started.
Trump's trade war triggered this.
And I am somebody who is very well known for saying that presidents don't really control the economy.
That's not what they do.
The economic growth, that's normally not up to the president.
You know, they can engage in policies that help or hurt,
but it's very rare for a president to enact something that does a whole bunch of good
or does a whole bunch of bad for the economy.
But Trump managed to do it.
He did it with the tariffs, which were bad.
We knew this was coming.
The pandemic slowed it.
It's actually slowed this process because the federal government put out a bunch of cash
trying to keep the economy flowing.
So it slowed the process.
But we knew it was going to happen,
and we knew that it was probably going to take so long
that the next president was going to be blamed for it.
It's in the video.
The only thing that's worth noting is that I thought it was going to occur before the two-year mark
because the trade wars were so disruptive to the supply chain.
And that's something else to remember.
Supply chain issues started before the pandemic with the trade war.
And I thought the recession was going to show up sooner because I didn't know that there was going to be a pandemic
and that the federal government was going to push out a bunch of cash
and that there were going to be things that provided cover for economic downturns.
But we knew then it was going to happen.
The Democratic Party could be out there explaining this.
It's not complicated.
I mean, it kind of is, but not really.
It's not something that most Americans can't grasp.
But right now, there are Democratic strategists who are just giving up
because they don't think it can be explained to us commoners.
That's one of the biggest problems with the Democratic Party.
You can show a timeline.
I'm not the only person who covered it.
You can go back to that same period on a whole bunch of channels.
And there's a whole bunch of people.
Well, that's it.
Bond curve inverted.
We're going into a recession.
It's going to take about two years.
Pandemic made it take a little bit longer.
I'll have the link down below.
Ignore all the advice at the end because we're in a very different situation now.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}