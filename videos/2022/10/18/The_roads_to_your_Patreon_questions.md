---
title: The roads to your Patreon questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UL_dYzBwIX0) |
| Published | 2022/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Answers Patreon members' questions.
- Considers collaborating with Robert Evans.
- Debunks the independent state legislature theory.
- Mentions an ongoing counterintelligence investigation.
- Talks about the lack of security culture in the White House under Trump.
- Addresses the issue of missing government documents.
- Shares thoughts on certain books and theories.
- Talks about climate change and bug-out bag essentials.
- Mentions the potential declassification of classified folders.
- Encourages organizing at the local level to address problems.
- Expresses admiration for the YouTube channel "Crime Pays But Botany Doesn't."
- Recounts a story about Kennedy visiting a Special Forces group.
- Talks about community policing and disaster relief efforts.
- Shares insights on historical figures and foreign policy issues.
- Talks about his accent and persona on YouTube.

### Quotes

- "Context is king."
- "We have progress, but there's an awful lot of pain in the short to midterm."
- "Capacity building, community networking, building that local network."
- "If you organize and build a network of people loosely committed to the same goal, you can address problems."
- "I try to focus on the positive where I can."

### Oneliner

Beau answers Patreon members' questions, debunks theories, encourages community organizing, and shares insights on various topics, all while maintaining a genuine and no-drama style.

### Audience

Community Members

### On-the-ground actions from transcript

- Build a local network to address problems ( suggested, exemplified )
- Contribute through YouTube's thanks button to support disaster relief efforts ( exemplified )

### Whats missing in summary

Insights on specific historical figures, foreign policy issues, and the impact of global dynamics on North America and Europe are best understood by watching the full transcript.

### Tags

#CommunityOrganizing #DebunkingTheories #DisasterRelief #ForeignPolicy #HistoricalInsights #ClimateChange #BugOutBag #BookRecommendations


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk to Patreon members.
This is a Q&A for Patreon.
The questions were gathered over the last three weeks or
month or so.
And we're going to go through and answer them.
If you're watching this and you're not part of Patreon,
I don't like to put any content behind a paywall.
So they saw it first.
but now everybody gets to see it.
OK, so diving right in here.
Have you ever considered collaborating
with Robert Evans?
Yeah, actually, we actually had a brief conversation about it.
That's something we have agreed to in principle.
I am one of those people I like to collaborate with others
when it's going to make the most impact.
So it would be a cause or a topic that would bring both of us together that we can leverage
to do some good or make a really important point. What are your thoughts on the independent state
legislature theory? I have some videos about this that have probably gone out since this question
was asked, it's not real. It relies on a willful misreading of everything. It's not a legitimate
theory. The problem is you have some people on the Supreme Court that have kind of expressed
interest in it, and we don't know why they're interested in it. Some seem to have expressed
support for it, but I'm not sure they're expressing support for the whole theory.
They may just be talking about bits and pieces.
This is something we'll probably find out, because I have a feeling something like that
is going to go to the Supreme Court relatively soon.
Let's see, is the Patreon Discord still active?
Can I get a new link?
Yes, hopefully one of the comments down below has one for you.
If not, I'll put a new one up there.
I would like to learn more about how the former president was able to remove the documents
government property from its secured storage.
Yeah, that's a question.
And believe me, there's an entire counterintelligence review going on right now trying to answer
that exact question.
My guess is that these documents were collected over a long period of time.
And it really stems from a lack of security culture within the White House under Trump.
people who support Trump may say that that's not true, yeah sure that's why he
had to complain about leakers every week. The security culture at the White House
under his administration was non-existent and that would explain how
documents that should have been returned never got returned. Have you read a
Stitch in Time by Andrew Robinson? No, but not because of a lack of interest.
I heard a rumor that there's going to be an audiobook read by him and that just seems
like really cool.
His voice and delivery is what made that character, so I'm kind of waiting for that.
Do you have an opinion on the Strauss and how generational theory and their concept
of turnings and generational archetypes. I think I remember this one, and generally
speaking when you're talking about things like this that try to define historical periods
as cyclical, they're a unique framing device but I don't put a whole lot of stock in them
as a predictive device. They are what amounts to a history horoscope. You can fit things into them.
I don't believe that it's something that can actually be used to predict how things are going
to go in the future. How long do you think it will be before climate refugees in the United States
will begin moving inland in big numbers, 10 years, 15 years. I recently did a video about Miami
and the numbers that are expected from there. I think that was 30 years, but those numbers were
in the millions. Let's see. I've been wondering how the how of all this. Since the dox are tracked,
is it just a pure volume issue? Okay, so not all of these documents would have been tracked.
Some of them are certainly tracked based on the markings. They're probably numbered. Some of them
wouldn't be, and then there is a volume issue. When you're talking about the Trump document case,
you're talking about a lot of paper. What would you suggest for an urban bug out bag?
The same thing that's in everything else.
Food, water, fire, shelter, first aid kit, knife.
You may want different tools, different types of things.
Like in a city, you may actually be
able to plug into something to charge your phone.
Maybe you don't need the battery.
But then you have to worry about blackout.
You have to shape it to your terrain,
but it's those five categories that always need to be there.
Food, water, fire, shelter, first aid kit, knife.
Could classified folders have been decommissioned?
Some of them may be sure, not through Trump's magical declassification process, but some
of them certainly could have been declassified in the meantime.
But that actually doesn't really affect the legality of it because it doesn't have to
do with it being classified, it has to do with it being national defense information.
it was classified, odds are it's national defense information. Do you have a
favorite book? All of them. I have a really hard time getting book
recommendations because I think most texts have something that you can take
away from it and a lot of times it's just getting the right book at the right
time in your life for it to be profound. Let's see, does it matter who paid off
Kevin Osdatz, or is that a red herring? I mean, it matters, but I don't know that it's ground breaking.
What actions can we take to solve some of the problems you bring up in some videos?
Organize. Organize at the local level. If you organize and you build a network of people that
is loosely committed to the same goal, you can leverage that network to address pretty much all
solve the problems in some way.
That's the key element is building that local network.
Let's see.
Have you heard about the YouTube channel, Crime Pays But Botany Doesn't?
Yes.
I love that channel, actually.
I think that both of us have offhandedly referenced the other channel in videos before.
I like the way he brings environmentalism and nature to people without it being a nature
show or just pure environmental lecture in theory.
He's actually out there.
If you haven't watched it, I would.
It's worth tuning in.
he's funny. I remember your story about Kennedy visiting a Special Forces group
to have them essentially reaffirm their oath and promise to fight against any
dictators that come to power. Do you have any other odd stories or conspiracies
that you either believe or wish to be true related to government or power
structures. Tons. Tons. But I'll save those for when they become relevant.
Done an excellent job keeping us up on the water problems happening around the
world. I believe water issues are perhaps the biggest problem we're facing. What
could I be doing locally western North Carolina to better understand what's
happening here. As far as I know, Western North Carolina, I don't think y'all have
an issue yet, but if you're looking for resiliency you might want to go ahead
and start looking into some of the technologies and strategies that are
being used out West because eventually a lot of that's gonna have to be applied
in different locations. As the climate changes there are going to be a lot of
of places that that don't have the the environmental makeup that they do today.
Like where I live is basically it's Alabama now. In, I want to say it's 15
years, it's supposed to be closer to Panama. It will become much more tropical.
Let's see, lots of questions about the documents and how they got out.
Let's see, this comes from an old video or response, can't remember which, but I think
you've mentioned you have a collection of history textbooks, yes.
What made you want to get started and how did you begin collecting them?
I think it actually started with newspaper coverage when I was a kid and seeing what
was being published in other countries.
And I think it probably started with a newspaper from Iraq that went through and talked about
glorious victory that Stam had had over the the coalition forces during the
first Gulf War and I just found that so interesting. And that's probably where it
started and now it's one of those things where if I see one I get it and
sometimes people will send them to me. Let's see. I recently came across a video
on social media warning about air guns being as lethal as regular guns and a
rise in automatic air gun sales, which is an unregulated market. Is this
something that should be making headlines that isn't? Okay, so there are
are some air guns that are lethal.
I don't know of many that are as lethal as a traditional firearm.
And then when you're talking about these, you're generally talking about something that
is cost prohibitive.
It's a lot like actual automatic firearms in the United States.
They're not actually illegal.
They're not banned.
You just have to have a license, and because of limited supply, they're cost prohibitive.
Most people can't buy them legally because of how much they cost, and this is kind of
the same thing.
I don't see this as a huge issue at the moment.
It might be something in the future that people have to worry about.
Any chance you could make or link an overview of how to get involved with StuffPage?
Maybe a wiki.
So, oh, well, y'all would know, because a lot of y'all are in the Discord.
Yeah, a wiki page is something that's kind of in development.
In fact, I'm way behind.
I think everybody's waiting on me at this point.
But I will, I'll try to move that to the front of the list.
Where do all of these crown jewels come from?
Are they blood diamonds, emeralds?
Probably.
I've never looked into it.
I just assumed they were, but I don't
know that I know that for a fact.
Can you do a video on FUD?
Fear, Uncertainty, and Doubt.
Propaganda tactic?
Yeah, I mean, that might be useful.
I'll try to remember that.
I remember in a previous Q&A that you answered a question pertaining to how you've changed
people's political outlooks before implying that you had a specific approach you used.
I'm curious what process method you use for doing so and how many people you've done this
with personally.
So it would depend on the person, but generally I use what I consider to be good teaching
techniques, small bits of digestible information that build on each other and normally lead
them to those pieces of information via the Socratic method.
Is typically how I do it in person, but I'm a big fan of diversity of tactics, so I will
I will also just flat out argue with somebody if I think that's the route it needs to go.
Let's see.
I'm very concerned about a Civil War iron.
I have never owned a gun before, but wonder if I should buy one.
Any advice on what kind?
I have a video that's titled, Buying a Gun Out of Fear of Political Violence.
that. Watch the whole thing. It's long and it's dark. I'll go ahead and
tell you that up front. And then there is actually a follow-up to that video, which
may actually be titled, let's talk about teaching techniques, because the person
that asked the question the first time followed the advice to the letter and
they wound up at the range with an old man and I'm not gonna ruin the twist to
that story, but it's worth watching. Hypothetically speaking, what do you think
regular people could do to prepare if they believe they live in a country at
risk of falling completely into totalitarian fascism? So when you're
talking about something like that, that's something you have to determine your own
level of involvement. For most people, the smart move would be to plan your
route, your family's route, on how to get out. I know a lot of people would have the
image of, or have the desire to stay and do something. That's not for everyone. And that's
not like a tough guy thing, you're not cut out for this type of thing. It takes a certain
personality to be able to do that.
If you can't lie well, leave.
I mean, just start with that.
Let's see.
Is there any truth to Joe Biden signing an executive order to facilitate government-issued
digital currency to replace paper money?
Not that I'm aware of.
I would love to know your take on Haiti
and how much our government has had a role in the chaos there.
A lot, a lot.
Now, this was actually before the latest flare-up.
Now, at time of filming, Haiti's actually
asking for troops to go in and stabilize it.
And I put out a video recently.
The US shouldn't be involved in that, and not with troops
anyway.
I have videos talking about how the United States injected itself into this region of
the world and the chaos that it caused over time.
Do you think there is any chance the Republicans will be successful in blocking student loan
forgiveness?
Any chance?
Yes.
High chance?
I don't think so.
think they're going to push it too hard. I think that they're going to put up a
performative fight and then kind of let it go from there. Let's see. How do we
amplify the conversation that money and politics is still one of the root causes
of nearly all of our issues in the United States? I think people know that
at this point. I don't think that this is something that people have to be told. I
I think we have to get to the point where these are the steps to get it out.
These are the steps to get money out of politics.
These are the steps for your average person to stand a chance as a candidate again.
That type of stuff.
I think on that front, for once, we're actually past the education phase.
I think we can actually start offering solutions.
Let's see, okay, that's time sensitive and expired.
Talk about options for changing the presidential pardon process.
Some ideas.
No person can pardon themselves.
All persons being pardoned shall be named.
No group pardons.
Only people charged, indicted, convicted for a crime can be pardoned.
No preemptive pardons.
Okay, so all of this may seem like good ideas. Here's the rub with this, the president's pardon
authority is spelled out in the Constitution, and it is super broad. It can only be reigned in
via a constitutional amendment. And the idea of getting a bunch of House of
Representatives members and members of the Senate to curtail the president's
ability to pardon them in the future seems hard. That doesn't seem like
something that's gonna occur. Let's see. So what I'm realizing as I scroll through
these is that when I put up one of these I need to get to them faster because a
lot of these questions were time-sensitive. Let's see, outside the
obvious political fallout, what do you think are some of the effects of the
war in Ukraine that we might see in the next few years.
If it keeps going the way it is, I would expect to see Ukraine emerge as a major power in
Europe, as a really big power in Europe.
I would also expect for Russia to have to work to become reintegrated into the international
system. And that may take some time. Everyone knows vote, but what are some
other activities people can do to try to turn the tide of authoritarianism? I have
a video on types of civic engagement. All of those. All of those are applicable
in some way and everybody has a skill that they can use and sometimes it's
capacity building you know which is what I talk about so much y'all probably get
you know tired of hearing about it when I talk about community networks there's
just normal advocacy there's demonstrations petitions there's there's
all kinds of things that can be done and they don't they don't all have to be
something openly indirect. Can you go more into your respect for the founding fathers
and compare and contrast that with some of the negative things that I've heard? For example,
the bunch of rich white men who didn't want to pay taxes narrative. You know, that part
isn't even like the bad part to me. I mean there's much worse narratives out there. When it comes to
the Founding Fathers, there's one I respect and that's Thomas Paine. The others I see as a product
of their times and they did horrible, horrible things. Many of them knew they were wrong when
they were doing them. There are a whole lot of Founding Fathers who were very open about the
fact that slavery was, quote, a hideous blot, but they still engaged in it. It isn't about
respect for these people. I am not somebody who likes the idea of American mythology.
These people, they were people that were flawed, they were messed up, they did horrible stuff.
don't deserve to be worshipped or put up on a pedestal. However, there is a... when
you look at what they accomplished as a totality, as not... I'm not talking about
their individual actions, I'm talking about the founding documents of this
country. While also flawed and also full of issues, they laid out the promises.
that they provided a framework to to a better future. Those ideas of you know
all men being created equal, unalienable rights and all of this stuff, that's what
I respect. Not the people who who put them together. Mike, there are not many
founding fathers that I would want in my house. Thomas Paine being the real only
notable exception. And it's worth noting that by the end of it most of the
founding fathers didn't like him.
Let's see. Let's see. I'd love to hear more about your native ancestry. Could
you possibly talk about how you're having that influence in your life shaped
who you are today? I remember hearing you mention that being the white cousin that
to talk to the cops. As someone who is also a light-skinned native, I think it would be
cool for people to realize that we come in all shapes, sizes, and colors. Yeah, so if you don't
know, I'm native and not in like an abstract way. Like, no, I'm a tribal member. I don't look it,
you know, my grandma married an Irish guy, and here I am.
Her brothers and sisters didn't. My cousins
look native, and there were quite a few times when we were
younger that the cops would roll up,
and I noticed pretty early on that if I talked to them,
everything was fine. But if they talked to them, there was a very different
attitude that might I didn't really I don't know that I put it together at the
time but that might have been one of my first experiences where I'm like wow I'm
really getting treated differently here based on this. I think I think the other
part of it that really plays into who I am and my development is storytelling. I
I have uncles that the way I talk is very closely related to the way they do, and the
way they try to put messages into normal stories.
Let's see.
in Ukraine, deep in Russian-held territory, you've mentioned partisans left behind to cause troubles.
Are these attacks the type of attacks that you mentioned? Yeah. Now, it's been a month and I
think that this is being covered a little bit more widely. In fact, I mentioned it today in a video.
But yes, partisan activity in Russian-occupied areas is becoming more pronounced at times
and specific regions.
In the international poker game, China hasn't fought a war since the 1970s.
Do you think they need a military victory?
Their foreign policy doesn't really rely on their military as much.
I know that's hard to believe based on the way the US media talks about their military,
And I think that their military is more of a deterrent against the United States.
That's how they view it.
And I don't think that they have any intention in the near term of using their military to
further their foreign policy aims with the exception of maybe Taiwan.
What is your analysis on the constitutional referendum in Chile, and how it plays out
on the world stage?
I don't know enough about it.
This is the problem with doing these, like, you know, with no time to research.
I know nothing about it.
I couldn't give you an opinion, you know, to save my life.
Let's see.
How do you keep up with all of the events?
I mean, besides multiple whiteboards.
The answer is multiple whiteboards
and a lot of reading.
That's really what it is.
What I really want to know is, when is the road show
happening?
I want to meet all my bo peeps.
It is definitely happening.
For those that don't know who may not have been around back then, we were going on the
road like we were going to go to different towns and meet with people, hopefully help
set up some community networks along the way, you know, just travel the country doing good
and all of this stuff.
And then the world stopped because of the pandemic.
And during that time, that's actually why this channel was launched.
And during that time, we wound up with projects that we put into motion to kind of fill the
the gap because we didn't know how long that was going to last and we now we're
kind of starting to finally wrap those projects up. Once all of that is done we
will look back at getting back on the road and shifting gears again.
How do you think threats of civil war will play out? Sometimes I'm in a
bring it all on frame of mind, other times I think it will play out around us
without really touching us.
I don't foresee the type of civil conflict that a lot of people are envisioning.
I think if it goes hot, it would be low intensity and it would be a campaign that is just, it's
just constant T word.
what it would end up being.
What we have seen is that as motivated as they were, as the far right was around January
6th, they didn't have the staying power numbers that were really committed to it, which is
good.
Most people saw through the rhetoric or they understand that that's not good for anybody.
That kind of conflict isn't good for anybody.
There are other ways to achieve change that don't involve that.
I'm sure other people will assign different motives to it, but at the end of it, even
with all of the rhetoric and a seeming sanctioning by the chief executive, it really wasn't there,
is good. Let's see. Thoughts regarding affordable housing in cities. So I don't
know a lot about this. You know, where I live, affordable housing is still a thing.
Like, you can find affordable housing in my little corner of the world. In
In fact, when I have friends that come in from LA or whatever, and they find out that
what they pay in rent for their one or two bedroom apartment would buy a five bedroom
house with 40 acres here, they lose their mind.
This isn't something that I have a lot of knowledge about.
It seems that because of the high demand and population density that the prices have continued
to go up and you have a lot of people who are using their second home to pay their mortgage
and to make a profit rather than just accept that they're gaining equity in the home.
And that probably contributes to it a lot as well.
Let's see.
What is the GOP's end game with introducing a 15-week ban?
I have no clue.
You know, are they trying to stake out a middle?
I don't think that many people have a middle here, you know.
This is, we want human rights.
will give you some? No, I don't think, whatever their strategy is, I don't think it's going
to work because, and I've talked to other people about this, nobody can really pin down
what they're actually trying to accomplish with this.
Can we see your Trump investigation whiteboard? No. And that's not like me trying to shield
secrets, I write things on there in various colors. Some of the colors are things that
are theories. I try to be very careful about putting stuff out there that isn't confirmed
or at least incredibly likely. So that's why I hide those.
Just like Putin wanted to destabilize the West and ended up doing the opposite, Republicans
struck down Roe versus Wade in order to control people and are ending up causing a blue wave.
Is this comparable?
Is there a name for this phenomenon?
Blowback.
I don't know that there's a name for it, but in this case,
it's the same cause, believing your own propaganda.
What happens when you're trying to change people's minds?
You put out propaganda.
And one of the main tactics is bandwagon.
Everybody's doing it.
That's why when companies are trying to sell you stuff,
there's a party going on, and everybody's
enjoying the product.
Same type of thing.
The problem is that over time, the group putting out
that propaganda actually starts to believe that everybody
really does believe the way they do.
And then they act on that.
And that's when they realize that their propaganda was,
in fact, propaganda.
So I don't know that there's a term for it.
Something, something on your own supply.
Let's see.
I've read through all the comments,
but I suspect mine will be echoed.
What's the best way we can be mobilized,
by which I mean coordinated actions spring about chains?
Yeah, I got to go back to capacity building, community
networking, building that local network, finding people
in your region or area that share similar values
and want roughly the same thing.
You don't have to agree on everything.
You can disagree on a lot as long
as you're moving in the same direction.
Think back to the bus analogy and start there.
That really is the building block.
I dislike that I constantly say that, but that's where it all starts.
I'm interested in your take on the dissent that is currently being reported in Russia.
And given that this was a month ago, I bet you're really interested in it now because
it's gone from little protests to hundreds of thousands of people fleeing the country.
This is definitely having a destabilizing effect on Putin.
I recently read some stuff about small businesses in Russia that are now having trouble finding
employees because of the mobilization, which is going to hit their economy even harder,
and the sanctions are beginning to show.
So it is destabilizing.
or not we're going to have a Swan Lake moment. We don't know yet, but there are a lot of factors
that could lead to one. You know, Biden recently tried to provide that off-ramp and basically was
like, hey, just lie to the Russian people and say you won and pull out, you know. And I mean,
that would be the smart move for Putin. His information apparatus is pretty good. He could
convince the local populace that they had achieved some of the goals that they wanted and now it's
up to Ukraine to stand on its own and grow into a proper country or whatever they're going to sell
them. If he doesn't do that and continues to go down the road that he's on, I don't know how he
ends up staying in power when this is all over.
Let's see.
We have a European war impacting oil, gas, grain, and other foodstuffs production, and
a potential Eurozone energy alert crisis of pricing.
With global inflation, prices, goods, foodstuffs, China repeated lockdowns.
Okay, here's the question.
What do you make of all of these various dynamics hitting globally and the financial, political,
and economic impacts for the last part of 2022 and into 2023 for North America and Europe
in particular?
One of the big issues right now is that China is coming back online and they're getting
to full production.
However, oil production, there's not a lot of spare capacity.
Because the world gets running again and people try to make up lost time, they're using more
resources and the system that is built, the infrastructure that exists, isn't capable
of supporting the demand for those resources, which then causes more supply chain issues
and we get a snowball effect.
This is something, the economic ups and downs that we have been experiencing, they're going
to continue.
I would say for at least the first half of 2023.
Is the Bo persona still necessary?
That's funny.
So if you don't know, Bo is a nickname.
And there's a lot of confusion about different things.
So for a long time I hid my accent and there are people who think that this isn't how
I talk because they knew me before when I constantly spoke in a non-regional accent
and did everything I could to enunciate very clearly so nobody would mistake me for a dumb
redneck and because of that there it's kind of led into the idea that like this
is a shtick it's not I'm I am from the south I yeah I live in the middle of
nowhere with with horses and yeah I am exactly as redneck as I seem so it's
It's not really a persona in that regard.
There are times when I do play up my hillbilliness to prove a point, but I think that the time
of it being really a persona was those first few videos.
If you go back and you listen, you can hear me really playing up my accent.
As you go on, you hear it soften to what you hear now, and that right there is your indication
of it kind of leveling off and moving out of, you know, the persona.
Let's see.
Traditionally, corporations and big business have been part and parcel to the Republican Party,
particularly in the way of finance. Is it just me?
or have the party leaders, particularly but not limited to DeSantis, been turning their
backs on corporate backers in a big way, and do you think this will affect future funding
by big business to the GOP?"
Yeah.
Yes.
The Republican Party is constantly biting the hand that feeds it, and they will end
up paying for it in the long term.
Right now, you have a lot of people who believe they are ideologues, and they think that those
businesses will carry them.
The problem is, businesses have one job, and that's to turn a profit.
If your policies are bad for business, or more importantly, them supporting a candidate
who supports, say, I don't know, hypothetically taking away people's rights, if that's bad
for their business, they won't do it.
And people in the Republican Party keep talking about this woke business or this is a conservative
business or whatever.
Most people are not ideologically motivated to that degree.
Businesses certainly aren't.
When you're talking about these big corporations, they have a responsibility to the shareholders
to turn a profit.
That's it.
that means support the Republican Party, support the Republican Party. If that
means support the Democratic Party, support the Democratic Party. If that
means donate to both of them so you had your bets, that's what they're gonna do.
But if donating to the Republican Party becomes a liability for the company
Could you do us all as t-shirt lovers a solid
and take a step back at the end of your videos showing off
more of the graphic?
So the reason it's framed this way
is because any other framing looks weird.
And I actually tried the step back thing,
but it's actually even stranger.
I've tried to figure out a solution to this.
I haven't come up with one yet.
But I'm still working on it.
Let's see.
About four years ago, you shared a story
about a Special Forces group.
I know it's just a legend, but with everything going down,
the past few years, including coup attempt,
if you've seen or heard any chatter about some legends
being truer than others.
I would just note that almost every single military person
that actively stood in the way during that time period had a green beret.
Go through the list of the major players who opposed Trump's actions.
You will see some unique things. And again, it's just a legend, it's just a story, but
I found it interesting.
How many t-shirts do you have? More or less than 15 boxes of documents. Probably about the same.
You've mentioned a local businessman friend you admire who runs his business like a socialist.
I own a hardware store in a rural, relatively conservative area. I'd love your thoughts
on how small businesses can support community building and if you know of ways I can connect
with like-minded folks. I have some ideas. I'm going to turn that into a video because
yeah and I'll go ask him. He's still one of the most successful people I know and I will
never forget the whole conversation about the minimum wage. You still going to give
them a bonus now that the minimum wage is $10. None of my people make less than whatever
it was, like $19 an hour or something. If you haven't watched that video, it's worth
watching. Let's see. Why does Christian nationalism sound like the American version of what was
in Afghanistan? Because it is. What's the largest caliber armament you've ever operated?
40 millimeter, I would guess.
Let's see.
You said that 1-6 would have played out differently if Trump led his mob.
Did the Secret Service actually save democracy with the no-go to the Capitol maneuver?
Um, maybe.
Maybe I don't think they meant to.
I mean, that's...
That was not their intention when it comes to not letting him go.
That didn't factor into it at all.
But it's one of those twist of fate things.
It does seem as though people who were pretty supportive of him actually stopped him from
going, and in many ways undermined his plan.
Let's see, what's that picture in the background next to the YouTube award, a painting of a
person?
That was sent by one of y'all.
That was sent by one of y'all, and I like it, so it's up there.
Let's see.
How do you separate who you are as a person in terms of morals, values, beliefs from who
you are on the main YouTube channel?
For me, it's a matter of realizing that where I'm at philosophically isn't where most people
are, and I try to provide what I provide on the channel, news coverage, political commentary.
And while my values and philosophy certainly enter into it, I don't try to convert people
to my philosophy.
I'm one of those people who is, I'm confident I'm right.
And because of that, I don't feel like I need to convince anybody.
I feel like if people have access to the information, they'll come to the same conclusion.
Let's see.
When you retire your t-shirts, are you going to make a quilt out of them?
what I'm doing with my content creator merch shirts when the graphics start to
wear. I mean, I'd never thought of it that sounds fun.
What's your YouTube channel workday like, and how many
folks does it take to create the videos?
So how many folks does it take to create
videos. The ones you watch every day? Me. Me. Just me. To create the video. As far
as research and filming these types of videos, it's just me. I am rabidly
defensive of content. However, as far as the day-to-day running of all of the
stuff that goes on with a YouTube channel, like taking this video, this video will end
up as a podcast.
I know that that's going to occur, I have no idea how it happens, I don't know the
process in removing the video and just leaving the audio and getting it up there, I don't
know when it's going to happen.
When we started building the team for this channel, when it started to take off, my thought
process was bring on people to do everything except for what y'all want me to do, which
is the content.
Now as far as my average workday, it's never the same.
Right now, as an example, I'm recording this, it's Friday.
I am already through Sunday as far as videos that are recorded to go out on the main channel.
I don't know when this one's going to go out.
So theoretically, I may not record for the next two days.
And then I might record for like 60 hours straight.
It just, it depends on how the news breaks.
And how many folks does it take to create the videos?
Let's do it a different way.
how many folks does it take to actually run this channel?
Let's see, one, two, three, four, five, me,
and we're actually looking for one more person.
So it will be a total of six very soon.
Let me recount that, one, two, three, four.
Yes. That's right. Sorry here. What is the second channel? The Roads with Boat. Don't worry. You're going to get a link
in Patreon. So you'll find it.
How could we get you to run for POTUS 2024? That's not, that's no, no. I don't even want to be like a
homeowners association president.  Not all religious communities are opposed to marriages between same-sex consenting
adults.
So should we allow some religious communities to tell us how to talk about unions?
Why? That's interesting. I'm not sure I get the... I'll have to come back to that.
Let's see.
Should media outlets provide context to why a celebrity or politician is wrong or does
something unprecedented by using history or previously untested law?
I believe so.
Context is king.
If you look at the playlists on this channel, there is one that is titled Context.
It is by far the largest playlist.
Context is just sorely lacking in modern media coverage, and yeah, I do think that providing
historical context, providing legal context, providing even just factual context, because
a lot of celebrities say things or politicians say things that are just objectively false.
I think that that would be incredibly beneficial.
Have you watched lower decks?
I have not watched Lower Decks yet.
Let's see.
So I'm scrolling through these, and a lot of them
are time-sensitive about things that have already
occurred and conclusions.
Or they've already had whole videos
made about them.
How did Graham and McConnell sleep at night?
I don't know.
Probably on a mattress, stuffed with $100 bills.
Please expand on your Irish heritage.
Just did.
Or James Connolly.
I'll do a video about that.
I could do a video about Connelly.
When you are doing hurricane disaster relief, are you coordinating with other groups like
the various Cajun Navy ones?
It depends on the disaster.
It depends on the hurricane.
After I want to say Harvey, I think it was Harvey, we didn't coordinate with the Cajun
Navy.
They're people that they told to do things.
They did everything and we just helped.
And sometimes we don't interact with them at all.
Sometimes we interact with groups that are present on the ground, local groups of various
kinds.
And sometimes it's just us.
So it's very fluid.
Can you expand on your optimism?
I've heard you say that in the long run, we have progress, but there's an awful lot
of pain in the short to midterm.
Yeah, no lies detected.
That's true.
That is true.
When you talk about the meaning of life, people assign whatever meaning they want.
I think the meaning is to leave this place better than you found it.
So even though there is a lot of pain and suffering in the short term, if we're still
moving in the right direction, I consider that a win.
And I tend to stay optimistic because, I mean, what's the alternative?
I mean, it doesn't change the fact that we still have to fight.
just you're in a bad mood and have to fight so I try to focus on the positive
where I can. Let's see I don't know what the other channel is I don't use Twitter
how do I participate in your next fundraiser I appreciate your work
delivering supplies to and hurricane people okay so now on YouTube we have a
thanks button you can click that and it will allow you to put in put in like a
a dollar amount or whatever, and you can contribute that way.
Those funds, it's cool because YouTube actually
marks it as a different kind of revenue, which, I mean,
that's basically, right now what we're going to use that for
is to rebuild the little buffer that we had built so we could
just go out and buy a bunch of generators and stuff
that we needed for Ian.
But basically, we're going to try to use all of that money,
or at least as much as we can, to do good stuff.
Let's see.
Been following you for a while.
You're cut through the BS and leave out
the judgmental rhetoric.
Style has a genuine no drama quality.
that in and of itself speaks to the credibility of your content.
Looking forward to participating.
Well, thank you.
Oh, and back to the other question.
I don't know what the other channel is.
It's called The Roads with Bo.
And there is some documentary content on there.
There's a lot of long format stuff.
And there's a whole bunch of interesting stuff
that will be showing up on there soon.
I have a complicated foreign policy question for you.
Why is Poland so loud about giving Ukraine weapons,
but Romanian and Bulgarian governments
aren't saying anything, but they're good.
OK.
So different countries have various foreign policies.
Some countries have a history of being targeted.
Some countries have a history of never providing military aid.
Some countries maybe have a historical issue
they would really like to tell Russia that they were helping.
The foreign policies of various countries varies because of their historical experience,
and what they are willing to claim or take credit for is also up in the air based on
that.
A lot of countries engage in covert actions.
It's kind of only the US that is afterward is like, yeah, we did that.
What are you going to do about it?
I mean we're arrogant Americans. A lot of countries would avoid that kind of
publicity and I think that's probably what they're doing. Okay so that wraps
this up. I skipped over the ones that were time sensitive or there was already
a video about it or the situation had just already resolved itself. I feel like
I got everybody. If I missed it, I'm sorry, it wasn't intentional. So this will go out on Patreon,
y'all will be able to watch it for a few days, and then it will go public because we still have that
thing about putting like real content behind a paywall. So I really, I'll try to do stuff like
like this more often and I really thank y'all for the support. I mean those
people, you know we're talking about the number of people involved, the Patreon is
is one of the things that allows us the stability to make sure we have the
people in place to make everything happen. So I really do appreciate all the
support.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}