---
title: Let's talk about a new parenting approach....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VvM3jUf00Q4) |
| Published | 2022/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a new method of raising children focused on developing skills for the future, popular in conservative circles.
- Parents allow kids to get away with deceptive actions to nurture skills like deception and manipulation for later success.
- Examples include children hiding a cookie jar, creating fake permission slips, and mocking others without consequences.
- Parents prioritize fostering these skills over holding children accountable for their actions.
- Suggests that parents holding their children to a higher standard than political leaders may influence children's political views.

### Quotes

- "You hold a toddler more accountable for their actions than you do the leader of your political party."
- "If you're wondering why your children may have a different view when it comes to politics now..."
- "Parents need to have that ability if they want to succeed."
- "It's the only way they will ever be able to be the Republican nominee for president."

### Oneliner

Introducing an unorthodox method of raising children that prioritizes nurturing skills over accountability, potentially impacting political views.

### Audience

Conservative parents

### On-the-ground actions from transcript

- Reassess parenting strategies to balance skill development and accountability (suggested)
- Encourage open political discourse with children to understand differing views (suggested)

### Whats missing in summary

The tone and nuances of Beau's delivery can be best understood by watching the full transcript.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a new method
of raising children.
It's a new approach.
The general idea behind it is to make sure
that children have the skills they
need to succeed in the future, which
sounds like a normal method of raising children.
This is a little bit, let's just say, unorthodox,
because it isn't holding children
accountable for actions that are generally
seen as bad in children.
This particular method of raising children
has become popular in conservative circles.
So what are the skills?
The idea is that as children get older,
they need skills that may not be ideal when they're kids,
but you don't want to deny the development of that skill.
OK, so as an example, little Jimmy,
he doesn't get his hand caught in the cookie jar.
He takes the whole cookie jar, hides it in his room.
Parents ask, Jimmy, do you have the cookie jar?
No.
They know that he has the cookie jar, so they look.
They search his room, and they find the cookie jar.
And then little Jimmy's like, I'm allowed to have those.
And even though they know that this isn't true,
they don't hold him accountable for it,
because they want him to have that skill later in life.
Another example would be little Susie and little Julie.
They want to go to a concert, but their parents
won't let them go.
And it's during school.
They'd have to leave when school was still in session.
So they create two sets of documents.
One, like a fake permission slip to give their parents
to make it seem like they're going somewhere else.
And then one, a note for the school from the parents
saying that it's OK that they missed that day,
so they cover their bases.
And the parents think that they understand what happened,
but at this point, they don't really know.
But they don't even look into it,
because they need to have that skill set.
They need to have that ability if they want to succeed.
Another example would be a child mocking somebody
for immutable characteristics that they might have.
They don't get punished for that,
because these skills, children having these skills,
it's the only way they will ever be able to be the Republican
nominee for president.
If you're wondering why your children may
have a different view when it comes to politics now,
if you're a conservative parent and your children
have very different political views from you,
it might be because they see that you hold them
to a higher standard than you hold the president.
In case you missed it, that's the tax thing up in New York.
That's the permission slips and the cookie jars,
the classified thing, and making fun of people.
Well, that's just Trump.
That may have something to do with it.
You hold a toddler more accountable for their actions
than you do the leader of your political party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}