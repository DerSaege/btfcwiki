---
title: Let's talk about the next 3rd ranked military....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZZ1vKbaA9wc) |
| Published | 2022/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau McGann says:

- Russia lost its top three military power status due to poor performance in Ukraine and degrading capabilities.
- The United States holds the number one spot in military power, with China at number two.
- The debate is around who will take the third spot in military power rankings.
- Traditional methods of predicting military effectiveness, like troop and tank numbers, are outdated.
- Modern military power comes from training, technology, financial resources, and the will to project force.
- Japan, historically reliant on the US for defense, has recently increased its defense spending and capabilities.
- Japan's focus on air and blue water capabilities positions them to be a significant player in the Pacific.
- Geography plays a significant role in a country's ability to project military power.
- Japan's demonstration of actual military capability will change how they are viewed internationally.
- Japan aims to be a partner with the US, leading Pacific nations in the future.

### Quotes

- "The face of war has changed."
- "Training, technology, financial resources, and the will, the desire to force project and be a player."
- "Japan becomes a lead nation with the backing of the United States rallying other Pacific nations."
- "They're going to be a force to be reckoned with."
- "They'll want to be able to stand on their own."

### Oneliner

Russia's fall from the top three military powers opens up space for Japan's rise, driven by increased defense spending and a focus on modern capabilities.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Monitor Japan's defense spending and military capabilities (implied)
- Stay informed about geopolitical developments in the Pacific region (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the shifting landscape of military power, particularly focusing on Japan's emergence as a potential top contender in defense capabilities. Watching the full video can provide additional insights into the nuances of international relations and military strategy.

### Tags

#MilitaryPower #Geopolitics #Japan #US #DefenseSpending


## Transcript
Well, howdy there, internet people.
It's Beau McGann.
So today, we are going to talk about military might.
What military power comes from today,
and who's going to take that third-ranked spot?
Because recently, I mentioned how Russia is,
they've fallen out of the top three
when it comes to military power.
And people have asked who is expected to take that spot,
because I mentioned there's debate over it.
Russia fell out of it because of their poor performance
in Ukraine and the losses sustained there,
the constant degrading of their military capability.
So number one would be the United States.
Number two would be China.
It's that third spot that has people debating.
Some people are looking at it like sports teams' rankings,
and they're looking at raw numbers, tank and troop
numbers.
Yeah, how did that work out when it
came to predicting the effectiveness of Russia's
military in Ukraine?
That's not what military power comes from anymore.
The face of war has changed.
What matters today, training, technology,
the financial resources to maintain that technology,
and the will, the desire to force project and be a player.
That's where military power comes from today.
There are a bunch of countries that could do this.
Most don't have the desire.
Then we look at Japan.
Since the end of World War II, Japan,
well, it started relying on the United States for defense.
But it didn't become a country that just
did whatever the US said.
They also maintained economic ties with China.
That situation, the United States
situation throughout the Cold War, it was pretty static.
That's what was happening.
That's how it was going to play out.
And nothing was changing.
Then around 2015, the Japanese government
decided it wanted to be a player.
It started committing more to defense.
And if I'm not mistaken, currently, they
are slated to be, or maybe they've already
become, the third ranked country when
it comes to defense spending.
And they're putting that money where it matters,
force projection.
They are putting that money into the air.
They are putting that money into blue water capabilities.
They're spending it as if they want
to be a very capable military.
That seems like the most likely answer.
And then when you get to the international poker game
where everybody's cheating, what also matters is geography.
If Argentina decided to build a top ranked military,
it really wouldn't matter because the geography there
doesn't put them in a position to be a player.
Japan is.
They are very much in a position to be
a major player in the Pacific.
So not just do they have the military power,
they will be presented with opportunities
to showcase it, which once that power is perceived,
well, it's real.
There's perceived capability and actual capability.
Once they demonstrate that they have an actual capability,
once, they will be viewed in a very different way.
It's also worth noting that Japan hasn't broken away
from the United States.
It's not like they're separating.
More like they want to become a partner.
And I can see a situation in which in the Pacific,
Japan becomes a lead nation with the backing of the United States
rallying other Pacific nations.
That's my guess.
Now, we're talking probably three to five years out
for this to really come into its own.
But I think once they establish the capabilities they're
talking about establishing, yeah,
they're going to be a force to be reckoned with.
And I think they will definitely be more powerful than Russia.
And I think that they will have more of a desire
to participate on the international scene
than a lot of other countries that have strong military capabilities as it is.
They'll want to be able to stand on their own.
They have a lot of economic interests.
That seems to be the most likely new contender.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}