---
title: Let's talk about what a Polk County incident can tell us about teachers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iCRBwh-7n3M) |
| Published | 2022/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A blue on blue moment occurred in Polk County, Florida involving the Polk County Sheriff's Department.
- Deputies were looking to apprehend someone with a felony failure to appear.
- The deputies conducted a slow and deliberate clear of a trailer where the suspect was located.
- The suspect raised a BB gun, leading to officers inside firing and a cop outside being hit and killed.
- Despite initial assumptions of shadiness due to the reputation of the Polk County Sheriff's Department, it seems they followed proper procedures.
- There are speculations about what may have gone wrong in the incident, but no clear evidence of gross negligence.
- Beau contrasts the controlled scenario of this incident with the chaotic nature of arming teachers in schools.
- He stresses that arming teachers is not a solution, as controlling rounds in enclosed spaces can be extremely challenging.
- Beau points out that this incident underscores the need for more proactive measures rather than reactive band-aid solutions.
- Despite initial reporting suggesting correct procedures were followed, further investigation may reveal more details about what went wrong.

### Quotes

- "Arming teachers isn't a solution. Bullets go through stuff. That isn't a solution. It's a band-aid at best."
- "This type of scenario, something similar to this, will play out before people realize that there need to be more proactive measures involved in this."
- "It doesn't look like anything like that is occurring. This looks like just fog of combat stuff."
- "But I don't think this is going to be a moment where it's very clear that they violated just the basic principles, which is what normally leads to a bad shoot."
- "Y'all have a good day."

### Oneliner

A blue-on-blue incident in Polk County prompts reflections on proper procedures and the limitations of reactive solutions like arming teachers.

### Audience

Law enforcement personnel

### On-the-ground actions from transcript

- Analyze proper procedures and protocols in law enforcement encounters (implied)
- Advocate for proactive measures rather than reactive solutions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of a specific law enforcement incident, discussing proper procedures, the limitations of reactive solutions, and the need for proactive measures in similar scenarios.

### Tags

#LawEnforcement #Procedures #ReactiveSolutions #ArmingTeachers #ProactiveMeasures


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about what
happened in Polk County, Florida with the Polk County Sheriff
Department down there and what we can learn from it,
even though a lot of the questions
aren't really what went down there.
OK, so if you have no idea what I'm talking about,
there was a blue on blue moment down there.
So there are a number of deputies,
and they are looking to pick up somebody who has a felony
failure to appear.
They arrive at a trailer, and somebody's like, yeah,
the person's inside.
One of the deputies takes a position outside,
and then the others, according to reporting,
kind of did a slow and deliberate clear
of the trailer.
They didn't go in, throw on their Oakleys,
and throw in a flashbang.
According to reporting, they did it the right way.
They did the right move.
They move through the trailer slowly.
They find the suspect.
The suspect raises what is later found out
to be a BB gun.
Officers inside fire.
The cop outside is hit and doesn't make it.
So my inbox immediately filled up with cop shot a cop,
and this is obviously shady.
The reason people are saying that is because Polk County
has a reputation for being kind of cowboyish.
Whether or not that reputation is well-earned, I don't know.
I've never dealt with them.
But that's the reputation.
Based on reporting, no.
In fact, they didn't even really do anything wrong.
It looks like time, distance, and cover was applied.
Theoretically, there should be two cops outside,
but that didn't contribute to what occurred.
They did the right move when they went in and cleared.
I mean, at some point, you will be
able to point to a specific moment
and say, this is where it went wrong.
And it could be anything from the cop outside finding
a position that protected him from the possibility of fire
from doors and windows, not really thinking about rounds
coming through a wall.
It could be shot placement from the officers inside.
There's a couple of things that it could be.
But there's no gross negligence here.
This is the main principles that we always talk about
and we go to and say, this is where it went wrong.
They're not at play here.
They did all of that stuff right,
and they still had an incident like this.
So if there's nothing shady and they did everything right
as far as best practices, why am I even talking about it, right?
Because there's something else we can learn from it.
Anytime the subject of arming teachers comes up,
I talk about how hard it is to retain control of your rounds
in an enclosed chaotic space with a bunch of people
running around.
But this, I want to say there are seven total people involved
in this, and it is nowhere near as chaotic
as a classroom or a hallway.
And at least by initial reporting,
they did it all right.
There's nothing that is readily apparent that was just
a huge failure here, and they still had this outcome.
I would suggest that it's probably even more likely
when it's a teacher.
Bullets go through stuff.
That isn't a solution.
It's a band-aid at best.
Arming teachers isn't a solution.
This type of scenario, something similar to this,
will play out before people realize
that there need to be more proactive measures involved
in this.
But to answer the questions, I couldn't find anything
that even remotely hints to something being shady.
I think that came from the idea because the cop outside
had only been on patrol like eight months or something
like that.
So I think there's an assumption that maybe he saw something
that he shouldn't have.
It doesn't look like anything like that is occurring.
This looks like just fog of combat stuff.
And as far as the procedures they used,
it all looks right based on initial reporting.
As footage and stuff like that comes out,
we might be able to pinpoint the moment it went wrong.
But I don't think this is going to be a moment where
it's very clear that they violated
just the basic principles, which is what normally
leads to a bad shoot.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}