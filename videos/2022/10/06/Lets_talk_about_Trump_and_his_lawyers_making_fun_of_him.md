---
title: Let's talk about Trump and his lawyers making fun of him....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZxzV85ED86M) |
| Published | 2022/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent stories portraying Trump as financially struggling, including court battles and a $250 million lawsuit.
- Mentions leaked emails where Trump's lawyers mock his lack of money and financial liquidity issues.
- Points out the potential consequences for Trump if he loses legal battles, such as having to sell his buildings at low prices.
- Talks about Trump's dependence on his New York skyscrapers for wealth and the threat to his business if he loses them.
- Notes that these financial challenges coincide with issues faced by Truth Social and Trump's legal and political setbacks.
- Suggests that Trump's self-perception linked to his wealth could lead to erratic behavior.
- Anticipates Trump appealing for money from supporters, potentially causing rifts among Republican candidates he endorsed.
- Raises the possibility of Trump boasting about his financial status amidst fundraising controversies and limited cash dispersal.
- Speculates that this boasting could fuel animosity between GOP candidates and Trump as they head into the general election.
- Concludes with the potential for internal GOP conflicts due to Trump's financial circumstances impacting his political influence.

### Quotes

- "These stories are coming out at the same time that the truth social venture is running into issues."
- "He described it as an existential threat to his business and his financial well-being."
- "So there's probably going to be a whole bunch of messages and emails sent out to the MAGA faithful saying, you know, give me $45."

### Oneliner

Beau explains Trump's financial struggles, leaked emails mocking his money woes, and potential fallout on GOP candidates.

### Audience

Political observers

### On-the-ground actions from transcript

- Support GOP candidates independent of Trump (implied)
- Stay informed about GOP internal dynamics (implied)

### Whats missing in summary

Insight into how Trump's financial challenges may impact his political influence in the future

### Tags

#Trump #FinancialStruggles #GOP #PoliticalImpact #Fundraising


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Trump, his money,
and why he's probably going to start talking about how much money he has
while simultaneously sending a bunch of erratic text messages and emails
begging for money from your uncle who is going to send him $45.
A number of stories have come out and they're all painting Trump as, well, broke,
for lack of a better word.
The first one, it came about because of a court battle between the January 6th committee
and John Eastman. The committee is trying to get access to his emails.
In a filing, emails got released that basically have some of Trump's campaign lawyers,
at least it appears, that they're making fun of him for not having any money.
One of the quotes says that a lawyer was doing his part to curry favor by lining his empty pockets,
by lining Trump's empty pockets.
This comes out around the same time that Trump biographer Tim O'Brien
was talking about the impacts of the New York suit,
$250 million suit that Trump is looking at.
And he said that because of Trump's lack of financial liquidity
and his habit of accumulating a lot of debt,
that if he was to go toe-to-toe with the state of New York and lose,
well, he would probably have to sell his buildings in New York, his skyscrapers,
which is where his actual wealth is,
and he would have to sell them at fire sale prices
because everybody would know that he's got to get out of them
because he doesn't have real cash anywhere else.
He described it as an existential threat to his business and his financial well-being.
These stories are coming out at the same time that the truth social venture is running into issues
and Trump himself is facing a lot of legal battles
and a lot of political losses and loss of the limelight.
These are all things that Trump kind of defines himself by.
The public perception that he may not have any money
might be something that kind of pushes him over the edge
and causes him to start being a little bit more erratic, if you could imagine.
So there's probably going to be a whole bunch of messages and emails sent out to the MAGA faithful saying,
you know, give me $45 or you won't be part of the MAGA gold club or whatever.
But more importantly, he might start talking about the amount of money he has
and the amount of money he has raised.
Given the limited dispersal of cash from his fundraising endeavors
that he has contributed to those he's endorsed,
this might seed a lot of animosity between Republican candidates
who he pushed through the primary and Trump
because they by now, as they move into the general election,
they've realized that he's a liability.
It may cause a little bit of infighting that the GOP probably doesn't want right now.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}