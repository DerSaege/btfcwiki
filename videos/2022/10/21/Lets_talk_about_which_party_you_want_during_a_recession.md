---
title: Let's talk about which party you want during a recession....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zzFvKxpP_cs) |
| Published | 2022/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Top economists are predicting a recession within the next year, raising concerns about the economy and its impact on the upcoming midterm elections.
- People are questioning how this economic information will benefit them in the context of potential layoffs during a recession.
- Companies no longer follow the trend of laying off newer employees first; instead, they often let go of longer-tenured employees due to higher costs.
- Government intervention to prevent layoffs is unlikely, as companies prioritize profit over job security.
- When considering which party to support in Congress to address layoffs, it's vital to look at their stance on social safety nets and support for those facing financial challenges.
- The Republican and Democratic parties have differing approaches to social safety nets, entitlements, and supporting individuals who have been laid off.
- Individuals are advised to scrutinize party platforms regarding assistance for those in financial distress, as this may influence their voting decisions.
- While the severity of the predicted recession is uncertain, understanding candidates' positions on social safety nets is key to making an informed choice.

### Quotes

- "Companies are cold, unfeeling, profit-driven machines."
- "If this is your concern, if this is something that you're going to send a message about, it's probably something that is weighing pretty heavily on you and is going to be a motivating factor."
- "One party you know won't [help]."

### Oneliner

Top economists predict a recession, raising concerns about layoffs and influencing midterm elections; understanding party positions on social safety nets is key to voting decisions amidst economic uncertainty.

### Audience

Voters

### On-the-ground actions from transcript

- Scrutinize party platforms on social safety nets and support for those facing financial challenges (suggested).
- Make an informed decision based on candidates' positions regarding assistance for individuals in financial distress (implied).

### Whats missing in summary

Detailed analysis of specific policies proposed by each party to address potential layoffs and economic challenges.

### Tags

#Economics #MidtermElections #Recession #SocialSafetyNets #Voting


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk a little bit more
about economics and the upcoming midterm elections.
And we're going to talk about people's concerns
about the latest news regarding the economy
and how people are reacting to it
and how it could influence votes.
So if you missed it, a whole bunch of top economists
are saying, hey, we're definitely going to have a recession like in the next year.
Okay, so we have this message.
I get what you're saying about how the economy went bad, but what good does that information do me?
Biden and Trump aren't running.
Recessions come with layoffs, right?
Who do we vote into Congress to stop people from getting laid off?
I'm new at my job, so I'm probably first to go, right?
It would be nice if there was some kind of government action to stop that.
Okay, so there's two questions there, really.
I'm gonna be first to go, right?
Probably not.
Not necessarily.
That used to be how it worked back when companies at least pretended to care about their employees.
They used to let the newer people go first, and then they would hire them back if they
had the ability.
A lot of companies now have taken a different route.
What they realized is that employees
that have been there longer have gotten raises.
They are more expensive.
So a lot of them will lay off from those
who have been there longer and just give the newer employees
more work for that lower rate of pay
and tell you to be thankful for it.
That's not all companies but that is a lot of them now.
Now as far as the other question, who do you put into Congress to stop layoffs?
It's not a thing.
Companies are cold, unfilling, profit-driven machines.
They have one job and one job only and that is to make money.
doesn't have a lot of power to regulate that aspect of it and to keep people at work.
So in absence of that, you would have to alter your question.
Switch it to, if I do get laid off, which party would I rather have in power?
Would you rather have the Republican party or the Democratic party?
One party is very much pull yourself up by your bootstraps, we're looking at making
social security discretionary, you know, people just need to get back to work, and very much
against entitlements, as they would call them.
And the other party tends to support social safety nets of the sort you would use if you
got laid off.
If this is your primary concern, if this is the thing that's going to decide your vote,
I mean, that seems pretty easy.
You look at the party platforms when it comes to helping people who have been laid off or
who are in financial straits.
One party is very different than the other on this one.
You know, there is that constant talking point.
the parties are the same, but a lot of times when you get down to the issues, they're very
much different, and this is definitely one where they're different.
I would also point out that you have all of these economists saying that this is going
to happen, and yeah, it probably is, but we don't know the severity.
So I wouldn't start panicking just yet.
But when you're looking at your candidates, see what their positions are when it comes
to social safety nets or entitlements depending on which party you're reading, which party
platform.
Because that's probably going to be, if this is your concern, if this is something that
you're going to send a message about, it's probably something that is weighing pretty
heavily on you and is going to be a motivating factor. Look at their
platforms, see who talks about helping those who are in financial straits. And
that way if it does go bad at least that party would have the opportunity to
enact some kind of programs to help because one party you know won't. Anyway
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}