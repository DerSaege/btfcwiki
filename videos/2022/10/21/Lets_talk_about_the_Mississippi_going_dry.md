---
title: Let's talk about the Mississippi going dry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-fY_ZLPhMgQ) |
| Published | 2022/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the current situation with the Mississippi River and addresses comparisons to Lake Mead drought concerns.
- Describes the immense size and vulnerability of the Mississippi River due to its length and the various sources flowing into it.
- Notes the unusual access to islands in the river, like Tower Rock, due to significantly low water levels.
- Mentions record low water levels in Chester, Illinois, and Memphis, impacting shipping and supply chain issues.
- Talks about the drought affecting states feeding into the Mississippi River and the lack of precipitation in the Ohio River Valley.
- States that experts suggest the situation will improve by spring, with expected rain in the Ohio area helping to raise water levels.
- Emphasizes that while the situation with the Mississippi River is concerning, it may not be as dire or prolonged as other water crises in the country.
- Points out that environmental changes like these are clear indicators that action needs to be taken to address climate issues.

### Quotes

- "The planet's not bad at messaging. It's super clear about what's going on."
- "It's another sign. It's another example of major features in the United States saying, hey, this is an issue."

### Oneliner

Beau explains the Mississippi River's low water levels, impacted by drought and climate, urging action to address environmental concerns.

### Audience

Climate activists, Environmentalists, General Public

### On-the-ground actions from transcript

- Monitor and support initiatives addressing climate change impacts on water sources (suggested)
- Stay informed about water conservation practices and advocate for sustainable water management (implied)

### Whats missing in summary

Beau's detailed insights and explanations on the Mississippi River's water levels and the broader implications of climate change on vital water sources.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the Mississippi River
and what's going on with it, and whether or not
it's the same sort of situation as Lake Mead,
because that's really the question that's coming in.
So if you don't know, the Mississippi River's going dry,
cue the music.
For those that don't know,
The Mississippi River is 2,350 miles long for those in pretty much the rest of the world.
That's 3,780ish kilometers.
It's huge.
Because of that, it is more susceptible to impacts from the climate because there are
so many places that run into it.
But before we get into that, go over just a brief overview
of how unique the situation is.
There are islands in the river that are now
accessible by foot, one of which is Tower Rock.
I read something that said you could walk to it now
without getting your feet wet or muddy.
This is a feature that is normally
only accessible by boat.
So yeah, the river has dropped quite a bit.
in Chester, Illinois the gauge read zero, water level of zero. On Monday the water
levels were the lowest on record in Memphis. The levels are low enough to be
impacting shipping which is less than ideal for our ongoing supply chain
issues because the Mississippi River moves a lot of cargo. Okay so is it the
same type of situation. Yes and no. Because the Mississippi has so much
stuff drain into it, the eastern edge of the drought that is impacting Lake Mead
is also impacting states that would normally have water feeding the
Mississippi. So that in and of itself lowers the water levels a little bit. At
the same time upstream in the Ohio River Valley there hasn't been enough
precipitation. So that's lowering it. So yes it has to do with climate, yes it has
a little bit to do with the drought. The difference being the people that I have
talked to have suggested that like worst case by spring everything's kind of back
to normal on this one. It might be slightly lower if the drought out west continues because
of those states that feed the river, but everything will more or less be back to normal when the
snow melts. There should be a bump soon because the Ohio area is expected to get some rain
And that should kind of trickle down.
So it isn't as dire a situation.
It won't go on as long.
But it's another sign.
It's another example of major features in the United States
saying, hey, this is an issue.
Y'all might want to do something about this.
You know, we talk about politicians being bad at messaging.
The planet's not.
It's super clear about what's going on.
And we probably want to do something about it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}