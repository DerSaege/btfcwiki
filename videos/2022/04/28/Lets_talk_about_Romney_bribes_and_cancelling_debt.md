---
title: Let's talk about Romney, bribes, and cancelling debt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=11gF3rcswOo) |
| Published | 2022/04/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes Mitt Romney's jest about forgiving trillions in student loans and other debt as not funny.
- Mentions forgiving medical debt as another significant aspect.
- Suggests that Romney's disconnectedness, having an elevator for his cars, hinders his understanding of income inequality.
- Emphasizes that forgiving student loan debt is not a bribe but a benefit to constituents and the country.
- Advocates for encouraging education as it is vital for democracy and benefits various aspects like the economy and national security.
- Points out that politicians may not want an educated populace to maintain a subservient underclass.
- Expresses disregard for prioritizing banks over addressing the massive wealth gap in the country.

### Quotes

- "Doing things that benefit your constituents, that's not a bribe. That's literally your job."
- "The only reason a politician wouldn't want an educated populace is if they wanted to create a permanent underclass."
- "Maybe it's time to do stuff for those on the bottom."

### Oneliner

Beau criticizes Mitt Romney's jest about forgiving debt, advocates for education, and addresses income inequality.

### Audience

Politically engaged individuals.

### On-the-ground actions from transcript

- Contact your representatives to advocate for forgiving student loan debt and promoting education (suggested).
- Support initiatives that aim to address income inequality and benefit those at the bottom (implied).

### Whats missing in summary

Beau's passionate delivery and nuanced arguments can be fully appreciated in the complete video. 

### Tags

#MittRomney #StudentLoans #Education #IncomeInequality #WealthGap


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about something that Mitt Romney said,
because it certainly caught my attention.
He said this in jest, of course, but
I don't...
I personally don't find it that funny.
Dems consider forgiving trillions in student loans,
other bribe suggestions,
forgive auto loans,
forgive credit card debt,
forgive mortgages,
and put a wealth tax on the super-rich to pay for it all.
What could possibly go wrong?
I mean,
you forgot medical debt,
but uh... yeah.
I mean, that's...
that would probably go a long way. If you ran on this back when you had the
nomination instead of that forty seven percent thing,
you'd probably still be in office.
Give us a little
action on climate change through the giant jobs program.
Change the Constitution, give you another term.
The...
the reality is
this statement,
you know, throwing it out there,
just shows how disconnected
a man who has an elevator
for his cars
might be from everybody else.
Maybe
if you have an elevator for your cars.
Perhaps it would be best if
when uh...
discussions on income inequality came up, you just sat that out.
You're probably not going to have an informed take.
Look, I don't know who you have advising you, but give me a call.
I got binders
full of suggestions here.
All jokes aside,
the thing that really caught my attention
is the idea
that forgiving student loan debt
would be a bribe.
There's probably a lot of people in DC who could benefit from hearing this.
Okay, doing things that benefit your constituents,
that's not a bribe.
That's literally your job.
That's what you're supposed to do up there.
Bribes are those things you get from lobbyists.
That's what they are.
Doing things that benefit the country as a whole
is what you're supposed to be doing up there.
Not just feathering your own nest.
And
encouraging education?
Yeah, that benefits the whole country.
In order for democracy to function,
you have to have an educated populace.
Democracy is advanced citizenship. You have to be involved. You have to be able
to
sift through a bunch of misinformation
normally coming from politicians.
That's what it takes.
And education
benefits the economy.
It benefits national security.
It benefits
everything.
The only reason a politician wouldn't want an educated populace
is if they wanted to create a permanent underclass
that was, I don't know, constantly in debt and subservient.
That would be the
reason.
So they're easy to manipulate.
Now, sure,
you can't do all of that at once because it would hurt the banks.
I don't care.
I do not care anymore.
The reality is
our country
has a massive issue
when it comes to the gap between
the haves and the have-nots.
There probably needs to
be something
that happens
to
kind of give those on the bottom
a do-over,
a chance to start
fresh, a chance that
allows them to get ahead,
something that gets them out from behind the eight ball.
Rather than constantly
doing stuff for
the ultra-wealthy,
the super-rich,
maybe it's time to do stuff for those on the bottom.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}