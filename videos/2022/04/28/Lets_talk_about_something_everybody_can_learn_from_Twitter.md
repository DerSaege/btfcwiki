---
title: Let's talk about something everybody can learn from Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pVLNLMb8rMY) |
| Published | 2022/04/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Twitter's global reach means it must abide by various countries' laws to remain viable.
- Musk has committed to following local laws that may restrict certain content on Twitter.
- Total deregulation on social media could lead to toxicity, causing marginalized groups to leave first.
- Diversity of thought is vital for social media platforms to thrive.
- Interesting individuals leaving a platform can result in its demise, as seen in social media history.
- Users hold the collective power that makes social media networks influential.
- Collective action by users can drive policy changes on platforms like Twitter.
- Social media networks prioritize making money, necessitating a diverse user base for success.
- Beau is not concerned about worst-case scenarios on social media platforms.
- Users have the ability to shape the direction and success of social media networks through their presence and actions.

### Quotes

- "You make Twitter powerful, not the other way around."
- "Social media networks run on Highlander rules."
- "Your work created that, and they're giving you back some of it."
- "Making money on social media requires a lot of users. That means you need a diverse group."
- "That new network, it gets its power from you, not the other way around."

### Oneliner

Twitter's global reach mandates adherence to local laws, while user diversity and collective action shape social media power dynamics.

### Audience

Social media users

### On-the-ground actions from transcript

- Advocate for diverse voices and viewpoints on social media platforms (implied)
- Participate in collective actions to influence policy changes on social media networks (implied)
- Support marginalized groups and ensure their voices are heard on social media (implied)

### Whats missing in summary

The full transcript provides deeper insights into the importance of user actions in shaping social media platforms and the potential consequences of neglecting diversity and moderation.

### Tags

#SocialMedia #Diversity #PowerDynamics #CollectiveAction #UserInfluence


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about something
that everybody can learn from the recent discussions
about Twitter.
Even if you don't care about Twitter,
there's a lesson in this for everybody.
I've gone over why I don't think it's
going to be quite as bad as other people are picturing.
And then I had messages that came in,
And people are like, well, what about this?
What if he allows this?
One thing I want to point out is that Twitter is global.
A lot of the things that people are worried about,
they're actually illegal in other countries.
Like, if, as an example, if Musk was to allow fans of the
guy with the little mustache during World War II to
congregate, the website wouldn't be viable in Germany.
They lose Germany, and with it, by extension, they lose a lot of the EU.
That's a lot of money.
A lot of the stuff that people are concerned about will be in violation of the local laws,
the national laws, in various markets that are incredibly important to Twitter.
It's also worth noting that Musk has already said they're going to abide by those.
A lot of the stuff that people are worried about, really, that's not going to happen,
more extreme scenarios but for a second because somebody said something and it
kind of caught my ear for a second let's just say that it's true let's play that
game okay total total deregulation there's no moderation whatsoever okay
what happens it becomes super toxic really quick then what happens people
start to leave. Who leaves first? The marginalized. Marginalized groups who get
targeted because somebody told somebody to kick down. They're out the door first.
Them and their allies. They're gone, right? Then more people leave. What makes
social media function? And this is gonna make a lot of right-wingers mad.
Diversity. That's what makes it interesting. This is why the right-wing
clone websites constantly fail because there's no diversity of thought, there's
no discussion, they become boring. So if all the interesting people are leaving
and you just have a bunch of toxic people, what about those people who just
use Twitter to stay up-to-date or whatever? They leave too because they
don't want to deal with it, it's not worth it. The interesting people are gone and
now all they have are the toxic people. Everybody migrates to another site, could
be mastodon or counter-social. There's a whole bunch of them out there. So the end
result is that if Musk decides to burn Twitter to the ground, well, everybody
just leaves. It's happened before in social media history, it will happen
again. But the part that caught my ear was somebody saying, you know, you don't
understand how powerful a tool Twitter is. You know, we get to leverage that
because of that audience, you know, that makes Twitter really powerful. No. You
You make Twitter powerful, not the other way around.
Social media networks run on Highlander rules.
They get a little bit of power for every little disembodied head they collect and put in a
profile picture.
That's what makes their sites interesting.
You have the power.
Sure, you can leverage the collective power that that social media network has, but it's
It's the collective you that gives it the power to begin with.
It's just like the company you work for.
They give you a check.
They give you those power coupons, but where did they get the money to do that?
Your work created that, and they're giving you back some of it.
We have a lot of massive problems in the world.
It's really important that people understand the power of collective action, the power
that a bunch of individuals together have.
Understand even just a small 10% of Twitter migrates, those policies are changing because
Musk above all else is a capitalist billionaire. He wants more money in his
Scrooge McDuckian vault on Mars or whatever he plans on building. That's not
going to change. You can count on Twitter to do what's best for Twitter in the
sense of making money. Making money on social media requires a lot of users.
That means you need a diverse group. That's how you maintain it. That's how it stays interesting.
I'm not worried about the disaster scenarios, but even if something like that does happen,
it's a social media network. There's another. It'll be all right. And that new network,
it gets its power from you, not the other way around. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}