---
title: Let's talk about a BBC analysis of Russian personnel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zbsC5zDVyfU) |
| Published | 2022/04/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Analyzing a BBC report in Russian about Russian military losses in Ukraine.
- Russia admits to losing 1351 people, but estimates suggest it's over 10,000.
- BBC identified 1083 of the lost individuals.
- High percentage of officers among the identified losses, indicating an inaccurate count.
- Paratroopers and special operations forces are among the casualties, affecting the professional corps.
- Most of the losses come from economically depressed areas.
- Moscow shows no losses, suggesting hiding of real numbers.
- The undercounting strategy may backfire as the war continues and families seek answers.
- Leadership drain and lack of experienced troops will impact Russia's military capability.
- Paratroopers and special operations forces are dwindling due to casualties.
- Pressure will mount on Putin to explain the missing troops, leading to potential discontent back home.

### Quotes
- "You can't lose that many officers."
- "Being lied to about it, that's something else."
- "You're going to have trouble executing the maneuvers they're going to need."

### Oneliner
Analyzing Russian military losses in Ukraine reveals significant officer casualties, impacting future military operations and potentially causing discontent at home.

### Audience
Military analysts, policymakers

### On-the-ground actions from transcript
- Contact military families for support and information (exemplified)
- Join organizations advocating for transparency in military reporting (suggested)
- Organize community events to raise awareness about the impact of war (suggested)

### Whats missing in summary
The full transcript provides detailed insights into the potential repercussions of Russian military losses and the implications for both the ongoing conflict and domestic stability.

### Tags
#RussianMilitary #UkraineConflict #MilitaryStrategy #LeadershipDrain #Transparency


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about an analysis
that the BBC put together and what it can help us forecast
when we go through it.
Now, I'll go ahead and tell you, even though it's BBC,
I was only able to find the article in Russian.
It'll be down below, but you're going
to have to use the translate for it if you want to read it.
There's more in it than what I'm going to go over.
It's pretty interesting.
OK, so what's the analysis of?
Russian lost.
The analysis is of Russian lost.
They're admitted numbers and information.
At the time of the analysis, Russia
admits that it had lost 1,351 people in Ukraine.
I will go ahead and tell you now, that's a lie.
That's incredibly low.
Most conservative estimates right now
put that number above 10,000.
1,351 does not even cover the cruise to the vehicles
that there are photos of that were destroyed.
It doesn't even cover that.
So there's about 9,000 missing from this,
and we'll get to why here in a little bit.
OK, now of the 1,351, BBC was able to identify 1,083.
And that's where all of these numbers come from.
It is tracking that 1,083 they were
able to really kind of hone in on.
OK, 20% of those were officers.
That's huge.
That's huge.
We've talked about the number of generals
that we know they've lost.
Now keep in mind, Russia admits they've lost one,
but we know there are more.
Out of that 20%, there were 30 lieutenant colonels
and colonels, about 30 majors, and about 155 junior officers.
This is a big deal.
Keep in mind that in the Russian military,
lower ranks sergeants are not like sergeants
in the US military or the British military
or the Canadian military.
They don't have a leadership role.
They can't adapt to overcome a situation.
They just follow the orders of the officers.
This percentage is ridiculously high, so high
we know it's not accurate.
But the reason this number is so high
is because the Russian military, well, they
return the remains of those people who matter,
the officers.
That's how they look at it.
The officers are more likely to be returned.
That number of 20% in real life is probably about 10,
but that's still going to lead to huge issues later on,
and it's because of where those officers are coming from.
So out of that 1,083, 15% were paratroopers.
These are the highly trained, the highly motivated.
15% were paratroopers.
On top of that, there were 25 that were special operations,
confirmed special operations.
Five of them were officers, also a huge deal.
So we're talking about GRU.
These numbers indicate a couple of things.
One, more evidence of that whole they're saving their good troops
till the end, not true.
Aside from that, if these percentages,
if these losses carry through, the other 9,000,
they're not going to have much of their professional corps
left soon.
They're going to be running on strictly conscripts
and reservists, which is going to increase
the rate of loss.
Something that isn't surprising, 80% of those lost
came from economically depressed or semi-depressed areas.
If there's one thing that's true pretty much everywhere,
it's just another poor boy off to fight a rich man's war.
They have certainly embraced that part of capitalism
pretty well.
An interesting thing to note is that there
are no lost from Moscow, none.
None.
This is more evidence that they're
hiding the real numbers.
What it appears is going on is they are releasing the numbers
to make it seem real, but they are focusing on those
in the areas that don't have any power, the poor areas.
They're releasing those numbers because those numbers are
going to upset people in those areas.
If they upset people in those areas,
well, they don't have any power.
They can't threaten Putin's rule.
This idea of releasing it like this
and drastically undercounting, this
would have worked if it had been over in a week, two weeks.
These numbers are adding up.
The rate of loss is far exceeding what they're
telling the people at home.
At some point, mothers and wives are
going to wonder where their sons and husbands are.
And by conservative estimates, there's
about 9,000 that don't have a clue.
It is common for Russian troops to leave remains behind,
so much so that Ukraine has offered
to return them at times.
There have been issues in actually working that out,
though.
So what does this tell us?
What can we really pull away from it?
They're going to suffer a leadership drain.
They're going to have an issue controlling their military
and getting it to go where they want it to,
beyond the communication issues they've had thus far.
They're going to have problems getting troops
to enact basic tasks, because again, they
don't have an NCO corps.
They don't have non-commissioned officers.
Sergeants are not like they are in Western militaries.
They don't have any real leadership skills
or leadership responsibility.
They may have the skills.
But the way the Russian military is structured,
the way it works, the orders come down.
The junior officers are expected to enact them,
and the troops are to follow those orders.
When you are losing officers at this rate, even half
of this rate, which is probably what it is,
you're going to have trouble executing the maneuvers that
they're going to need when it comes
to relaunching their offensive.
You're going to be relying on a bunch of brand new officers
who don't have a clue.
Ask anybody in the military, the most dangerous thing
in the world is a second lieutenant with a compass.
So that's going to probably cause issues
throughout the war now for them.
The paratroopers, if this rate holds through,
it doesn't render them combat ineffective.
They're still going to be around.
They'll still be making moves, and that's pretty much who
they're going to have to rely on.
But those numbers are dwindling.
If they're taking that kinds of hits,
if they continue to take those kinds of hits,
they won't have them to fall back on either.
It's worth noting that half of this, of the paratroop losses,
are probably from that very well-planned airdrop.
The special operations losses are interesting
because, in theory, you wouldn't expect many of those at all.
For them to have this many in such a small sample
indicates they're relying on them pretty heavily,
and they don't have the support they need.
One thing that, even with how far off a lot of Western
estimates were as far as warfighting capability,
their special operations were trained.
So what this probably indicates is
that they're relying on them to engage
in things that aren't special, on just basic tasks.
So they're suffering more hits than you would typically expect.
The real problem for Putin here is
going to come when the pressure mounts to explain
where these missing troops are.
At some point, they're going to have to come clean with this.
Even with the information blackout they have going on,
word is going to get out.
Word will filter back.
There are people who haven't heard from their loved ones.
And eventually, this is the type of thing
that could cause a lot of discontent back home.
You know, knowing that the casualties are high,
that can cause discontent.
Being lied to about it, that's something else.
That's a whole new type of angry mom.
So I know there's a lot of people talking about him
wanting to wrap this up.
We'll see.
We'll see.
What is certain is that they can't go on like this.
The losses are too high.
The demographics that they're losing them in are too high.
You can't lose that many officers.
You can't lose that high a percentage
of your crack troops.
And even if the 80% from the poor areas that
don't have any power, even if that holds true,
those areas are also less policed.
And those are areas within Russia
where you could see real, real damage.
You could see real civil disobedience start to break out.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}