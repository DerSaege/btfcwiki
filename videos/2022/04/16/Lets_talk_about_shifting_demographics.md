---
title: Let's talk about shifting demographics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-AkedaUBPsk) |
| Published | 2022/04/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing shifting demographics and how acceptance impacts growth.
- Comparing acceptance of trans people to historical acceptance of left-handed individuals.
- Explaining the historical stigma and suppression of being left-handed.
- Noting the increase in left-handed individuals as society became more accepting.
- Sharing statistics on the percentage of left-handed individuals over time.
- Emphasizing that acceptance allows individuals to be true to themselves.
- Rejecting the idea that acceptance is part of a harmful agenda.
- Encouraging a focus on personal life rather than criticizing others' choices.

### Quotes

- "The more accepting we are, the more of them exist."
- "I don't see it as a sign of it being some plot to program us into something."
- "Be more concerned about your own life than kicking down at somebody else for living theirs."

### Oneliner

Addressing shifting demographics and acceptance, Beau compares the growth of trans people to historical acceptance of left-handed individuals, stressing the importance of allowing individuals to be true to themselves.

### Audience

Those promoting acceptance and understanding.

### On-the-ground actions from transcript

- Support and advocate for the acceptance and rights of all individuals (implied).

### Whats missing in summary

Full context and detailed explanations provided by Beau.

### Tags

#Acceptance #ShiftingDemographics #Equality #Community #Inclusivity


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're going to talk about shifting demographics
and how things change over time and what
happens if we accept something and how that
may cause something to grow.
I was asked a question, and I'm going
to try to give the person a hand with it, a left hand to be exact.
Because it's one of those questions that you
see people asking today.
But if you take a step back, it's not a question.
I mean, it's really easy to see what's happening.
In fact, most times when the question is asked,
the answer's in it.
So here's the question.
How do you not see that it's programming, Bo?
The more accepting we are of trans people,
the more there are.
Same happened with the gays.
The gays.
You can't really think this is a good thing.
Yeah, I do not care.
This does not bother me.
The more accepting we are, the more of them exist.
Maybe it's just they always existed,
and they're not hiding it now.
Maybe that's what it is.
I mean, that makes a lot more sense.
Let me give you some statistics about it, about gay people.
In 1910, a little bit more than 3% of the population
said they were gay.
By 1960, it had grown to 12%.
And then it leveled off.
Hasn't changed since.
Wait, I'm sorry.
That's not the stats for gay people.
That's the stats for being left-handed.
See, there was a time when being left-handed,
when it was the devil's hand and all of that stuff.
And they tried and tried to just push it out of people.
Kids that were left-handed, they would force them
to write with their right hand.
They would do everything they could to break who they were.
Over time, people became more accepting.
Superstitions and silly notions just faded away.
People became more accepting.
So more people were willing to use or identify as left-handed.
Willing to use their left hand or identify as left-handed.
Until it hit the normal number, 12%.
Hasn't changed since.
I imagine the same thing is going to happen.
I would imagine that it isn't programming.
I would imagine that this is something that occurs.
It's always been there.
And because we weren't accepting of it, it was hidden.
It was pushed away.
They couldn't be who they were.
But as we become accepting of it, more and more people
are willing to step out and say, hey, I'm left-handed.
Or whatever else.
I personally don't see it as a bad thing.
I don't.
Maybe it's just the Southern area in me,
but that isn't my business.
I mean, that is not my business.
That line, you can't really think this is a good thing.
Yeah, I don't think it's a bad thing.
It's just people doing what they want to do.
I don't see how that's bad or could be perceived as bad
by the My Freedom crowd.
It seems pretty clear that there were a whole lot of people
who were forcing themselves to fit into a role that
wasn't really them.
And this has been going on a long, long time.
We're just more accepting of it so they
can be more open about it.
Nothing more.
It's not some plot.
You can find this trend with a whole lot of stuff.
Left-handed is just the funniest,
and I just saw a chart about it.
Generally speaking, if a behavior is seen as bad,
people hide it.
And then as people accept it, the number goes up,
and then it finds a happy spot and just levels off.
I don't see anything wrong with that.
I don't see it as a sign of it being some plot
to program us into something.
Doesn't even make sense.
I would be far more concerned about things that actually
impact your life than what people have told you
to be mad about.
What people have told you is somehow an affront to you
when it isn't.
Be more concerned about your own life than kicking down
at somebody else for living theirs.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}