---
title: Let's talk about why we broadcast NATO shipments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A-enw0Fmvdc) |
| Published | 2022/04/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Military aid to Ukraine is not fully disclosed in public announcements; there are undisclosed components accompanying the disclosed weapons.
- Publicizing military aid serves political purposes for the sending country, aiming to showcase support and commitment.
- Some items, like radios or personal equipment, are not always listed in disclosed shipments but are still sent.
- The disclosed armed shipments are somewhat transparent as Russia will eventually learn about them.
- Publicizing aid can be a power move, showing that the aid will reach Ukraine regardless of Russia's awareness.
- Information released in press releases or speeches is not always intended for the public; it can be part of a broader messaging strategy.
- Not all information shared is meant for domestic consumption; sometimes, it's directed at foreign audiences.
- Every country has figures who make extreme statements, but they may not represent the general population's sentiments.
- Media clips can skew perceptions; it's vital to understand the context and messaging behind statements made by public figures.
- In times of war, there is an information war, and not all information released should be taken at face value.

### Quotes

- "Every war has an information war."
- "The first casualty in any war is the truth."
- "Just understand there's a whole bunch of stuff that's going to go on behind the scenes that we won't know about for 10 years."

### Oneliner

Beau reveals the hidden aspects of military aid to Ukraine, shedding light on the political messaging behind public announcements and the importance of questioning information sources.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Question official narratives and seek alternative sources of information (suggested)
- Stay critical of media coverage and public statements during times of conflict (suggested)

### Whats missing in summary

Importance of critical media literacy and skepticism in consuming information during conflicts.

### Tags

#MilitaryAid #Propaganda #InformationWar #CriticalThinking #Skepticism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about shipments from NATO, messaging, why we do things the
way we do, and whether we know as much as we think we do at times.
I'm going to do this because I've got a question.
Beau, why do we announce what military aid we're sending to Ukraine?
Is it for legal reasons?
It seems like it just gives a heads up to Russia and lets them prepare for what and
where they will be used.
Okay so why do we announce what military aid we're sending to Ukraine?
We don't.
Not really.
There's the announcements that come out, but that's not really all we're sending.
I mean make no mistake about it, if you see a press release from your government saying
that they sent 300 anti-tank weapons, they did, but there was probably other stuff along
with it.
It's not quite as open as it may seem.
Why do they do this?
A large part of it is us, the commoners in the country that is sending the stuff.
The politicians want credit for doing their part.
So they put out this press release, we're sending this equipment, and we're like yay.
And that's a big part of it.
Another part of it is to demonstrate to Russia the commitment of the various countries.
But those lists are nowhere near complete.
There's other stuff that is being sent along with it, I am certain.
Matter of fact, there are certain things I've seen that are US origin that haven't been
on any of those lists.
Most of it is minor stuff like radios or personal equipment, like stuff you would wear, that
has shown up.
That sure, in theory, it could have been purchased on the civilian market and shipped there,
but it seems super unlikely when it's coming in those boxes.
So the answer is we really don't do that, not to the degree that I think people believe.
Those armed shipments are disclosed because Russia's going to find out about them anyway.
And most of the stuff that's being disclosed is being disclosed in a power move kind of
way.
We're sending this, and it doesn't matter if you know they have it on the battlefield
because you can't stop it.
And it's also part of a propaganda effort to boost Ukrainian morale and degrade Russian
morale.
So this brings us back to that video when all this started, where we were talking about
messaging.
Remember that a lot of the information that gets put out via press releases or said during
speeches, it's not for us.
Just like this, putting out this information, isn't for Ukraine.
They know what they're getting.
They don't need a press release on it.
It's not really to tell Russia.
That's for us, even though it's shaped in a different way.
It's to make us feel like we're involved, like we're part of it.
But normally when it's said, it's more about telling Ukraine that we're going to
do this for them.
But that's not really what it is.
The Ukrainians provided the list of what they want.
They know what we're sending.
They don't need an announcement.
They need a phone call saying yes.
That's for us.
Just remember it works the other way.
There's a lot of times when things are said to us that it's really messaging to another
country.
Another thing to remember that is starting to catch people's eye is that every country,
everywhere in the world, has a space laser lady or a Boebert or a Lindsey Graham, somebody
out there just saying ridiculous stuff.
I want you to think about how far off your analysis of the United States would be if
you were in another country and the sound bites that were played were of Graham, Boebert,
and I've actually forgotten her real name now, the space laser lady.
You'd probably have a very skewed opinion on popular sentiment.
When you're watching clips from Russia, just remember you might be watching their
Boebert.
You might be watching their Lindsey Graham.
So that wild thing that was said, that may not actually reflect the population.
It's messaging to try to get them more behind the war or this person has built a persona
on being the most extreme on any issue or they're saying it knowing that it's going
to be played here and it's part of a propaganda effort to impact us.
Every war has an information war.
The first casualty in any war is the truth.
A lot of this stuff, don't accept it at face value.
There is no way that we are cataloging and announcing everything we ship.
That's not occurring.
It seems like it because it seems as though they're giving pretty precise numbers and
dollar amounts and all that stuff.
Just understand there's a whole bunch of stuff that's going to go on behind the scenes that
we won't know about for 10 years.
So just remember when you're bringing this information in and you're kind of absorbing
it, try to every once in a while just stop and ask who is this information really for?
Why is it being put out?
Is it for me to consume or is it really something aimed at somebody else and I shouldn't even
pay attention to it because it's not going to be accurate.
It's a weird place to be but that's where we're at.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}