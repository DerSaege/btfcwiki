---
title: Let's talk about Patrick Lyoya in Grand Rapids....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2ZOK1xJeSmw) |
| Published | 2022/04/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing a police shooting in Grand Rapids involving the driver Patrick Leoia and a cop.
- Communication issues arise from the beginning of the interaction.
- Officer grabs Patrick from behind, initiating violence.
- Patrick breaks free and runs instead of fighting the cop.
- Cop escalates the situation by chasing Patrick instead of waiting for backup.
- Commands like "don't" and "stop" are ineffective in high-stress situations.
- Officer shoots Patrick in the base of the skull during a struggle.
- The officer's potential justification for the shooting may focus on fear of harm due to a taser threat.
- Beau questions the reasonableness of fearing someone who repeatedly tries to run away.
- The legal system's focus on what the law allows may override common sense and best practices.

### Quotes

- "What's going to happen is the argument is going to be made that Patrick was trying to get that taser so he could then turn it on the officer, use it, and get his gun."
- "There is a huge gap between what's best practices, what policy says, what training dictates, what common sense says, and what the law allows."
- "But it's what currently exists."
- "Even if there's a whole video showing that he has no intention of using violence."
- "Y'all have a good day."

### Oneliner

Beau analyzes a police shooting in Grand Rapids, questioning the escalation and potential justifications in a system that often favors legal technicalities over common sense and best practices.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Contact local officials to demand accountability for police actions (exemplified)
- Organize community meetings to address police violence and advocate for policy changes (exemplified)

### Whats missing in summary

The full transcript provides a detailed breakdown of a police shooting incident, raising questions about escalation, fear justifications, and the gap between legal standards and common sense.

### Tags

#PoliceShooting #Accountability #CommunityPolicing #LegalSystem #Justice


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about some news out of Grand Rapids
because the video was released.
So it's going to be one of those videos.
We're going to talk about what's on the video
and kind of go through it.
We'll talk about what best practices are, what policy is.
We'll talk about where it went wrong,
where things could have certainly altered the outcome.
And we will, from there, talk about what the investigation
is likely to lead to and whether or not charges will be filed.
If you don't know what I'm talking about,
the video of the police shooting of Patrick Leoia
has been released.
So what's the video show?
Cop pulls over a car.
The driver gets out of the car.
They start talking, going back and forth.
The guy, the driver, Patrick, is asking what he did wrong.
The cop is asking for a license.
From the very beginning, there's a communication issue.
The driver has an accent, seems to be from somewhere
in Central Africa.
The cop is aware of the communication issue
to the point where he asks, do you speak English?
And Patrick kind of pauses for a second before answering yes.
Patrick is concerned about what he did.
He's asking, what did I do wrong?
The cop is saying the tag doesn't match the vehicle.
This is not an answer that Patrick understands.
That's pretty clear from the video, because he asks again.
The cop is finally like, do you have a license?
He's like, yes.
Where is it?
In the car.
Get it for me.
Patrick opens the door, asks his passenger
to hand him his license.
And the passenger can't find it.
So Patrick closes the door and starts
to walk around the front of the vehicle,
seeming very clearly like he's going to go get his license.
At which point, the officer yells, no, no, stop, whatever,
from behind.
No actual command, just no and stop.
We'll get to this in a minute.
Yells it from behind as he grabs him from behind.
To be clear, the officer initiated violence, not Patrick.
When you grab somebody from behind,
especially by surprise, you trigger fight, flight,
or freeze.
After a brief tussle, because the officer
is unable to take Patrick to the ground, Patrick breaks free.
And this is where you get to find out,
did it trigger fight or flight?
Patrick doesn't turn around and beat this cop.
It seems pretty clear that he was totally capable of doing so,
because he just kind of shrugged this guy off.
But that's not what he did.
He ran.
Took off running.
So what's the smart move here if you're the cop?
You have somebody who you could not take down from behind
by surprise.
They are now running.
What do you do?
Let them run.
Let them run.
Wait for backup.
That's the smart move.
I know somebody's going to say, well, what if he runs away?
Well, he has an accent that isn't from north of our border
or south of our border.
He flew here.
We have his prints.
They're on file.
You have his car, probably his keys, probably his ID,
and his passenger.
Get him later.
That's the smart move.
That's not what the cop does, though.
Cop takes off running after him.
Jumps on him from behind again.
This whole time, the commands are don't, stop,
stop resisting, stuff like that.
If you haven't seen me talk about this before, understand
that means nothing.
In a high stress situation, the cop
might as well have been yelling flat tire.
It means nothing.
Have to give actual commands.
Tell them to do something rather than don't do something.
Because a biological response has been triggered.
Stop means nothing.
OK, so the cop ends up on Patrick's back.
He finally realizes that, hey, he should give a command.
And with Patrick on the ground in basically a pushup position,
arms extended, with the cop on his back,
the command he gives him is to put your hands
behind your back.
That's not a command that can be complied with.
Gravity has something to say about that.
So Patrick again gets up, breaks free.
Now when I say break free, understand,
he's not hitting this cop.
He's literally just kind of like shrugging the guy off.
The officer has kneed Patrick a couple times, stuff like that.
But Patrick has not responded with violence.
So takes off running again.
Cop catches him again.
This time, cop pulls a taser.
Patrick grabs the taser, points it away from him, and says,
I didn't do anything to you.
Cop attempts to deploy the taser.
It doesn't hit him, doesn't do anything.
They fall to the ground again.
At this point, the cop starts saying, drop the taser,
let go of the taser.
And he reaches back to his gun belt, pulls his pistol,
and shoots Patrick in the base of the skull
while they're laying on the ground.
During this time, the passenger has gotten out of the vehicle.
And during the scuffle, yells at the cop,
you can talk to him, just talk good to him.
You can talk to him.
To which the cop yells back, no.
OK, so given everything that I've said,
it's pretty clear that my perception of this
is that this officer escalated unnecessarily
at pretty much every possible opportunity.
Did not follow the smart move, didn't follow best practices.
So because of that, obviously, since the DA
is looking into it, the prosecutor's obviously
going to charge him, right?
Probably not.
Probably not, because that's not what's
going to matter at that point.
What's going to happen is the argument
is going to be made that Patrick was trying to get that taser
so he could then turn it on the officer,
use it, and get his gun.
And therefore, the officer has a reasonable fear
of grave bodily injury or death.
That's going to be what's argued.
And it will probably work.
Does that make any sense?
No, from the guy who tried to run repeatedly and never
used violence, no.
It doesn't make any sense.
But you're not talking about justice here.
You're talking about the legal system.
That's going to be the argument that's made.
It'll probably work.
It normally does.
I personally do not believe that it's
reasonable to be afraid of somebody who keeps
trying to run away from you.
I don't think that's reasonable.
But this argument normally works.
Now, given the last few years, there
has been more of an effort for accountability
when it comes to stuff like this.
So you have a chance that the prosecutor may decide
he initiated violence where none existed.
Therefore, everything afterward is the officer's fault.
But I wouldn't expect that.
There is a huge gap between what's best practices,
what policy says, what training dictates,
what common sense says, and what the law allows.
This is one of those cases where what the law allows
is going to override the other stuff, is what it seems like.
Given the relatively compliant nature of Patrick
when he got out of the car, the communication issue,
the officer initiating contact, all of this
should factor into the final outcome.
But it's probably not going to.
And again, this is one of those videos, I'm not saying that that's right.
I'm saying it's wrong and it needs to change.
But it's what currently exists.
I hope I'm wrong on this one, because normally when I do these videos,
I'm pretty confident there's a lot going on.
And I'm not saying that it's wrong.
I'm just saying that it's not going to work.
I'm pretty confident there's a lot going on in this one
that a prosecutor might look at and say, maybe this
is the one where we test a jury here.
But generally speaking, when the person they're trying to detain
grabs that taser, that becomes their total justification.
Even if there's a whole video showing that he
has no intention of using violence.
The ease at which Patrick brushed this cop off repeatedly
tells me that if he wanted to, it could have been the cop who
people were investigating the circumstances of.
So there's a possibility that the prosecution moves forward
and brings charges.
But it seems unlikely.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}