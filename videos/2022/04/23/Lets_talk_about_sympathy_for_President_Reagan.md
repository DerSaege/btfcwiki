---
title: Let's talk about sympathy for President Reagan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3AP7DysZDa8) |
| Published | 2022/04/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A high school student asked for help with a project on President Reagan depicting him as a hero or villain.
- The replies on Twitter gave the student a list of Reagan's deeds and misdeeds to help with the project.
- Beau criticizes the assignment for framing historical figures as heroes or villains, calling it mythology rather than history.
- He warns of the danger of overlooking the flaws of historical figures when classifying them in simplistic terms.
- Reagan, often depicted as a hero in American history mythology, had misdeeds that could be glossed over.
- Beau points out that historical figures like Reagan were flawed individuals, not purely good or evil.
- He cautions against viewing historical figures as gods or devils, urging to understand them as flawed people.
- Beau advocates using historical figures as teaching tools to learn from their mistakes and not repeat them.
- He warns against perpetuating the myth that historical figures were faultless, as this can lead to dangerous black-and-white thinking.
- Beau encourages acknowledging the flaws of historical figures and using them to learn deeper aspects of history.

### Quotes

- "But the reality is that's not history, that's mythology."
- "It's fine to use people as a teaching tool to get people to understand the story, the general narrative."
- "Every cop is a criminal, and all the sinner saints."
- "Historical figures were just people, not gods, not deities, not devils."
- "We can't shape history that way, because then it's not history, it's mythology."

### Oneliner

A critical reflection on the dangers of simplistically categorizing historical figures as heroes or villains, urging to view them as flawed individuals to learn from their mistakes.

### Audience

Students, Educators, Historians

### On-the-ground actions from transcript

- Question assignments that oversimplify historical figures as heroes or villains (exemplified).
- Encourage critical thinking in historical analysis (implied).
- Teach history as a complex narrative of real individuals with flaws and virtues (exemplified).

### Whats missing in summary

Importance of learning from the mistakes of historical figures to avoid repeating them in the present and future.

### Tags

#History #HistoricalFigures #Teaching #CriticalThinking #Mythology


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about a question I received
from a high school student about their homework
because they'd been assigned a project
to create a poster depicting President Reagan
as either a hero or make it like a wanted poster
because they're a villain.
And I took the question that they sent
and I put it on Twitter.
And the replies gave them their answer,
gave them the information they needed.
They got an entire list of his deeds and misdeeds
that they can use to decide
how they're gonna complete this project.
But it's the project itself that I wanna talk about
because that's how history gets taught a lot,
framing people, individuals, as either heroes or villains.
But the reality is that's not history, that's mythology.
And that image, it gets cast immediately.
So when you're talking about Reagan,
what's the first thing that happens?
You have to be careful, right?
Because of his medical condition there at the end,
people expect you to extend a courtesy,
have a little bit of sympathy, show some taste.
Yeah, if you're a Rolling Stones fan,
you already know where this is going.
And that's what's expected
because that mythology's already being built with Reagan.
It's been decided already that as history progresses,
Reagan will be a hero.
He will be depicted in American history mythology as a hero.
So a whole lot of his misdeeds,
well, they're just gonna be glossed over
because heroes in mythology,
well, they can't have done bad things.
And this is the danger of teaching history in this way,
of using this teaching tool.
It's fine to use people as a teaching tool
to get people to understand the story,
the general narrative, how things happened,
get them to understand the dates
and use those as references.
But when you actually start trying to classify
historical figures as heroes or villains,
you run into this problem and it's a big one
because you have to look at the person,
the time they were in, what they were doing,
what their intent was.
And with Reagan, what's gonna leave you puzzled
is the nature of his game.
What was he really trying to do, right?
And you start to examine him.
And what was he?
He's an actor, a rich guy,
pretty well-dressed for the time, spoke well.
He was a man of wealth and taste.
Because of the way he presented himself,
he'll be a hero.
And everything that he did that was wrong
will be kind of washed away.
We won't see it.
In 50 years, he'll probably be on currency because of
the mythology that gets built because of the clips,
that iconic speech, you know,
Mr. Gorbachev, tear down this wall.
He was a man who looked to Russia
and saw it was time for a change.
But that doesn't sum him up.
But that's what's gonna be remembered,
that clip over and over again.
And again, fine as a teaching tool,
as long as you understand it's not actually history.
Now, again, this teacher,
they could be using this assignment
to make this exact point because I would assume
different people in the class would view Reagan differently.
So it could be,
the whole project could be designed to show this.
But in case it's not,
you have to understand the danger
of thinking of historical figures
in terms of heroes and villains.
Because there aren't many people
who were wholly heroes or wholly villains.
There are a few,
but most times it's not that simple.
And the danger that comes with this
is that if you look to history and you see heroes
and you see people who were always right
because the negative stuff they did
was forgotten, washed away, never mentioned,
you start to look for those kinds of people existing today.
And you may think you find one.
And when you do,
you start to accept that hero's version of everything as fact.
This is how people end up selling out their country
for a red hat and a stupid slogan.
This is how it happens.
Because they become so ingrained with that mythology,
that black and white way of thinking,
good and evil,
and that binary,
that they think it really exists.
So they overlook everything negative
and just focus on the positive,
which is fine in the sense of,
if you believe it's a net good, it's okay.
But if you believe that people are actually like that,
if you believe that there are people out there
who can do no wrong and have done no wrong,
you're wrong.
Every cop is a criminal,
and all the sinner saints.
It's probably a good idea
to understand that historical figures were people.
They're flawed, just like everybody you know.
Do you know anybody who is purely good or purely evil?
I don't.
I don't know a single person like that.
Historical figures were just people,
a lot of them who we view as heroes were flawed.
They did bad things.
A lot of the people we view today as heroes
were flawed and did bad things.
Sure, they can make up for it.
Maybe that bad thing wasn't enough
to override all the good they did,
but that works the other way too.
And you have to acknowledge
that historical figures were people,
not gods, not deities, not devils.
They were just people.
These stories that we create,
the myths that we surround these people with,
they're of our making,
so we can justify our actions today.
It tends to lead to bad places.
It's probably a better idea
to leave great man theory in the past,
to use historical figures as a teaching tool
to get a story so people can understand
the deeper aspects of history
so they can hopefully not make the same mistakes
their heroes did,
because your hero, whoever it is,
they made mistakes.
They did bad things.
We can't shape history that way,
because then it's not history, it's mythology,
and then we expect myth to be true.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}