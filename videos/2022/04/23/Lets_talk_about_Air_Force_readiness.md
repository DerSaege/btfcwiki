---
title: Let's talk about Air Force readiness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nGYq1O-2Z38) |
| Published | 2022/04/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Three messages back-to-back about Air Force readiness sparked a need for a talk on the topic.
- The story involves conflicting messages about the impacts of laws in Texas on Air Force readiness.
- Details the process of Air Force personnel moving every four years and the implications of disruptions.
- Explains how a chain of events can lead to gaps in personnel, affecting readiness and causing economic impacts.
- Suggests that tracking gender and orientation could help mitigate some issues but poses challenges.
- Points out the domino effects on housing, local businesses, and the economy due to personnel disruptions.
- Emphasizes that laws impacting military personnel based on bigotry can have negative economic consequences.
- Encourages prioritizing policies that allow service members to thrive and contribute positively to the economy.
- Concludes by advocating for kindness and cautioning against supporting politicians who incite division for votes.

### Quotes

- "There aren't a lot of laws that are bigoted in nature that don't cause a negative economic impact."
- "It's cheaper to be a good person."
- "Always does."

### Oneliner

Three conflicting messages spark a talk on Air Force readiness, revealing the impacts of laws and policies on personnel movements and economic well-being.

### Audience

Military service members

### On-the-ground actions from transcript

- Advocate for policies that support military personnel and their families (suggested)
- Support businesses impacted by fluctuations in military personnel presence (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the challenges faced by the Air Force due to conflicting laws and policies, showcasing the broader implications on readiness and economic stability.

### Tags

#AirForce #Readiness #Personnel #EconomicImpact #Policies


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about Air Force readiness,
because something happened, and it just cracked me up.
Y'all send me a lot of messages every day.
And I try to read them all.
But over the course of 20 minutes,
I got three messages back to back all about readiness,
all very different messages, but all
revolving around that topic.
The first one was from somebody who was just
telling me I was wrong.
There's no way that the laws in Texas
would impact readiness or cause economic issues
for the areas around it, and that there's no way
that Congress would allow the Air Force to close
or deprioritize bases because of this.
The very next message was from somebody
at the Air Force personnel office,
their central bureaucracy for personnel,
asking me to put out a video explaining
how much of a headache this is going to cause for them
and how it would impact readiness.
The third message was from somebody who is trans
and is thinking about joining the Army
and is basically like, I have no idea what readiness is,
but this seems like something I really need to know about.
So that's why we're going to talk about it.
To answer that third one first, why are you
looking at joining the Army?
They didn't put out a memo like this.
They didn't say they were going to try to help or protect you.
That's just the Department of the Air Force.
You might want to rethink that.
And as I say that advice, and while it's true,
I want to point out that that might cause a readiness
issue for the Army, not being able to recruit people.
But that's not what we're here to talk about.
We're here to talk about Air Force readiness
and how the chain of events works.
The first thing you need to know is that generally speaking,
there are exceptions, but generally speaking,
if you're in the Air Force, you're going to move every four
years.
You and your dependents are going to pack up
and you're going to go to a different location.
And this happens because people leave the Air Force,
people get promoted.
There are slots that need to get filled.
There are positions that need to get filled.
The orders to do this, to move around,
come from that central bureaucracy
that sends out the orders.
OK, so Captain Smith is up at Minot.
And Captain Smith catches orders to go
to an installation in Texas.
Well, Captain Smith then has to go to his chain of command
and say, I can't go.
My kid's trans.
So has to work its way up the chain there at the base
and then get sent back to the central bureaucracy.
Right?
At that point, the people at the personnel office,
they start banging on their keyboards
because it's not just as simple as finding a new place
for Captain Smith to go.
They also have to find somebody to take that spot.
So they need another captain who has similar career development
to go and take that position.
That's going to be Captain Jones.
So they send new orders to Captain Jones.
Captain Jones is going to show up in March now instead
of Captain Smith showing up in January.
Already three months where they're
missing somebody at this base.
That's where the readiness issues begin.
Now, meanwhile, the people at the Air Force personnel office,
they're trying to figure out how to stop this from happening.
Because since they moved that second captain,
since they moved second Captain Jones,
they have to find somebody to replace her
wherever she was going to go.
And that's just this long domino thing
that goes on from there.
So they're trying to figure out how to stop it
from happening again.
And the only real way that I can think of
is for the Air Force to start tracking
the gender and orientation of every Air Force dependent.
That's going to get really expensive and really
complicated.
Because it would take the active duty member constantly updating
these forms.
Because people's status when it comes to that may change.
And there's also the issue of the fact
that we all know there's no way the military is going to make
a mistake with paperwork, right?
It's going to be a giant headache.
It's going to cause gaps where positions aren't filled.
And that's a readiness issue.
And then you have Captain Jones.
She shows up down in Texas, right?
She loves it there.
She loves it.
That's the base that she's going to tell everybody
for the rest of her career.
You know, this place is nice, but it's
nowhere near as nice as wherever, right?
And then six months after getting there,
her kid's like, hey, can I talk to you about something?
Then she has to go to her chain of command,
and she has to leave Texas.
That gap, well, it starts all over.
Because now they have to find somebody else to come in.
It absolutely will cause a readiness issue.
There's zero doubt about that.
It's going to cause personnel problems.
It's going to cause issues when it comes to training
and all kinds of other stuff.
There's a lot of downstream effects to this.
And I really can't think of a way
to avoid these kinds of problems without tracking everybody's
gender and orientation.
But even that doesn't solve it, because that changes.
And it may change while this active duty service
member is at an installation that
is hostile to the existence of their children.
And then you have some situations
in which the only location they could go for that job
happens to be in one of those states.
It's going to cause personnel problems.
It's going to cause problems.
And then as far as economic impact, sure,
Congress might step in and say, OK, you can't do this.
You can't close this base.
You can't deprioritize, whatever.
That can happen.
But I'm willing to bet the Air Force wouldn't open up
anything new there.
Why would you invest in an installation
you can't send all your people to?
And then as time goes on, the stuff there becomes obsolete,
gets replaced with new investments
in different locations.
Just takes longer.
So rather than an immediate economic impact,
the community around the base just withers.
Doesn't actually sound better to me.
But aside from that, without any of that happening,
just the readiness issue and the lack of personnel
and those gaps, that is going to cause an economic impact just
there.
So let's take a number, a nice round number, a million
dollars.
What would it take to take a million dollars directly out
of the economy when it comes to this?
We're talking about captains.
So their pay plus their housing allowance,
a dozen, 14, a dozen, 12 to 14, somewhere in there.
That happens.
12 to 14 captains can't go there.
And understand the Air Force is officer heavy.
And that's a million bucks out of the economy.
But that's just the starting point.
Just like with the Air Force bureaucracy,
there's domino effects.
Because those gaps create time when people aren't there,
totals up to 14 captains being gone for a year,
that is 14 three-bedroom houses that
aren't getting rented, which means the price of rent
drops just a little bit because we're only talking about a dozen
or so right now.
But because the price of rent drops,
property values will also go down.
Again, just a little bit, but each little thing adds up.
Captain Jones, Captain Jones used to be stationed at Eglin.
She loved that little coffee shop right there
on 20 outside the gate.
She'd go through there every day, get a $5 coffee
and a $4 sausage biscuit.
And it became a tradition.
She does it everywhere she goes now.
She's not doing that anymore because she's not there.
And that's part of that million dollars.
But because that million dollars isn't
being spent in that coffee shop, that coffee shop now
has less money.
So the coffee shop, maybe it pays its people less.
Maybe it doesn't hire a new employee.
Maybe it doesn't have flyers made
and hire the print shop down the road to make them.
And it just continues to grow.
Understand, without the Air Force doing anything
other than honoring the policy that's already in place
to protect their people, there will already
be economic impacts.
And the reality of this is these states, the politicians that
pass this stuff, they are asking the Air Force
to make all kinds of adjustments and policy changes
just to appease a campaign stunt so they
can get the bigot vote.
That's all it's for.
I don't think the Air Force is going
to be really receptive to that.
I would suggest that the Air Force is probably
going to be more interested in getting their people somewhere
where they can feel at home and their dependents
can feel at home.
They move around every four years as it is.
There aren't a lot of laws that are bigoted in nature that
don't cause a negative economic impact.
By definition, you're excluding people.
These are people that would be contributing to the economy.
Remember that the next time your politician
wants to catch your vote by telling you
that you're better than somebody else and to kick down at them.
It's going to hurt your wallet.
Always does.
It's cheaper to be a good person.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}