---
title: Let's talk about why parts of the right are supporting Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vcuvzyJCnJQ) |
| Published | 2022/04/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining why some people believe that everything is going according to Russia's plan in Ukraine, despite contradictory actions by Putin and the reality of the situation.
- Mentioning how both far-right and far-left individuals are repeating the lie that everything is going according to Russia's plan.
- Pointing out that certain survival channels continue to spread claims that Russia is winning and that the US could be next, fueling fear to keep their audience engaged.
- Debunking the idea that Russia poses a real invasion threat to the United States, citing population numbers and logistics.
- Emphasizing that the current military landscape and global awareness make a large-scale invasion impractical and unlikely.
- Noting that fear-mongering channels prioritize selling fear over educating their audience on more realistic threats like climate change.
- Stating that maintaining fear is profitable for these channels, even if it means perpetuating falsehoods and sensationalism.

### Quotes

- "They sell fear."
- "It's not profitable to tell the truth in that case."
- "Keeping that dream alive."
- "The days of having to worry about somebody invading the United States, those are long gone."
- "Rather than taking the time to educate their audience on how to prepare for stuff that might actually happen, like climate change."

### Oneliner

Beau explains the profit-driven fear tactics of survival channels spreading false invasion narratives to maintain audience engagement.

### Audience

Content Creators, Viewers

### On-the-ground actions from transcript

- Educate your audience on real threats like climate change, encouraging preparedness beyond fear-based scenarios (implied).

### Whats missing in summary

The full transcript provides an in-depth analysis of the profit-driven fear tactics used by certain channels, contrasting sensationalism with the reality of military capabilities and global awareness.

### Tags

#FearTactics #DebunkingInvasionThreats #ClimatePreparedness #ProfitOverTruth


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about why
some people still believe that everything is going according to plan. That Russia's doing just fine
over there and that's what they want to do and that's how things are supposed to be going
and they're just repeating that line. And why those people are doing that and some even suggesting
that the United States is next. I got this message. I have a question for you. I watch a lot of
American YouTubers. I see some of the far right and far left both repeating the lie that everything
is going according to plan, even after Putin spent days saying the people at the steel plant had to
surrender or else and then suddenly changing his mind about attacking. If he really was trying to
get rid of the far right in Ukraine, wouldn't that be the most important battle? Yeah, I mean it would.
That's kind of where they're all at. But they still accept his lies and repeat his lies.
I watched your video explaining why some who link Russia to the USSR on the left believe it or at
least want to believe it. But why do the right-wing survival channels also repeat these claims?
They say Russia is winning and that the US could be next.
Because they got to keep the dream alive. Most of those channels aren't really survival channels.
They don't sell information about survival. They sell fear. The whole idea of this is that
some of them probably know that if they admit that Russia is doing as poorly as it is,
it will set their audience's mind at ease. And they don't want that. That's not profitable to them.
If they acknowledge that a country they've been building up as a possible invasion force to the
United States can't take control of Ukraine, A, their audience will relax and B, they're going to
look silly. They have to keep that dream alive. It is like super hard to sell your channel's
branded buckets of freeze-dried food without some huge enemy. That's the main reason.
The reality is Russia can't. Russia can't even in peak condition. Russia does not pose a threat
to the United States as far as invasion. That's silly. We've gone over the numbers required
to occupy areas over and over and over again on this channel because stuff like this keeps coming
up. I'm not going to break out the whiteboard and do all the math again, but the US has more than
300 million people in it. Russia, if they mobilized the 2 million reservists they had and used every
single active duty member of the armed forces to include pulling people off of boats and out of
planes and put them all as boots on the ground in the United States, somehow without any
casualty, they would almost have about like 40% of what they need. It's not going to happen.
They don't have the troops. And understand those ratios of how many troops you need per
inhabitants, those are based on the inhabitants being in a relatively small area, not spread
across a country the size of the United States. They're also based on the idea of light resistance,
not invading a country that has more guns than people.
It's not a thing. Russia's not going to invade the United States, not anytime soon. A whole lot
of things have to shift in the balance of power for that to occur. There's really only one country
that can field a military large enough to do this, and that's China. And they have absolutely no
interest in occupying the United States. Aside from that, there's also one other thing.
This isn't World War II. How are you going to get them there? How are you going to get the troops
from China or Russia or from wherever to here? The second the fleet moves, the second the fleet
moves, every country on the planet is going to know it. There's no surprise. You can't airdrop
millions of troops. But they can't say this. They have to keep that dream alive. They have to keep
their audience scared. They have to make up stuff about Russian troops operating under the UN or
whatever, and how they're going to invade the U.S. They don't have the numbers. It's not a real
thing. But that doesn't sell channels like that. They don't want to admit they were wrong in their
early assessments, and they want to keep that fear present. That's it. The days of having to worry
about somebody invading the United States, those are long gone. The fleets can't move. There's no
way to do it by air with the troops that you need. It's just not a thing, especially with the U.S.
military's current posture and the total information awareness they've obtained when you're talking
about fleets moving and stuff like that. It's just not a thing. But they have to repeat it. They have
to keep that fear up, otherwise those channels won't exist. That's what they sell more than the
freeze-dried food. That's what they sell. They sell fear. Rather than taking the time to
educate their audience on how to prepare for stuff that might actually happen, like climate change,
but doing stuff to get ready for that, well that doesn't involve a plate carrier and a rifle.
That's not cool. It doesn't give people an excuse to fantasize about the worst. That's the reason.
It's not profitable to tell the truth in that case. Not for them. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}