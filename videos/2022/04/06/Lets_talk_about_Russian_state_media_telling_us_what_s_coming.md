---
title: Let's talk about Russian state media telling us what's coming....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lcPIhiIAUoU) |
| Published | 2022/04/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russian state media released an article titled "What Should Russia Do With Ukraine?" outlining a plan for imperialism, suggesting Ukraine is not a civilization.
- The article repeatedly labels Ukrainians as Nazis to create a villain for propaganda purposes.
- The plan involves dividing Ukraine, with the eastern part becoming Russian territory administered by Russia temporarily.
- The western part not conquered by Russia is where the "haters" will reside.
- Russia plans to conduct trials to determine whether individuals collaborated with the government they dislike in the colonized country.
- There are intentions to re-educate the populace in the colonized region.
- Russia aims to experiment with the division line, taking as much territory as possible.
- Ukrainians are called upon to resist the division of their country, as Russia's goal is to push as far as they can.
- The methods and rhetoric used have historical parallels to the 1930s and 40s, raising significant concerns.
- The article's content is likely to influence decision-making by Western powers regarding the situation in Ukraine.


### Quotes

- "Ukraine, it's not really a civilization. It's not a country. They don't have a right to exist."
- "Denying the right to existence of a country is a pretty big deal and the methods that they are talking about using are, they have parallels in the 1930s and 40s."
- "It's worth the read. I suggest you take the time to do it because I have a feeling this is going to impact a lot of decision-making when it comes to Western powers and what they're willing to do."


### Oneliner

Russian state media's plan for Ukraine involves dividing the country, denying its right to exist, and using rhetoric reminiscent of the 1930s and 40s, raising alarming concerns for Western powers.


### Audience

Global citizens


### On-the-ground actions from transcript

- Raise awareness about the dangerous rhetoric and intentions outlined in Russian state media (suggested).
- Support organizations working to defend Ukraine's sovereignty and raise funds for aid efforts (suggested).


### Whats missing in summary

Implications of the article on the current political landscape and potential international responses.

### Tags

#Russia #Ukraine #Imperialism #Propaganda #WesternPowers


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about that article.
By now I'm sure it's everywhere. If you're not familiar, if you don't know what I'm talking about,
Russian state media released an article and the title of it is, What Should Russia Do With Ukraine?
And it lays out the plan. Now this is Russian state media saying this is the plan. This isn't,
you know, the West saying that this is Russia's intentions. This is the media outlet controlled
by the Kremlin and this is the messaging they're putting out. It's imperialism. Okay, well let's
just start there. I'll have the article linked down below with the translation. The general tone
of it, it starts off by saying, hey, Ukraine, it's not really a civilization. It's not a country.
They don't have a right to exist. Pretty dangerous rhetoric. Goes on to constantly call them Nazis
over and over again, creating that villain that you need in propaganda. And then it lays out the
plan. The plan is to divide the country and everything to the east becomes a Russian territory
that is going to be administered at least temporarily, you know, not forever, but for
some time, how long, we don't know. It could take a while, at least a generation. It'll be
administered by Russia. And the western part that they don't conquer, well that's where all the
haters will go. So all those people who had the lovely messages for me after I suggested that
Russia would engage in forced relocation of people, well now the Russians are saying they're
going to do it. It's pretty wild rhetoric. It is, again, it's coming from the official state
outlet and that's their plan. They claim to hold the right to engage in trials afterward to
determine whether or not somebody was evil and collaborated with the government that
they don't like in this country that they have decided to colonize and take over. This should
be a concern. This should be a concern for a lot of people. Generally, when countries go down
this road and start using this kind of rhetoric, especially in state outlets like this, there
aren't a lot of countries that turn back from this. There aren't a lot of countries that
suddenly wake up and say, oh wait, that wasn't such a good idea. There is discussion of how
they plan on re-educating the populace in the part that they're going to administer, the part
they're going to colonize. It's rough stuff. It is interesting to note that at this point
now, they do seem aware that they will not take the whole country and they point out
that the line that is going to divide it, well that's going to be experimental, which
means they're going to take as much as they possibly can. It's up to Ukrainians to stop
them because their goal is to divide that country and they will push it as much as they
can because they're saying it's going to be experimental. We'll see what areas will go
with us, what areas will go with the, I think they said, the Ukraine hostile to Russia.
But denying the right to existence of a country is a pretty big deal and the methods that
they are talking about using are, they have parallels in the 1930s and 40s. It's worth
the read. I suggest you take the time to do it because I have a feeling this is going
to impact a lot of decision-making when it comes to Western powers and what they're
willing to do. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}