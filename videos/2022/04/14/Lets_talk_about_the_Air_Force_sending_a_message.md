---
title: Let's talk about the Air Force sending a message....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2jkxnriQvKg) |
| Published | 2022/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Defense (DOD) is known for being on the cutting edge of social acceptance historically, desegregating before the rest of the country on many issues.
- Many states have passed bigoted anti-LGBTQ legislation, prompting the Air Force to offer medical and legal aid to personnel affected by these laws.
- The Air Force is willing to transfer personnel to different bases outside of states with discriminatory laws, indicating a potential impact on readiness.
- The Department of Defense is apolitical but focuses on the needs of the Air Force, suggesting that bases may cease to exist if they cannot fulfill their functions due to personnel relocation.
- Beau warns residents in states with discriminatory laws, such as South Florida or the Panhandle, about the possible economic consequences if bases like McDeal or Eglin shut down.
- Politicians pushing discriminatory laws are criticized for diverting attention from their lack of policy ideas and job performance, potentially causing economic instability in affected areas.
- Residents in states with discriminatory laws are urged to pay attention as the warning from the Air Force indicates potential mass transfers of personnel and negative impacts on economic activity.
- Failure of politicians to recognize the warning may imply the need for smarter political choices to avoid drastic consequences.

### Quotes

- "If you live in one of those states that has passed this ridiculous legislation, you need to pay attention."
- "You had better start paying attention, like your checkbook depends on it, because it does."
- "The number of people that have to be moved depends entirely on whether or not these ridiculous laws impact readiness."
- "You're not going to get a warning beyond this."
- "If the politicians in your state don't see that, you probably should have elected smarter politicians."

### Oneliner

The Air Force's warning about potential base closures due to discriminatory laws serves as a wake-up call for residents to pay attention before economic stability is at risk.

### Audience

Residents in states with discriminatory laws

### On-the-ground actions from transcript

- Pay attention to the impact of discriminatory laws and be prepared for potential economic consequences (implied).
- Advocate for smarter political choices to avoid negative outcomes (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential economic and social ramifications of discriminatory laws on military bases and surrounding communities.


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
some news coming out of the Air Force and how a lot of people need to pay attention
to it, even if they're not in the Air Force. It's news that isn't really surprising. It
shouldn't be if you are aware of the historical trends regarding DOD. But I have a feeling
it's going to surprise a lot of people. Contrary to the image that a lot of people like to
foster about the military, the Department of Defense has always been on the cutting
edge of social acceptance. Always. That may be hard to believe, but go back and look at
the dates. Look at when the military desegregated and then when the rest of the country did.
Look at any issue like this. You will find that DOD is always on the cutting edge. So,
if you've missed it, there have been a lot of states that have passed a bunch of bigoted
anti-LGBTQ legislation. The Air Force has sent out a notice telling its personnel that
the Department of Defense will help them with medical and legal aid. This is the part that
people need to pay attention to. And if need be, will transfer them to another base out
of that state. Okay, so even though the Department of Defense has always been on the cutting
edge when it comes to stuff like this, DOD is an apolitical organization. They don't
engage in grandstanding. They don't come out and make silly statements about what they're
going to do. This is as close as you get to a warning shot. If you live in one of those
states that has passed this ridiculous legislation, you need to pay attention. Because the second
this legislation impacts readiness, those bases are gone. Their functions will be transferred
elsewhere. The Air Force doesn't make political statements. The Air Force guides itself on
the needs of the Air Force. If they cannot put the people they want where they're needed
at a certain base, the needs of the Air Force dictate that that base doesn't need to exist.
And its functions can be transferred elsewhere. If you live in South Florida, for example,
I want you to picture the area around McDeal and what would happen to the economic activity
there if McDeal ceased to be. If you live in the Panhandle, you know, here in Crestview,
Graceville, Fort Walton, someplace like that. How far do you think your property value will
drop if Eglin shut down and transferred elsewhere? I assure you that if this legislation impacts
readiness in any way, shape, or form, those bases will disappear and your economic stability
will go with it. You have nobody to blame except for these politicians who, in an attempt
to maintain power, picked a group they felt they could other because they don't have any
policy ideas. They just want to find somebody to get you looking down at, kicking down at,
so you don't notice that they're not doing anything to help you. They're not doing their
job. They're just finding something to stir outrage, to keep you focused on something
else so they can get away with whatever they want. If you are in one of these states that
has passed these kinds of laws, you had better start paying attention, like your checkbook
depends on it, because it does. You're not going to get a warning beyond this. This is
the warning. This is the Air Force flat out saying, we're going to have to move people
based on this. The number of people that have to be moved depends entirely on whether or
not these ridiculous laws impact readiness. If this, I'm telling you, the second they
do, you're going to see mass transfers. You're going to see the economic activity in those
areas plummet. This is the warning shot. If the politicians in your state don't see that,
you probably should have elected smarter politicians. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}