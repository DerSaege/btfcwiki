---
title: Let's talk about Ukrainians in Melitopol....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xqf-vKXPLN4) |
| Published | 2022/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent focus on expected Russian advance in the East overshadows news from other areas.
- Maritopol reports over 70 Russian troop casualties in three weeks to Ukrainian resistance.
- Ukrainian resistance likely comprises well-organized or semi-organized groups conducting surveillance and operations at night.
- Resistance groups are becoming better armed by keeping equipment from fallen troops.
- Low-level harassment by the resistance undermines Russian military authority.
- Russia must acknowledge and address the losses to avoid losing the city.
- Options for Russia include ceding the night to resistance or doubling up patrols.
- Russian troops live in constant fear and vulnerability, akin to a horror movie.
- Continuous losses are unsustainable for the Russian military.
- Resistance operations behind Russian lines create paranoia, demoralization, and logistical challenges.

### Quotes

- "Never say I'll be right back because you won't."
- "They're signing on for a very long, very nasty conflict."
- "These types of resistance operations change the course of wars."
- "Going after civilians as punishment never works."
- "It always strengthens the resistance."

### Oneliner

Recent focus on expected Russian advance overlooks Ukrainian resistance's effective low-level harassment, which undermines Russian military authority and may change the course of the conflict.

### Audience

Military analysts, policymakers, activists

### On-the-ground actions from transcript

- Support Ukrainian resistance efforts through aid and resources (implied)
- Raise awareness about the impact of resistance operations on the conflict (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the strategic implications of Ukrainian resistance and the challenges faced by the Russian military in occupied territories.

### Tags

#RussianMilitary #UkrainianResistance #Conflict #Occupation #StrategicImplications


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about how taking isn't holding.
Something we have mentioned over and over again,
and something the Russian military is beginning to find out.
You know, for the last week or so, most eyes have been completely focused,
totally focused, on the East, on the area where the expected renewed Russian advance
is going to start from.
And it's important to watch there, no doubt.
However, people seem to be overlooking news coming out of other areas.
Some interesting news came out of Maritopol today.
It was reported that over the last three weeks,
Russia has lost more than 70 troops to Ukrainian resistance in the town.
This is a town that Russia, quote, controls.
70 troops.
70 troops in three weeks?
That's possible.
That's doable.
But numbers like this tend to be inflated.
So let's cut it in half.
Just cut it in half.
35.
10 a week.
Russia can't do that.
That's not tenable.
That isn't tenable.
So what is the Ukrainian resistance doing right now?
More than likely, this is either one very well-organized group
or a bunch of semi-organized groups operating independently.
They probably have people who are just doing surveillance,
who never touch a weapon of any kind.
They're just figuring out where the patrols are going to be.
And then they have the other crew that shows up and does the work.
They're doing this at night when the Russians are out doing
their little patrolling activities to assert their authority.
This undermines their authority greatly.
It's worth noting that while I can't find this in any of the reporting,
I'm certain that it's occurring.
This resistance group or these resistance groups
are better armed now because every one that they send home,
they're keeping some of that equipment.
It's worth noting a lot of people are pointing to the fact
that some of them were sent home via knife.
I wouldn't make too much of that.
There are some people who are suggesting that means that they are under-armed.
They're doing them at night.
It could just be quieter that way.
This is the type of low-level harassment that breaks militaries.
It's also the kind that's really hard to stop.
So that's what the Ukrainian resistance is doing.
That's how they're achieving success.
What can Russia do to counter it?
First, they have to acknowledge what's happening,
which by now they probably have.
Then they have to figure out a way to stop the losses
because that's what matters at this point.
They can't sustain those losses.
If they do, they will lose the city.
So option one is they cede the night to the Ukrainian resistance,
which may be what they do.
And it's actually a pretty horrible move.
If that occurs, yes, they will no longer lose people
who are out on foot patrols.
Fact.
However, every time they leave,
they have to look as they drive through in the daytime.
Was that box there by the side of the road yesterday?
And it doesn't actually do any good
because they just end up losing more in the daytime
because the resistance team prepared at night.
Another option is to double up on patrols,
increase the size of the patrols.
There are two ways they can accomplish this.
One is that they can use more troops.
That means less troops capable of fighting or defending
because they're awake at night on patrol.
The other option is to run half as many patrols,
which in essence is just ceding the night
to the Ukrainian resistance.
For the average Russian troop, they're in a horror movie now.
They have to live by those rules.
Never go anywhere alone.
Never say I'll be right back because you won't.
That's what they're living in.
And that's what they will be living in
as long as this campaign continues.
And it is hard to disrupt something like this
because if you are the occupying force,
the opposition, the resistance, they can hit and miss.
They could spend, you know, days planning something
and then just be like, yeah, never mind.
This doesn't feel right and walk from it.
If you're the occupation, you can't ever miss
because every time you do, you lose somebody.
These losses aren't something
that the Russian military can sustain.
If this goes on while the reservists are being retrained,
they have to send a whole company of the reservists there
just to fill in for the people they've lost,
and that's not going to stop the problem.
These types of campaigns are likely happening,
I don't want to say everywhere,
but in a lot of locations behind Russian lines.
They're occurring, and they will have a marked effect.
It creates paranoia in the troops.
It demoralizes them.
They lose material.
They lose personnel.
They can't maintain the fight.
It's hard to take the offensive.
It's hard to regain the initiative
if you're being hit from behind,
and this has been the thing since the very beginning
when we were talking about troop numbers.
They don't have the troops necessary
to hold what they're trying to take.
They're signing on for a very long, very nasty conflict.
So while we still need to watch what's going on in the East,
we need to try to pay attention to this stuff as well
because this stuff could become crucial
if Russian command doesn't send replacements to them,
which I think is really likely
because Russian command has kind of promised
everything is going to go into this new offensive.
If they don't send them reinforcements,
and this continues, as the losses mount,
the town becomes something that could be retaken pretty easily.
These types of resistance operations,
as small as they may seem, as minor as they may appear,
as often as they're overlooked, they change the course of wars.
And given the fact that it's not getting a lot of media attention,
given the fact that we haven't seen Russia
engaging in the activities we would expect,
they're probably not paying attention to it either,
which means these activities will change the course of this war.
Now, Russia being the less than professional military
it has shown itself to be,
they at some point will probably attempt to engage
in an activity against civilians
to discourage the resistance groups.
Historically speaking, that literally never works.
It never does what it's supposed to.
It almost always strengthens the resistance group.
It creates additional groups
that are out there operating independently,
making it even harder for occupation forces.
But given Russia's behavior so far,
that's probably how they're going to attempt to deal with it
if they make any effort at all.
That's probably how they'll try to engage
in a counter campaign against this.
And undoubtedly, it will fail.
It doesn't work. It's never worked.
Going after civilians as punishment never works.
It always strengthens the resistance.
But that's likely to be their move.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}