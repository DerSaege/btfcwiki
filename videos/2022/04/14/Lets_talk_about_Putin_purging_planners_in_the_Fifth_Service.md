---
title: Let's talk about Putin purging planners in the Fifth Service....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=liCWOXetZFk) |
| Published | 2022/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Fifth Service was created by Putin in 1998 to conduct intelligence operations in former Soviet republics and maintain Russia's influence there.
- Putin is currently purging the Fifth Service, with the boss being placed under house arrest and around 150 officers being dismissed due to the invasion of Ukraine.
- The officers were allegedly fabricating reports about funding non-existent resistance groups in Ukraine and pocketing the money.
- This behavior hindered Russia's war planning and may have led to intelligence failures in the invasion of Ukraine.
- While the rumor about fake sources and resistance groups cannot be verified, it explains a lot of the questionable decisions made by Russia during the war.
- The purge of the Fifth Service could be a liability for Russia as they lose experienced intelligence officers familiar with the region.
- The disciplinary actions taken could lead to micromanagement of new officers and potential paranoia within the intelligence agency.
- Putin might be taking the purge personally, as the Fifth Service was his creation and its failures are seen as a betrayal of his vision.
- The rumor about fake reports and corruption within the Fifth Service, if true, could have significantly impacted Russia's actions in the war.
- Beau believes that the speculation about fake reports and resistance groups is plausible, even though it cannot be proven.

### Quotes

- "They were paying off sources that didn't exist and just pocketing the cash."
- "A lot of their planning starts to make a whole lot more sense if you factor in the existence of a partisan group loyal to Russia operating inside Ukraine."
- "It looks like corruption and greed undermined a war effort, a war effort that probably would not have occurred if those reports had never been made."

### Oneliner

Putin's purge of the Fifth Service reveals alleged fabrications of reports on non-existent resistance groups in Ukraine, impacting Russia's war planning and credibility.

### Audience

Intelligence analysts, policymakers

### On-the-ground actions from transcript

- Investigate and monitor the geopolitical implications of intelligence agency purges (suggested)
- Support transparency and accountability within intelligence agencies (suggested)
- Advocate for ethical practices and oversight in intelligence operations (suggested)

### Whats missing in summary

Insights into the broader implications of potential corruption and misinformation within intelligence agencies. 

### Tags

#Putin #Intelligence #Russia #Ukraine #Corruption


## Transcript
Well, howdy there, Internet of People.
It's Beau again.
So today, we are going to talk about the Fifth Service.
And just for the sake of continuity,
that's what we're going to call it.
Understand, like most intelligence or special
operations groups, the real name changes.
But in common conversation, it's the Fifth Service.
What is the Fifth Service?
Something Putin put together back in 98, I think.
Its sole purpose for being was to engage in intelligence
operations in former Soviet republics
and keep them in Russia's sphere of influence.
For those who are just dead set on the idea
that Russia doesn't engage in this type of activity,
please understand they had what amounts
to an entire directorate whose whole purpose in being
was doing the thing that a lot of people
don't believe they were doing.
The reason we're going to talk about them
is because Putin is in the process of purging them.
The boss over the group was placed on house arrest.
He has now been sent to a notorious prison,
something comparable to sending an American to Gitmo.
That is where the boss of the Fifth Service
is currently residing.
On top of that, they have dismissed around 150 officers
from this crew.
And it's all because of the invasion of Ukraine.
Now, there are the normal intelligence failures
that occur.
And we know that a bunch occurred in Ukraine.
We have the whole video talking about that.
So that is part of the reason they're being canned.
That's why this is happening.
I would point out that a mass firing in the intelligence
agency responsible for gathering the information
to plan this little adventure is a clear sign that Russia's,
oh, everything's going according to plan.
Well, that's not true.
So everything I've said up till now, fact, verifiable,
reportable, all of that stuff.
Now we're going to get to the rumor,
because there is a rumor that is going along with this.
And while it is just a rumor, it makes a whole lot of things
make sense.
So the rumor says that a lot of these officers
had reported that they were funding resistance groups
inside Ukraine that were loyal to Russia.
And the reality is those groups never existed.
They were just pocketing the cash.
They were paying off sources that didn't exist
and just pocketing the cash.
This is the rumor.
And that sounds wild, but it's really not,
because I mean, what it costs to arm a resistance group,
that's a lot of money.
It's so much money that it's no longer just money.
It turns into motive.
And it's cash.
And if you are under the assumption
that there's no way Putin would be crazy enough
to invade Ukraine, why not?
It's not like they'd ever be used, right?
So the rumor is this was very widespread,
this habit of creating sources and groups,
keeping the cash, sending up reports of information
that's just literally made up, and sending that to the Kremlin.
So it's a rumor, but it's one I believe,
because it makes so much just make sense.
That airdrop I constantly make fun of,
because there's no way it would work.
You know how it might be successful
if you had an armed resistance group
on the ground waiting for you?
A whole lot of their planning starts
to make a whole lot more sense if you factor
in the existence of a partisan group loyal to Russia
operating inside Ukraine.
That actually may be why their plan was so off
and why they attempted things that didn't make any sense,
because they believed they would have support on the ground
that literally didn't exist.
It was just made up.
So what does this mean from here?
While it does in some ways make sense
to fire all of these people who lied,
it's also a huge liability for Russia.
It hinders further war planning,
because even though they were skimming money,
even though they were engaged in this dishonest behavior,
they've actually been there.
They are intelligence officers who
are familiar with these countries, so much so
that they were able to manufacture, fabricate reports,
if you believe the rumors, that fooled the Kremlin.
They might have gone too far in the disciplinary procedures
here, because they may have just robbed themselves
of the entire intelligence corps that could have, in theory,
at least fixed some of this.
But now they have to bring in new people.
And because of the rampant paranoia that is certainly
going to occur now, they're going to be micromanaged.
And they're not going to do as well.
There's also a high likelihood that Putin
is taking this very personally, because this is his baby.
The Fifth Service, that was his baby.
And it was his child, all grown up, who turned on him
and destroyed his dreams.
Again, the verifiable part, this purge
has occurred, at least 150 officers.
The boss has been sent to a really bad prison.
Understand the prison that he was sent to.
This is a punishment worse than being sent off to Siberia.
He's having a really bad time right now.
All of that is verifiable.
And Putin blaming them for the intelligence failures,
that's verifiable.
The rumor is the part about the fake sources and fake
resistance groups.
But man, does it fit.
It explains so much.
And there's no way for us to verify this.
In fact, even if it's true, we'll never find out.
This will be one of those things that the Russian national
government is so embarrassed about that they'll hide forever.
Because if it's true, all of those losses,
they can be directly attributed to this,
to these fake reports and to the Kremlin falling for them.
It would make Russia look horrible.
So they would never let this see the light of day.
But it sure does add up, more so than anything else
Russia has done in this entire war.
In fact, if you factor in what was
said to be in these reports, a lot of their moves
that just didn't make any sense start to make sense.
It looks like they were played by their own people.
It looks like corruption and greed
undermined a war effort, a war effort that probably would not
have occurred if those reports had never been made.
Because those of us on the outside,
we understood the problems with trying
to hold this land with the troop numbers they had.
Well, it becomes easier if you have a resistance group that's
on your side, that's there that's going to help you.
Everything starts to add up if you factor
in these mythical partisan groups loyal to Moscow.
So there's the dividing line between what's
verified and the rumor, the speculation.
I will say that I fully believe the speculation.
I can't prove it, but yeah, that definitely
seems like what happened.
Totally tracks.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}