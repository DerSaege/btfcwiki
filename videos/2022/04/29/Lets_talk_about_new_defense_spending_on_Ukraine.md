---
title: Let's talk about new defense spending on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7jR0OffmpV4) |
| Published | 2022/04/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses aid going to Ukraine and a talking point about the defense industry making money from it.
- Demonstrates through numbers that the aid to Ukraine isn't significant for the defense industry's bottom line.
- Mentions that the defense industry will start making real money as the new aid package for Ukraine is $20 billion.
- Explains that although some of the aid is old stuff, the defense industry will benefit financially from providing it.
- Notes that the defense industry will profit significantly from the aid, indicating a shift in policy influence.
- Speculates on the defense industry influencing policy decisions through potential incentives like building factories in senators' hometowns.
- Suggests that the defense industry is now making substantial profits due to the aid package increase.
- Implies that the defense industry's influence may have played a role in the significant increase in military aid to Ukraine.
- Concludes that while Ukraine needs the equipment, the US defense industry is taking the lead in providing it.
- Leaves viewers with a thought-provoking perspective on the situation.

### Quotes

- "It is blood that makes the cash grow green."
- "The defense industry is now going to make a dump truck full of cash."
- "Perhaps we build a factory in your hometown, something along those lines."
- "Y'all have a good day."

### Oneliner

Beau addresses the increase in military aid to Ukraine and speculates on the defense industry's significant financial gains, implying potential policy influence.

### Audience

Advocates for Accountability

### On-the-ground actions from transcript

- Question policy decisions influenced by industries (implied)
- Stay informed about aid distribution and potential motives behind it (implied)

### Whats missing in summary

The full video provides a detailed breakdown of the aid situation in Ukraine, including implications of defense industry involvement and policy influences.

### Tags

#Ukraine #Aid #DefenseIndustry #USGovernment #PolicyInfluence


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about aid going to Ukraine.
In a recent video, I addressed a talking point about the defense industry and the idea that
they were pushing the aid to Ukraine so they could make a bunch of money.
And I went through the numbers and demonstrated very clearly that those numbers, the amount
of aid that was being delivered to Ukraine, those aren't defense industry numbers.
Tiny tiny tiny amounts that really in the grand scheme of things to their bottom line,
it's a tax write-off, not a motivation to make a call to one of their senators, right?
I also said yet.
That's not happening yet.
And described what it would look like if they started doing that sometime in the future.
It is now the future.
The defense industry definitely has the US government saying it's dangerous out there.
Take this.
When I made that video, the total amount of aid, military aid that had been provided to
Ukraine was about $3 billion.
The new package is $20 billion.
That's the kind of ballooning I was talking about in that video.
Now I know some people are going to say, well, a lot of that is old stuff that the US has
on the shelves and is now going to provide.
Yeah, that's true, but it's also going to have to be put back on the shelves and the
defense industry is going to start making some real money here.
So now that talking point, yeah, it's legitimate.
The defense industry is now going to make a dump truck full of cash.
After all, it is blood that makes the cash grow green.
So from here, the question is, did the defense industry dictate the policy?
No.
The aid was going.
That's something that was in line with US foreign policy interests and now the defense
industry is like, hey, since you're doing this, Senator, maybe you should do this as
well.
Perhaps we build a factory in your hometown, something along those lines.
I don't actually have a leaked phone call from one of the big defense industry executives
to a senator that they own, but given the fact that it was, this isn't happening, this
is what it would look like if it was happening, and now it looks exactly like that, that's
probably what's happening.
So does this mean that Ukraine doesn't need this equipment?
No, it doesn't.
It just means that the US defense industry, rather than allowing other countries to provide
it and make the cash, they're going to do it.
It looks like the US is taking the lead when it comes to providing the higher end stuff
that costs a bunch of money and providing it in bulk.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}