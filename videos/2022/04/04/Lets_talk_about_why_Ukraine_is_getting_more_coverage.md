---
title: Let's talk about why Ukraine is getting more coverage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2nRyE_JGdGM) |
| Published | 2022/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why the conflict in Ukraine is receiving more coverage compared to other conflicts around the world.
- Notes that some individuals question the coverage with less than ideal motives, seeking to divert attention from the conflict.
- Addresses the racial aspect, pointing out that Ukraine's population being mostly white and located in Europe contributes to the increased coverage.
- Mentions that the conflict was initially seen as the beginning of a new Cold War involving the United States, Russia, and China.
- Acknowledges that the conflict directly endangers energy supplies to many parts of the world, particularly Europe.
- Talks about the unexpected turn of events in the conflict, likening it to a major upset in sports.
- Emphasizes the importance of Russia's involvement as a nuclear power in direct competition with other nuclear powers.
- Expresses the significance of the conflict in terms of criminal activities during war and the potential for nuclear fallout.
- Stresses the importance of understanding Russia's activities and intentions leading up to the conflict.
- Shares personal reasons for covering the conflict extensively, citing expertise in this particular type of conflict.

### Quotes

- "It's race."
- "That is the most important reason."
- "Most times it's a combination of things that leads to any particular behavior."
- "Y'all have a good day."

### Oneliner

Beau explains why the conflict in Ukraine stands out in global coverage, touching on racial dynamics, geopolitical implications, and the looming threat of nuclear powers overshadowing other motives.

### Audience

Global citizens

### On-the-ground actions from transcript

- Analyze Russia's activities and intentions leading up to the conflict (suggested)
- Guard against assigning a single motive to conflicts and behaviors, considering multiple factors (implied)

### Whats missing in summary

Insights into the complex web of motives and dynamics influencing media coverage of conflicts around the world.

### Tags

#ConflictCoverage #Ukraine #Russia #Geopolitics #NuclearPowers


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna answer a question.
Why is this one different?
Why is it getting so much more coverage?
And this is a question that's been asked a lot.
Now, to be honest, most, not all, most,
of the people that I have seen ask this question
are doing it out of less than ideal motives.
It's more of a what about-ism, trying to get attention off of this conflict now that the
side they perceive as their team is losing.
The thing is, it's most.
It's not all.
There are people who are genuinely asking this, and it doesn't matter if it was all.
Because even if the person asking it has less than ideal motives, the question's there,
And you can't argue the fact that Ukraine's getting a whole lot more coverage than a lot
of the other conflicts around the world.
So what are the reasons?
One we've talked about on the channel before, and it's the obvious one, it's race.
It's race.
Ukraine's full of white people and it's in Europe.
And that leads to more coverage.
That's why we had newscasters say just horrible stuff.
There were RPGs like it was Afghanistan.
Can't believe that.
As if the people in Afghanistan somehow deserved to have RPGs fired at their buildings.
Or these people look just like you or me, forgetting that the US is kind of a fruit
salad with a whole bunch of different colors and not all of them look like the people in
Ukraine, but those comments in the early days of the conflict by newscasters, people who
should know better than to say stuff like that, it showed that that's definitely part
of it.
That's definitely part of the reason.
But that's not the reason this conflict is probably the most important conflict in the
world in the last half century.
And we'll get to that.
There are also other reasons.
This as it started, this was supposed to be the opening salvo in the near peer contest
between the United States, Russia, and China.
It was the beginning of the new Cold War, it was going to set up that multipolar environment
that was going to define foreign policy for the next few decades.
Now to be clear, at this point, it doesn't look like that's how it's going to shape
up.
It looks like it's going to be the US and China, and Russia is going to be a major power,
but they're going to fall in behind China.
That's part of it.
It's the opening gambit to a whole new phase of foreign policy, to a new world order.
To go ahead and throw that term out there.
Then there's the fact that this conflict directly threatens the energy supply to a lot of the
world and a whole lot of Europe.
Europe being the place where a whole lot of news outlets are based out of.
So it's going to get more coverage because of that.
Then there's the curiosity aspect because it's not going the way most people thought
it was going to go.
If you don't understand the way militaries are viewed, it's hard to explain how much
of an upset this is.
To put it into a sports analogy, a team that was headed to the Super Bowl decided to play
an unranked high school team, and here we are at halftime and they still haven't scored.
that aspect of it. It was not foreseen to play out this way. And the fact that another
pro team showed up to give the high school team pads and helmets shouldn't have changed
the outcome of the game that much. You know, Russia's military was very highly ranked
in view, through the lens of being able to project force and being able to accomplish
goals, what's occurring in Ukraine calls all that into question.
So there's that aspect of it.
Now you have the criminal activities in war that are definitely going to increase coverage
even more.
But none of these are the reason that this is the most important conflict the world has
seen in half a century.
This is Russia, a country with a massive nuclear arsenal in direct competition with a bunch
of other nuclear powers.
That is the most important reason.
That's why it's getting so much coverage.
We haven't seen something like this in a very, very, very long time.
Think about on this channel, in all of the conflicts I've covered, I've never had enough
requests from people to make not one, but two videos talking about nuclear fallout.
That has a lot to do with it.
Because of the tensions, because of the stakes, people are paying more attention to it, okay?
And now you have my reasons, because we're talking about the media in general here, you
have mine.
I think it's really important to take a good hard look at Russia's activities in the lead
up, what they were planning, what they intended to do, and hold that mirror up.
I think that's incredibly important because it looks a whole lot different when you see
another country doing it.
And then you also have the fact that there's a whole lot of people in the United States
who daydream about civil conflict.
I think it's important for people to understand what it's really like.
And for most of the people who view that as a pastime, as something to look forward to,
the race factor matters a lot.
Because it's happening in Europe, well, that's a little bit different.
Because most of the people who have the idea that that would somehow be a good thing, most
of them are racist.
So when it happens in other countries, they're the sort that would be like, well, it's Afghanistan.
Of course they have RPGs shooting at their buildings.
The fact that it's happening in Europe makes it a little bit more real to that particular
crowd.
And I think highlighting that would do everybody some good.
And then there's the final reason when it comes to me.
My area of study is this exact type of conflict.
This is what I know about.
I'm covering it a lot for the same reason somebody who studied pandemics their entire
life spent a lot of time covering the public health issue.
So those are the reasons that you're seeing a whole lot more coverage.
Race definitely has a lot to do with it.
I do.
right below the fact that we're talking about nuclear powers staring at each other.
Previous conflicts of this sort didn't bring them close geographically.
In Vietnam, you had nuclear power supporting one side and another side directly involved.
You had stuff like that.
But it wasn't quite so pronounced as it is now, wasn't quite so close in geography.
That's what makes this conflict so important.
Make no mistake about it, the other conflicts all over the world, they'll be viewed in
a less important light than what's happening in Ukraine because of the geopolitical fallout.
Right after that, I think the reason it gets more coverage is because it's white people.
And then all of these other reasons fall in behind that.
One of the big things you should probably try to guard against is trying to assign a
single motive to anything. Most times it's a combination of things that leads
to any particular behavior and if you come up with a single motive and it's a
good one like race in this case you might overlook the whole nuclear war
aspect of it because race that's a solid motive that explains why this coverage
is happening the way it is.
That's why the media is portraying it the way it is.
That's why it's getting around the clock coverage.
But if you just accept that as the only motive, you miss the other ones.
And in this case, I think the nukes might actually overshadow race.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}