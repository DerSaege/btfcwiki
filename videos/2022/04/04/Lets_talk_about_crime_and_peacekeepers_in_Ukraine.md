---
title: Let's talk about crime and peacekeepers in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ef0P6YeBOps) |
| Published | 2022/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ukrainian forces found evidence of horrific acts as they liberated towns formerly occupied by Russia.
- The comparison was made to the Srebrenica massacre during the Bosnian War, illustrating the severity of the situation.
- The idea of Western peacekeepers in Ukraine has been proposed, but there are significant challenges.
- Peacekeeping forces are not meant to operate in active war zones; inserting them may not be effective.
- The West's misunderstanding of peacekeeping versus peacemaking complicates the situation.
- Peacekeepers in Ukraine could potentially provide a safe haven for refugees in Western Ukraine.
- Direct confrontation with Russia through peacekeeping operations could escalate into a full-scale war.
- The options remain the same: do nothing, support Ukraine externally, or go to war with Russia.
- Engaging Russia directly still carries significant risks and challenges.
- Providing more assistance to Ukraine as Russia withdraws could be a more feasible approach.
- Advocating for peacekeeping operations may actually be advocating for war under a different name.

### Quotes

- "Peacekeeping force, not a peacemaking force."
- "A peacekeeping operation is just a step to going to war."
- "Advocating for peacekeeping operations may actually be advocating for war under a different name."

### Oneliner

Beau provides context on proposed Western peacekeepers in Ukraine post-Russian occupation, cautioning against the potential risks of escalating conflict.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Advocate for providing more assistance to Ukraine as Russia withdraws (implied)
- Support efforts to help refugees in Western Ukraine (implied)

### Whats missing in summary

In-depth analysis and historical context of proposed Western peacekeeping in Ukraine.

### Tags

#Ukraine #Russia #Peacekeeping #WesternResponse #SrebrenicaComparision


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the news that
came out over the weekend.
We're going to talk about the response to that news
and some ideas that are being floated because of that news,
some things that people are kind of proposing
to test the waters.
And we're going to provide some much-needed context
to a certain concept that is being suggested.
If you have no idea what I'm talking about, over the weekend as Ukrainian forces liberated
towns that were formerly occupied by Russia, they found evidence of some pretty horrible
stuff.
Some of it is horrible stuff that is the norm with conflicts like this.
is way outside of the norm. The immediate comparison that was made was to
Srebrenica. Now, if you're not familiar with this, it happened during the
Bosnian War, I want to say July in 95, and when you're talking about horrible
events, things that militaries do to civilians and stuff like that, whatever
it is you're picturing, that happened in Srebrenica en masse and a whole bunch of
stuff that you are probably well too well adjusted to imagine. If you're going
to look into this, I strongly suggest you start with a summary of events before
you get into any primary source stuff. It gets real dark real quick. That's the
comparison that's made. Because of that in the West, especially in the US, the
idea of putting in peacekeepers has been floated. Now this is troops on the ground
in Ukraine. I'm a huge supporter of the concept of peacekeeping missions. I've
talked about it numerous times in the past. The United States should be trying
to move from the world's policemen to the world's EMT, and this would be a step
in that direction. There are a number of problems in this particular instance.
First, we don't have a trained peacekeeping force that doesn't exist.
What we have done in the past is press military units into a peacekeeping role.
If there's anything this conflict has taught us, should have taught the world, it's the
value of training.
We don't actually have people trained for this.
Aside from that, the West in general misunderstands the concept, and it's weird because it's
in the name.
It's a peacekeeping force, not a peacemaking force.
You can't insert a peacekeeping mission into the middle of an active war zone.
It doesn't work.
The West has tried repeatedly.
It does not work.
There are roles that could, in theory, be handed to a peacekeeping force in Ukraine
right now.
None of them are what people are imagining, though.
It's more like taking a small section of Western Ukraine and putting peacekeepers there and
allowing that to be the place refugees go.
The refugees that would be fleeing the bad stuff, the horrible stuff that's happening
far far away on the front.
The peacekeepers can't go to that.
The peacekeeping force would not stop what happened north of the capital.
I am 100% certain that peacekeepers in Trebinica wouldn't have been able to stop it.
People in Europe right now are going...
Oddly enough, that doesn't really get talked about in the US, just so you know, that part
of the story gets left out, which is why people made this comparison and then were like, hey,
let's send in peacekeepers.
If you don't know, there were peacekeepers in Trebinates of when that happened.
That is not the tool.
That's not what that tool is for.
What people are imagining is a UN or NATO force to go in there and make sure that this
doesn't happen again.
Those are called soldiers.
A peacekeeping operation of the sort people are talking about, those are just soldiers.
Peacekeeping on the ground is much like a no-fly zone.
It's going to war with Russia with extra steps.
people are envisioning are NATO forces, UN forces, whoever, in direct confrontation and
conflict with Russia to stop these events.
That just leads to war with Russia.
That's not actually a peacekeeping operation because there's no peace yet.
Not to put too fine a point on this, the math hasn't changed.
The options are the same as they have always been.
Do nothing, assist Ukraine from outside to whatever degree, or go to war with Russia.
Those are the options.
There isn't going to be an event that really changes that math.
A peacekeeping operation is just a step to going to war.
It's slow walking the process of going to war.
Because the second those peacekeepers come in armed
contact with Russian troops, it's on.
Because somebody's calling for air support.
And then it just escalates from there.
there.
What you're looking at is certainly something that should motivate the West.
When you're talking about engaging Russia directly, everything that was there in the
beginning, it's still there.
didn't go away because they did this horrible thing. It is still there. There
are also people saying that this could be used as reasoning to provide even
more assistance. That makes a whole lot more sense to me. For all the reasons
we've already covered in the past and it's more feasible now because Russia is
withdrawing. They're moving further and further to the east, making the logistics
of getting stuff to Ukrainian forces a whole lot easier. Putting troops on the
ground is going to war with Russia. It's just like a no-fly zone. If you want to
advocate for going to war with Russia, that's fine, but don't trick yourself
into thinking that, well, I'm really just advocating for a peacekeeping operation.
Because what people want is for incidents like what happened north of the capital to not happen again.
In order for them to do that, they're not there on a peacekeeping mission.
They're there acting as soldiers, and it's just going to war with Russia under a different name.
name. So anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}