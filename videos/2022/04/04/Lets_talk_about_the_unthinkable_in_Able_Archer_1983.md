---
title: Let's talk about the unthinkable in Able Archer 1983....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A7pEuZDW3aw) |
| Published | 2022/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a close call with nuclear war during the Cold War in 1983 involving a NATO exercise called Able Archer.
- NATO's exercise simulated a nuclear war scenario, with generals participating and encrypted communications used.
- In the exercise, NATO decided to enact a nuclear signal by choosing to destroy the capital of Ukraine, escalating tensions with the Soviet Union.
- The Soviets misinterpreted NATO's exercise as a cover for a potential first strike against them, leading to heightened alerts and loading of live nuclear weapons on planes.
- A critical Air Force officer suggested de-escalation to prevent further tensions from spiraling out of control.
- Despite being a simulation, the exercise revealed how close the world came to a nuclear catastrophe in 1983.
- The public only learned about this close call years later, underlining the secrecy surrounding such events during the Cold War.

### Quotes

- "It takes both sides to make a mistake at the same time."
- "You wouldn't be watching this video right now because none of us would be here."
- "It was way too close for comfort."

### Oneliner

A recount of a chillingly close encounter with nuclear war during the Cold War in 1983, underscoring the delicate balance that prevented a global catastrophe.

### Audience

History enthusiasts, peace advocates

### On-the-ground actions from transcript

- Join peace organizations to advocate for nuclear disarmament (implied)
- Educate others on the dangers of nuclear weapons (implied)

### Whats missing in summary

Insights into the critical role of communication and de-escalation in averting potential nuclear disasters.

### Tags

#ColdWar #NuclearWar #AbleArcher #DeEscalation #PeaceAdvocacy


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about one of the many
close calls we've had with the unthinkable.
In a few videos, I've said that normally
when we're actually close to the unthinkable happening,
the public doesn't know.
And on the live stream recently, somebody asked
and was like, hey, can you tell us one of those stories?
And yes, yes we can.
We'll talk about one.
There's a few.
I may do another one later.
But this is one of the more interesting ones
and one that's kind of easy to research
if you want to learn a little bit more about it.
It happened in 1983 and NATO had just finished
a large scale conventional exercise.
I want to say that one was called Autumn Forge.
And they followed it up with an exercise called Able Archer.
And Able Archer was a role-playing thing.
They were training to release nuclear weapons.
So all of these generals are involved.
And they're wargaming a Soviet invasion of Western Europe.
They wanted it to be real, so they
used encrypted communications.
They involved heads of state in this training exercise.
Bases around the world would respond to the orders of the people playing the war game.
So planes would get loaded and stuff like that.
The original scenario was that, I want to say, Soviet tanks rolled across Yugoslavia
and caught NATO by surprise.
So NATO fell back, and they could never really reverse it, and they just kept getting beat.
So eventually, the people playing this war game decided to enact a nuclear signal, which
if you want to know how crazy the Cold War was, there was the belief that you could avoid
major nuclear incident by destroying a major city with a nuclear weapon and
that the opposition would rate that as a signal. In the war game, NATO elected to
destroy the capital of Ukraine. The war game continued from there and the
The signal was not received well by the Soviet side in the training exercise, and a back
and forth occurred, and the entire world was destroyed in the war game.
So that's what happened in the war game, in the training exercise.
Meanwhile in the real world, the Soviets were concerned because, I want to say it was medium
range missiles. NATO was deploying medium-range missiles around the same time and they kind
of believed that the whole training exercise was actually a cover to engage in a first
strike against Warsaw Pact. This was further complicated by the fact that the KGB had their
case officers, their spies, operating under this policy that allowed them to report observations.
They couldn't provide estimates or assessments or analysis.
They could only report what they saw.
So, hey, this plane was loaded.
And that's what the Soviet leadership got.
So they became incredibly worried.
They loaded live nuclear weapons onto planes and stated an incredibly high alert the entire
time.
NATO didn't know any of that was happening.
They had no clue.
And eventually as things started to get noticed, there was one Air Force officer who was like,
Maybe we should just stop escalating on our side and see what happens, and the Soviets
calm down.
That occurred in 83.
The presidential finding on it was written in 1990, and I don't think the public found
out about it for like another decade or two.
But we were pretty close at that moment.
had one side who was ready to go. And if you've heard me talk about this before,
you know, I keep saying it takes both sides to make a mistake at the same time.
In that case, the Soviets made a mistake. They misread NATO's intentions. If NATO
had made a mistake at the same time and misread the Soviet intentions, it's safe
to say you wouldn't be watching this video right now because none of us would be here.
Now there have been some academics who've looked at this information and said, well,
it really wasn't that close.
I've read the report, it seemed pretty close to me.
It definitely appears that Soviet leadership was incredibly concerned with this exercise.
And while maybe it wasn't as imminent as the presidential finding made it out to be,
it was way too close for comfort.
So this is one of those things, and this pattern repeats in pretty much every one of these
stories.
If you know about it, if there are tensions that are heightened and they're in the news,
odds are we're probably not that close.
And you may have been closer in your own lifetime because I know there's probably quite a few
of y'all who were alive in 83.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}