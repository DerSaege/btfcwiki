---
title: Let's talk about Betty White's weird connection to Thomas Jefferson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AEroqxcuY40) |
| Published | 2022/04/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how Americans, as a young country, tend to view history as ancient and far removed.
- Shares a personal anecdote of meeting people who were alive in the 1800s to illustrate the proximity of historical events.
- Breaks down the timeline between Thomas Jefferson, Harriet Tubman, Ronald Reagan, and Betty White to show how recent historical figures overlap.
- Emphasizes how close America's troubling past, like segregation, is to the present.
- Encourages history teachers to put history in perspective for students to understand the impact and relevance of past events.
- Suggests that understanding the overlap between historical figures' lives can spark curiosity and interest in history.
- Urges viewers to acknowledge how recent historical events are and to actively participate in shaping future history.

### Quotes

- "It really wasn't that long ago."
- "We like to pretend like all of that horrible stuff happened forever ago."
- "Just a couple of lives separates the time of Thomas Jefferson from the time of your students."

### Oneliner

Beau explains the proximity of historical events to challenge the perception of history as ancient, urging active participation in shaping future history.

### Audience

History teachers

### On-the-ground actions from transcript

- Challenge students to research and understand the overlaps between the lives of historical figures (suggested).
- Encourage students to actively participate in shaping future history by learning from the past (implied).

### Whats missing in summary

The full transcript provides a compelling perspective on the proximity of historical events, urging individuals to actively participate in shaping future history. Watching the full video can offer a deeper understanding of Beau's message and the importance of acknowledging the closeness of historical events to the present.

### Tags

#History #Teaching #Perspective #ActiveParticipation #AmericanHistory


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we are going to answer a question
from a history teacher.
And we're going to kind of go over it,
because this is something that I think a lot of Americans do.
A lot of Americans view history the way the students do here.
And we do it because we're a young country.
And it may not be the best way to view it.
So we're going to kind of go over this.
I've been teaching history for two years.
This year, I used your We Didn't Start the Fire video
and your Thomas Paine video.
The students loved learning about a founder who
wasn't a slave trading.
It goes on, but we're just going to say bad person.
But I faced the same problem all history teachers face,
trying to keep them engaged and curious about things
see as ancient. Any tips on sparking interest in things that happened hundreds of years
ago? Hundreds of years ago. And that's how we say it. That's how we view it. Hundreds
of years ago. Generations ago. Because we are a young country and because our political
cycles are pretty short, you know, four years for a president, we tend to exaggerate how
long ago things were. Because in the scope of our history, something was way
back at the beginning. But way back at the beginning actually wasn't that long
ago. Not really. Not when you stop thinking about it in terms of years or
decades or generations and start thinking about it in the terms of a life.
I personally met people who were alive in the 1800s. Wild, right? It seems
weird, but it's not. It's not. One of my favorite ways of kind of driving this
point home starts today, April 13, in 1743, because that's the day Thomas Jefferson
was born. And he died on July 4th, 1826, but this weird thing happened before he
died. In 1820 Harriet Tubman was born. Harriet Tubman was alive at the same
time Thomas Jefferson was. Before she died in 1913, Ronald Reagan was born in
1911. Before he died in 2004, I'm gonna guess that pretty much every senior at
your high school was born. The seniors at your high school are separated from
Thomas Jefferson by two lives, Harriet Tubman and Ronald Reagan. Wasn't that
long ago? Not when you really break it down. It seems long ago because we're a
young country and we look at things in very short increments of time. The sad
part about that example is that next year won't be any good because the seniors will
have been born the year after Reagan died.
But you could easily switch Reagan out for Betty White, who I think was born nine years
after Harriet Tubman died, and every single person that you're probably going to teach
in your whole career, was born before she died.
It really wasn't that long ago.
We like to distance ourselves from America's past, and especially we like to distance ourselves
from things that, well, we all know we're wrong.
I like to pretend like it happened way back then, long, long time ago.
I'm willing to bet that pretty much everybody watching this video met somebody who was alive
during segregation at some point in their life.
It wasn't that long ago.
We like to pretend like all of that horrible stuff happened forever ago.
didn't. Just a couple of lives separates the time of Thomas Jefferson from the
time of your students. I think putting it in that perspective might help. It might
make them realize how much they can accomplish or observe over the course of
life.
The overlaps between the lives of historical figures, it's pretty wild when you really
start looking at it, who was alive at the same time.
It might be something that, well, piques curiosity, keeps their interests, and more importantly,
hopes go ahead and establish how much history is going to occur during their life.
And hopefully, it'll make them want to be part of it.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}