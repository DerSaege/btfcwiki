---
title: Let's talk about the end to the age of the tank....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DPOSD5raqJU) |
| Published | 2022/04/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the current status of tanks and addresses the question of whether the age of the tank is over.
- Mentions that the end of the tank era is not primarily due to the conflict in Ukraine.
- Talks about the poor functionality and survivability of Russian tanks, attributing it to improper deployment and advances in anti-tank munitions.
- Points out that anti-tank rockets have advanced far beyond the capabilities of many tanks worldwide.
- Emphasizes the importance of deploying tanks properly, including having infantry screens.
- Notes that it's now more cost-effective to counter tanks with infantry-portable rockets and missiles rather than with other tanks.
- Mentions the changing landscape of conflict due to advances in anti-tank munitions and drone technology.
- Foresees the rise of unmanned tanks in the future, which will be remote-controlled and different in appearance.
- Compares the trajectory of tanks to battleships, stating that they are becoming easier targets in today's world.
- Predicts that tank arsenals worldwide will be phased out in the next 25 years, replaced by unmanned vehicles.

### Quotes

- "We're moving to an age of unmanned vehicles."
- "In some ways, the tank as we know it is going the way of the battleship."
- "It's changing. It's going to look different. It's going to be deployed differently."
- "It's not an end to that type of equipment. It's just shifting to a newer version of it."

### Oneliner

The age of the tank is changing, moving towards unmanned vehicles, as anti-tank munitions advance and drones reshape conflict dynamics.

### Audience

Military analysts, defense enthusiasts.

### On-the-ground actions from transcript

- Stay informed about advancements in military technology and their implications (implied).

### Whats missing in summary

Further insights on the potential implications of transitioning to unmanned tanks and the impact on warfare strategies.

### Tags

#Military #Technology #UnmannedVehicles #TankArsenals #FutureWarfare


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today we are going to talk about the end
of the age of the tank.
It's a common question.
I've seen it bounced around inside defense analyst circles.
I've seen it all over social media,
and I've had a few people ask me,
is the age of the tank over?
And the answer to that is yes.
but also no, and the part that yes isn't because of the conflict in Ukraine.
The reason this question is being asked primarily is because of how poorly Russian tanks are
functioning.
They have very low survivability right now.
And yeah, that's true, anti-tank rockets have moved to a whole new level, far beyond a lot
of tanks around the world.
At the same time, Russia is not deploying tanks properly.
Like basic stuff, infantry screens, stuff like that.
When you send out tanks, you're supposed to have people walking alongside of it.
That's how it should work.
They're not doing that and they're paying the cost for it.
That has a lot to do with why people are asking this question, but that's not really it.
It has to do with advances in anti-tank munitions.
It's now something that you can carry with you that can drop a tank.
more cost-effective to counter tanks with infantry-portable rockets and
missiles than it is to counter it with another tank. And then aside from that,
you have the drone issues. The world is changing when you're talking about
conflict and the tank is phasing out, but it's also not going away. It's just
changing. The reason tanks are so expensive is because they have to put a bunch of armor on it
because there's people inside of it. We're moving to an age of unmanned vehicles. Unmanned tanks are
coming. They're going to look different, but the idea of a fast armored vehicle that packs a lot
of firepower, that's on the way. It'll be remote controlled. It's going to be just like an unmanned
aircraft, those, they're being developed. So yeah, in some ways the tank as we
know it is going the way of the battleship, you know. It's just a big
target to get blown up in today's world. And the tanks headed that direction,
you know. I've said it before on the channel, during World War II a tank
Platoon was a major obstacle, and today it's something to be destroyed en route to the
main objective.
So yes, we're nearing the end of the age of the tank.
No it has nothing to really do with Ukraine, but also the tank isn't really going away.
It's changing.
It's going to look different.
It's going to be deployed differently.
Countries are going to be more willing to lose them because they're not going to have
people in them.
It's just changing.
So the tank arsenals that exist all over the world, yeah, they're going to be phased
out probably within the next 25 years, and they'll be replaced in very much the same
fashion that unmanned aircraft are becoming more and more prominent when it comes to maintaining
air superiority.
It's not an end to that type of equipment.
It's just shifting to a newer version of it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}