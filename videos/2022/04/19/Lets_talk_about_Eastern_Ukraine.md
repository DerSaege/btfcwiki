---
title: Let's talk about Eastern Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3AFMxP_yJwE) |
| Published | 2022/04/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides insights on the ongoing situation in Eastern Ukraine, discussing the offensive that Russia has initiated.
- Russia's move back east is seen as an acknowledgment of the failure of their previous operation.
- The offensive involves strong artillery and air power aimed at softening Ukrainian lines before attempting to create a breakout.
- The initiative lies with Russia, allowing them to dictate the next moves, like attempting breakouts or encircling Ukrainian forces.
- Contrary to mainstream media portrayals, Ukrainian military strategies extend beyond pitched battles and could involve partisan activities and fighting retreats.
- Beau believes Western analysts may underestimate the Ukrainian military if they focus solely on pitched battles.
- He suggests that allowing Russian forces to advance and then encircling them could be a successful strategy for Ukraine.
- Beau outlines the potential strategies available to Ukraine, including resisting, disappearing, fighting in the hills, and setting up partisan campaigns.
- Ukrainian commanders may be hesitant to employ certain strategies due to Russian behavior towards civilians in occupied areas.
- The situation in Eastern Ukraine remains uncertain, with various possible outcomes based on the strategies adopted by both sides.

### Quotes

- "It's defensive. It's about making it too costly for Russia to stay."
- "The good guys don't win every battle."
- "Everybody has a plan until you get punched in the face."
- "It's about breaking the hammer."
- "The initiative lies with Russia."

### Oneliner

Beau provides insights on the ongoing offensive in Eastern Ukraine, suggesting that Ukrainian military strategies extend beyond pitched battles, and Western analysts may underestimate their capabilities.

### Audience

Military analysts, strategists

### On-the-ground actions from transcript

- Strategize for diverse military tactics to counter threats effectively (implied)
- Prepare for potential partisan activities and fighting retreats (implied)

### Whats missing in summary

Insights on potential geopolitical implications and long-term strategies

### Tags

#Ukraine #EasternUkraine #MilitaryStrategy #Geopolitics #RussianConflict


## Transcript
Well, howdy there Internet people, it's Beau again.
So today we're going to talk about Ukraine,
specifically Eastern Ukraine.
We're going to talk about what's going to happen
and talk about what's likely over the next 72 hours.
And we're going to talk about how the Western media
and Western analysts have put people into a frame of mind
that may be once again significantly underestimating
Ukrainian military. Okay, so the offensive that everybody's been talking about, it
has started. What's it going to look like? Go back and watch that first video
again. This whole move, this whole recalculation, this whole shift back east
is Russia admitting that their entire operation was a failure. So they're
hitting the reset button and the whole thing is starting all over again. So
you're going to see the strong artillery and air gang. That's literally happening
right now while I'm filming this. Once Russia believes Ukrainian lines have
been softened enough, they will try once again to move forward. They will try to
create a breakout, which is where they punch a hole in Ukrainian lines and pour
troops through it. In theory, they should be trying to do it at a more
deliberate pace. We'll see how that works out for them. Russia has the initiative
right now. Russia gets to determine what happens next. So there is a strong
possibility that they try to come out of the center of those lines where that
little C is, where that curve is, they're going to try to come out and create a
breakout there. The other option would be to try to renew coming down from Izium
and encircle Ukrainian forces. Those are the two most likely moves from the
Russian side. Now, on the Ukrainian side, contrary to what you have seen on Network
TV, they have way more options than fighting pitched battles, which for
whatever reason, Western analysts and Western news organizations have kind of
presented as the only option. It's not the only option. In my opinion, it's not
even the best option. Pitched battles are what you think of when you think of a
a war, World War II style fights. For whatever reason, that's what's been
dominating the coverage. If I was a Ukrainian commander, I don't know
that that would be how I would want to prosecute the defense here. Most analysts
are saying that there won't be a breakout in the next 72 hours, and that's
That's possible.
At the same time, I think that if there is a breakout where Russian troops punch through
those Ukrainian lines, I think you'll see a lot of Western analysts start to switch
tracks and say, look, this is the Russian military we expected from day one.
Look how professional they're being now.
I think that would be greatly underestimating the Ukrainian military.
If you're looking at those maps and you have been paying attention to issues that the Russian
military has had with logistics and maintaining lines, and you have been paying attention
to the strengths of the Ukrainian side, you would probably walk away with a very different
attitude.
If I was a Ukrainian commander, the Russian military would definitely break out.
In fact, we try to make it happen as soon as possible because as those forces go through,
their lines get stretched.
There's nothing to suggest Russia has actually solved to those problems.
It looks like they put a bandaid on them.
The more they move, the more likely that bandaid falls off.
So allowing them to come through and then encircling the front portion of that advance
and destroying it, that's a strategy that I am confident the Ukrainian military could
pull off and it's one that's not being discussed really.
I wouldn't panic if you see a breakout.
It's also worth noting that Ukraine has the ability to allow Russian forces to advance
past areas that they have already ceded the ground in, meaning they have supplies there
to begin partisan activity behind Russian lines.
There are a bunch of strategies that are available that play to Ukraine's strengths that don't
involved pitched battles. So if it starts to shape up in a way that isn't what has
been discussed on the major networks over the last couple of weeks, I wouldn't be totally
surprised. Now, Ukrainian commanders may elect not to employ any of these strategies because
of how Russian forces have behaved towards civilians in occupied areas.
So they may decide that they're not willing to risk it even for a short period of time.
And I can understand that.
As far as fighting the war, I think those are probably the best moves.
They may want to avoid that because of how Russia has behaved.
If they choose to just fight the pitch battles, we're going to have to see.
It's going to heavily rely on the equipment that NATO has sent, and we're going to see
how it plays out.
Another option is to basically fight a really long, long fighting retreat and just keep
hitting and moving back slowly but surely.
Now there's the possibility of that Axis from Izium still encircling some Ukrainian forces.
But at this point, Ukraine had every opportunity to remove those troops.
If that happens, it's a sacrifice, not a mistake.
There's no way they didn't know.
They were literally countering the drive down from there at every turn.
They knew what the objective was.
So they may be, I don't want to say allowing the encirclement, but they may be viewing
it as acceptable because there's...
When you're looking at an encirclement from the operational level, it's always bad.
When you're looking at it from one level up, from the strategic level, it's fixing Russian
forces.
They can't move if they have a group surrounded.
And it may be something that is buying time for more resistance and mobilization activity
further into the country.
I'm surprised by how that has shaped up there.
Something that I think everybody needs to get ready for at this point, because of how
things have gone so far and the overwhelming Ukrainian successes, it's probably important
remind people that the good guys don't win every battle.
There will be battles lost by the Ukrainian side.
The other thing that's going on is that Western analysts, in particular, they're so used to
their country being on the offensive, being the one making the decisions, that they're
They're having a hard time dealing with being on the defensive side of things, which is
why I think you're seeing that focus on pitched battles, because that's how they would regain
the initiative.
This isn't an offensive war by Ukraine.
It's defensive.
It's about making it too costly for Russia to stay.
That's what it's about.
It's about breaking the hammer.
Not about who can dish out the most, but who can take the most.
That is something that has been sorely lacking in Western coverage of events.
Now, beyond that, we're back to that first video.
All the other predictions you're going to see, well, the Russians are going to try to
move here and take this and this and this.
Most of those are going to turn out to be completely inaccurate, just like last time.
Everybody has a plan until you get punched in the face.
We're back to that.
So we're going to have to wait and see how things shape up, how they play out, what Russia
tries to do versus what they're capable of accomplishing, and what strategy Ukraine adopts
because there are the ones that I've gone over, and then there's a couple of more that I don't
really want to talk about. And then there is the option of them putting up that stiff resistance
and then disappearing again. So, you have the stiff resistance, disappear, fight in the hills,
even though there's no hills there, that's an expression.
The fighting retreat, allowing them to break through, and then setting up for the partisan
campaign.
Those are the strategies that aren't really getting mentioned in addition to the pitched
battles.
And then there's a couple more that are a little bit sneakier that there appear to have
been some moves from Ukraine to kind of put it into place, but I'm not sure.
And since I haven't seen it talked about anywhere else, I'm not going to talk about it here.
So that's where we're at.
Now we're in a waiting game to see what happens and how things shape up on the battlefield.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}