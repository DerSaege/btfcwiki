---
title: Let's talk about China, supply chains, and public health....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kenubeaYcWI) |
| Published | 2022/04/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Series of lockdowns in China started on April 2, impacting about 373 million people, more than the US population.
- Lockdowns cover regions responsible for about 40% of China's GDP.
- World's largest and fourth largest ports are not operating due to lockdowns.
- 300 container ships and 500 bulk ships off the coast of China waiting to be unloaded.
- Impending supply chain issues globally due to the backlog of orders.
- China will rush production to meet backed-up orders once lockdowns lift.
- Delays expected in unloading ships in the US, causing backups in trucking and rail industries.
- Experts suggest potential for worse disruptions than seen before.
- Consumers might face longer wait times for products from China and potential price increases.
- Combined with inflation and political issues, a rough month is anticipated.

### Quotes

- "There have been a string of lockdowns in China."
- "So we will have ships sitting off of our coast waiting to be unloaded again."
- "If you're waiting on products from China, you may be waiting longer."
- "It's just kind of what we're expecting now in the 2020s."
- "We don't have an end date in sight for the lockdowns."

### Oneliner

Lockdowns in China impacting 373 million people with significant supply chain disruptions, potentially leading to longer wait times and price increases for products globally.

### Audience

Global Consumers

### On-the-ground actions from transcript

- Monitor your orders from China for potential delays (implied).
- Adjust expectations for product arrival times and potential price increases (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the supply chain disruptions caused by lockdowns in China and their potential global impacts, along with the uncertainty surrounding the duration of these disruptions.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about supply chains,
public health, and China.
There have been a string of lockdowns in China.
They started on April 2, and I think the most recent one
went into effect on the 11th.
In total, these lockdowns are covering
about 373 million people.
It's more than the population of the United States.
On top of that, the 373 million people
are responsible for about 40% of China's GDP,
their gross domestic product.
In the area that's impacted sit the world's largest
and the world's fourth largest ports.
They are not running right now.
Off the coast of China, there are 300 container ships
and 500 bulk ships waiting to be unloaded.
This is going to cause supply chain issues, big ones.
It's going to take a while.
So what's going to happen is when the lockdowns are lifted,
there's all the orders that are backed up that have been
coming in this whole time.
China will rush to produce them.
Once they get them produced, they'll get them on the ships.
Their ports are large, so they can get them moving out
pretty quickly.
It's going to take longer for the US to unload them.
So we will have ships sitting off of our coast
waiting to be unloaded again.
And it will then back up our trucking industry
and rail industry and everything else.
So we're back to that.
That's going to happen again.
The conditions are already laid.
Some of the people who specialize
in this kind of logistics are saying
that this may actually even be worse than the disruptions that
occurred the first time.
So just something to bear in mind.
If you're waiting on products from China,
you may be waiting longer.
These kinds of supply chain issues
tend to raise prices a little bit.
So we have more to go along with the inflation
that's happening right now.
This, added with the political stunts
along our southern border, we're probably
in for a rough month.
So it's something to be aware of.
There's not really anything you can do about it
other than be aware of it and make any changes you
can to what you're ordering, what you're expecting
to arrive, stuff like that.
There are parts.
Some of those ships that are sitting off the coast there,
we know they're parts.
So those parts will have to be unloaded
before the production can begin.
So that's yet another thing to add to the ongoing public
health issue, beginning of the new Cold War, war in Ukraine,
and everything else that's going on.
Yeah, I really don't have an upside to this one,
I'm going to be honest.
It's just kind of what we're expecting now in the 2020s.
We don't have an end date in sight for the lockdowns.
This is part of China's zero tolerance thing
when it comes to COVID.
So we don't know how long these are going to last,
how much longer they're going to last.
And we're going to have to wait and see
before we can even start counting down to the supply
chain being fixed, or at least getting back to what we have
become accustomed to now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}