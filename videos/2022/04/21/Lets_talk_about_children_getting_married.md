---
title: Let's talk about children getting married....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=u0YRbHs-2DE) |
| Published | 2022/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines statistics and data to address a Republican Party allegation regarding Democrats and children.
- Focuses on the claim that Democrats are encouraging children to partake in adult activities.
- Uses child marriage statistics as an indicator of attitudes towards children engaging in adult behaviors.
- Reveals the states with the highest rates of child marriage, showing Republican strongholds topping the list.
- Notes that treating children as adults is more prevalent in red states rather than blue states.
- Points out the Republican Party's strategy of marginalizing groups with false allegations.
- Suggests that if the Republican Party is genuinely concerned about the issue, they could focus on changing state laws in states they control.

### Quotes

- "Treating children as adults doesn't really seem to be a blue state thing here."
- "Statistics suggest that the ultimate culmination of that activity ending in marriage is most prevalent in red states, in Republican areas."
- "It's the Republican Party attempting to marginalize a group of people with an allegation that isn't true."

### Oneliner

Beau examines child marriage statistics to debunk the Republican Party's false claim against Democrats, revealing a prevalence of treating children as adults in Republican strongholds.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Contact local representatives in states with high child marriage rates to advocate for stricter laws (suggested)
- Join organizations working towards eliminating child marriage in communities (suggested)

### Whats missing in summary

Deeper exploration of the impact of false allegations and strategies for combating misinformation.

### Tags

#RepublicanParty #Democrats #ChildMarriage #Statistics #FalseAllegations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to take a look at some statistics,
look at some data, and we're gonna go through it
in hopes of shedding some light on a conversation
that's occurring in the United States.
And by a conversation, I mean the Republican Party
saying that something is occurring
and that the Democrats are responsible for it,
And they're now using this in fundraising efforts
and attempting to paint Democratic candidates
in a certain light.
So what we're going to do, and the allegation, by the way,
if you don't know what I'm talking about,
is that Democrats are encouraging children
to engage in activities that are better suited for adults.
Now, we don't actually have any statistics on that exact topic.
I mean, that's not something that anybody really tracks,
as far as I know.
But what we do have is child marriage.
That's what we have.
We have child marriage, and we can use that
as kind of a gauge of where that kind of attitude,
the attitude that it's OK for kids to do this sort of thing,
is more prevalent.
Right? So per 1,000, per 1,000, what state do you think is highest? And I know right
now everybody's saying West Virginia, right? Because of stereotypes. We're not
talking about stereotypes right now, we're talking about data. Talk about the
actual statistics on it. And number one is, well it actually is West Virginia. It
It is West Virginia at 7.1.
Texas, 6.9.
Nevada, 5.9.
Oklahoma, 5.8.
Arkansas, 5.6.
Top five, we're not seeing a blue stronghold anywhere.
You've got a swing state that is basically known for letting
anybody get married, and then four very, very Republican
states.
That's where it's most prevalent.
Now do you have to go much further to actually get to a blue state? You don't.
I would have stopped at the top five, that's where I planned to, but if I did
that somebody would say it was cherry picking. So we're going to go a little bit further.
California actually comes in
tied for six
with Tennessee
at 5.5.
So California and Tennessee are 5.5.
Next,
North Carolina,
5.4.
Then you have Virginia, Louisiana, Alabama, Georgia,
4.9.
You get in the picture?
Treating children as adults, that
doesn't really seem to be a blue state thing here.
The statistics suggest that the ultimate culmination
of that activity ending in marriage
is most prevalent in red states, in Republican areas.
This is one of those things that is,
it's the Republican Party attempting to take
and marginalize a group of people with an allegation
that isn't true.
At least I can't find any data to back up the claims.
In fact, everything that I've looked at
suggests the exact opposite,
that it is more prevalent in Republican areas,
not in Democratic areas.
But it's that same strategy, get people to kick down,
find a group of people, label them as something bad,
and then convince your base to mobilize against them.
That's what it is.
In this case, though, it seems that if the Republican Party
actually concerned about this they might be able to just focus on fixing the
state laws in the states they in many cases have absolute control over. Anyway
It's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}