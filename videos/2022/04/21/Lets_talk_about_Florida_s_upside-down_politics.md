---
title: Let's talk about Florida's upside-down politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0N2cqriDgLo) |
| Published | 2022/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Florida politics is unique and different, with its own set of challenges and peculiarities.
- Florida is home to a Democrat, Agriculture Commissioner Nikki Fried, who is suing the Biden administration over a Second Amendment issue.
- The issue involves a form for background checks when purchasing guns, where the question about drug use can put individuals in a difficult position.
- Fried is fighting for an exemption for individuals in states where medicinal drug use is allowed but impacts their ability to pass the background checks.
- The lawsuit was launched on April 20th, demonstrating Fried's sense of humor.
- This situation puts political commentators in a bind as it doesn't neatly fit into their usual talking points.
- Republicans might struggle with this issue as it doesn't follow the typical narrative of Democrats trying to take guns.
- Democratic commentators may find it challenging to attack Fried due to the potential positive impact on background checks.
- Fried's stance on this issue might create a dilemma for Florida voters between a pro-Second Amendment Democrat and Governor Ron DeSantis.
- Fried's gubernatorial campaign adds another layer of interest and potential conflict to the situation.

### Quotes

- "Florida is different. It's Australia-lite."
- "She's suing, trying to get an exemption for this."
- "It's going to defy a lot of people who try to create very simple narratives."

### Oneliner

Beau dives into the unique world of Florida politics, focusing on Democrat Nikki Fried's fight for Second Amendment rights, challenging traditional narratives and creating dilemmas for both parties.

### Audience

Political commentators

### On-the-ground actions from transcript

- Support Nikki Fried's lawsuit by spreading awareness and advocating for exemptions for individuals impacted by the background checks (implied).
- Stay informed about Florida politics and the nuances of Second Amendment issues to make well-informed decisions as a voter (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the complex world of Florida politics, the challenges faced by individuals caught in the Second Amendment debate, and the potential impact of Nikki Fried's legal battle.

### Tags

#Florida #Politics #SecondAmendment #NikkiFried #RonDeSantis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the upside-down world
of Florida politics and a story that, honestly,
I cannot wait to see covered by political commentators
because they are gonna have a real hard time
fitting this into their normal coverage
because it's not gonna match
any of their normal talking points.
If you have ever been to Florida,
especially if you have been outside
the overly developed areas, you are aware of the fact that Florida is different.
It's different.
For international viewers, Florida is a state where the soil, water, sun, weather, insects,
animals, and oftentimes people are all trying to kill you.
It's a unique place.
It's Australia-lite is probably the best way to look at it.
Most people know that it's home to Florida man and Florida woman, but a lot of people
don't know that it is home to a politician who has been fighting very, very hard to end
a practice concerning the Second Amendment that leaves a whole lot of people in limbo.
That's what we're going to talk about today.
As I said, Second Amendment, I'm sure there's a whole bunch of people assuming that I'm
about to talk about Republican Governor Ron DeSantis.
No, he's busy fighting Mickey Mouse and some weird culture war nonsense.
I'm going to talk about the Agriculture Commissioner, a Democrat, Nikki Fried, who is currently
suing the Biden administration over this weird situation that occurs in a lot of states.
states.
If you go to buy a gun, you fill out this form for the background check.
On that form, it asks you about drug use.
If you use, you can't buy.
Now in 36, 37 states, something like that, the state has decided that medicinal is okay,
right?
But that doesn't impact the form.
So people who are in these states, they're left in this weird situation where by state
law they're allowed to own, but they can't go through the process to buy one and go through
a background check, which just pushes them to the secondary market.
So she's suing, trying to get an exemption for this.
it's worth noting that the lawsuit was launched on April 20th. Yeah, she
definitely has a sense of humor. So the reason this is going to cause problems
for political commentators is because if they're a Republican, they're obviously
going to have an issue with fitting this into the whole idea of Democrats are
trying to take your guns. It also leaves Ron DeSantis in the uncomfortable
situation of having to answer why he's all fighting a bunch of princesses
instead of doing this himself. From the Democratic side, obviously this is a
pretty pro-gun stance, right? So Democratic commentators are going to want
to attack her. However, they're going to have to acknowledge that if she succeeds
more people will go through background checks. You know, that thing they always
campaign on rather than being pushed off to the secondary market where there is
no check. This way they'll go through the background check. It's going to defy a
lot of people who try to create very simple narratives. I honestly, I don't
know if a lot of people are going to cover it because it's going to poke holes in a lot of
their talking points. Now, Freed is also running for governor, so she may be running against Ron
DeSantis if she makes it through the primary, meaning Republicans in the state of Florida
are going to have to decide between a pro-second amendment Democrat and somebody who went to war
with Disney. It's one of those things where we're going to get to see whether policy is more
important than talking points so anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}