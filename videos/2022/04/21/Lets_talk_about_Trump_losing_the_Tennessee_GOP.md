---
title: Let's talk about Trump losing the Tennessee GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8QjNbBnJUUM) |
| Published | 2022/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the inner workings of the GOP in Tennessee and the case of Morgan Ortegas.
- Ortegas, a hopeful for the 5th District, was voted off the ballot by the executive committee of the Republican Party.
- Speculations include not meeting club rules, political associations, internal Tennessee politics, or discrimination.
- Ortegas was endorsed by Trump, which makes her removal significant due to Trump's slipping hold on the Republican Party.
- The move to bar Ortegas from running could be seen as a challenge to Trump's influence within the party.
- Trump's diminishing power is evident as the Republican Party in Tennessee didn't hesitate to oust his endorsed candidate.
- Beau anticipates Trump may retaliate to reclaim his diminishing influence, especially in Tennessee.
- If Trump doesn't respond aggressively to the Republican Party's actions in Tennessee, it signals a decline in his political power.
- The lack of fear among state-level party officials of angering Trump indicates a shift in the political landscape.
- Trump's ability to influence the Republican Party and his potential for 2024 presidential aspirations are at stake due to recent developments.

### Quotes

- "Trump's hold on the Republican Party is slipping."
- "Trump's reign as kingmaker of the Republican party will be over."
- "If he lets this slide, if he doesn't respond, there will be more."

### Oneliner

The GOP in Tennessee challenges Trump's influence by voting off his endorsed candidate, signifying his diminishing hold on the party and potential implications for 2024 aspirations.

### Audience

Political observers, GOP members

### On-the-ground actions from transcript

- Reach out to local Republican Party officials for insights and involvement in party decisions (implied)

### Whats missing in summary

Insights into the potential future actions and responses from Trump following this incident. 

### Tags

#GOP #Tennessee #Trump #RepublicanParty #PoliticalInfluence


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Tennessee
and the inner workings of the GOP there in that state,
and Morgan Ortegas in particular.
She was a hopeful for the 5th District there,
and, well, she was going to run in the primary,
but the executive committee there of the Republican Party
voted her off the ballot.
Basically said she didn't meet the club rules.
Now, you can chalk this up to a whole bunch of things, okay?
You could chalk it up to them not wanting the association
that occurred with her being a spokesperson.
It could be just normal Tennessee politics,
and, well, she's not in the club, which is my guess.
It could be something a little bit more discriminatory.
Could be a whole bunch of reasons,
and we don't know and we'll never really find out what it was,
but there's an important thing about this.
There were other candidates that the committee there
and the Republican Party there in Tennessee did this to,
but she's the one that matters.
The reason she matters more than any of the others
is because she was endorsed by Trump.
Trump's hold on the Republican Party is slipping.
He is going to be furious.
I almost wish he was back on Twitter.
The Republican Party, not the voters,
people in the Republican Party took the Trump-endorsed candidate
and voted them off the ballot,
said, no, you're not even going to be able to run.
Now, it would be great to say this was their way
of distancing themselves from Trump.
That's probably not the case.
My guess is that she's not in the club.
Now, the remaining candidates for this district,
they've all been Tennessee politicians
in the political machinery, been around for a while.
So I think it's probably more related to internal Tennessee politics
than them trying to step on Trump's toes or distance themselves.
But what matters is they did it.
A few months ago, you had serving secretaries of state
just cowering from this man.
You had Republican parties in all sorts of states
just terrified of angering Trump.
And now, in Tennessee, they don't seem that concerned about it.
Trump's hold is slipping, and it's slipping fast.
My guess is that we're going to see Trump try to make a splash, a reentry,
because he has to know how bad this looks for him
and how bad it really is.
Because if he doesn't respond,
if he doesn't pull the normal Trump move
and just really go after the Republican party there in Tennessee,
well, nobody's going to be afraid of him anymore.
And his political power is based on other politicians
being afraid of him mobilizing his base.
So it's safe to say that we're probably going to see
some Trump theatrics aimed at Tennessee soon.
Because if he lets this slide, if he doesn't respond, there will be more.
More state-level party officials will start pushing back.
And Trump's reign as kingmaker of the Republican party will be over.
And if that happens, his hopes for 2024 are also over.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}