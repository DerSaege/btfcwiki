---
title: Let's talk about how Russia could do things differently....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LwSoqU9MOzs) |
| Published | 2022/04/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Russia's approach in Ukraine, proposing alternative strategies.
- Responding to a challenge about offering solutions instead of just pointing out mistakes.
- Providing a rough sketch of Ukraine's map and identifying key areas like Crimea and the occupied territory.
- Suggesting to mobilize non-state actors in Crimea for increased mobility.
- Pointing out Russia's lack of mobility and proposing to provide SUVs and trucks instead of tanks.
- Noting Russian high command's commitment to launching the main operation from Izium.
- Recommending thinning out troops and subtly moving forces to Izium for a surprise offensive.
- Describing a tactical movement of forces at sunset to reach Izium by daybreak and liberate Ukraine.
- Emphasizing the element of surprise in the operation.
- Concluding with a thought-provoking idea for liberating Ukraine.

### Quotes

- "Crowdsourcing battle plans now?"
- "Non-state actors are typically more mobile than state actors."
- "Nobody's going to expect the Russian military to do anything at night."
- "Soon as you cross the border, poof, Ukraine's liberated."
- "Anyway, it's just a thought."

### Oneliner

Beau provides strategic insights on potentially liberating Ukraine as a Russian, focusing on mobility, surprise, and concentration of forces for a swift operation.

### Audience

Military strategists

### On-the-ground actions from transcript

- Mobilize non-state actors in Crimea for increased mobility (implied)
- Provide SUVs and trucks to enhance mobility of forces (implied)
- Thin out troops and subtly move them to Izium for a surprise offensive (implied)

### Whats missing in summary

Detailed analysis and background information on the ongoing situation in Ukraine and Russia.

### Tags

#Russia #Ukraine #MilitaryStrategy #Crowdsourcing #Liberation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about Russia and what they could do differently in Ukraine
because I got a question that contained a little bit of a challenge.
So why not?
Now the way the question was put to me was unique and I definitely want to read it because
it was entertaining.
I've watched 16 of your videos pointing out Russia's failures, as you say.
If you were truly unbiased and as knowledged as you seem, you would at least show what
you would do differently than Russia instead of just pointing out mistakes.
If you know better than our generals, tell us how you would liberate Ukraine as a Russian.
Our generals.
Crowdsourcing battle plans now?
Tell us how you would liberate Ukraine as a Russian.
Sure.
Why not?
So this is a rough sketch of Ukraine.
Down here, in fact, it's kind of not visible, but Crimea is down here and that's going to
be important.
So since it's not really in frame, just know it's there.
The brown section here, this is the occupied area.
This is what Russia is saying is secure, where all the partisan activity is occurring.
So if you're going to try to clean up somebody else's mess, what's the first thing you want
to look at?
Underutilized stuff.
Stuff that isn't being used the way it should be or isn't being used at all.
Down here in Crimea, Russia has a bunch of loyalists that are non-state actors.
A bunch of paramilitaries.
They're going to be super mobile.
It's the way of the world.
Non-state actors are typically more mobile than state actors, than conventional militaries.
What is one of the largest problems that Russia is facing?
A lack of mobility.
Mobilize them.
Don't give them tanks or anything to bog them down.
Just get them into the field.
Give them SUVs, trucks, doesn't matter.
Bring them up to here.
Bring them up to this area right here.
And then at this point, you have to take a look at what has already been determined that
you can't really change.
Russian high command, for whatever reason, is very much committed to the idea of launching
the main operation from Izium.
It's up here.
Now one of their big problems is they have this giant front that they're trying to maintain
and they don't really have the troops for it.
So what can they do?
Thin them out.
But if you do that, you might have a break.
But right now, the Ukrainians believe Russia is going on the offensive.
They wouldn't expect it.
So quietly, slowly, so it's not noticed, anything that isn't absolutely necessary gets pulled
off and pushed up to Izium, where it waits.
You leave some conventional forces here.
And then once everything's in position, this force right here starts moving first.
Right at sunset.
Nobody's going to expect the Russian military to do anything at night because the Russian
military hasn't been doing anything at night because you sold all of your night vision
on eBay or whatever.
So this force, leaving the paramilitaries behind, starts moving this way.
And as it moves, it's pulling everything off the line.
Everybody's coming off the line and they're joining the force.
They have to move quickly because they have to make it here by daybreak.
They have to be in line with Izium by daybreak.
So they have that period of time to cover that distance, pulling everybody off the line
as they move.
So by the next morning, you should have a large concentration of forces here and a large
concentration of forces there.
This is when the operation really starts.
Both forces form up and break east and go back to Russia.
Soon as you cross the border, poof, Ukraine's liberated.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}