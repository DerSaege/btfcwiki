---
title: Let's talk about CPAC leaving the US and Republicans leaving democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GT_RxFeUN3E) |
| Published | 2022/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party is moving away from democracy openly.
- The framing of the new Cold War is democracy versus authoritarianism.
- The Republican Party faces a dilemma because many members are authoritarians.
- The CPAC conference, a significant event for the Republican Party, will have Orban as the keynote speaker in Hungary, known to be sympathetic to Putin.
- 63 Republican congresspeople voted against a non-binding resolution that supported democracy.
- The resolution called for NATO to support democratic principles and institutions within its member countries.
- The Republican Party is embracing ideologies similar to authoritarian leaders globally.
- There are concerns that the Republican Party is being led by an authoritarian figure amid the new Cold War dynamics.
- Beau urges individuals within the Republican Party to re-evaluate their stance on democracy and their party affiliation.

### Quotes

- "The Republican Party is openly kind of shying away from democracy."
- "They couldn't even bring themselves to support democracy in words."
- "The Republican Party has become filled with people who share ideologies with strongmen, authoritarian goons all over the world."
- "The Republican Party wants to have Putin's man in NATO as their keynote speaker."
- "Those people who fashion themselves patriots, those people who say that they love this country, they've got some soul-searching to do."

### Oneliner

Beau reveals how the Republican Party is drifting from democracy, embracing authoritarian sympathies, and facing a critical identity crisis amid global political shifts.

### Audience

Republicans, Democrats, Activists

### On-the-ground actions from transcript

- Re-evaluate your stance on democracy and your party affiliation (suggested)
- Support democratic principles and institutions within your community (exemplified)
- Advocate for political leaders who uphold democratic values (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's current shift away from democracy and the implications of embracing authoritarian sympathies.

### Tags

#RepublicanParty #Democracy #Authoritarianism #CPAC #NATO


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about how the Republican Party
is openly kind of shying away from democracy.
I know that's a wild statement, right?
But it's true.
You know, before hostilities broke out,
we were talking about what the new framing
of the new Cold War was going to be.
And democracy versus authoritarianism, right?
We talked about it.
This is how it was gonna be framed
and packaged for those in the West.
Just like the last one was democracy versus communism
or capitalism versus communism,
this time it will be democracy versus authoritarianism.
This presents a problem for the Republican Party
because there are a lot of people in the Republican Party
that are in fact authoritarians.
So much so that a lot of their pre-planned stuff
is kind of giving them a bad look right now.
A good example is the CPAC conference, right?
Now, this is the Conservative Political Action Conference.
It's a big deal in the Republican Party, huge deal.
I mean, this is really where senators and presidents
and stuff like that, that's where they get picked.
This is a huge deal.
They're gonna have a meeting soon
and this one's gonna be in Hungary.
And the keynote speaker will be Orban,
who is, I don't wanna say a Putin ally.
Let's just say very sympathetic to Putin.
That's who the Republican Party
is going to have speak for them.
Now, this conference was actually supposed
to take place in March,
but that whole war thing kind of gave it a bad look.
So they put it off till May.
Now, you could write that off and say,
hey, maybe that's not really what it is.
They just, they didn't know, they scheduled this guy
and now the war's happened
and it looks worse than it really is.
And you could do that if it weren't for the fact
that 63 Republican congresspeople
just voted against democracy, literally,
in a non-binding resolution.
So to be clear, this is just words, okay?
They couldn't even bring themselves
to support democracy in words.
It doesn't actually do anything.
And they voted against it.
I'm just gonna read what they said no to.
This resolution reaffirms unequivocal support
for the North Atlantic Treaty Organization
as an alliance founded on democratic principles.
That's just a statement of fact.
There's nothing to really reaffirm there.
That's what happened and that's how it functions.
It's a democratic institution
with voting and all of that stuff.
The resolution also calls on the president
to use the voice and vote, democratic, right?
The voice and vote of the United States
to adopt a new strategic concept for NATO
that is clear about its support
for shared democratic values
and committed to enhancing NATO's capacity
to strengthen democratic institutions
within NATO member, partner, and aspirant countries.
What this says is that NATO should support democracy
and that it should take a more active role
in making sure that the countries
that team up with it are democracies.
Finally, the resolution calls on the president
to use the voice and vote of the United States
to establish a center for democratic resilience
within NATO headquarters.
This is part of a strategic concept.
The idea in general is to take NATO
from an organization that says it supports democracy
and turn it into an organization that really does.
Short version.
But the reality is none of this actually gets done.
This is just a resolution saying,
hey, it would be great
if Biden supported democracy through NATO.
They couldn't bring themselves to vote yes
on that simple statement because they don't.
The Republican Party has become filled with people
who share ideologies with strongmen,
authoritarian goons all over the world,
and they want them as keynote speakers.
They want their support.
They want to adopt their ideas.
The Republican Party is having a very hard time right now
separating the fact that it's led by an authoritarian
and that's the opposing side in the new Cold War.
The Republican Party has to shift.
This is one of those things that people talked about
when they embraced Trump,
when they embraced authoritarianism.
There were political commentators
all over the spectrum saying this isn't going to go well
because he's going to shift the party to a place
that it's going to be really hard to come back from.
Now they're there.
The United States is actively providing support
to a country that is at war with Putin,
and the Republican Party wants to have Putin's man in NATO
as their keynote speaker.
Those people who fashion themselves patriots,
those people who say that they love this country,
those people who say they support the ideas
of the Constitution and the Republic,
they've got some soul-searching to do,
or they have to leave the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}