---
title: Let's talk about West Virginia, solar, and stranded assets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H8ZUoqfkxfU) |
| Published | 2022/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- West Virginia is getting a massive 3,000 acre solar panel farm built on top of an old coal mine.
- The energy sector is facing the issue of stranded assets as the world shifts away from dirty energy sources.
- Stranded assets occur when investments made in an asset no longer produce the expected returns due to changes in the market.
- Companies may face liabilities if the value of their assets drops significantly, making them worth less than what was spent on them.
- As the world moves away from coal and oil, the demand for these products decreases, leading to assets becoming stranded.
- Personal investments in solar panels can also impact traditional energy companies, affecting their expected revenue.
- Beau finds it noteworthy that a former coal mine is being repurposed for a solar panel farm, showcasing adaptation to changing energy trends.
- States reliant on supplying energy can still thrive by transitioning to cleaner energy sources.
- Beau suggests re-evaluating investments in oil and dirty energy companies due to the impending shift towards cleaner energy.
- While not an expert, Beau encourages awareness of the potential impact of stranded assets in the energy sector.

### Quotes

- "West Virginia is getting a 3,000 acre solar panel farm. 3,000 acres, that is huge!"
- "As the world moves away from dirty energy, a whole lot of stuff isn't going to have the value it used to."
- "The state just has to keep up with times, and it looks like West Virginia is at least going to make the attempt to do that."
- "Y'all have a good day."

### Oneliner

West Virginia transitions from coal to solar, signaling the rise of stranded assets in the shifting energy sector and urging investment reconsideration towards cleaner energy.

### Audience

Investors, Energy Sector

### On-the-ground actions from transcript

- Re-evaluate investments in oil and dirty energy companies (suggested)
- Support and advocate for the transition to cleaner energy sources in your community (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the challenges and opportunities arising from the transition to cleaner energy sources and the implications for various stakeholders in the energy sector.


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
solar panel farms and stranded assets, because those two things are going to
start going together more and more and more. West Virginia is getting a 3,000
acre solar panel farm. 3,000 acres, that is huge! And it is going right on top of an
old coal mine. I love it. There's something poetic about that. But it
brings us to something else that's going to start hitting the energy sector. I can
see it happening anyway. A lot of their assets are going to become stranded. What
that means is a company puts out money to get an asset, some kind of investment,
right? And they do this with the expectation of it producing long-term,
and then something happens, and that asset isn't going to produce that money
anymore. But they still have the cost of that asset, and if it drops too far it
becomes a liability. It isn't worth what's owed on it, or what was spent. In the
energy sector, I imagine this is going to start happening a lot, because as the
world moves away from dirty energy, a whole lot of stuff isn't going to have
the value it used to. And you can think of simple stuff. Oil reserves, right? Let's
say company X has the rights to this chunk. Because they planned on sucking
all of that out of the ground and selling it, they also plan their
investments around that. They plan their expansions around that. They might have
even borrowed against that. But as the world moves away from that kind of
energy, well, what they can charge for that drops. If it drops too much, well, that
asset becomes stranded. And it doesn't even have to be something like that. It
could be the fact that as we move away from coal and oil, the demand for these
products drops, therefore the relative supply seems higher, which means they
have to charge less, which means assets become stranded. On a more personal level,
let's say you put in solar at your house and your neighbors and their neighbors.
Your whole neighborhood puts in solar. Your local energy company, your electric
company, they're not going to be happy about that, because that's revenue that they
thought was going to be coming in, and more than likely they planned on. And now
they're not going to get it. I know, poor oil companies, right? Why won't somebody think of
the billionaires? The reason I'm bringing this up is, well, twofold. One, I really do
just think it's amazing that a former coal mine is going to be converted in
the way that it is. You know, there are a lot of states that they kind of built
their economy around supplying energy. Just because the type of energy is
changing doesn't mean that that can't still be the industry in that state.
The state just has to keep up with times, and it looks like West Virginia is at least
going to make the attempt to do that. The other reason I'm bringing it up is, even
though I don't give investment advice, it's not something I'm an expert in, this
just seems like something people might want to be aware of. If you have stock in
oil companies, in dirty energy companies, I would start thinking about
re-evaluating if it was me. But again, I'm not an expert, I'm not an economist. This
just seems like something that's likely to happen to me. Anyway, it's just a
thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}