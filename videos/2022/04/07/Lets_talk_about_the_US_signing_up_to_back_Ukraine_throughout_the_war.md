---
title: Let's talk about the US signing up to back Ukraine throughout the war....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x8Sf6U7HseU) |
| Published | 2022/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is in the process of signing on to back Ukraine throughout the entire conflict.
- The Senate has already unanimously approved the Lend-Lease Act, allowing the U.S. to lend or lease defense articles to Ukraine.
- The agreement with Ukraine is not subject to the typical stipulation that equipment lent must be returned within five years.
- The U.S. commitment means supplying Ukraine throughout the entire war.
- This commitment puts the United States' production capability against Russia's sanctioned production capability.
- The Senate version implies support for Ukraine until Crimea is recovered.
- Russia seems prepared for a multi-year engagement in Ukraine.
- The conflict is likely to become protracted and escalate in the east.
- The situation is expected to worsen significantly.
- The United States will stand with Ukraine through it all.

### Quotes

- "The United States is signing on to supply Ukraine throughout the entire war."
- "It is going to get bad."
- "The United States has signed on to back Ukraine throughout the entire thing."

### Oneliner

The United States commits to backing Ukraine throughout the conflict, with implications of a protracted and escalating situation. NATO members might follow suit.

### Audience

Politically active individuals.

### On-the-ground actions from transcript

- Contact your representatives to express support for continued U.S. assistance to Ukraine (suggested).
- Stay updated on the situation in Ukraine and advocate for sustained international support (implied).

### Whats missing in summary

The detailed nuances of the conflict dynamics and potential implications of long-term U.S. support.

### Tags

#Ukraine #USsupport #LendLeaseAct #Russia #Conflict #InternationalRelations


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about how the United States is in the process of signing
on to back Ukraine throughout the entire conflict.
No more piecemeal stuff.
No more questions about how long the support is going to last.
The U.S. is in the process of agreeing to back Ukraine with equipment, supplies, whatever
they need throughout the conflict.
The Lend-Lease Act, probably learned about it in high school, it's coming back.
The Senate has already unanimously approved it.
This is certainly something that will get through the House over Republican objections,
I'm sure.
This allows the United States to lend or lease defense articles, military equipment, to Ukraine.
One of the interesting provisions in the Senate version is that this agreement is not subject
to the typical stipulation that however long the equipment is lended, it has to be less
than five years.
That's waived.
So what this means is that the United States is signing on to supply Ukraine throughout
the entire war.
So now when you're talking about the attrition strategy that is being employed in some places,
it really now boils down to United States production capability versus Russia's sanctioned
production capability.
Another interesting thing to note in the Senate version is that it states the conflict started
with the annexation of Crimea, which could be read in a way to suggest that the United
States will back Ukraine until that's recovered.
It appears that Russia has signed on for a multi-year engagement in Ukraine.
When we first started talking about all of the options before the shooting ever started,
we talked about it becoming protracted and that being the worst case for everybody.
That's where we're at now.
It looks like it's going to become a protracted conflict in the east.
It's going to get bad.
It is going to get bad.
And the United States has signed on to back Ukraine throughout the entire thing.
I would imagine that other NATO members will follow suit.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}