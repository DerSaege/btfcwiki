---
title: Let's talk about Disney and Desantis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2sCfOL_MAqY) |
| Published | 2022/04/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The state of Florida had a long-standing deal with Disney, allowing Disney to control the infrastructure, a critical aspect.
- DeSantis requested to terminate this deal, setting a distant date for after the election to avoid immediate consequences.
- Potential negative outcomes include residents being financially responsible for the bonds associated with this termination.
- Beau predicts that Disney will take legal action and likely win, reverting everything back to normal.
- If Disney doesn't win, there's a belief they may have to leave due to the inability to maintain their brand without controlling the infrastructure.
- The surrounding areas may struggle to meet Disney's standards without the necessary funds.
- Beau questions if Disney could work out a deal with the county but ultimately believes they may have to relocate.
- The damage to Disney's brand and the economic impact on the state could be significant if they have to leave.
- A signal has been sent to corporate America, warning against investing in Florida due to the uncertainty of deals with the state.
- This situation may have lasting negative effects on Florida's economy, regardless of the outcome with Disney.

### Quotes

- "Disney's lawyers will take this to court, and they'll win, and everything will go back to normal."
- "No company is going to come to Florida and invest billions when it can all be snatched away."
- "It is damage that can't be undone, regardless of what happens with Disney."

### Oneliner

The state of Florida's decision to end its long-standing deal with Disney sends a damaging message to corporate America and may have lasting negative effects on the state's economy.

### Audience

Florida residents, corporate America

### On-the-ground actions from transcript

- Advocate for transparency and accountability in government decisions (implied)
- Support businesses and organizations impacted by political decisions (implied)
- Stay informed about government actions that may affect local economies (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of Florida's decision to terminate its deal with Disney and the broader implications for the state's economy and corporate investments. Viewing the full transcript offers a comprehensive understanding of Beau's insights and predictions regarding this situation.

### Tags

#Florida #Disney #Economy #CorporateAmerica #LegalAction


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about Disney and
DeSantis and all of that. It's been covered pretty well so I'm just going to kind of do a quick
overview and get on to the rest of it. So the state of Florida had a deal with Disney. It's had this
deal for 50 years, something like that, and basically Disney was its own government, and
it allowed Disney to control the infrastructure.
That's what really matters in this when we get into it later.
They could control the infrastructure.
Now everything else was kind of the same, it was kind of like a little sub-county for
lack of a better term, but it's the control of the infrastructure that matters, and being
able to do things their way.
This deal has been in place a very, very long time, and the state of Florida, because DeSantis
asked for it, just yanked it.
Doesn't exist anymore.
It's going to go away.
They put a date far into the future after the election, of course, so when the negative
outcomes from this arise, well, they'll already have a secure re-election.
But this has already passed.
Now I'm sure that most of the coverage has talked about the bonds, the money that is
now going to end up being owed by the residents in the area surrounding Disney and all of
the possibilities that go along with this.
But one take is that, well, Disney's really invested too much to leave, and we're going
to talk about that.
But first, before we get to that, let's talk about what everybody's wondering.
What happens next?
My guess?
Disney takes them to court and wins.
I'm not a lawyer, and I'm not an expert in this kind of law, but I read through it.
I'm fairly certain the state legislature can't do what it just did.
So I would imagine that Disney's lawyers will take this to court, and they'll win, and everything
will go back to normal.
But there's one little genie that's already out of the bottle on this, but I am fairly
certain that Disney can win in court.
I don't know if you've ever met a Disney lawyer, but I have, and piranhas have more
compassion.
They're not going to let this go.
Now assuming that Disney doesn't win, the general consensus is that Disney won't leave.
I think that's wrong.
I think that they have to leave.
Not for any political reason or to teach DeSantis in the state of Florida a lesson or anything
like that. I think they have to leave because they can't run Disney anymore. If
you've ever been there, think about what it was like when you were driving around,
when you were in the area around Disney. Do you see any buildings that had
falling into disuse, disrepair, stickers on lamp posts or park benches, graffiti, potholes,
busted trees by the side of the road, anything.
Everything was perfect.
That's what makes it the most magical place on earth.
That's the brand.
If Disney can't control the infrastructure, it can't run Disney World.
The surrounding areas, the areas that will end up taking these responsibilities over,
they're not going to be able to do it to Disney standards because they're not going to have
the money.
There was a reason it was set up this way.
I don't think that Disney can stay, even if it wants to.
It's going to take a huge loss by moving.
But if it doesn't go somewhere where it can control the infrastructure, it'll take a bigger
loss with the damage to the brand because Disney won't be Disney anymore.
And it's sad because that is going to economically devastate that whole side of the state.
It's going to be a huge deal if Disney has to leave.
I'm sure they don't want to, that they don't want to give up the investments.
We're talking about billions here, but I don't think they're going to have a
choice if they can't control the infrastructure.
Maybe they could work out some deal with the county, but I don't know.
I'm fairly certain if this doesn't get put back to normal, they're going to have to leave.
It may not be immediate, but they're going to be looking for somewhere else to go because
they can't have Disney without being able to make it that idyllic place that people
want to escape to.
That's the whole point of it.
That's what they sell there.
Now, there's one other thing that happened.
Even though I do believe Disney is going to just take it to court and win, and this is
going to kind of all be for nothing.
A signal went out to corporate America and it says, do not invest in Florida.
Do not come to Florida.
Don't make a deal with the state.
Don't develop here.
Don't put any big projects here because it doesn't matter how long you've been here,
many jobs sprang up because of your little business, how many billions of
dollars you invested into infrastructure, how many billions of dollars you
contributed to the economy of the state. Doesn't matter. It can all be
taken away. That's the message that just went out. This, regardless of how it gets
resolved will damage Florida's economy for decades to come.
And that's already happened.
The G&E is out of the bottle, it is not going back in.
These companies, when they're putting together large projects, the kinds that alter the economy
of the state they're putting them in, they think this stuff out decades ahead.
If they can't plan decades ahead, because the PR people might say something that offends
the governor, they just can't build there.
The current administration in Florida, it damaged Florida a lot today.
It is damage that can't be undone, regardless of what happens with Disney.
It is damage that is going to last a long, long time.
No company is going to come to Florida and invest billions when it can all be snatched
away.
When no deal with the state matters, even if it's been in place for like half a century.
That's the message that went out to the corporate world today.
A lot of people think that Florida is kind of immune to all of this.
We don't need to worry about it because of the tourist dollars.
Understand, the tourists come here for the attractions.
attractions that require deals with the local and state government. Deals that
now everybody's gonna wonder if they're worth anything. Nobody moves, nobody
comes down here to visit to see Florida before it was improved. The area that is
around Disney. I believe it actually used to be called Mosquito County. Without
the investments, without the continued revenues from the economic activity,
those places are... they are not going to be the cities and towns that you know.
And that message went out loud and clear across the country. It was publicized at
every turn. Because even if Disney wins, that doesn't mean the next company will.
A company that doesn't have the resources to fight in court like Disney does, they
They can't take that risk.
This was a horrible move.
Setting aside the reasoning, which I find horrible, I'm just talking about this move
in particular at any point in time for any reason.
The way it was done, all it did was damage the state.
Damage the pockets of the people who live here.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}