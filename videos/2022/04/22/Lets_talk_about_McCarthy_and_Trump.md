---
title: Let's talk about McCarthy and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zgdf-SUkZxY) |
| Published | 2022/04/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New York Times reported McCarthy considered telling Trump to resign after January 6th events.
- McCarthy denied the report, calling it "totally false and wrong."
- New York Times released a recorded tape contradicting McCarthy's denial.
- McCarthy's denial may have political consequences, potentially impacting his goal of becoming Speaker of the House.
- McCarthy expressed intent to talk to Trump about resigning but not about Pence pardoning him.
- Beau questions why the topic of Pence pardoning Trump was even brought up.
- Beau suggests further investigation into why McCarthy thought Trump needed a pardon.
- Implications suggest McCarthy believed Trump committed actions requiring a pardon.
- Beau hints at the need for more clarity and investigation into McCarthy's statements and beliefs.
- The situation reveals potential inconsistencies and deeper issues within Republican leadership.

### Quotes

- "I want to know why he believed that the subject of Pence pardoning would come up."
- "This certainly appears to suggest that the lead Republican in the House believed that Trump did something that he'd need to be pardoned for."
- "I'd like further explanation on that, personally."

### Oneliner

The New York Times reported on McCarthy's consideration of telling Trump to resign post January 6th, leading to a recorded tape contradicting McCarthy's denial and raising questions about potential pardon needs. Further investigation is needed into McCarthy's beliefs.

### Audience

Political analysts, journalists

### On-the-ground actions from transcript

- Investigate further into McCarthy's statements and beliefs (implied).

### Whats missing in summary

Deeper analysis and context on the political implications and potential fallout from McCarthy's reported considerations and denial.

### Tags

#KevinMcCarthy #DonaldTrump #NewYorkTimes #RepublicanLeadership #PencePardon


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about House Minority Leader
Kevin McCarthy and former President Trump
and some reporting from the New York Times
and the situation that has evolved from that.
If you don't know, the New York Times put out an article
saying that McCarthy had entertained
the idea of reaching out to Trump
after the events of the 6th and telling him he needed to resign.
That would be a huge departure from everything McCarthy says in public.
Now for his part, it is worth noting that the House Minority Leader denied this.
He said, and I'll read part of it here, it comes as no surprise that the corporate media
is obsessed with doing everything it can to further a liberal agenda.
This promotional book tour is no different.
If the reporters were interested in the truth, why would they ask for comment after the book
was printed?
And he called the reporting, quote, totally false and wrong.
In response, the New York Times published a retraction.
No, of course not.
They just released the tape.
Yeah, that was recorded.
The whole thing was on tape.
I'm willing to bet that the House Minority Leader didn't know that.
It probably wouldn't have made such a long denial if he knew it was recorded.
And yeah, this is all funny.
And there are some political ramifications to just this part.
You know, McCarthy really wants to be Speaker of the House.
That's a big goal right there.
This probably endangered that, because Trump does have friends.
And unless Trumpism is totally defeated at the midterms,
those people are going to be, well,
they're going to be people who might be determining
who the Speaker of the House is.
this isn't going to sit well with them.
And that's interesting and certainly Trump is going to have issue with McCarthy
now, and it's going to cause even more Republican infighting and all of this is
worth noting, but there's a something I think is far more important in what was
said McCarthy when describing this conversation he was going to have within
President Trump, he goes on to say that he just wants to talk about him resigning.
He doesn't want to get into Pence pardoning him.
For what?
That to me is probably the question that needs to be asked.
about the fact that, okay, a politician didn't tell the truth. Shock. I want to
know why he believed that the subject of pence pardoning would come up. What did
he think Trump did? Insight and insurrection? Obstruct congressional
proceedings, engage in a conspiracy to do that. That to me, that little offhand comment seems far
more important than everything else. This is the top Republican in the House. This is a person who
went out there and tried to push the idea that somehow this was Pelosi's fault, I think, is who
he was trying to blame it on. I don't think he was wondering about talking
about a pardon for Pelosi. The idea was to remove the president, to get rid of
Trump. That's what he was talking about at the time, but he didn't want to talk
about Pence pardoning. That needs a little bit more investigation. I think
the committee might be interested in that. That seems to be where people should
kind of go with this story because it certainly appears to suggest that the
lead Republican in the House believed that Trump did something that he would
need to be pardoned for. I'd like further explanation on that, personally. Anyway,
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}