---
title: Let's talk about new tools in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vMmTx-1mdoE) |
| Published | 2022/04/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US is committed to giving Ukraine more significant tools to defend against Russia's new offensive.
- The US is no longer providing just defensive equipment but is now supplying deployable equipment like 155mm howitzers.
- An unmanned aircraft called Ghost Phoenix has been developed for Ukraine, possibly for intelligence gathering and surveillance.
- The speed at which the Ghost Phoenix was developed hints at off-the-shelf technology being adapted for new roles.
- This rapid development could level the playing field in providing air support, reducing the advantage the US has.
- Ukraine now has more tanks in the field than Russia, acquired through capture, redeployment of Russian equipment, and tanks from undisclosed countries.
- The West seems to be ensuring that Ukraine has the material edge over Russia in the ongoing conflict.
- The new weapons being delivered to Ukraine have higher capabilities and could potentially alter the course of the conflict.
- If Ukraine gains a material edge before Russia's offensive ramps up, it could significantly impact the situation.
- The developments in weaponry may lead to constantly evolving tactics on the battlefield.

### Quotes

- "The days of piecemeal deliveries of small stuff, that's ending."
- "The offensive that the Russians apparently have underway right now is shaping up to be different."
- "If Ukraine has the material edge before this offensive really gets rolling, that's going to change a lot of math."

### Oneliner

The US is providing Ukraine with more substantial tools for defense, including deployable equipment and new weaponry like the Ghost Phoenix, potentially shifting the balance of power in the conflict with Russia.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor the situation in Ukraine and Russia for updates on the evolving conflict (implied).
- Support diplomatic efforts to de-escalate tensions in the region (implied).

### Whats missing in summary

Analysis of potential diplomatic implications and international reactions to the changing dynamics in Ukraine. 

### Tags

#Ukraine #Russia #US #ForeignPolicy #Defense #Conflict


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about what
is being sent to Ukraine and how it
may change the dynamics of Russia's apparently underway
new offensive.
The US has very much committed to the idea
of giving Ukraine the tools it needs at this point.
The days of piecemeal deliveries of small stuff, that's ending.
The US has committed to providing
155 millimeter howitzers.
This is equipment that, by DOD statements,
will help them defend against the new Russian offensive.
And yeah, that's true.
It will.
It will also help them launch a counteroffensive
when the time comes.
The US, for the most part, has, for some reason,
stuck to the idea of providing equipment
that could be seen as defensive.
It is now definitely moving into the realm of equipment
that is deployable in either setting.
The howitzers certainly fit.
On top of that, there is an interesting development
with something called the Ghost Phoenix, or the Phoenix Ghost.
It's a brand new unmanned aircraft that, by the way,
the Air Force is talking about it,
it sounds like Ukraine was like, hey, this is what we need.
We would love to have an unmanned aircraft that
does this.
And then the Air Force developed it and filled it in weeks,
is the way it sounds.
We don't know if that's actually what happened,
because they're being pretty hush-hush about it.
We're not even 100% sure what this thing does.
What we can guess is that it's tube launched.
It'll have a range of 10 to 15 kilometers,
hover time of 15 to 20 minutes.
There are some rumors that suggest
it's kind of a specialized switchblade.
So it flies over, finds the target,
dives down, destroys itself and the target.
There are also a couple offhand comments
that make it seem like it is more something that could also
be used for intelligence gathering and surveillance.
Again, it's hard on this one, because you're
trying to read between the lines on something that, at this point,
they're not ready to disclose what it does.
The interesting part about this is the speed at which this
was apparently developed.
Under normal circumstances, the deployment
of conventional aircraft, that takes years, not weeks.
But now that we're talking about unmanned aircraft,
what seems to be very likely is a bunch of off-the-shelf
technology being pressed into new roles
as the situation on the ground changes in whatever conflict.
This is something that, over time,
is going to filter down to what the US sees as lesser powers.
This isn't expensive.
And over time, this is something that will whittle away
the giant edge the United States has when it comes
to providing air support, because this stuff's
cheap in comparison.
So other countries will be able to develop and deploy it
on their own, which means they'll be
able to modify it on their own, which
means the battlefield of the future
will be one where tactics have to be developed on the fly,
constantly, because there will always be new emerging threats.
A new weapon on the battlefield could
be something that happens two or three times a month,
rather than two or three times over the course
of an entire war.
Now, the other thing that has happened
is that, according to the latest Western estimates,
Ukraine now has more tanks in the field than Russia,
some through capture and redeployment
of Russian equipment, and some through tanks
that just showed up.
Tanks migrate this time of year.
And it seems like a whole lot of them
migrated from undisclosed countries and into Ukraine.
The offensive that the Russians apparently
have underway right now is shaping up to be different,
because it looks as though the West is making sure
that Ukraine has the material edge, not Russia.
If that is something that the West is capable of achieving
before this offensive really gets rolling,
assuming it does, that's going to change a lot of math.
So we're going to have to wait and see.
But the new weapons that are showing up,
the new stuff that's being delivered to Ukraine
is of a higher caliber, pardon the pun,
than what has been delivered before.
And it is stuff that can eventually
be used to make a difference in the future.
It can eventually be used to move
the line the other direction.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}