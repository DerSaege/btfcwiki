---
title: Let's talk about realization from a retweet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Iixx6TQ7WCs) |
| Published | 2022/04/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Retweeted a joke on Twitter that led to a realization about societal norms.
- Many people are discussing something he's believed in for a long time.
- Saw a meme about his joke and retweeted it, only to later find out the creator opposes something he supports.
- Contemplated removing the retweet but decided against it to remain a positive information source for the meme creator.
- Reflected on the meme's meaning, which sheds light on societal pressures and norms.
- Acknowledged the importance of not condemning people for stepping outside societal norms to live.
- Encouraged understanding and empathy towards those who may not conform to societal expectations.

### Quotes

- "I don't want them to lose what could be the only positive voice they have on this topic in their information cycle."
- "Maybe if it's something you wouldn't do yourself or you don't want to condone, maybe you still realize that at times people are going to step outside of societal norms, those that exist today, just to live."
- "Because after all, they're just trying to live."

### Oneliner

Beau contemplates societal norms and empathy after a Twitter joke leads to a realization, opting to remain a positive voice despite differing views.

### Audience

Social media users

### On-the-ground actions from transcript

- Support and amplify positive voices on social media (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of societal pressures, empathy, and understanding towards those who deviate from societal norms.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about me
retweeting something on Twitter
because it led to this bizarre chain of events
and kind of had a realization at the end of it.
Wanna share that thing that I noticed.
So I've been joking a lot about something on Twitter
because it's something that I've believed for a really long time
and now it has kind of entered the public consciousness
and a lot of people are talking about it.
So because it's something that I've believed a while,
I have a lot of jokes about it.
And this person made a meme about this joke.
They made a joke about the joke, right?
And they tweeted it out and they tagged me.
I saw it.
I thought it was funny.
I retweeted it.
Like six hours later, I see all the tags and the messages
that came in from people telling me, hey,
you know this person is like super opposed to something
you are very publicly in support of.
No, I actually didn't know that.
But then I'm sitting here thinking
about what I'm supposed to do about it.
I could hit the retweet button again
and it would disappear off my timeline, right?
I could do that.
But I don't know what that would accomplish
because that person, for all I know,
I may be the only person who is publicly in support
of trans people in the entire information ecosystem
that they consume, right?
And if they're following my social media closely enough
to make a joke like that, to make a meme about a joke,
I don't want them to cut that information source out.
I don't want them to lose what could
be the only positive voice they have
on this topic in their information cycle.
So I'm sitting here and I'm at a complete loss
as to how to respond to this.
And then I start thinking about the meme itself,
about the joke.
And the joke is that I develop bad eyesight in grocery stores
sometimes.
Like, I thought I saw that person put baby formula
in their bag, but I don't know.
Maybe I didn't.
And you get to thinking about what that really means.
It means that you're acknowledging that at times
society puts people in a position that
requires them to step outside of social norms just to live.
I would think anybody who would make a meme highlighting that
and kind of broadcasting it would understand
that you don't want to come down on somebody for just trying
to live and that there are a whole lot of societal norms
that put people in that position.
And maybe that eyesight issue, maybe
it's progressive in nature.
Maybe there's a moment when you realize
that even if it's something that might make you uncomfortable,
maybe if it's something you wouldn't do yourself
or you don't want to condone, maybe you still
realize that at times people are going to step outside
of societal norms, those that exist today, just to live.
And maybe in the future, your eyesight
gets bad on that issue too.
And you don't see it.
It doesn't bother you.
And you don't condemn the people that are doing it.
Because after all, they're just trying to live.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}