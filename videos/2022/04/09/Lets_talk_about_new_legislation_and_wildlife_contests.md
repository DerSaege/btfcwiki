---
title: Let's talk about new legislation and wildlife contests....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7BWwXJgYXL0) |
| Published | 2022/04/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces new federal legislation called the Prohibit Wildlife Killing Contests Act aimed at banning events where groups of people hunt predators like coyotes or foxes.
- Points out that the legislation applies to public lands, protecting half a billion acres.
- Mentions that these contests are not about conservation, but more about portraying an image of rugged mountain men.
- Criticizes the lack of skill involved in these contests due to technology making it easy to hunt predators en masse.
- Notes that winners of these contests merely receive a participation trophy and it's more about luck than skill.
- States that eight states have already banned this practice and two more states have stopped holding such events.
- Counters the argument that banning these contests on public lands will lead to increased hunting on private lands.
- Suggests using a camera instead of a gun if the goal is to challenge oneself against smart animals.
- Comments on knowing people who participated in these events and implies they could benefit from exploring their creative side.

### Quotes

- "It's not about skill anymore. It has nothing to do with that."
- "The winner of these things is really just getting a participation trophy."
- "You could always shoot them with a camera if that's really what it was about."

### Oneliner

Beau explains why wildlife killing contests are more about image than skill, advocating for a ban on public lands to protect predators.

### Audience

Conservationists, Wildlife Activists

### On-the-ground actions from transcript

- Advocate for wildlife protection laws (exemplified)
- Encourage creative pursuits as an alternative to hunting contests (implied)

### Whats missing in summary

The emotional impact of moving away from hunting contests towards more ethical and skill-based interactions with nature.

### Tags

#WildlifeProtection #Conservation #BanHuntingContests #PublicLands #AnimalRights


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about
some new legislation at the federal level that has just been introduced.
We're going to talk a little bit about the custom it's trying to prohibit.
We're going to talk about why that custom exists anyway and why it's
probably time for it to go by the wayside, legislation or not. So at time of
filming this bill has 16 sponsors. It's got a decent shot of getting somewhere.
It is called the Prohibit Wildlife Killing Contests Act and it applies to
public lands. So you're talking about half a billion acres that this protects.
These events, if you're not familiar with them, it's where a group of people get
together and they decide they're all going to go out and take coyotes or
whatever, foxes, generally a carnivore or a predator, something like that. Now the
one thing I will give the crowd that does this is that it is very rare to see
them be dishonest about it. They don't normally pretend this is about
conservation. Most of them are honest enough to say, yeah, taking out a whole
bunch of predators at one time from one species isn't exactly great for the
environment. It's rare to see somebody make that argument. So if it's not about
conservation, what's it about? Image. Image. It's about trying to cast the image
of the mountain men of yore, trying to play into that theme from literature,
man versus nature. The thing is, the people doing this today, they're not
wearing buckskin clothing and carrying a flintlock. They're on ATVs, using drones,
and using a scoped repeating rifle. It's not exactly the same. And if they ever
stopped to think about it, they'd realize that it's not even a challenge. How hard
can it be if when you go and set the parameters for your little contest,
you're assuming that everybody that goes out is going to get one? Some of them are
based on how many you can get. If you can go out and kill ten of a certain animal
at one time, it's probably not that hard. If 15 of you can go out and everybody
kill one in one day, it's probably not that hard. Technology has rendered these
contests pointless. It's not about skill anymore. It has nothing to do with that.
The winner of these things is really just getting a participation trophy.
It's rewarding the person who was lucky enough to have the biggest, or the one
with the longest tail, or biggest tooth walk by them. That's all it is. It doesn't
actually send the image that a lot of the people involved in it think that it
does. How hard can it be if all of y'all can get together and y'all all can go do it?
You're not really displaying a skill here. If it really was about the skill,
and the ability to track, or to get close to one of these animals, you want to
pitch yourself against a smart animal. That's why carnivores or predators are
normally chosen, right? You could always shoot them with a camera if that's
really what it was about. But it's not. It's about image. It's about trying to be a
tough guy. Now, as it stands, eight states have already banned this practice. And
there's two more where there's no law on the books stopping it, but they just
don't happen anymore because it's a practice that is going away. Now, the
argument that you will hear from people is that, you know, if this isn't done on
public land, well, they'll just grow in number and then they'll be on private
land. And this law doesn't stop people from doing it on private land. I would
point out that most times when people make the case that, oh, well, it was
coming off of public land, so I had to take it. Normally, they travel, you know,
50, 60, 100 miles to get near public land so that could happen. A lot of hunters
know that animals who typically reside on public land, well, they feel safer. So
they're a little more careless. So when they wander off public land, well, they're
an easier target. You know, because it's all about the challenge of it, right? If
you want to engage in this type of thing, you want to pitch yourself against an
animal like this, use a camera. Just use a camera. To be honest, I've known people
who have participated in these types of events. Every single one of them I
know could probably benefit from nurturing the creative side of their
life a little. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}