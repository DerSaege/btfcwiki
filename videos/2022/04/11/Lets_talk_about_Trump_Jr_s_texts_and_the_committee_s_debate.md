---
title: Let's talk about Trump Jr's texts and the committee's debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VXiR_PQhOJ8) |
| Published | 2022/04/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of the latest developments in Trump world, focusing on the committee's decisions regarding former President Trump.
- There is reporting on texts from Trump Jr.'s phone that suggest actions were planned before votes were counted, confirming suspicions.
- The committee is reportedly divided on referring Trump to the DOJ for criminal prosecution, with Cheney denying the division but potential concerns about the move.
- Concerns exist within the committee that referring Trump to the DOJ could politicize the parallel investigation already underway.
- Beau argues that the evidence against Trump is substantial regardless of political motivations, suggesting that a committee referral may not sway opinions.
- He believes that the majority of Americans will judge the evidence objectively and make their own conclusions, criticizing politicians for underestimating public discernment.
- Beau questions whether the committee's decision not to refer Trump could lead to confusion among the public and a lack of transparency in informing them.

### Quotes

- "I don't think that a referral from the committee is going to sway those people one way or the other."
- "I think one of the big mistakes that politicians make is constantly treating the American people as if they are incapable of discerning facts from fiction."

### Oneliner

Beau provides insights into the committee's dilemma over referring Trump for prosecution, questioning the impact on public perception and politicians' underestimation of public discernment.

### Audience

Politically Engaged Citizens

### On-the-ground actions from transcript

- Contact your representatives to express your opinion on how the committee should handle referring Trump to the DOJ (suggested).
- Stay informed about the developments in Trump world and hold politicians accountable for their actions (exemplified).

### Whats missing in summary

Further analysis of the potential consequences of the committee's decision and the importance of transparency in informing the public.

### Tags

#Trump #Committee #DOJ #Politics #PublicPerception


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump world a little bit.
We're going to check in on what's going on with Trump,
the committee, and all of that stuff.
First, there's the reporting about the texts
from Trump Jr.'s phone.
And really, if you were watching the channel back then,
none of that's a surprise.
Basically, the reporting says that the text indicate that
everything they did afterward was planned before the votes were even counted.
We kind of knew that already.
This is just confirmation of it,
although I don't think that's going to stop it from dominating the headlines for
a couple days.
The other news to me is probably a little bit more important,
although it may not seem like it.
There's reporting that says the committee is divided on whether or not to refer former
President Trump to the DOJ for criminal prosecution.
Now Cheney came out and said, no, that's not the case.
I think they may both be accurate.
From Cheney's perspective, there is probably no debate over whether or not they have the
evidence to refer Trump for criminal prosecution. However, there is probably discussion going
on within the committee over whether or not that's the right move. There's been concerned
voiced several times that if the committee refers the former president to DOJ, that DOJ's
parallel investigation that is going on outside of the committee will then be tainted by politics.
So they may not want to refer in hopes that DOJ just indicts on their own and therefore
it doesn't look like a political investigation.
I understand the optics desire there, however, I would point out with the amount of evidence
that we've seen, the amount of evidence we know exists that we haven't seen, and the
stuff we don't have a clue about, anybody who is going to look at that and then say,
oh, it's politically motivated, is going to say that no matter what.
I don't think that a referral from the committee is going to sway those people one way or the
other. The absence of the committee referring would probably be used as evidence. And say,
look, even the witch hunt committee couldn't find anything. There's no winning that one.
If it is more expedient to refer, then it would make sense to do that. I don't think that this
is the place for civility politics because I don't think it matters. I don't think there
are people who will look at the evidence and say it's politically motivated simply because
the committee referred. I think they would say that no matter what. But I do believe
if the committee doesn't refer, there will be people who will look at it and say the
committee couldn't find anything, but DOJ indicted anyway. It's a catch-22, but I
would suggest that the overwhelming majority of Americans will look at the
evidence and go from there. I think one of the big mistakes that politicians
make is constantly treating the American people as if they are incapable of discerning facts
from fiction.
And while they do that, and because they hold that belief, they never get out there and
make the case and try to inform the American people.
And that may be a mistake that the committee is about to make.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}