---
title: Let's talk about sanctions and Putin's cushion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XCuKldzsClc) |
| Published | 2022/04/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of sanctions and their purpose in the context of the current situation.
- Provides a perspective on the impact of sanctions on Russia's economy and military capabilities.
- Contrasts the significance of monetary amounts for individuals versus nation-states at war.
- Points out Russia's lack of economic power compared to NATO.
- Mentions the effects of sanctions on Russia's credit rating and economy.
- Talks about how capital controls are being used by Russia to stabilize their economy artificially.
- Emphasizes that sanctions aim to degrade Russia's ability to force project and widen the conflict.
- Notes that sanctions limit a country's resources for military operations.
- Predicts that over time, sanctions will make it increasingly difficult for Russia to sustain wartime production.
- Raises concerns about how sanctions impact ordinary citizens while achieving their intended objectives.

### Quotes

- "It's not like it's going to stop the war by itself, but what it does is it stops Russia's ability to widen the conflict because they don't have the money."
- "My concern with them is that it always hurts the little people along the way."
- "Power coupons. What's foreign policy about? Not right and wrong, not good and evil. Power."
- "Just first put them into perspective. And then, there isn't really a situation in which Russia has the economic power to outlast NATO."
- "The sanctions stop countries from getting more power coupons to use on the international scene and it pretty much always works."

### Oneliner

Beau provides insights on sanctions, showcasing their impact on Russia's economy and military capabilities, underscoring how they limit force projection and hurt civilians.

### Audience

Global citizens, policymakers.

### On-the-ground actions from transcript

- Support organizations providing humanitarian aid to civilians affected by conflicts (implied).
- Stay informed about the impact of sanctions and advocate for policies that minimize harm to vulnerable populations (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the economic and military implications of sanctions on Russia, shedding light on how these measures influence international power dynamics.

### Tags

#Sanctions #Russia #Economy #Military #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about sanctions
and how they work.
We're going to provide a little bit of perspective
and we're going to talk about what they're
supposed to accomplish.
We're going to do this because I keep seeing a number pop up.
And that number is attached to the idea
that the sanctions won't be effective.
I got a couple of questions about it, and I've seen this number a lot over the last
36 hours.
So one of the questions, how can the sanctions ever work?
I just saw an article saying that Putin had a $3.4 billion dollar cushion.
If you Google for a $3.4 billion dollar cushion, you'll find a bunch of articles.
So what happened?
Georgia put $3.4 billion into a rainy day fund to help stabilize their economy.
This money came from the first quarter oil and gas sales.
First quarter, $3.4 billion.
Okay, so let's get some perspective on that first.
A first quarter, roughly $3.5 billion, that's half of Disney if you only count the parks.
$3.4 billion to you or I, yeah, that's huge.
That's something that sets families up for generations to come.
For a nation state at war, that's nothing.
It's like finding a $5 bill in a parking lot.
Cool, but it's not going to change your overall economic status.
For some more relevant comparisons, $3.4 billion is enough to cover the cost of US operations
in Afghanistan for 10 or 11 days.
It's just not a lot when you're talking about war.
I want to say it was the middle of last month.
There was an incredibly conservative estimate, a low estimate, saying that Russia had already
at that point lost about $5 billion in hardware alone.
3.4 is nothing, and that 3.4 isn't money for the war.
to help stabilize their economy because the sanctions are in fact having effects and pretty
substantial ones.
When you're talking about Russia being able to weather this, rather than look at dollar
amounts that go into different funds and stuff like that, you need to look at total economic
power.
That's really what you should focus on.
The thing is, because until recently, Russia has been viewed as a near-peer militarily,
I think people are under the assumption that they are a near-peer economically.
They are not.
They're not even close.
Russia's GDP pre-sanction was about 1.5 trillion.
That is roughly half of California.
The US GDP is more than $20 trillion.
Economically there's no comparison when you're talking about economic power.
And remember, it's not Russia versus the US, it's Russia versus NATO.
So when you're talking about NATO's economic power, that number jumps up to like $43 trillion
versus 1.5.
Russia doesn't have the economic power for this, and it's already starting to show S&P
has moved Russia into selective default.
That is their credit rating, selective default, meaning they're not going to pay some stuff,
but they'll probably pay others.
It's at the point where they're already missing payments.
Even prior to this, Russia didn't have a great rating.
I want to say it was CC, and fact check me on that because I'm going off memory.
But that's highly vulnerable.
It's not like they had an A rating and then went to selective default because of the sanctions.
They didn't have the economic power to begin with.
The reason the Russian economy looks okay right now is because they have instituted
a lot of capital controls, which is a nice way of saying they're cooking the books.
There are a whole bunch of people who would love to dump their investments and get out,
which would send their economy even further down, but Russia has prohibited it.
In Russia, there is a...
It's not really that bizarre when you think about it historically, but the Russian people
pay very close attention to the exchange rates when it comes to the ruble.
It is in Putin's best interest to make certain that Russia has an exchange rate that people
are used to, and they will absolutely cook the books to make that happen.
if that number crashes and stays down too long, there will be a lot of
discontent. The thing is they can only cook the books for so long. They don't
have the economic power. Okay, so what's the purpose of these
sanctions that are being used. The actual goal here is to degrade Russia's ability
to force project. It's not like it's going to stop the war by itself, but what it does
is it stops Russia's ability to widen the conflict because they don't have the money.
It stops their ability to send in new equipment because they can't buy it or make it. It
It makes it harder to recruit troops because they can't offer huge enlistment bonuses.
Look at the U.S. military, those $60,000 bonuses.
They can't offer stuff like that because they can't pay for it.
Over time, sanctions like this pretty much always curtail a country's ability to force
project. So what it does is it puts Ukraine in the position of you need to
defeat the troops that are here. There's only so much more that Russia can send
before they run out of cash. So that's the goal and for that purpose it's
already working. It's going to get harder and harder for them to produce, to engage
in wartime production because they don't have the cash, they're not going to be able
to borrow, they're going to run into more and more issues.
So understand my objections when it comes to sanctions, they never have anything to
do with whether or not they work.
They always work to limit a country's ability to force project.
My concern with them is that it always hurts the little people along the way.
When you look at other countries that are heavily sanctioned, sure, a lot of them, that
system of government may survive.
The rulers may maintain power, but they don't have power on the international scene anymore
because money is power coupons.
foreign policy about? Not right and wrong, not good and evil. Power. The sanctions
stop countries from getting more power coupons to use on the international
scene and it pretty much always works. There's just a lot of people that get
caught up in it that don't have a say in what's going on. So when you hear these
numbers, 3.4 billion or whatever, just first put them into perspective. And then
remember there isn't really a situation in which Russia has the economic power
to outlast NATO. That's not a thing. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}