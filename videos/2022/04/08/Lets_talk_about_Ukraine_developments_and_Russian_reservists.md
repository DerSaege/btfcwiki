---
title: Let's talk about Ukraine developments and Russian reservists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=s-Fff7WP80A) |
| Published | 2022/04/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ukraine's situation on the ground is being discussed.
- A Russian advance from the area around Izyum has begun, but it hasn't shifted the lines much.
- Ukrainian forces are holding the advance in place, causing problems for the Russian military.
- There is a structural issue within the Russian military where officers are expected to follow orders without operational latitude.
- Russia is calling up reservists, indicating they may be in more trouble than previously thought.
- Russia is planning to call up people who left military service after 2012 for a three-month training before deployment.
- This move suggests that Russia is psychologically preparing for a prolonged conflict and facing significant losses.
- Russian reservists are not trained like American reservists and will receive minimal training before deployment.
- The losses for Russia may be much higher than what is being reported.
- Russia seems determined to continue the conflict and throw more people into the conflict until a resolution is reached.

### Quotes

- "Russia is planning to call up people who left military service after 2012 for a three-month training before deployment."
- "Russian reservists are not trained like American reservists and will receive minimal training before deployment."
- "Russia seems determined to continue the conflict and throw more people into the conflict until a resolution is reached."

### Oneliner

Beau reveals Russia's plan to call up reservists, indicating potential trouble and a prolonged conflict in Ukraine.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Monitor the situation in Ukraine closely and provide support to affected communities (implied)
- Stay informed about developments in the conflict and advocate for diplomatic resolutions (implied)

### Whats missing in summary

The full transcript provides detailed insights into the Russian military's current challenges and Ukraine's strategic position on the ground.

### Tags

#Ukraine #RussianMilitary #Reservists #Conflict #MilitaryStrategy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the situation
on the ground in Ukraine.
We're gonna talk about a report that has surfaced
and it would carry some pretty wide ranging implications
and we're gonna talk a little bit about a structural problem
inside the Russian military
that that is really starting to show itself.
Okay, so a few days ago
I did one of these and I said to watch the area around Izyum.
That's where we could expect
a Russian advance to emerge from because troops in that area were preparing.
They were gathering supplies and stuff like that.
That advance has started.
It hasn't advanced.
It really hasn't shifted the lines much at all.
it appears thus far that Ukrainian forces are holding it in place.
Now, we don't know if the Russian military is throwing its full weight behind this advance
yet, or if this is just the softening phase, but given the fact that the lines aren't
moving at all, it's pretty hopeful for Ukrainian forces.
The hopeful projection, a few days ago, is that they were able to disrupt this advance
and not get encircled.
The hopeful projection now is that they're able to hold that advance in place, and that's
a huge shift.
From the Russian side, they need this advance.
They need it.
This isn't an elective thing.
If Ukrainian forces are capable of holding this in place, Russia has to go back to the
drawing board for everything, because this advance is a supporting element to a whole
bunch of other stuff.
A stall here for the Russian military is going to be very unforgiving, and this is where
that structural issue is starting to show.
In most militaries, most modern professional militaries of today, you have your generals
and then you have the officers in the field.
The officers in the field have a lot of latitude.
Generals don't tell them how to do something.
They tell them what to do.
It's that old joke where the general shows up and asks the lieutenant, son, how'd you
take that that that hill and lieutenant looks at him and I said sergeant take
that hill and that's that's how most Western militaries operate they give
their junior staff and their non-commissioned officers a lot of
operational latitude Russia doesn't do that officers and the lower ranks are
are expected to just carry out the general's orders, which
means if they're told to drive through this area
and they meet stiff resistance, they're
still going to try to go through there.
That's what they're expected to do.
They're not going to try to encircle it, maneuver around it,
take it out, and bypass it.
They're going to try to drive through it.
That is a structural issue that is causing Russia a lot of problems right now.
Now, for this report, and I want to stress something before I start talking about it,
I have one source on this. One. I have one source. It's Ukrainian intelligence. Ukrainian
intelligence is saying this is occurring, and it matches up with other stuff on the
ground when you try to put the piece in.
But I don't have any other sources on it.
I'm talking about it because if this is accurate, this is a huge development.
So what is the report?
That Russia is calling up reservists.
That's really what the report is.
more to it, but that's the main line right there. You'll remember a few days
ago, all over the news, it was like Russia's calling up 134,000 more troops,
and I was like, nope, we talked about this a month ago. In the spring and in the
fall, Russia pulls in 130,000 conscripts. This is completely normal.
This isn't a signal of them being in trouble. Them pulling in reservists is a
huge signal that they're in trouble. In fact, they're in more trouble than even the most
pessimistic estimates you have seen from anybody. According to the reporting, Russia is planning
on calling people who have left military service any time after 2012. So, a conscript comes
in in 2011, gets out in 2012, goes back to civilian life, this person may now be called
back and given a three-month, so a 12-week boot camp and then be sent to the field.
That is what Ukrainian intelligence is reporting.
That would be a huge indication of a couple of different things.
One, that psychologically Russia is preparing itself to be there for months.
Because they're pulling people in now, probably take a month to process them in, three months
of training before they ever show up.
The other side to this is that they're in a lot of trouble.
American reservists are not like American reservists.
You call up an American reserve unit, these people are trained.
They know what they're doing.
They are on par with active duty troops because they get continual training.
One week in a month, two weeks a year.
You've heard the commercials.
Russian forces don't get anything after they leave.
going to get a 12 week refresher and then be sent into combat. That's wild. Not 12
weeks boot and then advanced individual training then you show up to your unit
and then you go over nothing like that. 12 weeks and then into the grinder you
go. If that's true it suggests that their losses are way higher than my
estimates for sure. They are certainly higher than anything the Russian
government is publishing. In fact, they may actually be close to Ukrainian
estimates. That's a big deal. Now, at time of filming, there's no other
confirmation on that. I want to make that part clear, but it's something worth
talking about, because if this is happening, Russia's in a really bad way.
And they also plan on at least attempting to stick it out, and that the conflict is
in fact going to be protracted, and Russia will keep throwing people into the grinder
until they're finally defeated by Ukraine.
at this point a miracle happens and Russia is able to break out. So anyway
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}