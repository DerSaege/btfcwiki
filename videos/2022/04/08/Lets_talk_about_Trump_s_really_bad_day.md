---
title: Let's talk about Trump's really bad day....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FaZ9G2A2SqE) |
| Published | 2022/04/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump had a bad day yesterday with a series of losses that could impact his political future.
- The Department of Justice is investigating classified documents found at Trump's golf course, a significant development.
- Trump could potentially face fines of $10,000 a day for contempt of court for not turning over documents.
- The investigation into the Trump Organization is still ongoing despite previous expectations of its conclusion.
- Republicans in the Senate unanimously voted against Russia, signaling a shift away from Trump's brand of authoritarianism.
- Mitch McConnell, a key Republican figure, announced support for Murkowski, undermining Trump's influence within the party.
- The Republican Party's backing of Murkowski indicates a significant blow to Trump's political standing.

### Quotes

- "It was a bad day. It is a bad sign for his political future."
- "But $10,000 a day adds up really quick."
- "That severely undercuts Trump."
- "The Department of Justice announced that it was looking into the trove of classified documents."
- "The unanimous votes against Russia are votes against Trump."

### Oneliner

Trump faced a series of significant losses, including DOJ investigations, fines, and Republican senators voting against Russia, indicating a potential shift away from his influence within the party.

### Audience

Political analysts, activists

### On-the-ground actions from transcript

- Contact political representatives to express support or opposition to their decisions (suggested)
- Stay informed about ongoing political developments and their implications (implied)

### Whats missing in summary

Insights on the potential long-term repercussions of these events on Trump's political career.

### Tags

#Trump #DOJ #RepublicanParty #MitchMcConnell #PoliticalFuture


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
Trump's bad day yesterday. And it was a bad day. It is a bad sign for his political future.
It was a list. Just loss after loss after loss in one single day. So the first one is
the Department of Justice announced that it was looking into the trove of classified documents
that were found at the golf course down there at Trump's little club. It's worth noting
that if this had been you or I, we'd already be in cuffs. That's a big deal. The fact that
DOJ is investigating it is nice. It seems like that should be well underway by this
point. And while this in and of itself isn't really a huge deal to Trump, he's used to
these kinds of investigations. It's just one more headache. On top of that, the Attorney
General in New York is asking the judge to fine Trump $10,000 a day for his contempt
of court for not turning over documents. Now we all know that Trump is a million-bazillionaire
or whatever, but $10,000 a day adds up really quick. And for somebody that is, let's just
say, in the middle of a not-good run with business ventures, hi to the two people who
linked here from Truth Social, that's probably going to hurt. On top of that, the case looking
into Trump Organization, most people, myself included, thought that was done, thought that
got squashed. But according to the prosecutor there, no, it's still ongoing. Now the reality
is we don't know if it's really still ongoing or the prosecutor is just saying that. But
it's one more headache. And then we get to the ones that really matter for his political
future. The first was all of those Republicans in the Senate unanimously voting against Russia.
That's voting against authoritarianism. That's voting against Trump's brand. That's a bigger
deal than I think people are going to realize at this point. You're starting to see that
fissure between the authoritarian Trumpism that has ruled the Republican Party for years
and what the Republican Party used to be. And the unanimous votes against Russia are
votes against Trump. They're votes against Trumpism. Quite a few of them cast by people
who are ardent supporters of Trumpism. And then you have the big one. Mitch McConnell,
who is really the lead Republican figure when you're talking about excluding the Trumpist
loyalists. He's the person that controls the Republican Party. He said that they would
be backing Murkowski. Now if you're not familiar, this is a senator who was very vocal when
it comes to opposing Trump. Trump has a hand-picked candidate trying to prime-air and McConnell
is openly saying that the Republican Party is going to support Murkowski. That severely
undercuts Trump. It undermines him and it casts a lot of doubt on his political future.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}