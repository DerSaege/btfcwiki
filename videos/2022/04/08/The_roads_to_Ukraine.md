---
title: The roads to Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E7AHt5_mQH4) |
| Published | 2022/04/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides historical context on the roads to Ukraine, going back to the Cold War and detailing events that shaped the country's current situation.
- Describes how major powers like the U.S. and Russia conducted intelligence operations and interfered in Ukraine to further their interests.
- Talks about the U.S. running a 25-year ad campaign in Ukraine to influence democratic values and Western interests.
- Explains events like the Budapest memos, the Orange Revolution, and the Maidan protests that marked significant shifts in Ukrainian politics.
- Analyzes Russia's actions, including annexing Crimea in violation of agreements, and Ukraine's response to resist Russian influence.
- Addresses misconceptions about the Maidan protests being labeled as a U.S. coup and stresses the agency of the Ukrainian people in shaping their own destiny.
- Emphasizes Ukraine's fight against Russian invasion and interference, showcasing their determination and resilience.

### Quotes

- "Ukrainians do not want to be under the thumb of Moscow."
- "The thing that's on loop is that Ukrainians do not want to be under the thumb of Moscow."
- "That's why they're fighting the way they are."
- "When you try to take that away, you are erasing them."
- "Don't take that away from them because it doesn't fit your narrative."

### Oneliner

Beau provides historical context on Ukraine, discussing major powers' interference, Ukrainian agency, and the ongoing fight against Russian influence.

### Audience

History enthusiasts, geopolitics followers.

### On-the-ground actions from transcript

- Support Ukrainian communities (suggested)
- Educate yourself on Ukrainian history and current events (implied)
- Advocate for international support for Ukraine's sovereignty (implied)

### Whats missing in summary

The full transcript provides a comprehensive overview of the geopolitical dynamics surrounding Ukraine, offering valuable insights into historical events that have shaped the country's current reality. Viewing the entire transcript can provide a deeper understanding of the context behind Ukraine's struggles and resilience against foreign interference.

### Tags

#Ukraine #Geopolitics #Russia #US #History #Maidan #Independence #Interference #Sovereignty


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the roads to Ukraine,
and how we wound up where we are.
We're going to provide some context.
Over on the other channel, we've been talking about what's
been happening a whole lot.
And people constantly say, well, this is because of this.
And regardless of whatever side they want to support,
the date they pick is something that
supports their narrative.
Because that's what context is, things
that support what you want to believe.
Of course not.
So, we're going to go through elections, we're going to talk about intelligence operations
that occurred, we're going to talk about foreign interference, we're going to talk about all
kinds of stuff.
And we're going to do this with an eye to a very specific point at the end.
But along the way, we're going to learn a lot.
And we're going to learn a lot about the things that created the themes that are shaping the
events of today.
We're not going to start in 2013.
We're not going to start in 2004.
We're going to go all the way back to the Cold War.
And we're going to see what events, what themes are on loop in the background and what brought
us to today.
Okay, so what's going on during the Cold War at this time?
Moscow is running Ukraine as a puppet.
You can frame it however you want.
You can church it up in any way you want.
You could say they're a Soviet state, they're a member of the glorious Soviet Empire.
It doesn't really matter.
Moscow is running Ukraine, right?
That's what's happening.
What's the U.S. doing?
Engaging in intelligence operations inside Ukraine designed to create internal disruptions.
The theory behind this is to get Moscow focusing on domestic issues, so they're not really
paying attention to stuff on the international scene.
What does that mean, causing internal disruptions?
It means funding dissident groups, training them, arming them, stuff like that, creating
a nuisance within the country.
This is pretty common among major powers.
Make no mistake about it, while the U.S. was doing this, the Soviets were doing the exact
same thing in Western Europe.
There's a reason why all of those armed dissident groups in Western Europe were carrying AKs
because the Soviets gave them to them.
This is a normal play on the international scene.
I do want to point out that just because it's common doesn't necessarily mean it's a good
idea.
I would point out that this is why the US tends to be in a situation of like, why are
we fighting y'all?
We trained y'all.
Because when major powers do this, they tend to not care about the ideology of the group
that they're assisting.
as long as they are a thorn in the side of their opposition.
So it creates an uncomfortable situation later on.
So 91 rolls around.
Independence referendum.
Soviet Union is breaking up and Ukraine puts out this referendum.
84% voter turnout.
So, high voter turnout.
Of that 84%, 92% were in favor of being independent from Moscow.
Ukraine doesn't want to be under Moscow's thumb.
Put that on loop in the background, because this theme is going to come up over and over
and over again.
All right, so Ukraine becomes independent.
What happens next?
Major powers do what major powers do.
Russia wants to find some way to turn it into a puppet again, to yank it back into its sphere
of influence and to keep it there.
The United States, they want the exact opposite.
Russia starts running intelligence operations, a bunch of them, and we'll detail them.
Some of them anyway.
What's the U.S. do?
Kinda does an ad campaign.
Something we've talked about over on the other channel.
The countries attempt to maintain and expand their empires in different ways.
Russia uses old school methods.
They're not really effective anymore, as we're about to find out.
The United States uses money and plays the long game.
So right after Ukraine becomes independent, the U.S. starts pumping in cash, pumping in
money to civic organizations, politicians, cultural icons, business leaders, stuff like
that.
you need to qualify for this cash? How do you get this money, get access to these
deals? How do you do all of this? Well, number one, you have to be in support of
democracy, at least a little bit. How much you have to support democracy is very
relative, but you also need to align with Western interests. That's what you need.
and the US starts doing this from the very beginning. Five billion dollars over
about 25 years. Now if you're not familiar with this type of stuff, that's
actually not really a lot. Normally stuff like this is way more expensive, but that
occurs. What's the US doing as it does this? We've talked about agents of
influence, right? Cultural icons, business leaders, politicians, shifting thought
within the country. Now these people aren't really agents of influence
because they're not being run by an intelligence agency, but they serve the
same function. They're influencers on the foreign policy scene, basically. They
work the same way. They present ideas and people either embrace or reject them.
That's what the US did. Put that on loop in the background because that goes on.
That is a consistent thing. Basically, the entire timeline we're going to cover,
However, that's happening in the background.
Okay, so then what?
Ninety-four, the Budapest memos.
This is the U.S., the United Kingdom, and Russia all agreeing to respect the territorial
integrity of Ukraine.
These countries agree to honor Ukraine's current borders.
File this away, because this becomes important later.
All right, 2004, so from 1994 to 2004, you have Kuchma.
And that's the leader there.
And he's transitioning the country from a Soviet state to a capitalist one.
Then in 2004, there's an election.
And this is where Russia really starts turning up the heat, really trying to bring Ukraine
back into its sphere of influence pretty heavily.
There's a person named Yanukovych, file that name away, because he's going to keep on
showing up.
Now Yanukovych is running for president, and this weird thing happens.
You know how like the Russians tend to poison their political opposition?
It's weird, because that happened to his political opposition, who did it.
don't know who. Then this other weird thing happened. That election, Yanukovych
won. But it sure seemed like it was rigged. This starts the Orange Revolution.
Ukrainians take to the street. Pressure goes to the Supreme Court. There's a
runoff and this time it's monitored. There's another vote and it's monitored and this weird
thing happens. When it's monitored, Yanukovych doesn't win. Yanukovych doesn't win.
it's worth noting that Yanukovych is pretty sympathetic to Moscow.
Okay, so then 2010 rolls around. Guess who's running? Yanukovych, again. It's weird. He seems
to have all the money and all the support in the world to run for office, it's odd.
This time he wins, this time he wins and says that Ukraine should be neutral, shouldn't
have anything to do with Russia, shouldn't have anything to do with NATO, should be neutral.
Now the people in Ukraine, they're really interested in a deal with the EU, an economic
package.
There's also a side deal with the US at the same time.
And it is worth noting that the EU deal requires some regulations to be enacted and things
to be deregulated.
There's strings attached to the EU deal.
And those strings are designed to further take Ukraine into the Western way of doing
things, right?
Now days before this package is supposed to be signed, Yanukovych changes his mind.
Instead, he turns and looks to Moscow, and I want to say there's two billion dollars
that gets transferred, and he starts siding with Moscow.
And Ukrainians take to the streets, like, yeah, we still know where the pitchforks are.
We did this once.
We can do it again.
This is the beginning of Maidan.
When people talk about it, this is when it starts.
This is also in 2013, 2014, this is what gets referred to as the U.S. coup in Ukraine.
It's weird because when you're describing it, there's not a lot of U.S. involvement.
So where's that idea come from?
The State Department kind of looked to those people who were attempting to oust Yanukovych.
We're like, hey, if you get into power, here's a list of people we like.
And I know that sounds like super cartoonish, but that's really what happened.
There's leaked audio of it.
Now keep in mind that this whole time in the background, the US has been funding thought
leaders in that country, swaying thought.
And that ad campaign, well it's paying off right now.
Now we don't have any evidence of US intelligence being on the ground, going up to the protesters
and telling them what to do.
Can't actually prove that occurred.
And I don't believe it occurred in that way.
But I certainly believe that some legal attaches from the embassy did meet with some of the
leaders and feel like, hey, now's your time to shine.
That doesn't really make it a U.S. coup.
So during all of these protests, during the Maidan, what's Russia doing, right?
They have intelligence officers on the ground advising Yanukovych's teams.
They have paramilitaries basically commanding the security services that are going up against
the protesters.
In fact, the brutality that occurred there is really more laid at Russia's feet than
at Yanukovych's.
Because after all, this is when we start to confirm a lot of things.
Yanukovych, after he's deposed, guess where he goes and hides?
Russia.
He was their puppet.
He was the person that was supposed to bring Ukraine back into Russia's sphere of influence.
But remember that referendum.
Ukrainians don't want to be under the thumb of Moscow.
And they showed twice that they know where the pitchforks are.
So what happens now?
Well in March of 2014, Russia annexes Crimea.
They take it over.
Remember the Budapest memos.
Violated.
They did not respect the territorial integrity of Ukraine.
They did not respect those borders.
Old men, Russian proxies, contractors paid by Russia, seize a portion of the country.
Now it is worth noting that once Russia has control over this area and some other areas,
they did do a referendum, but there are a lot of questions about it.
And to this point, I don't believe that the people in these areas actually had a say.
The country that employed contractors and supplied proxies and broke international agreements,
well they said that the vote was in favor of them doing it after the fact.
I'm a little skeptical of that.
So in April, they do the same thing, and they take over those little areas that become the
republics, the breakaway republics.
Now in May of 2014, the Ukrainians elect a very pro-Western politician in rhetoric.
Runs on the idea of moving away from Russia, getting rid of Russian intelligence operations
and influence and corruption within the country.
You may remember that prosecutor Shokin.
The reason this person was ousted is because they weren't doing that and that's what the
Ukrainian people wanted.
So much so that Zelensky was elected because the previous president wasn't going far enough.
It's also worth noting during this time the Minsk agreements occur.
Now these are another set of agreements that needed to be made.
It was a ceasefire that never actually stopped the shooting.
It was another set of agreements with Russia that nobody really believed.
There was no reason to believe that Russia would honor these agreements because they
violated the Budapest Memo because they violated the agreement before.
It's also worth noting that from 2016 till literally just before the invasion, Russia
was running cyber attacks and intelligence operations throughout the country.
There's your context.
He wins in a landslide because the current president wasn't pushing back against Russian
influence enough.
That's how he came to power.
That's the story.
That's how it happened.
There are tons of what we would picture U.S. intelligence doing, but it's Russia doing
it.
The U.S. ran a 25-year ad campaign in the country.
They elevated those people who supported democracy, who supported Western interests through nonviolent
measures.
Russia ran cyber attacks, had paramilitaries and intelligence officers basically running
the state security under their puppet.
It certainly appears that they rigged an election and tried to take out a candidate for head
of state.
It's also worth noting one little last bit about Yanukovych.
If there was ever any question about whether or not he was a Russian puppet, take a guess
who Vladimir Putin wanted to run Ukraine in the event the invasion was successful.
This is your context.
And the reason I really wanted to talk about this is because most times when people are
trying to push a narrative, they erase one really important part of this, the Ukrainians.
The idea that what happened in 2013-2014, that Maiden was a U.S. coup, is American exceptionalism
at its finest.
It is condoning imperialism at its finest because it's the same thought process.
The reality is, depending on the day, 400,000 to 800,000 Ukrainians took to these streets
in just the capital alone.
When you sit there and say, oh, it was a U.S. coup, you are erasing the voices, the actions
of millions of Ukrainians in taking away their say in what their government is, in what is
there to represent them.
And you're doing it under the guise that, oh, 12 CIA guys, that they just tricked them
because they're ignorant foreigners, don't know no better.
They need a major power to tell them what to do, right?
That's not what happened.
They did it in 2004 to stop a rigged election.
They did it again in 2013 and 2014 when the Russian puppet went against the will of the
people.
Go look at the polls.
See what the Ukrainian people supported when it came to that deal with the EU.
Calling it a U.S. coup is ridiculous.
Yeah, the U.S. ran a 25-year influence operation, and it worked.
But the idea is also there that if that operation hadn't occurred, the Ukrainians wouldn't
have done it.
That initial referendum says you're wrong.
84% voter turnout and 92% in favor of independence.
The thing that's on loop is that Ukrainians do not want to be under the thumb of Moscow.
That's what's playing in the background.
That's what you're seeing right now.
That's why they're fighting the way they are.
However, when the invasion happened and Ukraine took up arms, civilians started fighting back.
There weren't the promises of all of the assistance that they've gotten.
They were willing to do that on their own against a military that was perceived at the
time to be one of the top three in the world, and they were willing to fight back.
The U.S. didn't do that.
The West didn't do that, Ukraine did that.
When you try to take that away, you are erasing them.
You are saying that the only way a minor power would ever do something is if a larger power
told them to do it.
That is an imperialist mindset.
That's a colonizer mindset.
That's not what happened.
They fought back against the Russian invasion the same way they fought back against Russian
influence, the same way they fought back against Russia rigging an election, the same way they
voted when it was time for their independence.
Don't take that away from them because it doesn't fit your narrative.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}