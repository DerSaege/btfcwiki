---
title: Let's talk about questions on Russia's failure....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EVPIOwmWNpE) |
| Published | 2022/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the misconceptions surrounding Russia's capabilities and strategies in the current conflict in Ukraine.
- Russia's GDP of $ 1.5 trillion does not make them an economic superpower, limiting their military capabilities.
- Misinterpretation led to incorrect estimations about Ukraine, causing errors in judgment for both Russia and Western analysts.
- Russia's failed invasion of Ukraine was not part of some grand strategy but rather a miscalculation of their actual capabilities.
- Russia's lack of success in taking Ukraine's capital indicates a strategic failure rather than a deliberate diversion tactic.
- Russia's military shortcomings, such as the absence of infantry screens, have contributed to their struggles in the conflict.
- The belief that Russia could easily target Ukrainian President Zelensky is unrealistic due to their lack of specific technological capabilities.
- Russia's decision to not utilize its full military strength in the invasion is not a strategic move but rather a precaution against a potential NATO counterattack.
- Despite holding back troops for other concerns, Russia's miscalculations have been evident throughout the conflict.
- The conflict in Ukraine has exposed Russia's incompetence rather than any secretive or elaborate strategy.

### Quotes

- "Don't look for a hidden strategy where none exists."
- "Never attribute to conspiracy what you can attribute to incompetence."
- "Anything trying to explain away their failures is probably rooted in that."

### Oneliner

Addressing misconceptions about Russia's capabilities and strategies in the Ukraine conflict reveals incompetence over complex strategies.

### Audience

Analysts, policymakers, general public

### On-the-ground actions from transcript

- Support Ukraine by providing the necessary assistance to combat Russia's invasion (implied).
- Stay informed about the conflict and challenge misconceptions surrounding Russia's military capabilities (implied).

### Whats missing in summary

The full transcript provides an in-depth analysis of Russia's miscalculations and failures in the Ukraine conflict, offering valuable insights into the dynamics at play.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about some questions
that somebody sent me.
There are still people who are leaning into the idea
that Russia is on some next level 4D chess type of stuff.
So we're going to go through these questions, which
are being presented as unanswerable.
And we're going to answer them.
These questions get presented, and then it
leads people down this road where they
start embracing wild ideas.
And really, it's all centered on misunderstanding
one simple thing.
So we're just going to kind of run through it
and hope that we can kind of put this part behind us.
The real issue here for a lot of people
is perceived capability versus actual capability.
A lot of people, Western analysts included,
have equated Russia with the Soviet Union,
and therefore, Warsaw Pact, for a long time.
Russia has a GDP of $1.5 trillion.
They are not an economic superpower.
They cannot afford the military the Soviet Union had.
It's that simple.
That's where people need to start.
Because of the perceived capability being high
and the actual capability being pretty low,
it makes it even more dramatic.
So people have to seek out an explanation for why they
believed Russia was so strong, rather than just kind
of acknowledging that the estimates were wrong,
like they had to do when it came to Ukraine.
The estimates about Ukraine were that they
were going to fold in a week.
Most, not all, most Western analysts
believed Ukraine didn't have a shot.
They misread their intent, thought
they were going to roll over.
And that's the part you have to understand.
Russia did the same thing.
Russia believed, because of previous experience
with Ukraine in recent memory, they
believed Ukraine was just going to give up pretty easily,
that they were going to be able to occupy pretty easily.
So that's what they planned for.
And because they thought it was going to be easy,
they divided stuff up in a very PR fashion.
So each one of their little, so each group
within the Russian military got its time to shine.
So they would get good PR videos or whatever.
That's what happened.
And keep in mind, the operational template
that was used in Ukraine, exactly what they did,
you can go back and watch Let's Talk
About the Futures of Ukraine, and I outline exactly
what they did before they ever crossed the border.
It's not a mystery.
It's not some next level strategy.
They messed up.
Where did it start?
At the capital, which is good, because this is actually
the first question.
There are people who still believe
the capital was a diversion.
It wasn't.
This is how we know this.
They deployed the VDV, their paratroopers,
crack troops, top tier troops.
You don't waste troops like that on a diversion
where you know most of them aren't coming back.
They tried to take the capital from here.
So that's your big sign, is the presence of high end troops.
You don't waste them on diversions like this
that are going to have high losses.
The next thing people point to is, well, see,
it had to be a diversion.
That's why they ran out of supplies,
because they didn't really care about supplying them.
No, that doesn't make any sense.
The reality is the exact opposite.
They ran out of supplies because they
thought they were going to take the capital easily
and could resupply there.
So they didn't bring a lot.
If you were going to stage a diversion
and you knew you were going to be out in the field moving
around, you would bring more supplies.
You would bring more gas.
The thing that people are pointing to as evidence
of it being a diversion is pretty hard evidence
that they intended on taking the capital.
The next piece of evidence, a lot of these troops
brought their dress uniforms because they
thought it would be over.
They were coming for the parade.
Not something you would bring if you could be out
in the mud rolling around.
Another thing that they brought with them is the police.
And this is incredibly strong evidence.
If you're going to be doing a military diversion,
you don't bring a bunch of cops who don't know how to move.
You bring cops if you think you're
going to take the capital and you need riot control.
That's what they did.
It was a legitimate move on the capital, and they failed.
It's that simple.
Don't look for a hidden strategy where none exists.
They tried to take the capital, and they
tried to take it in the way that in that first video,
I said I didn't think was likely.
I didn't think it was likely because it
was pretty much guaranteed to end up the way it did.
There's the idea.
The next question is, there's the idea
that analysts are misreading Russia's grand strategy
because Western analysts don't know about combined arms,
I guess, because most Westerners who are looking at this,
they see it as hugely different because this
is on the plains of Europe and all of that.
They're looking at it and saying, this war is different.
Therefore, since it's different than what
Western analysts have been dealing with,
they don't understand it.
No.
That's not what's occurring.
People are pointing to the tank battles
and saying tank battles haven't occurred in a long time.
I mean, sure.
I mean, not in any widespread way, I guess.
The thing is, it isn't the West that
is failing to understand what they're doing.
Russia is not deploying infantry screens.
That's the reason Russia is taking so many losses when
it comes to armor.
Infantry screens are troops that sit outside.
They're outside of the tanks to protect the tanks
from things like javelins.
This is something that has been common since World War II.
This is something that any corporal in the Army
or the Marines would know.
It's a failure of doctrine on the Russian side of things.
That's why they're having so many problems,
because they have bad communication
and they can't actually achieve the combined part
of combined arms.
So the next question, why are they letting Zelensky,
why are they letting him go?
It's bold to assume that Russia has the capability
to take him out.
Let's be clear. They've tried.
They sent in Wagner.
It failed.
People are looking at Russia and believing
that it is comparable to the US in terms of its ability
to the US in every way.
Russia never put any research, any money, any desire
into developing total information
awareness on a battlefield.
That's not a thing that they tried to do.
So they don't have those capabilities.
It's also worth noting that I don't think the US could
do it in two months.
That's a pretty tall order.
This isn't a movie.
In real life, tracking down a specific person
across combat zones is really hard.
See, bin Laden took 10 years.
The idea that it's proof of some grand conspiracy
that Russia hasn't been able to do it in a couple months,
that's silly.
They have tried to do it, and they
have failed, because they don't have that technology.
Russia is a near peer.
They are not a peer.
They are not the Warsaw Pact.
Last one, and at least this one's true.
Russia isn't putting their full weight,
their full military strength, into this invasion.
That's true.
That's actually happening.
But it's not because there's some grand military strategy.
It's because they're staying prepared for a NATO
counterattack.
If they were to put all of their forces into Ukraine
and NATO was to respond, NATO would destroy them quickly.
Russia cannot stand up to NATO.
The only thing that makes them a near peer at this point
is their strategic arsenal, their nuclear arsenal.
That's it.
In a conventional fight, Russia versus NATO,
Russia doesn't stand a chance.
It's incredibly lopsided.
Aside from that, NATO isn't Russia's only concern.
They have taken bits of countries along their borders
for decades and decades and decades.
Japan, a country you wouldn't really
think of having a beef with Russia,
there's islands that are contested.
If Russia starts to waver, if they don't have the troops
to defend those islands, if Japan decides to take them,
Japan might decide to take them.
That's why those troops are being held in reserve.
It's not a sign of some grand strategy for Ukraine.
I think possibly the strongest evidence
that Russia miscalculated is them calling up
the reservists, troops that won't be ready for months.
Your diversion around the capital,
it would have to be diverting from something that mattered,
right?
They have months before the troops show up,
before they can actually really restart operations
on any significant level.
What Russia is going to try to do next, more than likely,
is move at a slow and deliberate pace
and take as much of the country as they can or they want.
That's where they're at next.
They're going to move out of the east and try to move slowly.
Now they're actually using a strategy from World War II.
This is why Ukraine is asking for different types
of equipment from the west now.
If the west gives it to them, Russia's probably
going to be defeated again.
The further they move from Russia,
the further those troops get out,
the more those logistical issues are going to show,
and the more they'll fail, especially
if Ukraine devotes any attention to disrupting those supplies.
People were expecting the Warsaw Pact.
They got Russia of today.
That's what happened.
There's no great mystery here.
Never attribute to conspiracy what you
can attribute to incompetence.
None of this is really shocking.
None of this is an unanswerable question.
It's all pretty basic.
Don't go down the conspiracy road with this at this point.
There were people who were making this claim
from the beginning.
The first week, we are weeks and weeks into this.
You don't suffer the kind of losses
Russia is suffering without enacting your grand plan
if you had one.
The reality is they're just losing the war.
That's the simple answer.
And if NATO continues to provide the assistance
that Ukraine needs, they will lose the war.
In many ways, when you're talking
about the actual goals of war, Russia lost in the first 72
hours because the war isn't always just about land.
There's the geopolitical considerations.
And after 72 hours, when they demonstrated
that they weren't the modern military that many countries
perceived them to be, they lost the war.
There is no way, no matter what happens at this point,
even if they were to take and hold the whole country,
Russia does not walk out of this in a better position
than when it started.
There's no way for that to occur now.
This was a huge error geopolitically.
There were huge errors in the strategic and operational
phases of this.
And even at the tactical level, because they're
using a lot of conscripts, they're failing there, too.
It isn't going well.
The answer is that Russia messed up,
not that there's some super secret strategy
that after all this time, they're
just waiting for the right moment to enact.
Don't look for a reason for your initial assumption
to be right.
The initial assumption was that Russia is the Warsaw Pact,
that they have that much power, that they have that capability.
They don't.
Anything trying to explain away their failures
is probably rooted in that.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}