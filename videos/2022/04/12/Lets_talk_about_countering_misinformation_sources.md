---
title: Let's talk about countering misinformation sources....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d2RtIxNdw-U) |
| Published | 2022/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is addressing the challenge of countering misinformation and stopping it before it spreads, especially when dealing with someone insistent on pushing false narratives onto others.
- Beau's mother's friend is trying to convert Beau's mom into a "do your own research" mindset, leading Beau to question how to handle the situation.
- Despite efforts to debunk misinformation, Beau feels like for every piece squashed, two more emerge, making it challenging to maintain the effort.
- Beau contemplates whether to continue engaging with the friend or find new strategies as they seem unable to convince the person.
- Beau suggests a proactive approach by catching misinformation sources off guard during subject shifts, exploiting their learning curve and tendency to make wild mistakes.
- By undermining the credibility of information sources and prompting critical thinking, Beau believes it is possible to reduce the spread of misinformation and train individuals to fact-check.
- Success in combating misinformation lies in seizing moments of vulnerability in sources and exposing their inaccuracies effectively, especially when the misinformation is recent and actively being spread.
- The key is to confront misinformation with irrefutable facts, prompting those spreading lies to question their sources and leading to a reduction in falsehoods being disseminated.
- Beau acknowledges the effectiveness of challenging misinformation with authority and correcting false statements to make a lasting impact on those propagating misinformation.
- Beau shares a strategy that has proven successful for them, advising others to capitalize on opportunities to debunk misinformation when sources are least prepared, thereby weakening their influence.

### Quotes

- "Every piece of misinformation is replaced by two the moment you squash it out."
- "You have a breather for a second."
- "So you have these people who buy into this talking point."
- "You start to train them to fact-check the people that are doing their research for them."
- "If you can catch them saying something with authority and it be just flat wrong, it tends to have a pretty marked effect."

### Oneliner

Beau addresses countering misinformation by proactively undermining sources during subject shifts, prompting critical thinking and reducing the spread of falsehoods.

### Audience

Friends combating misinformation.

### On-the-ground actions from transcript

- Challenge misinformation sources during subject shifts (implied).

### Whats missing in summary

Practical examples and detailed strategies for combatting misinformation.

### Tags

#Misinformation #FactChecking #Debunking #CriticalThinking #Community


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about how
to counter misinformation, how to kind of stop it
before it starts if you get somebody who's engaging.
Because I got this question.
A friend of my mother has turned out
to be one of those do your own research types
and seems insistent on converting my mom
to that outlook.
Being known as the one who stays informed at home,
I'm the one she comes to for a different perspective.
My god is the work hard.
Every piece of misinformation is replaced by two the moment
you squash it out.
I'm proud of my mother for not just taking one narrative
and accepting it, but I'm starting
to reach the end of my rope.
I vainly started thinking I could convince my mom's friend
through her, but at this point, that doesn't seem possible.
The problem is that my mom's is still in the middle.
I guess my question is either, what
do I do when I run out of rope, or how do I find more rope?
I understand the question is vague
and likely doesn't have an answer.
It does.
I could tell you where the rope store is.
Because your mom's actually engaging.
So what you're doing at the moment
is friend tells mom some piece of misinformation.
Mom asks you about it.
You debunk it, and then there's two more.
So the fact that there's engagement, though,
means that you can kind of go on the offensive here.
See, this weird thing happens.
Every time there's a subject shift,
like when you're talking about the misinformation sources,
the people who do it for profit, who
lean into the conspiracy stuff, and the do your own research
crowd, right?
Every time the subject shifts, like when
it shifts from vaccines to foreign policy or whatever,
there's a learning curve for them,
because they don't actually stick to a subject
that they know.
A lot of them come out in the beginning of a new topic,
a new news cycle, saying the most outlandish, weird stuff.
And that's the moment you can catch them.
And what you can do is undermine the entire source
of information, because most misinformation comes
from a few different sources.
A good example of this was at the beginning of Russia's
invasion of Ukraine, right?
You had a bunch of right-wing pundits come out,
and with their whole chests, just proclaim,
this wouldn't have happened under Trump, a question
nobody was asking.
But they went out there and said that.
And they provided these timelines.
See, this happened.
Under Obama, and this happened under Biden, but nothing
happened under Trump.
That opened the door to counter so many of them,
because they were so public about it.
The reality is the first time that Russian troops fired
at Ukrainian troops was under Trump.
Everything before then, everything before then,
everything before the Kurt Strait incident,
that was all contractors or proxies.
But the first time you had official Russian forces firing
on Ukrainian forces, and it was admitted, it was under Trump.
So you have these people who buy into this talking point.
And this is something that's irrefutable.
It occurred.
There's a Wicked Pundit.
It occurred. There's a Wikipedia page about it
when they do their own research.
That gives you that moment to ask one question,
because people who do their own research
tend to be conspiratorially minded.
Don't ask, why didn't they include this?
How did they miss this?
Why did they lie about this?
And let them do the work for you.
Because thus far, I have been presented
with a whole bunch of answers.
One was that the pundit was actually
part of a group that was opposed to Trump.
And this was done to make him look bad.
One was saying that they were paid by the Russians.
And one, they actually looked into the person,
and the person's actually just a horrible human.
But the point being is that these sources of information
aren't trusted anymore.
So then you're getting less misinformation coming out.
So when you squash one, it's not replaced by two immediately.
You have a breather for a second.
And if you can do this enough, you're
eliminating those sources of misinformation,
and you start to train them to fact check the people that
are doing their research for them.
So there's your directions to the rope store.
It works.
It works.
The problem is, most times, you have
to catch it when there's a subject shift.
In that period where the person who
is spreading lies for money hasn't read up
on the subject they're about to spread lies about,
because that is when they make the wild mistakes.
And once you can catch them in it,
you can undermine that whole source.
And the problem is, when you go back and try
to do it to something that happened in the past,
it's not as effective as something that they just heard
and they themselves have been repeating,
and then you show them it's a lie.
So that's where I'd go with it.
And it's worked.
It works well for me, but the people around me
are pretty full of pride.
So if you can catch them saying something with authority
and it be just flat wrong, it tends
to have a pretty marked effect.
I don't know if that transfers elsewhere in the country.
But around here, that one's super successful.
So I wish you luck.
Anyway, it's just a thought.
I have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}