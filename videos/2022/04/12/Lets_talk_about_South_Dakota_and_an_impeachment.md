---
title: Let's talk about South Dakota and an impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NX4bhLL1ZeU) |
| Published | 2022/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Attorney General of South Carolina, Jason Roundsburg, involved in a possible impeachment due to conduct in 2020.
- Roundsburg hit a pedestrian while driving, claimed he didn't know what he hit, and reported it to 911 before going home.
- State troopers indicate the pedestrian was on the hood of the vehicle for about a hundred feet, raising skepticism about Roundsburg's account.
- Roundsburg settled privately with the pedestrian's widow and pled no contest to misdemeanor crimes.
- Some legislators plan to pursue impeachment despite a select committee finding that Roundsburg's conduct did not warrant it.
- Roundsburg, supported by Trump, is likely to run for re-election despite the controversy.
- State troopers' evidence cast doubt on Roundsburg's narrative, potentially driving the impeachment effort.
- Vote on impeachment likely to occur on Tuesday.
- Roundsburg's decision to run for re-election despite the scandal might be fueling the impeachment push.
- Political scandal in state politics usually resolves quicker, making Roundsburg's situation unique.

### Quotes

- "State troopers are skeptical of his chain of events there."
- "It looks like the vote as to whether or not to pursue the impeachment will take place on Tuesday."

### Oneliner

Attorney General Jason Roundsburg faces possible impeachment in South Carolina over a 2020 incident involving a pedestrian, with state troopers' evidence casting doubt on his narrative and leading to a vote on Tuesday.

### Audience

Voters, legislators, community.

### On-the-ground actions from transcript

- Contact your legislators to express support or opposition to the impeachment proceedings (suggested).
- Stay informed about the developments and outcome of the impeachment vote (implied).

### Whats missing in summary

Details on the potential consequences of Roundsburg's impeachment and the impact on South Carolina politics. 

### Tags

#SouthCarolina #Impeachment #StatePolitics #JasonRoundsburg #Scandal


## Transcript
Well howdy there, Internet people. It's Beau again. So today we're going to talk about
some news out of South Carolina involving the Attorney General, Jason Roundsburg. It
involves conduct that occurred back in 2020 and looks to be leading to a possible impeachment.
Back in 2020, the Attorney General was driving down a rural country road and according to
him, hit something, didn't know what it was, but like a good law-abiding citizen, called
911 and reported it. Then he went home. The next day, the Attorney General returned to
the scene to find the pedestrian that he had hit and reported that discovery. South Dakota
state troopers are skeptical of his chain of events there. They provided a presentation
to the state legislature that indicated they believed the pedestrian was on the hood of
the vehicle for about a hundred feet and that the impact and the shattered windshield should
have told the Attorney General what he hit, especially considering the pedestrian's eyeglasses
were found in the vehicle. A select committee there determined that the AG's conduct did
not warrant impeachment. However, there are a number of legislators who plan on pursuing
it anyway and there are drafted articles of impeachment. It's worth noting the AG pled
no contest to some misdemeanor crimes and settled with the pedestrian's widow privately.
The AG there is in some ways supported by Trump, seems to have some connection there.
It's not clear how many Republicans are backing the impeachment effort. The Attorney General
is also a Republican, but it is likely that there's enough votes to kind of force the
Senate to at least consider it. The state troopers were apparently very convincing with
their evidence and the doubt that they cast on the Attorney General's narrative. I think
that most believed the AG would retire, not run for re-election, resign maybe. Since none
of that happened and it looks like the AG is going to run for re-election, that may
be driving this push. Normally, if there is a political scandal that can be capitalized
on, it doesn't quite take so long, especially when you're talking about state politics.
So we'll have to wait and see. It looks like the vote as to whether or not to pursue the
impeachment will take place on Tuesday. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}