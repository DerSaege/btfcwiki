---
title: Let's talk about getting banned books for free....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kmoiVDD9fpQ) |
| Published | 2022/04/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A trend of removing texts from libraries is not new in the US, with 2021 seeing 1,597 challenges or removals according to the American Library Association.
- Librarians have banned book week to bring attention to frequently challenged texts.
- It has become increasingly difficult to access certain texts in many places.
- The Brooklyn Public Library offers a virtual library card for individuals aged 13-21, granting access to 400,000 e-books, audio books, and other online resources.
- To obtain the virtual library card, individuals can email booksunbanned@bklynlibrary.org.
- Supporting this initiative can be done by donating through bklynlibrary.org.
- This access is available for individuals between 13-21 from New York State, with availability outside the state possibly limited.
- The initiative aims to ensure people can access the texts they want and serves as a temporary solution.
- Censorship in the US often involves denying access to texts rather than physically destroying them.
- Denying access to texts is a way to control who reads them without resorting to book burning.

### Quotes

- "Librarians have banned book week where they actually promote texts that are frequently challenged."
- "If you are between the ages of 13 and 21, you have a way to get a whole lot of them now."
- "They don't have to burn the book, just remove it."

### Oneliner

The Brooklyn Public Library provides virtual library cards to individuals aged 13-21, offering access to a wide range of texts amidst a trend of book removals.

### Audience

Book enthusiasts, Youth

### On-the-ground actions from transcript

- Email booksunbanned@bklynlibrary.org to obtain a virtual library card (suggested).
- Donate to support the Brooklyn Public Library's initiative through bklynlibrary.org (suggested).

### Whats missing in summary

The full transcript provides more context on the issue of book censorship and the challenges faced in accessing certain texts in the US.


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
books and how they are being removed and a workaround that has been put in place
by the Brooklyn Public Library, something they have come up with that definitely
deserves a little bit of attention. If you're not aware, the trend with
removing texts from libraries, that's not new in the US. It's not something that
started this year, it's just something that they really started pushing and
using as a political talking point. In 2021 there were 1,597 challenges or
removals from libraries and that's according to the American Library
Association. It's not a new problem and it goes back a long long time, so much so
that librarians have banned book week where they actually promote texts that
are frequently challenged. In a lot of places it's grown very difficult to get
your hands on certain texts. Well, if you are between the ages of 13 and 21, you
have a way to get a whole lot of them now. The Brooklyn Public Library will
grant you a library card, a virtual library card, that will allow you to
access about 400,000 e-books and audio books as well as other online resources
from the library like genealogy or accessing studies, stuff like that. All
you have to do to get it is send an email to booksunbanned, all one word, at
bklynlibrary, one word, dot org. Send them an email and they will guide you
through the process I guess. Now if you are interested in supporting this, you
can go to that website bklynlibrary.org and there is a place to donate. This is a
limited time thing. If you're between the ages of 13 and 21, you'll always be able
to get one if you're from New York, if you're from New York State. Outside of the
state, this is... it may not last forever, but it will serve as a little stopgap to
make sure that people can access the texts that they want. You know, when we
talk about censorship, when we talk about books, there's that image of
them being burned or people physically destroying the texts, taking them out of
circulation in that way, making it illegal for them to be sold or possessed.
In the US, they don't have to do that. Because of the way our society is
structured, those who probably need to read them the most, they just have to
deny them access. They don't have to burn the book, just remove it. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}