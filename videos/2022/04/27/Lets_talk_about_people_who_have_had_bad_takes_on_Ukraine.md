---
title: Let's talk about people who have had bad takes on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=36pzcSjwsYc) |
| Published | 2022/04/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing bad takes on Russia and Ukraine, particularly from leftist individuals supporting Russia in the conflict.
- People are doubting the beliefs of these individuals due to their support for an authoritarian state in an imperialist conquest.
- The association of Russia with the USSR and leftist ideologies is prevalent among some individuals.
- Many commentators who stray into topics outside their expertise make mistakes, particularly those focusing on philosophy and political science.
- Commentary on the conflict is influenced by pressure to talk about it and connections to the Soviet Union.
- Individuals discussing military affairs without expertise often make errors, like using outdated terminology such as "cauldron battle."
- The conflict in Ukraine requires a current understanding, not historical references from the past.
- U.S. propaganda and misinformation impact commentary on war, leading to overcorrections by commentators.
- Being wrong about a particular topic does not invalidate everything else a person has said.
- Ideas should be judged based on their merit, regardless of who presents them.

### Quotes

- "It's not unforgivable that somebody had a bad take on this."
- "Ideas stand and fall on their own."
- "Nobody is going to be right on it all the time."

### Oneliner

Addressing bad takes on Russia and Ukraine, commentators stray outside their expertise, influenced by pressure and historical connections, but ideas should stand on their merit alone.

### Audience

Commentators and viewers

### On-the-ground actions from transcript

- Fact-check information and avoid spreading misinformation (implied).
- Encourage critical thinking and independent analysis of ideas presented by commentators (implied).
- Seek out diverse sources of information to gain a well-rounded understanding of conflicts and political issues (implied).

### Whats missing in summary

The nuanced exploration of how individuals' areas of expertise can influence their commentary and the importance of separating ideas from the messenger in evaluating viewpoints.

### Tags

#Russia #Ukraine #Commentary #Propaganda #IdeasEvaluation


## Transcript
Well, howdy there, internet people, it's Beau again. So today, we're going to talk about
people who have had bad takes on Russia and Ukraine and
how to look at them in the future and
how you may feel about things they've said in the past now that these takes exist.
We're going to talk about this because I've got a whole bunch of messages on this topic
most related to people on YouTube.
And then I have one that flat out says it.
The part that we really need to address, it actually says the words, where the other ones
it didn't.
Bo, I've been struggling with something.
That is, why are so many seemingly leftist folks supporting Russia in the current conflict?
I wouldn't be conflicted if the folks I'm referring to weren't incredibly bright, intelligent
people.
They're some of the smartest people I've ever known.
I feel as though the reason for their support of Russia is foolish and shallow.
It seems they're thinking of Russia as some strange analog for socialism, which we've
talked about.
But all their other positions are thoughtful and well formulated.
I'm having trouble figuring out what to make of this discrepancy.
Honestly, it has me questioning a lot of the beliefs these folks have helped lead me to."
Their takes on Russia and Ukraine were so bad, it now has people doubting other things
that they'd said that that person had then accepted as right and true and good.
I just can't understand why such smart and anti-authoritarian thinkers are supporting
an authoritarian state in an imperialist conquest.
Yeah.
We've got one.
There is the habit among a lot of people to somehow associate Russia of today with the
USSR, which doesn't make any sense, but it's very prevalent.
They're especially among people of leftist ideologies.
They associate the USSR with socialism or whatever brand of leftist ideology they support,
and they feel some kind of weird loyalty to it, or at least they want to root for it every
once in a while, even though the Russia of today is not leftist in any way.
Then you also have a group of people who see Russia as a hedge against NATO expansion and
imperialism.
And we're going to have a whole video on this soon.
So you have that playing into it as well.
But there's something that's harder to spot, but is far more important.
Those are pretty easy to pick up on.
But the thing that's harder to spot, and this is true of every person that I've seen do
this on YouTube, now this person's message, it's written in a way where it could be about
people in their own personal life, but every other message I got on it was very much about
people on YouTube.
What's their area of expertise?
Why did you start following them?
When people start following commentators, they follow them for a reason.
There's something that initially caused you to start following them.
I'm willing to bet you didn't start following any of these people for their commentary on
war.
That's probably not why you started following them.
Because every one of them that I know that has gone down this road, their area is philosophy
and political science.
That's what they know about.
And now they're just providing commentary on something outside of their actual area
of expertise.
It's outside of their scope, so yeah, they make mistakes.
I mean, even if you really are well-versed, you're going to make mistakes.
But if it's just outside of your scope, you're more likely to.
So that should kind of alleviate some of it when you start to think about it.
They're not bad people.
It's not that they're not intelligent and they tricked you before.
It's that they're talking about something they aren't an expert on now, so they make
mistakes.
It's really that simple.
When you start providing commentary, people will ask you to provide commentary on all
kinds of things, and I would imagine those people felt a whole lot of pressure to provide
commentary about this conflict because it's everywhere.
It's all over the news.
It is the topic, right?
So they probably felt pressure to talk about it.
And then some of them, because of that weird connection to Russia via the Soviet Union,
they felt the need to cheerlead.
And they started providing commentary on what's going on on the ground.
And one of the things about that that is super easy to see is that most of them, that they
They didn't talk to people who are up on current military affairs.
They talked to military historians.
Those two things are related, but they're not the same.
It's like, hey, you make great food.
You're a really good cook.
Can you farm?
They're related, but they're not the same.
And I know this is true for a whole lot of them, because if you've been watching them,
I'm certain over the last couple of weeks you've heard the term cauldron battle over
and over and over again because they were predicting one develop on the eastern front.
The thing about that, that is a really old terminology.
That's a really old terminology and there is nothing that suggests Russia's up to that
right now.
The term cauldron, we've talked about it before, strategic, operational, and tactical.
There's three levels to this.
Cauldron battle is specific to the strategic level.
The operational level is called a bag or a sack.
The tactical level is a nest.
Now this is by Soviet doctrine, which is where they're pulling this stuff from.
Interestingly enough, most of this stuff comes from the first half of the 1900s.
They're talking about a strategy that was developed, I mean, 80 years ago.
They're talking about something that was developed before the invention of close air
support or GPS, things that altered the face of war.
But you've probably heard that term over and over again because what's happening there
is very reminiscent of some stuff that happened in the 1940s.
So it makes sense from that perspective.
However, the battlefield of today is super mobile.
And pulling off a cauldron intentionally, that requires a lot of ability.
that Russia has not shown on the strategic level. Strategically, on the
strategic level, this entire thing has been a disaster. There are no
redeeming points to it. And that term shows that they went to history
rather than current information. Because most of them are based in the field of
philosophy or political science, they know how thick U.S. propaganda is. They
understand how in the U.S. there are certain philosophies or political
ideologies that are just outside of the Overton window for no good reason other
than a constant barrage of this is the only acceptable ideology. You have to
believe this way. So they probably expect that when it comes to war, that
same level of propaganda, and it's there. Everybody at that poker table is lying
and cheating. We've had multiple videos on this because everything's an
information operation. What's said, what isn't, everything is. And because of that
they probably overcorrected and they went a little too far because this isn't
their area, they can't pick up on a lot of the stuff that people who are current on this
topic can.
So it makes their takes worse.
But again, I'm willing to bet you didn't start following any of them for their coverage
of this type of conflict.
started following them and started accepting their commentary for philosophical or political
reasons.
Them being wrong about this doesn't have anything to do with the validity of what they
said before.
Everybody will be wrong about something.
Everybody.
So I don't think that people having a bad take here means that they didn't mean what
they said before, or they didn't really believe.
They're not really anti-authoritarian.
They got swept up in propaganda because they were trying to guard against what they're
used to, so they went to the other side.
It's not, to me, it's not unforgivable that somebody had a bad take on this.
I would point out that as far as public-facing analysts who called into question the official
estimates on how all of this stuff was supposed to go, you're talking about less than five
out of hundreds and these are people who this is their area, this is their area of expertise,
it is their scope.
The fact that somebody who doesn't cover conflict or doesn't really have that deep
of an understanding of it made a mistake, that's kind of to be expected.
There were a lot of surprises in this one.
So I wouldn't go back and reanalyze everything they ever said.
Because at the end of the day, ideas stand and fall on their own.
Not the person who said it, not the person who disagreed with it.
They exist by themselves.
said it, sure, they introduced it to you, but whether that idea is something that
you want to accept or reject, it's based on the idea or it should be. It shouldn't
have anything to do with the messenger.
Nobody is going to be right on it all the time. Nobody who gets up here and
talks into a camera is always going to be right. At the end of the day, they're
providing their perspective, their commentary, and it is just a thought.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}