---
title: Let's talk about NATO expansion and Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d70Hz7x2rog) |
| Published | 2022/04/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the popular talking point of using NATO expansion to justify Russia's actions in Ukraine.
- Breaks down the concept of imperialism today, including force of arms, puppet governments, threats, and economic control.
- Clarifies that NATO expansion is not imperialism, but rather individual NATO countries may have engaged in imperialist actions.
- Points out that Russia utilized all forms of imperialism in its actions, branding it as engaged in imperialism.
- Dismisses the argument of Russia's preemptive strike due to NATO expansion as hollow and revealing an imperialist mindset.
- Criticizes justifying Russia's actions by comparing them to what the U.S. might do, pointing out U.S. history of imperialism.
- Challenges scenarios suggesting U.S. response to alliances with Russia as flawed logic.
- Addresses past U.S. politicians' views on NATO expansion and their imperialist mindset.
- Compares Russia's behavior to that of the U.S., both engaging in empire building and imperialism.
- Condemns the misguided argument of using NATO expansion to justify imperialism and aggression.

### Quotes

- "NATO expanding and accepting new members is not imperialism."
- "Russia is engaged in imperialism. Period."
- "This whole talking point of trying to blame it on NATO expansion is a colonizer imperialist mindset."
- "Russia is a capitalist oligarchy engaged in imperialist aggression."
- "Saying that it's okay is justifying imperialism."

### Oneliner

Beau explains the flaws in justifying Russian actions through NATO expansion, pointing out the imperialist mindset behind it and condemning imperialism.

### Audience

Activists, policymakers, educators

### On-the-ground actions from transcript

- Revisit and adjust your stance on justifying imperialism and aggression based on flawed comparisons and historical contexts (suggested).
- Research and understand the dynamics of NATO expansion and imperialist actions to prevent misguided justifications (exemplified).

### Whats missing in summary

Deeper insights into the implications of justifying imperialism and aggression, urging for critical examination of historical parallels and global power dynamics.

### Tags

#NATO #Imperialism #Russia #US #Aggression #Colonialism #GlobalPolitics #Policy #Activism


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about NATO expansion and how it works
and how people are using it as a reason, a cause, a justification for Russia's actions in Ukraine.
And we're going to kind of dive into that and see if it makes any sense,
because this has become a very popular talking point among people who,
if they analyzed it, I'm willing to bet they wouldn't be using it.
So we're going to kind of go through it.
The idea is that Russia had to respond because of NATO expansion,
and that term often gets conflated with NATO imperialism.
How is imperialism conducted today?
What are the methods? There's a few.
One is force of arms, where a country shows up and just takes the land and says,
you know what, this is our country now.
You're part of whatever country the military came from.
Or they show up force of arms and they set up a puppet government in that country,
and that puppet government is loyal and in the sphere of influence of the main country.
That's one way it happens. That's another way it happens.
A third way is just force of arms, but threats.
Do what we say because you're geographically close to us,
or we're going to show up with tanks and guns and stuff.
And then the fourth way is loosely the economy.
Corporate logos instead of flags, you know, kind of weaponizing debt.
If you control the economy, you control the money.
If you control the money, you control the politicians.
That's true in pretty much every country.
So that's a pretty effective way of doing it.
Which one did NATO do to get Ukraine to join?
None of those, right?
When countries join NATO, they ask to join NATO.
And it's actually a really lengthy process that has a whole lot of hoops
they have to jump through to get NATO membership.
NATO expanding is not NATO imperialism.
The term NATO imperialism is more about countries within NATO
engaging in imperialist actions in the past and to this day.
And that's a thing. That happens.
But the expansion of the alliance itself, that's not imperialism.
Those are just countries acting out their will on the foreign policy scene.
That's all that is.
NATO expanding and accepting new members is not imperialism.
A lot of the stuff that NATO countries do individually is.
So it's a misunderstanding of those two terms that leads to this.
Okay. So just out of curiosity, what about Russia?
Which of those three or four options of how imperialism is conducted today did it use?
Any of them that had force of arms? They did all of them.
Russia is engaged in imperialism. Period.
That's what it is.
You can try to say that Russia launched a preemptive strike because of NATO expansion.
It's hollow for a whole bunch of reasons that we've gone over in other videos.
But I mean you can say that. Sure. Why not?
But that's only a justification if you're an imperialist.
You can only use that as a valid justification if you yourself have an imperialist mindset
or a colonizer mindset.
Because at its core, what it's saying is that Russia had the right to invade a neighboring country
that was perceived to be in its sphere of influence because the neighboring country didn't do what Russia said.
It's an imperialist mindset.
This whole talking point of trying to blame it on NATO expansion is a colonizer imperialist mindset.
It's where it comes from.
The heart of it is that the smaller country must do what the larger country says to avoid being invaded
and broken apart into smaller puppets or just annexed, so on and so forth.
That's imperialism.
Now, how do people try to justify this?
How do they try to make this argument work? Because this part often gets pointed out.
They create scenarios like, what should the U.S. do or what would the U.S. do if Mexico signed an alliance with Russia?
If your strongest argument is comparing Russia's actions to what you think the U.S. would do,
maybe you need to rethink your position.
The United States is a country that has engaged in imperialism and empire building since day one.
So if you're saying that Russia would behave in the exact same way, it's imperialism.
I would also point out that that's not entirely accurate.
Most people, when they think of countries that are close to the United States, they think of Canada and Mexico.
And that's where the list ends. In the waters south of Florida, there are a whole bunch of islands.
And contrary to popular belief, those aren't just playgrounds for rich white folk.
Those are countries. Interestingly enough, they have a military alliance that the United States isn't part of.
Applying the same logic that gets used with Ukraine.
Ukraine joining an alliance that Russia is not part of.
You're giving the United States justification to invade Nevis and St. Kitts.
That may not be the best argument.
Just saying. Another one that gets brought up is a bunch of politicians, U.S. politicians back in the early 90s,
saying, you know, if NATO expanded, well, that would provoke the Russians.
And people today are pointing to this and saying, see, look.
Yeah. So a whole bunch of U.S. politicians, a country that has historically engaged in empire building,
viewed foreign policy that way.
If you can point to anybody who said that that wasn't an imperialist,
any of those public officials that signed that letter,
if you can point to any of them that wasn't an imperialist, that would be useful.
But you won't find one because they all are.
The position they were coming from was still maintaining Eastern Europe as subordinate to Moscow,
as subordinate to Russia. That was their sphere of influence.
Yeah, a bunch of imperialists gave you a justification based on the imperialist mindset that they have.
There's no big surprise there.
And then it goes to that one final piece.
There is that alliance with all of those countries south of Florida.
Why doesn't the U.S. invade them? Because they're not really powerful. Right?
What about the NATO countries that are right up against Russia's border?
And they've already been there. They were already in existence.
People kept saying, well, they couldn't have NATO right up against their border.
They already did. That was already a thing.
But they didn't matter. Why?
That wasn't enough to provoke a response. Why?
Because they weren't powerful. Not like Ukraine.
Not like a country that stands a chance of becoming a major power in Europe.
That one? Oh no, that one Moscow has to control.
Just like the United States ignores the island countries, Russia did the same thing.
Russia's behavior mirrors the United States.
If you would say that the U.S. engages in empire building, that it engages in imperialism,
and Russia's behavior is so identical that you don't even have to work to make an analogy.
You just say these two things both happened. What does that say about Russia?
Russia's an imperialist country. They always have been.
I mean, they literally had an imperial Russia period.
So this argument about NATO expansion, it's misguided.
It doesn't add up. It doesn't make any sense.
All it does is further the idea that imperialism is okay.
That's how the world should work.
Because at this point, if you're using this argument right now,
you're saying it is okay to bomb a country, to invade a country, to level cities.
Because, well, the smaller country didn't do what they were supposed to.
They exercised their sovereignty on the international scene against the wishes of their imperialist master.
That doesn't seem like a take that most people using it would really want to have.
But that's what it is. Make no mistake about it.
Russia is not the USSR. Russia is not a leftist country.
Russia is a capitalist oligarchy engaged in imperialist aggression.
That's what it is. There is no way to get around that.
I would, if you've used this argument, I would rethink it.
If you've heard it and it kind of made sense because of being more familiar
with NATO countries and their history, I might look a little more into it.
Because just like Russia's actions when we first started talking about this in the lead up to the invasion
and how they manufactured consent and the symbolism that they used and all of that stuff,
how we were able to hold up a mirror and see the United States,
the same thing works in reverse because both countries engage in empire building.
Both countries do this. That doesn't mean that it's right.
It's the way that it currently is.
But saying that it's okay is justifying imperialism
and it will be used to justify further American empire building.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}