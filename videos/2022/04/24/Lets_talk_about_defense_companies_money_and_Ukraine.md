---
title: Let's talk about defense companies, money, and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NYe5U7fsR9A) |
| Published | 2022/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the common talking point that the US provides military aid to help defense contractors, acknowledging its accuracy at times.
- Points out that defense companies profit from war, which can be seen as unethical or as blood money.
- Mentions the military-industrial complex created by the defense industry and Congress, leading to continuous cycles of profit.
- Provides a perspective on the amount of aid provided to Ukraine compared to the revenue of major defense contracting companies.
- Emphasizes that the aid to Ukraine, at $3 billion, is a very small percentage of the total revenue of these companies.
- States that for big defense contracting companies like Raytheon, the aid amount is insignificant and more like a tax write-off.
- Notes that while some companies may profit from developing new military technology like drones, they often end up being acquired by larger corporations.
- Suggests that while US policy is sometimes influenced by money in the defense industry, in this case, the aid to Ukraine is not a significant money-making venture for these companies.
- Warns about potential future increased spending in the defense industry post-conflict, where companies might profit substantially.
- Anticipates that if defense industry interests start influencing aid packages significantly, it will lead to a surge in total spending.

### Quotes

- "Defense companies make money off of war. It's literally the drive behind their product."
- "The amount of aid that we have provided to Ukraine is what gets rounded off when you're talking about their revenue."
- "To them, that's a tax write-off."
- "This isn't defense industry money."
- "It's not really happening with the aid."

### Oneliner

Beau explains the nuances of US military aid, clarifying that while defense contractors profit from war, the aid to Ukraine is not a significant money-maker for them.

### Audience

Public, Policy Makers

### On-the-ground actions from transcript

- Monitor future spending post-conflict for potential excessive defense industry influence (implied).
- Stay informed about military aid and defense industry dynamics (implied).

### Whats missing in summary

Insight into potential future implications of defense industry influence on military aid and spending.

### Tags

#USmilitaryaid #DefenseContractors #MilitaryIndustrialComplex #DefenseIndustryInfluence #FutureSpending


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a talking point,
something that comes out a lot.
And sometimes it makes sense,
and sometimes it's absolutely true.
But because it's something that is widely believed,
it gets used when it's not exactly accurate.
Anytime the United States engages in any kind of military aid,
you have people say,
well, that's to help the defense contractors.
I mean, yeah, they're going to make money on it.
But there's also something else at play.
The reason this talking point exists is because a lot of times it's true.
Defense companies make money off of war.
It's literally the drive behind their product.
So that, for most people, is kind of unethical.
It's not really what you want.
And then, at the very least, it's seen as blood money.
So you have that aspect of it.
And then you have the other part,
which is the defense industry, combined with Congress,
creates the much-famed military-industrial complex, right?
Where that soft corruption just cycles around and around and around,
and everybody makes money.
Because of those two things,
that talking point sticks easily,
even at times when it is less than accurate.
It's something that's now being said.
The U.S. is only helping Ukraine
because it makes the defense contractors money.
And when you see the dollar amounts of aid,
I mean, yeah, those are wild amounts.
I mean, you hit that point where a number gets so large
that you don't have any concept of it.
You have no idea of a way to boil that down.
How many cups of coffee is that?
But, like many things,
if you're trying to conceptualize something, put it into perspective.
So what we're going to do real quick is do that
with the aid that is being provided to Ukraine.
Okay, the big boys, right?
The huge defense contracting companies.
Raytheon, Boeing, General Dynamics, Lockheed, Northrop,
BAE, and Honeywell.
Okay, those are multi-billion dollar companies.
Together, if you rounded off their yearly revenue,
it would be $320 billion.
That's a lot of money.
How much aid have we provided to Ukraine?
$3 billion and change.
About 1%.
Another way to put it into perspective,
I said if you rounded it off, right?
You're rounding off from $323 billion.
The amount of aid that we have provided to Ukraine
is what gets rounded off when you're talking about their revenue.
It's not a driving factor.
It's not a driving factor.
Now, at the same time,
I understand that there are other times when this criticism is 100% accurate.
But it's one of those things when something becomes a talking point
and people try to apply it to everything.
In this case, yeah, Raytheon doesn't care.
They do not care.
The big defense contracting companies that people talk about
when they make this argument, this isn't money to them.
I know to us, our share of $3.whatever billion,
that's life-changing.
To them, that's a tax write-off.
So that's probably not a huge drive there
behind why the U.S. is providing this aid.
There's not much money there
because that's the total cost of this stuff,
not their profit. Their profit's even less.
And again, this is just seven companies
and that money would be divided amongst all of them
and all the companies that aren't named.
Now, there are some companies, like the ones developing the new drones,
they're going to make a fortune because they've got a new toy.
They have that new product and it's going to sell.
But realistically, within the next couple of years,
that company's going to be owned by one of the seven I named.
That's how it tends to work.
So, understand this isn't a bad talking point.
It's definitely something to always keep in mind
because there are times when U.S. policy is dictated by money
and sometimes that policy is dictated by money
that's going to be made by the defense industry.
That is a real thing. That actually happens.
It's just probably not happening here.
The numbers are too small.
This isn't money they'd get out of bed for, to be honest.
It's not really that much.
Now, what you need to watch for
and when the defense industry could kick in
and start to do its thing and make a bunch of money is after this.
Once the conflict's actually over and they convince Congress,
hey, you know what?
Because of that one week lag it took to get those switchblades,
you probably need about 10,000 of those
on a shelf somewhere just sitting, waiting,
in case you ever need them.
Why don't you go ahead and order those?
Here's a campaign contribution.
That is when it comes into play.
Not right now.
So the conflict in Ukraine will probably lead
to what people are talking about
and the just rampant spending that occurs in the US defense industry.
But it's not really happening with the aid.
If the defense industry was pushing the aid,
oh, those numbers would be so much bigger.
I'm kind of surprised they're not, to be honest.
If it goes on much longer, they may start.
And if you see those numbers,
the aid packages start to balloon in total price tag,
then it's probably happening.
But right now, this isn't defense industry money.
This isn't something that they're going to waste a call to one of their senators on.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}