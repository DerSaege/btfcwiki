---
title: Let's talk about Japanese-Russian relations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gMtPzQI6t4A) |
| Published | 2022/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Moscow promised not to invade Ukraine but later invaded and took land, breaking the promise.
- Japan historically refrained from criticizing Russia internationally due to hopes of getting the Kuril Islands back.
- Japan's response to Russia's invasion of Ukraine was different this time, with sanctions and diplomatic protests.
- Japan is sending Ukraine drones and CBRN equipment to protect against chemical, biological, radiological, and nuclear threats from Russia.
- Putin responded by saying Russia won't talk about giving the Kuril Islands back.
- Moscow's historical imperialism is concerning for Ukraine since they tend to keep any land they take and hold.
- Ukraine might push into Crimea or other contested areas as they regain the initiative, with increasing international support.
- The longer the conflict drags on, the more support Ukraine might gather.
- Russia has a history of changing maps and holding onto conquered territories.
- Ukraine can learn from Japan's experience with Russia in understanding the importance of historical precedents.

### Quotes

- "Moscow signs a statement of, you know, hey, don't worry, we're not gonna, we're not gonna come after you, we're gonna be neutral, don't worry about it."
- "They've apparently given up on that, or they realize that they're not going to get the islands back by remaining quiet."
- "We're just not going to even talk about giving this land back."

### Oneliner

Moscow's historical imperialism poses a threat as Japan's response to Russia's invasion of Ukraine shifts, with Ukraine likely to gain international support against Russian aggression.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Send aid and support to Ukraine (suggested)
- Advocate for diplomatic efforts to resolve conflicts (suggested)

### Whats missing in summary

The full transcript provides a detailed historical context for understanding the dynamics of Russian-Japanese relations and their impact on Ukraine.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Russian-Japanese relations.
How they sit on the foreign policy scene, particularly in how it pertains to Ukraine.
And you know, we all know how this started.
Moscow signs a statement of, you know, hey, don't worry, we're not gonna, we're not gonna
come after you, we're not gonna invade, we're gonna be neutral, don't worry about it.
And then, you know, four years later, country looks a little weak, so they invade, take
some land, and don't give it back.
So that's how it started.
But we're not talking about Ukraine right now.
We're talking about Japan in 1945.
In 1941, Moscow said that, you know, they weren't gonna go after Japan.
But towards the end of World War II, well they had a little change of heart, and they
seized the Kuril Islands.
Now because of this, Japan for a long time didn't want to criticize Russia on the international
stage.
Didn't want to take a hard stance, because there was always the hope of getting this
land that they refer to as the Northern Territories returned to them.
This is something that historically was Japanese land.
They've apparently given up on that, or they realize that they're not going to get the
islands back by remaining quiet.
So when Russia invaded Ukraine, the response from Japan was a little bit different this
time.
They said nothing really when Crimea happened.
But this time, well they amped things up a little bit.
There were sanctions, diplomatic protests, so on and so forth.
They've also chosen to send Ukraine drones and CBRN equipment.
CBRN being chemical, biological, radiological, and nuclear.
So they're sending them equipment to protect from the unthinkable, from Russia escalating
to things we hope they don't.
Now as can totally be expected, Putin comes out and says, oh well fine then, we're just
not going to even talk about giving this land back.
I was going to, you know, we were going to talk about it some more, talk about giving
you these islands back, but this stopped it after more than 75 years.
Russia's old-school imperialists.
Moscow has always been old-school imperialists.
They want to change maps.
This is a real concern for Ukraine at the moment, because if there's anything that Ukraine
can learn from Japan, it's that if there is any declaration that's signed, Russia's not
going to back out.
Any land that they can take and they can hold, they will, because that is what they have
always done.
That's the historical precedent.
This is why I'm fairly certain that eventually, as Ukraine regains the initiative, you're
probably going to see a push into Crimea or the other breakaway areas, the other contested
areas.
And the longer this drags on, the more international support it appears Ukraine's going to have.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}