---
title: Let's talk about gas, insulin, and republican strategy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nXYrzRcbUW8) |
| Published | 2022/04/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examining the Republican strategy of portraying the Biden administration as inactive in helping average Americans, using rising gas prices and other costs as a talking point.
- The Biden administration plans to release a million barrels a day from the strategic reserve to mitigate the impact of increasing gas prices, aiming to assist but facing skepticism about the significance of the action.
- The Republican Party is likely to criticize the administration's efforts as insufficient and propose measures like price caps to appeal to working Americans before the midterms.
- Despite the need for assistance, especially evident in the case of insulin prices, a bill passed by the House to cap insulin costs at $35 a month faces challenges in the Senate, where Republican support is necessary for it to pass.
- The bill aimed at helping millions of Americans with insulin costs faced significant Republican opposition in the House, illustrating a focus on obstructing rather than aiding struggling citizens.
- Beau criticizes Republicans for prioritizing political power over policies that could benefit Americans, pointing out their potential to blame rising costs on the Biden administration while actively impeding solutions.
- Some states have waived gas taxes during the crisis, contrasting with Republican governors who maintain such taxes, potentially hindering relief efforts and favoring political agendas.

### Quotes

- "They care about power, not policy."
- "Their strategy, going into the midterms, is to hurt you as much as possible and blame it on Biden."
- "They want Americans hurt so they can blame Biden."

### Oneliner

Republicans obstruct assistance measures, prioritizing power over people, aiming to blame Biden for economic challenges pre-midterms.

### Audience

Voters and community members.

### On-the-ground actions from transcript

- Contact your Senators to support bills aiding Americans with rising costs (suggested).
- Advocate for policies that prioritize people over political games (exemplified).

### Whats missing in summary

Full context and emotional delivery of Beau's passionate criticism of Republican actions and their impact on struggling Americans.

### Tags

#GasPrices #RepublicanParty #BidenAdministration #Midterms #Assistance #PoliticalAgendas


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about gas prices
and other costs for the average American.
And we're going to talk about the Republican talking point,
about how the Biden administration isn't doing anything.
Well, at least not doing enough.
And we're going to kind of examine that,
because that's their strategy going into the midterms.
The idea is to cast the Biden administration
as though it isn't doing anything
to help the little people
and that the Republican party, they would be different
because they care about those of us on the bottom.
That's the idea.
Okay, so the Biden administration has announced
that I guess they're gonna pump out a million barrels a day
out of the strategic reserve.
going to release that in an attempt to cushion the blow from rising gas prices.
The administration is saying it's going to be significant.
I don't know about that, but it is going to help.
It will help cushion the blow.
I just don't know about that significant part.
The Republican Party undoubtedly is going to say this isn't enough.
He should cap prices.
He should do whatever to help the average working American because that's the image
they want out there heading into the midterms, that it's the Republican Party who cares about
working Americans.
The thing is, we're sitting here, we're rationing gas basically.
I know a whole lot of people who used to be in the category of folks who always filled
their car up when they go to the gas pump, and now they're throwing in like 20
bucks. You know, not putting it all in right now. Imagine if you had to do that
with something that was supposed to keep you alive, like insulin. The news that's
going out is that the House passed a bill to cap the cost of insulin to, I want to
Let's say $35 a month.
This is something that will affect somewhere between seven and 10 million Americans.
That's a lot of families when you start breaking it down.
That's a whole lot of people that will be impacted other than those directly using it.
We focus on the seven to 10 who actually need the insulin, but those people have families.
This will help those families and their overall economic well-being.
passed the House. It passed the House with every single Democrat voting for it
and 12 Republicans. 193 voted against it. Something to help average working
Americans. And now the bill is going to go to the Senate and the odds are that it
won't make it. They have to find at least 10 Republicans to sign on in the Senate
for it to go through. But see, if they obstruct this, if they stop this, if they hurt seven
to ten million Americans and their families, well, that helps their narrative. That helps
them in the midterms. That helps them be able to say, see, the Biden administration didn't
do anything for you, even though it was them that stopped it. Their strategy, going into
to the midterms is to hurt you as much as possible and blame it on Biden.
A hundred and ninety-three Republicans in the House voted against this.
In these economic times, while they're sitting there just railing about a dollar
increase in the price of gas, they don't want to help people with medicine, with
something they need to survive, something they need to live, that, oh no, we can't
do that, because that might make the Biden administration look good, and that's
what they care about. They care about power, not policy. This bill, it's going
to the Senate because it did make it through the House over almost every
Republicans' objection. If there's that kind of turnout in the Senate, it won't pass.
And I am certain that the Republican Party will find some way to blame that on Biden
and say, hey, look how much you're paying for these meds. Think about all the costs
that are going up right now, that's the Biden administration's fault, never mind that they
torpedoed pretty much every economic package he's put forward, because that's their plan.
The plan heading into the midterms is to hurt you as much as possible.
You know, a whole lot of states have decided to go without their gas tax while this is
going on.
I know that there's at least one Republican governor who's constantly talking about gas.
And that state still has their gas tax.
Because they don't want to be part of the solution.
They don't want to help Americans.
They want Americans hurt so they can blame Biden.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}