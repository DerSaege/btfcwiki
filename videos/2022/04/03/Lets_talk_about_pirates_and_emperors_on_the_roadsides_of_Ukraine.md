---
title: Let's talk about pirates and emperors on the roadsides of Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Foi-W36dpho) |
| Published | 2022/04/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the concept of the Ukrainian resistance using roadside devices.
- Mentions people questioning the morality of such tactics.
- Shares a story from St. Augustine about pirates and emperors to illustrate moral perception.
- Questions the moral difference between roadside devices and artillery.
- Points out that the effectiveness of a tactic like roadside devices is why they are used.
- Advises not to base morality on a nation-state's perspective.
- Emphasizes that one can support or oppose actions of their government.
- Encourages opposing government morality to strive for improvement.

### Quotes

- "You are not your government."
- "You can support pieces of something and oppose others."
- "If you want to be an advanced citizen, you have to oppose your government's morality."

### Oneliner

Beau challenges the moral perceptions surrounding the use of roadside devices and urges individuals to think beyond their government's stance to be truly advanced citizens.

### Audience

Citizens

### On-the-ground actions from transcript

- Oppose your government's morality to push for better standards (suggested).
- Support or oppose specific actions of your government (exemplified).

### Whats missing in summary

Exploration of the importance of questioning and understanding the morality behind government actions.

### Tags

#UkrainianResistance #Morality #Government #Citizenship #MoralPerception


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about one of my favorite concepts, something I've talked about on the
channel over the years, and it has come up again. And it's a concept that has been
examined by a whole lot of people over time and it is rearing its head. So I
recently mentioned on Twitter and in videos how the Ukrainian resistance is
expected to start using roadside stuff. And in both locations on Twitter and on
YouTube I had people ask, how can you condone that? That's only used by the bad
people, right? And it goes back to a story from St. Augustine. And I'm going to
paraphrase a little bit here, but the way he tells it, Alexander catches a pirate.
And Alexander's like, what are you doing man? You can't go around marauding the sea,
that's wrong. And the pirate's like, what are you doing? You're doing the exact
same thing with the whole earth. But because I'm the little guy, I'm called a
pirate. Because you have a whole fleet of ships, well you're an emperor. And
Alexander was like, you got me. The concept of pirates and emperors. The idea
is that somehow, some way, doing the exact same thing, just using different means, is
somehow morally different. Goes back to that idea of a social construct. You have
people outraged that Ukraine does appear to be starting to use roadside devices
to hit convoys and Russian troops going by. Somehow that gets equated with, you
know, the US's enemies. Because it's a tactic they've used a lot. I mean, and I
get it. But the reality is, I don't think anybody can tell me any moral difference
between a roadside device and artillery. Nobody was going to be able to
explain how the roadside device is immoral and Russian artillery is moral. I
mean, the only real difference is that the roadside device is less likely to
hit civilians. It's not accurate. And when you're talking about the US, explain
the difference between the thing on the roadside and that unmanned aircraft. It's
the same thing. One just has more technology, one is the purview of the
state, and one is the purview generally of a non-state actor. In this case, you
have an imperiled state beginning to use that tactic. Why do they use it? Because it works.
It works. When you are trying to judge morality and determine what to be
outraged about, a general really accurate rule to use is never base your morality
on a nation-state's version of what's okay. Because they're going to have
different rules for their behavior and for yours. In this case, it is a nation-state
using it. And I don't see the difference between something on the roadside and a
traditional ambush. Especially given from what we've seen so far, the roadside
devices employed by Ukraine are on command. They have drones watching to
make sure that they're getting the right people. The tactic is a lot like most
science. It can be applied in good ways or bad ways. And whether or not it's good
or bad isn't determined by whether or not the person using it has the
authority of a major power. That's not what decides it. You are not your
government. You can support some actions and oppose others. You can
support pieces of something and oppose others. You don't have to co-sign
everything the government does and you certainly do not have to adopt your
government's moral stance on anything. Ever. In fact, I would suggest if you want
to be an advanced citizen, you have to oppose your government's morality
because you want it to be better. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}