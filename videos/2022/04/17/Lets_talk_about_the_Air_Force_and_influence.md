---
title: Let's talk about the Air Force and influence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v8AFGdLGmA4) |
| Published | 2022/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Air Force's decision and the announcement made in a memo.
- Addresses misunderstandings about the relationship between central intelligence and the Department of Defense.
- Illustrates a hypothetical scenario involving Walmart to showcase the positive influence of supporting trans kids.
- Emphasizes the positive impact of the Air Force's actions in supporting trans individuals and families.
- Points out the importance of the Air Force's proactive stance in addressing readiness issues.
- Mentions the policy's tangible effects on the safety of trans kids at various Air Force bases.
- Advocates for working with allies, even if not perfect, to achieve progress.
- Raises the potential economic impact and political implications of the Air Force's actions.
- Suggests drawing attention to the financial consequences for those supporting discriminatory laws.
- Argues that criticizing the Air Force for other reasons shouldn't detract from acknowledging the lives saved by their policy.

### Quotes

- "If it would save the life of one trans kid, I would work with the devil himself."
- "The world isn't binary. It's not black and white. It is super gray out there."
- "Maybe you don't like the Air Force. But are your criticisms worth ignoring the fact that what they're going to do here is going to save trans kids' lives?"

### Oneliner

Beau explains the Air Force's proactive stance in supporting trans kids, focusing on positive impact and readiness issues, urging collaboration for progress despite criticisms.

### Audience

Advocates for trans rights

### On-the-ground actions from transcript

- Support organizations advocating for trans rights (implied)
- Raise awareness about the positive impact of supporting trans individuals (implied)
- Advocate for inclusive policies in your community (implied)

### Whats missing in summary

The full transcript provides a comprehensive understanding of the Air Force's decision and its impact on the lives of trans kids, urging for recognition of positive actions despite criticisms.

### Tags

#AirForce #TransRights #Support #PositiveImpact #CommunityPolicing


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today, we are going to talk a little bit more about the
Air Force thing, their decision, what they've decided
to do, that announcement that went out the memo.
And we're going to try to put it in focus, because we've had
a lot of questions about it, some genuine, some maybe
they're genuine.
I don't know.
But one that really caught my attention was one that just
says, how would you respond to this?
And it's a screenshot.
I don't know where the screenshot came from, but it certainly appears to be addressed to
me.
I've followed you for a long time, Bo.
Supporting veterans?
Sure.
Act like the U.S. military has any good influence or redeeming factors, and I can only hope
that I'm not the only one that sees through this.
At least have the CIA pay you if you're going to do their dirty work.
Well first, I think you greatly misunderstand the relationship between central intelligence
and the Department of Defense, but whatever.
Good influence.
Let's try it a different way.
Let's say Walmart gathered all of their employees who were the parents of trans kids.
And they walked in the room and they said, hey, we're going to pay for the lawyers, all
of them, all the lawyers, to help navigate these laws that are coming up.
We're going to fund it completely, not a penny out of your pocket.
We got you.
And what's better, just help the rest of the community.
Once these arguments get made, we'll go ahead and put them out in the community so people
who aren't affiliated with Walmart can benefit from them.
And if the lawyers, you know, if the lawyers can't do what they should and find a way to
navigate the system, we'll go ahead and pay to move you and your entire family to another
state where these laws don't exist so your kids can be who they are.
I think that'd be a good influence.
I think that would be a good thing.
Understand that's just what the Air Force has done so far to date right now. That's already happening
Yeah, I think that's a good influence. I think that's a good thing
Can you imagine
how quickly the discussion in this country would change?
Because it's all about money, it's about power coupons, right?
You know who has a whole lot of those?
The Air Force.
And they're willing to use them for this.
Why?
Because they perceive a readiness issue.
And right, somebody right now says, see, they're not a real ally.
That's not the way they look at it, you're right.
They see a readiness issue.
Air Force philosophy is that if you take care of the people, the people will take
care of the mission. That's how they look at it. Most branches don't
look at it that way. The Air Force does. So they are willing to try to be
proactive on this and try to kind of mitigate things beforehand so people
can accomplish the mission, and that, I'm sure, is where this criticism is originating.
You want to criticize the Air Force's role in the American Empire.
I've got videos, give you tips on how to structure your arguments.
You want to talk about their use of unmanned aircraft.
I've got all the stats you need, I will be there backing you up with those criticisms.
What can't be denied is that right now, there are kids in Abilene, in San Angelo, San Antonio,
Universal City, Del Rio, I can't actually think of all of the Air Force bases in Texas.
There's too many.
But there are kids at each one of those bases who are a whole lot safer today because of
this.
This is a demographic that has a history of people within it hurting themselves because
of how they're treated, because they can't be who they are.
They're safer today because of this policy.
This is a tangible effect that has already occurred.
I've made it really clear that I don't see purity testing as a good thing.
You roll with the allies who are ready to roll with you, and you move as far as you
can together.
If it would save the life of one trans kid, I would work with the devil himself.
And that's just up to now.
That's just what they've done so far.
They're doing it because they perceive a readiness issue.
If that readiness issue grows too large, they close a base.
Forget about closing a base.
Let's talk about something a little less severe.
just shift operations away from a base. Move it to another base in another state that doesn't
have these laws. Power coupons. If the Air Force does that at one installation, one
base, they will do more for trans rights than I would be able to accomplish in three lifetimes
if I worked on it every day because of those power coupons. Because shifting that away
another location, it doesn't remove millions from the economy, not even billions.
You're talking about trillions of dollars over the long term.
That's something that gets state politicians to pay attention.
But if it doesn't, I think it would be a really good idea to draw attention to the
fact that this is going on.
Because those people who would be most impacted, those people who would see their property
values cut in half, those people who would lose their jobs, you know where they live?
Outside those bases.
they're the people who voted for the politicians who put these stupid laws in place in the
first place.
It might be a good idea to remind them that their financial well-being is tied to their
vote.
It's a whole lot easier to feel good about yourself kicking down when you don't have
to pay anything for it.
But I'm willing to bet that if people understood it could completely destroy their economic
They might realize maybe this isn't such an affront at all, maybe that's their
business and doesn't have anything to do with me, maybe I shouldn't worry about it
because their wallet's going to tell them to. I think that it's a good idea to
point this out. Again, you want to criticize the Air Force for its role in
stuff overseas, go right ahead. That doesn't change the fact that this policy,
the stuff that's already enacted, will save lives. That's not even a
question. And to be super clear on this, although this isn't a thing, but if the
CIA decided to run some operation where it was going to, I don't know, give the Air
force good PR by saving the lives of trans kids, you would find me on a bus with blacked
out windows pulling into a farm.
The world isn't binary.
It's not black and white.
It is super gray out there.
And right now, the strongest ally, the one that can accomplish the most, the one that
has expressed interest in using power coupons that nobody else has access to is the Air
Force.
They put out the memo.
They weren't pressed into that.
They already did the math.
They realized it's going to hurt readiness.
So they put this out there.
There wasn't any protest.
There wasn't any boycott.
There wasn't anything like that.
This was a decision that was made because it's what's best for the Air Force.
Maybe you don't like the Air Force.
Maybe you have the criticisms I do, and that's fine.
But are your criticisms that realistically aren't going to change anything yet, dealing
with that stuff, is it worth ignoring the fact that what they're going to do here is
going to save trans kids' lives?
I don't think it is.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}