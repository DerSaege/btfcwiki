---
title: Let's talk about an update on Russia's economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ntlp4YQfLHk) |
| Published | 2022/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Curiosity sparked an investigation into the Russian economy's performance under sanctions.
- Despite reaching out to experts, there was no consensus on the future of the Russian economy.
- Russia is currently in a grace period and may default on debt payments by the fourth of next month.
- Predictions vary from a sudden economic nosedive to a gentler decline or sector-by-sector chaos.
- Creative accounting measures may have masked the true impact of economic hits on Russia so far.
- Some believe Russia can continue to manipulate their economic data for months to come.
- The longer Russia delays facing economic realities, the worse the decline is expected to be.
- The Russian economy is projected to contract by 10%, the largest contraction ever in the Russian Federation.
- The uncertainty surrounding the impact of sanctions on Russia's economy is unprecedented.
- Sanctions aim to hinder Russia's ability to wage war, but their immediate effects remain unclear.

### Quotes

- "The Russian economy has already taken hits, they're cooking the books and hiding it, and that eventually it's going to show itself."
- "It tells us that we've got at least a couple of weeks before we're going to see anything major."
- "It's unprecedented. It's never happened before."
- "If they won't be noticed for months, the United States, NATO, they may be forced to pursue other options."
- "These sanctions are designed to slow Russia's ability to prosecute the war, to engage in that conflict."

### Oneliner

Beau investigates the uncertain future of the Russian economy under sanctions, revealing potential defaults, creative accounting, and a looming contraction, with experts offering varied predictions.

### Audience

Economists, policymakers, activists

### On-the-ground actions from transcript

- Monitor the situation closely for any signs of economic instability (implied).
- Stay informed about developments in the Russian economy and the impact of sanctions (implied).

### Whats missing in summary

Deeper insights into the potential geopolitical implications of the Russian economy's contraction and the broader effects on global markets.

### Tags

#Russian economy #Sanctions #Economic impact #Creative accounting #Geopolitics


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about the Russian economy.
We're gonna do this because I was curious.
I wanted to know how the sanctions were performing,
so I reached out.
For those who don't know,
when we talk about economics on this channel,
generally, it means that I reached out to people
who are way smarter than I am on this topic,
and I'm telling you what they told me.
In this case, I couldn't get a consensus.
And so instead of the normal three people that I reach out to, I reach out to a fourth
and still couldn't get one.
There are very, very few things that they all agree on.
So what we're going to do is go through the various scenarios that were presented and
I'll tell you what they told me.
Three of them say it's going to happen on the fourth, that the fourth will be the dividing
line, fourth of next month.
Why?
because apparently right now Russia is kind of technically already in default.
They're in a grace period.
This month, they were supposed to make payments in dollars
to service a debt.
They couldn't do it.
They tried to do it in rubles, but that's not what the deal is.
And they have until the fourth
to make good.
Three of them agreed that the fourth
is when it will really start to show.
Now, what happens from there?
Very, very different predictions.
One says it's just gonna drop like a rock, like that day.
Like as soon as it becomes official their economy
is going to nosedive.
One says they still have a little bit of power
to cook the books, so it's gonna be more
of a gentle glide down than just a rock dropping.
And then the third said it was going to be
sector by sector, which is more chaotic,
produces more uncertainty, talked about a possible run on the banks.
That one actually sounded like really bad to me.
Everybody that I talk to believes that the Russian economy has actually already taken
hits, but they have successfully cooked to the books, used quote, creative accounting
measures to make it appear as though it's fine, but the damage is already done.
It's just a matter of when it's going to show itself.
So those are the three that say it's going to start on the fourth.
Because there wasn't a consensus, I reached out to a fourth person and they said that,
well, the fourth doesn't matter at all.
Russia is strong enough, their economy is large enough, that they will be able to continue
cooking the books for a while, months.
scenario they presented was one where the war was over, peace had been achieved, sanctions
started to be lifted, and Russia unpins, quote, their economy stops engaging in all of the
control measures, at which point all of the weaknesses show themselves and the economy
takes a dive.
to them the longer they cook the books the deeper the decline will be. So not a
whole lot of consensus here other than the Russian economy has already taken
hits, they're cooking the books and hiding it, and that eventually it's going
to show itself. The majority of people say that that's going to start on the
We'll see.
I don't know.
I can't evaluate these.
The one thing we do know for a fact is that right now the Russian economy is on track
to contract by 10%.
The GDP is looking at contracting 10%.
That will be the largest contraction in the Russian Federation ever.
There's no more to that sentence, ever.
a big deal. That's a pretty huge hit. So what does this tell us? It tells us that we've
got at least a couple of weeks before we're going to see anything major. That's the one
thing that I was able to take away from this is that the sanctions are maybe having an
impact that we're not seeing, but we won't know that until at least the fourth. Other
Other than that, the overall economy, the GDP, is shrinking.
It is contracting.
And over time, that alone is going to cause major issues, and it will probably compound
as it goes along.
But I've been doing this a while, reaching out to these same people.
I don't know that this has ever happened, where they all had very, very different opinions
on what's going to occur, but it's because it's unprecedented.
It's never happened before.
So they're all just kind of guessing at this point.
So we'll have to wait and see what plays out around the fourth.
I would hope by then at least there would be some kind of sign.
These sanctions are designed to slow Russia's ability to prosecute the war, to engage in
that conflict.
If they won't take effect, if they won't be noticed for months, the United States, NATO,
they may be forced to pursue other options.
So anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}