---
title: Let's talk about whether America needs a bad guy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rasnv3yQzdU) |
| Published | 2022/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- America historically needed an external threat to unite the people.
- The idea of having a common enemy to rally against has been a unifying factor.
- Americans are often motivated by fear, which drives unity in the face of a perceived threat.
- Shifting the focus from external enemies to common issues like poverty and climate change is vital.
- Fighting climate change and striving for a better world can be unifying causes.
- Soldiers fight not out of hatred for the enemy but out of love for what they're protecting.
- The concept of honor and battlefield glory can be redirected towards noble causes like environmental preservation.
- Urges a shift from petty nationalism to global cooperation for the betterment of humanity.
- Emphasizes the need to focus on improving living standards and making the world a better place.
- Proposes that energy should be directed towards cleaning up the planet rather than fighting over territory.

### Quotes

- "We can fight climate change. We can try to reach the stars."
- "Our energy should go to cleaning up this planet."
- "That's how we achieve world peace."
- "We stop being the bad guy."
- "Y'all have a good day."

### Oneliner

America historically relied on external threats for unity, but shifting focus to common issues like climate change can lead to world peace and a brighter future.

### Audience

Global citizens, activists

### On-the-ground actions from transcript

- Fight climate change and advocate for environmental preservation (implied)
- Focus on raising living standards and improving the world (implied)
- Encourage global cooperation and unity towards common goals (implied)

### Whats missing in summary

Beau's passionate delivery and call for a shift in societal priorities towards global unity and cooperation can be best experienced by watching the full transcript.

### Tags

#Unity #ClimateChange #GlobalCooperation #CommunityBuilding #WorldPeace


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about whether or not
America needs a bad guy, whether the United States needs
an external group to focus on in order to achieve anything.
Because I got this message.
Is having a bad guy for all Americans to rally against
kind of something that's held America together
in that it's given the parts of us
that need something to focus their anger slash fear at
instead of each other?
If so, how do we become a people that
doesn't need a common enemy?
The answer is yes.
The United States is a country that
throughout modern history has needed an external threat.
It's needed something to focus on to unite everybody else
behind.
Most countries are actually like us.
Not all, though.
The question is, how do we move beyond that?
I love that question because it's literally,
how do we achieve world peace?
And the thing is that that sounds lofty and something
impossible to think about.
But it's not.
It's actually really simple.
We have to shift our concept of what gives people honor,
gives them glory, what is worthy of being feared.
We change those things.
We, as a society, feel like everybody's out to get us.
Because we're number one.
You know, hear it chant all the time.
Then you look at the statistics and wonder
why people chant it.
But as a rule, Americans are very much motivated by fear.
And we unite.
A lot of things fade away if there is an external threat.
So how do we shift it, though?
How do we get to a point where we don't need a war,
we don't need an enemy, we don't need a group of people
that we've othered to unite, to bring people together?
We change what we're afraid of.
We're not afraid of the other.
We're not afraid of Russians.
We're afraid of poverty.
We're afraid of people having a low standard of living.
We're afraid of climate change.
And that's the beautiful part about this.
What the world needs to build a society where we're not
constantly fighting over lines on a map
is to realize that there are things that we should be doing
to realize that there are things that we should be fighting
that aren't people, that aren't other countries, that aren't
a group that we've othered.
We can fight climate change.
We can try to reach the stars.
These are the things that could unite people.
But we have that concept of honor
and the glory of the battlefield or whatever.
And people want, on some subconscious level,
to take part in that.
But it's counterintuitive.
Soldiers don't go off to fight because they hate whoever
it is they're fighting.
They go off to fight because they
love whatever's behind them, whatever they're fighting for.
Climate change is something that threatens the world.
And it could be that thing, that catalyst,
that causes us to start thinking broader than a border,
start thinking about things other than flags and lines
on a map, and start thinking about each other.
But we have to have people willing to make the case
that that's where our energy should go.
Our energy should go to cleaning up this planet.
Our energy should go to raising the standard of living.
Our energy should go to making the world better instead
of trying to figure out how to divvy it up.
The United States and most countries
thrive when they have some external enemy.
That enemy doesn't have to be another country.
That enemy can be the conditions that people have to live in.
That enemy could be something that threatens us all.
But we have to move beyond the petty squabbling,
the basic nationalism.
We have to move as a people to start
thinking about the species rather than the bottom line.
That's how we achieve world peace.
That's how we get to the point where
we don't need a bad guy.
We stop being the bad guy.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}