---
title: Let's talk about Mallory McMorrow and identifying with the bad guy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9KgVF25J8cQ) |
| Published | 2022/04/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mallory McMorrow, a Democrat in the Michigan state legislature, faced a political attack for advocating history education that includes teaching about slavery's wrongs.
- Beau questions why some parents want their children taught to identify with figures who upheld slavery rather than those who fought against it.
- He points out that history doesn't inherently make children uncomfortable; it's the parents' moral choices in upholding certain figures as heroes.
- Beau challenges the notion that teaching history equates to blaming white children for past atrocities, instead advocating for honoring those who opposed systems like slavery.
- He urges parents to guide their children towards identifying with those who stood up against injustice throughout history, regardless of skin color.
- The concern lies in children being taught to view heroes as villains and vice versa, perpetuating a harmful narrative.
- Beau stresses the importance of dismantling the remnants of systems like slavery that still benefit certain groups today.
- He concludes by encouraging listeners to prioritize identifying with the morally upright individuals in history.

### Quotes

- "History doesn't make your child feel bad. You siding with the villain does."
- "I don't know why people want their children to identify with the villain in the story when there are heroes."
- "They existed throughout American history but they're overshadowed by indoctrination that said that slavery was some necessary evil which is garbage."

### Oneliner

Beau challenges parents to rethink why they want their children to identify with figures who upheld slavery instead of those who fought against it, urging a shift towards honoring heroes who stood up against injustice throughout history.

### Audience

Parents, educators, activists

### On-the-ground actions from transcript

- Guide children to identify with historical figures who fought against injustice, regardless of skin color (exemplified)
- Advocate for inclusive and accurate history education in schools (exemplified)

### Whats missing in summary

The emotional impact of parents upholding figures who supported slavery on their children.

### Tags

#History #Education #Injustice #Heroes #Slavery


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
identifying with the right person and we'll talk about Mallory McMorrow who is
in the state legislature in Michigan. She's a Democrat and she recently came
under attack, a political attack, that said that she was basically teaching
white kids to hate themselves because she wants history taught. I don't really
understand the attack but it boils down to the idea that somehow teaching white
students about American history will make them feel bad about themselves.
Let's just start with a simple question. Do you believe slavery was wrong? If the
answer to that is yes, then you have to ask another question. Why do you want
your kids taught that the people who upheld slavery, the people who reinforced
that system, those that participated in it, those that made it the incredibly
evil version that existed in the United States, why do you want them held up as
heroes? Why do you want your kids taught that those people are heroes? Why do you
want them identifying with the bad guy when there are other options? Understand,
since before the United States existed there were white people in the United
States who were speaking out against slavery, those who said that it had to go.
Not people who said that and then still did all kinds of horrible stuff, but
people who actually believed it. Why aren't those the people that are
elevated as heroes? Why are you teaching your child, why do you want your child
taught by the state to identify with the slaver rather than the freedom fighter?
It isn't history that makes your child feel bad. It's your moral judgment. It's
you upholding heroes that they know participated in something that was
objectively wrong and ignoring and attacking those who spoke out against
that system. That's what makes them feel bad because they have to look at their
parent and wonder what's wrong with them because they don't understand that the
system that benefits white people in this country needed that generation of
Americans to sympathize with the slaver in order to uphold that system. They
don't understand that their parents were indoctrinated. What they know is that
their parent is mad that a history book says that somebody who participated in
slavery wasn't necessarily a good person when that seems to be something that
just their kids gonna know. It's not the fact that they're being told
these white people did this and you're white so it's your fault. There were
white people who fought against it but they don't get held up as heroes. They
get cast as traitors, as outsiders, as people who didn't want to play ball and
they see their parents repeat this nonsense. That's what makes them feel bad.
I don't know why people want their children to identify with the villain in
the story when there are heroes. I would suggest that at this point in time we
should probably just be trying to identify with the good people regardless
of their skin tone. I mean that would be a step in the right direction. But if you
feel you have to make it about a white kid having a white hero, point them to
Thomas Paine. Point them to anybody who actually stood up against that system.
They existed. They existed throughout American history but they're overshadowed
by indoctrination that said that slavery was some necessary evil which is garbage.
They don't feel bad because they're being taught history. They feel bad
because they're taught history. They look at their parents and their parents are
looking at the villain of that story as a hero. How would you feel about that? How
does that make you feel about those around you when they want to uphold
something that was objectively wrong? And then the real worry for the parents is
that if their kids look at those who tried to tear down that system as heroes,
that they'll want to tear down the remnants of it. And the remnants of that
system, the remnants of slavery, still benefit white people today. And that's
the real worry. History doesn't make your child feel bad. You siding with the
villain does. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}