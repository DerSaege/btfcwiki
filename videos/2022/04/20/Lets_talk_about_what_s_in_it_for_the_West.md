---
title: Let's talk about what's in it for the West....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nC0goSptDmM) |
| Published | 2022/04/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how countries like the U.S. do not take moral stances in foreign policy, but rather act based on their interests.
- Describes the initial reactions and moves made by the West when Russia invaded Ukraine.
- Points out that countries do not have moral compasses, despite individuals within governments possibly believing in moral causes.
- Details the change in Western support for Ukraine when Ukraine appeared to have the upper hand in the conflict.
- Outlines the ideal outcome for the West in the Ukraine-Russia conflict, which involves Ukraine remaining neutral but strong against Russia.

### Quotes

- "Countries don't have morals. They have interests."
- "The ideal ending to this for the West [...] for Ukraine to remain neutral."
- "Don't turn it into the country having a moral compass."

### Oneliner

Beau explains how countries act in their own interests, not morality, in response to conflicts like the Ukraine-Russia situation, aiming for Ukraine's strength and neutrality.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Support Ukraine by giving military aid and resources to strengthen them (implied).

### Whats missing in summary

Detailed analysis of potential outcomes and implications beyond the conflict's resolution.

### Tags

#ForeignPolicy #Ukraine #Russia #WesternInterests #Neutrality


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to go over something
that I feel like I truly missed, that I never went over when it comes to what's happening
in Ukraine. And somebody sent me this message saying, hey, you know, you always refer to
foreign policy as an international poker game where everybody's cheating, and you always
say that it's not about right and wrong or good or evil or morality or any of that stuff.
It's always about power. So why does it seem like the U.S. is on the moral side here? What
are they getting out of it? And I can't believe I haven't gone over this before. A country
can end up on the right side, can end up on the moral side of something, but that's not
normally why they're there. They're normally not doing it because it's moral. They're doing
it because it aligns with their interests. Countries don't have morals. They have interests.
That's it. They don't have a sense of right and wrong. Okay. So to understand what the
U.S. and we're going to say the West in general, but you know, each country is going to have
different interests, but for most of the West, this is going to be true. You have to go back
to the very beginning and what the hand looked like when everybody sat down at the table,
the hand being the Russian invasion of Ukraine. That's the hand that we're playing here. What
did everybody think was going to happen? Some, as soon as three days, most saying within
a week. They thought Russia was going to take Ukraine. Right? That was the assumption at
the beginning of the hand. So when the West started playing the game, they flashed a couple
cards to Ukraine, let them know what was going on, try to give them a little bit of information.
They threw some chips in just to drive up the cost of the win for Russia, because at
the time Russia was perceived to be a near peer that the West was going to have to contend
with for a long time. So they just kind of wanted to make sure that Russia's reemergence
onto the international scene was costly. That's it. Don't assign other motives to it. Don't
turn it into the country having a moral compass. It doesn't. Now there may be people in each
government that was involved in this who truly believed that this was a fight for democracy,
this was the moral thing to do, we had to help and all of this stuff. As individuals,
they may have believed that. But the country's foreign policy didn't. Countries don't have
moral compasses. Okay, so that's the outset. That's what happened in the beginning. Now,
as soon as that first round goes on, that first bit of fighting, Ukraine flashes their
cards to the West, and all of a sudden they're holding three aces. And it looks like they
might be able to win. Everything changed. That's when all of the aid started pouring
in. That's when you're talking about billions of dollars being devoted to this. So what
would the West get out of it now? What would the ideal be for the West? Oddly enough, pretty
close to what Putin wanted. The ideal ending to this for the West would be for the lines
to be pushed back to roughly where it started pre-invasion. That would be the ideal. And
then for Ukraine to remain neutral. They won't join NATO when they have this tension going
on with Russia. So they become a neutral country. Ukraine has the possibility of emerging from
this conflict, if it survives, as an incredibly strong power. A neutral, strong power right
up against Russia's border that is engaging it in a cold war, a warm war, along the border
over contested areas. That's the ideal for the West because Russia doesn't have the economic
power to maintain that and be a real player on the international scene. So if tensions
remain elevated but stable between Ukraine and Russia, it basically takes what is now
perceived to be a near-ish peer out of the game and the West can turn and stare directly
at China. Now, if Ukraine totally defeats Russia, pushes them all the way out, reconquers
everything and everything goes back to the way it was, it's still okay because those
tensions will still be there. There will still be groups that are loyal to Russia inside
Ukraine. There will still be those tensions and it will still keep Russia distracted.
It would be better for the West if Russia held on to a little bit of Ukrainian territory.
Again, right now we're not talking about what's right and wrong. We're talking about foreign
policy. When I'm doing these videos, don't ever confuse what would be best for the West
with what I want to happen. That would be the ideal because all the West has to do is
allow investment in Ukraine, get those resources out, get the money flowing, get the rebuilding
going. It becomes a very strong country. The West supports Ukraine by giving it their old
stuff militarily, which is better than the Russian stuff, and that little Cold War gets
sponsored for next to nothing. And they end up taking Russia out of the big international
poker table and keep them distracted. They keep them out of the big game. Russia can't
sit at two tables at once. They don't have the money for it. So that's what the West
gets out of it. You know, if Ukraine emerges totally victorious, yeah, it's a great thing
for Western PR. But for Western foreign policy, ideally it comes to a stalemate with Russia
still holding little chunks. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}