---
title: Let's talk about a new law Tennessee about homelessness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Vv9GURQTNoM) |
| Published | 2022/04/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Legislation in Tennessee criminalizes homelessness.
- Makes camping on local public property a felony, punishable by up to six years in prison.
- Demonstrations against racial injustice led to the legislation.
- Asking for change or camping alongside highways also criminalized.
- The legislation fails to address the root causes of homelessness.
- Supporters and opponents of the legislation lack comprehensive answers to homelessness.
- The real problem is homelessness itself.
- Providing homes and addressing underlying issues like rehab and mental health care is the solution.
- Incarcerating homeless individuals for up to six years is not a solution.
- Housing homeless individuals in a humane way is more cost-effective and productive than incarceration.
- Politicians target homeless individuals with legislation to appease their base.
- Homeless individuals are not the source of societal problems; the government is.
- The focus should be on holding politicians accountable rather than targeting the homeless.
- Housing homeless individuals in a helpful way benefits everyone and is cost-effective.
- It's cheaper and better to address homelessness compassionately.

### Quotes

- "Homelessness. I mean, not to put too fine a point on it, but that's what it does."
- "The problem is in the name. Homelessness."
- "It's cheaper to be a good person."
- "Stop looking under the bridge and start looking up at that Capitol."
- "Those people with less institutional power than you have, less money, less resources, less everything than you have are never the source of your problem."

### Oneliner

Legislation in Tennessee criminalizes homelessness, targeting vulnerable individuals instead of addressing root issues, advocating for a compassionate approach to provide housing and support.

### Audience

Tennessee Residents

### On-the-ground actions from transcript

- Contact local representatives to oppose the legislation (suggested).
- Support organizations providing housing and support for the homeless (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of how legislation in Tennessee criminalizes homelessness and the importance of addressing root issues for a compassionate and cost-effective solution.

### Tags

#Tennessee #Homelessness #Legislation #RootIssues #Compassion


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about some legislation
in Tennessee that appears to be headed to the governor's desk
for a signature.
We're going to talk about that legislation.
We're going to talk about what's in it.
We're going to talk about how it's
going to impact the average person in Tennessee.
We're going to answer a question that apparently
the people behind this legislation
believe is unanswerable.
And we're just going to kind of go from there.
So what's the legislation?
Criminalizes homelessness.
I mean, not to put too fine a point on it,
but that's what it does.
That's not how it attempts to achieve it,
but that's the effect.
It goes about making camping on local public property
a felony, punishable by up to six years in prison.
It's worth noting that state land is already
treated in this fashion.
Up until 2020, it was just a misdemeanor.
But Tennessee had demonstrations against racial injustice.
And rather than address the problem,
they just decided to criminalize, make it a felony,
so those people wouldn't be able to vote anymore.
On top of this, it makes it a misdemeanor
to ask for change or to camp alongside highways or under the bridge.
You know, I was just thinking to myself that the way you get people out of the situation
where they're having to ask for change is to give them an economic penalty, a fine that
you know they can't pay, that more than likely will put them in a cycle that will lead to
them being incarcerated.
of the supporters behind the legislation says, I don't have the answer for homelessness.
Those that opposees this legislation, they don't have all the answers for homelessness.
Those that support this legislation, they don't have all the answers for homelessness.
Right.
Nobody knows.
I got it.
Let's just take it step by step.
The problem is in the name.
Homelessness.
don't have a home. They don't have a place to reside. They don't have a way to stay somewhere
out of the weather and kind of get back on their feet. That's the problem. The solution
would be to provide that. Call me, you know. And then if you wanted to actually address
it at a deeper level, while you're providing them a place to stay, you could address the
underlying issues, you know, rehab, mental health care, job assistance, so on and so
forth.
And you could actually move to eliminate the problem.
Or I guess you could just arrest them and throw them in jail for up to six years.
I mean, that's also a choice.
The interesting thing about this is that the solution is to house them.
That's the solution.
this legislation, I mean it kind of gets you there. The question is whether or not Tennesseans
want to pay to house them in a humane way that actually solves the problem or wants to pay for
them to get locked up. I would point out that locking them up is more expensive and less
productive. So I mean I would go with you know a more social approach to this
problem. But the thing is this isn't about solving the problem is it? It's
really about these politicians finding a group of people that they can target
with legislation to make their base feel better about the situation that they
find themselves in so they can look down and kick down on those people. Those
Those people are the enemy, those people who have nothing.
If you are in Tennessee and you are a voter, allow me to just help you with this.
Those people with less institutional power than you have, less money, less resources,
less everything than you have are never the source of your problem.
You're looking the wrong direction.
Stop looking under the bridge and start looking up at that Capitol.
That's where your issue lies.
This is going to end up housing them anyway, but it's going to end up housing them in a
way that doesn't help them, doesn't help you, doesn't help the state, and costs more.
As is typically the case, it's cheaper to be a good person.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}