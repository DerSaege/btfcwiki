---
title: Let's talk about the US already learning from Ukraine and what we should...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zM2wGGttdvE) |
| Published | 2022/04/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ukraine events have influenced U.S. training at the National Training Center.
- U.S. forces are currently at war with a fake country, Danovia, mimicking Russia.
- Russian combat tactics, including shelling civilian areas, are integrated into U.S. training.
- Danovians in the scenario will have cell phones and focus on information operations.
- Individuals can shape narratives through social media, impacting the outcome of events.
- Social media empowers non-state actors to generate propaganda and shape narratives.
- The importance of social media in influencing thought and creating change is recognized by the U.S. military.
- Activism and sharing information online can have a significant impact on society.
- Social media is a powerful tool that should be acknowledged in strategies for creating a better world.
- The U.S. military is strategizing on countering social media influence in conflicts.

### Quotes

- "Information is power."
- "Your cell phone, your ability to document what's happening and get that record created and put it out on social media, That is, that's so important."
- "Social media has handed individual people, non-state actors, the same power to generate propaganda, negative or positive."
- "Your activism, your desire for that better world. Your strategies for achieving it should probably acknowledge this."
- "On a long enough timeline, we win."

### Oneliner

Ukraine events shape U.S. training at National Training Center, integrating lessons on combat tactics and information operations, showcasing the power of social media in shaping narratives and fostering change.

### Audience

Activists, Social Media Users

### On-the-ground actions from transcript

- Document and share information on social media platforms to shape narratives and create awareness (exemplified).
- Utilize social media for activism and advocating for positive change in society (exemplified).

### Whats missing in summary

The full transcript delves into the significance of social media in shaping narratives, the power of individual activism, and the evolving nature of information warfare in modern conflicts.

### Tags

#Ukraine #USMilitary #SocialMedia #Activism #InformationWarfare


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about Ukraine, lessons learned,
and the power of information.
Because something interesting is happening right now.
The events in Ukraine have already provided lessons
learned to the United States, to the point
where it has shifted the way the US is training its people.
And that is very much on display right now
out at the National Training Center.
Now, if you don't know, there's a place called the National
Training Center where the United States is constantly
at war with fake countries, made up countries.
And while the country names are always fake,
right now that the U.S. is at war with Danovia, it's pretty easy to figure out who the fake
country is supposed to be mimicking.
Danovia happens to use Russian equipment, Russian doctrine, Russian tactics, and the
people playing the role of the Danovians will speak Russian.
So there's a fake town, sometimes a couple, and there are people there pretending to be
civilians, pretending to be Danovian troops, whatever, right?
And the U.S. forces are given an objective and they have to complete it in this environment.
Okay, so there's already a bunch of lessons learned that have made it into this scenario,
this mythical war with Danovia.
There were two that I found really interesting.
The first is just to highlight something because I think most people, especially people watching
this channel, know that this is happening.
I'm not sure that people understand the frequency with which it is occurring.
Russian forces are now being trained to fight an opposition force, Danovia, who has no problem
shelling urban civilian areas.
So U.S. urban combat training now includes fighting an opposition force that has no problem
with artillery and missile strikes on civilian areas.
Russia has been engaging in this behavior to such a degree that it has literally made
it into U.S. training.
I think that's worth noting.
Now the real thing that I found just incredibly fascinating is that the Danovians are all
going to have cell phones.
The DeNovian forces, the DeNovian civilians, they will all have cell phones and they will
be trying to manufacture a narrative.
Information operations are now part of this.
The civilians or the DeNovian forces will be filming American troops, trying to get
clips that they can take out of context and put onto a social media platform like Twitter
and frame a narrative around it.
Perhaps U.S. forces engage, De Novihan forces, and somebody loyal to the De Novihan side
manages to get a clip of the forces laying on the ground after all the weapons have been
policed up. Or a tank breaks down. Look how incompetent the Americans are. And all of
this will be uploaded to this fake social media and this is all going to be kind of
war-gamed. It's interesting that the US now feels that this is such an important
part of conflict, that it's becoming part of the scenarios to this degree.
It's also worth noting that most commanders I know are going to have the reaction of the
person who was quoted in the write-up, which was basically, you know, if I'm taking fire
on my left flank, I don't care what somebody's going to say about me on Twitter, which is
probably going to be the most common reaction, and it's also going to be the most wrong.
Information is power, and the information battle space is going to be incredibly important
in all future conflicts.
This is something that the U.S. is now looking at as something it needs to worry about when
war gaming events against near-ish peers.
So what does that tell us?
Information is power.
Your cell phone, when you're talking about social change, your cell phone, your ability
to document what's happening and get that record created and put it out on social media,
That is, that's so important, it changes the outcome of events so much that it's now become
part of training for combined arms commanders in the U.S. military.
If you ever feel like your activity online doesn't do anything, it doesn't mean anything,
It doesn't accomplish anything.
This should show you you're wrong.
Individual people creating these clips is part of the scenario.
The reality is that social media has handed individual people, non-state actors, the same
power to generate propaganda, negative or positive, that until very recently was
really kind of the realm of the state actor. It wasn't something that the
average person could do. They couldn't help shape a narrative. They couldn't
provide information that wasn't being filtered. Your activism, your desire for
that better world.
Your strategies for achieving it should probably acknowledge this.
I know there's a lot of people who get very kind of burned out because you don't feel
like you're accomplishing anything.
To change society you have to change thought and it's important enough, social media is
important enough a tool in that regard that the U.S. military is now wargaming how to
counter it.
Don't tell me it won't work with your local government.
It will.
On a long enough timeline, we win.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}