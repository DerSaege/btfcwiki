---
title: Let's talk about Texas, Abbott, and produce....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x7zrUiq1BL4) |
| Published | 2022/04/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Some Republicans are seeing through the political stunts of Governor Abbott in Texas.
- Operation Lone Star, the governor's attempt at border security, led to a quarter of a billion dollars in losses of fruits and vegetables.
- The costly operation, now shut down, caused produce to rot in trucks due to logistics failures.
- Republican Agriculture Commissioner Sid Miller criticized the policy, foreseeing rising food prices and shortages.
- Governor Abbott's policies are not stopping illegal immigration but hindering food supply chains.
- Abbott's administration faces criticism for various failures, including the power grid and costly security operations.
- Republicans and Democrats in Texas are starting to speak unfavorably about Governor Abbott's leadership.
- Some Republicans are recognizing the ineffectiveness of political stunts as leadership.
- Political stunts by leaders often lack measurable success metrics and end up harming the people they are supposed to help.
- Texans are urged to demand better leadership from Governor Abbott despite his name recognition and party affiliation.

### Quotes

- "Your inspection protocol is not stopping illegal immigration. It is stopping food from getting to grocery store shelves and in many cases, causing food to rot in trucks."
- "Those people in Texas, you've got your work cut out for you because Governor Abbott has name recognition going for him and he has that R after his name."
- "I think Texans deserve better."

### Oneliner

Some Republicans in Texas are seeing through Governor Abbott's costly political stunts that hinder food supply chains and fail to address real issues, urging Texans to demand better leadership.

### Audience

Texans

### On-the-ground actions from transcript

- Demand better leadership from Governor Abbott (implied)
- Stay informed and vocal about political decisions affecting food supply and border security in Texas (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Governor Abbott's controversial policies and the growing dissatisfaction among Republicans and Democrats in Texas. Watching the full video will offer a comprehensive understanding of the issues discussed.

### Tags

#Texas #GovernorAbbott #RepublicanParty #PoliticalStunts #Leadership


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Texas and Governor Abbott
and how some Republicans are starting to see
through the political stunts that are being performed
in an attempt to motivate the base
and how Republicans are starting to see Abbott for who he is.
If you were one of those who believed there was no way that any Texas program
could be worse than Operation Lone Star, you were proven wrong last week.
Operation Lone Star is the governor's attempt at a mobilization within Texas
to secure the border, which had its press release of successes ripped apart by
ProPublica and Texas Tribune, if you're not following those outlets, they're worth following.
In an attempt to, I guess, regain the momentum, he instituted what amounted to a second customs
checkpoint ran by the state in Texas, checking semi-trucks under the guise of border security,
just like Operation Lone Star.
The end result of which, it has now been shut down, but during that time, lost about a quarter
of a billion dollars in fruits and vegetables alone.
That's the cost.
Not the cost of actually running the operation, that's the cost that the average person in
Texas is going to have to deal with because he wants to motivate the base.
It led to fruit and veggies just rotting in the trucks because they didn't have the logistics
worked out.
This number of $240 million, that doesn't include the downstream losses from parts that
were coming over the border that didn't make it to where they were supposed to go in time,
and the loss of money there.
That is just fruits and vegetables.
It was such an unmitigated disaster that even Republicans began to speak out pretty vocally
about it.
I don't think anybody said it better than the Republican Agriculture Commissioner there
in Texas, Sid Miller, who said this policy will hurt Texas and American consumers by
driving up already skyrocketing food prices, worsening ongoing supply chain disruptions,
causing massive produce shortages, and saddling Texas and American companies with untold losses.
We're starting to get the tell on this part and how much it actually costs, and it's a
lot.
If your policy continues, within the next week, I predict that Texas consumers will
be paying $2 for a lime and $5 for an avocado.
The following week, Texas produce shelves will be empty for the third time because of
policies implemented by your administration.
Your inspection protocol is not stopping illegal immigration.
It is stopping food from getting to grocery store shelves and in many cases, causing food
to rot in trucks, many of which are owned by Texas and other American companies.
It is simply political theater.
The people of Texas deserve better.
That's a Republican.
You don't even want to hear what the Democrats are saying.
What seems to have happened is that Governor Abbott realized that former President Trump
was able to tap into the bigotry that gets directed at those coming across the border.
And he wants to duplicate it because his administration has not been successful by most counts between
the power grid and Operation Lone Star and now this and a litany of other stuff.
The people in Texas, even Republicans, are starting to speak less than favorably of the
governor.
It's also important to note that it seems like at least some Republicans are starting
to see these little PR stunts, these little political stunts for what they are, which
Which is good news, because for years now, the Republican Party has confused these kinds
of political stunts with actual leadership.
I'm hopeful that they're starting to realize these stunts don't do anything except for
hurt the people in that area.
It is incredibly rare that any of these little political stunts have any metrics to gauge
their success. They just order something to be done and then walk out and say
they accomplished something. But any time there's any examination of what
actually occurred and what they claimed, it casts a lot of doubt on the
efficiency of these little projects. Those people in Texas, you've got a, you've
got your work cut out for you because Governor Abbott has name recognition
going for him and he has that R after his name, but I happen to agree with Sid
Miller on this, not something I ever expected to say. I think Texans deserve
better. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}