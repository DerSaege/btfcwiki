---
title: Let's talk about Finland, Ukraine, and US diplomacy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qzfE0zdbL8A) |
| Published | 2022/04/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Russian offensive in eastern Ukraine is stalling, with minimal gains and attacks continuing without significant progress.
- There is debate over whether the current offensive is just a diversion or the main attack, suggesting disorganization in Russian military strategy.
- The US recently had high-level diplomatic visits to Ukraine, hinting at a limited embassy reopening and increased military aid in the form of financing for ammunition purchases.
- Finland and Sweden are showing interest in joining NATO, with concerns over security guarantees amidst Russian threats of deploying nukes.
- Suggestions have been made to provide security guarantees to Finland and Sweden upon applying to NATO to deter potential Russian aggression.
- Talks between Russia and Ukraine are at a standstill, waiting for the offensive's resolution before significant progress can be made.

### Quotes

- "The attacks are continuing. They're just not moving anywhere."
- "One of the things that is being brought up that I find interesting is Russia is already kind of grumbling and making threats."
- "They're not going to have to bring in a lot of equipment and stuff like that, but they're still on common ground."
- "An assurance is, we'll help you out if something happens. A guarantee is, you go to war, we go to war."
- "I think it's more for peace of mind than actual deterrence."

### Oneliner

The Russian offensive stalls in Ukraine, US provides aid, Finland and Sweden eye NATO amidst Russian threats, and talks await resolution.

### Audience

Diplomacy watchers

### On-the-ground actions from transcript

- Monitor diplomatic developments and advocate for peaceful resolutions (suggested)
- Stay informed about international relations and potential security threats (suggested)

### Whats missing in summary

Insights on the potential impacts of Finland and Sweden joining NATO

### Tags

#Russia #Ukraine #NATO #USdiplomacy #SecurityGuarantees


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we will provide a little update on Europe
and talk about what's going on.
We'll talk about Finland and Sweden
and their moves towards NATO and what some people are suggesting
might be necessary to facilitate that.
We'll talk about US diplomacy and what's been going on there.
We will talk about the offensive in the east
and how that's going, and that's where we're going to start.
The Russian offensive in eastern Ukraine is not moving,
not really, making very small gains that aren't really
that significant, so much so that the US,
they've said that the offensive is failing, their term.
I don't know that that's the right term to use.
Failing means like it's going to end soon.
I don't see that.
The attacks are continuing.
They're just not moving anywhere.
So I don't know that throwing the term failing out there
is a good idea.
It might lull people into the belief
that this is going to be over soon, and I don't see that.
I do not see this offensive ending quickly,
and that's kind of the tone that's being thrown out there.
The attacks are continuing.
They're just not really gaining ground.
It's not going well for Russia, so much so
that there is now debate over whether or not
what we've seen thus far is just a softening effort, a diversion,
or whether or not the diversion everybody's ignoring
is actually the main attack.
Throughout this conflict, I have not really
sang the praises of Russian military prowess,
but I would be really surprised if this is the best they've got.
If this is all they have left and they're
throwing their full weight behind this now,
I would be really surprised.
To me, it seems like it is disorganized to the point
that they're not capable of executing anything,
and that's something that could change.
And if it does, everything else changes with it.
I would be real cautious about believing
this offensive has failed already or is failing.
Now, as far as US diplomacy, there
was a high-level visit with Austin and Blinken,
so the Secretary of State and the Secretary of Defense
went to Ukraine.
They made it seem as though there
might be kind of a limited embassy reopening there soon,
and they committed to more military aid,
little more than $300 million.
But this is different.
This is financing, not direct equipment.
This is kind of co-signing a loan so Ukraine can go out
and get what it needs.
Looks like about half of that money
is going to go to purchase ammo.
One of the problems that Ukraine's running into
is that most of their small arms, their rifles and stuff
like that, it's Warsaw Pact stuff,
which means it's not the same caliber as NATO's rifles,
which means NATO doesn't make the ammo for it.
So it's hard for NATO to produce a lot of the ammo they need
because they don't make it.
So it looks like half of that $300-something million
is going to go to other countries
so the ammo can be purchased, so they
can get the ammo for their AKs and stuff like that.
And then we come to Finland and Sweden,
who are still making overtures towards joining NATO.
Joining NATO is typically a lengthy process.
Now a lot of that can be kind of skipped over
and it can be expedited because this
is two countries that have often trained with NATO,
so there's already common ground.
They're not going to have to bring in a lot of equipment
and stuff like that, but they're still
on common ground.
They're not going to have to bring them up to standards
or anything like that.
They're fine.
But one of the things that is being brought up
that I find interesting is Russia is already
kind of grumbling and making threats and stuff like that,
saying, oh, we're going to put nukes into the region.
They already have nukes in the region.
That doesn't even matter.
The threats are leading people to suggest
that as soon as Finland and Sweden apply,
that they are granted security guarantees.
And this is something we've talked about in other videos,
the difference between assurances,
which is what Ukraine had, and a guarantee,
which is what Ukraine wanted when the Budapest
memos were signed.
An assurance is, we'll help you out if something happens.
How much?
Well, we'll figure that out when it occurs.
A guarantee is, you go to war, we go to war.
There are people who are suggesting
that they go ahead and extend that as soon as they apply
to dissuade Russia from attacking.
One of the ways that they've suggested doing this
is taking the EU treaty, which has assurances,
and basically saying that from now on, those are guarantees,
and kind of forcing a mutual defense pact where one really
doesn't exist, but they're just going
to say it does for the time being until their NATO
membership is approved.
And the whole goal of that is to just kind of cool Russia out
on the threats.
Realistically, I would be really surprised
if Russia wanted to open up another conflict zone
right now.
They're not doing well with the one they have.
But I think it's more for peace of mind than actual deterrence.
But we'll see.
So this is where we're at.
Everything else has pretty much been going
the way you might expect.
There's more partisan activity in areas that Russia controls.
Other than that, everything's relatively static.
The talks between Russia and Ukraine, at this point,
they're not going to go anywhere, more than likely,
until this offensive is finished one way or another.
Because right now, both sides want
to kind of gain some leverage and some advantage
from however that plays out.
So it's unlikely that either side
is going to push for any real closure to those talks
until the offensive is resolved.
So that catches you up for the weekend.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}