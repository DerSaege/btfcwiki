---
title: Let's talk about Elon Musk buying Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EBeEjohoLiU) |
| Published | 2022/04/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk is reported to have bought Twitter, sparking varied responses from people.
- Some are excited at the prospect of an unmoderated Twitter, while others are concerned about the same.
- Musk's image as a free speech absolutist may not lead to Twitter becoming completely unmoderated.
- Even right-wing social media networks have moderation due to the necessity of safety policies.
- Removing Twitter's safety policies could make Musk and the company liable for any harm caused.
- There might be a PR shift in perception regarding moderation but not a significant actual change.
- Without moderation, Twitter could be inundated with botnets and toxic behavior, driving away core users.
- Musk's ideas for Twitter may sound good in theory but face challenges in implementation.
- Musk might face opposition from his lawyers if he tries to implement extreme moderation changes.
- The far-right social media networks that claim to be unmoderated are also moderated differently.

### Quotes

- "Musk's ideas for Twitter, they sound good in theory until somebody tries to implement them."
- "You're not going to have a social media network without moderation because it becomes a sewer very very quickly."
- "I wouldn't worry too much about it right now. I'd wait and see what shapes up."
- "And when you see the PR campaign, the ad campaign, ask yourself if you're really experiencing any difference on the site or if you're just being told things are done differently."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Elon Musk's purchase of Twitter may not lead to significant changes in moderation, amid concerns and expectations of unmoderated content.

### Audience

Social media users

### On-the-ground actions from transcript

- Monitor Twitter for any significant changes in moderation policies (suggested)
- Participate in the platform with awareness of potential shifts in perception regarding moderation (suggested)

### Whats missing in summary

Insights on the potential impacts of Elon Musk's ownership of Twitter on online discourse and user experience.

### Tags

#ElonMusk #Twitter #SocialMedia #Moderation #OnlineDiscourse


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Elon Musk buying
Twitter and what that means, because it is being reported
that that's a done deal.
Musk has control.
This is creating a wide array of responses from people.
Some are super excited because they believe Twitter is about
to become just wholly unmoderated.
And some are very concerned because they believe Twitter is
about to become wholly unmoderated.
So we're going to talk about that.
We're going to talk about what's likely.
But before we get into that, I have a quick announcement I
want to make, and I want to put it up front because I want to
make sure everybody hears this.
I don't give investment advice, ever, under any circumstance.
Not a thing I do.
There is a scam going around right now where it takes the
photo of somebody on YouTube and their name and uses that to direct people to WhatsApp
where it encourages them to buy crypto of some kind or promotes something to do with
cryptocurrency.
That's not me.
This is also hitting other people on YouTube.
It's not them either.
Just a quick tip.
People on YouTube want all of the interaction in their comments.
want it all down below because it promotes their channel, it boosts that
video. They're not going to direct people off the site. If they do, they'll direct
them to a pay site where they're gonna make money. I would obviously be very
suspicious of any service company or person who used this type of advertising,
this type of scam, to get people involved. If you happen to be approached, I always
would love to know what company, service, or person is being promoted via this.
Okay, so that's not me.
It's probably not anybody else that you see this happening with that's on YouTube.
All right, back to Twitter.
The idea that is present is that because Elon Musk has fostered the image of being a free
speech absolutist, that Twitter is about to become just completely
unmoderated. That is super unlikely. That's probably not going to happen.
There will probably be a PR shift and shift in perception, but it's unlikely that there's
going to be a lot of changes. And here's the reason why. Even all of those free speech,
far right-wing social media networks that have popped up, even they have
moderation. You can't run a social media network without it. It has to exist. Now
Twitter is an especially unique case because they already have policies in
place for safety, for the safety of their users. If Musk was to come in and get
rid of those policies, and then something was to happen, say somebody got hurt or
an insurrection got incited, whatever, he might be liable for that. Even if Musk is
as ideologically pure on this subject as he presents himself to be, his lawyers
will probably stop him because it would open him up to a lot of issues. It opened
the company up to a lot of issues.
With those policies currently being in place, it's Twitter saying, this is bad.
This is going to get somebody hurt.
This is blah, blah, blah.
And they have a foundation for that.
If those policies are removed and then something happens, it's going to be on the company.
That's how that's going to be framed, whether it is or not.
So what I would imagine is a lot of PR that is designed to shift perception of what gets
moderated.
Not a huge shift in what actually gets moderated.
You'll probably see some small stuff related to the culture wars, but that's about it.
Aside from that, when you're talking about a social media network that somebody has invested,
I want to say $43 billion and you want it to be healthy.
You want that network to continue, right?
If moderation went away, it would become even more swamped with botnets, with just overtly
toxic behavior, and Twitter's core users would disappear.
They would leave.
site. This would render that 43 billion dollar investment devalued. So I'm not
seeing a huge shift. There will probably be a huge PR shift designed to make
people think there was a big shift, but in practice it's probably not going to
change much. A lot of Musk's ideas for Twitter, they sound good in theory until
somebody tries to implement them. So there may be a disconnect
between somebody's ideological beliefs and reality. It's that intersection of
ideology and reality, there's a lot of car accidents there. And Musk may be using
the self-driving mode on one of his cars heading into that intersection right
now. Those sites that have popped up that are far right, that claim to be
unmoderated, they're moderated too. They just have a different point where the
moderation begins. You're not going to have a social media network without
moderation because it becomes a sewer very very quickly and and I don't think
that Musk intends on losing billions of dollars which is what would probably
happen and that's assuming he could implement those policies over the
objections of his own lawyers, which I'm sure would object. So I wouldn't worry
too much about it right now. I'd wait and see what shapes up. And when you see the
PR campaign, the ad campaign, ask yourself if you're really experiencing
any difference on the site or if you're just being told things are done
differently. Because outside of culture war stuff, I don't see a huge shift
coming. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}