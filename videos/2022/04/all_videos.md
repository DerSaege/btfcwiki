# All videos from April, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-04-30: Let's talk about public health updates.... (<a href="https://youtube.com/watch?v=FSMoQWuNCrY">watch</a> || <a href="/videos/2022/04/30/Lets_talk_about_public_health_updates">transcript &amp; editable summary</a>)

Dr. Fauci's interview quotes were twisted in headlines, falsely suggesting the pandemic's end, stressing the importance of context and accurate reporting.

</summary>

"We are out of the full-blown explosive pandemic phase."
"By no means is the pandemic over."
"Always read beyond that headline and get the full context to it."
"This type of misunderstanding happens a lot."
"They're just printing what somebody else said."

### AI summary (High error rate! Edit errors on video page)

Dr. Fauci's interview quotes were manipulated in headlines, leading to inaccurate interpretations.
The phrase "we are out of the pandemic phase" was twisted into "the pandemic is over" in headlines.
Dr. Fauci clarified that we are in a controlled phase, not out of the pandemic.
He emphasized the need for more vaccinations, better vaccines, early treatment access, and booster strategy for variants.
Despite positive trends like decreased cases and higher vaccination rates, the pandemic is not over.
Vaccine disparity between rich and developing nations raises concerns about variant emergence.
Context is vital when reading headlines with quotes to avoid misinterpretation.
Headlines often focus on attention-grabbing quotes without providing full context.
Misunderstandings due to misleading headlines are common in journalism.
Outlets use quotes as headlines to attract attention without verifying accuracy.

Actions:

for media consumers,
Read beyond headlines for full context (suggested)
Be cautious of misleading quotes in headlines (implied)
</details>
<details>
<summary>
2022-04-30: Let's talk about how Russia could do things differently.... (<a href="https://youtube.com/watch?v=LwSoqU9MOzs">watch</a> || <a href="/videos/2022/04/30/Lets_talk_about_how_Russia_could_do_things_differently">transcript &amp; editable summary</a>)

Beau provides strategic insights on potentially liberating Ukraine as a Russian, focusing on mobility, surprise, and concentration of forces for a swift operation.

</summary>

"Crowdsourcing battle plans now?"
"Non-state actors are typically more mobile than state actors."
"Nobody's going to expect the Russian military to do anything at night."
"Soon as you cross the border, poof, Ukraine's liberated."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addressing Russia's approach in Ukraine, proposing alternative strategies.
Responding to a challenge about offering solutions instead of just pointing out mistakes.
Providing a rough sketch of Ukraine's map and identifying key areas like Crimea and the occupied territory.
Suggesting to mobilize non-state actors in Crimea for increased mobility.
Pointing out Russia's lack of mobility and proposing to provide SUVs and trucks instead of tanks.
Noting Russian high command's commitment to launching the main operation from Izium.
Recommending thinning out troops and subtly moving forces to Izium for a surprise offensive.
Describing a tactical movement of forces at sunset to reach Izium by daybreak and liberate Ukraine.
Emphasizing the element of surprise in the operation.
Concluding with a thought-provoking idea for liberating Ukraine.

Actions:

for military strategists,
Mobilize non-state actors in Crimea for increased mobility (implied)
Provide SUVs and trucks to enhance mobility of forces (implied)
Thin out troops and subtly move them to Izium for a surprise offensive (implied)
</details>
<details>
<summary>
2022-04-29: Let's talk about new defense spending on Ukraine.... (<a href="https://youtube.com/watch?v=7jR0OffmpV4">watch</a> || <a href="/videos/2022/04/29/Lets_talk_about_new_defense_spending_on_Ukraine">transcript &amp; editable summary</a>)

Beau addresses the increase in military aid to Ukraine and speculates on the defense industry's significant financial gains, implying potential policy influence.

</summary>

"It is blood that makes the cash grow green."
"The defense industry is now going to make a dump truck full of cash."
"Perhaps we build a factory in your hometown, something along those lines."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses aid going to Ukraine and a talking point about the defense industry making money from it.
Demonstrates through numbers that the aid to Ukraine isn't significant for the defense industry's bottom line.
Mentions that the defense industry will start making real money as the new aid package for Ukraine is $20 billion.
Explains that although some of the aid is old stuff, the defense industry will benefit financially from providing it.
Notes that the defense industry will profit significantly from the aid, indicating a shift in policy influence.
Speculates on the defense industry influencing policy decisions through potential incentives like building factories in senators' hometowns.
Suggests that the defense industry is now making substantial profits due to the aid package increase.
Implies that the defense industry's influence may have played a role in the significant increase in military aid to Ukraine.
Concludes that while Ukraine needs the equipment, the US defense industry is taking the lead in providing it.
Leaves viewers with a thought-provoking perspective on the situation.

Actions:

for advocates for accountability,
Question policy decisions influenced by industries (implied)
Stay informed about aid distribution and potential motives behind it (implied)
</details>
<details>
<summary>
2022-04-29: Let's talk about Madison Cawthorn, a rumor, and North Carolina.... (<a href="https://youtube.com/watch?v=3eTd9v3BtUE">watch</a> || <a href="/videos/2022/04/29/Lets_talk_about_Madison_Cawthorn_a_rumor_and_North_Carolina">transcript &amp; editable summary</a>)

Beau dives into rumors surrounding Madison Cawthorn, shedding light on potential conflicts within the Republican party at both state and national levels.

</summary>

"It's just like this never-ending parade hitting Cawthorn and exposing things about him."
"The reason I find this super interesting is because this is yet another example on top of Georgia and Tennessee..."
"For once, it's not the state GOP upset with Trumpism."

### AI summary (High error rate! Edit errors on video page)

Madison Cawthorn is the subject of various stories, rumors, and information coming out almost daily.
Allegations regarding Cawthorn include issues with his driver's license, photos, investigations, and an incident with a gun at an airport.
The Republican establishment in DC is believed to be behind the negative publicity against Cawthorn due to him discussing non-existent Studio 54 parties.
However, a source close to North Carolina politics suggests that it's actually Republicans in the state who are displeased with Cawthorn.
Cawthorn is seen as confrontational and a liability by North Carolina Republicans, potentially jeopardizing a safe seat for them.
There are rumors that North Carolina Republicans are planning to push Cawthorn out in favor of another candidate named Edwards.
The situation with Cawthorn is seen as another example of state-level GOP pushing out a Trump supporter, despite Trumpism not being the main issue in this scenario.
Cawthorn's inability to fall in line with the state GOP's expectations seems to be a significant factor behind the potential push against him.

Actions:

for political enthusiasts,
Reach out to local North Carolina Republican groups to understand their stance on Madison Cawthorn and potential actions (implied).
</details>
<details>
<summary>
2022-04-29: Let's talk about Gerasimov, the most sought after man in the world.... (<a href="https://youtube.com/watch?v=7TJqxGYJsoQ">watch</a> || <a href="/videos/2022/04/29/Lets_talk_about_Gerasimov_the_most_sought_after_man_in_the_world">transcript &amp; editable summary</a>)

Russian General Grissomov's deployment to take personal command signals desperation in Ukraine conflict, posing significant challenges for both sides.

</summary>

"It's brilliant. However, it had a critical flaw."
"This is a pretty surprising development. This isn't something that really happens in real life."
"All Ukraine has to do is keep fighting. They don't have to win the battles."

### AI summary (High error rate! Edit errors on video page)

Russian General Grissomov is reportedly heading to Izyum to take personal command of an advance.
The Gerasimov doctrine, a Russian military strategy, is deemed brilliant but written for a military that no longer exists.
Vorosimov's personal command of a battle indicates the importance of the line of advance to Russia, a struggling offensive, and a broken chain of command.
Ukraine must counter Vorosimov with everything they have to avoid a demoralizing outcome.
NATO will likely encourage Ukraine to force Russian generals, like Vorosimov, to remove themselves from the battlefield rapidly.
Vorosimov's firsthand knowledge could be critical for NATO's long-term perspective, making it imperative to prevent him from leaving the battlefield.
This move of sending a high-ranking general to the front is unusual and signals desperation for Russia's progress in the conflict.
Vorosimov's defeat or failure to return home could have significant strategic, military, and propaganda implications for Russia.
Immense resources are likely being devoted to confirming Vorosimov's deployment, locating him, and ensuring he doesn't return home.
Vorosimov's deployment signifies the effectiveness of aid coming into Ukraine and the attempt to break the stalemate in the conflict.

Actions:

for military analysts, policymakers,
Verify the accuracy of reporting on Vorosimov's deployment and strategize accordingly (suggested)
Monitor Vorosimov's movements and prevent his return home (implied)
</details>
<details>
<summary>
2022-04-28: Let's talk about something everybody can learn from Twitter.... (<a href="https://youtube.com/watch?v=pVLNLMb8rMY">watch</a> || <a href="/videos/2022/04/28/Lets_talk_about_something_everybody_can_learn_from_Twitter">transcript &amp; editable summary</a>)

Twitter's global reach mandates adherence to local laws, while user diversity and collective action shape social media power dynamics.

</summary>

"You make Twitter powerful, not the other way around."
"Social media networks run on Highlander rules."
"Your work created that, and they're giving you back some of it."
"Making money on social media requires a lot of users. That means you need a diverse group."
"That new network, it gets its power from you, not the other way around."

### AI summary (High error rate! Edit errors on video page)

Twitter's global reach means it must abide by various countries' laws to remain viable.
Musk has committed to following local laws that may restrict certain content on Twitter.
Total deregulation on social media could lead to toxicity, causing marginalized groups to leave first.
Diversity of thought is vital for social media platforms to thrive.
Interesting individuals leaving a platform can result in its demise, as seen in social media history.
Users hold the collective power that makes social media networks influential.
Collective action by users can drive policy changes on platforms like Twitter.
Social media networks prioritize making money, necessitating a diverse user base for success.
Beau is not concerned about worst-case scenarios on social media platforms.
Users have the ability to shape the direction and success of social media networks through their presence and actions.

Actions:

for social media users,
Advocate for diverse voices and viewpoints on social media platforms (implied)
Participate in collective actions to influence policy changes on social media networks (implied)
Support marginalized groups and ensure their voices are heard on social media (implied)
</details>
<details>
<summary>
2022-04-28: Let's talk about Romney, bribes, and cancelling debt.... (<a href="https://youtube.com/watch?v=11gF3rcswOo">watch</a> || <a href="/videos/2022/04/28/Lets_talk_about_Romney_bribes_and_cancelling_debt">transcript &amp; editable summary</a>)

Beau criticizes Mitt Romney's jest about forgiving debt, advocates for education, and addresses income inequality.

</summary>

"Doing things that benefit your constituents, that's not a bribe. That's literally your job."
"The only reason a politician wouldn't want an educated populace is if they wanted to create a permanent underclass."
"Maybe it's time to do stuff for those on the bottom."

### AI summary (High error rate! Edit errors on video page)

Criticizes Mitt Romney's jest about forgiving trillions in student loans and other debt as not funny.
Mentions forgiving medical debt as another significant aspect.
Suggests that Romney's disconnectedness, having an elevator for his cars, hinders his understanding of income inequality.
Emphasizes that forgiving student loan debt is not a bribe but a benefit to constituents and the country.
Advocates for encouraging education as it is vital for democracy and benefits various aspects like the economy and national security.
Points out that politicians may not want an educated populace to maintain a subservient underclass.
Expresses disregard for prioritizing banks over addressing the massive wealth gap in the country.

Actions:

for politically engaged individuals.,
Contact your representatives to advocate for forgiving student loan debt and promoting education (suggested).
Support initiatives that aim to address income inequality and benefit those at the bottom (implied).
</details>
<details>
<summary>
2022-04-28: Let's talk about 4 developments in Ukraine.... (<a href="https://youtube.com/watch?v=i4gyEOomO-w">watch</a> || <a href="/videos/2022/04/28/Lets_talk_about_4_developments_in_Ukraine">transcript &amp; editable summary</a>)

Russia's gas cutoff impacts Poland and Bulgaria, Kherson fears a fake referendum, partisans face cyber threats, and the Eastern Offensive shows slight gains.

</summary>

"Partisans will now have to start switching out tags and making sure that the tag they obtain to put on the vehicle they're going to use is of the same make and model."
"Kherson has tons of partisan activity. This is not a pro-Russian area."
"Russia has a history of running fake referendums."

### AI summary (High error rate! Edit errors on video page)

Russia has cut off natural gas to Poland and Bulgaria, impacting price fluctuations rather than shortages.
Poland has been preparing for the gas cutoff with its own infrastructure, and it's almost May when gas usage decreases.
In Kherson, there's concern about a potential Russian referendum for the area to become an independent republic through a fake vote.
Kherson has a history of partisan activity and is not a pro-Russian area, making a Russia-favorable vote unlikely if it happens.
Russia has not officially announced a referendum, and it's currently speculation and worry from the Ukrainian government.
Russia's Eastern Offensive has resulted in small gains, but they are not substantial enough to indicate logistical issues yet.
The gains made by Russia in the Eastern Offensive have not stretched the lines enough to reveal any lingering logistical problems.
A Russian cyber offensive targeted vehicular insurance information, posing a threat to partisans who may be identified through vehicles they use.
Partisans will need to take more precautions, such as switching out vehicle tags or borrowing vehicles to avoid being linked to sympathetic individuals.
The developments in Ukraine over the last 48 hours will likely intertwine with future news stories as they progress.

Actions:

for activists, analysts, ukrainians,
Prepare for potential gas price fluctuations and shortages (implied)
Stay vigilant against potential cyber threats and take necessary precautions to protect information (implied)
Monitor the situation in Kherson and be prepared to respond to any developments regarding the fake referendum (implied)
</details>
<details>
<summary>
2022-04-27: Let's talk about people who have had bad takes on Ukraine.... (<a href="https://youtube.com/watch?v=36pzcSjwsYc">watch</a> || <a href="/videos/2022/04/27/Lets_talk_about_people_who_have_had_bad_takes_on_Ukraine">transcript &amp; editable summary</a>)

Addressing bad takes on Russia and Ukraine, commentators stray outside their expertise, influenced by pressure and historical connections, but ideas should stand on their merit alone.

</summary>

"It's not unforgivable that somebody had a bad take on this."
"Ideas stand and fall on their own."
"Nobody is going to be right on it all the time."

### AI summary (High error rate! Edit errors on video page)

Addressing bad takes on Russia and Ukraine, particularly from leftist individuals supporting Russia in the conflict.
People are doubting the beliefs of these individuals due to their support for an authoritarian state in an imperialist conquest.
The association of Russia with the USSR and leftist ideologies is prevalent among some individuals.
Many commentators who stray into topics outside their expertise make mistakes, particularly those focusing on philosophy and political science.
Commentary on the conflict is influenced by pressure to talk about it and connections to the Soviet Union.
Individuals discussing military affairs without expertise often make errors, like using outdated terminology such as "cauldron battle."
The conflict in Ukraine requires a current understanding, not historical references from the past.
U.S. propaganda and misinformation impact commentary on war, leading to overcorrections by commentators.
Being wrong about a particular topic does not invalidate everything else a person has said.
Ideas should be judged based on their merit, regardless of who presents them.

Actions:

for commentators and viewers,
Fact-check information and avoid spreading misinformation (implied).
Encourage critical thinking and independent analysis of ideas presented by commentators (implied).
Seek out diverse sources of information to gain a well-rounded understanding of conflicts and political issues (implied).
</details>
<details>
<summary>
2022-04-27: Let's talk about getting banned books for free.... (<a href="https://youtube.com/watch?v=kmoiVDD9fpQ">watch</a> || <a href="/videos/2022/04/27/Lets_talk_about_getting_banned_books_for_free">transcript &amp; editable summary</a>)

The Brooklyn Public Library provides virtual library cards to individuals aged 13-21, offering access to a wide range of texts amidst a trend of book removals.

</summary>

"Librarians have banned book week where they actually promote texts that are frequently challenged."
"If you are between the ages of 13 and 21, you have a way to get a whole lot of them now."
"They don't have to burn the book, just remove it."

### AI summary (High error rate! Edit errors on video page)

A trend of removing texts from libraries is not new in the US, with 2021 seeing 1,597 challenges or removals according to the American Library Association.
Librarians have banned book week to bring attention to frequently challenged texts.
It has become increasingly difficult to access certain texts in many places.
The Brooklyn Public Library offers a virtual library card for individuals aged 13-21, granting access to 400,000 e-books, audio books, and other online resources.
To obtain the virtual library card, individuals can email booksunbanned@bklynlibrary.org.
Supporting this initiative can be done by donating through bklynlibrary.org.
This access is available for individuals between 13-21 from New York State, with availability outside the state possibly limited.
The initiative aims to ensure people can access the texts they want and serves as a temporary solution.
Censorship in the US often involves denying access to texts rather than physically destroying them.
Denying access to texts is a way to control who reads them without resorting to book burning.

Actions:

for book enthusiasts, youth,
Email booksunbanned@bklynlibrary.org to obtain a virtual library card (suggested).
Donate to support the Brooklyn Public Library's initiative through bklynlibrary.org (suggested).
</details>
<details>
<summary>
2022-04-27: Let's talk about NATO expansion and Russia.... (<a href="https://youtube.com/watch?v=d70Hz7x2rog">watch</a> || <a href="/videos/2022/04/27/Lets_talk_about_NATO_expansion_and_Russia">transcript &amp; editable summary</a>)

Beau explains the flaws in justifying Russian actions through NATO expansion, pointing out the imperialist mindset behind it and condemning imperialism.

</summary>

"NATO expanding and accepting new members is not imperialism."
"Russia is engaged in imperialism. Period."
"This whole talking point of trying to blame it on NATO expansion is a colonizer imperialist mindset."
"Russia is a capitalist oligarchy engaged in imperialist aggression."
"Saying that it's okay is justifying imperialism."

### AI summary (High error rate! Edit errors on video page)

Explains the popular talking point of using NATO expansion to justify Russia's actions in Ukraine.
Breaks down the concept of imperialism today, including force of arms, puppet governments, threats, and economic control.
Clarifies that NATO expansion is not imperialism, but rather individual NATO countries may have engaged in imperialist actions.
Points out that Russia utilized all forms of imperialism in its actions, branding it as engaged in imperialism.
Dismisses the argument of Russia's preemptive strike due to NATO expansion as hollow and revealing an imperialist mindset.
Criticizes justifying Russia's actions by comparing them to what the U.S. might do, pointing out U.S. history of imperialism.
Challenges scenarios suggesting U.S. response to alliances with Russia as flawed logic.
Addresses past U.S. politicians' views on NATO expansion and their imperialist mindset.
Compares Russia's behavior to that of the U.S., both engaging in empire building and imperialism.
Condemns the misguided argument of using NATO expansion to justify imperialism and aggression.

Actions:

for activists, policymakers, educators,
Revisit and adjust your stance on justifying imperialism and aggression based on flawed comparisons and historical contexts (suggested).
Research and understand the dynamics of NATO expansion and imperialist actions to prevent misguided justifications (exemplified).
</details>
<details>
<summary>
2022-04-26: Let's talk about Trump's chances in Georgia.... (<a href="https://youtube.com/watch?v=t22sDpq5rT0">watch</a> || <a href="/videos/2022/04/26/Lets_talk_about_Trump_s_chances_in_Georgia">transcript &amp; editable summary</a>)

Trump's attempts to influence Georgia politics face resistance from Republicans, with his chosen candidates struggling and facing internal issues.

</summary>

"They can't even get Trump candidates to unify behind Trump candidates."
"His ego is damaged. He's looking to get even and he has fielded a slate of candidates that are not doing well."
"The Republican Party in Georgia is just viewing Perdue as somebody to basically a minor annoyance that they have to deal with before they get to the actual fight."

### AI summary (High error rate! Edit errors on video page)

Trump is trying to influence Georgia's political landscape by endorsing specific candidates.
Despite his efforts, Trump has faced opposition from the Republican Party in Georgia.
His chosen candidate for governor, Perdue, embraced Trumpism and repeated baseless election claims in a debate with the current governor, Kemp.
Kemp's campaign has shifted focus to preparing for a general election against Stacey Abrams instead of engaging with Perdue.
Even within Trump's slate of candidates, there are issues as not all have endorsed Perdue.
The Republicans in Georgia view Perdue as a minor annoyance and are confident in defeating him.
Trump's ego may be hurt by the lack of support for his chosen candidates.
Georgia Republicans refused to assist Trump in overturning the election, causing tension.
Trump's desire to be a kingmaker and prepare for a return in 2024 seems uncertain given the current situation in Georgia.

Actions:

for political analysts,
Support local political candidates (exemplified)
Stay informed about political developments in your state (exemplified)
</details>
<details>
<summary>
2022-04-26: Let's talk about Transnistria, the unknown, and new develops for Ukraine.... (<a href="https://youtube.com/watch?v=1ifnIX_f1mw">watch</a> || <a href="/videos/2022/04/26/Lets_talk_about_Transnistria_the_unknown_and_new_develops_for_Ukraine">transcript &amp; editable summary</a>)

Beau delves into the attack on a radio station in Transnistria, raising speculations about potential Russian involvement and the broader geopolitical implications.

</summary>

"This is definitely one of those moments that more than likely we’re going to be coming back to."
"Unless we have confirmation from multiple sources with higher grade information, we may never find out what really happened here."

### AI summary (High error rate! Edit errors on video page)

Transnistria, nestled near Ukraine, is officially part of Moldova but has been quite autonomous with pro-Moscow leanings.
A radio station in Transnistria was attacked over a holiday weekend, fueling speculations about the perpetrator.
Speculation suggests Russia may have orchestrated the attack as a pretext to intervene in the region.
The weapons used in the attack, initially thought to be Russian-only, may not be a conclusive indicator due to Ukraine's past captures of Russian equipment.
This incident draws eerie parallels to a 1939 event where Germans staged an attack on their own radio station to invade Poland.
The situation in Transnistria could potentially escalate, leading to a wider conflict if Russia decides to expand its influence.
Despite proximity and potential strategic gains, Beau questions the rationale behind Russia escalating the conflict in an area that's relatively favorable to them.
The response to any Russian aggression in Moldova could trigger a significant reaction from the West, especially given historical parallels.
The uncertainty surrounding the incident and lack of concrete information may leave the true instigators undisclosed, making it a recurring topic of interest.

Actions:

for world citizens,
Stay informed on developments in Transnistria and surrounding regions (suggested).
Monitor sources for updates on the situation in Transnistria (suggested).
</details>
<details>
<summary>
2022-04-26: Let's talk about Bishop Evans and Operation Lone Star.... (<a href="https://youtube.com/watch?v=jJ3jn3KB7J4">watch</a> || <a href="/videos/2022/04/26/Lets_talk_about_Bishop_Evans_and_Operation_Lone_Star">transcript &amp; editable summary</a>)

Operation Lone Star under Governor Abbott faces scrutiny for risking lives and state resources in a troubled mission, prompting troops' unionization and questioning Texans' support.

</summary>

"They're not going to admit that. They're going to say that they're frustrated and frustrated with their leadership because of how the operation's being run, probably because they know it's a PR stunt."
"How much money is Texas willing to lose? And how many troops is Texas willing to lose?"
"A governor attempting to secure re-election can have a photo op, can have a PR stunt, can find some way to channel his base's energy into kicking down at people."

### AI summary (High error rate! Edit errors on video page)

Operation Lone Star under Texas Governor Abbott gaining national attention due to Specialist Bishop Evans' heroic act gone wrong, trying to save drowning individuals.
Not the first death associated with Operation Lone Star, with at least the seventh known casualty.
Troops in the operation facing dire conditions, attempting to unionize due to the situation.
Operation Lone Star's claimed successes questioned by various reputable sources like Texas Tribune, ProPublica, NPR, and even the Army Times, labeling the operation as troubled.
Troops expressing skepticism and frustration with the leadership and the operation itself, feeling like they are being used as mere props for a photo op.
Operation Lone Star's outcomes: no drugs intercepted, no migrants caught, costing Texas $4.2 billion and shrinking the US GDP by $9 billion.
Criticism towards Governor Abbott for risking lives and state funds for a photo op and PR stunt at the border.
Texans urged to question how much money and troops they are willing to lose for a governor's reelection bid.

Actions:

for texans, voters,
Question and hold Governor Abbott accountable for the decisions regarding Operation Lone Star (implied)
Support troops' rights to unionize and express their concerns (implied)
</details>
<details>
<summary>
2022-04-25: Let's talk about Finland, Ukraine, and US diplomacy.... (<a href="https://youtube.com/watch?v=qzfE0zdbL8A">watch</a> || <a href="/videos/2022/04/25/Lets_talk_about_Finland_Ukraine_and_US_diplomacy">transcript &amp; editable summary</a>)

The Russian offensive stalls in Ukraine, US provides aid, Finland and Sweden eye NATO amidst Russian threats, and talks await resolution.

</summary>

"The attacks are continuing. They're just not moving anywhere."
"One of the things that is being brought up that I find interesting is Russia is already kind of grumbling and making threats."
"They're not going to have to bring in a lot of equipment and stuff like that, but they're still on common ground."
"An assurance is, we'll help you out if something happens. A guarantee is, you go to war, we go to war."
"I think it's more for peace of mind than actual deterrence."

### AI summary (High error rate! Edit errors on video page)

The Russian offensive in eastern Ukraine is stalling, with minimal gains and attacks continuing without significant progress.
There is debate over whether the current offensive is just a diversion or the main attack, suggesting disorganization in Russian military strategy.
The US recently had high-level diplomatic visits to Ukraine, hinting at a limited embassy reopening and increased military aid in the form of financing for ammunition purchases.
Finland and Sweden are showing interest in joining NATO, with concerns over security guarantees amidst Russian threats of deploying nukes.
Suggestions have been made to provide security guarantees to Finland and Sweden upon applying to NATO to deter potential Russian aggression.
Talks between Russia and Ukraine are at a standstill, waiting for the offensive's resolution before significant progress can be made.

Actions:

for diplomacy watchers,
Monitor diplomatic developments and advocate for peaceful resolutions (suggested)
Stay informed about international relations and potential security threats (suggested)
</details>
<details>
<summary>
2022-04-25: Let's talk about Elon Musk buying Twitter.... (<a href="https://youtube.com/watch?v=EBeEjohoLiU">watch</a> || <a href="/videos/2022/04/25/Lets_talk_about_Elon_Musk_buying_Twitter">transcript &amp; editable summary</a>)

Elon Musk's purchase of Twitter may not lead to significant changes in moderation, amid concerns and expectations of unmoderated content.

</summary>

"Musk's ideas for Twitter, they sound good in theory until somebody tries to implement them."
"You're not going to have a social media network without moderation because it becomes a sewer very very quickly."
"I wouldn't worry too much about it right now. I'd wait and see what shapes up."
"And when you see the PR campaign, the ad campaign, ask yourself if you're really experiencing any difference on the site or if you're just being told things are done differently."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Elon Musk is reported to have bought Twitter, sparking varied responses from people.
Some are excited at the prospect of an unmoderated Twitter, while others are concerned about the same.
Musk's image as a free speech absolutist may not lead to Twitter becoming completely unmoderated.
Even right-wing social media networks have moderation due to the necessity of safety policies.
Removing Twitter's safety policies could make Musk and the company liable for any harm caused.
There might be a PR shift in perception regarding moderation but not a significant actual change.
Without moderation, Twitter could be inundated with botnets and toxic behavior, driving away core users.
Musk's ideas for Twitter may sound good in theory but face challenges in implementation.
Musk might face opposition from his lawyers if he tries to implement extreme moderation changes.
The far-right social media networks that claim to be unmoderated are also moderated differently.

Actions:

for social media users,
Monitor Twitter for any significant changes in moderation policies (suggested)
Participate in the platform with awareness of potential shifts in perception regarding moderation (suggested)
</details>
<details>
<summary>
2022-04-24: Let's talk about defense companies, money, and Ukraine.... (<a href="https://youtube.com/watch?v=NYe5U7fsR9A">watch</a> || <a href="/videos/2022/04/24/Lets_talk_about_defense_companies_money_and_Ukraine">transcript &amp; editable summary</a>)

Beau explains the nuances of US military aid, clarifying that while defense contractors profit from war, the aid to Ukraine is not a significant money-maker for them.

</summary>

"Defense companies make money off of war. It's literally the drive behind their product."
"The amount of aid that we have provided to Ukraine is what gets rounded off when you're talking about their revenue."
"To them, that's a tax write-off."
"This isn't defense industry money."
"It's not really happening with the aid."

### AI summary (High error rate! Edit errors on video page)

Explains the common talking point that the US provides military aid to help defense contractors, acknowledging its accuracy at times.
Points out that defense companies profit from war, which can be seen as unethical or as blood money.
Mentions the military-industrial complex created by the defense industry and Congress, leading to continuous cycles of profit.
Provides a perspective on the amount of aid provided to Ukraine compared to the revenue of major defense contracting companies.
Emphasizes that the aid to Ukraine, at $3 billion, is a very small percentage of the total revenue of these companies.
States that for big defense contracting companies like Raytheon, the aid amount is insignificant and more like a tax write-off.
Notes that while some companies may profit from developing new military technology like drones, they often end up being acquired by larger corporations.
Suggests that while US policy is sometimes influenced by money in the defense industry, in this case, the aid to Ukraine is not a significant money-making venture for these companies.
Warns about potential future increased spending in the defense industry post-conflict, where companies might profit substantially.
Anticipates that if defense industry interests start influencing aid packages significantly, it will lead to a surge in total spending.

Actions:

for public, policy makers,
Monitor future spending post-conflict for potential excessive defense industry influence (implied).
Stay informed about military aid and defense industry dynamics (implied).
</details>
<details>
<summary>
2022-04-24: Let's talk about Japanese-Russian relations.... (<a href="https://youtube.com/watch?v=gMtPzQI6t4A">watch</a> || <a href="/videos/2022/04/24/Lets_talk_about_Japanese-Russian_relations">transcript &amp; editable summary</a>)

Moscow's historical imperialism poses a threat as Japan's response to Russia's invasion of Ukraine shifts, with Ukraine likely to gain international support against Russian aggression.

</summary>

"Moscow signs a statement of, you know, hey, don't worry, we're not gonna, we're not gonna come after you, we're gonna be neutral, don't worry about it."
"They've apparently given up on that, or they realize that they're not going to get the islands back by remaining quiet."
"We're just not going to even talk about giving this land back."

### AI summary (High error rate! Edit errors on video page)

Moscow promised not to invade Ukraine but later invaded and took land, breaking the promise.
Japan historically refrained from criticizing Russia internationally due to hopes of getting the Kuril Islands back.
Japan's response to Russia's invasion of Ukraine was different this time, with sanctions and diplomatic protests.
Japan is sending Ukraine drones and CBRN equipment to protect against chemical, biological, radiological, and nuclear threats from Russia.
Putin responded by saying Russia won't talk about giving the Kuril Islands back.
Moscow's historical imperialism is concerning for Ukraine since they tend to keep any land they take and hold.
Ukraine might push into Crimea or other contested areas as they regain the initiative, with increasing international support.
The longer the conflict drags on, the more support Ukraine might gather.
Russia has a history of changing maps and holding onto conquered territories.
Ukraine can learn from Japan's experience with Russia in understanding the importance of historical precedents.

Actions:

for foreign policy analysts,
Send aid and support to Ukraine (suggested)
Advocate for diplomatic efforts to resolve conflicts (suggested)
</details>
<details>
<summary>
2022-04-23: Let's talk about why parts of the right are supporting Russia.... (<a href="https://youtube.com/watch?v=vcuvzyJCnJQ">watch</a> || <a href="/videos/2022/04/23/Lets_talk_about_why_parts_of_the_right_are_supporting_Russia">transcript &amp; editable summary</a>)

Beau explains the profit-driven fear tactics of survival channels spreading false invasion narratives to maintain audience engagement.

</summary>

"They sell fear."
"It's not profitable to tell the truth in that case."
"Keeping that dream alive."
"The days of having to worry about somebody invading the United States, those are long gone."
"Rather than taking the time to educate their audience on how to prepare for stuff that might actually happen, like climate change."

### AI summary (High error rate! Edit errors on video page)

Explaining why some people believe that everything is going according to Russia's plan in Ukraine, despite contradictory actions by Putin and the reality of the situation.
Mentioning how both far-right and far-left individuals are repeating the lie that everything is going according to Russia's plan.
Pointing out that certain survival channels continue to spread claims that Russia is winning and that the US could be next, fueling fear to keep their audience engaged.
Debunking the idea that Russia poses a real invasion threat to the United States, citing population numbers and logistics.
Emphasizing that the current military landscape and global awareness make a large-scale invasion impractical and unlikely.
Noting that fear-mongering channels prioritize selling fear over educating their audience on more realistic threats like climate change.
Stating that maintaining fear is profitable for these channels, even if it means perpetuating falsehoods and sensationalism.

Actions:

for content creators, viewers,
Educate your audience on real threats like climate change, encouraging preparedness beyond fear-based scenarios (implied).
</details>
<details>
<summary>
2022-04-23: Let's talk about sympathy for President Reagan.... (<a href="https://youtube.com/watch?v=3AP7DysZDa8">watch</a> || <a href="/videos/2022/04/23/Lets_talk_about_sympathy_for_President_Reagan">transcript &amp; editable summary</a>)

A critical reflection on the dangers of simplistically categorizing historical figures as heroes or villains, urging to view them as flawed individuals to learn from their mistakes.

</summary>

"But the reality is that's not history, that's mythology."
"It's fine to use people as a teaching tool to get people to understand the story, the general narrative."
"Every cop is a criminal, and all the sinner saints."
"Historical figures were just people, not gods, not deities, not devils."
"We can't shape history that way, because then it's not history, it's mythology."

### AI summary (High error rate! Edit errors on video page)

A high school student asked for help with a project on President Reagan depicting him as a hero or villain.
The replies on Twitter gave the student a list of Reagan's deeds and misdeeds to help with the project.
Beau criticizes the assignment for framing historical figures as heroes or villains, calling it mythology rather than history.
He warns of the danger of overlooking the flaws of historical figures when classifying them in simplistic terms.
Reagan, often depicted as a hero in American history mythology, had misdeeds that could be glossed over.
Beau points out that historical figures like Reagan were flawed individuals, not purely good or evil.
He cautions against viewing historical figures as gods or devils, urging to understand them as flawed people.
Beau advocates using historical figures as teaching tools to learn from their mistakes and not repeat them.
He warns against perpetuating the myth that historical figures were faultless, as this can lead to dangerous black-and-white thinking.
Beau encourages acknowledging the flaws of historical figures and using them to learn deeper aspects of history.

Actions:

for students, educators, historians,
Question assignments that oversimplify historical figures as heroes or villains (exemplified).
Encourage critical thinking in historical analysis (implied).
Teach history as a complex narrative of real individuals with flaws and virtues (exemplified).
</details>
<details>
<summary>
2022-04-23: Let's talk about Air Force readiness.... (<a href="https://youtube.com/watch?v=nGYq1O-2Z38">watch</a> || <a href="/videos/2022/04/23/Lets_talk_about_Air_Force_readiness">transcript &amp; editable summary</a>)

Three conflicting messages spark a talk on Air Force readiness, revealing the impacts of laws and policies on personnel movements and economic well-being.

</summary>

"There aren't a lot of laws that are bigoted in nature that don't cause a negative economic impact."
"It's cheaper to be a good person."
"Always does."

### AI summary (High error rate! Edit errors on video page)

Three messages back-to-back about Air Force readiness sparked a need for a talk on the topic.
The story involves conflicting messages about the impacts of laws in Texas on Air Force readiness.
Details the process of Air Force personnel moving every four years and the implications of disruptions.
Explains how a chain of events can lead to gaps in personnel, affecting readiness and causing economic impacts.
Suggests that tracking gender and orientation could help mitigate some issues but poses challenges.
Points out the domino effects on housing, local businesses, and the economy due to personnel disruptions.
Emphasizes that laws impacting military personnel based on bigotry can have negative economic consequences.
Encourages prioritizing policies that allow service members to thrive and contribute positively to the economy.
Concludes by advocating for kindness and cautioning against supporting politicians who incite division for votes.

Actions:

for military service members,
Advocate for policies that support military personnel and their families (suggested)
Support businesses impacted by fluctuations in military personnel presence (implied)
</details>
<details>
<summary>
2022-04-22: Let's talk about new tools in Ukraine.... (<a href="https://youtube.com/watch?v=vMmTx-1mdoE">watch</a> || <a href="/videos/2022/04/22/Lets_talk_about_new_tools_in_Ukraine">transcript &amp; editable summary</a>)

The US is providing Ukraine with more substantial tools for defense, including deployable equipment and new weaponry like the Ghost Phoenix, potentially shifting the balance of power in the conflict with Russia.

</summary>

"The days of piecemeal deliveries of small stuff, that's ending."
"The offensive that the Russians apparently have underway right now is shaping up to be different."
"If Ukraine has the material edge before this offensive really gets rolling, that's going to change a lot of math."

### AI summary (High error rate! Edit errors on video page)

The US is committed to giving Ukraine more significant tools to defend against Russia's new offensive.
The US is no longer providing just defensive equipment but is now supplying deployable equipment like 155mm howitzers.
An unmanned aircraft called Ghost Phoenix has been developed for Ukraine, possibly for intelligence gathering and surveillance.
The speed at which the Ghost Phoenix was developed hints at off-the-shelf technology being adapted for new roles.
This rapid development could level the playing field in providing air support, reducing the advantage the US has.
Ukraine now has more tanks in the field than Russia, acquired through capture, redeployment of Russian equipment, and tanks from undisclosed countries.
The West seems to be ensuring that Ukraine has the material edge over Russia in the ongoing conflict.
The new weapons being delivered to Ukraine have higher capabilities and could potentially alter the course of the conflict.
If Ukraine gains a material edge before Russia's offensive ramps up, it could significantly impact the situation.
The developments in weaponry may lead to constantly evolving tactics on the battlefield.

Actions:

for foreign policy analysts,
Monitor the situation in Ukraine and Russia for updates on the evolving conflict (implied).
Support diplomatic efforts to de-escalate tensions in the region (implied).
</details>
<details>
<summary>
2022-04-22: Let's talk about McCarthy and Trump.... (<a href="https://youtube.com/watch?v=zgdf-SUkZxY">watch</a> || <a href="/videos/2022/04/22/Lets_talk_about_McCarthy_and_Trump">transcript &amp; editable summary</a>)

The New York Times reported on McCarthy's consideration of telling Trump to resign post January 6th, leading to a recorded tape contradicting McCarthy's denial and raising questions about potential pardon needs. Further investigation is needed into McCarthy's beliefs.

</summary>

"I want to know why he believed that the subject of Pence pardoning would come up."
"This certainly appears to suggest that the lead Republican in the House believed that Trump did something that he'd need to be pardoned for."
"I'd like further explanation on that, personally."

### AI summary (High error rate! Edit errors on video page)

New York Times reported McCarthy considered telling Trump to resign after January 6th events.
McCarthy denied the report, calling it "totally false and wrong."
New York Times released a recorded tape contradicting McCarthy's denial.
McCarthy's denial may have political consequences, potentially impacting his goal of becoming Speaker of the House.
McCarthy expressed intent to talk to Trump about resigning but not about Pence pardoning him.
Beau questions why the topic of Pence pardoning Trump was even brought up.
Beau suggests further investigation into why McCarthy thought Trump needed a pardon.
Implications suggest McCarthy believed Trump committed actions requiring a pardon.
Beau hints at the need for more clarity and investigation into McCarthy's statements and beliefs.
The situation reveals potential inconsistencies and deeper issues within Republican leadership.

Actions:

for political analysts, journalists,
Investigate further into McCarthy's statements and beliefs (implied).
</details>
<details>
<summary>
2022-04-22: Let's talk about Disney and Desantis.... (<a href="https://youtube.com/watch?v=2sCfOL_MAqY">watch</a> || <a href="/videos/2022/04/22/Lets_talk_about_Disney_and_Desantis">transcript &amp; editable summary</a>)

The state of Florida's decision to end its long-standing deal with Disney sends a damaging message to corporate America and may have lasting negative effects on the state's economy.

</summary>

"Disney's lawyers will take this to court, and they'll win, and everything will go back to normal."
"No company is going to come to Florida and invest billions when it can all be snatched away."
"It is damage that can't be undone, regardless of what happens with Disney."

### AI summary (High error rate! Edit errors on video page)

The state of Florida had a long-standing deal with Disney, allowing Disney to control the infrastructure, a critical aspect.
DeSantis requested to terminate this deal, setting a distant date for after the election to avoid immediate consequences.
Potential negative outcomes include residents being financially responsible for the bonds associated with this termination.
Beau predicts that Disney will take legal action and likely win, reverting everything back to normal.
If Disney doesn't win, there's a belief they may have to leave due to the inability to maintain their brand without controlling the infrastructure.
The surrounding areas may struggle to meet Disney's standards without the necessary funds.
Beau questions if Disney could work out a deal with the county but ultimately believes they may have to relocate.
The damage to Disney's brand and the economic impact on the state could be significant if they have to leave.
A signal has been sent to corporate America, warning against investing in Florida due to the uncertainty of deals with the state.
This situation may have lasting negative effects on Florida's economy, regardless of the outcome with Disney.

Actions:

for florida residents, corporate america,
Advocate for transparency and accountability in government decisions (implied)
Support businesses and organizations impacted by political decisions (implied)
Stay informed about government actions that may affect local economies (implied)
</details>
<details>
<summary>
2022-04-21: Let's talk about children getting married.... (<a href="https://youtube.com/watch?v=u0YRbHs-2DE">watch</a> || <a href="/videos/2022/04/21/Lets_talk_about_children_getting_married">transcript &amp; editable summary</a>)

Beau examines child marriage statistics to debunk the Republican Party's false claim against Democrats, revealing a prevalence of treating children as adults in Republican strongholds.

</summary>

"Treating children as adults doesn't really seem to be a blue state thing here."
"Statistics suggest that the ultimate culmination of that activity ending in marriage is most prevalent in red states, in Republican areas."
"It's the Republican Party attempting to marginalize a group of people with an allegation that isn't true."

### AI summary (High error rate! Edit errors on video page)

Examines statistics and data to address a Republican Party allegation regarding Democrats and children.
Focuses on the claim that Democrats are encouraging children to partake in adult activities.
Uses child marriage statistics as an indicator of attitudes towards children engaging in adult behaviors.
Reveals the states with the highest rates of child marriage, showing Republican strongholds topping the list.
Notes that treating children as adults is more prevalent in red states rather than blue states.
Points out the Republican Party's strategy of marginalizing groups with false allegations.
Suggests that if the Republican Party is genuinely concerned about the issue, they could focus on changing state laws in states they control.

Actions:

for policy advocates,
Contact local representatives in states with high child marriage rates to advocate for stricter laws (suggested)
Join organizations working towards eliminating child marriage in communities (suggested)
</details>
<details>
<summary>
2022-04-21: Let's talk about Trump losing the Tennessee GOP.... (<a href="https://youtube.com/watch?v=8QjNbBnJUUM">watch</a> || <a href="/videos/2022/04/21/Lets_talk_about_Trump_losing_the_Tennessee_GOP">transcript &amp; editable summary</a>)

The GOP in Tennessee challenges Trump's influence by voting off his endorsed candidate, signifying his diminishing hold on the party and potential implications for 2024 aspirations.

</summary>

"Trump's hold on the Republican Party is slipping."
"Trump's reign as kingmaker of the Republican party will be over."
"If he lets this slide, if he doesn't respond, there will be more."

### AI summary (High error rate! Edit errors on video page)

Talking about the inner workings of the GOP in Tennessee and the case of Morgan Ortegas.
Ortegas, a hopeful for the 5th District, was voted off the ballot by the executive committee of the Republican Party.
Speculations include not meeting club rules, political associations, internal Tennessee politics, or discrimination.
Ortegas was endorsed by Trump, which makes her removal significant due to Trump's slipping hold on the Republican Party.
The move to bar Ortegas from running could be seen as a challenge to Trump's influence within the party.
Trump's diminishing power is evident as the Republican Party in Tennessee didn't hesitate to oust his endorsed candidate.
Beau anticipates Trump may retaliate to reclaim his diminishing influence, especially in Tennessee.
If Trump doesn't respond aggressively to the Republican Party's actions in Tennessee, it signals a decline in his political power.
The lack of fear among state-level party officials of angering Trump indicates a shift in the political landscape.
Trump's ability to influence the Republican Party and his potential for 2024 presidential aspirations are at stake due to recent developments.

Actions:

for political observers, gop members,
Reach out to local Republican Party officials for insights and involvement in party decisions (implied)
</details>
<details>
<summary>
2022-04-21: Let's talk about Florida's upside-down politics.... (<a href="https://youtube.com/watch?v=0N2cqriDgLo">watch</a> || <a href="/videos/2022/04/21/Lets_talk_about_Florida_s_upside-down_politics">transcript &amp; editable summary</a>)

Beau dives into the unique world of Florida politics, focusing on Democrat Nikki Fried's fight for Second Amendment rights, challenging traditional narratives and creating dilemmas for both parties.

</summary>

"Florida is different. It's Australia-lite."
"She's suing, trying to get an exemption for this."
"It's going to defy a lot of people who try to create very simple narratives."

### AI summary (High error rate! Edit errors on video page)

Florida politics is unique and different, with its own set of challenges and peculiarities.
Florida is home to a Democrat, Agriculture Commissioner Nikki Fried, who is suing the Biden administration over a Second Amendment issue.
The issue involves a form for background checks when purchasing guns, where the question about drug use can put individuals in a difficult position.
Fried is fighting for an exemption for individuals in states where medicinal drug use is allowed but impacts their ability to pass the background checks.
The lawsuit was launched on April 20th, demonstrating Fried's sense of humor.
This situation puts political commentators in a bind as it doesn't neatly fit into their usual talking points.
Republicans might struggle with this issue as it doesn't follow the typical narrative of Democrats trying to take guns.
Democratic commentators may find it challenging to attack Fried due to the potential positive impact on background checks.
Fried's stance on this issue might create a dilemma for Florida voters between a pro-Second Amendment Democrat and Governor Ron DeSantis.
Fried's gubernatorial campaign adds another layer of interest and potential conflict to the situation.

Actions:

for political commentators,
Support Nikki Fried's lawsuit by spreading awareness and advocating for exemptions for individuals impacted by the background checks (implied).
Stay informed about Florida politics and the nuances of Second Amendment issues to make well-informed decisions as a voter (implied).
</details>
<details>
<summary>
2022-04-20: Let's talk about what's in it for the West.... (<a href="https://youtube.com/watch?v=nC0goSptDmM">watch</a> || <a href="/videos/2022/04/20/Lets_talk_about_what_s_in_it_for_the_West">transcript &amp; editable summary</a>)

Beau explains how countries act in their own interests, not morality, in response to conflicts like the Ukraine-Russia situation, aiming for Ukraine's strength and neutrality.

</summary>

"Countries don't have morals. They have interests."
"The ideal ending to this for the West [...] for Ukraine to remain neutral."
"Don't turn it into the country having a moral compass."

### AI summary (High error rate! Edit errors on video page)

Explains how countries like the U.S. do not take moral stances in foreign policy, but rather act based on their interests.
Describes the initial reactions and moves made by the West when Russia invaded Ukraine.
Points out that countries do not have moral compasses, despite individuals within governments possibly believing in moral causes.
Details the change in Western support for Ukraine when Ukraine appeared to have the upper hand in the conflict.
Outlines the ideal outcome for the West in the Ukraine-Russia conflict, which involves Ukraine remaining neutral but strong against Russia.

Actions:

for foreign policy analysts,
Support Ukraine by giving military aid and resources to strengthen them (implied).
</details>
<details>
<summary>
2022-04-20: Let's talk about a new law Tennessee about homelessness.... (<a href="https://youtube.com/watch?v=Vv9GURQTNoM">watch</a> || <a href="/videos/2022/04/20/Lets_talk_about_a_new_law_Tennessee_about_homelessness">transcript &amp; editable summary</a>)

Legislation in Tennessee criminalizes homelessness, targeting vulnerable individuals instead of addressing root issues, advocating for a compassionate approach to provide housing and support.

</summary>

"Homelessness. I mean, not to put too fine a point on it, but that's what it does."
"The problem is in the name. Homelessness."
"It's cheaper to be a good person."
"Stop looking under the bridge and start looking up at that Capitol."
"Those people with less institutional power than you have, less money, less resources, less everything than you have are never the source of your problem."

### AI summary (High error rate! Edit errors on video page)

Legislation in Tennessee criminalizes homelessness.
Makes camping on local public property a felony, punishable by up to six years in prison.
Demonstrations against racial injustice led to the legislation.
Asking for change or camping alongside highways also criminalized.
The legislation fails to address the root causes of homelessness.
Supporters and opponents of the legislation lack comprehensive answers to homelessness.
The real problem is homelessness itself.
Providing homes and addressing underlying issues like rehab and mental health care is the solution.
Incarcerating homeless individuals for up to six years is not a solution.
Housing homeless individuals in a humane way is more cost-effective and productive than incarceration.
Politicians target homeless individuals with legislation to appease their base.
Homeless individuals are not the source of societal problems; the government is.
The focus should be on holding politicians accountable rather than targeting the homeless.
Housing homeless individuals in a helpful way benefits everyone and is cost-effective.
It's cheaper and better to address homelessness compassionately.

Actions:

for tennessee residents,
Contact local representatives to oppose the legislation (suggested).
Support organizations providing housing and support for the homeless (exemplified).
</details>
<details>
<summary>
2022-04-20: Let's talk about Mallory McMorrow and identifying with the bad guy.... (<a href="https://youtube.com/watch?v=9KgVF25J8cQ">watch</a> || <a href="/videos/2022/04/20/Lets_talk_about_Mallory_McMorrow_and_identifying_with_the_bad_guy">transcript &amp; editable summary</a>)

Beau challenges parents to rethink why they want their children to identify with figures who upheld slavery instead of those who fought against it, urging a shift towards honoring heroes who stood up against injustice throughout history.

</summary>

"History doesn't make your child feel bad. You siding with the villain does."
"I don't know why people want their children to identify with the villain in the story when there are heroes."
"They existed throughout American history but they're overshadowed by indoctrination that said that slavery was some necessary evil which is garbage."

### AI summary (High error rate! Edit errors on video page)

Mallory McMorrow, a Democrat in the Michigan state legislature, faced a political attack for advocating history education that includes teaching about slavery's wrongs.
Beau questions why some parents want their children taught to identify with figures who upheld slavery rather than those who fought against it.
He points out that history doesn't inherently make children uncomfortable; it's the parents' moral choices in upholding certain figures as heroes.
Beau challenges the notion that teaching history equates to blaming white children for past atrocities, instead advocating for honoring those who opposed systems like slavery.
He urges parents to guide their children towards identifying with those who stood up against injustice throughout history, regardless of skin color.
The concern lies in children being taught to view heroes as villains and vice versa, perpetuating a harmful narrative.
Beau stresses the importance of dismantling the remnants of systems like slavery that still benefit certain groups today.
He concludes by encouraging listeners to prioritize identifying with the morally upright individuals in history.

Actions:

for parents, educators, activists,
Guide children to identify with historical figures who fought against injustice, regardless of skin color (exemplified)
Advocate for inclusive and accurate history education in schools (exemplified)
</details>
<details>
<summary>
2022-04-19: Let's talk about winning and losing in Ukraine.... (<a href="https://youtube.com/watch?v=-pK6NI37Zm4">watch</a> || <a href="/videos/2022/04/19/Lets_talk_about_winning_and_losing_in_Ukraine">transcript &amp; editable summary</a>)

Beau explains winning and losing in war through examples, stating that Ukraine could still lose, but Russia cannot win the war despite potential for winning the fighting.

</summary>

"The war is lost to Russia, but they still could win the fighting."
"Everything that's happening now is just determining how much it costs to lose, and it's just waste."
"The longer they pursue it, the more they lose it by."
"The days of there being something that they could redeem and being able to walk away with this or walk away from this in a more powerful position, they're long, long gone."
"they just have to realize that they lost."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of winning and losing in the context of the situation in Ukraine.
Raises questions about whether Ukraine can lose and if it's impossible for Russia to win.
Defines war as a continuation of politics by other means, aiming to achieve political goals.
Gives examples like the Iraq War and the War of 1812 to illustrate winning and losing wars.
Mentions that Ukraine could still lose, but Russia cannot win the war at this point.
States that Russia lost the war in the first four days due to economic and military damage.
Notes the importance of perceived military strength in determining a country's power.
Emphasizes that Russia may still win the fighting but cannot win the war geopolitically.
Expresses frustration at the ongoing costs and waste in the conflict.
Concludes that Russia needs to realize they have lost the war.

Actions:

for global citizens, policymakers,
Stay informed on the situation in Ukraine and advocate for peaceful resolutions (implied)
</details>
<details>
<summary>
2022-04-19: Let's talk about Eastern Ukraine.... (<a href="https://youtube.com/watch?v=3AFMxP_yJwE">watch</a> || <a href="/videos/2022/04/19/Lets_talk_about_Eastern_Ukraine">transcript &amp; editable summary</a>)

Beau provides insights on the ongoing offensive in Eastern Ukraine, suggesting that Ukrainian military strategies extend beyond pitched battles, and Western analysts may underestimate their capabilities.

</summary>

"It's defensive. It's about making it too costly for Russia to stay."
"The good guys don't win every battle."
"Everybody has a plan until you get punched in the face."
"It's about breaking the hammer."
"The initiative lies with Russia."

### AI summary (High error rate! Edit errors on video page)

Beau provides insights on the ongoing situation in Eastern Ukraine, discussing the offensive that Russia has initiated.
Russia's move back east is seen as an acknowledgment of the failure of their previous operation.
The offensive involves strong artillery and air power aimed at softening Ukrainian lines before attempting to create a breakout.
The initiative lies with Russia, allowing them to dictate the next moves, like attempting breakouts or encircling Ukrainian forces.
Contrary to mainstream media portrayals, Ukrainian military strategies extend beyond pitched battles and could involve partisan activities and fighting retreats.
Beau believes Western analysts may underestimate the Ukrainian military if they focus solely on pitched battles.
He suggests that allowing Russian forces to advance and then encircling them could be a successful strategy for Ukraine.
Beau outlines the potential strategies available to Ukraine, including resisting, disappearing, fighting in the hills, and setting up partisan campaigns.
Ukrainian commanders may be hesitant to employ certain strategies due to Russian behavior towards civilians in occupied areas.
The situation in Eastern Ukraine remains uncertain, with various possible outcomes based on the strategies adopted by both sides.

Actions:

for military analysts, strategists,
Strategize for diverse military tactics to counter threats effectively (implied)
Prepare for potential partisan activities and fighting retreats (implied)
</details>
<details>
<summary>
2022-04-19: Let's talk about China, supply chains, and public health.... (<a href="https://youtube.com/watch?v=kenubeaYcWI">watch</a> || <a href="/videos/2022/04/19/Lets_talk_about_China_supply_chains_and_public_health">transcript &amp; editable summary</a>)

Lockdowns in China impacting 373 million people with significant supply chain disruptions, potentially leading to longer wait times and price increases for products globally.

</summary>

"There have been a string of lockdowns in China."
"So we will have ships sitting off of our coast waiting to be unloaded again."
"If you're waiting on products from China, you may be waiting longer."
"It's just kind of what we're expecting now in the 2020s."
"We don't have an end date in sight for the lockdowns."

### AI summary (High error rate! Edit errors on video page)

Series of lockdowns in China started on April 2, impacting about 373 million people, more than the US population.
Lockdowns cover regions responsible for about 40% of China's GDP.
World's largest and fourth largest ports are not operating due to lockdowns.
300 container ships and 500 bulk ships off the coast of China waiting to be unloaded.
Impending supply chain issues globally due to the backlog of orders.
China will rush production to meet backed-up orders once lockdowns lift.
Delays expected in unloading ships in the US, causing backups in trucking and rail industries.
Experts suggest potential for worse disruptions than seen before.
Consumers might face longer wait times for products from China and potential price increases.
Combined with inflation and political issues, a rough month is anticipated.

Actions:

for global consumers,
Monitor your orders from China for potential delays (implied).
Adjust expectations for product arrival times and potential price increases (implied).
</details>
<details>
<summary>
2022-04-18: Let's talk about the US already learning from Ukraine and what we should... (<a href="https://youtube.com/watch?v=zM2wGGttdvE">watch</a> || <a href="/videos/2022/04/18/Lets_talk_about_the_US_already_learning_from_Ukraine_and_what_we_should">transcript &amp; editable summary</a>)

Ukraine events shape U.S. training at National Training Center, integrating lessons on combat tactics and information operations, showcasing the power of social media in shaping narratives and fostering change.

</summary>

"Information is power."
"Your cell phone, your ability to document what's happening and get that record created and put it out on social media, That is, that's so important."
"Social media has handed individual people, non-state actors, the same power to generate propaganda, negative or positive."
"Your activism, your desire for that better world. Your strategies for achieving it should probably acknowledge this."
"On a long enough timeline, we win."

### AI summary (High error rate! Edit errors on video page)

Ukraine events have influenced U.S. training at the National Training Center.
U.S. forces are currently at war with a fake country, Danovia, mimicking Russia.
Russian combat tactics, including shelling civilian areas, are integrated into U.S. training.
Danovians in the scenario will have cell phones and focus on information operations.
Individuals can shape narratives through social media, impacting the outcome of events.
Social media empowers non-state actors to generate propaganda and shape narratives.
The importance of social media in influencing thought and creating change is recognized by the U.S. military.
Activism and sharing information online can have a significant impact on society.
Social media is a powerful tool that should be acknowledged in strategies for creating a better world.
The U.S. military is strategizing on countering social media influence in conflicts.

Actions:

for activists, social media users,
Document and share information on social media platforms to shape narratives and create awareness (exemplified).
Utilize social media for activism and advocating for positive change in society (exemplified).
</details>
<details>
<summary>
2022-04-18: Let's talk about Texas, Abbott, and produce.... (<a href="https://youtube.com/watch?v=x7zrUiq1BL4">watch</a> || <a href="/videos/2022/04/18/Lets_talk_about_Texas_Abbott_and_produce">transcript &amp; editable summary</a>)

Some Republicans in Texas are seeing through Governor Abbott's costly political stunts that hinder food supply chains and fail to address real issues, urging Texans to demand better leadership.

</summary>

"Your inspection protocol is not stopping illegal immigration. It is stopping food from getting to grocery store shelves and in many cases, causing food to rot in trucks."
"Those people in Texas, you've got your work cut out for you because Governor Abbott has name recognition going for him and he has that R after his name."
"I think Texans deserve better."

### AI summary (High error rate! Edit errors on video page)

Some Republicans are seeing through the political stunts of Governor Abbott in Texas.
Operation Lone Star, the governor's attempt at border security, led to a quarter of a billion dollars in losses of fruits and vegetables.
The costly operation, now shut down, caused produce to rot in trucks due to logistics failures.
Republican Agriculture Commissioner Sid Miller criticized the policy, foreseeing rising food prices and shortages.
Governor Abbott's policies are not stopping illegal immigration but hindering food supply chains.
Abbott's administration faces criticism for various failures, including the power grid and costly security operations.
Republicans and Democrats in Texas are starting to speak unfavorably about Governor Abbott's leadership.
Some Republicans are recognizing the ineffectiveness of political stunts as leadership.
Political stunts by leaders often lack measurable success metrics and end up harming the people they are supposed to help.
Texans are urged to demand better leadership from Governor Abbott despite his name recognition and party affiliation.

Actions:

for texans,
Demand better leadership from Governor Abbott (implied)
Stay informed and vocal about political decisions affecting food supply and border security in Texas (implied)
</details>
<details>
<summary>
2022-04-17: Let's talk about whether America needs a bad guy.... (<a href="https://youtube.com/watch?v=rasnv3yQzdU">watch</a> || <a href="/videos/2022/04/17/Lets_talk_about_whether_America_needs_a_bad_guy">transcript &amp; editable summary</a>)

America historically relied on external threats for unity, but shifting focus to common issues like climate change can lead to world peace and a brighter future.

</summary>

"We can fight climate change. We can try to reach the stars."
"Our energy should go to cleaning up this planet."
"That's how we achieve world peace."
"We stop being the bad guy."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

America historically needed an external threat to unite the people.
The idea of having a common enemy to rally against has been a unifying factor.
Americans are often motivated by fear, which drives unity in the face of a perceived threat.
Shifting the focus from external enemies to common issues like poverty and climate change is vital.
Fighting climate change and striving for a better world can be unifying causes.
Soldiers fight not out of hatred for the enemy but out of love for what they're protecting.
The concept of honor and battlefield glory can be redirected towards noble causes like environmental preservation.
Urges a shift from petty nationalism to global cooperation for the betterment of humanity.
Emphasizes the need to focus on improving living standards and making the world a better place.
Proposes that energy should be directed towards cleaning up the planet rather than fighting over territory.

Actions:

for global citizens, activists,
Fight climate change and advocate for environmental preservation (implied)
Focus on raising living standards and improving the world (implied)
Encourage global cooperation and unity towards common goals (implied)
</details>
<details>
<summary>
2022-04-17: Let's talk about the Air Force and influence.... (<a href="https://youtube.com/watch?v=v8AFGdLGmA4">watch</a> || <a href="/videos/2022/04/17/Lets_talk_about_the_Air_Force_and_influence">transcript &amp; editable summary</a>)

Beau explains the Air Force's proactive stance in supporting trans kids, focusing on positive impact and readiness issues, urging collaboration for progress despite criticisms.

</summary>

"If it would save the life of one trans kid, I would work with the devil himself."
"The world isn't binary. It's not black and white. It is super gray out there."
"Maybe you don't like the Air Force. But are your criticisms worth ignoring the fact that what they're going to do here is going to save trans kids' lives?"

### AI summary (High error rate! Edit errors on video page)

Explains the Air Force's decision and the announcement made in a memo.
Addresses misunderstandings about the relationship between central intelligence and the Department of Defense.
Illustrates a hypothetical scenario involving Walmart to showcase the positive influence of supporting trans kids.
Emphasizes the positive impact of the Air Force's actions in supporting trans individuals and families.
Points out the importance of the Air Force's proactive stance in addressing readiness issues.
Mentions the policy's tangible effects on the safety of trans kids at various Air Force bases.
Advocates for working with allies, even if not perfect, to achieve progress.
Raises the potential economic impact and political implications of the Air Force's actions.
Suggests drawing attention to the financial consequences for those supporting discriminatory laws.
Argues that criticizing the Air Force for other reasons shouldn't detract from acknowledging the lives saved by their policy.

Actions:

for advocates for trans rights,
Support organizations advocating for trans rights (implied)
Raise awareness about the positive impact of supporting trans individuals (implied)
Advocate for inclusive policies in your community (implied)
</details>
<details>
<summary>
2022-04-17: Let's talk about an update on Russia's economy.... (<a href="https://youtube.com/watch?v=Ntlp4YQfLHk">watch</a> || <a href="/videos/2022/04/17/Lets_talk_about_an_update_on_Russia_s_economy">transcript &amp; editable summary</a>)

Beau investigates the uncertain future of the Russian economy under sanctions, revealing potential defaults, creative accounting, and a looming contraction, with experts offering varied predictions.

</summary>

"The Russian economy has already taken hits, they're cooking the books and hiding it, and that eventually it's going to show itself."
"It tells us that we've got at least a couple of weeks before we're going to see anything major."
"It's unprecedented. It's never happened before."
"If they won't be noticed for months, the United States, NATO, they may be forced to pursue other options."
"These sanctions are designed to slow Russia's ability to prosecute the war, to engage in that conflict."

### AI summary (High error rate! Edit errors on video page)

Curiosity sparked an investigation into the Russian economy's performance under sanctions.
Despite reaching out to experts, there was no consensus on the future of the Russian economy.
Russia is currently in a grace period and may default on debt payments by the fourth of next month.
Predictions vary from a sudden economic nosedive to a gentler decline or sector-by-sector chaos.
Creative accounting measures may have masked the true impact of economic hits on Russia so far.
Some believe Russia can continue to manipulate their economic data for months to come.
The longer Russia delays facing economic realities, the worse the decline is expected to be.
The Russian economy is projected to contract by 10%, the largest contraction ever in the Russian Federation.
The uncertainty surrounding the impact of sanctions on Russia's economy is unprecedented.
Sanctions aim to hinder Russia's ability to wage war, but their immediate effects remain unclear.

Actions:

for economists, policymakers, activists,
Monitor the situation closely for any signs of economic instability (implied).
Stay informed about developments in the Russian economy and the impact of sanctions (implied).
</details>
<details>
<summary>
2022-04-16: Let's talk about why we broadcast NATO shipments.... (<a href="https://youtube.com/watch?v=A-enw0Fmvdc">watch</a> || <a href="/videos/2022/04/16/Lets_talk_about_why_we_broadcast_NATO_shipments">transcript &amp; editable summary</a>)

Beau reveals the hidden aspects of military aid to Ukraine, shedding light on the political messaging behind public announcements and the importance of questioning information sources.

</summary>

"Every war has an information war."
"The first casualty in any war is the truth."
"Just understand there's a whole bunch of stuff that's going to go on behind the scenes that we won't know about for 10 years."

### AI summary (High error rate! Edit errors on video page)

Military aid to Ukraine is not fully disclosed in public announcements; there are undisclosed components accompanying the disclosed weapons.
Publicizing military aid serves political purposes for the sending country, aiming to showcase support and commitment.
Some items, like radios or personal equipment, are not always listed in disclosed shipments but are still sent.
The disclosed armed shipments are somewhat transparent as Russia will eventually learn about them.
Publicizing aid can be a power move, showing that the aid will reach Ukraine regardless of Russia's awareness.
Information released in press releases or speeches is not always intended for the public; it can be part of a broader messaging strategy.
Not all information shared is meant for domestic consumption; sometimes, it's directed at foreign audiences.
Every country has figures who make extreme statements, but they may not represent the general population's sentiments.
Media clips can skew perceptions; it's vital to understand the context and messaging behind statements made by public figures.
In times of war, there is an information war, and not all information released should be taken at face value.

Actions:

for policy analysts, activists,
Question official narratives and seek alternative sources of information (suggested)
Stay critical of media coverage and public statements during times of conflict (suggested)
</details>
<details>
<summary>
2022-04-16: Let's talk about shifting demographics.... (<a href="https://youtube.com/watch?v=-AkedaUBPsk">watch</a> || <a href="/videos/2022/04/16/Lets_talk_about_shifting_demographics">transcript &amp; editable summary</a>)

Addressing shifting demographics and acceptance, Beau compares the growth of trans people to historical acceptance of left-handed individuals, stressing the importance of allowing individuals to be true to themselves.

</summary>

"The more accepting we are, the more of them exist."
"I don't see it as a sign of it being some plot to program us into something."
"Be more concerned about your own life than kicking down at somebody else for living theirs."

### AI summary (High error rate! Edit errors on video page)

Addressing shifting demographics and how acceptance impacts growth.
Comparing acceptance of trans people to historical acceptance of left-handed individuals.
Explaining the historical stigma and suppression of being left-handed.
Noting the increase in left-handed individuals as society became more accepting.
Sharing statistics on the percentage of left-handed individuals over time.
Emphasizing that acceptance allows individuals to be true to themselves.
Rejecting the idea that acceptance is part of a harmful agenda.
Encouraging a focus on personal life rather than criticizing others' choices.

Actions:

for those promoting acceptance and understanding.,
Support and advocate for the acceptance and rights of all individuals (implied).
</details>
<details>
<summary>
2022-04-16: Let's talk about Patrick Lyoya in Grand Rapids.... (<a href="https://youtube.com/watch?v=2ZOK1xJeSmw">watch</a> || <a href="/videos/2022/04/16/Lets_talk_about_Patrick_Lyoya_in_Grand_Rapids">transcript &amp; editable summary</a>)

Beau analyzes a police shooting in Grand Rapids, questioning the escalation and potential justifications in a system that often favors legal technicalities over common sense and best practices.

</summary>

"What's going to happen is the argument is going to be made that Patrick was trying to get that taser so he could then turn it on the officer, use it, and get his gun."
"There is a huge gap between what's best practices, what policy says, what training dictates, what common sense says, and what the law allows."
"But it's what currently exists."
"Even if there's a whole video showing that he has no intention of using violence."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analyzing a police shooting in Grand Rapids involving the driver Patrick Leoia and a cop.
Communication issues arise from the beginning of the interaction.
Officer grabs Patrick from behind, initiating violence.
Patrick breaks free and runs instead of fighting the cop.
Cop escalates the situation by chasing Patrick instead of waiting for backup.
Commands like "don't" and "stop" are ineffective in high-stress situations.
Officer shoots Patrick in the base of the skull during a struggle.
The officer's potential justification for the shooting may focus on fear of harm due to a taser threat.
Beau questions the reasonableness of fearing someone who repeatedly tries to run away.
The legal system's focus on what the law allows may override common sense and best practices.

Actions:

for community members, activists,
Contact local officials to demand accountability for police actions (exemplified)
Organize community meetings to address police violence and advocate for policy changes (exemplified)
</details>
<details>
<summary>
2022-04-15: Let's talk about the Moskva.... (<a href="https://youtube.com/watch?v=pDEF_X65cZ8">watch</a> || <a href="/videos/2022/04/15/Lets_talk_about_the_Moskva">transcript &amp; editable summary</a>)

The sinking of the Moskva, a Russian warship, by Ukraine marks a historic naval event with implications for morale and leadership on both sides.

</summary>

"This is kind of like getting fired on your day off."
"It is incredibly unlikely that all of them were currently above water."
"The loss of this ship will weigh heavily in the mind of Putin."
"This is the ship that was involved in the Snake Island thing."
"I think its impact on Putin is going to be more important than its impact on the average soldier."

### AI summary (High error rate! Edit errors on video page)

The Moskva, a Russian warship and flagship of the Black Sea Fleet, now rests off the coast of Ukraine after sinking.
The Ukrainian military claims they sank the Moskva, while the Russian military attributes its sinking to spontaneous combustion.
The ship, originally named Slava, underwent costly refits, with the latest renaming it the Snake Island Memorial Reef in 2022.
This event marks a historic moment in modern naval history as the flagship of a fleet was sunk by a country without an operational Navy.
The loss of the Moskva is significant due to its high value, substantial refits, and planned service until 2040.
The impact stretches beyond monetary loss to include morale and the challenge of replacing such a valuable asset.
While jokes are inevitable, it's vital to recognize the human cost, with around 500 sailors unlikely to have survived.
The sinking may not change much on the ground but could make Russian ships cautious near Ukrainian shores.
The loss of their flagship to a country without an operational Navy could have a profound impact on Russian morale and leadership.
This event presents numerous PR opportunities for Ukraine and may boost morale among Ukrainians involved in the Snake Island incident.

Actions:

for military analysts, naval historians,
Monitor and analyze the geopolitical implications of the Moskva sinking (suggested)
Support efforts to boost morale among Ukrainian forces (exemplified)
</details>
<details>
<summary>
2022-04-15: Let's talk about a BBC analysis of Russian personnel.... (<a href="https://youtube.com/watch?v=zbsC5zDVyfU">watch</a> || <a href="/videos/2022/04/15/Lets_talk_about_a_BBC_analysis_of_Russian_personnel">transcript &amp; editable summary</a>)

Analyzing Russian military losses in Ukraine reveals significant officer casualties, impacting future military operations and potentially causing discontent at home.

</summary>

"You can't lose that many officers."
"Being lied to about it, that's something else."
"You're going to have trouble executing the maneuvers they're going to need."

### AI summary (High error rate! Edit errors on video page)

Analyzing a BBC report in Russian about Russian military losses in Ukraine.
Russia admits to losing 1351 people, but estimates suggest it's over 10,000.
BBC identified 1083 of the lost individuals.
High percentage of officers among the identified losses, indicating an inaccurate count.
Paratroopers and special operations forces are among the casualties, affecting the professional corps.
Most of the losses come from economically depressed areas.
Moscow shows no losses, suggesting hiding of real numbers.
The undercounting strategy may backfire as the war continues and families seek answers.
Leadership drain and lack of experienced troops will impact Russia's military capability.
Paratroopers and special operations forces are dwindling due to casualties.
Pressure will mount on Putin to explain the missing troops, leading to potential discontent back home.

Actions:

for military analysts, policymakers,
Contact military families for support and information (exemplified)
Join organizations advocating for transparency in military reporting (suggested)
Organize community events to raise awareness about the impact of war (suggested)
</details>
<details>
<summary>
2022-04-15: Let's talk about Republicans refusing to debate.... (<a href="https://youtube.com/watch?v=MXt8D6N8WyQ">watch</a> || <a href="/videos/2022/04/15/Lets_talk_about_Republicans_refusing_to_debate">transcript &amp; editable summary</a>)

Republican Party's debate boycott offers Democrats a golden chance to win by inviting Libertarians for meaningful dialogues and attracting undecided voters.

</summary>

"Let them get on Fox News and rant and rave to their base all they want."
"If you're going to run a series like that you need to keep the videos like three to twelve minutes."
"The party that might have policy ideas."
"This is a golden opportunity as long as the Democratic Party doesn't waste it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Republican Party voted unanimously to not participate in the 2024 presidential debates, handing Democrats a golden chance to win.
Recommends Democrats invite the Libertarian Party to debates to attract more viewers and sway undecided voters.
Suggests that Republicans want to avoid being fact-checked, lie without repercussions, and focus on culture war instead of policy.
Points out that if Libertarian Party gains even a small percentage of votes, Republicans will suffer long-term damage.
Advises Democrats to create good TV with back-and-forth dialogues, directing conservative independent voters towards Libertarians.
Emphasizes the importance of seizing this rare moment and not providing first aid to the self-inflicted wound of the Republican Party.

Actions:

for politically engaged citizens,
Invite the Libertarian Party to participate in political debates (suggested)
Encourage viewership of dialogues between Democratic and Libertarian parties (implied)
Direct conservative independent voters towards the Libertarian Party (implied)
</details>
<details>
<summary>
2022-04-14: Let's talk about the Air Force sending a message.... (<a href="https://youtube.com/watch?v=2jkxnriQvKg">watch</a> || <a href="/videos/2022/04/14/Lets_talk_about_the_Air_Force_sending_a_message">transcript &amp; editable summary</a>)

The Air Force's warning about potential base closures due to discriminatory laws serves as a wake-up call for residents to pay attention before economic stability is at risk.

</summary>

"If you live in one of those states that has passed this ridiculous legislation, you need to pay attention."
"You had better start paying attention, like your checkbook depends on it, because it does."
"The number of people that have to be moved depends entirely on whether or not these ridiculous laws impact readiness."
"You're not going to get a warning beyond this."
"If the politicians in your state don't see that, you probably should have elected smarter politicians."

### AI summary (High error rate! Edit errors on video page)

The Department of Defense (DOD) is known for being on the cutting edge of social acceptance historically, desegregating before the rest of the country on many issues.
Many states have passed bigoted anti-LGBTQ legislation, prompting the Air Force to offer medical and legal aid to personnel affected by these laws.
The Air Force is willing to transfer personnel to different bases outside of states with discriminatory laws, indicating a potential impact on readiness.
The Department of Defense is apolitical but focuses on the needs of the Air Force, suggesting that bases may cease to exist if they cannot fulfill their functions due to personnel relocation.
Beau warns residents in states with discriminatory laws, such as South Florida or the Panhandle, about the possible economic consequences if bases like McDeal or Eglin shut down.
Politicians pushing discriminatory laws are criticized for diverting attention from their lack of policy ideas and job performance, potentially causing economic instability in affected areas.
Residents in states with discriminatory laws are urged to pay attention as the warning from the Air Force indicates potential mass transfers of personnel and negative impacts on economic activity.
Failure of politicians to recognize the warning may imply the need for smarter political choices to avoid drastic consequences.

Actions:

for residents in states with discriminatory laws,
Pay attention to the impact of discriminatory laws and be prepared for potential economic consequences (implied).
Advocate for smarter political choices to avoid negative outcomes (implied).
</details>
<details>
<summary>
2022-04-14: Let's talk about Ukrainians in Melitopol.... (<a href="https://youtube.com/watch?v=xqf-vKXPLN4">watch</a> || <a href="/videos/2022/04/14/Lets_talk_about_Ukrainians_in_Melitopol">transcript &amp; editable summary</a>)

Recent focus on expected Russian advance overlooks Ukrainian resistance's effective low-level harassment, which undermines Russian military authority and may change the course of the conflict.

</summary>

"Never say I'll be right back because you won't."
"They're signing on for a very long, very nasty conflict."
"These types of resistance operations change the course of wars."
"Going after civilians as punishment never works."
"It always strengthens the resistance."

### AI summary (High error rate! Edit errors on video page)

Recent focus on expected Russian advance in the East overshadows news from other areas.
Maritopol reports over 70 Russian troop casualties in three weeks to Ukrainian resistance.
Ukrainian resistance likely comprises well-organized or semi-organized groups conducting surveillance and operations at night.
Resistance groups are becoming better armed by keeping equipment from fallen troops.
Low-level harassment by the resistance undermines Russian military authority.
Russia must acknowledge and address the losses to avoid losing the city.
Options for Russia include ceding the night to resistance or doubling up patrols.
Russian troops live in constant fear and vulnerability, akin to a horror movie.
Continuous losses are unsustainable for the Russian military.
Resistance operations behind Russian lines create paranoia, demoralization, and logistical challenges.

Actions:

for military analysts, policymakers, activists,
Support Ukrainian resistance efforts through aid and resources (implied)
Raise awareness about the impact of resistance operations on the conflict (suggested)
</details>
<details>
<summary>
2022-04-14: Let's talk about Putin purging planners in the Fifth Service.... (<a href="https://youtube.com/watch?v=liCWOXetZFk">watch</a> || <a href="/videos/2022/04/14/Lets_talk_about_Putin_purging_planners_in_the_Fifth_Service">transcript &amp; editable summary</a>)

Putin's purge of the Fifth Service reveals alleged fabrications of reports on non-existent resistance groups in Ukraine, impacting Russia's war planning and credibility.

</summary>

"They were paying off sources that didn't exist and just pocketing the cash."
"A lot of their planning starts to make a whole lot more sense if you factor in the existence of a partisan group loyal to Russia operating inside Ukraine."
"It looks like corruption and greed undermined a war effort, a war effort that probably would not have occurred if those reports had never been made."

### AI summary (High error rate! Edit errors on video page)

The Fifth Service was created by Putin in 1998 to conduct intelligence operations in former Soviet republics and maintain Russia's influence there.
Putin is currently purging the Fifth Service, with the boss being placed under house arrest and around 150 officers being dismissed due to the invasion of Ukraine.
The officers were allegedly fabricating reports about funding non-existent resistance groups in Ukraine and pocketing the money.
This behavior hindered Russia's war planning and may have led to intelligence failures in the invasion of Ukraine.
While the rumor about fake sources and resistance groups cannot be verified, it explains a lot of the questionable decisions made by Russia during the war.
The purge of the Fifth Service could be a liability for Russia as they lose experienced intelligence officers familiar with the region.
The disciplinary actions taken could lead to micromanagement of new officers and potential paranoia within the intelligence agency.
Putin might be taking the purge personally, as the Fifth Service was his creation and its failures are seen as a betrayal of his vision.
The rumor about fake reports and corruption within the Fifth Service, if true, could have significantly impacted Russia's actions in the war.
Beau believes that the speculation about fake reports and resistance groups is plausible, even though it cannot be proven.

Actions:

for intelligence analysts, policymakers,
Investigate and monitor the geopolitical implications of intelligence agency purges (suggested)
Support transparency and accountability within intelligence agencies (suggested)
Advocate for ethical practices and oversight in intelligence operations (suggested)
</details>
<details>
<summary>
2022-04-13: Let's talk about the end to the age of the tank.... (<a href="https://youtube.com/watch?v=DPOSD5raqJU">watch</a> || <a href="/videos/2022/04/13/Lets_talk_about_the_end_to_the_age_of_the_tank">transcript &amp; editable summary</a>)

The age of the tank is changing, moving towards unmanned vehicles, as anti-tank munitions advance and drones reshape conflict dynamics.

</summary>

"We're moving to an age of unmanned vehicles."
"In some ways, the tank as we know it is going the way of the battleship."
"It's changing. It's going to look different. It's going to be deployed differently."
"It's not an end to that type of equipment. It's just shifting to a newer version of it."

### AI summary (High error rate! Edit errors on video page)

Explains the current status of tanks and addresses the question of whether the age of the tank is over.
Mentions that the end of the tank era is not primarily due to the conflict in Ukraine.
Talks about the poor functionality and survivability of Russian tanks, attributing it to improper deployment and advances in anti-tank munitions.
Points out that anti-tank rockets have advanced far beyond the capabilities of many tanks worldwide.
Emphasizes the importance of deploying tanks properly, including having infantry screens.
Notes that it's now more cost-effective to counter tanks with infantry-portable rockets and missiles rather than with other tanks.
Mentions the changing landscape of conflict due to advances in anti-tank munitions and drone technology.
Foresees the rise of unmanned tanks in the future, which will be remote-controlled and different in appearance.
Compares the trajectory of tanks to battleships, stating that they are becoming easier targets in today's world.
Predicts that tank arsenals worldwide will be phased out in the next 25 years, replaced by unmanned vehicles.

Actions:

for military analysts, defense enthusiasts.,
Stay informed about advancements in military technology and their implications (implied).
</details>
<details>
<summary>
2022-04-13: Let's talk about Russia's long-term futures.... (<a href="https://youtube.com/watch?v=CwRZRYl-yPc">watch</a> || <a href="/videos/2022/04/13/Lets_talk_about_Russia_s_long-term_futures">transcript &amp; editable summary</a>)

Russia's invasion of Ukraine exposes military weakness, leading to long-term repercussions and limited future prospects, urging acceptance of a diminished role in the global order to achieve stability.

</summary>

"This fight that it picked has set Russia back on the geopolitical stage a lot, and they're going to end up paying for it for a very, very long time."
"The invasion aimed to exert Russian influence and serve as a warning to other countries. That didn't work."
"The sooner Russia accepts this reality, the better it's going to be for everybody, Russians included."

### AI summary (High error rate! Edit errors on video page)

Russia's invasion of Ukraine displayed military weakness, impacting its long-term prospects.
The invasion aimed to assert Russian influence in Eastern Europe but backfired.
Countries like Finland and Sweden are considering joining NATO due to Russia's actions, weakening Russia's stance in Europe.
Economic and diplomatic penalties from the invasion will have lasting effects on Russia.
Putin's dream of restoring Russia's glory like the USSR is undermined.
Russia's income inequality, corruption, bad demographics, and reliance on dirty energy were existing issues.
Options for Russia's future include accepting a secondary role to China, internal change by removing Putin, turning to former Soviet republics, or adopting isolationism.
Beau hopes Russia modernizes and accepts its diminished role in the global order.
Russia will not be a military center of an alliance, overshadowed by China and viewed as relatively weak despite nuclear weapons.
Accepting this reality sooner will benefit everyone and lead to a more stable world.

Actions:

for global policymakers,
Russians: Remove Putin and his cronies from power to modernize and potentially reemerge as a world power (implied).
</details>
<details>
<summary>
2022-04-13: Let's talk about Betty White's weird connection to Thomas Jefferson.... (<a href="https://youtube.com/watch?v=AEroqxcuY40">watch</a> || <a href="/videos/2022/04/13/Lets_talk_about_Betty_White_s_weird_connection_to_Thomas_Jefferson">transcript &amp; editable summary</a>)

Beau explains the proximity of historical events to challenge the perception of history as ancient, urging active participation in shaping future history.

</summary>

"It really wasn't that long ago."
"We like to pretend like all of that horrible stuff happened forever ago."
"Just a couple of lives separates the time of Thomas Jefferson from the time of your students."

### AI summary (High error rate! Edit errors on video page)

Explains how Americans, as a young country, tend to view history as ancient and far removed.
Shares a personal anecdote of meeting people who were alive in the 1800s to illustrate the proximity of historical events.
Breaks down the timeline between Thomas Jefferson, Harriet Tubman, Ronald Reagan, and Betty White to show how recent historical figures overlap.
Emphasizes how close America's troubling past, like segregation, is to the present.
Encourages history teachers to put history in perspective for students to understand the impact and relevance of past events.
Suggests that understanding the overlap between historical figures' lives can spark curiosity and interest in history.
Urges viewers to acknowledge how recent historical events are and to actively participate in shaping future history.

Actions:

for history teachers,
Challenge students to research and understand the overlaps between the lives of historical figures (suggested).
Encourage students to actively participate in shaping future history by learning from the past (implied).
</details>
<details>
<summary>
2022-04-12: Let's talk about questions on Russia's failure.... (<a href="https://youtube.com/watch?v=EVPIOwmWNpE">watch</a> || <a href="/videos/2022/04/12/Lets_talk_about_questions_on_Russia_s_failure">transcript &amp; editable summary</a>)

Addressing misconceptions about Russia's capabilities and strategies in the Ukraine conflict reveals incompetence over complex strategies.

</summary>

"Don't look for a hidden strategy where none exists."
"Never attribute to conspiracy what you can attribute to incompetence."
"Anything trying to explain away their failures is probably rooted in that."

### AI summary (High error rate! Edit errors on video page)

Addressing the misconceptions surrounding Russia's capabilities and strategies in the current conflict in Ukraine.
Russia's GDP of $ 1.5 trillion does not make them an economic superpower, limiting their military capabilities.
Misinterpretation led to incorrect estimations about Ukraine, causing errors in judgment for both Russia and Western analysts.
Russia's failed invasion of Ukraine was not part of some grand strategy but rather a miscalculation of their actual capabilities.
Russia's lack of success in taking Ukraine's capital indicates a strategic failure rather than a deliberate diversion tactic.
Russia's military shortcomings, such as the absence of infantry screens, have contributed to their struggles in the conflict.
The belief that Russia could easily target Ukrainian President Zelensky is unrealistic due to their lack of specific technological capabilities.
Russia's decision to not utilize its full military strength in the invasion is not a strategic move but rather a precaution against a potential NATO counterattack.
Despite holding back troops for other concerns, Russia's miscalculations have been evident throughout the conflict.
The conflict in Ukraine has exposed Russia's incompetence rather than any secretive or elaborate strategy.

Actions:

for analysts, policymakers, general public,
Support Ukraine by providing the necessary assistance to combat Russia's invasion (implied).
Stay informed about the conflict and challenge misconceptions surrounding Russia's military capabilities (implied).
</details>
<details>
<summary>
2022-04-12: Let's talk about countering misinformation sources.... (<a href="https://youtube.com/watch?v=d2RtIxNdw-U">watch</a> || <a href="/videos/2022/04/12/Lets_talk_about_countering_misinformation_sources">transcript &amp; editable summary</a>)

Beau addresses countering misinformation by proactively undermining sources during subject shifts, prompting critical thinking and reducing the spread of falsehoods.

</summary>

"Every piece of misinformation is replaced by two the moment you squash it out."
"You have a breather for a second."
"So you have these people who buy into this talking point."
"You start to train them to fact-check the people that are doing their research for them."
"If you can catch them saying something with authority and it be just flat wrong, it tends to have a pretty marked effect."

### AI summary (High error rate! Edit errors on video page)

Beau is addressing the challenge of countering misinformation and stopping it before it spreads, especially when dealing with someone insistent on pushing false narratives onto others.
Beau's mother's friend is trying to convert Beau's mom into a "do your own research" mindset, leading Beau to question how to handle the situation.
Despite efforts to debunk misinformation, Beau feels like for every piece squashed, two more emerge, making it challenging to maintain the effort.
Beau contemplates whether to continue engaging with the friend or find new strategies as they seem unable to convince the person.
Beau suggests a proactive approach by catching misinformation sources off guard during subject shifts, exploiting their learning curve and tendency to make wild mistakes.
By undermining the credibility of information sources and prompting critical thinking, Beau believes it is possible to reduce the spread of misinformation and train individuals to fact-check.
Success in combating misinformation lies in seizing moments of vulnerability in sources and exposing their inaccuracies effectively, especially when the misinformation is recent and actively being spread.
The key is to confront misinformation with irrefutable facts, prompting those spreading lies to question their sources and leading to a reduction in falsehoods being disseminated.
Beau acknowledges the effectiveness of challenging misinformation with authority and correcting false statements to make a lasting impact on those propagating misinformation.
Beau shares a strategy that has proven successful for them, advising others to capitalize on opportunities to debunk misinformation when sources are least prepared, thereby weakening their influence.

Actions:

for friends combating misinformation.,
Challenge misinformation sources during subject shifts (implied).
</details>
<details>
<summary>
2022-04-12: Let's talk about South Dakota and an impeachment.... (<a href="https://youtube.com/watch?v=NX4bhLL1ZeU">watch</a> || <a href="/videos/2022/04/12/Lets_talk_about_South_Dakota_and_an_impeachment">transcript &amp; editable summary</a>)

Attorney General Jason Roundsburg faces possible impeachment in South Carolina over a 2020 incident involving a pedestrian, with state troopers' evidence casting doubt on his narrative and leading to a vote on Tuesday.

</summary>

"State troopers are skeptical of his chain of events there."
"It looks like the vote as to whether or not to pursue the impeachment will take place on Tuesday."

### AI summary (High error rate! Edit errors on video page)

Attorney General of South Carolina, Jason Roundsburg, involved in a possible impeachment due to conduct in 2020.
Roundsburg hit a pedestrian while driving, claimed he didn't know what he hit, and reported it to 911 before going home.
State troopers indicate the pedestrian was on the hood of the vehicle for about a hundred feet, raising skepticism about Roundsburg's account.
Roundsburg settled privately with the pedestrian's widow and pled no contest to misdemeanor crimes.
Some legislators plan to pursue impeachment despite a select committee finding that Roundsburg's conduct did not warrant it.
Roundsburg, supported by Trump, is likely to run for re-election despite the controversy.
State troopers' evidence cast doubt on Roundsburg's narrative, potentially driving the impeachment effort.
Vote on impeachment likely to occur on Tuesday.
Roundsburg's decision to run for re-election despite the scandal might be fueling the impeachment push.
Political scandal in state politics usually resolves quicker, making Roundsburg's situation unique.

Actions:

for voters, legislators, community.,
Contact your legislators to express support or opposition to the impeachment proceedings (suggested).
Stay informed about the developments and outcome of the impeachment vote (implied).
</details>
<details>
<summary>
2022-04-11: Let's talk about sanctions and Putin's cushion.... (<a href="https://youtube.com/watch?v=XCuKldzsClc">watch</a> || <a href="/videos/2022/04/11/Lets_talk_about_sanctions_and_Putin_s_cushion">transcript &amp; editable summary</a>)

Beau provides insights on sanctions, showcasing their impact on Russia's economy and military capabilities, underscoring how they limit force projection and hurt civilians.

</summary>

"It's not like it's going to stop the war by itself, but what it does is it stops Russia's ability to widen the conflict because they don't have the money."
"My concern with them is that it always hurts the little people along the way."
"Power coupons. What's foreign policy about? Not right and wrong, not good and evil. Power."
"Just first put them into perspective. And then, there isn't really a situation in which Russia has the economic power to outlast NATO."
"The sanctions stop countries from getting more power coupons to use on the international scene and it pretty much always works."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of sanctions and their purpose in the context of the current situation.
Provides a perspective on the impact of sanctions on Russia's economy and military capabilities.
Contrasts the significance of monetary amounts for individuals versus nation-states at war.
Points out Russia's lack of economic power compared to NATO.
Mentions the effects of sanctions on Russia's credit rating and economy.
Talks about how capital controls are being used by Russia to stabilize their economy artificially.
Emphasizes that sanctions aim to degrade Russia's ability to force project and widen the conflict.
Notes that sanctions limit a country's resources for military operations.
Predicts that over time, sanctions will make it increasingly difficult for Russia to sustain wartime production.
Raises concerns about how sanctions impact ordinary citizens while achieving their intended objectives.

Actions:

for global citizens, policymakers.,
Support organizations providing humanitarian aid to civilians affected by conflicts (implied).
Stay informed about the impact of sanctions and advocate for policies that minimize harm to vulnerable populations (suggested).
</details>
<details>
<summary>
2022-04-11: Let's talk about Trump Jr's texts and the committee's debate.... (<a href="https://youtube.com/watch?v=VXiR_PQhOJ8">watch</a> || <a href="/videos/2022/04/11/Lets_talk_about_Trump_Jr_s_texts_and_the_committee_s_debate">transcript &amp; editable summary</a>)

Beau provides insights into the committee's dilemma over referring Trump for prosecution, questioning the impact on public perception and politicians' underestimation of public discernment.

</summary>

"I don't think that a referral from the committee is going to sway those people one way or the other."
"I think one of the big mistakes that politicians make is constantly treating the American people as if they are incapable of discerning facts from fiction."

### AI summary (High error rate! Edit errors on video page)

Beau provides an overview of the latest developments in Trump world, focusing on the committee's decisions regarding former President Trump.
There is reporting on texts from Trump Jr.'s phone that suggest actions were planned before votes were counted, confirming suspicions.
The committee is reportedly divided on referring Trump to the DOJ for criminal prosecution, with Cheney denying the division but potential concerns about the move.
Concerns exist within the committee that referring Trump to the DOJ could politicize the parallel investigation already underway.
Beau argues that the evidence against Trump is substantial regardless of political motivations, suggesting that a committee referral may not sway opinions.
He believes that the majority of Americans will judge the evidence objectively and make their own conclusions, criticizing politicians for underestimating public discernment.
Beau questions whether the committee's decision not to refer Trump could lead to confusion among the public and a lack of transparency in informing them.

Actions:

for politically engaged citizens,
Contact your representatives to express your opinion on how the committee should handle referring Trump to the DOJ (suggested).
Stay informed about the developments in Trump world and hold politicians accountable for their actions (exemplified).
</details>
<details>
<summary>
2022-04-10: Let's talk about why the West is slow-walking the sanctions.... (<a href="https://youtube.com/watch?v=O-cqcWF_tys">watch</a> || <a href="/videos/2022/04/10/Lets_talk_about_why_the_West_is_slow-walking_the_sanctions">transcript &amp; editable summary</a>)

The West's slow sanction approach balances hurting Russia with economic stability, impacting the bottom before the powerful, waiting for Russia's economic slip.

</summary>

"While it hurts Russia for the West to sanction it, the West also needs stuff."
"Large-scale sanctions, they hit the people on the bottom first, and it takes time to trickle up."
"The Russian economy is not in a good way."

### AI summary (High error rate! Edit errors on video page)

The West is slow in imposing sanctions on Russia to strike a balance between hurting Russia and ensuring their own supply needs.
Sanctions are being slowly increased to find alternative sources and maintain economic stability.
The strategy is not just about being nice to average Russians but about keeping Western economies intact.
The West's ability to supply Ukraine with defense relies on maintaining their own economies.
The decision-making process behind imposing sanctions involves a mix of logical and cynical reasons.
Large-scale sanctions impact the bottom first and take time to affect those in power.
The Russian economy is struggling, resorting to tactics to sustain itself temporarily.
The West is waiting for a slip in Russia's economic shell game before exerting full pressure.
Slowly increasing pressure allows the West to maintain economic stability while sanctioning Russia.
Despite appearing slow, there are strategic reasons behind the gradual approach to sanctions.

Actions:

for global citizens,
Monitor the situation in Ukraine and Russia closely to understand the impact of sanctions (implied)
Support organizations providing aid to those affected by the conflict (implied)
</details>
<details>
<summary>
2022-04-10: Let's talk about hope and acceleration.... (<a href="https://youtube.com/watch?v=U9akT4JplzU">watch</a> || <a href="/videos/2022/04/10/Lets_talk_about_hope_and_acceleration">transcript &amp; editable summary</a>)

Beau addresses maintaining hope, navigating accelerationism, and building community networks to shape a fairer future amidst chaos and uncertainty.

</summary>

"The goal should be to stop that from happening."
"The same thing that can stop it is the thing that would lead to a better world if it failed to stop it."
"If you do that, and the system still collapses, climate change still just runs rampant, those power structures, those people who have connected in that way, who have built that network, they're going to be better positioned to ride it out."
"And if you do it well enough, we don't even have to go through a collapse."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing the importance of hope amidst chaos and adversity.
Questioning how to maintain hope and motivation in the face of increasing challenges.
Exploring the concept of accelerationism and its implications.
Emphasizing the necessity of considering what happens after potential collapse scenarios.
Advocating for building local community networks for effecting real change.
Warning against the assumption that good outcomes are guaranteed post-collapse.
Encouraging proactive efforts to create a power structure that benefits the average person.
Stressing the value of grassroots movements in shaping a fairer society.
Acknowledging the reluctance of many to take on leadership roles for fear of corruption.
Arguing that organizing and building networks now is key to preventing disastrous outcomes in the future.

Actions:

for community organizers, activists,
Build local community networks to affect change locally and create a foundation for broader impact (implied)
Organize grassroots movements for the betterment of people (implied)
Actively work towards delaying ideological framework that may lead to negative outcomes (implied)
</details>
<details>
<summary>
2022-04-09: Let's talk about the new DOJ referrals.... (<a href="https://youtube.com/watch?v=obwlGWbHhHI">watch</a> || <a href="/videos/2022/04/09/Lets_talk_about_the_new_DOJ_referrals">transcript &amp; editable summary</a>)

Beau manages expectations around January 6th committee referrals, expressing concerns over accountability delays and eroding public faith in the justice system.

</summary>

"The longer these indictments don't exist, the longer DOJ just doesn't follow up on the referral, doesn't issue a statement as to why they're not going to indict or a statement that they're going to indict something, the more people lose faith in that system."
"The longer this goes on without developments, the more Americans start to see that as what's happening."
"The longer things draw out, the longer this goes on without developments, the more Americans start to see that as what's happening."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the new referrals for contempt from the January 6th committee, managing expectations surrounding them.
The committee can refer individuals to the Department of Justice for indictment but cannot indict on its own.
Four people have been referred by the committee: Steve Bannon, Mark Meadows, Dan Scavino, and Peter Navarro.
Steve Bannon has been indicted and is going to trial, while the others have not faced indictments yet.
If indictments are delayed beyond the midterms, Republicans retaking the House could potentially squash the proceedings.
Lack of accountability and delays in indictments erode faith in the system and the prospect of justice for those involved in the January 6th events.
The Department of Justice's actions and investigations are not transparent, leading to uncertainty and skepticism among the public.
Beau notes that Attorney General Garland's approach is not focused on frequent press releases or updates, which may contribute to public frustration.
The longer the delays in accountability and indictments persist, the more it reinforces the perception that those in power are not held responsible for their actions.
Additional referrals from the committee may not hold significance if the Department of Justice does not proceed with indictments.

Actions:

for concerned citizens,
Stay informed about developments in the January 6th committee referrals (suggested).
Advocate for transparency and accountability in the Department of Justice's investigations (implied).
</details>
<details>
<summary>
2022-04-09: Let's talk about realization from a retweet.... (<a href="https://youtube.com/watch?v=Iixx6TQ7WCs">watch</a> || <a href="/videos/2022/04/09/Lets_talk_about_realization_from_a_retweet">transcript &amp; editable summary</a>)

Beau contemplates societal norms and empathy after a Twitter joke leads to a realization, opting to remain a positive voice despite differing views.

</summary>

"I don't want them to lose what could be the only positive voice they have on this topic in their information cycle."
"Maybe if it's something you wouldn't do yourself or you don't want to condone, maybe you still realize that at times people are going to step outside of societal norms, those that exist today, just to live."
"Because after all, they're just trying to live."

### AI summary (High error rate! Edit errors on video page)

Retweeted a joke on Twitter that led to a realization about societal norms.
Many people are discussing something he's believed in for a long time.
Saw a meme about his joke and retweeted it, only to later find out the creator opposes something he supports.
Contemplated removing the retweet but decided against it to remain a positive information source for the meme creator.
Reflected on the meme's meaning, which sheds light on societal pressures and norms.
Acknowledged the importance of not condemning people for stepping outside societal norms to live.
Encouraged understanding and empathy towards those who may not conform to societal expectations.

Actions:

for social media users,
Support and amplify positive voices on social media (implied)
</details>
<details>
<summary>
2022-04-09: Let's talk about new legislation and wildlife contests.... (<a href="https://youtube.com/watch?v=7BWwXJgYXL0">watch</a> || <a href="/videos/2022/04/09/Lets_talk_about_new_legislation_and_wildlife_contests">transcript &amp; editable summary</a>)

Beau explains why wildlife killing contests are more about image than skill, advocating for a ban on public lands to protect predators.

</summary>

"It's not about skill anymore. It has nothing to do with that."
"The winner of these things is really just getting a participation trophy."
"You could always shoot them with a camera if that's really what it was about."

### AI summary (High error rate! Edit errors on video page)

Introduces new federal legislation called the Prohibit Wildlife Killing Contests Act aimed at banning events where groups of people hunt predators like coyotes or foxes.
Points out that the legislation applies to public lands, protecting half a billion acres.
Mentions that these contests are not about conservation, but more about portraying an image of rugged mountain men.
Criticizes the lack of skill involved in these contests due to technology making it easy to hunt predators en masse.
Notes that winners of these contests merely receive a participation trophy and it's more about luck than skill.
States that eight states have already banned this practice and two more states have stopped holding such events.
Counters the argument that banning these contests on public lands will lead to increased hunting on private lands.
Suggests using a camera instead of a gun if the goal is to challenge oneself against smart animals.
Comments on knowing people who participated in these events and implies they could benefit from exploring their creative side.

Actions:

for conservationists, wildlife activists,
Advocate for wildlife protection laws (exemplified)
Encourage creative pursuits as an alternative to hunting contests (implied)
</details>
<details>
<summary>
2022-04-08: The roads to Ukraine.... (<a href="https://youtube.com/watch?v=E7AHt5_mQH4">watch</a> || <a href="/videos/2022/04/08/The_roads_to_Ukraine">transcript &amp; editable summary</a>)

Beau provides historical context on Ukraine, discussing major powers' interference, Ukrainian agency, and the ongoing fight against Russian influence.

</summary>

"Ukrainians do not want to be under the thumb of Moscow."
"The thing that's on loop is that Ukrainians do not want to be under the thumb of Moscow."
"That's why they're fighting the way they are."
"When you try to take that away, you are erasing them."
"Don't take that away from them because it doesn't fit your narrative."

### AI summary (High error rate! Edit errors on video page)

Provides historical context on the roads to Ukraine, going back to the Cold War and detailing events that shaped the country's current situation.
Describes how major powers like the U.S. and Russia conducted intelligence operations and interfered in Ukraine to further their interests.
Talks about the U.S. running a 25-year ad campaign in Ukraine to influence democratic values and Western interests.
Explains events like the Budapest memos, the Orange Revolution, and the Maidan protests that marked significant shifts in Ukrainian politics.
Analyzes Russia's actions, including annexing Crimea in violation of agreements, and Ukraine's response to resist Russian influence.
Addresses misconceptions about the Maidan protests being labeled as a U.S. coup and stresses the agency of the Ukrainian people in shaping their own destiny.
Emphasizes Ukraine's fight against Russian invasion and interference, showcasing their determination and resilience.

Actions:

for history enthusiasts, geopolitics followers.,
Support Ukrainian communities (suggested)
Educate yourself on Ukrainian history and current events (implied)
Advocate for international support for Ukraine's sovereignty (implied)
</details>
<details>
<summary>
2022-04-08: Let's talk about how Putin is saving the Republican party from itself.... (<a href="https://youtube.com/watch?v=0Os1BYw1s_E">watch</a> || <a href="/videos/2022/04/08/Lets_talk_about_how_Putin_is_saving_the_Republican_party_from_itself">transcript &amp; editable summary</a>)

Putin's actions in Ukraine have pushed the Republican Party to distance itself from authoritarian tendencies, leading to a realignment with traditional American values.

</summary>

"Putin kind of saved the Republican Party from itself."
"The authoritarian right-wing mindset is going to be upended."
"Now that's the bad guy."
"They have to walk back that authoritarian rhetoric."
"The votes are a very obvious sign when you have nobody in the Senate on the Republican side of the aisle willing to say no on something that they know is going to pass anyway."

### AI summary (High error rate! Edit errors on video page)

Putin's invasion of Ukraine may have inadvertently pushed the Republican Party to distance itself from authoritarian tendencies.
The Republican Party previously focused on demonizing China, which attracted supporters with xenophobic tendencies and authoritarian leanings.
However, Putin's actions in Ukraine shifted the American consciousness, making him the new enemy in the eyes of the public.
Republicans found themselves supporting Putin-aligned authoritarian leaders, which became politically inconvenient after the shift in perception.
US support for Ukraine led to Republicans voting against Russian interests, such as reinstating the Lindley Act and banning Russian energy.
The new Cold War is framed as democracy versus authoritarianism, not democracy versus communism as previously assumed.
Republicans are now compelled to backtrack on their previous pro-authoritarian stances to realign with traditional American values.
The normal Republican voter may overlook certain issues but openly supporting Putin's allies goes against US military interests.
Politicians within the Republican Party are likely to retract their authoritarian rhetoric to maintain their political base and image.
Putin inadvertently forced the Republican Party to reexamine and adjust its stance on authoritarianism for political survival.

Actions:

for political observers,
Support political candidates who prioritize democratic values and denounce authoritarianism (implied)
Stay informed about foreign policy decisions and their implications on domestic politics (implied)
</details>
<details>
<summary>
2022-04-08: Let's talk about Ukraine developments and Russian reservists.... (<a href="https://youtube.com/watch?v=s-Fff7WP80A">watch</a> || <a href="/videos/2022/04/08/Lets_talk_about_Ukraine_developments_and_Russian_reservists">transcript &amp; editable summary</a>)

Beau reveals Russia's plan to call up reservists, indicating potential trouble and a prolonged conflict in Ukraine.

</summary>

"Russia is planning to call up people who left military service after 2012 for a three-month training before deployment."
"Russian reservists are not trained like American reservists and will receive minimal training before deployment."
"Russia seems determined to continue the conflict and throw more people into the conflict until a resolution is reached."

### AI summary (High error rate! Edit errors on video page)

Ukraine's situation on the ground is being discussed.
A Russian advance from the area around Izyum has begun, but it hasn't shifted the lines much.
Ukrainian forces are holding the advance in place, causing problems for the Russian military.
There is a structural issue within the Russian military where officers are expected to follow orders without operational latitude.
Russia is calling up reservists, indicating they may be in more trouble than previously thought.
Russia is planning to call up people who left military service after 2012 for a three-month training before deployment.
This move suggests that Russia is psychologically preparing for a prolonged conflict and facing significant losses.
Russian reservists are not trained like American reservists and will receive minimal training before deployment.
The losses for Russia may be much higher than what is being reported.
Russia seems determined to continue the conflict and throw more people into the conflict until a resolution is reached.

Actions:

for military analysts, policymakers,
Monitor the situation in Ukraine closely and provide support to affected communities (implied)
Stay informed about developments in the conflict and advocate for diplomatic resolutions (implied)
</details>
<details>
<summary>
2022-04-08: Let's talk about Trump's really bad day.... (<a href="https://youtube.com/watch?v=FaZ9G2A2SqE">watch</a> || <a href="/videos/2022/04/08/Lets_talk_about_Trump_s_really_bad_day">transcript &amp; editable summary</a>)

Trump faced a series of significant losses, including DOJ investigations, fines, and Republican senators voting against Russia, indicating a potential shift away from his influence within the party.

</summary>

"It was a bad day. It is a bad sign for his political future."
"But $10,000 a day adds up really quick."
"That severely undercuts Trump."
"The Department of Justice announced that it was looking into the trove of classified documents."
"The unanimous votes against Russia are votes against Trump."

### AI summary (High error rate! Edit errors on video page)

Trump had a bad day yesterday with a series of losses that could impact his political future.
The Department of Justice is investigating classified documents found at Trump's golf course, a significant development.
Trump could potentially face fines of $10,000 a day for contempt of court for not turning over documents.
The investigation into the Trump Organization is still ongoing despite previous expectations of its conclusion.
Republicans in the Senate unanimously voted against Russia, signaling a shift away from Trump's brand of authoritarianism.
Mitch McConnell, a key Republican figure, announced support for Murkowski, undermining Trump's influence within the party.
The Republican Party's backing of Murkowski indicates a significant blow to Trump's political standing.

Actions:

for political analysts, activists,
Contact political representatives to express support or opposition to their decisions (suggested)
Stay informed about ongoing political developments and their implications (implied)
</details>
<details>
<summary>
2022-04-07: Let's talk about the US signing up to back Ukraine throughout the war.... (<a href="https://youtube.com/watch?v=x8Sf6U7HseU">watch</a> || <a href="/videos/2022/04/07/Lets_talk_about_the_US_signing_up_to_back_Ukraine_throughout_the_war">transcript &amp; editable summary</a>)

The United States commits to backing Ukraine throughout the conflict, with implications of a protracted and escalating situation. NATO members might follow suit.

</summary>

"The United States is signing on to supply Ukraine throughout the entire war."
"It is going to get bad."
"The United States has signed on to back Ukraine throughout the entire thing."

### AI summary (High error rate! Edit errors on video page)

The United States is in the process of signing on to back Ukraine throughout the entire conflict.
The Senate has already unanimously approved the Lend-Lease Act, allowing the U.S. to lend or lease defense articles to Ukraine.
The agreement with Ukraine is not subject to the typical stipulation that equipment lent must be returned within five years.
The U.S. commitment means supplying Ukraine throughout the entire war.
This commitment puts the United States' production capability against Russia's sanctioned production capability.
The Senate version implies support for Ukraine until Crimea is recovered.
Russia seems prepared for a multi-year engagement in Ukraine.
The conflict is likely to become protracted and escalate in the east.
The situation is expected to worsen significantly.
The United States will stand with Ukraine through it all.

Actions:

for politically active individuals.,
Contact your representatives to express support for continued U.S. assistance to Ukraine (suggested).
Stay updated on the situation in Ukraine and advocate for sustained international support (implied).
</details>
<details>
<summary>
2022-04-07: Let's talk about West Virginia, solar, and stranded assets.... (<a href="https://youtube.com/watch?v=H8ZUoqfkxfU">watch</a> || <a href="/videos/2022/04/07/Lets_talk_about_West_Virginia_solar_and_stranded_assets">transcript &amp; editable summary</a>)

West Virginia transitions from coal to solar, signaling the rise of stranded assets in the shifting energy sector and urging investment reconsideration towards cleaner energy.

</summary>

"West Virginia is getting a 3,000 acre solar panel farm. 3,000 acres, that is huge!"
"As the world moves away from dirty energy, a whole lot of stuff isn't going to have the value it used to."
"The state just has to keep up with times, and it looks like West Virginia is at least going to make the attempt to do that."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

West Virginia is getting a massive 3,000 acre solar panel farm built on top of an old coal mine.
The energy sector is facing the issue of stranded assets as the world shifts away from dirty energy sources.
Stranded assets occur when investments made in an asset no longer produce the expected returns due to changes in the market.
Companies may face liabilities if the value of their assets drops significantly, making them worth less than what was spent on them.
As the world moves away from coal and oil, the demand for these products decreases, leading to assets becoming stranded.
Personal investments in solar panels can also impact traditional energy companies, affecting their expected revenue.
Beau finds it noteworthy that a former coal mine is being repurposed for a solar panel farm, showcasing adaptation to changing energy trends.
States reliant on supplying energy can still thrive by transitioning to cleaner energy sources.
Beau suggests re-evaluating investments in oil and dirty energy companies due to the impending shift towards cleaner energy.
While not an expert, Beau encourages awareness of the potential impact of stranded assets in the energy sector.

Actions:

for investors, energy sector,
Re-evaluate investments in oil and dirty energy companies (suggested)
Support and advocate for the transition to cleaner energy sources in your community (implied)
</details>
<details>
<summary>
2022-04-07: Let's talk about CPAC leaving the US and Republicans leaving democracy.... (<a href="https://youtube.com/watch?v=GT_RxFeUN3E">watch</a> || <a href="/videos/2022/04/07/Lets_talk_about_CPAC_leaving_the_US_and_Republicans_leaving_democracy">transcript &amp; editable summary</a>)

Beau reveals how the Republican Party is drifting from democracy, embracing authoritarian sympathies, and facing a critical identity crisis amid global political shifts.

</summary>

"The Republican Party is openly kind of shying away from democracy."
"They couldn't even bring themselves to support democracy in words."
"The Republican Party has become filled with people who share ideologies with strongmen, authoritarian goons all over the world."
"The Republican Party wants to have Putin's man in NATO as their keynote speaker."
"Those people who fashion themselves patriots, those people who say that they love this country, they've got some soul-searching to do."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is moving away from democracy openly.
The framing of the new Cold War is democracy versus authoritarianism.
The Republican Party faces a dilemma because many members are authoritarians.
The CPAC conference, a significant event for the Republican Party, will have Orban as the keynote speaker in Hungary, known to be sympathetic to Putin.
63 Republican congresspeople voted against a non-binding resolution that supported democracy.
The resolution called for NATO to support democratic principles and institutions within its member countries.
The Republican Party is embracing ideologies similar to authoritarian leaders globally.
There are concerns that the Republican Party is being led by an authoritarian figure amid the new Cold War dynamics.
Beau urges individuals within the Republican Party to re-evaluate their stance on democracy and their party affiliation.

Actions:

for republicans, democrats, activists,
Re-evaluate your stance on democracy and your party affiliation (suggested)
Support democratic principles and institutions within your community (exemplified)
Advocate for political leaders who uphold democratic values (implied)
</details>
<details>
<summary>
2022-04-06: Let's talk about what NATO involvement might look like.... (<a href="https://youtube.com/watch?v=vQMzl9H-Lb0">watch</a> || <a href="/videos/2022/04/06/Lets_talk_about_what_NATO_involvement_might_look_like">transcript &amp; editable summary</a>)

Beau explains his opposition to NATO involvement in a conflict, instead suggesting providing tools to Ukraine while outlining potential risks and scenarios, including a safer limited ground assault option.

</summary>

"Nobody can touch the U.S. in the sky."
"NATO achieves air superiority pretty quickly."
"A miscalculation here is tens of millions gone."
"Direct confrontation between NATO and Russia is not a good idea."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains his opposition to the idea of NATO getting involved in a conflict and prefers giving Ukraine the necessary tools.
Describes a hypothetical scenario where NATO achieves air superiority to support Ukrainian ground forces.
Points out the risks and unanswered questions associated with NATO involvement, such as engaging with areas already taken and handling missiles launched from Russia.
Outlines the unlikely option of sending in US or NATO troops due to lack of appetite among most countries in NATO.
Details a potential combined arms advance scenario starting with air superiority and a single route of advance.
Suggests a safer approach of a limited ground assault by NATO forces to quickly turn the tide of the war without crossing the Russian border.
Expresses concerns about the possibility of NATO inadvertently destroying the Russian military too quickly and triggering an existential threat response.
Warns against direct confrontation between NATO and Russia and stresses the need for any potential action to be quick, well-defined, and away from the border.

Actions:

for international policymakers,
Coordinate diplomatic efforts to provide Ukraine with necessary tools (implied).
Advocate for diplomatic solutions to prevent escalation of conflicts (implied).
</details>
<details>
<summary>
2022-04-06: Let's talk about Stanislav Petrov saving the world.... (<a href="https://youtube.com/watch?v=8k2BAVI95HA">watch</a> || <a href="/videos/2022/04/06/Lets_talk_about_Stanislav_Petrov_saving_the_world">transcript &amp; editable summary</a>)

Stanislav Petrov's critical thinking and decision to break protocol averted a potential nuclear disaster in 1983, showcasing the importance of independent judgment in high-stakes situations.

</summary>

"He did absolutely nothing, didn't report it. Didn't send the information up the chain."
"The only way to win is not to play."

### AI summary (High error rate! Edit errors on video page)

Stanislav Petrov, known as the man who saved the world, prevented a potential nuclear catastrophe in 1983 by not following protocol.
Petrov's job was to monitor satellites for US launches, and he noticed the equipment reporting a launch followed by five more launches.
Petrov's decision to wait and double-check the information instead of immediately reporting it prevented a mistaken retaliatory strike by the Soviets.
The incident occurred shortly after the Soviets mistakenly shot down Korean Air Flight 007, heightening tensions.
The monitoring system mistook sunlight bouncing off clouds for the launch of nuclear missiles, triggering the false alarm that Petrov averted.
Petrov's actions bought time and prevented an escalation that could have led to a full-scale nuclear conflict.
This incident exemplifies the dangers of near-peer contests and the potential for mistakes or glitches to trigger catastrophic events.
Petrov was not punished for breaking protocol but also did not receive any rewards for his critical actions.
The public only learned about Petrov's heroic act in 1998, 15 years after the incident occurred.
Petrov's decision to pause and verify the information before taking action showcases the importance of critical thinking and independent judgment in high-stakes situations.

Actions:

for history enthusiasts, policymakers, activists,
Study historical incidents like Stanislav Petrov's to understand the importance of critical thinking and independent judgment (suggested)
Share Petrov's story to raise awareness about the risks of nuclear escalation and the impact of individual actions (implied)
</details>
<details>
<summary>
2022-04-06: Let's talk about Russian state media telling us what's coming.... (<a href="https://youtube.com/watch?v=lcPIhiIAUoU">watch</a> || <a href="/videos/2022/04/06/Lets_talk_about_Russian_state_media_telling_us_what_s_coming">transcript &amp; editable summary</a>)

Russian state media's plan for Ukraine involves dividing the country, denying its right to exist, and using rhetoric reminiscent of the 1930s and 40s, raising alarming concerns for Western powers.

</summary>

"Ukraine, it's not really a civilization. It's not a country. They don't have a right to exist."
"Denying the right to existence of a country is a pretty big deal and the methods that they are talking about using are, they have parallels in the 1930s and 40s."
"It's worth the read. I suggest you take the time to do it because I have a feeling this is going to impact a lot of decision-making when it comes to Western powers and what they're willing to do."

### AI summary (High error rate! Edit errors on video page)

Russian state media released an article titled "What Should Russia Do With Ukraine?" outlining a plan for imperialism, suggesting Ukraine is not a civilization.
The article repeatedly labels Ukrainians as Nazis to create a villain for propaganda purposes.
The plan involves dividing Ukraine, with the eastern part becoming Russian territory administered by Russia temporarily.
The western part not conquered by Russia is where the "haters" will reside.
Russia plans to conduct trials to determine whether individuals collaborated with the government they dislike in the colonized country.
There are intentions to re-educate the populace in the colonized region.
Russia aims to experiment with the division line, taking as much territory as possible.
Ukrainians are called upon to resist the division of their country, as Russia's goal is to push as far as they can.
The methods and rhetoric used have historical parallels to the 1930s and 40s, raising significant concerns.
The article's content is likely to influence decision-making by Western powers regarding the situation in Ukraine.

Actions:

for global citizens,
Raise awareness about the dangerous rhetoric and intentions outlined in Russian state media (suggested).
Support organizations working to defend Ukraine's sovereignty and raise funds for aid efforts (suggested).
</details>
<details>
<summary>
2022-04-05: Let's talk about the situation on the ground today.... (<a href="https://youtube.com/watch?v=7TwiDGMAfn8">watch</a> || <a href="/videos/2022/04/05/Lets_talk_about_the_situation_on_the_ground_today">transcript &amp; editable summary</a>)

Russian forces in Ukraine are redeploying, Ukrainian tactics may shift, and a conventional fight looms as the conflict evolves.

</summary>

"Russian forces around the capital of Ukraine are almost non-existent."
"Ukraine may make a mistake by switching to a conventional fight from their successful unconventional tactics."
"The ongoing conflict is shifting back towards a conventional fight in certain areas."

### AI summary (High error rate! Edit errors on video page)

Russian forces around the capital of Ukraine are almost non-existent, with little pockets left.
These forces are likely to be redeployed in the east.
The most contested areas in Ukraine, such as Mariupol and the east, have seen no significant changes.
Russian forces in Kharkiv and Izium are preparing for a potential offensive, gathering supplies and food.
Russian forces facing logistical issues are still gathering resources from the countryside.
The most probable move by Russian forces is to encircle Ukrainian forces in the east.
Ukraine may make a mistake by switching to a conventional fight from their successful unconventional tactics.
If Ukraine rejects mobility and goes static, Russian forces may have initial success but will face logistical issues.
Russian command appears to be planning a slow and deliberate retake of more Ukrainian territory.
Ukrainian counteroffensive may be possible if they can break Russian forces coming from Kharkiv and Izium.
The behavior of Russian forces is likely to motivate Ukrainians to fight harder and not surrender.
The ongoing conflict is shifting back towards a conventional fight in certain areas.
The defenders in Mariupol will eventually face defeat without relief or a counteroffensive.
Peace talks are expected to continue alongside the military developments in Ukraine.

Actions:

for international observers,
Support relief efforts for defenders in Mariupol by providing aid and assistance (implied).
Stay informed about the situation in Ukraine and advocate for peaceful resolutions (implied).
</details>
<details>
<summary>
2022-04-05: Let's talk about a Russian superweapon.... (<a href="https://youtube.com/watch?v=Z174MnstL0o">watch</a> || <a href="/videos/2022/04/05/Lets_talk_about_a_Russian_superweapon">transcript &amp; editable summary</a>)

Beau speculates on Russian responses, introduces a super weapon program, and stresses the urgent need to combat climate change as a national security threat.

</summary>

"Climate change is a national security issue."
"We can shift. We can shift. We can keep this from happening."
"This is not a super weapon that the Russians have. It's just the future if we don't change."

### AI summary (High error rate! Edit errors on video page)

Speculates on possible Russian responses to dissuade NATO.
Mentions Russia's super weapon program called Izmeninia Klimatsa.
Describes the impact of the super weapon, causing drought, water shortages, and food shortages in the US.
Predicts economic repercussions leading to internal displacement and economic refugees.
Points out that climate change is the real super weapon, known as Russian for climate change.
Emphasizes treating climate change as a national security threat.
Criticizes politicians prioritizing campaign contributions over protecting the country.
Calls for a shift in energy production and consumption to combat climate change.
Urges for political will to address climate change.
Warns about the urgent need to reduce carbon emissions to meet warming goals.

Actions:

for climate advocates, policymakers,
Shift energy production and consumption to combat climate change (implied)
</details>
<details>
<summary>
2022-04-05: Let's talk about Ketanji Brown Jackson and Republican legacy.... (<a href="https://youtube.com/watch?v=sfgHUGhlYkM">watch</a> || <a href="/videos/2022/04/05/Lets_talk_about_Ketanji_Brown_Jackson_and_Republican_legacy">transcript &amp; editable summary</a>)

Republicans face the choice between securing their legacy or succumbing to political stunts in the confirmation of Ketanji Brown Jackson, the first black woman Supreme Court nominee.

</summary>

"If they vote yes, it secures their legacy. They will be on the right side of history."
"When this is looked back at through the lens of history, what you're going to see is the first black woman up for the Supreme Court meeting resistance that doesn't make any sense unless you view it through the lens of she's the first black woman."
"They had to make something up to oppose her."
"If it turns out they actually care about their legacy, that's something we need to keep in mind."
"Those who really do want a society that's moving backwards in time."

### AI summary (High error rate! Edit errors on video page)

Speculates on Supreme Court nominee Ketanji Brown Jackson's confirmation.
Notes resistance from the Republican Party despite likely confirmation.
Mentions three Republican senators planning to vote to confirm.
Raises the question of legacy for Republicans in their confirmation vote.
Emphasizes the significance of the first black woman Supreme Court nominee facing resistance.
Criticizes opposition to Jackson as manufactured culture war tactics.
Suggests that the real reason for opposition is to manipulate and appease a certain base.
Contemplates the implications of Republicans prioritizing personal legacy over political stunts for their base.
Raises the possibility of countering authoritarian tendencies within the Republican Party.
Concludes with a reflection on the situation and leaves with well wishes.

Actions:

for political observers,
Keep a close eye on how Republican senators vote in the confirmation process (implied).
Advocate for fair and unbiased confirmation processes for all Supreme Court nominees (implied).
Counter authoritarian tendencies within the Republican Party through activism and awareness (implied).
</details>
<details>
<summary>
2022-04-04: Let's talk about why Ukraine is getting more coverage.... (<a href="https://youtube.com/watch?v=2nRyE_JGdGM">watch</a> || <a href="/videos/2022/04/04/Lets_talk_about_why_Ukraine_is_getting_more_coverage">transcript &amp; editable summary</a>)

Beau explains why the conflict in Ukraine stands out in global coverage, touching on racial dynamics, geopolitical implications, and the looming threat of nuclear powers overshadowing other motives.

</summary>

"It's race."
"That is the most important reason."
"Most times it's a combination of things that leads to any particular behavior."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains why the conflict in Ukraine is receiving more coverage compared to other conflicts around the world.
Notes that some individuals question the coverage with less than ideal motives, seeking to divert attention from the conflict.
Addresses the racial aspect, pointing out that Ukraine's population being mostly white and located in Europe contributes to the increased coverage.
Mentions that the conflict was initially seen as the beginning of a new Cold War involving the United States, Russia, and China.
Acknowledges that the conflict directly endangers energy supplies to many parts of the world, particularly Europe.
Talks about the unexpected turn of events in the conflict, likening it to a major upset in sports.
Emphasizes the importance of Russia's involvement as a nuclear power in direct competition with other nuclear powers.
Expresses the significance of the conflict in terms of criminal activities during war and the potential for nuclear fallout.
Stresses the importance of understanding Russia's activities and intentions leading up to the conflict.
Shares personal reasons for covering the conflict extensively, citing expertise in this particular type of conflict.

Actions:

for global citizens,
Analyze Russia's activities and intentions leading up to the conflict (suggested)
Guard against assigning a single motive to conflicts and behaviors, considering multiple factors (implied)
</details>
<details>
<summary>
2022-04-04: Let's talk about the unthinkable in Able Archer 1983.... (<a href="https://youtube.com/watch?v=A7pEuZDW3aw">watch</a> || <a href="/videos/2022/04/04/Lets_talk_about_the_unthinkable_in_Able_Archer_1983">transcript &amp; editable summary</a>)

A recount of a chillingly close encounter with nuclear war during the Cold War in 1983, underscoring the delicate balance that prevented a global catastrophe.

</summary>

"It takes both sides to make a mistake at the same time."
"You wouldn't be watching this video right now because none of us would be here."
"It was way too close for comfort."

### AI summary (High error rate! Edit errors on video page)

Recounts a close call with nuclear war during the Cold War in 1983 involving a NATO exercise called Able Archer.
NATO's exercise simulated a nuclear war scenario, with generals participating and encrypted communications used.
In the exercise, NATO decided to enact a nuclear signal by choosing to destroy the capital of Ukraine, escalating tensions with the Soviet Union.
The Soviets misinterpreted NATO's exercise as a cover for a potential first strike against them, leading to heightened alerts and loading of live nuclear weapons on planes.
A critical Air Force officer suggested de-escalation to prevent further tensions from spiraling out of control.
Despite being a simulation, the exercise revealed how close the world came to a nuclear catastrophe in 1983.
The public only learned about this close call years later, underlining the secrecy surrounding such events during the Cold War.

Actions:

for history enthusiasts, peace advocates,
Join peace organizations to advocate for nuclear disarmament (implied)
Educate others on the dangers of nuclear weapons (implied)
</details>
<details>
<summary>
2022-04-04: Let's talk about crime and peacekeepers in Ukraine.... (<a href="https://youtube.com/watch?v=ef0P6YeBOps">watch</a> || <a href="/videos/2022/04/04/Lets_talk_about_crime_and_peacekeepers_in_Ukraine">transcript &amp; editable summary</a>)

Beau provides context on proposed Western peacekeepers in Ukraine post-Russian occupation, cautioning against the potential risks of escalating conflict.

</summary>

"Peacekeeping force, not a peacemaking force."
"A peacekeeping operation is just a step to going to war."
"Advocating for peacekeeping operations may actually be advocating for war under a different name."

### AI summary (High error rate! Edit errors on video page)

Ukrainian forces found evidence of horrific acts as they liberated towns formerly occupied by Russia.
The comparison was made to the Srebrenica massacre during the Bosnian War, illustrating the severity of the situation.
The idea of Western peacekeepers in Ukraine has been proposed, but there are significant challenges.
Peacekeeping forces are not meant to operate in active war zones; inserting them may not be effective.
The West's misunderstanding of peacekeeping versus peacemaking complicates the situation.
Peacekeepers in Ukraine could potentially provide a safe haven for refugees in Western Ukraine.
Direct confrontation with Russia through peacekeeping operations could escalate into a full-scale war.
The options remain the same: do nothing, support Ukraine externally, or go to war with Russia.
Engaging Russia directly still carries significant risks and challenges.
Providing more assistance to Ukraine as Russia withdraws could be a more feasible approach.
Advocating for peacekeeping operations may actually be advocating for war under a different name.

Actions:

for policy makers, activists,
Advocate for providing more assistance to Ukraine as Russia withdraws (implied)
Support efforts to help refugees in Western Ukraine (implied)
</details>
<details>
<summary>
2022-04-03: Let's talk about pirates and emperors on the roadsides of Ukraine.... (<a href="https://youtube.com/watch?v=Foi-W36dpho">watch</a> || <a href="/videos/2022/04/03/Lets_talk_about_pirates_and_emperors_on_the_roadsides_of_Ukraine">transcript &amp; editable summary</a>)

Beau challenges the moral perceptions surrounding the use of roadside devices and urges individuals to think beyond their government's stance to be truly advanced citizens.

</summary>

"You are not your government."
"You can support pieces of something and oppose others."
"If you want to be an advanced citizen, you have to oppose your government's morality."

### AI summary (High error rate! Edit errors on video page)

Talks about the concept of the Ukrainian resistance using roadside devices.
Mentions people questioning the morality of such tactics.
Shares a story from St. Augustine about pirates and emperors to illustrate moral perception.
Questions the moral difference between roadside devices and artillery.
Points out that the effectiveness of a tactic like roadside devices is why they are used.
Advises not to base morality on a nation-state's perspective.
Emphasizes that one can support or oppose actions of their government.
Encourages opposing government morality to strive for improvement.

Actions:

for citizens,
Oppose your government's morality to push for better standards (suggested).
Support or oppose specific actions of your government (exemplified).
</details>
<details>
<summary>
2022-04-03: Let's talk about gas, insulin, and republican strategy.... (<a href="https://youtube.com/watch?v=nXYrzRcbUW8">watch</a> || <a href="/videos/2022/04/03/Lets_talk_about_gas_insulin_and_republican_strategy">transcript &amp; editable summary</a>)

Republicans obstruct assistance measures, prioritizing power over people, aiming to blame Biden for economic challenges pre-midterms.

</summary>

"They care about power, not policy."
"Their strategy, going into the midterms, is to hurt you as much as possible and blame it on Biden."
"They want Americans hurt so they can blame Biden."

### AI summary (High error rate! Edit errors on video page)

Examining the Republican strategy of portraying the Biden administration as inactive in helping average Americans, using rising gas prices and other costs as a talking point.
The Biden administration plans to release a million barrels a day from the strategic reserve to mitigate the impact of increasing gas prices, aiming to assist but facing skepticism about the significance of the action.
The Republican Party is likely to criticize the administration's efforts as insufficient and propose measures like price caps to appeal to working Americans before the midterms.
Despite the need for assistance, especially evident in the case of insulin prices, a bill passed by the House to cap insulin costs at $35 a month faces challenges in the Senate, where Republican support is necessary for it to pass.
The bill aimed at helping millions of Americans with insulin costs faced significant Republican opposition in the House, illustrating a focus on obstructing rather than aiding struggling citizens.
Beau criticizes Republicans for prioritizing political power over policies that could benefit Americans, pointing out their potential to blame rising costs on the Biden administration while actively impeding solutions.
Some states have waived gas taxes during the crisis, contrasting with Republican governors who maintain such taxes, potentially hindering relief efforts and favoring political agendas.

Actions:

for voters and community members.,
Contact your Senators to support bills aiding Americans with rising costs (suggested).
Advocate for policies that prioritize people over political games (exemplified).
</details>
<details>
<summary>
2022-04-02: Let's talk about what's next in Ukraine.... (<a href="https://youtube.com/watch?v=kaL8BL7Vu44">watch</a> || <a href="/videos/2022/04/02/Lets_talk_about_what_s_next_in_Ukraine">transcript &amp; editable summary</a>)

Beau explains the strategic shifts in Ukraine and warns of potential dark paths ahead under Russian occupation.

</summary>

"They're not retreats as in we're accepting defeat."
"That seems like their most likely move."
"That's always a bad move and it always proceeds even worse moves."
"So whatever outcome they want to achieve, they have to do it quickly."
"I don't believe that though."

### AI summary (High error rate! Edit errors on video page)

Explains moves on the ground in Ukraine and what they signify.
Russia appears to have settled for taking land in eastern Ukraine.
The move is seen as a strategic shift rather than a retreat.
Beau predicts that Russia will try to subdue resistance in the occupied area.
Russia may resort to forcibly relocating Ukrainian citizens in their strategy.
Beau expresses concern about the potential for ethnic cleansing in the region.
He doubts the feasibility of Russia's forces subduing an insurrection in Ukraine.
Beau suggests that international reaction to such actions is critical.
Considers the possibility of a prolonged conflict if Russia attempts to take the whole country.
Beau does not believe that Putin genuinely seeks a settlement in peace talks.

Actions:

for global citizens,
Monitor and raise awareness about the situation in Ukraine (suggested)
Advocate for diplomatic intervention and peace efforts (suggested)
</details>
<details>
<summary>
2022-04-02: Let's talk about some good climate news.... (<a href="https://youtube.com/watch?v=Ili1zHezXoI">watch</a> || <a href="/videos/2022/04/02/Lets_talk_about_some_good_climate_news">transcript &amp; editable summary</a>)

A report shows significant clean energy progress worldwide, signaling hope in meeting climate targets and urging Western nations to prioritize energy transition for economic and security benefits.

</summary>

"Denmark is leading the way."
"It seems possible."
"Hitting the 1.5."

### AI summary (High error rate! Edit errors on video page)

A recent report reveals that 10.3% of the world's energy is sourced from wind and solar power, with a total of 38% coming from clean energy sources including hydro and nuclear.
The current rate of growth in clean energy suggests that the power sector is on track to meet the 1.5 degree Celsius limit critical for combating climate change, potentially averting catastrophic scenarios.
Western countries, including the US, must revamp their power sectors due to energy and political instability in certain regions, making it economically imperative to shift towards cleaner energy sources.
The geopolitical landscape necessitates transitioning from fossil fuels to cleaner energy sources for enhanced national security, ensuring uninterrupted energy supply and reducing dependency on other nations.
Denmark leads by example with over half of its energy derived from clean sources, setting a benchmark for other countries to follow suit.
Despite criticisms like "whataboutism," industrialized Western nations bear the responsibility to lead the transition to cleaner energy, considering their significant energy consumption compared to other regions.
While progress in the power sector is promising, there is a pressing need for all industries to embrace cleaner energy practices to combat climate change effectively.
Achieving the 1.5 degree Celsius target appears feasible, offering a glimmer of hope in the fight against climate change.

Actions:

for policy makers, environmentalists, energy sector,
Advocate for and support policies that prioritize clean energy adoption across industries (implied)
Engage in community initiatives promoting renewable energy sources and sustainability practices (suggested)
</details>
<details>
<summary>
2022-04-02: Let's talk about if it would be better in Ukraine without the west.... (<a href="https://youtube.com/watch?v=y-LwkaXKskw">watch</a> || <a href="/videos/2022/04/02/Lets_talk_about_if_it_would_be_better_in_Ukraine_without_the_west">transcript &amp; editable summary</a>)

Exploring the fallacy of the West prolonging conflict in Ukraine by supplying arms, Beau explains why swift Russian defeat is key to minimizing civilian harm and preventing a devastating occupation.

</summary>

"The occupation's the hard part. It's supposed to be easy to take the country."
"If you can't take the country easily, you certainly can't occupy it."
"The resistance is the hard part. That's when it gets really bad."
"Occupations take time, and the resistance is the hard part."
"The West is responsible for the suffering because they gave Ukraine the armaments to fight off an invasion."

### AI summary (High error rate! Edit errors on video page)

Exploring the idea that the West is prolonging the conflict in Ukraine by supplying arms initially seems logical, but upon deeper reflection, it doesn't hold true.
Russia hasn't faced the most challenging aspect yet, which is the occupation phase after taking a country.
The tools provided by the West to Ukraine are aiding in repelling Russia, potentially bringing a quicker end to the conflict and reducing civilian harm.
Occupying a country requires significantly more troops than just taking it, and Russia lacks the numbers needed for a successful occupation.
A prolonged occupation leads to increased harm to civilians, as seen historically in conflicts like Afghanistan.
The resistance phase, post-occupation, is when situations typically escalate and harm to civilians intensifies.
The West supplying arms to Ukraine is not responsible for the suffering but rather a means to prevent a more devastating outcome.
Without the armaments, an occupation by Russia could have commenced, resulting in more harm and prolonged conflict.
Occupations are difficult and prolonged, leading to continued fighting and increased civilian casualties.
Ending the conflict swiftly by repelling Russia is the best course to minimize harm to civilians and prevent a prolonged occupation.

Actions:

for global citizens, policymakers,
Support initiatives that aim to end conflicts swiftly to minimize civilian harm (implied)
Advocate for diplomatic solutions to prevent prolonged occupations and reduce civilian casualties (implied)
</details>
<details>
<summary>
2022-04-01: Let's talk about Ukrainian helicopters and Russian oil.... (<a href="https://youtube.com/watch?v=uwpEXFlxuEY">watch</a> || <a href="/videos/2022/04/01/Lets_talk_about_Ukrainian_helicopters_and_Russian_oil">transcript &amp; editable summary</a>)

An oil depot attack in Russia, likely by Ukrainian pilots, shifts the dynamic and prompts speculation on future outcomes.

</summary>

"The most likely explanation is that Ukraine did it."
"It's worth noting that Russia has been targeting Ukrainian oil depots for quite some time."
"Either way, this shifts the dynamic."
"This is probably something that's going to get a lot of discussion."
"In most scenarios, it's a good move for Ukraine."

### AI summary (High error rate! Edit errors on video page)

April Fool's Day coincides with Dan Ashby's birthday.
An oil depot in Russia was hit by at least two helicopters, with conflicting attributions.
The attack likely involved Ukrainian pilots flying under Russian air defenses.
Russia had portrayed Ukraine as without air capabilities, which this incident disproves.
The oil depot attack caused panic buying and awareness of the war's proximity.
Speculation arises about Russia using the incident for support amid conscription.
The attack may prompt a disproportionate Russian response.
The Ukrainian forces possibly executed a bold, legitimate military target strike.
Russia has a history of targeting Ukrainian oil depots.
The situation is still developing, with potential propaganda value for Ukraine.

Actions:

for world citizens,
Stay informed on the developing situation and geopolitical dynamics (implied).
Support efforts for peace and de-escalation in the region (implied).
Advocate for transparency and accountability in international conflicts (implied).
</details>
<details>
<summary>
2022-04-01: Let's talk about Trump's truth network.... (<a href="https://youtube.com/watch?v=q6Axsi1O5cU">watch</a> || <a href="/videos/2022/04/01/Lets_talk_about_Trump_s_truth_network">transcript &amp; editable summary</a>)

Former President Trump's Truth Social experiences initial success but faces functionality issues and dwindling enthusiasm, leaving investors at risk of substantial losses.

</summary>

"Those of us who have been following this really closely understand that it's April Fool's Day."
"It doesn't appear as though the deadline for getting it up and running in March is really met."
"If you didn't find this joke funny, I mean, just remember that there were a whole bunch of politicians who invested tons of cash in this who are probably losing tons of money."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's social media network, Truth Social, was anticipated to rival Twitter and Facebook, drawing skepticism and mockery.
Despite initial doubts, Truth Social gained millions of active users, functioning as a Twitter clone and establishing a conservative echo chamber.
Conservative companies are investing heavily in advertising on the platform, leading to financial gains for investors.
Surprisingly, the app saw a significant increase in new installs, defying expectations based on Trump's past business ventures.
However, as April Fool's Day approaches, the platform's success seems short-lived, with a drastic drop in new installs.
Users faced difficulties accessing Truth Social, resorting to discussing it on Twitter due to functionality issues.
The platform's failure to meet its March launch deadline and handle user load indicates diminishing enthusiasm for the project.
Lack of major advertisers transitioning to Truth Social suggests a bleak outlook for the platform's viability.
The investment made by politicians and others in the project may result in substantial financial losses, potentially adding humor to the situation.
Overall, the developments surrounding Truth Social are not surprising, except perhaps for those closely involved in the project.

Actions:

for social media users,
Monitor alternative social media platforms for potential trends or developments (implied)
Stay informed about the functionality and success of various social media networks (exemplified)
</details>
