---
title: Let's talk about if it would be better in Ukraine without the west....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=y-LwkaXKskw) |
| Published | 2022/04/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the idea that the West is prolonging the conflict in Ukraine by supplying arms initially seems logical, but upon deeper reflection, it doesn't hold true.
- Russia hasn't faced the most challenging aspect yet, which is the occupation phase after taking a country.
- The tools provided by the West to Ukraine are aiding in repelling Russia, potentially bringing a quicker end to the conflict and reducing civilian harm.
- Occupying a country requires significantly more troops than just taking it, and Russia lacks the numbers needed for a successful occupation.
- A prolonged occupation leads to increased harm to civilians, as seen historically in conflicts like Afghanistan.
- The resistance phase, post-occupation, is when situations typically escalate and harm to civilians intensifies.
- The West supplying arms to Ukraine is not responsible for the suffering but rather a means to prevent a more devastating outcome.
- Without the armaments, an occupation by Russia could have commenced, resulting in more harm and prolonged conflict.
- Occupations are difficult and prolonged, leading to continued fighting and increased civilian casualties.
- Ending the conflict swiftly by repelling Russia is the best course to minimize harm to civilians and prevent a prolonged occupation.

### Quotes

- "The occupation's the hard part. It's supposed to be easy to take the country."
- "If you can't take the country easily, you certainly can't occupy it."
- "The resistance is the hard part. That's when it gets really bad."
- "Occupations take time, and the resistance is the hard part."
- "The West is responsible for the suffering because they gave Ukraine the armaments to fight off an invasion."

### Oneliner

Exploring the fallacy of the West prolonging conflict in Ukraine by supplying arms, Beau explains why swift Russian defeat is key to minimizing civilian harm and preventing a devastating occupation.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Support initiatives that aim to end conflicts swiftly to minimize civilian harm (implied)
- Advocate for diplomatic solutions to prevent prolonged occupations and reduce civilian casualties (implied)

### Whats missing in summary

The full transcript provides detailed insights into the consequences of military occupations and the importance of swift resolutions in conflicts to protect civilian populations.

### Tags

#ConflictResolution #Ukraine #MilitaryOccupation #CivilianProtection #Diplomacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a new talking point.
And we're just going to kind of roll through it
and think about it a little bit deeper than the surface of it.
Because when you just look at the surface,
it kind of makes sense.
But the more you think about it, the less sense it makes.
So it's the idea, and I've seen this presented
in a number of different ways, but it's
the idea that if the West hadn't supplied Ukraine once
the fighting started, well, then it would have been over.
And therefore, all of the suffering
is on the West because they're prolonging the conflict.
I mean, when you first say that, that kind of makes sense.
But it's not true.
First, I do want to say I really don't like the idea of just
do whatever your attacker tells you
and you won't be hurt to become policy.
That doesn't sound like a good idea to me.
Aside from that, something I've said
throughout this entire thing, Russia
hasn't gotten to the hard part yet.
Taking the country is easy.
Well, I mean, it should be.
They're having a little bit of difficulty with it right now.
But generally speaking, taking a country is the easy part.
It's holding it.
It's the occupation that's the hard part.
If you go back and look at the video,
let's talk about the futures of Ukraine.
Go back and watch that.
The worst case scenario, and I go over quite a few,
the one I describe as the worst case scenario
is the one where Russia begins an occupation because then it
becomes protracted and it gets bad.
The tools that the West gave the Ukrainians,
it's speeding up them repelling Russia,
which should bring the fighting to a close faster.
Any situation in which Russia leaves
is faster and better and less civilians
come to harm than any situation in which they win
and they occupy because the occupation starts
the hard part.
That's when stuff's on the roadside.
That's when a military like Russia
would engage in reprisals.
That's when things would get really bad,
especially in the case of Russia here
because we've talked about the number of troops
that you would need to occupy Ukraine.
And they didn't send them.
They didn't send them.
And it's a classic mistake.
It's one a lot of militaries make.
But in order to successfully occupy and therefore speedily
end the resistance, they would need three times the troops
they sent.
They didn't have them.
They don't have them.
And they have no indication of being able to pull it off
because that's trained troops.
And they don't have troops trained in that.
If you want the least amount of harm to come to civilians,
then you're hoping for a speedy Russian defeat,
not that they can hurry up and conquer
so the resistance starts the hard part.
That would end up costing much, much more.
And you can look to Afghanistan as a good example.
There wasn't a major power supplying the Afghans
with high-tech equipment.
They just had the will to fight.
And because of that, it kept going and going and going.
And more and more and more civilians got harmed.
The same thing would have happened in Ukraine.
If you want the least amount of civilian harm,
you want the aggressing force out before they
can start an occupation.
Because occupations take time, and the resistance
is the hard part.
That's when it gets really bad.
That's when it gets really bad.
We saw for the first time the other day
an improvised device on a roadside.
That's not something that Russia's had to face yet,
not in any widespread way.
That's when it would start getting bad.
That becomes commonplace.
And the response from the soldiers against the general
populace is normally pretty bad.
So the idea that the West is responsible for the suffering
because they gave Ukraine the armaments
to fight off an invasion, it doesn't hold up
to any scrutiny.
Because without those armaments, then the occupation
would have started.
And an occupation is far worse.
So anyway, if you happen to see this come up,
there's your information.
The occupation's the hard part.
It's supposed to be easy to take the country.
If you can't take the country easily,
you certainly can't occupy it.
They didn't have the troops.
And it lasts years if there's an occupation.
The fighting continues.
And that always causes more harm to civilians.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}