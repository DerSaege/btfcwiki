---
title: Let's talk about Bishop Evans and Operation Lone Star....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jJ3jn3KB7J4) |
| Published | 2022/04/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Operation Lone Star under Texas Governor Abbott gaining national attention due to Specialist Bishop Evans' heroic act gone wrong, trying to save drowning individuals.
- Not the first death associated with Operation Lone Star, with at least the seventh known casualty.
- Troops in the operation facing dire conditions, attempting to unionize due to the situation.
- Operation Lone Star's claimed successes questioned by various reputable sources like Texas Tribune, ProPublica, NPR, and even the Army Times, labeling the operation as troubled.
- Troops expressing skepticism and frustration with the leadership and the operation itself, feeling like they are being used as mere props for a photo op.
- Operation Lone Star's outcomes: no drugs intercepted, no migrants caught, costing Texas $4.2 billion and shrinking the US GDP by $9 billion.
- Criticism towards Governor Abbott for risking lives and state funds for a photo op and PR stunt at the border.
- Texans urged to question how much money and troops they are willing to lose for a governor's reelection bid.


### Quotes

- "They're not going to admit that. They're going to say that they're frustrated and frustrated with their leadership because of how the operation's being run, probably because they know it's a PR stunt."
- "How much money is Texas willing to lose? And how many troops is Texas willing to lose?"
- "A governor attempting to secure re-election can have a photo op, can have a PR stunt, can find some way to channel his base's energy into kicking down at people."


### Oneliner

Operation Lone Star under Governor Abbott faces scrutiny for risking lives and state resources in a troubled mission, prompting troops' unionization and questioning Texans' support.


### Audience

Texans, Voters


### On-the-ground actions from transcript

- Question and hold Governor Abbott accountable for the decisions regarding Operation Lone Star (implied)
- Support troops' rights to unionize and express their concerns (implied)


### Whats missing in summary

The full transcript provides more in-depth details on the controversial Operation Lone Star, shedding light on the risks, losses, and questionable motives associated with Governor Abbott's actions.


### Tags

#OperationLoneStar #GovernorAbbott #Troops #BorderSecurity #Texas #Unionization #Accountability


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
Operation Lone Star a little bit more and some other things that are going on down along the
border under Texas Governor Abbott. This is something we've been talking about on the
channel for a while, but it is starting to gain more and more national attention,
primarily due to Specialist Bishop Evans. Evans was attempting to save people who appeared to
be drowning in a river and was washed away. His body was recovered and this has obviously
caught headlines. It's worth noting that as far as I can tell, legit act of heroism here.
He tried to save people and lost himself in the process. Something else that's worth noting is
that this is not the first death associated with Operation Lone Star. It's hard to get an accurate
count because it's not a combat operation, so they're not tracking as well as they normally
would. This isn't DOD running it. This is Governor Abbott. This is the state of Texas.
We know for a fact that this is at least the seventh lost in this operation.
This stunt down at the border, this is the same operation that has created conditions so bad for
the troops that they're trying to unionize. This is the same operation that has cooked numbers as
far as the successes that the governor claims. Numbers that have been, let's just say, called
into question by Texas Tribune, ProPublica, NPR, even the Army Times. The Army Times referred to
this failed operation as troubled. This isn't something you would normally expect from the Army
Times, but it's that bad. They called it down in an article about yet another leadership
change, trying to get this somewhat functional. They have also run articles talking about the
polling that was done of troops that were deployed on this, and that more than half of the troops
actually there were willing to admit that they expressed skepticism and frustration with their
leadership because of how the operation's being run. They're not going to admit that.
They're going to say that they're frustrated and frustrated with their leadership because of how
the operation's being run, probably because they know it's a PR stunt. They've been taken away from
their families involuntarily. These are people who signed up to help, and now they're being used as
window dressing for a photo op. Now, keep in mind, this is completely separate from the border
controlled, and now that we have the final counts on that, let me go ahead and give you all of that.
How much drugs was intercepted? None. Migrants? None. Cost to Texas? 4.2 billion dollars.
Cost to the United States? The projection shows it shrank the gross domestic product of the United
States by 9 billion dollars. Those who are in the Republican Party who are oh so loving of Putin,
they can call them up and say, hey, we know you can't sanction us in return, but don't worry,
Governor Abbott's got your back. The question here is how much money is Texas willing to lose?
And how many troops is Texas willing to lose? So a governor attempting to secure re-election
can have a photo op, can have a PR stunt, can find some way to channel his base's energy
into kicking down at people. That's the real question the Texans have to ask.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}