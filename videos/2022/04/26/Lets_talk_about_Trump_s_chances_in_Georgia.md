---
title: Let's talk about Trump's chances in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t22sDpq5rT0) |
| Published | 2022/04/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is trying to influence Georgia's political landscape by endorsing specific candidates.
- Despite his efforts, Trump has faced opposition from the Republican Party in Georgia.
- His chosen candidate for governor, Perdue, embraced Trumpism and repeated baseless election claims in a debate with the current governor, Kemp.
- Kemp's campaign has shifted focus to preparing for a general election against Stacey Abrams instead of engaging with Perdue.
- Even within Trump's slate of candidates, there are issues as not all have endorsed Perdue.
- The Republicans in Georgia view Perdue as a minor annoyance and are confident in defeating him.
- Trump's ego may be hurt by the lack of support for his chosen candidates.
- Georgia Republicans refused to assist Trump in overturning the election, causing tension.
- Trump's desire to be a kingmaker and prepare for a return in 2024 seems uncertain given the current situation in Georgia.

### Quotes

- "They can't even get Trump candidates to unify behind Trump candidates."
- "His ego is damaged. He's looking to get even and he has fielded a slate of candidates that are not doing well."
- "The Republican Party in Georgia is just viewing Perdue as somebody to basically a minor annoyance that they have to deal with before they get to the actual fight."

### Oneliner

Trump's attempts to influence Georgia politics face resistance from Republicans, with his chosen candidates struggling and facing internal issues.

### Audience

Political analysts

### On-the-ground actions from transcript

- Support local political candidates (exemplified)
- Stay informed about political developments in your state (exemplified)

### Whats missing in summary

Insights on the potential impact of Trump's involvement in Georgia politics and the implications for the upcoming elections. 

### Tags

#Georgia #RepublicanParty #Trump #PoliticalInfluence #Election2024


## Transcript
Well howdy there internet people. It's Beau again. So today we are going to talk
about Trump's chances in Georgia. Trump is trying to play Republican kingmaker.
He's trying to put his chosen people in certain positions and he's been running
into some pretty stiff opposition from the Republican Party and has suffered
a couple of big losses already. Fresh off the loss in Tennessee where the
Republican Party told his chosen candidate, yeah you know what we're not
even going to put you on the ballot. He's having issues in Georgia which is going to
probably really bother Trump. Georgia is a state that he has leaned very heavily
into. He's endorsed a senatorial candidate, Hershel Walker. He endorsed a
candidate for lieutenant governor, Bert Jones, and for Secretary of State, Jody
Heiss, as well as a candidate for governor, Perdue. Now Perdue, among
Republicans until very recently, Perdue was seen as kind of relatable and then
he fell down the rabbit hole of Trumpism and started embracing the weirder
aspects of it. In a recent debate with the current governor of Georgia, a
Republican named Kemp, Perdue started off by just repeating Trump's baseless
debunked claims about the last election. It was such an embarrassment for the
Trump team that Kemp's campaign has just kind of realized, you know what, we're
going to steamroll this person. We need to devote all of our attention to getting
ready to fight Stacey Abrams in the general election. And that's probably
a really smart move for Republicans because I had a feeling she'd be a whole lot
harder to beat. But aside from that, even within the Trump slate of candidates
there's issues. None of those other people that I named have endorsed Perdue.
They're trying to pass Trump off as a unifier of the Republican Party. Trying
to get Republicans everywhere to unify behind his candidates. They can't even
get Trump candidates to unify behind Trump candidates. And this is probably
going to hurt Trump's ego a lot. The reason I think he's leaning so heavily
into Georgia is because this is one of those states where the Republicans in
the state refused to play ball with him. When he called and asked for
help, they're like, you know what, I don't think I'm going to help you overthrow
the election. And it hurt his feelings. So his ego is damaged. He's looking to get
even and he has fielded a slate of candidates that are not doing well. So
much so that the top of the ticket is now kind of being seen as a joke.
The Republican Party in Georgia is just viewing Perdue as somebody to... basically a
minor annoyance that they have to deal with before they get to the actual fight.
They don't foresee any real problem with defeating this candidate. And I
honestly can't say that I blame them after that debate. But I would imagine
that with the way things are shaping up, Trump will probably, in true Trump
fashion, double down. It's unlikely that he'll do what he did in Alabama, which is
just abandon his chosen candidate. But I guess that's a possibility as well. Either
way, Trump's desire to play kingmaker and set the stage for his triumphant
return in 2024 isn't looking good. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}