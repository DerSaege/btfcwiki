---
title: Let's talk about why the West is slow-walking the sanctions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=O-cqcWF_tys) |
| Published | 2022/04/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The West is slow in imposing sanctions on Russia to strike a balance between hurting Russia and ensuring their own supply needs.
- Sanctions are being slowly increased to find alternative sources and maintain economic stability.
- The strategy is not just about being nice to average Russians but about keeping Western economies intact.
- The West's ability to supply Ukraine with defense relies on maintaining their own economies.
- The decision-making process behind imposing sanctions involves a mix of logical and cynical reasons.
- Large-scale sanctions impact the bottom first and take time to affect those in power.
- The Russian economy is struggling, resorting to tactics to sustain itself temporarily.
- The West is waiting for a slip in Russia's economic shell game before exerting full pressure.
- Slowly increasing pressure allows the West to maintain economic stability while sanctioning Russia.
- Despite appearing slow, there are strategic reasons behind the gradual approach to sanctions.

### Quotes

- "While it hurts Russia for the West to sanction it, the West also needs stuff."
- "Large-scale sanctions, they hit the people on the bottom first, and it takes time to trickle up."
- "The Russian economy is not in a good way."

### Oneliner

The West's slow sanction approach balances hurting Russia with economic stability, impacting the bottom before the powerful, waiting for Russia's economic slip.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor the situation in Ukraine and Russia closely to understand the impact of sanctions (implied)
- Support organizations providing aid to those affected by the conflict (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the reasons behind the West's slow approach to imposing sanctions on Russia.

### Tags

#Sanctions #Russia #Economy #GlobalRelations #Ukraine


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about why the West is slow walking the sanctions.
I've had a couple people ask this question in various ways.
Why the West is moving so slowly in instituting the sanctions on Russia?
Why it's going at a snail's pace the way it seems.
The easiest way to explain this is, you know that fast food joint in town that always gets
your order wrong?
You know, you go in and you order your burger with cheese and a coke and fries, and you
know when you pull away from the drive-thru window you've got a fish sandwich, tater tots
and a sweet tea at that place?
What do you say when that happens?
Never coming back to this place, just like you said the last 11 times.
But you normally do.
Because while denying them your business hurts them, it also hinders your ability to get
that product, right?
It's the same thing with sanctions.
While it hurts Russia for the West to sanction it, the West also needs stuff.
So it has to strike a balance.
It has to find alternative sources, and it has to move slowly and slowly ratchet up the
pressure, which is what they said in the beginning.
They said they were going to move slowly and build on it.
They made it seem like it was a strategy in the interest of being nice to the average
Russian.
That's not really the case.
It has to do with keeping their own economies intact while the sanctions go into effect.
And while that seems really cynical, and it seems like they're putting money over effectiveness
when it comes to the people in Ukraine who are caught in the middle of this, waiting
for the sanctions to take effect and really show an impact, it is cynical, and I'm sure
that there are a whole lot of people who are making these decisions who are just looking
at the economic numbers and the poll numbers.
But also keep in mind that Ukraine's ability to defend itself right now relies heavily
on the West's ability to produce, and the Western countries having the economy and the
power to deliver the supplies that they need.
So that's the logical reason behind it, and then there's the cynical reason that it's
all about money.
In this case, the people making the decisions probably are looking at it through the lens
of it's all about money, but there is a logical strategic reason for doing it this way as
well when it comes to the war front side.
Now me personally, I think they could go a little bit faster, and I wish they would.
Not for the reason that I think most people want them to go faster, though.
Large-scale sanctions, they hit the people on the bottom first, and it takes time to
trickle up.
The more severe they are, the faster they hit the people that actually have any power.
Until the sanctions get rolling full speed, it's just hurting the people at the bottom.
The people at the top, yeah, they lose a yacht or whatever, and they have a harder time moving
their billions around, but they don't feel the squeeze yet.
The Russian economy is not in a good way.
They're having to resort to some pretty wild stuff to keep their economy afloat, and in
large part it's a shell game.
They're moving stuff around, and eventually a hand's going to slip, and that's what the
West is waiting for.
In the meantime, they're slowly turning up the pressure, making sure that they can keep
pressure on Russia while at the same time maintain their economy at home.
So that's why it appears that they're slow walking it, because they are.
But there are reasons for them to do it that way.
Anyway, it's just thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}