---
title: Let's talk about hope and acceleration....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U9akT4JplzU) |
| Published | 2022/04/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the importance of hope amidst chaos and adversity.
- Questioning how to maintain hope and motivation in the face of increasing challenges.
- Exploring the concept of accelerationism and its implications.
- Emphasizing the necessity of considering what happens after potential collapse scenarios.
- Advocating for building local community networks for effecting real change.
- Warning against the assumption that good outcomes are guaranteed post-collapse.
- Encouraging proactive efforts to create a power structure that benefits the average person.
- Stressing the value of grassroots movements in shaping a fairer society.
- Acknowledging the reluctance of many to take on leadership roles for fear of corruption.
- Arguing that organizing and building networks now is key to preventing disastrous outcomes in the future.

### Quotes

- "The goal should be to stop that from happening."
- "The same thing that can stop it is the thing that would lead to a better world if it failed to stop it."
- "If you do that, and the system still collapses, climate change still just runs rampant, those power structures, those people who have connected in that way, who have built that network, they're going to be better positioned to ride it out."
- "And if you do it well enough, we don't even have to go through a collapse."
- "Y'all have a good day."

### Oneliner

Beau addresses maintaining hope, navigating accelerationism, and building community networks to shape a fairer future amidst chaos and uncertainty.

### Audience

Community organizers, activists

### On-the-ground actions from transcript

- Build local community networks to affect change locally and create a foundation for broader impact (implied)
- Organize grassroots movements for the betterment of people (implied)
- Actively work towards delaying ideological framework that may lead to negative outcomes (implied)

### Whats missing in summary

The full transcript provides a nuanced exploration of hope, accelerationism, and community building in the face of societal challenges.

### Tags

#Hope #Accelerationism #CommunityBuilding #Grassroots #Activism


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about hope,
keeping hope as everything goes awry,
and acceleration, and changing the world,
and picking up pieces.
Because I got a message.
There's the introduction part, and then it
gets to what really matters here.
There's something I've been wanting to ask for a long time
though.
How do you not just lose hope and give up?
You're well informed, and that certainly
must come with a certain amount of dread.
How do you keep doing what you do
as things seem to just get progressively worse?
Why not just vanish into your homestead,
free of the machinations of those
who seek to broaden their influence despite who it hurts?
Any advice for someone who is incredibly burned out,
who feels like the scales of power are so unfairly skewed
that we pass the point of average people
effecting legitimate change for the workers
before they were even born?
I used to be an accelerationist.
You changed my mind.
But sometimes I still think that just rushing
into that final disastrous conclusion
and picking up the pieces is better than dragging out
whatever we're doing now, at least for the generations that
come after.
Maintaining hope and being an accelerationist.
Yeah.
Um, it's funny because I guess maybe a year after I'd first
started doing this on YouTube, I had somebody send me a message
and they had talked to somebody who knew me.
And this person sent this message, like super mad,
that I wasn't using this platform to train people
how to fight in the glorious revolution or some nonsense.
And the answer is kind of the same.
What happens after?
Good guys win.
Troops home by Christmas.
What happens after?
If you don't have an answer to that, normally, well,
it just turns into another group of people in power.
There's no real change, not for the average people,
not for the people on the bottom.
And that's the question that you have
to ask when you're talking about picking up the pieces.
Let's say the system does collapse under its own weight.
Capitalism, run amok, climate change, whatever it is,
it brings it all down.
Who's there to pick up the pieces?
When you run through those scenarios
and you think about the people who
are likely to be in a position to pick up the pieces
afterward, what kind of system are they going to build?
You talk about the capitalist system just crashing, right?
The wealthy are still wealthy.
They're the ones who are going to have the resources to ride it out.
What kind of system do you think they're going to build when they emerge
from their billion dollar bunkers?
Talk about climate change.
Those who are in the best position to ride that out,
what's their ideology?
Sure, there are some who are like me, who want a world where
everybody gets a fair shake.
But when you think of those people who are ready to ride out
long-term disasters, what are they?
70% of them, right wing preppers, right?
At the very least, they're social Darwinists of some kind.
You want to live in the world that they build, future generations?
Probably not.
I don't.
I don't.
And that's the thing.
The goal should be to stop that from happening.
And how do you do it?
I mean, my answer, if you've watched the channel long enough,
community networks, building that local power structure that
can affect change locally, that then can be leveraged to get larger changes.
Everybody relies on each other.
Things get better.
You don't have to rely on entities that are, at this point, corrupted.
But see, here's the twist to that.
If you fail, if you don't stop the inevitable destruction that you're seeing,
what happens then?
When the power structure that exists, when it collapses, what remains?
A power structure built by people who want everybody to get a fair shake.
It's funny that the tool that, in my opinion, is probably the only thing left
for affecting real change, which is groups coming together, acting for
the betterment of people.
It's got to be average people, because those at the top are happy with the system
the way it is.
But if you do that, and the system still collapses, climate change still just runs
rampant, those power structures, those people who have connected in that way,
who have built that network, they're going to be better positioned to ride it out.
And they'll be there to pick up the pieces.
The same thing that can stop it is the thing that would lead to a better world
if it failed to stop it.
So if you find yourself slipping into that accelerationist mindset,
just understand there's no guarantee that after that inevitable collapse,
that the good guys are going to be the ones to pick up the pieces.
So if you're of that mindset, if you think that's going to happen,
it's probably even more important for you to retain a little hope,
for you to get active now, for you to try to build that power structure today.
And maybe you're wrong.
Maybe that power structure is the tool that you need to affect real change
for the average working person.
And it's hard because most people who want the world where everybody gets a fair shake,
most of them don't want to be leaders.
You know, they don't want to take that responsibility on themselves,
not because they're afraid of responsibility, not because they're not willing to work,
but because they're afraid of turning into what they see now.
That's the beauty of it though.
If you set those networks up on the local level, you don't get that.
That leader, that person who organized that,
they don't have enough power to become as corrupt as what you see today.
If you're sliding back in to just being despondent,
and hoping for the system to come crashing down,
because you think afterward things will get better,
the most important thing to realize is that it probably won't.
The same people will probably be in charge with even less constraints.
And that the only way to stop them is to organize
and delay that ideological framework today.
And if you do it well enough, we don't even have to go through a collapse.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}