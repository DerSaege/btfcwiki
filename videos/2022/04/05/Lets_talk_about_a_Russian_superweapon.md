---
title: Let's talk about a Russian superweapon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Z174MnstL0o) |
| Published | 2022/04/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on possible Russian responses to dissuade NATO.
- Mentions Russia's super weapon program called Izmeninia Klimatsa.
- Describes the impact of the super weapon, causing drought, water shortages, and food shortages in the US.
- Predicts economic repercussions leading to internal displacement and economic refugees.
- Points out that climate change is the real super weapon, known as Russian for climate change.
- Emphasizes treating climate change as a national security threat.
- Criticizes politicians prioritizing campaign contributions over protecting the country.
- Calls for a shift in energy production and consumption to combat climate change.
- Urges for political will to address climate change.
- Warns about the urgent need to reduce carbon emissions to meet warming goals.

### Quotes

- "Climate change is a national security issue."
- "We can shift. We can shift. We can keep this from happening."
- "This is not a super weapon that the Russians have. It's just the future if we don't change."

### Oneliner

Beau speculates on Russian responses, introduces a super weapon program, and stresses the urgent need to combat climate change as a national security threat.

### Audience

Climate advocates, policymakers

### On-the-ground actions from transcript

- Shift energy production and consumption to combat climate change (implied)

### Whats missing in summary

Beau's detailed explanation on the potential consequences of climate change if urgent action is not taken.

### Tags

#ClimateChange #NationalSecurity #RussianSuperWeapon #PoliticalWill #CarbonEmissions


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about another possibility.
You know ever since this whole thing started over there more than a month ago,
people have been talking about different possible Russian responses.
We're going to talk about different things that Russia might do in an attempt to dissuade NATO.
And you know it always falls back to X, Y, and Z and they might launch, right?
I've made it pretty clear that I don't see that as incredibly likely.
I certainly don't think it's something you should be worried about all the time.
But since we are talking about this, I want to talk about a different thing.
A super weapon. There's no other word for it.
And throughout the last half of the 20th century, most major powers had some kind of program trying to harness this.
Russia calls theirs Izmeninia Klimatsa.
And we'll talk about how it does what it does afterward.
But to pique your curiosity here, I'll tell you what it does.
Now they probably hit, I don't know, LA with it.
Now the thing about this is once it starts, it continually expands.
So we start talking about LA, but eventually it's like all of the American Southwest.
What we would notice first would be drought.
That's the first thing we would notice.
And that drought would cause water shortages, which then causes food shortages.
Now we're the US, so we're large enough to be able to mitigate some of the effects.
And we would try to pipe in or truck in water and food, you know, whatever.
However, we're the US, which means somebody's going to try to make money off that,
which is going to raise the cost of living in these areas substantially.
It's going to make it really expensive.
So expensive, in fact, that a whole bunch of people are going to move.
They're going to become internally displaced.
They'll become economic refugees, right?
And they'll go to areas that have food and water.
The problem is now there's more people, so those supplies become strained as well.
This, as the feedback loop just continues and things get worse and worse and worse,
this ends up disrupting pretty much all American economic activity.
The thing is, it also continues to expand.
Hits other places, which means there are places that are worse off than the US,
so you have people coming here, refugees from this.
And it's something that countries have tried to harness,
but to my knowledge, nobody's been able to.
That phrase I said, it's climate change.
It's just Russian for climate change.
If we ever treated climate change like the national security threat it is,
like the national security threat the Department of Defense says we should treat it as,
we'd probably be a lot better off.
If we looked at it through that lens and how damaging it could be,
we'd probably take action because if there was a weapon that could accomplish the same thing,
it would lead to an arms race that's just unbelievable
because we can't fathom allowing that kind of power to sit in somebody else's hands.
Nobody should be allowed to do that to the United States, right?
But we're going to do it to ourselves out of greed
because you have a bunch of politicians who are more interested in taking kickback,
campaign contributions, sorry, from energy companies than they are in protecting the country.
Climate change is a national security issue.
It has been one for a very long time, but they don't want to frame it that way
because they know that anybody who has a patriotic bone in their body would want to stop this.
And the reality is it's not really that hard.
We just need the political will.
But that political will has been bought by companies who stand to make tons of money off of the status quo.
We can shift. We can shift.
We can shift our energy production. We can shift our consumption.
We can do a whole lot of stuff and we can keep this from happening.
And understand when it comes to climate change, this isn't even the worst of it.
We don't have much time to hit the marks.
There are certain levels of warming that have been deemed goals.
We don't have a lot of time to put ourselves in the position to meet those goals
because it's like an 18-wheeler.
When you slam on the brakes, it doesn't stop immediately.
It takes time.
We have to cut our carbon emissions.
This is not a super weapon that the Russians have.
It's just the future if we don't change.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}