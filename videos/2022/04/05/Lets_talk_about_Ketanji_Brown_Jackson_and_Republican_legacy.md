---
title: Let's talk about Ketanji Brown Jackson and Republican legacy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sfgHUGhlYkM) |
| Published | 2022/04/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on Supreme Court nominee Ketanji Brown Jackson's confirmation.
- Notes resistance from the Republican Party despite likely confirmation.
- Mentions three Republican senators planning to vote to confirm.
- Raises the question of legacy for Republicans in their confirmation vote.
- Emphasizes the significance of the first black woman Supreme Court nominee facing resistance.
- Criticizes opposition to Jackson as manufactured culture war tactics.
- Suggests that the real reason for opposition is to manipulate and appease a certain base.
- Contemplates the implications of Republicans prioritizing personal legacy over political stunts for their base.
- Raises the possibility of countering authoritarian tendencies within the Republican Party.
- Concludes with a reflection on the situation and leaves with well wishes.

### Quotes

- "If they vote yes, it secures their legacy. They will be on the right side of history."
- "When this is looked back at through the lens of history, what you're going to see is the first black woman up for the Supreme Court meeting resistance that doesn't make any sense unless you view it through the lens of she's the first black woman."
- "They had to make something up to oppose her."
- "If it turns out they actually care about their legacy, that's something we need to keep in mind."
- "Those who really do want a society that's moving backwards in time."

### Oneliner

Republicans face the choice between securing their legacy or succumbing to political stunts in the confirmation of Ketanji Brown Jackson, the first black woman Supreme Court nominee.

### Audience

Political observers

### On-the-ground actions from transcript

- Keep a close eye on how Republican senators vote in the confirmation process (implied).
- Advocate for fair and unbiased confirmation processes for all Supreme Court nominees (implied).
- Counter authoritarian tendencies within the Republican Party through activism and awareness (implied).

### Whats missing in summary

The emotional weight and detailed analysis present in Beau's full commentary. 

### Tags

#SupremeCourt #Confirmation #RepublicanParty #KetanjiBrownJackson #Legacy #Resistance #CultureWar #Authoritarianism


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about Supreme Court nominee
Ketanji Brown Jackson and how it seems pretty likely that my guess is by the end of the week
she'll be Supreme Court Justice Ketanji Brown Jackson and she will be confirmed.
The Republican Party put up a whole lot of resistance. Normal obstructionists stuffed
up their seats because it's Biden's nominee, right? There were a lot of fireworks, there
was the deadlock on a committee, so on and so forth, but at the end of the day when you
get to the vote that actually matters, you already have three Republicans, Collins, Murkowski,
and Romney, saying they're going to vote to confirm. That kind of makes her nomination
and her confirmation all but assured. So now we get to see something interesting. We get
to observe. One of the things that gets brought up when we talk about Republican legislation
is the idea of legacy and how they're going to be remembered. We're going to find out
whether or not that actually matters to them, because at this point any of the Republicans
who have said, who haven't committed one way or the other to how they're going to vote,
their vote's kind of irrelevant. The votes are there for her to get confirmed. So if
they vote yes, it secures their legacy. They will be on the right side of history when
people look back. If they vote no, it doesn't do anything, but it makes them look, I don't
know, tough to their base. It allows them to continue to play into that garbage. So
we're going to get to find out whether or not their own personal legacy matters more
than a political stunt for their base, because that's what it boils down to. When this is
looked back at through the lens of history, what you're going to see is the first black
woman up for the Supreme Court meeting resistance that doesn't make any sense unless you view
it through the lens of she's the first black woman. Her qualifications, her experience,
is more than anybody on the court, anybody currently up there. There's no real reason
to oppose her on any ideological grounds, on anything that matters. Just silly culture
war stuff that they manufactured for their base. Stuff to keep those people who are easily
manipulated angry. Therefore, they require the Republican Party to save them. That's
really the only reason they have. They had to make something up to oppose her. So when
the lens of history passes over this, that's not what's going to be seen. What's going
to be seen is them trying to block the first black woman with no real reason. This is something
that might be useful later. If it turns out they actually care about their legacy, that's
something we need to keep in mind. If it turns out all they care about is manipulating their
base, well then at least we know that and it provides other avenues to counter those
who are more authoritarian in the Republican Party. Those who really do want a society
that's moving backwards in time. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}