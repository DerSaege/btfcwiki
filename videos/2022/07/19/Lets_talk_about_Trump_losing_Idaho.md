---
title: Let's talk about Trump losing Idaho....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dbSnIJ-bydw) |
| Published | 2022/07/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party in Idaho recently set up their party platform during their convention, adopting extreme and unique positions. 
- They decided not to include rejection of the 2020 election results, unlike some other states that claimed Trump won and Biden is illegitimate.
- Idaho's platform included extreme planks like no exemptions for the life of the mother and suggesting the repeal of the 16th Amendment.
- This move by Idaho Republicans, known for being one of the most dependent states on federal support, is seen as surprising.
- Despite adopting extreme positions, Idaho Republicans didn't go as far as embracing Trump's baseless claims about the election.
- Idaho's decision not to adopt the rejection of the 2020 election results is a significant blow to Trump's attempts to rally support.
- The rejection of Trump's claims didn't even make it out of committee during the Idaho convention.
- This move from Idaho Republicans, a traditionally safe area for the party, is a clear message that they are not willing to be swayed by Trump's narrative.
- The unique development in Idaho's platform, despite its extreme elements, shows a departure from supporting Trump.
- The rejection of Trump's claims by Idaho Republicans is a notable and unexpected turn of events within the party.

### Quotes

- "They're willing to adopt these just wild party planks, and that's fine. It's their party."
- "That is really bad news for Trump."
- "For a state platform that was willing to be seen as incredibly extreme to not adopt that language, that is a slap in the face."
- "I imagine there will be some Idaho potatoes and ketchup on the wall when he hears the news."
- "Even with all of the extreme language, they're not backing Trump anymore."

### Oneliner

The Republican Party in Idaho adopts extreme planks but surprises by not backing Trump's election claims, delivering a significant blow to his support.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact Idaho Republican Party officials to express support for their decision not to adopt rejection of the 2020 election results (suggested).
- Attend local political events or meetings to stay informed about party platforms and decisions (exemplified).

### Whats missing in summary

Insights on the potential impact of Idaho Republicans' decision on future party dynamics and Trump's support base.

### Tags

#RepublicanParty #Idaho #Trump #ElectionResults #PartyPlatform


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about how Trump lost Idaho.
The Republican Party there, the state level,
they just had their convention,
as many states are doing right now,
and they're setting up their party platform.
The Republican Party in Idaho decided
that they had no problem with being seen as,
let's just say an outlier,
being seen as a bit more unusual.
Some might use the word extreme.
They adopted a lot of unique pieces to their platform.
One is that, according to their platform,
there will be no exemptions for the life of the mother.
If mom's life is in jeopardy, well, it doesn't matter.
You know, no treatment.
You still have to have the birth.
Now, how they expect the pregnancy to be completed
if mom goes, I will never know,
but this is the sort of thing they adopted.
It's extreme by anybody's measure, right?
They also adopted language suggesting that the 16th Amendment
to the US Constitution be repealed.
That is taxation at the federal level,
which is unique coming from Idaho,
because I think they're rated the sixth most dependent state
on the federal government.
So that seems odd to me, but whatever,
they're willing to be seen this way.
They're willing to adopt these just wild party planks,
and that's fine.
It's their party.
They can do what they want.
You know what didn't get adopted?
Rejection of the 2020 election.
Other states passed that.
Other states passed language that said,
you know, hey, Biden's illegitimate or whatever.
Trump really won.
The Republican Party put that into their platform
in other states, but not in Idaho.
Idaho, a state that by a four to one margin
said that mom's life doesn't matter.
They were like, no, no.
Believe in Trump's baseless claims?
No, that's a bridge too far for us.
Even we're not that out there.
That is really bad news for Trump.
It's going to be incredibly hard for the former president
to galvanize any support, to galvanize that base,
when one of the more extreme state parties
left him out in the cold.
It didn't even make it out of committee.
That's got to hurt.
When it comes to the Republican Party,
I mean, Idaho, that's a pretty safe area.
For a state platform that was willing to be seen
as incredibly extreme to not adopt that language,
that is a slap in the face.
That is them saying, yeah, I mean, we might be nuts,
but I mean, hey, we can count.
Not going to fool us again.
That's bad news for Trump.
I imagine there will be some Idaho potatoes and ketchup
on the wall when he hears the news.
So while the platform that was adopted in Idaho
is obviously not ideal by any stretch of the measure,
that is an incredibly unique development.
Even with all of the extreme language,
they're not backing Trump anymore.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}