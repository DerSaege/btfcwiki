---
title: Let's talk about a phone number you need in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_CskN0vLigc) |
| Published | 2022/07/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the new number 988 as a mental health emergency hotline in the United States.
- Compares 988 to 911 but specifically for mental health crises.
- Mentions that currently, 988 connects callers to counselors, initially focused on suicide prevention.
- Describes future plans for 988, including mobile care units to provide mental health support.
- Advocates for shifting responsibilities away from law enforcement towards trained professionals in mental health.
- States that the United States government is investing a significant amount of money, around a quarter billion dollars, into developing 988 services.
- Envisions establishing physical locations similar to county clinics dedicated to mental health care.
- Expresses hope in this initiative as a positive step for a country with a history of neglecting mental health.
- Emphasizes the importance of remembering the number 988 for future use during mental health emergencies.
- Indicates that state legislation may be introduced to fund the 988 services, akin to how 911 is funded.

### Quotes

- "The number is 988. And it will be 911, but for mental health."
- "The idea is to eventually also have what amounts to like the little county clinics that exist, but for mental health."

### Oneliner

Beau introduces 988 as the mental health equivalent of 911, heralding a positive shift towards professional mental health support in the United States.

### Audience

Policy Makers, Mental Health Advocates

### On-the-ground actions from transcript

- Support state legislation designed to fund aspects of the 988 mental health emergency hotline (suggested).
- Memorize and spread awareness about the number 988 for mental health emergencies (implied).

### Whats missing in summary

Importance of advocating for continued funding and expansion of 988 services nationwide. 

### Tags

#MentalHealth #EmergencyHotline #SuicidePrevention #CommunitySupport #GovernmentFunding


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a new number,
a number everybody in the United States needs to memorize.
Even though right now, as it's being rolled out,
it is not yet what it will soon be, it's a first step.
And it's a good first step.
And eventually, this is going to be something
that is going to save a lot of lives.
The number is 988.
And it will be 911, but for mental health.
As it rolls out, I actually think it's active now,
you will be connected to counselors.
And this is, at least for the moment,
heavily aimed at suicide prevention.
But as time goes on, they appear to be gearing up
to add other services.
Right now, it's a counselor on the phone.
They have plans for mobile care units.
So rather than dialing 911 and sending the cops
to somebody's house, well, you can send to counselors.
This is a good thing.
This is something that's been needed.
There was a lot of campaigning around the idea
of taking some responsibilities away from law enforcement,
lightening their load, so to speak,
and putting it into the hands of people
who might be a little bit more trained
or educated on that topic.
This is the first step to doing it.
The United States government is devoting,
I want to say, like a quarter billion dollars to this
to kind of build up these capabilities.
The idea is to eventually also have what amounts
to like the little county clinics that exist,
but for mental health.
So there's an in-person place that you can go to.
In a country that has a very long history
of just ignoring mental health, this is a very hopeful sign.
The number is 988.
So file that away, 988.
And you will probably, depending on the state you live in,
you'll probably eventually hear about legislation
that's designed to fund some aspects of it,
because it's going to be a lot like 911.
The state is going to bear some of the cost.
So when that comes up, when they start talking about funding 988,
this is what it's for.
It is to create a 911 for mental health.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}