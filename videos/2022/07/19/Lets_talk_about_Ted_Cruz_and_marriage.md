---
title: Let's talk about Ted Cruz and marriage...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0cBJoax1tq8) |
| Published | 2022/07/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ted Cruz criticized the court's decision on gay marriage, advocating for states' rights.
- Numerous bills targeting the LGBTQ community have been introduced this year.
- The Republican Party uses scapegoating as a political strategy.
- The LGBTQ community is the current target for attacks on rights.
- Politicians often follow through on curtailing freedom to motivate their base.
- The GOP aims to secure the bigot vote by targeting marginalized groups.
- Actions against minority rights are not just empty promises.
- The push to limit rights is a consistent tactic to maintain power.
- Beau warns against underestimating the seriousness of these threats.
- The intention behind targeting rights is to create division and maintain hierarchies.

### Quotes

- "Don't mistake this for empty rhetoric."
- "They mean what they say."
- "The LGBTQ community is the current target."
- "Actions against minority rights are not just empty promises."
- "They're telling you who they are."

### Oneliner

Ted Cruz criticizes the court's decision on gay marriage, signaling a dangerous attack on LGBTQ rights and freedom by the Republican Party.

### Audience

Advocates for LGBTQ rights

### On-the-ground actions from transcript

- Stand with and support LGBTQ community members (implied)
- Advocate against discriminatory legislation (implied)

### Whats missing in summary

The full transcript provides additional context on the GOP's political strategies and the historical context of attacks on minority rights.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about Ted Cruz
and freedom
and them telling you what they're going to do
and people
hoping it isn't true,
not believing them.
If you missed it,
Senator Cruz,
he came out and said that he thought the court made the wrong decision.
He said he thought it was overreach,
that it should have been left up to the states
as to whether or not gay people should be able to get married.
They're coming for it.
It's not empty rhetoric.
You've had
minor players in the Republican Party say stuff like this. Like it or not,
Ted Cruz is a major player,
openly stating this.
This is policy.
They're coming after it.
That's their goal.
They're telling you they're going to do it.
It's not empty rhetoric the same way it wasn't empty rhetoric when they said
they were coming for Roe.
This year,
162
bills targeting the LGBTQ community
have been introduced nationwide.
162.
This year. It's only July.
They're coming.
They need a group to scapegoat.
It's the Republican Party
method.
They need somebody to kick down at.
They have to say, those people, we're going to take away their rights.
We're going to go after them.
You need to join us.
Bring people along.
And to say they're doing it in the name of freedom.
In 2016, it was immigrants.
Now,
they're going after
the LGBTQ community.
That's going to be their scapegoat.
And
they're going to expand it as wide as possible.
Now, he said that he doesn't believe that the court has the appetite
to
overturn that ruling.
And that may be true.
I wouldn't put any stock in that.
I also
wouldn't put any
faith in the idea that if the Republican Party
took the House and Senate,
that they wouldn't try to push it through on their own.
This is their move.
This is who they're going to try to scapegoat.
This is how they're going to try to get the bigot vote.
It's not rhetoric.
Don't mistake it
for
empty promises.
Politicians, yeah,
they lie all the time
when they talk about
the good things they're going to do.
But when it comes to targeting people,
when it comes to limiting freedom, when it comes to curtailing freedom,
they always try to follow through on that stuff.
Because that's what motivates their base.
Because if they can keep those people down,
it makes their base feel a little bit better about the position they're in.
They have somebody to look down on.
And that's the whole goal.
Don't mistake this for empty rhetoric.
Don't make the same mistake
that people made
when it came to Roe.
They mean what they say.
They're telling you who they are.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}