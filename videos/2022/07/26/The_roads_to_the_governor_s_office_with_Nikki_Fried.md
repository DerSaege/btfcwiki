---
title: The roads to the governor's office with Nikki Fried....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Hrlj7W_8BaY) |
| Published | 2022/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau and Nikki Freed talk about the impact of the race for governor in Florida and its national implications.
- Nikki Freed explains her role as the commissioner of agriculture and consumer services, overseeing various aspects like agriculture, food programs, energy, and security.
- Freed shares her experience working with non-Democratic officials in Tallahassee and her efforts to find compromise.
- The transcript delves into Freed's stance on cannabis legalization and her lawsuit against President Biden regarding gun rights for medical marijuana patients.
- Freed differentiates herself from her primary opponent, Chris, by discussing her long-standing Democratic values and commitment to various social issues.
- The importance of beating current Governor DeSantis is emphasized, with Freed labeling him as authoritarian and a threat to democracy.
- Freed outlines her plans if elected, including declaring housing and gas emergencies, focusing on education funding, expanding Medicaid, and protecting women's rights.
- The transcript touches on the need for air conditioning in prisons as a basic necessity.
- Freed criticizes Governor DeSantis for his actions against organizations like Disney and the cruise industry, showcasing a lack of civil discourse.
- Nikki Freed encourages voter participation in the upcoming primary and presents herself as a new type of leadership for Florida.

### Quotes

- "Try something new."
- "This isn't about me at all. It's about the people."
- "We haven't had a female governor of our state."

### Oneliner

Beau and Nikki Freed dissect the impact of the governor race in Florida, Freed's roles, values, plans, and the necessity to defeat DeSantis for the people's sake.

### Audience

Florida voters

### On-the-ground actions from transcript

- Support Nikki Freed in the upcoming primary election (implied)
- Educate yourself on the candidates' policies and backgrounds (suggested)
- Encourage voter turnout and engagement in the political process (implied)

### Whats missing in summary

Detailed insights into Nikki Freed's background, her campaign's grassroots support, and the urgency of challenging authoritarian governance in Florida.

### Tags

#Florida #GovernorRace #NikkiFreed #DeSantis #Election #Democracy #VoterEngagement #GrassrootsSupport


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to be talking
about the race for governor
and how it's gonna impact you and the nation.
And who better to have along on this little trip
than a candidate for governor herself?
Would you like to introduce yourself?
I would love to.
Well, first of all, Bo, thank you so much for having me on.
I've been watching all of your posts over the last months.
And so I was enthralled by some.
So I'm Nikki Freed.
I am our current statewide elected Democratic
of Agriculture.
And now I am running for governor of our state.
Born and raised in Miami.
Gator for any of my gators out there.
Triple gator, undergrad, master's, law degree.
Then was practicing law for about 50 years.
And then going off and running for office.
Some people call me crazy.
All right.
So for people who may not know,
why don't we fill them in real quick
on what the commissioner of agriculture does here?
Cause it's a little bit more than corn and cows.
Yeah.
So, you know, that was part of what I did a lot in 2018,
trying to explain to people
what the commission of agriculture does.
So obviously all of agriculture.
Agriculture is second largest economic driver
for our state.
Million people that work inside of agriculture
in our state.
$380 billion.
So that's agriculture aspect.
But I'm commissioner of agriculture and consumer services.
Though we oversee all of the food nutrition program
for the state.
I have the office of energy.
We do fair rides, profits.
We do security guards, the consumer fraud
and the gas pump that's on the gas station.
And to make sure there's no fraud there.
We oversee weights and measures
that you are at the airport.
And you would think that you're being scammed
on the weights when you have to put the 50 pounds
to be charged by the airlines.
We calibrate those.
And of course, concealed weapons is a big thing
that people don't understand.
Of agriculture.
We've got forestry.
We've got agriculture law enforcement.
It is about 4,600 people that work for the department.
I mean, different divisions.
And of course I oversee the hemp program.
Commissioner.
All right.
Okay, so you are working out of Tallahassee.
And who are your allies there?
Who are the other state elected Democrats
that you get to work with?
There are none.
There are not.
It is just me, myself and I.
There's four members of the cabinet.
So there is the governor,
the chief financial officer, attorney general and myself.
Those three are not Democrats.
We've got two US senators.
They're not Democrats.
So the Democrats that I get to work with
are our state senators and our state representatives.
But look, I actually tried.
I tried when I first got into office.
My dad is a diehard Republican.
So I was taught in a household.
I kind of listened to both sides of the conference.
I have friends on both sides.
I thought that this was an opportunity
to provide four members of the cabinet
that are all under the age of 15 with no families,
that we were gonna show the world and the nation
that what bipartisan looks like.
I have never been so wrong in my entire life.
But so it's all good.
It's all good.
There's times that we can find compromise,
but for the most part,
it's my team inside the top of the pack, who has my back.
All right.
Well, okay.
So we've definitely answered the electability question
when it comes to a Democrat in Florida.
You've already won the state once.
So let's dive into the hard questions,
the mud that is being thrown your way real quick.
Are you friends with Matt Gaetz?
No.
Okay, so here's the story, okay?
So we have, Tallahassee is a small town
in the Grand Forks.
And if you are a legislator
and if you are involved in politics in Tallahassee,
everybody knows each other.
We're the same age.
You know, everybody, when they go out at night,
everybody knows each other.
And of course we know that he's a strong proponent
of cannabis, though we work together
in the cannabis industry.
But outside of that, you know, it's crazy.
I mean, it really is.
I mean, we know each other, of course.
And look, and like I said, and what I just said,
you know, I wanna work across the aisle
on ways to get things accomplished.
Of course, my first year, I went up to DC
and lobbied for the disaster package
for the panhandle from Hurricane Michael,
was making sure that we brought resources back down
for the citrus industry.
And we were going after the implementation of the USMCA,
which was NAFTA, because that hurts Florida agriculture.
So I lobbied everybody, including Matt Gaetz.
But outside of that, I think the things that he does
right now, the things that he says,
ooh, he's drinking some type of water that I'm not,
I don't know what's in it.
Right.
For those that aren't familiar with Tallahassee,
the government area, it's like 10 blocks.
You know, when people think of Florida,
most people think Miami's the capital.
It's not, it's a relatively small place in North Florida.
Okay, so why did you sue Biden?
I actually saw your clip on this.
I love you.
When I first was running for office,
I made it very clear that I am pro cannabis legalization.
In fact, my first announcement video in 2018,
because I came from the cannabis industry.
That's where Matt and I really had a lot of interaction.
I came from the cannabis industry
and to get to legalization, not only in Florida,
but across the country.
And so we even said in our first opening video,
you know, that the under regulation of guns,
but the over-regulation of cannabis,
and that's something that seems to be really off.
And as I've traveled the country,
and especially here in the state of Florida,
you know, I hear from our medical marijuana patients
all the time, something that I campaigned on,
fighting for our medical marijuana patients.
And I hear from our patients,
our VAs can't get their benefits.
I hear from people that are losing their jobs
because they're testing positive for THC.
People can't rent their, can't get rentals and houses,
and they can't purchase new guns.
And so I got asked that question
as I oversee the concealed weapons program,
can I have both my cards,
my concealed weapons permitting card,
as well as my medical marijuana card?
I have both.
The issue is not the getting of the concealed permit care.
The issue is purchasing a gun.
And so if somebody has a card
or is a medical marijuana patient,
or even in a legal state,
that on the ATF form,
they've added an asterisk,
and it's not President Biden,
it happened way before President Biden,
but it's an asterisk that got added
on a question of basically being under,
are you under the influence of
or addicted to a controlled substance?
And they added the asterisk that says,
even if your state has legalized
or has a medical program,
it's still considered federally illegal.
And I just think that that's wrong.
That why should somebody have to choose
between their two constitutional rights,
constitutional right for medical marijuana in Florida
and their second amendment.
And this is just kind of one of those issues
that actually creates more safety
because right now people in the medical marijuana field,
if they want a gun,
they're going onto the black market
and not getting the background checks.
If somebody has no guns and wants marijuana,
they're going onto the illegal market
and not getting onto the regulated program.
And so both creates unsafe environments.
This was an attempt to hopefully
get the federal government to wake up
and say enough is enough.
Let's legalize.
Let's see that we have issues here.
And so that's what the point of the lawsuit is.
Right.
And there is a,
it looks like Schumer is gonna introduce something.
So we'll see what happens.
So did I miss any of the mud
that's getting thrown your way?
Is there anything else you want to address on that front?
I don't think so,
but I think that it's interesting
because everybody got out,
oh my gosh,
she wants to go back into the ethos
and we're supposed to be Democrats.
And I say this very clearly.
Democrats are not trying to take people's guns.
We all want to have safety.
We all want to make sure
that we have responsible gun owners.
And so we had actually a Zoom after my lawsuit dropped
with a lot of the gun sense organizations.
I had a whole Zoom with a whole bunch of them
and I won't want to name names because they wanted,
it was kind of an intimate setting.
And after I explained the lawsuit,
they're all like, well, this makes sense.
And so it's throwing,
it's people throwing rocks,
but not really understanding the issue.
Right.
I mean, when you get to the point of
this means more background checks,
that's kind of the moment when people are like,
oh, well, maybe this isn't such a bad idea.
Let's see.
Okay, well then,
so differentiate yourself from Chris,
because that is who you were facing off with in the primary.
Yes, everything.
I've been a Democrat since I was 17 years old.
We can start there.
I have been a fighter for my whole life.
Something new.
He has for his entire career,
besides when he became a Democrat,
he has anti-Trump, supported the NRA,
took money from the NRA,
went out of his way to actually appoint
some of the most extremist judges to the bent.
And now we have a Supreme Court justice,
Florida Supreme Court justice
that's gonna overturn our right to privacy.
He has the infamous picture of holding up chains
because he wanted to put our prisoners back in chains,
back, look in the very slavery aspect
and making criminal justice
and making it more stringent and harsher on crime.
And I was a past public defender.
So the time that he was trying to put more
black and brown people in jail,
I was defending their rights.
So that's, you know,
everything that is wrong with politics is Charlie.
He has been a Republican.
He's been an independent.
He's been a Democrat.
He's lost three times statewide.
Has never won as a Democrat statewide.
And people are tired of career politicians.
They want somebody who's a fighter,
who is true to themselves.
And we just can't trust Charlie.
People may not always agree with everything I say,
but I'm not gonna tomorrow say something different
than somebody else or make promises that I know I can't.
I'm a woman of my word.
And that's a big difference between myself and Charlie.
So the thing is, he is Florida politics defined.
When you and I were being told to get home
before it was dark, he was already in office.
When you were finishing up your degree,
he was a Republican under Jeb Bush.
How are you going to beat and overcome his machine
and that name recognition?
We're doing it.
We're absolutely doing it.
You're seeing momentum on the ground
because look, we are at a time right now
that we are seeing our democracy
be the culture wars all across the country.
And the climate is the biggest threat to our democracy.
And Democrats want, they want somebody on DeSantis
and they want to fight her.
And I am both.
I've been taking on DeSantis for three and a half years
in cabinet meetings and policy debate.
I've been our standard bearer.
And so when people get introduced and see me
and hear me and understand,
you know, I've been practicing law for nine years.
I'm a statewide elected Democrat.
I know how to win our state at the policy wonk.
It's a breath of fresh air.
And we know that as soon as we get used to people,
they're coming to us and talk about what happened.
We were supposed to both be at an event in,
which is one of the largest five plus communities
in our county, if not in almost all the South Florida.
And there was probably about 300 people that showed up
because they thought that we were going to both
be there in person.
And Charlie decided to go do debate prep instead of,
and so he zoomed in to the event
and the people there were so insulted
that he didn't have the time,
even though he promised to be there in person,
didn't show up.
We got there.
I was honest.
I was pragmatic.
And at the end, everybody said,
look, I was thinking about voting for Charlie
because I knew him.
You got my 110% support.
And that's what we're doing all over the state.
And that's what happened after Roe v. Wade.
Women are mad.
They're angry.
And they know that the only way that we really,
truly are going to have protection
is to put a woman in charge.
And you kind of touched on something
that we're going to get to here in a second,
but other than you have a lot of demographics
that are coming out in favor of you,
but where are your endorsements at?
So a lot of my endorsements are grassroots.
I've got the Democratic Black Caucus.
I've got the Democratic Environmental Caucus,
College Democrats, a lot of the organizers on the ground,
the people that are actually out there in the communities
that are coming out and supporting us.
We've got electeds, but truth be told,
I didn't seek out the electeds.
That's what old school politicians do.
I went out and I sought after the support of the people.
And that's what you're seeing all over my social media.
That's why you see a lot of hashtag something new
because I would rather the support of that single mom
who's working at Publix and has two jobs,
I'd rather her support than the city commissioner
from that community.
Because that means that she knows
that I'm going to do something good for her,
that I'm going to protect her.
So the local electeds, the statewide elect,
the state electeds,
they're looking for their political careers
and they're making calculated political decisions.
I'm looking out for the people.
And so when I get somebody's vote,
that means the absolute world.
Okay.
So getting past Crist, that's just the first hurdle.
And this is why I really hope that people watching
understand that the Florida election is of national interest
because you're going up against DeSantis.
You've called him an authoritarian in the past.
So how important is it to beat him,
not just personally, but nationally?
It's nothing to do with me personally.
This is, I can go back to practicing law.
I can go back to fighting to get legalization done
in Washington, DC.
This isn't about me at all.
It's about the people.
And being a member of the cabinet,
I have been watching what he has done to our state,
taking away people's rights to vote,
taking away people's rights to protest.
What he did during the pandemic,
it was his way or the highway,
putting in somebody who should never even be a doctor
as our Surgeon General,
going after our school board members,
going after our teachers,
going after kids that wear masks,
going after the design industry.
He has become a dictator,
taking away power from the Supreme Court,
taking away power from the legislature
and from the cabinet.
And so it is so essential that we win in November
because our state is so divided
because of culture wars that he's created.
When at the end of the day, people just wanna know,
are you gonna help me pay for my rent down?
Are you gonna make sure that I can help pay my electric bills
and food on my plate and have a good job
that I can work with dignity?
That our seniors know that they,
towards the latter chapters of their lives,
that they aren't gonna be stressed
about their increase of healthcare costs.
All of these issues are so important
to the people of our state.
But we also know that democracy is on the line
because if we don't win, we know where he's going.
He wants to run for president of the United States.
And if he is to get there,
I don't know what happens to our black community.
I don't know what happens to our voting rights.
My God, I don't know what happens for LGBTQ community.
Everybody that doesn't agree with him
is on the chopping block
and I just couldn't sit back and watch.
So it is essential that we win in November
and bring decency and compromise
and common sense back to our state.
That's right.
And for those in other states,
if the Republican party realizes
he can't carry Florida as a governor,
they know he won't carry it as the president
as that candidate.
So he won't be able to run that way.
That's correct.
So, I mean, I definitely think that
if he was to be beat by something new,
that would probably be the end
of his presidential aspirations.
Okay, so what are you gonna do for the people of Florida?
I have a laundry list.
Day one, we're declaring a housing emergency
because that's exactly what it is.
It is an emergency in the state of Florida.
People can't afford their rent.
They can't afford their property insurance.
They can't afford their mortgages.
They can't afford their property taxes.
Would also be declaring,
if we're still having at that point, a gas situation,
be declaring an emergency on gas.
So that way we can fix that issue.
We gotta be working on our environment.
I would also see how we can make sure
that we are moving our state forward.
I've been doing it as commissioner of agriculture.
I put together a 72 page plan
of where we need to be when it comes to energy
and coming to, I don't wanna call it climate crisis.
I wanna call it climate rescue
because that means I have a plan
and I would rescue our climate.
We are 49 in the nation
when it comes to funding of our education.
So my commitment is that we are going to double
the per pupil funding when I am governor.
We've gotta make sure that we're expanding Medicaid.
We gotta protect a woman's rights.
So I've said that not only will I be declaring an emergency
to make sure that we are not sending money
down to our state attorneys and our prosecutors
to prosecute doctors and women
for going through with an abortion
outside of the 15 weeks that is law right now.
We can talk through what's happening
with our mental and physical disability community,
that we have those that are still on the wait list
of tens of thousands that are on the wait list.
I mean, the list goes on and on.
We've had 28 straight years
of one party control of our state.
And we are seeing the demise of all of these things
that have to become a top priority for us.
All right.
Let's see.
What was something recent?
Air conditioning in prisons.
So what happened with that?
Because that came out of nowhere, or it seemed to,
at least for where I was sitting.
You know, first of all, as a past public defender,
I know the conditions of our jails and prisons.
And I've known that, but I've been talking a lot more
on the campaign trail, a lot of these spouses
and a lot of family members who have family
outside of our prison system.
And for them to tell me that we don't have air
inside of our prison systems,
I can't even imagine living in that.
These are still human beings.
Yes, they've committed a crime.
Yes, they have to serve their time,
but they're still people.
We still need to feed them.
We still need to clothe them.
We still need to give them the basic necessity.
And to live here in the state of Florida
with the temperatures that we have,
and I'm talking about climate rescue,
the fact that the world is literally burning,
that we're not doing something
like putting air conditioning in,
seems just unfathomable.
You know, this is torture.
I remember after Hurricane Ranger down in Miami,
not having air conditioning for a week.
And I was able to open windows and walk outside.
And we were miserable that we had to leave the house
and go to another part of the electricity
so we can have air conditioning.
These are still human beings.
And this is not putting a spa,
we're not giving them five-star restaurants.
Air conditioning seems basic,
basic living here in the state of Florida.
Well, yeah, it definitely does, especially down,
I mean, true here, but especially down South.
And I was just, it kind of struck me as odd
because that in this area, like that became a thing.
Like all of the jails here had AC like in the 90s.
So like hearing that there were other parts of the state
where that wasn't happening was surprising.
And then, okay, so what else do we have here?
Do you want to talk about anything about Disney or no?
Yeah, this is so crazy about this guy.
Like I need everybody to wake up.
Like this was done for revenge.
Like, how dare you cross me?
I'm gonna show you.
This is a governor of the state of Florida,
the third largest state in the nation.
And this is how you respond
to one of the biggest employers of our state,
one of probably the biggest tourist industry of our state.
But he doesn't just do it there.
He did it to the cruise line industry.
He does it if, you know,
I remember even getting out the vaccines,
part of a community said,
you only are putting vaccines in this part of the town.
He said, well, then I don't have to put the vaccine
in there at all.
This guy has no respect,
whatever happened to civil discourse
that you may not disagree, may not always agree,
but you still have conversations.
This guy won't do that.
The opening of his state of the state address
this past legislative session,
he started it with, we were right and they were wrong.
And I'm sitting in the front row going,
you govern over 20 states,
and you just told half the state that they were wrong.
How about you find a way to bring people together,
to find ways to lift people up, to be inspirational,
to make people believe in that American dream
of activism and freedom, not what this guy does.
And that's something that I need people to be waking up
for, it's not a good guy.
There's nothing here.
There's no heart, there's no passion,
no compassion for anybody.
And we got to get them out of here.
So is there anything that you feel like we missed,
or is there anything else that you want to say
to the people of Florida or the US,
because I know you've got to go.
Well, first of all, we've got a primary August.
So make sure that you are on your ballot,
that you're finding ways that you're voting.
I hope I have earned your vote,
but that momentum out of the primary,
that excitement, that effort is what we're going to need
to take on this.
And so I'm asking everybody for their vote,
try something new.
We haven't had a female governor of our state.
And so we have a different type of leadership,
somebody who will roll up sleeves, get to work,
not worry about her ego, bring in people.
I didn't let my department and fire all the Republicans.
That's not who I am.
If you are a hardworking person
that has your right mindset on doing good,
I don't care if you're a Democrat or Republican, I am,
but we got to win.
And so I'm somebody who is not a career politician.
I have been doing service to our state all life,
and that's what you see, and that's what you get.
All right, all right.
Well, I really appreciate you stopping by to talk.
Talk to us and everybody at home, I guess that's it.
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}