---
title: Let's talk about Team Pence and Marc Short talking to a grand jury....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H0mMB-MsdUw) |
| Published | 2022/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- High-ranking individuals from Pence's inner circle testified before a federal grand jury last week.
- Commentary has shifted from DOJ inaction to DOJ involvement with Trump.
- DOJ's investigation largely occurs out of public view, focusing on the fake electors scheme.
- Activities such as subpoenas, FBI and National Archives involvement, and Eastman's case are related to the scheme.
- Testimony from Pence's chief of staff and another individual fits neatly into the investigation.
- Speculation surrounds whether Trump is involved, with indications pointing in that direction.
- DOJ actively investigates the fake electors scheme, indicating White House involvement.
- Testimonies suggest Pence may not need to testify if individuals are forthcoming.
- Rumors suggest questioning focused on Giuliani and Eastman, related to the fake electors scheme.
- The investigation likely extends beyond what is currently publicly known.

### Quotes

- "DOJ is actively, very actively, looking into the fake electors scheme."
- "They believe this scheme reaches into the White House."
- "It's not really a turning point in the investigation."

### Oneliner

High-ranking Pence associates testify before grand jury, signaling DOJ's active investigation into fake electors scheme and potential White House involvement.

### Audience

Political commentators, concerned citizens

### On-the-ground actions from transcript

- Stay informed on developments in the investigation (suggested)
- Advocate for transparency and accountability in government actions (implied)

### Whats missing in summary

Insights on the potential impact of the investigation's findings on future political landscapes. 

### Tags

#DOJ #Investigation #FakeElectorsScheme #Pence #WhiteHouse #Transparency


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about people from
Team Pence talking to a grand jury because news has broke that a couple of high-ranking people
in Pence's inner circle have testified before a federal grand jury and they did this last week.
Because of this development a lot of commentary has shifted when you're talking about political
commentators. People who were saying DOJ isn't doing anything have now jumped to DOJ has Trump
and then there are people saying that this doesn't fit with what we know about DOJ's probe.
So we're going to talk about that but before we get into it I kind of want to address something
because since this news broke I had more than one message come in basically saying hey how did you
know because in the last video I said I thought high-ranking Republicans were going to end up
getting arrested soon. Sometimes somebody has a source that is so well placed they can't be
disclosed and other times there's coincidence and that's what this is. I had no idea that
this was happening today. That's it's just coincidence. Okay so let's talk about it not
fitting neatly because that has a lot to do with the people who say DOJ isn't doing anything.
We've talked about it on the channel. We have little bits and pieces that show up that we get
to hear about but the majority of the investigation is occurring outside of public view. We know about
the subpoenas and they were all related to one thing. We know about the FBI along with cops from
the National Archives going out and talking to people. We know about what happened with Eastman.
All of this has to do with the fake electors scheme. That's what all of these little bits and pieces
that we get to hear about. They're all focused on that. So DOJ's activities are at least somewhat
focused on that. We could just be hearing about this stuff first because this is the easiest part
to prove because a whole lot of this was in writing. So you have that going on. The two people
who testified, they would also fit very neatly into this line of investigation. If the Trump
White House specifically to quote the hearings, if team crazy within the Trump White House
asked Pence to stall, overturn, throw away some electors, any of this stuff, any of this that
could be construed as being a criminal act, it would have gone through Mark Short,
Pence's chief of staff, who is one of the people who testified. The other person who testified
would also know about it. So it does in fact fit. It fits very neatly. Now as far as them
having Trump, yeah I'm not ready to say that yet. I think they have other people, but I don't think
they have Trump yet. That does appear to be where the investigation is going, but it doesn't look
like we're at the point where they hop out of the van and pull the mask off of the person behind
this little caper yet. So what we do know at this point, what we can confirm, is that DOJ is
actively, very actively, looking into the fake electors scheme. But honestly we already knew this.
What this really shows is that they believe this scheme reaches into the White House.
That's the real takeaway here. As far as it meaning that they have Trump, I don't buy that.
It's also worth noting that if the people who are testifying, if the people who have testified
from within Pence's inner circle are being very forthcoming, we may not see Pence testify. He may
not have to because all he would be doing is relaying what they told him because all of this
stuff would have traveled through the people who have testified. The rumor mill says that the
questioning was about Giuliani and Eastman, which again would line up with the fake electors scheme,
but that's also a rumor. We don't know that. What we know is that they testified before the grand
jury. It's worth noting that there is probably a lot more being investigated. This is just
what's coming up in the public view because they can proceed more openly because a lot of it is
written down. This might be a turning point as far as coverage is concerned, but it's not really a
turning point in the investigation. Just maybe more of public perception of what's going on
because now names that people recognize that have been in headlines before, they're now being asked
questions not by the hearing, but by the Department of Justice. So that's where we're at.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}