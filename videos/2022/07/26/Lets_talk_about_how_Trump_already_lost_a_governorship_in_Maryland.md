---
title: Let's talk about how Trump already lost a governorship in Maryland....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RJ8iSsNaGz4) |
| Published | 2022/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's influence in the primaries has likely cost the Republican Party a governorship in Maryland.
- Trump threw his weight behind a far-right candidate named Daniel Cox in Maryland, opposing the moderate Republican governor's chosen successor.
- Democratic Governors Association allegedly helped Daniel Cox win the primary because they saw him as an easy opponent to beat.
- Daniel Cox is described as a "Q whack job," while the Democratic nominee, Wes Moore, is a war veteran and Rhodes Scholar.
- Beau predicts Wes Moore will win the election in Maryland due to the circumstances created by Trump.
- Republicans are facing a dilemma where they need Trump's endorsement to win the primary but struggle in the general election with his endorsement.
- Beau places the blame on the Republican Party leadership for allowing Trump's influence to shape their candidates.
- The situation in Maryland is likely to repeat in other areas where candidates endorsed by Trump may struggle in general elections.
- Despite some slim chance for Cox to win, projections suggest Wes Moore will win by a significant margin, showcasing Trump's impact.
- Beau concludes by reflecting on the consequences of Trump's influence on Republican candidates.

### Quotes

- "Republicans are in a position where they can't win the primary without Trump's endorsement, but they can't win the general election if they have Trump's endorsement."
- "It is the Republican leadership's fault that this is happening because they have refused to lead."
- "That's Trump at work right there."

### Oneliner

Trump's influence in Republican primaries may cost them the governorship in Maryland, revealing a cycle where candidates need Trump to win primaries but struggle in general elections due to his endorsement.

### Audience

Political observers, Republican voters

### On-the-ground actions from transcript

- Support candidates based on qualifications and policies rather than blind loyalty to a particular figure (implied).

### Whats missing in summary

The full transcript provides detailed insights into the impact of Trump's endorsements on Republican candidates, illustrating a broader pattern of challenges faced by the party due to internal dynamics and lack of leadership.

### Tags

#Trump #RepublicanParty #Governorship #PrimaryElections #PoliticalInfluence


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about how it's incredibly likely
that Trump has already cost the Republican Party a governorship
due to his influence in the primaries.
We've talked on the channel about how Trump is throwing his weight behind candidates that
in many cases their main qualification is that they will echo his claims.
They will do what they're told.
In Maryland, there's a Republican governor, Hogan, moderate and pretty well liked.
Hogan had a chosen successor, but Trump didn't like that.
So Trump waded into the primary and threw his weight behind a person named Daniel Cox,
a person that the Democratic Party saw as so easy to beat,
the allegation is that the Democratic Governors Association actually helped him win the primary.
So rather than having a moderate candidate in Maryland, which is a very blue state,
there is now a far right Trump endorsed candidate.
Daniel Cox has been described as, quote, a Q whack job.
That's the Republican nominee.
The Democratic nominee is Wes Moore, a veteran of the war in Afghanistan,
if I'm not mistaken, a literal Rhodes Scholar.
I know it's a little bit early to call a race,
but I'm fairly certain that Moore is going to win this one.
It would take something wild to alter the outcome of this.
And make no mistake about it, this happened,
the Republican Party is going to lose the governor's office in Maryland because of Trump.
Because he couldn't stand the idea of somebody winning without his approval, I guess.
And this goes back to what we've been talking about since Trump started getting involved
in the primaries.
Republicans are in a position in a whole lot of places where they can't win the primary
without Trump's endorsement, but they can't win the general election if they have Trump's endorsement.
Trump did this, but at the end of the day, it's the Republican Party's fault.
It is the Republican leadership, the establishment of the Republican Party,
it is their fault that this is happening because they have refused to lead.
Because they haven't done what they should after the 6th.
They didn't take control of their party.
So now Trump is out there.
This scenario, it's going to play out again.
I honestly see zero chance of Cox winning.
It's politics, so sure, 1% chance.
But the early projections I've seen show more winning by more than 25 points.
That's Trump at work right there.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}