---
title: Let's talk about military recruitment problems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KJe-eSmqTsc) |
| Published | 2022/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing recruitment issues in the Department of Defense (DOD) due to trouble recruiting.
- Not surprised to find readiness issues in the establishment.
- Majority of recruitment issues not related to what Beau has discussed previously.
- Target recruitment age for new enlistees is 17 to 24.
- Reduction in eligible candidates from 29% to 23%.
- Criminal records, substance use, and obesity contributing to the shrinking recruitment pool.
- 57% of those not interested in serving fear emotional or physical harm.
- Army hitting only 68% of its recruitment goals through April.
- COVID and a strong job market impacting recruitment challenges.
- Falling patriotism and disillusionment contributing to lack of interest in joining.
- Critiques MAGA crowd's perception of patriotism.
- Issues with the demographic makeup, public health, and economy affecting recruitment.

### Quotes

- "Y'all aren't the solution. Y'all are the problem."
- "Not exactly Medal of Honor material, just saying."
- "If you don't want to make the country better and move forward, you are not a patriot."

### Oneliner

Addressing recruitment issues in the Department of Defense, Beau breaks down the factors impacting the shrinking recruitment pool, from criminal records to falling patriotism, and questions the country's values and treatment of its citizens.

### Audience

Recruitment officials, policymakers, concerned citizens

### On-the-ground actions from transcript

- Advocate for policies that improve recruitment eligibility (implied)
- Support programs that address substance use and obesity in communities (implied)
- Promote a positive and inclusive national narrative to boost patriotism and interest in service (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the complex factors affecting military recruitment, including societal issues and political influences, offering a deeper understanding of the challenges faced by the Department of Defense.

### Tags

#Recruitment #DepartmentofDefense #NationalSecurity #Patriotism #SocietalIssues


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about recruitment issues at DOD.
Because they have said, hey, we're having trouble recruiting,
and people have come out with their reasons.
And they're trying to spin it to fit their narrative.
I, for one, am shocked to find readiness issues
in this establishment.
Shocked, I say.
But to be honest, most of it actually
doesn't have to do with what I've
been talking about on the channel for years.
OK, so let's run through the biggest issues when it
comes to military recruitment.
The target recruitment age for new enlistees
is 17 to 24.
Under normal circumstances, 29% of 17 to 24-year-olds
are eligible for military service.
They fit the requirements.
Right now, that number is 23%.
That's a reduction of almost a fifth.
It's a big deal.
That is actually the majority of it.
A smaller recruitment pool means less recruits.
You don't have to have a high ASVAB score to figure that out.
So why did the recruitment pool shrink?
Number one, criminal records.
The over-criminalization of things,
treating everything as a felony, probably isn't a great idea.
It is now having national security impacts.
The next, substance use.
Yeah, yeah, that's still disqualifying.
And then the last one is obesity.
That has shrank the recruitment pool.
Therefore, there's less recruits.
Seems simple.
Now, add into that that in their survey, in DOD's survey,
they found out that 57% of those who don't want to serve
think that service will emotionally or physically
break them.
Why would they think that after 20 years of war,
seeing their friends with prosthetics and PTSD?
That probably factors into it a little bit.
Now, so how bad is the problem?
Through April, the Army was hitting 68% of its goals.
Right?
OK.
So a fifth, about 20%, gets taken care of
by the smaller recruitment pool.
Explains it.
But that doesn't take us down all the way.
That only takes us to 80%.
And we need to get to 68%.
COVID makes it harder to recruit.
It's harder to schedule meetings.
It's harder to have events that recruiters can show up to,
and so on and so forth.
The other thing is there's a hot job market right now.
I know there's a lot of doom and gloom about the economy,
but the job market is doing incredibly well.
In fact, it's doing as well as it has in 40, 50 years.
Incidentally, the last time DOD had this kind of issue
was in 1973, when a lot of these other factors were at play.
OK.
So that takes care of a lot of it.
There's no way to quantify how much COVID or the job market
is impacting recruitment, but it's
going to be a pretty big chunk, right?
Because right now, we're only talking about trying to figure
out that extra 10%.
OK.
So that leads us to the fact that only 9% of those eligible
have any interest in joining.
One out of 10.
One out of 10 of that 23% have interest in joining.
Why?
Falling patriotism.
That has a lot to do with it.
And right now, the MAGA crowd, they're like, see?
We told you.
No.
Y'all aren't the solution.
Y'all are the problem.
You want to go fight and die for something that isn't great?
Probably not, right?
It's not great now.
We got to make it great.
You want to be called losers and suckers
if you don't make it back?
Probably not.
Not great for morale, especially when that claim might
come from the sort of person who would want to award themselves
the Medal of Honor, even though they didn't have the courage
to lead their troops, stayed hidden away in the dining room
while others went and did the fighting.
Not exactly Medal of Honor material, just saying.
Do you want to fight for a country that is actively
trying to take away your rights?
No.
That wouldn't make sense.
Do you want to join a place, join an outfit that
might send you to a state where you, your dependents,
are going to be treated as second-class citizens,
where your kids or spouse can't get the health care
that they could elsewhere?
I mean, from what I remember, I think the goal of DOD
was to try to recruit smart people.
So what about fighting for a country that
doesn't own up to its past?
Make America great again.
And then when you find out when the again was,
when they're referring to, it's a period
where a whole lot of people who wear a uniform
wouldn't have rights.
I mean, that can't be good for morale.
That's probably not good for recruitment.
What about if a major political party in the country
votes against getting extremists out of the military?
You want to fight alongside people like that?
Probably not.
What about voting against them getting health care
if they were exposed to toxic substances?
Republicans did that too.
You're not the solution.
You're the problem.
What you want, the idea, is to wave that flag
and teach people lies that they know are lies in school,
indoctrinate them, and claim that that's patriotism.
It's not.
If you don't want to make the country better and move forward,
you are not a patriot.
These are the issues.
These are the issues.
The overwhelming majority of it has
to do with the demographic thing.
A large portion of what is left has
to do with the current situation involving the public health
issue and involving the economy and a strong job market.
The rest of that, yeah, it's up for debate.
But I can't see people wanting to fight and lay down
their lives for a country that tells them constantly
that they're not welcome, that they're not wanted,
that they're the problem, that allows them to be gunned down
with no accountability, that allows
them to be othered and villainized
for the sake of political expediency.
If the government, namely one political party,
will other them and leave them behind just for votes,
I imagine they would do it in other places.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}