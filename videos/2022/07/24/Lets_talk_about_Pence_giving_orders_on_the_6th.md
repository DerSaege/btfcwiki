---
title: Let's talk about Pence giving orders on the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=T8ZTytjqceE) |
| Published | 2022/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pence did not have the authority to order the National Guard to do anything.
- Acting Secretary of Defense Miller, who has the authority, likely received a strong suggestion from Pence.
- Miller could have then issued orders based on Pence's suggestion.
- General Milley might have been directed by Miller to assist Pence if needed.
- The media misunderstanding led to the perception of Pence giving orders, when it was actually Miller who did.
- There was no de facto 25th Amendment situation, and it's unlikely that Miller or Milley spoke directly to the Commander-in-Chief during the Capitol issue.
- Tough questions may arise due to the media's portrayal of Pence giving orders, but they can be answered by clarifying the chain of command.
- It's improbable that anyone bypassed the Commander-in-Chief due to concerns of a coup attempt.

### Quotes

- "Pence did not have the authority to order the National Guard to do anything."
- "The media misunderstanding led to the perception of Pence giving orders."
- "Tough questions may arise due to the media's portrayal of Pence giving orders."

### Oneliner

Pence lacked authority to command National Guard; media confusion led to misinterpretation.

### Audience

Citizens, Military Personnel

### On-the-ground actions from transcript

- Verify facts about chain of command (suggested)
- Seek clarification on military orders (suggested)

### Whats missing in summary

Detailed breakdown of military chain of command and decision-making process.

### Tags

#ChainOfCommand #Misinterpretation #MilitaryOrders #Pence #MediaConfusion


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Pence giving orders,
because I got a message about that.
This is something that is going to come up at some point in the future.
I have hinted to a possible discrepancy since this happened.
And I recently made a comment about Pence maybe not having the authority
to order some of the things that he is credited with ordering.
Got a message saying, hey, after you said that,
I looked into the military chain of command.
Am I reading this right?
And it has a copy of the chain of command.
Yeah, you're reading it right.
The vice president is not in it.
Vice President Pence absolutely did not have the authority
to order the National Guard to do anything.
And I'm sure that's not what happened.
My understanding is that Pence spoke with the acting Secretary of Defense Miller,
and I believe General Milley as well.
While Pence has absolutely no authority to do anything like this,
acting Secretary of Defense Miller does.
Now, what I think is happening is that people are mistaking Pence's strong suggestion
for an order to the acting Secretary of Defense, who has full authority,
number two in the chain of command.
So Miller, acting on that strong suggestion, gave the orders.
And if he hypothetically told General Milley, give Pence whatever he needs,
that could totally explain some of the units that were rumored to have been mobilized
if there was an issue with the National Guard,
if the National Guard couldn't complete the objective.
Because the acting Secretary of Defense does have the authority to issue orders.
I'm sure that this is just a misunderstanding coming from the media.
I'm certain that a de facto 25th Amendment did not take place,
even though from my understanding, neither Miller nor Milley spoke to the Commander-in-Chief
on that day or during the issue at the Capitol.
It appears that they got guidance, suggestions.
Perhaps they were just trying to get information about what was happening from the Vice President.
But they absolutely did not take orders from the Vice President.
Those orders came from the acting Secretary of Defense, Christopher Miller,
who certainly knew his place in the chain of command.
So, yeah, at some point, there's probably going to be a lot of questions about that.
And it could be explained that easily.
However, because the media keeps using the term order from Pence,
eventually there are going to be people who are going to be asking tough questions.
I am certain that those tough questions will be answered in a way that is very close to what I just said,
because I'm positive that's what happened.
I don't believe that anybody circumvented the Commander-in-Chief
because they believed he was involved in a coup attempt.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}