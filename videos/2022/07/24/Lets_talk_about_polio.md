---
title: Let's talk about polio....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jF7UPZ-Ws8I) |
| Published | 2022/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on a concerning situation involving polio, a disease thought to be eradicated.
- Reports a case of a young adult in New York contracting polio, the first confirmed case in the US since 2013.
- Explains the virus is vaccine-derived, meaning someone outside the US shed the virus after being vaccinated with the oral version.
- Emphasizes that vaccine gaps are causing the resurgence of polio.
- Notes the serious effects of the disease, such as infections of the brain, spine, paralysis, and death, with no known cure.
- Mentions warnings issued in Britain and on his channel to stay up to date with vaccinations due to traces found in sewage.
- Addresses accusations of being a "shill for big vaccine," clarifying that his motivation comes from opposing the industry of children's coffins.
- Concludes with a thought about the importance of staying informed and taking necessary precautions against diseases like polio.

### Quotes

- "I'm not a shill for big vaccine."
- "I have another motivation. And it's not that I'm for any particular industry. I'm actually against another large industry, children's coffins."
- "Y'all have a good day."

### Oneliner

Beau provides an update on a polio case in New York, stressing the importance of vaccinations and dispelling misconceptions about his motivations.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Get vaccinated and stay up to date with vaccinations to prevent the spread of diseases like polio. (suggested)
- Stay informed about health warnings and take necessary precautions. (suggested)

### Whats missing in summary

Importance of community education and awareness to prevent the resurgence of preventable diseases.

### Tags

#Polio #Vaccinations #Health #Prevention #PublicHealth


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to provide a little update on
something I never thought I would have to say.
Maybe you remember the video, because there have been some
developments in something that I thought certainly wouldn't
be necessary to talk about in this day and age.
In New York, a young adult is paralyzed right now.
Contracted polio.
First case in the United States, at least first confirmed case in the United States
since 2013, when a seven-month-old from India
had it in San Antonio.
It appears that the virus is vaccine-derived.
Now, that doesn't mean that somebody got a vaccine
and got it.
What it means is that somebody outside of the United States,
because the US doesn't use the vaccines with live viruses
anymore.
They were vaccinated using it, probably the oral version.
And during the period in which they shed the virus,
they gave it to the patient in New York who was unvaccinated.
According to the AP, this person had not been overseas.
disease. So this is caused by vaccine gaps. That's why this is coming back. Now, if you
are infected, you have a 1 in 200 chance of developing serious effects. It's like infections
of the brain, spine, paralysis, death.
There is no cure.
Last month, health officials in Britain issued a warning telling everybody, hey, make sure
you're up to date, get vaccinated, because they found some in their sewage.
Three weeks ago, on this channel, I provided a similar warning for the same reason.
One of the, well, more than one of the responses said that I was a shill for big vaccine.
And I get it.
It's a big industry.
I understand where that sentiment comes from, but that's not what it is.
Big vaccine doesn't pay me.
I'm not a shill for big vaccine.
I have another motivation.
And it's not that I'm for any particular industry.
I'm actually against another large industry,
children's coffins.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}