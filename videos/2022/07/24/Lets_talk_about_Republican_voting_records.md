---
title: Let's talk about Republican voting records....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rvUGIutwMOY) |
| Published | 2022/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring Republican voting records and the disparity between rhetoric and actions.
- Recent examples of Republicans voting against protecting access to contraceptives, interracial marriage, and same-sex marriage.
- Republicans claiming to be moderate but voting against these protections.
- Republicans claiming it's about states' rights but voting against measures that allow interstate travel for family planning.
- Over 200 Republicans in the House voting against interstate travel for family planning, contradicting their supposed support for states' rights.
- Revealing the true intentions behind Republican votes: control, not states' rights or freedom.
- Over 150 Republicans voting against protecting interracial marriage and same-sex marriage.
- Over 190 Republicans voting against ensuring access to birth control.
- Republicans voting against these measures knowing they wouldn't stop them from passing, showing their commitment to their positions.
- The disconnect between Republican rhetoric and their actual votes, exposing their authoritarian tendencies.

### Quotes

- "It's not about states' rights. It's not about freedom. It never was. That's a lie."
- "They're telling you who they are."
- "They don't care about freedom. They don't care about states' rights. They don't care about anything that they said they do."
- "People need to acknowledge it, accept it for what it is."
- "You are taking away their rights, their freedoms, so you can continue a habit that you've had."

### Oneliner

Exploring Republican voting records reveals a pattern of control over freedom and rights, contradicting their claimed values.

### Audience

Voters

### On-the-ground actions from transcript

- Hold elected officials accountable for their actions (exemplified)
- Educate others about the disconnect between political rhetoric and voting records (exemplified)
- Support candidates who truly uphold values of freedom and rights (exemplified)

### Whats missing in summary

The full transcript provides detailed examples of recent Republican votes and the discrepancy between their rhetoric and actions.

### Tags

#Republican #VotingRecords #RhetoricVsActions #Rights #Freedom


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
Republican voting records real quick. What the real ideas of the party are and how they're
reflected in their votes. Because a lot of times you can look at rhetoric and you can
listen to what they say, but that's not really how they vote. And we've had some recent examples
of this that I think are worth mentioning. You know, when that decision came down from
the Supreme Court, inside it there was the idea that there were some other cases that
needed to be reviewed, revisited, overturned, done away with. And these are cases that protected
access to contraceptives, interracial marriage, same-sex marriage. Now Republicans already
feeling the heat from Roe. They were like, oh no, we would never do that. That's not
us. We're not that extreme. We're your friendly neighborhood moderate. It's those Democrats.
They're the ones that are really, they've moved far away. I'm going to throw something
else in here because it really has to do with Roe and their whole argument there. And because
they said that was about state rights. The states should decide. So when it came time
to make sure that people could travel across state lines to get family planning, you know,
to another state and allow that state decide, out of the 211, 213 Republicans in the House,
205 voted no. It's not about states' rights. That's a lie. That's a lie. They don't care
about states' rights. They want control. They want to find people to kick down at, tell
them they need to stay in their place and do what they're told. It's not about states'
rights. It's not about freedom. It never was. That's a lie. 211, 213 of them, somewhere
in there. When it came time to protect interracial marriage, same-sex marriage, 157 voted no.
They're not for that either. The idea that they're not that extreme, that's not true.
That's not true. And then when it came time to make sure that people could access birth
control, 195 voted no. They're telling you who they are. And here's the other thing about
this, and this is why this is really important to understand. They passed. All of these bills
passed over Republican objections, right? Over these votes, they still passed. Which
means the Republicans knew that their votes weren't actually going to stop it. They believed
so much in these positions that they will take the heat for it. They will expend that
political capital to vote against something that's going to pass anyway. A smart politician,
one that isn't radicalized, one that hasn't lost the plot, they would have abstained,
voted yes, just to avoid upsetting the voters. But they couldn't do that because it's hard-wired
into them. They're authoritarians. They don't care about freedom. They don't care about
states' rights. They don't care about anything that they said they do. It's about control,
and it always was. Everything else, that's just, it's just empty rhetoric that does not
matter. It didn't matter to them. Them saying they're not that extreme, they couldn't even
fake it. They absolutely are. People need to remember this. People need to acknowledge
it, accept it for what it is, because understand, if you support these people, come election
time, you need to come home from the polling station and apologize to your kids because
you are taking away their rights, their freedoms, so you can continue a habit that you've had.
So you can continue voting for somebody simply because they have an R after their name, because
you think that at some point in time they reflected your values. I don't think they
do. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}