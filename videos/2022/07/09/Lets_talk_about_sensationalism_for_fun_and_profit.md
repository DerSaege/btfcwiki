---
title: Let's talk about sensationalism for fun and profit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_JJ4aIS7Rqg) |
| Published | 2022/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques media sensationalism and profit-driven coverage.
- Expresses frustration at lack of front-page coverage for the Colorado River issue.
- Believes the story of the Colorado River is one of the most significant in the United States.
- Points out that sensational stories are profitable but not when they can't be sensationalized profitably.
- Calls out media models that generate outrage but discourage viewer involvement.
- Emphasizes the importance of individual actions in building grassroots movements for change.
- Condemns the narrative that individual efforts are insignificant compared to larger entities' actions.
- Warns against the self-defeating nature of narratives that discourage political involvement.
- Urges for basic coverage to prompt change and encourage solutions for the Colorado River issue.
- Advocates for awareness and action to address the water supply demand issue in the American Southwest.

### Quotes

- "It is easy to sensationalize the story, but it can't be sensationalized profitably."
- "I think it's one of the worst things for change, for building a better world, is people doing this."
- "Those efforts by individual people, that's what builds grassroots movements that then can affect real change with the large company."
- "It's a problem that probably should be addressed, but it will only be addressed if people know about it and if they should."

### Oneliner

Beau criticizes media sensationalism, advocates for individual action to address the Colorado River issue, and calls for basic coverage to prompt change.

### Audience

Media consumers, environmental activists

### On-the-ground actions from transcript

- Encourage grassroots movements (implied)
- Raise awareness about the Colorado River issue (implied)
- Take individual actions to conserve water (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the challenges of media coverage, the significance of grassroots movements, and the urgency for action on the Colorado River issue.

### Tags

#MediaSensationalism #ColoradoRiver #GrassrootsMovements #EnvironmentalActivism #IndividualAction


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
media coverage and sensationalism for fun and profit.
After that recent video
about the Colorado River and the fish,
somebody sent me a message saying, hey, it really seemed like you were holding
back when you were talking about the media coverage.
Yeah, probably.
I mean, I didn't go back and watch the video again, but probably. That tracks, that makes
sense because my, uh, from where I sit, the story of the Colorado River, what is
happening there is one of the most important stories in the United States
today. Something that will end up in history books. And I understand that
there are a lot of things going on right now that will end up in history books.
But this is, to me, something that warrants, you know, front page above the
fold coverage pretty frequently. It's not getting that. Not in the way it should.
And the reason is that it can't be sensationalized profitably. Profitably
being the key word. It's easy to sensationalize the story. The downstream
impacts, pardon the pun, are pretty pronounced. You're talking about the
water supply of tens of millions of people.
It's a sensational story to begin with, but it can't be sensationalized
profitably. Right?
See, one of the better models for sensationalizing something to make money
off of ad revenue is to sensationalize the story, tap into it in that, in that
manner that gives you that emotional reaction that gets people outraged. You
know, you give them their two minutes of hate straight out of 1984, right? Then you
simultaneously alleviate any responsibility or desire that your
viewer, listener, or reader might have to get involved. Because then they get the
viewer. They get that emotional reaction, but they also feel good because
there's nothing they can do about it. So they come back. And there are people who
build audiences this way. And we are talking about major media and smaller
political commentators. It's something that happens. And I think it's one of the
worst things for change, for building a better world, is people doing this.
Because it leads to, well, what the kids called doomerism. People who are
hyper-aware of what's going on, but they don't feel like they can do anything
about it. That type of profit model, what it might do is say, oh, you conserving
water at home? That's not going to do anything until giant company X does, does
something. They need to conserve water first. The viewer is completely absolved
of getting involved, right? But the reality is, if the viewer was going to
conserve five gallons of water, that's still five gallons of water, you know.
Sure, it's not as much, but it's still effective. It's still something, right?
More importantly, those efforts by individual people, that's what builds
grassroots movements that then can affect real change with the large
company. So it becomes self-defeating. You see the same thing with a lot of people
who are into electoral politics, you know. And you'll see them, and they will
rail about how the Democrats didn't do enough, or the Republicans did too much,
or whatever. And this is something that does happen all across the political
spectrum. They will sit there and cater to people who are interested in
electoral politics, and then tell them that they shouldn't get involved.
Because whatever party isn't going to do anything anyway. It is a very
self-defeating from the viewpoint of changing the world. It's very successful.
It's a winning strategy when you're just talking about making money.
This story, the story about the Colorado River, is something that is of major
importance. I love the American Southwest. Love it. It is beautiful. You couldn't
give me land there. I wouldn't move there. There are cities that in just a few
decades, they're not going to be there anymore. It seems like that warrants a
little bit of coverage, a little bit of attention. And more importantly, if that
coverage existed, even just basic coverage that explained what is going to
happen, it might help prompt change. It might make the world better. It might
encourage those grassroots movements to spring up and come up with solutions.
Those solutions are needed. The amount of water that is being demanded far exceeds
the supply for the foreseeable future. That's a problem. It's one that probably
should be addressed, but it will only be addressed if people know about it and feel
like they can do something about it and that they should. Anyway, it's just a
thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}