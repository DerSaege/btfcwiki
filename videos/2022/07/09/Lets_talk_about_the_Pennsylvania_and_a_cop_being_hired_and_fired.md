---
title: Let's talk about the Pennsylvania, and a cop being hired and fired....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=el6JBAUvoQ4) |
| Published | 2022/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Cop named Loman killed Tamir Rice in 2014 for playing with a toy at a park.
- Loman never charged, eventually fired for relying on their application.
- Loman resurfaced in Tioga Borough, Pennsylvania, got hired, but resigned after the community protested.
- Mayor was unaware of Loman's background, suggesting the borough council knew.
- Need for a police database evident in this incident.
- Rural communities like Tioga share distrust of cops and outsiders.
- Tioga Mayor, a big-bearded man, protested against cop but is the same person.
- Rural communities seek right and wrong, not political agendas.
- Common ground can be found between truly rural people.
- Stereotypes may cloud perceptions of allies in accountability.

### Quotes

- "This is a community that once the information was known, they decided they wanted something else."
- "There's common ground to be had because it's not a matter of a political agenda at that point."
- "Once I got this message, I went and found the footage."
- "Most times when you see somebody who looks like the mayor or me, you're probably not thinking ally."

### Oneliner

Cop killing of Tamir Rice revisited in rural Pennsylvania, leading to community pushback and mayor's surprising protest, showcasing the need for police accountability databases and common ground in rural distrust.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Contact local officials to advocate for police accountability databases (implied)
- Build bridges between rural communities and law enforcement for better understanding (implied)

### Whats missing in summary

The full transcript provides depth on the impact of police actions on rural communities and the importance of transparency and accountability in law enforcement.

### Tags

#PoliceAccountability #TamirRice #CommunityPolicing #RuralCommunities #SocialJustice


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
Pennsylvania and the continuation of a story from 2014 and cops and rural people. So, a
cop named Loman, back in 2014, he pulled up to a park and there was a kid playing with
a toy, Tamir Rice. He killed him. The cop killed him. This is a case that gets used
as an example to show what happens when best practices are not followed, when officers
do not use time, distance, and cover. The cop never got charged, but was eventually
fired, I think for relying on their application or something like that. Now, the cop recently
surfaced because in Tioga Borough in Pennsylvania, he was hired. And the people there in town,
well, once they found out, they decided he shouldn't be the cop and he resigned. All
of this happened over the course of like a week. The mayor of the town is still kind
of going to the press about it because he was unaware of Loman's background, of Loman's
history. And he's saying that the borough council, he's kind of suggesting that they
were, but he wasn't. If there is a single incident that shows the need, that demonstrates
clearly the need for a police database, for a database of cops, this is it. This is it.
This is a community that once the information was known, they decided they wanted something
else. So it's a clear example. Now, I also got a message from somebody who sends me messages
pretty often asking about the weird things that white people do. And it's related.
What's up with your people? A bunch of rednecks protesting the cop who killed Tamir Rice.
Man with a big beard in the back of a pickup truck talking to a bunch of other rednecks
looking like they're about to riot. Where was that energy for the last few years? So
once I got this message, I went and found the footage. The guy in the back of the pickup
truck with the big beard, that's the mayor. That is the mayor. Tioga is a rural town.
It's a town of 600 people. This cop would have been the only cop in town. So what happened
is something that is just kind of true in all rural communities. You know, that's Pennsylvania,
so it's a little bit different because it's up north. But rural people pretty much everywhere
have two things in common. They're distrusting of cops and they're distrusting of outsiders.
This is an outsider who came in to be the only cop in town. And what they found out
about the person who would be the person interacting with their kids was this. Yeah, they were
pretty heated. They got pretty heated. This is one of those moments. There's common ground
to be had between people who are truly rural. And I understand there's suburbs, there's
just outside of town a little bit country, and then there is rural. This is rural. This
is way out there. There's common ground to be had because it's not a matter of a political
agenda at that point. It's a matter of right and wrong. And creating a bridge there is
probably a really good idea. I know because of stereotypes. Most times when you see somebody
who looks like the mayor or me, you're probably not thinking ally. But when it comes to unaccountable...

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}