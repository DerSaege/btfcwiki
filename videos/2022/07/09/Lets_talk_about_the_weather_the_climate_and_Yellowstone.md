---
title: Let's talk about the weather, the climate, and Yellowstone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UQ0PSdFh9IE) |
| Published | 2022/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wild flooding in Yellowstone resulted in unpredicted devastation, including the evacuation of 10,000 people and destruction of property.
- Hydrologic models used for forecasting floods are becoming less reliable due to the changing climate rendering historical data inadequate.
- Climate change has led to less snowfall in winter, more rain in spring, creating a recipe for flash floods in areas like Yellowstone.
- Extreme weather incidents are becoming more common, with events previously considered 100-year occurrences now happening more frequently, almost every 20 years.
- Residents in areas prone to extreme weather need to be prepared for more frequent and intense events with less warning, such as floods or tornadoes.
- Emergency responders were caught off guard by the severity of the flooding, indicating a lack of adequate warning systems for these increasingly common events.
- People should start keeping necessary supplies on hand to better prepare for sudden extreme weather events.
- Historical weather patterns are becoming less useful for predicting future events due to climate change.

### Quotes

- "Extreme weather incidents are going to become more and more common."
- "If you live in an area where there are extreme weather events, you might want to be prepared for them to occur more frequently and with less warning."
- "It might be a good idea to start getting in the habit of keeping what you need on hand."

### Oneliner

Be prepared for more frequent and intense extreme weather events with less warning, as climate change continues to disrupt traditional forecasting methods.

### Audience

Residents in areas prone to extreme weather.

### On-the-ground actions from transcript

- Prepare an emergency kit with essentials for extreme weather events (implied).
- Stay informed about the changing weather patterns in your area and adapt your preparedness accordingly (implied).

### Whats missing in summary

Importance of adapting preparedness strategies to the evolving climate patterns for increased community resilience. 

### Tags

#ExtremeWeather #ClimateChange #EmergencyPreparedness #CommunityResilience #Forecasting


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the weather
and the climate and Yellowstone and what happened.
If you don't know, there was some wild flooding out there.
Wild.
But that's not what the forecast called for at all.
Forecast was pretty mild.
and it called for minor flooding certainly didn't hint at some of the
worst flooding on record didn't really predict flooding that would hit towns
take out bridges wash out roads remove houses and lead to the evacuation of
10,000 people that wasn't in the forecast why what happened what's going
on. The hydrologic models that are used to forecast flooding like this, they're
based on history. The problem is we're moving into an era where historical
records, when it comes to the climate, they're less useful because the climate
is changing. So the historical precedent not really adequate when it comes to predicting
what's going to happen. You know, we joke about people who forecast the weather all
the time and talk about how they're never right, but generally they can give us pretty
good heads up when something bad's coming, right? That's going to not be the case. It's
It's going to become less frequent that they're able to give us those heads up.
When it comes to Yellowstone, what they've documented in the change is that there is
less snowfall in the winter, more rain in the spring, which, yeah, that's a recipe
for flash floods.
When it comes to stuff like this, extreme weather incidents are going to become more
and more and more common.
So much so that events that are normally called 100-year events, meaning that in any given
year there's a 1% chance of them happening, 100-year events now are more accurately described
just 20-year events because there's been an increase, a five-fold increase in the
frequency. That's a big deal and it will probably accelerate. There will probably
be more and more events like this. So what's that mean for you? If you live in
an area where there are extreme weather events, you might want to be prepared for
them to occur more frequently and with less warning. So if you're in an area
where flooding happens occasionally, you might want to have all of your stuff
ready, because it may happen without the warning that you normally get. That's
what happened in this case. There were a whole lot of people caught by surprise,
even emergency response. They were out there in the middle of the night
shutting down roads because they didn't know this was coming, at least not to the
degree that it was. And it could happen with floods, tornadoes, anything that
might occur in your area, that has historically occurred in your area.
It's probably going to happen more frequently and stronger.
It might be a good idea to start getting in the habit of keeping what you need on hand.
So you're not caught as off guard.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}