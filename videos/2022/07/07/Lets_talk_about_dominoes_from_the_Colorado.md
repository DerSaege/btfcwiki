---
title: Let's talk about dominoes from the Colorado....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oH-3ZjlcMas) |
| Published | 2022/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of domino effects in the environment, where one issue leads to another.
- Talks about how people tend to overlook environmental news and its impact because they don't realize the interconnectedness of everything.
- Focuses on the under-reported story of the Colorado River and the downstream effects.
- Mentions the drop in water levels in Lake Powell leading fish like smallmouth bass into new areas.
- Raises concern about the endangered humpback chub species being threatened by the presence of smallmouth bass below the dam.
- Warns that if the current problem is not addressed, it will lead to more issues down the line, potentially impacting the riverbed and eventually people.
- Criticizes media for not adequately covering environmental issues and waiting until it's too late to address them.
- Urges people to pay attention to environmental stories and listen to experts who often get sidelined by business interests.
- Emphasizes the importance of balancing environmental impacts and addressing negative news before it escalates.
- Encourages viewers to be more aware and proactive about environmental issues for a sustainable future.

### Quotes

- "Every piece of negative environmental news creates another, and then another."
- "We need to start paying better attention to environmental stories that are going on."
- "We have to get to the point where we can kind of live in balance."

### Oneliner

Beau explains the domino effects in the environment, urges attention to under-reported stories like the Colorado River, and stresses the importance of addressing environmental issues promptly to avoid escalating impacts.

### Audience

Environmental activists

### On-the-ground actions from transcript

- Listen to environmental experts and prioritize their insights (suggested)
- Stay informed about environmental issues and stories (exemplified)
- Advocate for balanced environmental management and solutions (implied)

### Whats missing in summary

The full transcript provides detailed examples of how interconnected environmental issues can escalate and impact ecosystems and human communities if not addressed promptly. Watching the full video can offer a deeper understanding of these concepts.

### Tags

#DominoEffects #EnvironmentalIssues #ColoradoRiver #MediaCoverage #BalancedManagement


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about dominoes
when it comes to the environment. How one thing leads to the next, that leads to the next, that leads to the next.
See, normally when a piece of environmental news breaks, most people look at it and they're like,
oh that's horrible. Oh well, and they go on about their day, right? They don't give it any further
thought because they either aren't aware or don't want to accept the fact that we are all connected,
we're all one. And that little development will have another little development that will make
them say, oh, and go on about their day. You do that long enough, it's going to impact you directly
in a really bad way. So today we are going to talk about the downstream effects when it comes
to the environment. In this case, both figuratively and literally. You know, right now I think the
single most under-reported story that is occurring at the moment is what's happening with the Colorado
River. What time is given to this story? Well, it tends to focus on water supplies for people,
you know, because that tends to be the priority. But there's another thing that's happening,
another development. See, in Lake Powell, there are fish. And there are fish that live near the
surface of the water. But because the water level has dropped, the surface of the water is now
closer to the part of the dam that allows you to get through, right? So some fish are doing that.
So they're getting into a new area. And what is of particular concern to biologists right now
are smallmouth bass ending up below the Glen Canyon Dam. Because that's where this endangered
species, the humpback chub, lives. And they have found that there are smallmouth bass down there.
And they can safely assume at this point that they're reproducing. The chub was brought back
from the brink of extinction. But now it has a problem again. Rest assured, if this problem is
not corrected, if it's not fixed, that will cause another problem. And then another. And then
another. And eventually, it could end up being something like, I don't know, the riverbed
deteriorating. It could cause a lot of issues that might eventually end up impacting people.
But by the time it does, it's too late. And if there is one thing that you can learn from the
media coverage of what's happening with the Colorado River, it's that they're not going to
tell you how bad the problem is. They're barely going to mention it. They're going to wait until
there's not much that can be done about it. We need to start paying better attention to
environmental stories that are going on. And listening to those people who have made that
their pet calls. You know, there are people out there who are subject matter experts. And it's
not that they don't say what's going on. They do. It's just that normally large business
interests kind of push them off to the side. Oh, don't listen to them, those tree huggers. You
know, we should probably listen to them. They know what they're talking about. And generally
speaking, they've been right. So this is another development. In and of itself, sure, the news,
and go on about your day. But these are signs that the problems are going to get worse.
Every piece of negative environmental news creates another, and then another. We have to get to the
point where we can kind of live in balance. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}