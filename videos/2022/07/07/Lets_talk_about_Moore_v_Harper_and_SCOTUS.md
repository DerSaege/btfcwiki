---
title: Let's talk about Moore v Harper and SCOTUS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ANrBplbZsFM) |
| Published | 2022/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to Moore v. Harper, a Supreme Court case from North Carolina examining the Independent State Legislature Doctrine.
- The doctrine, rejected by the Supreme Court for over a century, could jeopardize voting rights and the will of the people.
- If implemented, the doctrine could allow state legislatures to manipulate votes, control representation, and influence the federal government.
- Beau warns that this is an existential threat to democracy and a pivotal moment for the country.
- The case is considered as significant, if not more, than overturning Roe v. Wade.
- Beau suggests considering options like expanding the Supreme Court to counter the potential impacts of this doctrine.
- He stresses the importance of understanding the implications and long-term consequences of allowing state legislatures unchecked power.
- Beau urges people from all parties to pay attention and act against this threat to democracy.
- This issue is seen as a critical reason to get rid of the filibuster and potentially expand the court.
- The fate of the country seems to rest on the decisions of certain justices, raising concerns about their qualifications.

### Quotes

- "This is an existential threat to the republic, to the idea of a representative democracy."
- "When you look back at the times when the Supreme Court has tossed this theory aside, that's what they said."
- "This is a moment where people of every party need to make sure that they understand the consequences of it."
- "If they decide to implement this doctrine, that's kind of it."
- "The fate of the country, in a way, rests in the hands of a justice who could not identify the elements of the First Amendment during her hearing."

### Oneliner

Beau warns of an existential threat to democracy as the Supreme Court considers the Independent State Legislature Doctrine, potentially undermining voting rights and the will of the people.

### Audience

Voters, activists, concerned citizens

### On-the-ground actions from transcript

- Inform others about the potential implications of the Independent State Legislature Doctrine (implied)
- Advocate for expanding the Supreme Court as a potential countermeasure (implied)
- Educate yourself and others on the long-term consequences of this doctrine (implied)

### Whats missing in summary

The full transcript provides a detailed analysis and urgency around the implications of the Independent State Legislature Doctrine, urging action and vigilance in protecting democracy.

### Tags

#SupremeCourt #VotingRights #Democracy #IndependentStateLegislatureDoctrine #ExpandTheCourt


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Moore v. Harper.
It's a case out of North Carolina
that the Supreme Court has decided to take a look at.
The goal is to examine something
called the Independent State Legislature Doctrine, okay?
This is an argument, a legal theory that has been advanced over and over again in American
history and the Supreme Court has rejected it.
This has happened over a hundred years.
The court has agreed to look at it.
Four members of the court have expressed some support in some way or another for this doctrine.
The reason the Supreme Court has always shot this doctrine down is because it quite literally
would mean the end of the will of the people.
What it would do is stop state courts from basically protecting voting rights in any
way shape or form.
It's based on a reading of the Constitution that requires you to believe that the right
to select the representatives wasn't there.
It requires adding words.
It requires this bizarre twist to get it to say what the proponents of this theory want
it to say.
If the court decides that this doctrine should be in effect, your vote no longer matters.
That's not an overstatement.
The state legislature in your state could be like, oh, that blue area over there, that
area that always votes blue, we didn't like something that they did with how they counted
the votes or how the votes were cast.
We're not going to count those.
Or that rural area out there, they stayed open three minutes too late.
We're not going to count those votes.
And there's nothing that could be done.
They can gerrymander, they can do whatever they want to keep themselves in power in the
state legislature and then use those seats to control the federal government.
This is an existential threat to the republic, to the idea of a representative democracy.
And it's being reviewed by a court, four of which at some point have expressed some
support for the idea.
Now understand there are varying levels of this theory.
The one that I just kind of showcased is the literal, there is no limits to what the state
legislature can do.
And that is, seems to be the theory that's being advanced.
It's the only thing that makes sense because the case itself, Mor v. Harper, it's in North
Carolina where the state legislature has explicitly said how these cases are supposed to be handled.
So the only reason to hear it under this doctrine would be to say that the state legislature
can't even delegate it, even if they chose to.
They can't give the court's permission.
This is as important, if not more important, of a case than overturning Roe.
If you needed more incentive to get behind the idea of expanding the Supreme Court, this
is it.
Or perhaps, I know some people have suggested impeaching judges.
I don't know that that's going to work.
It seems like it would be easier to expand the court.
This is a huge deal.
It sounds very dry, very boring.
When you read about it, yeah, it is.
But when you take this to its conclusion, what it means is you don't have a voice anymore.
And when you look back at the times when the Supreme Court has tossed this theory aside,
that's what they said.
They flat out, they're like, yeah, we have to decide whether the state legislature can
do whatever it wants or we're a country that has, you know, some idea, at least some representation
and that the will of the people is upheld.
This isn't one to look away on.
This isn't one to think, oh, well, it'll be okay.
It'll work itself out.
It won't.
This is a moment where people of every party need to make sure that they understand the
consequences of it.
There are, I mean, it just goes on and on.
Because once this kind of power is handed to the state legislature, they really have
all of the power because they can control the vote to that degree. This is
a really big deal. If you're one of those people who's like, get rid of the
filibuster, we need to... this is something you need to be talking about and this is
something you need to brush up on and make sure that when somebody asks you
about it, you understand the long-term implications of what this means. This is
probably a better reason to get rid of the filibuster and expand the court than anything.
Because if they decide to implement this doctrine, that's kind of it.
And right now, I want to point out that the judges who have expressed their support for
it are countered by four judges who are kind of like, no, that's not a good idea at all.
So the fate of the country, in a way, rests in the hands of a justice who could not identify
the elements of the First Amendment during her hearing.
That doesn't really leave me feeling very comfortable.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}