---
title: Let's talk about the Georgia Guidestones....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2jmjdmpltg0) |
| Published | 2022/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Georgia Guidestones, a stone monument in Georgia, and addresses questions about its purpose and destruction.
- Notes the suspicions and conspiracy theories surrounding the monument, particularly among the far right, including concerns about a global court system and maintaining humanity under 500 million.
- Describes the monument's inscriptions in multiple languages, outlining rules for society like maintaining balance with nature and guiding reproduction wisely.
- Mentions the monument's Cold War origins and its purpose as a guide for rebuilding society after a nuclear war.
- Speculates on the identity of the individuals who erected the monument, pointing to shaky evidence linking it to a right-wing white supremacist.
- Comments on the destruction of the monument, possibly due to conspiracy theories and echo chambers, with the Georgia Bureau of Investigation releasing footage related to the incident.
- Expresses a lack of concern about the monument being a guide to world domination, viewing it as a Cold War relic erected by peculiar individuals.
- Raises concerns about people acting on theories they hear in echo chambers and the potential consequences of such actions.

### Quotes

- "Maintain humanity under 500 million in perpetual balance with nature."
- "People are getting so far down into these echo chambers."
- "It's just a thought."

### Oneliner

Beau addresses suspicions and conspiracy theories surrounding the Georgia Guidestones, a Cold War relic destroyed amidst concerns about echo chambers influencing actions.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Contact the Georgia Bureau of Investigation to provide any relevant information about the destruction of the Georgia Guidestones (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of the Georgia Guidestones, their history, conspiracy theories surrounding them, and their recent destruction, offering insights into societal suspicions and echo chambers.

### Tags

#GeorgiaGuidestones #ConspiracyTheories #EchoChambers #ColdWar #Destruction


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about America's Stonehenge.
We're gonna talk about the Georgia Guidestones
because I got a whole bunch of questions about it.
And oddly, the questions aren't about what happened to it.
The questions are about what it is.
I don't know from the South,
I thought everybody knew about this thing, but I guess not.
So we'll talk about what it is, what it was,
what people thought it was and what happened to it.
So if you don't know, the Georgia Guidestones were,
well, they're past tense now because somebody blew it up.
A candidate for governor in Georgia
made it a campaign pledge to get rid of this monument.
There are a lot of people on the far right side of things
who viewed this with suspicion, to put it mildly.
It's been brought up a lot,
including by like the space laser lady.
Just to give you a frame of reference
for the type of people who were truly concerned
about this thing.
Now, they thought that it was kind of a guide
for some secret shadowy organization to rule the world.
And because of some of the things that were written on it,
they, I mean, they took it out of its context
and basically thought that it meant something it didn't.
So let's talk about what it is real quick, what it was.
It was a stone monument that had some features
that could be used for astronomy.
It served as a calendar and a clock and stuff like that.
And it also had some rules for society.
And these rules were inscribed on it in English, Spanish,
Swahili, Hindi, Hebrew, Arabic, Chinese, and Russian.
I don't know if y'all have ever been to this part of Georgia,
but I have.
Most of these languages, they're not really common there.
And that should be the first sign that this may not be
for the people who live around there.
The construction of it was supposed to be something
that could survive catastrophic events.
Given what happened to it,
I think there might've been some engineering failures,
but inscribed on it in these various languages
are these rules.
Maintain humanity under 500 million
in perpetual balance with nature.
Guide reproduction wisely, improving fitness and diversity.
Unite humanity with a living new language.
Rule passion, faith, tradition, and all things
with tempered reason.
Protect people and nations with fair laws and just courts.
Let all nations rule internally,
resolving external disputes in a world court.
Avoid petty laws and useless officials.
Balance personal rights and social duties.
Prize truth, beauty, love, seeking harmony with the infinite.
Be not a cancer on the earth, leave room for nature.
Okay, so I mean, aside from the whole,
like, eugenics-y thing here,
most of this is pretty simple, right?
The reason it became a focus of suspicion among the right
are just a few little pieces in this.
First, the world court.
As we know, in the right-wing world,
the idea of a global court system is just, that's bad, okay?
Maintain humanity under 500 million.
There's a lot more people than 500 million.
So in the right-wing mindset, it's, well,
that means they've got to get rid of a bunch of us, okay?
And that, in and of itself, those are the two key parts
that really kind of drove the suspicion.
I would point out that it says maintain at 500 million.
The most likely reason for this thing,
the reason it was built,
was to serve as a guide for rebuilding society
after the impending nuclear war
that was supposed to take place.
This was built in, I want to say it was built in 79
and, like, unveiled in 80 or something like that.
Cold War stuff.
So it's not that they were going to get rid
of a bunch of people.
I think they assumed that the nuclear war
was going to do that.
It was written in all of these different languages.
So whoever found it could use it as, like, a Rosetta Stone
to uncover and translate other texts.
But that is not how it was perceived.
And understand this thing has been a focal point
and a subject of theories ever since it was put up.
Because the people who put it up did so in secret.
There has been some evidence that links it
to a specific person who, as irony would have it,
was actually somebody who would be aligned
with the people who are mad at it today,
who view the monument with suspicion today.
The most likely candidate was a right-wing white supremacist,
as far as who put it up.
But we don't really know that.
The evidence on that is kind of shaky.
But that's what it is.
Now, because of the fixation with these conspiracy theories
and the way people have fallen into echo chambers
and then it becoming mainstreamed
through a candidate for governor,
somebody decided to blow it up.
GBI, the Georgia Bureau of Investigation, has released some footage.
If you know anything about stuff that goes boom, go watch it.
I'm fairly certain these people are going to get caught.
But that's what happened.
That's what it is.
I never viewed this thing as something to be overly concerned about.
I did not think that it was a guide to world domination by lizards or whatever.
To me, it was a Cold War relic put up by some very strange people.
But, I mean, it's gone now.
So there's that.
Most of the questions were about what it is.
That's what it is.
As far as something that might be worth noting, this was rocks.
It was. It was stone.
It's not a big deal.
However, the reason that this happened is probably something
we should pay attention to.
People are getting so far down into these echo chambers,
and they're becoming more willing to leave the computer
and act on some of the theories that they've heard.
And that's a little concerning.
I will say that as far as I know, and as far as everything that's been reported,
nobody was actually hurt in this.
They did it in the middle of the night.
But I'm sure that that part of Georgia is going to be a little upset
because there were people who drove from all over to just go and see it.
Kind of a tourist trap type of thing.
So we'll have to see what happens next.
I do believe that the candidate for governor claimed that God did it.
That's who destroyed the monument.
We'll have to wait and see if GBI books God.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}