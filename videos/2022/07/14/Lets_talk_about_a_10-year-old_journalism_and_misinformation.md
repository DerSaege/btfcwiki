---
title: Let's talk about a 10-year-old, journalism, and misinformation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a-gldvbNtKg) |
| Published | 2022/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on a recent video to address misinformation and explain why he didn't cover the continuing story that emerged.
- Talks about a story involving a 10-year-old crossing state lines for a procedure due to a Supreme Court decision.
- Mentions pushback and accusations of the story being a hoax coming from centrist/liberal and right-wing outlets or politicians.
- Points out the arrest of the person allegedly involved in the incident as casting doubt on the hoax claims.
- Explains his confidence in the initial reporting and why he didn't request a retraction.
- Talks about red flags for spotting misinformation, focusing on the headline and journalistic standards.
- Addresses the plausibility of the story and the lack of red flags.
- Criticizes major outlets for sensationalizing the story and seeking confirmation aggressively.
- Explains the reluctance of involved parties to talk to larger outlets due to the sensitive nature of the story involving a 10-year-old.
- Condemns those who rushed to cast doubt on the story and the impact on future victims coming forward.

### Quotes

- "Ego."
- "If you're one of those people who couldn't wait, who couldn't just wait for things to play out, and instead cast doubt on a child, you never get to ask about why women don't come forward again."
- "It's a 10-year-old child. That's the story."

### Oneliner

Beau addresses misinformation surrounding a sensitive story involving a 10-year-old and criticizes those who rush to cast doubt, impacting future victims coming forward.

### Audience

Journalists, Activists, News Consumers

### On-the-ground actions from transcript

- Support responsible journalism by subscribing to credible news sources (exemplified)
- Advocate for ethical reporting practices in media outlets (exemplified)
- Stand against rushing to cast doubt on sensitive stories and victims (exemplified)

### Whats missing in summary

The full transcript provides a detailed breakdown of how misinformation spreads and impacts sensitive stories involving children, urging for responsible journalism and empathy towards victims.

### Tags

#Misinformation #Journalism #Ethics #SensitiveStories #SupportVictims


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to provide an update on a video
from a few days ago.
We're going to kind of go through it.
And hopefully, it will help people spot misinformation.
And it will also explain why I didn't feel the need
to delve in to the continuing coverage that
occurred after that video.
So a few days ago, a story broke about a 10-year-old
that crossed state lines to get a procedure done,
something they felt they had to do because of the Supreme
Court's illustrious decision.
Not long after this story broke, people
started pushing back on it to the point
where eventually it was being called a hoax, saying
that it didn't happen.
The pushback came from two different groups.
One, large centrist or even liberal in some cases,
outlets.
And right-wing outlets, the right-wing outlets
or politicians, who should definitely know better.
But the politicians all had a political or ideological reason
to push back on it.
Their motives were suspect, right?
OK.
This got to such an extreme that if you go to that video
on this channel, I think it's called
Let's Talk About Ohio and Indiana or something like that,
and you filter the comments by newest first,
you will see people asking for a retraction
because they were so certain it was false,
because they had people they trust go out there
and say that it was false.
OK.
So if you don't know, the person responsible
for putting the 10-year-old girl in that position,
or the person alleged to be responsible, has been arrested.
I mean, that casts a whole lot of doubt
on the idea that it's a hoax.
OK.
So let's go to that initial reporting.
And we'll talk about why I didn't ask for a retraction.
Let's go to that initial reporting, and we'll talk about why I absolutely
did not feel the need to dive back into this,
because I was fairly confident in the reporting.
Why?
Over on the other channel, I have a video.
It's like the roads to understanding misinformation
or something like that.
And I give a bunch of red flags, warning signs
to look for in articles to let you know
when you're being manipulated.
The big one is the headline.
If it's an inflammatory headline, be on guard.
If it's just a quote, be on guard.
There's a bunch of them.
The headline in the initial reporting on this, super boring.
Super boring.
It's something like people from out of state travel.
It doesn't even have 10-year-old in the headline.
If the journalist in this case did make a mistake,
it was that, way to bury the lead.
So the headline is normal journalism.
It's exactly what you would expect.
You go to the byline, the people who wrote it.
You have a general assignment reporter and a person
who is health and science, health and medicine, something like that.
And you can click on the names on most outlets,
and you can pull up the articles they've written before.
It's all journalism, just straight journalism.
It's not inflammatory.
They're not out there looking for viral articles.
They're just doing their job.
The article itself answers the who, what, when, where, why.
Pretty simple.
Sure, sourcing on this particular thing is light.
The public facing sourcing, anyway, is light.
But that's understandable, and we'll get to why here in a second.
Is, so the article's fine.
Is the general idea of this, that this occurred, is it plausible?
Yeah.
Yeah, the fact that people are going to have to engage in this
and that some of them are going to be literal children because
of situations like this, yeah, that's plausible.
In fact, it was predicted by pretty much every family planning
specialist in the country.
So the idea that it happened, that's not surprising.
There's no red flags.
There's no reason to doubt this at all.
OK, so who cast doubt?
Giant outlets, big outlets that wanted
to take that story that was normal journalism
and turn it into a national story, right?
And they wanted confirmation.
And when they couldn't get it, they said, oh, well, it's just a single source story.
Right, and these are outlets who oftentimes will run stories
with phrases like, people familiar with the conversation who
weren't authorized to speak on it.
And they grant them anonymity for that, right?
But this, this is an issue.
So why'd they get so bent out of shape?
Let me tell you a phrase that I am positive
was spoken in one of those newsrooms.
Oh, sure, they'll confirm it to the Indianapolis star, but not to us.
Ego.
Ego.
And there's a reason that that might have happened, and we'll get to that.
And then the other side are people who have a very vested political interest
in saying that this didn't happen, because if it did happen,
it makes them look like monsters.
Incidentally, they look like even larger monsters today.
So why didn't the people, why didn't the people involved,
why didn't they want to talk?
Why didn't they want to talk to the larger outlets?
Why didn't they want to provide confirmation?
Well, most people understand that this is a, it's a huge story.
You know, you get that.
It's a huge story.
It's nationally significant.
There's something else at play here.
It's a 10-year-old girl.
It is a 10-year-old child.
That's the story, to be clear.
Can you think of any positive sourcing, any sourcing that would be able
to 100% confirm this that wouldn't also disclose her name, who she is?
Those who are politically motivated, given what we've seen lately,
would probably plaster that child's name all over their website
and find some way to say she was lying,
because that's what passes for journalism today.
The other thing to note is that while they say it's single source,
it probably wasn't.
I don't know this, to be clear.
I don't know this for a fact.
But I know that some journalists, they had two notebooks.
They had the sources they'd use and the sources that never existed.
And they probably were not willing to share the information that they had,
because they knew what would happen to it.
And then you have one other big issue here, as far as the journalism side of things,
the news cycle.
They wanted that information and they wanted it now.
I mean, let's be clear.
This story, there was never any doubt about whether or not
it was going to be confirmed or debunked.
That was going to happen.
One way or the other, that was going to occur, because there's
the case that goes along with it.
Either somebody was going to be arrested for it or they weren't,
because this was a unique case.
And I think everybody knew that from the beginning.
So the story was either going to be confirmed or denied.
But rather than waiting, doing the ethically responsible thing,
you had people fill the time lapse with speculation,
to the point that it was called a hoax.
To the point where an attorney general, somebody
who would be responsible for prosecuting somebody who did something like this,
they said that there wasn't really any evidence.
To the point where in my comment section on that video,
there's people saying she made it up, that it didn't happen,
that she cried, woof, not the term.
If you're one of those people who couldn't wait,
who couldn't just wait for things to play out,
and instead cast doubt on a child, you never get to ask about
why women don't come forward again.
You're the reason.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}