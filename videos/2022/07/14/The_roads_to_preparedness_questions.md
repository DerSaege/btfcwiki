---
title: The roads to preparedness questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=obqLU7c6QbI) |
| Published | 2022/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People tend to overlook the importance of keeping their papers, like birth certificates and insurance documents, in an emergency.
- When it comes to preparing for adrenaline and trauma, you can't really prepare but being comfortable with the tasks at hand is key.
- In an everyday carry (EDC) and survival kit, essentials include food, water, fire, shelter, first aid kit, and a knife.
- Refraining refrigerated medications during power outages is a challenge with limited solutions.
- Beau advocates for Wilderness First Responder courses for emergency preparedness but notes their cost and intensity.
- Using zombies as a metaphor can help in discussing emergency preparedness with friends.
- Beau suggests having a raft in case of heavy flooding as an additional preparedness item.
- Two weeks of supplies are recommended as a minimum for emergency preparedness.
- Emergency preparedness should not become an overwhelming lifestyle but a freeing safety net.
- Different considerations for emergency prep arise for targeted minorities due to potential biases in disaster situations.

### Quotes

- "Knowledge weighs nothing."
- "Emergency preparedness is supposed to be freeing."
- "Different considerations for emergency prep arise for targeted minorities."

### Oneliner

Be prepared for emergencies by safeguarding vital documents, understanding tasks, carrying essentials, and fostering self-reliance.

### Audience

Emergency planners

### On-the-ground actions from transcript

- Compile and digitize vital documents for emergency preparedness (suggested).
- Enroll in Wilderness First Responder or first aid courses for better readiness (suggested).
- Initiate zombie apocalypse metaphor talks to spark emergency prep awareness among friends (suggested).

### Whats missing in summary

The full transcript provides in-depth insights on emergency preparedness strategies, including the importance of self-reliance, understanding mobility issues, and staying off immediate front lines during disasters.

### Tags

#EmergencyPreparedness #DisasterResponse #SelfReliance #CommunityPreparedness #InclusivePlanning


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about emergency preparedness
again with your questions.
These are questions from people on Twitter,
and we're just going to go through them one at a time,
kind of work our way through, and it
provides a wide frame of reference.
So OK, what's the one thing that people forget in an emergency?
I mean, the best answer to this is they're cool.
But if you're talking about an item,
every time I have done relief after a hurricane, papers.
They're papers.
Birth certificates, homeowner's insurance, life insurance,
Social Security cards, their paperwork,
the stuff that they have to have to continue their life.
People tend to forget that in an emergency.
My advice when it comes to something like this,
take that stuff, put it on a thumb drive.
At least you'll have a digital reference for it.
How do you prepare yourself for the adrenaline and trauma?
You can't.
I know that's not the answer people want to hear,
but the reality is you can't.
You cannot prepare yourself for it.
When you think about jobs that are high stress,
jobs where you see things that are less than pleasant to see,
how do they train?
They do the same thing over and over and over again.
So it becomes second nature.
That's how you deal with it.
All of the mental toughness exercises and stuff like that,
I mean, you can try it.
It can't hurt.
But in my experience, that's not what will get you through.
What will get you through is being very comfortable
with the tasks that you're going to have to complete.
And if you know how to do them, you can ignore the other stuff
and collapse later.
People like to imagine how they would handle those situations.
I have seen people who were certain that they would just
be totally calm and be able to handle anything,
completely lose it.
And I have seen people who were terrified on the ride there
be just completely mellow.
So I don't think that there is a mental exercise that you can do.
I mean, sure, reading the resistance stuff in the serial
manual, that might help.
But I think the most important thing you can do
is be very familiar with the task
that you're going to have to complete
so you aren't overwhelmed by the stuff that's
happening around you while trying to do something.
I think that's what will get you through the best.
What do you need in an EDC?
For those that don't know, EDC is everyday carry.
This is the stuff that you're supposed to have with you
all the time.
What do you need in a survival kit?
Food, water, fire, shelter, first aid kit, and a knife.
That's what you need in your EDC, too.
The goal of the EDC is to get you to wherever your stuff is.
So you need the same things in smaller quantities,
maybe less flexibility, and it needs to be compact.
This should fit in your pocket.
Let's see.
What are some useless or scam items for emergencies?
Yeah, there's probably not any videos on that.
And that's because people who would review emergency
or survival items are people who are survival minded.
And what is far more important than any purchased product
is the mindset of everything's useful,
you just have to find out why.
So generally speaking, people who are into that world,
they'll find a use for it.
I can't really think of anything off the top of my head
that is just absolute garbage.
But at the same time, I'm one of those people
who focuses more on knowing how to do stuff with less
than I am on gadgets.
I mean, as we talked about in the other video
on this channel about survival, knowledge weighs nothing.
Everything else you have to carry,
and it gets heavy after a while.
Refrigerated meds.
This is a question that comes up often,
because there's no good answer.
If you're going to be in a situation without power
for an extended period, and you are somebody
who needs medications that are refrigerated, yeah,
you've got to work that out.
None of your options are good.
You have the cooler option.
But that's only going to last as long as the ice does.
And how you're actually going to regulate the temperature
to keep it in the range you need it to be in,
that's anyone's guess.
You can get a generator.
However, some people can't afford them.
And then you also run into the issue
of them running out of fuel.
One that I've seen that I thought
was unique was somebody getting one of the little refrigerators
that you plug into a car, because then it was mobile.
And I thought it was a unique answer.
But still, eventually, you'd run out of gas.
There is no good answer to this.
And as a demonstration of how much of a problem it is,
if you're an insulin-dependent diabetic, you can't deploy.
Can't join the military, because it's
too hard to maintain the refrigeration.
It's a limiting factor that even with the logistical capability
of the United States military, they still
haven't found a way around it.
Not a reliable one.
What are the essential classes?
I am a huge advocate for the WUFR, the Wilderness First
Responder.
Because again, it's teaching you to do more with less.
At the same time, that course is expensive.
I think the cheapest one I've ever seen was $500.
And it's also pretty intensive as well.
There are a bunch of courses out there for first aid.
Any of those.
Make sure you have good first aid training.
You know how to stop a bleed.
You know how to apply a tourniquet.
The basic stuff.
And then I would go to archive.org, type in filled manual,
and look through the topics.
That website has just this massive collection
of military filled manuals.
When people think of military filled manuals,
you think of soldier stuff, right?
The thing is, there are manuals on basic carpentry,
on how to generate electricity, all of this stuff.
It's there.
And if you read these and just kind of read through them
once or twice, you're going to remember them.
They're written in a way so everybody can understand it.
They have lots of diagrams, lots of pictures,
lots of breaking it down into plain English.
So I would definitely recommend that.
How do you get your friends ready as well?
That's a tough one.
And the answer is zombies.
The thing I've seen have the best results
is to not try to talk about hurricanes, or floods,
or wildfires, or whatever it is that's in your area.
Talk about zombies.
And if you do that, what would you
do in a zombie apocalypse type thing,
people actually start thinking about it.
And they put more thought into it
than they would if you asked them
to talk about something that could actually happen.
And then once it's done, once they're done describing,
hey, that's the same thing that you
would need to do for this natural disaster.
It's a really good metaphor.
So I would go that route because inevitably
when they're describing what they're
going to do in their zombie apocalypse plan,
it's going to include teaming up with their friends.
So I would go through that and then try to work that
into real-world subjects.
There's heavy duty work to do.
There's heavy flooding in my area.
What's an additional thing that I would need?
A boat, obviously.
And that sounds like a joke.
I'm not joking.
There's a company called Intex.
And they make very inexpensive little rafts.
They range from $20 to maybe $200.
But that one seats like 10 people.
Get one of those.
If you're looking for extra stuff
and your concern is flooding, yeah, get a raft.
How much is too much?
Are we looking for a week or a year?
Minimum two weeks.
You want to have two weeks of supplies
because you can end up in a situation like Katrina
where it just takes forever for the help to get there.
When you get beyond that, when does it become too much?
When it's no longer about being prepared
and it becomes a habit unto itself.
It becomes something that isn't about planning
for a specific event.
It becomes a lifestyle.
That's when I think it's too much.
If you can spend one day every three months or whatever
and you're prepared for 10 years, I mean, that's fine.
It doesn't consume your life.
Comfort items to keep your mind busy.
Playing cards, travel board games, coloring books,
stuff like that.
Let's see, does emergency prep look different
for targeted minorities?
Yeah, everything looks different for them.
Yes.
If it was me, I would focus on being even more self-reliant
because when you look at news coverage about hurricanes,
when that helicopter is flying over
and they see a bunch of white people coming out
of a supermarket, white people find supplies.
Well, they don't say white people,
but they say people find supplies.
That's what's in the headline down below.
That's what's on the ticker.
And when they're black, looting after storm.
Law enforcement will probably view it the same way.
I would try to be even more self-reliant
because there probably not going to be much help.
MREs or canned goods.
Well, you have a number of things to consider here.
One is price.
Canned goods are way cheaper, way cheaper.
Then you have the length of time you're
planning on storing them.
MREs, they last a little bit longer.
I don't know that it warrants the price,
but they do last a little longer.
And then you have to look at how many people
you're talking about.
If you have a giant family, you probably
want to go with canned goods.
If it's just you, you can probably
do a couple cases of MREs.
But canned goods for most people is probably
going to be the answer because you can rotate them,
put the new stuff in the back, the stuff that was in there
moves to the front, and you can keep a base built,
and it's inexpensive to build it up because canned goods are
cheap.
So canned goods are going to be the answer for most people.
When does being prepared become being overprepared?
When that's what you're focused on.
Emergency preparedness is supposed
to be something that is freeing.
You don't have to worry as much because you already
have it taken care of.
It's not supposed to become a lifestyle.
It's not supposed to become something
that's totally consuming.
It's supposed to be a lifestyle that's
freeing and that's not going to be a lifestyle that's
going to be a lifestyle that's totally consuming.
It's supposed to be there when you need it
and to alleviate concern.
You don't have to worry about this
because you have that handled.
And then there's a suggestion to ask,
what are you preparing for?
And that's a really good question.
Not just what type of event, but are you
talking about short term, medium term, long term?
How long are you talking about?
Most people just need short term, a few weeks.
When you start looking into longer than that,
you're talking about major issues
where you could be isolated or stranded.
Or you're talking about, when you get to the long term stuff,
you're talking about the breakdown of civilization.
That's an entirely different type of preparedness.
That requires a whole lot of different thought processes.
And that's not, I don't want to say that that's not something
you should do because climate change is real.
But I would plan more for resiliency
than just the breakdown of civilization.
But I'm an optimist.
What about people with disabilities or people
with mobility issues?
This is another question that people
tend to dodge because there's no good answer.
If you live somewhere that's secure,
your best bet is probably not to bug out.
It's to shelter in place and stay where you're at.
And you work on making that place, that location,
more resilient.
If that isn't an option, the next thing
is just realizing that your solution to emergency
preparedness is more about being aware,
knowing when the storms are coming,
knowing when fires are getting close, that type of stuff.
So you can get out ahead of time with time to spare
and you can evacuate.
Because again, mobility, that is one of those things
that it's limiting to the degree that the military itself hasn't
really found the logistics to deal with it.
And you're looking at setting up that same type
of logistic network by yourself.
It's hard.
The answer there is you secure that location
and you get it prepared.
And for people who are in that situation,
I would go well beyond two weeks.
Because two weeks is what it takes for them, in some cases,
to set up distribution points.
For them to get to the point where people will bring stuff
to you, it might be another two weeks.
So I would go a little bit beyond on that one.
That or if you have the ability, be prepared, be alert,
and remove yourself from the situation.
Remove yourself from the situation beforehand.
Those are your best answers.
Water filtration.
And this question also had some stuff
like asking about different equipment and how to use it.
The first video, I think it's actually
called the Roads to Basic Survival
or something like that, we go through
how to use several different types of filter
and demonstrate how to use them, sucking water out
of a less than clear pond.
Now soon, we will have a video here
that shows how to build a solar still as well.
And then how do you get power from a car battery?
Keith, the guy in that video, gave me this.
I don't know if he made it himself or he bought it,
but I'm sure they can be bought.
This end, jumper cables.
The other end, a lighter.
So you can drop a USB connection in here,
the type you plug into the lighter in your car.
Or you can get a power inverter that
uses a lighter adapter in a car.
And then you have it.
You can use car batteries to power the power adapter
or the power inverter.
And those things, power inverters,
run like $20 at Walmart, maybe $40 at AutoZone,
something like that.
And those give you outlets like you have in a wall.
And then the last one.
How can you help in a disaster without getting in the way?
Stay off the line.
That's it.
Stay off of the immediate front line.
Everywhere else, believe me, having
done a lot of relief work, they'll find a spot for you.
There's always something that can be done.
And if you show up and you say, I'm here to help, believe me,
they will put you to work.
The key thing is stay out of the immediate impact area,
like where people are being cut out of houses,
or if there's a fire, obviously you
don't want to be near that.
Stay away from that front line until you're
really ready to do that.
And again, that's a different world.
When you start doing that stuff, you're
accepting a whole lot of risk and liability.
So you can show up at the relief centers
and just say you want to help.
They will find a spot for you that isn't in the way.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}