---
title: Let's talk about why the cost of beef will go up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=i-mK-RGaQrM) |
| Published | 2022/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ranchers are taking portions of their herd to market early due to drought affecting pasture land, particularly in areas like Texas.
- Normally, when pasture land goes bad, ranchers bring in hay from outside, but fuel prices are too high to make that feasible in this large-scale disruption.
- With cows going to market early and weighing less, there will be less supply with the same demand, leading to increased beef prices.
- Ranchers are holding onto younger cows in hopes of pasture recovery, but if conditions don't improve, more cows will likely head to market.
- The impact of climate change, with increased heat and drought, is making cattle ranching harder and more expensive.
- The water and land-intensive nature of beef production is becoming more challenging with climate change effects.
- The current situation indicates a glimpse of the challenges ahead in cattle ranching due to climate change.
- Bringing livestock up is manageable if there is pasture for them, but it becomes prohibitively expensive if hay needs to be brought in due to widespread drought.
- Beau mentions that those on plant-based diets might see high beef prices lead to a search for alternative protein sources.
- The East Texas Livestock Showroom supervisor raised concerns about how high prices could go before people seek alternative proteins.

### Quotes

- "You can expect to pay more for beef."
- "It's water intensive. It's land intensive."
- "You are starting to see the first glimpses of it right now."

### Oneliner

Ranchers facing drought-induced challenges are sending cattle to market early, leading to higher beef prices, showcasing the impact of climate change on ranching economics and sustainability.

### Audience

Ranchers, consumers, policymakers

### On-the-ground actions from transcript

- Support local ranchers by purchasing beef directly from them (suggested).
- Look for alternative protein sources to reduce reliance on beef (suggested).

### Whats missing in summary

The full transcript provides a detailed insight into the current challenges faced by ranchers due to drought and climate change, impacting beef prices and sustainability.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about ranching,
and cows, and drought, and climate change,
and economics, and why your beef is probably
gonna cost a little bit more come fall
and all the way through the holidays.
Ranchers, a lot of ranchers are taking
decent sized segments of their herd to market now.
If you know about ranching, you can go ahead
and skip the next like minute, minute and a half
of this video.
There's a drought.
We know that about the southwest.
As far as pasture land goes, it is impacting areas
as far east as east Texas.
The number I saw said about four fifths
of the pasture land in Texas was rated poor or very poor.
That's bad.
Now under normal circumstances, if your pasture goes bad,
something happens, you bring in hay from outside.
However, because the area that is being disrupted
is so large, you can't really do that
because fuel prices are too high.
So you can't bring in the hay to feed the cows.
So they're going to market early.
Since they're going to market early, they weigh less.
Since they weigh less and they aren't going to gain any weight,
that's going to create less supply with the same demand.
So you can expect to pay more for beef.
Now from what I understand, the ranchers out there
are at this point just using segments of their herd,
not all of it at once.
They're keeping the younger cows and hoping
that something is going to happen and bring back
the pastures.
At this point, they would need a tropical storm or hurricane,
something that's going to come in and sit and drop
a bunch of water over a few days to bring that pasture back.
If that doesn't happen, those cows
will probably be going to market sooner as well.
I've heard anecdotally that the same thing is happening
not just in Texas, but pretty much everywhere else,
all the way up to Oklahoma.
So I would be ready for that.
Now when people talk about climate change,
one of the things that always comes up
is the right wing response of, well,
they don't want us to eat hamburgers or beef.
It's not that people don't want you to.
I mean, some don't.
But it's this.
It's water intensive.
It's land intensive.
There's going to be increased heat, increased drought,
more evaporation.
It's going to be harder and harder to do.
You are starting to see the first glimpses of it right now.
So that's what's happening.
And honestly, I don't see a lot of change.
And honestly, I don't see a way for that really to get better.
It's one of those things where if you have pasture,
if you have something to feed the cattle,
then it's actually not horribly expensive
to bring the livestock up.
But if you're having to bring in hay,
it gets incredibly expensive.
If you're talking about a really wide area
that you can't get hay in because everywhere is impacted
by the drought, it becomes cost prohibitive to feed the cows.
So that's something that's on the horizon.
Now, for those who were probably cringing
throughout this entire thing, those on plant-based diets,
I would point out that one of the larger livestock showrooms
is the East Texas Livestock Showroom.
And the person there that kind of runs it
said they wondered aloud about how high prices could get
before people looked for a different type of protein.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}