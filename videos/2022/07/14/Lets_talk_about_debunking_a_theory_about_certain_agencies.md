---
title: Let's talk about debunking a theory about certain agencies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9i5gkFvRUsg) |
| Published | 2022/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the reasoning behind government agencies' seemingly odd purchases of weapons, addressing concerns raised by the public.
- Points out that every federal agency has an Office of the Inspector General with law enforcement functions, hence the need for weapons.
- Mentions specific agencies like the IRS, Department of Education, and NOAA that have their own law enforcement divisions requiring firearms.
- Provides insight into the Department of Energy's Office of Secure Transport, which transports nuclear weapons and materials across the country, justifying their high-end purchases.
- Notes that agencies like Veterans Affairs also have police forces to protect hospitals.
- Emphasizes that the widespread presence of law enforcement functions within federal agencies showcases the extensive reach of policing in the United States.
- Clarifies that the concern over agencies purchasing weapons periodically is not a cause for alarm but rather stems from a lack of understanding about these agencies' functions.
- Draws a distinction between conspiracy theories and the factual basis of government agency purchases, stating this topic falls into the latter category.
- Addresses the common question of why non-traditional agencies like Department of Energy require firearms, shedding light on their unique functions.
- Concludes by reassuring the audience that the situation is nothing to worry about, framing the issue as more about curiosity than alarm.

### Quotes

- "There is a maze of police agencies, law enforcement agencies, armed agencies throughout the federal government that you've never heard of."
- "It really goes to show exactly how widespread law enforcement functions are in the United States."

### Oneliner

Beau sheds light on the reasons behind government agencies purchasing weapons, revealing the extensive presence of law enforcement functions within federal bodies and dispelling concerns as more about curiosity than alarm.

### Audience

Government Accountability Advocates

### On-the-ground actions from transcript

- Contact your local representatives to inquire about the law enforcement functions and purchases of weapons by government agencies (suggested).

### Whats missing in summary

The full transcript provides a detailed explanation of the rationale behind government agencies' purchases of weapons and the extensive reach of law enforcement functions within federal bodies.

### Tags

#Government #Agencies #LawEnforcement #FederalGovernment #Weapons #Purchases


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about government agencies
and their purchases and how, at times,
they can seem odd to the point of really raising eyebrows.
I had quite a few questions come in about this,
because I guess this topic has arisen again.
I'll go ahead and start off by telling you
this is a, quote, conspiracy theory, but also not.
This isn't like the normal kind that we
talk about on this channel, where people have just
made stuff up.
This is something that is very much grounded in fact,
but the reason the question arises
is because people don't understand the bureaucratic maze
that is the federal government.
So a whole bunch of questions about why various agencies
are purchasing weapons.
And we're talking about IRS, Department of Education,
Department of Energy, a whole bunch.
And any time this comes up, and I have dealt with this
since 2013 or so is the first time
I've had somebody ask me about it,
it's presented in a way that is very alarming,
because why are these agencies, why do they need these guns?
OK, so basically every federal agency
has an OIG, Office of the Inspector General, cop.
They have guns.
A lot of times, an agency that you may not really
think about it in that way has a law enforcement function.
Department of Education has the Civil Rights Division
or something like that, Office of Civil Rights maybe.
Yeah, they have a law enforcement function.
The NOAA, National Oceanographic and Atmospheric
Administration, they have their own cops.
So NOAA buys guns.
The IRS does.
The Department of Energy is normally my favorite,
because most years, they're buying super high-end stuff.
And it makes people really wonder,
especially if they know about firearms,
they're wondering why the Department of Energy
is buying this stuff.
When you think of Department of Energy, what do you think?
Light bulbs, right?
Maybe if you put some thought into it,
you think about the possibility of them
guarding nuclear power plants.
And that could explain some of it.
But we're talking about real high-end stuff in this case.
The Department of Energy has something called the Office
of Secure Transport.
Yeah, they're armed to the teeth,
because they transport nuclear weapons and nuclear material
all over the country in semi-trucks.
You've probably, at some point in your life,
driven by a nuclear weapon being transported in secret.
The people who are part of this office,
most are recruited straight out of high-speed teams
in the military.
And yeah, they have a lot of unique equipment
that you wouldn't expect to find in a civilian agency.
This is true of pretty much every federal agency.
They have law enforcement or paramilitary functions
within them that you may not think about initially.
Veterans Affairs is one that came up a couple of years ago.
People asking about that.
They have cops.
They guard hospitals, which, as we know, have recently,
and for the recent past, become targets.
So yeah, the real takeaway from this
isn't something that's alarming in the sense
that there are weapons and ammo that
is being purchased by agencies that don't need them.
It's that there is a maze of police agencies, law
enforcement agencies, armed agencies
throughout the federal government
that you've never heard of.
It really goes to show exactly how widespread law enforcement
functions are in the United States.
They really are everywhere.
But the alarm that gets raised over this every couple
of years, no, it's nothing to worry about.
At the same time, I don't normally,
like when people ask about this, I don't normally
throw them in the category of people who believe in lizards.
This is more of why on earth is this happening.
So anyway, it's just a thought.
Y'all have a good day. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}