---
title: Let's talk about Liz Cheney, the partisan committee, and Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BWUPH8u_jiQ) |
| Published | 2022/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Egan says:

- Explaining the term "partisan" in relation to the committee, mostly used by Republicans in discussing it.
- Noting that the witnesses are mainly Republicans, particularly mentioning Liz Cheney's involvement.
- Republicans on the committee are trying to correct their party and address what they see as a threat to the country.
- Many Republicans are acknowledging that the strain of authoritarianism seen is not compatible with the United States or its Constitution.
- Cheney expressed that one cannot be loyal to both Donald Trump and the Constitution.
- The committee is viewed as an attempt by Republicans to address and remove what they see as a threat to their party and the nation.
- The committee is not partisan but rather an American effort, with critical evidence and testimony coming from Republicans.
- Beau criticizes those who try to undermine the committee's credibility by labeling it as partisan, similar to baseless claims about election fraud.
- The importance of the committee is emphasized, with Beau suggesting that many are missing out on a significant political event due to previous allegiances.
- Overall, the committee is seen as a critical effort by Republicans to address internal threats and defend American values.

### Quotes

- "You can be loyal to Donald Trump or you can be loyal to the Constitution. You can't do both."
- "If it is partisan, it is a Republican committee."
- "This isn't partisan. This is American."
- "This committee may very well be one of the most significant political events that occurs in your life."
- "Many are missing out on a significant political event due to past allegiances."

### Oneliner

Republicans on the committee are addressing threats to their party and country by confronting authoritarianism, making it an American, not partisan, effort.

### Audience

Political observers

### On-the-ground actions from transcript

- Watch the committee to understand the critical evidence and testimony presented (suggested)
- Acknowledge the importance of the committee as a significant political event (suggested)

### Whats missing in summary

Full understanding of the depth and significance of the committee's efforts and the need to prioritize American values over partisan allegiances.

### Tags

#Committee #Partisanship #Republicans #Authoritarianism #AmericanValues


## Transcript
Well, howdy there, internet people.
It's Bo Egan.
So today, we are going to talk about the committee,
because there's a term that keeps showing up now
when people are discussing the committee.
Well, when Republicans are discussing it.
And I just kind of want to go through that and look at that.
That term is partisan.
In which way is this a partisan committee?
The witnesses, I mean, they're mostly Republicans by a large margin.
And as far as the committee, Liz Cheney is kind of still on the show if we're being honest.
Cheney, a name synonymous with the Republican Party.
I don't know that partisan is the right word.
doesn't seem to be what's happening here. At least not in the way that the people
who say that intend it. When you look at what's being presented, what you find are
a bunch of Republicans trying to correct their party. They're trying to address
something they see as a threat to their party and a threat to the country.
That's what the committee has really shaped up to be.
So yeah, I mean, you could say it's partisan if you mean in the sense of it has to do with
one party trying to fix itself.
There are a whole bunch of Republicans who looked at what happened and realized that
That strain of authoritarianism is not compatible with the United States because it's not compatible
with the Constitution.
Cheney said it herself, you can be loyal to Donald Trump or you can be loyal to the Constitution.
You can't do both.
observer here to those Republicans watching this, please understand that
from my perspective, yeah, the name Cheney is synonymous with Republican. It's also
synonymous with authoritarianism. So from where I'm sitting, this is a Cheney
looking at Republican authoritarianism and saying, whoa, whoa, whoa, that is way
too far, which is wild to me. The desire to cast this committee as partisan, to
undermine the end results of this committee by painting it in that
partisan light is, well, it's the same thing they did with the election. They
say over and over again, oh there's fraud, there's fraud, and it's contrary to all
of the evidence. It's a baseless claim, but they say it enough and people believe
it. Watch the committee. Go back and see where the the most critical evidence
came from, where the most critical testimony came from. They all have ours
after their name. If it is partisan, it is a Republican committee. It is the
Republican Party trying to perform surgery on itself in public and get rid
of something that shouldn't be there, that they see as a threat to their party
and a threat to this country. Undermining things that the Republican Party, I mean
And again, outside looking in, the Republicans have never been great about defending some
of this stuff.
But this went too far even for them.
It's not partisan, not in the way they're trying to frame it.
That came up because they weren't allowed to disrupt it.
There were too many people willing to come forward to serve on the committee that had
ours after their name.
There are too many witnesses coming forward with the evidence, with the testimony that
have ours after their name.
This isn't partisan.
This is American.
This committee may very well be one of the most important political events that occurs
in your life, and a whole lot of people are off missing it right now because some dude
sold him a red hat a few years ago, and they're too scared to admit they were wrong about
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}