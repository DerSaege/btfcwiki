---
title: Let's talk about finding your community network....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2l6F3_3sARQ) |
| Published | 2022/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of starting a community network and finding people for it.
- Talks about receiving messages from people wanting to start a community network but not knowing where to find like-minded individuals.
- Explains using Twitter's search function to find people nearby.
- Shares personal experience of using Twitter to find people close by.
- Mentions searching for specific terms related to like-minded individuals.
- Describes finding familiar and unfamiliar people through the search function.
- Suggests reaching out, getting to know people online, and eventually meeting in person.
- Mentions that most social media platforms have similar search functions.
- Encourages transforming online communities into real-world impactful networks.

### Quotes

- "Got some people I want you to meet."
- "You can transform that online community into something they can do good in the real world."

### Oneliner

Beau explains how to use social media platforms like Twitter to find like-minded people nearby and build a community network that can make a real-world impact.

### Audience

Community builders, social media users

### On-the-ground actions from transcript

- Use social media platforms like Twitter to search for like-minded individuals nearby (exemplified).
- Reach out, get to know people online, and plan to meet in person (exemplified).
- Transform online communities into impactful real-world networks (implied).

### Whats missing in summary

The full transcript provides a detailed guide on leveraging social media platforms to connect with individuals nearby and establish a community network with real-world impact. Viewing the full transcript can offer additional insights and personal anecdotes from Beau on this topic.

### Tags

#CommunityBuilding #SocialMedia #Networking #TransformativeChange #OnlineCommunities


## Transcript
Well, howdy there, Internet of People. It's Beau again.
So today we're going to talk about how to start your community network.
How to find the people for your community network.
I mentioned community networking in a recent video,
and I got a bunch of messages, mainly from people saying,
I really want to start one.
Have no idea where to find people.
I don't know how to find like-minded people.
So, these messages came in over Twitter.
So I'm going to tell you how to do it using Twitter.
A standard caveat about meeting strangers from the Internet and all of that stuff.
On Twitter there's a search function.
You can type in any word you want, click search.
Next to the little magnifying glass, there's another icon.
Looks like a slider.
If you click that, it brings up an option to search nearby.
You have to use the phone app to do this.
You click nearby, and it brings up people that are close to you geographically.
I tried this myself.
And at first, because I live out in the middle of nowhere,
I typed in the word the and clicked the button and went to nearby.
And yeah, there were a whole bunch of people,
which is surprising to me, to be honest.
So then I started searching for terms that were a little bit more specific
to people that are of my mindset.
And I found a lot of what I expected, people I knew,
because let's be honest, where I live, there's not a lot of people of my mindset.
So most of the names that I saw were people I already knew.
But that's because I live in an area where there's only like 10 of us.
But even in my tiny, tiny, tiny area, there were quite a few I didn't know.
Or in one case, there's a person who owns a store that I have shopped at repeatedly,
who I know, who I didn't know their political views,
but I recognize her profile picture.
And I found them by typing in the word row.
And I promise you, next time I go into that store, I'm like,
hey, saw your Twitter account.
Got some people I want you to meet.
And you can find your base that way.
You can reach out to them, get to know them online,
and then one day meet somebody somewhere very public, you know,
and start building it in that manner.
Most social media platforms have a function like this.
I'm sure on Facebook, there's some way to search by location and keyword.
You can find them that way.
So that might be the route.
If you really don't know anybody, you can't find those,
that first group of eight people or whatever to start building that network.
Maybe that's a tool you can use because even in an area as small as the one I live in,
there were a number of people who either I didn't know their political views
or I had never met, which is weird.
But it's an option, and you can do this with just about any social media network.
And you can transform that online community
into something they can do good in the real world.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}