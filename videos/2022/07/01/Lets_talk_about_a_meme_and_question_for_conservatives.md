---
title: Let's talk about a meme and question for conservatives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2Qtkf2LkawI) |
| Published | 2022/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing a meme sent by a conservative follower that portrays stick figures labeled left, right, and me.
- Revealing the message behind the meme and discussing the implications.
- Posing a question to conservative friends and family about changing political opinions.
- Expressing surprise at a conservative follower who admitted to never changing their political opinion.
- Noting the rigidity and inflexibility of holding the same political opinions for over thirty years.
- Criticizing the arrogance and entitlement associated with refusing to adapt political beliefs despite new information.
- Pointing out the entrenched nature of political beliefs that hinder progress and adaptation.
- Contrasting the perception of entitlement between young people and conservatives in terms of political beliefs.
- Critiquing the lack of substantial changes in the Republican Party's positions over time.
- Addressing the resurgence of a far-right movement within the Republican Party and its implications on revisiting past laws.
- Challenging the notion that the world should conform to conservative beliefs without updating them.
- Encouraging provoking political discourse by asking when was the last time someone changed their political opinion.
- Contrasting the readiness to adapt political opinions between left-leaning individuals and conservatives.
- Emphasizing the importance of accepting new ideas and information to progress and lead in society.
- Criticizing the tendency to cling to outdated political talking points instead of embracing progress.

### Quotes

- "When's the last time you changed your political opinion on something?"
- "That is wild."
- "You want the United States to be a world leader, you have to lead."
- "They're meeting the world head on instead of hiding in the past."
- "There's no progress at all."

### Oneliner

Beau challenges conservative beliefs by questioning the refusal to adapt in a changing world, criticizing entitlement and lack of progress.

### Audience

Conservative friends and family

### On-the-ground actions from transcript

- Provoke a political discourse by asking conservative friends and family when was the last time they changed their political opinion (suggested).

### Whats missing in summary

The full transcript provides additional insights into the rigidity of conservative beliefs and the necessity for adaptation in a changing world.

### Tags

#ConservativeBeliefs #PoliticalOpinions #Progress #Adaptation #Entitlement


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about a meme I was sent.
And the message behind that meme.
And then we're going to talk about a simple question
that you might want to ask your conservative friends and family
because it might prove to be pretty illuminating.
So, one of the people who follows the channel,
who is a conservative,
okay, now when I say that I mean like normal, sane conservative,
not, you know, crazy one.
He sends me this message and it has this meme attached.
And you might have seen the meme. It's three stick figures, right?
And one's labeled left, one's labeled right, and one's labeled me.
And they're all standing equal distances apart.
And then in the next frame, the one on the left,
the one that's labeled left, runs further left, right?
And the general tone of it is, you know,
I didn't move further right, the left moved away from me.
Yeah, okay, if you say so.
I asked a simple question.
When's the last time you changed your political opinion on something?
There was a pause,
then the answer came back. Never.
This guy's older than I am.
So, he's probably held these opinions, these same opinions,
for thirty years or so.
That is wild.
Think about everything that's happened in the world.
But your opinions, they're infallible.
All of the new information,
all of the new data that has entered the world,
that is available at your fingertips, on your phone,
none of it has changed your political opinion at all.
In fact, you become more entrenched, right?
Because you're not going to move at all.
Even though the world is moving on without you.
Man, that is the height of arrogance right there.
And it's easy to actually just write it off as, you know,
stubborn, arrogant people.
But it's more than that.
It's the whiny, entitled nature
that comes along with having that rigid conservative belief.
And I know that sounds kind of backwards
because they always say that, you know,
it's the young people who are entitled.
No, the young people don't expect the world to stop
so their political talking points from thirty years ago are still valid.
No, that's an entitlement you only find among conservatives.
Those who believe that they can just disregard
everything that has happened over the decades.
And when you think about it,
the Republican Party as a whole,
when's the last time they've changed their positions
on anything substantial?
When the Southern strategy happened?
I mean, other than that, it's been really minor.
And now, because of the rebirth of this bizarre far-right movement
that has emerged within the Republican Party,
they want to go further back.
You know, these Supreme Court cases that the justices want to revisit,
some of them are from the mid-1900s.
And because of conservative talking points,
you've got what, the governor there in Texas saying,
you know what, if those get overturned,
we are absolutely going back to those old laws.
That is wild.
The world hasn't changed at all.
They don't want it to change.
You can't stop it. It's going to.
That is just an entitled nature showing itself,
believing that the whole world should bend to your way of thinking,
even though you won't update your way of thinking,
even with all of the new information that has come in.
If you want to provoke a conversation
with your conservative friends and family,
this might be the question to ask.
I mean, when it gets turned around, you know,
if somebody who's a lefty or even a mainstream liberal, progressive,
if they get asked,
when's the last time you changed your political opinion about something?
It's not measured in years.
It's measured in books, you know.
It wasn't that long ago.
They will have shifted in a major way because that's the idea.
They're trying to move forward.
They're accepting new ideas, new information.
They're meeting the world head on instead of hiding in the past.
If you want the United States to be a world leader, you have to lead.
You can't actually go back and hide and just stick your head in the sand
and pretend that the modern world doesn't exist.
You want to go back to an era before cell phones,
before cordless phones to find your political talking points.
That's the problem.
There's no progress at all.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}