---
title: Let's talk about Biden, inflation, and Lucky Charms....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UQFBDqGt4zo) |
| Published | 2022/07/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party has been trying to connect Biden to inflation, despite it being a global problem.
- Key elements like crude oil, lumber, used cars, cereal, and shipping rates are starting to decrease.
- Companies increased prices during the pandemic to recover lost profits, contributing to inflation.
- If inflation eases before the midterms, it could trouble the Republican Party.
- Job growth remains steady, and if inflation gets under control, it will dismantle a manufactured talking point against Biden.

### Quotes

- "The Republican Party has done everything within its power to connect Biden to inflation."
- "If inflation comes under control, it's going to definitely rob them of one of their talking points, one that they manufactured."

### Oneliner

The Republican Party's attempt to blame Biden for inflation may unravel as key elements start to decrease, potentially impacting the midterms.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Monitor economic trends and stay informed (implied)
- Stay updated on political narratives and their impact (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the factors influencing inflation and the potential political implications as trends shift.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about why Biden
might want to thank his lucky charms.
The Republican Party has done everything within its power
to connect Biden to inflation.
If you live overseas, if you're watching this from overseas,
you understand that doesn't make any sense
because it's a global thing.
Inflation is a global problem right now.
But that hasn't stopped the Republican Party
from lying to its base and trying to connect the two
and trying to reach into that independent group
and connect Biden to inflation, make
it seem as though it's his fault.
They've actually had some success doing this.
Here's the thing.
Nobody's going to say that it's over.
But there are some key elements that
are starting to go the other way.
Crude oil, lumber, used cars, cereal, and most importantly,
shipping rates, the cost to ship one of those big containers,
all of these things are going the other way now.
Now, this isn't going to have an immediate effect.
And I mean, something could still
happen and send them back up.
But at this moment, it looks like things
are going to start to ease.
Now, did Biden do this?
I mean, with the exception of maybe a little bit
of the crude prices?
No, he didn't have anything to do with it.
This actually doesn't really have much
to do with the people in power.
This is a market thing.
Most of the inflation, in my opinion,
was caused by companies jacking up prices much higher
than they needed to in an attempt
to make the money they didn't make during the pandemic,
when the pandemic was in full swing
and they were losing money because people weren't out.
So they were trying to recoup that.
They jacked up prices on consumers.
And that, to me, seems to be where most of it came from.
That's why there's record profits.
But since the Republican Party has
done a pretty good job of connecting Biden to inflation,
Biden-flation, right?
If it starts to go down and if things start to normalize
before the midterms, they might be in trouble.
There's still steady job growth.
Everything else is looking pretty good.
If inflation comes under control,
it's going to definitely rob them
of one of their talking points, one that they manufactured.
I mean, they went out of their way to lie and create this.
And it may end up making Biden look good, which is, I mean,
to me, this is funny because it really isn't Biden.
And they manufactured the talking point.
And now it may actually.
Anyway, so these trends, they're starting to move back down.
It could take a month for this to start spreading.
And it could go back up.
This isn't an exact science.
But you're starting to see these happening.
But with the shipping rates going down,
that's going to help cross the board.
And it's going to be harder and harder for companies
to justify keeping the prices jacked up to make those profits.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}