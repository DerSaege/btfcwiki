---
title: Let's talk about the Orville, interpretation, and Topa....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=r80t29BtzIE) |
| Published | 2022/07/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the importance of defending positive interpretations in culture and media.
- Talks about the show "The Orville" and a specific episode that sparked interest.
- Describes a society in the show with strict gender rules and a child wanting to transition.
- Emphasizes the two different interpretations of the episode: supportive vs. right-wing view.
- Points out the positive symbolism and messages in the show regarding breaking gender rules.
- Stresses the need to defend and uphold positive messages in media for cultural impact.
- Advocates for standing up against negative interpretations and ensuring positive ideas are supported.
- Mentions the significance of creators' intentions and how ideas resonate in society over time.
- Encourages defending positive impacts of media messages for long-lasting effects on society.

### Quotes

- "You have to have the positive message out there."
- "Ideas stand and fall on their own."
- "We have to make sure that when good ideas get out there, that they can stand, that they're backed up."
- "No matter how silly it may seem, we have to defend the positive impact."
- "It's worth your time because it has a long, long lasting impact."

### Oneliner

Beau stresses defending positive cultural interpretations and media messages for lasting impact, especially in the face of differing views.

### Audience

Culture Defenders

### On-the-ground actions from transcript

- Reach out to creators of media to understand their intentions (suggested).
- Support positive interpretations and messages in media (implied).
- Defend and uphold positive impacts of media content (implied).

### What's missing in summary

The full transcript provides a detailed analysis of a specific episode in "The Orville," showcasing the importance of defending positive interpretations and messages in media for long-term societal impact.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about interpretations
and culture
and why
it might be important
to defend
interpretations
that are positive.
And we're going to talk about The Orville,
which is a show on Hulu.
We're going to talk about that show in particular because
like fifty of y'all messaged me asking me about a specific episode.
I'm not really sure why. I have never mentioned this show
because I have never seen a single episode.
So over the last week
I watched the entire series.
Now,
for
those who aren't familiar with it,
it's
Star Trek but not.
It's a show that exists
in a very similar universe
to Star Trek.
But it is not part
of the Star Trek universe. It's kind of a
parody in some ways.
And the first episode is kind of goofy.
But as the series progresses it really does turn into a normal sci-fi show
with a lot of social commentary.
What
kind of piqued people's interest
was a particular storyline
involving a society that has very very strict
rules when it comes to gender.
You're male.
Everybody.
Everybody's male.
Born female?
Nope.
Boom. Now you're male.
Everybody's male.
This rule set
is forced upon people
by society as a whole.
And
in the storyline of the show there is a child
who was born female.
Now one parent is
very supportive
of breaking the
gender rules
and
allowing her to stay
female.
Another parent is incredibly
determined to make sure that
gender rules are
abided by.
The
parent who
wants the rules abided by wins the initial contest.
So
the child is male.
And then the series progresses.
The episode in question that prompted all of the messages.
Well
the child is uh...
less than happy.
So much so
that they start asking questions about
what it would be like if they weren't even here anymore.
This leads to the discovery
that
hey
yeah we knew early on that
you really weren't male.
And the child wants to transition
to female.
And it happens.
Now the reason this has prompted so many questions is because there are two reads
to this.
There's
the one that I see
which is
I would
say
more than sympathetic, I would say very supportive
of
people who transition in
our world,
in our society. Those who want to
get rid of those
gender rules
that are forced upon people
by society.
So there's that read. And then there's another read coming out of
the right wing
that basically says
look
you know the child transitioned and then had to transition back to be happy.
Yeah, I mean
that's kind of ignoring like everything else.
That's a very literal take
that ignores a lot of context
such as the fact that it wasn't
the child's choice. That was something that society said
this is how you have to behave.
When the child had a choice
to break from that rule,
they did.
And that's when they became happy.
And there's a bunch of things in the show
that would lead you to
the positive interpretation of that.
I think the biggest is the rhetoric of the parents.
The
parent who is
just determined to keep those gender rules in place
uses rhetoric
that is
common
among those who are
sticklers for our gender rules.
Terms like abomination
and malformed,
stuff like that.
Monstrosity.
Horrible stuff.
That aligns with
a certain group today.
Then you have the
obvious
symbolism that occurs in that final episode
when
the child
first appears.
They're on the bridge of a ship.
But it's not real.
It's not real. It's fake.
After the transition,
the episode ends with them on the bridge of a ship.
And it is real.
The happiness, everything
that goes along with it
supports a positive read. A read that is supportive
of people who want to break
gender rules today.
That's what you see.
That's what I see.
But
even though the
right-wing interpretation is lacking,
it doesn't really make sense when you view it in context, especially with the
rest of the show.
It just doesn't add up.
It's still a fight you have to have.
You still have to defend it.
Because when it comes to
the cultural impact
of shows like this, it's big.
You have to have the positive message out there.
You can't let them have an inch
when it comes to this. This is why I do so many
videos that are basically like, nope, that show was always woke, you just didn't
know it.
Because you can't let them have anything.
When it comes to this topic,
when those positive messages are out there, they have to be defended.
Now,
even though
what really matters
is how the content is
received by the viewer, the listener,
the reader.
In this case, I mean, you could just go ask the creators what they intended.
Even though,
to me, that's
not really what matters. How it's received is what matters. Ideas stand and fall on
their own.
But if you were going to do that,
if you were going to try to reach out to the creators, what would you do?
Go to their social media, right?
If you were to do that,
and you were to go to Twitter,
what you would find is that on that social media,
the
show has retweeted
one take.
They have retweeted
comments
that are about it being supportive
of people who want to break those gender rules.
They haven't retweeted any
saying,
yay, thanks for not being woke.
I think that
probably speaks
a lot to their intention.
But their intention only goes so far.
When it comes to stuff like this,
when it comes to things that can resonate throughout society,
and they can either have a positive
impact or a negative impact,
no matter how silly it may seem,
we have to defend the positive impact.
We have to make sure that when good ideas get out there,
that they can stand,
that they're backed up.
Because ideas stand and fall on their own, right?
Those messages,
once they're out there, they resonate
for a very, very long time.
So much so
that there are shows today
that try to emulate
the method
that Star Trek
used to
promote equality
fifty years ago.
It's, uh...
it seems like a silly fight,
but it's worth your time
because it has a long,
long lasting impact.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}