---
title: Let's talk about the most important election of your life....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AR3scusFA1s) |
| Published | 2022/07/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of the election and a message he received regarding electoral politics.
- Mentions that every election is labeled as the most critical one, but things don't necessarily improve significantly.
- Expresses his belief that real change happens outside of politics and that voting is the least effective form of civic engagement.
- Shares that he has never endorsed a political candidate on his channel except for Big Bird against Ted Cruz.
- Acknowledges that the world of cooperation and helping each other is far from reality currently.
- Points out the impact of the Democratic Party's success in elections on various key issues like healthcare, equality, and environmental protection.
- Emphasizes that punishing the Democratic Party for not achieving certain goals ultimately affects the vulnerable demographics they represent.
- Urges people to think about the consequences for those who will suffer if the situation worsens politically.
- Stresses the importance of considering the outcomes of elections beyond just immediate frustrations.
- Encourages viewers to understand the implications of not supporting certain political efforts and the potential consequences.

### Quotes

- "Real change comes from outside."
- "Punishing the Democratic Party because they didn't have the votes… You're punishing the people who are in the demographics that are affected by that."
- "It's not really a surprise."
- "I understand the frustration."
- "Think about whether or not you want it to get worse for those people who are in those demographics."

### Oneliner

Beau stresses the importance of looking beyond electoral politics for real change, while cautioning against actions that could harm vulnerable communities.

### Audience

Voters and activists

### On-the-ground actions from transcript

- Support and strengthen community-based initiatives to address social issues (implied)
- Advocate for policies that benefit marginalized groups in society (implied)
- Engage in grassroots efforts to bring about meaningful change outside of traditional politics (implied)

### Whats missing in summary

Deeper insights into how individual actions can impact broader societal changes through community involvement and advocacy.

### Tags

#Election #RealChange #CommunityEngagement #DemocraticParty #Voting


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about the most important election of your life.
And a message I got.
You talked about people saying not to get involved in electoral politics,
but I think what you're missing is that they tell us every election
that it's the most important election of our lives.
And then if they win, things don't really get better.
They just stop getting worse as fast.
That's discouraging.
I think you don't see this in this way,
because while you cover electoral politics,
you're pretty clear that you think it's a band-aid
and that real change is made outside of politics.
I want that world you say is forever away, where we all cooperate.
We're not voting that in any time soon.
Yeah, I get it.
I mean, that makes sense.
I mean, I understand what's here.
I...
If you don't know, if you're newer to the channel,
I am not somebody who is super supportive of electoral politics.
Blank is the least effective form of civic engagement.
People who have been watching this channel long enough,
they know that the blank is voting.
And I've never endorsed a political candidate on this channel,
with the exception of Big Bird running against Ted Cruz.
That happened.
So I am very much immune to that concept of pushing people to vote.
It's not something that I do.
And yeah, the world I want,
that world that is thinking far, far beyond borders,
and it's just people helping people and cooperating,
because it's the right thing to do,
not because there's some grand coercive government looming over us.
We're just a well-educated populace.
Yeah, that is generations away.
True.
The message is spot on.
And I can see how it would be annoying to constantly hear,
this is the most important election of your life.
I can actually remember the first time I heard it.
I think Perot was one of the candidates.
This has been said a long time.
Yeah, it's the parties getting out their base.
I get it.
I understand.
But as somebody that has years of videos
showcasing that they are not somebody who is just super supportive of electoral politics,
I will tell you the most important elections of your life.
The most important was 2016.
Didn't go well.
The next was 2020.
And sure, he won,
but if you go back and watch those videos without a landslide,
without a mandate,
we're going to be back in the same situation.
Said it at the time.
And here we are.
That world that I want, it is generations away.
As somebody who has a very established track record of not being super supportive of electoral politics,
I understand that if the Democratic Party does not hold in the House and expand in the Senate,
you're going to be looking at social safety nets, voting rights, women's rights, public education,
health care, environmental protections, equality for the LGBTQ community,
the same way I look at the world I want.
Generations away.
The American experiment is on the ballot.
It really is.
And you can go back and look.
I have a very established track record in this regard.
This isn't the most important election of your life.
But it is important.
And if the outcome isn't decidedly in favor of the Democratic Party, there's going to be problems.
And a lot of the things that people who watch this channel care about, they're not getting addressed.
Maybe the Democratic Party, maybe.
OK, the Democratic Party is not great at fixing things.
That has to happen outside of politics, in my view.
But even in this criticism, it doesn't get worse as fast.
Objectively, that's a better outcome.
The idea of punishing the Democratic Party because they didn't have the votes,
because they had two people who wouldn't do what they wanted, or they put tradition over people or whatever.
I get it from a political standpoint on some level.
I understand it.
But understand, you're not punishing them.
They are millionaires who live in gated communities who don't care.
You're punishing the people who are in the demographics that are affected by that list.
That's who gets punished because for them, it will get worse faster, even by your own message.
It is. It's a band-aid. Real change comes from outside.
Absolutely. There are some people who can't get out there for whatever reason.
Pretty much everybody can do this.
I don't see the usefulness in undermining that effort to hold the line or not fall back as quickly.
So, I understand the frustration.
I really do.
But if you go back and watch those videos from 2020, from the election cycle,
if Biden didn't get that landslide, this is what was going to happen.
We all knew it. It's not really a surprise.
So, I understand the frustration.
But think about whether or not you want it to get worse for those people who are in those demographics.
If you want it to get worse, faster.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}