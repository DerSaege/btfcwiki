---
title: Let's talk about 3 factions of republicans, Trump, Pence, and AZ....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0YmvgBYNqI8) |
| Published | 2022/07/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- The Republican Party is divided into three factions with different views on the party's future.
- The party's unity has been its strength, unlike the Democratic Party, which is a coalition.
- The three factions are the Sedition Caucus (Trump supporters), the never Trumpers, and those who believe they can ignore Trump.
- Pence is seen as part of the faction that believes they can ignore Trump by endorsing candidates opposing Trump's choices.
- Trump plans to announce his candidacy to potentially slow down his legal troubles.
- The never Trumpers believe the party can only move forward by dropping Trumpist elements.
- If the party doesn't make a push to distance itself from Trump during the midterms or by 2024, they will face consequences during the elections.
- Pence's endorsements against Trump's candidates are an attempt to move forward without confronting Trump directly.
- Trump may still hold influence over the party and his supporters for a long time.
- The Republican Party faces a choice of pushing back against Trump or being steamrolled by him.

### Quotes

- "The only way for the Republican Party to move forward is to drop the Trumpist elements."
- "Underestimating Trump is what happened in 2016, right?"
- "If they were to do it now, they might be back in shape for 2024."
- "Trump is losing support, but he's not gone."
- "He's going to be dealing with Trump and Trump wannabes for a very very long time."

### Oneliner

The Republican Party is at a crossroads, divided into factions over Trump's influence, facing a choice of pushing back or being steamrolled.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Support and advocate for the faction within the Republican Party that aims to distance itself from Trump's influence (suggested).
- Engage in internal party dialogues and actions to address the divisions caused by differing views on Trump's role within the party (implied).

### Whats missing in summary

Insights on the potential long-term consequences for the Republican Party's unity and electoral success if it fails to address the Trumpist elements within the party. 

### Tags

#RepublicanParty #Trump #Pence #PoliticalDivisions #ElectionStrategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump and Pence
and the three factions of the Republican Party
that now exist and what all this has to do with Arizona.
OK, so the Republican Party is split.
It is split into three very distinct factions.
And they all view the way forward for the Republican Party very differently.
And this is a major problem for Republicans.
The great strength of the Republican Party has been their unity.
The Democratic Party, it's a coalition.
It's leftist and liberals and it's a cross-section of a lot of people who don't necessarily agree
on things.
to get stuff done, right? The Republican Party has always been very unified. That's gone.
That doesn't exist anymore. So, the three factions as it stands right now, for lack
of a better word, you have the Sedition Caucus. You've got the Trump crew, the people who
still support Trump, the people who have modeled themselves after Trump in that authoritarian
style, the group of people who see Trump or people like Trump as the way forward for the
Republican Party, right?
On the other end, you have the never Trumpers.
You have the people who know that the only way forward is to push back against Trump,
To get that messaging, to get that idea out of the party, and to just clear the Republican
Party of anything that is Trumpian.
Then you have the people who believe they can ignore Trump.
Politics is usual and just forget about it.
Don't talk about it.
act like it never happened and move forward in that manner.
Pence appears to be in that category right now.
In Arizona, Pence has endorsed Robson for governor.
Now, whoever Trump endorsed is part of the sedition caucus,
lack of a better term, right? And the idea is that if Pence can win here, well it shows that that's
the way forward because he won in Georgia. Remember Pence endorsed Kemp and Trump endorsed whoever
lost, right? So that middle faction, they believe that they can show this is the
way forward. However, they're missing something that the never Trumpers know.
Trump is going to announce his candidacy. Trump believes, rightly or wrongly, that
announcing his candidacy will, it will slow the legal troubles that he's facing.
So he's going to. They know that. And then all of these figures who have been
quietly working to just move forward, they're gonna have to deal with Trump.
He's going to be on the stage with them and then they're going to have to make a choice.
Do they push back or do they get steamrolled again?
Underestimating Trump is what happened in 2016, right?
The never Trumpers are right.
The only way for the Republican Party to move forward is to drop the Trumpist elements.
Yeah, it's going to cost them an election or two.
If they were to do it now, they might be back in shape for 2024.
I don't know if they will, but if they don't do it now, if they don't make that push during
the midterms, they're going to have to do it during 2024.
And all of the fallout that occurs will happen then during that election because Trump's
going to run.
So the endorsements that Pence is making in opposition to Trump's candidates, that's
his way of trying to move forward without directly confronting Trump.
It's not going to work, not in the long term.
Trump is losing support, but he's not gone.
If he gets up on that stage and just steamrolls them the way that he has in the past, and
just pushes them into doing his bidding, they're going to be dealing with Trump.
and Trump wannabes for a very very long time. Anyway, it's just a thought. Y'all
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}