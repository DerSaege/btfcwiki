---
title: Let's talk about the memo, Garland, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pPDVnHT-vv8) |
| Published | 2022/07/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a Department of Justice memo requiring approval for investigations into politically sensitive figures like presidential candidates to maintain DOJ's apolitical nature.
- Mentions that similar memos have existed for years and the purpose is to prevent the perception of partisan activity within the DOJ.
- Emphasizes Merrick Garland's response that nobody is above the law when asked about investigations, including former President Trump.
- Garland's decision to use a method starting from small investigations to big rather than a hub and spoke method is discussed, showing his commitment to conducting investigations privately and without leaks.
- Describes Garland's belief that the investigation into Trump is significant, relating it to the events at the Capitol on January 6th.
- Views the investigation as a wide-ranging conspiracy, potentially alarming certain individuals in Congress or former government positions.
- Acknowledges Garland's commitment to DOJ's apolitical stance but notes the influence politics can have on investigations.
- Speculates on the possibility of the investigation turning into a criminal matter against Trump based on Garland's statements about following the facts.
- Raises concerns about individuals wanting to protect the institution of the presidency possibly interfering with the investigation's integrity.
- Concludes that despite political posturing, Garland's commitment to following the facts offers hope for a thorough investigation.

### Quotes

- "No one is above the law."
- "This is the most wide-ranging and most important investigation in DOJ's history."
- "An investigation into Trump is part of what happened at the Capitol that day."
- "Garland has made it clear that in this investigation they're going to go wherever the facts lead them."
- "But, like a whole lot of this, we just have to wait and see."

### Oneliner

Beau explains a DOJ memo, Garland's response on investigations into politically sensitive figures, and implications of the wide-ranging Trump investigation.

### Audience

Legal analysts, concerned citizens

### On-the-ground actions from transcript

- Contact legal experts for insights on DOJ procedures and investigations (suggested)
- Stay informed about updates on the investigation and its implications (exemplified)

### Whats missing in summary

Full context and depth on Garland's approach to investigating politically sensitive figures and the wide-ranging implications of the Trump investigation.

### Tags

#DOJ #MerrickGarland #Investigations #Apolitical #Trump #CapitolInsurrection


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
that memo, that Department of Justice memo. We're going to talk about Merrick Garland's
response to that memo. We're going to talk about the DOJ being apolitical. And we're
going to talk about something I think Garland let slip that I think most people missed.
Okay, so if you have no idea what I'm talking about, a memo surfaced. This memo says, hey,
and it's directed to people in DOJ, if you're going to open an investigation into somebody
who is politically sensitive, meaning somebody who has declared their candidacy for president
or something like that, you need approval. It's not that these investigations can't happen.
It's that somebody higher up in the organization has to say yeah. Okay, first, this isn't really
a bombshell. Not really. Memos like this have been on record for years and years and years
and years and years. This isn't a new policy. And it's not really that big of a deal. The
theory behind it, in case you're curious, is that the Department of Justice has to remain
apolitical. Even the perception of partisan activity would undermine it. So they have
to avoid that. So not everybody can just open up an investigation into a politically sensitive
person. They need approval from higher up. That's the idea behind it. Whether or not
you agree with that, that's a debate that you can definitely have. But that's why they
exist. After the memo surfaces, Garland is asked some pretty specific questions because
it was presented in the media very much as, well, Trump can't be investigated. So he's
asked point blank, you know, who can be investigated? And he says, nobody or no one in this country
is above the law. Somebody shouts back. Even a former president and Garland is just kind
of like, how many times you gonna make me say this? No one is above the law. Right?
This is the soundbite. This is what's being shown on the media. Because it's the clearest
representation of the entire discussion. Sure. However, it's not the most important piece
of that discussion. Garland is asked in that same discussion why he decided to go with
the little fish to big fish method of investigation rather than using something called hub and
spoke, which is how you kind of get to a central figure in a conspiracy. And Garland, he's
like, yeah, I can see this is a very leading question. And he goes on to say, you know,
the Department of Justice conducts its investigations in private. We know people are speculating.
They can continue to speculate because we've talked about it on the channel before. These
pieces of the Department of Justice's identity, Garland really believes in them. No leaks.
This is who he is. Investigations are done in private, out of sight. When there's news,
it's big news. And it's how he's conducted this investigation so far. And he makes a
point of saying all of this. But he also says this and this conversation, to be clear, is
specific to Trump. There's no ambiguity here. He says this is the most wide ranging and
most important investigation in DOJ's history. Wide ranging is really important there because
we know what the most wide ranging investigation in DOJ's history is. It's the events on the
ground at the Capitol on the 6th. In Garland's mind, an investigation into Trump is part
of what happened at the Capitol that day. Meaning an investigation into Trump isn't
limited to the electors thing. It's not limited to a phone call to Georgia. It's not limited
to a pressure campaign on Pence. In Garland's mind, it ties him directly to the events on
the ground there at the Capitol. That is honestly the most hopeful thing I have ever heard come
out of his mouth. He's tight lipped. He doesn't say a lot. I don't think that he misspoke.
That's how he views the investigation. Which means that the entire thing, everything, is
being treated as one conspiracy. Which means if...it means that there are people who are,
I don't know, perhaps in Congress, maybe used to be in government, that should be pretty
concerned. Now, so there's some hopeful stuff right there. We now know how Garland views
it. At the same time, it's a mixed bag. Because Garland does really believe in this apolitical
orientation of the Department of Justice. And sure, I mean, that's how it's supposed
to work. However, he has to understand that even though an investigation or an organization
might be apolitical, that doesn't mean that politics can't influence it. The reality is
he has less than half a year to wrap this up and do whatever it is he's going to do.
Because if it goes beyond that, it becomes partisan. Whether or not he wants it to. He's
running against a clock. Now, do we know what he wants to do? No. And we probably won't
know until he decides. Until he decides it's going to be public. That statement though
leads me to believe that he is, that the Department of Justice is pursuing it as a criminal matter
against Trump. And that that investigation leads to the events of the 6th on the ground
that day. Because if it had been broke off, it wouldn't be the most wide-ranging investigation.
It wouldn't be part of that. Now, that may be how Garland views it. We don't know. At
the same time, there are a whole lot of people who want to protect the institution of the
presidency. Who believe in that identity the same way Garland believes in making sure DOJ
is apolitical. And there are people who would go through a lot to limit the embarrassment
to the Oval Office as an institution. Now, that I definitely think is faulty. I think
that is a faulty thought process. So, at the end of this, the memo doesn't really mean
much. Not really. Garland has made it clear that in this investigation they're going to
go wherever the facts lead them. Now, you could say that that's just political posturing.
Maybe it is. No one's above the law. That sounds really good. But then, there's that
bit about the widest-ranging investigation. I take that as pretty hopeful. But, like a
whole lot of this, we just have to wait and see. Anyway, it's just a thought. Y'all have
a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}