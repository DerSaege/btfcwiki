---
title: Let's talk about accelerating and a better use of your time....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3Qg-JA5TZsA) |
| Published | 2022/07/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the idea of accelerating change by burning down the system during election periods.
- Questions the impact of insurgency on civilians, especially the most vulnerable.
- Describes the ripple effects of disrupting infrastructure, like stopping truck deliveries.
- Details the consequences of a breakdown in systems, such as running out of food, fuel, electricity, and clean water.
- Challenges accelerationists to provide solutions for critical needs like medical infrastructure, clean water, and food.
- Emphasizes the importance of building local networks and power structures to help communities in crisis.

### Quotes

- "Your anger is a gift. Use it to help rather than hurt."
- "Start building power at the local level."
- "You can help people now. You don't have to wait."
- "You need a bunch of 55-gallon drums, fine sand, coarse sand, and gravel."
- "If you really care about those people who are being hurt by the system that exists today, you don't want to make it worse for them."

### Oneliner

Beau questions the consequences of burning down systems, urging for local empowerment to help communities in crisis.

### Audience

Community organizers

### On-the-ground actions from transcript

- Build local networks to support communities (suggested)
- Prepare emergency supplies like drums, sand, and gravel (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the potential consequences of advocating for radical change without considering the impact on vulnerable communities.

### Tags

#CommunityEmpowerment #LocalNetworks #CrisisResponse #CivilianImpact #SystemChange


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about things accelerating
because it's that time again.
We are moving into an election,
and this is the period in which certain people show up
and decide to make their views known
and suggest that nothing's going to work,
that the real answer is to just start all over.
It's time, right?
Burn it down, the whole system, start over.
And they say that they want to do this
because the system is unjust, okay?
And those people on the bottom,
well, those are the people they want to do it for.
All right.
I have questions.
I have questions.
Three main ones that I'll ask in a minute,
but first I want to start with one
just to set the scene a little bit.
How long does the average insurgency last?
According to Rand Corporation, it's about 10 years.
It's about 10 years.
I have some qualms with the way they did that one.
I think it's actually a little bit less.
But you know what?
We're not even going to deal with that
because this is the United States, right?
And if there's one thing we're really good at,
it's death, destruction, and hurting people.
So we're just going to say three years.
We're going to cut it down.
Start to finish, three years.
Easy as pie, right?
Now, during stuff like this, who pays the heaviest price?
Civilians.
Don't believe me?
Pick any modern engagement like this and look.
Combatants lost, civilians lost.
See which number is higher.
And then keep in mind that normally in those numbers,
that's just direct impact.
That doesn't include things like famine,
lack of access to medical care, disease, stuff like that.
Look at the numbers.
That's to help those on the bottom, by the way.
So what happens during something like this?
Infrastructure gets disrupted, right?
Because you're burning down the whole system.
Infrastructure gets disrupted.
And at some point, it really gets disrupted.
And those trucks stop.
Those semi trucks, those 18-wheelers
you see out on the road, those stop.
They stop going into an area because it's not safe.
They're worried about security.
They're worried about getting stolen, whatever.
Those trucks stop.
At that point, in 24 hours, hospitals
are out of basic supplies.
Most gas stations are empty.
No fuel.
There is going to be a little bit in some places.
But for the most part, they're already empty after 24 hours.
Manufacturing, because of just-in-time inventory
practices, yeah, manufacturing grinds to a halt.
We have been dealing with this in a limited fashion
because of the supply chain issues caused by the pandemic.
It's worth noting this isn't something
we're factoring into this discussion
because I'm trying to keep this super simple.
But in situations where conflict arises,
generally speaking, companies from overseas,
they don't like to export to the country with a conflict
because their stuff could get destroyed or stolen
and they wouldn't get paid for it.
But we're not even going to worry about that, really.
So that's after 24 hours.
Skip ahead 72 hours.
There's no food in the grocery stores.
Again, just-in-time inventory.
Our modern society is very fragile.
So 72 hours after those trucks stop, the food you have,
that's what you have.
What would it be?
Right now, go look in your pantry.
How long do you have before you start
having to make some very difficult decisions?
72 hours, there's definitely no fuel at the gas stations,
which means there's no repairs.
So tank, MRAP, whatever, it cuts a corner a little close,
sets a pole, town goes dark.
Well, town's just dark.
No electricity because there's no fuel for that bucket truck.
I hope it's not winter or summer.
That could have some pretty negative outcomes.
Garbage pickup stops.
No fuel, remember?
No way to get it out of there.
So yeah, that's not great for disease.
Two weeks, your clean water starts to fail.
They don't have the supplies to keep it going
because there's no trucks bringing it in.
Hospitals, they're out of pretty much everything,
to include stuff like oxygen. One month,
there is no clean water.
This is a very abbreviated list of things
that cause ripple effects.
Very abbreviated to keep these questions very
simple and very short.
But that's just a month.
That is just a month without those trucks.
And understand, it doesn't have to be a total shutdown.
Even if it's just severely limited,
it's causing huge problems and huge amounts of loss.
So here are the questions.
You can ask your friendly neighborhood accelerationist
this.
How do you plan on keeping refrigerated medical
infrastructure up and running?
And if they don't have an answer and they don't,
that is them saying that their rhetoric, what
they are advocating, what they're pushing out there.
Well, 100,000 insulin-dependent diabetics, children only.
Well, that's just the cost of doing business.
That's how they're going to help you.
That's how they're going to make it better for those people
on the bottom.
Right?
Got to make an omelet, right?
Ask them how they plan on providing clean water at scale.
That LifeStraw, it's about 10,000 gallons.
Sawyer, 55,000.
So let's assume a family of four cuts their typical water usage
in half because it's a bad situation.
That filter gives a family of four 10 months, more or less.
So yeah, that's 26 months short on our abbreviated timeline.
And that's just a family of four.
You are now on the Oregon Trail.
What about food?
I'm not going to have an answer for that either.
That's how they're going to help.
And when they talk about this, they always
talk about those people on the bottom
because we are in a system that is unjust
and there's a lot of inequality.
Who does this hurt the most?
What if this 30-day period, what if it starts on like the 25th
before those people on assistance get it?
They don't have anything in their pantry.
What about the people that it's supposed to hurt?
The people at the top of this system?
Well, they're at their compound with their generator
and their reverse osmosis filter.
Doesn't hurt them.
And I know people are saying, people who perhaps
believe in this theory, they say,
well, this doesn't happen in other countries
when something like this occurs.
Yeah, it doesn't because the United States military
has a huge, huge logistical network
to stop it from happening.
It's one of the main goals in a situation like this.
So if it was up and running, if the US military did
engage in this, who would have the loyalty of the people?
The empty rhetoric saying, we're helping you
by putting you in a situation where
you have to make the decision to say, no,
forget about that other kid.
That insulin is mine.
I need that for my kid.
Or the people providing the insulin,
the people distributing MREs, the people purifying water,
fixing the electricity.
Not just does it make it worse, it doesn't work.
This isn't the way to go about anything.
All you do is hurt people.
I personally started looking left
because I got tired of watching people suffer.
I get it.
I really do.
You're angry.
The system is messed up.
It's broke.
It's also not sustainable.
All of this stuff, eventually it's
going to happen with or without your help.
Climate, there's going to be a whole bunch of things that
can cause this situation to happen.
Your anger is a gift.
Use it to help rather than hurt.
Start building the capacity to answer these questions.
Start building power at the local level.
Start building that network that can help people rather than
put them in an even worse situation.
That is probably a much better use of your time
because you can help people now.
You can start with that now.
You don't have to wait.
And then as that network grows, as that local power structure
grows, when things start deteriorating
because of the climate or because of whatever,
your community knows that you cared about them,
that you put forth the effort, that you know why.
You need a bunch of 55-gallon drums, fine sand, coarse sand,
and gravel.
That is probably a much better use of your time
and much more in line with your stated beliefs.
If you really care about those people who
are being hurt by the system that exists today,
you don't want to make it worse for them.
And that's what will happen.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}