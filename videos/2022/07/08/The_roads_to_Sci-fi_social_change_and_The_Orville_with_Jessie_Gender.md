---
title: The roads to Sci-fi, social change, and The Orville with Jessie Gender....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WUU9AGzu3b0) |
| Published | 2022/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the topic of science fiction and societal change, focusing on the show "The Orville."
- Jessie Gender is introduced as a guest, known for discussing social and political issues in nerdy communities.
- The episode was prompted by a flood of messages about "The Orville" and a particular episode.
- The episode being discussed features a character, Topa, who is intersex in an alien society called the Moklins.
- Topa is forced to undergo surgery as a child to conform to societal norms of gender.
- The episode "A Tale of Two Topas" focuses on Topa's journey to realize their true gender identity and desire to reverse the previous surgery.
- The distinction between being intersex and transgender is discussed, focusing on bodily autonomy and self-perception.
- Misinterpretations of the storyline within the show led to some viewers perceiving it as anti-trans.
- The importance of representation in sci-fi shows like "The Orville" and "Star Trek" is discussed in influencing societal change.
- Positive and negative aspects of using allegory and metaphor in sci-fi for social commentary are explored.

### Quotes
- "Transness is more about getting to dictate how you see yourself and other people getting to see you."
- "It allows them to sort of not see that adversary relationship with the discussion, but just a discussion about it."
- "It's supremely helpful. And even when we get to something like the Orville and older stuff in general."

### Oneliner
Beau and Jessie Gender dissect the nuanced portrayal of gender identity and societal norms in "The Orville," discussing the impact of representation in sci-fi on social change.

### Audience
Sci-fi fans, LGBTQ+ community

### On-the-ground actions from transcript
- Watch and support sci-fi shows that provide representation and meaningful social commentary (exemplified).
- Engage in critical analysis of media portrayals of gender identity and societal norms (implied).

### Whats missing in summary
Analysis of the impact of media representation on shaping societal perceptions and attitudes towards gender identity.

### Tags
#ScienceFiction #GenderIdentity #Representation #SocialChange #SocietalNorms #Transgender #Intersex #MediaRepresentation


## Transcript
So it looks like we are rolling. Well, howdy there internet people, it's Bo again. So today we're
going to talk a little bit about science fiction. We are going to talk about how it influences
societal change. And we're going to talk about the Orville. And I have a guest with me who is
definitely the person to ask when it comes to all of this. So I'm gonna allow
Jessie here to introduce herself. Hi, hello my name is Jessie Earl. A lot of
people know me as Jessie Gender on YouTube. I am a huge gigantic dork is
what I like to choose myself as. But yeah, I do YouTube stuff just like you
And I talk about mainly social and political issues through nerddoms and geekdoms most notably about
LGBTQ issues and trans issues because I am
Trans and also bisexual so I like to focus on those issues
But just try to talk about kind of the same things that you talk about but just they're a nerdy lens because that's who
I am  And it and it shows so this whole episode was prompted because I
I got just a flood of messages asking about the Orwell or Orville and
Particularly one episode in particular, so I hadn't seen any of the series
So I went back and watched the entire series in like a week
That's impressive. Yeah, and
then I recorded my
Interpretation of what I saw and I thought I had a pretty good handle on it
And then I did this horrible thing and I watched your video and I realized that you might be a little bit
more nuanced with the nerdy, so
Rather than reinvent the wheel we could just bring you on
Well, I am honored because I'm always a big fan of your work and isn't that always also like the youtuber problem
You're like I'm gonna make this video. It's gonna be great. And then you watch someone else do a much better job
I was like, Well, I don't know why I'm here. Right. I mean, you know, in when I watched it, I saw, you know, from that
very first storyline, and I think it maybe it was because I knew how the storyline progressed. But when it first
started, I looked at it as here's this person who early on they identified as being outside of the societal rules when
it comes to gender. So that was just how I took it, was that they forced the child to
stay within those rules. Watching yours, you brought up this whole other topic. So why
don't you tell us a little bit about it?
Yeah. So it's a really interesting discussion when it comes to Orville, because it kind
of hits on a confluence of a bunch of different things in terms of like talking about how
we represent things through science fiction allegory
and things like that.
And the most notable one to me,
cause I started watching the Orville when it first came out,
was actually less talking about like transgender issues
and even LGBTQ issues in general
when we talk about sexuality, but intersex issues.
Which if you don't know what intersex is,
essentially it is, in our society today,
we are in human species
cause we're talking about aliens here.
We're born male and female mostly,
with people with XX chromosomes, XY chromosomes,
and other things too that can influence
like how we're perceived, like hormonal levels,
whether it makes you taller, skinnier,
whether you have big breasts, no breasts,
all of those things are influenced
by a bunch of different things,
but we tend to equate it down to hormones,
but there are, sorry, chromosomes.
But then intersex people are people that are born
sort of outside those sort of two poles.
So you'll get people who are born
with XXY chromosomes, for example.
You'll also get people who are born with hormonal imbalances.
Maybe they'll have XXY chromosomes,
but their hormones would be different.
So they'll develop different genitals
or they'll develop different body features,
things like that.
And for the most part, people that are born that way
can live full, happy, productive lives.
Some may have to have medical issues
that are dealt with when they're born,
but they'll be medically fine for the most part.
However, what the Orville touched upon
with Gerica Topa in the early episode
was the fact that a lot of intersex people,
because we sort of have this society
where we sort of say, oh, everyone needs to be male or female
in line with these sort of norms that we have.
A lot of intersex kids will sometimes have surgeries
done to them when they're young,
either by their doctor or by the parents
or typically chosen by both
to sort of make them align with one or the other.
And that's usually because the baby's obviously done
without their consent or knowledge.
And oftentimes many intersex people when they grow up
don't even know that it happened.
And that's something that was reflected
with the character of Topa,
that this was a character who within the society
of the Moklins, which is the alien species in the show,
all those alien species are born male.
And I think, I don't think there's a specific number given,
but like generally, at least perceived by the culture,
most Moklins are born as guys.
And so women are considered to be intersex
in their society.
By human standards, not the same way,
but within their society,
females would be considered intersex.
And so they had the whole storyline
where they sort of forced Topa to undergo a surgery
as a kid without her consent,
that forces her into being and presenting as a male.
They don't say specifically, but presumably
they sort of shape her body to have particular genitals.
I know it's a long-winded explanation.
No, no, no, especially for people
who haven't seen the show, that's probably very helpful.
And then from there, it brings us to the episode
that everybody is talking about, A Tale of Two Topas.
And so tell us what happens in that.
Yeah, so at this point,
the character had only been intersex
and we had seen the character Topa come up a few times
because this is an alien show,
even though it's three seasons,
they've aged to like 14 years old in three years,
because welcome to science fiction.
um but they they they've had some issues with like dealing with masculinity and and having violent
outbursts and things like that when they were younger uh and through a couple episodes but when
we hit a tale of two topaz the storyline suddenly becomes about topa now feeling that they are she
doesn't really fit into how she's been told to be at this point she doesn't know that she's she's
intersex that she had the surgery done to her and but she's feeling something's off and i'm saying
she now, but at the time, she thinks she's a guy, uses he, him pronouns. And through a sort of
series of events of the episode, she comes to realize, oh, I had this surgery done to me.
I don't feel like I'm a boy, and I wish to reverse the procedure that was done to me to be seen as
not only just be seen as a woman, but also have my body aligned with being a woman and getting to
live that life and identify in that gender. And so this is actually where we can get into the nitty
gritty of all of this. Up until this point, she was not transgender, even though when she was a
kid, she had her body changed against her will. That's not being trans. Trans is when you have
been told to be something by society. In this case, for Tokva, she was told that she was a guy,
and they had a surgery to make her do that. But then you sort of realize that doesn't fit you,
And you sort of choose to transition in whatever way should he perform that is whether it be through surgeries or it be
through
Just asking for things like pronouns or dressing differently can be a myriad of these things mix and match
for her it was a surgery and
That that led her to be be perceived as woman
So now she is a trans character on top of being an intersex character
So is there
You know one of the things that has made this episode
episode. So talk about is the various takes that different groups have had. Do you see
anything at all in this episode or in this storyline that could be taken as being anti
trans?
Yeah, because I was kind of hinting at it there. And this is sort of what gets confusing
when we talk about alien metaphor, right? Um, is that a lot of people equate the act
of just having surgery or like something being done to your body when it comes to sex or
gender to instantly being trans. And so because she had this surgery done to her as a kid
against her will, people equated that with being seen as that's being trans, right? Like,
oh, you're trans because you had this surgery done to you. And now that she's older and
she's getting it reversed, it's saying like, oh, they this society trans, the children
trans her as a child, and now she's reversing it. And so many people took that as being
oh this is not this is the show saying you shouldn't force gender upon this kid that you shouldn't like
force gender ideology upon this child when that's not really I it's not really what being trans is
because and again it gets confusing when we talk about this within the world of the Orville because
there's sort of this added layer of uh alienness to it but when we talk about intersex when we
When we talk about transness, the whole point of being trans is you're told to exist one
way.
Oh, did I just get dropped?
Oh, there we go.
No, you're good.
I don't know what is going on there.
I'll wait until are we, should I just keep going or?
Yeah, just keep going.
It'll bounce back.
I'm sure.
I hope.
Yeah.
I know where it is.
The whole point of it is like when we, when we talk about, I'm sorry, I lost my train
I thought so I'll just try to collect it real quick when we talk about transness
it's not getting surgeries we usually just equate transness with like just
having a surgery a lot there's a lot of misinformation about that like being
trans requires you to have some sort of medical intervention to get it done and
so when she had the surgery as a kid to change her sex against her will
Still, many people said like, oh, that's being trans.
And so that was sort of seen in the negative context, both within the show and by people
outside of it.
And because people misinterpreted that as being transgender, that's where the anti-trans
narratives come from, or many people receiving the show to be anti-transgender.
But when we talk about transness and people being trans, the actual biggest thing that
we need to talk about is actually bodily autonomy, which I know is like the buzzword and a lot
things we're talking about when we talks about Roe v Wade and women and other people who are able
to give birth and being able to have access to abortions and things like that. But it also comes
in with being trans because when we talk about people wanting to transition to asking for
different pronouns or asking for people to perceive them a certain way or be able to wear certain
clothes that they want to wear, all that comes down to just being able to control and dictate
how our body is seen and how we wish to be perceived in it. And part of that can be surgeries,
like for me, I'm actually, you can see my face is actually a little bit puffy and swollen. I was,
I actually just had a surgery on my face that was related to transgender stuff just recently
for me. But that was something that I entered into that I had full awareness of, was like told
by a doctor, like we had years of going to the therapist and talking to people about it.
it, it was a choice that I made because it was to make me happier.
And so transness is more about getting to dictate how you see yourself and other people
getting to see you, of which surgeries may be part of, but it's not just surgeries.
And so that's sort of the distinction between intersex issues and trans issues.
But because the show places it into alien metaphor, and you sort of have this sort of
added layer of, like, oh, we're talking about men and women. And in a macular society, there, women are intersex, but in
human society, they aren't, it sort of adds that layer of confusion on top of all of it.
Yeah, and there's, you know, when you're talking about metaphors, and you're talking about stuff like this, most
times, the intent of the people making them really doesn't matter, it boils down to how it's received. But one
thing that I do want to point out was that if you were to reach out to the show and try to find out what they were
trying to say, you would go to their social media, where you would find them retweeting stuff that is very
supportive, the interpretations that are very supportive of the trans community, not the other take. And I think
that's a pretty clear sign. So when you are talking about the
Orville, right? It's not Star Trek, but it is, right? I mean,
it is, it's actually written by the same people who wrote older,
like Brandon Braga, who wrote like Star Trek Voyager and
Next Generation is a writer on this show. So it's a little bit
the same people of older Star Trek, not current modern
Star Trek.
Right. I mean, it's, it's very much, I mean, it really
does seem like the same universe. And that crosses into, you know, Star Trek, this isn't the first time in this
particular
genre, that you have people seeing a metaphor for trans people. Like with the DS9, right? Is that? So when you're
You're talking about affecting social change.
How important do you see shows like this when that metaphor comes out, even though, as we
just saw, it's not always received the way you would hope it would be?
Do you think that it's helpful?
I think it's extremely helpful for a myriad of different reasons, but there are positives
and negatives.
But the pauses, I think, are incredible, especially at a time where, like, older Star
Trek's been produced, where a lot of these issues couldn't get on screen.
And so, I mean, even on older Star Trek, you had producers of those shows when writers
were like, hey, can we have actual, like, human LGBTQ characters?
They were told no.
But then they could have episodes where they were like, let's talk, let's have Jetsy Adax,
who is one of my personal favorite characters, talk about being a woman who used to, through
alien weird stuff going on, used to be a guy.
the characters even calling her old man
as a sort of term of endearment
and just sort of like a friendship bond between them.
But she would bring that up all the time.
And that allowed a lot of people to see
like transgender issues being discussed through that.
And then also for me, you know,
I was a little kid growing up loving Star Trek
and not getting to see trans stuff.
I loved Dax because I got to see myself in her
and that just like meant so much to me.
And it gave me ability to like talk
and think about what I was feeling
in a way that I didn't really,
couldn't really articulate before.
So I think for those reasons,
it's supremely helpful.
And even when we get to something like the Orville
and older stuff in general,
I think what is so great about using allegory
and metaphor like this is it allows people
to sort of take their biases
out of our current politics today
and put them in a context that's completely separate
from what we're dealing with
and allow people to just analyze from the outside,
like, oh, this is what is going on here.
These are the feelings going on here.
These are the feelings that these people are expressing.
And this is how maybe these bigotries
and prejudices may be enacted and get to like analyze them
and be like, oh, I see how that fits where I'm at.
But it allows the guard to go down
because they're not seeing in like terms of like
Democrat versus Republican or the left versus the right.
They don't have those terms that they have to put it into.
And so it allows them to sort of not see
that adversary relationship with the discussion,
but just a discussion about it.
I think that that's really powerful.
However, the negative does come in to the fact,
I think in twofold, is one,
it does tend to be up to the biases
of the people writing the show.
I mean, older Star Trek, for example,
was written mostly by a lot of non-transgender people.
And so a lot of the metaphors that they use
to talk about LGBTQ stuff and trans stuff
was often filtered through those lenses.
And there were biases that were present.
Like, for example, there was an LGBTQ episode
of next generation that was like even talking about pronouns
and things way back in the early nineties,
which was amazing, but it's still ended on a story
that like had the character get her mind erased at the end
that in a way almost confirms like conversion therapy
which we know is like a harmful thing
that LGBT people can be forced into kind of almost says
that that works, which it doesn't.
It just sort of pushes people
into feeling more suicidal and depressed.
So there are those biases that are present.
And when we come to the oracle, we see that here
because you can sort of see like,
there's misinterpretations, it's not exactly clear.
I think the current episode of Tale of Tutopas
does a great job at expressing these issues,
but because the earlier episodes also intermixed it
with gender issues, intersex issues,
when we get to talking about trans stuff,
it gets a bit confusing because it isn't so direct.
And then the other problem with metaphor sometimes
is that it's otherizing.
As much as I love Dax, I adore her with all my heart,
I only got to see myself as an alien.
I never got to see myself like being a full person.
And so while that's great to teach other people
about what trans people are
and even teach trans people about ourselves,
it doesn't let us get to be seen as like,
oh, we can actually exist as human beings
and not just aliens.
So those are some of the positives and negatives of allegory.
Right.
I would imagine that,
I mean, you don't get the representation that you,
yeah, I mean, I can definitely see that.
So what about Garrick?
Garrick, oh my God.
Garrick is, Garrick is fascinating.
So Garrick, I could go down a whole rabbit hole.
I mean, if you watch Garrick,
I love how you just say that
and I know what you're talking about.
Garrick was a character who I love way back in Deep Space Nine
and he was actually performed
in the initial episodes of the show as a gay man.
Like he literally goes up to Bashir
and like propositions him for lunch.
And it's very clear if you watch that early scene
that he's like, I find this dude hot
and I kind of want to ask him out.
And that is pretty explicit.
However, the show never actually stated it.
And actually there was an interview done
with the writer of the show, Ira Steven there.
And he actually made a point that like,
they never asked if they can make Garrick explicitly gay.
Like they never even asked if they can make him that.
And so a lot of people, because it was never stated
and it was just sort of like these undertones
that many people miss that fact
that's how he was played by the actor. And then as the show went along, because it was never
stated or made explicit, they actually, you saw a lot of storylines that started to be shifted,
but they started putting Garrick in relationships with women. And they could kind of now change it
so to be like, make it the show more heterosexual, if you want to put it that way, because it was
never actually stated. And I, I'm a huge nerd, so I know a lot about behind the scenes. There's a
a whole bunch of stories about maybe some producers on the show that were possibly homophobic,
that were pushing them to not go that route. And so that's when we talk about like explicit
representation versus like coding of characters, where you have characters that are coded one
way. And that's great. And you can find maybe representation in that, but it allows people
to miss it who don't necessarily aren't aware of that, that aren't looking for that, who
aren't aware of lgbtq issues but also it also allows people to sort of change it if they wish
to if it's not exactly stated right right um that's uh it's actually one of my favorite characters
like in in all of sci-fi like right every scene he's in i just like oh yeah you know this is
going to be a good episode. So the Orville as a whole, what do you think of it? Is it worth
watching? I think so. To be honest with you, I'm deeply impressed with this season. I was up and
down on it in previous seasons. I liked elements of it, but I did feel a little bit like kind of
the Seth and the Farland family guy, like here's jokes every two seconds and half of them are okay,
none of them are super great. And I also found some of the episodes to be a little bit redundant
of Next Generation, because the show is clearly in that style, so I would watch and be like this is
the same episode as that one, like I saw Data doing the same thing. But these past few episodes
of this season three, I've been actually, for the most part, been like, wow, they're tackling some
really good issues. Like, I think they talked about how harassment leads to suicide, suicidal
thoughts in the first episode. They talked about abortion issues in recent episodes, talked about
Religious orthodoxy and how that can sort of influence politics as well
You're an election stuff which from like 2016 election issues. They discussed I was like, wow
They're actually like they're really pushing it forward. So I recommend it. I think it's imperfect, but no show is so
Yeah, I mean when I have to be honest like when I first was like, okay
Well, I'll sit down and watch all of these that first episode
I was just like yeah
This is I really wish I hadn't already committed myself in my mind to doing this
But it did it got it got a lot better from where I'm sitting
So as far as sci-fi in general
When you're talking about things that move society forward to help build that better world what
Episodes or movies or books. Do you think provide the
the better allegories, the better metaphors,
the thing is that you could subtly
introduce to people and hope that they get the message.
I would actually give you two things.
Because one, I think it'll be an interesting contrast.
I personally, actually I'll give you three real quick.
Personally, I like modern,
I know it's been kind of a hit or miss for a lot of people.
Modern Star Trek, I think does
really good job at, kind of in the opposite way to the Orville, having more literal representation.
I do think Modern Star Trek sometimes fails when they talk about like delving into like
the metaphor and like getting into the hard-hitting issues and they sometimes become more action
and adventure-y. But what I do like about Modern Star Trek is they do have a lot of
actual transgender LGBTQ people of color on people, like characters, human characters
on the show. And it's just like letting people out to exist authentically. And you can tell
the show has gone out of its way to not just use people as like a tool for learning but just letting
people exist as themselves and I think that that's absolutely wonderful. I think that getting to see
like trans people be a queer camp over the top villain in a recent episode of Strange New World
I think is fun. So there's that. I think the best science fiction show though that I have seen of
the past few years by far is The Expanse. If you have not watched The Expanse, it is the best way
I could pitch it is it's like Game of Thrones before Game of Thrones got bad, but in space.
And they do, it does such a great job of talking about kind of that same thing where we're talking
about politics today, but placed into a really complex web of politics and
school action, things like that. So the basic plot of that show, just a quick pitch, is you have Earth
Earth and Mars, which are kind of at war with each other, like a kind of a cold war. Mars
has become more of a militaristic organization, very nationalistic. They're trying to tear
apart Mars. And so they're always like, yeah, we're the hard grip, like bite our knees down
and we'll get this done sort of people. And then Earth's become more of this society where
like they're have plenty of abundance, they have a ton of resources, they can do whatever.
And then in between, you have these people called the Belters who live in space and have
lived in space for generations now, who have been, don't have a centralized
government, kind of feel displaced and dispossessed and have started forming
different kind of terrorist groups, kind of like the IRA in Ireland, to sort
of fight back and push back.
And the show just talks about the complexity of all of that.
There's no villains or heroes.
It's just sort of like, they're all sort of dealing with the complex
ramifications of this.
I think it's just, it really smartly talks about bigotries, harassment,
how politics plays into all of that,
how people manipulate it.
It's very good.
And then my final one that I'll give you,
if you're looking for just to show that it feels positive
and just takes delight in the future and diversity
of humanity, well, it's not necessarily a future set.
So it is a science fiction show set today,
and that's called Sense8.
And it's made by the same people who made The Matrix.
And it is just a show that is full of joy
and happiness, and it's got a fun like action adventure plot.
They're trying to take down this evil corporation thing.
But the show is more just about these people
being diverse people, getting to interact
and have fun with each other and taking a joy
and just getting to know and be with each other.
And it centers around eight people
who are able to jump into their own,
into their bodies and thoughts
and share skills with each other.
And they jump around the world
and helping each other deal with their problems.
So you'll have like a cop in Chicago
able to help out in a business woman in Japan dealing with things there and sort of her kung fu
skills then help with someone else in Nigeria. And it's just a really cool sort of sci-fi story
about like how we all relate to each other even despite our vast cultural and political differences.
Cool, cool. Okay. So is there anything that you want to touch on that you don't think we did?
That was pretty much it. Thank you for dealing with my long-winded rants. I tend to be a very
long-winded person so no no it's it's good no that that's really kind of what I was hoping for
because you know again you know it's it's that thing that happens every once in a while you make
that video you look at somebody else's and you're like man mine was horrible so this is exactly what
I was hoping for um plug your stuff tell everybody who you are again where they can find you all that
stuff well first off thank you for having me it was it's an honor honestly to talk to you I've
And you say, like, oh, my video wasn't great.
Your videos are wonderful, and I adore them.
So thank you for that.
As for me, you can find me at JSCGender on YouTube.
That's where I do sort of my main stuff.
And like I said, if you liked me talking about these issues,
that's a lot of what I do on the channel.
I think probably my two videos most recently that
would be akin to this discussion would be I
did a video on Sex and Star Trek, The Next Narration, which
is a very long video, but it's meant to be watched in chunks,
where I sort of talk about the history of Star Trek,
the next generation go into the nitty gritty
of the behind the scenes and talk about both the issues
and problems, but also what they got right
when it came to talking about LGBTQ issues,
talking about gender, talking about sex,
talking about men versus women in the treatment
and men versus women on screen and behind the scenes.
And then I also have a video recently talking
about director James Gunn, who, if you know him,
he directed like Guardians of the Galaxy
and the recent Suicide Squad movie.
And it was a video about how throughout his work,
James Gunn has tried to do radicalize
his own sort of sense of masculinity
in the sense that he sort of saw himself
like having to need to be tough and strong
and that sort of hurting him
and leading him to go and to be abusive
and feeling depressed and drunk
and overusing drugs a lot of the time.
But then how he used his films
to express a lot of those feelings and break them down
and try to form a new version of manhood
masculinity through that and I think it was a very uh it's a wonderful look I think at at just how
he's done that um so those are two videos if you're like nerdy stuff that's what I that's sort of
what I do um and yeah that's that's pretty much me all right did you wait did you say the name
of your channel yes just you gender just type in just your gender you'll find it you'll see my
my face on there so okay um all right well I guess that's it I hope that that answered everybody's
questions. It has ever, we kind of hit on everything that was one of the questions I've written down here. So I hope
that covered what people wanted to know from somebody who definitely has a better take. And I guess that's it. Thank
Thank you so much for joining us and it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}