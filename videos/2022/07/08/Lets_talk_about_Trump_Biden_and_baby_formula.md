---
title: Let's talk about Trump, Biden, and baby formula....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-qP1dn1uBik) |
| Published | 2022/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a friend's struggle in a rural area to find baby formula for his infant son.
- Beau helps his friend by locating and delivering baby formula from stores in his area.
- Laughs at the decentralized way different areas came together to meet the need.
- Mentions disruptions and how the idea of making everything at home doesn't always work out.
- The Trump-era policy of tariffs on baby formula from Canada contributed to the shortage.
- Eric Miller explains how the tariffs have affected supply and prices in the US.
- The shortage prompted the Bynum administration to seek solutions by allowing imports from safe countries.
- Points out that the tariffs were more about maintaining profit margins for certain companies rather than punishing other countries.
- Emphasizes the impact of tariffs on keeping prices high for consumers in the US.
- Wraps up by sharing these insights and wishing everyone a good day.

### Quotes

- "Different areas coming together to supply the needs."
- "That nonsense about make everything at home, blah blah blah blah it never works out."
- "The US has basically said our priority is keeping subsidized imports and products made with subsidized inputs out of the US market."
- "Keeping the price high for you."
- "It was never about punishing these other countries."

### Oneliner

Beau shares a rural area struggle to find baby formula, revealing insights on tariffs, supply disruptions, and profit margins.

### Audience

Community members, policymakers

### On-the-ground actions from transcript

- Help distribute baby formula in areas facing shortages (exemplified)
- Advocate for policies that prioritize affordable prices for consumers (exemplified)

### Whats missing in summary

The full transcript provides a detailed look at how policy decisions impact everyday people's access to essentials like baby formula.


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
baby formula. We're going to talk about baby formula and how that is being addressed and
how it paralleled something that happened to me. You know my kids are old enough that
they don't need formula anymore, but a friend of mine, his son now has an infant and the
area where they live, well there wasn't any baby formula. Now understand rural area. You
know in a city if you go to two shops and they're out of what you need, you know there's
forty more shops to go to. If you live in a rural area and you go to the two shops and
they don't have what you need, you have a forty mile drive ahead of you. So he called
me and I looked at the stores in my area and sure enough there were some there. So I picked
it up and I drove it over to him and you know it kind of made me laugh because decentralization,
right? Different areas coming together to supply the needs. So what happens? It's pretty
much always what's best. That nonsense about make everything at home, blah blah blah blah
it never works out and this was just a micro version of that. There's always disruptions.
So that happened, I don't know, three four days ago. The Bynum administration has instructed
the FDA to set up a method so foreign producers of baby formula from safe countries set up
a method for them to import permanently to the United States to solve the problem. What
was the problem? A shortage in one location, right? Help coming in from outside. Yeah and
it's interesting because one of the reasons the shortage is occurring is a Trump era policy
that puts tariffs on baby formula from Canada. That talking point about bringing all the
production home, it's bad. It's always been bad. This is from Eric Miller who is the president
of an international trade advisory firm. We've reached the point where producers are no longer
able to maintain supply and there aren't other close at hand markets that you can turn to.
The US has basically said our priority is keeping subsidized imports and products made
with subsidized inputs out of the US market. So there's no excess production that's available
and we keep prices high for consumers in the US. That's the actual impact of those tariffs,
keeping the price high for you. It was never about punishing these other countries. It
was about maintaining the profit margins of companies within the United States that were
supportive of the former president. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}