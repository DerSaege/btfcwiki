---
title: Let's talk about DOJ looking at Abbott's Lone Star....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qXwyzAWSLI4) |
| Published | 2022/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Operation Lone Star in Texas is reportedly under investigation by the Department of Justice's Civil Rights Division.
- ProPublica and Texas Tribune collaborated on reporting this investigation.
- The investigation uncovered formal inquiries into Operation Lone Star by the DOJ.
- Texas agencies involved in the operation were unavailable for comment.
- Operation Lone Star, Governor Abbott's border security initiative, involved deploying 10,000 National Guard troops.
- Statistics surrounding the operation have been amended, indicating potential manipulation to exaggerate success.
- National Guard troops are facing poor conditions and are attempting to unionize.
- There is a shortage of resources within the operation.
- The multi-billion dollar initiative is seen by many as a state-funded campaign event.
- The DOJ may be investigating civil rights violations related to the operation.
- Potential violations could be linked to the Civil Rights Act and issues surrounding national origin.
- Confirmation of the investigation's findings beyond leaked emails is expected soon.
- The investigation could impact Governor Abbott's re-election prospects.
- The operation's outcomes may not match the portrayed success.
- The investigation suggests deeper issues within Operation Lone Star.

### Quotes

- "Operation Lone Star in Texas is reportedly under investigation by the Department of Justice's Civil Rights Division."
- "The multi-billion dollar initiative is seen by many as a state-funded campaign event."
- "The DOJ may be investigating civil rights violations related to the operation."
- "National Guard troops are facing poor conditions and are attempting to unionize."
- "Confirmation of the investigation's findings beyond leaked emails is expected soon."

### Oneliner

Operation Lone Star in Texas faces DOJ investigation for potential civil rights violations, raising concerns about manipulated success data and poor conditions for National Guard troops.

### Audience

Texas residents, Civil Rights activists

### On-the-ground actions from transcript

- Contact local Civil Rights organizations to stay updated on the investigation (suggested)
- Support National Guard troops in improving their conditions and unionization efforts (implied)
- Stay informed about Operation Lone Star's developments and implications (suggested)

### Whats missing in summary

Insights on the potential implications of the investigation results and how they might impact future border security initiatives. 

### Tags

#OperationLoneStar #DOJInvestigation #CivilRights #GovernorAbbott #NationalGuard #Texas


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
Operation Lone Star again and how reporting suggests Governor Abbott's operation there
in Texas is under investigation by the Department of Justice, the Civil Rights Division. This
reporting comes from ProPublica and Texas Tribune. I think it was a joint thing between
them. Two outlets that I always like to stop for a moment and plug if you're not following
them, you probably should be. It is old school journalism. Digging through documents and
emails, finding leads that way, then reaching out to get comment from the people involved,
getting their denials, but still having the paperwork in hand to run the story. Good old
fashioned journalism. So what did they uncover? They got an email that says, if you're not
already aware, the Civil Rights Division of the DOJ is investigating Operation Lone Star.
Apparently that was Kalyn Betts, Department of Public Safety Assistant General Counsel
in an email. And there's another one from the Texas Department of Criminal Justice citing
a formal investigation of Operation Lone Star by the DOJ. Now they do what they do and they
reached out to DOJ for confirmation. And DOJ said that they cannot comment on the existence
or lack thereof of any potential investigation or case on any matter not otherwise a part
of the public court record. And I believe the Texas agencies involved were unavailable
for comment. Now Operation Lone Star is that PR stunt. We've covered it on the channel
for months. This is the thing where the governor of Texas is like, well we're going to secure
the border. And they put like 10,000 National Guard troops out there. And since this whole
thing has started, they've had to amend their statistics and their stats, because it certainly
appears that they cooked the books to make it look more successful than it was. The National
Guard troops are in such bad conditions that they're attempting to start a union and there
is a shortage of, well, everything. This multi-billion dollar PR stunt, which is really in many ways
viewed by a lot of people as just a state-funded campaign event, it has not been incredibly
successful and now it looks like there might be cause to believe the Department of Justice
believes civil rights violations are occurring, probably having to do with the Civil Rights
Act and that whole bit about nation of origin would be my guess. So I would expect here
in the coming weeks for there to be confirmation of some kind beyond the emails that are saying
that it's happening that ProPublica was able to get. So this might throw a wrench into
Abbott's re-election hopes. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}