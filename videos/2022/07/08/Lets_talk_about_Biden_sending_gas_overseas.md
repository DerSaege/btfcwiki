---
title: Let's talk about Biden sending gas overseas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mL_XpdM5gWg) |
| Published | 2022/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation regarding gas, gasoline, oil, and the Strategic Reserve.
- Responds to a viewer's concern about Fox News possibly lying about Biden sending gasoline overseas.
- Confirms that the Biden administration did allow 5 million barrels of oil to be sent overseas.
- Clarifies that the oil came from the strategic reserve because US refineries are operating at capacity.
- Addresses the misconception about refineries running at full capacity and explains the reason for the backlog of oil.
- Mentions that sending oil overseas affects the global oil market and can potentially lower oil prices.
- Emphasizes that while the release of oil for export can help lower costs, it is a gradual process.
- Concludes that Fox News may not have lied but rather omitted relevant information in their reporting.

### Quotes

- "The Biden administration approved the release of 1 million barrels of oil per day."
- "So not really lying, just omission of pieces of relevant information."

### Oneliner

Beau clarifies the situation around oil being sent overseas, addressing concerns about Fox News and explaining the impact on global oil prices.

### Audience

Viewers concerned about accurate information on oil and gasoline.

### On-the-ground actions from transcript

- Contact your representatives to advocate for transparent and accurate information on energy policies (suggested).

### Whats missing in summary

The full transcript provides a detailed breakdown of the oil situation, including the impact of sending oil overseas and its effects on global prices.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about gas, gasoline,
and oil, and the Strategic Reserve,
and oil being sent overseas.
We're going to do this because I got a message from somebody
who said that they're a conservative who
watches to see what the other side thinks,
and they're wondering if Fox is lying to them.
Because according to the message, Fox said that Biden sent over 5 million barrels of
gasoline overseas, while prices were really high here.
He found this hard to believe because even though he views Biden as, let's just say
not smart, nobody would do that.
So my guess is that Fox isn't lying.
They are leaving things open to lead people to a false conclusion.
Did the Biden administration allow 5 million barrels of oil, not gas, to be sent overseas?
Yes.
Came out of the strategic reserve.
Why?
Because oil has to be refined.
The refineries in the United States are running at capacity.
OK, I say that.
I know somebody is going to say this.
They are running at effective capacity.
They're running at like 95.4% or 94.5%, something like that.
Theoretically, sure, they could find some way
to squeeze out that extra couple of percent,
but that's just normal.
It's normal to not be able to run at full capacity
all the time.
The Biden administration approved the release of 1 million barrels of oil per day.
So there's a backlog of oil that isn't getting processed.
That was sent overseas because oil is a global market.
If Saudi Arabia sends oil to Germany, it reduces the global oil price for crude oil.
So in theory, it will bring down oil prices, but it happens slowly because when it comes
to oil, the price goes up real fast, but it takes a while for it to go back down.
So they probably didn't lie.
They just equated oil with gas, or they left out the part about the refineries.
So will the release of this oil to export,
will that help lower the cost?
Yes, but it's a slow process.
So not really lying, just omission
of pieces of relevant information.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}