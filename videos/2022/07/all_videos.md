# All videos from July, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-07-31: Let's talk about the PACT Act, Jon Stewart, and details.... (<a href="https://youtube.com/watch?v=Zo0Bb9C0NfI">watch</a> || <a href="/videos/2022/07/31/Lets_talk_about_the_PACT_Act_Jon_Stewart_and_details">transcript &amp; editable summary</a>)

Beau breaks down the critical details of the PACT Act, exposing the stakes for sick veterans and questioning Republican obstructionism with high emotional investment.

</summary>

"Republicans are patting themselves on the back for denying health care to veterans..."
"Every day this drags on over, in best case scenario, an accounting issue."
"It's high stakes. People are sick. They need the treatment."
"The longer the treatment is delayed, the more likely it is they don't have a positive outcome."
"It's time sensitive because people are sick right now."

### AI summary (High error rate! Edit errors on video page)

Explains the PACT Act and Jon Stewart's involvement in advocating for it to provide medical care to veterans exposed to toxic substances while working for the US government.
Details how the bill aims to cover millions of people exposed to substances like Agent Orange and burn pits, with an estimated cost of $280 billion over 10 years.
Notes that the bill didn't pass the Senate due to Republicans not backing it, leading to a filibuster.
Points out the shift in Republican support from 84 votes in June to 55 votes in July, with a change in reasoning to make the funding discretionary rather than mandatory.
Questions the Republican Party's stated reasons for the change and speculates on possible motives, including obstructionism or retaliation against Democratic reconciliation deals.
Condemns the delay in providing healthcare to sick veterans and criticizes the idea of making the funding discretionary as a potential method for future cuts.
Mentions a planned Senate vote on the bill and expresses concern over the impact of delays on veterans' health outcomes and time with their families.

Actions:

for advocates, veterans, supporters,
Contact your Senators to support the PACT Act and urge them to prioritize veterans' healthcare (suggested).
Join advocacy groups working towards ensuring medical care for veterans exposed to toxic substances (exemplified).
</details>
<details>
<summary>
2022-07-31: Let's talk about NASA and the Dark side of the moon.... (<a href="https://youtube.com/watch?v=YPSi5mTltho">watch</a> || <a href="/videos/2022/07/31/Lets_talk_about_NASA_and_the_Dark_side_of_the_moon">transcript &amp; editable summary</a>)

NASA plans to send unmanned payloads to the dark side of the moon in 2025 to gather vital information, paving the way for future manned endeavors and inspiring humanity's journey beyond Earth.

</summary>

"At some point, assuming we don't filter ourselves out, we will be in other places."
"These little steps, these are the first steps towards us really getting off of this rock."
"It's events, launches, stuff that is propelling us off of this rock that I have always found inspiring."
"Maybe it won't be true as long as we can make it till then."
"Every once in a while it's important to note that stuff like that is on the horizon."

### AI summary (High error rate! Edit errors on video page)

NASA is planning to send unmanned payloads to the dark side of the moon, specifically to Schrodinger's crater, in 2025.
The mission aims to gather information on the moon's history, general environment, impact from space objects, earthquakes, moonquakes, and seismic activity.
This will be the first time NASA places anything on that side of the moon due to the challenges like no direct line of sight and the need for satellite relay for communication.
Artemis, the program associated with returning humanity to the moon, is named after a Greek goddess heavily linked with the moon and Apollo's twin.
NASA's mission to the dark side of the moon is geared towards better preparing for manned endeavors and astronaut trips.
Beau finds stories like this inspiring as they propel humanity beyond Earth, towards new chapters in space exploration.
Beau expresses excitement over events like this, which move humanity towards exploring beyond Earth.
He believes these small steps are paving the way for humans to venture into space and embrace a new future.
Beau suggests that, if humanity perseveres, the idea of having just one Earth might not hold true forever.
He acknowledges the importance of considering the advancements on the horizon like space exploration.

Actions:

for space enthusiasts, science enthusiasts,
Stay informed about NASA's Artemis program and future space exploration missions (implied).
</details>
<details>
<summary>
2022-07-31: Let's talk about GOP vs Trump.... (<a href="https://youtube.com/watch?v=wYAMTyLGyK4">watch</a> || <a href="/videos/2022/07/31/Lets_talk_about_GOP_vs_Trump">transcript &amp; editable summary</a>)

Trump faces pressure to delay announcing his run, fearing stronger opposition; internal politics seek to sideline him and remove the MAGA faction.

</summary>

"Delaying his announcement until after the midterms could further weaken Trump."
"Their goal is to delay his announcement in hopes of sinking his candidates and removing the MAGA faction."
"If Trump doesn't announce before the midterms, his political power's gone."
"The idea that he is a defeated candidate, that's a statement of fact."
"Delaying his announcement until after the midterms could further weaken Trump."

### AI summary (High error rate! Edit errors on video page)

Trump is being urged to delay announcing his run until after the midterms, with Kellyanne Conway publicly supporting this notion.
Some speculate that Pence and McConnell are trying to weaken Trump by pressuring him not to announce before the primary elections.
There are concerns that if Trump announces now and wins, he will become stronger, which is not in the interest of certain factions within the Republican Party.
Trump is perceived as a liability by some in the Republican establishment, who want to return to business as usual without his interference.
Some believe that Trump's chances of winning have diminished, especially with the baggage of January 6th and other controversies.
Delaying his announcement until after the midterms could further weaken Trump and suit the goals of those who want him out of the picture.
While some within the MAGA world may disrupt establishment plans, Trump remains the primary threat to their positions.
There is a strategy to portray Trump as a loser to discourage him from running, with hopes of minimizing potential losses for the Republican Party.
Beau expresses a desire for Trump to delay his announcement, believing it will decrease voter turnout on the Democratic side and ultimately weaken Trump's political power.
If Trump doesn't announce before the midterms, his best chance may be to break with the Republican Party and form a third party to maintain any political influence.

Actions:

for political strategists,
Pressure Trump to delay his announcement (implied)
Advocate for a shift in dynamics within the Republican Party (implied)
</details>
<details>
<summary>
2022-07-30: Let's talk about the RNC cutting Trump off.... (<a href="https://youtube.com/watch?v=WL8ATFzi0F4">watch</a> || <a href="/videos/2022/07/30/Lets_talk_about_the_RNC_cutting_Trump_off">transcript &amp; editable summary</a>)

The Republican Party pressures Trump to delay announcing his run, fearing his liability and preferring DeSantis as a replacement, potentially gaining control over him.

</summary>

"They want him to delay announcing his run until after the midterms because he's a liability."
"The Republican establishment has the upper hand."
"He's losing ground. He's sagging in the polls and nothing is going his way."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is cutting off Trump from legal bill payments once he announces his run for president, hinting at a potential ulterior motive.
They may want Trump to delay his announcement until after the midterms due to his perceived liability as a defeated defendant.
Internal polling may suggest to the Republican leadership that they can't win with Trump in either primary or general elections.
The Party aims to keep Trump energized while avoiding alarming independents and Democrats who fear his return in 2024.
Trump, needing money for legal battles, is in a position where he may be controlled by the Republican Party if he accepts their financial support.
Trump's declining popularity and the Republican establishment's preference for DeSantis as a potential replacement indicate their desire for Trump to step aside.
The Party fears that having a defeated and legally embattled figure as the face of the party will alienate moderate voters.
The pressure on Trump to step back seems coordinated, with the establishment possibly gaining the upper hand in controlling him.

Actions:

for political analysts, republican voters,
Support potential Republican candidates who prioritize unity and represent a broader appeal (implied)
Stay informed about internal dynamics and decisions within political parties (implied)
</details>
<details>
<summary>
2022-07-30: Let's talk about an order from Karl Rove to Trump.... (<a href="https://youtube.com/watch?v=xd2CH5PmesU">watch</a> || <a href="/videos/2022/07/30/Lets_talk_about_an_order_from_Karl_Rove_to_Trump">transcript &amp; editable summary</a>)

Karl Rove advises Trump on campaign timing, signaling a potential power shift within the Republican Party.

</summary>

"He is either going to fall in line and do what he's told like an obedient lackey, or they're going to destroy him."
"If he goes along with it, he's done. He has no power anymore."

### AI summary (High error rate! Edit errors on video page)

Karl Rove, a Republican strategist, wrote an op-ed questioning where Trump's donations go, suggesting Trump should wait until after the midterms to announce his presidential campaign.
Rove advises Trump to avoid upstaging the midterms and to set up a new committee for his campaign's political expenses.
Trump, if he defies Rove's advice, risks becoming another pawn in the Republican establishment or facing their opposition.
The Republican Party seems to be turning against Trump, viewing him as a liability if he announces before the midterms.
Trump's decision on when to announce his campaign could impact his influence over the MAGA movement within the Republican Party.
Any announcement by Trump before the midterms may lead to internal conflicts within the Republican Party.

Actions:

for political strategists,
Monitor Republican Party dynamics closely (implied)
</details>
<details>
<summary>
2022-07-29: Let's talk about planes, trains, and ethical consumption.... (<a href="https://youtube.com/watch?v=BRZQ1BZ3ddo">watch</a> || <a href="/videos/2022/07/29/Lets_talk_about_planes_trains_and_ethical_consumption">transcript &amp; editable summary</a>)

Two groups plan a trip, one chooses planes, the other trains for environmental reasons; individual actions can spark broader change, influencing corporations and cultural shifts.

</summary>

"Every journey begins with a single step."
"If it's profitable, they'll do it. If it doesn't make money, well, they won't."
"Cultural shift is necessary to influence companies' practices."

### AI summary (High error rate! Edit errors on video page)

Two groups planning a trip, one choosing planes and the other trains due to environmental concerns.
The debate arises about the impact of individual actions versus larger corporate decisions.
Four friends who saved up for four years to go on a trip together.
Even if the four friends take the environmentally friendly option, the plane will still fly.
Beau criticizes the defeatist mentality that individual actions don't matter.
Points out that collective individual actions can lead to broader trends and influence corporations.
Emphasizes that while the four friends' choice may not change anything, widespread changes can occur with more people making environmentally conscious decisions.
Cultural shift is necessary to influence companies' practices.
Capitalism prioritizes profit over environmental impact.
Companies will only change practices if it becomes unprofitable to continue environmentally harmful actions.

Actions:

for environmentally conscious individuals,
Choose environmentally friendly travel options (exemplified)
Encourage others to make environmentally conscious decisions (implied)
</details>
<details>
<summary>
2022-07-29: Let's talk about a billion trees.... (<a href="https://youtube.com/watch?v=plB4iiTTlWY">watch</a> || <a href="/videos/2022/07/29/Lets_talk_about_a_billion_trees">transcript &amp; editable summary</a>)

Government aims to combat wildfires and climate change by increasing tree replanting efforts, from a backlog of 4.1 million acres, to 400,000 acres annually, with plans to spend up to $260 million yearly.

</summary>

"It's really important that we do."
"It looks like they're just starting off slow, but they already have the plan to get it to a point where it actually matters."

### AI summary (High error rate! Edit errors on video page)

Government program to replant areas damaged by wildfires due to climate change and frequency of fires.
Backlog of 4.1 million acres needs to be replanted, but only 60,000 acres are done in an average year.
Administration aims to increase replanting to 400,000 acres annually, but this won't catch up to the 5 million acres burned on average every five years.
The administration plans to spend $100 million next year on replanting efforts, with a potential increase to $260 million annually.
Replanting trees is vital as they serve as a carbon sink.
The goal is to not just catch up but get ahead in replanting efforts to combat the effects of wildfires and climate change.

Actions:

for climate activists, environmentalists,
Support tree planting initiatives in your community (exemplified)
Advocate for increased funding for tree replanting efforts (exemplified)
</details>
<details>
<summary>
2022-07-29: Let's talk about Democratic party strategy.... (<a href="https://youtube.com/watch?v=_2EbROpSQdA">watch</a> || <a href="/videos/2022/07/29/Lets_talk_about_Democratic_party_strategy">transcript &amp; editable summary</a>)

Beau delves into the Democratic Party's short-term focus, lack of strategy, and the necessity for consistent policy and messaging to solidify its position.

</summary>

"It's probably not a great idea to elevate the MAGA faction of the Republican Party."
"The Democratic Party is worried about DeSantis in 2024."
"The Democratic Party needs to come up with a sign you see in front of a roller coaster."
"Nobody runs unopposed."
"They don't have consistent messaging because they don't have consistent policy."

### AI summary (High error rate! Edit errors on video page)

Differentiates between tactics and strategy within the Democratic Party.
Warns against elevating the MAGA faction of the Republican Party as a long-term plan.
Criticizes the Democratic Party for lacking a long-term strategy and only focusing one election ahead.
Points out the importance of preventing DeSantis from getting reelected as governor to hinder his potential presidential run.
Urges the Democratic Party to think in a longer timeframe and develop and implement policy effectively.
Suggests that the Democratic Party should establish clear progressive positions for members to adopt.
Emphasizes the need for consistent messaging and cheerleaders within the Democratic Party.
Advocates for a stronger ground game at the state and local levels for the Democratic Party.
Stresses the importance of converting popular ideas into successful legislation for the Democratic Party's platform.

Actions:

for political strategists, democratic party members,
Ensure no Republican candidate runs unopposed, even at the local level (suggested).
Strengthen the ground game at the state and local levels for the Democratic Party (implied).
Establish clear progressive positions for Democratic Party members to adopt (implied).
</details>
<details>
<summary>
2022-07-28: Let's talk about the Rhine and recession.... (<a href="https://youtube.com/watch?v=VukZffko4C8">watch</a> || <a href="/videos/2022/07/28/Lets_talk_about_the_Rhine_and_recession">transcript &amp; editable summary</a>)

Beau tackles the impact of low Rhine river levels on economies globally, while addressing climate change skepticism in a humorous message.

</summary>

"What's up, little Beau Peep with your little sheep?"
"Climate change is a socialist plot. It's not real."
"Wake up and wake others up or just bah."

### AI summary (High error rate! Edit errors on video page)

Beau plans to switch up his usual format by first doing a video and then reading a related message about the topic.
The topic of the video is the Rhine river, inflation, Germany, and their impact on economies globally.
The Rhine river, a vital route for transporting goods, is currently running low, causing issues for barges which need more depth to operate efficiently.
Due to the low water levels, barges can carry fewer goods, leading to disruptions in the supply chain, increased shipping rates, and ultimately higher prices.
This situation is worsened by inflation and could potentially push Germany into a recession, with possible repercussions across Europe.
Beau draws parallels between the economic situations in Germany and the United States, suggesting that both countries might be facing recessionary pressures.
He speculates that climate change could play a role in these economic challenges, hinting at a global economic downturn.
The related message Beau reads dismisses climate change as a socialist plot, attributing issues like the low Colorado River levels to mere drought rather than climate change.
The message challenges the idea of climate change impacting multiple regions and questions the authenticity of its effects.
Beau ends on a humorous note, sharing the message he received and encouraging people to raise awareness about climate change.

Actions:

for climate change activists,
Raise awareness about climate change and its impacts on economies globally (implied)
</details>
<details>
<summary>
2022-07-28: Let's talk about Trump's new relationship with conservative media.... (<a href="https://youtube.com/watch?v=wxfZQGur8Xs">watch</a> || <a href="/videos/2022/07/28/Lets_talk_about_Trump_s_new_relationship_with_conservative_media">transcript &amp; editable summary</a>)

Trump's once loyal conservative media outlets are now breaking ties, speculated reasons include journalistic integrity and money motives, shifting the landscape as 2024 approaches.

</summary>

"Outlets that were once unquestioningly loyal to Trump are now breaking with him."
"Speculations on why this shift is happening include journalistic integrity, fatigue with Trump's scandals, and awareness of the DOJ investigation."
"Beau believes the primary motivator behind the behavior shift is money."

### AI summary (High error rate! Edit errors on video page)

Trump's relationship with conservative media is shifting.
Outlets that were once unquestioningly loyal to Trump are now breaking with him.
OANN lost their last major carrier, impacting their reach to help Trump.
Fox News, traditionally loyal, has started criticizing Trump's inaction and unfavorable polling numbers.
The New York Post and The Wall Street Journal, both favorable to Trump, have condemned him recently.
Speculations on why this shift is happening include journalistic integrity, fatigue with Trump's scandals, and awareness of the DOJ investigation.
One theory suggests outlets are positioning themselves for the 2024 Republican nominee race to maximize ad revenue.
Beau believes the primary motivator behind the behavior shift is money.
Trump might face challenges as he loses the unwavering support of news outlets.

Actions:

for media analysts,
Analyze and understand the shifting dynamics between Trump and conservative media outlets (suggested).
Stay informed about the motivations behind media behavior shifts (suggested).
Monitor how media outlets position themselves for the 2024 Republican nominee race (suggested).
</details>
<details>
<summary>
2022-07-28: Let's talk about Republicans bizarre request of DOD.... (<a href="https://youtube.com/watch?v=5cWHtdsXHH8">watch</a> || <a href="/videos/2022/07/28/Lets_talk_about_Republicans_bizarre_request_of_DOD">transcript &amp; editable summary</a>)

Senate Armed Services Committee suggests halting efforts to root out extremists in the military, raising questions on motives and fiscal responsibility, met with strong opposition and calls for scrutiny.

</summary>

"Spending additional time and resources to combat exceptionally rare instances of extremism in the military is an inappropriate use of taxpayer funds."
"That is something else."
"Is that a good use of taxpayer funds?"
"I think that this little part of this report should probably get a little bit more debate, a little bit more discussion, definitely more coverage."
"We have to find out why exactly Republicans don't want the Department of Defense getting rid of extremist elements."

### AI summary (High error rate! Edit errors on video page)

Senate Armed Services Committee requested Pentagon to stop rooting out extremists in the military.
Every Republican on the Committee voted for this request.
Independent Senator Angus King also supported the request.
The suggestion claimed combating extremism in the military is a wasteful use of taxpayer funds.
The suggestion was included in the Senate's National Defense Authorization Act (NDAA).
Both Democrats and independents opposed the suggestion.
Beau questions the reasoning behind allowing extremists in the military.
He calls for a cost-benefit analysis of this decision.
Beau sees it as an attempt to shape a particular narrative.
He urges for more debate and scrutiny on this issue.
The Department of Defense is not bound to follow this suggestion.
Beau is confident that the Department will continue rooting out extremists.
He stresses the importance of understanding the motives behind this request.
Beau calls for accountability and answers regarding why some Republicans support this stance.

Actions:

for congressional representatives,
Question the motives behind supporting the suggestion (suggested)
Push for more debate and coverage on this issue (suggested)
Call for accountability and answers regarding the stance on combatting extremism in the military (suggested)
</details>
<details>
<summary>
2022-07-27: Let's talk about the emails about the Trump's electors.... (<a href="https://youtube.com/watch?v=4zEPB2mFBoE">watch</a> || <a href="/videos/2022/07/27/Lets_talk_about_the_emails_about_the_Trump_s_electors">transcript &amp; editable summary</a>)

Exploring the fake elector scheme, revealed emails hint at a deeper plot, implicating White House involvement and challenging the idea of a contingency plan, adding legal troubles for the former president.

</summary>

"So is this a smoking gun? No."
"There's not going to be a single smoking gun. You're going to have a bunch of stuff that piles up."
"This is bad news on top of more bad news for the former president."

### AI summary (High error rate! Edit errors on video page)

Exploring the fake elector scheme and recently revealed information.
Some emails have become public, revealing plans to send fake electoral votes to Pence.
The use of quotation marks around "fake" and "someone" in the emails raises questions.
Beau speculates that the quotation marks indicate a direct quote from someone speaking cryptically.
The emails show White House involvement in the elector scheme, dispelling any doubts.
They also challenge the defense of acting as a contingency plan by suggesting secrecy until January 6th.
The revelation of these emails adds to the legal troubles for the former president.
Confirmation of Trump being a target of a criminal investigation has been made official.
Beau anticipates more developments and the importance of the quoted sentences in the emails.

Actions:

for legal analysts, political activists.,
Analyze the implications of the revealed emails and stay informed about the legal developments (implied).
Support transparency and accountability in political processes through advocacy and awareness-raising efforts (implied).
</details>
<details>
<summary>
2022-07-27: Let's talk about Vegas, pools, and public relations.... (<a href="https://youtube.com/watch?v=4g-AlrsiSJU">watch</a> || <a href="/videos/2022/07/27/Lets_talk_about_Vegas_pools_and_public_relations">transcript &amp; editable summary</a>)

Las Vegas capping pool sizes sends a public message about water scarcity, but tangible actions are needed for real results as warnings alone won't suffice for future water issues.

</summary>

"This is a very public message that Vegas isn't going to have enough water."
"I hope people understand that you can't continue to expand these cities."
"The reality is we're way past warnings."
"It's a step in the right direction for once."
"I wouldn't expect this to even remotely impact the water issues that Vegas is going to have in the future."

### AI summary (High error rate! Edit errors on video page)

Las Vegas is capping the size of backyard swimming pools due to inadequate water supply.
The average pool size in Vegas is 470 square feet, while the cap is set at 600.
This move targets those wanting giant pools, not for major water conservation.
It serves as a public message about water scarcity in Vegas.
The commission acknowledges this as a PR move, knowing its limited effectiveness.
Despite saving a few million gallons of water, more significant decisions lie ahead.
The world is warming, leading to more droughts and less water.
Beau sees this as a warning shot rather than a solution.
While a step in the right direction, tangible actions are needed for real results.
Beau doesn't expect this cap to solve Vegas's future water issues.

Actions:

for residents, policymakers, environmentalists,
Address water scarcity through tangible actions (implied)
Advocate for sustainable city planning to combat future water issues (implied)
Raise awareness about the impacts of urban expansion on water resources (implied)
</details>
<details>
<summary>
2022-07-27: Let's talk about VP Harris and that weird introduction.... (<a href="https://youtube.com/watch?v=qd3UC3dOpZc">watch</a> || <a href="/videos/2022/07/27/Lets_talk_about_VP_Harris_and_that_weird_introduction">transcript &amp; editable summary</a>)

Beau points out the missed message of kindness and empathy in mocking Vice President Harris's self-description, reminiscent of Mr. Rogers's subtle methods of promoting tolerance.

</summary>

"If you did grow up on Mr. Rogers, and your first inclination upon seeing that clip was to mock it, I think you missed the point of the show."
"It's weird."
"I'd bet Mr. Rogers would be pretty disappointed in you."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Standing in a shop wearing a blue t-shirt with an iconic scene depicting Mr. Rogers and Officer Clemens with their feet in a kiddie pool.
Mr. Rogers used subtle yet direct methods to encourage tolerance and kindness during a time when people had issues sharing pools.
Vice President Harris introduced herself at a disability conference, practicing self-description for the benefit of those with limited sight.
Mocking of the clip from the conference was primarily done by individuals who grew up watching Mr. Rogers, missing the point of the show.
Mr. Rogers, known for encouraging people to look for helpers, had some odd behaviors like narrating feeding his fish for a blind viewer.
The act of self-description, like Harris did, is a way to make events more accessible for those with disabilities.
Beau questions the response of those mocking Harris's self-description, suggesting they may have missed the show's message of kindness and empathy.
Encourages reflection on the deeper meaning behind seemingly simple actions like those of Mr. Rogers and Vice President Harris.
Emphasizes the importance of understanding the intentions behind actions rather than just surface-level appearances.
Beau implies that Mr. Rogers himself might have been disappointed in those who missed the point of his message of kindness and understanding.

Actions:

for viewers,
Attend or support events and conferences designed to be accessible to individuals with disabilities (implied).
</details>
<details>
<summary>
2022-07-26: The roads to the governor's office with Nikki Fried.... (<a href="https://youtube.com/watch?v=Hrlj7W_8BaY">watch</a> || <a href="/videos/2022/07/26/The_roads_to_the_governor_s_office_with_Nikki_Fried">transcript &amp; editable summary</a>)

Beau and Nikki Freed dissect the impact of the governor race in Florida, Freed's roles, values, plans, and the necessity to defeat DeSantis for the people's sake.

</summary>

"Try something new."
"This isn't about me at all. It's about the people."
"We haven't had a female governor of our state."

### AI summary (High error rate! Edit errors on video page)

Beau and Nikki Freed talk about the impact of the race for governor in Florida and its national implications.
Nikki Freed explains her role as the commissioner of agriculture and consumer services, overseeing various aspects like agriculture, food programs, energy, and security.
Freed shares her experience working with non-Democratic officials in Tallahassee and her efforts to find compromise.
The transcript delves into Freed's stance on cannabis legalization and her lawsuit against President Biden regarding gun rights for medical marijuana patients.
Freed differentiates herself from her primary opponent, Chris, by discussing her long-standing Democratic values and commitment to various social issues.
The importance of beating current Governor DeSantis is emphasized, with Freed labeling him as authoritarian and a threat to democracy.
Freed outlines her plans if elected, including declaring housing and gas emergencies, focusing on education funding, expanding Medicaid, and protecting women's rights.
The transcript touches on the need for air conditioning in prisons as a basic necessity.
Freed criticizes Governor DeSantis for his actions against organizations like Disney and the cruise industry, showcasing a lack of civil discourse.
Nikki Freed encourages voter participation in the upcoming primary and presents herself as a new type of leadership for Florida.

Actions:

for florida voters,
Support Nikki Freed in the upcoming primary election (implied)
Educate yourself on the candidates' policies and backgrounds (suggested)
Encourage voter turnout and engagement in the political process (implied)
</details>
<details>
<summary>
2022-07-26: Let's talk about military recruitment problems.... (<a href="https://youtube.com/watch?v=KJe-eSmqTsc">watch</a> || <a href="/videos/2022/07/26/Lets_talk_about_military_recruitment_problems">transcript &amp; editable summary</a>)

Addressing recruitment issues in the Department of Defense, Beau breaks down the factors impacting the shrinking recruitment pool, from criminal records to falling patriotism, and questions the country's values and treatment of its citizens.

</summary>

"Y'all aren't the solution. Y'all are the problem."
"Not exactly Medal of Honor material, just saying."
"If you don't want to make the country better and move forward, you are not a patriot."

### AI summary (High error rate! Edit errors on video page)

Addressing recruitment issues in the Department of Defense (DOD) due to trouble recruiting.
Not surprised to find readiness issues in the establishment.
Majority of recruitment issues not related to what Beau has discussed previously.
Target recruitment age for new enlistees is 17 to 24.
Reduction in eligible candidates from 29% to 23%.
Criminal records, substance use, and obesity contributing to the shrinking recruitment pool.
57% of those not interested in serving fear emotional or physical harm.
Army hitting only 68% of its recruitment goals through April.
COVID and a strong job market impacting recruitment challenges.
Falling patriotism and disillusionment contributing to lack of interest in joining.
Critiques MAGA crowd's perception of patriotism.
Issues with the demographic makeup, public health, and economy affecting recruitment.

Actions:

for recruitment officials, policymakers, concerned citizens,
Advocate for policies that improve recruitment eligibility (implied)
Support programs that address substance use and obesity in communities (implied)
Promote a positive and inclusive national narrative to boost patriotism and interest in service (implied)
</details>
<details>
<summary>
2022-07-26: Let's talk about how Trump already lost a governorship in Maryland.... (<a href="https://youtube.com/watch?v=RJ8iSsNaGz4">watch</a> || <a href="/videos/2022/07/26/Lets_talk_about_how_Trump_already_lost_a_governorship_in_Maryland">transcript &amp; editable summary</a>)

Trump's influence in Republican primaries may cost them the governorship in Maryland, revealing a cycle where candidates need Trump to win primaries but struggle in general elections due to his endorsement.

</summary>

"Republicans are in a position where they can't win the primary without Trump's endorsement, but they can't win the general election if they have Trump's endorsement."
"It is the Republican leadership's fault that this is happening because they have refused to lead."
"That's Trump at work right there."

### AI summary (High error rate! Edit errors on video page)

Trump's influence in the primaries has likely cost the Republican Party a governorship in Maryland.
Trump threw his weight behind a far-right candidate named Daniel Cox in Maryland, opposing the moderate Republican governor's chosen successor.
Democratic Governors Association allegedly helped Daniel Cox win the primary because they saw him as an easy opponent to beat.
Daniel Cox is described as a "Q whack job," while the Democratic nominee, Wes Moore, is a war veteran and Rhodes Scholar.
Beau predicts Wes Moore will win the election in Maryland due to the circumstances created by Trump.
Republicans are facing a dilemma where they need Trump's endorsement to win the primary but struggle in the general election with his endorsement.
Beau places the blame on the Republican Party leadership for allowing Trump's influence to shape their candidates.
The situation in Maryland is likely to repeat in other areas where candidates endorsed by Trump may struggle in general elections.
Despite some slim chance for Cox to win, projections suggest Wes Moore will win by a significant margin, showcasing Trump's impact.
Beau concludes by reflecting on the consequences of Trump's influence on Republican candidates.

Actions:

for political observers, republican voters,
Support candidates based on qualifications and policies rather than blind loyalty to a particular figure (implied).
</details>
<details>
<summary>
2022-07-26: Let's talk about Team Pence and Marc Short talking to a grand jury.... (<a href="https://youtube.com/watch?v=H0mMB-MsdUw">watch</a> || <a href="/videos/2022/07/26/Lets_talk_about_Team_Pence_and_Marc_Short_talking_to_a_grand_jury">transcript &amp; editable summary</a>)

High-ranking Pence associates testify before grand jury, signaling DOJ's active investigation into fake electors scheme and potential White House involvement.

</summary>

"DOJ is actively, very actively, looking into the fake electors scheme."
"They believe this scheme reaches into the White House."
"It's not really a turning point in the investigation."

### AI summary (High error rate! Edit errors on video page)

High-ranking individuals from Pence's inner circle testified before a federal grand jury last week.
Commentary has shifted from DOJ inaction to DOJ involvement with Trump.
DOJ's investigation largely occurs out of public view, focusing on the fake electors scheme.
Activities such as subpoenas, FBI and National Archives involvement, and Eastman's case are related to the scheme.
Testimony from Pence's chief of staff and another individual fits neatly into the investigation.
Speculation surrounds whether Trump is involved, with indications pointing in that direction.
DOJ actively investigates the fake electors scheme, indicating White House involvement.
Testimonies suggest Pence may not need to testify if individuals are forthcoming.
Rumors suggest questioning focused on Giuliani and Eastman, related to the fake electors scheme.
The investigation likely extends beyond what is currently publicly known.

Actions:

for political commentators, concerned citizens,
Stay informed on developments in the investigation (suggested)
Advocate for transparency and accountability in government actions (implied)
</details>
<details>
<summary>
2022-07-25: Let's talk about the people Trump doesn't know.... (<a href="https://youtube.com/watch?v=3f3PrMoG8zU">watch</a> || <a href="/videos/2022/07/25/Lets_talk_about_the_people_Trump_doesn_t_know">transcript &amp; editable summary</a>)

Trump disavows supporters once their use is over, letting others pay for his mistakes, even those who risked everything for him.

</summary>

"You do not represent our movement. You do not represent our country."
"Even those people willing to put themselves at risk, those people willing to quite literally stand on the front lines for him, those people willing to be in custody for him, they're not part of his movement."
"Loyalty shown to the former president will never be returned."

### AI summary (High error rate! Edit errors on video page)

Criticisms of Trump result in him claiming he doesn't know the individuals, disavowing them by pretending he never met them.
Trump labeled the Capitol attack as a heinous attack after it could potentially harm his image, distancing himself from the incident.
Despite wearing MAGA gear and being his supporters, Trump turned on the Capitol rioters, stating, "You will pay" and that they don't represent his movement or country.
Trump's habit of disavowing people and distancing himself from mistakes by letting others pay for them is evident in his reaction to the Capitol attack.
Loyalty shown to Trump by many individuals will never be reciprocated, as he easily discards people once they no longer serve his purpose.
Even those who risked their safety, stood on the front lines, or faced custody for Trump are disregarded and excluded from his movement once he no longer needs them.

Actions:

for supporters of trump,
Stand up for accountability within political leadership (implied)
Recognize and acknowledge patterns of behavior in political figures (implied)
</details>
<details>
<summary>
2022-07-25: Let's talk about Wyoming, Cheney, and the primary.... (<a href="https://youtube.com/watch?v=Z71YjJaNvAg">watch</a> || <a href="/videos/2022/07/25/Lets_talk_about_Wyoming_Cheney_and_the_primary">transcript &amp; editable summary</a>)

Beau warns of the consequences of Liz Cheney potentially losing the primary in Wyoming and predicts her enduring national influence despite the sacrifice.

</summary>

"If she loses, that means the Trump machine gets entrenched in Wyoming."
"She chose to support the Constitution over Trump."
"She has set herself up very well to be on the national scene in 2024 or 2028."
"She gets that forever."
"She has set herself up as the force that can turn the GOP back into an American political party."

### AI summary (High error rate! Edit errors on video page)

Explains the impact of Liz Cheney potentially losing the primary in Wyoming.
Warns that if Cheney loses, the Trump machine will be entrenched in Wyoming, leading state and local officials to adopt Trump-like strategies for power.
Points out that people in Wyoming are shielded from the negative effects of Trumpism at the national level.
Predicts that Wyoming residents may not appreciate the authoritarian policies that could follow a Cheney loss.
Acknowledges that Cheney's stance of putting country over party and holding Trump accountable could lead to her losing the primary.
Suggests that Cheney's sacrifice of her office for ethics and integrity could make her a significant figure in national politics in the future.
Speculates that Cheney's actions position her as a potential force in the Republican Party to steer it away from being a cult of personality.
Emphasizes that Cheney's decision to support the Constitution over Trump could shape her political career positively in the long term.
Contrasts Wyoming's deep red political landscape with Georgia's swing state status in evaluating Cheney's chances in the primary.
Concludes with the idea that Cheney may not fade away from politics after the primary, leveraging her sacrifice for political advantage.

Actions:

for political observers, republican voters,
Support political candidates who prioritize ethics and upholding the Constitution (exemplified)
Stay informed about the impact of local and state officials' political strategies on communities (implied)
</details>
<details>
<summary>
2022-07-25: Let's talk about Season 2 of the Jan 6 hearings.... (<a href="https://youtube.com/watch?v=9o6-2TWmu4c">watch</a> || <a href="/videos/2022/07/25/Lets_talk_about_Season_2_of_the_Jan_6_hearings">transcript &amp; editable summary</a>)

Beau anticipates upcoming September hearings focusing on non-political figures' ties, potential arrests in the Republican establishment, and Trump's influence on the midterms, warning of the GOP's failure to hold Trump accountable.

</summary>

"The Republican Party has made a lot of mistakes over the last few years."
"The choice will be clear."
"Had they taken control of their party after the 6th, they probably would have been in pretty good shape."
"But I do not see at this point how the Republican Party plans on dodging this."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Overview of upcoming hearings in September with a focus on non-political figures and their ties to various groups.
Mention of a subpoena for Jenny Thomas, wife of Justice Clarence Thomas, and withheld information about members of Congress.
Speculation on potential arrests of prominent Republican establishment members before the midterms.
Prediction that these developments will impact the outcomes of the upcoming elections.
Suggestion that Trump's legal troubles may escalate in Georgia rather than DC.
Analysis of Trump's potential influence on the midterms and his strategy to maintain support.
Anticipation of Trump pushing his baseless claims during the midterms and influencing Republican candidates.
Warning about the Republican Party's failure to hold Trump accountable after January 6th and its long-term consequences.
Doubt about the Republican Party's ability to recover from ongoing issues in time for the 2024 elections.
Concern about the potential consequences if those protecting the institution of the presidency do not prevail.

Actions:

for politically engaged individuals,
Contact local representatives to express concerns about political accountability (implied)
Support organizations working towards political transparency and accountability (suggested)
Stay informed about political developments to make informed voting decisions (implied)
</details>
<details>
<summary>
2022-07-24: Let's talk about polio.... (<a href="https://youtube.com/watch?v=jF7UPZ-Ws8I">watch</a> || <a href="/videos/2022/07/24/Lets_talk_about_polio">transcript &amp; editable summary</a>)

Beau provides an update on a polio case in New York, stressing the importance of vaccinations and dispelling misconceptions about his motivations.

</summary>

"I'm not a shill for big vaccine."
"I have another motivation. And it's not that I'm for any particular industry. I'm actually against another large industry, children's coffins."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Provides an update on a concerning situation involving polio, a disease thought to be eradicated.
Reports a case of a young adult in New York contracting polio, the first confirmed case in the US since 2013.
Explains the virus is vaccine-derived, meaning someone outside the US shed the virus after being vaccinated with the oral version.
Emphasizes that vaccine gaps are causing the resurgence of polio.
Notes the serious effects of the disease, such as infections of the brain, spine, paralysis, and death, with no known cure.
Mentions warnings issued in Britain and on his channel to stay up to date with vaccinations due to traces found in sewage.
Addresses accusations of being a "shill for big vaccine," clarifying that his motivation comes from opposing the industry of children's coffins.
Concludes with a thought about the importance of staying informed and taking necessary precautions against diseases like polio.

Actions:

for health-conscious individuals,
Get vaccinated and stay up to date with vaccinations to prevent the spread of diseases like polio. (suggested)
Stay informed about health warnings and take necessary precautions. (suggested)
</details>
<details>
<summary>
2022-07-24: Let's talk about Republican voting records.... (<a href="https://youtube.com/watch?v=rvUGIutwMOY">watch</a> || <a href="/videos/2022/07/24/Lets_talk_about_Republican_voting_records">transcript &amp; editable summary</a>)

Exploring Republican voting records reveals a pattern of control over freedom and rights, contradicting their claimed values.

</summary>

"It's not about states' rights. It's not about freedom. It never was. That's a lie."
"They're telling you who they are."
"They don't care about freedom. They don't care about states' rights. They don't care about anything that they said they do."
"People need to acknowledge it, accept it for what it is."
"You are taking away their rights, their freedoms, so you can continue a habit that you've had."

### AI summary (High error rate! Edit errors on video page)

Exploring Republican voting records and the disparity between rhetoric and actions.
Recent examples of Republicans voting against protecting access to contraceptives, interracial marriage, and same-sex marriage.
Republicans claiming to be moderate but voting against these protections.
Republicans claiming it's about states' rights but voting against measures that allow interstate travel for family planning.
Over 200 Republicans in the House voting against interstate travel for family planning, contradicting their supposed support for states' rights.
Revealing the true intentions behind Republican votes: control, not states' rights or freedom.
Over 150 Republicans voting against protecting interracial marriage and same-sex marriage.
Over 190 Republicans voting against ensuring access to birth control.
Republicans voting against these measures knowing they wouldn't stop them from passing, showing their commitment to their positions.
The disconnect between Republican rhetoric and their actual votes, exposing their authoritarian tendencies.

Actions:

for voters,
Hold elected officials accountable for their actions (exemplified)
Educate others about the disconnect between political rhetoric and voting records (exemplified)
Support candidates who truly uphold values of freedom and rights (exemplified)
</details>
<details>
<summary>
2022-07-24: Let's talk about Pence giving orders on the 6th.... (<a href="https://youtube.com/watch?v=T8ZTytjqceE">watch</a> || <a href="/videos/2022/07/24/Lets_talk_about_Pence_giving_orders_on_the_6th">transcript &amp; editable summary</a>)

Pence lacked authority to command National Guard; media confusion led to misinterpretation.

</summary>

"Pence did not have the authority to order the National Guard to do anything."
"The media misunderstanding led to the perception of Pence giving orders."
"Tough questions may arise due to the media's portrayal of Pence giving orders."

### AI summary (High error rate! Edit errors on video page)

Pence did not have the authority to order the National Guard to do anything.
Acting Secretary of Defense Miller, who has the authority, likely received a strong suggestion from Pence.
Miller could have then issued orders based on Pence's suggestion.
General Milley might have been directed by Miller to assist Pence if needed.
The media misunderstanding led to the perception of Pence giving orders, when it was actually Miller who did.
There was no de facto 25th Amendment situation, and it's unlikely that Miller or Milley spoke directly to the Commander-in-Chief during the Capitol issue.
Tough questions may arise due to the media's portrayal of Pence giving orders, but they can be answered by clarifying the chain of command.
It's improbable that anyone bypassed the Commander-in-Chief due to concerns of a coup attempt.

Actions:

for citizens, military personnel,
Verify facts about chain of command (suggested)
Seek clarification on military orders (suggested)
</details>
<details>
<summary>
2022-07-23: Let's talk about the Trump outtakes and the Bannon audio.... (<a href="https://youtube.com/watch?v=62LV-o8VxnM">watch</a> || <a href="/videos/2022/07/23/Lets_talk_about_the_Trump_outtakes_and_the_Bannon_audio">transcript &amp; editable summary</a>)

Beau underscores the significance of the ban on audio and outtakes in revealing manipulation by the former president, urging individuals to challenge conspiracy beliefs by discussing these pieces of evidence.

</summary>

"He put them at risk over something he knew to be untrue."
"Those two pieces of evidence."
"They look for patterns."
"Turns out the person they gave their trust to was the person that was manipulating them."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talks about the ban on audio and outtakes amplifying negative opinions of the former president.
Mentions the ban on audio from before the election, where Trump planned to declare victory even if he lost.
Explains how lack of knowledge about early voting and mail-in voting was exploited to manipulate people.
Describes how the red mirage concept was used to deceive rather than inform the public.
Expresses frustration that even after certification, Trump continues to spread false claims about the election.
Notes that by January 7th, Trump knew he had lost but continues to propagate falsehoods.
Criticizes the manipulation of people through false claims and fundraising.
Urges using evidence of the ban on audio and Trump's post-election actions to challenge conspiracy beliefs.
Emphasizes the importance of getting individuals to acknowledge Trump's knowledge of his loss.
Encourages questioning and discussing the ban on audio and Trump's persistent false claims with those who believe them.

Actions:

for concerned citizens,
Challenge conspiracy beliefs by discussing the ban on audio and Trump's false claims with believers (suggested).
Encourage acknowledgment of Trump's knowledge of his loss by sharing evidence from the transcript (implied).
</details>
<details>
<summary>
2022-07-23: Let's talk about the Secret Service, deleted texts, fear, and reason.... (<a href="https://youtube.com/watch?v=DMmihMHRFmA">watch</a> || <a href="/videos/2022/07/23/Lets_talk_about_the_Secret_Service_deleted_texts_fear_and_reason">transcript &amp; editable summary</a>)

The Inspector General investigates the Secret Service for potentially deleting text messages, raising questions about their actions and fear levels on January 6th.

</summary>

"If it was done intentionally, it would be a crime."
"I don't believe they misjudged their opposition by that much without information that led them to do that."
"They had every right to be scared."
"The level of fear that they had, that's a question."
"Yeah, they need to do whatever they can to get those text messages."

### AI summary (High error rate! Edit errors on video page)

The Inspector General's Office is investigating the Secret Service for potentially deleting text messages, treating it as a criminal investigation.
Questions arose about the Secret Service's actions on January 6th, with concerns about their fear levels and communication practices.
The Secret Service was criticized for sheltering in place during high-security events, but mobility is vital in such situations.
Despite being scared and using less than ideal communication practices, the Secret Service handled the situation well on January 6th.
Beau suggests that the fear exhibited by the Secret Service may have been based on perceived rather than actual threat capability.
Beau calls for an inquiry into why the Secret Service was so scared on January 6th, pointing to the importance of looking at threat assessments.
There is speculation about the content of deleted text messages and their potential connection to the level of fear within the Secret Service.
Beau comments on the seriousness of deleting government records and the potential criminal implications if done intentionally or systematically.
Despite some shortcomings in communication practices, the Secret Service's actions on January 6th were effective in ensuring Pence's safety.
Beau underscores the importance of investigating the level of fear within the Secret Service on January 6th and understanding the basis for it.

Actions:

for oversight committees,
Investigate the threat assessments and information the Secret Service received regarding the January 6th events (suggested)
Take steps to retrieve the potentially deleted text messages for investigation (suggested)
</details>
<details>
<summary>
2022-07-23: Let's talk about the Constitution, marriage, and a talking point.... (<a href="https://youtube.com/watch?v=GYDzwiw5TFI">watch</a> || <a href="/videos/2022/07/23/Lets_talk_about_the_Constitution_marriage_and_a_talking_point">transcript &amp; editable summary</a>)

Addressing misconceptions about constitutional rights and the Ninth Amendment, Beau dismantles the argument that specific rights must be listed, urging critical understanding over blind adherence.

</summary>

"Where in the Constitution does it say that straight people have a right to get married, right to travel?"
"The people you're following, the people you're getting these talking points from, are the sort of people that the people who wrote the Constitution viewed as their enemy."
"It's the Ninth Amendment."
"Stop. It doesn't send the message you think."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing the Republican Party's attempt to undermine gay marriage by claiming it's not in the Constitution.
Explaining the Ninth Amendment and how it protects rights not explicitly listed in the Constitution.
Criticizing those who push the talking point that rights must be specifically listed to exist.
Pointing out the danger of authoritarian figures trying to limit freedoms by demanding specific rights be listed.
Reminding followers to stop parroting misleading talking points and to understand the true spirit of the Constitution.
Calling out individuals who claim to be "originalists" but fail to grasp the essence of the Constitution.
Urging people to read and understand the Constitution rather than blindly following misleading narratives.
Emphasizing the importance of recognizing and defending rights beyond those explicitly stated in the Constitution.
Encouraging self-reflection and critical thinking when it comes to constitutional rights and principles.
Condemning those who undermine freedom and the values enshrined in the Constitution.

Actions:

for constitutional rights advocates,
Read and understand the Constitution (suggested)
Stop parroting misleading talking points (suggested)
Engage in self-reflection on constitutional principles (implied)
</details>
<details>
<summary>
2022-07-22: Let's talk about the Republican takeaway from the hearings.... (<a href="https://youtube.com/watch?v=ovVgzxHClB8">watch</a> || <a href="/videos/2022/07/22/Lets_talk_about_the_Republican_takeaway_from_the_hearings">transcript &amp; editable summary</a>)

Addressing Republicans about acknowledging being tricked and played by those they trusted, urging critical analysis of politicians echoing Trump's claims to avoid being manipulated or betrayed.

</summary>

"You have to acknowledge that you got tricked. You got played."
"You have to figure out who's going to stab you in the back."
"There's nothing to suggest that he was doing anything other than trying to stay in power by any means necessary."

### AI summary (High error rate! Edit errors on video page)

Addressing Republicans about their takeaway from recent hearings where evidence was presented, including audio of Bannon discussing Trump's actions and intentions.
Pointing out that if Republicans believe the claims presented, it means the people they trusted were laughing at them.
Emphasizing the need for Republicans to acknowledge that they were tricked and played by those they trusted.
Suggesting that Republicans should critically analyze politicians echoing Trump's claims and question their motives, whether they were manipulated by Trump or are using the claims for their own benefit.
Urging Republicans to discern between those who genuinely believed Trump and those who knew he was lying but used it as a path to power.
Encouraging Republicans to identify who they can trust moving forward and be wary of individuals who may betray their trust.
Stating that there is no evidence to support the belief that Trump acted in good faith during the election aftermath, indicating his primary goal was to stay in power regardless of the consequences.
Implying the importance of seeking clarity and potentially reassessment of beliefs in light of presented evidence.

Actions:

for republicans,
Question politicians echoing Trump's claims in primaries, assessing their motives and actions (suggested)
Identify trustworthy individuals in political spheres and be cautious of potential betrayal (implied)
</details>
<details>
<summary>
2022-07-22: Let's talk about Trump, Wisconsin, and personality.... (<a href="https://youtube.com/watch?v=vAa9INuk9e4">watch</a> || <a href="/videos/2022/07/22/Lets_talk_about_Trump_Wisconsin_and_personality">transcript &amp; editable summary</a>)

Former President Trump's actions in Wisconsin reveal concerning personality traits that are detrimental to effective leadership and political integrity.

</summary>

"I didn't lose, they cheated, and then flips over the Monopoly board."
"The inability to accept when you are wrong just leads you down a road where you continue to make mistakes."

### AI summary (High error rate! Edit errors on video page)

Former President Trump called the Speaker of the House in Wisconsin, asking for the election to be decertified.
Trump's actions prompt questions about his intent and personality characteristics.
One option is that Trump truly believes decertifying the election could lead to him returning to the White House.
Another option is that Trump is attempting to build an insanity defense.
Trump's behavior fits with his personality traits of not letting go of grievances and always claiming victory.
Decertifying the election in Wisconsin wouldn't change anything practically but could energize Trump's base.
Trump's reluctance to admit defeat and always claiming he is right is detrimental to effective leadership.
Political figures emulating Trump's behavior of not admitting defeat will lead to similar failures.
The desire to appear infallible is common among authoritarians and those seeking power.
The inability to accept mistakes leads to a cycle of repeating errors, as seen during and after Trump's presidency.

Actions:

for political observers,
Hold political figures accountable for their actions (implied)
Support leaders who admit mistakes and work towards improvement (implied)
</details>
<details>
<summary>
2022-07-22: Let's talk about Day 8 of the hearings.... (<a href="https://youtube.com/watch?v=wJfsTMFeL6g">watch</a> || <a href="/videos/2022/07/22/Lets_talk_about_Day_8_of_the_hearings">transcript &amp; editable summary</a>)

Recap of day eight hearing focusing on Trump's inaction, Pence's actions, and the premeditated nature of the events.

</summary>

"Main focus on Trump's inaction during the Capitol attack."
"Pence portrayed as taking action while Trump did nothing."
"Republicans challenged by continuing to support baseless election claims."

### AI summary (High error rate! Edit errors on video page)

Recap of day eight of the hearing focusing on the case against the former president's lack of leadership and initiative.
Building a case suggesting the events were premeditated and Trump was in dereliction of duty.
More hearings coming in September with new information and sources.
Main focus on Trump's inaction during the Capitol attack.
Detailing how easy it was for Trump to address the crowd and stop the violence.
Secret Service agents attached to Pence were in fear for their lives.
Testimonies backing up Hutchinson's account of Trump's behavior in the motorcade.
Committee focusing on disproving minute details to discredit the entire hearing.
Pence portrayed as taking action while Trump did nothing.
Outtakes reveal insincerity of Trump's remarks and reluctance to condemn his supporters.
Republicans challenged by continuing to support baseless election claims.
The hearing aimed to show the premeditated nature of the events and Trump's involvement.

Actions:

for committee members, concerned citizens,
Reach out to Department of Justice to support the investigation (implied)
Be willing to talk to the committee if you have relevant information (implied)
</details>
<details>
<summary>
2022-07-21: Let's talk about the memo, Garland, and Trump.... (<a href="https://youtube.com/watch?v=pPDVnHT-vv8">watch</a> || <a href="/videos/2022/07/21/Lets_talk_about_the_memo_Garland_and_Trump">transcript &amp; editable summary</a>)

Beau explains a DOJ memo, Garland's response on investigations into politically sensitive figures, and implications of the wide-ranging Trump investigation.

</summary>

"No one is above the law."
"This is the most wide-ranging and most important investigation in DOJ's history."
"An investigation into Trump is part of what happened at the Capitol that day."
"Garland has made it clear that in this investigation they're going to go wherever the facts lead them."
"But, like a whole lot of this, we just have to wait and see."

### AI summary (High error rate! Edit errors on video page)

Explains a Department of Justice memo requiring approval for investigations into politically sensitive figures like presidential candidates to maintain DOJ's apolitical nature.
Mentions that similar memos have existed for years and the purpose is to prevent the perception of partisan activity within the DOJ.
Emphasizes Merrick Garland's response that nobody is above the law when asked about investigations, including former President Trump.
Garland's decision to use a method starting from small investigations to big rather than a hub and spoke method is discussed, showing his commitment to conducting investigations privately and without leaks.
Describes Garland's belief that the investigation into Trump is significant, relating it to the events at the Capitol on January 6th.
Views the investigation as a wide-ranging conspiracy, potentially alarming certain individuals in Congress or former government positions.
Acknowledges Garland's commitment to DOJ's apolitical stance but notes the influence politics can have on investigations.
Speculates on the possibility of the investigation turning into a criminal matter against Trump based on Garland's statements about following the facts.
Raises concerns about individuals wanting to protect the institution of the presidency possibly interfering with the investigation's integrity.
Concludes that despite political posturing, Garland's commitment to following the facts offers hope for a thorough investigation.

Actions:

for legal analysts, concerned citizens,
Contact legal experts for insights on DOJ procedures and investigations (suggested)
Stay informed about updates on the investigation and its implications (exemplified)
</details>
<details>
<summary>
2022-07-21: Let's talk about accelerating and a better use of your time.... (<a href="https://youtube.com/watch?v=3Qg-JA5TZsA">watch</a> || <a href="/videos/2022/07/21/Lets_talk_about_accelerating_and_a_better_use_of_your_time">transcript &amp; editable summary</a>)

Beau questions the consequences of burning down systems, urging for local empowerment to help communities in crisis.

</summary>

"Your anger is a gift. Use it to help rather than hurt."
"Start building power at the local level."
"You can help people now. You don't have to wait."
"You need a bunch of 55-gallon drums, fine sand, coarse sand, and gravel."
"If you really care about those people who are being hurt by the system that exists today, you don't want to make it worse for them."

### AI summary (High error rate! Edit errors on video page)

Critiques the idea of accelerating change by burning down the system during election periods.
Questions the impact of insurgency on civilians, especially the most vulnerable.
Describes the ripple effects of disrupting infrastructure, like stopping truck deliveries.
Details the consequences of a breakdown in systems, such as running out of food, fuel, electricity, and clean water.
Challenges accelerationists to provide solutions for critical needs like medical infrastructure, clean water, and food.
Emphasizes the importance of building local networks and power structures to help communities in crisis.

Actions:

for community organizers,
Build local networks to support communities (suggested)
Prepare emergency supplies like drums, sand, and gravel (suggested)
</details>
<details>
<summary>
2022-07-21: Let's talk about 3 factions of republicans, Trump, Pence, and AZ.... (<a href="https://youtube.com/watch?v=0YmvgBYNqI8">watch</a> || <a href="/videos/2022/07/21/Lets_talk_about_3_factions_of_republicans_Trump_Pence_and_AZ">transcript &amp; editable summary</a>)

The Republican Party is at a crossroads, divided into factions over Trump's influence, facing a choice of pushing back or being steamrolled.

</summary>

"The only way for the Republican Party to move forward is to drop the Trumpist elements."
"Underestimating Trump is what happened in 2016, right?"
"If they were to do it now, they might be back in shape for 2024."
"Trump is losing support, but he's not gone."
"He's going to be dealing with Trump and Trump wannabes for a very very long time."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is divided into three factions with different views on the party's future.
The party's unity has been its strength, unlike the Democratic Party, which is a coalition.
The three factions are the Sedition Caucus (Trump supporters), the never Trumpers, and those who believe they can ignore Trump.
Pence is seen as part of the faction that believes they can ignore Trump by endorsing candidates opposing Trump's choices.
Trump plans to announce his candidacy to potentially slow down his legal troubles.
The never Trumpers believe the party can only move forward by dropping Trumpist elements.
If the party doesn't make a push to distance itself from Trump during the midterms or by 2024, they will face consequences during the elections.
Pence's endorsements against Trump's candidates are an attempt to move forward without confronting Trump directly.
Trump may still hold influence over the party and his supporters for a long time.
The Republican Party faces a choice of pushing back against Trump or being steamrolled by him.

Actions:

for republican party members,
Support and advocate for the faction within the Republican Party that aims to distance itself from Trump's influence (suggested).
Engage in internal party dialogues and actions to address the divisions caused by differing views on Trump's role within the party (implied).
</details>
<details>
<summary>
2022-07-20: Let's talk about what Greek statues can teach us about American history.... (<a href="https://youtube.com/watch?v=0BwW7H1-Ya0">watch</a> || <a href="/videos/2022/07/20/Lets_talk_about_what_Greek_statues_can_teach_us_about_American_history">transcript &amp; editable summary</a>)

Beau addresses misconceptions about history, urging for a critical examination of American heroes and the country's past to fulfill its founding promises.

</summary>

"President Lincoln was not a conservative. He was a progressive who got fan mail from Karl Marx."
"It's not new history. It's not revisionist history. It's just history."
"If you're ever reading a book and you walk away from it thinking, yay, everything's great, you're not reading history."

### AI summary (High error rate! Edit errors on video page)

Addresses the Republican Party's stance on public education in response to a viewer's feedback.
Talks about the importance of revisiting history, specifically mentioning Greek statues and their original coloring.
Disputes the idea that new historical information always attacks conservative heroes.
Mentions how information that challenges the existing narrative often gets overlooked until it enters public debate.
Emphasizes the need to confront uncomfortable aspects of American history to fulfill the promises of the founding documents.
Urges for a critical examination of history and a willingness to acknowledge the country's past shortcomings.
Warns against viewing history through a rose-colored lens and advocates for facing the uncomfortable truths it reveals.

Actions:

for history enthusiasts, educators, students,
Question existing historical narratives and seek out diverse perspectives (implied)
Encourage critical thinking about historical figures and events in educational settings (implied)
</details>
<details>
<summary>
2022-07-20: Let's talk about the probably not last hearing.... (<a href="https://youtube.com/watch?v=aZPB7wp-WpE">watch</a> || <a href="/videos/2022/07/20/Lets_talk_about_the_probably_not_last_hearing">transcript &amp; editable summary</a>)

Beau speculates on the upcoming hearing, questioning the committee's exclusion of certain information and hinting at potential future revelations.

</summary>

"Trump didn't act to end it."
"He didn't attempt to stop it because he wanted it to happen."
"There's information I feel like should have been brought in."

### AI summary (High error rate! Edit errors on video page)

Previewing the upcoming hearing, suggesting it will showcase previously known details about Trump's actions not in line with his constitutional duty.
Speculating on whether the committee's decision to exclude publicly known information was bad planning or strategic politics.
Predicting that the hearing will focus on showcasing Trump's inaction and lack of efforts to stop certain events.
Anticipating potential future hearings to make broader allegations and connect the missing pieces.
Expressing skepticism about this being the final hearing and expecting more to come.
Noting that the upcoming hearing might seem repetitive to those who have followed previous ones, with live testimony from insiders in the Trump White House.
Hoping for new information to be revealed, while also expecting a rerun with emphasis on Trump's inaction.
Mentioning significant pieces of information left out from previous hearings, hinting at a planned follow-up to complete the narrative.
Concluding with uncertainty about the ending of the hearing and the possibility of missing information being addressed in the future.

Actions:

for political analysts,
Stay updated on the developments from the hearings (implied)
Analyze the information presented critically (implied)
</details>
<details>
<summary>
2022-07-20: Let's talk about privacy and your cell phone.... (<a href="https://youtube.com/watch?v=b3xN_EP-iQ8">watch</a> || <a href="/videos/2022/07/20/Lets_talk_about_privacy_and_your_cell_phone">transcript &amp; editable summary</a>)

The ACLU investigates government agencies buying bulk data to track individuals' movements, prompting bipartisan support for the Fourth Amendment is Not for Sale Act amid concerns over privacy rights.

</summary>

"The ACLU is obviously fighting this."
"At current, it seems like the best way to curtail this practice is the Fourth Amendment is not for sale act."

### AI summary (High error rate! Edit errors on video page)

The ACLU is investigating government agencies buying bulk data from cell phones to track people's movements and develop a "pattern of life."
The Supreme Court previously ruled in Carpenter v. U.S. that authorization is needed for such surveillance, but this current practice involves gathering information from over 250 million phones daily.
The ACLU discovered more than 100,000 location markers in a three-day period in just one region of the U.S.
There's bipartisan support for the Fourth Amendment is Not for Sale Act, which aims to prevent government agencies from bypassing the Fourth Amendment by purchasing information from third parties.
Beau questions how the current Supreme Court, with some members not strongly supporting privacy rights, will respond to this issue.
He warns that those cheering for reduced privacy protections may eventually have their own privacy rights affected.
Beau believes that supporting the Fourth Amendment is Not for Sale Act is the most direct way to combat this surveillance practice.
However, he acknowledges that the act's support might be impacted by other pressing political issues.
The information gathered from cell phones can reveal detailed aspects of individuals' lives, such as their locations and social circles.
Government agencies currently do not need a warrant to access this data; they can simply purchase it from third parties.

Actions:

for privacy advocates, concerned citizens,
Support the Fourth Amendment is Not for Sale Act (suggested)
Stay informed about privacy rights issues and advocate for transparency (implied)
</details>
<details>
<summary>
2022-07-19: Let's talk about a phone number you need in the US.... (<a href="https://youtube.com/watch?v=_CskN0vLigc">watch</a> || <a href="/videos/2022/07/19/Lets_talk_about_a_phone_number_you_need_in_the_US">transcript &amp; editable summary</a>)

Beau introduces 988 as the mental health equivalent of 911, heralding a positive shift towards professional mental health support in the United States.

</summary>

"The number is 988. And it will be 911, but for mental health."
"The idea is to eventually also have what amounts to like the little county clinics that exist, but for mental health."

### AI summary (High error rate! Edit errors on video page)

Introduces the new number 988 as a mental health emergency hotline in the United States.
Compares 988 to 911 but specifically for mental health crises.
Mentions that currently, 988 connects callers to counselors, initially focused on suicide prevention.
Describes future plans for 988, including mobile care units to provide mental health support.
Advocates for shifting responsibilities away from law enforcement towards trained professionals in mental health.
States that the United States government is investing a significant amount of money, around a quarter billion dollars, into developing 988 services.
Envisions establishing physical locations similar to county clinics dedicated to mental health care.
Expresses hope in this initiative as a positive step for a country with a history of neglecting mental health.
Emphasizes the importance of remembering the number 988 for future use during mental health emergencies.
Indicates that state legislation may be introduced to fund the 988 services, akin to how 911 is funded.

Actions:

for policy makers, mental health advocates,
Support state legislation designed to fund aspects of the 988 mental health emergency hotline (suggested).
Memorize and spread awareness about the number 988 for mental health emergencies (implied).
</details>
<details>
<summary>
2022-07-19: Let's talk about Trump losing Idaho.... (<a href="https://youtube.com/watch?v=dbSnIJ-bydw">watch</a> || <a href="/videos/2022/07/19/Lets_talk_about_Trump_losing_Idaho">transcript &amp; editable summary</a>)

The Republican Party in Idaho adopts extreme planks but surprises by not backing Trump's election claims, delivering a significant blow to his support.

</summary>

"They're willing to adopt these just wild party planks, and that's fine. It's their party."
"That is really bad news for Trump."
"For a state platform that was willing to be seen as incredibly extreme to not adopt that language, that is a slap in the face."
"I imagine there will be some Idaho potatoes and ketchup on the wall when he hears the news."
"Even with all of the extreme language, they're not backing Trump anymore."

### AI summary (High error rate! Edit errors on video page)

The Republican Party in Idaho recently set up their party platform during their convention, adopting extreme and unique positions.
They decided not to include rejection of the 2020 election results, unlike some other states that claimed Trump won and Biden is illegitimate.
Idaho's platform included extreme planks like no exemptions for the life of the mother and suggesting the repeal of the 16th Amendment.
This move by Idaho Republicans, known for being one of the most dependent states on federal support, is seen as surprising.
Despite adopting extreme positions, Idaho Republicans didn't go as far as embracing Trump's baseless claims about the election.
Idaho's decision not to adopt the rejection of the 2020 election results is a significant blow to Trump's attempts to rally support.
The rejection of Trump's claims didn't even make it out of committee during the Idaho convention.
This move from Idaho Republicans, a traditionally safe area for the party, is a clear message that they are not willing to be swayed by Trump's narrative.
The unique development in Idaho's platform, despite its extreme elements, shows a departure from supporting Trump.
The rejection of Trump's claims by Idaho Republicans is a notable and unexpected turn of events within the party.

Actions:

for political observers,
Contact Idaho Republican Party officials to express support for their decision not to adopt rejection of the 2020 election results (suggested).
Attend local political events or meetings to stay informed about party platforms and decisions (exemplified).
</details>
<details>
<summary>
2022-07-19: Let's talk about Ted Cruz and marriage... (<a href="https://youtube.com/watch?v=0cBJoax1tq8">watch</a> || <a href="/videos/2022/07/19/Lets_talk_about_Ted_Cruz_and_marriage">transcript &amp; editable summary</a>)

Ted Cruz criticizes the court's decision on gay marriage, signaling a dangerous attack on LGBTQ rights and freedom by the Republican Party.

</summary>

"Don't mistake this for empty rhetoric."
"They mean what they say."
"The LGBTQ community is the current target."
"Actions against minority rights are not just empty promises."
"They're telling you who they are."

### AI summary (High error rate! Edit errors on video page)

Ted Cruz criticized the court's decision on gay marriage, advocating for states' rights.
Numerous bills targeting the LGBTQ community have been introduced this year.
The Republican Party uses scapegoating as a political strategy.
The LGBTQ community is the current target for attacks on rights.
Politicians often follow through on curtailing freedom to motivate their base.
The GOP aims to secure the bigot vote by targeting marginalized groups.
Actions against minority rights are not just empty promises.
The push to limit rights is a consistent tactic to maintain power.
Beau warns against underestimating the seriousness of these threats.
The intention behind targeting rights is to create division and maintain hierarchies.

Actions:

for advocates for lgbtq rights,
Stand with and support LGBTQ community members (implied)
Advocate against discriminatory legislation (implied)
</details>
<details>
<summary>
2022-07-18: Let's talk about systemic failures in Uvalde.... (<a href="https://youtube.com/watch?v=rqmPXdWls2E">watch</a> || <a href="/videos/2022/07/18/Lets_talk_about_systemic_failures_in_Uvalde">transcript &amp; editable summary</a>)

Beau sheds light on systemic failures in law enforcement, pointing out the hindrance to accountability and the need for legislation to ensure immediate protection of children in schools.

</summary>

"Without accountability, there's no change."
"This report and the way it defuses responsibility means that nothing is going to change."
"I think that might be a good little litmus test."
"Most of them will do everything they can to stop that statute from existing."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Commentary on report from Texas on systemic failures.
Report deflects responsibility and hinders accountability.
Mention of historical rulings impacting law enforcement's duty to protect the public.
Link between systemic failures, cultural failures, and individual failures.
Criticism of law enforcement's shift from "protecting and serving" to mere law enforcement.
Describes the failure of 376 officers to handle a situation involving one armed individual in a school.
Comparison made to a successful military operation involving far fewer personnel.
Emphasizes the need for legislation to enforce immediate protection obligations.
Suggests pushing for legislation as a litmus test for law enforcement's priorities and accountability.

Actions:

for law enforcement reform advocates,
Advocate for legislation enforcing immediate protection obligations (suggested)
Push for laws holding law enforcement accountable for protecting children in schools (suggested)
</details>
<details>
<summary>
2022-07-18: Let's talk about reading people you don't agree with.... (<a href="https://youtube.com/watch?v=bgvrdaYJbf0">watch</a> || <a href="/videos/2022/07/18/Lets_talk_about_reading_people_you_don_t_agree_with">transcript &amp; editable summary</a>)

Beau encourages intellectual growth through engaging with diverse viewpoints and challenging one's beliefs to evolve and pursue knowledge and truth.

</summary>

"The way to have an educated populace or the way to have an exercised mind is to read the opinions of those that you don't necessarily agree with."
"Your ideas should never 100% reflect anybody else's."
"The only way you're going to find out they're wrong or they're right is if you're talking to other people who disagree."
"If you only read things that you agree with 100%, you will never grow as a person."
"That is how we evolve."

### AI summary (High error rate! Edit errors on video page)

Encourages exercising the mind by stepping outside your comfort zone and reading works of people you don't agree with.
Notes the importance of being able to entertain an idea without fully accepting it as a sign of a well-educated mind.
Shares a message he received about recommending Emma Goldman, a philosopher he doesn't agree with entirely.
Explains the value of exposing oneself to new ideas and perspectives, even if they conflict with your own beliefs.
Talks about Emma Goldman's revolutionary mindset and the reasons behind her opposition to women's suffrage.
Mentions that reading and engaging with diverse viewpoints is necessary for personal growth and intellectual development.
Emphasizes the importance of challenging and examining ideas that may not line up with your own beliefs.
Suggests that true intellectual growth comes from engaging with differing opinions and testing them against practical contexts.
Advocates for thinking beyond conventional boundaries and limitations when forming and evaluating ideas.
Concludes by underlining the significance of engaging with diverse perspectives to evolve and pursue knowledge and truth.

Actions:

for intellectuals, students, educators,
Read works of authors and philosophers you may not necessarily agree with (exemplified)
Engage in respectful debates and dialogues with individuals holding different perspectives (exemplified)
Challenge your own beliefs by considering opposing viewpoints (exemplified)
</details>
<details>
<summary>
2022-07-18: Let's talk about Republicans and public education.... (<a href="https://youtube.com/watch?v=C4p6wFaPquk">watch</a> || <a href="/videos/2022/07/18/Lets_talk_about_Republicans_and_public_education">transcript &amp; editable summary</a>)

Trump's ex-Secretary's push to abolish education prompts scrutiny on GOP's anti-education stance; they thrive on an uneducated base to avoid accountability for harmful policies and discourage critical thinking.

</summary>

"They love the uneducated, and they told you that."
"Republican Party relies on an uneducated populace to avoid defending its historical positions."
"They have to embrace conspiratorial thinking."
"The party discourages critical thinking and history education."
"Republicans prefer obedient, uneducated followers."

### AI summary (High error rate! Edit errors on video page)

Trump's former Secretary of Education suggested abolishing the Department of Education, leading to questions about the Republican Party's stance on education.
Republican Party relies on an uneducated populace to avoid defending its historical positions.
Areas leaning Democratic tend to have longer lifespans due to better healthcare policies, contrasting with Republican states.
Lack of education allows the Republican Party to avoid answering for policies that harm their base, such as healthcare.
Republican voters often support policies that may harm their own future, like environmental issues, due to lack of critical thinking.
The party discourages critical thinking and history education to maintain control over their base.
Republicans prefer obedient, uneducated followers over an educated populace that questions policies.
The party thrives on conspiratorial thinking and convincing their base to ignore facts.
Republicans aim to keep their base in line and discourage independent thinking through anti-education stances.
The Republican Party's position on education is driven by a desire for compliant followers rather than an informed electorate.

Actions:

for voters, educators, activists,
Challenge anti-education policies and advocate for critical thinking in schools (exemplified)
Support educational initiatives that encourage questioning and independent thinking (exemplified)
Engage in community education efforts to combat misinformation and encourage informed decision-making (exemplified)
</details>
<details>
<summary>
2022-07-17: Let's talk about the constitution and the kids suing Montana.... (<a href="https://youtube.com/watch?v=J6IyWBz3MDk">watch</a> || <a href="/videos/2022/07/17/Lets_talk_about_the_constitution_and_the_kids_suing_Montana">transcript &amp; editable summary</a>)

Youth in Montana challenge state support for fossil fuels, invoking constitutional duty to protect the environment; Held v. State of Montana faces resistance but moves forward, potentially setting a precedent for environmental cases nationwide.

</summary>

"The kids are all right."
"They do not have a constitutional right. That's not there."
"The name of the case, if you're curious, is Held v. State of Montana."

### AI summary (High error rate! Edit errors on video page)

Youth in Montana have launched a lawsuit against the state, claiming that the state's support for fossil fuels is contributing to environmental degradation and climate change.
The kids argue that the state of Montana has a duty to maintain and improve a clean environment for present and future generations, as stated in the Montana State Constitution.
The case, known as Held v. State of Montana, has faced resistance from the state but has been allowed to proceed by the Montana Supreme Court.
The trial date for the case is set for early February 2023, with potential delays expected due to the case's significance.
Success in this case could have a widespread impact, potentially influencing similar cases in Hawaii, Virginia, and Utah.
Beau expresses admiration for the youth involved in the lawsuit and is eager to see how the case unfolds.
The lawsuit represents a significant effort by young individuals to hold the state accountable for environmental protection and sustainability.

Actions:

for youth, environmental activists, legal advocates,
Support youth-led environmental initiatives in your community (exemplified)
Stay informed about environmental lawsuits and their potential impact (exemplified)
Advocate for sustainable practices and policies in your area (exemplified)
</details>
<details>
<summary>
2022-07-17: Let's talk about Trump, Pence, loss, and gain.... (<a href="https://youtube.com/watch?v=Tg4NRTZF3WA">watch</a> || <a href="/videos/2022/07/17/Lets_talk_about_Trump_Pence_loss_and_gain">transcript &amp; editable summary</a>)

Beau speculates on the potential testimonies of Trump and Pence before a committee, suggesting Trump has everything to lose while Pence has everything to gain, based on their actions and motivations.

</summary>

"He has everything to lose. He has everything to lose by doing this."
"Pence has everything to gain."
"This is a person that it doesn't matter how you feel about him politically or even as a human being, okay?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Speculates on Trump and Pence testifying before a committee.
Questions Trump's courage to testify and suggests he may not listen to good advice.
Considers two potential outcomes if Trump does testify: vague, unhelpful answers or potential perjury.
Suggests allowing Trump to testify only if his mic can be cut to prevent perjury.
Examines the risks for Trump in testifying and how he currently benefits from standing apart and denying allegations.
Contrasts Trump's situation with Pence, who stands to gain credibility and respect by testifying truthfully.
Believes Pence's faith may motivate him to testify truthfully.
Foresees Pence's testimony elevating his stature within the Republican Party.
Suggests a different approach for getting Pence to testify, starting with a casual, behind-closed-doors interaction.
Contemplates the motivations behind the committee's consideration of Trump and Pence testimony.

Actions:

for committee members,
Approach Pence for a casual, behind-closed-doors interaction to begin discussing his testimony (implied).
Contemplate the motivations behind the committee's consideration of Trump and Pence testimony (implied).
</details>
<details>
<summary>
2022-07-17: Let's talk about Biden's defense budget.... (<a href="https://youtube.com/watch?v=EYIMfuc2JGk">watch</a> || <a href="/videos/2022/07/17/Lets_talk_about_Biden_s_defense_budget">transcript &amp; editable summary</a>)

The House proposed a $839 billion defense budget, $37 billion more than Biden's request, revealing Congress's power over budget decisions and potential industry influences.

</summary>

"Congress has the power of the purse."
"The Biden administration did not ask for this."
"The defense budget, like anything else, goes up each year because of inflation."
"That is power reserved to Congress."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The House released a defense budget blueprint of $839 billion, prompting questions.
The Biden administration did not ask for such a bloated budget; it's $37 billion more than requested.
The House rejected the administration's plans for force reduction.
Congress holds the power of the purse, as outlined in the US Constitution.
The defense budget often increases due to inflation and contributions from the defense industry.
The Senate has yet to finalize its plan, which will eventually be sent to Biden for approval.
This situation showcases the checks and balances system in government.
The Senate's decision on the budget is still pending.
The influence of lobbyists on the final budget decision remains uncertain.
Congress ultimately determines the budget, not the administration.

Actions:

for budget-conscious citizens,
Monitor and advocate for responsible defense budget allocations (implied).
Stay informed about campaign contributions to representatives, especially from the defense industry (implied).
</details>
<details>
<summary>
2022-07-16: Let's talk about the people who believed Trump.... (<a href="https://youtube.com/watch?v=cIA_QpCoems">watch</a> || <a href="/videos/2022/07/16/Lets_talk_about_the_people_who_believed_Trump">transcript &amp; editable summary</a>)

Someone fell into an information silo, woke up to Trump's lies, and now seeks help to guide others out of the echo chamber.

</summary>

"You fell into an information silo."
"If you are prone to following rabbit holes, you may not be great at getting out of them."
"Use unbiased places. Use AP. Fact-check for them."
"You can shout down into that silo and help other people get out."
"You bear some of the blame for that. But those people who profited from it, they bear probably more of the blame."

### AI summary (High error rate! Edit errors on video page)

Talks about how someone fell for Trump's lies and the realization that came later.
Explains the dynamics of falling into an information silo and how it affects beliefs.
Mentions the smugness from liberals when discussing being conned by Trump supporters.
Advises on staying out of information silos and providing fact-checking assistance.
Urges individuals to help others escape information silos and be wary of getting pulled back in.
Acknowledges the warning signs about Trump's actions before the election.
Emphasizes the importance of helping others who may still be trapped in misinformation.

Actions:

for trump supporters and those seeking to understand how individuals fall into information silos.,
Fact-check for others (suggested).
Provide unbiased information (implied).
Help others escape information silos (exemplified).
</details>
<details>
<summary>
2022-07-16: Let's talk about how the Democratic party can play hard ball.... (<a href="https://youtube.com/watch?v=K13NLVX5uIQ">watch</a> || <a href="/videos/2022/07/16/Lets_talk_about_how_the_Democratic_party_can_play_hard_ball">transcript &amp; editable summary</a>)

Beau suggests disrupting Trump's event by questioning his credibility in a surprise hearing to unsettle him before his speech, urging the Democratic Party to strategically fight to win.

</summary>

"Trump loves to go off script."
"Engage in some shenanigans of your own for a change."
"The credibility of Trump, the mindset of Trump in relation to the people around him at that time, that's materially important to the committee's work."

### AI summary (High error rate! Edit errors on video page)

Explains the idea of the Democratic Party playing hardball after receiving a message post a video on Trump's speech in DC.
Suggests disrupting Trump's event by announcing a surprise hearing focused on questioning the credibility of the president the next morning after his speech.
Points out that calling Trump's credibility into question could get under his skin and impact his big speech negatively.
Mentions that Trump loves to go off script and predicts that if a committee disrupts his event, he may deviate from his planned speech.
Urges the Democratic Party to use tactics that criticize Trump before his speech to throw him off balance and make him focus on criticism, rather than his vision for 2024.
Addresses potential concerns about politicizing hearings and argues that critics of the process will never be satisfied regardless of how it is conducted.
Emphasizes the importance of examining Trump's credibility and mindset in relation to those around him at specific times for the committee's work.
Encourages the Democratic Party to take action and fight strategically to win politically.

Actions:

for politically active individuals,
Disrupt events strategically to unsettle opponents before key moments (exemplified)
Focus on questioning credibility strategically to impact opponent's focus (exemplified)
Engage in tactics to throw opponents off balance before significant speeches (exemplified)
</details>
<details>
<summary>
2022-07-16: Let's talk about Republican judges disagreeing with Trump... (<a href="https://youtube.com/watch?v=lvFfxqkheq0">watch</a> || <a href="/videos/2022/07/16/Lets_talk_about_Republican_judges_disagreeing_with_Trump">transcript &amp; editable summary</a>)

Trump's baseless fraud claims debunked in a report titled Lost, Not Stolen, challenging Republicans to reject falsehoods and lead.

</summary>

"Lost, Not Stolen."
"Trump's lying to what they're saying."
"It's the time for the Republican Party to do something that, well, it just hates to do, lead."

### AI summary (High error rate! Edit errors on video page)

Trump is trying to make the midterms about himself by pushing baseless claims of fraud.
Conservative judges and lawyers released a report titled Lost, Not Stolen, debunking Trump's fraud claims from the 2020 election.
The report found no evidence of fraud on a scale that could change any election results.
They urge fellow conservatives to move on, advance candidates focused on peace and prosperity, and reject Trump's false claims.
The report aims to salvage the Republican Party by exposing Trump's lies and baseless allegations.
Without the myth of fraud, Trump's chances are slim, posing a problem for him but offering a solution for the Republican Party.
Republicans may need to reject Trump to deal with the problem he poses to the party.
Apologies from politicians who once supported Trump's claims could be accepted by the Republican base.
Many Republicans fell for Trump's claims, so admitting to being wrong may not be a career-ending move.
It's time for the Republican Party to step up and lead by rejecting Trump's falsehoods.

Actions:

for conservatives, republicans,
Share the report Lost, Not Stolen with fellow conservatives to challenge false claims (exemplified)
Encourage politicians to issue apologies for supporting baseless claims (exemplified)
Support candidates focused on peace and prosperity rather than division (implied)
</details>
<details>
<summary>
2022-07-15: Let's talk about the Secret Service deleting texts.... (<a href="https://youtube.com/watch?v=g3s_7mKjC0U">watch</a> || <a href="/videos/2022/07/15/Lets_talk_about_the_Secret_Service_deleting_texts">transcript &amp; editable summary</a>)

The Secret Service and the Office of the Inspector General clash over deleted texts, raising concerns of intentional record destruction and criminal conspiracy.

</summary>

"You deleted what?"
"The intentional destruction of records to impede, that's huge."
"We're going to have to wait to see how this plays out."
"This is definitely a very different ball game when it comes to acceptable behavior."
"If these allegations are true, this is going to be a game changer."

### AI summary (High error rate! Edit errors on video page)

The Secret Service and the Office of the Inspector General from DHS are at odds over the deletion of texts related to the events of January 6th.
The Office of the Inspector General is meticulous and methodical, so their claims should be taken seriously.
The Secret Service denies the allegations, attributing the missing texts to a migration process.
Beau expresses skepticism towards the Secret Service's explanation and leans towards believing the Office of the Inspector General.
If true, the intentional destruction of records by the Secret Service could have significant implications, including criminal conspiracy and cover-up.
Beau distinguishes between ethical considerations in protecting a client's dignity and the serious nature of destroying government records.
The issue goes beyond individual agents; it suggests a systemic problem within the agency.
The situation is still unfolding, with accusations and denials but no concrete evidence yet.
Beau underscores that this incident, if proven, will be a significant departure from acceptable behavior within the agency.
The potential implications of these allegations could be a game changer.

Actions:

for watchdog agencies, concerned citizens.,
Monitor developments and hold accountable those responsible (implied).
Support transparency and accountability within government agencies (implied).
</details>
<details>
<summary>
2022-07-15: Let's talk about benefits and Republicans telling you who they are.... (<a href="https://youtube.com/watch?v=P8HRlEPK7SQ">watch</a> || <a href="/videos/2022/07/15/Lets_talk_about_benefits_and_Republicans_telling_you_who_they_are">transcript &amp; editable summary</a>)

Addressing the alarming belief that child pregnancy can be a benefit, revealing the need for political accountability and awareness of the potential consequences.

</summary>

"10-year-old moms, that's what they're saying. It's not regrettable. It's a benefit."
"Roe is on the ballot, in fact. But they're not presenting that."
"If they believe it is an ultimate benefit to have 10-year-old moms, that needs to be a question in every debate."

### AI summary (High error rate! Edit errors on video page)

Addressing the claimed benefits and perceived benefits of a situation involving a 10-year-old child.
Jim Bopp, lead lawyer for the National Right to Life Group, expressed his opinion on legislation impacting children.
Bopp's legislation lacked exceptions, meaning a 10-year-old could be forced to have a child.
Bopp suggested that forcing a 10-year-old to have a child could have benefits, like saving money on childcare.
Beau criticizes the idea of forcing a child to have a baby and questions the motives behind such legislation.
The statement by Bopp reveals a worldview where child pregnancy is seen as a benefit, not a regrettable event.
Beau calls out the Democratic Party for not effectively communicating the implications of overturning Roe v. Wade.
Urging for more awareness about the potential consequences of legislation that normalizes child pregnancy.
Beau stresses the importance of holding politicians accountable for their support of such legislation.
Emphasizing the need for widespread attention to Bopp's statement and questioning politicians about their stance on such beliefs.

Actions:

for politically engaged individuals,
Question politicians about their stance on legislation impacting children (suggested)
Raise awareness about the implications of normalizing child pregnancy (implied)
</details>
<details>
<summary>
2022-07-15: Let's talk about Trump going to Washington.... (<a href="https://youtube.com/watch?v=iK-t9m38ZZo">watch</a> || <a href="/videos/2022/07/15/Lets_talk_about_Trump_going_to_Washington">transcript &amp; editable summary</a>)

Trump's return to D.C. for a supposed policy speech sparks concerns for the Republican Party as he sets the stage for his campaign and potentially navigates legal challenges, creating a political dilemma akin to "Frankenstein's monster."

</summary>

"He's a private citizen. He doesn't direct policy."
"Trump is a Republican problem now. Frankenstein's monster."
"This might be time for the Democratic Party to start playing hardball."

### AI summary (High error rate! Edit errors on video page)

Trump is planning to go to Washington for a speech, billed as a major policy speech.
Trump's move is seen as the beginning of his campaign, just before the midterms.
His speech aims to turn the midterms into a referendum on his baseless claims, which is a problem for the Republican Party.
Trump's return to D.C. may be driven by his concern for polls and approval ratings.
A New York Times poll shows that 49% of Republicans back Trump in a primary run.
Trump's motivation to start campaigning might be influenced by potential legal challenges.
There's a belief that running for president could offer Trump some form of protection.
The possibility of Trump or his top people getting indicted poses a threat to the Republican Party.
The impact of a major party candidate for president being indicted could hurt everyone across the board.
This situation presents a challenge for the Republican Party, similar to dealing with Frankenstein's monster.

Actions:

for political analysts, democratic strategists,
Start playing hardball (implied)
</details>
<details>
<summary>
2022-07-14: The roads to preparedness questions.... (<a href="https://youtube.com/watch?v=obqLU7c6QbI">watch</a> || <a href="/videos/2022/07/14/The_roads_to_preparedness_questions">transcript &amp; editable summary</a>)

Be prepared for emergencies by safeguarding vital documents, understanding tasks, carrying essentials, and fostering self-reliance.

</summary>

"Knowledge weighs nothing."
"Emergency preparedness is supposed to be freeing."
"Different considerations for emergency prep arise for targeted minorities."

### AI summary (High error rate! Edit errors on video page)

People tend to overlook the importance of keeping their papers, like birth certificates and insurance documents, in an emergency.
When it comes to preparing for adrenaline and trauma, you can't really prepare but being comfortable with the tasks at hand is key.
In an everyday carry (EDC) and survival kit, essentials include food, water, fire, shelter, first aid kit, and a knife.
Refraining refrigerated medications during power outages is a challenge with limited solutions.
Beau advocates for Wilderness First Responder courses for emergency preparedness but notes their cost and intensity.
Using zombies as a metaphor can help in discussing emergency preparedness with friends.
Beau suggests having a raft in case of heavy flooding as an additional preparedness item.
Two weeks of supplies are recommended as a minimum for emergency preparedness.
Emergency preparedness should not become an overwhelming lifestyle but a freeing safety net.
Different considerations for emergency prep arise for targeted minorities due to potential biases in disaster situations.

Actions:

for emergency planners,
Compile and digitize vital documents for emergency preparedness (suggested).
Enroll in Wilderness First Responder or first aid courses for better readiness (suggested).
Initiate zombie apocalypse metaphor talks to spark emergency prep awareness among friends (suggested).
</details>
<details>
<summary>
2022-07-14: Let's talk about why the cost of beef will go up.... (<a href="https://youtube.com/watch?v=i-mK-RGaQrM">watch</a> || <a href="/videos/2022/07/14/Lets_talk_about_why_the_cost_of_beef_will_go_up">transcript &amp; editable summary</a>)

Ranchers facing drought-induced challenges are sending cattle to market early, leading to higher beef prices, showcasing the impact of climate change on ranching economics and sustainability.

</summary>

"You can expect to pay more for beef."
"It's water intensive. It's land intensive."
"You are starting to see the first glimpses of it right now."

### AI summary (High error rate! Edit errors on video page)

Ranchers are taking portions of their herd to market early due to drought affecting pasture land, particularly in areas like Texas.
Normally, when pasture land goes bad, ranchers bring in hay from outside, but fuel prices are too high to make that feasible in this large-scale disruption.
With cows going to market early and weighing less, there will be less supply with the same demand, leading to increased beef prices.
Ranchers are holding onto younger cows in hopes of pasture recovery, but if conditions don't improve, more cows will likely head to market.
The impact of climate change, with increased heat and drought, is making cattle ranching harder and more expensive.
The water and land-intensive nature of beef production is becoming more challenging with climate change effects.
The current situation indicates a glimpse of the challenges ahead in cattle ranching due to climate change.
Bringing livestock up is manageable if there is pasture for them, but it becomes prohibitively expensive if hay needs to be brought in due to widespread drought.
Beau mentions that those on plant-based diets might see high beef prices lead to a search for alternative protein sources.
The East Texas Livestock Showroom supervisor raised concerns about how high prices could go before people seek alternative proteins.

Actions:

for ranchers, consumers, policymakers,
Support local ranchers by purchasing beef directly from them (suggested).
Look for alternative protein sources to reduce reliance on beef (suggested).
</details>
<details>
<summary>
2022-07-14: Let's talk about debunking a theory about certain agencies.... (<a href="https://youtube.com/watch?v=9i5gkFvRUsg">watch</a> || <a href="/videos/2022/07/14/Lets_talk_about_debunking_a_theory_about_certain_agencies">transcript &amp; editable summary</a>)

Beau sheds light on the reasons behind government agencies purchasing weapons, revealing the extensive presence of law enforcement functions within federal bodies and dispelling concerns as more about curiosity than alarm.

</summary>

"There is a maze of police agencies, law enforcement agencies, armed agencies throughout the federal government that you've never heard of."
"It really goes to show exactly how widespread law enforcement functions are in the United States."

### AI summary (High error rate! Edit errors on video page)

Explains the reasoning behind government agencies' seemingly odd purchases of weapons, addressing concerns raised by the public.
Points out that every federal agency has an Office of the Inspector General with law enforcement functions, hence the need for weapons.
Mentions specific agencies like the IRS, Department of Education, and NOAA that have their own law enforcement divisions requiring firearms.
Provides insight into the Department of Energy's Office of Secure Transport, which transports nuclear weapons and materials across the country, justifying their high-end purchases.
Notes that agencies like Veterans Affairs also have police forces to protect hospitals.
Emphasizes that the widespread presence of law enforcement functions within federal agencies showcases the extensive reach of policing in the United States.
Clarifies that the concern over agencies purchasing weapons periodically is not a cause for alarm but rather stems from a lack of understanding about these agencies' functions.
Draws a distinction between conspiracy theories and the factual basis of government agency purchases, stating this topic falls into the latter category.
Addresses the common question of why non-traditional agencies like Department of Energy require firearms, shedding light on their unique functions.
Concludes by reassuring the audience that the situation is nothing to worry about, framing the issue as more about curiosity than alarm.

Actions:

for government accountability advocates,
Contact your local representatives to inquire about the law enforcement functions and purchases of weapons by government agencies (suggested).
</details>
<details>
<summary>
2022-07-14: Let's talk about a 10-year-old, journalism, and misinformation.... (<a href="https://youtube.com/watch?v=a-gldvbNtKg">watch</a> || <a href="/videos/2022/07/14/Lets_talk_about_a_10-year-old_journalism_and_misinformation">transcript &amp; editable summary</a>)

Beau addresses misinformation surrounding a sensitive story involving a 10-year-old and criticizes those who rush to cast doubt, impacting future victims coming forward.

</summary>

"Ego."
"If you're one of those people who couldn't wait, who couldn't just wait for things to play out, and instead cast doubt on a child, you never get to ask about why women don't come forward again."
"It's a 10-year-old child. That's the story."

### AI summary (High error rate! Edit errors on video page)

Provides an update on a recent video to address misinformation and explain why he didn't cover the continuing story that emerged.
Talks about a story involving a 10-year-old crossing state lines for a procedure due to a Supreme Court decision.
Mentions pushback and accusations of the story being a hoax coming from centrist/liberal and right-wing outlets or politicians.
Points out the arrest of the person allegedly involved in the incident as casting doubt on the hoax claims.
Explains his confidence in the initial reporting and why he didn't request a retraction.
Talks about red flags for spotting misinformation, focusing on the headline and journalistic standards.
Addresses the plausibility of the story and the lack of red flags.
Criticizes major outlets for sensationalizing the story and seeking confirmation aggressively.
Explains the reluctance of involved parties to talk to larger outlets due to the sensitive nature of the story involving a 10-year-old.
Condemns those who rushed to cast doubt on the story and the impact on future victims coming forward.

Actions:

for journalists, activists, news consumers,
Support responsible journalism by subscribing to credible news sources (exemplified)
Advocate for ethical reporting practices in media outlets (exemplified)
Stand against rushing to cast doubt on sensitive stories and victims (exemplified)
</details>
<details>
<summary>
2022-07-13: Let's talk about the institution of the Presidency and policy decisions.... (<a href="https://youtube.com/watch?v=jLUE-nHzOQk">watch</a> || <a href="/videos/2022/07/13/Lets_talk_about_the_institution_of_the_Presidency_and_policy_decisions">transcript &amp; editable summary</a>)

Beau explains the danger of preserving the institution of the presidency over upholding the Constitution and warns against allowing unchecked executive power.

</summary>

"Preserving the institution of the presidency by allowing unchecked executive power is the ultimate betrayal of the Constitution."
"The Constitution was written to protect this country from unchecked executive power."
"The institution of the presidency is worthless. It is worthless."
"If there's no accountability for that, it's not a failed coup attempt. It's practice."
"Preserving the office of the presidency at the expense of that document is a pathway to disaster."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of preserving the institution of the presidency.
Mentions the emergence of talking points as evidence links to the White House.
Questions the idea that indicting Trump is a policy decision rather than evidence-based.
Raises concerns about the unchecked executive power if former presidents are not indicted.
Warns about the danger of allowing a president to be above the law.
Emphasizes that the Constitution was written to prevent unchecked executive power.
Condemns preserving the institution of the presidency over upholding the Constitution.
Raises the potential threat of a president refusing to leave office.
Points out the risk of setting a dangerous precedent for seizing control using military support.
Describes the situation as suggesting an attempted self-coup with no accountability.
Warns that without checks, the presidency could devolve into a dictatorship.
Stresses the importance of upholding the checks and balances within the Constitution.
Urges to prioritize the Constitution over preserving the office of the presidency.
Concludes by warning against sacrificing the Constitution for the sake of the presidency.

Actions:

for advocates and defenders of constitutional integrity.,
Challenge and question policies that prioritize preserving the presidency over upholding constitutional values (implied).
Advocate for accountability and checks on executive power within the government (implied).
Support initiatives that prioritize constitutional integrity over individual office preservation (implied).
</details>
<details>
<summary>
2022-07-13: Let's talk about representation, personal growth, and a question.... (<a href="https://youtube.com/watch?v=-XssiIwthrE">watch</a> || <a href="/videos/2022/07/13/Lets_talk_about_representation_personal_growth_and_a_question">transcript &amp; editable summary</a>)

Beau addresses a critical message, praises personal growth, and encourages clearer emotional expression in a thought-provoking reflection.

</summary>

"I thought I was gonna get some nice sci-fi content. Instead, I got a bunch of woke bull."
"I won't be taking the advice of people who can't define what a woman is."
"Somewhere between the first paragraph and the second paragraph, this person, the light bulb came on."
"I'm proud of you for growing as a person in that way."
"If we could just get you to express your feelings a little bit more clearly, you might turn into a pretty decent person."

### AI summary (High error rate! Edit errors on video page)

Recounts receiving messages following an interview with Jesse that prompted varied reactions, including some negative ones.
Shares a specific message critiquing his content for pushing "trans ideology" and lacking diversity in interviews.
Responds to the criticism by addressing the importance of representation in media and points out his own representation as a white male on his channel.
Acknowledges not having interviewed a white man, but notes the presence of white males on his channel, including himself.
Emphasizes a significant moment of personal growth in a critical message where the sender transitions from questioning trans ideology to recognizing the representation of trans women.
Commends the sender for their personal growth and willingness to share it publicly.
Expresses pride in the sender's evolution and suggests they continue to work on expressing their feelings more clearly.

Actions:

for online content creators,
Share personal growth moments (exemplified).
Express emotions clearly (implied).
</details>
<details>
<summary>
2022-07-13: Let's talk about a recap of the hearing and missing the forest for the trees... (<a href="https://youtube.com/watch?v=ooUrPA4tbtk">watch</a> || <a href="/videos/2022/07/13/Lets_talk_about_a_recap_of_the_hearing_and_missing_the_forest_for_the_trees">transcript &amp; editable summary</a>)

Key takeaways from the hearing include tying Trump personally to the events of January 6th and showcasing evidence of his involvement, with implications for potential criminal charges and ongoing conspiracy.

</summary>

"Trump's rhetoric got someone killed."
"Trump's former campaign manager said that Trump's rhetoric got someone killed."
"Trump saw those tweets, that draft tweet, that meeting, everything shows that the events of the 6th can be directly tied to him as a person."

### AI summary (High error rate! Edit errors on video page)

Providing a recap of a hearing and discussing the forest-for-the-trees issue with coverage.
Key takeaways include the inciting tweet, the involvement of more Congress people, and the pre-planned Capitol march.
Noteworthy moments from the hearing include Trump's former campaign manager attributing a death to Trump's rhetoric.
The committee has shown that Trump continued to lie about the election results and attempted to legitimize baseless claims.
Today's focus was on tying Trump personally to the events of January 6th, showing his role in inciting the Capitol riot.
The evidence presented aims to directly link Trump's actions to the events of January 6th.
While this hearing isn't a courtroom, the intent is to present evidence, keeping in mind the presumption of innocence.
There will be a forthcoming video discussing the institution of the presidency and the protection of its integrity.
Mention of ongoing criminal conspiracy attempts is made, possibly showing that current actions could be linked to past events.

Actions:

for congressional members, concerned citizens.,
Contact elected representatives to express concerns about the implications of the evidence presented (implied).
</details>
<details>
<summary>
2022-07-12: Let's talk about the what fire in California can teach us.... (<a href="https://youtube.com/watch?v=Pt-bTuv1pSg">watch</a> || <a href="/videos/2022/07/12/Lets_talk_about_the_what_fire_in_California_can_teach_us">transcript &amp; editable summary</a>)

California's approach to fire prevention near the giant sequoias shows the importance of early action and preparedness, offering valuable insights for informing national policy on climate issues.

</summary>

"It's like prescribed burns, carting out stuff that might burn in the event of a fire nearby."
"If you start acting before the fires at your front door, so to speak, things go a lot smoother."
"Maybe we should look that direction and try to start before it gets really, really bad."

### AI summary (High error rate! Edit errors on video page)

California's large fire near the giant sequoias has raised concerns due to their age and historical significance.
Officials are confident in the protection of the trees, attributing it to years of preventative measures and infrastructure development.
Efforts like fuel reduction, removal, prescribed burns, and sprinkler systems have been key in safeguarding the area.
The frequency of fires due to climate issues underscores the importance of preparedness and early action.
California's approach to fire prevention and response could serve as a valuable lesson for informing national policy.
Acting proactively before crises escalate can lead to smoother outcomes and reduce risks and dramatic actions.
Beau suggests that addressing climate issues preemptively, like California did with the fires, could be beneficial on a larger scale.

Actions:

for environmental activists, policymakers, community leaders,
Develop and implement infrastructure for fire prevention and response (exemplified)
Support and fund preventative measures like fuel reduction and prescribed burns (exemplified)
Advocate for early action and preparedness in addressing climate issues (exemplified)
</details>
<details>
<summary>
2022-07-12: Let's talk about Jefferson, Monticello, and Fox.... (<a href="https://youtube.com/watch?v=A5z9HhuPny8">watch</a> || <a href="/videos/2022/07/12/Lets_talk_about_Jefferson_Monticello_and_Fox">transcript &amp; editable summary</a>)

Fox News guest criticizes Monticello for being frank about Thomas Jefferson's history of slavery, but Beau argues that addressing Jefferson's flaws is necessary for understanding history.

</summary>

"Monticello didn't go woke, as is often the case."
"Not discussing Jefferson's history is not history, but American mythology."
"They were people just like everybody else. They made mistakes."
"That's the belief that these people were infallible."
"Thomas Jefferson was aware of the injustice, too, and he would be happy to know that people today were criticizing him for it."

### AI summary (High error rate! Edit errors on video page)

Fox News guest criticized Monticello for being frank about Thomas Jefferson's history of slavery.
Jefferson acknowledged slavery as evil, but engaged in it anyway.
Jefferson believed that over time, people's attitudes towards slavery would change.
Beau argues that Jefferson was aware of his actions and foresaw criticism for them.
Not discussing Jefferson's history is not history, but American mythology.
Beau asserts that addressing the flaws of historical figures is necessary for learning and understanding history.

Actions:

for history enthusiasts,
Address the flaws and mistakes of historical figures in educational settings (implied)
</details>
<details>
<summary>
2022-07-12: Let's talk about Biden's new rule on emergency treatment.... (<a href="https://youtube.com/watch?v=CMw5fGHah9k">watch</a> || <a href="/videos/2022/07/12/Lets_talk_about_Biden_s_new_rule_on_emergency_treatment">transcript &amp; editable summary</a>)

Biden administration's guidance on treatment for mothers in danger underlines the need for clear legal guidelines to prevent delays in necessary care.

</summary>

"Biden administration's rule on treatment isn't new; federal law already dictates providers must act if mother is in danger."
"Providers may delay necessary care to avoid legal trouble, resulting in unnecessary loss of life."
"Need for clear guidelines and legal affirmation to ensure providers can act in the best interest of patients."

### AI summary (High error rate! Edit errors on video page)

Biden administration's rule on treatment isn't new; federal law already dictates providers must act if mother is in danger.
Bans on treatment since Roe v. Wade have exemptions for mother's health, limiting impact of Biden's press release.
Federal guidance allows providers to stabilize patients in danger, but state investigations may lead to delayed care.
Providers may delay necessary care to avoid legal trouble, resulting in unnecessary loss of life.
Lack of clear guidelines from federal government puts providers in a difficult position, facing potential criminal charges.
Providers may err on side of caution due to lack of clarity, impacting patient care.
Some are considering performing procedures on boats outside state waters to navigate legal restrictions.
Efforts are underway to make certain forms of birth control available over the counter, but process may take time.
Uncertainty persists due to conflicting laws, jurisdictions, and bureaucratic processes.
Need for clear guidelines and legal affirmation to ensure providers can act in the best interest of patients.

Actions:

for healthcare providers, policymakers,
Advocate for clear guidelines and legal affirmation for healthcare providers (implied)
Stay informed on developments regarding access to birth control and reproductive healthcare (implied)
</details>
<details>
<summary>
2022-07-11: Let's talk about leaving your state.... (<a href="https://youtube.com/watch?v=FxaDVhnNABY">watch</a> || <a href="/videos/2022/07/11/Lets_talk_about_leaving_your_state">transcript &amp; editable summary</a>)

Beau advises making informed decisions about moving based on safety, resistance, business, and family impacts, prioritizing individual circumstances over external opinions.

</summary>

"If you're worried about your safety, yeah, that's an easy one. If you have the ability, yeah, get out."
"Don't listen to anybody on social media, any commentator, anything like that. You do what's right for you and your family, nothing else."
"I don't think it's wrong to leave. I don't think it's wrong to stay."
"It's a huge, it's a life-altering decision."
"These states probably do not understand what they have done to themselves."

### AI summary (High error rate! Edit errors on video page)

Addressing the topic of moving due to recent legislation or rulings, or considering leaving the U.S. to return to one's home country.
Encouraging individuals to make their own decisions about moving based on their unique circumstances.
Advising that safety concerns should be a priority when deciding whether to stay or leave.
Noting that leaving a state does not necessarily mean giving up, especially if it's related to resistance.
Pointing out the importance of considering business implications when deciding whether to relocate.
Mentioning the potential economic downturn in states with controversial legislation.
Urging individuals to prioritize the opinions of those directly impacted by the situation.
Acknowledging that the decision to move is significant and life-altering.
Emphasizing that individuals should do what is best for themselves and their families, disregarding external opinions.
Suggesting that the impact of these decisions may not be fully understood by the states involved in the long term.

Actions:

for individuals considering moving,
Make a safety plan for potential relocation (suggested)
Consult with family and household members directly impacted by the situation (implied)
</details>
<details>
<summary>
2022-07-11: Let's talk about Texas, ERCOT, and the future.... (<a href="https://youtube.com/watch?v=scoOftuZKFo">watch</a> || <a href="/videos/2022/07/11/Lets_talk_about_Texas_ERCOT_and_the_future">transcript &amp; editable summary</a>)

Texans urged to conserve energy to avoid interruptions, as outdated infrastructure and political choices exacerbate the crisis.

</summary>

"The future is in your hands. These policies, they're causing this."
"Climate change is here. The future is today."
"Texas bears a lot of the responsibility because it's the energy sector in Texas, that old money, that is responsible for this."
"Those refugees, those people seeking asylum at the border, understand it's going to start to your west."
"The good people of Texas, well, we've got them. They're tricked."

### AI summary (High error rate! Edit errors on video page)

Texans are asked to conserve energy from 2 p.m. to 8 p.m. to avoid service interruptions.
Even if individuals conserve, interruptions may occur due to collective efforts.
High electricity usage to combat heat is causing the current issues in Texas.
Republican energy policies at state and national levels contribute to the problem.
Lack of investment in clean energy and infrastructure maintenance exacerbates the situation.
Political resistance to clean energy research and technology development worsens the crisis.
Current extreme temperatures can have fatal consequences.
Texas' outdated energy sector and profit-driven motives are major factors in the crisis.
Climate change impacts will lead to more states facing similar issues.
Residents are being misled by politicians prioritizing profit over environmental concerns.

Actions:

for texans, environmentalists,
Conserve energy during peak hours to support grid stability (exemplified)
Advocate for clean energy policies and infrastructure updates (exemplified)
Educate community members on the impacts of climate change (exemplified)
</details>
<details>
<summary>
2022-07-11: Let's talk about Day 7 of the hearings, and maybe day 8.... (<a href="https://youtube.com/watch?v=Sl6DqsLkbGo">watch</a> || <a href="/videos/2022/07/11/Lets_talk_about_Day_7_of_the_hearings_and_maybe_day_8">transcript &amp; editable summary</a>)

Beau speculates on upcoming hearings, discussing potential topics like White House connections to extremists and warning against witnesses shaping the format.

</summary>

"This is not going well for Trump."
"The only real card they have to play is to try to delegitimize and disrupt the hearings."
"It doesn't seem like putting them on TV live is a good idea."
"There's only so much they can say and still mount a defense."
"It's time to start tying them together with testimony that may even be more dramatic than what we've heard thus far."

### AI summary (High error rate! Edit errors on video page)

Speculation about hearings on day seven and possibly day eight, including a potential surprise hearing.
Topics to be discussed include White House connections to extremists, a meeting between Trump and "Team Crazy," and the possibility of seizing voting machines.
Focus on statements by Cipollone that may corroborate previous testimony and be damaging to the Trump inner circle.
Mention of testimony from someone linked to groups present on January 6th.
Allies of the former president willing to testify only if it's live, raising concerns about shaping the format of the hearings.
Not going well for Trump and his circle; their strategy may involve delegitimizing and disrupting the hearings.
Warning against allowing witnesses to dictate the outcome and format of the hearings.
Suspicions about witnesses facing serious charges and the potential impact of their testimony.
Emphasizing the need for tying together individual pieces with dramatic testimony moving forward.

Actions:

for political observers,
Wait for updates and follow the developments in the hearings (implied)
</details>
<details>
<summary>
2022-07-10: Let's talk about the most important election of your life.... (<a href="https://youtube.com/watch?v=AR3scusFA1s">watch</a> || <a href="/videos/2022/07/10/Lets_talk_about_the_most_important_election_of_your_life">transcript &amp; editable summary</a>)

Beau stresses the importance of looking beyond electoral politics for real change, while cautioning against actions that could harm vulnerable communities.

</summary>

"Real change comes from outside."
"Punishing the Democratic Party because they didn't have the votes… You're punishing the people who are in the demographics that are affected by that."
"It's not really a surprise."
"I understand the frustration."
"Think about whether or not you want it to get worse for those people who are in those demographics."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of the election and a message he received regarding electoral politics.
Mentions that every election is labeled as the most critical one, but things don't necessarily improve significantly.
Expresses his belief that real change happens outside of politics and that voting is the least effective form of civic engagement.
Shares that he has never endorsed a political candidate on his channel except for Big Bird against Ted Cruz.
Acknowledges that the world of cooperation and helping each other is far from reality currently.
Points out the impact of the Democratic Party's success in elections on various key issues like healthcare, equality, and environmental protection.
Emphasizes that punishing the Democratic Party for not achieving certain goals ultimately affects the vulnerable demographics they represent.
Urges people to think about the consequences for those who will suffer if the situation worsens politically.
Stresses the importance of considering the outcomes of elections beyond just immediate frustrations.
Encourages viewers to understand the implications of not supporting certain political efforts and the potential consequences.

Actions:

for voters and activists,
Support and strengthen community-based initiatives to address social issues (implied)
Advocate for policies that benefit marginalized groups in society (implied)
Engage in grassroots efforts to bring about meaningful change outside of traditional politics (implied)
</details>
<details>
<summary>
2022-07-10: Let's talk about the Orville, interpretation, and Topa.... (<a href="https://youtube.com/watch?v=r80t29BtzIE">watch</a> || <a href="/videos/2022/07/10/Lets_talk_about_the_Orville_interpretation_and_Topa">transcript &amp; editable summary</a>)

Beau stresses defending positive cultural interpretations and media messages for lasting impact, especially in the face of differing views.

</summary>

"You have to have the positive message out there."
"Ideas stand and fall on their own."
"We have to make sure that when good ideas get out there, that they can stand, that they're backed up."
"No matter how silly it may seem, we have to defend the positive impact."
"It's worth your time because it has a long, long lasting impact."

### AI summary (High error rate! Edit errors on video page)

Exploring the importance of defending positive interpretations in culture and media.
Talks about the show "The Orville" and a specific episode that sparked interest.
Describes a society in the show with strict gender rules and a child wanting to transition.
Emphasizes the two different interpretations of the episode: supportive vs. right-wing view.
Points out the positive symbolism and messages in the show regarding breaking gender rules.
Stresses the need to defend and uphold positive messages in media for cultural impact.
Advocates for standing up against negative interpretations and ensuring positive ideas are supported.
Mentions the significance of creators' intentions and how ideas resonate in society over time.
Encourages defending positive impacts of media messages for long-lasting effects on society.

Actions:

for culture defenders,
Reach out to creators of media to understand their intentions (suggested).
Support positive interpretations and messages in media (implied).
Defend and uphold positive impacts of media content (implied).
</details>
<details>
<summary>
2022-07-10: Let's talk about Biden, inflation, and Lucky Charms.... (<a href="https://youtube.com/watch?v=UQFBDqGt4zo">watch</a> || <a href="/videos/2022/07/10/Lets_talk_about_Biden_inflation_and_Lucky_Charms">transcript &amp; editable summary</a>)

The Republican Party's attempt to blame Biden for inflation may unravel as key elements start to decrease, potentially impacting the midterms.

</summary>

"The Republican Party has done everything within its power to connect Biden to inflation."
"If inflation comes under control, it's going to definitely rob them of one of their talking points, one that they manufactured."

### AI summary (High error rate! Edit errors on video page)

The Republican Party has been trying to connect Biden to inflation, despite it being a global problem.
Key elements like crude oil, lumber, used cars, cereal, and shipping rates are starting to decrease.
Companies increased prices during the pandemic to recover lost profits, contributing to inflation.
If inflation eases before the midterms, it could trouble the Republican Party.
Job growth remains steady, and if inflation gets under control, it will dismantle a manufactured talking point against Biden.

Actions:

for voters, political analysts,
Monitor economic trends and stay informed (implied)
Stay updated on political narratives and their impact (implied)
</details>
<details>
<summary>
2022-07-09: Let's talk about the weather, the climate, and Yellowstone.... (<a href="https://youtube.com/watch?v=UQ0PSdFh9IE">watch</a> || <a href="/videos/2022/07/09/Lets_talk_about_the_weather_the_climate_and_Yellowstone">transcript &amp; editable summary</a>)

Be prepared for more frequent and intense extreme weather events with less warning, as climate change continues to disrupt traditional forecasting methods.

</summary>

"Extreme weather incidents are going to become more and more common."
"If you live in an area where there are extreme weather events, you might want to be prepared for them to occur more frequently and with less warning."
"It might be a good idea to start getting in the habit of keeping what you need on hand."

### AI summary (High error rate! Edit errors on video page)

Wild flooding in Yellowstone resulted in unpredicted devastation, including the evacuation of 10,000 people and destruction of property.
Hydrologic models used for forecasting floods are becoming less reliable due to the changing climate rendering historical data inadequate.
Climate change has led to less snowfall in winter, more rain in spring, creating a recipe for flash floods in areas like Yellowstone.
Extreme weather incidents are becoming more common, with events previously considered 100-year occurrences now happening more frequently, almost every 20 years.
Residents in areas prone to extreme weather need to be prepared for more frequent and intense events with less warning, such as floods or tornadoes.
Emergency responders were caught off guard by the severity of the flooding, indicating a lack of adequate warning systems for these increasingly common events.
People should start keeping necessary supplies on hand to better prepare for sudden extreme weather events.
Historical weather patterns are becoming less useful for predicting future events due to climate change.

Actions:

for residents in areas prone to extreme weather.,
Prepare an emergency kit with essentials for extreme weather events (implied).
Stay informed about the changing weather patterns in your area and adapt your preparedness accordingly (implied).
</details>
<details>
<summary>
2022-07-09: Let's talk about the Pennsylvania, and a cop being hired and fired.... (<a href="https://youtube.com/watch?v=el6JBAUvoQ4">watch</a> || <a href="/videos/2022/07/09/Lets_talk_about_the_Pennsylvania_and_a_cop_being_hired_and_fired">transcript &amp; editable summary</a>)

Cop killing of Tamir Rice revisited in rural Pennsylvania, leading to community pushback and mayor's surprising protest, showcasing the need for police accountability databases and common ground in rural distrust.

</summary>

"This is a community that once the information was known, they decided they wanted something else."
"There's common ground to be had because it's not a matter of a political agenda at that point."
"Once I got this message, I went and found the footage."
"Most times when you see somebody who looks like the mayor or me, you're probably not thinking ally."

### AI summary (High error rate! Edit errors on video page)

Cop named Loman killed Tamir Rice in 2014 for playing with a toy at a park.
Loman never charged, eventually fired for relying on their application.
Loman resurfaced in Tioga Borough, Pennsylvania, got hired, but resigned after the community protested.
Mayor was unaware of Loman's background, suggesting the borough council knew.
Need for a police database evident in this incident.
Rural communities like Tioga share distrust of cops and outsiders.
Tioga Mayor, a big-bearded man, protested against cop but is the same person.
Rural communities seek right and wrong, not political agendas.
Common ground can be found between truly rural people.
Stereotypes may cloud perceptions of allies in accountability.

Actions:

for community members, activists,
Contact local officials to advocate for police accountability databases (implied)
Build bridges between rural communities and law enforcement for better understanding (implied)
</details>
<details>
<summary>
2022-07-09: Let's talk about sensationalism for fun and profit.... (<a href="https://youtube.com/watch?v=_JJ4aIS7Rqg">watch</a> || <a href="/videos/2022/07/09/Lets_talk_about_sensationalism_for_fun_and_profit">transcript &amp; editable summary</a>)

Beau criticizes media sensationalism, advocates for individual action to address the Colorado River issue, and calls for basic coverage to prompt change.

</summary>

"It is easy to sensationalize the story, but it can't be sensationalized profitably."
"I think it's one of the worst things for change, for building a better world, is people doing this."
"Those efforts by individual people, that's what builds grassroots movements that then can affect real change with the large company."
"It's a problem that probably should be addressed, but it will only be addressed if people know about it and if they should."

### AI summary (High error rate! Edit errors on video page)

Critiques media sensationalism and profit-driven coverage.
Expresses frustration at lack of front-page coverage for the Colorado River issue.
Believes the story of the Colorado River is one of the most significant in the United States.
Points out that sensational stories are profitable but not when they can't be sensationalized profitably.
Calls out media models that generate outrage but discourage viewer involvement.
Emphasizes the importance of individual actions in building grassroots movements for change.
Condemns the narrative that individual efforts are insignificant compared to larger entities' actions.
Warns against the self-defeating nature of narratives that discourage political involvement.
Urges for basic coverage to prompt change and encourage solutions for the Colorado River issue.
Advocates for awareness and action to address the water supply demand issue in the American Southwest.

Actions:

for media consumers, environmental activists,
Encourage grassroots movements (implied)
Raise awareness about the Colorado River issue (implied)
Take individual actions to conserve water (implied)
</details>
<details>
<summary>
2022-07-08: The roads to Sci-fi, social change, and The Orville with Jessie Gender.... (<a href="https://youtube.com/watch?v=WUU9AGzu3b0">watch</a> || <a href="/videos/2022/07/08/The_roads_to_Sci-fi_social_change_and_The_Orville_with_Jessie_Gender">transcript &amp; editable summary</a>)

Beau and Jessie Gender dissect the nuanced portrayal of gender identity and societal norms in "The Orville," discussing the impact of representation in sci-fi on social change.

</summary>

"Transness is more about getting to dictate how you see yourself and other people getting to see you."
"It allows them to sort of not see that adversary relationship with the discussion, but just a discussion about it."
"It's supremely helpful. And even when we get to something like the Orville and older stuff in general."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of science fiction and societal change, focusing on the show "The Orville."
Jessie Gender is introduced as a guest, known for discussing social and political issues in nerdy communities.
The episode was prompted by a flood of messages about "The Orville" and a particular episode.
The episode being discussed features a character, Topa, who is intersex in an alien society called the Moklins.
Topa is forced to undergo surgery as a child to conform to societal norms of gender.
The episode "A Tale of Two Topas" focuses on Topa's journey to realize their true gender identity and desire to reverse the previous surgery.
The distinction between being intersex and transgender is discussed, focusing on bodily autonomy and self-perception.
Misinterpretations of the storyline within the show led to some viewers perceiving it as anti-trans.
The importance of representation in sci-fi shows like "The Orville" and "Star Trek" is discussed in influencing societal change.
Positive and negative aspects of using allegory and metaphor in sci-fi for social commentary are explored.

Actions:

for sci-fi fans, lgbtq+ community,
Watch and support sci-fi shows that provide representation and meaningful social commentary (exemplified).
Engage in critical analysis of media portrayals of gender identity and societal norms (implied).
</details>
<details>
<summary>
2022-07-08: Let's talk about Trump, Biden, and baby formula.... (<a href="https://youtube.com/watch?v=-qP1dn1uBik">watch</a> || <a href="/videos/2022/07/08/Lets_talk_about_Trump_Biden_and_baby_formula">transcript &amp; editable summary</a>)

Beau shares a rural area struggle to find baby formula, revealing insights on tariffs, supply disruptions, and profit margins.

</summary>

"Different areas coming together to supply the needs."
"That nonsense about make everything at home, blah blah blah blah it never works out."
"The US has basically said our priority is keeping subsidized imports and products made with subsidized inputs out of the US market."
"Keeping the price high for you."
"It was never about punishing these other countries."

### AI summary (High error rate! Edit errors on video page)

Talks about a friend's struggle in a rural area to find baby formula for his infant son.
Beau helps his friend by locating and delivering baby formula from stores in his area.
Laughs at the decentralized way different areas came together to meet the need.
Mentions disruptions and how the idea of making everything at home doesn't always work out.
The Trump-era policy of tariffs on baby formula from Canada contributed to the shortage.
Eric Miller explains how the tariffs have affected supply and prices in the US.
The shortage prompted the Bynum administration to seek solutions by allowing imports from safe countries.
Points out that the tariffs were more about maintaining profit margins for certain companies rather than punishing other countries.
Emphasizes the impact of tariffs on keeping prices high for consumers in the US.
Wraps up by sharing these insights and wishing everyone a good day.

Actions:

for community members, policymakers,
Help distribute baby formula in areas facing shortages (exemplified)
Advocate for policies that prioritize affordable prices for consumers (exemplified)
</details>
<details>
<summary>
2022-07-08: Let's talk about DOJ looking at Abbott's Lone Star.... (<a href="https://youtube.com/watch?v=qXwyzAWSLI4">watch</a> || <a href="/videos/2022/07/08/Lets_talk_about_DOJ_looking_at_Abbott_s_Lone_Star">transcript &amp; editable summary</a>)

Operation Lone Star in Texas faces DOJ investigation for potential civil rights violations, raising concerns about manipulated success data and poor conditions for National Guard troops.

</summary>

"Operation Lone Star in Texas is reportedly under investigation by the Department of Justice's Civil Rights Division."
"The multi-billion dollar initiative is seen by many as a state-funded campaign event."
"The DOJ may be investigating civil rights violations related to the operation."
"National Guard troops are facing poor conditions and are attempting to unionize."
"Confirmation of the investigation's findings beyond leaked emails is expected soon."

### AI summary (High error rate! Edit errors on video page)

Operation Lone Star in Texas is reportedly under investigation by the Department of Justice's Civil Rights Division.
ProPublica and Texas Tribune collaborated on reporting this investigation.
The investigation uncovered formal inquiries into Operation Lone Star by the DOJ.
Texas agencies involved in the operation were unavailable for comment.
Operation Lone Star, Governor Abbott's border security initiative, involved deploying 10,000 National Guard troops.
Statistics surrounding the operation have been amended, indicating potential manipulation to exaggerate success.
National Guard troops are facing poor conditions and are attempting to unionize.
There is a shortage of resources within the operation.
The multi-billion dollar initiative is seen by many as a state-funded campaign event.
The DOJ may be investigating civil rights violations related to the operation.
Potential violations could be linked to the Civil Rights Act and issues surrounding national origin.
Confirmation of the investigation's findings beyond leaked emails is expected soon.
The investigation could impact Governor Abbott's re-election prospects.
The operation's outcomes may not match the portrayed success.
The investigation suggests deeper issues within Operation Lone Star.

Actions:

for texas residents, civil rights activists,
Contact local Civil Rights organizations to stay updated on the investigation (suggested)
Support National Guard troops in improving their conditions and unionization efforts (implied)
Stay informed about Operation Lone Star's developments and implications (suggested)
</details>
<details>
<summary>
2022-07-08: Let's talk about Biden sending gas overseas.... (<a href="https://youtube.com/watch?v=mL_XpdM5gWg">watch</a> || <a href="/videos/2022/07/08/Lets_talk_about_Biden_sending_gas_overseas">transcript &amp; editable summary</a>)

Beau clarifies the situation around oil being sent overseas, addressing concerns about Fox News and explaining the impact on global oil prices.

</summary>

"The Biden administration approved the release of 1 million barrels of oil per day."
"So not really lying, just omission of pieces of relevant information."

### AI summary (High error rate! Edit errors on video page)

Explains the situation regarding gas, gasoline, oil, and the Strategic Reserve.
Responds to a viewer's concern about Fox News possibly lying about Biden sending gasoline overseas.
Confirms that the Biden administration did allow 5 million barrels of oil to be sent overseas.
Clarifies that the oil came from the strategic reserve because US refineries are operating at capacity.
Addresses the misconception about refineries running at full capacity and explains the reason for the backlog of oil.
Mentions that sending oil overseas affects the global oil market and can potentially lower oil prices.
Emphasizes that while the release of oil for export can help lower costs, it is a gradual process.
Concludes that Fox News may not have lied but rather omitted relevant information in their reporting.

Actions:

for viewers concerned about accurate information on oil and gasoline.,
Contact your representatives to advocate for transparent and accurate information on energy policies (suggested).
</details>
<details>
<summary>
2022-07-07: Let's talk about the Georgia Guidestones.... (<a href="https://youtube.com/watch?v=2jmjdmpltg0">watch</a> || <a href="/videos/2022/07/07/Lets_talk_about_the_Georgia_Guidestones">transcript &amp; editable summary</a>)

Beau addresses suspicions and conspiracy theories surrounding the Georgia Guidestones, a Cold War relic destroyed amidst concerns about echo chambers influencing actions.

</summary>

"Maintain humanity under 500 million in perpetual balance with nature."
"People are getting so far down into these echo chambers."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the Georgia Guidestones, a stone monument in Georgia, and addresses questions about its purpose and destruction.
Notes the suspicions and conspiracy theories surrounding the monument, particularly among the far right, including concerns about a global court system and maintaining humanity under 500 million.
Describes the monument's inscriptions in multiple languages, outlining rules for society like maintaining balance with nature and guiding reproduction wisely.
Mentions the monument's Cold War origins and its purpose as a guide for rebuilding society after a nuclear war.
Speculates on the identity of the individuals who erected the monument, pointing to shaky evidence linking it to a right-wing white supremacist.
Comments on the destruction of the monument, possibly due to conspiracy theories and echo chambers, with the Georgia Bureau of Investigation releasing footage related to the incident.
Expresses a lack of concern about the monument being a guide to world domination, viewing it as a Cold War relic erected by peculiar individuals.
Raises concerns about people acting on theories they hear in echo chambers and the potential consequences of such actions.

Actions:

for history enthusiasts,
Contact the Georgia Bureau of Investigation to provide any relevant information about the destruction of the Georgia Guidestones (implied).
</details>
<details>
<summary>
2022-07-07: Let's talk about dominoes from the Colorado.... (<a href="https://youtube.com/watch?v=oH-3ZjlcMas">watch</a> || <a href="/videos/2022/07/07/Lets_talk_about_dominoes_from_the_Colorado">transcript &amp; editable summary</a>)

Beau explains the domino effects in the environment, urges attention to under-reported stories like the Colorado River, and stresses the importance of addressing environmental issues promptly to avoid escalating impacts.

</summary>

"Every piece of negative environmental news creates another, and then another."
"We need to start paying better attention to environmental stories that are going on."
"We have to get to the point where we can kind of live in balance."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of domino effects in the environment, where one issue leads to another.
Talks about how people tend to overlook environmental news and its impact because they don't realize the interconnectedness of everything.
Focuses on the under-reported story of the Colorado River and the downstream effects.
Mentions the drop in water levels in Lake Powell leading fish like smallmouth bass into new areas.
Raises concern about the endangered humpback chub species being threatened by the presence of smallmouth bass below the dam.
Warns that if the current problem is not addressed, it will lead to more issues down the line, potentially impacting the riverbed and eventually people.
Criticizes media for not adequately covering environmental issues and waiting until it's too late to address them.
Urges people to pay attention to environmental stories and listen to experts who often get sidelined by business interests.
Emphasizes the importance of balancing environmental impacts and addressing negative news before it escalates.
Encourages viewers to be more aware and proactive about environmental issues for a sustainable future.

Actions:

for environmental activists,
Listen to environmental experts and prioritize their insights (suggested)
Stay informed about environmental issues and stories (exemplified)
Advocate for balanced environmental management and solutions (implied)
</details>
<details>
<summary>
2022-07-07: Let's talk about Moore v Harper and SCOTUS.... (<a href="https://youtube.com/watch?v=ANrBplbZsFM">watch</a> || <a href="/videos/2022/07/07/Lets_talk_about_Moore_v_Harper_and_SCOTUS">transcript &amp; editable summary</a>)

Beau warns of an existential threat to democracy as the Supreme Court considers the Independent State Legislature Doctrine, potentially undermining voting rights and the will of the people.

</summary>

"This is an existential threat to the republic, to the idea of a representative democracy."
"When you look back at the times when the Supreme Court has tossed this theory aside, that's what they said."
"This is a moment where people of every party need to make sure that they understand the consequences of it."
"If they decide to implement this doctrine, that's kind of it."
"The fate of the country, in a way, rests in the hands of a justice who could not identify the elements of the First Amendment during her hearing."

### AI summary (High error rate! Edit errors on video page)

Introduction to Moore v. Harper, a Supreme Court case from North Carolina examining the Independent State Legislature Doctrine.
The doctrine, rejected by the Supreme Court for over a century, could jeopardize voting rights and the will of the people.
If implemented, the doctrine could allow state legislatures to manipulate votes, control representation, and influence the federal government.
Beau warns that this is an existential threat to democracy and a pivotal moment for the country.
The case is considered as significant, if not more, than overturning Roe v. Wade.
Beau suggests considering options like expanding the Supreme Court to counter the potential impacts of this doctrine.
He stresses the importance of understanding the implications and long-term consequences of allowing state legislatures unchecked power.
Beau urges people from all parties to pay attention and act against this threat to democracy.
This issue is seen as a critical reason to get rid of the filibuster and potentially expand the court.
The fate of the country seems to rest on the decisions of certain justices, raising concerns about their qualifications.

Actions:

for voters, activists, concerned citizens,
Inform others about the potential implications of the Independent State Legislature Doctrine (implied)
Advocate for expanding the Supreme Court as a potential countermeasure (implied)
Educate yourself and others on the long-term consequences of this doctrine (implied)
</details>
<details>
<summary>
2022-07-06: Let's talk about Pat Cipollone talking to the committee.... (<a href="https://youtube.com/watch?v=FePp1hA7ZYo">watch</a> || <a href="/videos/2022/07/06/Lets_talk_about_Pat_Cipollone_talking_to_the_committee">transcript &amp; editable summary</a>)

Pat Cipollone's role in protecting the presidency and upholding the rule of law, shedding light on Trump's actions.

</summary>

"His client isn't any individual president. The client here is the Constitution, the rule of law."
"Cipollone should be on the front lines of saying this is what happened, this is why he was wrong, this is where he broke the law."

### AI summary (High error rate! Edit errors on video page)

Pat Cipollone agreed to talk to the committee after being subpoenaed.
The testimony will be behind closed doors, possibly transcribed and recorded.
Cipollone's role is not like the Secret Service; he is meant to protect the institution of the presidency.
White House counsel's job is to ensure the president acts within the bounds of the Constitution and the rule of law.
The White House counsel should prevent the president from engaging in illegal or unconstitutional actions.
Establishing attorney-client privilege between Cipollone and Trump is among the counsel's duties.
Cipollone is expected to provide information about illegal and unconstitutional actions taken by Trump.
While Cipollone may not volunteer everything, he is not likely to hide significant information.
Cipollone is believed to have first-hand knowledge of events such as seizing voting machines and the fake elector scheme.
Cipollone should prioritize revealing the truth about Trump's actions that undermined the Constitution and the rule of law.

Actions:

for legal experts, political analysts, concerned citizens,
Contact legal organizations for updates on Cipollone's testimony (suggested)
Stay informed about developments related to the committee's proceedings (implied)
</details>
<details>
<summary>
2022-07-06: Let's talk about Georgia, Trump, Rudy, Lindsey, and subpoenas.... (<a href="https://youtube.com/watch?v=YXitTL_yuys">watch</a> || <a href="/videos/2022/07/06/Lets_talk_about_Georgia_Trump_Rudy_Lindsey_and_subpoenas">transcript &amp; editable summary</a>)

Beau dives into Georgia's grand jury subpoenas, revealing expected and missing names and hinting at potential revelations beyond what's anticipated.

</summary>

"Looks like it's going to be 1773, because I think a whole bunch of people are about to spill the tea."
"The grand jury is looking into everything."
"While everybody who is named is somebody you'd expect, there are people that you'd expect that weren't named."
"Maybe there are people who were on the SS Trump, who realized they were polishing brass on the Titanic, and they decided to jump ship."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Exploring the state of Georgia and the special grand jury's subpoenas related to possible criminal interference in the 2020 elections.
Subpoenas were sent out to expected names like Rudy Giuliani, Lindsey Graham, and lesser-known individuals, indicating a broad investigation.
Subpoenas cover a range of activities from trying to find votes to lying to lawmakers.
Individuals subpoenaed are mostly lawyers or lawmakers who may try to avoid testifying using attorney-client privilege or speech and debate clause.
Despite expectations, some key figures were not subpoenaed, raising questions about oversight or potential agreements.
Speculation on reasons for certain exclusions, such as individuals jumping ship from the Trump administration or agreements to provide information.
Humorous reference to individuals who predicted a 1776-like event on January 6th, contrasting it with the possibility of revelations leading to a different outcome.

Actions:

for legal observers, political analysts,
Speculate responsibly on the potential implications of missing names in the grand jury subpoenas (implied).
Stay informed about the developments in the investigation and be ready to analyze and interpret new information (implied).
</details>
<details>
<summary>
2022-07-06: Let's talk about American pride.... (<a href="https://youtube.com/watch?v=t08l68z6kSk">watch</a> || <a href="/videos/2022/07/06/Lets_talk_about_American_pride">transcript &amp; editable summary</a>)

Recent poll data reveals record low American pride, with young people feeling left out and unrepresented in a country that doesn't prioritize their future.

</summary>

"Why should they be? What have they been handed?"
"They want a better future and they're not going to be proud until they get it."
"You can't force people to take pride in a country that doesn't care about them."

### AI summary (High error rate! Edit errors on video page)

Recent poll data shows record low levels of American pride.
Americans are asked how proud they are to be American, with options ranging from not at all to extremely.
Only 38% of Americans said they were extremely proud, a record low.
Republicans are at 58%, while Independents are at 34%.
Demographics show that males (43%), those over 55 (51%), and those without college degrees (41%) are prouder.
The demographic least proud are those aged 18-34, with only 25% extremely proud.
Young people are not proud of the U.S. due to concerns like war, loss of rights, authoritarianism, lack of action on climate change, and economic struggles.
The older population is more easily swayed against their interests, with 51% extremely proud of the country.
Younger Americans want a better future and refuse blind nationalism without progress.
The divide in pride levels is more about age than party affiliation.
The youth feels left out and unrecognized by a country that doesn't prioritize their needs or rights.

Actions:

for american citizens,
Reach out to young people in your community to understand their concerns and amplify their voices (implied).
Support initiatives that address the issues young Americans care about, such as climate change and economic opportunities (implied).
</details>
<details>
<summary>
2022-07-05: Let's talk about parenting for the future.... (<a href="https://youtube.com/watch?v=rmK7LCdeyME">watch</a> || <a href="/videos/2022/07/05/Lets_talk_about_parenting_for_the_future">transcript &amp; editable summary</a>)

As a parent, Beau grapples with uncertainty in preparing kids for an unpredictable future, stressing the importance of leading by example, fostering curiosity, and encouraging adaptability.

</summary>

"You can't tell a kid anything. You have to show them."
"They don't know you. They don't know what you've been through. They don't know your life experiences."
"Encourage curiosity and make sure that they're adaptable."
"If you combine all of that, you're going to create a great little person there."

### AI summary (High error rate! Edit errors on video page)

Expresses uncertainty and feeling lost as a parent amidst preparing his child for an uncertain future.
Acknowledges the challenge of guiding children in an ever-changing world.
Emphasizes the importance of leading by example rather than just giving instructions to kids.
Points out that children may not fully understand or know their parents as individuals, separate from their parental roles.
Shares a personal anecdote about his son's lack of knowledge regarding his mother's past experiences.
Stresses the significance of fostering curiosity in children and encouraging adaptability for an unpredictable future.

Actions:

for parents, caregivers,
Lead by example in your actions and behaviors to show children how to read, help others, and more (implied).
Encourage curiosity by engaging with and supporting your child's interests, no matter how fleeting (implied).
Teach children adaptability by showing them that not everything always goes as planned (implied).
</details>
<details>
<summary>
2022-07-05: Let's talk about a teacher's funny story.... (<a href="https://youtube.com/watch?v=I7tarbtxDR8">watch</a> || <a href="/videos/2022/07/05/Lets_talk_about_a_teacher_s_funny_story">transcript &amp; editable summary</a>)

A teacher's $2,000 investment in medical equipment and training contrasts starkly with the need for classroom safety amidst controversies like CRT protests.

</summary>

"It's what seals have."
"She's getting her woofer, her Wilderness First Responder."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

A teacher invested around $2,000 in medical equipment and training to ensure student safety, including a stomp bag and additional supplies like chest seals and tourniquets.
Despite the teacher's proactive measures, the only trouble she ever faced during her years of teaching was for giving a student Midol.
The teacher's dedication to student safety is evident through the expensive medical equipment and obtaining a Wilderness First Responder certification.
The location where the teacher works has seen parents protesting over Critical Race Theory (CRT) at the school board meetings.
Beau points out the irony of teachers needing to spend significant amounts on equipment and training to keep their classrooms safe in a world where radicalization and controversies like CRT protests exist.
The acronym STOMP stands for CIL Team Operational Medical Pack, reflecting the level of preparation teachers believe they need in today's environment.

Actions:

for teachers, educators,
Advocate for better funding for school safety equipment and training (implied)
Support teachers in obtaining necessary medical equipment and training (implied)
Raise awareness about the financial burdens teachers face to keep classrooms safe (implied)
</details>
<details>
<summary>
2022-07-05: Let's talk about Sweden, Finland, and NATO.... (<a href="https://youtube.com/watch?v=BOXeMtpRlZI">watch</a> || <a href="/videos/2022/07/05/Lets_talk_about_Sweden_Finland_and_NATO">transcript &amp; editable summary</a>)

Debunks concerns about Finland and Sweden joining NATO, showcasing how NATO serves as a deterrent to war and unites against aggression.

</summary>

"They're wrong. They're wrong."
"Nobody wants to deal with that. It's a deterrent."
"It doesn't increase the risk."

### AI summary (High error rate! Edit errors on video page)

Debunks the argument against allowing Finland and Sweden into NATO, stating it's wrong.
Emphasizes the importance of looking at the scenario from NATO's and the West's point of view.
Explains that Finland and Sweden are not opposition nations and are generally already allies with NATO.
Points out that if these countries were to go to war, they are more likely to be attacked than to attack.
Asserts that if Finland joins NATO, an attack on them means going to war with the entire alliance, acting as a deterrent.
Mentions how Russia's attack on Ukraine accelerated NATO expansion and led to the likelihood of more nations joining.
Views NATO as a deterrent to war, creating a united front against aggression.

Actions:

for foreign policy enthusiasts,
Join organizations supporting NATO expansion (exemplified)
</details>
<details>
<summary>
2022-07-04: Let's talk about a July 4th Public Health update.... (<a href="https://youtube.com/watch?v=L9eC4Uaggsw">watch</a> || <a href="/videos/2022/07/04/Lets_talk_about_a_July_4th_Public_Health_update">transcript &amp; editable summary</a>)

Beau provides an optimistic update on the current global public health situation, with reasons to be cautiously optimistic until fall, attributing improvements to immunity levels and better medical treatments.

</summary>

"Docs currently have given us permission to be cautiously optimistic, quote, at least until fall."
"We have permission to be cautiously optimistic."
"All of this taken as a whole means higher survivability, which is good."

### AI summary (High error rate! Edit errors on video page)

Addressing the audience on Independence Day, mentioning a potential different kind of Independence Day celebration.
Providing an update on the global public health situation, with permission to be cautiously optimistic until fall.
Not expecting significant changes until it starts to cool down again.
Noting the current death toll in the United States and the decrease from the previous year.
Explaining the reasons behind the cautious optimism, including immunity levels and improved medical treatments.
Mentioning that most people have likely had the virus and with vaccinations, immunity has increased.
Pointing out that while things are improving, it doesn't mean it's over yet.
Expecting a possible level-off in fall and a more seasonal nature for the virus.
Some suggest the virus will remain in the background but won't cause massive loss.
Beau will continue monitoring the situation and update if needed.

Actions:

for public health-conscious individuals.,
Monitor updates from trusted sources regularly (exemplified).
Stay informed about public health guidelines and recommendations (exemplified).
</details>
<details>
<summary>
2022-07-04: Let's talk about Democratic messaging.... (<a href="https://youtube.com/watch?v=rLYZhYJh24k">watch</a> || <a href="/videos/2022/07/04/Lets_talk_about_Democratic_messaging">transcript &amp; editable summary</a>)

The Democratic Party faces challenges in making issues like Roe v. Wade a priority despite majority support, stressing the need for clear messaging to motivate voter turnout and elevate these issues.

</summary>

"It's not on the ballot just because it's an impact from the election."
"You can actually put Roe on the ballot. But you can't be timid."
"Those that were surveyed after the decision came down, 30%."

### AI summary (High error rate! Edit errors on video page)

The Democratic Party struggles with messaging and getting their policies seen as a priority.
Despite polls showing support for Democratic positions, the party faces challenges in making issues like Roe v. Wade a priority.
A poll by AP revealed that before the decision on Roe v. Wade, only 13% saw family planning and women's rights as a priority. Afterward, this number rose to 30%.
While 64% of Americans believe abortion should be legal in all or most cases, only 30% prioritize it as an issue.
Beau suggests that the Democratic Party needs to focus on explaining the economic and social impacts of issues like Roe v. Wade to increase prioritization by voters.
Candidates need to be frank about the consequences and investigations that may occur if certain policies are not protected.
The messaging should center on the potential economic strain, impact on social safety nets, and invasion of privacy resulting from policy changes.
Making issues like family planning and women's rights a priority is key to motivating voter turnout.
Beau stresses the importance of clear and consistent messaging to raise the issue's priority level among voters.
The Democratic Party needs to communicate effectively to elevate issues to ballot priorities.

Actions:

for democratic party members,
Communicate the economic and social impacts of policies like Roe v. Wade to raise voter awareness and prioritize these issues (suggested).
Be frank with voters about the consequences and investigations that may occur if certain policies are not protected (suggested).
</details>
<details>
<summary>
2022-07-04: Let's talk about China, North Dakota, and spies... (<a href="https://youtube.com/watch?v=8c8aHr7V5WI">watch</a> || <a href="/videos/2022/07/04/Lets_talk_about_China_North_Dakota_and_spies">transcript &amp; editable summary</a>)

Beau received a message about Chinese spy base rumors in North Dakota, leading to concerns about a Chinese food company's land purchase near an Air Force base and potential intelligence surveillance. Senate intelligence oversight has expressed concerns, sparking community turmoil and speculation on the project's intentions.

</summary>

"Your neighbor hasn't lost the plot. This is actually something that's being discussed."
"It's going to become more and more pronounced, especially if Russia does get bogged down in Ukraine the way it appears they will."

### AI summary (High error rate! Edit errors on video page)

Received a message about Chinese spy base rumors in North Dakota.
Chinese food company purchased land near an Air Force base in North Dakota.
Concerns raised about Chinese intelligence gathering information.
Senate intelligence oversight expressed concerns about the situation.
The surveillance concerns could be plausible due to electronic surveillance capabilities.
The facility's proximity to the Air Force base has caused turmoil in the community.
The project's potential cancellation is suggested due to security concerns.
There are easier ways for intelligence gathering than a 300-acre facility.
Speculation on the intentions behind the land purchase and surveillance concerns.
Uncertainty surrounds the true motives behind the project.
The situation has attracted national attention and made it from North Dakota to D.C.
Beau raises questions about the credibility and secrecy of such a large project.
Anticipates more situations where U.S. intelligence counters perceived threats from China.
Warns of increasing intelligence threats in the next few decades.

Actions:

for community members, concerned citizens,
Monitor local news and developments related to the situation (implied)
Stay informed about intelligence threats and security concerns in your area (implied)
</details>
<details>
<summary>
2022-07-03: Let's talk about the Secret Service talking.... (<a href="https://youtube.com/watch?v=tEH77n9128c">watch</a> || <a href="/videos/2022/07/03/Lets_talk_about_the_Secret_Service_talking">transcript &amp; editable summary</a>)

Beau addresses the dangers of breaching secrecy within the Secret Service and urges focus on the core issue of the former president's alleged desire to lead rioters at the Capitol.

</summary>

"We cannot normalize the Secret Service talking."
"We can't normalize this."
"The bombshell is that he wanted to go."
"Everybody needs to focus on the important part."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses the Secret Service, presidency, secrecy, and trust.
Waits to make a video until the narrative shifts in his favor regarding the former president.
Expresses distrust in the denial from the Secret Service regarding an incident in the limo.
Emphasizes the importance of the Secret Service maintaining secrecy and not talking to the press.
Gives scenarios illustrating the dangers of the Secret Service revealing confidential information about the president.
Stresses that focusing on salacious details rather than the core issue is a distraction.
Urges everyone to concentrate on the fact that the former president allegedly wanted to lead the rioters at the Capitol.

Actions:

for concerned citizens,
Respect confidentiality and privacy in all aspects of your life (implied).
Uphold trustworthiness by keeping sensitive information confidential (implied).
Encourage others to focus on critical issues rather than sensational details (implied).
</details>
<details>
<summary>
2022-07-03: Let's talk about an update on Flint... (<a href="https://youtube.com/watch?v=tR8_jaT_SjM">watch</a> || <a href="/videos/2022/07/03/Lets_talk_about_an_update_on_Flint">transcript &amp; editable summary</a>)

The Supreme Court voids charges in Flint case due to procedural errors, reflecting a growing issue of officials prioritizing self-interest over public service, impacting innocent citizens severely.

</summary>

"The Flint water crisis stands as one of this country's greatest betrayals of citizens by their government."
"This type of betrayal, it's becoming more common in the United States where elected officials at least appear to be putting their own interest above the interests of the people they're supposed to serve."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court in Michigan declared charges against the former governor and others void due to procedural issues.
The law authorizes a judge to issue arrest warrants and subpoena witnesses, but not indictments.
The case is linked to the lead contamination crisis in Flint during 2014-2015.
The state plans to continue trying to prove the allegations and see the process through.
The Supreme Court emphasized the severe impact of the government's actions on innocent citizens in Flint.
The crisis is regarded as one of the country's greatest betrayals of citizens by their government.
The legal process involves an additional step before obtaining indictments.
The state is committed to pursuing indictments and moving forward with the case.
The Supreme Court decision was unanimous, possibly due to misinterpreting a rarely used law.
Elected officials prioritizing self-interest over public service is a growing issue in the United States.

Actions:

for legal advocates, activists, concerned citizens,
Contact local representatives to advocate for justice for the Flint community (suggested)
Support organizations working towards justice for communities affected by similar crises (implied)
</details>
<details>
<summary>
2022-07-03: Let's talk about Texas and teaching history.... (<a href="https://youtube.com/watch?v=OxDuReePwP8">watch</a> || <a href="/videos/2022/07/03/Lets_talk_about_Texas_and_teaching_history">transcript &amp; editable summary</a>)

Texas proposed softening the truth of history by replacing "slave trade" with "involuntary relocation," aiming to teach American mythology instead of critical history.

</summary>

"You can't soften what happened. It was horrible. It was hideous."
"They want American mythology. They want propaganda taught, and they want it packaged as if it was history."
"There are no heroes. Not really. They were people. They were flawed."

### AI summary (High error rate! Edit errors on video page)

Texas proposed replacing "slave trade" with "involuntary relocation" in history classes.
Department of Education in Texas rejected the proposal, acknowledging the problematic nature of the term.
The goal behind introducing such terminology is to soften the brutal reality of slavery and historical atrocities.
There's a push to teach American history as mythology and propaganda rather than critically.
People want to whitewash history to make themselves and the country look better.
Teaching history inaccurately can lead to misinformation and a lack of understanding.
Softening the truth in history education can lead to future generations repeating mistakes.
American history is full of heroes who spoke out against atrocities, but their voices are often silenced.
Education is key to ensuring that future generations make informed decisions and understand the past.
It's vital to teach history accurately, including the horrors and atrocities that occurred.
Softening the truth of history by using new terms is not a solution to the real issues that happened.
The youth need to be educated, not indoctrinated or fed myths.
Heroes in history were flawed individuals who did both good and bad.
Understanding the truth of history is critical for raising informed and empathetic individuals.

Actions:

for educators, historians, students,
Ensure accurate and critical history education in schools (implied)
Encourage the teaching of American history in its full context, including the good and the bad (implied)
Advocate for educating the youth on the true, unfiltered history of their country (implied)
</details>
<details>
<summary>
2022-07-02: Let's talk about the committee reaching Republicans.... (<a href="https://youtube.com/watch?v=beY6RoWz2No">watch</a> || <a href="/videos/2022/07/02/Lets_talk_about_the_committee_reaching_Republicans">transcript &amp; editable summary</a>)

The committee's effective narrative shifts, as seen through Andrew McCarthy's changed views, underscore the importance of influencing opinions for potential legal repercussions, aiming to impact future jury members.

</summary>

"The committee is changing this person's mind."
"It is shifting opinion. It is changing thought."
"Those shifting opinions matter if they're going to charge Trump."
"The committee's doing its job."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Examining whether the committee is effective in changing minds regarding the events of a particular day.
People are polarized in their opinions about guilt or innocence based on their interpretations of the actions of that day.
The committee targets those whose opinions can be shifted to change the narrative.
Andrew McCarthy, a former federal prosecutor known for his partisan views, has shown a shift in opinion regarding Trump's actions on that day.
McCarthy previously defended Trump but now believes Trump could be prosecuted for his role in the events.
The committee's efforts have influenced McCarthy's perspective, indicating a successful impact on changing minds.
Despite initial skepticism, Beau believes that continuing to present information is vital for shifting opinions and potentially influencing future jury members.
Beau underscores the importance of reaching those who were previously in echo chambers to facilitate opinion shifts.

Actions:

for concerned citizens,
Contact your representatives to express your views on the committee's proceedings (suggested).
Stay informed about ongoing developments related to the events discussed in the transcript (implied).
</details>
<details>
<summary>
2022-07-02: Let's talk about Ohio, Indiana, a girl, and the Supreme Court.... (<a href="https://youtube.com/watch?v=wyZkhr7aQNw">watch</a> || <a href="/videos/2022/07/02/Lets_talk_about_Ohio_Indiana_a_girl_and_the_Supreme_Court">transcript &amp; editable summary</a>)

Ohio, Indiana, SC ruling, ten-year-old girl sent away – GOP future and lack of compassion exposed, urgent call for reflection on priorities.

</summary>

"We are headed down a very, very bad road."
"They don't care about this."
"Go kick down at that ten-year-old girl."
"Hope it makes you feel better."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Ohio and Indiana in focus.
Three-day response after SC ruling.
Ten-year-old girl sent from Ohio to Indiana for services.
Republicans' future offering scrutinized.
Concern over federal ban implications.
GOP base energized against those in need.
Potential nationwide ban if GOP takes House and Senate.
Urgent situation of ten-year-olds in need.
Republican Party's role in current state criticized.
Ill consequences of prioritizing politics over compassion.
Call to reconsider supporting such policies.
Warning against being manipulated by politicians.
GOP's priorities questioned.

Actions:

for voters, parents, activists,
Re-evaluate political priorities and policies (suggested)
Advocate for compassionate and inclusive services for all children (implied)
</details>
<details>
<summary>
2022-07-02: Let's talk about Arizona, Trump, and DOJ.... (<a href="https://youtube.com/watch?v=thpgMUuCmh8">watch</a> || <a href="/videos/2022/07/02/Lets_talk_about_Arizona_Trump_and_DOJ">transcript &amp; editable summary</a>)

Department of Justice's subpoenas in Arizona suggest an investigation linking the fake elector scheme to Trump's inner circle, indicating a potential criminal case.

</summary>

"We will have to keep watching, but there is yet another little piece of evidence that fits into that pattern that says, yeah, they actually are doing something."
"It's worth noting that if I'm not mistaken, Fan had a conversation with Rusty Bowers, who's the speaker out there, speaker of the house, who in that conversation kind of indicated that somebody from Trump's team had encouraged her to help him find a way to get the lead in the votes."
"What we're seeing is that circle closing in slowly but surely around the Trump inner circle."

### AI summary (High error rate! Edit errors on video page)

Department of Justice subpoenas were issued to Arizona Senate President Karen Fann and State Senator Kelly Townsend.
DOJ is seeking communications between Townsend and Trump's lawyers regarding the alleged fake elector scheme.
The end goal of the subpoenas appears to be establishing a connection to Trump's inner circle.
DOJ seems actively investigating a criminal case involving someone within Trump's circle.
Efforts are focused on connecting the fake elector scheme to Trump's inner circle.
There is skepticism about whether the DOJ will pursue Trump.
Despite criticism of Merrick's handling, it seems in line with his usual approach of avoiding headlines until necessary.
DOJ's actions indicate a tightening circle around Trump's inner circle, with Arizona playing a significant role.
DOJ appears to be following up on interactions between Arizona officials and Trump's team regarding the election.
The investigation appears to be moving towards a criminal case, although Merrick's approach keeps details scarce.

Actions:

for legal analysts, political commentators,
Monitor developments and updates on the investigation in Arizona (implied).
</details>
<details>
<summary>
2022-07-01: Let's talk about finding your community network.... (<a href="https://youtube.com/watch?v=2l6F3_3sARQ">watch</a> || <a href="/videos/2022/07/01/Lets_talk_about_finding_your_community_network">transcript &amp; editable summary</a>)

Beau explains how to use social media platforms like Twitter to find like-minded people nearby and build a community network that can make a real-world impact.

</summary>

"Got some people I want you to meet."
"You can transform that online community into something they can do good in the real world."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of starting a community network and finding people for it.
Talks about receiving messages from people wanting to start a community network but not knowing where to find like-minded individuals.
Explains using Twitter's search function to find people nearby.
Shares personal experience of using Twitter to find people close by.
Mentions searching for specific terms related to like-minded individuals.
Describes finding familiar and unfamiliar people through the search function.
Suggests reaching out, getting to know people online, and eventually meeting in person.
Mentions that most social media platforms have similar search functions.
Encourages transforming online communities into real-world impactful networks.

Actions:

for community builders, social media users,
Use social media platforms like Twitter to search for like-minded individuals nearby (exemplified).
Reach out, get to know people online, and plan to meet in person (exemplified).
Transform online communities into impactful real-world networks (implied).
</details>
<details>
<summary>
2022-07-01: Let's talk about a meme and question for conservatives.... (<a href="https://youtube.com/watch?v=2Qtkf2LkawI">watch</a> || <a href="/videos/2022/07/01/Lets_talk_about_a_meme_and_question_for_conservatives">transcript &amp; editable summary</a>)

Beau challenges conservative beliefs by questioning the refusal to adapt in a changing world, criticizing entitlement and lack of progress.

</summary>

"When's the last time you changed your political opinion on something?"
"That is wild."
"You want the United States to be a world leader, you have to lead."
"They're meeting the world head on instead of hiding in the past."
"There's no progress at all."

### AI summary (High error rate! Edit errors on video page)

Analyzing a meme sent by a conservative follower that portrays stick figures labeled left, right, and me.
Revealing the message behind the meme and discussing the implications.
Posing a question to conservative friends and family about changing political opinions.
Expressing surprise at a conservative follower who admitted to never changing their political opinion.
Noting the rigidity and inflexibility of holding the same political opinions for over thirty years.
Criticizing the arrogance and entitlement associated with refusing to adapt political beliefs despite new information.
Pointing out the entrenched nature of political beliefs that hinder progress and adaptation.
Contrasting the perception of entitlement between young people and conservatives in terms of political beliefs.
Critiquing the lack of substantial changes in the Republican Party's positions over time.
Addressing the resurgence of a far-right movement within the Republican Party and its implications on revisiting past laws.
Challenging the notion that the world should conform to conservative beliefs without updating them.
Encouraging provoking political discourse by asking when was the last time someone changed their political opinion.
Contrasting the readiness to adapt political opinions between left-leaning individuals and conservatives.
Emphasizing the importance of accepting new ideas and information to progress and lead in society.
Criticizing the tendency to cling to outdated political talking points instead of embracing progress.

Actions:

for conservative friends and family,
Provoke a political discourse by asking conservative friends and family when was the last time they changed their political opinion (suggested).
</details>
<details>
<summary>
2022-07-01: Let's talk about Liz Cheney, the partisan committee, and Republicans.... (<a href="https://youtube.com/watch?v=BWUPH8u_jiQ">watch</a> || <a href="/videos/2022/07/01/Lets_talk_about_Liz_Cheney_the_partisan_committee_and_Republicans">transcript &amp; editable summary</a>)

Republicans on the committee are addressing threats to their party and country by confronting authoritarianism, making it an American, not partisan, effort.

</summary>

"You can be loyal to Donald Trump or you can be loyal to the Constitution. You can't do both."
"If it is partisan, it is a Republican committee."
"This isn't partisan. This is American."
"This committee may very well be one of the most significant political events that occurs in your life."
"Many are missing out on a significant political event due to past allegiances."

### AI summary (High error rate! Edit errors on video page)

Explaining the term "partisan" in relation to the committee, mostly used by Republicans in discussing it.
Noting that the witnesses are mainly Republicans, particularly mentioning Liz Cheney's involvement.
Republicans on the committee are trying to correct their party and address what they see as a threat to the country.
Many Republicans are acknowledging that the strain of authoritarianism seen is not compatible with the United States or its Constitution.
Cheney expressed that one cannot be loyal to both Donald Trump and the Constitution.
The committee is viewed as an attempt by Republicans to address and remove what they see as a threat to their party and the nation.
The committee is not partisan but rather an American effort, with critical evidence and testimony coming from Republicans.
Beau criticizes those who try to undermine the committee's credibility by labeling it as partisan, similar to baseless claims about election fraud.
The importance of the committee is emphasized, with Beau suggesting that many are missing out on a significant political event due to previous allegiances.
Overall, the committee is seen as a critical effort by Republicans to address internal threats and defend American values.

Actions:

for political observers,
Watch the committee to understand the critical evidence and testimony presented (suggested)
Acknowledge the importance of the committee as a significant political event (suggested)
</details>
