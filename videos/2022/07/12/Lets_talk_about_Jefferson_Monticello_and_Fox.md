---
title: Let's talk about Jefferson, Monticello, and Fox....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A5z9HhuPny8) |
| Published | 2022/07/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fox News guest criticized Monticello for being frank about Thomas Jefferson's history of slavery.
- Jefferson acknowledged slavery as evil, but engaged in it anyway.
- Jefferson believed that over time, people's attitudes towards slavery would change.
- Beau argues that Jefferson was aware of his actions and foresaw criticism for them.
- Not discussing Jefferson's history is not history, but American mythology.
- Beau asserts that addressing the flaws of historical figures is necessary for learning and understanding history.

### Quotes

- "Monticello didn't go woke, as is often the case."
- "Not discussing Jefferson's history is not history, but American mythology."
- "They were people just like everybody else. They made mistakes."
- "That's the belief that these people were infallible."
- "Thomas Jefferson was aware of the injustice, too, and he would be happy to know that people today were criticizing him for it."

### Oneliner

Fox News guest criticizes Monticello for being frank about Thomas Jefferson's history of slavery, but Beau argues that addressing Jefferson's flaws is necessary for understanding history.

### Audience
History enthusiasts

### On-the-ground actions from transcript

- Address the flaws and mistakes of historical figures in educational settings (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the implications of acknowledging and learning from the flaws of historical figures.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Thomas Jefferson.
We're gonna talk about Thomas Jefferson and Monticello
and American history and American mythology and Fox News.
We're gonna do this because Fox brought on a guest
who had recently gone to Monticello.
That's Jefferson's home.
And the guest was bothered by the fact
that the tour, which is an educational experience,
was very frank about some of the things
that Jefferson had done.
And if you don't know, Jefferson did some messed up stuff.
And the tour, according to the guest,
just frankly admitted it and talked about it.
OK.
This prompted the guest to suggest
that Monticello had gone woke, suffered from a woke-ism.
This, of course, prompted a lot of conversation
on social media where people were discussing
all of the horrible stuff that Jefferson did.
And there were people saying that we
shouldn't talk about this, that he didn't know any better,
that it was just the times,
and that this is just woke revisionism.
I have kind of a hot take here.
I'm going to suggest that Jefferson knew slavery was evil.
He knew it was a moral depravity.
He knew it was a hideous blot.
And he did it anyway.
I am further going to say that Jefferson would be happy
knowing that people today were criticizing him
for engaging in it.
Now, bold claim, right?
Yeah, those are his words.
He said that.
Jefferson, a person who engaged in slavery a lot,
referred to it as evil, referred to it as a moral depravity,
referred to it as a hideous blot.
He also said, I am happy in believing
that the conviction of the necessity of removing this evil
gains ground with time.
He knew all of that stuff.
He knew it was horrible.
He knew it was wrong.
He did it anyway.
And he also knew that on a long enough timeline,
the attitudes of people in general towards slavery
would change.
And they would look at it and see horror on horror's face.
They would recognize it as evil.
And that they would see it as a necessity to remove it.
So unless you believe that Thomas Jefferson had absolutely
no foresight, no understanding of history,
no understanding of the way people just are,
unless you believe that, you can't say that you shouldn't
criticize him.
Because he, in fact, would be happy about it
by his own writings.
He knew it was going to happen.
You can't say that Thomas Jefferson wasn't aware
of the fact that people tend to criticize
those who engaged in barbaric behavior in the past.
He did it himself.
Aside from that, not addressing this,
not addressing somebody who was as formative to the United
States as Thomas Jefferson, not addressing their history,
not talking about it hundreds of years later
so others can learn from it, that's not history.
That's American mythology.
That's the belief that these people were infallible.
They weren't.
They were people just like everybody else.
They made mistakes.
They did horrible things.
Sometimes they did horrible things
that they knew were depraved.
They did them anyway.
Monticello didn't go woke, as is often the case.
People are mistaking just history for being woke
because history is aware of injustice.
Thomas Jefferson was aware of the injustice, too,
and he would be happy to know that people today
were criticizing him for it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}