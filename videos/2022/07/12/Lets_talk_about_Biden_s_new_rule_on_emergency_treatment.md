---
title: Let's talk about Biden's new rule on emergency treatment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CMw5fGHah9k) |
| Published | 2022/07/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration's rule on treatment isn't new; federal law already dictates providers must act if mother is in danger.
- Bans on treatment since Roe v. Wade have exemptions for mother's health, limiting impact of Biden's press release.
- Federal guidance allows providers to stabilize patients in danger, but state investigations may lead to delayed care.
- Providers may delay necessary care to avoid legal trouble, resulting in unnecessary loss of life.
- Lack of clear guidelines from federal government puts providers in a difficult position, facing potential criminal charges.
- Providers may err on side of caution due to lack of clarity, impacting patient care.
- Some are considering performing procedures on boats outside state waters to navigate legal restrictions.
- Efforts are underway to make certain forms of birth control available over the counter, but process may take time.
- Uncertainty persists due to conflicting laws, jurisdictions, and bureaucratic processes.
- Need for clear guidelines and legal affirmation to ensure providers can act in the best interest of patients.

### Quotes

- "Biden administration's rule on treatment isn't new; federal law already dictates providers must act if mother is in danger."
- "Providers may delay necessary care to avoid legal trouble, resulting in unnecessary loss of life."
- "Need for clear guidelines and legal affirmation to ensure providers can act in the best interest of patients."

### Oneliner

Biden administration's guidance on treatment for mothers in danger underlines the need for clear legal guidelines to prevent delays in necessary care.

### Audience

Healthcare providers, policymakers

### On-the-ground actions from transcript

- Advocate for clear guidelines and legal affirmation for healthcare providers (implied)
- Stay informed on developments regarding access to birth control and reproductive healthcare (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges faced by healthcare providers in navigating legal restrictions and the need for clear guidelines to ensure timely and effective patient care.

### Tags

#BidenAdministration #Healthcare #ReproductiveRights #LegalGuidelines #PatientCare


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the Biden
administration's new rule about treatment,
except it's not new, and it's not a Biden administration
rule.
This wasn't something I was going to talk about,
but a whole bunch of questions came in.
So here we are.
If you don't know, the Biden administration
kind of put out a press release almost,
saying that providers in states where
they have bans since the overturning of Roe,
if the mother is in danger, you do what you have to do.
And federal law preempts state law.
And I mean, it sounded cool, and it
sounds like a whole lot of people took that as something
that the Biden administration did.
No, that's just federal law.
That was that way before Biden ever took office.
I'd also point out that in most cases,
the bans or the proposed bans do have exemptions
for the health of the mother.
Right?
So this really isn't going to do much.
I mean, I'm sure from the provider point of view,
it's nice to know the feds are on your side.
And the guidance that went out did
provide a couple of examples, like ectopic pregnancies
or preeclampsia, something like that, as justifications.
And they did use the term stabilize the patient,
rather than the way a lot of the state laws work.
It really seems like the mother's life
has to be in immediate danger.
Right?
So there is that.
At the same time, if you're the provider, what do you know?
You know that if you perform that procedure,
well, the state is going to come in and investigate
and in the beginning, just like any law,
there's going to be overzealous enforcement.
So providers are probably going to delay care.
They're going to wait until the absolute last moment.
So when that investigation comes, they can, look,
I waited.
I did everything I could.
Right?
That will result in delay of care for the patient.
And eventually, that will result in losing a patient
that didn't have to be lost.
At some point, the federal government
and the state governments are going
to have to come up with guidelines on this,
because the providers are in a very uncomfortable situation.
This isn't something that is something
that malpractice insurance is going to take care of.
This is a criminal charge that they
would be looking at if they wound up
on the wrong side of one of these decisions.
More importantly, they could be facing a criminal charge
if the investigator thinks they were on the wrong side of one
of those decisions.
It has put them in a very unenviable position.
The federal government needs to be way more clear.
The guidance that went out, that's not enough.
They have to be way, way more clear.
And they have to affirm it through the courts
before it's really going to take hold,
because providers out of self-preservation
are going to err on the side of caution.
Now, in related news, there is apparently
a push in this general region to set up boats,
leave state waters, and the procedure
could be performed out there.
They're looking into it, and they're
trying to find locations and methods of doing this.
That's unique.
We'll have to wait and see how that plays out.
But that is something else that is going on along the family
planning front.
At the same time, there's also news
about them trying to provide over-the-counter birth
control.
They're applying with the FDA to make
certain forms of birth control available over the counter.
And a process like that's probably going to take a year.
That's another development.
People right now are looking for some kind of certainty
on this topic.
You're not going to find it for a while.
You have a lot of competing laws and jurisdictions
and bureaucracies that are going to be duking it out
until there's finally some stabilized lines on what
can be done where and when and in what situations.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}