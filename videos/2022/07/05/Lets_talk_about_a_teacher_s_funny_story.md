---
title: Let's talk about a teacher's funny story....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=I7tarbtxDR8) |
| Published | 2022/07/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A teacher invested around $2,000 in medical equipment and training to ensure student safety, including a stomp bag and additional supplies like chest seals and tourniquets.
- Despite the teacher's proactive measures, the only trouble she ever faced during her years of teaching was for giving a student Midol.
- The teacher's dedication to student safety is evident through the expensive medical equipment and obtaining a Wilderness First Responder certification.
- The location where the teacher works has seen parents protesting over Critical Race Theory (CRT) at the school board meetings.
- Beau points out the irony of teachers needing to spend significant amounts on equipment and training to keep their classrooms safe in a world where radicalization and controversies like CRT protests exist.
- The acronym STOMP stands for CIL Team Operational Medical Pack, reflecting the level of preparation teachers believe they need in today's environment.

### Quotes

- "It's what seals have."
- "She's getting her woofer, her Wilderness First Responder."
- "It's just a thought."

### Oneliner

A teacher's $2,000 investment in medical equipment and training contrasts starkly with the need for classroom safety amidst controversies like CRT protests.

### Audience

Teachers, educators

### On-the-ground actions from transcript

- Advocate for better funding for school safety equipment and training (implied)
- Support teachers in obtaining necessary medical equipment and training (implied)
- Raise awareness about the financial burdens teachers face to keep classrooms safe (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the financial sacrifices teachers make for student safety and the societal challenges they navigate.

### Tags

#TeacherSafety #StudentSafety #ClassroomPreparedness #Education #CommunitySupport


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a message
that I got from a teacher.
And it's humorous on some levels.
So we're going to go through that,
and maybe there's something important to note in it
as well.
Hey, Beau, I thought this would be
a good laugh for your audience.
I'm a teacher in blank area.
I'm in the process of getting my WFR, Wilderness First
Responder.
I had no idea why you pronounced it Wolfer.
I purchased a top of the line medical kit
and then added vented chest seals, NPR,
that's the bag you squeeze to do air, everything
I could possibly need.
Apparently, it's what seals have.
And what she has, the base kit that she bought
was a stomp bag.
I bought eight extra TQs with the pouches
that go on the outside of the bag.
I took it to get approval to have it in the class.
And walking in, I remembered all of the travel size medicines
in it.
The only thing I have ever gotten in trouble
for in all of my blank years of teaching
was giving a student Midol.
OK, so while, yes, that is something straight out
of South Park, there's something that I kind of
want to point out here.
The stomp bags aren't cheap.
What she got is not cheap.
They range, I don't know, $300 to $600,
depending on what type you get.
She added a whole bunch of additional stuff.
In the photo she sent, I am looking at at least $1,000
worth of medical equipment.
On top of that, she's getting her woofer, her Wilderness
First Responder.
Depending on where she lives, that
could run anywhere from $600 to $1,500.
This is a teacher dropping probably around $2,000,
somewhere around $2,000 to make sure
that the students in her class are safe in this eventuality,
safer.
They have a chance, at least.
The one thing about her location I can tell you,
and that I will tell you, is that it
is a location where there have been parents screaming
at the school board over CRT.
It would be great if the school board
would stop being radical.
It would be great if people would stop radicalizing
those who do stuff like this.
It would be great if we lived in a world where teachers didn't
feel the need to spend a couple thousand dollars
on medical equipment and training
to keep their class safer.
Incidentally, STOMP, that acronym,
is like CIL Team Operational Medical Pack,
or something like that.
That's what teachers feel they need
to have in their class to deal with the USA today.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}