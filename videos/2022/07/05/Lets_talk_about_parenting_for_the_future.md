---
title: Let's talk about parenting for the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rmK7LCdeyME) |
| Published | 2022/07/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses uncertainty and feeling lost as a parent amidst preparing his child for an uncertain future.
- Acknowledges the challenge of guiding children in an ever-changing world.
- Emphasizes the importance of leading by example rather than just giving instructions to kids.
- Points out that children may not fully understand or know their parents as individuals, separate from their parental roles.
- Shares a personal anecdote about his son's lack of knowledge regarding his mother's past experiences.
- Stresses the significance of fostering curiosity in children and encouraging adaptability for an unpredictable future.

### Quotes

- "You can't tell a kid anything. You have to show them."
- "They don't know you. They don't know what you've been through. They don't know your life experiences."
- "Encourage curiosity and make sure that they're adaptable."
- "If you combine all of that, you're going to create a great little person there."

### Oneliner

As a parent, Beau grapples with uncertainty in preparing kids for an unpredictable future, stressing the importance of leading by example, fostering curiosity, and encouraging adaptability.

### Audience

Parents, caregivers

### On-the-ground actions from transcript

- Lead by example in your actions and behaviors to show children how to read, help others, and more (implied).
- Encourage curiosity by engaging with and supporting your child's interests, no matter how fleeting (implied).
- Teach children adaptability by showing them that not everything always goes as planned (implied).

### Whats missing in summary

Beau's heartfelt reflections on parenting and guiding children through an uncertain future.

### Tags

#Parenting #Uncertainty #LeadingByExample #Curiosity #Adaptability


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk
about parenting a little bit. Because I got a question. So we're going to run
through four things that I think are important to this question. I normally
don't like giving parenting advice at all, mainly because I have a bunch of
kids. And if I was asked advice that would apply to all of them, I really
couldn't come up with any. They're different because they're like different
little people. But this is very general. So I have a question for you as a parent
and for the other parents among your followers. I'm asking lots of people so
no pressure to respond if you don't feel like you have any insight or interest. I
know the raw national news is keeping everyone with the platform pretty busy
commentary wise. As a parent, I feel like I've completely lost the plot. I try to
prepare my kid for the world they're gonna be facing without me someday.
And I knew going in I was gonna have to be flexible. Help them cast a wide net
skills wise. Work around plot twists and general unknowables and who they turned
out to be as a person with their own tastes and aptitudes. I don't think I was
flat-out naive about politics or the environment either. But looking back and
looking around, I truly have no idea what it is I should be preparing them for
that I have any hope of succeeding at. You're obviously a highly capable guy
with a strong sense of who you are and what you stand for. But I know a lot of
folks I describe that way who admit to feeling kind of lost on this edge of the
future too. Do you get to feeling like this as a parent? And if so, how do you
address it? I don't really have this particular concern. But I think there's
some general advice that might help. Four things. Two things to encourage and two
things that parents really need to understand, I think. The first thing is
that you can't tell a kid anything. You have to show them. If you want them to read, you
better make sure they see you reading. If you want them to help people, you better
be sure they see you helping people. They're not going to do what they don't
see you do. And then the other thing is to remember that while this may seem
really weird, because you know everything about them, right? You know what time
they need to go to sleep, what time they're going to wake up, you know their
favorite food, you know everything about them. They don't know you, like at all.
Not as a person. They know you as mom or dad. That's it. They know you at the
period of time where they began to acknowledge you as a human until whatever
age they are. That's it. They don't know you. They don't know what you've been
through. They don't know your life experiences. They don't know any of this.
The other day, my youngest son, he's on this thing where he's really
interested in ships. So we're watching some documentary and it talks about how
I guess a sister ship of the Titanic was converted into a hospital ship during
the war. And he had some question about wartime nursing. I don't really remember
what the question was, but I was like, you should ask your mom. And he's like, what
would mom know about that? And I paused for a second. I'm just like, man, you
better be glad she didn't hear you say that. Because he has no idea that mom, the
woman who takes care of everything and manages that chaos that is our household,
she's former captain mom. Like, he's literally asking about her area. But he
doesn't know that. They don't know you. So any hope that you might have of them
learning from your mistakes that you made when you were younger, probably not.
Probably not going to happen. Because to them, even when they hear it, it's just a
story. You can't tell a kid anything. You have to show them. Your moral framework,
they'll pick that up because they'll see it. Those are the two things I think
parents really need to grasp. And then as far as what you encourage in your kids,
always encourage curiosity. One becomes interested in boats, sure, dive right in
with them. Watch documentaries, read books, whatever. Encourage that. And encourage it
about every curiosity they have, no matter how fleeting. Because this helps
them build that wide skill set. And the other thing is to make sure that they're
adaptable. You know, I know people say that kids need structure, and I get it.
But they also need to understand that not everything is always going to go
according to plan. And that adaptability, combined with a wide skill set, that's
what will prepare them for the future. If they're educated, and they're willing to
apply that education in different ways, and they maintain that level of
curiosity, they have a solid moral framework. And believe me, if you're
trying to figure out how best to raise your kid, you probably have a solid moral
framework that they will pick up. If you combine all of that, you're going to
create a great little person there. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}