---
title: Let's talk about Trump, Wisconsin, and personality....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vAa9INuk9e4) |
| Published | 2022/07/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump called the Speaker of the House in Wisconsin, asking for the election to be decertified.
- Trump's actions prompt questions about his intent and personality characteristics.
- One option is that Trump truly believes decertifying the election could lead to him returning to the White House.
- Another option is that Trump is attempting to build an insanity defense.
- Trump's behavior fits with his personality traits of not letting go of grievances and always claiming victory.
- Decertifying the election in Wisconsin wouldn't change anything practically but could energize Trump's base.
- Trump's reluctance to admit defeat and always claiming he is right is detrimental to effective leadership.
- Political figures emulating Trump's behavior of not admitting defeat will lead to similar failures.
- The desire to appear infallible is common among authoritarians and those seeking power.
- The inability to accept mistakes leads to a cycle of repeating errors, as seen during and after Trump's presidency.

### Quotes

- "I didn't lose, they cheated, and then flips over the Monopoly board."
- "The inability to accept when you are wrong just leads you down a road where you continue to make mistakes."

### Oneliner

Former President Trump's actions in Wisconsin reveal concerning personality traits that are detrimental to effective leadership and political integrity.

### Audience

Political observers

### On-the-ground actions from transcript

- Hold political figures accountable for their actions (implied)
- Support leaders who admit mistakes and work towards improvement (implied)

### Whats missing in summary

The full transcript provides a deeper analysis of Trump's behavior and its potential impact on political figures emulating his actions.

### Tags

#Trump #Wisconsin #Leadership #Politics #Accountability


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk
about Trump and Wisconsin and intent and personality characteristics because
something happened and I think most people are kind of laughing it off
because it's odd. At the same time there's probably something to kind of
noticed from it, former President Trump called the Speaker of the House in
Wisconsin, the State House, the Assembly up there, and asked Voss to decertify the
election, the 2020 election, you know the thing all the hearings are about right
now. I mean, that's odd when you really think about it. We're talking about, and
I'm not talking about like way back then, I'm talking about sometime within the
last two weeks. Trump called and asked for this to be pushed through up there.
And that, you know, prompts my normal question of why? Intent, you know? What's
going on. And when you break it down, there aren't many options. I mean,
either the former president has completely lost the plot and actually
believes that somehow this would lead to him going to the White House, like there's
still some way to undo the election or something like that, which that's not a
thing. So that's one option. Another option is that he's attempting to build
an insanity defense, which sure, why not? But when you look at everything else,
this kind of fits with his personality. And it could be a personal motivation or
a more practical one. The personal one could just be, he is what many people
believe him to be. Somebody who can't let go of his grievance. Somebody who's very
much the type of person who, I didn't lose, they cheated, and then flips over the
Monopoly board, you know? And this is him unable to let it go, right? That's one
option. Another is that he's doing it for political reasons. He sees his
failing numbers, his slipping power, and he's trying to find some way to energize
his base. And this is a way he sees that might do it, give him a win of some kind,
legitimize those claims, at least symbolically. I mean you have to
understand even if Wisconsin did this it wouldn't actually change anything. It's a
very odd ask. So it could be about that base and playing into that image because
that's what his base wants, you know. They want that person that just wouldn't back
down and I mean that sounds good in theory but when you are talking about
somebody who's leading a country that's actually horrible. That's horrible and at
this point we're not just talking about Trump, we're talking about Trump and all
of those like him who share that personality trait. I'm never wrong, I
don't lose. Yeah, that makes for great social media, but it's also a horrible
leader. Can you imagine the situations that arose during his presidency that
that didn't get addressed because he wouldn't admit he was wrong, I can think of a few.
And all of those political personalities who have modeled themselves after him, they're
going to share that same trait, and they will fail in all of the ways he failed, and they
will plunge the country into the mess that we're in. I'm never wrong attitude.
It's not a good one. Everybody's wrong. Everybody's wrong about something that
they believe right now. The desire to paint yourself as infallible. That's a
desire that authoritarians have. That's a desire that those who would take your
freedom, it's a desire they have. This current wave of political personality
that can't admit defeat, can't admit when they were wrong, even when faced with
mountains of evidence. This is something that is very detrimental to the United
States and since a whole lot of their supporters don't care about that, it's
something that's very detrimental to the Republican Party. It's very detrimental
to the right wing, to conservative values and all of those identities that you've
latched onto. The inability to accept when you are wrong just leads you down a
road where you continue to make mistakes. It happened over and over again during
Trump's presidency. It's happening now after his presidency. And those
personalities, those political figures who modeled themselves after Trump,
they're going to be the exact same way. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}