---
title: Let's talk about the Republican takeaway from the hearings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ovVgzxHClB8) |
| Published | 2022/07/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Republicans about their takeaway from recent hearings where evidence was presented, including audio of Bannon discussing Trump's actions and intentions.
- Pointing out that if Republicans believe the claims presented, it means the people they trusted were laughing at them.
- Emphasizing the need for Republicans to acknowledge that they were tricked and played by those they trusted.
- Suggesting that Republicans should critically analyze politicians echoing Trump's claims and question their motives, whether they were manipulated by Trump or are using the claims for their own benefit.
- Urging Republicans to discern between those who genuinely believed Trump and those who knew he was lying but used it as a path to power.
- Encouraging Republicans to identify who they can trust moving forward and be wary of individuals who may betray their trust.
- Stating that there is no evidence to support the belief that Trump acted in good faith during the election aftermath, indicating his primary goal was to stay in power regardless of the consequences.
- Implying the importance of seeking clarity and potentially reassessment of beliefs in light of presented evidence.

### Quotes

- "You have to acknowledge that you got tricked. You got played."
- "You have to figure out who's going to stab you in the back."
- "There's nothing to suggest that he was doing anything other than trying to stay in power by any means necessary."

### Oneliner

Addressing Republicans about acknowledging being tricked and played by those they trusted, urging critical analysis of politicians echoing Trump's claims to avoid being manipulated or betrayed.

### Audience

Republicans

### On-the-ground actions from transcript

- Question politicians echoing Trump's claims in primaries, assessing their motives and actions (suggested)
- Identify trustworthy individuals in political spheres and be cautious of potential betrayal (implied)

### Whats missing in summary

Exploration of the emotional impact on Republicans realizing they were manipulated and the need for critical thinking moving forward.

### Tags

#Republicans #Trump #Election #CriticalAnalysis #PoliticalTrust


## Transcript
Well, howdy there, internet people. It's Beau again.
So today,
we're going to talk directly to the Republicans.
We're going to talk about what the Republican takeaway
from the hearing should be.
You know, there's a lot of people
who are liberals who have a
very high expectation
that the hearings are somehow going to convert you into a liberal.
It's not going to happen, right? You know it and I know it.
That isn't going to occur.
You're not going to suddenly become a Democrat
over this, right?
So,
what's your takeaway from it?
When you look at the evidence that's been presented,
when you
listen to that audio
of Bannon saying, hey, this is what Trump's going to do.
This is how he's going to trick everybody.
This is how he's going to cause this mess.
Laughing at you.
Make no mistake about it.
If you believe to these claims,
the people you trusted
were literally laughing at you.
That audio
and then the outtakes
from after the 6th.
These outtakes from the 7th
where he's saying he doesn't want to say the election's over.
Even though at that point he knew.
Between that you have to know.
You have to know you got tricked, right?
Somebody you trusted abused your trust.
It happens.
And
those pieces of evidence,
that's not
anybody you can deflect from.
This is them.
Their words, their faces.
It's them.
If
all of the failed lawsuits,
the total lack of evidence,
the investigations that turned up nothing.
If that didn't convince you,
certainly
their own words should.
You have to acknowledge
that you got tricked.
You got played.
Okay.
Fine, you acknowledge it. Now what?
You're not a liberal, right?
That didn't change your views
on the state of the world.
So the Republican takeaway
isn't vote blue no matter who, right?
But what should it be?
To me,
the number one thing
that Republicans
who believed this,
who bought these claims,
or Republicans in general,
the one thing they need to be doing is looking at everybody
who's, who
ran in the primaries
echoing Trump's claims.
Those Republicans you're about to be asked to vote for.
You need to look at them.
Did they get up there and question
the election?
Did they repeat his claims?
Did they push that information out there?
After all of this time,
just to keep the fundraising money going,
to keep tricking you,
you have to look at them
and you have to ask yourself,
was that person, was that candidate,
were they tricked to themselves?
Or are they also laughing at you
behind your back?
Because it's one of the two.
Either
Trump manipulated them
the same way he did you,
or
they're in some room
laughing
at all of the money you're sending them.
Laughing at the trust
they can use however they want.
It's one of the two.
And I'm sure,
and I mean this,
there are probably
cases of both.
There are probably people
who truly believed Trump
who are now running for office.
And there are probably people
who knew
he was lying the whole time
but didn't care
because it was their gateway to power.
It was their gateway
to get you to trust them
so then they could play you.
If you're a Republican, you have to figure out who is who.
You got to figure out who's going to stab you in the back.
I don't envy the task
because
it's probably going to be hard.
But that's what you have to do.
At this point, you've acknowledged it.
And if you haven't acknowledged it,
with everything that has been shown so far,
if you still believe
something happened with that election,
if you still believe
Trump was acting in good faith,
you're uh...
you probably need to talk to somebody
because
there's nothing to support that.
There's nothing to suggest
that
he
he was doing anything
other than trying to stay in power
by any means necessary.
And that included
betraying you.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}