---
title: Let's talk about Day 8 of the hearings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wJfsTMFeL6g) |
| Published | 2022/07/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recap of day eight of the hearing focusing on the case against the former president's lack of leadership and initiative.
- Building a case suggesting the events were premeditated and Trump was in dereliction of duty.
- More hearings coming in September with new information and sources.
- Main focus on Trump's inaction during the Capitol attack.
- Detailing how easy it was for Trump to address the crowd and stop the violence.
- Secret Service agents attached to Pence were in fear for their lives.
- Testimonies backing up Hutchinson's account of Trump's behavior in the motorcade.
- Committee focusing on disproving minute details to discredit the entire hearing.
- Pence portrayed as taking action while Trump did nothing.
- Outtakes reveal insincerity of Trump's remarks and reluctance to condemn his supporters.
- Republicans challenged by continuing to support baseless election claims.
- The hearing aimed to show the premeditated nature of the events and Trump's involvement.

### Quotes

- "Main focus on Trump's inaction during the Capitol attack."
- "Pence portrayed as taking action while Trump did nothing."
- "Republicans challenged by continuing to support baseless election claims."

### Oneliner

Recap of day eight hearing focusing on Trump's inaction, Pence's actions, and the premeditated nature of the events.

### Audience

Committee members, concerned citizens

### On-the-ground actions from transcript

- Reach out to Department of Justice to support the investigation (implied)
- Be willing to talk to the committee if you have relevant information (implied)

### Whats missing in summary

Insights into the specific testimonies and evidence presented during the hearing.

### Tags

#CapitolAttack #Trump #Pence #Hearing #Inaction #Preplanned #Justice


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about day eight of the hearing and what happened.
So,
this is a kind of a recap.
We will be going into some of the topics we're about to go over in depth.
There will be more videos this weekend.
Okay, so this was definitely the
tell them what you told them phase. They stuck to the template.
Tell them what you're going to tell them.
Tell them what you told them.
They tied everything together,
added some new details, reiterated
the former president's
lack of
leadership,
initiative,
caring,
and illustrated it pretty well.
They're building a case, demonstrating a case that suggests
it was premeditated
and
at the same time suggesting that
Trump
was in dereliction of duty.
Which, yes.
Okay,
so
to get to the recap,
first thing is this isn't the end.
There are more hearings coming in September.
So there will probably
be another set of hearings
that very much matches this one
with the new information,
with new
sources
that have kind of
appeared since
these hearings.
This was shaking the tree a little bit.
So there will be another set.
It will probably function very much the same way.
Tell them what you're going to tell them.
Tell them what you told them.
You may get a
tell them what you already told them phase before then
and then start the new stuff.
Main focus was on Trump's inaction,
the fact that he did nothing,
nothing
for hours
trying to rein in his people.
And he had the ability to do so.
They demonstrated
how easy it would have been
for him to
get on TV
and address the crowd.
They illustrated
that
the crowd was in fact reading his tweets and
viewing them as orders in a way.
And they highlighted
that
he
he did not seem
as though he wanted it to stop.
They went into
a lot of detail
about the Secret Service agents who were attached to Pence,
went into
their state of mind
in their actions.
There's going to be a whole other video on this
because there's already a lot of questions that have come in.
But generally
they were
in fear.
They were afraid
to the point
that they
reportedly
were saying goodbye.
It takes a lot to get to that point.
They introduced more testimony backing up Hutchinson
in regards
to the exchange in the motorcade,
which, once again, I would like to point out
the only reason
they're having to talk about this
temper tantrum
is because thus far
this is the only thing
that the Republican Party
has felt that they could dispute
is whether or not
he lunged at a driver,
whether or not he really acted like a child
in the motorcade.
There were two
witnesses
who were able to provide confirmation
of different elements of the story.
So
there's that. But again,
that's a red herring.
It's completely irrelevant
to the actual
purpose of this hearing
to determine how severe
Trump's temper tantrum was.
That doesn't matter.
But
with the way the Republican base is,
if the
Republican leadership
can find some way
to
disprove
any little minute detail,
well, that's the window to discredit the entire thing.
So while it is annoying to me
that the committee is focusing this much time on this,
I understand why they're doing it.
It's probably the right move,
even though it
bothers me personally.
They also painted Pence
as somebody who was acting,
who was reaching out,
giving orders,
trying to get it stopped.
While
the president
did nothing.
There will be more questions about that
as far as
Pence giving orders
in the future, I'm sure,
because there's still an unresolved thing there.
In some of the instances,
Pence might not have really
had the authority
to give certain orders.
I think that it's understandable
why he did, though.
The outtakes,
you know,
on the 7th,
the day after all this happened,
Trump,
he
gave some prepared remarks.
The outtakes
came into the committee's possession,
and they show exactly
how insincere
the former president was,
how he didn't want to come down too hard and be too mean
to the people he sent down there
while he was disavowing them.
And he didn't want to admit that he had lost the election still,
even though
all of his aides at this point
had told him that.
And
they took some shots
at Republicans
who, even after this event,
continue to challenge the election.
There's also going to be a whole other video
on this and one on the outtakes.
The fact that
many Republicans
who are fully aware
that his claims were baseless,
were fully aware
that he set a lot of this in motion,
they still continue to parrot his claims.
And that's something that
the Republican Party is going to have to address.
So that was the general content,
but the whole point of this
was to show
that the plan
to do this
originated before.
It was a pre-planned event.
And they tied it back to Trump knowing his claims were false.
They tied it back to
him engineering
the whole thing.
And it was all a very much,
very much a
tell them what you told them thing.
So a lot of people
felt like it was a rerun.
There weren't a lot of,
you know, giant bombshells in this one,
other than the outtakes.
That was big.
The radio traffic from the Secret Service, that was big.
But I don't know that any of this was unexpected.
So that's
what you missed if you missed it last night.
During this break,
I would imagine that we'll see a lot of
movement from the Department of Justice.
And we'll probably get a lot of leaks about people who are now willing to talk to the committee.
Because at this point,
anybody with a,
anybody with a lawyer
should know where this is headed.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}