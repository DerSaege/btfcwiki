---
title: Let's talk about planes, trains, and ethical consumption....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BRZQ1BZ3ddo) |
| Published | 2022/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two groups planning a trip, one choosing planes and the other trains due to environmental concerns.
- The debate arises about the impact of individual actions versus larger corporate decisions.
- Four friends who saved up for four years to go on a trip together.
- Even if the four friends take the environmentally friendly option, the plane will still fly.
- Beau criticizes the defeatist mentality that individual actions don't matter.
- Points out that collective individual actions can lead to broader trends and influence corporations.
- Emphasizes that while the four friends' choice may not change anything, widespread changes can occur with more people making environmentally conscious decisions.
- Cultural shift is necessary to influence companies' practices.
- Capitalism prioritizes profit over environmental impact.
- Companies will only change practices if it becomes unprofitable to continue environmentally harmful actions.

### Quotes

- "Every journey begins with a single step."
- "If it's profitable, they'll do it. If it doesn't make money, well, they won't."
- "Cultural shift is necessary to influence companies' practices."

### Oneliner

Two groups plan a trip, one chooses planes, the other trains for environmental reasons; individual actions can spark broader change, influencing corporations and cultural shifts.

### Audience
Environmentally conscious individuals

### On-the-ground actions from transcript
- Choose environmentally friendly travel options (exemplified)
- Encourage others to make environmentally conscious decisions (implied)

### Whats missing in summary
The full transcript delves deeper into the debate between individual actions and corporate responsibility, providing insights into the importance of collective behavior change in influencing broader trends and company practices.

### Tags
#EthicalConsumption #EnvironmentalImpact #CollectiveAction #CorporateResponsibility #CulturalShift


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about ethical consumption
and the environment and your individual actions versus what
corporations do.
And this all gets brought up because two of you
are planning a trip.
I'm not going to recount the whole story.
But short version, there are four people
who have been saving up for four years
to do a trip together.
They started saving in their freshman year of college.
They're getting ready to do it.
And two of them want to take planes and travel via air.
And the other two want to take trains
because they are far more environmentally friendly.
And this prompted the discussion about the fact
that even if they decide to take the trains,
that plane is still going to fly.
And that the individual action doesn't
matter because it really doesn't cut down on any emissions.
And I understand this, and this is true.
If these four people decide to take a train,
the flight that they would have got on, it's still going to fly, right?
The problem with this way of thinking is that A, it's very defeatist.
There's nothing you can do.
And B, it doesn't take into account trends and individual action
that leads to wider trends among the community,
and those being building blocks.
If the four of you decide to get on a train, it doesn't matter.
It's not going to change anything.
However, if a whole bunch of people
begin making the environmentally conscious decision on their own,
well, then it does matter.
Because if it's not four people that
would have been on the 7 p.m. flight out of town if it's 50 well then that flight
doesn't move it makes it less profitable for the company therefore the company
cuts flights now you can't organize something like this it's a cultural
shift, the talking point of there's nothing you can do stops that cultural
shift from happening. These four people taking this trip via rail, it doesn't
change anything, but if they set that tone and other people follow suit, it does.
Every journey begins with a single step and all of that stuff.
The way capitalism functions and the reason the environment is in so much trouble is because
if it's profitable, they'll do it.
It doesn't matter what havoc it causes.
If it makes money, the companies will do it.
But the inverse of that is true as well.
If it doesn't make money, well, they won't.
So there's no right answer here because both points of view are actually valid.
It's not going to change anything in the immediate.
It's not going to stop that plane from taking off.
But if it sets the tone over time, it does reduce things.
Now I can't help with the ethical debate there.
Both points of view are correct.
That's something y'all are going to have to argue about.
I can kind of help with something else though.
way I read that message was that this whole thing is about an adventure. Cool. I'd
probably take the train. Probably have a lot more fun that way. Anyway, it's just a
thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}