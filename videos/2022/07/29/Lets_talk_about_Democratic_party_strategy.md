---
title: Let's talk about Democratic party strategy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_2EbROpSQdA) |
| Published | 2022/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Differentiates between tactics and strategy within the Democratic Party.
- Warns against elevating the MAGA faction of the Republican Party as a long-term plan.
- Criticizes the Democratic Party for lacking a long-term strategy and only focusing one election ahead.
- Points out the importance of preventing DeSantis from getting reelected as governor to hinder his potential presidential run.
- Urges the Democratic Party to think in a longer timeframe and develop and implement policy effectively.
- Suggests that the Democratic Party should establish clear progressive positions for members to adopt.
- Emphasizes the need for consistent messaging and cheerleaders within the Democratic Party.
- Advocates for a stronger ground game at the state and local levels for the Democratic Party.
- Stresses the importance of converting popular ideas into successful legislation for the Democratic Party's platform.

### Quotes

- "It's probably not a great idea to elevate the MAGA faction of the Republican Party."
- "The Democratic Party is worried about DeSantis in 2024."
- "The Democratic Party needs to come up with a sign you see in front of a roller coaster."
- "Nobody runs unopposed."
- "They don't have consistent messaging because they don't have consistent policy."

### Oneliner

Beau delves into the Democratic Party's short-term focus, lack of strategy, and the necessity for consistent policy and messaging to solidify its position.

### Audience

Political strategists, Democratic Party members

### On-the-ground actions from transcript

- Ensure no Republican candidate runs unopposed, even at the local level (suggested).
- Strengthen the ground game at the state and local levels for the Democratic Party (implied).
- Establish clear progressive positions for Democratic Party members to adopt (implied).

### What's missing in summary

Beau's detailed analysis and recommendations for the Democratic Party's long-term strategy and policy implementation are worth exploring further in the full transcript. 

### Tags

#DemocraticParty #PoliticalStrategy #Messaging #ProgressiveIdeas #PolicyImplementation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the Democratic Party's tactics and strategy.
Because after that video mentioned what they
may have done in Maryland to help elevate
a less than electable candidate on the Republican side,
a lot of people asked if I thought that was a good idea.
idea. It's a tactic. It's not a strategy. It's something that can be used at times
but not always. Do I think it was a good idea in that situation? I mean probably
Hogan's candidate? I mean that would have been a tough fight. A moderate in
in Maryland versus a far-right, quote, Q-whack job.
In that case, it was probably a good move.
Is it a good move normally?
It's probably not a great idea to elevate the MAGA
faction of the Republican Party.
You give them name recognition.
They could come and run again later.
It promotes the idea that that is how you win primaries.
There's downsides to it.
It's a tactic.
It's not an overall strategy.
And that's actually the Democratic Party's problem.
They don't have a long-term strategy.
Not really.
The Democratic Party tends to focus one election ahead,
at most too. They don't think three or four presidential elections into the
future. A good example of this, you know, over on the other channel, The Roads with
Bo, I talked to Nikki Fried, who is running to be governor in Florida, and in
In that conversation, we talked about what happens if DeSantis loses his reelection bid.
The Democratic Party is worried about DeSantis in 2024.
You know how you stop him from running for president?
Make sure he doesn't get reelected as governor because then the Republican Party knows he
can't carry Florida.
If he can't carry Florida as governor, won't carry Florida as a presidential candidate,
can't win, right?
Whoever wins the Democratic primary in Florida should be showered with cash, tons.
Even if they end up losing, it eats into DeSantis' war chest, you know, because he has stacked
away some serious cash.
That should be a goal.
The Democratic Party should already have plans for this, to do this.
And I doubt that they do.
And this is a good example of why what they did in Maryland is a good tactic and not a
strategy.
If you were to apply that to what's going on here, right, you have DeSantis and Trump
facing off.
The reality is, if you're being objective, DeSantis is smarter.
He's more polished, objectively a better leader.
Now I'm not talking about my personal views, I think he's leading in the wrong direction.
But if you compare them to Trump, the takeaway is that DeSantis is the stronger candidate.
Do you elevate Trump?
That wouldn't make sense.
That move is a tactic.
It's not a strategy.
It's not something they can apply across the board.
So they need to start thinking in a longer timeframe.
Aside from that, they have to develop and implement policy.
This is hard.
The Democratic Party is a coalition.
It's not a bunch of people who want the status quo like the Republican Party.
The Democratic Party is full of a bunch of people who are progressive to some degree,
far more than others, right? The Democratic Party needs to come up with a
like a sign you see in front of a roller coaster. Must be this tall to ride, must
be this progressive to ride. If you want to be part of the Democratic Party, you
have to adopt these positions. If you want any help from the Democratic
machine, you have to have these positions and they need to use that. And then they
use their time in office to implement that policy. Democratic positions
are popular. They're incredibly popular. The problem is the Democratic Party
isn't great at actually converting the position into law. So even people who
align with the Democratic Party. They're not enthusiastic about voting necessarily
because are they really going to do anything if they get into office? That's
the way they look at it. The other thing is that the Democratic Party should
probably look at state and local positions a whole lot more, especially
with everything that's going on with the current Supreme Court, there is going to
be a lot of key fights that are decided in state and local positions, not in DC.
And I don't know, this may just be the area I live in. The Democratic Party
really doesn't have a strong ground game. I think it would be important to set
the idea that nobody runs unopposed. No Republican candidate anywhere runs
unopposed. It doesn't matter if it is the Reddus County in the Reddus state for
dogcatcher. Nobody runs unopposed. If you want to run for the Republican Party
you're going to spend some money. That would be a smart move. It would also
help create a lot more engagement. The last thing that I think the Democratic
Party is kind of missing out on, they don't have consistent messaging because
they don't have consistent policy, right? But beyond that, they don't have people
to carry it forward. They don't have cheerleaders. They don't have people who
are running their podcasts pushing the Democratic Party platform. In fact, some
of the people who speak speak highly of the Democratic Party aren't even
Democrats, but they're trying to explain what's going on. The Democratic Party
doesn't have the equivalent of Fox News, of the podcast community to help carry
those progressive ideas out there because there isn't consistent policy,
there can't be consistent messaging. Those are the things that the Democratic
Party needs to work on. That's as far as strategy, this is what they need.
The hardball tactics that they're starting to use, good, but without a
consistent strategy to back it up it it's not sustainable and the big part of
this is taking those popular ideas and converting them into successes when it
comes to legislation and make sure that your platform the platform that's
developed make sure that it's expansive there's a bunch of things don't just
Just make it a few big ticket items because if you end up in a situation where you can't
get those big ticket items passed, well you're out of luck.
You're done.
Your candidate looks like a failure.
However, if there's a bunch of things, I'm going to do these 185 things and a hundred
of them are simple things.
messaging can be, we succeeded at these hundred items, not we failed at these
six. But the big hang-up is the lack of an overall strategy because of the lack
of consistent policy and messaging. If you want to fix the Democratic Party,
that's where you need to start. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}