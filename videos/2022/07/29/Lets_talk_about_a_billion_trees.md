---
title: Let's talk about a billion trees....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=plB4iiTTlWY) |
| Published | 2022/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Government program to replant areas damaged by wildfires due to climate change and frequency of fires.
- Backlog of 4.1 million acres needs to be replanted, but only 60,000 acres are done in an average year.
- Administration aims to increase replanting to 400,000 acres annually, but this won't catch up to the 5 million acres burned on average every five years.
- The administration plans to spend $100 million next year on replanting efforts, with a potential increase to $260 million annually.
- Replanting trees is vital as they serve as a carbon sink.
- The goal is to not just catch up but get ahead in replanting efforts to combat the effects of wildfires and climate change.

### Quotes

- "It's really important that we do."
- "It looks like they're just starting off slow, but they already have the plan to get it to a point where it actually matters."

### Oneliner

Government aims to combat wildfires and climate change by increasing tree replanting efforts, from a backlog of 4.1 million acres, to 400,000 acres annually, with plans to spend up to $260 million yearly.

### Audience

Climate activists, environmentalists

### On-the-ground actions from transcript

- Support tree planting initiatives in your community (exemplified)
- Advocate for increased funding for tree replanting efforts (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into the importance of tree replanting efforts to combat wildfires and climate change, showcasing the government's plan to increase funding and acreage for this vital initiative.

### Tags

#TreeReplanting #ClimateChange #Wildfires #EnvironmentalAction #GovernmentInitiative


## Transcript
Well, howdy there, internet people.
Let's go again.
So today, we're going to talk about trees, planting trees,
lots of trees, a whole lot of trees, 1.2 billion trees.
OK, so wildfires, they damage trees, right?
Most times, the area will regrow on its own.
Sometimes, the fires are so bad, burn so hot,
that for lack of a better word, it pauses regrowth.
It stops it.
For a while, the government has had a program
to go in and replant these areas.
The thing is, because of climate change,
because of the frequency of the fires
and how destructive they have been, a backlog
has developed of about 4.1 million acres
that needs to be done.
In an average year, they replant about 60,000 acres.
The administration wants to up that just a wee bit,
wants to up production.
And they want to bring that up to 400,000 acres.
That's good.
That's good.
It's aggressive.
It's ambitious.
It's a move in the right direction, absolutely.
There is one slight issue with it.
Even at 400,000 acres a year being replanted,
there's a gap that's still going to have to be filled.
Because over an average five-year period,
about 5 million acres get burned.
So if 5 million acres are being burned,
even though a lot of that's going to regrow by itself,
the 400,000 isn't really going to eat into that backlog.
They're going to stop falling behind,
but they're not really going to get ahead on this.
And this is something that, because it is a carbon sink,
it's important to get a move on.
The good news is that the administration
appears to understand this.
Next year, the year they plan to do 400,000 acres,
they plan on spending about $100 million to make it happen.
It looks like that number will steadily increase,
and they will get more and more budget to plant the trees.
And they're expecting it to cap out at around $260 million
a year.
And if you do the math on it, you'll
find out that's going to put them ahead.
It'll get them to where they are not just playing catch up,
but they're actually getting ahead.
So this is one of those things it will probably
be talked about, and this may even
be one of those things that people
try to turn into a talking point.
Why are we spending so much money on doing this?
Because it's really important that we do.
But those are the numbers behind it.
It's why it's happening.
And this is one of those things where
it looks like they're just starting off slow,
but they already have the plan to get it to a point
where it actually matters.
With a lot of stuff that deals with the climate,
there's the token response.
In this case, it looks like they've already earmarked
the money to get it to a real response.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}