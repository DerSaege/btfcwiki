---
title: Let's talk about Republicans and public education....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C4p6wFaPquk) |
| Published | 2022/07/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's former Secretary of Education suggested abolishing the Department of Education, leading to questions about the Republican Party's stance on education.
- Republican Party relies on an uneducated populace to avoid defending its historical positions.
- Areas leaning Democratic tend to have longer lifespans due to better healthcare policies, contrasting with Republican states.
- Lack of education allows the Republican Party to avoid answering for policies that harm their base, such as healthcare.
- Republican voters often support policies that may harm their own future, like environmental issues, due to lack of critical thinking.
- The party discourages critical thinking and history education to maintain control over their base.
- Republicans prefer obedient, uneducated followers over an educated populace that questions policies.
- The party thrives on conspiratorial thinking and convincing their base to ignore facts.
- Republicans aim to keep their base in line and discourage independent thinking through anti-education stances.
- The Republican Party's position on education is driven by a desire for compliant followers rather than an informed electorate.

### Quotes

- "They love the uneducated, and they told you that."
- "Republican Party relies on an uneducated populace to avoid defending its historical positions."
- "They have to embrace conspiratorial thinking."
- "The party discourages critical thinking and history education."
- "Republicans prefer obedient, uneducated followers."

### Oneliner

Trump's ex-Secretary's push to abolish education prompts scrutiny on GOP's anti-education stance; they thrive on an uneducated base to avoid accountability for harmful policies and discourage critical thinking.

### Audience

Voters, educators, activists

### On-the-ground actions from transcript

- Challenge anti-education policies and advocate for critical thinking in schools (exemplified)
- Support educational initiatives that encourage questioning and independent thinking (exemplified)
- Engage in community education efforts to combat misinformation and encourage informed decision-making (exemplified)

### Whats missing in summary

The full transcript provides a deeper dive into the Republican Party's motivations and strategies for maintaining power through anti-education stances. Viewing the full transcript offers a comprehensive understanding of the party's approach to education and governance.

### Tags

#RepublicanParty #Education #CriticalThinking #Conspiracies #Voting


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about the Republican Party
and their position on public education.
We're going to do this because, if you don't know,
Trump's former Secretary of Education advocated
that the Department of Education be done away with.
And obviously, this has prompted a lot of questions,
basically summed up by,
why is the Republican Party at war with education?
I mean, they kind of have to be, don't they?
Sure, the whole, I love the uneducated quote,
that's a Trump thing.
But for the Republican Party as a whole,
making sure that the American people are uneducated,
I mean, that's a matter of survival for the Republican Party.
Because if you have an educated populace,
well, they're going to ask questions.
And then the Republican Party is going to have to defend
its historic positions.
And they can't.
So they have to embrace a lack of education
and conspiratorial thinking.
Historic positions.
Recently, I did a video that highlighted the fact
that if you live in an area that is predominantly Democratic,
it leans towards the Democratic Party,
that you're going to live longer than a person
who lives in an area
that leans towards the Republican Party.
Now, you can chalk a lot of that up to just lifestyle choices,
right, but it also has to do with the fact
that Republican states didn't really go in on health care.
When all that legislation got passed,
blue states, they went in and expanded on it.
They made sure it was available.
The Republican Party pushed back against it,
scared their base, told them there'd be death panels.
And here we are later, and it's the Republican Party.
It's the areas that don't have those policies
that have shorter lifespans.
If they had an educated populace,
they would have to answer for that, right?
But right now, in Texas, guaranteed,
when Election Day comes,
there's going to be some rancher
who climbs into their giant diesel truck
that they use as a personal vehicle,
screaming, drill, baby, drill,
and heads down to the polling place,
where they're going to vote a straight Republican ticket,
completely unaware of the fact that every time they do that,
they're putting another brick in the wall
that separates their grandkids from that ranch.
With the drought that's going on,
an educated populace, they would ask questions
about how that's tied to the climate
and how that's tied to emissions.
The Republican Party doesn't want that.
They don't have to answer for that.
What they want is good little worker bees.
They want people who know to stay in their place,
to do what their bettors tell them to.
You don't have to be educated for that.
So that's why they're pushing back against education.
That's why they don't want critical thinking.
That's why they don't want history taught,
because you can learn from history.
It's a defense mechanism.
They have to embrace conspiratorial thinking.
They have to convince their base
to ignore what they see with their own eyes.
They have to convince their base
that being smart, being educated, is bad,
and that what you really need to do is stay in your place.
And they're doing a really good job of it.
That's why the Republican Party has the stance it does
when it comes to education.
They love the uneducated, and they told you that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}