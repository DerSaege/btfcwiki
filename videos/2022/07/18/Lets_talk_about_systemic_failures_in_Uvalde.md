---
title: Let's talk about systemic failures in Uvalde....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rqmPXdWls2E) |
| Published | 2022/07/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commentary on report from Texas on systemic failures.
- Report deflects responsibility and hinders accountability.
- Mention of historical rulings impacting law enforcement's duty to protect the public.
- Link between systemic failures, cultural failures, and individual failures.
- Criticism of law enforcement's shift from "protecting and serving" to mere law enforcement.
- Describes the failure of 376 officers to handle a situation involving one armed individual in a school.
- Comparison made to a successful military operation involving far fewer personnel.
- Emphasizes the need for legislation to enforce immediate protection obligations.
- Suggests pushing for legislation as a litmus test for law enforcement's priorities and accountability.

### Quotes

- "Without accountability, there's no change."
- "This report and the way it defuses responsibility means that nothing is going to change."
- "I think that might be a good little litmus test."
- "Most of them will do everything they can to stop that statute from existing."
- "Y'all have a good day."

### Oneliner

Beau sheds light on systemic failures in law enforcement, pointing out the hindrance to accountability and the need for legislation to ensure immediate protection of children in schools.

### Audience

Law enforcement reform advocates

### On-the-ground actions from transcript

- Advocate for legislation enforcing immediate protection obligations (suggested)
- Push for laws holding law enforcement accountable for protecting children in schools (suggested)

### Whats missing in summary

Insights into the current state of law enforcement accountability and the importance of legislative action to address systemic failures and ensure public safety.

### Tags

#LawEnforcement #Accountability #SystemicFailures #Legislation #PublicSafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about that report a little bit.
We're going to talk about that.
Because I've had people ask why my commentary of the
revelations coming out of Texas, why it stopped.
And the simple answer to that is, I try to keep this
channel suitable for everybody.
And most of my commentary has not
been fit for public consumption.
But that report has come out and says
that there were systemic failures, Yuvaldi.
Yeah, I mean, that's true.
That's true.
And that report, it's part of those failures.
It is part of those failures because it
defuses responsibility. I mean, let's be honest for a second, there were officers
on scene immediately. That's not a failure, that's ideal, and it broke down
from there, right? But the report casts a wide net and defuses responsibility. That
That report is part of the problem because it hinders responsibility and accountability.
Without accountability, there's no change.
There were systemic failures.
There were cultural failures.
There were individual failures that day.
To me, the systemic failures started in 1981 with Warren versus D.C., a ruling that said
cops, they don't have a duty to protect the general public. It was reinforced in
1989, DeShaney versus Winnebago County. The Supreme Court got in on it. Cops don't
have a duty to protect the general public unless some form of special
relationship had been formed, like a prisoner in custody. Other than that, they
don't have a legal obligation. Now I would suggest that students attempting
to fulfill state mandated educational requirements, I would suggest that that
special relationship exists. But what do I know, right? Maybe that needs to be
written down somewhere. So there are systemic failures. There is no duty to
protect. And that systemic failure, that created a cultural failure. Cops aren't
there for the idea of protecting and serving anymore. It's a law enforcement,
right? They feel no duty to the public, no obligation to the public. So you get
statements like, I'm going home at the end of my shift. Right? And that cultural
failure leads to individual failures. And by that in this case, I mean every
single person with a firearm who was present. According to that report, almost
400 cops, 376 responded. Two companies, and they couldn't take back a school from one
person because they didn't fill an obligation to, because they didn't have a duty to,
If you look at that footage, look at the cops outside, and that pretty much says everything.
They're facing the wrong way.
They're looking out at their enemy, the public.
376, two companies. For comparison, Operation Neptune Spear, that was the
raid to snatch or otherwise remove the most wanted man on the planet. You know how
many operators were involved in that? About 80. Now don't get me wrong, I
understand that the cops there in Texas are not as well-trained. I get that. But
the cops there in Texas were also not involved in an operation to snatch the
most wanted person on the planet in a non-permissive environment at night. They
They just had to deal with one person with a rifle in a school.
A person that, according to reporting, had 315 rounds in that school.
Live and fired, when it was all said and done, 315 rounds in there.
376 officers.
They had enough people to pull a Zap Brannigan offensive.
This report and the way it defuses responsibility means that nothing is going to change.
This is going to happen again.
It will happen again.
And I guess the only thing that would change it is for legislation to be put forward to
dispel out the legal obligation to act immediately to protect kids in a school.
I think that's probably the only thing that would change it because apparently they literally
need that written down.
And here's the thing, if you want to know about the state of law enforcement in your
area, push to get that statute written and see whether or not the cops in your area oppose
it.
Because I'm willing to bet most of them will.
Most of them will do everything they can to stop that statute from existing because, again,
it creates responsibility and therefore accountability.
I think that might be a good little litmus test.
how things really function. And whether or not the law enforcement in your area
is worth anything. Whether or not they oppose the idea of having a
responsibility to save kids. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}