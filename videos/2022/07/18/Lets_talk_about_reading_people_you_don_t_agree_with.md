---
title: Let's talk about reading people you don't agree with....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bgvrdaYJbf0) |
| Published | 2022/07/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Encourages exercising the mind by stepping outside your comfort zone and reading works of people you don't agree with.
- Notes the importance of being able to entertain an idea without fully accepting it as a sign of a well-educated mind.
- Shares a message he received about recommending Emma Goldman, a philosopher he doesn't agree with entirely.
- Explains the value of exposing oneself to new ideas and perspectives, even if they conflict with your own beliefs.
- Talks about Emma Goldman's revolutionary mindset and the reasons behind her opposition to women's suffrage.
- Mentions that reading and engaging with diverse viewpoints is necessary for personal growth and intellectual development.
- Emphasizes the importance of challenging and examining ideas that may not line up with your own beliefs.
- Suggests that true intellectual growth comes from engaging with differing opinions and testing them against practical contexts.
- Advocates for thinking beyond conventional boundaries and limitations when forming and evaluating ideas.
- Concludes by underlining the significance of engaging with diverse perspectives to evolve and pursue knowledge and truth.

### Quotes

- "The way to have an educated populace or the way to have an exercised mind is to read the opinions of those that you don't necessarily agree with."
- "Your ideas should never 100% reflect anybody else's."
- "The only way you're going to find out they're wrong or they're right is if you're talking to other people who disagree."
- "If you only read things that you agree with 100%, you will never grow as a person."
- "That is how we evolve."

### Oneliner

Beau encourages intellectual growth through engaging with diverse viewpoints and challenging one's beliefs to evolve and pursue knowledge and truth.

### Audience

Intellectuals, students, educators

### On-the-ground actions from transcript

- Read works of authors and philosophers you may not necessarily agree with (exemplified)
- Engage in respectful debates and dialogues with individuals holding different perspectives (exemplified)
- Challenge your own beliefs by considering opposing viewpoints (exemplified)

### Whats missing in summary

The full transcript provides a detailed exploration of the importance of intellectual growth through exposure to diverse viewpoints and the need to challenge one's beliefs for personal evolution.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about exercising your mind,
stepping outside your comfort zone,
reading people, reading the works of people
that you don't necessarily agree with,
and how to handle it if you are reading something
and you find ideas that conflict with your own.
One of the hallmarks of a very well-educated mind
is the ability to entertain an idea without accepting it.
Let's just say that it's true and approach it
from a position that you don't actually believe
and kind of entertain it and look at that idea
and examine it for its worth in its best context
and go from there.
Reason we're going to talk about this
is because I got a message from somebody who I guess,
at some point during a live stream, somebody asked, hey,
give me some philosophers to read.
And one of the ones that I named was Emma Goldman.
And they sent me this message.
And they read a lot about her and some of her works.
And they sent me an itemized list
of places where we disagree.
And the question is, why recommend somebody
that you don't agree with on everything?
Well, if you only read things that you agree with 100%,
you will never grow as a person.
You'll never be exposed to new ideas.
And it's also important to note that when
you're talking about Emma, she was born in 1869-ish,
somewhere in there.
Yeah, we're going to disagree on stuff.
Just through the passage of time, things will change.
Emma was also a revolutionary.
She had a revolutionary mindset.
She was a radical.
When people talk about the radical left today, yeah,
they really shouldn't be picturing Nancy Pelosi.
They should be picturing her.
Right?
Two of the things that were in this list, one of them
was Emma was opposed to women's suffrage.
Yes, but also no.
She was.
She was, but not for the reason you might imagine.
Her opposition to women voting, women holding office,
all of that had nothing to do with the abilities of women.
Her position was that, A, women could just as easily
be bought or be corrupted, and that, B,
the process of voting itself was an integral part of a system
that she saw as inherently unjust.
So she wasn't really opposing women's suffrage.
She was opposing the entire system
and using that as a catalyst to talk about it.
And then her opposition to voting
was one of the other things that showed up in that list.
And whether you realize it or not,
there's a whole bunch of people who are watching this video
who have probably never heard of her.
You're probably familiar with her ideas on voting, though.
If voting changed anything, they'd make it...
If you know the next word's illegal, yeah, that's Emma.
It's an idea that has been paraphrased and reshaped
over and over and over again, right?
If it changed anything, they'd make it illegal.
Okay. What if she was right about that?
I know, right?
What if she was right?
When people point to this, they make the assumption
that she is correct in this belief.
If it changed anything, they'd make it illegal.
The normal take from this is,
well, you shouldn't vote because it doesn't change anything.
Right, but please pick up a newspaper or go to your favorite news outlet.
They are trying to make it illegal.
They are.
They're trying to water down the vote.
They're trying to suppress the vote in any way that they can.
They're trying to make it as difficult as possible to vote.
Because while it would never...
Voting would never produce the giant shift that Emma was looking for,
it can produce change, just not on the level she wanted.
But even that small amount of change that is possible through the ballot,
they're trying to take that away as well.
So her idea of if it changed anything, they'd make it illegal.
Well, they are trying to make it illegal.
They're trying to depress it.
They're trying to make sure it's as hard as possible
because it does change something.
So while you may look at that as a disagreement,
I mean, what she said, I mean, look around.
The issue there is that Emma was of a revolutionary mindset.
She wanted an entirely different system,
one that could definitely never be voted in, right?
At least not within her lifetime.
I mean, if everything went right for thousands of years,
sure, it could be voted in.
But that was her view, not looking at it from a reform standpoint
or a hold the line standpoint or anything like that.
So I don't think that it's important to only read people you agree with.
In fact, I think that's really bad.
I think that you can find observations that are just, they're true.
They're pure truth in people that not just do you not agree with on everything.
They may be people that you don't agree with on anything else.
The way to have an educated populace or the way to have an exercised mind
is to read the opinions of those that you don't necessarily agree with.
Challenge them, accept them, play with them, flush them out.
See if they make sense in an ideal context.
See if they hold up to the practical world and go from there.
Your ideas should never 100% reflect anybody else's.
If you do, you're probably not thinking as much as you should
because you should have, you should hold an idea of your own that is true
and an idea of your own that's wrong because nobody's right at all times.
But you should be thinking about things outside of that normal framing,
that normal set of limitations that is put on you.
You should be thinking outside of that.
And you should be entertaining it, seeing if it's practical,
seeing if it's something that holds up.
And you'll have ideas that you believe to be right that are wrong.
And you'll have some that are right too.
But the only way you're going to find out they're wrong or they're right
is if you're talking to other people who disagree,
if you're reading philosophers you don't agree with,
if you're actually trying to pursue knowledge and truth.
That is how we evolve.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}