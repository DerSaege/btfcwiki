---
title: Let's talk about China, North Dakota, and spies...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8c8aHr7V5WI) |
| Published | 2022/07/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message about Chinese spy base rumors in North Dakota.
- Chinese food company purchased land near an Air Force base in North Dakota.
- Concerns raised about Chinese intelligence gathering information.
- Senate intelligence oversight expressed concerns about the situation.
- The surveillance concerns could be plausible due to electronic surveillance capabilities.
- The facility's proximity to the Air Force base has caused turmoil in the community.
- The project's potential cancellation is suggested due to security concerns.
- There are easier ways for intelligence gathering than a 300-acre facility.
- Speculation on the intentions behind the land purchase and surveillance concerns.
- Uncertainty surrounds the true motives behind the project.
- The situation has attracted national attention and made it from North Dakota to D.C.
- Beau raises questions about the credibility and secrecy of such a large project.
- Anticipates more situations where U.S. intelligence counters perceived threats from China.
- Warns of increasing intelligence threats in the next few decades.

### Quotes

- "Your neighbor hasn't lost the plot. This is actually something that's being discussed."
- "It's going to become more and more pronounced, especially if Russia does get bogged down in Ukraine the way it appears they will."

### Oneliner

Beau received a message about Chinese spy base rumors in North Dakota, leading to concerns about a Chinese food company's land purchase near an Air Force base and potential intelligence surveillance. Senate intelligence oversight has expressed concerns, sparking community turmoil and speculation on the project's intentions.

### Audience

Community members, concerned citizens

### On-the-ground actions from transcript

- Monitor local news and developments related to the situation (implied)
- Stay informed about intelligence threats and security concerns in your area (implied)

### Whats missing in summary

The full transcript provides additional context and details on the concerns surrounding potential Chinese intelligence surveillance in North Dakota and the implications for national security.

### Tags

#NorthDakota #China #IntelligenceThreats #CommunitySecurity #NationalSecurity


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a message I received.
That, I have to admit, at first I kind of blew off.
Didn't put too much stock in the message,
but eventually got around to checking it out.
And that message will lead us to talk about North Dakota and China
and a whole bunch of other stuff.
So the message comes in and it says,
hey, my neighbor has been telling me for weeks
that the Chinese are putting in a spy base here in North Dakota.
Sure they are.
He said that he blew it off initially,
but his neighbor told him that it was on MSNBC.
He said he went and looked, couldn't find anything on MSNBC,
but thought he'd ask me just in case.
I looked.
I couldn't find anything on MSNBC either.
However, over on CNBC, I did find something.
So this is probably a story that's going to eventually
be national news.
A Chinese food manufacturing company
purchased 300 acres of land,
trying to purchase 300 acres of land in North Dakota
to put in a milling facility for corn.
Nothing untoward about that.
However, it is really close to an Air Force base there.
And a major at the Air Force base basically informed OSI,
the Air Force's security group, about some concerns as far as
maybe because this is a Chinese company
and because it's so close to the Air Force base,
that Chinese intelligence might put passive surveillance devices there
so they can pick up information about drones and communications
and stuff like that.
OK.
And then come to find out that this has already hit
the Senate intelligence oversight
and they have expressed their concerns.
So is it real or is it xenophobia?
Yeah.
Yeah.
The type of surveillance that they're concerned about,
could it be done from this facility?
Yes.
Absolutely.
That kind of surveillance, when you're talking about electronic surveillance
and signals intelligence, it's pretty easy to hide.
So easy to hide that you probably don't need a 300-acre facility to do it.
What I was able to find about what they're concerned about,
you could put that equipment in a residential house,
like not even a big one.
So that concern is there.
And it has actually led to a lot of turmoil in the community
around this facility because it's in North Dakota
and this is something that's going to bring a couple hundred jobs.
Given the fact that it has reached Senate oversight,
there might be something to it.
Maybe there's information that isn't public.
At the same time, right now, it's that near-peer contest.
So there's probably a lot of paranoia,
and it may also have something to do with maybe something else entirely.
But this is a good reason to torpedo this project.
I mean, you've entered the world where the intelligence community
and the corporate community have met,
and now they're operating in another country.
That's what you're theoretically talking about.
You will never get to the bottom of that.
Could it be? Sure.
But that does seem like a massive footprint
for something that doesn't really need to be that big.
This isn't the 1980s.
There's far easier ways to gather that kind of information
that doesn't require 300 acres
and is something that is just so overly obvious.
So I would be surprised if that was the intention when the deal started.
Now, maybe at some point later, Chinese intelligence was like,
hey, wait, you're getting land where?
15 minutes away from this?
Maybe we should talk.
And maybe U.S. intelligence picked up on something like that,
and it coincided with this major at the base putting out this information.
Or maybe the major at the base was just saying,
hey, this is a concern of mine, tells OSI like they tell them to,
and the memo got out of hand.
There's no real way to know the answer to this.
But your neighbor hasn't lost the plot.
This is actually something that's being discussed.
It has made it from North Dakota to D.C.,
and it was, in fact, mentioned on CNBC.
I have a lot of questions about the concerns.
This is one of those things, because it's such a big project,
it would just attract so many eyes.
If this same thing had been said about, I don't know, a restaurant,
I would probably give it a little bit more credibility,
because you're talking about a facility that's going to have hundreds of people.
It's really hard to keep a secret like that.
But it's interesting, and we will probably hear more about this
and more about similar situations in which U.S. intelligence
or counterintelligence is working to mitigate a perceived intelligence threat from China.
This is something you should probably get used to,
because this is going to occur over the next 10, 15, 20 years.
It's going to become more and more pronounced,
especially if Russia does get bogged down in Ukraine the way it appears they will.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}