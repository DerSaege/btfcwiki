---
title: Let's talk about Democratic messaging....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rLYZhYJh24k) |
| Published | 2022/07/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Democratic Party struggles with messaging and getting their policies seen as a priority.
- Despite polls showing support for Democratic positions, the party faces challenges in making issues like Roe v. Wade a priority.
- A poll by AP revealed that before the decision on Roe v. Wade, only 13% saw family planning and women's rights as a priority. Afterward, this number rose to 30%.
- While 64% of Americans believe abortion should be legal in all or most cases, only 30% prioritize it as an issue.
- Beau suggests that the Democratic Party needs to focus on explaining the economic and social impacts of issues like Roe v. Wade to increase prioritization by voters.
- Candidates need to be frank about the consequences and investigations that may occur if certain policies are not protected.
- The messaging should center on the potential economic strain, impact on social safety nets, and invasion of privacy resulting from policy changes.
- Making issues like family planning and women's rights a priority is key to motivating voter turnout.
- Beau stresses the importance of clear and consistent messaging to raise the issue's priority level among voters.
- The Democratic Party needs to communicate effectively to elevate issues to ballot priorities.

### Quotes

- "It's not on the ballot just because it's an impact from the election."
- "You can actually put Roe on the ballot. But you can't be timid."
- "Those that were surveyed after the decision came down, 30%."

### Oneliner

The Democratic Party faces challenges in making issues like Roe v. Wade a priority despite majority support, stressing the need for clear messaging to motivate voter turnout and elevate these issues.

### Audience

Democratic Party members

### On-the-ground actions from transcript

- Communicate the economic and social impacts of policies like Roe v. Wade to raise voter awareness and prioritize these issues (suggested).
- Be frank with voters about the consequences and investigations that may occur if certain policies are not protected (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the Democratic Party's struggle with messaging and prioritizing key issues, urging for clearer communication to mobilize voters effectively.

### Tags

#Messaging #DemocraticParty #VoterPriorities #RoevWade #ClearCommunication


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about polling and messaging
and whether or not things are on the ballot.
We're going to talk about the Democratic Party
trying to succeed at something it doesn't have a really good track record with.
When you look at polls,
most Americans agree with the Democratic Party's position
on a whole lot of issues.
However, the Democratic Party has a real problem with messaging
and actually getting that policy or that issue
seen as something that's a priority.
And this is something that has gone on a long time.
Right now, we keep hearing Roe is on the ballot.
Roe is on the ballot. And is it?
I mean, here in reality, yeah, it absolutely is.
If the Democratic Party doesn't hold in the House and expand in the Senate,
you're in trouble. If it loses, yeah, there's going to be more restrictions.
But is it really on the ballot?
There's no box to check, I support Roe.
The only way it becomes something that's actually on the ballot
is if voters decide it's a priority.
So this really interesting thing happened.
AP was in the middle of conducting a poll when the decision came down.
So it was after the decision was leaked.
And then it continued when the decision came down.
So it has about half, a little less than half of the results
are from before the decision officially came down.
And about half come from afterward.
From before, family planning and women's rights,
well, that's about 13%. They thought it was a priority.
Afterward, it's about 30%.
Now, understand that under normal circumstances in most years,
you're looking at 1 to 3% have this as a priority.
This is why when you heard Democrats say it wasn't a legislative priority,
it's because that's what the polling reflected.
But now it's up to 30%.
And that sounds good.
And don't get me wrong, it's a dramatic improvement.
But is it really putting it on the ballot?
How many people agree with that position?
How many people, what percentage of people agree with the Democratic position here?
64%.
But only 30% see it as a priority.
That's a big gap.
64% of Americans believe that abortion should be legal in all cases
or legal in most cases.
Only 9% believe it should be illegal in all cases.
If you were to add illegal in all cases and illegal in most cases,
you get 33%.
34% of Americans believe it should be legal in all cases.
This is a winning issue for the Democratic Party by a huge margin.
But it's only a winning issue if they can actually get it on the ballot,
if they can make it a priority.
And right now, the Democratic Party's strategy
generally consists of saying, hey, we need to defend this.
And those people who know what happens if it goes away,
yeah, they understand.
That's your 30%.
Those are the people who are like, yeah, this needs to be a priority.
But there's a whole lot of people who don't know what that means,
who don't understand the downstream impacts of this going away.
The Democratic messaging really needs to focus on that.
They really need to get out there and talk about the economic strain
it is going to put on states and schools and social safety nets,
how it's going to impact mothers, all of this.
And they need to talk about it consistently.
When they get out there and they say, well, give us more seats
and we'll protect it, sure.
But that's not going to motivate voters.
That's not going to bring them out because it's not a priority.
It has to be made a priority in order to get people to show up.
That's what you have to put on the ballot.
It's not on the ballot just because it's an impact from the election.
What's on the ballot is what the voter says the priority is.
30%?
Yeah, it's good, but that's not getting out the vote numbers.
That number needs to go up a whole lot.
And it's going to take candidates being very frank with the American people
and explaining the investigations that are going to occur,
how your private life is going to become public,
how they're going to dig into you.
That needs to be the messaging.
The economic impact, the invasion of privacy, because for years,
these numbers floated around 1% to 3%.
Family planning and women's rights, they're not on the ballot.
Right now, it's up to 30.
Those that were surveyed after the decision came down, 30%.
That can be higher with the right messaging,
and you can actually put Roe on the ballot.
But you can't be timid.
You have to actually tell people what's going to happen if this continues,
if it can't be protected.
Not just today, but five years and 10 years down the line.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}