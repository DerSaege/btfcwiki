---
title: Let's talk about a July 4th Public Health update....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L9eC4Uaggsw) |
| Published | 2022/07/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the audience on Independence Day, mentioning a potential different kind of Independence Day celebration.
- Providing an update on the global public health situation, with permission to be cautiously optimistic until fall.
- Not expecting significant changes until it starts to cool down again.
- Noting the current death toll in the United States and the decrease from the previous year.
- Explaining the reasons behind the cautious optimism, including immunity levels and improved medical treatments.
- Mentioning that most people have likely had the virus and with vaccinations, immunity has increased.
- Pointing out that while things are improving, it doesn't mean it's over yet.
- Expecting a possible level-off in fall and a more seasonal nature for the virus.
- Some suggest the virus will remain in the background but won't cause massive loss.
- Beau will continue monitoring the situation and update if needed.

### Quotes

- "Docs currently have given us permission to be cautiously optimistic, quote, at least until fall."
- "We have permission to be cautiously optimistic."
- "All of this taken as a whole means higher survivability, which is good."

### Oneliner

Beau provides an optimistic update on the current global public health situation, with reasons to be cautiously optimistic until fall, attributing improvements to immunity levels and better medical treatments.

### Audience

Public health-conscious individuals.

### On-the-ground actions from transcript

- Monitor updates from trusted sources regularly (exemplified).
- Stay informed about public health guidelines and recommendations (exemplified).

### What's missing in summary

The full transcript provides more detailed insights into the current state of the global public health situation and the reasons behind cautious optimism.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So in theory, this would go out on Independence Day,
so happy Independence Day.
And maybe we're celebrating
another kind of Independence Day.
Today, we are going to talk about
our global public health issue,
because docs currently have given us permission
to be cautiously optimistic, quote, at least until fall.
So what they're saying is that right now, until fall,
they're not expecting much.
They're kind of predicting that things
are gonna kind of level off right where they're at,
and that we'll maintain this until at least
it starts to get cool again.
Now, right now in the United States,
we are losing about 360 per day.
And during the lull last year, it was 228 per day.
The difference, and a huge reason behind the docs' attitudes
is that last year, there were about 20,000 cases per day.
And this year, right now, we're running about 109,000 cases
per day.
So we're doing much better.
Now, they're attributing this to a bunch of different things.
The first is that the models that most people are relying on
are saying that eight out of 10 of us, well, we've had it.
And then you add that in with people getting vaccinated
a couple of times, maybe getting boosted,
maybe actually contracting it.
All of this added together has gotten us to the point
where we have a decent amount of immunity.
And then the docs themselves have gotten better.
They've gotten better at treating it.
All of this taken as a whole means higher survivability,
which is good.
Now, none of them are saying that we're done
and that we're out of it yet.
But there's a general attitude that come fall,
it really may not be that bad.
It'll be worse than now.
But the general tone at the moment
that's being reported in trusted outlets, AP,
places like that,
is that the docs are feeling pretty comfortable with it.
And that they're kind of expecting it
to eventually level off
and be something that is more seasonal in nature.
And then there are some that suggest
it's always gonna be around in the background,
not really seasonal, always there,
but just kind of background noise.
And it won't be causing massive amounts of loss anymore.
So all of this is good.
That being said, I've heard stuff like this before.
I will continue watching the numbers.
And if any hills start to develop,
believe me, I'll let you know.
But right now, it seems like the docs
are having a moment where they've decided,
hey, take a break.
We've got this for a little bit.
We have permission to be cautiously optimistic.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}