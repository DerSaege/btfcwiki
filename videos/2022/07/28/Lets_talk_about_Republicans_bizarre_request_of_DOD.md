---
title: Let's talk about Republicans bizarre request of DOD....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5cWHtdsXHH8) |
| Published | 2022/07/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senate Armed Services Committee requested Pentagon to stop rooting out extremists in the military.
- Every Republican on the Committee voted for this request.
- Independent Senator Angus King also supported the request.
- The suggestion claimed combating extremism in the military is a wasteful use of taxpayer funds.
- The suggestion was included in the Senate's National Defense Authorization Act (NDAA).
- Both Democrats and independents opposed the suggestion.
- Beau questions the reasoning behind allowing extremists in the military.
- He calls for a cost-benefit analysis of this decision.
- Beau sees it as an attempt to shape a particular narrative.
- He urges for more debate and scrutiny on this issue.
- The Department of Defense is not bound to follow this suggestion.
- Beau is confident that the Department will continue rooting out extremists.
- He stresses the importance of understanding the motives behind this request.
- Beau calls for accountability and answers regarding why some Republicans support this stance.

### Quotes

- "Spending additional time and resources to combat exceptionally rare instances of extremism in the military is an inappropriate use of taxpayer funds."
- "That is something else."
- "Is that a good use of taxpayer funds?"
- "I think that this little part of this report should probably get a little bit more debate, a little bit more discussion, definitely more coverage."
- "We have to find out why exactly Republicans don't want the Department of Defense getting rid of extremist elements."

### Oneliner

Senate Armed Services Committee suggests halting efforts to root out extremists in the military, raising questions on motives and fiscal responsibility, met with strong opposition and calls for scrutiny.

### Audience

Congressional representatives

### On-the-ground actions from transcript

- Question the motives behind supporting the suggestion (suggested)
- Push for more debate and coverage on this issue (suggested)
- Call for accountability and answers regarding the stance on combatting extremism in the military (suggested)

### Whats missing in summary

The full transcript provides a deeper understanding of the concerning request made by the Senate Armed Services Committee and the strong opposition it faced, prompting a call for transparency and accountability in addressing extremism within the military.

### Tags

#SenateArmedServicesCommittee #Extremism #Military #DepartmentOfDefense #TaxpayerFunds


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a rather unusual request
that came from the Senate Armed Services Committee
and went to the Pentagon.
It was attached to their budget.
This portion of the report,
going along with the Senate's NDAA,
it made it in because every Republican voted for it.
Every Republican on the Senate Armed Services Committee
voted for it and an independent,
Angus King, Senator King.
The request, the suggestion,
was to stop trying to root out extremists in the military.
Now, normally when somebody says something like that,
that would be hyperbole.
This is exactly what it says.
Spending additional time and resources
to combat exceptionally rare instances of extremism
in the military is an inappropriate use
of taxpayer funds
and should be discontinued
by the Department of Defense immediately.
That's wild.
Every single Republican, every Democrat voted against it.
That is something else.
I cannot imagine the reasoning behind this.
I would really like to know why Republicans think
that it's okay if extremists are in the military,
trained by the military,
have access, perhaps get equipment.
Is that a good use of taxpayer funds?
I would love to see the cost-benefit analysis on this.
This sounds like people trying to create a narrative.
This sounds like rhetoric
that maybe they don't understand the impacts of.
It's what it seems like to me.
I think that this little part of this report
should probably get a little bit more debate,
a little bit more discussion,
definitely more coverage,
because I can't imagine the motive behind this.
Now, for those at home, there is some good news.
If you're not familiar with how this process works,
this is a suggestion.
It is a suggestion.
Normally, suggestions that are included,
they are normally given consideration
by the Department of Defense.
However, they are suggestions.
They are not binding.
DFD does not have to do this, and it will not.
I can tell you that now.
They are not going to stop looking for extremists
in their midst.
That's not going to happen.
But even the suggestion bears a whole lot of scrutiny.
We have to find out why exactly Republicans
don't want the Department of Defense
getting rid of extremist elements.
This is a question that needs to be asked,
and it needs to be answered.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}