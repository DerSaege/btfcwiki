---
title: Let's talk about Trump's new relationship with conservative media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wxfZQGur8Xs) |
| Published | 2022/07/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's relationship with conservative media is shifting.
- Outlets that were once unquestioningly loyal to Trump are now breaking with him.
- OANN lost their last major carrier, impacting their reach to help Trump.
- Fox News, traditionally loyal, has started criticizing Trump's inaction and unfavorable polling numbers.
- The New York Post and The Wall Street Journal, both favorable to Trump, have condemned him recently.
- Speculations on why this shift is happening include journalistic integrity, fatigue with Trump's scandals, and awareness of the DOJ investigation.
- One theory suggests outlets are positioning themselves for the 2024 Republican nominee race to maximize ad revenue.
- Beau believes the primary motivator behind the behavior shift is money.
- Trump might face challenges as he loses the unwavering support of news outlets.

### Quotes

- "Outlets that were once unquestioningly loyal to Trump are now breaking with him."
- "Speculations on why this shift is happening include journalistic integrity, fatigue with Trump's scandals, and awareness of the DOJ investigation."
- "Beau believes the primary motivator behind the behavior shift is money."

### Oneliner

Trump's once loyal conservative media outlets are now breaking ties, speculated reasons include journalistic integrity and money motives, shifting the landscape as 2024 approaches.

### Audience

Media Analysts

### On-the-ground actions from transcript

- Analyze and understand the shifting dynamics between Trump and conservative media outlets (suggested).
- Stay informed about the motivations behind media behavior shifts (suggested).
- Monitor how media outlets position themselves for the 2024 Republican nominee race (suggested).

### Whats missing in summary

Insights on the potential impact of Trump losing support from conservative media outlets on his future political endeavors.

### Tags

#Trump #ConservativeMedia #Journalism #2024Election #MoneyMotives


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Trump's new relationship
with the media, with conservative media in particular,
because it's shifting.
For a long time, Trump has been able to count
on certain outlets to have his back, to, well, in some cases,
be unquestioningly loyal to him and his agenda.
And that is shifting.
There's been a lot of speculation as to why,
and we're going to go over that.
And I have an additional theory that I think is far simpler
that might be the answer.
OK, so the first outlet that Trump lost
is OANN, One America, whatever that is.
They lost their last major carrier.
So it's not that they really had a split with Trump.
They just don't have any reach to help anymore.
So that's one.
And Trump is going to have to cope with that.
Another is Fox News.
Fox News has been unquestioningly loyal.
There are personalities on Fox that are friends with Trump.
And for years, they defended anything that he did.
Now, over the last couple of days,
we have seen people on Fox describe Trump
as looking, quote, horrific because of his inaction.
We have seen shows on Fox read bad polling numbers
related to Trump.
And that apparently got under his skin
and actually prompted a response.
Something as simple as reading the polling numbers that
aren't favorable to Trump, that's
not something Fox News would have done not too long ago.
They would have just left that out.
The New York Post, been pretty favorable for Trump
for a very long time.
They trashed him over his silence and inaction
related to January 6th.
The Wall Street Journal referred to him
as the president who stood still and suggested
that his character failed him.
We're not even talking about small breaks.
We're talking about condemnation from outlets that
have traditionally been very loyal.
So why is this happening?
There's a couple of theories.
One is that some of these outlets
have found their journalistic integrity
and that the spell is broken.
I am skeptical.
Another is acknowledging that three of these
are in the same family run by Murdoch.
And one theory says Murdoch's just tired of him.
Tired of Trump, tired of the scandals,
tired of constantly doing damage control, tired.
So he's just letting him go.
A variation on this suggests that Murdoch
is letting Trump go to adopt Ron DeSantis as the new mascot
and they'll start cheerleading for him.
OK, maybe.
Still another suggests that some of the journalists
at some of these outlets are aware of how the DOJ
investigation is progressing and they're
starting to break with Trump now.
So it doesn't look bad when they just throw him under the bus.
Also possible.
However, all of these have an almost conspiratorial belief
in the idea that content, minor content,
is being directed from way up top.
I don't know that that's true because I
see a far simpler explanation.
These outlets have to position themselves before the race
to become the Republican nominee for president in 2024
before that starts.
They have to position themselves so they
can appear to not be backing any particular candidate
because they all run off of ad revenue.
And the controversies that will inevitably
come from the Republican primary,
they're going to be a moneymaker.
They are going to make money for these outlets
because they can feed into it.
It's not that they care.
It's that they can make money.
I think that's a far simpler explanation
and it fits more in line with most of the behavior
from these outlets over the years.
I don't think there's a conspiratorial secret content
board that pushes employees to read bad polling numbers
or describe a former president in an unfavorable light.
I think it's more along the lines of you
don't have to constantly defend this person anymore.
We need to get ready for this.
And then they're just doing what they do naturally.
That seems to be more likely to me.
But I think the motivator is money.
Because when it comes to the Republican primary,
you're talking about millions upon millions upon millions.
It's not money, it's motive.
And I think that's a far simpler explanation for the behavior.
Now as far as Trump's concerned, he's going to have some issues
because he has grown very accustomed to having news
outlets that will kneel before him and do whatever he wants.
And I don't think he's going to have that anymore.
Just maybe a few personalities who
haven't seen him for what he is.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}