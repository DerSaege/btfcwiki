---
title: Let's talk about the Rhine and recession....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VukZffko4C8) |
| Published | 2022/07/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau plans to switch up his usual format by first doing a video and then reading a related message about the topic.
- The topic of the video is the Rhine river, inflation, Germany, and their impact on economies globally.
- The Rhine river, a vital route for transporting goods, is currently running low, causing issues for barges which need more depth to operate efficiently.
- Due to the low water levels, barges can carry fewer goods, leading to disruptions in the supply chain, increased shipping rates, and ultimately higher prices.
- This situation is worsened by inflation and could potentially push Germany into a recession, with possible repercussions across Europe.
- Beau draws parallels between the economic situations in Germany and the United States, suggesting that both countries might be facing recessionary pressures.
- He speculates that climate change could play a role in these economic challenges, hinting at a global economic downturn.
- The related message Beau reads dismisses climate change as a socialist plot, attributing issues like the low Colorado River levels to mere drought rather than climate change.
- The message challenges the idea of climate change impacting multiple regions and questions the authenticity of its effects.
- Beau ends on a humorous note, sharing the message he received and encouraging people to raise awareness about climate change.

### Quotes

- "What's up, little Beau Peep with your little sheep?"
- "Climate change is a socialist plot. It's not real."
- "Wake up and wake others up or just bah."

### Oneliner

Beau tackles the impact of low Rhine river levels on economies globally, while addressing climate change skepticism in a humorous message.

### Audience

Climate change activists

### On-the-ground actions from transcript

- Raise awareness about climate change and its impacts on economies globally (implied)

### Whats missing in summary

The full transcript provides additional context on the challenges posed by low river levels and inflation on economies and the dismissive attitude towards climate change.

### Tags

#Rhine #Inflation #Germany #EconomicImpact #ClimateChange


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to do things backwards.
Normally, I read a message and then provide
some form of commentary or answer.
Today, I'm going to do the video and then read the message
because I was in the process of putting together
a video on a topic that's about to become apparent.
And I got a message that is somewhat related to it.
So we'll do the video as intended
and then read the message afterward.
OK, so today, we're going to talk about the Rhine,
inflation, Germany, and what it has
to do with economies everywhere.
The Rhine is a river.
And it is used to transport a lot of goods, a lot of goods
that are going to industrial centers.
Now, the river is running low.
At a bottleneck, the current depth is about 2.3 feet.
The problem is the barges, they need about 3.9.
So with less water, the barges will carry less goods
to reduce their draft so they can make it down
in shallower water.
Because they are carrying less, they have to make more trips.
Because of this, it disrupts the supply chain.
Because of this, the shipping rates will go up.
Because of this, prices will go up.
This is bad.
Under normal circumstances, it is very bad
in a time of inflation.
There are possibilities.
They could shift to using rail.
But it's a little bit harder.
And I guess they haven't had much success
with that in the past.
Germany is like the US.
It's been kind of teetering on the brink of recession.
I know here in the United States,
the Biden administration says it's not really a recession.
I mean, OK, I'm not an economist.
By the standard that I have used my entire life,
we just went into a recession.
So maybe it's a shallow recession.
Maybe it's an unusual one.
But by the standard I've always used, we're in one.
This may be the thing that sends Germany into one.
And with that, it's safe to assume that it
will spread throughout Europe.
So this climate change may actually
be the thing that's pushing us globally
into an economic downturn.
So it's something to watch.
Now, for the message.
What's up, little Bo Peep with your little sheep?
Are you dumb or are you in on it?
Climate change is a socialist plot.
It's not real.
What's happening with the Colorado River is just a drought.
The climate doesn't just change in one place.
It would change all over the world.
Think.
Can you name another river doing like this?
No, because it's just a drought.
Wake up and wake others up or just bah.
Actually wrote that.
Yeah.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}