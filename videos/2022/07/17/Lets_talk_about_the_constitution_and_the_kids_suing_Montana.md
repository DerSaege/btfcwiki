---
title: Let's talk about the constitution and the kids suing Montana....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J6IyWBz3MDk) |
| Published | 2022/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Youth in Montana have launched a lawsuit against the state, claiming that the state's support for fossil fuels is contributing to environmental degradation and climate change.
- The kids argue that the state of Montana has a duty to maintain and improve a clean environment for present and future generations, as stated in the Montana State Constitution.
- The case, known as Held v. State of Montana, has faced resistance from the state but has been allowed to proceed by the Montana Supreme Court.
- The trial date for the case is set for early February 2023, with potential delays expected due to the case's significance.
- Success in this case could have a widespread impact, potentially influencing similar cases in Hawaii, Virginia, and Utah.
- Beau expresses admiration for the youth involved in the lawsuit and is eager to see how the case unfolds.
- The lawsuit represents a significant effort by young individuals to hold the state accountable for environmental protection and sustainability.

### Quotes

- "The kids are all right."
- "They do not have a constitutional right. That's not there."
- "The name of the case, if you're curious, is Held v. State of Montana."

### Oneliner

Youth in Montana challenge state support for fossil fuels, invoking constitutional duty to protect the environment; Held v. State of Montana faces resistance but moves forward, potentially setting a precedent for environmental cases nationwide.

### Audience

Youth, Environmental Activists, Legal Advocates

### On-the-ground actions from transcript

- Support youth-led environmental initiatives in your community (exemplified)
- Stay informed about environmental lawsuits and their potential impact (exemplified)
- Advocate for sustainable practices and policies in your area (exemplified)

### Whats missing in summary

The full transcript provides additional context on the legal battle between Montana youth and the state, offering insights into the constitutional basis of their claims and the potential implications of the case's outcome.

### Tags

#YouthActivism #EnvironmentalJustice #LegalAction #Montana #FossilFuels #ClimateChange


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Montana
and something that is going on there with the kids,
because the kids are all right.
Youth in Montana have launched a lawsuit
against the state of Montana,
alleging that Montana fostering fossil fuels
the way that they are,
well, it's contributing to environmental degradation
and that it's just going to disrupt
and destroy their future via climate change.
And the first time I talked to somebody about this,
they're like, yeah, they've got a constitutional right.
And I'm like, look, I love the idea
and I'm very sympathetic,
but they do not have a constitutional right.
That's not there.
And they sent me the Montana State Constitution,
which says, the state and each person
shall maintain and improve a clean
and healthful environment in Montana
for present and future generations.
The legislature shall provide for the administration
and enforcement of this duty.
The legislature shall provide adequate remedies
for the protection of the environmental life support system
from degradation and provide adequate remedies
to prevent unreasonable depletion
and degradation of natural resources.
All right, you got me.
They do. It's there.
So, kids have been fighting this for a while.
The state, of course, has been fighting back.
They went to the judge and were like,
yeah, you got to get rid of this case.
The judge is like, no.
They took it to the Montana Supreme Court
and the Supreme Court there is allowing it to go forward.
Currently, the trial date is set in early February 2023.
The way things normally work, though,
you're probably looking at early summer.
There's going to be delays
because this is going to be a big case.
A lot of depositions.
It's interesting because if this succeeds,
it will probably cause a ripple effect in a lot of places.
I would note that a very brief search found similar cases
in Hawaii, Virginia, and Utah.
It's an interesting tactic.
I can't wait to see how it plays out,
but I'm pretty impressed, to be honest.
The name of the case, if you're curious,
is Held v. State of Montana.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}