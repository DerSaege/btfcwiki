---
title: Let's talk about Trump, Pence, loss, and gain....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Tg4NRTZF3WA) |
| Published | 2022/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on Trump and Pence testifying before a committee.
- Questions Trump's courage to testify and suggests he may not listen to good advice.
- Considers two potential outcomes if Trump does testify: vague, unhelpful answers or potential perjury.
- Suggests allowing Trump to testify only if his mic can be cut to prevent perjury.
- Examines the risks for Trump in testifying and how he currently benefits from standing apart and denying allegations.
- Contrasts Trump's situation with Pence, who stands to gain credibility and respect by testifying truthfully.
- Believes Pence's faith may motivate him to testify truthfully.
- Foresees Pence's testimony elevating his stature within the Republican Party.
- Suggests a different approach for getting Pence to testify, starting with a casual, behind-closed-doors interaction.
- Contemplates the motivations behind the committee's consideration of Trump and Pence testimony.

### Quotes

- "He has everything to lose. He has everything to lose by doing this."
- "Pence has everything to gain."
- "This is a person that it doesn't matter how you feel about him politically or even as a human being, okay?"
- "Y'all have a good day."

### Oneliner

Beau speculates on the potential testimonies of Trump and Pence before a committee, suggesting Trump has everything to lose while Pence has everything to gain, based on their actions and motivations.

### Audience

Committee members

### On-the-ground actions from transcript

- Approach Pence for a casual, behind-closed-doors interaction to begin discussing his testimony (implied).
- Contemplate the motivations behind the committee's consideration of Trump and Pence testimony (implied).

### Whats missing in summary

Insight into the potential implications of Trump and Pence's testimonies on their political futures.

### Tags

#Trump #Pence #Testimony #Committee #Speculation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about Trump and Pence
and loss and gain and the idea that the committee might be
entertaining having them testify.
A lot of questions about it.
Personally, personal opinion, I don't think
Trump has the courage to do it.
Aside from that, I think if he's getting good advice, they'll tell him not to.
At the same time, he's not exactly known for listening to good advice, so he might agree.
And if he does, personal opinion, there's kind of two options.
He's going to go in there and give very dry yes-no answers, not answering that, don't
remember that. Kind of a useless interview. The other thing that I could
see happening is the committee provoking him and sending him into rants. And if
that happens, I'd give it a pretty high likelihood that he perjures himself
during it. Now, I don't know if I would allow Trump to testify live unless he
the ability to cut his mic while still allowing it to record. But for Trump, he
has everything to lose. He has everything to lose by doing this. At the moment, he
gets to stand from afar and say, that's not true, that's not true, under oath with
the committee there to rebut with evidence that they have gotten from
other places. That's a dangerous proposition for him. On the other hand, you
have Pence. Pence has everything to gain. Pence has everything to gain. This is a
person that it doesn't matter how you feel about him politically or even as a
human being, okay? I do not believe that that man would swear to tell the truth,
the whole truth, and nothing but the truth, so help him God, and then lie.
I don't think that would happen. There's a lot of things to question about Pence.
his faith isn't one of them. I think that'd be a very strong motivator for
him. So I think he'd give truthful testimony and then you also have to
factor in his position. Where he's at now, he's kind of a pariah within the
Republican Party. He could walk in there what amounts to a vice president that
will go nowhere else and walk out one of the most respected people in the
Republican Party because based on the other testimony we have a pretty good
idea of what his testimony is going to be and if he goes in there and does that
and says, this is the position I was in, this is why I didn't betray the
Constitution and the American people. I think he would come out on top within
the Republican Party. I think his stature would elevate a lot. He has
everything to gain. So you have one person with everything to lose, the other
with everything to gain.
Again I don't know about the method of doing it.
With somebody like Pence, it would probably be best to do it behind closed doors first.
out of his personality, it might be better to get him past those barriers
because I don't think he's gonna want to talk about it, but I also don't think
he'd lie. So I would do it in a casual conversation to begin with and then see
about a live testimony but it's an interesting move and there are some
things you could theoretically read into this because this is this is off
schedule this isn't this isn't part of the tell them what you're gonna tell
Tell them, tell them, tell them what you told them.
This is something new.
Somebody has suggested it might be that certain people may be unavailable for testimony.
Others have said, well, they hit a brick wall.
I don't believe that one.
And others suggest that it's a way of putting Trump on the record.
forcing his public actions to reflect that private testimony if they were to
allow him to do it behind closed doors. I don't know. I don't know what the
motivation is here. With Trump, I see it really unlikely that it's going to
happen. With Pence, much much higher probability because even if he doesn't
want to, I don't think he'll lie and he has a lot to gain by doing it. Anyway,
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}