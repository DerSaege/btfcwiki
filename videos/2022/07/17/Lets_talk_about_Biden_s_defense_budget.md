---
title: Let's talk about Biden's defense budget....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EYIMfuc2JGk) |
| Published | 2022/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The House released a defense budget blueprint of $839 billion, prompting questions.
- The Biden administration did not ask for such a bloated budget; it's $37 billion more than requested.
- The House rejected the administration's plans for force reduction.
- Congress holds the power of the purse, as outlined in the US Constitution.
- The defense budget often increases due to inflation and contributions from the defense industry.
- The Senate has yet to finalize its plan, which will eventually be sent to Biden for approval.
- This situation showcases the checks and balances system in government.
- The Senate's decision on the budget is still pending.
- The influence of lobbyists on the final budget decision remains uncertain.
- Congress ultimately determines the budget, not the administration.

### Quotes

- "Congress has the power of the purse."
- "The Biden administration did not ask for this."
- "The defense budget, like anything else, goes up each year because of inflation."
- "That is power reserved to Congress."
- "It's just a thought."

### Oneliner

The House proposed a $839 billion defense budget, $37 billion more than Biden's request, revealing Congress's power over budget decisions and potential industry influences.

### Audience

Budget-conscious citizens

### On-the-ground actions from transcript

- Monitor and advocate for responsible defense budget allocations (implied).
- Stay informed about campaign contributions to representatives, especially from the defense industry (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of the intricacies of defense budgeting and the role of different branches of government in decision-making.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the defense budget
and what's happening there.
The House released its blueprint for the defense budget,
$839 billion, and that has prompted a lot of questions.
But really, there's just two types of the questions
that came in, it's really just two questions.
The first, why did the Biden administration ask
such a bloated defense budget? And then the second, well, not to give away the answer
to the first, why did the House do what it did? Okay, why did the Biden administration
ask for such a bloated defense budget? They didn't. They didn't. The Biden administration
did not ask for this. So the bottom line on the spending is $839 billion. That is $37
billion more than the Biden administration asked for, which is 30 billion dollars more
than DOD got last year.
So this one isn't actually on Biden.
The 30 billion could be chalked up to inflation.
As far as everything else that goes along with it, the Biden administration was actually
planning a little bit of force reduction.
Not a lot, but a little bit.
They were going to scrap a nuclear cruise missile program and maybe retire a carrier
or two, something like that, and the House said no.
The House said no.
So the obvious question is why and how did this happen?
How did it happen?
Article 1, Section 9, Clause 7.
United States, Congress has the power of the purse. It's in the US Constitution, that's why.
If the Biden administration walked in and said, to run DOD, I need $1.15, and Congress was like,
yeah, here's $3 trillion. That's how it works. And the Biden administration will sign it because
it's a must-pass bill. So they definitely have the authority to do it.
Now why? That's another question. There's a couple possible reasons. When it comes
to the carrier thing, since World War II aircraft carriers have been a symbol of
American force projection, right? Times are changing, the world is changing,
technology is changing, the second largest Air Force in the world, the US
Navy, is not going to surrender air superiority without a plan, right? But
emotions run high. People may just not be ready to give that symbol up yet.
Eventually, the carrier, as we know it anyway, will go the way of the battleship.
But there's probably a much more likely reason.
If you were to go over to the website OpenSecrets, you'd probably find out.
My guess is you could go over there and find out who bought your representative.
Look at the campaign contributions.
contributions, probably going to be a whole lot from the defense industry.
So that's where that additional money came from.
The defense budget, like anything else, goes up each year because of inflation, at least
a little bit.
The House is, I mean, their blueprint includes double, more than double, the increase that
the Biden administration was looking for.
Now the Senate has yet to put together their plan and once the Senate does that, I think
that happens September maybe, somewhere in there, the two plans will go together and
then eventually it will go up to Biden.
But whatever number Congress decides on, that's the number.
The Biden administration really doesn't control that, and this is a good example of how things
are supposed to work in theory as far as checks and balances.
Congress has the power of the purse to remind the president that they're not a dictator.
They're not a monarch.
They don't get to spin the treasury as they see fit.
That is power reserved to Congress.
So that's what happened, and we don't really know what the Senate's going to do.
I would be surprised if the Senate's number was as high as the House, but we'll have
to wait and see what the lobbyists were able to come up with, I guess.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}