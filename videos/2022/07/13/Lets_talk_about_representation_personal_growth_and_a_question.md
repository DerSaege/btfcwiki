---
title: Let's talk about representation, personal growth, and a question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-XssiIwthrE) |
| Published | 2022/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts receiving messages following an interview with Jesse that prompted varied reactions, including some negative ones.
- Shares a specific message critiquing his content for pushing "trans ideology" and lacking diversity in interviews.
- Responds to the criticism by addressing the importance of representation in media and points out his own representation as a white male on his channel.
- Acknowledges not having interviewed a white man, but notes the presence of white males on his channel, including himself.
- Emphasizes a significant moment of personal growth in a critical message where the sender transitions from questioning trans ideology to recognizing the representation of trans women.
- Commends the sender for their personal growth and willingness to share it publicly.
- Expresses pride in the sender's evolution and suggests they continue to work on expressing their feelings more clearly.

### Quotes

- "I thought I was gonna get some nice sci-fi content. Instead, I got a bunch of woke bull."
- "I won't be taking the advice of people who can't define what a woman is."
- "Somewhere between the first paragraph and the second paragraph, this person, the light bulb came on."
- "I'm proud of you for growing as a person in that way."
- "If we could just get you to express your feelings a little bit more clearly, you might turn into a pretty decent person."

### Oneliner

Beau addresses a critical message, praises personal growth, and encourages clearer emotional expression in a thought-provoking reflection.

### Audience

Online content creators

### On-the-ground actions from transcript

- Share personal growth moments (exemplified).
- Express emotions clearly (implied).

### Whats missing in summary

The full transcript showcases a journey from criticism to personal growth, encouraging introspection and open expression.

### Tags

#PersonalGrowth #Representation #Criticism #OnlineContentCreators #EmotionalExpression


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about personal growth
and admitting personal growth when it happens.
And we're gonna do this because of a message I got.
You know, I did that interview
with Jesse over on the other channel
and it prompted a lot of great conversation,
prompted a lot of really nice messages,
and it prompted some messages that, well,
they weren't so nice, right?
But one of them, well, it wasn't nice.
Inside, there was just something so beautiful.
And I want to share it with you all.
Okay, so here's the message.
And again, it's not a nice message,
but inside it, there's something there.
I see you're still pushing trans ideology.
You're a disgrace.
I won't be using the advice of people
who can't define what a woman is.
I thought I was gonna get some nice sci-fi content.
Instead, I got a bunch of woke bull,
there's some colorful language in this
that we're gonna ignore, shoved down my throat.
You're trash.
It made me laugh though, when you talked about
how important representation is.
You still haven't had a white male on for an interview.
Why don't you take your own advice?
Where's our representation?
Always pushing the leftist agenda.
No normal content.
You're a disgrace.
Okay, so again, not a nice message.
And there's a couple of different pieces to it,
so we're gonna kind of zip through the first two.
Science fiction, generally speaking,
as a general rule, is woke.
The whole point of science fiction,
it's a method of pulling a topic,
a hard topic, out of its current framing,
and setting it somewhere else
so it can be examined freely without the bias.
That's its purpose.
Same thing with comic books, by the way,
just so you know.
And then it goes to the representation part,
because you still haven't had a white male
on for an interview.
Well representation is important.
I want to point out that there's a white male
in every episode over on the other channel.
Me.
I'm there a lot.
It's my channel.
Aside from me, though, the person with the most
screen time on that channel, other than me,
is another white male.
So there is representation,
even though I know white men are like,
you know, super underrepresented.
But in all fairness, it does say for an interview.
And I'm gonna have to agree with that.
I have not had a white man on for an interview.
I haven't interviewed myself.
I haven't interviewed the other Florida man.
Fine.
Okay.
You got me there.
But now I want to get to the part
that to me is just beautiful.
It's beautiful.
See in this first paragraph,
it says,
I won't be taking the advice of people
who can't define what a woman is.
That silly gotcha question.
You know, just being led around
by right wing pundits asking it.
It is what it is, right?
In the second part, though,
somewhere between the first paragraph
and the second paragraph,
this person, the light bulb came on.
A massive amount of personal growth happened.
And they were willing to share it with all of us.
Because it says,
you still haven't had a white man on for an interview.
And that's true.
That's true.
I've had more than one trans woman on though.
Right?
This person,
somewhere between the first paragraph
and the second paragraph,
they figured it out.
They answered that silly gotcha question themselves.
Because when they were scrolling through,
they saw women, not men.
Right?
Somewhere, maybe they went and read the actual definitions,
which would be a good place to start
if you were looking to define a word.
If you scroll through,
most of them are going to have something along the lines
of a person displaying feminine characteristics.
That's going to be one of the definitions.
So maybe they went and did that.
Or maybe they realized,
all on their own,
that it was a silly gotcha question.
And they came to the conclusion
that it's a social contract,
that it deals with gender.
It has to do with identity
and the way people present and stuff like that.
And they did it themselves.
And they were willing to share
that moment of personal growth
with tens of thousands of people.
I think that's beautiful.
I'm proud of you.
I'm proud of you for growing as a person in that way.
I do, I think it's beautiful.
Now, the funny part about that, though,
is I guess the other alternative
would be that they always knew the definition.
They always knew the definition.
They ask that question in the first paragraph in bad faith
because they think it's funny.
But when it comes down to it,
they know that, well, they're not men.
They're women.
So they don't see the representation there.
Could be either one.
I personally want to give the person
the benefit of the doubt
and believe that they had that moment of personal growth
and they were willing to share it with all of us
in such a beautiful fashion.
Now, if we could just get you to express your feelings
a little bit more clearly,
not have to put up that macho front,
you might turn into a pretty decent person.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}