---
title: Let's talk about a recap of the hearing and missing the forest for the trees...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ooUrPA4tbtk) |
| Published | 2022/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing a recap of a hearing and discussing the forest-for-the-trees issue with coverage.
- Key takeaways include the inciting tweet, the involvement of more Congress people, and the pre-planned Capitol march.
- Noteworthy moments from the hearing include Trump's former campaign manager attributing a death to Trump's rhetoric.
- The committee has shown that Trump continued to lie about the election results and attempted to legitimize baseless claims.
- Today's focus was on tying Trump personally to the events of January 6th, showing his role in inciting the Capitol riot.
- The evidence presented aims to directly link Trump's actions to the events of January 6th.
- While this hearing isn't a courtroom, the intent is to present evidence, keeping in mind the presumption of innocence.
- There will be a forthcoming video discussing the institution of the presidency and the protection of its integrity.
- Mention of ongoing criminal conspiracy attempts is made, possibly showing that current actions could be linked to past events.

### Quotes

- "Trump's rhetoric got someone killed."
- "Trump's former campaign manager said that Trump's rhetoric got someone killed."
- "Trump saw those tweets, that draft tweet, that meeting, everything shows that the events of the 6th can be directly tied to him as a person."

### Oneliner

Key takeaways from the hearing include tying Trump personally to the events of January 6th and showcasing evidence of his involvement, with implications for potential criminal charges and ongoing conspiracy.

### Audience

Congressional members, concerned citizens.

### On-the-ground actions from transcript

- Contact elected representatives to express concerns about the implications of the evidence presented (implied).

### Whats missing in summary

The emotional impact and tension of the hearing, as well as the potential implications for future legal actions based on the evidence presented.

### Tags

#Hearing #Trump #CapitolRiot #Presidency #CriminalConspiracy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about the hearing.
We're going to provide a little recap of the hearing
and talk about what they were trying to show.
We're going to do this because I've been reading a whole lot
of the key takeaways, and I feel like the coverage is,
in many cases, missing the forest for the trees.
They're talking about the individual pieces
that were laid out today and not talking
about the unifying theme, which in these hearings,
that's important.
And this is one of the issues with trying
to pack too much into one day.
So we're going to go through all of that
and then kind of refocus on what the overall theme is.
OK, so key takeaways from the day, individual pieces.
Obviously, the tweet.
The tweet that was the inciting tweet, bringing people in,
all of that stuff, and how it was received
by the right-wing ecosphere.
How they viewed it as a call to arms,
a moment for a red wedding.
It's dramatic, and I can see how people
are getting lost on that.
Then you have the idea that there are way more Congress
people involved in this than we know.
I believe the number of people who
are in Congress who will be eventually tied to this
is going to grow a lot.
We know now that the march was planned in advance,
not just the event, but the march to the Capitol.
We know this via a draft tweet.
You saw people regretting getting involved.
You saw people who said, you know, Trump tricked me.
I feel horrible.
He ruined my life.
All of this stuff.
And sure.
And that's, I mean, it's important to note.
You had a pretty high-profile moment
where Trump's former campaign manager said that Trump's
rhetoric got someone killed.
These aren't Democrats saying this.
And then you have the moment where
they're talking about the meeting,
and you're hearing about nothing but contempt and disdain
for the president.
It's like Navarro the other day saying
that Pence committed treason against Trump.
Treason is loving war against the United States.
It's not, you can't commit treason
against the president.
That's not a thing.
This is something that is outlined in the Constitution.
But they have a very authoritarian view.
They feel their loyalty is to a single person
rather than to the country, rather than
to the Constitution, right?
These are the takeaways that are being talked about.
What has the committee shown so far?
That Trump had been informed that he lost
by the people that matter and said, hey, you lost this.
Sorry.
It is what it is.
That after that point, he continued to lie,
continued with that rhetoric, continued
acting as though he had won, even though he
knew it not to be true.
This is the committee's case.
That Trump and team manufactured pretexts
to provide an air of legitimacy to their claims,
even though Trump had been informed that he lost,
that his claims were baseless.
Right?
And they showed that the goal was to subvert the election,
to overturn the will of the people.
This is what the committee has demonstrated thus far.
So what happened today?
They tied Trump as an individual,
as an individual person, to the events of the 6th.
Him.
Not the office, not his inner circle.
They tied him to the events of the 6th,
laying out that basically it wouldn't have happened
if it wasn't for him, if it wasn't for his actions.
His tweet, the response to that tweet, the red wedding stuff,
the call to arms stuff, all of that,
he was aware of how the right wing, how
the base of Trump world was viewing it.
He knew that that's what they took it as,
and he didn't call it off.
To the contrary, he amped up the rhetoric.
He amped up the rhetoric.
He put words back into speeches.
He incited the march to the Capitol.
He put it all into motion.
That's what the committee tried to show today.
That was their evidence.
That's what they brought about.
That's what the theme here is.
All of the individual stuff, yeah, I mean,
there's some pretty dramatic stuff there,
and there's some stuff that is worth covering
in and of itself on its own,
especially those who look at their involvement
and realize that they were duped.
I think that there should be a lot of coverage on that
because I think that would help a whole lot of people
understand what happened.
But the overall theme here was tying Trump as a person
to the events of the 6th.
Trump's rhetoric got someone killed.
It's what his former campaign manager said.
Trump saw those tweets, that draft tweet,
that meeting, everything shows that the events of the 6th
can be directly tied to him as a person.
That's what they're trying to show,
and that's the evidence that they presented.
Now, is this going to be enough for DOJ to indict?
I mean, yeah, I would point out that if this does proceed
in a criminal fashion, those warrants,
they're going to be super broad,
and there's going to be a whole bunch of other evidence
that comes up.
But we also have to keep in mind,
this hasn't been cross-examined.
This is not a courtroom.
This is the hearing, and you have to keep that in mind
because in the United States,
even though some people don't support the founding ideas
of this country or the Constitution
or any of that stuff,
I still do believe in the presumption of innocence.
So that was their intent.
Now, there will be a video coming out tomorrow,
it was supposed to go out tonight,
but overtaken by events,
about the institution of the presidency.
And you finally have those people,
those talking points starting to show up,
suggesting that the decision to charge Trump
is a policy decision rather than one of evidence.
And I think there might be a whole lot of people in Congress
or in government who believe the habit
of protecting the institution of the presidency
will somehow shield them.
That is not the case.
In order for the institution of the presidency
to be protected, there will be false people.
There will be people who will take the blame,
and it will be those who think it's going to help them now.
It won't. That's not how it works.
You need to look back at the other times
when the institution of the presidency was protected.
And then the other thing that keeps getting brought up,
there at the end,
Cheney talking about how a witness was contacted.
And most people are looking at that as,
look, it's evidence of obstruction,
witness tampering, so on and so forth.
Sure, maybe, but I don't think that's why
Cheney's bringing it up.
I think this is showing, attempting to show,
attempting to demonstrate that this is an ongoing
criminal conspiracy.
I think that's the goal there,
to show that there are still acts being committed
that are related to this,
meaning those people assisting now,
if this is charged in a broad enough fashion,
they will be viewed as being part of the Sixth,
even if they had nothing to do with it at the time,
because it's going to be a giant conspiracy.
I think that's what she's trying to do.
I don't know that. I don't have an inside source.
I haven't heard a rumor, to use other terminology.
That's just a gut feeling.
But I think it's an attempt to show that it's ongoing.
When we are watching these hearings,
remember, they're following the template.
Tell them what you're going to tell them.
Tell them what you told them.
It's all part of a theme.
Everything on every day will be linked,
and they're doing a really good job of sticking to that.
It just seems like the coverage is missing it today
because, I mean, you know,
there's a Game of Thrones reference,
and you have people involved
or formerly involved with these groups,
and you have a former campaign manager
saying that the president's rhetoric got someone killed.
I think people are getting lost,
and we need to focus on what's important overall.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}