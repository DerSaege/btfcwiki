---
title: Let's talk about the institution of the Presidency and policy decisions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jLUE-nHzOQk) |
| Published | 2022/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of preserving the institution of the presidency.
- Mentions the emergence of talking points as evidence links to the White House.
- Questions the idea that indicting Trump is a policy decision rather than evidence-based.
- Raises concerns about the unchecked executive power if former presidents are not indicted.
- Warns about the danger of allowing a president to be above the law.
- Emphasizes that the Constitution was written to prevent unchecked executive power.
- Condemns preserving the institution of the presidency over upholding the Constitution.
- Raises the potential threat of a president refusing to leave office.
- Points out the risk of setting a dangerous precedent for seizing control using military support.
- Describes the situation as suggesting an attempted self-coup with no accountability.
- Warns that without checks, the presidency could devolve into a dictatorship.
- Stresses the importance of upholding the checks and balances within the Constitution.
- Urges to prioritize the Constitution over preserving the office of the presidency.
- Concludes by warning against sacrificing the Constitution for the sake of the presidency.

### Quotes

- "Preserving the institution of the presidency by allowing unchecked executive power is the ultimate betrayal of the Constitution."
- "The Constitution was written to protect this country from unchecked executive power."
- "The institution of the presidency is worthless. It is worthless."
- "If there's no accountability for that, it's not a failed coup attempt. It's practice."
- "Preserving the office of the presidency at the expense of that document is a pathway to disaster."

### Oneliner

Beau explains the danger of preserving the institution of the presidency over upholding the Constitution and warns against allowing unchecked executive power.

### Audience

Advocates and defenders of constitutional integrity.

### On-the-ground actions from transcript

- Challenge and question policies that prioritize preserving the presidency over upholding constitutional values (implied).
- Advocate for accountability and checks on executive power within the government (implied).
- Support initiatives that prioritize constitutional integrity over individual office preservation (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the risks associated with prioritizing the institution of the presidency over constitutional integrity.

### Tags

#Presidency #Constitution #UncheckedExecutivePower #Accountability #InstitutionPreservation


## Transcript
Well, howdy there, internet people. Let's be able to get on.
So today we're going to talk about the institution of the presidency
and what that term means
and why it might
become pretty important soon.
Since
all this started with the committee,
I've kind of talked about the fact that there will be people
who want to preserve
the institution
of the presidency.
The idea behind that is that the integrity of the office itself is supreme.
That's what makes the United States function.
Therefore, protecting the institution is what matters because the office is more important
than any individual president.
I mean, it sounds good in theory and all that, right?
on cue right as the committee is starting to tie the pieces of evidence
together and provide evidence of links to the White House. We're starting to see
talking points emerge because this same evidence, this same narrative, this same
storyline may someday be examined in a criminal case, not during a hearing. And
And there's two that I've already picked up on. Two of these talking points. One is, well, it's South American countries
that indict, you know, former presidents.
Yeah, that's American exceptionalism at its finest right there. And then the other is that DOJ has a policy decision to
make.
policy decision. Whether to indict Trump is a policy decision. That's weird. I always
thought it had to do with evidence, but I guess not. I guess it must be like that.
Policy decision that says they won't indict a sitting president. But if they
won't indict a sitting president and they make the policy decision to not
indict a former president, what does that mean? It means the president is above the
law. It means the president has unchecked executive power. Why was the Constitution
of the United States written? What was its main goal? To prevent unchecked
executive power. Checks and balances, right? We all heard about it in grade school.
That's what's contained in it.
But the real reason it's written the way it is was unchecked executive power, because
that's what they'd been dealing with.
That was the goal.
Preserving the institution of the presidency by allowing unchecked executive power is the
ultimate betrayal of the Constitution.
It's the whole reason it's written that way, is to prevent the very thing that people are
now quietly starting to gear up to advocate for, preserving the institution, the office.
That's what's important.
More importantly, if a president can't be indicted while they're in office, and they
be indicted afterward, what is to stop President Biden from walking out right now and saying,
I'm boss, I'm not leaving? The will of the people? What if he ignores it? Setting this policy
decision, it is setting the stage for a president who can gather the support of the military and
and the law enforcement complexes in this country
and seize control.
That's what it's doing.
When you are talking about an instance like this,
this isn't normal unchecked executive power.
If you are looking at the evidence that
is being presented by the committee, the narrative
and storyline that they are laying out,
It is suggesting an attempted self-coup.
That's what they're suggesting.
If there's no accountability for that,
it's not a failed coup attempt.
It's practice.
And they will try again and again and again
until they succeed, because there's nothing stopping them,
The Constitution was written to protect this country from unchecked executive power.
The kind that Trump tried to exercise.
The institution of the presidency is worthless.
It is worthless.
It means absolutely nothing if there is no check on it.
there's no check, it will devolve into a dictatorship, into totalitarianism. That's
why those checks were written that way. I know there are a lot of people who care
about those institutions. Those institutions were framed in that document
and whether you support that document or not, if you're somebody who looks to the
institution of the presidency as something that is sacred, that document
came first. Preserving the office of the presidency at the expense of that
document is a pathway to disaster. It's not a policy decision. It shouldn't be.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}