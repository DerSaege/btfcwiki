---
title: Let's talk about NASA and the Dark side of the moon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YPSi5mTltho) |
| Published | 2022/07/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA is planning to send unmanned payloads to the dark side of the moon, specifically to Schrodinger's crater, in 2025.
- The mission aims to gather information on the moon's history, general environment, impact from space objects, earthquakes, moonquakes, and seismic activity.
- This will be the first time NASA places anything on that side of the moon due to the challenges like no direct line of sight and the need for satellite relay for communication.
- Artemis, the program associated with returning humanity to the moon, is named after a Greek goddess heavily linked with the moon and Apollo's twin.
- NASA's mission to the dark side of the moon is geared towards better preparing for manned endeavors and astronaut trips.
- Beau finds stories like this inspiring as they propel humanity beyond Earth, towards new chapters in space exploration.
- Beau expresses excitement over events like this, which move humanity towards exploring beyond Earth.
- He believes these small steps are paving the way for humans to venture into space and embrace a new future.
- Beau suggests that, if humanity perseveres, the idea of having just one Earth might not hold true forever.
- He acknowledges the importance of considering the advancements on the horizon like space exploration.

### Quotes

- "At some point, assuming we don't filter ourselves out, we will be in other places."
- "These little steps, these are the first steps towards us really getting off of this rock."
- "It's events, launches, stuff that is propelling us off of this rock that I have always found inspiring."
- "Maybe it won't be true as long as we can make it till then."
- "Every once in a while it's important to note that stuff like that is on the horizon."

### Oneliner

NASA plans to send unmanned payloads to the dark side of the moon in 2025 to gather vital information, paving the way for future manned endeavors and inspiring humanity's journey beyond Earth.

### Audience

Space enthusiasts, Science enthusiasts

### On-the-ground actions from transcript

- Stay informed about NASA's Artemis program and future space exploration missions (implied).

### Whats missing in summary

The full transcript provides a detailed insight into NASA's upcoming mission to the dark side of the moon, touching on its significance in space exploration and humanity's future beyond Earth.

### Tags

#NASA #Artemis #SpaceExploration #MoonMission #Humanity #FutureInSpace


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about NASA
and Artemis and Schrodinger and Pink Floyd. Not really, but we are going to talk about the dark
side of the moon. I think most people are familiar with the Artemis program, which is slated to
return humanity to the moon around 2025, if everything goes according to plan. Artemis,
by the way, is the name of a Greek goddess who is heavily associated with the moon,
who also happens to be Apollo's twin. Isn't that cute? That is the big news. Like that's the thing
that is getting all of the attention. Last week NASA announced that it will be sending
unmanned payloads to the dark side, to the far side of the moon, to Schrodinger's crater.
The touchdown is also in 2025. This will be the first time NASA has put anything on that side of
the moon because it's, well, it's difficult. There's no direct line of sight, there's no radio,
everything has to be relayed out via satellite. China got there first, I want to say it was 2019,
fact check that, but it's difficult. So it'll be the first time NASA is there. The idea is to
deposit these payloads to conduct experiments and gather information on an area that we're not going
to get otherwise. The information being collected has to do with the moon's history, its general
environment, how often it gets hit by stuff from space, earthquakes, moonquakes, seismic activity.
How about that? And all of this is hopefully going to better inform trips for astronauts,
for manned endeavors. Stuff like this is always exciting to me. You know, we get bogged down
in everything that is going on here on Earth. And it's stories like this, it's events, launches,
stuff that is propelling us off of this rock that I have always found
inspiring in a way. At some point, assuming we don't filter ourselves out,
we will be in other places. And you know, that saying, you know, we only have one Earth,
maybe it won't be true as long as we can make it till then. These little steps,
these are the first steps towards us really getting off of this rock and getting out into the
expanse of space and propelling humanity into a very new chapter.
And I think every once in a while it's important to note that stuff like that is on the horizon.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}