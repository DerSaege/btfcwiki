---
title: Let's talk about GOP vs Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wYAMTyLGyK4) |
| Published | 2022/07/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is being urged to delay announcing his run until after the midterms, with Kellyanne Conway publicly supporting this notion.
- Some speculate that Pence and McConnell are trying to weaken Trump by pressuring him not to announce before the primary elections.
- There are concerns that if Trump announces now and wins, he will become stronger, which is not in the interest of certain factions within the Republican Party.
- Trump is perceived as a liability by some in the Republican establishment, who want to return to business as usual without his interference.
- Some believe that Trump's chances of winning have diminished, especially with the baggage of January 6th and other controversies.
- Delaying his announcement until after the midterms could further weaken Trump and suit the goals of those who want him out of the picture.
- While some within the MAGA world may disrupt establishment plans, Trump remains the primary threat to their positions.
- There is a strategy to portray Trump as a loser to discourage him from running, with hopes of minimizing potential losses for the Republican Party.
- Beau expresses a desire for Trump to delay his announcement, believing it will decrease voter turnout on the Democratic side and ultimately weaken Trump's political power.
- If Trump doesn't announce before the midterms, his best chance may be to break with the Republican Party and form a third party to maintain any political influence.

### Quotes

- "Delaying his announcement until after the midterms could further weaken Trump."
- "Their goal is to delay his announcement in hopes of sinking his candidates and removing the MAGA faction."
- "If Trump doesn't announce before the midterms, his political power's gone."
- "The idea that he is a defeated candidate, that's a statement of fact."
- "Delaying his announcement until after the midterms could further weaken Trump."

### Oneliner

Trump faces pressure to delay announcing his run, fearing stronger opposition; internal politics seek to sideline him and remove the MAGA faction.

### Audience

Political strategists

### On-the-ground actions from transcript

- Pressure Trump to delay his announcement (implied)
- Advocate for a shift in dynamics within the Republican Party (implied)

### Whats missing in summary

In-depth analysis of the potential consequences of Trump's announcement timing.

### Tags

#Trump #RepublicanParty #MAGA #ElectionStrategy #PoliticalPressure


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk a little bit more about Trump being pushed to hold
off on announcing his run, because now Kellyanne Conway even has publicly said
that he needs to wait until after the midterms.
I got a message from somebody and not a typical viewer of this channel, but it's
worth summarizing because this person actually has a pretty good read on this.
I support Trump. I only watch you because I know where you stand and you're at
least honest about where you stand. I know you don't like Trump under
statement of the year, but honestly, if he doesn't announce before the election
has he been... I don't have a cute euphemism for this... emasculated by Pence
and McConnell. They're pushing him under the bus, aren't they? Pence was nothing
without him. He has to announce before the midterms to be able to take credit
for leading his endorsed candidates to victory, right? They're all just pushing
him to not announce so he'll lose the primary and be weaker, right? Right. Yeah.
He is being outmaneuvered right now. Their view of him, and I know it's not
gonna be your view, but their view of him is that he's he's baggage. He's not
worth anything. If they believed that he was a benefit to them, again we're not
talking about the Republican Party right now, we're talking about internal
politics, if they believed that he was a benefit to them they'd want him to
announce. Their concern is that if he announces now and he wins he's going to
be stronger. So their goal is to basically delay his announcement in hopes that that
further sinks his candidates and they can get rid of the MAGA faction of the Republican
Party without having to do anything themselves. All they have to do is pressure Trump to delay
his announcement.
Those who actually???and keep in mind, there are a lot of the people who just care about
the Republican Party who are also pressuring him to do it.
Those people know that he couldn't beat Biden before, and now he has all of the baggage
of the sixth and everything else.
He's not a winning candidate anymore.
He's really not.
It would be very hard for him to stage a comeback.
Could he do it if he put off his announcement until after the midterms?
Maybe.
He has proven himself to be very resilient, but the longer he delays his announcement,
the weaker he gets.
And that's what the Republican establishment wants.
They're tired of him getting in the way of business as usual.
His rhetoric was fine and all to energize the base.
But now what they want to do is take Trumpism, take that rhetoric, and just use the rhetoric,
continue business as usual, and feather their own nests.
That's what they're looking to do.
If he doesn't announce before the midterms, he'll probably be able to do it.
Now there are other people within the MAGA world, people who have his ideology, who may
come out of nowhere when it comes to the establishment in the Republican Party and disrupt their
plans.
But to them, nobody is as much of a threat to their position as Trump is.
So they want him weak.
And all of those people who he pushed around when he was in office, all those people that
he used Twitter to get to kneel before him, they're all getting even right now.
They're all going to push him to delay because they know that's what will make him weak.
Because right now, what's the read?
The read is they don't want him to run because he's a loser.
And if he comes out and he announces, well, that's just going to make him lose a whole
bunch of seats.
So they're already casting him as a candidate that's worthless.
And as you said, I'm honest, I want Trump gone.
I hope he announces after the primary.
I would be willing to risk the voter turnout because if he announces before, he will increase
voter turnout on the Democratic side.
But if he doesn't announce before, yeah, he's pretty much done.
Even if none of the lawsuits, none of the investigations, if nothing else pans out,
his political power's gone.
So yeah, I hope that he holds until after the midterms.
Now at the same time, it's worth noting that I know you're a supporter, but if he announces
before, you may not like the outcome.
Because the idea that he is a defeated candidate, that's a statement of fact.
did lose, and because he lost, he will have a negative impact on some races.
He may be done anyway, but if he doesn't announce beforehand, before the midterms, his only
chance is to basically break with the Republican party, go third party, and try to siphon off
enough of the America first crowd into the MAGA party or whatever and try to become president
that way because if he doesn't announce before the midterms he's not going to have the internal
power within the Republican party.
Not unless there's a serious shift in the dynamics and right now everything is moving
away from him, and I for one am glad. And to be honest, the only reason I'm saying
this is because I don't think there's anything that can be done to change it.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}