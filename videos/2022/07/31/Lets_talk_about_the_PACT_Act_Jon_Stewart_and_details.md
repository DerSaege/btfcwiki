---
title: Let's talk about the PACT Act, Jon Stewart, and details....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zo0Bb9C0NfI) |
| Published | 2022/07/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the PACT Act and Jon Stewart's involvement in advocating for it to provide medical care to veterans exposed to toxic substances while working for the US government.
- Details how the bill aims to cover millions of people exposed to substances like Agent Orange and burn pits, with an estimated cost of $280 billion over 10 years.
- Notes that the bill didn't pass the Senate due to Republicans not backing it, leading to a filibuster.
- Points out the shift in Republican support from 84 votes in June to 55 votes in July, with a change in reasoning to make the funding discretionary rather than mandatory.
- Questions the Republican Party's stated reasons for the change and speculates on possible motives, including obstructionism or retaliation against Democratic reconciliation deals.
- Condemns the delay in providing healthcare to sick veterans and criticizes the idea of making the funding discretionary as a potential method for future cuts.
- Mentions a planned Senate vote on the bill and expresses concern over the impact of delays on veterans' health outcomes and time with their families.

### Quotes

- "Republicans are patting themselves on the back for denying health care to veterans..."
- "Every day this drags on over, in best case scenario, an accounting issue."
- "It's high stakes. People are sick. They need the treatment."
- "The longer the treatment is delayed, the more likely it is they don't have a positive outcome."
- "It's time sensitive because people are sick right now."

### Oneliner

Beau breaks down the critical details of the PACT Act, exposing the stakes for sick veterans and questioning Republican obstructionism with high emotional investment.

### Audience

Advocates, Veterans, Supporters

### On-the-ground actions from transcript

- Contact your Senators to support the PACT Act and urge them to prioritize veterans' healthcare (suggested).
- Join advocacy groups working towards ensuring medical care for veterans exposed to toxic substances (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics surrounding the PACT Act, shedding light on the importance of timely medical care for sick veterans and raising questions about potential Republican motives.

### Tags

#Veterans #PACTAct #Healthcare #RepublicanParty #Advocacy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the PACT Act and Jon Stewart
and stated reasons and possible reasons.
And I'm going to provide some details about this
because this subject got very impassioned
and I think some of the details got lost.
And to me, the details are important on this.
Emotions ran high.
Now, as far as commentary goes, yeah, I pretty much
co-sign everything Jon Stewart said.
But the details are still important.
Okay, so what is it?
If you have no idea what I'm talking about,
it's a bill sitting in the Senate.
The purpose of this is to provide medical care
to veterans who, while they were working for the US government,
were exposed to toxic substances.
This is something that would impact millions of people.
This would absolutely save lives.
And it's time sensitive because people are sick right now.
It's wide-ranging.
It would cover people who were exposed to Agent Orange
decades ago and people who were exposed to burn pits
recently.
Overall price tag on this looks to be around $280 billion
for 10 years, so $28 billion a year.
It's sitting in the Senate because Republicans
wouldn't back it, so it didn't get over the filibuster.
Didn't get past the filibuster.
Now, in July, in July, it only got 55 votes.
It needed 60.
The thing is, in June last month, 84 votes.
It got over it, but there was a technical issue.
Somewhere between June and July, well, a problem developed.
Republican Party decided it had made a mistake,
and it didn't really want to support this
because the people who are sick have all the time in the world,
I guess.
They can just wait until everybody figures it out.
Now, for me, I'm one of those people
who believes everybody deserves health care, right?
And to me, this is a little different,
but it's not actually the veteran thing
that makes it different.
This is more like workers' comp.
They were working for the US government when they got sick.
It only stands to reason that the US government
puts the bill.
I mean, I don't think that that's controversial.
Now, the Republican Party's stated reason for the change
is that they don't want it to be mandatory spending.
They want it to be discretionary.
They want it to go through appropriations, right?
Every year, they get to fund it.
Or not fund it, I guess.
There's a lot of speculation about their possible reasons,
because a lot of people don't believe that.
Because, I mean, that's kind of a garbage reason,
to be honest, right?
You're not going to make people wait even longer
for health care simply because of how it's getting funded
in this regard, unless, of course, the idea
is to not fund it, right?
I mean, if you were going to fund it,
if you actually supported it, you
wouldn't mind if it was mandatory.
It just seems odd.
So a lot of people don't buy that.
The two leading possible reasons are,
one being that it's standard Republican obstruction.
There's a habit right now of voting against things
that are good for the American people, because deep down,
it's good for the Republican Party
if the American people suffer right now, so they blame Biden.
The other possible explanation is
that they're doing it because the Democratic Party might
have worked out a reconciliation deal on their side,
and this is them lashing out like an entitled child.
Those are the two possible explanations
that people are giving.
That's speculation, though.
Nobody knows that.
Let's go back to what they said.
They're holding this up because it's
funded through a mandatory mechanism,
rather than going through appropriations,
rather than being discretionary.
To me, the only reason that that would be a concern
is if you planned on cutting it later.
I don't see any other possible reason for this to be an issue.
So forget about the speculation.
Their stated reason is bad enough, in my eyes.
Now, this isn't over.
I want to say they're going to try to push it through again
on Monday.
We don't know what's going to happen,
because Republicans are patting themselves
on the back for denying health care to veterans who
got exposed to this stuff while they were working for the US
government.
And I know that sounds like hyperbole,
saying they're patting themselves on the back,
but I'm not going to be saying they're patting themselves
on the back.
Immediately after the vote, they were fist bumping.
So there are a lot of emotions about this,
because it's high stakes.
It's high stakes.
People are sick.
They need the treatment.
The longer the treatment is delayed,
the more likely it is they don't have a positive outcome.
Every day, this drags on over, in best case scenario,
an accounting issue.
It's something that's going to cost people
time with their families.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}