---
title: Let's talk about an order from Karl Rove to Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xd2CH5PmesU) |
| Published | 2022/07/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Karl Rove, a Republican strategist, wrote an op-ed questioning where Trump's donations go, suggesting Trump should wait until after the midterms to announce his presidential campaign.
- Rove advises Trump to avoid upstaging the midterms and to set up a new committee for his campaign's political expenses.
- Trump, if he defies Rove's advice, risks becoming another pawn in the Republican establishment or facing their opposition.
- The Republican Party seems to be turning against Trump, viewing him as a liability if he announces before the midterms.
- Trump's decision on when to announce his campaign could impact his influence over the MAGA movement within the Republican Party.
- Any announcement by Trump before the midterms may lead to internal conflicts within the Republican Party.

### Quotes

- "He is either going to fall in line and do what he's told like an obedient lackey, or they're going to destroy him."
- "If he goes along with it, he's done. He has no power anymore."

### Oneliner

Karl Rove advises Trump on campaign timing, signaling a potential power shift within the Republican Party.

### Audience

Political strategists

### On-the-ground actions from transcript

- Monitor Republican Party dynamics closely (implied)

### Whats missing in summary

Analysis of the potential consequences of Trump's decision on announcing his campaign.

### Tags

#Politics #RepublicanParty #Trump #KarlRove #CampaignStrategy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to continue on our theme from earlier.
And we're going to talk about an order that Trump got from Karl Rove.
Did y'all hear that? It's Wolf. It's weird. It's because I used his full name.
If you're older, been paying attention to politics a while, you know who Karl Rove is.
For Gen Z, he was depicted in the cartoon American Dad.
He shows up to help somebody win an election, and he's just wearing robes, like a hood.
Very magical looking and everything.
At some point, the robes blow open and one of the children starts screaming,
where does its food go? That's Karl Rove.
He's not a Republican operative. He's the Republican devil.
He's a scary story that Republicans tell their children to get them to behave.
Make sure you wear your red tie with your blue blazer or Karl Rove is going to get you.
Now, he wrote an op-ed, and the title was, Where Do Trump's Donations Go?
And there's a lot of window dressing in it.
It talks about how Trump's brought in like $121 million to his pack,
but he's only given like $365,000 to the candidates he's endorsed, so on and so forth.
But none of that really matters.
What matters is if Mr. Trump decides he must upstage the midterms and announce this fall,
rather than waiting, he'll immediately need to file a new committee for his presidential campaign
to pay his political expenses.
Karl Rove is telling Trump to wait until after the midterms to announce.
And the reality is, Trump kind of has to do it.
Unless Trump thinks he has more chips in the game than Karl Rove.
He doesn't. Nobody does.
Remember Crossroads Zills? Karl Rove has all the chips.
But it puts Trump in a pretty unenviable position.
Because what this shows is that the Republican establishment, they are done with Trump.
They are over Trump.
He is either going to fall in line and do what he's told like an obedient lackey,
or they're going to destroy him.
The amount of information coming out saying you do not get to announce until after the midterms,
that's what's going on.
And Trump, I mean he probably should listen to him.
But at the same time, if he does, well, then the reality is he's just become another rhino.
Another cog in the Republican will doing what he's told.
If he wants to maintain any hope of, I don't know, controlling the MAGA movement within the Republican Party,
because there's no way he can control the Republican Party now,
but if he wants to maintain some kind of grip on the MAGA movement,
his only option is to announce now, before the midterms.
And if he does that, it's going to start a little pushing contest inside the Republican Party.
One that I don't think Trump can win, to be honest.
I would imagine we will see more and more of this, because as we talked about before,
the Republican Party is worried about Trump announcing before the midterms,
because they know he's a liability.
He's a defeated defendant.
He's not going to help him if he announces.
So they're going to try to stop him.
But if he goes along with it, he's done.
He has no power anymore.
Anyway, it's just a thought. Thanks for your time.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}