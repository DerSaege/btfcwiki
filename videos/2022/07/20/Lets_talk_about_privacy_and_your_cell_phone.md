---
title: Let's talk about privacy and your cell phone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b3xN_EP-iQ8) |
| Published | 2022/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The ACLU is investigating government agencies buying bulk data from cell phones to track people's movements and develop a "pattern of life."
- The Supreme Court previously ruled in Carpenter v. U.S. that authorization is needed for such surveillance, but this current practice involves gathering information from over 250 million phones daily.
- The ACLU discovered more than 100,000 location markers in a three-day period in just one region of the U.S.
- There's bipartisan support for the Fourth Amendment is Not for Sale Act, which aims to prevent government agencies from bypassing the Fourth Amendment by purchasing information from third parties.
- Beau questions how the current Supreme Court, with some members not strongly supporting privacy rights, will respond to this issue.
- He warns that those cheering for reduced privacy protections may eventually have their own privacy rights affected.
- Beau believes that supporting the Fourth Amendment is Not for Sale Act is the most direct way to combat this surveillance practice.
- However, he acknowledges that the act's support might be impacted by other pressing political issues.
- The information gathered from cell phones can reveal detailed aspects of individuals' lives, such as their locations and social circles.
- Government agencies currently do not need a warrant to access this data; they can simply purchase it from third parties.

### Quotes

- "The ACLU is obviously fighting this."
- "At current, it seems like the best way to curtail this practice is the Fourth Amendment is not for sale act."

### Oneliner

The ACLU investigates government agencies buying bulk data to track individuals' movements, prompting bipartisan support for the Fourth Amendment is Not for Sale Act amid concerns over privacy rights.

### Audience

Privacy advocates, concerned citizens

### On-the-ground actions from transcript

- Support the Fourth Amendment is Not for Sale Act (suggested)
- Stay informed about privacy rights issues and advocate for transparency (implied)

### Whats missing in summary

The full transcript provides detailed insights on the ACLU's fight against government surveillance practices and the potential implications for privacy rights, warranting a deeper dive into the ongoing developments.

### Tags

#PrivacyRights #GovernmentSurveillance #FourthAmendment #ACLU #BipartisanSupport


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the ACLU,
American Civil Liberties Union,
privacy, the Fourth Amendment, and your cell phone.
The ACLU just put out some information.
See, back in 2020, the Wall Street Journal broke a story,
and the ACLU's been following up on it ever since,
trying to use the Freedom of Information Act
to gain access to information,
to get some government records to clarify what's going on.
And it involves government agencies buying bulk data
that was retrieved from people's cell phones.
And according to the documents,
the information is so specific that it could track people
and develop what they're calling a pattern of life.
That's pretty intensive surveillance,
even though it's very passive.
Now, a few years ago in Carpenter v. U.S.,
the Supreme Court said, you know,
you kind of need authorization to do something like this.
And that was in a case about one person.
According to the documents the ACLU has obtained,
this practice can gather information
from more than 250 million phones every day.
It's a big deal.
They looked at a three-day period,
and over that three-day period,
they found more than 100,000 location markers.
And this is just from one area
in the southwestern United States,
because they've been having to fight to get the records.
So the records they have are incomplete.
They don't actually show how widespread this is.
But the information that's provided with what they have
is pretty startling.
So there is an attempt to curtail this.
It's called the Fourth Amendment is Not for Sale Act,
and it has bipartisan support
from Senators Ron Wyden and Rand Paul.
Basically, this act would preempt law enforcement.
It would stop government agencies
from kind of doing an end-run around the Fourth Amendment
and buying the information from somebody else.
If you were to take this out of the digital age,
the suggestion here is that, well,
the government can't get a warrant to search your house,
so they just pay somebody else to do it.
I don't know how this is going to be viewed
by the Supreme Court.
I don't know how the current court
is going to respond to this.
There are people on the court
who aren't too supportive of privacy rights,
as we have found out.
This is one of those moments when people
who might be cheering the elimination of privacy
in one direction are going to find out
that those same people will remove
your privacy rights as well.
At current, it seems like the best way
to curtail this practice is the Fourth Amendment
is not for sale act.
That seems like the most direct route.
However, how much support it's going to get in a climate
that is charged with a whole bunch
of other political issues
that may push general privacy down on the priority list,
we probably don't know.
To me, this should be a priority for most Americans.
The type of information that would be described
as something that can develop a pattern of life,
that's pretty specific.
It's going to say where you go, who you meet with,
because your phones are in close proximity.
There's a lot of information that can be gathered
via your phone this way.
And the way government agencies are currently doing it,
it doesn't require them to have a warrant.
They can just purchase it from a third party.
The ACLU is obviously fighting this.
They're investigating it right now
to determine how widespread it is,
but this definitely falls into the purview of things
they're going to push back against hard.
This is a story that will continue developing
probably over the next year.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}