---
title: Let's talk about the probably not last hearing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aZPB7wp-WpE) |
| Published | 2022/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Previewing the upcoming hearing, suggesting it will showcase previously known details about Trump's actions not in line with his constitutional duty.
- Speculating on whether the committee's decision to exclude publicly known information was bad planning or strategic politics.
- Predicting that the hearing will focus on showcasing Trump's inaction and lack of efforts to stop certain events.
- Anticipating potential future hearings to make broader allegations and connect the missing pieces.
- Expressing skepticism about this being the final hearing and expecting more to come.
- Noting that the upcoming hearing might seem repetitive to those who have followed previous ones, with live testimony from insiders in the Trump White House.
- Hoping for new information to be revealed, while also expecting a rerun with emphasis on Trump's inaction.
- Mentioning significant pieces of information left out from previous hearings, hinting at a planned follow-up to complete the narrative.
- Concluding with uncertainty about the ending of the hearing and the possibility of missing information being addressed in the future.

### Quotes

- "Trump didn't act to end it."
- "He didn't attempt to stop it because he wanted it to happen."
- "There's information I feel like should have been brought in."

### Oneliner

Beau speculates on the upcoming hearing, questioning the committee's exclusion of certain information and hinting at potential future revelations.

### Audience

Political analysts

### On-the-ground actions from transcript

- Stay updated on the developments from the hearings (implied)
- Analyze the information presented critically (implied)

### Whats missing in summary

Insights on the potential impact of future hearings.

### Tags

#PoliticalAnalysis #TrumpAdministration #HearingSpeculation #CommitteeStrategy


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about the hearing.
Prime time. Coming up on Thursday, right? This should be the one where they pull the mask off
and Trump screams, I would have got away with it if it wasn't for that meddling committee, right?
Sticking to the template that we talked about before this started. Tell them what you're going
to tell them, tell them, tell them what you told them. This next hearing should be the tell them
what you told them phase. And sure enough, there's reporting from AP. Panel members say Thursday's
hearing will be the most specific to date in laying out and weaving together previously known
details on how Trump's actions were at odds with his constitutional legal duty. Okay. So they're
sticking to it. This is tell them what you told them. So it seems like this hearing will be
basically showcasing everything that they've already shown, tying it together with inaction
and framing it for the prime time audience that didn't see all of the other hearings.
Right? This makes sense. Here's the thing. There is publicly known stuff that wasn't brought in,
that didn't get tied in. Now we kind of have to wonder, is this bad planning on the committee's
part or is it good politics? We'll find out if there's more hearings. If there's more hearings,
then I think this was planned. I think that there is stuff that they intentionally did not bring up
so they could showcase it later. What it seems like they're doing is setting the stage.
At this point, they're going to very loudly proclaim Trump didn't stop it and we showed that.
We showed that he put it in action and just really bring it together, that Trump didn't act to end it.
And then later in hearings that aren't scheduled, that don't exist, they're going to make some broader allegations.
And if that happens, I definitely believe it was planned since the beginning.
Unique, nice twist, but we have to wait and see because as of this moment, this is theoretically
the last hearing. I don't believe it's going to be, though, to be really clear on that. I'm sure
there will be more. So watching this, for most of those people who have kept up with the hearings,
this may seem a little bit like a rerun. They have some pretty important people who are going to be
giving live testimony, and these are insiders inside the Trump White House.
How provocative that testimony is going to be, well, we're going to have to wait and see.
One of the people in particular has the ability to have access to a whole lot of information.
So we'll just, we can keep our hopes up for something new. But overall, I would be expecting
more or less a rerun with the addition of he didn't do anything. He just watched TV.
He didn't attempt to stop it because he wanted it to happen, but I don't know that they're going to say that part.
So to me, it seems like some stuff was left out. They're pretty big pieces.
They're pieces we've talked about on the channel, not secret stuff, but they didn't lay it out
in the previous hearings. So my best guess is that they have a follow-on that's planned.
You have up to this point getting the American people used to the idea that their president
did nothing while this was occurring, and then bring in the rest.
But that's gut at this point because this seems like a very odd way to end the hearing.
There's information I feel like should have been brought in.
So we'll have to wait and see what happens on Thursday. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}