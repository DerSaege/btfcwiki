---
title: Let's talk about what Greek statues can teach us about American history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0BwW7H1-Ya0) |
| Published | 2022/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the Republican Party's stance on public education in response to a viewer's feedback.
- Talks about the importance of revisiting history, specifically mentioning Greek statues and their original coloring.
- Disputes the idea that new historical information always attacks conservative heroes.
- Mentions how information that challenges the existing narrative often gets overlooked until it enters public debate.
- Emphasizes the need to confront uncomfortable aspects of American history to fulfill the promises of the founding documents.
- Urges for a critical examination of history and a willingness to acknowledge the country's past shortcomings.
- Warns against viewing history through a rose-colored lens and advocates for facing the uncomfortable truths it reveals.

### Quotes

- "President Lincoln was not a conservative. He was a progressive who got fan mail from Karl Marx."
- "It's not new history. It's not revisionist history. It's just history."
- "If you're ever reading a book and you walk away from it thinking, yay, everything's great, you're not reading history."

### Oneliner

Beau addresses misconceptions about history, urging for a critical examination of American heroes and the country's past to fulfill its founding promises.

### Audience

History enthusiasts, educators, students

### On-the-ground actions from transcript

- Question existing historical narratives and seek out diverse perspectives (implied)
- Encourage critical thinking about historical figures and events in educational settings (implied)

### Whats missing in summary

A deeper dive into the impact of mythologized history on societal perceptions and the importance of confronting uncomfortable truths for progress.

### Tags

#History #AmericanHistory #RevisionistHistory #Education #CriticalThinking


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about what Greek statues
can teach us about American history,
revisionist American history, that new American history.
We're going to do this because in a recent video,
I talked about the Republican Party's stance
when it comes to public education,
and it prompted a message.
Your last video about the Republican Party and education
bothered me when you said the Republican Party
didn't like history.
No, we just don't like your history,
all capital letters for your new history.
I looked up what you said about Karl Marx and Abraham Lincoln.
I found it, but how am I supposed to know that's real?
Why is it that every piece, every new piece of history
attacks a conservative hero?
It's revisionism.
That's the history we don't like
and don't want taught to our kids.
Why would we want to trash conservative heroes
if we are conservatives?
Use your head, man.
President Lincoln was not a conservative.
He was a progressive who got fan mail from Karl Marx.
He was not a conservative.
The Republican Party was not the conservative party
at that time.
That's the kind of history that we're talking about, though.
And what's your response to that information?
How am I supposed to know that's real?
In the video you're talking about,
I say the only solution for the Republican Party
is to not teach history
and to encourage conspiratorial thinking, and here we are.
Do you honestly think there's a giant conspiracy
to link Abraham Lincoln to Karl Marx?
That letter's been known about.
It's just not something that was in public discussion
because the Republican Party keeps hammering it in.
The party of Lincoln, the party of Lincoln,
trying to tie themselves to his progressive ideas, right?
They don't want to stand on the conservative position
of the time, which was the status quo.
Lincoln was a progressive,
so tying him to Karl Marx isn't trashing a conservative hero
because he wasn't a conservative.
It's also just history.
It's fact.
It's something that happened.
And you say, why is it that every new piece of information
attacks a conservative hero?
That's not true either.
It's just that the stuff that attacks
a perceived conservative hero
is what they tell you to be mad about.
New information, or more accurately,
information that has been known for a long time
that enters the public discussion
and kind of changes the way we look at history,
that occurs all the time.
And it doesn't always have to do with conservative figures
or those that people believe are conservative.
Right now, and this is a good one
because this is something that I thought everybody knew
because it was talked about in a lot of history books.
But somehow, it just never got picked up in public discussion,
didn't become common knowledge, right?
I want you to picture Greek statues,
you know, the ones that you see in the movies
that were featured in Hercules.
Describe them.
They're beautiful, right?
They're gorgeous.
And they're all white.
No, they weren't.
They were brightly colored when they were made.
The paint faded over time.
This information is viral right now because of an art show.
It's showing up because of an art show.
This isn't new information.
People knew about this for a very, very, very long time.
But because it was presented in public
and because there were some viral articles about it,
it has entered public discussion.
It's a completely nonpartisan piece of new history,
revisionist history, I guess.
It happens all the time.
They just don't tell you to be mad about the stuff
that doesn't impact their narrative.
A lot of what we know about American heroes is mythologized,
not too dissimilar from the subjects of a lot of those statues.
It's not real.
It's just made up.
Or it's heavily sanitized, so we only hear about the good parts.
Those people on Twitter already know this.
But I'm working on a book.
And in it, it discusses a lot of people from American history.
And it goes through and it presents the public image of them.
And then we talk about the other stuff,
the stuff that doesn't get brought up.
None of this is new information.
It is information that has been known for an incredibly long time.
It's not new at all.
But I'm sure that if you were to read it,
you would say it was revisionist.
And it's not only attacking conservatives.
If you watched this channel for any length of time,
you know that when I talk about Teddy Roosevelt,
this is somebody that I believe has a lot of admirable traits.
Also a subject in the book, because also did a lot of messed up stuff.
Not going to overlook him just because he was a progressive.
Incidentally, another Republican who was a progressive.
A progressive from back then, because that's what the Republican Party was back then.
It's not an attack.
It's not a plot.
It's not a conspiracy.
It's just history.
Information comes out into public discussion at times when it's needed.
And right now, the United States needs to address a lot of those things that happened,
that we like to pretend didn't.
If we want to fulfill the promises laid out in those founding documents that no doubt you love,
we have to address the fact that when they were written,
we weren't even close to keeping them.
This country did not view all men as created equal.
This country didn't live up to its founding promises.
And in order to do that, we have to address the history.
It's not going to be pleasant.
If you're ever reading a book and you walk away from it thinking,
yay, everything's great, you're not reading history.
You're reading mythology.
You're reading propaganda.
History is messed up in this country and the world over.
A lot of bad things happened.
And so if you read a history book about American history and you don't come away thinking,
what were we doing, you probably weren't getting real history.
You were getting something designed to make you feel like you should chant USA, USA,
and never make it better, never fix the problems that have been here since this country was founded.
It's not new history.
It's not revisionist history.
It's just history.
And they don't want you to know it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}