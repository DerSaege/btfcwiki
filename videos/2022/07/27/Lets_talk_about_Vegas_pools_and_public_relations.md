---
title: Let's talk about Vegas, pools, and public relations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4g-AlrsiSJU) |
| Published | 2022/07/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Las Vegas is capping the size of backyard swimming pools due to inadequate water supply.
- The average pool size in Vegas is 470 square feet, while the cap is set at 600.
- This move targets those wanting giant pools, not for major water conservation.
- It serves as a public message about water scarcity in Vegas.
- The commission acknowledges this as a PR move, knowing its limited effectiveness.
- Despite saving a few million gallons of water, more significant decisions lie ahead.
- The world is warming, leading to more droughts and less water.
- Beau sees this as a warning shot rather than a solution.
- While a step in the right direction, tangible actions are needed for real results.
- Beau doesn't expect this cap to solve Vegas's future water issues.

### Quotes

- "This is a very public message that Vegas isn't going to have enough water."
- "I hope people understand that you can't continue to expand these cities."
- "The reality is we're way past warnings."
- "It's a step in the right direction for once."
- "I wouldn't expect this to even remotely impact the water issues that Vegas is going to have in the future."

### Oneliner

Las Vegas capping pool sizes sends a public message about water scarcity, but tangible actions are needed for real results as warnings alone won't suffice for future water issues.

### Audience

Residents, policymakers, environmentalists

### On-the-ground actions from transcript

- Address water scarcity through tangible actions (implied)
- Advocate for sustainable city planning to combat future water issues (implied)
- Raise awareness about the impacts of urban expansion on water resources (implied)

### Whats missing in summary

Beau's tone and nuanced perspective on the limitations of the pool size cap for addressing long-term water issues in Las Vegas. 

### Tags

#LasVegas #WaterScarcity #PoolSizeCap #EnvironmentalAction #UrbanPlanning


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Las Vegas and pools and water and public relations.
Okay so it is all over the news, lots of coverage on it.
Vegas has capped the size of a swimming pool that you can put in your backyard.
Now they're doing this because it's a city in the middle of the desert and the water
supply is less than adequate.
Does this cap actually do anything?
That's the real question.
Okay so this first number, it's a little bit fuzzy because it's hard to get a nationwide
average on this, but as near as I can tell, the size of the average pool in the United
States, the most common size, is 28 by 14, that's 392 square feet.
For a family of six to eight, the recommended size is 18 by 36, that's 648 square feet.
The average size of a pool in Vegas is 470 square feet.
What was the cap set at?
600.
So the average pool is 470, the cap is set at 600.
So this is specifically targeting people who want to put in giant pools in their backyard.
Is this going to be a major water saving thing?
No.
No.
It's a drop in a pool.
This is targeting just the most extreme scenarios.
Does that mean that it's worthless though?
I mean as far as actual water conservation, kind of.
But not entirely.
It's not entirely worthless from a PR standpoint.
This is a very public message that Vegas isn't going to have enough water.
And this is a message that will be sent to those people moving in who want to install
pools.
So the reality is, most people moving in, they wouldn't have put in a pool this size.
But when they hear about this, they might think twice about moving to the middle of
the desert.
The other thing that I do want to point out is that the commission there, they kind of
know that this is a PR move.
That it's not going to be super effective.
Sure, they're talking about the few million gallons of water that it's going to save.
At the same time, if the trends continue and the lake continues to decline, then this may
be one of the least of the tough decisions that we'll be making over the course of time.
That's the Clark County Commission Chairman.
They understand that this is PR.
I hope it's effective.
I hope people understand that you can't continue to expand these cities.
Because there are going to be tough decisions that have to be made.
The world is getting warmer.
The droughts will become the norm.
There's going to be less water in these areas.
This is a good move as a warning shot.
And I'm glad that we're finally moving in the direction of starting to address it.
But the reality is we're way past warnings.
They're going to have to do something a lot more tangible to get any real results.
So I wouldn't herald this as a major triumph, but it's good.
I mean, it's a step in the right direction for once.
I just wouldn't expect this to even remotely impact the water issues that Vegas is going
to have in the future.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}