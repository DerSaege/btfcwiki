---
title: Let's talk about VP Harris and that weird introduction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qd3UC3dOpZc) |
| Published | 2022/07/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Standing in a shop wearing a blue t-shirt with an iconic scene depicting Mr. Rogers and Officer Clemens with their feet in a kiddie pool.
- Mr. Rogers used subtle yet direct methods to encourage tolerance and kindness during a time when people had issues sharing pools.
- Vice President Harris introduced herself at a disability conference, practicing self-description for the benefit of those with limited sight.
- Mocking of the clip from the conference was primarily done by individuals who grew up watching Mr. Rogers, missing the point of the show.
- Mr. Rogers, known for encouraging people to look for helpers, had some odd behaviors like narrating feeding his fish for a blind viewer.
- The act of self-description, like Harris did, is a way to make events more accessible for those with disabilities.
- Beau questions the response of those mocking Harris's self-description, suggesting they may have missed the show's message of kindness and empathy.
- Encourages reflection on the deeper meaning behind seemingly simple actions like those of Mr. Rogers and Vice President Harris.
- Emphasizes the importance of understanding the intentions behind actions rather than just surface-level appearances.
- Beau implies that Mr. Rogers himself might have been disappointed in those who missed the point of his message of kindness and understanding.

### Quotes

- "If you did grow up on Mr. Rogers, and your first inclination upon seeing that clip was to mock it, I think you missed the point of the show."
- "It's weird."
- "I'd bet Mr. Rogers would be pretty disappointed in you."
- "Have a good day."

### Oneliner

Beau points out the missed message of kindness and empathy in mocking Vice President Harris's self-description, reminiscent of Mr. Rogers's subtle methods of promoting tolerance.

### Audience

Viewers

### On-the-ground actions from transcript

- Attend or support events and conferences designed to be accessible to individuals with disabilities (implied).

### Whats missing in summary

The full transcript provides a poignant reminder about the importance of empathy, understanding, and kindness in our actions towards others.


## Transcript
Well, howdy there, internet people.
It's Beau again.
My pronouns are he and him.
I'm standing in a shop wearing a blue t-shirt.
It depicts a scene with Mr. Rogers and Officer Clemens.
It's that iconic scene from the TV show
where they both have their feet in a kiddie pool.
It first went out at a time in the United States
when people had issues sharing pools.
It was a very subtle, yet very direct method
of encouraging tolerance, kind of a way to set the example
and hope that others pick up the behavior.
That's the idea behind it.
Vice President Harris introduced herself recently
in a similar fashion to the way I just did.
A clip of it wound up on social media,
and it went viral, people sharing it and mocking it.
And one of the things that I noticed
was that most of the people who were mocking it
were my age or older, people who would have grown up
on Mr. Rogers. Mr. Rogers, somebody
who encouraged you to always look for the helpers, right?
Thing is, he did a lot of weird stuff, too.
If you ever go back and watch the shows,
you will notice some odd behavior from Mr. Rogers.
One of the things he did that just seemed so odd
is when he fed his fish, he would tell you
he's feeding his fish while you're
watching him feed the fish.
It's weird.
It's weird.
Do you know why he did that all those years ago?
Because he got a message.
Dear Mr. Rogers, please say when you are feeding your fish,
because I worry about them.
I can't see if you are feeding them,
so please say you are feeding them out loud.
Signed, Katie, age five.
Father's note, Katie is blind, and she
does cry if you don't say that you fed your fish.
Harris was at a disability conference.
That's why she described what she was wearing.
It's a practice called self-description, I think.
I actually think it's that simple.
It's for the benefit of those people who have limited sight.
That's why that's done.
The conference itself being designed
to be as accessible as possible, this
might be Vice President Harris's way of, I don't know,
maybe setting the example, trying to be a helper,
sending a subtle yet direct message, that kind of thing.
If you did grow up on Mr. Rogers,
or you remember that show fondly,
and your first inclination upon seeing that clip
was to mock it, I think you missed the point of the show,
I'd bet Mr. Rogers would be pretty disappointed in you.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}