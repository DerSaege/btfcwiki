---
title: Let's talk about the emails about the Trump's electors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4zEPB2mFBoE) |
| Published | 2022/07/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the fake elector scheme and recently revealed information.
- Some emails have become public, revealing plans to send fake electoral votes to Pence.
- The use of quotation marks around "fake" and "someone" in the emails raises questions.
- Beau speculates that the quotation marks indicate a direct quote from someone speaking cryptically.
- The emails show White House involvement in the elector scheme, dispelling any doubts.
- They also challenge the defense of acting as a contingency plan by suggesting secrecy until January 6th.
- The revelation of these emails adds to the legal troubles for the former president.
- Confirmation of Trump being a target of a criminal investigation has been made official.
- Beau anticipates more developments and the importance of the quoted sentences in the emails.

### Quotes

- "So is this a smoking gun? No."
- "There's not going to be a single smoking gun. You're going to have a bunch of stuff that piles up."
- "This is bad news on top of more bad news for the former president."

### Oneliner

Exploring the fake elector scheme, revealed emails hint at a deeper plot, implicating White House involvement and challenging the idea of a contingency plan, adding legal troubles for the former president.

### Audience

Legal analysts, political activists.

### On-the-ground actions from transcript

- Analyze the implications of the revealed emails and stay informed about the legal developments (implied).
- Support transparency and accountability in political processes through advocacy and awareness-raising efforts (implied).

### Whats missing in summary

Full context and detailed analysis of the legal implications and potential consequences of the revealed emails.

### Tags

#FakeElectorScheme #WhiteHouseInvolvement #CriminalInvestigation #Transparency #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk a little bit more about the fake elector scheme
and the information that we have recently been able to see.
I no longer have a problem calling it the fake elector scheme
since they referred to them as fake electors in their emails.
If you don't know, some emails have become public
and they are something else.
There's a lot of commentary on it. We're going to go through some of it.
One of the key sentences that is in it,
we would just be sending in fake electoral votes to Pence
so that someone in Congress can make an objection
when they start counting votes and start arguing
that the fake votes should be counted.
People are pointing to this and suggesting that the person who wrote it,
that this is proof that they knew they were fake.
The thing is, fake both times in this is in quotation marks.
So is someone.
I haven't seen many people address this.
I have a wild theory on why quotation marks were used
and it's kind of out there so stick with me.
It's because they're a quote.
It's because they're quoting someone.
A direct quote of what they were told.
Now I don't know who would speak that vaguely, that cryptically,
to say, oh someone in Congress,
maybe one of the Republican congressmen, something like that.
I don't know who talks like that,
but I have a feeling if they were named in court,
they would probably declare it fake news.
I don't think that that sentence is actually showing
what a lot of people think it means.
I actually think, I think it's a quote.
And I think we'll find out who used those terms at some point in the future.
One thing that these emails definitely show
is that the White House was involved in the in the elector scheme.
A lot.
So that's no longer up for debate.
That was something that the committee demonstrated early on.
And I think they did a really good job demonstrating that that was the case.
These emails definitely do a better job.
And then the other thing that the emails cast out on
is the whole idea of a contingency.
The main defense for the people involved in this is that,
oh no, no, no, we just did this as a contingency.
You heard it when things started showing up in the press.
They said it's just to preserve Trump's legal rights,
you know, in case he got a court decision
or a legislature determined in his favor or something like that.
Right? Just a contingency.
Well, that's weird because the emails suggest that
at least some of the people involved wanted to keep it an absolute secret
until the 6th so they could surprise the Dems and the media.
It would be really hard to rely on a court case
or a legislature making a determination and keep them secret
because the court case and the legislature, that would be in public.
We will see these emails again, I assure you.
They will probably be marked exhibit at some point.
This is bad news on top of more bad news for the former president.
It was kind of made official yesterday that yes,
Trump is a target of a criminal investigation.
And no surprise, Garland was once again asked about it.
And he said that they would follow it wherever
and that anybody involved was going to be held accountable.
We'll have to wait and see.
But these emails, they are pretty...
I would be concerned about them if I was somebody that is named in them,
wrote one, received one, anything like that.
They would be concerning to me.
So is this a smoking gun?
No.
But when it comes to a case like this with this many players,
there's not going to be a single smoking gun.
You're going to have a bunch of stuff that piles up.
And that's really...
Yeah, this is going in the pile.
So I would expect to hear more about this.
And I would definitely expect to hear that sentence that I read quite a few times.
And the meaning of those quotations, that's going to be important.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}