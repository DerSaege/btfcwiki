---
title: Let's talk about Arizona, Trump, and DOJ....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=thpgMUuCmh8) |
| Published | 2022/07/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice subpoenas were issued to Arizona Senate President Karen Fann and State Senator Kelly Townsend.
- DOJ is seeking communications between Townsend and Trump's lawyers regarding the alleged fake elector scheme.
- The end goal of the subpoenas appears to be establishing a connection to Trump's inner circle.
- DOJ seems actively investigating a criminal case involving someone within Trump's circle.
- Efforts are focused on connecting the fake elector scheme to Trump's inner circle.
- There is skepticism about whether the DOJ will pursue Trump.
- Despite criticism of Merrick's handling, it seems in line with his usual approach of avoiding headlines until necessary.
- DOJ's actions indicate a tightening circle around Trump's inner circle, with Arizona playing a significant role.
- DOJ appears to be following up on interactions between Arizona officials and Trump's team regarding the election.
- The investigation appears to be moving towards a criminal case, although Merrick's approach keeps details scarce.

### Quotes

- "We will have to keep watching, but there is yet another little piece of evidence that fits into that pattern that says, yeah, they actually are doing something."
- "It's worth noting that if I'm not mistaken, Fan had a conversation with Rusty Bowers, who's the speaker out there, speaker of the house, who in that conversation kind of indicated that somebody from Trump's team had encouraged her to help him find a way to get the lead in the votes."
- "What we're seeing is that circle closing in slowly but surely around the Trump inner circle."

### Oneliner

Department of Justice's subpoenas in Arizona suggest an investigation linking the fake elector scheme to Trump's inner circle, indicating a potential criminal case.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Monitor developments and updates on the investigation in Arizona (implied).

### Whats missing in summary

Full context and nuances of the ongoing investigation and potential implications can be better understood by watching the entire transcript. 

### Tags

#DOJ #Investigation #Arizona #Trump #FakeElectorScheme


## Transcript
Well howdy there internet people. It's Beau again. So today we're going to talk about Arizona
and Trump and whether or not they're connected because that's apparently what the Department
of Justice wants to find out. Another set of subpoenas was issued this time to the
Senate President there in Arizona, Karen Fann and State Senator Kelly Townsend.
Now Townsend told Channel 12 News that what the DOJ wanted was her communications
with Trump's lawyers. Now these are people who are alleged to have been involved in the
fake elector scheme. We don't get a lot of press releases or leaks out of
out of Merrick. That's just not something that happens you know.
This is yet another example where the end result of what the subpoenas are trying to get to
is the Trump inner circle. At this point I think it's pretty safe to say that the Department of
Justice is actively investigating a criminal case and the suspect is somebody within the Trump circle.
And that they are moving trying to establish a connection between the fake elector scheme
and Trump's inner circle. And that is what a lot of the movement that we're seeing from DOJ
all around the country is really focused on. There is
a lot of doubt among people about whether or not the Department of Justice is really going to go after Trump.
All of the subpoenas, when we find out what the Department of Justice is actually trying to get access to,
what information they're trying to get, what they're asking people about, everything is moving that direction.
I know that the way Merrick is handling it is not the way most people want.
And I can't really disagree with that. But at the same time what we do know is this is
how he's always done stuff. He's always been, throughout his career,
very opposed to generating headlines until it's time. What we're seeing is that circle
closing in slowly but surely around the Trump inner circle. And a lot of it seems to do
with Arizona. It's worth noting that if I'm not mistaken,
Fan had a conversation with Rusty Bowers, who's the speaker out there,
speaker of the house, who in that conversation kind of indicated that somebody from Trump's team
had encouraged her to help him find a way to get the lead in the votes
in a state that Trump lost by 10,500 votes. And this is the same Fan that put together that
partisan review, audit, whatever, of Maricopa County. It appears that the Department of Justice
is following up on all of this and attempting to tie all of it back to the Trump inner circle.
And the way they're proceeding, it looks like a criminal case. We don't know because Merrick
doesn't tell anybody anything, but that's what it looks like. So we will have to keep watching,
but there is yet another little piece of evidence that fits into that pattern that says,
yeah, they actually are doing something. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}