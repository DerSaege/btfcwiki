---
title: Let's talk about Ohio, Indiana, a girl, and the Supreme Court....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wyZkhr7aQNw) |
| Published | 2022/07/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ohio and Indiana in focus.
- Three-day response after SC ruling.
- Ten-year-old girl sent from Ohio to Indiana for services.
- Republicans' future offering scrutinized.
- Concern over federal ban implications.
- GOP base energized against those in need.
- Potential nationwide ban if GOP takes House and Senate.
- Urgent situation of ten-year-olds in need.
- Republican Party's role in current state criticized.
- Ill consequences of prioritizing politics over compassion.
- Call to reconsider supporting such policies.
- Warning against being manipulated by politicians.
- GOP's priorities questioned.

### Quotes

- "We are headed down a very, very bad road."
- "They don't care about this."
- "Go kick down at that ten-year-old girl."
- "Hope it makes you feel better."
- "Y'all have a good day."

### Oneliner

Ohio, Indiana, SC ruling, ten-year-old girl sent away – GOP future and lack of compassion exposed, urgent call for reflection on priorities. 

### Audience

Voters, parents, activists

### On-the-ground actions from transcript

- Re-evaluate political priorities and policies (suggested)
- Advocate for compassionate and inclusive services for all children (implied)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's words can be best experienced by watching the full transcript. 

### Tags

#Politics #RepublicanParty #Compassion #Children #Future


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the future.
And the future also being the past.
And we're going to talk about Ohio.
And we're going to talk about Indiana.
And we're going to talk about the Supreme Court.
This reporting has surfaced and that reporting says three days.
Three days.
One two three days after the Supreme Court sent down its edict from up on high that a
ten-year-old girl had to leave Ohio and go to Indiana to get services.
Ten years old.
It only took three days for a case like that to show up.
Don't have to sit around and wait for something dramatic to get headlines.
Three days.
A ten-year-old girl.
When the Republican Party talks about this, understand this is the future they are offering
you.
This is the future they are offering your kids.
When they sit there and they talk about a federal ban, where would she go then?
What would happen then to that ten-year-old child?
When Mitch McConnell promises action, this is what he's talking about.
The Republican Party for years rallied their base.
They energized their base by saying those people, those people who need these kinds
of services, they are the enemy.
You can look down on them.
It's okay.
Those people, they don't need your compassion.
They're the bad people.
Look for me and I'll fix it.
Does it seem fixed?
If the Republican Party retakes the House and Senate, make no mistake about it, they
will view it as a mandate to create a nationwide ban.
That is how they will see it and rest assured they will do everything within their power
to make it happen.
And then you will have ten-year-old girls all across the country with nowhere to go.
It only took three days for a situation like this to arise.
There are more, undoubtedly, that didn't make the news.
This was just one that the story broke.
This is one that got reported.
It's going to happen over and over again.
What's going to happen to these kids?
And see, right now, the fact that I would imagine most people don't know if I'm talking
about the pregnancy or the child, that should be a sign.
We are headed down a very, very bad road.
And that road was built by the Republican Party, by their desire to motivate their base
by kicking down at others.
And now we have ten-year-olds on interstate trips because they can't get the services
they need, because it was good politics to make sure they didn't get any compassion,
make sure they didn't get any assistance.
Those are the evil people.
Ten-year-old.
When you hear Republicans talk about this, this is what you're going to get, they're
promising that you're going to get it, and they're going to try to deliver.
You really need to take a moment and think about whether or not this is a priority, whether
or not this is what you want for your kids, whether or not you are still going to let
them play you.
Because make no mistake about it, they are.
They don't care about this.
Most people in the Republican Party, they wouldn't trust one of these politicians to
balance...
They wouldn't trust one of these politicians to balance the budget, to balance their own
checkbook.
But you are entrusting your daughter's freedom to them because they tricked you, because
they played you, because they knew if they gave you somebody to kick down at, that you
would vote for them.
Well go kick down at that ten-year-old girl.
Hope it makes you feel better.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}