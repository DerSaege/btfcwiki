---
title: Let's talk about the committee reaching Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=beY6RoWz2No) |
| Published | 2022/07/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examining whether the committee is effective in changing minds regarding the events of a particular day.
- People are polarized in their opinions about guilt or innocence based on their interpretations of the actions of that day.
- The committee targets those whose opinions can be shifted to change the narrative.
- Andrew McCarthy, a former federal prosecutor known for his partisan views, has shown a shift in opinion regarding Trump's actions on that day.
- McCarthy previously defended Trump but now believes Trump could be prosecuted for his role in the events.
- The committee's efforts have influenced McCarthy's perspective, indicating a successful impact on changing minds.
- Despite initial skepticism, Beau believes that continuing to present information is vital for shifting opinions and potentially influencing future jury members.
- Beau underscores the importance of reaching those who were previously in echo chambers to facilitate opinion shifts.

### Quotes

- "The committee is changing this person's mind."
- "It is shifting opinion. It is changing thought."
- "Those shifting opinions matter if they're going to charge Trump."
- "The committee's doing its job."
- "Y'all have a good day."

### Oneliner

The committee's effective narrative shifts, as seen through Andrew McCarthy's changed views, underscore the importance of influencing opinions for potential legal repercussions, aiming to impact future jury members.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Contact your representatives to express your views on the committee's proceedings (suggested).
- Stay informed about ongoing developments related to the events discussed in the transcript (implied).

### Whats missing in summary

Insights on the potential ripple effects of shifting public opinion and influencing legal actions.

### Tags

#CommitteeEffectiveness #OpinionShift #Influence #NarrativeChange #LegalRepercussions


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the committee and whether or not it's changing minds.
That's really one of the big questions about it is whether or not it's being effective.
It's shifting the opinion by demonstrating that evidence.
Because there are many people who are going to be intractable when it comes to their position on this.
Either they saw the actions of that day and they presume him to be guilty,
and then there's those who saw the actions of that day and presume him to be not guilty.
It's those people whose opinions can be changed.
That's who the committee is really trying to talk to right now.
So we're going to talk about one person in particular, a person, Andrew McCarthy,
former prosecutor for the feds and not exactly a nonpartisan person.
Here are some of the things that he's written.
There's one titled How Obama Embraces Islam's Sharia Agenda.
There's one titled The Grand Jihad, How Islam and the Left Sabotage America.
Faithless Execution, Building the Political Case for Obama's Impeachment.
Ball of Collusion, which was, I mean, it was kind of a defense of Trump.
Okay, this is not a person who is in any way a rhino.
This is not a person who is in any way even sympathetic to the Democratic Party.
So what's he have to say?
Trump was clearly aware just moments before he took the podium
that you had a mob of heavily armed people.
The critical thing he says is they're not here to hurt me,
which implies that in his mind he knows they're here to hurt someone.
He goes on.
And the second thing he says, they can come in, they can hear me,
and then they can march to the Capitol.
So he's very aware that you have a mob that's armed to the teeth
that he is planning to encourage to march on the Capitol.
That knowledge opens up the possibility that you could prosecute him
for aiding and abetting the intimidation of federal officials,
which is a pretty serious crime.
He's then asked, do you expect that we're going to see Trump prosecuted
by the Justice Department on any of these charges?
McCarthy, I do now.
Yeah.
I do now.
The committee has changed this person's mind.
This is somebody who, if I'm not mistaken,
worked directly for Giuliani, was prosecutor, I want to say,
80, mid-80s, 85, 86, somewhere in there,
and worked directly for Giuliani, no love for the Democratic Party,
wrote a book that, for lack of a better word,
the whole goal was to defend Trump,
and here we are now with him saying he expects Trump to be charged.
The committee's doing its job.
They told America what they were going to tell him.
They're now telling him, and it's having an impact.
It is shifting opinion.
It is changing thought.
You may not like the way they're doing it.
You may think that it is past the point,
you know, that those who are intractable saying he's not guilty,
well, all of those people have shifted,
so there's no point in doing this any further.
I don't think that's true.
I think there's a whole lot of people who were in echo chambers at the time
who, as they hear this, their opinions will shift,
and those shifting opinions matter if they're going to charge Trump
because it's going to be those people who eventually end up in a jury box.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}