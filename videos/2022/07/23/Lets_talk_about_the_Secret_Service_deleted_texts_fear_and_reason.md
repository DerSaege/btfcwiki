---
title: Let's talk about the Secret Service, deleted texts, fear, and reason....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DMmihMHRFmA) |
| Published | 2022/07/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Inspector General's Office is investigating the Secret Service for potentially deleting text messages, treating it as a criminal investigation.
- Questions arose about the Secret Service's actions on January 6th, with concerns about their fear levels and communication practices.
- The Secret Service was criticized for sheltering in place during high-security events, but mobility is vital in such situations.
- Despite being scared and using less than ideal communication practices, the Secret Service handled the situation well on January 6th.
- Beau suggests that the fear exhibited by the Secret Service may have been based on perceived rather than actual threat capability.
- Beau calls for an inquiry into why the Secret Service was so scared on January 6th, pointing to the importance of looking at threat assessments.
- There is speculation about the content of deleted text messages and their potential connection to the level of fear within the Secret Service.
- Beau comments on the seriousness of deleting government records and the potential criminal implications if done intentionally or systematically.
- Despite some shortcomings in communication practices, the Secret Service's actions on January 6th were effective in ensuring Pence's safety.
- Beau underscores the importance of investigating the level of fear within the Secret Service on January 6th and understanding the basis for it.

### Quotes

- "If it was done intentionally, it would be a crime."
- "I don't believe they misjudged their opposition by that much without information that led them to do that."
- "They had every right to be scared."
- "The level of fear that they had, that's a question."
- "Yeah, they need to do whatever they can to get those text messages."

### Oneliner

The Inspector General investigates the Secret Service for potentially deleting text messages, raising questions about their actions and fear levels on January 6th.

### Audience

Oversight Committees

### On-the-ground actions from transcript

- Investigate the threat assessments and information the Secret Service received regarding the January 6th events (suggested)
- Take steps to retrieve the potentially deleted text messages for investigation (suggested)

### Whats missing in summary

Further details on the potential implications of the missing text messages and their impact on understanding the Secret Service's actions on January 6th.

### Tags

#SecretService #InspectorGeneral #January6th #Investigation #ThreatAssessments


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the Secret Service.
Two different topics that people are asking about, and
they just collided in a weird way, in my mind anyway.
The first is the obvious one.
The OIG, the Inspector General's Office, has
apparently told the Secret Service, hey,
Why don't you stop looking for those text messages
because we're going to find them ourselves,
or we're going to be looking into it ourselves?
And why don't you just treat this
like a criminal investigation because it is?
Yeah, like I said, if it turns out
they did that intentionally, it was going to be a big deal.
Rock the Secret Service from top to bottom.
That looks like what's happening.
So there's the deleted text messages,
and we'll come back to that.
Then there were questions that came in about the Secret Service there on the 6th, whether
they did it right, all of that stuff, their radio traffic.
Yeah, they did fine.
I guess somebody said on some network show that they never should have been in a situation
where they were sipping and sheltering in place.
They never should have been in a situation where they are sheltering in place.
And yes, that's true.
In that world, mobility is life.
It's really important.
That's why when you go to high-security events, there's always these really long lines.
Because entrances are closed off.
They're closed off to maintain exit routes.
Mobility is really important.
So yeah, I mean, that shouldn't happen, but this is also kind of a unique situation.
It's not ideal to be in a situation like that, true.
But they handled it.
They handled it well.
And they were very restrained.
That's something that I haven't really seen come up.
Understand, their job is to get him out of there.
If you're in the way of doing their job, they're going to do their job.
They were very restrained.
And at the same time, they were scared.
They were really worried.
And somebody asked about the way they talk to each other over their comms.
that normal? No, it's not. But no harm, no foul on that one. That's because they
were scared. The reason they were talking to each other in less than best
practices, that's why. That's not a big deal there either. But I want to know why
they were scared. I want to know why they were scared to the degree that they were.
I'm not downplaying the event, but a detail is more than capable of handling the actual
capability that they were facing.
The opposition, the rioters, coup attempters, whatever you want to call them, their actual
capability wasn't high.
So the fear was mainly based on perceived capability, which, I mean, sometimes it's
just off.
There's a reason there's two terms for it, because actual capability and perceived capability,
they don't always match.
At the same time, that's not a mistake the Secret Service normally makes.
So I'm curious.
I would be very curious.
I think the committee should be curious as to why they were so scared that they were
saying goodbye.
That should be a line of inquiry, finding out why they were as scared as they were.
And again, not, why were you such a chicken?
That's not what I'm saying.
They had every right to be scared.
But they normally don't get scared without reason.
There are a bunch of reasons that could lead to detail to believe that their opposition
has a higher capability than it does.
I think that should probably be a line of inquiry for the committee at this point.
I would want to look at their threat assessments.
information the Secret Service received from all the other agencies that talked
about the event that day and what they could expect to encounter. If a detail
was told that, hey, this group probably has a bunch of weapons, well, they might
be a little bit more scared. They might be a little bit more concerned about
about that, then they were just a mob, I would be interested to see what the
threat assessment said, and then kind of acknowledge that if Pence's detail got
them, all of the details got them. The former president's detail had them. That
information would have been known to the inner circle in the White House. Those
threat assessments could have been made a while before the event.
Again, perhaps illustrating that Trump knew the type of crowd he was riling up.
Now, that's one possibility. Another is, it's kind of out there a little bit, like so much
I'm not really going to go into it, but I really want to see those text messages now.
There might be a reason in those text messages that led Pence's detail to be as scared as they were.
Now, back to the original thing with the OIG talking to the Secret Service.
Okay, as I said in the second video on the Secret Service on this topic,
No, deleting those messages, that is not within the realm of protecting your clients' dignity.
We are now talking about deleting records that could be operationally linked.
This is destruction of government records.
And yeah, the OIG does appear to be launching a full-blown investigation into it.
This would be a crime.
would be a crime. If this was done intentionally, it would be a crime. If it
was done systematically, it would probably be a larger crime. If a whole
bunch of people got together and said, oh these need to disappear, these don't need
to make it through the migration or whatever, yeah that's that's gonna be a
bigger thing, and I think that's probably pretty likely. So as far as the
investigation, yeah, OIG, that was shots across the bow saying, hey, we're looking
into this. Secret Service actions that day, they did well as evidenced by the
fact that Pence is still here. That's their job. They won. It may not
have been pretty, but they won.
The radio traffic, not best practices, but also not hugely out of line.
The level of fear that they had, that's a question.
And that's a question that needs to be answered.
I don't believe that it was just, I don't believe they misjudged their opposition by
that much without information that would have led them to do that.
I would highly suggest the committee look at the threat assessments and find out what
those agents knew about that crowd.
And then, yeah, they need to do whatever they can to get those text messages.
Now there is also the possibility that some of them may have been deleted because they
were embarrassing to the agents themselves.
But I don't really see that as likely.
It's a possibility, but that doesn't explain the massive amount of missing messages.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}