---
title: Let's talk about the Trump outtakes and the Bannon audio....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=62LV-o8VxnM) |
| Published | 2022/07/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the ban on audio and outtakes amplifying negative opinions of the former president.
- Mentions the ban on audio from before the election, where Trump planned to declare victory even if he lost.
- Explains how lack of knowledge about early voting and mail-in voting was exploited to manipulate people.
- Describes how the red mirage concept was used to deceive rather than inform the public.
- Expresses frustration that even after certification, Trump continues to spread false claims about the election.
- Notes that by January 7th, Trump knew he had lost but continues to propagate falsehoods.
- Criticizes the manipulation of people through false claims and fundraising.
- Urges using evidence of the ban on audio and Trump's post-election actions to challenge conspiracy beliefs.
- Emphasizes the importance of getting individuals to acknowledge Trump's knowledge of his loss.
- Encourages questioning and discussing the ban on audio and Trump's persistent false claims with those who believe them.

### Quotes

- "He put them at risk over something he knew to be untrue."
- "Those two pieces of evidence."
- "They look for patterns."
- "Turns out the person they gave their trust to was the person that was manipulating them."
- "Y'all have a good day."

### Oneliner

Beau underscores the significance of the ban on audio and outtakes in revealing manipulation by the former president, urging individuals to challenge conspiracy beliefs by discussing these pieces of evidence.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Challenge conspiracy beliefs by discussing the ban on audio and Trump's false claims with believers (suggested).
- Encourage acknowledgment of Trump's knowledge of his loss by sharing evidence from the transcript (implied).

### Whats missing in summary

The full transcript provides a comprehensive breakdown of how the former president manipulated people with false claims post-election, using the ban on audio and outtakes as evidence to challenge conspiracy beliefs effectively.

### Tags

#FormerPresident #Manipulation #ElectionClaims #ConspiracyBeliefs #Challenge


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about those outtakes
and the ban on audio.
Because watching that, if you are somebody
who didn't fall for the president,
the combination of the ban on audio and the outtakes,
you can't have a lower opinion of the former president.
Those two things together, they just
amplify everything else he did during his administration.
But they're also a tool.
They're going to be super useful.
If you still have friends, family, who still
believe that man's nonsense, those two pieces of evidence,
the ban on audio and those outtakes.
The ban on audio is from before the election.
He's saying, oh, yeah, even if Trump's losing,
he's going to say he won.
Talks about the early voting, the mail-in voting,
and how that gets counted later.
The red mirage, we talked about it on the livestream
during the election.
But rather than educating people about it,
they just used it to trick them, to play them, to dupe them.
Because they knew they wouldn't understand it.
And that lack of knowledge was a window,
was an opening for them to get in before the election.
And then that's what happened.
And then after, after everything that
happened on the 6th, after the lawsuits,
after the election had been certified,
I don't want to say that.
I don't want to say the election's over.
Yeah, I bet you don't.
What it shows is that no matter what, by January 7th, he knew.
He knew 100%, indisputable, his words, his face, he knew.
But here we are, almost two years later,
the man is still calling up to Wisconsin,
still peddling these claims, because they
found that opening.
They planted that seed.
They got inside, and they are manipulating people.
Those two pieces should be pretty useful tools
in demonstrating that.
These are people, the people who believe these claims,
these are people who believe in conspiracies.
You're showing them one.
You're showing, look, here it is, right here.
Once they can acknowledge that on the 7th, he knew,
knew he lost, and that he's still doing it today,
still peddling those claims, still raising that money.
Once you can get them to acknowledge that part,
then you move back to the ban on audio.
And you show, hey, from the very beginning.
It's going to be painful for a lot of people,
because Cheney's right.
Every time I say that, it just hurts.
These are people who, in their mind,
they were defending the country, because they believed him.
He put them at risk over something he knew to be untrue.
Those two pieces of evidence.
When you encounter these people from now on,
friend, family, co-worker, somebody you don't know,
that's what you're asking them.
Explain the ban on audio, where they said
they were going to do it before the election,
and then Trump saying he didn't want to call it off,
didn't want to stop pretending even after the 6th.
Ask him to explain that.
They look for patterns.
They look and believe that people played them.
They're just looking the wrong way.
Turns out the person they gave their trust to
was the person that was manipulating them.
That's normally how it works.
Generally speaking, the people that you view with suspicion
can't actually manipulate you.
But that person that you shared the memes of,
of Jesus standing behind him,
the person you elevated to the stature of a saint,
yeah, he can play you pretty easily
because you believe him.
Those two clips.
I think that'll reach a lot of people.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}