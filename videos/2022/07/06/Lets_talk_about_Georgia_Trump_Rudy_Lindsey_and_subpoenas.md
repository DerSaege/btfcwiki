---
title: Let's talk about Georgia, Trump, Rudy, Lindsey, and subpoenas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YXitTL_yuys) |
| Published | 2022/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the state of Georgia and the special grand jury's subpoenas related to possible criminal interference in the 2020 elections.
- Subpoenas were sent out to expected names like Rudy Giuliani, Lindsey Graham, and lesser-known individuals, indicating a broad investigation.
- Subpoenas cover a range of activities from trying to find votes to lying to lawmakers.
- Individuals subpoenaed are mostly lawyers or lawmakers who may try to avoid testifying using attorney-client privilege or speech and debate clause.
- Despite expectations, some key figures were not subpoenaed, raising questions about oversight or potential agreements.
- Speculation on reasons for certain exclusions, such as individuals jumping ship from the Trump administration or agreements to provide information.
- Humorous reference to individuals who predicted a 1776-like event on January 6th, contrasting it with the possibility of revelations leading to a different outcome.

### Quotes

- "Looks like it's going to be 1773, because I think a whole bunch of people are about to spill the tea."
- "The grand jury is looking into everything."
- "While everybody who is named is somebody you'd expect, there are people that you'd expect that weren't named."
- "Maybe there are people who were on the SS Trump, who realized they were polishing brass on the Titanic, and they decided to jump ship."
- "It's just a thought."

### Oneliner

Beau dives into Georgia's grand jury subpoenas, revealing expected and missing names and hinting at potential revelations beyond what's anticipated.

### Audience

Legal Observers, Political Analysts

### On-the-ground actions from transcript

- Speculate responsibly on the potential implications of missing names in the grand jury subpoenas (implied).
- Stay informed about the developments in the investigation and be ready to analyze and interpret new information (implied).

### Whats missing in summary

Insights on the nuances of the investigation and potential reasons behind the exclusion of certain individuals can be better understood by watching the full transcript. 

### Tags

#Georgia #GrandJury #Subpoenas #ElectionInterference #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today, we are going to talk about the state of Georgia
and some letters, some correspondence
that they sent out.
We'll talk about who got letters, who didn't get letters,
and what that can tell us.
I'm going to dash everybody's hopes just a little bit
and then provide you with a different type of hope.
Okay, so I would imagine most of you know what I'm talking about by now,
but if you don't, the special grand jury there in Georgia
that is looking into possible criminal interference in the 2020 elections,
they sent out some subpoenas, quite a few of them.
And for the most part, they're the names you would expect.
Rudy Giuliani, Eastman, Jenna Ellis, Lindsey Graham,
the people that you would expect.
There's also a host of lesser-known people
who the names probably won't mean much
unless you've been following it very closely.
However, the scope of these subpoenas can tell us something,
and that's that the grand jury is looking into everything.
Everything.
From, I don't know, perhaps calling and trying to find votes,
to the fake electors thing, to simple stuff.
You know, stuff that seems quaint almost, like lying to lawmakers.
It appears that they're looking into all of it.
Now, to dash everybody's hopes for a second,
one thing I want to point out is that pretty much everybody,
it may actually be everybody, is either a lawyer or a lawmaker,
meaning they're going to fight these subpoenas.
They will either argue that they have attorney-client privilege,
or they will argue that the speech and debate clause protects them.
They will try to get out of testifying, of talking to the grand jury.
I don't think they'll win, but if you're looking for a speedy resolution to this,
I would manage your expectations a little bit.
But there is something else here.
When you look at the scope of what they're looking into,
and the names of people that they subpoenaed,
while everybody who is named is somebody you would expect,
there are people that you would expect that weren't named.
There are people who might have been involved in some of this,
or have material knowledge about it, that didn't get subpoenas.
And these are people who maybe there was even testimony in DC
related to them having knowledge about it.
And you have to wonder why they didn't get subpoenaed, right?
I mean, I would. I do.
It seems as though that could just be an oversight on the part of Georgia,
or maybe a situation has arisen that negates the need for a subpoena.
Maybe there are people who were on the SS Trump,
who realized they were polishing brass on the Titanic,
and they decided to jump ship.
Maybe they entered into an agreement that negates the need for a subpoena,
because by that agreement they're going to say everything they know.
It's a hunch. It's a possibility.
Because there are definitely people who I feel would be included in this batch that weren't.
And sure, I mean, it could just be an oversight, an accident,
or it could mean something else.
But one thing that I find very humorous, myself anyway,
is that in the lead up to the 6th, a whole lot of these people,
they were saying stuff like, it's going to be 1776.
Doesn't look like it.
Looks like it's going to be 1773,
because I think a whole bunch of people are about to spill the tea.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}