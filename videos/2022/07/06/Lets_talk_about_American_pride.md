---
title: Let's talk about American pride....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t08l68z6kSk) |
| Published | 2022/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent poll data shows record low levels of American pride.
- Americans are asked how proud they are to be American, with options ranging from not at all to extremely.
- Only 38% of Americans said they were extremely proud, a record low.
- Republicans are at 58%, while Independents are at 34%.
- Demographics show that males (43%), those over 55 (51%), and those without college degrees (41%) are prouder.
- The demographic least proud are those aged 18-34, with only 25% extremely proud.
- Young people are not proud of the U.S. due to concerns like war, loss of rights, authoritarianism, lack of action on climate change, and economic struggles.
- The older population is more easily swayed against their interests, with 51% extremely proud of the country.
- Younger Americans want a better future and refuse blind nationalism without progress.
- The divide in pride levels is more about age than party affiliation.
- The youth feels left out and unrecognized by a country that doesn't prioritize their needs or rights.

### Quotes

- "Why should they be? What have they been handed?"
- "They want a better future and they're not going to be proud until they get it."
- "You can't force people to take pride in a country that doesn't care about them."

### Oneliner

Recent poll data reveals record low American pride, with young people feeling left out and unrepresented in a country that doesn't prioritize their future.

### Audience

American citizens

### On-the-ground actions from transcript

- Reach out to young people in your community to understand their concerns and amplify their voices (implied).
- Support initiatives that address the issues young Americans care about, such as climate change and economic opportunities (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the generational divide in American pride and the reasons behind it. Viewing the complete video allows for a deeper understanding of the impact of historical events and policies on different age groups' perceptions of national pride.

### Tags

#AmericanPride #GenerationalDivide #YouthConcerns #Nationalism #PollData


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today,
we're going to talk about being proud to be an American.
Because the poll came out.
Now for twenty years or so,
people have been asked, are you proud to be
an American?
And the options they're given
are not at all,
only a little,
moderately, very, or extremely.
We're at record lows
as far as pride.
Now as I go through these numbers for those who are overseas,
just understand these are low numbers for the U.S.
You know, generally speaking, the United States is a very nationalistic country.
So,
the percentage of Americans
that said they were extremely proud,
thirty-eight percent.
And if I'm not mistaken, that is a record low
the whole time they've been asking this.
Republicans
still are at, I want to say, fifty-eight percent.
But that's very low
for them.
Independents are at like thirty-four percent.
Who is proud?
Top demographics.
If you're male,
forty-three,
forty-three percent.
If you are over the age
of fifty-five,
fifty-one percent.
If you didn't go to college,
forty-one percent.
Who isn't proud?
What demographic stands out
as
people who just
aren't proud of the U.S.?
People aged eighteen to thirty-four.
Doesn't have to do with party affiliation, anything like that.
It's those under the age of thirty-four.
That's twenty-five
are extremely proud.
Interestingly enough, those who said not at all or only a little,
those two, that's also twenty-five percent.
People will talk,
undoubtedly, when this poll like starts going out everywhere.
People are going to talk about the partisan differences.
I think
it might be
more important to look at the ages
because that isn't a partisan thing, that's age.
And
people are going to want to know why aren't young people proud?
That's the wrong question.
Why should they be?
What have they been handed?
They've been handed what?
Twenty years of war,
a Supreme Court that is going to constantly take away more and more of
their rights,
a coup attempt,
a slide into authoritarianism where they lose more and more of their freedoms,
no action on
climate change or anything that they actually care about,
and an economy that does not allow them to prosper
because generally speaking
those
who were older
were
more easily conned into voting against their own interests.
Fifty-five and older,
fifty-one percent
are extremely proud.
Under the age of thirty-four,
twenty-five percent.
That's a pretty big difference.
The
younger portion
of the United States,
they're not proud because
they're not buying the lines.
They're not going to sit there and chant USA, USA
when they know that the United States has fallen behind
in a whole lot of ways.
They want a better future
and they're not going to be proud
until they get it.
You can't force something like that.
You can't force people to take pride in a country that doesn't care about them.
Their concerns,
their interests,
their issues,
not only
are they not addressed,
people are actively working against them.
That's why.
And while I'm sure
because
playing into the left-right divide
is
more profitable
for views,
I'm sure that people are going to focus on that.
The difference between
how Democrats
or Independents or Republicans,
that's what people will focus on.
But the biggest
difference,
the one
that matters because it isn't partisan,
it's age.
Those under the age of 34,
they feel left out
because they were.
Anyway,
it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}