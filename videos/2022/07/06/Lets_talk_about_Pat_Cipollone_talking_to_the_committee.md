---
title: Let's talk about Pat Cipollone talking to the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FePp1hA7ZYo) |
| Published | 2022/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pat Cipollone agreed to talk to the committee after being subpoenaed.
- The testimony will be behind closed doors, possibly transcribed and recorded.
- Cipollone's role is not like the Secret Service; he is meant to protect the institution of the presidency.
- White House counsel's job is to ensure the president acts within the bounds of the Constitution and the rule of law.
- The White House counsel should prevent the president from engaging in illegal or unconstitutional actions.
- Establishing attorney-client privilege between Cipollone and Trump is among the counsel's duties.
- Cipollone is expected to provide information about illegal and unconstitutional actions taken by Trump.
- While Cipollone may not volunteer everything, he is not likely to hide significant information.
- Cipollone is believed to have first-hand knowledge of events such as seizing voting machines and the fake elector scheme.
- Cipollone should prioritize revealing the truth about Trump's actions that undermined the Constitution and the rule of law.

### Quotes

- "His client isn't any individual president. The client here is the Constitution, the rule of law."
- "Cipollone should be on the front lines of saying this is what happened, this is why he was wrong, this is where he broke the law."

### Oneliner

Pat Cipollone's role in protecting the presidency and upholding the rule of law, shedding light on Trump's actions.

### Audience

Legal experts, political analysts, concerned citizens

### On-the-ground actions from transcript

- Contact legal organizations for updates on Cipollone's testimony (suggested)
- Stay informed about developments related to the committee's proceedings (implied)

### Whats missing in summary

Insights into the potential impact of Cipollone's testimony on ongoing investigations and accountability efforts.

### Tags

#PatCipollone #WhiteHouseCounsel #Testimony #RuleOfLaw #Constitution


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about Pat
Cipollone and we are going to clear up some questions about that. If you don't know,
he agreed to talk to the committee. He was given a subpoena and he was like, sure, I'll talk to him.
One of the things that I've seen that I think needs to be cleared up right away is that this
will not be testimony like Hutchinson. This will be behind closed doors and depending on the
reporting, it will either be transcribed or it will be transcribed and recorded. I believe it
will be transcribed and recorded. Whether or not we ever see that recording, that's an entirely
different question. But this won't be a hearing setting when he is talking, at least this time.
I guess somewhere on the news, somebody said that his job is to protect the institution of the
presidency and that led to people sending me messages, is this a thing like the Secret Service
where he really shouldn't talk? No, exact opposite. Exact opposite. There are different ways to
protect an institution. Cipollone doesn't have anything to do with safety and security. That's
not his job. He was the White House counsel under Trump. Now with that name, you would think this is
Trump's lawyer. It's not the case. The president is not the White House counsel's client. The
Constitution is. The Constitution, the rule of law, this is what the White House counsel is really
there to protect. It protects the institution of the presidency in the sense that a president says,
hey, I'm going to do this thing. White House counsel is supposed to walk in and be like,
no, that's unconstitutional. You need to stop. Their job is to protect the president
and stop the president from embarrassing the presidency by doing things that are illegal or
unconstitutional. So that's the kind of thing that the White House counsel is supposed to do.
Now, there's a lot of other things that the White House counsel is supposed to do.
One of them is to make sure that there's an attorney-client privilege established
between Cipollone and Trump. Trump may not have known that though,
but that's going to play a lot into this. There are some other minor privilege claims that
are not as far as ethics and morals. To me, Cipollone is the person who should walk in and be
like, hey, he did this that was illegal, this that was unconstitutional, tried to do this,
was going to do this, asked about this, because that's the job. If there is somebody who should
spill the beans, it's Cipollone. Now, how far he's going to be willing to go in this regard
and how much he's going to volunteer, we don't know yet. But I have a feeling based on the other
testimony that he's not going to hide a whole lot. He's not going to try to downplay. I definitely
think that there will be things he doesn't volunteer because he's a lawyer and that's what
they do. But when it comes to being asked a question, do you know about this? Yeah, this is
what happened. I don't think he's going to try to paint Trump in an untrue light. I think he's
going to come out and say he wanted to do this, we told them this was illegal. The testimony suggests
that Cipollone knows about everything and was present for conversations about seizing voting
machines, about sending stuff with the whole fake elector scheme, going to the Capitol the day of
the event. All of this Cipollone is said to have first-hand knowledge of. I think he'll answer the
questions and I don't see it as a situation where he shouldn't. His client isn't any individual
president. The client here is the Constitution, the rule of law, you know, all the things that Trump
certainly appears to have tried to undermine. So Cipollone should be on the front lines of saying
this is what happened, this is why he was wrong, this is where he broke the law, this is all the
evidence I have. But we'll have to see and it will be a while really before we get the full scope
of what Cipollone talks about. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}