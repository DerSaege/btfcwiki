---
title: Let's talk about Day 7 of the hearings, and maybe day 8....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Sl6DqsLkbGo) |
| Published | 2022/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculation about hearings on day seven and possibly day eight, including a potential surprise hearing.
- Topics to be discussed include White House connections to extremists, a meeting between Trump and "Team Crazy," and the possibility of seizing voting machines.
- Focus on statements by Cipollone that may corroborate previous testimony and be damaging to the Trump inner circle.
- Mention of testimony from someone linked to groups present on January 6th.
- Allies of the former president willing to testify only if it's live, raising concerns about shaping the format of the hearings.
- Not going well for Trump and his circle; their strategy may involve delegitimizing and disrupting the hearings.
- Warning against allowing witnesses to dictate the outcome and format of the hearings.
- Suspicions about witnesses facing serious charges and the potential impact of their testimony.
- Emphasizing the need for tying together individual pieces with dramatic testimony moving forward.

### Quotes

- "This is not going well for Trump."
- "The only real card they have to play is to try to delegitimize and disrupt the hearings."
- "It doesn't seem like putting them on TV live is a good idea."
- "There's only so much they can say and still mount a defense."
- "It's time to start tying them together with testimony that may even be more dramatic than what we've heard thus far."

### Oneliner

Beau speculates on upcoming hearings, discussing potential topics like White House connections to extremists and warning against witnesses shaping the format.

### Audience

Political observers

### On-the-ground actions from transcript

- Wait for updates and follow the developments in the hearings (implied)

### Whats missing in summary

Insights on the potential impact of tying together dramatic testimony for a conclusive outcome.

### Tags

#Hearings #Trump #Testimony #Extremists #Allegations


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about day seven
of the hearings and maybe also day eight of the hearings.
We may be talking about both right now.
We're not sure.
We know that there's going to be a hearing tomorrow.
There are rumors, suggestions of a surprise hearing
occurring on Thursday in prime time.
My guess is that's going to happen,
but we don't know that for sure yet.
So the information that is coming out
when people are talking about
what they're going to talk about, it seems pretty broad.
This may actually be covering both days
because this is a lot of ground to cover
in one day of hearings, but we'll have to wait and see.
Maybe they plan on really running through
some of this quickly.
Okay, so one of the first things that is being reported
that will be discussed is the White House's connections,
the Trump inner circle connections,
to use their term, extremists.
Now, what does that mean?
We don't actually know yet because there have been
statements that have come out of people in the know
that have talked about both a group of lawyers
who held, let's just say,
less than conventional legal theories,
as well as statements related to the more militant groups
that were there on the 6th.
So it could be about both.
But again, that seems like a lot of information
to cover in one day.
There's also going to be a focus on a meeting
that took place between Trump and, again,
using their term, Team Crazy,
a group of advisors, primarily lawyers, I guess,
who had a meeting with Trump,
and apparently that meeting was very formative,
and it may have served as a catalyst for later events.
There will probably be a focus on a tweet
that was made shortly after this meeting.
I would expect that throughout all of this,
we will get sprinkles of Cipollone statements.
Now, people familiar with what he said
have said that he didn't contradict
any of the other witnesses.
That is really bad news for the Trump inner circle.
A lot of the testimony that has just been jaw-dropping
has been about stuff that he was the person
who really had the closest view of.
If his statements match everything else that's been said,
that's really, really bad news for the Trump circle.
Incredibly bad news.
There will apparently also be discussions
about the Trump effort,
or perhaps them just raising the possibility
of seizing the voting machines in an effort to, again,
manufacture that plausibility
that there was something wrong with the election,
because what the committee has shown so far
is that early on, he knew that he didn't win,
or he at least should have known,
and that those around him were telling him that.
And then afterward, there is reported
to have been a lot of activity
aimed at altering that outcome.
So that's what we can expect for either day seven
or day seven and day eight.
I would imagine that there is also some surprises.
Like, we also know that there's going to be somebody
who is very closely linked to one of the groups
that was there on the 6th
that is going to be providing testimony.
What that's going to be, we don't really know yet.
Now, in related news,
there are suddenly people who are allies
of the former president who,
well, we're willing to talk now.
We'll testify, but only if it's live.
Yeah, I mean, that doesn't seem like a good idea at all.
At this point, the committee has followed the template,
and they've followed it well.
Tell them what you're going to tell them.
Tell them. Tell them what you told them.
Tie everything together.
They're doing a really good job.
I think deviating from that format is a bad move.
I also think it's a bad move to set the precedent
that witnesses can...
They get to shape the outcome of the hearings.
They get to shape the format.
They get to make demands.
That seems counterproductive.
It's also worth noting that at this point in time,
this is not going well for Trump.
This is not going well for that circle of people.
The only real card they have to play at this point
is to try to delegitimize and disrupt the hearings.
That's the only move they have.
I don't know that that's the plan of the people
who are suddenly willing to talk, but it might be.
And it doesn't seem like putting them on TV live is a good idea.
It's also worth noting that some of them
are accused of pretty serious crimes,
and their testimony is probably going to reflect that.
There's only so much they can say and still mount a defense.
It would be a very bad move for anybody facing charges
that serious to talk candidly about what happened.
So it seems very sus to me.
But I mean, they may have the purest of motives.
I don't know.
It doesn't seem like a deviation that I would make
if I was running the hearings.
So that's where we're at.
And we're going to have to wait and see how this plays out.
Soon, what you would normally expect at this point,
they have laid out a lot of the individual pieces.
It's time to start tying them together with testimony
that may even be more dramatic than what we've heard thus far.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}