---
title: Let's talk about leaving your state....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FxaDVhnNABY) |
| Published | 2022/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the topic of moving due to recent legislation or rulings, or considering leaving the U.S. to return to one's home country.
- Encouraging individuals to make their own decisions about moving based on their unique circumstances.
- Advising that safety concerns should be a priority when deciding whether to stay or leave.
- Noting that leaving a state does not necessarily mean giving up, especially if it's related to resistance.
- Pointing out the importance of considering business implications when deciding whether to relocate.
- Mentioning the potential economic downturn in states with controversial legislation.
- Urging individuals to prioritize the opinions of those directly impacted by the situation.
- Acknowledging that the decision to move is significant and life-altering.
- Emphasizing that individuals should do what is best for themselves and their families, disregarding external opinions.
- Suggesting that the impact of these decisions may not be fully understood by the states involved in the long term.

### Quotes

- "If you're worried about your safety, yeah, that's an easy one. If you have the ability, yeah, get out."
- "Don't listen to anybody on social media, any commentator, anything like that. You do what's right for you and your family, nothing else."
- "I don't think it's wrong to leave. I don't think it's wrong to stay."
- "It's a huge, it's a life-altering decision."
- "These states probably do not understand what they have done to themselves."

### Oneliner

Beau advises making informed decisions about moving based on safety, resistance, business, and family impacts, prioritizing individual circumstances over external opinions.

### Audience

Individuals considering moving

### On-the-ground actions from transcript

- Make a safety plan for potential relocation (suggested)
- Consult with family and household members directly impacted by the situation (implied)

### Whats missing in summary

The emotional weight and personal reflection Beau brings to the complex decision-making process around moving in response to political circumstances.

### Tags

#Moving #Legislation #DecisionMaking #Safety #BusinessImpact


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about moving,
because a whole lot of y'all are thinking about it.
Gotten a whole bunch of messages from people
thinking about leaving a state
because of recent legislation,
or leaving a state because of a recent ruling,
or leaving the U.S.
and going back to the country that they came from
because they feel like they're taking advantage
of the situation here.
I don't have answers to this.
These decisions are decisions you're going to have to make.
You're going to have to sit down and evaluate it honestly,
and there's things you need to consider.
You know, if you're worried about your safety,
yeah, that's an easy one.
If you have the ability, yeah, get out.
When it comes to something like that,
where there's doubt, there is no doubt, right?
When it comes to whether or not you should stay
and be part of the resistance in that state,
I've seen people talk about it and say,
you know, well, leaving the state is just giving up.
I've noticed that it's mainly people
in my demographic saying that.
People who aren't, you know, actually at risk.
I would weigh their opinion a little bit less
than what you see and what you feel.
I don't think it's wrong to leave.
I don't think it's wrong to stay.
If you're in the fight and you feel like you have to stay,
good, and you may be in the fight so much
that you're worried about harassment.
So you feel like you have to move just over the state line.
Everybody's situation on this is going to be different.
There's no one-size-fits-all advice.
If you're a business owner, yeah, that one,
it makes sense to me.
If you're a business owner,
especially if you do business-to-business sales
or something like that, yeah,
you might want to consider relocating
because the states that enacted this kind of legislation,
they're going to have an economic downturn.
They're going to have issues.
Think about all of the people who are considering packing up
and leaving the state that they live in.
Now imagine the people coming the other way.
It's a whole lot easier to decide,
oh, no, no, Texas is now out.
We are definitely not moving there,
than it is to actually pick up and move from Texas.
Businesses understand this.
They're not going to want to move to places
they're going to have trouble recruiting.
They're not going to want to move to places
that have a strained social safety net,
strained schools,
because it's going to make it harder to recruit.
These states have caused a future economic downturn
for themselves.
That's something else to bear in mind.
How long it's going to take to show itself
depends on how aggressively these new laws are enforced.
But it will happen.
So this is a decision that you as an individual
or you with your family have to make.
Don't listen to anybody on social media,
any commentator, anything like that.
You do what's right for you and your family, nothing else.
I will say that I don't think that you're overreacting.
I mean, Florida actually didn't have any of those laws, right?
But my wife and I talked about it, where we might end up.
So I don't think you're overreacting,
but at the same time, I can't give you advice.
That's something you're going to have to think about.
It's a huge, it's a life-altering decision.
I understand why it's coming up.
And even just from the economic standpoint, it makes sense.
These states probably do not understand
what they have done to themselves.
And the situation they're going to find themselves
in five or 10 years.
So I would put emphasis on the opinions of those people
in your family, in your circle, in your household
who are directly impacted.
Those are the opinions that matter.
Nobody else's.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}