---
title: Let's talk about the Secret Service deleting texts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g3s_7mKjC0U) |
| Published | 2022/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Secret Service and the Office of the Inspector General from DHS are at odds over the deletion of texts related to the events of January 6th.
- The Office of the Inspector General is meticulous and methodical, so their claims should be taken seriously.
- The Secret Service denies the allegations, attributing the missing texts to a migration process.
- Beau expresses skepticism towards the Secret Service's explanation and leans towards believing the Office of the Inspector General.
- If true, the intentional destruction of records by the Secret Service could have significant implications, including criminal conspiracy and cover-up.
- Beau distinguishes between ethical considerations in protecting a client's dignity and the serious nature of destroying government records.
- The issue goes beyond individual agents; it suggests a systemic problem within the agency.
- The situation is still unfolding, with accusations and denials but no concrete evidence yet.
- Beau underscores that this incident, if proven, will be a significant departure from acceptable behavior within the agency.
- The potential implications of these allegations could be a game changer.

### Quotes

- "You deleted what?"
- "The intentional destruction of records to impede, that's huge."
- "We're going to have to wait to see how this plays out."
- "This is definitely a very different ball game when it comes to acceptable behavior."
- "If these allegations are true, this is going to be a game changer."

### Oneliner

The Secret Service and the Office of the Inspector General clash over deleted texts, raising concerns of intentional record destruction and criminal conspiracy.

### Audience

Watchdog agencies, concerned citizens.

### On-the-ground actions from transcript

- Monitor developments and hold accountable those responsible (implied).
- Support transparency and accountability within government agencies (implied).

### Whats missing in summary

Full context and depth of analysis can be gained by watching the entire video. 

### Tags

#SecretService #DHS #OfficeoftheInspectorGeneral #GovernmentAccountability #Transparency


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the Secret Service
and the Office of the Inspector General, their DHS,
because they have differing views of something that happened.
A letter was uncovered that says the Office of the Inspector General
is telling the committee that, well, Secret Service deleted
some texts pertaining to the 6th.
The Secret Service is denying this.
They put out a statement saying, hey, you know,
this was part of a migration.
Some stuff was lost, but we were able to give them
what they asked for, so on and so forth.
Had some questions that came in about that.
People that work for the Office of the Inspector General,
generally speaking, they're records people.
You don't end up in that office because you're afraid of paperwork.
They're very methodical.
They are very thorough, generally speaking.
I would need a whole lot more from the Secret Service
than a press release to start to lean their direction
and believe their version of events.
But this is really early.
That's just based on the idea that sometimes past performance
can predict future results.
So we're going to have to wait to see how this plays out
before we really pronounce anything as fact.
But starting off, I would tend to believe the OIG.
Another set of questions that came in was basically asking
if this was similar to not remembering things,
to Secret Service agents not wanting to say anything
that would embarrass the presidency.
No, no, this is all new level.
There's a huge difference between this and, I don't know,
forgetting to record your client's mistress on the sign-in sheet
or calling up housekeeping and having them wait outside
while you run the vacuum because you don't want them to see
the powder that's all over the floor.
This is not the same.
If this occurred, it's a huge deal.
It's going to rock the Secret Service top to bottom.
This is, if this was the intentional destruction of records to impede,
that's huge.
That is huge.
It is not the same.
One is something that could be viewed as ethical.
It's part of the institutional culture.
You're not just there to protect the client's safety,
but also their dignity.
This is the destruction of government records
and possibly participating in a criminal conspiracy and cover-up.
They're not exactly the same.
It would also indicate that we're not talking about agents
as individuals who are saying,
yeah, no, I'm not talking.
We're talking about the agency as a whole.
If you go back and watch that video,
even when I'm kind of taken up for Secret Service agents
who are like, yeah, I don't remember that,
I said that the agency as a whole could issue a statement,
could provide facts.
For the agency as a whole to go rogue,
yeah, that's a big deal.
It's not the same.
Again, reporting is real light on this at the moment.
You have an accusation and you have a denial.
We don't have any evidence yet as of time of filming,
so we're going to have to wait and see how this plays out.
But this is definitely a very different ball game
when it comes to acceptable behavior.
Even people who would be like,
yeah, of course he didn't see that,
are going to be like, you deleted what?
This is going to be a game changer
if these allegations are true.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}