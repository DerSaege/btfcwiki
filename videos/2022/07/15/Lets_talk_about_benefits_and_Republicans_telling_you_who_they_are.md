---
title: Let's talk about benefits and Republicans telling you who they are....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P8HRlEPK7SQ) |
| Published | 2022/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the claimed benefits and perceived benefits of a situation involving a 10-year-old child.
- Jim Bopp, lead lawyer for the National Right to Life Group, expressed his opinion on legislation impacting children.
- Bopp's legislation lacked exceptions, meaning a 10-year-old could be forced to have a child.
- Bopp suggested that forcing a 10-year-old to have a child could have benefits, like saving money on childcare.
- Beau criticizes the idea of forcing a child to have a baby and questions the motives behind such legislation.
- The statement by Bopp reveals a worldview where child pregnancy is seen as a benefit, not a regrettable event.
- Beau calls out the Democratic Party for not effectively communicating the implications of overturning Roe v. Wade.
- Urging for more awareness about the potential consequences of legislation that normalizes child pregnancy.
- Beau stresses the importance of holding politicians accountable for their support of such legislation.
- Emphasizing the need for widespread attention to Bopp's statement and questioning politicians about their stance on such beliefs.

### Quotes

- "10-year-old moms, that's what they're saying. It's not regrettable. It's a benefit."
- "Roe is on the ballot, in fact. But they're not presenting that."
- "If they believe it is an ultimate benefit to have 10-year-old moms, that needs to be a question in every debate."

### Oneliner

Addressing the alarming belief that child pregnancy can be a benefit, revealing the need for political accountability and awareness of the potential consequences.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Question politicians about their stance on legislation impacting children (suggested)
- Raise awareness about the implications of normalizing child pregnancy (implied)

### Whats missing in summary

The emotional impact and urgency of addressing harmful beliefs regarding child pregnancy.

### Tags

#ChildPregnancy #Legislation #RoevWade #Accountability #Awareness


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to become very pragmatic for a minute.
We're going to talk about claimed benefits, perceived
benefits of something.
And we're going to talk about a very powerful person's opinion
on a situation that is going to impact thousands upon thousands
upon thousands of children, and specifically
talking about that situation involving the 10-year-old
child.
The statement comes from Jim Bopp,
who is like the lead lawyer for the National Right to Life
Group.
This is a person, when I read this,
understand this isn't just some random person
in a parking lot at a KFC.
This is somebody who drafts model legislation for states
to enact.
This is somebody who has a whole lot of influence over policy
decisions in this country.
This isn't just some random person.
When talking about the situation involving that child,
he seemed a little upset because in his model legislation,
the one that was presented to states to enact after Roe
was overturned, there weren't exceptions.
And under his law, yeah, that 10-year-old, well,
she would have been forced to be a part of that.
To have that child.
Now to me, that alone is just wild and out there.
But that's actually not the wild part of the statement.
It says here that we would hope that she would understand
the reason and ultimately the benefit of having the child.
I did my best, and I set my personal beliefs
about bodily autonomy and all of that stuff
and government overreach and set it all aside for a second
and just tried to come up with the pragmatic, practical benefits
of a 10-year-old being forced by the government to have a child.
I came up with, I mean, you could say that it's two,
but to me it's really just one.
Mom would be able to save a little bit of cash
on child care and transportation costs.
So you could divide that into two if you wanted,
but save a little bit of money on child care
and transportation costs when they were both riding the bus
to school at the same time.
Because understand, at 10 years old, that would happen.
Unless, of course, due to the massive strains
of raising a child, mom decided to drop out.
Then it wouldn't be a benefit at all.
How would it?
The key part to take away from this
is that, once again, they're telling you who they are.
They're telling you the world they
want to create in language that is very clear.
There is no ambiguity here.
They're creating a situation where child pregnancy,
it's not even this regrettable thing that happens.
It's a benefit.
It's a good thing.
And that on a long enough timeline,
well, she'll recognize that.
She'll be forced to recognize that.
This is the world they're promising you.
10-year-old moms, that's what they're saying.
It's not regrettable.
It's a benefit, ultimately a benefit.
The Democratic Party is doing a very, very bad job
of explaining what is coming down the road.
They are not putting Roe on the ballot,
other than saying how they're going to do it.
Other than saying, hey, Roe's on the ballot.
People need to know exactly what that means.
Understand, when they're at the point
where they're talking about a 10-year-old child
and learning their lesson, or whatever it is,
through this method, and it being an ultimate benefit,
they're telling you.
They plan on controlling your children to that degree.
They plan on normalizing this type of stuff to that degree
so they can stick with their talking point,
so they can continue to raise money.
The Democratic Party needs to get on their messaging.
Roe is on the ballot, in fact.
It really is.
But they're not presenting that.
They're not putting that out there the way they need to.
This quote needs to be front page everywhere.
Everybody who's running on a platform that aligns with this,
they need to be asked about this quote in particular
and see where they stand.
If they're willing to actually tell the voters
what the person who drafted the legislation
that they voted for, they introduced,
they support, where their motivations lie.
If they're willing to support those motivations,
those beliefs, if they believe it is an ultimate benefit
to have 10-year-old moms, that needs to be a question
in every debate, in every discussion,
in every interview by a reporter,
because that's newsworthy.
That is newsworthy.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}