---
title: Let's talk about Trump going to Washington....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iK-t9m38ZZo) |
| Published | 2022/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is planning to go to Washington for a speech, billed as a major policy speech.
- Trump's move is seen as the beginning of his campaign, just before the midterms.
- His speech aims to turn the midterms into a referendum on his baseless claims, which is a problem for the Republican Party.
- Trump's return to D.C. may be driven by his concern for polls and approval ratings.
- A New York Times poll shows that 49% of Republicans back Trump in a primary run.
- Trump's motivation to start campaigning might be influenced by potential legal challenges.
- There's a belief that running for president could offer Trump some form of protection.
- The possibility of Trump or his top people getting indicted poses a threat to the Republican Party.
- The impact of a major party candidate for president being indicted could hurt everyone across the board.
- This situation presents a challenge for the Republican Party, similar to dealing with Frankenstein's monster.

### Quotes

- "He's a private citizen. He doesn't direct policy."
- "Trump is a Republican problem now. Frankenstein's monster."
- "This might be time for the Democratic Party to start playing hardball."

### Oneliner

Trump's return to D.C. for a supposed policy speech sparks concerns for the Republican Party as he sets the stage for his campaign and potentially navigates legal challenges, creating a political dilemma akin to "Frankenstein's monster."

### Audience

Political analysts, Democratic strategists

### On-the-ground actions from transcript

- Start playing hardball (implied)

### Whats missing in summary

Insight into the potential implications of Trump's actions on the political landscape and strategies moving forward

### Tags

#Trump #RepublicanParty #2024Election #PoliticalStrategy #DemocraticParty


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about Trump going to Washington,
going back to DC.
He's planning on going up there to give a speech.
It is being billed as a major policy speech.
Policy?
What, are they changing the dress code at Mar-a-Lago?
He's a private citizen.
He doesn't direct policy.
He might be talking about his vision
for what he would take in 2024, but it is not a policy speech.
He is not the president.
So it's going to be, what, July 26, somewhere around there.
It's going to be a multi-day event,
and he'll be speaking on the second day in the afternoon.
And now, Trump is a Republican problem.
He's a problem for the Republican Party,
because he's making this move now.
This is the beginning of his campaign,
but there may be an ulterior motive for that.
But he's making this move now, just before the midterms,
which means his speech and what he's going to do,
he's going to try to turn the midterms into a referendum
on his baseless claims.
And that's a problem for the Republican Party.
Whether they win or lose, it's a problem.
They had all this time to shake this dude off their neck,
and they didn't do it.
They're going to end up paying for it.
So why is Trump doing it now, this return to D.C.?
Let's see.
I don't know.
We know that he really cares about polls and approval.
And the latest New York Times Santa College poll
says that 49% of Republicans would back him in a primary run.
He doesn't even have a majority of Republicans.
So he's got to come out and start making waves,
start getting attention back on him.
He's probably also very aware that when the numbers come out
for people being asked about whether he should be charged,
that they're pretty high.
I think most Americans feel that Trump should be more concerned
about looking at a jury box than looking to put
his name in a ballot box.
But that probably still doesn't totally
explain why he's doing it.
There may be the belief that if he's running for president,
if he started campaigning and he started
doing more official events, things that look like campaign
events, even if he doesn't announce,
even if he doesn't make it official, if he gets indicted
or his top people get indicted, he
can say that it's political.
I think that might have something to do with it.
He may even believe that there's some form of protection
that is offered by him running.
This is also a problem for the Republican Party
because whether or not DOJ is going to indict,
nobody knows, right?
But can you imagine the impact of a major candidate
from a major party for president being indicted?
That would probably hurt everybody
across the board a few points.
I don't think that would be limited.
Trump is a Republican Party problem now.
Frankenstein's monster.
I have no idea what they're going to do.
But this might be time for the Democratic Party
to start playing hardball.
Just saying.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}