---
title: Let's talk about the people who believed Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cIA_QpCoems) |
| Published | 2022/07/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about how someone fell for Trump's lies and the realization that came later.
- Explains the dynamics of falling into an information silo and how it affects beliefs.
- Mentions the smugness from liberals when discussing being conned by Trump supporters.
- Advises on staying out of information silos and providing fact-checking assistance.
- Urges individuals to help others escape information silos and be wary of getting pulled back in.
- Acknowledges the warning signs about Trump's actions before the election.
- Emphasizes the importance of helping others who may still be trapped in misinformation.

### Quotes

- "You fell into an information silo."
- "If you are prone to following rabbit holes, you may not be great at getting out of them."
- "Use unbiased places. Use AP. Fact-check for them."
- "You can shout down into that silo and help other people get out."
- "You bear some of the blame for that. But those people who profited from it, they bear probably more of the blame."

### Oneliner

Someone fell into an information silo, woke up to Trump's lies, and now seeks help to guide others out of the echo chamber.

### Audience

Trump supporters and those seeking to understand how individuals fall into information silos.

### On-the-ground actions from transcript

- Fact-check for others (suggested).
- Provide unbiased information (implied).
- Help others escape information silos (exemplified).

### What's missing in summary

The emotional impact and personal growth journey of realizing being misled and seeking redemption. 

### Tags

#TrumpSupporters #InformationSilo #FactChecking #HelpingOthers #Misinformation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the people who believed Trump.
And how that happened.
Why it happened anyway.
Because I got a message and it's long but I'm going to read the whole thing.
Because I think it might be pretty enlightening for some people.
And if you have a family member who still believes Trump, this might really help.
I started watching you because of your videos on Ukraine.
Then I found your videos on guns.
You're honestly the only liberal I can stand to listen to for any length of time.
I have a question for you.
I admit I fell for Trump.
I know today he was lying, especially after the Steve Bannon tape was leaked.
I knew before then, but that really sold it for me.
But back then, I didn't know, I could see a world where I ended up there on the 6th.
It wouldn't have taken that much.
I really believed him.
My question is, why is there so much smugness when liberals say we were conned or duped
or tricked?
You say it, but at least you seem to understand that you could have been tricked if things
were different.
I get it, we were tricked.
And I feel bad about it.
But acting like everybody knew back then that he was lying is just dumb.
Is there something I'm missing?
Am I really just a not smart person?
Not his term?
Sorry this is so long, but at least now I get to see if the messages you read are real.
You're not dumb.
You fell into an information silo.
I'm the only liberal that you could stand to listen to.
I imagine it was worse back then.
So all the people around you, all the people that you got information from, they were all
saying the same thing.
It's not just Trump.
A whole lot of them were lying and knew they were lying.
They used it to build an audience, to build a brand, to make money.
And that information silo, once you're in it, it's all self-reinforcing.
One personality says something, then the next feeds off of it, makes it a little bit more
interesting or wild, appeals to your emotions.
And just like a grain silo, you get sucked down in it.
That's what happened.
And I know that's what happened because you're asking, how did everybody know?
Down below I will put some links to past articles.
These are major outlets.
Before the election happened, the content of that banning tape, that was public knowledge.
To people outside of that information silo, it was public knowledge.
The fact that he was going to declare victory and then claim all of this stuff, it was public
knowledge.
The story broke before the election.
It just didn't make it in to the information silo that you were at.
So you didn't know.
You're out now, obviously, at least somewhat.
Stay out.
If you can't stand to listen to liberals, read articles.
Don't fall back in.
It's easy to fall back in to an information silo if you look at the overlap between people
who got sucked down in to various conspiracy theory silos and the Trump supporters.
There's a huge overlap.
If you are prone to following rabbit holes, you may not be great at getting out of them.
You said you feel bad.
You want to do something about it, help other people get out.
Fact check.
Use unbiased places.
Use AP.
Places like that.
Fact check for them.
Provide that information.
Because unless you intentionally withdrew yourself, you're still in that silo.
There are still people on your social media who are there.
You probably have a megaphone.
You can shout down in to that silo and help other people get out.
Because believe me, he's not done.
He's going to try again.
And undoubtedly there are people in that silo where you were at with their hand outstretched
trying to find their way out right now.
You can reach out for them.
Just be aware that you can also get sucked back in to it.
Yeah, I mean people are pretty smug about it.
There was a lot of warning that it was going to happen.
If you were watching this channel back then, years before the election, we talked on this
channel about how he was going to try to mess with the election.
Because it was the only one of the 14 characteristics he hadn't filled yet.
So it's not that you're dumb.
All of the information around you said one thing.
You got out.
And as far as I can tell from the message, you got out before any serious damage was
caused to your life.
Because normally when I get a message like this, it's the person's already lost their
friends and family.
Yeah, I would try to help other people.
The reason people are smug is because there was a lot of information saying he was going
to do it.
The problem is those stories, those articles, they didn't get in to the information ecosystem
that you were in.
Because of that, you didn't know about it.
And yeah, you bear some of the blame for that.
But those people who profited from it, those people who existed in that ecosystem, feeding
off of each other, they bear probably more of the blame.
Because whether they realized it or not, they were in a position of trust.
And they failed.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}