---
title: Let's talk about how the Democratic party can play hard ball....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K13NLVX5uIQ) |
| Published | 2022/07/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the idea of the Democratic Party playing hardball after receiving a message post a video on Trump's speech in DC.
- Suggests disrupting Trump's event by announcing a surprise hearing focused on questioning the credibility of the president the next morning after his speech.
- Points out that calling Trump's credibility into question could get under his skin and impact his big speech negatively.
- Mentions that Trump loves to go off script and predicts that if a committee disrupts his event, he may deviate from his planned speech.
- Urges the Democratic Party to use tactics that criticize Trump before his speech to throw him off balance and make him focus on criticism, rather than his vision for 2024.
- Addresses potential concerns about politicizing hearings and argues that critics of the process will never be satisfied regardless of how it is conducted.
- Emphasizes the importance of examining Trump's credibility and mindset in relation to those around him at specific times for the committee's work.
- Encourages the Democratic Party to take action and fight strategically to win politically.

### Quotes
- "Trump loves to go off script."
- "Engage in some shenanigans of your own for a change."
- "The credibility of Trump, the mindset of Trump in relation to the people around him at that time, that's materially important to the committee's work."

### Oneliner
Beau suggests disrupting Trump's event by questioning his credibility in a surprise hearing to unsettle him before his speech, urging the Democratic Party to strategically fight to win.

### Audience
Politically active individuals

### On-the-ground actions from transcript
- Disrupt events strategically to unsettle opponents before key moments (exemplified)
- Focus on questioning credibility strategically to impact opponent's focus (exemplified)
- Engage in tactics to throw opponents off balance before significant speeches (exemplified)

### Whats missing in summary
Tips on strategic political maneuvering and disrupting opponents effectively.

### Tags
#PoliticalStrategy #DemocraticParty #Trump #Credibility #StrategicFighting


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about the Democratic Party playing hardball.
Because after that video about Trump's speech in DC, I got a message.
It's basically, hey, you said the Democratic Party should play hardball.
The Democratic Party doesn't know how to play hardball.
Why don't you give them some tips?
And then it kind of went on from there.
And honestly, like if somebody told me, hey, that came from a congressional staffer looking
for ideas, I'd believe it.
Okay.
Well, I mean, to be honest, I tried to hint to it in that video.
Trump is speaking on the second day in the afternoon of that conference up there.
He's speaking on the second day in the afternoon.
So on the first day, after the speeches start getting rolling and the schedule is firm,
right, announce a surprise hearing for the next day in the morning.
That would probably disrupt his little event, probably get inside his head, especially if
the focus of that hearing was the credibility of the president, the credibility of Trump.
And a whole lot of the testimony in the clips that were played were all of his closest allies
saying that he was a little off, that, well, he lied, that he made stuff up, that he should
have known better.
Anything that would call his credibility into question, anything that would get under his
skin, get in his head.
Trump loves to go off script.
This is something he's billing as a major policy speech.
The committee does this, I give it two minutes before he's off script and that whole speech
turns into an old man yelling at clouds letter to the editor.
I mean, engage in some shenanigans of your own for a change.
He is not somebody who handles criticism well.
And if just before he goes on for his big triumphant return speech, he gets just hammered
with criticism all over the news, that's what he'll be talking about.
The idea of laying out his vision for 2024, well, that just goes out the window.
And I know somebody's going to say, well, we don't want to politicize the hearings.
The people who are saying that it's politicized, they're not watching.
They don't care.
It doesn't matter if everything was run the way a courtroom was, they would still say
it's political.
They would still say it's a witch hunt.
The people that you're trying to appease will never be appeased.
The credibility of Trump, the mindset of Trump in relation to the people around him at that
time, that's materially important to the committee's work.
Just do it at a time that is politically advantageous.
And that's just a start.
That's a whole bunch of stuff the Democratic Party could do if it just fought to win.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}