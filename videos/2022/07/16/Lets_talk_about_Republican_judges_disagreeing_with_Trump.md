---
title: Let's talk about Republican judges disagreeing with Trump...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lvFfxqkheq0) |
| Published | 2022/07/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is trying to make the midterms about himself by pushing baseless claims of fraud.
- Conservative judges and lawyers released a report titled Lost, Not Stolen, debunking Trump's fraud claims from the 2020 election.
- The report found no evidence of fraud on a scale that could change any election results.
- They urge fellow conservatives to move on, advance candidates focused on peace and prosperity, and reject Trump's false claims.
- The report aims to salvage the Republican Party by exposing Trump's lies and baseless allegations.
- Without the myth of fraud, Trump's chances are slim, posing a problem for him but offering a solution for the Republican Party.
- Republicans may need to reject Trump to deal with the problem he poses to the party.
- Apologies from politicians who once supported Trump's claims could be accepted by the Republican base.
- Many Republicans fell for Trump's claims, so admitting to being wrong may not be a career-ending move.
- It's time for the Republican Party to step up and lead by rejecting Trump's falsehoods.

### Quotes

- "Lost, Not Stolen."
- "Trump's lying to what they're saying."
- "It's the time for the Republican Party to do something that, well, it just hates to do, lead."

### Oneliner

Trump's baseless fraud claims debunked in a report titled Lost, Not Stolen, challenging Republicans to reject falsehoods and lead.

### Audience

Conservatives, Republicans

### On-the-ground actions from transcript

- Share the report Lost, Not Stolen with fellow conservatives to challenge false claims (exemplified)
- Encourage politicians to issue apologies for supporting baseless claims (exemplified)
- Support candidates focused on peace and prosperity rather than division (implied)

### Whats missing in summary

Full understanding of the implications and urgency for the Republican Party to confront Trump's lies and lead effectively.

### Tags

#Trump #RepublicanParty #ElectionFraud #Conservatives #Leadership


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about a little bit of trouble
Trump's running into all of a sudden.
Trump is attempting to make the midterms a referendum
about him.
Some very prominent conservative judges and lawyers,
well, they put out a report about, well,
some of his wilder assertions, some of his baseless claims, right?
And they don't hold back.
The title of the report is Lost, Not Stolen.
They examined, I want to say, 62 cases from six key battlegrounds in the 2020 election,
at Trump's claims about fraud. And what they found, absolutely no evidence of
fraud on the magnitude necessary to shift the result in any state, let alone
the nation as a whole. In fact, there was no fraud that changed the outcome in
even a single precinct. Now that's breaking it down into crayons right
there. That, I mean, you can't get more concise and direct than that. They go on.
Questions of election legality must be resolved as passionately in courts of
law, not through rallies and demonstrations, and most emphatically, not
by applying political pressure and threats to induce Congress to ignore its
constitutional duty and the electoral outcome for which the people voted, in which the legal
processes of the affected states had examined and confirmed.
They go on and they urge their fellow conservatives, fellow Republicans, to drop it, to move on,
to start trying to advance candidates that have a plan for peace and prosperity rather
than division.
When talking about it, one of the people who was involved said that one of the motivations
for releasing it now and for continuing to pursue this after all of this time was some
of the testimony from the sixth, where one of the people involved was like, if I had
had known there was no evidence, I wouldn't have been there.
They're trying to make it as clear as possible.
It is very, it's a readable report and they're trying to make it very clear.
Trump's lying to what they're saying.
There's no evidence to back up his claims that everything that's happened was based
on an election.
lost. It wasn't stolen from him. And they're trying to do it in a way that
can salvage the Republican Party. If this report gets any traction, if there are
conservatives in the Republican Party who still, I don't know, care about the
Constitution, any of that stuff, and this gets pushed out to the front, presents a
major problem for Trump, because without the myth of his claims, without those
baseless claims still in circulation, he doesn't have a chance.
At the same time as it being a problem for Trump, it's also a solution for the
Republican Party. Trump will bend to the Republican Party to his will, attempt to.
He will be a thorn in their side until he is rejected by Republicans themselves.
Until that happens, the Republican Party is going to have to deal with him. This if
If pushed out there, if it becomes a talking point, if you have some major politicians
go out there and issue apologies, I believe them after reading this report.
I can't believe I fell for it.
I don't think the Republican base is going to want to punish those candidates.
If they come out and admit they were wrong, I don't think they'll suffer much at the
polls among Republicans for that.
I think their careers will survive because many Republicans themselves rank and file.
They fell for those claims too.
It's the time for the Republican Party to do something that, well, it just hates to
do, lead.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}