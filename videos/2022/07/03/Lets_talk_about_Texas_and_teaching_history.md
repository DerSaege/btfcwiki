---
title: Let's talk about Texas and teaching history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OxDuReePwP8) |
| Published | 2022/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas proposed replacing "slave trade" with "involuntary relocation" in history classes.
- Department of Education in Texas rejected the proposal, acknowledging the problematic nature of the term.
- The goal behind introducing such terminology is to soften the brutal reality of slavery and historical atrocities.
- There's a push to teach American history as mythology and propaganda rather than critically.
- People want to whitewash history to make themselves and the country look better.
- Teaching history inaccurately can lead to misinformation and a lack of understanding.
- Softening the truth in history education can lead to future generations repeating mistakes.
- American history is full of heroes who spoke out against atrocities, but their voices are often silenced.
- Education is key to ensuring that future generations make informed decisions and understand the past.
- It's vital to teach history accurately, including the horrors and atrocities that occurred.
- Softening the truth of history by using new terms is not a solution to the real issues that happened.
- The youth need to be educated, not indoctrinated or fed myths.
- Heroes in history were flawed individuals who did both good and bad.
- Understanding the truth of history is critical for raising informed and empathetic individuals.

### Quotes

- "You can't soften what happened. It was horrible. It was hideous."
- "They want American mythology. They want propaganda taught, and they want it packaged as if it was history."
- "There are no heroes. Not really. They were people. They were flawed."

### Oneliner

Texas proposed softening the truth of history by replacing "slave trade" with "involuntary relocation," aiming to teach American mythology instead of critical history.

### Audience
Educators, historians, students

### On-the-ground actions from transcript

- Ensure accurate and critical history education in schools (implied)
- Encourage the teaching of American history in its full context, including the good and the bad (implied)
- Advocate for educating the youth on the true, unfiltered history of their country (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the dangers of distorting history and the importance of educating the youth on the true and unvarnished version of American history.

### Tags

#History #Education #Truth #Mythology #AmericanHistory


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're gonna talk about Texas,
a term, and teaching history because there was a proposal
to introduce some new terminology to history classes there.
And the term they wanted to introduce
was involuntary relocation.
There are a lot of points in American history where that is an appropriate term
because that's what happened, right? The problem is in this case they wanted to
use the term involuntary relocation as a substitute for the slave trade.
Yeah, I don't think that fits. That doesn't seem appropriate.
Now, luckily, the Department of Education there in Texas, they agree.
And they kind of shot this down, sent it back and was like, yeah, you need to work on this
term.
And good that they shot this down.
But let's talk about how we got to this point.
Why is there a push to introduce this terminology or similar terminology?
What's the goal?
soften it. That's the goal. The goal is to make the hideous blot of slavery seem
a little bit more palatable. Why? Because a whole bunch of people got together and
decided, oh we don't want history taught critically. We don't want students to
know the good, the bad, and the ugly, and the hideous of American history. We just
want them to feel good about themselves. We just want them to feel good about the
country. They don't want American history taught, they want American mythology. They
want propaganda taught, and they want it packaged as if it was history. And if you
do this, if this is allowed to stand, and this continues, eventually, well, it takes
on whatever meaning the new term is going to be.
And that's what people will think.
What happens to a student who is told that slavery, the slave trade, was involuntary
relocation?
happens to them when they start studying the 1900s and they find out about the
Tennessee Valley Authority? Because that was involuntary relocation. That's what
happened. They were forced to relocate, right? But I'm pretty sure they weren't
beat, whipped, and had their children sold. Why do you want to downplay this? And the
answer is they want American mythology. They want a package that they can sell.
They want to relate to the bad guy. That's what it's about. They've decided
that their heroes couldn't have been wrong. They couldn't have been people who
did horrible things, hideous things in their own words. They were heroes so
they're infallible. Rather than just acknowledging they were villains. They
They were the bad guy.
American history is full of people who spoke out and stood up against that hideous blot.
But they're barely mentioned because a large portion of the American population wants to
identify with certain characters in American mythology. I don't want to say
people in American history because that's not what they teach. They want to
identify with this mythical version of people that actually existed. They want
to identify with this heroic figure and forget about all of the other stuff that
they did, even when they themselves said it was hideous. If this happens, if we
allow those who are terrified of the truth, those who want their children
ignorant to succeed, and they can soften the language with which we teach
history. Make no mistake about it, our grandchildren will make the same
mistakes because they won't know any better. Because they won't understand
there's a difference between being told you have to move because this valley is
going to flood and slavery. This is what happens when you fall into bumper
sticker politics, some slogan, when you allow a term CRT to be demonized and you
don't know what it is, this is the outcome. Of course the children are going to be
less than educated. The parents refuse to educate themselves on things they're
voting on.
This is what will cause America to be less competitive.
Because they're not going to have any frame of reference for anything.
History is important.
You can't soften what happened.
It was horrible.
It was hideous.
You can't just slap a new term on it and hope that people overlook it.
We know what happened.
If you want to avoid similar mistakes in the future, it has to be taught.
People have to understand it in all of its horror.
The United States is at a crossroad and the most important thing right now is for the
youth to be educated because educated people make better decisions.
They're not played by some two-bit con man.
understand the way the world works because they know what happened in the
past. This is probably one of the most important fights right now. It's to make
sure that the youth are educated, that they're not indoctrinated, that they're
They're not fed some myth that they're not lied to.
But a whole bunch of people in the U.S. want to lie to their kids so they can feel good
about some mythical version of a person that actually existed.
There are no heroes.
Not really.
They were people.
They were flawed.
They did messed up stuff, and the only way to make your kids better is to make sure that
they understand what happened then.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}