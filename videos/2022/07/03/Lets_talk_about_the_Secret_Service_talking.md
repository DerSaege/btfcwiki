---
title: Let's talk about the Secret Service talking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tEH77n9128c) |
| Published | 2022/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the Secret Service, presidency, secrecy, and trust.
- Waits to make a video until the narrative shifts in his favor regarding the former president.
- Expresses distrust in the denial from the Secret Service regarding an incident in the limo.
- Emphasizes the importance of the Secret Service maintaining secrecy and not talking to the press.
- Gives scenarios illustrating the dangers of the Secret Service revealing confidential information about the president.
- Stresses that focusing on salacious details rather than the core issue is a distraction.
- Urges everyone to concentrate on the fact that the former president allegedly wanted to lead the rioters at the Capitol.

### Quotes

- "We cannot normalize the Secret Service talking."
- "We can't normalize this."
- "The bombshell is that he wanted to go."
- "Everybody needs to focus on the important part."
- "Y'all have a good day."

### Oneliner

Beau addresses the dangers of breaching secrecy within the Secret Service and urges focus on the core issue of the former president's alleged desire to lead rioters at the Capitol.

### Audience

Concerned Citizens

### On-the-ground actions from transcript

- Respect confidentiality and privacy in all aspects of your life (implied).
- Uphold trustworthiness by keeping sensitive information confidential (implied).
- Encourage others to focus on critical issues rather than sensational details (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the risks associated with breaching confidentiality within the Secret Service and the importance of maintaining trust and secrecy in high-security positions.

### Tags

#SecretService #Presidency #Secrecy #Trust #CapitolRiot #Confidentiality


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about the Secret Service,
the presidency, secrecy, trust,
and what's happening right now.
I waited to make this video until the narrative,
the storyline, kind of swung back in my direction.
I think most people know how I feel about the former president.
So I didn't want to make this video
while the Secret Service was saying,
oh, no, it didn't happen.
If you don't know, at time of filming,
there is sourcing from within the Secret Service
saying the incident in the limo, in the SUV,
occurred, that the Secret Service had been gossiping
about it ever since it happened, that everybody knew it happened,
and that they've all been talking about it.
Another way of saying this is that there are sources
within the Secret Service
that are talking about something embarrassing
to the presidency with the press.
I will be less than trusting of any statements
that come from the Secret Service
because they should be denying.
Anything that's embarrassing, they should be denying it.
And if they're not denying it, I have to wonder why.
So I can't trust the denial because, I mean,
really, they should be lying from what I believe happened.
And if they're admitting it, that's also wrong
because the job is to protect the safety
and dignity of the client.
Even in the private world,
nowhere near the level of the Secret Service,
when somebody is asked about a client,
you no comment.
You say nothing.
Let's say the sweetest people that I have ever worked for,
that I ever worked for, was a country music band.
And they were just the nicest, friendliest people,
so happy, just great people to work for.
And a reporter comes up and asks me,
hey, what was it like working for them?
The answer is no comment.
Even if I had absolutely nothing bad to say about them, ever,
the answer would be no comment
because the next question out of the reporter's mouth
could be, hey, well, what was it like
working for this business person?
So if I had said, oh, that country music band,
they're the sweetest people ever.
They're great.
What was it like working for this business person?
No comment.
What does that tell the reporter that there's a story there?
We cannot normalize the Secret Service talking.
That's bad all the way around.
Allow me to give you some scenarios.
Let's say, let's start with something
a little less dramatic.
Let's say there's a president who is rumored
to wear adult diapers.
This isn't a big deal.
This doesn't really matter.
This isn't a huge thing.
No big deal.
However, if the Secret Service confirmed that
and that information wound up in that international poker game
where everybody's cheating, wow, that
could be embarrassing to the presidency.
They can't talk.
Let's make it a little bit more severe, something
a little bit more serious.
Let's say, I don't know, it's the 1980s,
and the president is engaged in a contest with a near peer that
requires total clarity of thought at all times
because there's a lot of tension, high stakes.
What if that president has moments
where they're not so clear, to the point
where the cabinet's actually talking about invoking
the 25th Amendment?
The Secret Service would be aware of this.
They can't talk to the press about that.
Worse, they could end up talking to somebody
who they thought was the press but was really somebody else.
And in that period where the president isn't so clear,
well, that's the moment where the near peer
decides to go, surprise.
Surprise.
You can't normalize this.
Another example, let's say the president has a girlfriend.
The president has to be able to trust
that the Secret Service isn't going to tell the press.
Because if the president can't trust the Secret Service,
what's the president do when he wants to see his girlfriend?
He kind of has two options, right?
One, ditches detail.
Well, that's dangerous.
The other is find somebody on the detail
who's willing to break protocol and sneak a person in.
That's also really, really bad.
We cannot normalize this.
There shouldn't be comments coming from the Secret Service.
I understand that most Americans want the Secret Service
to just spill the beans about former President Trump.
I get it.
I really do.
And when it comes to something that
rises to the severity of the sixth,
sure, maybe the agency could issue a statement
to the committee saying what they know.
Not individual agents.
And it should be limited to material facts.
And that's the important part here.
Again, waiting until the storyline
is swinging my perceived direction, right?
Right now, they're saying it happened.
There's a bunch of sourcing that says that.
OK, it doesn't matter.
That's not the bombshell.
The bombshell isn't that the former president
had an altercation with the Secretary of Defense.
He had an altercation with the Secret Service, allegedly.
That isn't the bombshell.
That doesn't even matter.
The bombshell is that the former president
is alleged to have wanted to go lead the rioters.
That's the bombshell.
He wanted to show up and support his people.
That's the bombshell.
That isn't being disputed by any of these stories.
People are focusing on the scandal,
on the salacious tidbits, rather than the material facts,
what actually matter.
And they're going to lose the plot.
They're going to get into this battle about which Secret
Service agents do you believe.
It doesn't matter.
It's completely irrelevant.
And none of them should be talking.
The president has to be able to trust his or her detail.
They can't just, they can't be surrounded
by people they don't trust.
They can't be surrounded by people
that they perceive as being a possible media
source or a security leak.
We can't normalize this.
We can't normalize this.
We can't normalize this.
We can't normalize this.
The bombshell is that he wanted to go.
That's what matters, not whether or not
he lunged over a seat or grabbed or yelled at somebody.
None of that matters.
Whether it happened or not, it doesn't matter.
And we should know about it.
Everybody needs to focus on the important part.
With everything that was going on at the Capitol,
the former president wanted to go there, by all the testimony
that we've seen.
That's what matters, not whether or not
he grabbed somebody or attempted to.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}