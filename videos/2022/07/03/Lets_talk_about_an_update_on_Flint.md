---
title: Let's talk about an update on Flint...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tR8_jaT_SjM) |
| Published | 2022/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court in Michigan declared charges against the former governor and others void due to procedural issues.
- The law authorizes a judge to issue arrest warrants and subpoena witnesses, but not indictments.
- The case is linked to the lead contamination crisis in Flint during 2014-2015.
- The state plans to continue trying to prove the allegations and see the process through.
- The Supreme Court emphasized the severe impact of the government's actions on innocent citizens in Flint.
- The crisis is regarded as one of the country's greatest betrayals of citizens by their government.
- The legal process involves an additional step before obtaining indictments.
- The state is committed to pursuing indictments and moving forward with the case.
- The Supreme Court decision was unanimous, possibly due to misinterpreting a rarely used law.
- Elected officials prioritizing self-interest over public service is a growing issue in the United States.

### Quotes

- "The Flint water crisis stands as one of this country's greatest betrayals of citizens by their government."
- "This type of betrayal, it's becoming more common in the United States where elected officials at least appear to be putting their own interest above the interests of the people they're supposed to serve."

### Oneliner

The Supreme Court voids charges in Flint case due to procedural errors, reflecting a growing issue of officials prioritizing self-interest over public service, impacting innocent citizens severely.

### Audience

Legal advocates, activists, concerned citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for justice for the Flint community (suggested)
- Support organizations working towards justice for communities affected by similar crises (implied)

### Whats missing in summary

The full transcript provides detailed insights into the legal process surrounding the Flint water crisis and the challenges faced in seeking justice for affected communities.


## Transcript
Well, howdy there internet people, it's Bill again.
So today we're going to talk about Flint, Michigan, and what's going on up there.
The Supreme Court there has decided that the charges against the former governor and others are void.
That the way the charges were obtained wasn't procedurally correct.
They said that the way the law is written, it authorizes a judge to investigate subpoena
witnesses and issue arrest warrants, but they do not authorize the judge to issue indictments.
This is of course related to the lead contamination back in 2014-2015.
Now the state is saying that this isn't over.
They're going to continue to try to prove the allegations.
And they're trying to see the process through.
It's worth noting that the Supreme Court said, if the allegations can be proved, it is impossible
to fully state the magnitude of the damage state actors have caused to an innocent group
of people, a group of people that they were entrusted to serve.
The Flint water crisis stands as one of this country's greatest betrayals of citizens
by their government.
This has been going on for years and years and years, and it's a procedural thing.
I understand what they're saying.
Basically, in this case, a single judge reviewed everything and said, yes, charge them.
It looks like they're going to have to go through another step before they can get the
indictments.
It does appear that the state is going to try to do everything within their power to
actually get the indictments and continue to move forward with this case.
It's also worth mentioning that the Supreme Court made this decision unanimously.
So this was probably a bad read on a rarely used law, and it has led to this procedural
issue.
This type of betrayal, to use the Supreme Court's term, it's becoming more common in
the United States where elected officials at least appear to be putting their own interest
above the interests of the people they're supposed to serve.
These kind of cases are probably going to become more common.
Hopefully they will not be as severe as the case in Flint, Michigan.
And hopefully they won't take almost a decade to resolve.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}