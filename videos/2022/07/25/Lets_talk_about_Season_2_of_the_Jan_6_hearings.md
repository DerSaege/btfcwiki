---
title: Let's talk about Season 2 of the Jan 6 hearings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9o6-2TWmu4c) |
| Published | 2022/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of upcoming hearings in September with a focus on non-political figures and their ties to various groups.
- Mention of a subpoena for Jenny Thomas, wife of Justice Clarence Thomas, and withheld information about members of Congress.
- Speculation on potential arrests of prominent Republican establishment members before the midterms.
- Prediction that these developments will impact the outcomes of the upcoming elections.
- Suggestion that Trump's legal troubles may escalate in Georgia rather than DC.
- Analysis of Trump's potential influence on the midterms and his strategy to maintain support.
- Anticipation of Trump pushing his baseless claims during the midterms and influencing Republican candidates.
- Warning about the Republican Party's failure to hold Trump accountable after January 6th and its long-term consequences.
- Doubt about the Republican Party's ability to recover from ongoing issues in time for the 2024 elections.
- Concern about the potential consequences if those protecting the institution of the presidency do not prevail.

### Quotes

- "The Republican Party has made a lot of mistakes over the last few years."
- "The choice will be clear."
- "Had they taken control of their party after the 6th, they probably would have been in pretty good shape."
- "But I do not see at this point how the Republican Party plans on dodging this."
- "Y'all have a good day."

### Oneliner

Beau anticipates upcoming September hearings focusing on non-political figures' ties, potential arrests in the Republican establishment, and Trump's influence on the midterms, warning of the GOP's failure to hold Trump accountable.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact local representatives to express concerns about political accountability (implied)
- Support organizations working towards political transparency and accountability (suggested)
- Stay informed about political developments to make informed voting decisions (implied)

### Whats missing in summary

Analysis of the potential impact on voter trust and democracy

### Tags

#Hearings #RepublicanParty #Trump #Accountability #Midterms


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
season two of the hearings and what's likely to come up in September. The committee has
announced there will be more hearings in September, and people are kind of wondering what they're
going to be about. They haven't really said. My guess is that the primary focus is going
to be bringing in information about people who are not in political office or government
and tying it in. They will probably also work to solidify the ties between the elector scheme,
the militant groups, fundraising, outside pressure, all of this stuff, and showcase
it more clearly. They're already talking about issuing a subpoena for Jenny Thomas,
that is the wife of Justice Clarence Thomas. We also know that the committee has held information
related to members of Congress that hasn't been published yet, that hasn't been put
out by the committee yet. The committee early on kind of gave a shot across the bow to the
Republican Party and said, hey, y'all need to do the right thing, and the Republican
Party didn't do it. They're convinced they can ride this out. So there's a lot of information
that I think is going to come out in these hearings that people who are currently in
Congress probably don't want to and probably thought that the committee would kind of look
the other way on. I don't think that's true anymore. Now the thing about this is that
these September hearings, they're on a crash course. They are going to run smack into the
midterms. This is really bad news for the Republican Party, but it's self-inflicted.
They could have handled this problem themselves. They chose not to. The Republican Party could
have dealt with this issue immediately after the 6th, and none of this would be happening.
But the hearings will go on and it will probably end up influencing outcomes of the midterms.
Now other developments. I expect pretty prominent members of the Republican Party establishment.
So I'm not necessarily talking about those holding political office, and I'm not necessarily
talking about Trump when I say this, but I think there are going to be prominent members
of the Republican establishment that get arrested between now and the midterms. And I think
every time that happens, it's going to send a little wave through the Republican Party.
And every time that happens, I think there are going to be people who are going to be
like, well, I don't want to be next. And they will disclose what they know to the committee
or to the Department of Justice directly. Now as far as Trump's concerned, I think his
lawyers should be looking at Georgia more than looking towards DC. I think he might
be in real trouble there. Now with all of this going on and the knowledge that Trump
definitely wants to pursue another run towards the White House, or at least maintain the
appearance that he wants to run again for the White House so he can continue getting
money from his base, the hearings are going to draw attention to what happened in 2020.
Trump won't want to be seen as the loser who lost. So during the midterms, he will be out
there pushing his bogus claims, claims that the majority of Americans already know are
false. But he's going to push them to try to energize that base. He will, in essence,
try to turn the midterms into a referendum on him, which means he will be pushing Republican
candidates for various offices to echo his claims. And this is where you run into that
issue of you can't win a primary without Trump, but you can't win a general with him.
There is a high likelihood that Trump will push his endorsed candidates to back his statements
by threatening to withdraw endorsements or talking bad about them on social media because
it worked before. That's how he got the entire Republican Party to do his bidding. That's
how they wound up in this mess. And the people who are most likely to continue to obey him,
they're going to be the names in the press. So they'll be out there pushing these claims
and it will taint the entire Republican Party. It will present a very black and white picture
for the American people. The choice will be clear. The Republican Party has made a lot
of mistakes over the last few years. The biggest they made was not holding Trump accountable
after the 6th because that's going to have the longest lasting effects. Had they taken
control of their party after the 6th, they probably would have been in pretty good shape
to run for 2024. The way it's shaping up, I don't think they will be. I think they'll
still be recovering from what's going to occur over the next few months. This is all assuming
that those who want to protect the institution of the presidency don't win the little tug
of war that's going on right now. This is assuming that DOJ or local prosecutors do
their job. But I do not see at this point how the Republican Party plans on dodging
this. They seem to be under the appearance that their followers are so enamored and so
conditioned to vote Republican no matter what that it's not going to matter. I don't know
that that's true. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}