---
title: Let's talk about Wyoming, Cheney, and the primary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Z71YjJaNvAg) |
| Published | 2022/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the impact of Liz Cheney potentially losing the primary in Wyoming.
- Warns that if Cheney loses, the Trump machine will be entrenched in Wyoming, leading state and local officials to adopt Trump-like strategies for power.
- Points out that people in Wyoming are shielded from the negative effects of Trumpism at the national level.
- Predicts that Wyoming residents may not appreciate the authoritarian policies that could follow a Cheney loss.
- Acknowledges that Cheney's stance of putting country over party and holding Trump accountable could lead to her losing the primary.
- Suggests that Cheney's sacrifice of her office for ethics and integrity could make her a significant figure in national politics in the future.
- Speculates that Cheney's actions position her as a potential force in the Republican Party to steer it away from being a cult of personality.
- Emphasizes that Cheney's decision to support the Constitution over Trump could shape her political career positively in the long term.
- Contrasts Wyoming's deep red political landscape with Georgia's swing state status in evaluating Cheney's chances in the primary.
- Concludes with the idea that Cheney may not fade away from politics after the primary, leveraging her sacrifice for political advantage.

### Quotes

- "If she loses, that means the Trump machine gets entrenched in Wyoming."
- "She chose to support the Constitution over Trump."
- "She has set herself up very well to be on the national scene in 2024 or 2028."
- "She gets that forever."
- "She has set herself up as the force that can turn the GOP back into an American political party."

### Oneliner

Beau warns of the consequences of Liz Cheney potentially losing the primary in Wyoming and predicts her enduring national influence despite the sacrifice.

### Audience

Political observers, Republican voters

### On-the-ground actions from transcript

- Support political candidates who prioritize ethics and upholding the Constitution (exemplified)
- Stay informed about the impact of local and state officials' political strategies on communities (implied)

### Whats missing in summary

Insights on the potential ripple effects of Liz Cheney's actions on the Republican Party and national politics.

### Tags

#LizCheney #WyomingPrimary #Trumpism #RepublicanParty #Ethics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Wyoming and Cheney
and the primary coming up, what it means, the outcomes.
I got a message basically saying, hey,
I'm voting for Cheney on August 16.
I'm a lifelong Republican, but I'm
pretty sure she's going to lose.
What does that mean for Wyoming, and what does it mean for Cheney?
What does it mean for Wyoming?
Nothing good.
If she loses, that means the Trump machine gets entrenched in Wyoming.
It gets locked down, and state and local officials will try to model themselves after Trump in
order to get elected.
Now, people in Wyoming right now, they may be like, yeah, that's going to be great.
But that's mainly because they're pretty well insulated from all of the issues that
Trumpism causes at the national level.
They don't actually see it.
They just hear about it through a filter of Fox News or whatever.
When state and local officials start doing it, they have to engage in the same type of
political strategy that Trump does, which means they have to other people inside Wyoming.
They have to create division and drama inside Wyoming to propel their political careers,
And they'll do it because the people who follow Trump, they're not actually about their rhetoric.
They're about power.
I don't think people in Wyoming are going to like that.
Eventually they'll have to come to terms with the policy decisions they get made by an entrenched
Trumpist regime.
I don't think they're going to like that either because it's authoritarian and Wyoming is
rural and generally speaking people in rural areas they don't actually like
authoritarian policies when they impact them and they'll start. So if she loses
it's bad for Wyoming but that doesn't mean that people aren't going to make
that decision come the 16th. They probably will. That being said, the same
strategy that was at play in Georgia that helped put normal conservatives
over the top is at play in Wyoming. Is it going to be as successful as it was in
Georgia? I don't know. I don't know because Georgia was pretty close to
to swing state. Wyoming, that's deep red. So, while I am not ready to say that it's
a guaranteed loss for Cheney, it's probably pretty likely that she lose. There are things
that can happen that could lead to her winning. But, more than likely, she lost. Now, what's
What's that mean for Cheney?
Not what people are thinking.
Cheney's not new.
Cheney's not new to politics.
She's a Cheney.
The moment she decided to put country over party, the moment she decided to attempt to
hold Trump accountable, she knew that it was a high likelihood she'd lose this primary.
At the same time, as you acknowledge that, she did this.
She chose to support the Constitution over Trump, right?
That sounds very altruistic, especially when you're talking about sacrificing your office.
you're gonna lose, right? Yeah. Keep in mind, she's a Cheney. She's a Cheney. I don't expect
Cheney to go away. She has set herself up very well to be on the national scene in 2024 or 2028.
The idea of, I sacrificed my office because of ethics, honor, integrity, my
oath to the Constitution, all of this stuff, she gets that forever. She gets
that as a political talking point, as a political tool forever. When the
Republican primaries roll around for president, believe me, her name's gonna be
in the hat. It would really surprise me if she disappears from politics after
this because she has set herself up as the force that can turn the GOP back
into an American political party rather than a cult of personality. I don't
think that that was happenstance. I have no doubt that she truly does want to
hold Trump accountable and does believe that he violated the Constitution, broke
laws, violated his oath. I have no doubt about that. I also know or believe that
she's aware of the fact that even though it might hurt her in Wyoming, it turns
her into a national figure whose honor is unquestionable because she sacrificed
her office to uphold the Constitution. She crossed party lines to support the
Constitution to support the United States. She gets that forever. As the
rhetoric of Trump loses its luster, I think she'll end up being probably more
of a force in the Republican Party than Vice President Cheney was.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}