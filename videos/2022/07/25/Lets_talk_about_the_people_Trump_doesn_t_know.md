---
title: Let's talk about the people Trump doesn't know....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3f3PrMoG8zU) |
| Published | 2022/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticisms of Trump result in him claiming he doesn't know the individuals, disavowing them by pretending he never met them.
- Trump labeled the Capitol attack as a heinous attack after it could potentially harm his image, distancing himself from the incident.
- Despite wearing MAGA gear and being his supporters, Trump turned on the Capitol rioters, stating, "You will pay" and that they don't represent his movement or country.
- Trump's habit of disavowing people and distancing himself from mistakes by letting others pay for them is evident in his reaction to the Capitol attack.
- Loyalty shown to Trump by many individuals will never be reciprocated, as he easily discards people once they no longer serve his purpose.
- Even those who risked their safety, stood on the front lines, or faced custody for Trump are disregarded and excluded from his movement once he no longer needs them.

### Quotes

- "You do not represent our movement. You do not represent our country."
- "Even those people willing to put themselves at risk, those people willing to quite literally stand on the front lines for him, those people willing to be in custody for him, they're not part of his movement."
- "Loyalty shown to the former president will never be returned."

### Oneliner

Trump disavows supporters once their use is over, letting others pay for his mistakes, even those who risked everything for him.

### Audience

Supporters of Trump

### On-the-ground actions from transcript

- Stand up for accountability within political leadership (implied)
- Recognize and acknowledge patterns of behavior in political figures (implied)

### What's missing in summary

The emotional impact and frustration from individuals who have been discarded and disavowed by Trump despite their loyalty and sacrifices.

### Tags

#Trump #loyalty #accountability #politicalleadership


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about
how Trump doesn't know these people.
You know, it's a common refrain.
Many of the people who criticize him,
when their criticisms become public,
he says he doesn't know them.
Never knew her, never met him.
Coffee boy, right?
And his base buys this.
And the thing is, I feel like by this point they should know
that this is just how he disavows people
and throws them under the bus.
And he did it to them.
What happened on the 6th?
His base will call legitimate political discourse,
a tourist visit, a protest.
What did Trump call it?
A heinous attack.
Because it could have came back on him, right?
Looked bad on him.
So he disavowed it, tried to move away from it.
To the people who were there, you know,
the people wearing like all the MAGA stuff,
very much his supporters, his movement, what did he say?
You will pay.
You do not represent our movement.
You do not represent our country.
Those people that now he's pretending that he cares about
because it's good for him politically,
the day after, they weren't his people.
He didn't know them.
Never met them, right?
Certainly didn't encourage them to go to the Capitol.
Didn't say that he was going to walk up there,
meet them there, all of that stuff.
They went under the bus just like everybody else.
It's what he does.
This is how he disavows people.
It's how he separates himself from his mistakes
and lets other people pay for them.
So many people going out of their way
to try to show loyalty to the former president
when it will never be returned.
Even those people willing to put themselves at risk,
those people willing to quite literally
stand on the front lines for him,
those people willing to be in custody for him,
they're not part of his movement.
And they became not part of his movement
as soon as he no longer had a use for him.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}