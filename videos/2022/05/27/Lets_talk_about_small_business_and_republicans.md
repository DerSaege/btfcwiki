---
title: Let's talk about small business and republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OIrVN-XbPz0) |
| Published | 2022/05/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Politicians use talking points that quickly expire, but they expect us to forget about them.
- Republicans were pushing for people to shop and consume at small businesses during the early days of the global public health issue.
- A bill went to the Senate to help gyms and restaurants impacted by the pandemic, but Republicans largely voted against it.
- The bill aimed to refill a fund to support struggling businesses, including restaurants and gyms.
- Only a fraction of the restaurants that applied for relief actually received any funds due to the program running out of money.
- The Independent Restaurant Coalition warns that up to half of the 177,000 restaurants awaiting funds could close without this support.
- The failure to pass the bill puts tens of thousands of restaurants and small businesses at risk of closure.
- The bill also had provisions for gyms, live event operators, and possibly ferries.
- Republicans used small businesses as talking points to encourage economic activity, but failed to follow through with meaningful support.
- The new talking point is to argue against providing relief by deeming certain groups as no longer useful.

### Quotes

- "Of course, maybe we do, because you don't really see politicians held accountable for this behavior."
- "Some of you may die, but it's a risk they're willing to take."
- "Guess that talking point expired."

### Oneliner

Politicians' talking points on supporting small businesses quickly expire without real action, leaving thousands of businesses at risk of closure due to lack of support.

### Audience

Voters, activists, community members

### On-the-ground actions from transcript

- Support local restaurants and small businesses by dining in or ordering takeout (implied)
- Advocate for government support for struggling businesses by contacting elected officials (implied)

### Whats missing in summary

The emotional impact of businesses closing and the lack of accountability for politicians' empty promises.

### Tags

#SmallBusinesses #PoliticalPromises #GovernmentSupport #CommunitySupport #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about talking points
and the expiration date on those talking points.
Because it's something that's always made me laugh.
Those expiration dates, they come quick.
You know, you can have politicians out there
talking about something.
And they just repeat it over and over and over again.
They make it seem like it's the most important thing
in the world to them.
And then like a year later,
they don't do anything about it.
You know, they act like we're just supposed to forget.
Of course, maybe we do,
because you don't really see politicians
held accountable for this behavior.
So maybe we do actually forget about it.
So let me remind you one.
You remember back in the early days
of our global public health issue,
how Republicans were out there
talking about small businesses?
We have to just keep going.
It's your duty to get out there and shop, consume.
Don't live in fear, go about your life as normal.
Y'all remember this?
Specifically citing restaurants and gyms
as like the main things
that we really needed to pay attention to.
Because if we didn't go out there and shop
and be good consumers,
well, they would go out of business.
Y'all remember that?
I do.
I do.
That wasn't that long ago.
So there was a bill that went to the Senate
specifically to help gyms and restaurants
actually like listed out
that were strained by the pandemic.
And you know Republicans lined up
to vote in favor of that bill, right?
Nah, nah.
They lined up to vote against it.
Only five Republicans voted for it.
Didn't get passed.
Didn't get passed.
It was supposed to give $48 billion
and distribute it to companies,
small businesses that were impacted heavily.
And it's not a new program.
It's a program that had already been established,
but it ran out of money.
The restaurant revitalization fund,
which was just part of it.
I want to say initially they had $27 billion.
It was less than $30 billion.
But only one out of three restaurants that applied
actually got any money because they ran out of money.
So this bill was supposed to kind of refill this fund
and help those businesses that Republicans pretended
they cared about, you know, in the beginning.
But now is the part where a lot of people are going to realize
that they don't care, that that's not what it was about,
that that was just a useful tool
to get people to go about their lives as normal,
to keep the economy rolling.
Because if the economy stays rolling,
well, those senators,
they don't lose out in their stock portfolios as much.
I know it's risky to do it that way.
Some of you may die,
but it's a risk they're willing to take.
Yeah, so that doesn't pass.
Now, the Independent Restaurant Coalition
says that of the 177,000 or so restaurants
that are waiting for this fund,
that had applied for it and are waiting for this money,
that up to half of them could close in the next few months
because this didn't pass.
Tens of thousands of restaurants, small businesses,
you know, the ones Republicans care about.
Yeah, that's wild.
That is wild.
It also had provisions for gyms and for live event operators.
I want to say ferries as well,
but it was the gyms and the restaurants
that really stuck out to me
because those were their examples.
Those were their talking points.
It's what they used to try to motivate people
to throw their family under the bus,
onto the altar of the stock market.
But when it came time to actually do something about it,
they blocked it.
So when you see those restaurants,
tens of thousands, close,
those gyms close in your town,
just remember why.
They're waiting on relief
that the Republicans in the Senate
decided they didn't need.
Guess that talking point expired.
The new one is to say that,
well, we shouldn't help because,
well, you're not useful to them anymore.
It's really that simple.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}