---
title: Let's talk about something better than a ban....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g5g7OE3REME) |
| Published | 2022/05/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the importance of focusing on the big picture in the current gun debate in the U.S.
- Points out that debates often get stuck on insignificant details like magazine size or cosmetic features of firearms.
- Questions the practicality of gun confiscation and the logistics behind it.
- Suggests focusing on the people behind the firearms rather than the firearms themselves.
- Notes the high percentage of individuals involved in large incidents with a history of domestic violence.
- Emphasizes the need to address domestic violence legislation to prevent gun violence effectively.
- Proposes targeting specific markers in individuals to close loopholes and prevent incidents.

### Quotes

- "None of that matters. None of it matters. If you're focusing on stuff like that, you're missing the big picture."
- "It's not impossible, but that's not going to happen. Not in a time frame that will matter."
- "Rather than looking at the firearms, look at the person behind them."
- "That's your key. That's your thread."
- "What matters is the person behind it."

### Oneliner

Beau addresses the need to focus on the individuals behind gun violence and proposes legislative changes to prevent incidents effectively.

### Audience

Legislators, activists, community members

### On-the-ground actions from transcript

- Address domestic violence legislation loopholes (suggested)
- Advocate for laws targeting individuals with specific markers for prevention (suggested)

### Whats missing in summary

The detailed breakdown of statistics regarding individuals with a history of domestic violence involved in large incidents is missing from the summary.

### Tags

#GunDebate #DomesticViolence #Legislation #Prevention #CommunityPolicing


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about some very important information when it comes
to
the current debate in this country.
If you're one of those people who's looking around saying, how are we ever
going to stop this,
watch this video.
Especially watch past the beginning, because you're not going to like the stuff at the
beginning.
But, it's where everybody turns.
Every time something like this happens, the debate goes to one place in
particular.
And everybody starts talking about
magazines or
the...
some vague description,
or specific model,
or something like that.
None of that matters.
None of it matters. Forget all about it.
In fact, that's one of the reasons there's no forward movement.
Because if you're focusing on stuff like that, you're missing the big picture.
If you're focusing on trying to
limit magazine size,
or the type of operation, or the length of it, or the cosmetic features of it,
stuff like that,
you've missed the point.
It's not going to change a thing.
I know somebody's going to say, well it's always this particular rifle.
That's the most popular rifle in the United States.
That's why it's always that one.
So, because I said this, and said, well, focusing on specific types isn't going to
do any good,
I'm sure there's somebody out there saying, well, let's just take them all.
Okay.
Let's just do this from a logistics standpoint.
One a minute,
you are getting one gun off the street every minute of every day.
How long do you think it'll take?
Five years?
Ten? Twenty?
Since this conversation is prompted,
and by these incidents, let's do it a different way.
If a child was born today,
how old would their headstone be if they lived to be a hundred?
And the answer to that is more than 500 years old.
If you were taking one gun off the street every minute of every day,
it would take more than 600 years to get rid of them.
That's how many guns are in the US.
When you start talking about confiscation,
you are not talking about things that are logistically practical.
It's not impossible, but that's not going to happen.
Not in a time frame that will matter.
By the time that occurs, we'll still be sitting here arguing what types of backpacks
are best to stop the lasers.
That's not a solution.
In the United States, Pandora's box is opened.
They're everywhere.
Taking that route, it will take hundreds of years.
Even if you were to just focus on certain types,
or those that are just adamant and violent in their response,
you're still talking about a hundred years or more.
I think we can do better than that.
Rather than that, rather than looking at the firearms,
look at the person behind them.
60% of the people responsible for the large incidents
have a domestic violence history, or the act itself was DV.
And this is a confirmed connection.
This is something that is really underreported.
The real numbers are going to be much higher.
That's your key.
That's your thread.
You add being mean to animals in with this,
and you are knocking out the overwhelming majority of these incidents.
And it's something that can be done now, right now, this minute.
It can happen.
It's not something that's going to take centuries to enact.
Now I know somebody on the pro-gun side right now saying,
well, there's already a law.
No, there's not.
There's a law that says that it impacts this, but it doesn't.
As evidenced by the fact that 60% have a connection.
That shows you that it doesn't work, because it has tons of loopholes.
The way it's written, it doesn't do any good.
The loopholes are there, some people would say,
because of the prevalence of domestic violence in law enforcement.
And maybe that has something to do with it.
But what we do know is that they exist.
The boyfriend loophole, there are tons that make that amendment worthless.
It doesn't do anything.
That's your key.
That's where your legislation needs to be.
First off, it's super easy to package.
When you're talking about the right wing, right?
Think about how easy it would be if you got out there and said,
hey, we want to take guns away from wife beaters.
They'll be all over it.
And then you can drag those NRA-owned senators up there and ask them,
hey, why are you defending going out of your way to defend, you know,
this particular demographic of people who not only did this thing,
but also are responsible for 60% of these incidents?
This is something that is politically viable.
It can get through and it'll be effective.
I don't know what more you want.
That's your solution.
That's your route to go.
It'll work.
And it'll move forward.
A lot of this stuff right now, it's the same conversation
I have seen over and over again.
Talking about magazine size, talking about what qualifies as an assault weapon,
talking, none of it matters.
None of it matters.
What matters is the person behind it.
And that person has some pretty specific markers.
And it's relatively easy to close the loopholes.
That's the route you need to go.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}