---
title: Let's talk about 2 possible legislative futures for Democrats....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FxOjnCs6ykA) |
| Published | 2022/05/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democrats facing a choice between an assault weapons ban and closing domestic violence loopholes.
- The potential consequences of pursuing an assault weapons ban.
- Addressing only 16% of mass incidents with an assault weapons ban.
- The likelihood of an assault weapons ban facing political and legal challenges.
- The idea that banning assault weapons might not be as effective in reducing casualties.
- Proposing closing domestic violence loopholes as a more effective approach.
- The higher potential impact of closing domestic violence loopholes.
- The underreporting of domestic violence incidents.
- The importance of upping reporting to prevent access to firearms by domestic violence offenders.
- Aiming to shift the gun culture by closing loopholes instead of focusing on assault weapons bans.

### Quotes

- "Rather than just changing the incident, you're stopping it."
- "Rather than making it the forbidden fruit to get to own the libs."
- "When you look at the data and put everything in context, it's the only thing that actually makes sense."

### Oneliner

Beau presents a compelling case for the Democratic Party to prioritize closing domestic violence loopholes over pursuing an assault weapons ban to effectively address mass incidents.

### Audience

Legislators, Democratic Party members

### On-the-ground actions from transcript

- Advocate for closing domestic violence loopholes to prevent access to firearms (implied).
- Support measures that increase reporting of domestic violence to prevent firearm access (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential impacts and effectiveness of different approaches in addressing mass incidents, offering valuable insights for policymakers and advocates.

### Tags

#DemocraticParty #GunControl #DomesticViolence #Legislation #PolicyChange


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about two possible futures
for the Democratic Party and their moves from here,
because they are promising that they're going to move forward
and it looks like they're going to move a certain direction
when it comes to legislation.
And I have been pretty vocal about the fact
that I think that's a really bad idea,
that they're focusing on the wrong thing.
And I've had a lot of questions about why I believe that.
So that's what we're going to talk about today.
We're going to talk about the possibility of Democrats
moving forward with an assault weapons ban
versus the possibility of them moving forward,
closing the loopholes when it comes to domestic violence
and those people owning firearms.
OK.
OK.
So if they move forward with an assault weapons ban,
what's actually going to happen?
I mean, sure, there's going to be
all kind of political backlash.
They're going to burn political capital, all of that stuff.
Absolutely.
It's going to happen.
It's gun control.
Hot button issue.
What happens when it gets to the Senate?
It gets defeated.
It gets defeated, but let's say it gets through the Senate.
What happens when it gets to the Supreme Court?
It gets overturned.
They've pretty much already said that.
But let's say it doesn't.
Let's say it doesn't get overturned for whatever reason.
OK.
It addresses 16% of mass incidents.
Now, we're using information from, what is it,
Gun Safety Everywhere, Everytown.
I'll have the link.
16%.
And we'll get to what addresses means here in a second.
Now, what happens in this case?
The assault weapons are gone, right?
But it's not actually stopping anybody from getting one.
It's just a certain type are gone.
So they use a different type of weapon.
They just use one that isn't an assault weapon, right?
And there is the idea that that would make them less lethal.
But is there anything to back that up?
In that study, it says, hey, they're
responsible for more injuries.
Yeah, and that's true.
But it also says that 3 quarters of those in public
were assault weapons.
There are more people in public than there
are in private homes, which is actually where
most mass incidents happen.
So that kind of throws that off as far as number of people.
So what you would have to do is find another way
to corroborate that idea.
If it was true that assault weapons meant an incident
would have more casualties, then you
could look at the list, the giant list of mass incidents
in this country, and you would find the assault weapons up top
and the non-assault weapons at the bottom.
Is that what happens when you look at that list?
No, it's not.
It's not, because that doesn't have anything
to do with the lethality of it.
The third most deadly incident in this country
is Virginia Tech, done entirely with pistols.
So it's just going to make them switch the type of weapon,
and it's not actually going to help,
because it's not going after the people who
are most likely to do this.
It's regulating a type of weapon,
and then they can still get other stuff.
And they will, and they'll use it.
Another one that you might want to think about is Parkland.
And I know somebody's going to say, well, he had an AR.
He did, but he had 10 round magazines,
which made it any other varmint rifle in the country.
It doesn't have the impact that people think it will.
And that's assuming it can get through all
of the political hurdles.
Even once it goes into effect, it
won't have the impact that people think it will.
Bad people will still be able to do it,
because they'll still be able to get stuff.
Now, the alternative course of action
would be to go after closing all of the domestic violence
loopholes.
Go that route.
Could you get it through the Senate?
I think that would be easy to package.
I think it would be easy to sell to the public.
I think it would be easy to get senators that
might stand against it to go for it if the Democratic Party just
called them out on it.
This is who we're trying to stop.
And I don't believe that the Supreme Court would overturn it,
because Congress has that power.
This is why felons can't own.
Congress has the ability to restrict
based on behavior like this.
So it would get through, whereas the other one probably won't.
It would be upheld, whereas the other one probably won't.
And what does it do?
It makes it much harder for those people.
In that study, it's 53%.
Now, you've heard me say 60, because I have a different
study that I've been using for a long time.
I'm using this one now, simply because it's
the only study I found that has both sets of numbers,
the percentage for assault weapons used
and the percentage for DV.
OK, so it actually stops these people,
makes it much harder anyway, for them to get firearms.
That seems like the better route,
because then rather than just changing the incident,
you're stopping it.
That seems to make more sense.
And that 53% or that 60%, that includes some of that 16.
That seems like the way to go.
Now, the other thing to remember is that DV
is drastically underreported.
And the 53% is off of confirmed histories, documented histories.
So if we could also up the reporting of this, which
that might be more likely if the people knew they couldn't go out
and get a gun, those numbers would be higher.
It's not going to stop them all.
It won't.
But it will stop some.
Whereas with the assault weapons ban,
I don't think it'll stop any.
It will just change them to something else.
And the perception is that it'll be less lethal,
but history says otherwise.
That's why I am so much in favor of going
after closing the loopholes.
Rather than just altering it, you stand a chance of stopping it.
And if you do that and you create that kind of barrier
to catching a DV charge, you might also stop that.
I mean, not stop it completely, but you'll reduce it.
It'll be another deterrent.
And going that way stands a better chance
of beginning to shift the whole gun culture.
Because of the length of time it would take to actually remove
firearms from circulation, the route you have to go
is shifting thought to where people don't want them.
This is a very important point.
And I think that's why I'm so much in favor of closing
the loopholes.
Because if you do that, you'll reduce the number of people
who don't want them.
This is going to help with that.
Rather than making it the forbidden fruit
to get to own the libs.
Rather than giving it another Republican victory
for their Supreme Court.
That's the reason.
And when you look at the data and you
put everything in context, it's the only thing
that actually makes sense.
If you're talking about addressing mass incidents,
that's the way to go.
And the best part is that it could actually pass.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}