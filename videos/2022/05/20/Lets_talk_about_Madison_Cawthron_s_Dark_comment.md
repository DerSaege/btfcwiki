---
title: Let's talk about Madison Cawthron's Dark comment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TBp5wji8f3Y) |
| Published | 2022/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzes Madison Cawthorn's statement and the narrative building around it.
- Talks about information consumption and shaping narratives in the media.
- Mentions the habit of fitting new information into existing narratives.
- Suggests using a method to filter out narrative building and access raw information.
- Encourages looking into the origins of terms before narrative building began.
- Explains how to use the date filter option on search engines to access raw information.
- Links a Newsweek article about "Darkmaga" and describes it as fascism.
- Criticizes the rebranding and insertion of authoritarian ideologies into conservative politics.
- Emphasizes the importance of calling out fascism when it is present, without downplaying it.
- Raises awareness about the implications of certain political movements, including fascism.
- Points out the significance of recognizing and labeling authoritarian governance attempts.
- Encourages viewers to analyze meme movements and understand their underlying messages.

### Quotes

- "The time for, I'm assuming this means Gentile, but that's not what he typed, politics as usual has come to an end."
- "This isn't something that the media should try to downplay because it is very open."
- "When you see this, just call it what it is. It's fascism."

### Oneliner

Beau dissects Madison Cawthorn's statement, urges raw information consumption, and exposes fascist undertones in "Darkmaga".

### Audience

Media Consumers

### On-the-ground actions from transcript

- Read the Newsweek article about Darkmaga and understand its implications (suggested).
- Analyze meme movements to comprehend their underlying messages (suggested).
- Call out authoritarian ideologies and fascism when identified (exemplified).

### Whats missing in summary

In-depth analysis of Madison Cawthorn's statement and the media's role in shaping narratives.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Madison Cawthorn and his statement
and what we can learn about information consumption while looking into his statement.
Because when he said what he said, he used a term,
and immediately that term became the focus of article after article after article.
People trying to shape a narrative.
People trying to insert that term into their own belief system.
So it means what they want it to mean.
And this happens a lot when there's a dynamic event that brings something new
into the main political discussion or the main public consciousness.
There is a habit of trying to fit it in so it doesn't disrupt the current narrative.
They just want to plug it in somewhere.
It would be great if there was a method of going into some dimension
where that dynamic event didn't occur and then looking to find out what it meant there.
And we're going to learn how to do that.
It's actually really simple.
You probably already know the tool exists, but you probably haven't used it.
So first, what is Madison Cawthorn's statement?
If you don't know, he lost his primary and he is not happy about that.
So he says,
When the establishment turned their guns on me,
when the Uniparty coalesced to defeat an America First member,
very few people had my back.
This list includes the lion's share of figures that came to my defense
when it was not politically profitable.
These are honorable men and women who are the type of friends anyone yearns to have.
At the beginning of a change, the patriot is a rare and hated man.
These are those rare and hated men slash women.
There are other national figures who I believe are patriots,
but I am on a mission now to expose those who say and promise one thing,
yet legislate and work towards another, self-profiteering, globalist goal.
The time for, I'm assuming this means Gentile, but that's not what he typed,
politics as usual has come to an end.
It's time for the rise of the new right.
It's time for Dark Maga to truly take command.
We have an enemy to defeat, but we will never be able to defeat them
until we defeat the cowardly and weak members of our own party.
Their days are numbered.
We are coming.
So after he puts this out, Dark Maga, this new kind of Maga,
becomes a topic.
There's article after article about it.
But when you read them, you can tell a lot of them are either trying to downplay
what Cawthorn has said, or turn it into a joke, or accept the terminology,
or dismiss it because it doesn't line up with the way they frame things.
So we need to know what this term means before that narrative building started.
Now on Google and on most search engines, there's an option to filter by date.
You go up to search tools or search options.
You click it and it gives you two dates that you can set.
So one you set for a year ago, and then the other you set for the day before
this dynamic event occurred, the day before Cawthorn put this out.
And then you will eliminate all of the narrative building and you will just get
the raw information.
This is incredibly useful if you really want to get a handle on things
before story building got involved.
This is how you do it.
In this case, there's a bunch of information.
I will put a link to a Newsweek article about Darkmaga down below.
It's from April.
For those who are frequent viewers of the channel, you know how rare it is
for me to actually link something.
Go read it.
That's what it is.
If you don't want to go read it, it's fascism.
That's what it is.
It's using that imagery to promote that authoritarian,
vengeful return of former President Trump.
The goal here is to take this ideology, because they can't just call it that,
rebrand it, and then insert it into normal conservative politics.
That's what they're trying to do.
They've been doing this for a while.
This is just very, very mask off.
I mean, understand, there's a whole lot of people who didn't want to believe
that's what a lot of the Make America Great Again movement was.
You had experts on political theory saying, hey, this is fascism.
You had experts on political violence saying, hey, this is fascism.
You had experts on fascism saying, hey, this is fascism.
But the media didn't want to really call it what it was.
They always tried to downplay it.
And they may do the same thing here.
The United States has a choice in this.
One of the options that is being offered through political discussion right now
is fascism.
And I need to point out that, yeah, he lost the primary,
but this is somebody who is currently a U.S. congressperson.
This little technique of filtering out the results,
the most recent results, is incredibly useful at times.
You can go back.
You can see how this meme movement developed.
And it started back in January.
And there have been people who have been watching it for a while
and identifying what it is because of the imagery that's used in it.
And basically, for some people, it's a joke.
For some people, it is very much a joke.
But for others, it's not.
It's a method to insert this particular brand of authoritarian governance
into American society.
This isn't something that the media should try to downplay
because it is very open.
I don't think using the terminology that they want you to use is a good idea.
When you see this, just call it what it is.
It's fascism.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}