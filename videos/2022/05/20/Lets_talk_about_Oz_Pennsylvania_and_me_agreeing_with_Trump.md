---
title: Let's talk about Oz, Pennsylvania, and me agreeing with Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DE4TQJxCGGI) |
| Published | 2022/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about former President Donald J. Trump and Dr. Oz in Pennsylvania.
- Trump suggested Dr. Oz should declare victory to prevent cheating with ballot finds.
- Despite no evidence of voter fraud, Trump's statement implies possible cheating.
- Beau disagrees with authoritarian tactics but agrees on the cheating implication.
- Trump's desire to find 11,780 votes in Georgia is revisited as a concerning pattern.
- Beau stresses the need to constantly remind people of Trump's baseless claims.
- Trump's actions reveal doubts about his sincerity towards democracy.
- Every accusation from Trump appears to be a confession of his own tactics.
- Overall, Trump's actions indicate a lack of belief in democratic principles.

### Quotes

- "People who are just, you know, out there looking and happen to find votes, that they're cheating and probably engaged in some kind of conspiracy, that I believe."
- "The only person that we have audio of trying to find votes is him."
- "Every accusation from this man seems like it's really a confession."

### Oneliner

Beau dissects Trump's tactics, revealing a pattern of baseless claims and a lack of belief in democratic principles.

### Audience

Voters, democracy advocates.

### On-the-ground actions from transcript

- Constantly remind others of baseless claims to counter misinformation (suggested).
- Initiate dialogues about the importance of democracy and fair elections (implied).

### Whats missing in summary

Deeper insights into the implications of Trump's actions on the democratic process.

### Tags

#DonaldJTrump #DrOz #Election #VoterFraud #Democracy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about former President Donald J. Trump
and Dr. Oz in Pennsylvania.
And, well, something he said, something Trump said,
and I can't believe I'm going to say this,
but for once, I kind of agree with him.
I really do. Let me lay out my logic.
But for once, at least part of what he's saying is crystal clear that it's true.
If you don't know what happened, Trump is backing Dr. Oz in this election up there.
And Trump said that Oz should just declare victory
because it would make it harder for people to cheat and try to find votes.
Now, to be super clear on this, there's absolutely no evidence of any kind of voter fraud
or anything like that whatsoever. None.
Just like last time, there's no evidence of any kind of widespread issue. None.
But what he said in his words, Dr. Oz should declare victory.
It makes it much harder for them to cheat with the ballots that they, quote, just happened to find.
Now, I don't agree that Dr. Oz should just declare victory.
That's not how elections work.
That's what authoritarian goons do.
That's not the right move.
But that last part, people who are just, you know, out there looking and happen to find votes,
that they're cheating and probably engaged in some kind of conspiracy,
that I believe, I think he's right there.
I do. And I don't agree with this man very often.
But I have to admit that that makes sense.
So look, all I want to do is this.
I just want to find 11,780 votes, quote, Donald J. Trump.
It's what he said in Georgia. His exact words.
So look, all I want to do is this.
I just want to find 11,780 votes, which is one more than we have
because we won the state, declaring victory and then trying to find those votes to back it up.
It's exactly what he did in 2020.
These two statements taken together, that should put to rest any of his claims.
He's telling you that if somebody is doing this, they're, quote, trying to cheat.
That's what he's saying in his statement when it comes to Dr. Oz.
Those two statements together need to be on the air constantly, reminding people the claims are baseless.
The claims are baseless.
The only person that we have audio of trying to find votes is him.
This should put to rest any questions, any lingering doubts you might have about his sincerity,
about his belief in the Republic, the Constitution, the United States in general,
the general concept of representative democracy, the idea that you should have a voice.
He doesn't. He doesn't believe in any of that.
He wants to find the votes because he's worried his candidate didn't actually win.
He wants to declare a victory ahead of time.
This needs to be remembered.
This needs to be aired.
People need to talk about this.
These two statements taken together say a whole lot.
Every accusation from this man seems like it's really a confession.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}