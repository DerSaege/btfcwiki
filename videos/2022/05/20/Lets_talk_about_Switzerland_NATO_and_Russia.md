---
title: Let's talk about Switzerland, NATO, and Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=poXvL7j5Q38) |
| Published | 2022/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic by discussing the legendary neutrality of Switzerland amidst Russia's invasion of Ukraine.
- Switzerland, known for staying neutral for two centuries, is now discussing NATO membership due to Russia's actions.
- Despite being fiercely independent and avoiding international wars, Switzerland is considering ways to cooperate with NATO without officially joining.
- The Swiss Ministry of Defense is planning to support other countries by backfilling munitions if they provide aid to Ukraine.
- Switzerland's potential involvement in joint exercises with NATO and leadership meetings signifies a significant shift from their traditional stance.
- The online debate on whether Russia's actions were justified is dismissed by Switzerland's contemplation of NATO membership.
- Despite Switzerland's constitutional commitment to neutrality, they are exploring ways to push the boundaries of their involvement in response to Russian aggression.
- Switzerland's potential move towards NATO due to Russian aggression is seen as a significant development in the international arena.
- The transcript concludes with Beau stating that there is no justification for Russia's aggression, labeling it as manufactured and offensive.
- Beau leaves the audience with his thoughts, wishing them a good day.

### Quotes

- "There is no justification for the Russian aggression, none. It doesn't exist. It's manufactured."
- "The argument over any justification for this war is over."
- "When it comes to the international poker table, the entrance of Switzerland and them just watching the game, that's a huge deal."

### Oneliner

Switzerland's potential move towards NATO due to Russian aggression challenges its legendary neutrality, signifying a significant shift in international relations with no justification for the aggression.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support countries by backfilling munitions in times of crisis (implied)
- Participate in joint exercises and leadership meetings with international partners (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Switzerland's historical neutrality and its current contemplation of NATO membership due to Russian aggression, shedding light on evolving international dynamics.

### Tags

#Switzerland #Neutrality #NATO #RussianAggression #InternationalRelations


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today, we're going to talk about the stuff of legends.
We're going to talk about something
that I don't think many people really would have considered,
something that is of mythical proportions,
for lack of a better term.
Due to Russia's invasion of Ukraine,
we have talked a lot about non-aligned countries,
and countries that were traditionally neutral, Finland,
Sweden. But we haven't talked about just the gold standard of neutrality. Switzerland.
It hasn't really come up because I mean it's Switzerland. It's not like they
would ever choose a side. And if they were to ever be in a situation where
they did choose a side, I mean that would say a whole lot about the actions
that led them to that point. We're talking about a country that didn't join
the United Nations until 2002. It's fiercely independent. It hasn't had an
international war in two centuries. It managed to stay out of World War I and
World War II. They discussed NATO membership. Because of Russia's actions,
they discussed NATO membership. If you want to know whether or not Russia's
actions were in any way acceptable, were in any way justified, that's a
pretty strong signal right there. A country that has stayed out of every war
for 200 years discussed NATO membership and currently the Ministry of Defense in
Switzerland is drawing together options and coming up with a plan on how to
cooperate with NATO but not officially join it because the neutrality is
actually enshrined in their Constitution. So they're talking about how they can
backfill munitions. So if Poland sends Ukraine 500,000 rounds of ammunition,
well Switzerland can then send Poland 500,000 rounds of ammunition. So they're
not actively participating, but they're increasing the ability of other
countries to help. They're talking about joint exercises, regular leadership
meetings between their brass and NATO brass. That's uh, I mean, that's remarkable.
That's wild when you really think about it. There is an online debate that exists over
whether or not Russia's actions were in some way justified. A country with a 200-year
commitment to neutrality is reviewing it because of Russian aggression. They're
looking for ways to come right up to the edge of what they're allowed to do by
their own Constitution. When it comes to the international poker table where
everybody's sitting around, the entrance of Switzerland and them just watching
the game and maybe loaning a player a little bit of money, that's a huge deal.
That is a huge deal.
The argument over any justification for this war is over.
There is no justification for the Russian aggression, none.
It doesn't exist.
It's manufactured.
It is so offensive that Switzerland considered joining NATO so it could participate in a
response.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}