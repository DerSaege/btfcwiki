---
title: Let's talk about why the gun control debate is about to change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dBuFeSz9AnI) |
| Published | 2022/05/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the changing debate in the United States around firearms and the upcoming shift in the narrative due to recent developments.
- Believes that prohibition, whether in family planning, substances, or guns, does not address root issues causing problems.
- Points out that the gun culture in the United States has turned firearms into status symbols rather than tools.
- Explains the misconceptions about the AR-15 rifle, clarifying that it is not a high-powered weapon but rather low intermediate power.
- Talks about the new rifle, XM5, adopted by the Army, which is more powerful than the AR-15 and could potentially change the gun debate significantly.
- Mentions the impact of the new rifle on both the gun control side and the gun crowd, as well as the need for a cultural shift in how firearms are viewed.
- Emphasizes the need for responsible gun ownership and understanding that firearms are tools, not symbols of masculinity or power.

### Quotes

- "Guns are not status symbols. Guns are not toys. They're not things to make you look tough. They're tools."
- "This is going to be a game-changer in this debate."
- "The high capacity magazine talking point. You don't need 30 rounds."

### Oneliner

Beau explains why the debate on firearms in the United States is about to change, focusing on the shift from viewing guns as status symbols to tools and the impact of the new XM5 rifle.

### Audience

Gun owners, gun control advocates

### On-the-ground actions from transcript

- Understand and advocate for responsible gun ownership (implied)
- Educate others on the difference between viewing guns as tools versus status symbols (implied)
- Advocate for cultural shifts surrounding firearms in the United States (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the changing dynamics in the firearms debate and the importance of responsible gun ownership and cultural shifts in how firearms are perceived.

### Tags

#Firearms #GunDebate #Responsibility #CulturalShift #UnitedStates


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about why
an entire debate in the United States is about to change.
And it's not because of what happened this weekend.
Before we get into it, I wanna point out a couple of things.
First, is that no matter where you stand on this issue,
you're going to hear things that you don't like in this video
before we get to the important part,
because there's some background information that you need.
And then the other thing is that, generally,
when somebody talks about something like this,
people want to know where they stand on a particular issue.
I, to sum it up, don't believe that prohibition really works.
And that goes for family planning, it goes for substances, it goes for guns.
I don't think it actually addresses the root issues that cause the problem.
At the same time, I believe the problem is 100% the fault of the gun crowd because of
the culture they have fostered around firearms in the United States where they no longer
view a rifle as a tool, but view it as a status symbol or a symbol of masculinity.
Okay, so I haven't talked about this for a long time, so I'm going to do a recap
of some previous videos. A lot of this I had pushback on from various sides.
Everything I'm about to say, there are detailed videos going over each and
point. The number one thing I get pushback on from the gun control side is
when I say the AR-15 is not a high-powered rifle because the media
constantly says that it is. The AR-15 is not a high-powered rifle. It is low
intermediate power and this is important to understand. It's not high-powered. The
AR, it's not particularly high-tech. It's a really basic rifle. It's not great. It's
not some super weapon. So, well then why is it, you know, always brought up? Why is it
used? You know, so much. Because of the culture that the gun crowd has fostered. Because it's
a status symbol. Because it's a symbol of masculinity. Because they want to stand there
hold the rifle in their social media photo and say I got the same gun the
army has, at least one that looks like it. It's the most popular rifle in the
United States. Because of that more people have access to it so it gets used
more. The way the gun crowd looks at firearms and the rhetoric that
surrounds it is why it's around so much and it centers on the fact that it's the
gun the Army has. Not anymore. The new information, the part you need, the part
that's going to change this debate is that the Army has decided to pick up a
new rifle. It's currently called the XM5. When it's
fielded, it will just be called the M5. It is not low intermediate power. A quick
crash course in this for the, well it's actually for people on both sides
because there's a whole lot of people in the gun crowd who don't know anything
about guns, but the AR-15 round 5.56 by 45 millimeter. The new round is 6.8 by 51 millimeter.
So those are the dimensions of the bullet, the part that actually comes out of the barrel.
But just because the new round is bigger doesn't necessarily mean it's more powerful. You also
have to factor in the mass and how fast it's going, right? So the AR round is
about 60 grains. The new round is 140. They're both traveling around 3,000 feet
per second. The AR round is a wee bit faster, like 3110 I think, which means
the AR round produces 1300, roughly, foot-pounds of energy. 1325. The new round
is about double that. 2700 foot-pounds of energy. If you've watched the older
videos, there's a part where I'm like, please don't ban the AR-15. The reason
it's used is because it's the most popular. If you were to get rid of that
and like weapons, the most popular rifles at that point would be the M14 and the
Garand, and I described that as a nightmare.
The new rifle is more powerful than the M14 and just a wee bit less powerful
than the.30-06. It has a little bit less energy. For those in the gun crowd who
don't know a whole lot about ammunition, this is more powerful than the round
that comes out of the M60, the gun that Rambo carried with the belt hanging out
the side. Okay, so why is this going to change the debate? Why do people have the
AR? Because it's a status symbol. Because they want the same rifle the Army has.
Which means very soon they won't be buying ARs. They'll be buying the
civilian version of the XM5, which is a whole lot more powerful. Now, for the gun
control side, a lot of your talking points, they don't matter anymore because
for a long time, the idea has been to just demonize the AR-15 and then use
that as like the gateway to enact legislation. Well, it won't be long
before that's not what's being used, so you're gonna have to start that whole
process all over again. The high capacity magazine talking point. You don't need 30
rounds. The new one isn't going to have 30 rounds. I have a video on why this is a bad
talking point. I use a toy AR that is functional to show you how quickly you can change magazines.
The Army understands this. The XM5, I want to say it's 20 rounds, which in a lot of states
that actually have gun control, that's going to be legal.
Now for the gun crowd, why does this matter to you?
Because the first time somebody walks into a building with one of these, that whole conversation
is going to change because that footage is going to look a whole lot different.
You are going to have a whole lot more support for gun control.
The gun crowd has a very limited amount of time to alter the entire culture surrounding
firearms in this country.
Because this will shift the conversation.
This is going to make things look very, very differently.
So when can you expect this development to happen?
While the Army won't be able to get their first units filled with this weapon until
late in 2024, civilians can get theirs now.
A first production limited run of the Spear, which is the civilian name for it, is already
on the market.
The rifles are about $8,000.
That's expensive.
You're not going to have a whole lot of people pick those up.
However, now that the Army has agreed to this contract, the more of them that are produced,
the less expensive they're going to get.
The one that's on the market now comes with a suppressor, which means there's a lot of
background checks with it.
As time goes on, they'll probably offer one with a 16-inch barrel that doesn't have a
suppressor that you could buy in just about any gun store. This is going to
change things. The thing is it's going to cause something bad is going to have to
happen first unless people understand what's coming and act on it now. Guns are
not status symbols. Guns are not toys. They're not things to make you look
tough. They're tools. And if the gun crowd had remembered that and just treated
them that way, none of this would be happening. None of this would be
occurring. But because of the rhetoric that exists in the gun crowd, because of
the desire among a whole lot of people in the gun crowd to go down the might
makes right road and instill that in the kids, you have created the opposition.
You have created the opposition to gun ownership in this country because you're
You're so interested in owning the libs that you have turned it into a common thing to
threaten violence.
You have turned it into a common thing for people who are not mature enough to handle
firearms to have access to them.
This is going to be a game-changer in this debate.
As the gun crowd starts buying this more than they buy the AR-15, it's going to shift the
dynamics.
It's going to get bad.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}