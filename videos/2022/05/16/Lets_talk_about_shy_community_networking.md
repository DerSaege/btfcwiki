---
title: Let's talk about shy community networking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JPPoImeUKag) |
| Published | 2022/05/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the topic of shy community networks and the challenges of real-world interpersonal interactions.
- Recalls a personal story from the late 1900s about a shy roommate in a community network.
- Describes how the shy roommate, skilled in computers, helped with graphic design and creating a website for activist groups.
- Explains how the group of online computer-savvy individuals made a positive impact by working together virtually.
- Mentions how a therapist suggested they move their activities into the real world for environmental cleanup efforts.
- Emphasizes the importance of finding a liaison or connection to be part of something bigger, even if you are shy.
- Concludes by encouraging everyone to overcome reasons not to participate, as there is a need for every skill set to make the world better.

### Quotes

- "There is no skill that is unneeded in the fight to make the world better."
- "You just have to find the right little cubby for you, the right group for you, the right way for you to help."
- "No matter how hard it may seem or how challenging it might be in the beginning, there's a spot for you."

### Oneliner

Be part of something bigger by finding your niche in community networks, even if you're shy or prefer online interactions.

### Audience

Community members

### On-the-ground actions from transcript

- Find a niche within a community network (implied)
- Offer your skills to help a cause (implied)
- Participate in virtual or real-world activities based on your comfort level (implied)

### Whats missing in summary

The full transcript provides a detailed narrative of how individuals with different personalities and skill sets can contribute to community networks, showcasing the importance of finding a place where one feels comfortable and valued in making a positive impact.

### Tags

#CommunityNetworks #OnlineInteraction #SkillContribution #CommunityEngagement #Shyness


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we have another installment
talking about community networks.
This one is about shy community networks,
or being in a community network when you're shy,
or when you have some other reason that you don't want
to have real-world interpersonal interaction.
This is something that comes up way more than you might think.
Prompted by this comment, my problem
is that I'm not social enough, and I shy away
from making real-life connections.
It's a common thing.
The thing is, I encountered this in the first community network
that I ever witnessed.
In fact, it was so long ago, I didn't even
know that's what it was called.
I want to reiterate, this was a long time ago.
So as I go through this story, just
understand that this is what my friend's kid would
refer to as the late 1900s.
Yeah.
My neighbor, she was the just stereotype
of the activist social butterfly.
She was plugged into everything on campus, everything
that was going on in town.
She knew everybody.
And we hit it off for whatever reason.
At this point in my life, I was not
plugged into that scene at all.
But we hung out a lot.
And one day, I'm sitting in her apartment,
as I had for the previous months, just hanging out.
She is in her bedroom.
And this woman walks in.
And she walks right past me, doesn't acknowledge me,
and opens the door to what I thought was a closet.
It wasn't.
It was her room.
She had a roommate.
The activist butterfly had this roommate
that was very much somebody who kept to herself, so much so
that after months, I didn't even know she existed.
She was what we would call today a very online person.
She was going to school for something
to do with computers.
I don't know.
It was a long time ago.
But as time went by, her social butterfly activist roommate
was complaining about getting positive messaging out
on campus and how hard it was.
So she helped.
She used the skills that she had to help.
And then it grew.
And other people who were going to school for stuff
to do with computers, they started handling
the graphic design on the flyers.
And they made a website that each little activist group
could log into and update like a calendar of events.
Understand, that was some high tech stuff back then.
And they did this for months and months and months.
And this little circle of very online computer people,
they had meetings.
But they were in a chat room.
I want to say it was IRC, which is it was discord.
And as time progressed, one of their therapists
suggested that they actually take this activity
and get out and get involved and get into the real world.
The therapist was literally like touch grass.
And they started going out and doing
environmental cleanup stuff and stuff
like that that didn't require a lot of interaction
outside of the circle of people they
were very comfortable with.
I went with them once.
And it was a very unique experience.
They referred to each other by their screen names.
It was unique.
But it worked for them.
And it goes to show that even if you are somebody who just does
not want to deal with people, or you have a reason
you can't deal with people, or whatever, all you need
is that one person who you can kind of use as a liaison.
And you can still be part of something or spawn your own.
I mean, by the time this was over,
that little circle of computer friends
was basically the PR firm for every activist group in town.
They made a positive change.
And while this story definitely speaks
to those people who may be shy or may not
want to have those real world interactions,
it also shows something else.
There is no skill that is unneeded in the fight
to make the world better.
No matter what your skill set is,
it doesn't matter what your situation,
there's some way that you can plug into a network and help.
And if you're somebody who doesn't
want to leave your house, you don't have to,
unless the therapist tells you to.
But one of the things you have to overcome
when you're doing this is all of the reasons
you have not to, because you're going to have tons.
You're going to have stuff like this, where it just
doesn't suit your personality, or so you think.
Or you're going to have scheduling conflicts.
You're going to have, it's going to be hard to find the time.
You're not going to like one person involved.
There's always going to be a reason to quit.
And we have to find a way to overcome it.
Now, as far as their real world activities,
I have no idea how that wound up playing out,
because that happened like two months before I moved.
But there's always a need for you, for whatever
it is that you're good at.
Whatever your skill set is, there's a need for it.
The world has a lot of problems.
And there is some way in which you can contribute.
And there is plenty of accommodation
for your personality type, your skill set,
your personal preferences, all of that stuff.
There's a way to help.
You just have to find the right little cubby for you,
the right group for you, the right way for you to help
and to maintain who you are, because you can't,
as much as everybody wants to, you can't actually
let that define you as a person.
It has to be in addition to your normal activities.
So no matter how hard it may seem
or how challenging it might be in the beginning,
there's a spot for you.
And there are people waiting for you to fill that spot.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}