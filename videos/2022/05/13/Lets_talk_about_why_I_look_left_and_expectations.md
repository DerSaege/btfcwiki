---
title: Let's talk about why I look left and expectations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f6-H0UyoGPg) |
| Published | 2022/05/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he started looking left politically: anti-authoritarianism and a desire to prevent suffering.
- Rejects the idea of accelerationism, which aims for suffering to push political awakening.
- Expresses disappointment in lower views on community networking videos, but values potential impact on even a small percentage of viewers.
- Believes in the importance of building a better world, even though it will be challenging.
- Emphasizes that changing the world is difficult but necessary to prevent suffering and create positive change.

### Quotes

- "I started looking left because I was always anti-authoritarian and then I got really tired of watching people suffer and there's nothing on the right to combat that."
- "I cannot get behind that, period."
- "Not every video is for every person."
- "It's worth doing because it's hard. It's worth doing because it's the right thing to do."
- "Encourage the suffering to get to the point where they learn their lesson. I will never sign off on that."

### Oneliner

Beau explains his anti-authoritarian stance, rejects accelerationism, values impact over views, and stresses the difficulty but importance of building a better world.

### Audience

Activists, community builders, supporters

### On-the-ground actions from transcript

- Start a community network (exemplified)
- Engage in building a better world through actions that prevent suffering and create positive change (suggested)

### Whats missing in summary

The emotional depth and personal conviction Beau brings to his rejection of accelerationism and commitment to preventing suffering and creating positive change can best be experienced by watching the full transcript.

### Tags

#CommunityBuilding #Anti-Authoritarianism #PreventingSuffering #PositiveChange #PoliticalActivism


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about why I look left
and we're going to talk about how things are hard and measuring success and managing expectations
and understanding what you can do in the short term and the long term. We're going to do this because I
got a message that I think a lot of people would benefit from hearing.
I'm a big fan of yours, but I'm hoping you see the error in your strategy. I've noticed your videos on
community networking have substantially less views than your other videos. I'm sure you've noticed
that too. I hope that watching what is easily the most important videos on your channel get less
views shows you why it's pointless to talk to centrists or liberals. You're putting this effort
into putting out solid advice and one percent of your subscribers will do something with it.
This is why the real solution is to let the Democrats lose so they learn their lesson and
either they fix it or they don't and all of those people who should have woken up sooner
will have no choice but to move left and actually get in the fight.
There's a lot to that. Last part first. The idea here is to let Democrats lose, let Republicans just
run full bore into just this grand authoritarian theocratic type of government, and that those
people who should have woken up sooner, well, they will suffer so much that they're going to
have to fight back out of self-defense. That's the idea. It's accelerationist.
Here's the thing. I didn't start looking left because I wanted an identity. I didn't start
I didn't start looking left because I read a book that just spoke to me on some deep level.
I didn't start looking left because I heard an impassioned speech.
I started looking left because I was always anti-authoritarian and then I got really tired
of watching people suffer and there's nothing on the right to combat that.
That's why I started looking left.
Given the fact that my whole reason for looking to the left is not wanting people to suffer,
that there's no way I can sign on to a plan that requires large amounts of people to suffer,
particularly in this situation where the people who under this plan would be asked to endure this
so they learn their lesson have historically suffered the most in this country.
I cannot get behind that, period.
There's no reason to send me any messages encouraging accelerationism.
It's not going to be well received. I'm not a receptive audience for that.
It literally goes against everything that brought me to this point.
Now, as far as the views on the community networking stuff, yeah, they're lower.
Okay.
1%.
Huh?
1% might do something with it.
I'll be honest, if 1% of my total subscriber base across all the platforms,
if they did something with it, if they started a community network,
I would consider myself the most successful person on YouTube in history.
That's 10,000 community networks being set up.
That's more than I could ever hope for.
1 tenth of 1% is still amazing.
That's a thousand.
And even at 1% of 1%, that's still two networks in every state.
That's worth my time.
Not every video is for every person.
There are some people who legitimately can't get involved in that way for whatever reason.
And the goal here is to build a better world.
I've got more than 2,000 videos.
Go back and find any in which I say it's going to be easy.
It's not.
It's going to be hard.
But it's worth doing because it's hard.
It's worth doing because it's the right thing to do.
It's worth doing because it will stop people from suffering.
Yeah, it's going to be hard.
I don't know what you're expecting.
You're literally talking about changing the world.
It's not going to be easy.
It's going to be a fight.
And I don't think that we should wait until people just can't take it anymore.
And encourage the suffering to get to the point where they learn their lesson.
I will never sign off on that.
The other thing is, I don't know that that's what would happen.
Have you ever considered that the actual outcome of this plan would be that things get so bad
that everybody just falls in line out of self-preservation?
Because that's actually historically what happens under totalitarian regimes.
This strategy that you're proposing, it probably would help the other side.
It would get so bad and people would be forced to choose between a red hat and a hole.
Self-preservation is a very strong motivator.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}