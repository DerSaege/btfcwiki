---
title: Let's talk about coffee shops and labor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7tDZEgnNq9A) |
| Published | 2022/05/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The National Labor Relations Board (NLRB) filed a complaint against Starbucks in Buffalo, alleging 29 unfair business practices and 200 violations of the National Labor Relations Act.
- Starbucks was accused of trying to prevent unions from forming and putting pressure on them against the law.
- In Memphis, the NLRB filed a petition to reinstate seven employees due to Starbucks' conduct interfering with their federally protected rights.
- Kathleen McKinney from the NLRB emphasized the need for immediate relief to prevent harm to the campaign in Memphis and to ensure all Starbucks workers can freely exercise their labor rights.
- The power of unions lies in collective action, as demonstrated by big businesses' efforts to prevent their formation and the resources they invest in doing so.
- Companies go to great lengths, including hiring consultants, conducting surveillance, and holding mandatory meetings, to combat unionization because they recognize the threat unions pose to their control.
- The significant monetary investment made by companies to impede unions showcases the effectiveness and importance of collective action in improving working conditions.
- Unions represent power and protection for workers, leading to better conditions that companies wouldn't be concerned about if unions were ineffective or insignificant.
- The collective power of unions is substantial, with the ability to negotiate for better wages, benefits, and working conditions through unified action.
- The resistance and tactics employed by companies against unionization underscore the threat unions pose to the status quo and the transformative potential of collective organizing.

### Quotes

- "There is power in numbers. There's safety in numbers."
- "It represents power. Collective action will often get the goods."

### Oneliner

Starbucks faces allegations of unfair business practices and violations as unions fight for workers' rights – the power of collective action against corporate resistance.

### Audience

Workers, activists, advocates

### On-the-ground actions from transcript

- Support unionization efforts in your workplace to protect workers' rights and improve working conditions (exemplified)
- Educate fellow employees on the benefits and importance of collective action through union representation (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges faced by workers in unionizing efforts and the significance of collective action in holding corporations accountable and improving labor conditions.


## Transcript
Well, howdy there, Internet of People. It's Beau again. So today we're going to talk
about coffee, coffee, coffee. Well, we're going to talk about Starbucks and unions and
how the National Labor Relations Board is getting pretty active. Now up in Buffalo,
the NLRB put out a complaint that alleged 29 unfair business practices, including 200
violations of the National Labor Relations Act. Now the general tone of this was that
Starbucks was trying to stop the unions from forming and putting pressure on them in violation
of the act. Again, at this point, it's complaints, allegations. In Memphis, the National Labor
Relations Board went ahead and filed a petition in district court to have seven employees
reinstated. The regional director of the NLRB, Kathleen McKinney, said,
Given Starbucks' egregious conduct interfering with the federally protected rights of its
employees, we are asking the court to swiftly grant the injunction. Without immediate interim
relief from this court, Starbucks could irreparably harm the campaign in Memphis and send a chilling
message to its employees across the country that they too will suffer the same fate as
the terminated Memphis employees if they dare to exercise their right to engage in the protected
activities. It is crucial that these seven employees be reinstated and that Starbucks
cease its unlawful conduct immediately so that all Starbucks workers can fully and freely
exercise their labor rights. Again, petition, court hadn't decided on it as of time of filming.
So one of the questions that always comes up anytime the subject of unions comes up
is are they really worth it? Is it worth coming together and organizing and trying to get
a union in your shop, wherever you are? The best answer to that is to look at how much
big business tries to stop them. There is power in numbers. There's safety in numbers.
You've heard it since you were a kid. The collective power that is exercised by an organization,
by a union, something like that, is immense. So much so that large companies spend untold
amounts of money in an attempt to make sure that a union doesn't get into their place
because that union is there to protect the workers. When you really start looking at
it, I mean a lot of firms, and I don't know if Starbucks did any of this, a lot of firms
will hire outside consultants, security consultants. They're not cheap. They'll conduct surveillance.
They'll bring in big managers. They'll hold mandatory meetings. All kinds of stuff. And
all of this costs money. If there wasn't power in the union, if it was irrelevant, if it
didn't lead to better conditions, the companies wouldn't care. If it really was just some
scam to take a couple dollars a month out of each employee's pocket and put it into
somebody else's, the company wouldn't care. It represents power. Collective action will
often get the goods. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}