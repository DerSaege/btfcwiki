---
title: Let's talk about a cliff analogy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hAWppTCyOB4) |
| Published | 2022/05/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a hypothetical scenario to counter arguments against family planning.
- Advocates for focusing the debate on bodily autonomy rather than false equivalencies.
- Describes a scenario involving a fertilized egg and a baby named Billy hanging over a cliff to illustrate the difference.
- Emphasizes that the argument of viability is key in discussing moral questions surrounding abortion.
- Advises always redirecting the debate back to bodily autonomy.
- Dismisses debates over when life begins as not scientific and irrelevant compared to viability.
- Stresses the importance of understanding moral questions change at the point of viability.

### Quotes

- "Can you force a person to do something with their own body against their will?"
- "The real question is about bodily autonomy."
- "Don't get into debates over when it starts, because it doesn't matter."
- "The question is, when is it viable on its own?"
- "There isn't really the moral question that they're trying to present."

### Oneliner

Beau presents a scenario to counter false equivalencies against family planning, urging a focus on bodily autonomy and viability rather than arbitrary debates on when life begins.

### Audience

Advocates for reproductive rights.

### On-the-ground actions from transcript

- Redirect debates on family planning back to bodily autonomy (suggested).
- Focus on the question of viability in the abortion debate (suggested).

### Whats missing in summary

The full transcript provides a detailed and effective strategy to counter false equivalencies against family planning by shifting the focus to bodily autonomy and viability rather than when life begins.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a scenario,
a hypothetical scenario that you can present to people
if you encounter a very specific talking point.
A couple of things I want to point out before we get into this
is that, one, this only works up to a certain point.
And two, I didn't come up with this.
This is a scenario that I have used with 100% effectiveness.
Anytime I have used this, I got the desired result.
But I didn't come up with it.
I can't remember who I stole it from,
but I just modified it a little bit.
And so if I took it from you, I'm sorry.
OK, so what am I talking about?
There are a lot of people who are against family planning,
who try to establish a false equivalency
and say the reason that they're against it
is because that's taking out a baby.
No, it's not.
It's not.
It isn't the same thing.
And it's a desire to establish that false equivalency
with something that just is very different than the reality.
And that's why they want to use it.
Now, the best strategy, in my opinion,
is to avoid it because it's a red herring.
It would be to bring the discussion back
to bodily autonomy, which is actually the question at hand.
So that's where I would try to go with it.
But sometimes, they don't let that part of it go.
So you have to destroy the false equivalency.
So this is what I have used.
And it's literally always worked.
So I'm going to present this in the exact way I do.
So start off.
And like I say, you're saying that that's a baby at three
weeks or six weeks.
Yes.
OK.
So I'm going to present you with a scenario.
Don't answer until I ask you a direct question
because this isn't going where you think it is.
I just kind of want to explore something real quick.
All right.
In my left hand, I have a fertilized egg slated
to be implanted tomorrow.
And in my right hand, I have a baby named Billy.
Don't answer because this isn't going where you think.
They're both hanging over the edge of a cliff.
My question is, what kind of ice cream
do you think Billy's going to like when he grows up?
Because we both know which one you're going to tell me to drop
because they're not the same.
You start there.
But understand there's limitations to that
because that argument only works up to the point of viability.
Once it's viable on its own, then
whether you like it or not, there's
a whole slew of legitimate moral questions that arise.
And in my opinion, those are moral questions
to be decided by the individual.
But understand there are going to be
a whole lot of people who do not take that stance.
There's a desire to create that false equivalency
so they can use images of cute little white babies
with blonde hair and blue eyes and full sets of teeth.
And it alters the perception of it.
That's why they want to use it.
But it is a red herring.
The real question is about bodily autonomy.
Can you force a person to do something with their own body
against their will?
And that's why when it comes up to the point of viability
that all the math changes.
Because at that point, there are moral questions
that have to be decided.
But up until then, there's not.
There really isn't.
So that's how I have always responded to that.
But the important part is to take it back
to bodily autonomy, even if you have
to destroy this argument in the process.
Go back to bodily autonomy as soon as you're done.
Don't get into debates over when it starts,
because it doesn't matter.
Those are not based in science.
Those aren't really based in anything other than a feeling.
The question is, when is it viable on its own?
And at that point, up until then,
there isn't really the moral question
that they're trying to present.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}