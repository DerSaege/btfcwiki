# All videos from May, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-05-31: Let's talk about the EU quitting Russian oil.... (<a href="https://youtube.com/watch?v=p0G4KTJCj7E">watch</a> || <a href="/videos/2022/05/31/Lets_talk_about_the_EU_quitting_Russian_oil">transcript &amp; editable summary</a>)

The EU implements stringent sanctions on Russia, including a 90% cut in oil imports, aiming to impact its economy while considering global repercussions and Ukraine's food security.

</summary>

"You have to weigh the increased costs or the driving less against that fact."
"It may save lives."
"The food security aspects are very pressing."
"This will probably drive prices up even further."
"It's just something to keep in mind the next time you go to the gas pumps."

### AI summary (High error rate! Edit errors on video page)

The EU is preparing to implement the sixth round of sanctions against Russia, which includes a significant cut in Russian oil imports by 90% by the end of the year.
This move will involve pushing nearly $10 billion US into the Ukrainian economy to provide support.
The sanctions also involve cutting Russia's largest bank out of SWIFT and blocking the distribution of content from Russia's major state-owned media companies in the EU.
Along with targeted sanctions on individuals, assets freezing, and travel restrictions, this round of sanctions aims to impact Russia's economy.
The reduction in Russian oil imports is a key demand from Zelensky to weaken Russia's economic stability.
The global impact of these sanctions is not isolated, as it will lead to shifts in oil trading globally.
There are speculations that Russia might sell its excess oil at a reduced rate to new buyers, potentially impacting gas prices worldwide.
Despite such speculations, any market instability or change often results in increased prices.
EU countries are collaborating to facilitate Ukraine's grain export, a critical aspect for Ukraine's food security and global trade.
Balancing the potential rise in gas prices due to sanctions with the effort to stop the war in Ukraine is a critical consideration for the global community.

Actions:

for global citizens,
Support initiatives that aid Ukraine's economy and food security (suggested)
Stay informed about the global impact of sanctions and trade shifts (suggested)
</details>
<details>
<summary>
2022-05-31: Let's talk about if the ban worked.... (<a href="https://youtube.com/watch?v=5ZoYEiui7iM">watch</a> || <a href="/videos/2022/05/31/Lets_talk_about_if_the_ban_worked">transcript &amp; editable summary</a>)

Beau questions the effectiveness of the assault weapons ban, pointing out that it failed to remove firearms from the streets and only regulated cosmetic features, leading to continued production and sales of the same weapons.

</summary>

"How did it not work?"
"The assault weapons ban didn't work."
"No matter what metric you're using, it didn't work."
"Your statistics are all based on correlation."
"It didn't actually stop the type of weapon that people think it stopped from going onto the street."

### AI summary (High error rate! Edit errors on video page)

Beau questions the effectiveness of the assault weapons ban and why there are conflicting opinions on its success.
The assault weapons ban did not remove any firearms off the streets; weapons like ARs and AKs continued to be sold during and after the ban.
The ban focused on cosmetic features of firearms rather than reducing their actual firepower, leading to the same weapons being sold but with minor alterations.
There was a massive surge in production of rifles, especially ARs, after the ban, with spikes in production coinciding with political events like elections.
Beau speculates that fear-mongering, social media influence, and a toxic gun culture may contribute to the increasing frequency of incidents involving firearms.
Despite differing views on gun control, one point of agreement among pro-gun, anti-gun, and firearm experts is that the assault weapons ban failed to achieve its intended goals.

Actions:

for advocates for gun control,
Educate communities on the nuances of firearms legislation and the limitations of cosmetic-based regulations (exemplified)
Initiate open dialogues and debates within communities about effective gun control measures (exemplified)
Advocate for comprehensive gun control policies that address the actual firepower of firearms rather than cosmetic features (implied)
</details>
<details>
<summary>
2022-05-31: Let's talk about Georgia showing Wyoming the way.... (<a href="https://youtube.com/watch?v=sk3RgpMHmlU">watch</a> || <a href="/videos/2022/05/31/Lets_talk_about_Georgia_showing_Wyoming_the_way">transcript &amp; editable summary</a>)

Beau explains a political strategy incident in Georgia with implications for defeating Trumpism and selecting opposition candidates.

</summary>

"If defeating Trumpism is important to you and everything that goes along with it, and you live in Wyoming, it's worth noting..."
"Defeating Trump and his crowd in Wyoming, that's it for the worth of his endorsements. That's it."
"If you're concerned about the authoritarian slide this country has been on, it's just worth remembering that this is something that happens."
"Selecting your opposition might be a way to look at it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talks about a political strategy incident in Georgia with potential implications for the Democratic Party in Wyoming.
Points out the crossover voting from the Democratic primary to the Republican primary in Georgia.
Mentions how Raffensperger needed the help and avoided a runoff by 27,000 votes.
Notes that 37,000 people who voted in the Democratic primary previously voted in the Republican primary this time.
Connects Raffensperger's victory to his refusal to help Trump overturn the election.
Emphasizes the impact of defeating Trumpism and the importance of participating in selecting representatives.
Mentions the significance of Trump's endorsements and how defeating Trump in Wyoming could diminish his influence.
Encourages considering the impact of selecting opposition candidates in the context of the country's authoritarian slide.
Wraps up by urging listeners to think about the implications of these political dynamics.

Actions:

for wyoming residents,
Change your voter registration to participate in selecting representatives (suggested)
Engage in voting strategically to have an impact on political outcomes (implied)
</details>
<details>
<summary>
2022-05-30: Let's talk about a comprehensive gun control strategy.... (<a href="https://youtube.com/watch?v=SwOVUt7myLU">watch</a> || <a href="/videos/2022/05/30/Lets_talk_about_a_comprehensive_gun_control_strategy">transcript &amp; editable summary</a>)

Beau proposes a comprehensive plan to address mass incidents in the US, focusing on age-based restrictions, cultural shifts, and social support.

</summary>

"Start there. Think about the way certain products can't be advertised to kids. Eliminate that tie."
"Come out for six weeks, or eight weeks, or whatever. Do you want to be a warrior, or a wannabe?"
"This might be the way to go."

### AI summary (High error rate! Edit errors on video page)

Proposes a comprehensive plan to address mass incidents in the United States by setting aside ideological hangups.
Suggests raising the age to 21 for firearm access and assigning criminal liability for unsupervised access to firearms under 21.
Advocates for closing all domestic violence loopholes, starting with the boyfriend loophole.
Recommends temporary prohibitions for any violent conviction and permanent prohibition for hurting animals.
Emphasizes behavior or age-based solutions that have traditionally been upheld by the Supreme Court.
Calls for triggering a cultural shift within the gun crowd by banning military-tied advertisements and reaching out to Hollywood.
Proposes a campaign against misogyny and racism to address cultural factors linked to mass incidents.
Stresses the importance of better social safety nets and Medicare for All with mental health care to relieve economic stressors and provide necessary support.
Advocates for a multi-pronged approach that limits access for individuals displaying concerning behaviors without infringing on Second Amendment rights.

Actions:

for legislators, gun advocates,
Contact legislators to advocate for raising the age for firearm access to 21 (suggested).
Join or support campaigns against misogyny and racism in schools and communities (suggested).
Organize or participate in initiatives to improve social safety nets and access to mental health care (suggested).
</details>
<details>
<summary>
2022-05-30: Let's talk about Starbucks, unions, and you.... (<a href="https://youtube.com/watch?v=Ht0PULmvL9I">watch</a> || <a href="/videos/2022/05/30/Lets_talk_about_Starbucks_unions_and_you">transcript &amp; editable summary</a>)

Unions are not just for blue-collar workers; they empower all workers in hostile environments, providing representation and collective bargaining power to improve working conditions and rights.

</summary>

"Unions aren't just for blue-collar workers. Unions are for any worker where the boss is not really looking out for them."
"Divided we beg, united we bargain."
"If you're in a workplace and you're not treated well, a union might be the thing for you."
"If unions weren't effective, if they didn't work, big business wouldn't spend so much money trying to defeat them."
"There are networks that can help you do it."

### AI summary (High error rate! Edit errors on video page)

Unions are traditionally associated with blue-collar jobs like coal miners and electricians, but the service industry is seeing a rise in unionization.
There's a misconception that unions only thrive in progressive areas, but recent events like Starbucks in Birmingham, Alabama, voting to unionize, challenge this idea.
Unions are not limited to blue-collar workers; they are for any worker who needs representation in a hostile environment.
The process of unionizing is not as difficult as it may seem, despite pressure from businesses against it.
The National Labor Relations Board outlines the process for forming a union, including holding elections if 30% of workers express interest in unionizing.
Businesses often resist unions, but collective bargaining through unions gives workers more power and representation.
Unions are effective, as evidenced by the significant efforts businesses put into defeating them.
If you're in a workplace where you're not treated well and lack necessary benefits, a union might be the solution.
There are networks available to help workers navigate the unionization process, making it more accessible than perceived.
Considering joining a union can empower workers and improve their working conditions.

Actions:

for workers seeking empowerment,
Contact networks that assist with unionizing (suggested)
Join a union or start the process of forming one (implied)
</details>
<details>
<summary>
2022-05-30: Let's talk about Russia advancing.... (<a href="https://youtube.com/watch?v=g23_i5RLJSA">watch</a> || <a href="/videos/2022/05/30/Lets_talk_about_Russia_advancing">transcript &amp; editable summary</a>)

Beau provides an update on the situation in Ukraine, discussing Russia's gains, Ukraine's strategy, and the importance of perseverance in the conflict.

</summary>

"They don't have to win. They just have to keep fighting."
"Russia appears to have regained the initiative."
"It really boils down to whether or not Ukraine can keep fighting."

### AI summary (High error rate! Edit errors on video page)

Providing an update on the situation in Ukraine, mentioning Russian forces making modest gains.
Posing the question of whether these gains are real or if Ukraine is intentionally stretching Russian lines.
Expressing uncertainty about Ukraine's strategy and Russia's pace due to logistical reasons.
Noting Russia's shortage of manpower, leading them to lift the age limit for military service to 50 years old.
Addressing rumors of Russia feeling confident and aiming to take Ukraine's capital by fall.
Mentioning Russia's reliance on Western support drying up and the skepticism towards this idea.
Stating that Russia seems to have regained the initiative in the conflict.
Speculating on Ukraine's defense strategy and the mixed news from inside Russia regarding capabilities and intent.
Emphasizing Russia's commitment to a prolonged conflict in Ukraine.
Stating that Ukraine's goal is not necessarily to win but to keep fighting to break Russian resolve.

Actions:

for global citizens, policymakers.,
Support Ukraine with humanitarian aid and resources (suggested).
Advocate for continued Western support for Ukraine (implied).
</details>
<details>
<summary>
2022-05-29: Let's talk about desks, creative solutions, and realism.... (<a href="https://youtube.com/watch?v=rtgGht7YDcc">watch</a> || <a href="/videos/2022/05/29/Lets_talk_about_desks_creative_solutions_and_realism">transcript &amp; editable summary</a>)

Beau contemplates a new idea of desks as bulletproof shields for kids, stressing the importance of creative yet feasible solutions for school safety.

</summary>

"We need creative ideas. We need to think in ways that can actually be enacted right now."
"I refuse to believe that the best we can come up with is making sure that we turn every child into Ragnar Lodbrok just so they can survive elementary school."

### AI summary (High error rate! Edit errors on video page)

Reacts to a new idea of desks doubling as bulletproof shields for kids in schools.
Contemplates the practicality and feasibility of the idea.
Describes the potential materials and features of the detachable bulletproof shield desks.
Acknowledges the need for creative solutions to protect children in schools.
Expresses that this idea might be the best one he's heard that could actually be implemented.
Quotes an activist's principle of being a realist and an idealist.
Recognizes the current political reality regarding gun control measures.
Emphasizes the necessity for actionable and creative ideas to address school safety.
Rejects the notion of turning every child into a warrior for their own protection.
Calls for more innovative solutions to safeguard children.

Actions:

for activists, educators, parents,
Develop and propose practical, creative solutions for school safety (implied)
Advocate for innovative approaches to protect children in schools (implied)
</details>
<details>
<summary>
2022-05-29: Let's talk about a criticism from a cop about physics and the best men.... (<a href="https://youtube.com/watch?v=bYM6e87fWBw">watch</a> || <a href="/videos/2022/05/29/Lets_talk_about_a_criticism_from_a_cop_about_physics_and_the_best_men">transcript &amp; editable summary</a>)

Beau corrects misconceptions about countering a rifle with a pistol, citing real-life incidents and stressing the importance of commitment over gear in law enforcement.

</summary>

"Countering an AR with a pistol can be done, and it's done all the time."
"It's not the equipment. They've proven that over and over again."
"It's just physics. I don't even know what that means."

### AI summary (High error rate! Edit errors on video page)

Corrects criticism of law enforcement regarding countering a rifle with a pistol.
Shares a recent incident where a woman used a pistol to stop a shooter with an AR at a graduation party.
Points out that shooters often have a domestic violence connection.
Emphasizes that countering an AR with a pistol is possible and done frequently.
Argues that it's not about gear but about the warrior mentality and commitment.
States that the U.S. military could issue gear to everyone, but it's the commitment that matters.
Explains that what makes elite individuals special is their ability to perform without relying solely on equipment.
Disputes the idea that gunfights are determined by the power of the weapons involved.

Actions:

for law enforcement critics,
Understand the importance of commitment and warrior mentality in law enforcement (implied)
</details>
<details>
<summary>
2022-05-29: Let's talk about Biden, a cynical good sign, and power lines.... (<a href="https://youtube.com/watch?v=NamSBx4fhSk">watch</a> || <a href="/videos/2022/05/29/Lets_talk_about_Biden_a_cynical_good_sign_and_power_lines">transcript &amp; editable summary</a>)

The Biden-approved Energy Gateway South Transmission Line signals a shift towards big money supporting renewable energy, accelerating the transition to a cleaner and more reliable power grid.

</summary>

"Big money is now behind green energy."
"They're going to make money on it."
"It's great for the environment."
"They're going to make a dump truck full of cash on it."
"We're finally getting movement in the direction we need to."

### AI summary (High error rate! Edit errors on video page)

The Biden administration approved the Energy Gateway South Transmission Line, a 416-mile power line bringing renewable wind energy from Wyoming through Colorado to Utah.
This project aims to increase reliability in the power grid and set the stage for 25 gigawatts of clean energy from public lands by 2025.
The construction of this power line, along with another called Gateway West, is set to begin soon and should be completed by 2024 if they stick to the schedule.
What makes this power line significant is the involvement of Warren Buffett's Pacific Corp, signaling a shift towards big money backing green energy initiatives.
Pacific Corp plans to install 2000 miles of new transmission lines, retire 22 coal plants, and replace them with renewable energy sources.
The profitability of clean energy is attracting major investors, influencing policy decisions and accelerating the shift towards renewable energy.
While it may be uncomfortable to see billionaires profit, their involvement is necessary to drive the transition towards sustainable energy.
Despite the profit-driven motives, the transition towards clean energy is positive for the environment and necessary for a sustainable future.
The financial incentives for big corporations to invest in clean energy may speed up the transition due to their influence and resources.
This movement towards clean energy, led by major players in the industry, is a critical step in addressing environmental challenges and moving towards a more sustainable future.

Actions:

for environmental advocates, policymakers, activists.,
Support renewable energy initiatives by advocating for clean energy policies and investments (implied).
Stay informed and vocal about the benefits of transitioning to clean energy sources to encourage further support and investment in renewable technologies (implied).
</details>
<details>
<summary>
2022-05-28: Let's talk about why you watch the news.... (<a href="https://youtube.com/watch?v=p1OajhVHV-c">watch</a> || <a href="/videos/2022/05/28/Lets_talk_about_why_you_watch_the_news">transcript &amp; editable summary</a>)

Beau questions the purpose of news consumption, advocates for actionable information with context, and warns against propaganda outlets that aim to influence rather than inform.

</summary>

"Information, being better informed about something, should remove fear."
"If it's just leaving you angry and scared, you should probably stop watching."

### AI summary (High error rate! Edit errors on video page)

Raises the question of why people watch the news, whether it's for entertainment or to stay informed.
Emphasizes the importance of actionable information that can help in making better decisions.
Points out that accurate information without context is not very useful.
Criticizes news outlets that sensationalize stories with dramatic headlines that often turn out to be exaggerated or debunked.
Warns against being misled by news sources that prioritize ratings over accuracy.
Talks about how some news outlets intentionally publish false information or lack context, making it impossible to use the information for decision-making.
Gives an example of misleading reporting on Biden's supply chain issues, stressing the importance of providing context.
Explains how good information with context can prepare individuals for the future and help them understand the full picture.
Draws a distinction between news outlets that inform and those that serve as propaganda, aiming to influence rather than inform.
Encourages viewers to question the news sources they consume and to stop watching if it doesn't improve their understanding or quality of life.

Actions:

for news consumers,
Question the news sources you consume (implied)
Stop watching news outlets that leave you angry and scared (implied)
</details>
<details>
<summary>
2022-05-28: Let's talk about teachers, old ideas, and new dynamics.... (<a href="https://youtube.com/watch?v=t8WTgSjuGVY">watch</a> || <a href="/videos/2022/05/28/Lets_talk_about_teachers_old_ideas_and_new_dynamics">transcript &amp; editable summary</a>)

Beau challenges the flawed logic of arming teachers, questioning its efficacy and impact on school environments, labeling it as a superficial solution.

</summary>

"The gun makes the man. Give them a gun and well, they've got the power now."
"It's a band-aid on a bullet wound, at best."
"There's one waiting for them at the school."
"This isn't a solution."
"It's thoughts and prayers."

### AI summary (High error rate! Edit errors on video page)

An old video of his resurfaces, sparking a chain of events and comments due to addressing different talking points.
The current focus is on the idea of arming teachers, a suggestion now put forth by the Republican Party.
Beau questions the logic of giving guns to teachers who face trust issues even with basic topics like discussing diversity or using rainbow stickers.
He argues that arming teachers doesn’t automatically turn them into warriors as suggested by some.
Beau believes teachers may be more willing to intervene in dangerous situations compared to police, but their role is to save lives, not take them.
He criticizes the mindset required for teachers to carry guns, as it shifts the learning environment into a classification of threat levels.
Beau links the idea of arming teachers to the attitude of shooters seeking power, which is reinforced by the gun's symbolism.
He challenges excuses made for police officers facing armed threats, debunking the notion that a bigger gun always equals more power.
The rhetoric around guns and power is a concerning factor in American society, contributing to a flawed perception of strength.
Beau raises the issue of putting more guns in schools and how it may not serve as a practical or effective solution.

Actions:

for educators, policymakers, parents,
Challenge proposals to arm teachers in your community (implied)
Advocate for comprehensive safety measures in schools without resorting to arming teachers (implied)
</details>
<details>
<summary>
2022-05-28: Let's talk about investigating Trump's campaign in Georgia.... (<a href="https://youtube.com/watch?v=F-4LQXLKws4">watch</a> || <a href="/videos/2022/05/28/Lets_talk_about_investigating_Trump_s_campaign_in_Georgia">transcript &amp; editable summary</a>)

Federal government investigating potential criminality in the fake electors scheme post-2020 election, expanding probes across states with a focus on individuals backing out but possibly possessing critical information.

</summary>

"We won't find out for a while. These things take time."
"It's worth noting when little bits and pieces like this come out."
"The FBI believes there might be some kind of criminality involved."
"So when little bits and pieces like this come out, it's worth noting."
"But the interesting thing is that it's people who backed out."

### AI summary (High error rate! Edit errors on video page)

Federal government's investigation into Trump's attempts to overturn the 2020 election is shrouded in mystery, leaving many questioning their actions.
Signs indicate that the government may be building a case regarding the fake electors scheme in Georgia and other states.
The probe, originally in Georgia, has expanded to other states, with Michigan confirmed and signs of further expansion.
The FBI in Georgia is questioning prominent Republicans about their interactions with Trump, Giuliani, and others tied to Trump.
Subpoenas have been issued to individuals who backed out of being electors, suggesting they may have relevant information.
Subpoenas request communications with over 20 named individuals dating back to before the 2020 election.
In Michigan, a potential fake elector backed out, citing illness as the reason.
The FBI's interest in individuals who didn't participate but may have knowledge indicates a focus on potential criminality.
The presence of someone from the National Archives during interactions adds weight to the idea of building a criminal case.
The investigation is ongoing, with a grand jury set to meet in D.C., indicating a lengthy process.

Actions:

for concerned citizens,
Contact local representatives to advocate for transparency and accountability in the investigation (suggested).
Stay informed about updates on the investigation and share relevant information with your community (exemplified).
Support efforts to uphold the integrity of elections and ensure attempts to undermine democracy are addressed (implied).
</details>
<details>
<summary>
2022-05-27: Let's talk about something better than a ban.... (<a href="https://youtube.com/watch?v=g5g7OE3REME">watch</a> || <a href="/videos/2022/05/27/Lets_talk_about_something_better_than_a_ban">transcript &amp; editable summary</a>)

Beau addresses the need to focus on the individuals behind gun violence and proposes legislative changes to prevent incidents effectively.

</summary>

"None of that matters. None of it matters. If you're focusing on stuff like that, you're missing the big picture."
"It's not impossible, but that's not going to happen. Not in a time frame that will matter."
"Rather than looking at the firearms, look at the person behind them."
"That's your key. That's your thread."
"What matters is the person behind it."

### AI summary (High error rate! Edit errors on video page)

Addresses the importance of focusing on the big picture in the current gun debate in the U.S.
Points out that debates often get stuck on insignificant details like magazine size or cosmetic features of firearms.
Questions the practicality of gun confiscation and the logistics behind it.
Suggests focusing on the people behind the firearms rather than the firearms themselves.
Notes the high percentage of individuals involved in large incidents with a history of domestic violence.
Emphasizes the need to address domestic violence legislation to prevent gun violence effectively.
Proposes targeting specific markers in individuals to close loopholes and prevent incidents.

Actions:

for legislators, activists, community members,
Address domestic violence legislation loopholes (suggested)
Advocate for laws targeting individuals with specific markers for prevention (suggested)
</details>
<details>
<summary>
2022-05-27: Let's talk about small business and republicans.... (<a href="https://youtube.com/watch?v=OIrVN-XbPz0">watch</a> || <a href="/videos/2022/05/27/Lets_talk_about_small_business_and_republicans">transcript &amp; editable summary</a>)

Politicians' talking points on supporting small businesses quickly expire without real action, leaving thousands of businesses at risk of closure due to lack of support.

</summary>

"Of course, maybe we do, because you don't really see politicians held accountable for this behavior."
"Some of you may die, but it's a risk they're willing to take."
"Guess that talking point expired."

### AI summary (High error rate! Edit errors on video page)

Politicians use talking points that quickly expire, but they expect us to forget about them.
Republicans were pushing for people to shop and consume at small businesses during the early days of the global public health issue.
A bill went to the Senate to help gyms and restaurants impacted by the pandemic, but Republicans largely voted against it.
The bill aimed to refill a fund to support struggling businesses, including restaurants and gyms.
Only a fraction of the restaurants that applied for relief actually received any funds due to the program running out of money.
The Independent Restaurant Coalition warns that up to half of the 177,000 restaurants awaiting funds could close without this support.
The failure to pass the bill puts tens of thousands of restaurants and small businesses at risk of closure.
The bill also had provisions for gyms, live event operators, and possibly ferries.
Republicans used small businesses as talking points to encourage economic activity, but failed to follow through with meaningful support.
The new talking point is to argue against providing relief by deeming certain groups as no longer useful.

Actions:

for voters, activists, community members,
Support local restaurants and small businesses by dining in or ordering takeout (implied)
Advocate for government support for struggling businesses by contacting elected officials (implied)
</details>
<details>
<summary>
2022-05-27: Let's talk about 2 possible legislative futures for Democrats.... (<a href="https://youtube.com/watch?v=FxOjnCs6ykA">watch</a> || <a href="/videos/2022/05/27/Lets_talk_about_2_possible_legislative_futures_for_Democrats">transcript &amp; editable summary</a>)

Beau presents a compelling case for the Democratic Party to prioritize closing domestic violence loopholes over pursuing an assault weapons ban to effectively address mass incidents.

</summary>

"Rather than just changing the incident, you're stopping it."
"Rather than making it the forbidden fruit to get to own the libs."
"When you look at the data and put everything in context, it's the only thing that actually makes sense."

### AI summary (High error rate! Edit errors on video page)

Democrats facing a choice between an assault weapons ban and closing domestic violence loopholes.
The potential consequences of pursuing an assault weapons ban.
Addressing only 16% of mass incidents with an assault weapons ban.
The likelihood of an assault weapons ban facing political and legal challenges.
The idea that banning assault weapons might not be as effective in reducing casualties.
Proposing closing domestic violence loopholes as a more effective approach.
The higher potential impact of closing domestic violence loopholes.
The underreporting of domestic violence incidents.
The importance of upping reporting to prevent access to firearms by domestic violence offenders.
Aiming to shift the gun culture by closing loopholes instead of focusing on assault weapons bans.

Actions:

for legislators, democratic party members,
Advocate for closing domestic violence loopholes to prevent access to firearms (implied).
Support measures that increase reporting of domestic violence to prevent firearm access (implied).
</details>
<details>
<summary>
2022-05-26: Let's talk about metaphors, hair, and Florida.... (<a href="https://youtube.com/watch?v=STW75DixoAw">watch</a> || <a href="/videos/2022/05/26/Lets_talk_about_metaphors_hair_and_Florida">transcript &amp; editable summary</a>)

At a Florida high school graduation, a speech on embracing curly hair and individuality, reaffirming the right to exist as you are amidst diverse support networks.

</summary>

"You want to make everybody have the same hairstyle. It's not going to happen."
"Curly haired people exist and they have every right to."
"You will not deprive people of that support network."

### AI summary (High error rate! Edit errors on video page)

Recaps a speech given at a Florida high school graduation about embracing individuality.
The speaker, a curly-haired person, shares their journey of accepting their hair.
Describes the struggle of trying to manage and straighten their curly hair when younger.
Talks about finding a support network that helped them accept their curly hair.
Explains how embracing their curly hair made them happier and improved their life.
Mentions the climate in Florida and how it affects curly hair due to humidity.
Asserts that those opposing the acceptance of curly hair will not succeed.
Emphasizes the importance of diverse support networks and communities.
Affirms the right of curly-haired individuals to exist and be accepted.
Concludes by stating that legislation cannot dictate people's hairstyles.

Actions:

for high school students, curly-haired individuals,
Support and celebrate individuals with diverse hair types (implied)
Cultivate inclusive communities that embrace individuality (implied)
</details>
<details>
<summary>
2022-05-26: Let's talk about ice cream.... (<a href="https://youtube.com/watch?v=-ytVbUJ3lD8">watch</a> || <a href="/videos/2022/05/26/Lets_talk_about_ice_cream">transcript &amp; editable summary</a>)

Walmart's themed ice cream sparks controversy over perceived "wokeness," revealing a culture war fueled by outrage stokers and capitalism, prompting Beau to humorously advise letting go.

</summary>

"Let it go."
"You're melting down over ice cream there, snowflake."
"It's just red velvet ice cream in a different container."

### AI summary (High error rate! Edit errors on video page)

Walmart introduced themed ice cream flavors representing pride and Juneteenth, sparking controversy on the right.
The outrage stems from the perception of Walmart being "woke," which is refuted as capitalism, not activism.
Many individuals within the target demographics view these products as pandering and are not pleased.
The situation is portrayed as part of a new culture war, where representation in products becomes a contentious topic.
The reaction to the ice cream products is seen as an overblown response fueled by outrage stokers seeking to provoke anger for engagement.
Beau criticizes those who easily get riled up over minor instances of representation, calling it a sad way to live.
He suggests that those manipulated by outrage are being trained to live in a constant state of anger and paranoia.
The ice cream flavors are likened to basic capitalism rather than a political statement or agenda.
Beau humorously advises letting go of the outrage, likening it to dealing with something frozen.
The overall message is a call to not let minor issues consume one's energy and to focus on more significant matters.

Actions:

for consumers, activists,
Let go of outrage and focus on more significant issues (suggested)
</details>
<details>
<summary>
2022-05-26: Let's talk about cops waiting outside, Rule 303, and the way.... (<a href="https://youtube.com/watch?v=DKjSLAUFWSo">watch</a> || <a href="/videos/2022/05/26/Lets_talk_about_cops_waiting_outside_Rule_303_and_the_way">transcript &amp; editable summary</a>)

Beau talks about the origin of "Rule 303," its evolution, and stresses the importance of responsible action, especially in law enforcement, while rejecting the "might makes right" narrative.

</summary>

"If you have the means to help, you have the responsibility to do so."
"Nothing else is acceptable but continual pressure."
"That's the way of the warrior. Because you can't stop. The mission is all that matters."
"If you can't be in that position, if you can't commit to that and act on it when the time comes, you need a different assignment."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the origin of "Rule 303" and how it evolved on his channel due to the community.
Describes how the term was initially tied to summary executions and later adopted by contractors who went beyond the rules based on their judgment.
Talks about how the term made its way to law enforcement but shifted in meaning to "might makes right."
Calls out law enforcement actions during incidents like Parkland where officers failed to act appropriately.
Stresses the importance of officers engaging and pressing in situations of threat rather than waiting outside.
Criticizes the glorification of the "warrior cop" mentality and underscores the true meaning of being a warrior.
Emphasizes the mission's importance and the resolute acceptance of death as part of the warrior's path.
Concludes with a reminder that those unable to commit to the mission's demands should seek a different assignment.

Actions:

for law enforcement officers,
Engage and press in threatening situations until there is no longer a threat (implied)
</details>
<details>
<summary>
2022-05-25: Let's talk about the Georgia primaries and what's next for Trump.... (<a href="https://youtube.com/watch?v=oWy_6bkK2Og">watch</a> || <a href="/videos/2022/05/25/Lets_talk_about_the_Georgia_primaries_and_what_s_next_for_Trump">transcript &amp; editable summary</a>)

Beau examines the divide in the GOP between Trump candidates and traditional Republicans and speculates on the potential impact on elections due to Trump's influence.

</summary>

"Does he not show up to help the candidate who in this case totally embarrassed him, Kemp?"
"They may just stay home and it may cost Republicans elections around the country."
"All because they still refuse to do the one thing they have to do with Trump, which is get him out of the party."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the Republican primary in Georgia, particularly focusing on the governor's race and the anticipated results.
Despite voting not being over yet, Beau assumes Kemp has won based on the polls.
There is a divide within the GOP between Trump candidates and traditional Republicans competing for control in primaries.
Beau questions Trump's reaction in states where his preferred candidate loses, pondering if he will continue supporting the Republican Party or drive down voter turnout.
He expresses concerns about an unenergized Republican base affecting elections, specifically in Kemp's case against Abrams.
Beau suggests that the MAGA faction's vindictive nature might lead to decreased voter turnout if Trump continues to be a divisive figure within the party.

Actions:

for political observers,
Monitor the dynamics within the GOP and how Trump's influence affects elections (implied)
Support candidates based on their policies and values rather than loyalty to a specific figure (implied)
</details>
<details>
<summary>
2022-05-25: Let's talk about grain, Ukraine, and messaging.... (<a href="https://youtube.com/watch?v=jGOq1ze-OgM">watch</a> || <a href="/videos/2022/05/25/Lets_talk_about_grain_Ukraine_and_messaging">transcript &amp; editable summary</a>)

Beau outlines a proposal to break the Russian blockade in Ukraine, raising concerns about potential escalation and the critical importance of securing grain supplies for countries in need, waiting on Russia's response.

</summary>

"If that grain doesn't get to where it needs to be, people will die."
"Do you take the risk or do you once again allow those people in countries that don't have the economic power, that have nothing to do with this, pay the price for European imperialism."
"I guess we're all waiting to see what happens."

### AI summary (High error rate! Edit errors on video page)

Proposal to break the Russian blockade in Ukraine to allow grain to be shipped out.
Framed as a coalition of the willing outside of NATO.
Risk of escalation exists despite efforts to minimize it.
Ukrainian grain supply critical for many countries.
More countries may be on board with the proposal.
Recent weapons shipments may be connected to this plan.
Russia is being messaged about the impending action.
Possibility of Russian ships loading Ukrainian grain.
Decision likely made to move forward with the plan despite risks.
The importance of ensuring grain reaches those in need.
Lack of U.S. involvement in the publicly named countries supporting the plan.
Uncertainty about how Russia will respond to the proposal.
European imperialism's impact on countries without economic power.
UK and countries receiving the grain potentially committing ships to the plan.
Waiting for Russia's response before taking further action.

Actions:

for global policymakers,
Support organizations providing humanitarian aid to regions impacted by food insecurity (suggested)
Stay informed about international developments and crises affecting food supplies (implied)
</details>
<details>
<summary>
2022-05-24: Let's talk about renaming military bases.... (<a href="https://youtube.com/watch?v=zk_ZZvGgSi4">watch</a> || <a href="/videos/2022/05/24/Lets_talk_about_renaming_military_bases">transcript &amp; editable summary</a>)

The US military is renaming installations named after Civil War traitors by 2023, with widespread support and a focus on honoring diverse heroes.

</summary>

"It is time for this to happen, and it has a lot of support, both across the country and within the military community."
"There are a lot of people who don't like working at installations that are named after people that they really don't like."

### AI summary (High error rate! Edit errors on video page)

The US military is renaming installations named after Civil War traitors by 2023.
Congress-created commissions suggest new names for the installations, which must be confirmed by Congress.
Fort Bragg will become Liberty, Hood will be renamed Cavazos, and Gordon will change to Eisenhower.
Lee will be jointly renamed after General Greg and Charity Adams, the first black woman officer in the Women's Army Auxiliary Corps.
Pickett will be renamed after Van Barfoot, a Medal of Honor recipient known for embarrassing Klansmen.
A.P. Hill will be named after Dr. Mary Walker, an abolitionist and Medal of Honor recipient during the Civil War.
Benning will be renamed after Hal Moore, known from the movie "We Were Soldiers."
Rucker will be renamed after Michael Novosel, a Medal of Honor recipient who flew helicopters during Vietnam.
Fort Polk will be named after Sergeant William Henry Johnson, a Medal of Honor recipient from World War I.
The renaming process has widespread support and aims to address discomfort at installations named after controversial figures.

Actions:

for military personnel, historians, activists.,
Support the renaming process by advocating for the changes in your community (implied).
</details>
<details>
<summary>
2022-05-24: Let's talk about hurricanes and supply chains.... (<a href="https://youtube.com/watch?v=F2HGluxjzRA">watch</a> || <a href="/videos/2022/05/24/Lets_talk_about_hurricanes_and_supply_chains">transcript &amp; editable summary</a>)

Be prepared for hurricanes this year - stressed supply chains mean longer recovery times and reduced supplies.

</summary>

"Make sure that you're prepared for that."
"Plan your evacuation route now."
"You need to prepare now, not once the news of the storm breaks."
"It's going to take even longer to get back up and running."
"Keep an eye on the weather."

### AI summary (High error rate! Edit errors on video page)

Loop current active, big hurricane year expected.
Stressed supply chains mean delays in supplies and relief post-hurricane.
Private companies may struggle to provide relief due to supply chain issues.
Individuals in hurricane-prone areas urged to prepare for longer recovery times.
People in evacuation areas should also prepare for reduced supplies due to increased demand.
Even those far from hurricanes should prepare for supply chain stress post-storm.
Stress on supply chains nationwide after hurricanes will impact availability of goods.
Urges viewers to plan evacuation routes, monitor weather, and ensure access to gas.
Recommends checking home insurance policy and gathering necessary documents.
Suggests preparing emergency supplies including water, fire, shelter, knife, first aid kit, meds, tarp, charcoal, and water.
Encourages finding ways to charge electrical devices in advance.
Warns of high prices during evacuation and need to be prepared for extended sheltering.
Expects longer recovery periods post-hurricane due to stressed supply chains.

Actions:

for residents in hurricane-prone areas,
Plan your evacuation route now (suggested)
Gather emergency supplies including water, fire, shelter, knife, first aid kit, meds, tarp, charcoal, and water (suggested)
Ensure access to gas for evacuation (suggested)
Prepare to outlast normal period if sheltering in place (suggested)
</details>
<details>
<summary>
2022-05-24: Let's talk about Biden, China, and intent.... (<a href="https://youtube.com/watch?v=QLqtYPWvHh0">watch</a> || <a href="/videos/2022/05/24/Lets_talk_about_Biden_China_and_intent">transcript &amp; editable summary</a>)

Biden's remarks on defending Taiwan reveal the complex dance of foreign policy, where ambiguity and unpredictability can be strategic assets.

</summary>

"What Biden did is normal. That's what foreign policy is supposed to look like."
"It's an international poker game where everybody's cheating."
"This is what foreign policy looks like when you have a good team."
"It's actually an asset because people know that he goes off script."
"Confusion and ambiguity can be strategic tools in international relations."

### AI summary (High error rate! Edit errors on video page)

Biden's statement about defending Taiwan led to confusion and speculation.
White House clarified Biden "misspoke" and the US is not ready to defend Taiwan.
Speculation arises about Biden's competence and decision-making.
Russia's poor planning and intelligence in Ukraine led to unexpected outcomes.
Understanding an opponent's intent is key in foreign policy.
Lack of information about Putin's intent creates speculation and worry.
Biden's unpredictability adds to the uncertainty in foreign relations.
Openly stating intentions, like in the Afghanistan withdrawal, can have negative consequences.
Masking intent is vital in foreign policy to keep adversaries guessing.
Confusion and ambiguity can be strategic tools in international relations.

Actions:

for foreign policy analysts,
<!-- Skip this section if there aren't physical actions described or suggested. -->
</details>
<details>
<summary>
2022-05-23: Let's talk about what Dems can learn from the Scopes trial.... (<a href="https://youtube.com/watch?v=wk9pJVkYGG0">watch</a> || <a href="/videos/2022/05/23/Lets_talk_about_what_Dems_can_learn_from_the_Scopes_trial">transcript &amp; editable summary</a>)

The Scopes trial history teaches changing societal thinking drives lasting change, urging political power to uphold majority values against regressive actions.

</summary>

"To change society, you don't have to change the law. You have to change the way people think."
"It's regressive. It isn't trying to change thought and then change the law. It's trying to use the law to change thought."
"Thought is already in agreement on the pro-freedom side, the side that wants choice, that wants options, that wants education."
"It's the raw exercise of political power that will shift this dynamic."
"If you don't fight back, then it doesn't matter what thought gets shifted."

### AI summary (High error rate! Edit errors on video page)

Explains the historical significance of the Scopes trial in the 1920s and its impact on current social issues being debated in courts and legislation.
Describes the Butler Act in Tennessee prohibiting the teaching of evolution and the teacher who challenged it in court in 1925.
Details the teacher's appeal to the Tennessee Supreme Court on the grounds of the law being overly broad, violating free speech, Tennessee's constitution, and freedom of religion.
Mentions the Supreme Court case in 1968, Epperson v. Arkansas, which found such laws violated the establishment clause of the First Amendment.
Emphasizes the importance of changing societal thinking to bring about real change, rather than just changing laws.
Criticizes current Republican actions as regressive, authoritarian, and against the majority of society's desires.
Stresses the need for political power to drive change based on societal thought and values, pointing out that people already support freedom, choice, education, and critical thinking.
Urges the Democratic Party to exercise political power effectively to counter regressive authoritarianism and uphold the majority's wishes for progress and freedom.

Actions:

for democratic activists, concerned citizens,
Mobilize for election issues (implied)
Advocate for freedom, choice, and education (implied)
Support critical thinking in schools (implied)
Pressure representatives to act in accordance with majority wishes (implied)
Fight against regressive authoritarianism through political power (implied)
</details>
<details>
<summary>
2022-05-23: Let's talk about falling dominoes.... (<a href="https://youtube.com/watch?v=t9jTMOu8UnQ">watch</a> || <a href="/videos/2022/05/23/Lets_talk_about_falling_dominoes">transcript &amp; editable summary</a>)

Beau explains how dismantling systemic racism is the key to addressing various societal issues affecting white people.

</summary>

"The first step to getting to any of this is systemic racism."
"We've got to move past that in this country."
"As long as they're doing better than those other people, well, they'll keep voting that way."
"It's so pervasive in this country that even something as simple as pools doesn't escape its grasp."
"The first step to getting to any of this is systemic racism."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of dominoes falling in relation to systemic racism.
Points out how systemic racism affects social safety nets, wages, and housing investments.
Links tipping to a legacy of slavery and suppression of black wages.
Connects objections to affordable housing to racism and voter motivations.
Attributes the militarization of police to the war on drugs targeting specific communities.
Describes the cycle of underfunding in schools due to racial disparities.
Reveals how racism is used as a tool to manipulate white voters against their interests.
Raises questions about airport security, war involvement, and public pool closures in the context of systemic racism.
Stresses the need to dismantle systemic racism as the first step towards positive change.

Actions:

for white americans,
Research the history of tipping and its impact on racial wage disparities (suggested)
Investigate the reasons behind objections to affordable housing in your community (suggested)
Learn about the racial implications of the war on drugs and militarization of police (suggested)
</details>
<details>
<summary>
2022-05-23: Let's talk about answering Trump's tweet.... (<a href="https://youtube.com/watch?v=XswrMFlnb_o">watch</a> || <a href="/videos/2022/05/23/Lets_talk_about_answering_Trump_s_tweet">transcript &amp; editable summary</a>)

Beau challenges the fantasy of civil war scenarios, questions the lack of tangible grievances, and urges critical reflection on the destructive impact of blindly following divisive rhetoric.

</summary>

"The Constitution, it's not magic, it's just words on paper."
"It's a whole lot easier to destroy than it is to build, right?"
"What's it going to be like? Am I really willing to sacrifice everything that I've ever known or loved?"
"Are you really willing to commit years of your life and destroy the country for whatever that grievance is?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses Trump's tweet calling for civil war and focuses on the people affected by such messages.
Questions the fantasy of civil war scenarios presented by some individuals.
Criticizes the lack of tangible grievances or reasons behind the calls for civil war.
Challenges the notion of blindly following anti-establishment sentiments without clear goals.
Points out the futility of aiming to "restore the Constitution" without a concrete plan.
Emphasizes the importance of active citizenship and constructive engagement with governance.
Urges individuals to think critically about the implications and consequences of advocating for civil war.
Contrasts the demographics discussing civil war with those who truly face grievances and hardships.
Criticizes how a tweet can incite destructive tendencies in individuals.
Encourages reflection on the real-life impact of civil war, likening it to the experiences of interpreters in conflict zones.
Challenges the division and demonization of political ideologies within communities.
Raises awareness about the dangerous consequences of blindly following divisive rhetoric.
Urges individuals to reconsider the destructive path of violence and conflict.
Encourages seeking understanding and empathy towards others, even those with differing political views.
Concludes by questioning the rationality of destroying everything based on incendiary messages.

Actions:

for community members,
Talk to someone who has experienced conflict or war (exemplified)
Engage in constructive political participation (implied)
Foster empathy and understanding towards individuals with different political views (implied)
</details>
<details>
<summary>
2022-05-22: Let's talk about what aliens can teach us about tech companies.... (<a href="https://youtube.com/watch?v=sRTyf-1drzk">watch</a> || <a href="/videos/2022/05/22/Lets_talk_about_what_aliens_can_teach_us_about_tech_companies">transcript &amp; editable summary</a>)

Tech companies must prioritize ethics and equality to prevent humanity from self-destruction amidst technological advancements.

</summary>

"Tech companies have to skew woke. They have to skew egalitarian."
"If they skew towards nationalism, if they skew towards subjugation, if they skew towards old ideas of hierarchy and domination, well, those ideas will continue."
"If tech companies are controlled by profit motive, we're doomed."
"It's incredibly important for humanity to continue to grow at a faster rate than our technology."
"If humanity doesn't philosophically advance along with it, it doesn't matter because that technology won't be around long."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of aliens and their relationship to tech companies.
Mentions the Fermi paradox and the theory that intelligent species tend to self-destruct before exploring space.
Suggests that humanity's technological advancements have often been driven by war or a desire for control.
Emphasizes the need for ethics in tech companies to move away from competitive and primitive instincts.
Argues that tech companies should prioritize promoting equality and enlightenment for all of humanity.
Criticizes tech figures who reject woke and egalitarian ideas, calling them con artists focused on profit rather than progress.
Stresses the importance of an egalitarian society for the species to advance beyond current limitations.
Warns that without philosophical advancement alongside technological progress, humanity is doomed to self-destruct.

Actions:

for tech professionals, activists, policymakers.,
Advocate for ethics and equality in tech companies (implied).
Support initiatives and policies that prioritize the betterment of humanity over profit (implied).
</details>
<details>
<summary>
2022-05-22: Let's talk about the military going green.... (<a href="https://youtube.com/watch?v=pSgN8skKZJ8">watch</a> || <a href="/videos/2022/05/22/Lets_talk_about_the_military_going_green">transcript &amp; editable summary</a>)

The Department of Defense is advancing environmentally friendly initiatives while critics question their feasibility; Ukraine showcases the effectiveness of e-bikes in military operations, debunking skeptics' claims.

</summary>

"Those who say something is impossible should probably stop interrupting those who are doing it."
"It's one of those things you can keep up or get left behind, but nobody really cares anymore."

### AI summary (High error rate! Edit errors on video page)

The Department of Defense (DOD) has launched various climate and environmentally friendly initiatives, aiming to reduce the carbon footprint by 50% and more.
Politicians, backed by dirty energy companies, often criticize and mock these initiatives, claiming they are impossible to achieve.
A common criticism is the idea that electric vehicles won't outrun Chinese tanks, like a Prius versus a tank scenario.
In Ukraine, Russian tank drivers are facing a new challenge from Ukrainian soldiers on e-bikes.
Ukrainian soldiers use e-bikes, donated and modified by a private company, to set up ambushes against tanks effectively.
E-bikes provide speed, quietness, and agility, making them more efficient for military operations compared to soldiers on foot.
The real-life scenario in Ukraine showcases tanks running from electric vehicles, contrary to the skeptics' claims.
Russia's invasion in Ukraine faced setbacks not due to Ukrainian resistance initially but because their vehicles ran out of gas.
The future of military vehicles may involve producing their own energy or having convoy vehicles that generate energy for others.
Those deeming things impossible should not hinder those actively working towards achieving them.

Actions:

for environmental activists, military personnel,
Support environmentally friendly initiatives in your local military or community by advocating for sustainable practices (exemplified)
Donate resources or funds to provide efficient modes of transportation for military or emergency response teams (exemplified)
</details>
<details>
<summary>
2022-05-22: Let's talk about the Great Salt Lake, Utah, and climate.... (<a href="https://youtube.com/watch?v=n2WWA4uZAC0">watch</a> || <a href="/videos/2022/05/22/Lets_talk_about_the_Great_Salt_Lake_Utah_and_climate">transcript &amp; editable summary</a>)

The Great Salt Lake's decline due to climate change underscores the urgent need for proactive, sustainable solutions to address environmental impacts and ensure a livable future for all.

</summary>

"The longer we put off making the changes that are necessary, the worse it's going to get."
"We can't just keep pretending like it's not happening."
"There are impacts from climate change that are guaranteed to happen. They won't just skip us."
"We're just seeing the tip of the iceberg that's melting."
"There are news stories every single day that show the impacts that are already here."

### AI summary (High error rate! Edit errors on video page)

The Great Salt Lake in Utah is receding, currently 11 feet lower than when pioneers arrived, causing damage to birds and wildlife.
For every foot the lake drops, 150 square miles of the lake bed are exposed, impacting the economy and Utah's GDP.
Proposed solution: building a pipeline to pump in salt water from the Pacific, facing potential environmentalist backlash.
Climate change is a significant factor in the lake's decline, with impacts being felt particularly in the western and southwestern regions.
The prevailing attitude of trying to overcome climate change without changing our way of living is unsustainable and doomed to fail.
Exposed lake beds pose additional risks due to arsenic content, exacerbating the already dire situation.
Delaying necessary changes will only worsen the effects of climate change and necessitate more drastic actions in the future.
The impacts of climate change are inevitable and will affect everyone, requiring proactive measures from legislatures across the country.
Utah's consideration of solutions like the pipeline is a start, but real change may involve altering consumption patterns and industries in the state.
Internal and external migration due to climate change is likely, underscoring the need to acknowledge and prepare for these realities.

Actions:

for legislatures, policymakers, environmentalists,
Start considering proactive measures to address climate change impacts (implied)
Advocate for sustainable solutions and policies in response to environmental challenges (implied)
</details>
<details>
<summary>
2022-05-21: Let's talk about an unusually honest message.... (<a href="https://youtube.com/watch?v=hf0amSOuOX0">watch</a> || <a href="/videos/2022/05/21/Lets_talk_about_an_unusually_honest_message">transcript &amp; editable summary</a>)

Beau explains the necessity of dismantling systemic racism as the first step towards addressing inequalities and improving lives for all.

</summary>

"You want a reason that is self-interested. That's it."
"You have more in common with the black people in your area than you do with those who actually really benefit from this system."
"We get rid of that, all the other dominoes fall. Your life will improve. It won't get worse."
"There's plenty of cookies. We just have to get enough people to say that everybody deserves a plate."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of removing systems that contribute to inequality, addressing the need for more than a simple answer.
Shares a message from someone who believes they are not racist but questions tearing down a system that benefits them.
Challenges the idea of white privilege by detailing personal struggles with debt and working hard to stay afloat.
Points out that systemic racism benefits those in power and keeps individuals down, even if they believe they are doing better than others.
Illustrates the concept of systemic racism through a cookie analogy, showing how the system pits individuals against each other based on race.
Emphasizes the need to dismantle systemic racism first before addressing other inequalities that directly impact individuals.
Encourages self-interest as a reason to dismantle systemic inequalities and challenges the notion of feeling superior due to a slight edge over others.
Urges individuals to recognize their commonalities with people of different races and to understand who truly benefits from the current system.
Stresses the necessity of removing systemic racism as the initial step towards improving lives and breaking down other structures of inequality.
Concludes by advocating for collective action to ensure everyone has a fair share, promoting unity and equality.

Actions:

for community members, activists,
Challenge systemic racism through education and advocacy (implied)
Advocate for policies that address systemic inequalities in society (implied)
Engage in community organizing to dismantle oppressive structures (implied)
</details>
<details>
<summary>
2022-05-21: Let's talk about adaptation, Canada, and climate.... (<a href="https://youtube.com/watch?v=v1MJKFpJuKs">watch</a> || <a href="/videos/2022/05/21/Lets_talk_about_adaptation_Canada_and_climate">transcript &amp; editable summary</a>)

The Canadian government crowdsources citizen suggestions to tackle climate change while the U.S. lags behind, urging for faster action.

</summary>

"Countries are going to start paying more attention, and they're going to come up with various ways."
"There has been a pretty marked shift in what's being talked about, and it's shifting away from stopping climate change."
"Canada is using what it is calling a whole of Canada approach."
"Americans will always do the right thing as soon as we try everything else first."
"Maybe we could just speed that along this time because we don't have a lot of time."

### AI summary (High error rate! Edit errors on video page)

Contrasting the citizen response to climate change in Canada and the United States.
Canadian government crowdsourcing plans to deal with climate change.
Asking citizens for suggestions on how to make Canada more resilient to climate change impacts.
Categories for suggestions include improving natural environment, disaster resilience, health, and managing infrastructure.
Canada using a "whole of Canada approach" to tackle climate change.
Shift in focus from stopping climate change to mitigating its impacts.
Deadline for submitting ideas is in July of this year.
Comparing the approach in Canada to the situation in the United States.
Mentioning the Mississippi River as natural infrastructure.
Urging for quicker action due to the urgent nature of climate change.

Actions:

for climate activists, concerned citizens.,
Submit your ideas via letstalkadaptation.ca (suggested).
Participate in crowdsourcing efforts for climate change adaptation (suggested).
</details>
<details>
<summary>
2022-05-21: Let's talk about Trump, the Kingmaker of Georgia.... (<a href="https://youtube.com/watch?v=xcHPGNxkyOk">watch</a> || <a href="/videos/2022/05/21/Lets_talk_about_Trump_the_Kingmaker_of_Georgia">transcript &amp; editable summary</a>)

Beau challenges the narrative of Trump as a kingmaker, suggesting that pushing back against him may be a smarter strategy for Democrats and showcasing instances where his endorsement doesn't matter.

</summary>

"Trump's not a kingmaker. He's a loser."
"Bucking Trump, a Republican candidate who is just standing on their own saying, no, I'm not going to help you destroy the American democracy, that seems to be more valuable than dancing like a puppet for Trump to get his endorsement."
"He's always been a loser."

### AI summary (High error rate! Edit errors on video page)

Criticizes the narrative pushed by the Democratic establishment that Trump is the kingmaker of the Republican Party.
Points out that Trump often takes credit for endorsements that come in after results are known and polls have solidified.
Suggests that showcasing instances where Trump's endorsement doesn't matter could be more valuable than playing along with the narrative.
Mentions that the logic behind getting MAGA candidates nominated is to make them easier to beat in the general election.
Argues that pushing back against Trump and not seeking his endorsement may be a smarter strategy for Democrats.
Uses the example of Georgia to demonstrate that Trump's endorsement doesn't always have a significant impact.
Notes that candidates who stand on their own without seeking Trump's endorsement have done well in elections.
States that Trump is not a kingmaker but rather a loser, contrary to the media consensus.

Actions:

for political strategists, democratic party members,
Support candidates who stand against Trump's influence (implied)
</details>
<details>
<summary>
2022-05-20: Let's talk about Switzerland, NATO, and Russia.... (<a href="https://youtube.com/watch?v=poXvL7j5Q38">watch</a> || <a href="/videos/2022/05/20/Lets_talk_about_Switzerland_NATO_and_Russia">transcript &amp; editable summary</a>)

Switzerland's potential move towards NATO due to Russian aggression challenges its legendary neutrality, signifying a significant shift in international relations with no justification for the aggression.

</summary>

"There is no justification for the Russian aggression, none. It doesn't exist. It's manufactured."
"The argument over any justification for this war is over."
"When it comes to the international poker table, the entrance of Switzerland and them just watching the game, that's a huge deal."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic by discussing the legendary neutrality of Switzerland amidst Russia's invasion of Ukraine.
Switzerland, known for staying neutral for two centuries, is now discussing NATO membership due to Russia's actions.
Despite being fiercely independent and avoiding international wars, Switzerland is considering ways to cooperate with NATO without officially joining.
The Swiss Ministry of Defense is planning to support other countries by backfilling munitions if they provide aid to Ukraine.
Switzerland's potential involvement in joint exercises with NATO and leadership meetings signifies a significant shift from their traditional stance.
The online debate on whether Russia's actions were justified is dismissed by Switzerland's contemplation of NATO membership.
Despite Switzerland's constitutional commitment to neutrality, they are exploring ways to push the boundaries of their involvement in response to Russian aggression.
Switzerland's potential move towards NATO due to Russian aggression is seen as a significant development in the international arena.
The transcript concludes with Beau stating that there is no justification for Russia's aggression, labeling it as manufactured and offensive.
Beau leaves the audience with his thoughts, wishing them a good day.

Actions:

for global citizens,
Support countries by backfilling munitions in times of crisis (implied)
Participate in joint exercises and leadership meetings with international partners (implied)
</details>
<details>
<summary>
2022-05-20: Let's talk about Oz, Pennsylvania, and me agreeing with Trump.... (<a href="https://youtube.com/watch?v=DE4TQJxCGGI">watch</a> || <a href="/videos/2022/05/20/Lets_talk_about_Oz_Pennsylvania_and_me_agreeing_with_Trump">transcript &amp; editable summary</a>)

Beau dissects Trump's tactics, revealing a pattern of baseless claims and a lack of belief in democratic principles.

</summary>

"People who are just, you know, out there looking and happen to find votes, that they're cheating and probably engaged in some kind of conspiracy, that I believe."
"The only person that we have audio of trying to find votes is him."
"Every accusation from this man seems like it's really a confession."

### AI summary (High error rate! Edit errors on video page)

Talking about former President Donald J. Trump and Dr. Oz in Pennsylvania.
Trump suggested Dr. Oz should declare victory to prevent cheating with ballot finds.
Despite no evidence of voter fraud, Trump's statement implies possible cheating.
Beau disagrees with authoritarian tactics but agrees on the cheating implication.
Trump's desire to find 11,780 votes in Georgia is revisited as a concerning pattern.
Beau stresses the need to constantly remind people of Trump's baseless claims.
Trump's actions reveal doubts about his sincerity towards democracy.
Every accusation from Trump appears to be a confession of his own tactics.
Overall, Trump's actions indicate a lack of belief in democratic principles.

Actions:

for voters, democracy advocates.,
Constantly remind others of baseless claims to counter misinformation (suggested).
Initiate dialogues about the importance of democracy and fair elections (implied).
</details>
<details>
<summary>
2022-05-20: Let's talk about Madison Cawthron's Dark comment.... (<a href="https://youtube.com/watch?v=TBp5wji8f3Y">watch</a> || <a href="/videos/2022/05/20/Lets_talk_about_Madison_Cawthron_s_Dark_comment">transcript &amp; editable summary</a>)

Beau dissects Madison Cawthorn's statement, urges raw information consumption, and exposes fascist undertones in "Darkmaga".

</summary>

"The time for, I'm assuming this means Gentile, but that's not what he typed, politics as usual has come to an end."
"This isn't something that the media should try to downplay because it is very open."
"When you see this, just call it what it is. It's fascism."

### AI summary (High error rate! Edit errors on video page)

Analyzes Madison Cawthorn's statement and the narrative building around it.
Talks about information consumption and shaping narratives in the media.
Mentions the habit of fitting new information into existing narratives.
Suggests using a method to filter out narrative building and access raw information.
Encourages looking into the origins of terms before narrative building began.
Explains how to use the date filter option on search engines to access raw information.
Links a Newsweek article about "Darkmaga" and describes it as fascism.
Criticizes the rebranding and insertion of authoritarian ideologies into conservative politics.
Emphasizes the importance of calling out fascism when it is present, without downplaying it.
Raises awareness about the implications of certain political movements, including fascism.
Points out the significance of recognizing and labeling authoritarian governance attempts.
Encourages viewers to analyze meme movements and understand their underlying messages.

Actions:

for media consumers,
Read the Newsweek article about Darkmaga and understand its implications (suggested).
Analyze meme movements to comprehend their underlying messages (suggested).
Call out authoritarian ideologies and fascism when identified (exemplified).
</details>
<details>
<summary>
2022-05-19: Let's talk about the future of the US, Russia, and Africa.... (<a href="https://youtube.com/watch?v=8q34HggiVME">watch</a> || <a href="/videos/2022/05/19/Lets_talk_about_the_future_of_the_US_Russia_and_Africa">transcript &amp; editable summary</a>)

Beau outlines how the US is gearing up to extend influence in Africa under the guise of countering Russian activities, with a focus on soft power strategies and a long-term foreign policy vision.

</summary>

"Africa is the new hotspot right then."
"Generally speaking, these types of operations are not horrible for the countries that they're run in."
"The United States is committed to this course."
"The United States is going to try to implement."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talks about the future of the foreign policy of the United States, focusing on Africa and Russia.
Mentions the creation of Africa Command by the United States in preparation for Africa becoming a priority.
Describes a bill in Congress, HR 7311, aimed at countering Russian influence in Africa.
Points out that while framed as countering Russia, the bill actually prepares the US for increased influence in Africa.
Outlines the strategy of strengthening democratic institutions, human rights, and monitoring natural resources to bring African nations into the US sphere of influence.
Notes that the US intends to use soft power to establish influence rather than military force.
Mentions the creation of de facto agents of influence and elevating cultural leaders friendly to US interests as part of the strategy.
Indicates that the US plans a long-term foreign policy strategy in Africa akin to its campaign in Ukraine.
Emphasizes that while these operations may not be horrible for the countries involved, there is always a coercive element accompanying them.
Concludes that with the bill's passage, Africa will become a hotspot for US influence and resources.

Actions:

for policy advocates,
Contact local representatives to express concerns about US foreign policy strategies in Africa (suggested).
Join organizations focused on monitoring and advocating for transparency in foreign policies (exemplified).
</details>
<details>
<summary>
2022-05-19: Let's talk about baby formula and Republicans.... (<a href="https://youtube.com/watch?v=hwuDsv2Nwls">watch</a> || <a href="/videos/2022/05/19/Lets_talk_about_baby_formula_and_Republicans">transcript &amp; editable summary</a>)

Republicans block bill to address baby formula shortage, prioritizing politics over children's well-being.

</summary>

"They'll spend more to make sure they get to sit in that seat than they will to make sure kids have food in this country."
"But they're not going to put any effort behind making sure that child has safe food."
"That doesn't seem very pro-life to me."

### AI summary (High error rate! Edit errors on video page)

Addresses the shortage of baby formula in the United States caused by a plant shutdown and Trump-era trade policies.
Republicans have been criticizing Biden for not doing enough about the baby formula shortage, despite having the power to propose legislation themselves.
The Democratic Party introduced a bill to provide $28 million for FDA inspectors to address the formula shortage, but 192 Republicans in the House voted against it.
Republicans seem more focused on making the American people suffer to blame Biden than on finding solutions.
Biden has taken actions like directing the military to fly in formula and using the DPA to expedite ingredients to address the shortage.
Beau questions why Republicans voted against the bill given the relatively small amount of money involved compared to campaign costs.
Criticizes politicians for spending more on campaigns than on ensuring children have food.
Points out the irony of passing legislation to restrict reproductive rights while neglecting efforts to provide safe food for children.

Actions:

for legislative advocates,
Contact your representatives to advocate for legislation addressing issues like the baby formula shortage (implied).
</details>
<details>
<summary>
2022-05-19: Let's talk about George Bush, Iraq, Ukraine, and mirrors.... (<a href="https://youtube.com/watch?v=x-Vd8RPWdA4">watch</a> || <a href="/videos/2022/05/19/Lets_talk_about_George_Bush_Iraq_Ukraine_and_mirrors">transcript &amp; editable summary</a>)

Beau compares international actions to a mirror, reflecting on the similarities and propaganda tactics used to manufacture consent for wars.

</summary>

"Truth isn't told, it's realized."
"The aggressing country, the invading country, they have to invade that other country because there's that really bad group."
"Truth is always the first casualty of war."
"His mistake probably far more effective than any video that I could put together."
"But it might change the next one."

### AI summary (High error rate! Edit errors on video page)

Compares viewing events in other countries to using a mirror instead of a cartoon villain lens.
Points out similarities between actions taken by the United States and actions condemned in other countries.
Mentions Russia's invasion of Ukraine as an example.
Talks about holding up a mirror through videos to show the reflection of actions.
Emphasizes subtlety in conveying the message so that truth is realized rather than told.
Expresses the challenge of getting the people behind US actions to admit the similarities.
References former President George W. Bush's slip-up in mentioning Iraq instead of Ukraine, hinting at a Freudian slip.
Notes the significance of Bush's slip in reflecting the mirror image.
Draws parallels between propaganda tactics used by different countries to manufacture consent for wars.
Stresses the repetition of similar stories and justifications in different conflicts due to their effectiveness in manipulating public opinion.

Actions:

for global citizens,
Share George W. Bush's slip-up video to raise awareness (suggested)
Encourage others to watch and share the video to prompt realization (suggested)
</details>
<details>
<summary>
2022-05-18: Let's talk about conservatives admitting systemic racism exists.... (<a href="https://youtube.com/watch?v=N0jW_AmNRk4">watch</a> || <a href="/videos/2022/05/18/Lets_talk_about_conservatives_admitting_systemic_racism_exists">transcript &amp; editable summary</a>)

Beau sheds light on the roots of fear and anger perpetuated by systemic racism, questioning how to effectively counter harmful beliefs.

</summary>

"It's not your problem. It's a problem that exists within white people."
"The root core of this belief and of the fear is that systemic racism is real."
"If you believe that this is what will happen, it seems like the answer would be to get rid of the systemic racism."
"I know why it's happening. I don't really know how to counter it, though."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Received a message from someone in Buffalo struggling with anger due to the spread of hatred by media outlets like Fox News.
Views the problem as existing within white people, not something that people who are black can correct.
Believes that systemic racism is the root cause of the fear and anger perpetuated by certain narratives.
Notes that those spreading these theories, like Fox News, may not actually believe them but use them to manipulate their audience.
Explains the fear behind embracing these theories: fear of losing societal edge and fear of being subject to racial inequality.
Suggests that dismantling power structures upholding systemic racism could address these fears but acknowledges the resistance to giving up that societal edge.
Expresses uncertainty about effectively countering these harmful beliefs and fears, recognizing that educating people out of them may take too long.

Actions:

for activists, community members,
Challenge harmful narratives and spread awareness about the root causes of fear and anger (implied).
Support initiatives aimed at dismantling power structures upholding systemic racism (implied).
</details>
<details>
<summary>
2022-05-18: Let's talk about a foreign policy report card for Biden on Ukraine.... (<a href="https://youtube.com/watch?v=wOmfYOdsRRo">watch</a> || <a href="/videos/2022/05/18/Lets_talk_about_a_foreign_policy_report_card_for_Biden_on_Ukraine">transcript &amp; editable summary</a>)

Beau explains Biden's foreign policy actions on Ukraine, critiquing speed and praising team decisions, ultimately impacting global power dynamics positively.

</summary>

"He got Americans to cheer and the world to unite around the idea of economically isolating a country."
"What more do you want from him?"
"It really couldn't have gone better for the U.S."
"He did all of this without directly involving the United States in a war."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains his hesitation to cheerlead for Biden due to Biden not being progressive enough for him.
Addresses a viewer's question about not critiquing Biden's foreign policy decisions regarding Ukraine.
Talks about the foreign policy situation presented to Biden involving a nuclear-armed country invading a non-aligned country.
Describes Biden's actions to unify behind Ukraine, strengthen NATO, and galvanize a coalition to fund and arm Ukraine.
Mentions the economic isolation of Russia and the shift in the balance of power among Russia, China, and the United States.
Critiques the speed of Biden's actions and the piecemeal delivery of equipment in the Ukraine situation.
Acknowledges that Biden's decisions are made by his foreign policy team and credits them for their actions.
Comments on the setup of a second State Department by the Biden administration to handle such international issues.
Concludes with a reflection on the impact of the Biden administration's foreign policy decisions on the global power dynamics.

Actions:

for observers of biden's foreign policy,
Support Ukraine with aid and assistance (implied)
Acknowledge and credit the foreign policy team behind leaders' decisions (exemplified)
</details>
<details>
<summary>
2022-05-18: Let's talk about SCOTUS making it easier to buy a Senator.... (<a href="https://youtube.com/watch?v=NUoKtbuyC8w">watch</a> || <a href="/videos/2022/05/18/Lets_talk_about_SCOTUS_making_it_easier_to_buy_a_Senator">transcript &amp; editable summary</a>)

The Supreme Court's decision enables wealthy donors to buy influence in Congress by allowing post-election repayments, undermining efforts to keep money out of politics and prioritizing the interests of the political establishment over the people.

</summary>

"The Supreme Court decision makes it easier for representatives in Congress to be bought by wealthy donors."
"It is going to do everything it can to make sure the establishment can protect itself and can further enrich themselves at the expense of the country and the people."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court decision makes it easier for representatives in Congress to be bought by wealthy donors by striking down a law limiting post-election campaign repayments.
Politicians can now use their own money to fund their campaigns, win elections, and then accept repayments from donors.
Donors can give money to elected officials after they win, essentially putting cash in their pockets in exchange for potential favors.
The dissenting opinion warns about the heightened risk of corruption when politicians can personally benefit from post-election repayments.
The law that was struck down already allowed for $250,000 post-election repayments, but it wasn't enough for politicians.
Senator Ted Cruz brought the challenge to this law, paving the way for increased corruption in politics.
The Supreme Court's decision undermines efforts to keep money out of politics and ensures that representatives prioritize the wealthy over constituents.
This decision is seen as a way for the political establishment to protect and enrich themselves at the expense of the country and its people.

Actions:

for voters, activists, advocates,
Challenge corrupt campaign finance practices by supporting and advocating for campaign finance reform (suggested)
Hold elected officials accountable by demanding transparency and accountability in their campaign finances (suggested)
</details>
<details>
<summary>
2022-05-17: Let's talk about when the wealthy meet climate change.... (<a href="https://youtube.com/watch?v=hYa19YeYE-s">watch</a> || <a href="/videos/2022/05/17/Lets_talk_about_when_the_wealthy_meet_climate_change">transcript &amp; editable summary</a>)

Wealthy individuals seek exemptions from water usage restrictions in drought-stricken areas, ignoring climate change impacts and stressing the importance of adjusting lifestyles to fit within regionally sustainable parameters.

</summary>

"The response from the wealthy is to try to buy their way out of it. It's not going to work. The water doesn't exist."
"Your footprint in the area that you live has to exist within the confines of what that area can provide."
"Lawns are going to turn brown. You're going to have landscaping that has to come from the native area."
"Everybody knows this is coming. We have to make preparations."
"The funny thing is, this region, it's actually really pretty when you don't mess it up with a bunch of fake landscaping."

### AI summary (High error rate! Edit errors on video page)

Major drought in the southwestern United States leading to water usage regulations.
Las Virginas in California, an area with high water consumption and wealthy residents, imposed water usage restrictions.
Wealthier individuals sought exemptions for koi ponds, car washing, and lawns despite facing climate change impacts.
Urges rethinking landscaping to fit the region's resources and adjust to climate change reality.
Growing food to combat food insecurity and adapting landscaping to be sustainable are key solutions.
Emphasizes that people must adjust their lifestyles to match what the region can sustain.
Wealthier individuals may try to bypass regulations to maintain their lifestyle, impacting less fortunate communities.
Calls for vigilance at the local level as climate change impacts worsen.
Criticizes the surprise displayed by some individuals at being asked to reduce water consumption despite widespread awareness of climate change.
Advocates for preparing for climate change impacts and adjusting lifestyles to reduce stress on resources.

Actions:

for residents, environmentalists, community leaders,
Adjust your landscaping to be more compatible with the region you live in (suggested).
Grow food to combat food insecurity (suggested).
Advocate for sustainable practices in your community (implied).
</details>
<details>
<summary>
2022-05-17: Let's talk about wheat, India, and Ukraine.... (<a href="https://youtube.com/watch?v=4k8jMr2I3sM">watch</a> || <a href="/videos/2022/05/17/Lets_talk_about_wheat_India_and_Ukraine">transcript &amp; editable summary</a>)

The global wheat supply chain disruption due to the Ukraine conflict leads to rising prices and potential food insecurity worldwide.

</summary>

"Food insecurity is a global issue, and in some areas, it is more dramatic than others."
"It's going to be until the next crop."
"When you see prices on these items start to rise even more, well, you're going to know why."

### AI summary (High error rate! Edit errors on video page)

Talks about the impact of the wheat supply chain disruption due to the conflict in Ukraine.
Mentions that India produces wheat but has restricted exports, leading to potential global food insecurity.
Notes that the Russian invasion removed a significant portion of the wheat supply.
Predicts that prices of grains and vegetable oils will increase substantially in the US and Europe.
Warns that in some countries, food might not even be available due to the supply chain disruptions.
States that the United States and India might make exceptions to help areas facing food insecurity.
Emphasizes that the situation will persist until the next crop season, leading to prolonged price increases.
Points out the importance of world leaders addressing supply chain issues to stabilize prices.
Acknowledges the uncertainty regarding the severity and duration of the global food crisis.
Concludes by indicating that the effects of the disrupted wheat supply chain are now being felt globally.

Actions:

for world leaders,
Monitor local food prices and availability to prepare for potential increases (implied).
Support community food banks and organizations aiding those facing food insecurity (implied).
</details>
<details>
<summary>
2022-05-17: Let's talk about more questions about the army's new rifle.... (<a href="https://youtube.com/watch?v=5Pc0cYxopHM">watch</a> || <a href="/videos/2022/05/17/Lets_talk_about_more_questions_about_the_army_s_new_rifle">transcript &amp; editable summary</a>)

Beau explains the implications of the Army's new rifle, challenges misconceptions about gun culture, and addresses America's decline due to nostalgia and ignorance.

</summary>

"The Army realized that during Afghanistan, engagements were testing the limits of the 5.56 round and AR platform."
"Y'all have been saying guns don't kill people. People kill people. I'm blaming the people."
"It's absolutely the gun crowd's fault."

### AI summary (High error rate! Edit errors on video page)

Explains how civilians are getting the new rifle first, clarifying that gun companies develop designs based on Department of Defense criteria and rejected designs often end up in the civilian market.
Details the reasons behind the Army changing rifles, including the need for increased range and power to counter body armor in engagements.
Points out the potential impact of the new rifle on domestic incidents, especially in terms of defeating body armor and altering the dynamics of confrontations.
Responds to a comment attempting to correct misconceptions about the term "AR" in AR-15 and provides historical context about weapon weights during World War II.
Criticizes the blaming of gun violence on gun owners, questioning the lack of personal responsibility and endorsing violence within gun culture.
Attributes America's decline to demonizing education and nostalgia for an idealized past, urging people to embrace progress and move forward.

Actions:

for gun owners, policymakers, activists,
Contact local policymakers to advocate for responsible gun laws and regulations (suggested)
Join community organizations working towards reducing gun violence (implied)
</details>
<details>
<summary>
2022-05-16: Let's talk about why the gun control debate is about to change.... (<a href="https://youtube.com/watch?v=dBuFeSz9AnI">watch</a> || <a href="/videos/2022/05/16/Lets_talk_about_why_the_gun_control_debate_is_about_to_change">transcript &amp; editable summary</a>)

Beau explains why the debate on firearms in the United States is about to change, focusing on the shift from viewing guns as status symbols to tools and the impact of the new XM5 rifle.

</summary>

"Guns are not status symbols. Guns are not toys. They're not things to make you look tough. They're tools."
"This is going to be a game-changer in this debate."
"The high capacity magazine talking point. You don't need 30 rounds."

### AI summary (High error rate! Edit errors on video page)

Addresses the changing debate in the United States around firearms and the upcoming shift in the narrative due to recent developments.
Believes that prohibition, whether in family planning, substances, or guns, does not address root issues causing problems.
Points out that the gun culture in the United States has turned firearms into status symbols rather than tools.
Explains the misconceptions about the AR-15 rifle, clarifying that it is not a high-powered weapon but rather low intermediate power.
Talks about the new rifle, XM5, adopted by the Army, which is more powerful than the AR-15 and could potentially change the gun debate significantly.
Mentions the impact of the new rifle on both the gun control side and the gun crowd, as well as the need for a cultural shift in how firearms are viewed.
Emphasizes the need for responsible gun ownership and understanding that firearms are tools, not symbols of masculinity or power.

Actions:

for gun owners, gun control advocates,
Understand and advocate for responsible gun ownership (implied)
Educate others on the difference between viewing guns as tools versus status symbols (implied)
Advocate for cultural shifts surrounding firearms in the United States (implied)
</details>
<details>
<summary>
2022-05-16: Let's talk about shy community networking.... (<a href="https://youtube.com/watch?v=JPPoImeUKag">watch</a> || <a href="/videos/2022/05/16/Lets_talk_about_shy_community_networking">transcript &amp; editable summary</a>)

Be part of something bigger by finding your niche in community networks, even if you're shy or prefer online interactions.

</summary>

"There is no skill that is unneeded in the fight to make the world better."
"You just have to find the right little cubby for you, the right group for you, the right way for you to help."
"No matter how hard it may seem or how challenging it might be in the beginning, there's a spot for you."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of shy community networks and the challenges of real-world interpersonal interactions.
Recalls a personal story from the late 1900s about a shy roommate in a community network.
Describes how the shy roommate, skilled in computers, helped with graphic design and creating a website for activist groups.
Explains how the group of online computer-savvy individuals made a positive impact by working together virtually.
Mentions how a therapist suggested they move their activities into the real world for environmental cleanup efforts.
Emphasizes the importance of finding a liaison or connection to be part of something bigger, even if you are shy.
Concludes by encouraging everyone to overcome reasons not to participate, as there is a need for every skill set to make the world better.

Actions:

for community members,
Find a niche within a community network (implied)
Offer your skills to help a cause (implied)
Participate in virtual or real-world activities based on your comfort level (implied)
</details>
<details>
<summary>
2022-05-16: Let's talk about Ukraine invading Russia.... (<a href="https://youtube.com/watch?v=IpXRW-wmBTY">watch</a> || <a href="/videos/2022/05/16/Lets_talk_about_Ukraine_invading_Russia">transcript &amp; editable summary</a>)

Ukraine's counteroffensive reaches the Russian border, symbolically significant but unlikely to advance further, sticking to their successful strategy.

</summary>

"Ukraine's successful counteroffensive has led to Ukrainian troops reaching the Russian border."
"Extending to the Russian border is symbolic but not strategically significant."
"Ukraine is likely to stick to their current successful strategy."

### AI summary (High error rate! Edit errors on video page)

Ukraine's successful counteroffensive in the northern portion of the Eastern Front near Izium has led to Ukrainian troops reaching the Russian border.
Media reports framing it as Ukraine pushing Russian troops back into Russia are inaccurate; Russian forces moved south, still in Russian-occupied Ukraine.
While a symbolic victory, extending to the Russian border, strategically, this isn't a significant development in the long-term course of the war.
The possibility of Ukraine advancing into Russian territory exists, but it's unlikely given the challenges of holding conquered territory.
The counteroffensive is expected to turn south to apply pressure on Russian forces in Ukraine, not to advance into Russian villages.
Ukraine's leadership is likely to stick to their current successful strategy rather than attempting risky advances into Russian territory.
Russia's threats towards Finland for joining NATO are seen as typical rhetoric, especially during their current war with Ukraine.

Actions:

for foreign policy analysts,
Support organizations providing aid to those affected by the conflict in Ukraine (suggested)
Advocate for diplomatic solutions and peace negotiations in the region (implied)
</details>
<details>
<summary>
2022-05-15: Let's talk about how South Korea learned to love the bomb.... (<a href="https://youtube.com/watch?v=H_w71xvwH1A">watch</a> || <a href="/videos/2022/05/15/Lets_talk_about_how_South_Korea_learned_to_love_the_bomb">transcript &amp; editable summary</a>)

South Korea's new president's posture may escalate tensions with North Korea, but negotiation without nuclear weapons is possible amid regional dynamics and economic incentives.

</summary>

"Around 70 percent of South Koreans support the South developing nuclear weapons."
"Negotiation and accommodation without nuclear weapons on the peninsula are possible."
"Most countries aren't actually cartoon villains."

### AI summary (High error rate! Edit errors on video page)

South Korea's new conservative president may escalate tensions with North Korea in the competition between the US and China.
Around 70% of South Koreans support developing nuclear weapons for deterrence.
The South Korean president wanted the US to put nukes back in South Korea, which was not openly agreed upon.
The purpose of a nuclear deterrent is for it to be known, yet the US could secretly place nukes in South Korea.
Economic incentives have been offered to North Korea to abandon its nuclear ambitions.
After Russia's invasion of Ukraine, nuclear powers are seen as less likely targets for invasion.
Negotiation and accommodation without nuclear weapons on the peninsula are possible, despite North Korea being portrayed negatively.
Trump attempted negotiations with North Korea but was unsuccessful.
Confrontation and saber-rattling are not seen as effective ways to keep nuclear weapons off the Korean peninsula.
The details of economic packages and incentives from South Korea might influence North Korea's leadership.
There is potential for dialogues and negotiations with North Korea, as most countries aren't "cartoon villains."
These developments will have long-term implications for the region, involving various approaches to North Korea by neighbors and global powers.

Actions:

for global policymakers,
Monitor developments in the Korean peninsula and advocate for peaceful negotiations (exemplified)
Support initiatives that focus on economic incentives for denuclearization in North Korea (exemplified)
</details>
<details>
<summary>
2022-05-15: Let's talk about a problem at an intelligence school.... (<a href="https://youtube.com/watch?v=tYuc2VRU47Q">watch</a> || <a href="/videos/2022/05/15/Lets_talk_about_a_problem_at_an_intelligence_school">transcript &amp; editable summary</a>)

Bowie Gun stresses the need for the US military to combat misogyny and dehumanization to enhance intelligence capabilities and reduce civilian casualties.

</summary>

"Because their counterparts who are women, well they'll be underutilized because they're undervalued, because they're underestimated, because of the misogynistic attitude."
"How does that happen normally? In real life, it isn't, you know, the sergeant gets hit and then the lower enlisted just lose it on an entire village. That happens, but it's super rare."

### AI summary (High error rate! Edit errors on video page)

Addresses the need for the US military to become more "woke" by having basic respect for others.
Describes a Marine School for Intelligence where instructors were investigated for harassment and misogynistic behavior.
Mentions inappropriate relationships, a chat room with misogynistic attitudes, and a general tone of acceptance towards such behavior.
Points out the negative impact of this behavior on female students, potentially discouraging them from pursuing intelligence roles.
Warns that male students may adopt misogynistic attitudes, leading to undervaluing their female counterparts.
Emphasizes the danger in deploying individuals who underestimate their female counterparts due to misogyny.
Notes that women can excel in intelligence roles as they are often underestimated and can access certain information more effectively than men.
Criticizes the lack of adequate corrective action taken in the Marine School for Intelligence.
Raises concerns about the dehumanizing nature of militarism and its implications for civilian casualties.
Stresses the importance of proper intelligence in minimizing civilian losses during military operations.
Calls for the military to address dehumanizing behaviors to prevent further civilian harm.

Actions:

for military personnel, advocates for gender equality,
Advocate for gender sensitivity training in military institutions (suggested)
Support initiatives that address misogyny and harassment in military training (exemplified)
</details>
<details>
<summary>
2022-05-15: Let's talk about Russia's GRU taking over.... (<a href="https://youtube.com/watch?v=Sy9CehvOy3U">watch</a> || <a href="/videos/2022/05/15/Lets_talk_about_Russia_s_GRU_taking_over">transcript &amp; editable summary</a>)

Beau breaks down the shift from the FSB to GRU in Russia, noting internal politics over battlefield impact.

</summary>

"The time this would have mattered was in early February."
"It's beyond its use date."
"The skills that they have honed, what they are famous for, it's not really useful in a war zone."
"This is more internal politics and a show internally than anything that's going to impact the battlefield."
"Just who's running the show now."

### AI summary (High error rate! Edit errors on video page)

Explains the news out of Russia regarding Ukraine, comparing the FSB and GRU.
Describes the GRU's specialization in running illegals, spies without diplomatic cover.
Notes that the GRU may be more capable in performing advanced operations and has more assets and agents overseas.
Clarifies that the GRU's expertise lies in peacetime rather than wartime operations.
Points out that the GRU's capabilities, while impressive, may not be relevant in a wartime setting.
Suggests that the GRU's change in status is more about internal politics than battlefield impact.
Mentions that Putin's ties to the FSB add weight to the significance of the change.
Emphasizes that the shift from FSB to GRU leadership may not result in significant changes on the battlefield.
Concludes that the leadership change is more about who is in charge rather than tactical implications for Ukraine.

Actions:

for foreign policy analysts,
Stay informed about international relations and shifts in intelligence agencies (implied)
</details>
<details>
<summary>
2022-05-14: Let's talk about baby formula, Biden, and solutions.... (<a href="https://youtube.com/watch?v=R7vVDUfqbOY">watch</a> || <a href="/videos/2022/05/14/Lets_talk_about_baby_formula_Biden_and_solutions">transcript &amp; editable summary</a>)

Beau addresses the baby formula shortage controversy, criticizes government intervention as a merger of state and corporate power, and warns against excessive reliance on the White House for solutions.

</summary>

"You're literally advocating for fascism just to throw that out there."
"They're not supposed to be looking to the White House to have dear leader tell them what to do."
"We have to stop looking to the White House to solve every single problem that comes along."
"It's not a good system."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing the controversy surrounding the shortage of baby formula and criticism towards Biden for not doing enough.
Criticizing the idea of government intervention in directing corporations on what to do as a merger of state and corporate power.
Drawing parallels between advocating for government control over corporations and fascism, mentioning Benito Mussolini and corporatism.
Pointing out that members of Congress should be introducing legislation to address issues like the baby formula shortage instead of looking to the White House for solutions.
Suggesting that breaking up monopolies to prevent widespread issues like a recall impacting half the country could be a solution.
Expressing concern over the trend of people looking to the White House as the ultimate solution to all problems, including inflation.
Questioning the actions of governors and members of Congress who are quick to ask what the White House is doing about inflation without taking steps within their own states to address the issue.
Warning against the dangers of constantly relying on the White House to solve every problem, leading to excessive control by one person.

Actions:

for american citizens,
Advocate for legislation to address issues like the baby formula shortage (implied).
Support efforts to break up monopolies that control significant portions of the market (implied).
Take steps within your own state to help with issues like inflation, such as reducing gas taxes (implied).
</details>
<details>
<summary>
2022-05-14: Let's talk about Ukraine, Finland, and gofundme.... (<a href="https://youtube.com/watch?v=8lKuLv_Lypw">watch</a> || <a href="/videos/2022/05/14/Lets_talk_about_Ukraine_Finland_and_gofundme">transcript &amp; editable summary</a>)

Developments in Ukraine show strategic setbacks for Russia, with Finland eyeing NATO and Ukrainian forces making progress against Russian troops armed with outdated rifles.

</summary>

"War is a continuation of politics by other means."
"A major military holding a GoFundMe for equipment is not indicative of future success."

### AI summary (High error rate! Edit errors on video page)

Finland is looking to join NATO, a move that Russia sees as a worst-case scenario.
Russia's intervention in Ukraine aimed to seize territory and weaken NATO's influence, but it backfired.
The war in Ukraine has produced outcomes opposite to what Russia desired strategically.
Russia threatened to cut off electrical flows to Finland in response to its potential NATO membership.
Turkey and other NATO countries may oppose Finland's NATO membership, leading to potential secondary alliances.
The Ukrainian counteroffensive is making progress, pushing Russian forces back.
Ukrainian forces have received artillery and equipment aid from the US and the Dutch, now active on the front lines.
Russian proxy forces armed with bolt-action rifles are appearing in the conflict, a surprising choice for modern warfare.
Russian telegram channels are fundraising for troops' equipment, indicating a lack of proper resources.
Overall strategic developments include countries moving towards NATO, Ukrainian counteroffensive progress, and static Russian forces.
The situation suggests future developments favoring NATO countries and Ukraine, with Russian forces facing challenges.

Actions:

for international observers, policymakers,
Support Ukrainian humanitarian efforts by donating to reputable organizations (implied)
Advocate for peaceful resolutions to international conflicts through activism and awareness campaigns (implied)
</details>
<details>
<summary>
2022-05-14: Let's talk about Trump, Governors, and Georgia.... (<a href="https://youtube.com/watch?v=uvzXsI3NqpU">watch</a> || <a href="/videos/2022/05/14/Lets_talk_about_Trump_Governors_and_Georgia">transcript &amp; editable summary</a>)

Trump's endorsements may not hold as much sway as media suggests, with Georgia's primary results being a key indicator of his influence.

</summary>

"Trump's endorsement may not be what is deciding a lot of these races."
"Trump's name, despite the media coverage, may not be carrying the influence that that coverage suggests."
"His name is so toxic among a large segment of the normal Republican party."
"As you hear more and more reporting talking about Trump's status and his endorsements and how influential he is, I would wait to see what happens in Georgia first."

### AI summary (High error rate! Edit errors on video page)

Trump is endorsing candidates, focusing on Georgia to control the state-level Republican Party apparatus.
Despite media hype about Trump's endorsements, his candidate lost in Ohio and Nebraska.
In Georgia, Trump's endorsed candidate is trailing the incumbent, reflecting potential waning influence.
In West Virginia, two incumbent Republican congressional reps faced off due to district mergers.
Trump's endorsement in West Virginia primary didn't guarantee a win, as it boiled down to turnout.
Reports suggest Trump's name may be toxic among some Republicans, leading independents to vote against his endorsed candidate in Georgia.
Influence of Trump's endorsements may not be as significant as media portrays, especially in major races like Georgia.

Actions:

for political analysts, voters,
Monitor the outcome of the Georgia primary elections to gauge the true impact of Trump's endorsements (suggested).
</details>
<details>
<summary>
2022-05-13: Let's talk about why I look left and expectations.... (<a href="https://youtube.com/watch?v=f6-H0UyoGPg">watch</a> || <a href="/videos/2022/05/13/Lets_talk_about_why_I_look_left_and_expectations">transcript &amp; editable summary</a>)

Beau explains his anti-authoritarian stance, rejects accelerationism, values impact over views, and stresses the difficulty but importance of building a better world.

</summary>

"I started looking left because I was always anti-authoritarian and then I got really tired of watching people suffer and there's nothing on the right to combat that."
"I cannot get behind that, period."
"Not every video is for every person."
"It's worth doing because it's hard. It's worth doing because it's the right thing to do."
"Encourage the suffering to get to the point where they learn their lesson. I will never sign off on that."

### AI summary (High error rate! Edit errors on video page)

Explains why he started looking left politically: anti-authoritarianism and a desire to prevent suffering.
Rejects the idea of accelerationism, which aims for suffering to push political awakening.
Expresses disappointment in lower views on community networking videos, but values potential impact on even a small percentage of viewers.
Believes in the importance of building a better world, even though it will be challenging.
Emphasizes that changing the world is difficult but necessary to prevent suffering and create positive change.

Actions:

for activists, community builders, supporters,
Start a community network (exemplified)
Engage in building a better world through actions that prevent suffering and create positive change (suggested)
</details>
<details>
<summary>
2022-05-13: Let's talk about coffee shops and labor.... (<a href="https://youtube.com/watch?v=7tDZEgnNq9A">watch</a> || <a href="/videos/2022/05/13/Lets_talk_about_coffee_shops_and_labor">transcript &amp; editable summary</a>)

Starbucks faces allegations of unfair business practices and violations as unions fight for workers' rights – the power of collective action against corporate resistance.

</summary>

"There is power in numbers. There's safety in numbers."
"It represents power. Collective action will often get the goods."

### AI summary (High error rate! Edit errors on video page)

The National Labor Relations Board (NLRB) filed a complaint against Starbucks in Buffalo, alleging 29 unfair business practices and 200 violations of the National Labor Relations Act.
Starbucks was accused of trying to prevent unions from forming and putting pressure on them against the law.
In Memphis, the NLRB filed a petition to reinstate seven employees due to Starbucks' conduct interfering with their federally protected rights.
Kathleen McKinney from the NLRB emphasized the need for immediate relief to prevent harm to the campaign in Memphis and to ensure all Starbucks workers can freely exercise their labor rights.
The power of unions lies in collective action, as demonstrated by big businesses' efforts to prevent their formation and the resources they invest in doing so.
Companies go to great lengths, including hiring consultants, conducting surveillance, and holding mandatory meetings, to combat unionization because they recognize the threat unions pose to their control.
The significant monetary investment made by companies to impede unions showcases the effectiveness and importance of collective action in improving working conditions.
Unions represent power and protection for workers, leading to better conditions that companies wouldn't be concerned about if unions were ineffective or insignificant.
The collective power of unions is substantial, with the ability to negotiate for better wages, benefits, and working conditions through unified action.
The resistance and tactics employed by companies against unionization underscore the threat unions pose to the status quo and the transformative potential of collective organizing.

Actions:

for workers, activists, advocates,
Support unionization efforts in your workplace to protect workers' rights and improve working conditions (exemplified)
Educate fellow employees on the benefits and importance of collective action through union representation (exemplified)
</details>
<details>
<summary>
2022-05-13: Let's talk about a cliff analogy.... (<a href="https://youtube.com/watch?v=hAWppTCyOB4">watch</a> || <a href="/videos/2022/05/13/Lets_talk_about_a_cliff_analogy">transcript &amp; editable summary</a>)

Beau presents a scenario to counter false equivalencies against family planning, urging a focus on bodily autonomy and viability rather than arbitrary debates on when life begins.

</summary>

"Can you force a person to do something with their own body against their will?"
"The real question is about bodily autonomy."
"Don't get into debates over when it starts, because it doesn't matter."
"The question is, when is it viable on its own?"
"There isn't really the moral question that they're trying to present."

### AI summary (High error rate! Edit errors on video page)

Introduces a hypothetical scenario to counter arguments against family planning.
Advocates for focusing the debate on bodily autonomy rather than false equivalencies.
Describes a scenario involving a fertilized egg and a baby named Billy hanging over a cliff to illustrate the difference.
Emphasizes that the argument of viability is key in discussing moral questions surrounding abortion.
Advises always redirecting the debate back to bodily autonomy.
Dismisses debates over when life begins as not scientific and irrelevant compared to viability.
Stresses the importance of understanding moral questions change at the point of viability.

Actions:

for advocates for reproductive rights.,
Redirect debates on family planning back to bodily autonomy (suggested).
Focus on the question of viability in the abortion debate (suggested).
</details>
<details>
<summary>
2022-05-12: Let's talk about what Republicans said with the vote in the Senate.... (<a href="https://youtube.com/watch?v=q7pc7jfQh8E">watch</a> || <a href="/videos/2022/05/12/Lets_talk_about_what_Republicans_said_with_the_vote_in_the_Senate">transcript &amp; editable summary</a>)

Republicans in the Senate voted against turning Roe into federal law, endangering thousands of women's lives, prioritizing talking points over constituents.

</summary>

"The Republican Party as a whole just voted to endanger the lives of tens of thousands of women per year."
"This is the Republican Party actively targeting Republican women."
"They put party over people. They put a talking point over the lives of people."
"Women will die. Women will die."
"Because the Republican Party doesn't have a backbone, because the Republican Party doesn't have any principle."

### AI summary (High error rate! Edit errors on video page)

Explains the recent Senate vote on turning Roe into federal law and its failure.
Notes that every Republican senator, along with Joe Manchin, voted against the legislation.
Points out that without federal legislation, the decision reverts back to the states.
Emphasizes the lack of exemptions for ectopic pregnancies in many state laws if Roe is overturned.
Describes the risks of ectopic pregnancies, including infection, internal bleeding, and death.
Criticizes the Republican Party for endangering the lives of tens of thousands of women by not including exemptions for ectopic pregnancies.
Condemns the Republican Party for valuing a talking point over the lives of their own constituents.
Raises concerns about Republican women being disproportionately affected by these laws.
Suggests that decisions on women's reproductive rights are being made by "mediocre men" influenced by lobbyists.
Urges Republican women to reconsider supporting senators who endanger their lives for political gains.

Actions:

for republican women,
Mobilize within the Republican Party to demand exemptions for ectopic pregnancies in state laws (implied).
Advocate for stronger protections for women's reproductive rights within the party (implied).
</details>
<details>
<summary>
2022-05-12: Let's talk about Quadry Sanders and Oklahoma.... (<a href="https://youtube.com/watch?v=8nLV05YvEz8">watch</a> || <a href="/videos/2022/05/12/Lets_talk_about_Quadry_Sanders_and_Oklahoma">transcript &amp; editable summary</a>)

Analyzing a perplexing shooting in Oklahoma where officers face charges, but justice may be questioned due to the unexplainable nature of the events.

</summary>

"The shooting is unexplainable and perplexing."
"I do not understand why they fired."
"This is the most unexplainable shooting I've ever seen."
"I have a high expectation that there's going to be convictions on this."
"But the normal defenses that officers use are not really going to help in this one."

### AI summary (High error rate! Edit errors on video page)

Analyzing the Sanders case in Oklahoma where two cops shot and killed someone.
Usually, after-action analyses focus on policy deviations leading to bad outcomes, but this case is different.
The shooting is unexplainable and perplexing, with officers already arrested and charged with manslaughter.
The severity of the charges gives a high likelihood of conviction, but sentences for taking a life in Oklahoma are relatively low.
The minimum sentence on the charge is four years, requiring 85% of the sentence to be served before release.
The video footage doesn't provide a clear understanding of why the officers fired, making it one of the most inexplicable shootings.
The charge presupposes that the officers panicked and fired, potentially leading to convictions despite normal justifications being insufficient.
Despite the expected convictions, there may be public outcry over the sentencing.
Beau expresses disbelief and confusion over the events, unable to comprehend why the officers fired.
This case stands out as exceptionally puzzling and raises doubts about whether justice will be perceived.

Actions:

for advocates, activists, reformers,
Advocate for transparent investigations into police shootings (implied)
Support initiatives for police accountability and training (implied)
</details>
<details>
<summary>
2022-05-12: Let's talk about Manchin, reactions and responses, and systems.... (<a href="https://youtube.com/watch?v=N-q4HtfgHIg">watch</a> || <a href="/videos/2022/05/12/Lets_talk_about_Manchin_reactions_and_responses_and_systems">transcript &amp; editable summary</a>)

Beau explains why focusing on power structures over individuals like Joe Manchin is key to creating lasting change and urges for strategic responses over reactions for effective progress.

</summary>

"Reacting to something is bad. You have to respond to it."
"The easiest way to make this transition is to ask what's next."
"You have to defeat that structure. The easiest way to do that is to build a better one."
"We can't react. We have to respond if you want to win."
"What he cares about is maintaining power, and he needs that structure to do it."

### AI summary (High error rate! Edit errors on video page)

Explains why he focused on the Republican Party in a recent video about a Senate vote, rather than Joe Manchin, due to the power structure in place.
Points out that the real issue lies within a power structure that values talking points over people's lives, specifically within the Republican Party.
Emphasizes the importance of moving from reacting to news to responding by considering the next steps and outcomes.
Stresses the need to understand and work within the existing power structures to bring about change effectively.
Suggests that simply removing Joe Manchin from the Democratic Party won't solve the underlying issue of power dynamics.
Advocates for building alternative power structures that are more connected to the people to create meaningful progress.
Encourages focusing on long-term civic engagement and building networks to influence and change existing power structures.
Argues that defeating the current power structure requires constructing a stronger and more people-centric alternative.
Warns against reactive responses to emotional news and advocates for strategic and thoughtful actions to achieve desired outcomes.

Actions:

for change-makers, activists, voters,
Build alternative power structures connected to the community to challenge existing power dynamics (implied).
Focus on long-term civic engagement and network building to influence and change power structures (implied).
Avoid reactive responses to emotional news and instead strategize thoughtful actions for desired outcomes (implied).
</details>
<details>
<summary>
2022-05-11: Let's talk about midterm math for Democrats.... (<a href="https://youtube.com/watch?v=MmoINamsvb4">watch</a> || <a href="/videos/2022/05/11/Lets_talk_about_midterm_math_for_Democrats">transcript &amp; editable summary</a>)

Beau breaks down how midterm elections hinge on voter turnout, urging against actions that discourage participation and potentially harm vulnerable groups.

</summary>

"The midterm election is a moment where Democrats can come out ahead."
"Don't do it. Don't do it."
"Depressing voter turnout on the side that is going to protect the people you say are your allies."
"Even in a jurisdiction that has a 10-point lead built in for Republicans, Democrats can win in the midterms."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of midterm elections as a moment for one party to gain an advantage by putting in effort.
Illustrates the impact of voter turnout in midterm elections using a hypothetical voting jurisdiction with 167,000 voters.
Breaks down the scenario of a gerrymandered district giving Republicans a 10-point lead and the effect of voter turnout on election results.
Points out that a decrease in voter turnout during midterms can lead to unfavorable outcomes for certain groups.
Stresses the importance of encouraging voter participation, particularly among Democrats, to secure wins in elections.
Shows how even in districts with a built-in advantage for Republicans, Democrats can win if their voters show up in larger numbers during midterms.
Urges against spreading messages that discourage voting, especially if it undermines protecting the rights of vulnerable groups.
Emphasizes the critical role of midterm elections in determining the preservation or loss of rights and freedoms for many individuals.

Actions:

for voters, advocates,
Encourage voter turnout among Democrats to secure wins in elections (suggested).
Avoid spreading messages that discourage voting, especially if it undermines protecting the rights of vulnerable groups (suggested).
</details>
<details>
<summary>
2022-05-11: Let's talk about inflation and Biden.... (<a href="https://youtube.com/watch?v=-PvFtnzeImU">watch</a> || <a href="/videos/2022/05/11/Lets_talk_about_inflation_and_Biden">transcript &amp; editable summary</a>)

Republicans blame Biden for inflation, but it's a global issue; misinformation traps the Republican base in an information silo.

</summary>

"Republicans are blaming Biden for inflation, but it's actually a global issue."
"The Republican Party has successfully created a base that exists in a total information silo."
"Nobody who loves America wants its populace uneducated."

### AI summary (High error rate! Edit errors on video page)

Republicans are blaming Biden for inflation, but it's actually a global issue.
Inflation rates are record-breaking, with fuel prices up by 35% in the last 12 months.
Building materials have increased by 15.4% over a year ago.
Consumer durables like cars and furniture are experiencing the fastest inflation in three decades.
Grocery prices have surged, with fruits and vegetable prices up by 6.75%.
China is somewhat insulated from global inflation due to price controls.
Economists predict that inflation may be peaking, but their accuracy is questionable.
Russian inflation is at 20%, and the eurozone at 7.5%.
The Republican base is unaware of the global nature of inflation, influenced by misinformation.
The Republican Party has created a base in an information silo, pushing false narratives about Biden causing global inflation.

Actions:

for american voters,
Educate others on the global nature of inflation (implied)
Share accurate information with those influenced by misinformation (implied)
</details>
<details>
<summary>
2022-05-11: Let's talk about Natives, history, and volume being released.... (<a href="https://youtube.com/watch?v=IDUg7ezeBsU">watch</a> || <a href="/videos/2022/05/11/Lets_talk_about_Natives_history_and_volume_being_released">transcript &amp; editable summary</a>)

The Secretary of the Interior's report sheds light on the harrowing history of boarding schools for indigenous children, stressing the importance of understanding and addressing these atrocities for a better future.

</summary>

"Understanding these types of actions and making sure that they don't happen again is what will shape this country."
"We have to go through it. We have to understand it. We have to make sure it never happens again."

### AI summary (High error rate! Edit errors on video page)

Secretary of the Interior released a report on federal government's role in boarding schools for indigenous children, parts of American history concealed and overlooked.
Report examines the 1819 Civilization Fund Act authorizing stripping indigenous people of their identities, culture, spirituality, hair, names, and families.
Schools were horrific with unmarked graves and tragic stories.
Purpose isn't just to dwell on historical wrongs but to pave the way for healing and closure.
Legislation will be discussed to establish a commission similar to Canada's to address this history.
Some resist discussing this history as it challenges the American myth taught in schools.
Acknowledging and understanding these atrocities is vital to shaping the country's future.

Actions:

for educators, activists, legislators,
Establish legislation to create a commission for resolution and closure (implied)
Advocate for the inclusion of this history in educational curriculums (implied)
</details>
<details>
<summary>
2022-05-10: Let's talk about turnkey community networks.... (<a href="https://youtube.com/watch?v=z0BHKvaiRE8">watch</a> || <a href="/videos/2022/05/10/Lets_talk_about_turnkey_community_networks">transcript &amp; editable summary</a>)

Beau stresses the importance of activating existing community networks and forming alliances to create a stronger collective impact for progressive causes.

</summary>

"There already are power structures that you can ally with or recruit from."
"When you start combining your network with others, that's when that block develops."
"It's better if everybody involved isn't close friends to begin with."
"You show up for them, they show up for you type of thing."
"There are more of these videos coming."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of community networks, specifically turnkey networks that need activation or mutual assistance agreements.
Mentions the example of the Republican Party using the Evangelical Church to push their agenda and how similar power structures exist on the progressive side.
Suggests looking into nonprofits, newly unionized places, art galleries, gay clubs, liberal churches, student groups, environmental clubs, and more as potential allies.
Points out the value of combining networks to create a stronger collective impact beyond just voting power.
Recommends utilizing social media to connect with like-minded individuals and expand networks.
Advises on the benefit of not starting with a network of close friends to maximize reach and impact.
Encourages viewers to recognize existing power structures they can collaborate with or recruit from and teases more upcoming videos on the topic.

Actions:

for community organizers, activists,
Join a nonprofit or community organization to connect with like-minded individuals (implied).
Establish connections with newly unionized places for mutual support (implied).
Collaborate with art galleries, gay clubs, liberal churches, student groups, environmental clubs, and other organizations to build a network (implied).
Utilize social media to find and connect with potential allies (implied).
Encourage diverse connections within networks to maximize reach and impact (implied).
</details>
<details>
<summary>
2022-05-10: Let's talk about civic engagement, Roe, and electoral politics.... (<a href="https://youtube.com/watch?v=EUApNV7S-Ig">watch</a> || <a href="/videos/2022/05/10/Lets_talk_about_civic_engagement_Roe_and_electoral_politics">transcript &amp; editable summary</a>)

Beau explains types of civic engagement, defends electoralism's role in preventing harm, and urges concrete actions over mere criticism.

</summary>

"If you want to get out there and talk about these other methods, these other types of civic engagement, and promote them, I get it."
"Electoralism isn't the best tool for deep systemic change, but it can stop harm."
"At the bare minimum, it holds the line."
"Just screaming that the Democrats didn't do enough and that there's no reason to vote, that's not helping."
"There are people who are going to be in harm's way because of this."

### AI summary (High error rate! Edit errors on video page)

Explains different types of civic engagement, including capacity building, community networking, direct service, research, advocacy and education, electoralism, personal responsibility, philanthropy, and participation in association.
Addresses the criticism of electoralism and its effectiveness in creating change.
Points out the importance of electoralism in preventing harm and holding the line, especially in the context of the jeopardy faced by Roe.
Challenges the notion that electoralism is a dead end for change and showcases its role in influencing laws and policies.
Emphasizes the need for concrete steps and actions rather than just criticizing electoralism without providing alternatives.
Raises awareness about the impact of reduced voter turnout on vulnerable populations facing potential harm.

Actions:

for activists, voters, community members,
Promote different types of civic engagement and encourage community involvement (implied)
Take concrete steps to address immediate needs, create long-term solutions, and mitigate harm in your community (implied)
Advocate for causes through demonstrations, petitions, and public education efforts (implied)
Ensure voter turnout by educating and motivating individuals to participate in elections (implied)
</details>
<details>
<summary>
2022-05-10: Let's talk about Britney Spears being every woman in the US.... (<a href="https://youtube.com/watch?v=6YXvuIET2rI">watch</a> || <a href="/videos/2022/05/10/Lets_talk_about_Britney_Spears_being_every_woman_in_the_US">transcript &amp; editable summary</a>)

Britney Spears' situation epitomizes the ongoing battle for women's autonomy and the underlying desire for control over their choices in society.

</summary>

"It's about control. It always has been. Everything else, that's just camouflage."
"It's about returning women to a subservient class where they have to do what they're told."
"It's about preserving mediocre men's power over women."
"If you think it's about anything other than control, you're wrong."
"Freedoms that should be extended to everybody at all times."

### AI summary (High error rate! Edit errors on video page)

Britney Spears' situation mirrors that of every woman in the United States right now, as she faced backlash for posting photos in her birthday suit.
Despite being 40 years old, Britney is not trusted with bodily autonomy and is seen as needing someone else to make decisions for her.
There is a push to return women to a legal structure where others control their choices, reminiscent of historical restrictions on women's freedom.
The argument to overturn Roe v. Wade is not about moral reasons but about control and limiting women's autonomy.
The double standard applied to Britney showcases society's desire to control women and restrict their agency.
The focus on controlling women is about preserving power for mediocre men over women, rather than genuine concerns about family planning.
The criticism and attempts to control Britney reveal deeper societal issues around gender roles and power dynamics.
Restrictions on women's autonomy are not equally applied and are often rooted in outdated stereotypes and biases.
The push to control women's choices extends beyond family planning and into various aspects of their lives.
The underlying issue is about control and maintaining power dynamics that favor certain groups over others.

Actions:

for advocates for gender equality,
Challenge and push back against attempts to control and restrict women's autonomy (implied)
Support organizations and movements that advocate for women's rights and autonomy (suggested)
</details>
<details>
<summary>
2022-05-09: Let's talk about the parade in Russia and developments.... (<a href="https://youtube.com/watch?v=atmY8RDPkYw">watch</a> || <a href="/videos/2022/05/09/Lets_talk_about_the_parade_in_Russia_and_developments">transcript &amp; editable summary</a>)

May 9th parade in Russia sparks speculation but ends with Putin's speech on Ukraine, reminding of the human cost of conflict.

</summary>

"Don't let talking heads convince you that something really, really bad is right around the corner because there's going to be a parade."
"Keep calm and carry on type of thing right now."
"When you see that many people and you visualize that and all of the families that were impacted, it's a good tool to visualize the human cost of wars like this."

### AI summary (High error rate! Edit errors on video page)

May 9th parades in Russia are a significant source of national pride but speculation about potential announcements during this year's parade did not materialize.
Speculation ranged from full mobilization to a formal declaration of war, but ultimately, nothing major happened except for the parade and Putin's speech framing the invasion of Ukraine as defensive.
The media's readiness for the conflict to end contributes to the spread of speculation as there are no dynamic developments drawing viewers.
Lack of on-the-ground sources leads to speculation spreading rapidly, indicating a need for caution regarding media reports.
Media outlets reporting on speculation in the absence of actual developments may lead to unnecessary alarm and should be approached with skepticism.
Beau advises against being swayed by sensationalist reporting and urges to remain calm and composed amidst uncertain situations.
The visualization of 11,000 troops in the Moscow parade serves as a stark reminder of the human cost of war, being fewer than the lowest estimates of Russian troop losses in the conflict.
Beau underscores the importance of recognizing the human toll of conflicts like these, particularly on civilians who often bear the brunt of the consequences.

Actions:

for media consumers,
Visualize and acknowledge the human cost of war by reflecting on the impact on families and communities (implied).
</details>
<details>
<summary>
2022-05-09: Let's talk about community networks and what you can do.... (<a href="https://youtube.com/watch?v=kQWp5xRelI0">watch</a> || <a href="/videos/2022/05/09/Lets_talk_about_community_networks_and_what_you_can_do">transcript &amp; editable summary</a>)

Beau shares insights on community networking using Hurricane Michael relief efforts to illustrate the impact of individual contributions and the necessity of leveraging diverse skills in crisis situations.

</summary>

"Your skill, whatever it is, is needed."
"It's all a matter of applying what you're good at to making things better for everybody."
"The solution to the problems that we're going to be facing has centered where you're at."
"There's always a way you can help."
"What can I do? And it's specific."

### AI summary (High error rate! Edit errors on video page)

Talks about community networking and addresses the common question of "What can I do?" when it comes to helping out.
Uses Hurricane Michael relief efforts as an example to demonstrate the power of community networks in disaster relief.
Describes how people from various networks came together to provide aid after the hurricane hit Panama City.
Explains the different roles individuals played in the relief efforts, from collecting supplies to monitoring social media for requests and road updates.
Emphasizes the importance of utilizing one's skills and resources in crisis situations.
Acknowledges the valuable contributions of individuals who may not physically participate but support through other means like donations.
Stresses the significance of multiple networks collaborating to achieve greater impact in addressing challenges.
Encourages individuals to recognize their skills and find ways to contribute to making things better for everyone, regardless of their abilities.
Mentions the necessity of local networks in mitigating harm and solving problems faced by communities.
Concludes by affirming that everyone's skill set is valuable and needed in community efforts.

Actions:

for community members,
Join local community organizations to build networks and support disaster relief efforts (exemplified)
Organize collection drives for supplies in preparation for future emergencies (suggested)
Monitor social media for real-time needs and updates during crises (exemplified)
</details>
<details>
<summary>
2022-05-08: Let's talk about community networking 101.... (<a href="https://youtube.com/watch?v=TZJuWScu3qQ">watch</a> || <a href="/videos/2022/05/08/Lets_talk_about_community_networking_101">transcript &amp; editable summary</a>)

Beau explains the importance of community networks, focusing on inclusivity, versatility, and collaboration to achieve positive outcomes.

</summary>

"A community network is a versatile and effective way to bring people together to help the community."
"Bringing different social circles together is key to creating a network that can achieve positive outcomes."
"If you're going to try to start doing this, your first step is to sit down and think about all of the people you know who might be like-minded."

### AI summary (High error rate! Edit errors on video page)

Social media inquiries led to the question of starting a community network after previous videos.
The network Beau is part of incorporated the channel to supply shelters during the public health crisis.
Leveraging the network helped in providing supplies for hospitals and clinics when the pandemic started.
Under normal circumstances, acquiring supplies like gowns and masks for nurses requires significant effort.
A community network is described as a versatile and effective way to bring people together to help the community.
It's suggested to keep the idea of the network open-ended to encourage participation.
The ideal size for a network is around 8 to 12 people to avoid personality conflicts.
Allowing natural splits in the group can lead to more coverage and collaboration in the community.
Bringing different social circles together is key to creating a network that can achieve positive outcomes.
Identifying like-minded individuals willing to commit time is the first step in starting a community network.

Actions:

for community organizers,
Identify like-minded individuals willing to commit time to start a community network (implied)
Encourage open-ended participation in the network to foster collaboration and inclusivity (implied)
</details>
<details>
<summary>
2022-05-08: Let's talk about SETI vs METI and who speaks for us.... (<a href="https://youtube.com/watch?v=ZmyBmcb0JN4">watch</a> || <a href="/videos/2022/05/08/Lets_talk_about_SETI_vs_METI_and_who_speaks_for_us">transcript &amp; editable summary</a>)

Beau questions accountability and representation in decision-making processes, drawing parallels between unaccountable scientists in space messaging and decision-makers impacting the world today.

</summary>

"Who speaks for everybody? Who speaks for humanity?"
"It's really just in the hands of scientists. They're not accountable to anybody."
"Makes you think about all of the people who are not accountable to anybody, who are making decisions for the entire world here."
"Who's speaking for you? Who's speaking for us today? Here."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Distinguishes between SETI (passive) and METI (active) in the search for extraterrestrial intelligence.
Explains that METI involves sending messages to extraterrestrial intelligence, while SETI involves listening for signs.
Mentions two upcoming METI projects: one from China in 2023 and another on October 4 sending a message 39 light years away.
Raises ethical questions about contacting extraterrestrial intelligence, including the risk of a clash of civilizations.
Questions who speaks for humanity and makes decisions about sending messages to potential extraterrestrial life.
Points out the lack of regulations in determining who can send messages to space, as it currently rests in the hands of unaccountable scientists.
Draws parallels between unaccountable scientists in the space messaging field and those making decisions impacting the world today.
Encourages reflection on who truly represents and speaks for individuals and the global population.
Calls for a pause to ponder the accountability of decision-makers, both in the space messaging realm and on Earth.
Ends with a thought-provoking message about accountability and representation in decision-making processes.

Actions:

for decision-makers, activists, thinkers,
Question accountability of decision-makers and demand transparency (implied)
Advocate for regulations and oversight in decision-making processes (implied)
</details>
<details>
<summary>
2022-05-07: Let's talk about the statements by Justice Clarence Thomas.... (<a href="https://youtube.com/watch?v=YxUCoygG_ao">watch</a> || <a href="/videos/2022/05/07/Lets_talk_about_the_statements_by_Justice_Clarence_Thomas">transcript &amp; editable summary</a>)

Beau questions Justice Clarence Thomas' statements, expressing concerns about the erosion of faith in American institutions and the potential loss of rights, urging for a reevaluation of recent actions.

</summary>

"I don't understand why something like that would be written."
"It's not that I think he's wrong. It's that I don't understand why he's surprised."

### AI summary (High error rate! Edit errors on video page)

Questions Justice Clarence Thomas' statements and ideas presented in the Washington Post.
Expresses confusion over the erosion of faith in American institutions.
Questions which institutions are being referred to, mentioning the presidency, Congress, and the Supreme Court.
Criticizes the Supreme Court for potentially stripping rights from half the country for partisan and religious reasons.
Raises concerns about justices on the Supreme Court potentially lying to secure their positions.
Argues that Americans may be losing faith in institutions due to catering to a smaller group of people.
Criticizes the idea of protests being seen as bullying institutions rather than exercising First Amendment rights.
Expresses skepticism about maintaining faith in institutions given recent actions undermining trust.
States that the US may be heading towards a long road of regaining lost rights.
Questions the logic behind reversing freedoms and limiting rights that have been in place for years.

Actions:

for american citizens,
Challenge actions undermining trust in institutions (suggested)
Advocate for maintaining and expanding rights for all (implied)
</details>
<details>
<summary>
2022-05-07: Let's talk about a Russian take on mobilization.... (<a href="https://youtube.com/watch?v=cykjxyuMSXc">watch</a> || <a href="/videos/2022/05/07/Lets_talk_about_a_Russian_take_on_mobilization">transcript &amp; editable summary</a>)

Beau delves into the Russian perspective on mobilization in Ukraine, revealing challenges and potential futility in the face of resource shortages.

</summary>

"There will be mobilization or we will lose the war."
"They don't have the resources they need to do this."
"All they have to do is keep fighting."

### AI summary (High error rate! Edit errors on video page)

Exploring the Russian perspective on the situation in Ukraine, specifically discussing mobilization and its potential impact.
Mentioning the Russian company Wagner, known as a private military company and special operations group.
Sharing information from Wagner's telegram channel indicating the need for 600 to 800,000 troops to defeat Ukraine.
Noting the challenges Russia may face in mobilizing such a large number of troops, including minimal resistance, training requirements, and equipment shortages.
Speculating on the possibility of Russia launching a full mobilization on May 9th.
Emphasizing that even with a massive influx of conscripts, Russia may still struggle due to lack of training, equipment, and resources.
Pointing out Russia's difficulties in supplying and equipping troops currently present in Ukraine.
Stating that Ukraine's strategy is to make the conflict too costly for Russia to continue rather than winning battles outright.
Acknowledging the growing realization within Russia about the challenges and costs of the conflict.
Suggesting that despite potential mobilization attempts, Russia may not have the necessary resources for prolonged engagement.

Actions:

for military analysts, policymakers,
Monitor developments in Ukraine and Russia (suggested)
Support diplomatic efforts for a peaceful resolution (suggested)
</details>
<details>
<summary>
2022-05-07: Let's talk about Russian analysis of Ukraine.... (<a href="https://youtube.com/watch?v=WiRIJegvpKY">watch</a> || <a href="/videos/2022/05/07/Lets_talk_about_Russian_analysis_of_Ukraine">transcript &amp; editable summary</a>)

Beau explains the disconnect between media portrayal and analyst perspectives on the war in Ukraine, showcasing Igor Gherkin's bleak assessment that echoes Western analyses.

</summary>

"Nationalism is politics for basic people."
"The meaty is the spin of that."
"Ukraine doesn't have to win the battles. They just have to make it too costly to continue."

### AI summary (High error rate! Edit errors on video page)

Explains why he previously didn't understand a common question about Russian analysts and the war in Ukraine.
Points out that good analysts, regardless of nationality, will provide similar analyses of the situation in Ukraine.
Describes the disconnect between Western media coverage and in-depth analysis of the war in Ukraine.
Illustrates the focus of media on dramatic but less strategically significant aspects of the conflict.
Emphasizes the significant disparity between analysts' perspectives and what is portrayed in the news, particularly in countries where the government controls the narrative.
Introduces Igor Gherkin as a Russian analyst with a background in the military and intelligence.
Shares Igor Gherkin's bleak analysis of the situation in Ukraine, indicating competent defense by the enemy and challenges for the Russian side.
Mentions that Igor Gherkin's assessment mirrors Western analyses, with slight variations.
Notes that Ukraine doesn't necessarily have to win battles but make the cost too high for Russia to continue.
Concludes by underlining the consistency in analyses regardless of analysts' nationalities and warns against conflating media narratives with actual analysis.

Actions:

for analytical viewers,
Analyze international news sources to understand varying perspectives on global conflicts (suggested).
Support independent journalism to access diverse viewpoints on geopolitical events (implied).
</details>
<details>
<summary>
2022-05-06: Let's talk about why the democrats didn't codify.... (<a href="https://youtube.com/watch?v=L3l34lC7qds">watch</a> || <a href="/videos/2022/05/06/Lets_talk_about_why_the_democrats_didn_t_codify">transcript &amp; editable summary</a>)

Beau explains why focusing on federal legislation for Roe v. Wade isn't the solution; local power structures are key in the ongoing political battle against regressive policies.

</summary>

"The solution to this is not in D.C. It is at the local level."
"You're not dealing with anything more than the raw exercise of power at this point."
"If you are not a straight white guy who happens to also be a theocratic Christian, they're coming for you."
"This isn't politics as usual."
"It is more important to understand the kind of fight you're actually in and that this is just the beginning."

### AI summary (High error rate! Edit errors on video page)

Dissects the historical fact about federal legislation and Roe v. Wade, pointing out its limited impact.
Emphasizes the importance of understanding the raw exercise of power by the Supreme Court.
Asserts that the solution lies at the local level and not in Washington, D.C.
Warns that the current political situation is not ordinary and stresses the need for people to realize this.
Mentions the ongoing threats beyond abortion rights, indicating a broader agenda.
Criticizes the focus on federal legislation as merely adding a speed bump for those in power.
Talks about the importance of building local power structures instead of relying solely on federal actions.
Illustrates how certain groups like the Tea Party and MAGA have influenced national parties by starting at the local level.
Concludes by reiterating the limited impact of past legislative actions in the current political climate.

Actions:

for activists, community leaders,
Build and strengthen local power structures to resist regressive policies (implied).
Motivate and mobilize the base by developing policies that resonate with normal people (implied).
</details>
<details>
<summary>
2022-05-06: Let's talk about voting harder.... (<a href="https://youtube.com/watch?v=ITaqQu0K9SM">watch</a> || <a href="/videos/2022/05/06/Lets_talk_about_voting_harder">transcript &amp; editable summary</a>)

Beau urges a shift towards community organizing and local power structures to counter regressive political agendas, advocating for voting out of self-defense and active participation to safeguard fundamental rights.

</summary>

"How do we fight back? Give me something."
"You can call that voting harder if you want to. I don't see that as an inaccurate description of what they did."
"That strategy, you can use it too. There's nothing saying you can't."
"It's not relying on the Democratic establishment in DC. They're not your solution."
"That's the solution."

### AI summary (High error rate! Edit errors on video page)

Analyzing how the country ended up in its current situation, where an opinion heavily reliant on a person who believed in witches is overturning 50 years of legal precedent.
Expressing concern over the defeatist attitude and doomerism among some individuals regarding the Democrats' failure to prevent the current situation.
Questioning how to fight back against the right-wing successes in stripping rights away from the majority.
Exploring the tactics used by the Republican Party, such as leveraging community networks like churches for political gains.
Emphasizing the power of community networks in influencing election results and ultimately shaping the national landscape.
Noting the need for a shift in mindset towards electoral politics, especially in light of recent events affecting fundamental rights.
Urging people to organize and build local power structures as a way to counter regressive policies and protect individual freedoms.
Stating that voting out of self-defense and organizing at the local level are key strategies in resisting harmful political agendas.
Encouraging individuals to take action and participate in building a stronger, more resilient community network to combat oppressive policies.
Acknowledging the long and challenging road ahead in undoing existing damage and preventing further erosion of rights.

Actions:

for community activists, organizers, voters.,
Organize with local community members to establish strong local power structures (suggested).
Participate actively in building resilient community networks to resist regressive policies (implied).
Shift focus towards electoral politics and voting out of self-defense to protect fundamental rights (implied).
</details>
<details>
<summary>
2022-05-06: Let's talk about the megadrought, Lake Powell, and the future.... (<a href="https://youtube.com/watch?v=RkdVy8oHCKM">watch</a> || <a href="/videos/2022/05/06/Lets_talk_about_the_megadrought_Lake_Powell_and_the_future">transcript &amp; editable summary</a>)

Beau addresses the urgent impact of the megadrought on Lake Powell, stressing the necessity for immediate climate action to prevent severe consequences like electricity shortages in seven states.

</summary>

"We have to get a handle on this."
"As much as people talk about massive climate action as a political talking point and as a jobs program and as all of the other things that it is, it's a necessity."
"We don't have a choice."
"It won't be long before we have internally displaced people in the United States."
"If we continue to ignore this, it will happen."

### AI summary (High error rate! Edit errors on video page)

Talks about the impact of the megadrought on Lake Powell, a reservoir that generates electricity for 5.8 million homes and businesses in seven states.
Lake Powell is at its lowest levels ever, and if it drops another 32 feet, it won't be able to generate electricity.
Over the last three years, the water level of Lake Powell has dropped by a hundred feet.
The federal government is taking temporary measures to mitigate the situation, like holding back more water and bringing water from upstream reservoirs.
These temporary measures might only buy about six months due to the current rates of loss.
The states impacted by this issue include Arizona, California, Colorado, Nevada, New Mexico, Utah, and Wyoming.
Beau stresses that this is a pressing issue related to climate change and resource management.
If long-term solutions are not implemented, there will be significant problems in the future.
The choice between water supply and electricity is a critical issue in the affected areas.
Beau warns about the potential consequences of not taking immediate action to address climate change.

Actions:

for policy makers, environmental activists,
Contact local representatives to advocate for sustainable water and energy management practices (suggested)
Join or support organizations working on climate change mitigation and water conservation efforts (implied)
Coordinate with community members to raise awareness about the impacts of climate change on water resources (exemplified)
</details>
<details>
<summary>
2022-05-05: Let's talk about the dreams of the American worker.... (<a href="https://youtube.com/watch?v=ptdpKBETXDY">watch</a> || <a href="/videos/2022/05/05/Lets_talk_about_the_dreams_of_the_American_worker">transcript &amp; editable summary</a>)

Combat vets share nightmares about past jobs, challenging stereotypes on PTSD triggers, calling for a labor movement to improve worker conditions.

</summary>

"The stress came from a car dealership, a restaurant, a home improvement store."
"The United States needs a resurgence in the labor movement."
"Trauma can come from all kinds of places."
"PTSD really only affects war events. And it's not true."
"Those who deployed apparently have the same complaints."

### AI summary (High error rate! Edit errors on video page)

A group of combat vets share nightmares about their past jobs, revealing unexpected sources of trauma.
The group finds humor in each other's stories, showing gallows humor as a coping mechanism.
There is a misconception in the United States that PTSD only stems from war events.
Trauma can manifest from various sources, not limited to combat situations.
Beau's Twitter post about job nightmares sparks responses from others sharing similar experiences.
Combat vets talk about stressful jobs like working at a cell phone store, challenging the stereotype of PTSD triggers.
The cavalier attitudes towards American workers discussing their job conditions are concerning.
Beau suggests that the United States needs a labor movement to improve worker conditions.
The stress faced by tough men at places like car dealerships points to underlying issues in the American workforce.
Complaints about job conditions shouldn't be dismissed by comparing them to deployed individuals' experiences.

Actions:

for american workers,
Support and advocate for labor movements to improve worker conditions (suggested)
Educate others on the diverse sources of trauma and the need for empathy and understanding (implied)
</details>
<details>
<summary>
2022-05-05: Let's talk about Russians and John Deere.... (<a href="https://youtube.com/watch?v=h0kbx8zJZvQ">watch</a> || <a href="/videos/2022/05/05/Lets_talk_about_Russians_and_John_Deere">transcript &amp; editable summary</a>)

Russian troops in Ukraine loot John Deere dealership, discover they can't use the high-ticket items due to remote access lockout, rendering $5 million worth of equipment useless.

</summary>

"Crime doesn't pay, not even in a war zone, I guess."
"John Deere has locked them out of it via remote access."
"Smaller farms going with other companies."
"Russian forces' activity is easily traceable."
"They had a bunch of scrap metal, maybe a few engine parts and tires."

### AI summary (High error rate! Edit errors on video page)

Russian troops in Ukraine looted a John Deere dealership, taking equipment back to Chechnya.
John Deere equipment is not easily repairable by smaller operators due to technology.
Smaller farms are opting for other companies over John Deere due to complex technology.
The expensive equipment stolen by Russian troops is controlled via remote access by John Deere.
Russian troops won't be able to use the high-ticket items they stole due to being locked out remotely.
The looted equipment is worth $5 million, including harvesters worth a quarter million each.
Only smaller tractors might be salvageable, while the expensive equipment is rendered useless.
This act by troops violates the laws of armed conflict.
The Russian forces' activity is easily traceable, even down to the farm in Grozny.
Crime doesn't pay, even in a war zone.

Actions:

for farmers, tech enthusiasts, activists,
Contact local authorities about potential thefts or suspicious activities in your area (suggested)
Join or support organizations that work towards accountability for armed forces (suggested)
</details>
<details>
<summary>
2022-05-05: Let's talk about Gaetz and over-educated women.... (<a href="https://youtube.com/watch?v=izYZUCu-jg4">watch</a> || <a href="/videos/2022/05/05/Lets_talk_about_Gaetz_and_over-educated_women">transcript &amp; editable summary</a>)

Beau addresses misconceptions about education, criticizes the Republican Party's preference for compliance over critical thinking, and advocates for personal growth and choice.

</summary>

"The Republican Party doesn't want educated people. They don't want successful people. They want compliant people."
"Women are supposed to be at home. And this feeds right into the real reasoning behind the desire to overturn Roe."
"If you're somebody who wants to make America great again, why would you want to relegate half of the population to the kitchen?"
"Rather than focusing on controlling other people, focusing on becoming over-educated by your standards."
"Celebrating mediocrity really shouldn't be a party platform."

### AI summary (High error rate! Edit errors on video page)

Addresses a statement made by Matt Gaetz about overeducated women and their alleged problems.
Questions the logic behind the statement and points out the misconception about education.
Criticizes the Republican Party for its stance on education, preferring compliance over critical thinking.
Emphasizes the Republican Party's discomfort with educated and successful individuals.
Talks about the underlying theme of wanting compliant and controllable individuals.
Condemns the idea of women being relegated to traditional roles and the kitchen.
Explains the real motivation behind the desire to overturn Roe v. Wade – control.
Advises young women to be cautious of those who use terms like "overeducated" or make jokes about women's roles.
Encourages individuals to focus on self-improvement rather than controlling others.
Concludes by questioning the Republican Party's celebration of mediocrity and advocating for personal growth and choice.

Actions:

for women, educators, activists,
Stand up against gender stereotypes and advocate for equal opportunities for education and success (implied).
Support organizations that empower women and provide educational resources (implied).
</details>
<details>
<summary>
2022-05-04: Let's talk about a Ukraine update and the race.... (<a href="https://youtube.com/watch?v=isg2gWe7uGc">watch</a> || <a href="/videos/2022/05/04/Lets_talk_about_a_Ukraine_update_and_the_race">transcript &amp; editable summary</a>)

Beau provides updates on Ukraine, discussing static front lines and a critical race between Russia mobilizing reserves and Ukraine receiving supplies.

</summary>

"The same thing is occurring right now."
"And that's the race."
"That's probably going to be the next big development there."
"Once somebody wins that race, there will start to be movement."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Providing an update on the situation in Ukraine and discussing the static front lines.
Ukrainian forces launched a counteroffensive around Kharkiv, gaining 35-40 kilometers.
Speculating on the possibility of Ukraine encircling Russian troops at Izium.
Mentioning the challenges of encirclement maneuvers for both Russia and Ukraine.
Describing the ongoing race between Russia mobilizing reserves and Ukraine receiving supplies.
Drawing parallels to the World War II Red Ball Express for Ukraine's logistical support.
Emphasizing the critical role of timely and efficient supply delivery.
Noting the potential impact of Ukraine receiving sophisticated supplies before Russia resolves its issues.
Anticipating that this supply race could be the next significant development in the conflict.
Pointing out that the outcome of this race may determine the future movements and dynamics of the war.

Actions:

for observers, analysts, supporters,
Support organizations providing aid to Ukraine (suggested)
Stay informed about the ongoing situation in Ukraine (implied)
</details>
<details>
<summary>
2022-05-04: Let's talk about Trump vs the state GOP.... (<a href="https://youtube.com/watch?v=sMXq1aQVmvo">watch</a> || <a href="/videos/2022/05/04/Lets_talk_about_Trump_vs_the_state_GOP">transcript &amp; editable summary</a>)

Trump loyalists target state GOP for control, facing resistance as state Republicans in various states push back, leading to a resource-draining struggle.

</summary>

"They're making sure that they maintain their power, and that means undercutting Trump."
"This struggle is sapping resources from both parties."
"The state level GOP is at odds with the Trump machine."
"It's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump loyalists are aiming to control the Republican party at the state level by focusing on offices related to elections.
Their attention is particularly on secretaries of state who didn't comply with Trump's request to find votes.
Trump's move against state power structures faces resistance from states like Tennessee, North Carolina, Georgia, and now Nevada.
In Nevada, despite Trump's endorsement, the state Republicans endorsed different candidates, showing a divide within the party.
The state Republican parties are pushing back against Trump's influence by choosing candidates independently.
Some view this resistance as a patriotic act against Trump's power grab, while others see it as political self-defense to maintain their own power.
The ongoing struggle between the state GOP and Trump's machine is leading to resource depletion for both parties.
Ohio's Republican candidate for governor successfully fought off three pro-Trump primary challengers.
Despite media attention on certain Trump-endorsed candidates like Vance, overall, the state-level GOP seems to be conflicting with Trump's influence.
The battle between the state GOP and Trump's faction is significant and continues to evolve.

Actions:

for political observers,
Support state Republican parties in their efforts to maintain independence from Trump's influence (implied).
Stay informed about the ongoing power struggles within the Republican party (implied).
</details>
<details>
<summary>
2022-05-04: Let's talk about Abbott costing Texas while helping New Mexico.... (<a href="https://youtube.com/watch?v=Xy0a5DAEqAg">watch</a> || <a href="/videos/2022/05/04/Lets_talk_about_Abbott_costing_Texas_while_helping_New_Mexico">transcript &amp; editable summary</a>)

Governor Abbott’s costly border stunt leads to billions in economic losses for Texas, diverting vital trade routes and potentially impacting his re-election prospects.

</summary>

"The point of the stunt was to perform inspections on trucks coming up from the border."
"Rather than going through Texas and adding to the economy there, it will now be going through New Mexico."
"This is going to weigh heavily on Governor Abbott's re-election chances."
"It's a method of convincing Texans to other and kick down."
"The fallout from Governor Abbott's moves along the border will continue to grow."

### AI summary (High error rate! Edit errors on video page)

Governor of Texas engaged in a political stunt at the border that backed up trade from Mexico, costing Texas $4.2 billion, including $240 million in spoiled produce.
The stunt involved unnecessary truck inspections that had already been conducted by the federal government, resulting in zero drug seizures or stopping of undocumented individuals.
As a response, the Minister for the Economy in Mexico diverted the TMEC Corridor Railroad away from Texas, causing a loss of billions with the new route through Santa Teresa, New Mexico.
This rail line was initially planned to run from Mexico to Canada through Texas but will now bypass Texas entirely.
Governor Abbott's actions along the border are causing severe economic damage to Texas, leading to significant financial losses.
The governor's actions, aimed at exaggerating a situation for political gain, have backfired by draining money from Texans during a time of increased financial strain.
The economic repercussions of Abbott's decisions will likely impact his chances of re-election, with potential primary challengers, independents, and Democrats expected to capitalize on detailing the financial losses incurred by the state.
The fallout from these border moves will continue to escalate, with increasing scrutiny on the financial implications and political consequences.
The diversion of the railroad away from Texas signifies a substantial blow to the state's economy and serves as a direct result of Governor Abbott's misguided actions.
Beau concludes by reflecting on the significant financial toll inflicted on Texas due to Governor Abbott's costly and ineffective border policies.

Actions:

for texans, voters, border communities,
Contact local representatives or organizations to advocate for responsible and effective border policies (suggested)
Support primary challengers, independents, or Democrats who prioritize sound economic decisions and community well-being (implied)
</details>
<details>
<summary>
2022-05-03: Let's talk about the 80s, Grenada, and a history quirk.... (<a href="https://youtube.com/watch?v=9B4BEYdnpx8">watch</a> || <a href="/videos/2022/05/03/Lets_talk_about_the_80s_Grenada_and_a_history_quirk">transcript &amp; editable summary</a>)

Beau breaks down the US invasion of Grenada, challenging popular misconceptions and discussing the implications of imperialism in historical events.

</summary>

"The US invasion of Grenada wasn't just about rescuing medical students."
"The US intervention in Grenada raises questions about imperialism and sphere of influence."
"It's just interesting to note the way the United States remembers that war because of a movie."

### AI summary (High error rate! Edit errors on video page)

Explains the historical context of the 80s and Grenada invasion, pointing out a significant historical oversight in how the United States remembers this event.
Mentions the Regional Security System (RSS) in the Caribbean and its role in the US invasion of Grenada.
Criticizes the popular perception of the invasion being solely about rescuing medical students, as portrayed in the movie "Heartbreak Ridge."
Provides a more nuanced explanation of the events leading to the US intervention in Grenada, citing a forcible change of government.
Talks about the US administration's fear of another hostage situation like the one in Iran, which influenced their decision to intervene.
Describes the involvement of various Caribbean nations in the invasion alongside the US forces, challenging the typical American narrative.
Mentions the presence of troops from not just Grenada and Cuba but also Soviet, Libyan, East German, and possibly Bulgarian forces on the opposition side.
Raises questions about whether the US intervention in Grenada was driven by imperialism to prevent another communist country in the region.
Points out that the regional security alliance was explicitly anti-communist, hinting at US influence in its formation.
Concludes by reflecting on the importance of understanding history beyond popular narratives and movies, urging viewers to think critically.

Actions:

for history enthusiasts, critical thinkers.,
Question historical narratives and seek out diverse perspectives (implied).
Educate others on the complex historical context of past events (implied).
</details>
<details>
<summary>
2022-05-03: Let's talk about SCOTUS, dirty little secrets, and hypocrisy.... (<a href="https://youtube.com/watch?v=a4zUTEU7mD0">watch</a> || <a href="/videos/2022/05/03/Lets_talk_about_SCOTUS_dirty_little_secrets_and_hypocrisy">transcript &amp; editable summary</a>)

Small-town dynamics reveal the layers of secrecy and hypocrisy surrounding personal choices, especially in conservative settings, impacting decisions on healthcare and voting rights.

</summary>

"It's happened a lot over the course of my life, and those are just the ones I picked up on."
"They treat it like a dirty little secret that nobody's ever going to know about or discuss."
"I think that the Republicans have made a grave error."
"Not everybody gets a happy ending."
"They're going to go somewhere, they're going to do something, and they're never going to tell us all about it."

### AI summary (High error rate! Edit errors on video page)

Living near small towns, understanding hypocrisy and secrets, sets the stage.
Tells a story about a woman splitting from her fiancé and lying about going to Pensacola Beach.
Beau catches the woman in her lies, revealing her true intentions.
Talks about small towns and the need for secrecy due to social pressures.
Mentions the prevalence of lying about visits to certain facilities in conservative areas.
Evangelical Protestants and Catholics make up a significant portion of visits to these facilities.
Despite holding certain beliefs publicly, individuals keep their visits a secret due to societal pressures.
Beau criticizes Republicans for not matching policies with the reality of what people want and do.
Acknowledges the tough choices many women face and the importance of preserving options like safe access to healthcare.
Concludes with the prediction that many women will keep their voting choices as another "dirty little secret" to protect future options.

Actions:

for small-town residents,
Support and advocate for policies that protect healthcare access for all individuals, especially in conservative areas (implied).
Encourage open and honest dialogues about sensitive topics to reduce the need for secrecy and social pressures (implied).
</details>
<details>
<summary>
2022-05-03: Let's talk about Republican reaction to the SCOTUS leak.... (<a href="https://youtube.com/watch?v=sOZTy0h6XZY">watch</a> || <a href="/videos/2022/05/03/Lets_talk_about_Republican_reaction_to_the_SCOTUS_leak">transcript &amp; editable summary</a>)

Republicans achieved a significant victory but avoid celebrating it due to overwhelming opposition, prompting Democrats to seize the moment and prioritize addressing the issue.

</summary>

"Americans don't want this, right?"
"You're a dog that caught the car. Now you got to figure out what to do."
"And this is the moment for the Democratic Party to step up."
"If you're handed the House, if you don't just suffer amazing losses, the midterms, and you don't fulfill that implied promise to the women of this country, you can kiss 2024 goodbye."
"It's what you campaigned on. It's what you promised. And you finally fulfilled it."

### AI summary (High error rate! Edit errors on video page)

Republicans achieved a significant victory but are not celebrating it.
The Republican Party seems to be avoiding talking about the victory because it is overwhelmingly opposed by the American people.
There is concern among Republicans about the information regarding the victory being leaked before the midterms.
Beau suggests that the Republicans used this issue as a wedge to motivate their base, even though it is unpopular.
He encourages the Republican Party to stand by its actions and claim credit for fulfilling their promises.
Beau believes that Democrats should seize this moment and make passing legislation a priority to address the issue that the Republicans pushed.
Failure to address this issue could lead to significant losses for the Democratic Party in the midterms and beyond.

Actions:

for political activists,
Pass legislation to address the issue pushed by Republicans (implied)
</details>
<details>
<summary>
2022-05-02: Let's talk about Fox news ratings.... (<a href="https://youtube.com/watch?v=nLrS9yiXoww">watch</a> || <a href="/videos/2022/05/02/Lets_talk_about_Fox_news_ratings">transcript &amp; editable summary</a>)

Beau explains why Fox News has high ratings, debunking misconceptions about political leanings in news media and reassuring viewers that it's a normal phenomenon.

</summary>

"They're the most viewed because it's a small demographic of people of a certain age who want their views reflected back to them."
"You will never hear on CNN that the solution to your problem is dismantling capitalism."
"This isn't a sign that more people are suddenly trusting Tucker Carlson."

### AI summary (High error rate! Edit errors on video page)

Beau addresses concerns about network news and ratings, particularly focusing on Fox News and its high ratings.
He explains that Fox News has the highest ratings because it caters to the demographic seeking right-wing content.
Beau clarifies that centrist outlets like CNN and MSNBC are not leftist but lean liberal.
He points out that Fox's ratings are inflated due to its specific audience demographic, primarily older viewers who prefer TV news.
Beau mentions that the high ratings of Fox News are not necessarily indicative of widespread trust or viewership.
He criticizes Fox News for using a propaganda strategy to claim they are the most trusted due to being the most viewed.
Beau distinguishes between centrist, liberal-leaning outlets and leftist news networks like PLN on YouTube.
He encourages viewers to watch PLN to understand the difference between leftist and centrist news perspectives.
Beau concludes by stating that Fox's higher ratings are normal and not a cause for alarm, as it correlates with the age demographic that consumes TV news.
Overall, Beau aims to provide perspective on network news ratings and to debunk misconceptions about political leanings in news media.

Actions:

for media consumers,
Watch leftist news networks like PLN on YouTube to understand different news perspectives (suggested)
Analyze news sources critically and seek diverse viewpoints to avoid misinformation (implied)
</details>
<details>
<summary>
2022-05-02: Let's talk about 10 propaganda techniques you'll see in the midterms.... (<a href="https://youtube.com/watch?v=Uq5FwK43Gc4">watch</a> || <a href="/videos/2022/05/02/Lets_talk_about_10_propaganda_techniques_you_ll_see_in_the_midterms">transcript &amp; editable summary</a>)

Beau explains ten propaganda techniques used in elections to manipulate opinions, urging viewers to recognize and resist these tactics to make informed choices.

</summary>

"Propaganda doesn't care whether or not it's true or not."
"Appeal to fear generates anxiety and fear of the alternative."
"Bandwagon: You want to be one of us, right?"
"Inevitable victory: Making people want to be on the winning side."
"Glittering generalities: A general term that is just supposed to make people feel better."

### AI summary (High error rate! Edit errors on video page)

Explains ten propaganda techniques that surface in elections, including ad hominem, ad nauseam, appeal to authority, appeal to fear, appeal to prejudice, bandwagon, inevitable victory, beautiful people, card stacking, and glittering generalities.
Propaganda doesn't necessarily mean it's false; it can be true information presented in a manipulative way.
Emphasizes the importance of recognizing these techniques to become less susceptible to manipulation during elections.
Describes how appeals to authority involve endorsements and support from various groups.
Points out the use of fear in politics to sway opinions by presenting one position as the lesser evil.
Talks about appealing to prejudices by associating emotional value with certain concepts, like the term "woke" or "CRT."
Mentions the bandwagon technique where individuals are pressured to join a group to be part of the "in-crowd."
Explains how politicians often use the concept of inevitable victory to attract supporters and create an aura of success.
Comments on the strategy of showcasing "beautiful people" to support a political cause and how it plays into societal biases.
Describes card stacking as providing selective information to support a particular narrative while omitting critical details.
Talks about glittering generalities, which are vague terms used to evoke positive emotions without substance.

Actions:

for voters,
Analyze political messages critically (implied)
Educate others about propaganda techniques (implied)
Fact-check political claims and statements (implied)
</details>
<details>
<summary>
2022-05-01: Let's talk about science fiction.... (<a href="https://youtube.com/watch?v=1oY2LmTSAYI">watch</a> || <a href="/videos/2022/05/01/Lets_talk_about_science_fiction">transcript &amp; editable summary</a>)

Beau delves into how science fiction offers a unique lens to analyze truth and societal commentary, urging deeper thought for a better world.

</summary>

"Facts mixed with a little bit of fiction can often get you to truth and that's what science fiction is."
"Good science fiction is commentary on the world around the author."
"Science fiction can literally change the world."
"If we want a better world, we have to analyze, we have to think deeper."
"That type of thought, that deeper thought, is kind of the only thing that's going to save the world at this point."

### AI summary (High error rate! Edit errors on video page)

Beau dives into the importance of science fiction in exploring truth and facts.
Science fiction allows individuals to analyze ideas without the influence of societal norms and biases.
It serves as a tool for examining commentary on the world by placing it in a different context.
Good science fiction can inspire change in perspectives and values.
Beau likens science fiction to gonzo journalism, focusing on getting to the truth rather than just facts.
He mentions how works like "1984" are science fiction that provide commentary on society and its future.
Star Trek's enduring fan base is attributed to its valid commentary on societal issues.
Science fiction helps people escape reality and return with new perspectives.
Beau believes that the lack of analysis and understanding in society is a significant issue.
Analyzing deeper cues in media, like in science fiction, can lead to a better world.

Actions:

for enthusiasts of science fiction,
Dive into science fiction literature and films to analyze societal commentary (suggested)
Encourage others to think deeper about the media they consume (exemplified)
</details>
<details>
<summary>
2022-05-01: Let's talk about pipelines and Putin.... (<a href="https://youtube.com/watch?v=G0SHeWrScSs">watch</a> || <a href="/videos/2022/05/01/Lets_talk_about_pipelines_and_Putin">transcript &amp; editable summary</a>)

Beau explains why Ukraine hasn't attacked Russian pipelines and why removing Putin isn't a viable option strategically or morally.

</summary>

"Is it more beneficial to Ukraine to have those economies functioning and pumping in what they need, or to cut off Russia's cash flow and perhaps anger the European economies?"
"There are people who want a strongman ruler who's going to tell them what to do and just cast that image of power, even if it's hurting them."
"If it's forced on them from outside, it will go bad."
"That's not good."
"It's not like the US has the same philosophical objections I do when it comes to removing a foreign head of state."

### AI summary (High error rate! Edit errors on video page)

Explains why Ukraine hasn't seized the initiative to attack critical Russian infrastructure like pipelines due to complex capabilities and balancing economic considerations.
Points out that cutting off Russia's cash flow by attacking pipelines could harm European economies supporting Ukraine's war effort.
Stresses the importance of maintaining economies that are aiding Ukraine and not angering them, even though going green is a national security priority.
Dissects the question of why the West doesn't remove Putin, citing potential negative outcomes of destabilizing Russia and the uncertainty of who might come to power after Putin.
Warns about the risks of engaging in actions to remove Putin and the potential consequences of a power struggle or breakup of Russia.

Actions:

for global citizens,
Support initiatives that prioritize clean energy and national security. (exemplified)
Advocate for diplomatic solutions and support efforts to maintain stability in regions of conflict. (exemplified)
</details>
