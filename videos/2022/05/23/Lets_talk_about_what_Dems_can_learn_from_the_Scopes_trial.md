---
title: Let's talk about what Dems can learn from the Scopes trial....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wk9pJVkYGG0) |
| Published | 2022/05/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the historical significance of the Scopes trial in the 1920s and its impact on current social issues being debated in courts and legislation.
- Describes the Butler Act in Tennessee prohibiting the teaching of evolution and the teacher who challenged it in court in 1925.
- Details the teacher's appeal to the Tennessee Supreme Court on the grounds of the law being overly broad, violating free speech, Tennessee's constitution, and freedom of religion.
- Mentions the Supreme Court case in 1968, Epperson v. Arkansas, which found such laws violated the establishment clause of the First Amendment.
- Emphasizes the importance of changing societal thinking to bring about real change, rather than just changing laws.
- Criticizes current Republican actions as regressive, authoritarian, and against the majority of society's desires.
- Stresses the need for political power to drive change based on societal thought and values, pointing out that people already support freedom, choice, education, and critical thinking.
- Urges the Democratic Party to exercise political power effectively to counter regressive authoritarianism and uphold the majority's wishes for progress and freedom.

### Quotes

- "To change society, you don't have to change the law. You have to change the way people think."
- "It's regressive. It isn't trying to change thought and then change the law. It's trying to use the law to change thought."
- "Thought is already in agreement on the pro-freedom side, the side that wants choice, that wants options, that wants education."
- "It's the raw exercise of political power that will shift this dynamic."
- "If you don't fight back, then it doesn't matter what thought gets shifted."

### Oneliner

The Scopes trial history teaches changing societal thinking drives lasting change, urging political power to uphold majority values against regressive actions.

### Audience

Democratic activists, concerned citizens

### On-the-ground actions from transcript

- Mobilize for election issues (implied)
- Advocate for freedom, choice, and education (implied)
- Support critical thinking in schools (implied)
- Pressure representatives to act in accordance with majority wishes (implied)
- Fight against regressive authoritarianism through political power (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of historical events to draw parallels with contemporary challenges and stresses the importance of political power in upholding societal values against regressive actions.

### Tags

#ScopesTrial #PoliticalPower #DemocraticParty #Education #Freedom #Authoritarianism


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about the Scopes trial.
Scopes trial from the 1920s. We're going to talk about that and we're going to talk about what it
can teach us about what's going to happen in the United States when it comes to a lot of social
issues that are being debated in courts and in legislation and how it's going to kind of shake
out over the long term. Because right now we have an anomaly. The way things are working right now,
it's not how it's supposed to work. So we're going to take a look at how it historically does work
and then we'll come back to what's happening now. So the Scopes trial is the monkey trial.
If you're not familiar with this, the United States has a long history of rejecting science
and education in general. There was a law called the Butler Act on the books in Tennessee that
prohibited the teaching of evolution. A teacher took it to court July 10th through the 21st in
1925, took it to court and lost, just so you know, lost. Fined $100 in the 1920s. That was a lot of
money. Then appealed it to the Tennessee Supreme Court and they appealed on four main grounds.
The first is that the law was overly broad. What is evolution? And the court was like, yeah,
I mean, we don't know, but we know it's something bad. We generally understand what it is. It's not
overly broad. They appealed on free speech grounds and the court was like, nope, you're
contracted through the state. Agents of government do not have free speech. They appealed on the
grounds that it violated Tennessee's constitution because Tennessee was supposed to respect and
uphold science and literature. And the court basically said, oh, it's not our job to sit in
judgment of the legislature's intent, which I mean, no, that quite literally is the court's job.
And then the last ground was freedom of religion. And the court basically said,
we don't see what this has to do with any particular religion at all. Lost. Lost.
Then in 1968, Epperson v. Arkansas, Supreme Court of the United States,
they found that these kind of laws do in fact violate the establishment clause
of the First Amendment. So it was struck down. But see, here's the thing. The Butler Act,
that law in Tennessee, it had been removed the year before.
To change society, you don't have to change the law. You have to change the way people think.
You have to change thought. You have to shift thought. The law is the law.
You have to shift thought. The law is an after effect. Changing the law is an after effect.
But see, that's not what's happening right now. What's going on right now with a lot of the stuff
that Republicans are pushing through, whether it be bans on books or CRT or family planning
or whatever, it goes against thought. It goes against what the overwhelming majority of society
wants. It's regressive. It isn't trying to change thought and then change the law.
It's trying to use the law to change thought. It is regressive. It is a reaction. It's authoritarian.
And this is only happening through the raw exercise of political power because the Republican Party
has abandoned the Republic, the Constitution, and democracy in general. I mean, that's why it's
occurring. If they cared about any of these things, this stuff wouldn't be occurring.
So what does that tell you? How do you counter it? Do you counter it by waiting for a whole
bunch of new Supreme Court justices? No, you don't need to. You've already changed thought.
Thought is already in agreement on the pro-freedom side, the side that wants choice, that wants
options, that wants education. It's already there. How did the Butler Act get repealed?
Political power. Once you have thought shifted using the mechanisms and the levers of power
of government that are granted in a democracy to the majority, well, that's how it's supposed to
work. The law is supposed to catch up as an after effect. The way this is countered is through the
raw exercise of political power. There doesn't have to be a giant campaign to change people's
minds. People are already in agreement. People are already curious about what's in those books.
People already want to learn. They want critical thinking taught in the schools.
They want family planning. It's already there. These are election issues.
And the opposing party has to be willing to exercise raw political power
to move the nation forward so we're not just held down by regressive authoritarian goons.
This is advanced citizenship in this country. This is part of it. The representatives that are in DC
and in your state capitol are supposed to be doing what the people want. And the
overwhelming majority of people want freedom. They want choice. They want education.
They don't want to be held back by people who are violating the constitution.
They don't want to be held back by people who are rejecting reality and substituting
nostalgic TV shows from the 1950s for an understanding of history.
It's the raw exercise of political power that will shift this dynamic. And the Democratic Party
has to get on the ball. They have to be willing to fight. If you don't fight back, well,
then it doesn't matter what thought gets shifted. Because those who are willing to exercise
that power will always retain control. It's time for the Democratic Party to take the gloves off
in a lot of ways. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}