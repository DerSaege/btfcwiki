---
title: Let's talk about answering Trump's tweet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XswrMFlnb_o) |
| Published | 2022/05/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses Trump's tweet calling for civil war and focuses on the people affected by such messages.
- Questions the fantasy of civil war scenarios presented by some individuals.
- Criticizes the lack of tangible grievances or reasons behind the calls for civil war.
- Challenges the notion of blindly following anti-establishment sentiments without clear goals.
- Points out the futility of aiming to "restore the Constitution" without a concrete plan.
- Emphasizes the importance of active citizenship and constructive engagement with governance.
- Urges individuals to think critically about the implications and consequences of advocating for civil war.
- Contrasts the demographics discussing civil war with those who truly face grievances and hardships.
- Criticizes how a tweet can incite destructive tendencies in individuals.
- Encourages reflection on the real-life impact of civil war, likening it to the experiences of interpreters in conflict zones.
- Challenges the division and demonization of political ideologies within communities.
- Raises awareness about the dangerous consequences of blindly following divisive rhetoric.
- Urges individuals to reconsider the destructive path of violence and conflict.
- Encourages seeking understanding and empathy towards others, even those with differing political views.
- Concludes by questioning the rationality of destroying everything based on incendiary messages.

### Quotes

- "The Constitution, it's not magic, it's just words on paper."
- "It's a whole lot easier to destroy than it is to build, right?"
- "What's it going to be like? Am I really willing to sacrifice everything that I've ever known or loved?"
- "Are you really willing to commit years of your life and destroy the country for whatever that grievance is?"
- "Y'all have a good day."

### Oneliner

Beau challenges the fantasy of civil war scenarios, questions the lack of tangible grievances, and urges critical reflection on the destructive impact of blindly following divisive rhetoric.

### Audience

Community members

### On-the-ground actions from transcript

- Talk to someone who has experienced conflict or war (exemplified)
- Engage in constructive political participation (implied)
- Foster empathy and understanding towards individuals with different political views (implied)

### Whats missing in summary

The full transcript provides a nuanced exploration of the dangers of advocating for civil war and the importance of critical thinking in the face of divisive rhetoric.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, I would imagine that most of y'all have already heard about Trump's
tweet or retweet or retruth or whatever it was, and I would imagine that that's
what most of the coverage is about this morning.
Was he predicting it?
Was he calling for it?
Was he just some old man that doesn't know how to run his own social media network?
What happened?
What are the implications of it? Should he be calling for this? What does this say when a former president does this?
Because of those two little words, right?
civil war. The thing is, I don't want to talk about that. I'm pretty sure that everybody's
going to be talking about that. I want to talk about the people that that message was
meant for because those conversations come up in my geographic area a lot. And I hear
I'm in the diners every once in a while and I always insert myself in because I
have questions when people start talking about it. The first is what is it going
to be like? And it never ceases to baffle me that the descriptions I hear
they're just abject fantasy. They're total fantasies. Even those people who
were over there. They were in Iraq or Afghanistan. What they describe is their
experience over there. But that's not what it would be like. You wouldn't be
the visiting team. You wouldn't be the visiting team. What it would be like
would be like those horrible stories you heard from the Terps, from the
interpreters. Those stories so dark, can't even say them on YouTube, right? That's
what it would be like. Because you're not the visiting team. It's happening in your
home. Can never get an accurate description of what they think it would
be like, and when they have some idea in their head, it's not even close to reality.
And then you ask, you know, why?
What is it at this moment that has got you ready to rip the country you supposedly love
apart and just shred it, destroy everything you have ever known or ever said that you
loved? Because that's what would happen, right? There's never an answer for that
either. It's just general angst. You know, right now I'm sure somebody would say
something about inflation. You know, a global issue, meaning it has nothing to
do with who's sitting in DC. Not really.
There's that. And then I got some bad news about the value of your current
during a civil war. So most people, in fact I've never had somebody give me an
accurate description of what it would be like, and I've never had anybody give me
a tangible grievance, you know, some intolerable act that they can point to
and say this is why. It's just general angst. And then you ask my favorite
question. What happens after? You don't get an answer there either. And if you
push and you really try to just drag one out of it, you get one of two things. Some
general anti-establishment attitude, which hey, I can get behind that, I get it. But
then I gotta ask, in what world is a trust fund baby billionaire not the
establishment. Just a different faction of it. And that's normally what civil
wars are. A method of creating a game, a life-and-death game for poor people to
play so one faction of the establishment can get a little bit more power. And if
If it's not that, they'll say something generic, like restore the Constitution.
And if you press them and ask, what exactly does that mean, you can never get an answer.
I've got some bad news about that too.
The Constitution either explicitly allowed everything that has happened, or it was powerless
to stop it.
It's a hard pill to swallow, but it's 100% truth.
The Constitution, it's not magic, it's just words on paper.
The Constitution was built around the idea of advanced citizenship.
You have to be involved.
You have to make the country better rather than relying on the decisions of your betters.
Of those people you hold up as messiahs.
Those people who could convince you with a tweet that it might be time to destroy everything
you've ever known or loved.
The Constitution didn't work because you got angry, because you gave in to the idea of
violence being the answer rather than building.
It's a whole lot easier to destroy than it is to build, right?
If you're one of those who carries that idea that it's time for civil war, ask yourself
those questions.
What's it going to be like?
Am I really willing to sacrifice everything that I've ever known or loved?
What's the actual grievance?
What happens after?
Are you really willing to commit years of your life and destroy the country for whatever
that grievance is?
I'm willing to bet that if you stop and you actually thought about it for a second, you
would realize how wild it sounds.
There are a lot of people who have legitimate grievances in this country who are just dying
for change, literally in some cases. But those aren't the demographics I see in the diners.
Those aren't the people I hear talking about this. The people I hear talking about this
are the people who drive to the diner for lunch. They're not at the bottom. They're
They're not those struggling to get by.
They aren't those with a target on their back.
They aren't those that were forgotten or ignored or explicitly exempted when the Constitution
was written.
And because some guy tweeted, I have no doubt in my mind that that's what people are going
to be talking about today. Because he stirred people up. He found a way to
reach into that group that has that angst and give them permission to be
their worst. I'm just wondering if it's really worth destroying your town, your
village. Think of those stories from the interpreters and if you don't know any,
talk to somebody who was over there because that's what the stories will be
like. It's not convoys, it's not patrols, it's those origin stories for the
interpreters or maybe some of the people who joined the National Army or the
Kurds, that's what it would be like because you're not the visiting team.
This happens in your town.
And one of the other things that always comes up, because this generally comes from one
side around here is the idea that oh well it's it we're just gonna we're
gonna fight the leftists. I always find that funny. I'm like you won't kill me. I
thought we were friends. Oh no no I'm not talking about you. I'm talking about
But those other ones, yeah, I get it, I'm one of the good ones, right?
That created enemy that goes against everything you know, because I'm willing to bet every
one of you has liberals or leftists or whoever in your life that you view as friends.
you wouldn't really want to see go but I mean there is that tweet some rich guy
you never met doesn't care about you tweeted something that might be a good
reason to destroy everything that you've ever known anyway it's just a thought
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}