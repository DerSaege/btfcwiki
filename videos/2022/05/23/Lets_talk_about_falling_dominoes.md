---
title: Let's talk about falling dominoes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t9jTMOu8UnQ) |
| Published | 2022/05/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of dominoes falling in relation to systemic racism.
- Points out how systemic racism affects social safety nets, wages, and housing investments.
- Links tipping to a legacy of slavery and suppression of black wages.
- Connects objections to affordable housing to racism and voter motivations.
- Attributes the militarization of police to the war on drugs targeting specific communities.
- Describes the cycle of underfunding in schools due to racial disparities.
- Reveals how racism is used as a tool to manipulate white voters against their interests.
- Raises questions about airport security, war involvement, and public pool closures in the context of systemic racism.
- Stresses the need to dismantle systemic racism as the first step towards positive change.

### Quotes

- "The first step to getting to any of this is systemic racism."
- "We've got to move past that in this country."
- "As long as they're doing better than those other people, well, they'll keep voting that way."
- "It's so pervasive in this country that even something as simple as pools doesn't escape its grasp."
- "The first step to getting to any of this is systemic racism."

### Oneliner

Beau explains how dismantling systemic racism is the key to addressing various societal issues affecting white people.

### Audience

White Americans

### On-the-ground actions from transcript

- Research the history of tipping and its impact on racial wage disparities (suggested)
- Investigate the reasons behind objections to affordable housing in your community (suggested)
- Learn about the racial implications of the war on drugs and militarization of police (suggested)

### Whats missing in summary

Insights on the ripple effects of systemic racism on various aspects of society and how dismantling it can lead to positive change. 

### Tags

#SystemicRacism #SocialJustice #WhitePrivilege #CommunityAction #Equality


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about dominoes falling.
Because after that last video about the unusually honest
question, a whole bunch of new questions came in.
Some were just normal excuse making.
Some were people like, hey, I want a list of these dominoes.
This would be really useful in conversation.
And some people who, in good faith, don't see them.
So what we're going to do today is
we're going to go over the dominoes that would fall.
And we're going to start with ones
that I think are relatively easy to see.
And as soon as they're said, you're going to be like, oh,
yeah, I guess so.
And then we're going to go to some that
are a little bit more complex.
And then we're going to go to one that just shows you
how widespread it is.
So we're going to talk about the dominoes that would fall
if we could end systemic racism in this country
and how those dominoes that fall would help white people.
OK, why don't we have good social safety
nets in this country?
Start there.
Welfare queen, right?
That's how they motivate people to vote against it.
What color is she?
They pillar that racism, right?
Or they talk about immigrants coming over to get welfare
so they don't want to fund it, right?
If they get welfare, what are they going to do?
Get Rams and iPhones, right?
The main push, the main tool that conservatives
use to stop social safety nets from developing in this country
is an appeal to racism.
You get rid of that, those social safety nets show up.
That one seems pretty obvious.
Why are your wages so low?
And if you work for tips, you definitely
want to look into this.
Tipping is a legacy of slavery.
White business owners didn't want black people
to make good pay, so they got an exemption.
They were tipped employees.
And you can Google this.
This has been fact checked by everybody from Pick Your Outlet
to USA Today.
Tipping existed beforehand, but it became widespread,
and it became very important as a tool
to suppress black wages.
And then, well, it spread.
What about housing?
I know, it's another social safety net.
Why don't people want to invest in affordable housing?
Why do people object when subsidized housing
gets put in near them?
Because of the image of the projects, right?
Racism.
Playing off of that motivates voters,
not in my backyard type of voters.
Makes it harder.
Militarized police.
Why'd that come about?
The war on drugs.
Who was that aimed at?
Right?
Why don't you have good schools in this country?
Because they try to keep that disparity.
Those neighborhoods don't need as much funding.
Those neighborhoods don't perform as well,
so we're going to give them less funding,
making them perform worse, so we can justify giving them
less funding later.
See, the problem is that school that
gets the less funding from that neighborhood, right?
And it's not a white neighborhood, is it?
But that level of funding, that becomes the baseline.
So everybody else's funding gets depressed as well.
They use it as a tool.
They use racism as a tool to get white voters
to vote against their own interests.
As long as they're doing better than those other people,
well, they'll keep voting that way,
even though it hurts them.
Those seem to me like they would be easier to see once they
were just pointed out.
But here's some a little bit more complex.
Why do you take your shoes off at the airport?
Well, for security, no, not really.
I mean, it's theater.
It's security theater.
That's not a big deal.
But how did it start?
Why did it make people feel safer?
How did you end up losing that freedom at the airport
to keep your shoes on?
An attempt, right?
An attempt.
But it's because the person that attempted it.
Because that group of people, well, they'd been othered.
They were pushed out.
The racism came into play.
And it was OK because it was going to catch them.
It's not actually about stopping mass incidents.
I would imagine that if that was the case,
we'd probably have a lot stricter gun laws.
But it's not.
Your sons and daughters, your friends,
why are they constantly being shipped off to the global south
to go and fight in wars?
Because systemic racism exists.
Those countries, well, they're not run by white people,
are they?
They need somebody to tell them what to do.
They need somebody to run their country.
It's a route of imperialism.
Knocking down systemic racism opens the door
to make the country better in so many ways.
We should want to get rid of systemic racism
because, well, it's racist.
I mean, just the general good that that would do,
that's the reason to do it.
However, for a lot of white people,
they need that self-interest.
And there it is.
Why don't you have public pools?
Do a little bit of research on that.
When they started desegregating pools,
a lot of places just closed them up.
They weren't as popular anymore.
It used to be a really big thing.
Not so much anymore.
It's so pervasive in this country
that even something as simple as pools doesn't escape its grasp.
It's everywhere.
If racism can factor into whether or not
your community has a public pool,
it probably factors into a lot of stuff
you don't really think about.
There are a bunch of dominoes.
But the first one that has to go to get to any of this
is systemic racism.
And I think that's the most important thing.
The first step to getting to any of this is systemic racism.
We've got to move past that in this country.
That's the first step.
Once that domino gets knocked down,
all of this other stuff is going with it.
Anyway, it's just a thought. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}