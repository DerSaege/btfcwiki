---
title: Let's talk about teachers, old ideas, and new dynamics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t8WTgSjuGVY) |
| Published | 2022/05/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An old video of his resurfaces, sparking a chain of events and comments due to addressing different talking points.
- The current focus is on the idea of arming teachers, a suggestion now put forth by the Republican Party.
- Beau questions the logic of giving guns to teachers who face trust issues even with basic topics like discussing diversity or using rainbow stickers.
- He argues that arming teachers doesn’t automatically turn them into warriors as suggested by some.
- Beau believes teachers may be more willing to intervene in dangerous situations compared to police, but their role is to save lives, not take them.
- He criticizes the mindset required for teachers to carry guns, as it shifts the learning environment into a classification of threat levels.
- Beau links the idea of arming teachers to the attitude of shooters seeking power, which is reinforced by the gun's symbolism.
- He challenges excuses made for police officers facing armed threats, debunking the notion that a bigger gun always equals more power.
- The rhetoric around guns and power is a concerning factor in American society, contributing to a flawed perception of strength.
- Beau raises the issue of putting more guns in schools and how it may not serve as a practical or effective solution.

### Quotes

- "The gun makes the man. Give them a gun and well, they've got the power now."
- "It's a band-aid on a bullet wound, at best."
- "There's one waiting for them at the school."
- "This isn't a solution."
- "It's thoughts and prayers."

### Oneliner

Beau challenges the flawed logic of arming teachers, questioning its efficacy and impact on school environments, labeling it as a superficial solution.

### Audience

Educators, policymakers, parents

### On-the-ground actions from transcript

- Challenge proposals to arm teachers in your community (implied)
- Advocate for comprehensive safety measures in schools without resorting to arming teachers (implied)

### Whats missing in summary

The emotional depth and detailed analysis Beau provides on the implications of arming teachers in schools.

### Tags

#SchoolSafety #GunControl #TeacherEmpowerment #CommunityEngagement


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
an old idea, an old video, and new dynamics when it comes to teachers.
I was sitting there and I started getting all the notifications, and I knew they were coming
because it happens every time. I started getting all these notifications that an older video of mine
was being viewed widely again. Lots of comments and stuff like that.
I knew it was going to happen because the same videos happen in the same order every single time
like this occurs. They get widely viewed because they address the different talking points.
It's a chain of events. It starts with this and it moves on. We are currently at the armed teacher
talking point. But see, the dynamics have changed since that video was released. It's the Republican
Party suggesting this. Just give the teachers guns. That'll solve it. Right. Give the teachers guns.
Teachers that you don't trust with the word gay. Teachers that you don't trust to lead a discussion
about racism. Teachers that you don't trust to talk about different philosophies or ideologies.
That you don't trust with rainbow stickers or access to classic books. Things that actually
have to do with their job. You want to give them a gun. And the idea is because, well, they get
that gun, well then they're a warrior. Contrary to the propaganda that's in the gun crowd,
taking people that don't have the right mindset, who aren't trained, and handing them a gun
doesn't actually do anything. It doesn't create a warrior. You want evidence? Look at those
cops that were there. That's not what happens. I do believe that teachers would be more willing
to intervene. Historically, they have proven themselves more willing to intervene than cops
because they're already there. But that's in an effort to save lives, not take them.
The mindset necessary to do what you're talking about is different. Because once you adopt those
mindsets, once you really get to the point that you are truly ready to respond to a situation
like that, those aren't students. You've classified them. Unlikely threat, potential threat,
likely threat. That's not a learning environment. It's not a learning environment.
And when I talk about how the gun crowd and their rhetoric is responsible for the attitude
of the shooters, this is what I'm talking about. This is what I'm talking about right here.
The idea of just giving guns to teachers, that's part of it. Because what are you saying?
The gun makes the man. Give them a gun and well, they've got the power now.
It's funny because that's one of the things that is a marker for the shooters.
Seeking that power, if only for a moment. It's in the studies.
And that's what you're indicating is the case. Because that gun gives you power.
It's the source of all power in America today.
And then the same attitude is reinforced when you hear people making excuses for the cops.
Well, they just had pistols and he had a rifle. First, I want to be clear on this.
I don't believe that. I think that's a lie. I do believe they had access to rifles.
But let's pretend it's true. Let's say that it's true.
Okay, there's a pistol and a rifle.
And unless you're telling me that this shooter's out of range, this doesn't actually matter.
It's just that image of the bigger gun being more powerful.
The pistols at the range they were at, they have the power necessary to put somebody down.
It's just more of that rhetoric, reinforcing that idea that the gun makes the man.
And I know somebody right now is going to say, why do you keep saying man?
Shouldn't you say person? A, that's not the saying.
And B, it's pretty much always men that do this.
This isn't a solution. I understand the mitigation factor.
And sure, there might be individual cases where it would make sense on some level,
as far as the teacher being actually capable of responding.
I know some people that were high speed that became teachers.
And they did it because they didn't want to hurt people anymore.
But they still have that skill set.
So there may be isolated cases where sure, in theory, it might work,
if that's the exact scenario that played out.
Okay, I get it.
But the other side to that is, what does it really do?
It puts a bunch of guns in the school,
which means kids don't have to get a part-time job at a fast food joint to go and buy one.
There's one waiting for them at the school.
That doesn't really seem like a winning solution.
It's a band-aid on a bullet wound, at best.
It's thoughts and prayers.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}