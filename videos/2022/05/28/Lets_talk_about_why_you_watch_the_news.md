---
title: Let's talk about why you watch the news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p1OajhVHV-c) |
| Published | 2022/05/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises the question of why people watch the news, whether it's for entertainment or to stay informed.
- Emphasizes the importance of actionable information that can help in making better decisions.
- Points out that accurate information without context is not very useful.
- Criticizes news outlets that sensationalize stories with dramatic headlines that often turn out to be exaggerated or debunked.
- Warns against being misled by news sources that prioritize ratings over accuracy.
- Talks about how some news outlets intentionally publish false information or lack context, making it impossible to use the information for decision-making.
- Gives an example of misleading reporting on Biden's supply chain issues, stressing the importance of providing context.
- Explains how good information with context can prepare individuals for the future and help them understand the full picture.
- Draws a distinction between news outlets that inform and those that serve as propaganda, aiming to influence rather than inform.
- Encourages viewers to question the news sources they consume and to stop watching if it doesn't improve their understanding or quality of life.

### Quotes

- "Information, being better informed about something, should remove fear."
- "If it's just leaving you angry and scared, you should probably stop watching."

### Oneliner

Beau questions the purpose of news consumption, advocates for actionable information with context, and warns against propaganda outlets that aim to influence rather than inform.

### Audience

News consumers

### On-the-ground actions from transcript

- Question the news sources you consume (implied)
- Stop watching news outlets that leave you angry and scared (implied)

### Whats missing in summary

The importance of critically evaluating news sources and their impact on decision-making.

### Tags

#NewsConsumption #MediaLiteracy #Context #Propaganda #DecisionMaking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about why you watch the news.
Why do you watch the news?
For some, it's probably just entertainment.
They have chosen to be spectators in the world.
But for most people, if you ask them that question,
they'll say to stay informed.
Why?
Why do you want to stay informed?
Is it information for information's sake?
Or is it the hope that being better informed
makes you capable of making better decisions?
So you want actionable information.
You want information that pertains
to you that can help you make better decisions in your life.
Right?
I mean, accurate information is great.
But if there is no context to help it be applied,
it really doesn't do much good.
So the question then becomes, what
happens if your news outlet, the one that you watch or favor,
doesn't help with that?
It doesn't actually provide you that.
What if it's a news source that's constantly debunked?
Because they start off with some very dramatic headline,
and then they constantly have to walk it back.
Does that help you stay better informed?
No, because you're misled.
That dramatic headline in that huge story, breaking news,
bombshell report that most times doesn't really
turn out to be that big of a deal,
it's not actionable because it's not accurate.
It's misleading you for the sake of ratings.
So those outlets that publish bombshell reports on the daily
and they're normally kind of debunked
or they're downgraded as time goes on,
they're pretty much worthless.
They're not actually helping you stay informed,
not in the way you want to be informed.
Not in the way you want to be informed,
because that's the other part about it.
You can be informed incorrectly.
A lot of outlets do that.
A lot of outlets publish stuff they know isn't true.
And then sometimes they lack context.
And then that in and of itself makes it not actionable.
You can't use that information to make better decisions.
Perfect example right now, Biden's supply chain issues.
What?
I mean, that doesn't make a whole lot of sense.
Sure, there are supply chain issues under Biden,
but trying to assign ownership of them to Biden
kind of removes all of the context.
Because the first time I have the term supply chain
in a video on this channel,
and we know how I title my videos, is April of 2020.
And in that video,
I'm talking about how we've been talking about it.
Supply chain issues started in late 2019, early 2020.
Trying to assign ownership, removing that context,
that's not news.
That's not keeping you informed.
That's propaganda.
That's there to change the way you feel,
not inform you so you can make your own decisions.
If you get good information, you're prepared for what's coming.
You can see the future.
You know what's coming down the road.
Actually, I'm going to put that video down below
because in it, it talks about the America First strategy
of avoiding supply chain issues by bringing all production home
and just relying on production here
because then we wouldn't have these issues.
And I provide the context in that video that if we do that,
and that's how we set everything up,
it won't take a global pandemic to create supply chain issues.
It could be a localized issue at one facility
that shuts that facility down,
and then we have shortages all over the country.
How's your baby formula doing?
That's what providing context can do.
That news outlets that produce content
that intentionally removes context,
they're not news outlets.
They're propaganda outlets.
They're there to influence you, not inform you.
If somebody is trying to influence you,
they don't want you informed.
They want you misinformed.
How do you feel when you get done
watching your favorite news outlet?
If you just feel helpless, you feel afraid,
you're not watching a news outlet
because information, being better informed about something,
should remove fear.
People fear the unknown.
News should be calming to a degree.
Even if it's bad news, at least you know what's happening.
So if at the end of every monologue
put out by some network news host,
you're angry, you're afraid,
you're worried that somebody's going to take over,
are you being fed news or are you being fed propaganda?
And then you have those that don't even pretend to inform.
They just ask questions of their audience.
They ask questions in a leading way
to elicit an emotion to influence.
And if they're influencing you,
they don't want you informed.
They don't want you to have context.
Most of the pundits who go around
just asking questions,
they're intentionally leaving out context.
They're intentionally misleading their viewers
because most times the questions that they're asking,
they have answers.
Most questions do.
And most times they're easy to find.
But if they answer them and they answer them truthfully,
well, that undermines their narrative.
So what they want to do is ask a question,
that way they're legally shielded.
They didn't lie.
They didn't put out false information.
They're just asking questions.
So they ask that question in a leading way,
removing the context and allow you to draw your own conclusion
knowing that you don't have the context.
So then they can shape and influence how you feel
and they're not legally responsible for it.
The news outlets that people consume,
they greatly influence the way they behave.
They influence the quality of life that they have.
Those who consume outlets that provide context,
that provide actionable information,
they're less afraid.
They're better prepared.
They make better decisions.
They lead a better life.
It's time to start asking,
why am I watching this particular news outlet?
And if it isn't making your life better,
it isn't providing you the information you need
to actually make decisions in your life.
If it's just leaving you angry and scared,
you should probably stop watching.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}