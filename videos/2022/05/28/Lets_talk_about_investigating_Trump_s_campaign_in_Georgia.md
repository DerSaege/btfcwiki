---
title: Let's talk about investigating Trump's campaign in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F-4LQXLKws4) |
| Published | 2022/05/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Federal government's investigation into Trump's attempts to overturn the 2020 election is shrouded in mystery, leaving many questioning their actions.
- Signs indicate that the government may be building a case regarding the fake electors scheme in Georgia and other states.
- The probe, originally in Georgia, has expanded to other states, with Michigan confirmed and signs of further expansion.
- The FBI in Georgia is questioning prominent Republicans about their interactions with Trump, Giuliani, and others tied to Trump.
- Subpoenas have been issued to individuals who backed out of being electors, suggesting they may have relevant information.
- Subpoenas request communications with over 20 named individuals dating back to before the 2020 election.
- In Michigan, a potential fake elector backed out, citing illness as the reason.
- The FBI's interest in individuals who didn't participate but may have knowledge indicates a focus on potential criminality.
- The presence of someone from the National Archives during interactions adds weight to the idea of building a criminal case.
- The investigation is ongoing, with a grand jury set to meet in D.C., indicating a lengthy process.

### Quotes

- "We won't find out for a while. These things take time."
- "It's worth noting when little bits and pieces like this come out."
- "The FBI believes there might be some kind of criminality involved."
- "So when little bits and pieces like this come out, it's worth noting."
- "But the interesting thing is that it's people who backed out."

### Oneliner

Federal government investigating potential criminality in the fake electors scheme post-2020 election, expanding probes across states with a focus on individuals backing out but possibly possessing critical information.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for transparency and accountability in the investigation (suggested).
- Stay informed about updates on the investigation and share relevant information with your community (exemplified).
- Support efforts to uphold the integrity of elections and ensure attempts to undermine democracy are addressed (implied).

### Whats missing in summary

Insights on the potential implications of the ongoing investigation and the importance of monitoring government actions closely.

### Tags

#GovernmentInvestigation #FakeElectorsScheme #2020Election #FBI #NationalArchives


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about Georgia,
the electors thing that went on after the 2020 election and the government looking into that
because we get so little news from the federal government in relation to looking into Trump's
attempts to take the election that, you know, leaves a lot of people wondering if they're
doing anything at all. But now we have signs that they may actually be trying to build a case
in regards to the fake electors scheme. So we've known there was a probe going on in Georgia.
That has expanded to other states. Michigan is confirmed, but there are other states that there
are signs that it's expanded to as well. The FBI in Georgia has been talking to very prominent
Republicans down there and asking them specifically about conversations with Trump,
Giuliani, and Eastman, along with other campaign officials, people directly tied to Trump.
They've also issued subpoenas, and there's a common thread with all of the people that have
gotten these subpoenas that we know about. They all backed out. They were all supposed to be
electors. They were all going to be electors, but then didn't for whatever reason. So these are
people who might know something but didn't go through with it. So we've seen that. We've seen
it. The subpoenas are asking for communications with more than 20 named people going back to
October 1st, 2020, before the election even happened. And that's interesting as well.
In Michigan, a guy named Wall was set to be an elector on the fake thing, the alternate electors,
but he backed out. Reporting says he got corona, but either way, he didn't do it.
When he was talking to CNN about the FBI and somebody from the National Archives,
also interesting, coming to him, one of the things that he said was,
I didn't sign any affidavits, as if that was something that they had asked about,
which might make sense. He was given a subpoena to testify, and he apparently told them that he
wasn't going. That's a bold strategy. Let's see how it plays out. But the interesting thing is
that it's people who backed out and the questions about the affidavits and that statement about the
affidavits, almost as if the FBI believes there might be some kind of criminality involved with
signing. So they're looking for people who might be aware of the plan, but didn't actually engage
in that conduct. And that's an interesting turn of events, especially with somebody from the
National Archives being present. It does lend to the idea that they're actually trying to build a
criminal case. We won't find out for a while. These things take time, and there's a grand jury
apparently going to meet in D.C. about it. So we'll have to see how it develops. But this is
information coming out from DOJ about a topic that most times we hear nothing about, to the point
where we don't even know if they're doing anything. So when little bits and pieces like this come out,
it's worth noting. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}