---
title: Let's talk about adaptation, Canada, and climate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v1MJKFpJuKs) |
| Published | 2022/05/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contrasting the citizen response to climate change in Canada and the United States.
- Canadian government crowdsourcing plans to deal with climate change.
- Asking citizens for suggestions on how to make Canada more resilient to climate change impacts.
- Categories for suggestions include improving natural environment, disaster resilience, health, and managing infrastructure.
- Canada using a "whole of Canada approach" to tackle climate change.
- Shift in focus from stopping climate change to mitigating its impacts.
- Deadline for submitting ideas is in July of this year.
- Comparing the approach in Canada to the situation in the United States.
- Mentioning the Mississippi River as natural infrastructure.
- Urging for quicker action due to the urgent nature of climate change.

### Quotes

- "Countries are going to start paying more attention, and they're going to come up with various ways."
- "There has been a pretty marked shift in what's being talked about, and it's shifting away from stopping climate change."
- "Canada is using what it is calling a whole of Canada approach."
- "Americans will always do the right thing as soon as we try everything else first."
- "Maybe we could just speed that along this time because we don't have a lot of time."

### Oneliner

The Canadian government crowdsources citizen suggestions to tackle climate change while the U.S. lags behind, urging for faster action.

### Audience

Climate activists, concerned citizens.

### On-the-ground actions from transcript

- Submit your ideas via letstalkadaptation.ca (suggested).
- Participate in crowdsourcing efforts for climate change adaptation (suggested).

### Whats missing in summary

More details on specific suggestions and ideas from citizens in Canada for climate change adaptation.

### Tags

#ClimateChange #Canada #CitizenEngagement #Adaptation #USPolicy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about adaptation, Canada,
and climate change.
Shortly after I filmed that video about what's
going on in California and the citizen response to water
usage regulations out there and how the government is
engaging the citizens in California,
somebody sent me some information about what's going on in Canada, and I thought
it was an interesting contrast. So the Canadian government is crowdsourcing
its plans to deal with climate change. They're asking for the citizens to
participate in it, to provide suggestions, to talk about how to mitigate and make
Canada more resilient to the impacts of climate change. Categories that they're
looking for information and is talking about improving the natural environment,
disaster resilience and security, health and well-being, and managing natural and
built infrastructure, natural infrastructure. In the United States, the
Mississippi River is natural infrastructure. So they're reaching out
to the ground level and saying, hey, y'all actually get to see this stuff
everywhere, maybe throw us some ideas. It's interesting. You can you can submit
your ideas via letstalkadaptation.ca and it runs through July of this year. So you
have a couple months to to make your suggestions. Countries are going to start
paying more attention, and they're going to come up with various ways. What's
interesting to note is that there has been a pretty marked shift in what's
being talked about, and it's shifting away from stopping climate change, and
it's moving to how to deal with it, how to mitigate it, because it's happening,
It's occurring. There are some impacts that are now unstoppable. If we don't get
on the ball, there's going to be a lot more. Canada is using what it is calling
a whole of Canada approach. It's a good idea. It's what the United States could
be doing, you know, if we didn't have half the country worrying about something not
being taught in schools and passing legislation to stop the thing that's not
being taught, arguing with Disney or other assorted cartoon characters, and just spending
most of their time being racists.
We could be moving forward too.
But as has been pointed out throughout history, Americans will always do the right thing as
soon as we try everything else first.
Maybe we could just speed that along this time because we don't have a lot of time.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}