---
title: Let's talk about Trump, the Kingmaker of Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xcHPGNxkyOk) |
| Published | 2022/05/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the narrative pushed by the Democratic establishment that Trump is the kingmaker of the Republican Party.
- Points out that Trump often takes credit for endorsements that come in after results are known and polls have solidified.
- Suggests that showcasing instances where Trump's endorsement doesn't matter could be more valuable than playing along with the narrative.
- Mentions that the logic behind getting MAGA candidates nominated is to make them easier to beat in the general election.
- Argues that pushing back against Trump and not seeking his endorsement may be a smarter strategy for Democrats.
- Uses the example of Georgia to demonstrate that Trump's endorsement doesn't always have a significant impact.
- Notes that candidates who stand on their own without seeking Trump's endorsement have done well in elections.
- States that Trump is not a kingmaker but rather a loser, contrary to the media consensus.

### Quotes

- "Trump's not a kingmaker. He's a loser."
- "Bucking Trump, a Republican candidate who is just standing on their own saying, no, I'm not going to help you destroy the American democracy, that seems to be more valuable than dancing like a puppet for Trump to get his endorsement."
- "He's always been a loser."

### Oneliner

Beau challenges the narrative of Trump as a kingmaker, suggesting that pushing back against him may be a smarter strategy for Democrats and showcasing instances where his endorsement doesn't matter.

### Audience

Political strategists, Democratic party members

### On-the-ground actions from transcript

- Support candidates who stand against Trump's influence (implied)

### Whats missing in summary

The full transcript provides detailed insights into the dynamics of political endorsements and strategies within the Republican Party, offering a critical perspective on the influence of Trump and the Democratic establishment's approach.

### Tags

#PoliticalStrategy #TrumpEndorsements #RepublicanParty #DemocraticEstablishment


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about a narrative
in conventional media that I think's wrong.
And I think the Democratic establishment
is making an error in trying to push this narrative.
And that's the idea that Trump's the kingmaker
of the Republican Party.
If you look at a lot of these endorsements,
they come in once the result is known,
once the polls have really solidified,
and Trump just takes credit for stuff that other people did,
as he's done pretty much his whole career.
And the Democratic establishment seems
to be willing to play along with this narrative.
It doesn't make any sense.
And they're pushing it as well.
They're out there saying that it's true.
Now, from their perspective, from the Democratic
establishment's perspective, it makes sense
to get the MAGA candidates nominated,
because they think they'll be easier to beat
in the general election.
That's the logic.
And I get it.
But I think it might be more valuable to actually push Trump
out and showcase the instances where his endorsement doesn't
mean anything, which is a lot of them.
This looks like an endorsement that's worth a couple of points.
It's not a thing that drastically alter races.
And what's happening with the Democratic establishment
pushing people into this narrative
is that Republican candidates, because they believe
he's the kingmaker, swing his direction,
repeat his claims in order to get this endorsement.
If you want to know how valuable Trump's endorsement is
and how much rank and file Republicans actually
care about what he thinks, take a look at Georgia.
Georgia's Trump's, that's his windmill.
You know, that's his Moby Dick.
He's mad because Georgia wouldn't
help him find the votes.
And he has saved up a lot of that wrath
for Georgia's governor, Kemp.
Kemp is currently leading Trump's endorsed candidate
by 30 points.
That's not even close.
And this is the contest that all the Republicans
know is most important to Trump.
And they don't care.
Bucking Trump, a Republican candidate
who is just standing on their own saying,
no, I'm not going to help you destroy the American democracy,
that seems to be more valuable than dancing
like a puppet for Trump to get his endorsement.
Seems to be worth a whole lot more points.
That might be the smarter strategy for Democrats
to push, because right now, they're
pushing Republican candidates into his camp
to get this endorsement that I don't really
think it's worth what the consensus in the media says
it's worth.
It doesn't look like it to me.
When you look at the candidates who didn't play ball,
you look at the candidates who didn't bend to his whims,
you look at the candidates who really didn't even
try to get his endorsement, they did pretty well.
And it's happened over and over again.
And the one that's most important to him,
because everybody knows that Georgia is on Trump's mind,
his candidate is trailing by 30 points.
Trump's not a kingmaker.
He's a loser.
He's always been a loser.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}