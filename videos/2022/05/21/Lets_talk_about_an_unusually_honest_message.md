---
title: Let's talk about an unusually honest message....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hf0amSOuOX0) |
| Published | 2022/05/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of removing systems that contribute to inequality, addressing the need for more than a simple answer.
- Shares a message from someone who believes they are not racist but questions tearing down a system that benefits them.
- Challenges the idea of white privilege by detailing personal struggles with debt and working hard to stay afloat.
- Points out that systemic racism benefits those in power and keeps individuals down, even if they believe they are doing better than others.
- Illustrates the concept of systemic racism through a cookie analogy, showing how the system pits individuals against each other based on race.
- Emphasizes the need to dismantle systemic racism first before addressing other inequalities that directly impact individuals.
- Encourages self-interest as a reason to dismantle systemic inequalities and challenges the notion of feeling superior due to a slight edge over others.
- Urges individuals to recognize their commonalities with people of different races and to understand who truly benefits from the current system.
- Stresses the necessity of removing systemic racism as the initial step towards improving lives and breaking down other structures of inequality.
- Concludes by advocating for collective action to ensure everyone has a fair share, promoting unity and equality.

### Quotes

- "You want a reason that is self-interested. That's it."
- "You have more in common with the black people in your area than you do with those who actually really benefit from this system."
- "We get rid of that, all the other dominoes fall. Your life will improve. It won't get worse."
- "There's plenty of cookies. We just have to get enough people to say that everybody deserves a plate."
- "It's just a thought. Y'all have a good day."

### Oneliner

Beau explains the necessity of dismantling systemic racism as the first step towards addressing inequalities and improving lives for all.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Challenge systemic racism through education and advocacy (implied)
- Advocate for policies that address systemic inequalities in society (implied)
- Engage in community organizing to dismantle oppressive structures (implied)

### Whats missing in summary

The full transcript provides a detailed and compelling argument for dismantling systemic racism and addressing inequalities to create a more just society. Viewing the entire message offers a deeper understanding of the interconnectedness of individual struggles and the need for collective action.

### Tags

#SystemicRacism #Inequality #CommunityAction #Unity #CollectiveChange


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about why
you should want to
remove
systems
that contribute to inequality.
And that sounds like something that should
be a simple answer. You know, it's the right thing to do.
But sometimes people need more than that.
Because I got a pretty unique message
from somebody who watched that
video
going over why
people fall for that replacement stuff.
I always wondered if these messages were real,
because the people from my side always seem so dumb in them.
I guess I'll find out.
Watched your video about why we believe it. Yes, we.
You'd call me a racist.
I don't think I am.
I don't want anything bad to happen to other people,
but I don't want to lose to them either.
Yeah, the system is made for white people, but I've never seen any white
privilege in my life.
I work overtime every week just to get by.
I'm over my head in medical debt.
Every other month I'm trying to figure out which bill to pay late.
My friends and me carpool because normally only one of our cars is running.
I have to work my
rear
off to stay afloat.
Give me one reason to want to tear down a system that benefits me.
I don't wish ill on anyone,
but I have to take care of me first.
Give you one reason
to want to tear down a system that benefits you.
Does it though?
Does it really benefit you?
Read back your summary of your life.
Does the current system benefit you?
Or does it just benefit you a little bit more
than them?
Is it really a net good for you?
Or do you only feel like
you're better because it gave you a little bit of edge
over a group that you can look down on?
It's that cartoon, man.
It is that cartoon.
You've got the white worker and the black worker sitting there.
The white worker's got a cookie.
The business owner's sitting there, whole plate full of cookies.
Looking at the white guy,
telling the white guy that the black guy's going to take his cookie.
And you fell for it.
The system that exists, the power structures that uphold systemic racism
in this country are the same power structures that keep you down.
One thing,
in order to get to them,
the ones that impact you directly,
we have to go through systemic racism first.
We have to get rid of that first.
The rest of them will fall like dominoes.
You want a reason
that is
self-interested.
That's it.
Right now, sure,
you feel like you're doing better than the black guy down the road.
And the people who really benefit from this system,
those in DC,
those people who own millions and millions and millions of dollars worth of company assets,
they count on you to look down on him.
They count on you looking at your skin and seeing their skin
and thinking that y'all are somehow more similar
than the black guy down the road in the town you live in,
who's working overtime every week,
who's trying to figure out which bill to pay,
whose car's always breaking down,
the guy who lives the exact same life you do.
He just has a different skin tone.
You have more in common
with the black people in your area
than you do with those who actually really benefit from this system.
If you want to get to a point where you're not working overtime just to get by,
you have to start removing the systemic inequalities,
the things that are holding people down.
And before you can get to any of the other ones,
any of the ones that are going to help you directly,
we have to get rid of systemic racism.
That's the first structure that's got to go.
Because until that's gone,
there's a whole bunch of white people who think just the way you wrote,
who sit there and think,
I don't want to take that down,
because if we take that down,
then I may be doing worse off by comparison.
You're going to be in the same spot.
But once that power structure is gone,
those on the bottom,
they don't have that crutch.
They can't look down at people that had nothing to do with the problem that they're facing.
They only have one way to look.
This list of your life,
the problems that you're facing,
none of them were caused by black people.
They were caused by a system that has a lot of inherent inequality.
The biggest one and the first barrier that has to go is racism.
We get rid of that,
all the other dominoes fall.
Your life will improve.
It won't get worse.
That's the fear.
That's him telling you,
that guy is going to take your cookie.
There's plenty of cookies.
We just have to get enough people to say that everybody deserves a plate.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}