---
title: Let's talk about Natives, history, and volume being released....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IDUg7ezeBsU) |
| Published | 2022/05/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Secretary of the Interior released a report on federal government's role in boarding schools for indigenous children, parts of American history concealed and overlooked. 
- Report examines the 1819 Civilization Fund Act authorizing stripping indigenous people of their identities, culture, spirituality, hair, names, and families.
- Schools were horrific with unmarked graves and tragic stories.
- Purpose isn't just to dwell on historical wrongs but to pave the way for healing and closure.
- Legislation will be discussed to establish a commission similar to Canada's to address this history.
- Some resist discussing this history as it challenges the American myth taught in schools.
- Acknowledging and understanding these atrocities is vital to shaping the country's future.

### Quotes

- "Understanding these types of actions and making sure that they don't happen again is what will shape this country."
- "We have to go through it. We have to understand it. We have to make sure it never happens again."

### Oneliner

The Secretary of the Interior's report sheds light on the harrowing history of boarding schools for indigenous children, stressing the importance of understanding and addressing these atrocities for a better future.

### Audience

Educators, Activists, Legislators

### On-the-ground actions from transcript

- Establish legislation to create a commission for resolution and closure (implied)
- Advocate for the inclusion of this history in educational curriculums (implied)

### Whats missing in summary

The emotional weight and depth of the stories behind the horrors of the boarding schools can be best understood by watching the full transcript.

### Tags

#IndigenousHistory #BoardingSchools #Healing #Legislation #AmericanMythology


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about something
that's going to be released today.
Has to do with American history.
There are parts of American history that are forgotten,
that are overlooked, that are overshadowed.
There are also parts of American history
that are concealed, hidden away, because it's not something the United States, as a country,
wants to acknowledge or remember.
Back in June, the Secretary of the Interior commissioned a report that examined the federal
government's role in boarding houses, boarding schools for indigenous children.
The first volume of we don't know how many volumes of this report is being released today.
If you are not familiar with these schools, just go ahead and brace yourself.
This will be something that ends up alongside all of the great wrongs this country has committed
in the past.
I haven't read the first volume.
I didn't get an advanced copy or anything like that, but I would hope that it draws
attention to the 1819 Civilization Fund Act.
an act that authorized the president to civilize the indigenous people in this
country. It authorized him to instruct them in ways that might improve their
habits and conditions." Quote. What that really meant was there was money, there
was federal funding to strip indigenous people of their identities, their culture,
their spirituality, their hair, their names, and their families. These schools
are pretty horrid. On top of everything else, you will hear about unmarked graves.
you will hear about a lot of very tragic stuff. The point of this isn't just to go
through American history and look at the bad things. The point is to hopefully get
to a point where there could be some healing. Later, I want to say later this
week, there will be a subcommittee hearing about some legislation to
establish a commission to kind of get to the bottom of this and try to come up
with some form of resolution and closure. It's going to be heavily modeled and
based on the one that existed in Canada because they did the same thing. So this
This undoubtedly will be something that gets talked about.
There will be a lot of people who do not really want this to get in the airplay, that don't
want it talked about, certainly don't want it taught in schools, because it undermines
the myth that has been created, that American mythology that so many people want taught
in schools rather than American history.
If we want a country that lives up to the promises, all of those promises set out in
those founding documents.
If we want to get to the point where those documents ever mean anything, we have to acknowledge
stuff like this.
We have to go through it.
We have to understand it.
We have to make sure it never happens again.
These type of endeavors, they're not pointless.
It's not just some historical survey.
It's not something that is so long ago, it doesn't matter.
Understanding these types of actions and making sure that they don't happen again is what
will shape this country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}