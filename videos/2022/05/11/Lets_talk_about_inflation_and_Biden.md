---
title: Let's talk about inflation and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-PvFtnzeImU) |
| Published | 2022/05/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are blaming Biden for inflation, but it's actually a global issue.
- Inflation rates are record-breaking, with fuel prices up by 35% in the last 12 months.
- Building materials have increased by 15.4% over a year ago.
- Consumer durables like cars and furniture are experiencing the fastest inflation in three decades.
- Grocery prices have surged, with fruits and vegetable prices up by 6.75%.
- China is somewhat insulated from global inflation due to price controls.
- Economists predict that inflation may be peaking, but their accuracy is questionable.
- Russian inflation is at 20%, and the eurozone at 7.5%.
- The Republican base is unaware of the global nature of inflation, influenced by misinformation.
- The Republican Party has created a base in an information silo, pushing false narratives about Biden causing global inflation.

### Quotes

- "Republicans are blaming Biden for inflation, but it's actually a global issue."
- "The Republican Party has successfully created a base that exists in a total information silo."
- "Nobody who loves America wants its populace uneducated."

### Oneliner

Republicans blame Biden for inflation, but it's a global issue; misinformation traps the Republican base in an information silo.

### Audience

American voters

### On-the-ground actions from transcript

- Educate others on the global nature of inflation (implied)
- Share accurate information with those influenced by misinformation (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how global inflation is being wrongly attributed to Biden, revealing the impact of misinformation on the Republican base.

### Tags

#Inflation #Misinformation #GlobalIssue #RepublicanParty #Education


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Biden and inflation.
You know, Republicans have been pointing out
just the massive inflation.
Pretty much every Republican is out there talking about it.
And Biden recently addressed it.
And his response was to blame it on Russia,
blame it on the public health thing,
just point the finger anywhere but himself.
So what we're going to do is we're
just going to kind of sift through the information.
And we're going to see who is telling most of the truth
here.
I mean, they're politicians.
None of them are going to be telling the complete truth.
But before we do that, we need to get a real good frame
of reference on these numbers, because this
is some record-breaking stuff.
OK, fuel prices have jumped by 35% in the last 12 months.
That's the largest annual increase since 1990.
Building materials are up 15.4% over a year ago.
Consumer durables, that's like cars, household furniture,
stuff like that.
The inflation on that is rising at its fastest pace
in three decades.
Grocery prices, excluding fruits and veggies,
that's a different category, rose 2.8%
in this quarter, which is the strongest quarterly increase
since 1983.
Fruits and vegetable prices are 6.75% higher than a year ago.
Those are huge numbers, massive.
And I don't know that we could just let Biden point somewhere
and not take credit for this.
I mean, this is wild.
This is obviously his fault, since it's Australia.
These numbers are from Australia, not the US.
It's global inflation.
The eurozone is sitting at 7.5% inflation.
71% of 109 emerging and developing economies
had inflation of 5% or more in 2021,
which is double what they had in 2020.
It's global.
It's global inflation following a global pandemic.
The only country that really seems
to be insulated from this is China.
Why?
Because they have price controls.
Their economy is very, very different.
They're not getting hammered yet.
They have a lot of short-term solutions,
but long-term it tends to catch up.
Now, some economists are saying that inflation
is starting to peak.
But I'll be honest, I trust most economists' predictions
about as much as I trust a Russian gas mask.
And be glad you're not there, because their inflation
is 20%.
I want to say 17.7.
It's global.
And this is a fun gag.
I mean, it's a fun joke.
It's fun to say and point this out in this manner.
I'm sure there's a bunch of Republicans watching this
that are upset.
But it is what it is.
These are the facts.
Now, my question is this.
We know the talking heads on the conservative side,
the Republican talking heads who are putting this out there.
It's Biden's fault. It's Biden's fault.
It's Biden's fault.
We know they're invested all over the world.
We know they know it's global inflation.
And we don't expect them to tell those people who
consume their product the truth.
We gave up on that a long time ago.
They're operating in bad faith, and we've just
kind of surrendered that.
My question is, why doesn't the Republican base know this?
This is basic information.
Why are they just completely absent
when it comes to this information?
Which is weird, because some of this I found on Fox Business,
by the way.
It's not that it's not out there,
but it's not being fed to them as a talking point,
as an outrage of the day, as something to get mad about,
something to be afraid of, something to blame somebody for.
That's why they don't know it.
The Republican Party has successfully created a base
that exists in a total information silo.
The idea that Biden is responsible for the inflation
that is occurring all over the world
in more than 100 countries, I mean, that's kind of silly.
But that's the main talking point right now.
That's what they're really trying to push,
because those at the top of the Republican Party
believe that their base doesn't know any better.
And if their base falls for it, they don't know any better.
My question is, how did the Republican Party
become this easily manipulated, this easily lied to,
this easily tricked,
probably through years of demonizing education?
I love the uneducated, right?
That's how you have a large segment
of the American population
falling for a talking point that is objectively false,
that is easily debunked.
You can look to any economy in the world,
with the exception of China,
and show that it's not Biden that's causing this.
It was a reduction in demand during the pandemic,
and then a reduction in production along with it.
And then as we come out of the pandemic,
demand increases, but production is still low.
Therefore, there's low supply and high demand,
and prices go up.
It's happening all over the world.
Unless the Republican Party is suggesting
that Biden is the most powerful man on the planet,
and his policies,
whichever one they're going to try to tie this to,
are somehow responsible for inflation on multiple continents.
It just doesn't make any sense.
The only reason it's resonating
is because the base of the Republican Party
has fallen so far down that information silo,
they can't find their way out.
And if they aren't just handed the information
by somebody who claims to love America,
they won't believe it.
Nobody who loves America wants its populace uneducated.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}