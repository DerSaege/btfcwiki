---
title: Let's talk about midterm math for Democrats....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MmoINamsvb4) |
| Published | 2022/05/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of midterm elections as a moment for one party to gain an advantage by putting in effort.
- Illustrates the impact of voter turnout in midterm elections using a hypothetical voting jurisdiction with 167,000 voters.
- Breaks down the scenario of a gerrymandered district giving Republicans a 10-point lead and the effect of voter turnout on election results.
- Points out that a decrease in voter turnout during midterms can lead to unfavorable outcomes for certain groups.
- Stresses the importance of encouraging voter participation, particularly among Democrats, to secure wins in elections.
- Shows how even in districts with a built-in advantage for Republicans, Democrats can win if their voters show up in larger numbers during midterms.
- Urges against spreading messages that discourage voting, especially if it undermines protecting the rights of vulnerable groups.
- Emphasizes the critical role of midterm elections in determining the preservation or loss of rights and freedoms for many individuals.

### Quotes

- "The midterm election is a moment where Democrats can come out ahead."
- "Don't do it. Don't do it."
- "Depressing voter turnout on the side that is going to protect the people you say are your allies."
- "Even in a jurisdiction that has a 10-point lead built in for Republicans, Democrats can win in the midterms."
- "Y'all have a good day."

### Oneliner

Beau breaks down how midterm elections hinge on voter turnout, urging against actions that discourage participation and potentially harm vulnerable groups.

### Audience

Voters, Advocates

### On-the-ground actions from transcript

- Encourage voter turnout among Democrats to secure wins in elections (suggested).
- Avoid spreading messages that discourage voting, especially if it undermines protecting the rights of vulnerable groups (suggested).

### Whats missing in summary

A deeper understanding of the mathematical dynamics behind voter turnout and election outcomes.

### Tags

#MidtermElections #VoterTurnout #Democrats #RepublicanAdvantage #ProtectRights


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the math behind midterm elections
and why they're a moment for one party to come out on top
if they put forth the effort.
Midterm elections present an opportunity to shore up support,
to gain seats, even in districts that are gerrymandered.
And we're going to go over the math on how this works today.
We're going to do this because in a recent video I mentioned how
running around saying Democrats don't do anything and they're not going to help you
might cause people not to vote.
And some people had an issue with this.
I'm going to show why reducing voter turnout during midterms is bad.
We're going to take a hypothetical voting jurisdiction.
It has 167,000 voters in it, and we're using this to keep the math easy for me.
It is gerrymandered to give Republicans 55% of the vote out of the gate.
So Republicans get 55%, Democrats get 45%.
There's a 10-point lead built into this district for Republicans.
Now, in a presidential election, the year the president's being elected,
voter turnout is about 60%, which means 100,000 people are going to show up.
55,000 votes for Republicans, 45,000 for Democrats.
Republicans win.
In a midterm election, voter turnout drops to 40%, about,
which means 55% to Republicans becomes 36,700.
45% to Democrats becomes 30,060, which means that there are 15,000 Democrats
who are in this area who believe in the electoral process.
They do vote, they just don't vote in the midterms.
15,000.
What happens if Democrats just get half of them to show up?
Just half.
They win.
They win the election.
Half of it is 7,500, which means 36,700 votes for Republicans,
37,560 for Democrats.
This is in a district with a built-in lead for Republicans of 10 points.
That's why it's probably not a great idea to run around and do this right now.
Now, you may think that the Democratic Party is not great,
but they might hold the line, right?
There are a whole bunch of people who are going to have their rights on the line,
and this midterm election is going to have a whole lot to do with whether or not they get to keep them.
So even if you are somebody who, for whatever reason, doesn't vote,
maybe right now just become a little scarce.
Don't tell people not to do it.
There are people who are going to be in harm's way,
and this election is going to have a lot to do with whether or not they lose their rights,
whether or not freedoms that they have enjoyed get taken away.
The midterm election is a moment where Democrats can come out ahead.
All they have to do is to get their voters to show up in higher numbers,
and it's easier to do in the midterms because voter turnout is less.
So they have to overcome a smaller number.
Even in a jurisdiction that has a 10-point lead built in for Republicans,
Democrats can win in the midterms.
That's why right now both sides are the same rhetoric and everything.
It's very self-defeating.
The people who you may say that you're an ally to
are going to need this electoral process that you're undermining.
Don't do it.
Don't do it.
You may hold that view.
You may say it's worthless, it's pointless.
That may be the way you look at it, and that's fine.
But I don't understand the value in running around and spreading that message,
depressing voter turnout on the side that is going to protect the people
you say are your allies.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}