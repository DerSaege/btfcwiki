---
title: Let's talk about cops waiting outside, Rule 303, and the way....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DKjSLAUFWSo) |
| Published | 2022/05/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the origin of "Rule 303" and how it evolved on his channel due to the community.
- Describes how the term was initially tied to summary executions and later adopted by contractors who went beyond the rules based on their judgment.
- Talks about how the term made its way to law enforcement but shifted in meaning to "might makes right."
- Calls out law enforcement actions during incidents like Parkland where officers failed to act appropriately.
- Stresses the importance of officers engaging and pressing in situations of threat rather than waiting outside.
- Criticizes the glorification of the "warrior cop" mentality and underscores the true meaning of being a warrior.
- Emphasizes the mission's importance and the resolute acceptance of death as part of the warrior's path.
- Concludes with a reminder that those unable to commit to the mission's demands should seek a different assignment.

### Quotes

- "If you have the means to help, you have the responsibility to do so."
- "Nothing else is acceptable but continual pressure."
- "That's the way of the warrior. Because you can't stop. The mission is all that matters."
- "If you can't be in that position, if you can't commit to that and act on it when the time comes, you need a different assignment."
- "Y'all have a good day."

### Oneliner

Beau talks about the origin of "Rule 303," its evolution, and stresses the importance of responsible action, especially in law enforcement, while rejecting the "might makes right" narrative.

### Audience

Law enforcement officers

### On-the-ground actions from transcript

- Engage and press in threatening situations until there is no longer a threat (implied)

### Whats missing in summary

Beau's passionate delivery and nuanced explanation of complex issues can best be understood by watching the full video. 

### Tags

#Rule303 #Responsibility #LawEnforcement #CommunityPolicing #WarriorMindset


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about Rule 303 again.
You know, I made a video using that term in 2018, I think.
And because of y'all, because of who y'all are collectively,
it became a big part of the channel.
If you have the means at hand, you have the responsibility
to act.
And because y'all are good people,
it really turned into if you have the means to help,
you have the responsibility to do so.
And I love it.
And I love it because I love it when something good
comes from something bad.
And Rule 303, it doesn't have a good origin.
It doesn't have a good origin story.
It initially, it referenced a summary execution.
And then it became a very popular term
in the contracting community.
And it referenced times when contractors
did stuff a little outside of the lines
because they thought it was the right thing to do.
Sometimes it was morally the right thing to do,
but it was outside the lines.
Sometimes it was flat out wrong, but it
was what they felt at the time.
From there, it went into the regular army, and it got used.
And some of those soldiers became cops.
And that term traveled into law enforcement.
And somewhere along the way, it shifted meaning.
And it no longer meant, hey, you have the responsibility to act.
It meant might makes right.
And that's not what that means.
It's not what it means.
The first time I made the video was
because I saw a cop down there in Parkland wearing
one of these patches right after that cop waited outside
that school.
And reporting is saying that cops at this latest one
did the same thing, dressed in full garb, full kit.
Not just did they wait outside.
They stopped parents from entering on their own.
If you are an officer, you're ever in a situation like that.
The only acceptable tactic is to press.
You press.
You keep them engaged until there is no longer a threat.
Nothing else is acceptable.
You press.
You keep them busy.
You keep them engaged.
Yeah, they're probably going to shoot at you.
They may hit you.
But if they are shooting at you, they're
not looking at those kids.
You hit a locked door, you go through a window,
you go through the ceiling, you go through the wall.
You don't stop.
Nothing else is acceptable but continual pressure.
That warrior cop thing, it's become a thing.
But it's just clothes.
It's just gear.
What is the way of the warrior?
Death.
It's death.
That's the answer to that question.
And when you are young and stupid and you hear that,
you're like, yeah, because I'm a killer.
Death is all around me.
And then you realize that's not what it means.
It is the resolute acceptance of your own death.
That's the way of the warrior.
Because you can't stop.
The mission is all that matters.
If you can't handle that, if you don't understand that,
you need to get a different assignment.
If you can't be in that position,
if you can't commit to that and act on it when the time comes,
you need a different assignment.
You don't belong.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}