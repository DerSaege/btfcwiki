---
title: Let's talk about metaphors, hair, and Florida....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=STW75DixoAw) |
| Published | 2022/05/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recaps a speech given at a Florida high school graduation about embracing individuality.
- The speaker, a curly-haired person, shares their journey of accepting their hair.
- Describes the struggle of trying to manage and straighten their curly hair when younger.
- Talks about finding a support network that helped them accept their curly hair.
- Explains how embracing their curly hair made them happier and improved their life.
- Mentions the climate in Florida and how it affects curly hair due to humidity.
- Asserts that those opposing the acceptance of curly hair will not succeed.
- Emphasizes the importance of diverse support networks and communities.
- Affirms the right of curly-haired individuals to exist and be accepted.
- Concludes by stating that legislation cannot dictate people's hairstyles.

### Quotes

- "You want to make everybody have the same hairstyle. It's not going to happen."
- "Curly haired people exist and they have every right to."
- "You will not deprive people of that support network."

### Oneliner

At a Florida high school graduation, a speech on embracing curly hair and individuality, reaffirming the right to exist as you are amidst diverse support networks.

### Audience

High school students, curly-haired individuals

### On-the-ground actions from transcript

- Support and celebrate individuals with diverse hair types (implied)
- Cultivate inclusive communities that embrace individuality (implied)

### Whats missing in summary

The full transcript provides a detailed and engaging narrative on embracing individuality and the importance of supportive networks.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a speech that was given at a high school graduation
here in Florida.
And it was something else.
It was amazing.
You know if you've watched this channel for any length of time you know I love speaking
in metaphors.
And it's not just that I like the mental exercise.
It's that if you're doing it right you can actually get people to pay more attention.
You know a little bit of fiction can help get to the truth.
And if you're speaking in metaphors people tend to pay more attention to what you're
saying because they're trying to figure out what you're saying.
So this speech starts off with this kid and they're talking about how they want to talk
about the thing that defines them most.
The thing that most people would identify them as right off the bat.
So today I'm going to talk about my curly hair.
And they go on to talk about how when they were younger they hated their curly hair.
It was very unmanageable.
They didn't know what to do with it.
They spent a lot of time trying to straighten their curly hair.
But it didn't take.
And eventually they grew to accept it.
And over time they found a support network that helped them have the strength to just
admit that they have curly hair.
And a big part of that support network was at the school because I believe somebody at
the school also had curly hair.
And from there they talk about how once they embraced having curly hair that they became
happy.
Once they were just like yeah I've got curly hair deal with it.
It had a marked difference in their life.
It made their life better.
It's a really amazing speech.
Talked about the climate in Florida that doesn't benefit people with curly hair.
The humidity of course.
Just absolutely wonderful.
Now to those people in Florida who really don't want curly hair discussed at school,
just understand you will lose.
You will lose.
You will not deprive people of that support network.
You won't deprive them of that community because there are people with long straight hair.
There are people with no hair.
There are people with multicolored hair that are going to be very supportive of people
with curly hair.
Curly haired people exist and they have every right to.
And nothing you're going to do is going to stop that.
No legislation that gets passed is going to alter that.
You want to make everybody have the same hairstyle.
It's not going to happen.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}