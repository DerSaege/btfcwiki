---
title: Let's talk about ice cream....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-ytVbUJ3lD8) |
| Published | 2022/05/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Walmart introduced themed ice cream flavors representing pride and Juneteenth, sparking controversy on the right.
- The outrage stems from the perception of Walmart being "woke," which is refuted as capitalism, not activism.
- Many individuals within the target demographics view these products as pandering and are not pleased.
- The situation is portrayed as part of a new culture war, where representation in products becomes a contentious topic.
- The reaction to the ice cream products is seen as an overblown response fueled by outrage stokers seeking to provoke anger for engagement.
- Beau criticizes those who easily get riled up over minor instances of representation, calling it a sad way to live.
- He suggests that those manipulated by outrage are being trained to live in a constant state of anger and paranoia.
- The ice cream flavors are likened to basic capitalism rather than a political statement or agenda.
- Beau humorously advises letting go of the outrage, likening it to dealing with something frozen.
- The overall message is a call to not let minor issues consume one's energy and to focus on more significant matters.

### Quotes

- "Let it go."
- "You're melting down over ice cream there, snowflake."
- "It's just red velvet ice cream in a different container."

### Oneliner

Walmart's themed ice cream sparks controversy over perceived "wokeness," revealing a culture war fueled by outrage stokers and capitalism, prompting Beau to humorously advise letting go.

### Audience

Consumers, Activists

### On-the-ground actions from transcript

- Let go of outrage and focus on more significant issues (suggested)

### Whats missing in summary

The full transcript offers more humorous commentary and detailed explanations on the intersection of capitalism, representation, and outrage culture, providing a deeper understanding of the issue.

### Tags

#Walmart #IceCream #Representation #Capitalism #OutrageCulture


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about representation
and two flavors of themed ice cream and Walmart.
Because if you don't know, Walmart
has introduced two flavors of themed ice cream,
and it has caused quite a stir.
However, they introduced one flavor
that is there to represent pride,
and another that is there to represent Juneteenth.
This has a whole lot of people on the right wing super mad
and claiming that Walmart has gone woke.
Well, at least for once, this won't
be a video where the whole point of it
is to say, hey, that's always been woke.
No, it's Walmart.
It's definitely not woke.
Pro tip for the right wing, if your thesis,
if your idea includes Walmart being woke, you're wrong.
So what's happening here?
There is a perceived market for a product.
A company is trying to make money
by filling that perceived market.
This isn't anything woke.
This is capitalism.
That's all it is.
It seems odd to you, because for a long time,
it was a successful business model to be bigoted.
It's not anymore.
Now you make more money if you're inclusive.
The entertaining part about this is
that most of the people I know who are in the demographics
that these products are supposed to reach
aren't really happy about them.
They see it for what it is, just rampant capitalism
that's kind of pandering.
And they're not thrilled.
But apparently, this is the new culture war.
And that's really what I want to talk about.
How did this happen?
How did this become a topic?
Somebody was walking through the grocery store aisle
there in Walmart, and they were attacked by ice cream?
No.
Somebody was walking through the aisle, looked over,
saw the slightest bit of representation, and got irate,
got inflamed, took a picture of it,
and posted it to social media.
What a sad way to live your life.
You could be derailed that easily.
That's just horrifying.
But is that really what happened?
Probably not.
Probably not.
The reason you're going to be made fun of so much,
and believe me, there's going to be jokes,
and you have them coming, because you're melting down
over ice cream there, snowflake.
But what happened was somebody, those people
who like to stoke outrage, saw it.
And they saw a moment to reach into that angst
and give you permission to be your worst.
So they took a picture of it.
They put it on social media.
And they used it to stir you up, because they
know if they keep you angry, well, you'll
keep coming back to them, because they've
tapped into that.
They have you trained to basically live
your life in a constant state of anger and paranoia
that the ice cream is out to get you.
It's just red velvet ice cream in a different container.
Y'all understand that, right?
It's, anyway.
What you're dealing with here is just
basic tenets of capitalism.
That's it.
There's no agenda.
It's just a way for companies to make money.
And I think the best course of action for y'all
would be to do what you should do with anything that's frozen.
Let it go.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}