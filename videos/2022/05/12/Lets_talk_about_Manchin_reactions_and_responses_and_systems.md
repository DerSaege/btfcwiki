---
title: Let's talk about Manchin, reactions and responses, and systems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N-q4HtfgHIg) |
| Published | 2022/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he focused on the Republican Party in a recent video about a Senate vote, rather than Joe Manchin, due to the power structure in place.
- Points out that the real issue lies within a power structure that values talking points over people's lives, specifically within the Republican Party.
- Emphasizes the importance of moving from reacting to news to responding by considering the next steps and outcomes.
- Stresses the need to understand and work within the existing power structures to bring about change effectively.
- Suggests that simply removing Joe Manchin from the Democratic Party won't solve the underlying issue of power dynamics.
- Advocates for building alternative power structures that are more connected to the people to create meaningful progress.
- Encourages focusing on long-term civic engagement and building networks to influence and change existing power structures.
- Argues that defeating the current power structure requires constructing a stronger and more people-centric alternative.
- Warns against reactive responses to emotional news and advocates for strategic and thoughtful actions to achieve desired outcomes.

### Quotes

- "Reacting to something is bad. You have to respond to it."
- "The easiest way to make this transition is to ask what's next."
- "You have to defeat that structure. The easiest way to do that is to build a better one."
- "We can't react. We have to respond if you want to win."
- "What he cares about is maintaining power, and he needs that structure to do it."

### Oneliner

Beau explains why focusing on power structures over individuals like Joe Manchin is key to creating lasting change and urges for strategic responses over reactions for effective progress.

### Audience

Change-makers, Activists, Voters

### On-the-ground actions from transcript

- Build alternative power structures connected to the community to challenge existing power dynamics (implied).
- Focus on long-term civic engagement and network building to influence and change power structures (implied).
- Avoid reactive responses to emotional news and instead strategize thoughtful actions for desired outcomes (implied).

### Whats missing in summary

The full transcript provides a nuanced understanding of how power structures impact political decisions and advocates for strategic responses over reactionary actions for creating lasting change.

### Tags

#JoeManchin #DemocraticParty #PowerStructures #PoliticalChange #CivicEngagement


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about Joe Manchin,
the Democratic Party, systems,
structures, reactions, and responses.
We're gonna do this because I got a message.
In a recent video, I talked about that vote
that took place in the Senate.
Why didn't you even mention Manchin?
You focused on the Republican Party, but they needed his vote.
You could have at least mentioned him or, better yet, called for him to be thrown out
of the party."
Okay, first, I actually did mention him.
I mentioned him by name in the video.
Just throwing that out there.
But that doesn't actually really address your point.
I mean, that's just a funny thing.
You didn't hear it in the beginning.
That's fine.
I focused on the Republican Party.
Yeah.
structure that's in the way. People are focusing on Joe Manchin crossing party
lines. However, that's not really the issue. I mean, it's not good, don't get me
wrong, but the real problem is a power structure that's willing to put talking
points over people's lives. That's the issue. The Republican Party power
structure. Without that, Joe Manchin is completely irrelevant. You're focusing on
the wrong thing. That's why I talked about the Republican Party and that
structure. You could have at least mentioned him or, better yet, called for
him to be thrown out of the party. That's a reaction. That's a reaction. And any
time you're faced with news that is eliciting a reaction, you need to stop
and turn it into a response. Reacting to something is bad. You have to respond to
it. The easiest way to make this transition is to ask what's next. Okay
so the Democratic Party throws Joe Manchin out of the party. What happens
next? I don't know. You got to ask Senator McConnell because he's in control
control now. I don't see the value in that, personally. And I get it, it's sending a
message. No, it's not. Not really. They don't care. They don't care. They care
about re-election. They care about the power. I mean, understand. Joe Manchin's a
cold king. You will never find him and I on the same side of, I mean, anything,
really. We're not going to be buddies. But he's got that money, he's got that
power structure in West Virginia that's going to keep him in office. You want to
get rid of him? You have to respond. You want to replace him with somebody who's
going to at least somewhat align with the Democratic Party in their votes? You
have to respond. And how do you do that? Why does he keep getting elected? Because
he's got the money and that power structure behind him. Why was the
Republican Party the focus of that video? Because they're the power structure. How
do you think you get rid of Joe Manchin helping to build a power structure in
West Virginia to do that? Those networks we've been talking about all week, that's
what matters. You have to help people in West Virginia or maybe you're in West
Virginia yourself, I don't know, build a network that can first primary him and
win, and then win a general. You have to be able to do both. You can't just do one.
Beating him in the primary does nothing. You have to be able to win the general
as well. That's what has to happen. If you can't do that, well then I mean sure
he's gonna vote with the Republican Party a whole lot, but that is a seat on
the other side of the aisle that counts towards who controls the legislative
agenda. If you want to work within the system, you have to understand how the
system works. And if you want to win, you have to understand how the system works to
the point where you can appear to be playing by the system's rules while
quietly playing by your own. Because that's how you're going to get progress.
That's how you're going to build that better world. You're not going to be able
to vote it in. The vote every two years or four years thing, yeah, that's status
That's status quo.
You want to build that better world, you better look at those other types of civic engagement.
You better start building power structures that are aligned with you.
And then you can use those to insert into power structures that already exist.
That's how you move forward.
If you want to use electoralism, that's how you're going to have to do it.
Joe Manchin out, calling him a bad Democrat, he doesn't care. He doesn't care.
What he cares about is maintaining power, and he needs that structure to do it. So
you have to defeat that structure. The easiest way to do that is to build a
better one, build a stronger one, build one that is more linked to people than
in coal dollars.
We are probably coming into a period where there's going to be a lot of news that is
going to elicit an emotional response.
We can't react.
We have to respond if you want to win.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}