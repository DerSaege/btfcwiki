---
title: Let's talk about Quadry Sanders and Oklahoma....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8nLV05YvEz8) |
| Published | 2022/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the Sanders case in Oklahoma where two cops shot and killed someone.
- Usually, after-action analyses focus on policy deviations leading to bad outcomes, but this case is different.
- The shooting is unexplainable and perplexing, with officers already arrested and charged with manslaughter.
- The severity of the charges gives a high likelihood of conviction, but sentences for taking a life in Oklahoma are relatively low.
- The minimum sentence on the charge is four years, requiring 85% of the sentence to be served before release.
- The video footage doesn't provide a clear understanding of why the officers fired, making it one of the most inexplicable shootings.
- The charge presupposes that the officers panicked and fired, potentially leading to convictions despite normal justifications being insufficient.
- Despite the expected convictions, there may be public outcry over the sentencing.
- Beau expresses disbelief and confusion over the events, unable to comprehend why the officers fired.
- This case stands out as exceptionally puzzling and raises doubts about whether justice will be perceived.

### Quotes

- "The shooting is unexplainable and perplexing."
- "I do not understand why they fired."
- "This is the most unexplainable shooting I've ever seen."
- "I have a high expectation that there's going to be convictions on this."
- "But the normal defenses that officers use are not really going to help in this one."

### Oneliner

Analyzing a perplexing shooting in Oklahoma where officers face charges, but justice may be questioned due to the unexplainable nature of the events.

### Audience

Advocates, Activists, Reformers

### On-the-ground actions from transcript

- Advocate for transparent investigations into police shootings (implied)
- Support initiatives for police accountability and training (implied)

### Whats missing in summary

Context on the legal and societal implications of the case.

### Tags

#Oklahoma #PoliceShooting #Justice #Accountability #Manslaughter


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about something
that happened out in Oklahoma.
We're going to kind of analyze the Sanders case, what
happened out there.
This is going to be very different than most
of the videos on this channel in that genre.
You're going to hear me say something I don't think I've
for because this is a very different situation. It's one that, well we'll get there. If you
don't know what I'm talking about, two cops shot and killed somebody out in Oklahoma.
I have done hundreds of after action stuff like this. There's probably 50 videos just
on the channel where I've done this. Normally what we do is we go through the video step
step by step and I'm telling you what happens and where they break from policy,
where the cops break from policy or best practices and where they do something
that's on this list that I have another video it's called let's talk about a cop
asking me for training and I have a list of things that just tend to lead to bad
shoots and we go through and we talk about what happened and why they
occurred. Not as an excuse for what happened, but to pinpoint where the
failures occurred. This shooting is different. I can't do that because I
don't have a clue what happened. There are things that are on that list.
They didn't really use time, distance, and cover. It doesn't look like they
understood, fight, flight, or freeze, multiple officers giving commands, there's
stuff like that in it. But I don't know how any of that led to the shooting. The
shooting from where I'm sitting is unexplainable. The first part, the first
barrage is unexplainable. The second is just beyond me. I don't understand what
occurred and this is... understand I've done this a lot and that explains why
things have progressed the way they have. The officers have already been arrested.
The footage didn't come out until after they'd been arrested. My guess is that
the DA called in state law enforcement and asked them to do what I do in these
videos and state law enforcement was like, I have a clue. I have no idea why
they fired. And that led to the charges. Now they have been charged with man one.
The two officers have been charged with man one.
And there are questions about that.
And I had some.
And I looked at the statutes in Oklahoma.
And after you read the statutes and you look at the elements that the prosecution would
have to prove for the various charges, this is the most severe charge that they stand
a high likelihood of getting a conviction.
what this is. The problem is I don't think people are going to see this as
justice. The minimum sentence on this charge is four years. Now it is an 85 law
which means they'll have to serve 85 percent of the sentence before they
could even be considered for release. So it won't be one of those things where
They're sentenced to four years and due six months.
But still, it's four years.
And I went through and I looked at the other statutes,
governing, taking another person's life.
It's just a thing in Oklahoma.
They're all low.
They are all low sentences.
So it's just odd.
I don't feel that people are going
look at this video and then look at the probable sentence and feel like it's
justice. You know, one of the lawyers said that, you know, there wasn't anything
redeemable in the tape. Normally the lawyers for the families, they tend to
overstate it. I can't disagree with what he said. I do not understand why they
fired. I don't understand why they're fired the first time and the second time is just...
It... it's out there. Normally I watch these videos and break them down so y'all don't have
to watch them. If you want to understand what something looks like when it just doesn't add up,
up, this is the video to watch. This is, as far as a shooting, this is the most unexplainable
one I've ever seen. I do not get what happened. Now, the charge basically presupposes that
they panicked and fired. And maybe that's what happened. I don't know. I do not
understand what occurred here. But the normal, the normal justifications that
officers would give, they're kind of built into this statute as, yeah, okay, so
you were afraid and fired in the heat of the moment, that actually makes you
guilty of this charge. So the normal defenses that officers use are not really
going to help in this one. I have a high expectation that there's going to be
convictions on this, but then when it gets to the sentencing, there's probably
going to be outcry over that. So that's an overview of it. I don't want to say
an analysis because I do not understand what occurred. I don't understand why they fired.
So, anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}