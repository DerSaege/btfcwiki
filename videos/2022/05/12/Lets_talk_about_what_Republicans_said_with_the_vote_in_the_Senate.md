---
title: Let's talk about what Republicans said with the vote in the Senate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=q7pc7jfQh8E) |
| Published | 2022/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent Senate vote on turning Roe into federal law and its failure.
- Notes that every Republican senator, along with Joe Manchin, voted against the legislation.
- Points out that without federal legislation, the decision reverts back to the states.
- Emphasizes the lack of exemptions for ectopic pregnancies in many state laws if Roe is overturned.
- Describes the risks of ectopic pregnancies, including infection, internal bleeding, and death.
- Criticizes the Republican Party for endangering the lives of tens of thousands of women by not including exemptions for ectopic pregnancies.
- Condemns the Republican Party for valuing a talking point over the lives of their own constituents.
- Raises concerns about Republican women being disproportionately affected by these laws.
- Suggests that decisions on women's reproductive rights are being made by "mediocre men" influenced by lobbyists.
- Urges Republican women to reconsider supporting senators who endanger their lives for political gains.

### Quotes

- "The Republican Party as a whole just voted to endanger the lives of tens of thousands of women per year."
- "This is the Republican Party actively targeting Republican women."
- "They put party over people. They put a talking point over the lives of people."
- "Women will die. Women will die."
- "Because the Republican Party doesn't have a backbone, because the Republican Party doesn't have any principle."

### Oneliner

Republicans in the Senate voted against turning Roe into federal law, endangering thousands of women's lives, prioritizing talking points over constituents.

### Audience

Republican women

### On-the-ground actions from transcript

- Mobilize within the Republican Party to demand exemptions for ectopic pregnancies in state laws (implied).
- Advocate for stronger protections for women's reproductive rights within the party (implied).

### Whats missing in summary

The emotional impact on women's lives and the urgent need for advocacy to protect reproductive rights.

### Tags

#Senate #RoeVWade #ReproductiveRights #RepublicanParty #Advocacy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the Senate.
We're gonna talk about that vote in the Senate,
what it means, the message that Republicans
sent to the entire country.
We're gonna talk about the downstream effects,
the impacts that are going to occur because of this.
And we're gonna talk about what happens
when this decision reverts back to the states.
Now, if you don't know what I'm talking about,
in the United States Senate, there was a vote held.
It was a test vote to see whether or not
there was support to take Roe and turn it into federal law.
It failed.
Every single member of the Republican Party voted against it.
And Joe Manchin also voted against it.
So it will not be turned into federal law.
Absent federal legislation, this decision
reverts back to the states.
The individual states get to decide.
Many states already have laws on the books
that are set to go into effect if Roe is overturned.
There is also pending legislation all over the country.
A lot of this legislation does not include exemptions
for ectopic pregnancies.
An ectopic pregnancy is a pregnancy that is tubal.
It's not going to succeed.
It will not succeed.
Roughly 1 out of 50, 2%, tens of thousands of women per year
experience this.
Without an exemption for it, treating it is illegal.
The risks of an ectopic pregnancy
include infection, internal bleeding, and death.
The Republican Party as a whole in the Senate
just voted to endanger the lives of tens of thousands
of women per year.
The problem here is that the Republican Party
took a talking point.
And they value that talking point
more than they value people, their own constituents.
Because understand, these laws will be in very red places.
It's state by state.
This is the Republican Party actively
targeting Republican women.
Those are the people who are going to bear the brunt of this.
They put party over people.
They put a talking point over the lives of people.
That's what happened.
You're looking for a talking point, that's it.
Because that just occurred.
That's what happened.
If this is not remedied, if this isn't fixed,
women will die.
And if this is not remedied, if this isn't fixed,
women will die.
Or the law will be ignored.
This is a byproduct of allowing a bunch of mediocre men
to make decisions about things they know nothing about.
This is a byproduct of allowing people
who are under the influence of money
from lobbyists and big groups to make decisions about things
they know nothing about.
The Republican Party as a whole voted
to endanger the lives of tens of thousands of women per year.
That's what just happened.
And this isn't something that I think Republican women will
be OK with, because this isn't some other group.
This is them.
You know, when they talk about family planning,
they always paint it as the woman
that they can find some way to other and look down on.
This is 2% of women.
This is the woman at your church.
They're at risk because the Republican Party doesn't have
a backbone, because the Republican Party doesn't have
any principle, because the Republican Party is more
interested in a talking point than they
are in the lives of their own constituents.
That's why this risk now exists.
It might be a good idea to have a woman
be a good idea for the women in the Republican Party
to kind of really take stock and ask
if they are willing to lay down their lives on the re-election
campaign of some Republican senator who has never
done anything for them, because that's the situation they
find themselves in.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}