---
title: Let's talk about grain, Ukraine, and messaging....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jGOq1ze-OgM) |
| Published | 2022/05/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Proposal to break the Russian blockade in Ukraine to allow grain to be shipped out.
- Framed as a coalition of the willing outside of NATO.
- Risk of escalation exists despite efforts to minimize it.
- Ukrainian grain supply critical for many countries.
- More countries may be on board with the proposal.
- Recent weapons shipments may be connected to this plan.
- Russia is being messaged about the impending action.
- Possibility of Russian ships loading Ukrainian grain.
- Decision likely made to move forward with the plan despite risks.
- The importance of ensuring grain reaches those in need.
- Lack of U.S. involvement in the publicly named countries supporting the plan.
- Uncertainty about how Russia will respond to the proposal.
- European imperialism's impact on countries without economic power.
- UK and countries receiving the grain potentially committing ships to the plan.
- Waiting for Russia's response before taking further action.

### Quotes

- "If that grain doesn't get to where it needs to be, people will die."
- "Do you take the risk or do you once again allow those people in countries that don't have the economic power, that have nothing to do with this, pay the price for European imperialism."
- "I guess we're all waiting to see what happens."

### Oneliner

Beau outlines a proposal to break the Russian blockade in Ukraine, raising concerns about potential escalation and the critical importance of securing grain supplies for countries in need, waiting on Russia's response. 

### Audience

Global policymakers

### On-the-ground actions from transcript

- Support organizations providing humanitarian aid to regions impacted by food insecurity (suggested)
- Stay informed about international developments and crises affecting food supplies (implied)

### Whats missing in summary

The full transcript provides detailed insights into the geopolitical implications of the proposal and the potential consequences of action or inaction in addressing the grain supply situation.

### Tags

#Ukraine #RussianBlockade #GrainSupply #Geopolitics #HumanitarianAid


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about grain in Ukraine, and a
very interesting proposal that is certain to get a lot of
attention from Lithuania.
And the countries that would need to sign off on this, at
least a few of them have agreed in principle to it.
And that proposal is to break the Russian blockade, to open up the shipping lanes, to
use naval forces to open up the shipping lanes, to get to Odessa, to allow that grain to get
out.
Now they are framing this as a coalition of the willing, not terminology I would use,
it is the terminology they're using. So it's outside of NATO. This isn't a NATO
thing. This is a group of countries deciding to do this to secure the food
supply. When it's framed in the media, from what I've seen so far, they're
making it seem as though there is no risk of escalation. That's not true. There
is the way they're doing it outside of NATO makes escalation a whole lot less likely.
They're doing it in a way that is the least likely to create escalation, but there's still
a risk.
Ukraine supplies the world with grain, and there are a lot of countries, let's just say
countries that aren't major economic powers without Ukrainian grain they're
in trouble real trouble first it starts with economic problems and then if it
goes on long enough you're talking about hunger so the situation as it is is you
If you have something bad might happen, escalation, if you do nothing, something bad will happen,
hunger, people will die.
That's the situation that's been created.
Now, my read on this is that more countries than have been publicly named are already
on board.
that this is something that a lot of countries have already agreed to.
The plan is there.
It actually explains some of the weapons that have been shipped in lately.
And that what we're seeing now is the messaging portion of the show.
This is Russia, we're about to do this, and they're waiting for a response.
They're waiting to see what Russia is going to say.
At the same time they do appear to be prepping the public for the announcement this is going
to occur.
We just had satellite photos that show Russian ships loading Ukrainian grain.
Now there is, I'll say this about that too, there is a small chance, a very small chance
that that grain is from Crimea.
That's unlikely though because it's a lot and there have already been reports of Russian
troops emptying silos into trucks and taking it down.
That information being brought up at the same time as this plan is starting to be publicly
messaged, I think the decision's been made that they're going to go forward with it.
But I can't say I blame them.
If that grain doesn't get to where it needs to be, people will die.
So I can understand that.
At the same time, this does carry a risk.
Because if Russia does respond, either intentionally or they don't plan to, but something happens
accidentally with ships on the water near each other, it could escalate.
Regardless of how it's going to be framed in the media, that is a possibility, but you
have to ask, do you take the risk or do you once again allow those people in countries
that don't have the economic power, that have nothing to do with this, pay the price
for European imperialism. It's interesting to note that the countries
on board that have been publicly named thus far don't include the United States.
I haven't seen any messaging suggesting the US is going to play a role in this,
which is surprising to be honest. It looks like the UK is on board and then a
lot of the countries that would be receiving the grain are willing to
commit ships to this, at least in theory. We'll have to wait and see the details
as those get worked out. But my guess is this has all been agreed to behind
closed doors and right now this community, this coalition of the willing,
is waiting to see how Russia is going to respond so I guess we're all waiting to
see what happens. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}