---
title: Let's talk about the Georgia primaries and what's next for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oWy_6bkK2Og) |
| Published | 2022/05/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into the Republican primary in Georgia, particularly focusing on the governor's race and the anticipated results.
- Despite voting not being over yet, Beau assumes Kemp has won based on the polls.
- There is a divide within the GOP between Trump candidates and traditional Republicans competing for control in primaries.
- Beau questions Trump's reaction in states where his preferred candidate loses, pondering if he will continue supporting the Republican Party or drive down voter turnout.
- He expresses concerns about an unenergized Republican base affecting elections, specifically in Kemp's case against Abrams.
- Beau suggests that the MAGA faction's vindictive nature might lead to decreased voter turnout if Trump continues to be a divisive figure within the party.

### Quotes

- "Does he not show up to help the candidate who in this case totally embarrassed him, Kemp?"
- "They may just stay home and it may cost Republicans elections around the country."
- "All because they still refuse to do the one thing they have to do with Trump, which is get him out of the party."

### Oneliner

Beau examines the divide in the GOP between Trump candidates and traditional Republicans and speculates on the potential impact on elections due to Trump's influence.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor the dynamics within the GOP and how Trump's influence affects elections (implied)
- Support candidates based on their policies and values rather than loyalty to a specific figure (implied)

### Whats missing in summary

Insights into the potential long-term implications of Trump's influence on the Republican Party and electoral outcomes.

### Tags

#Georgia #RepublicanPrimary #Trump #GOP #Elections


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the Republican primary
in Georgia, specifically the governor's race
and the results.
Now I'm gonna go ahead and be honest,
voting hasn't ended yet.
Okay, voting hasn't started.
It's a couple of days ago by the time y'all watch this.
But those polls, wow.
So if I'm wrong about this,
feel free to go ahead and Photoshop my face
onto that Dewey defeats Truman photo.
But I'm going to assume that Kemp has won.
Now it gets interesting though.
I mean, it's funny, yeah, Trump's candidate lost,
probably going off the polls by a pretty wide margin.
So what happens next for Trump?
What's he gonna do when it comes to Georgia?
There's an interesting dynamic that's being set up
throughout the GOP right now.
There's Trump candidates
and then there's like normal Republicans.
Those are who's vying for control in these primaries.
In states where Trump's candidate loses,
what does he do?
Does he not show up to help the candidate
who in this case totally embarrassed him, Kemp?
Does he just write off the Republican Party
in states where his candidate doesn't win?
And if so, what does that do for Republican voter turnout
in the general elections?
Trump's combative nature combined with his
just historic levels of pettiness
leads me to believe that in states
where his candidate doesn't win,
he won't back the Republican Party.
In fact, he may actually talk bad about the people
who beat his candidates.
Talk bad about the people who beat his candidates
or he'll say nothing
and drive down Republican voter turnout in those states.
I mean, Kemp is clearly at this point
when I'm filming, clearly the forecasted winner.
But without an energized Republican base,
are they gonna show up when he squares off against Abrams?
That is something that is probably weighing pretty heavily
on the minds of normal Republicans
because the MAGA faction with their level of pettiness,
there's no other word, the vindictive nature of this group,
they may just stay home
and it may cost Republicans elections around the country
all because they still refuse to do the one thing
they have to do with Trump, which is get him out of the party.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}