---
title: Let's talk about Republican reaction to the SCOTUS leak....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sOZTy0h6XZY) |
| Published | 2022/05/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans achieved a significant victory but are not celebrating it.
- The Republican Party seems to be avoiding talking about the victory because it is overwhelmingly opposed by the American people.
- There is concern among Republicans about the information regarding the victory being leaked before the midterms.
- Beau suggests that the Republicans used this issue as a wedge to motivate their base, even though it is unpopular.
- He encourages the Republican Party to stand by its actions and claim credit for fulfilling their promises.
- Beau believes that Democrats should seize this moment and make passing legislation a priority to address the issue that the Republicans pushed.
- Failure to address this issue could lead to significant losses for the Democratic Party in the midterms and beyond.

### Quotes

- "Americans don't want this, right?"
- "You're a dog that caught the car. Now you got to figure out what to do."
- "And this is the moment for the Democratic Party to step up."
- "If you're handed the House, if you don't just suffer amazing losses, the midterms, and you don't fulfill that implied promise to the women of this country, you can kiss 2024 goodbye."
- "It's what you campaigned on. It's what you promised. And you finally fulfilled it."

### Oneliner

Republicans achieved a significant victory but avoid celebrating it due to overwhelming opposition, prompting Democrats to seize the moment and prioritize addressing the issue.

### Audience

Political activists

### On-the-ground actions from transcript

- Pass legislation to address the issue pushed by Republicans (implied)

### Whats missing in summary

The full transcript provides detailed insights into the political landscape surrounding a significant victory achieved by Republicans and the implications for both parties.

### Tags

#Republicans #Democrats #PoliticalStrategy #Legislation #Midterms


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about what Republicans are talking about.
Of course, I can't really figure out why they're talking about it.
Doesn't make any sense to me.
Because if you think about it, the Republican Party just
had a great victory, a massive victory, a huge victory.
They have achieved something that they have campaigned on for decades.
They fulfilled a promise decades in the making.
But they're not out there running victory laps.
It's really weird.
I mean, it seems like everybody involved in the Republican Party
should be out there going, look what we did.
It took us a long time, but look what we did for you, the American people.
Look how we achieved our goals.
Look how we set out to accomplish what you wanted.
And there it is.
Because Americans don't want this, right?
80% of Americans support access.
And right around 70%, what is it, 68% to 72%, 67% to 72%,
somewhere in there, depending on the poll,
specifically oppose roving being overturned.
And only 20% of Americans point it banned.
It's almost like they didn't actually really want to do this.
It's almost like it was a wedge issue to motivate a base.
And now that it has happened, or is apparently going to happen,
they're super worried.
They're worried about that information coming out before the midterms.
That's what it's about, right?
I mean, it seems as though y'all should be trumpeting this and saying,
look, re-elect us.
We did what you wanted.
But the reality is you did something that is just overwhelmingly opposed.
So they don't want to talk about it.
Instead, they want to talk about the leak.
If you're talking about the leak rather than what's going on,
if you're talking about the leak and wanting
to go after whoever did it instead of celebrating what you accomplished,
I think the only thing people can infer from that
is that you're too scared to tell the American people that you had
something to do with it because you know that it is wildly unpopular.
You used it as a wedge issue to motivate a base,
apparently not paying attention to the fact that for the last,
I don't know, decade and a half or so, that position
has become super unpopular.
And it becomes more unpopular as time goes on.
So this victory, you can't expect us to forget about it.
You can't expect us not to talk about it.
We have to make sure that the Republican Party gets
all the credit it deserves for accomplishing this.
And I know what you're saying.
It's probably going to cost the Republican Party
the woman vote for a generation.
But I mean, it's a small price to pay for doing what's right, isn't it?
That's what you've said for decades.
It's what you campaigned on.
It's what you promised.
And you finally fulfilled it.
So get out there and just claim that.
Stand by it.
Be bold.
Go out there and say, yes, we accomplished it for you,
the American people.
I think that's the smartest move.
In fact, I think it might be a good idea
to make sure that anybody who voted
to put these Supreme Court justices up there is tied to it.
I think everybody should be talking about that, especially
the Democrats.
I think they should be mentioning it constantly.
80% of Americans want access.
Right around 70 oppose overturning Roe.
You're a dog that caught the car.
Now you got to figure out what to do.
And this is the moment for the Democratic Party
to step up, because this is your chance for the midterms.
You have to seize it.
And if the American people do hand the Democratic Party
the House and the Senate, the number one priority
should be to pass legislation to make sure
that this wedge issue goes away, to turn it into law
and do what should have been done a long time ago,
do what the American people are asking you to do.
And if you don't, if you're handed the House,
if you don't just suffer amazing losses, the midterms,
and you don't fulfill that implied promise
to the women of this country, you can kiss 2024 goodbye.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}