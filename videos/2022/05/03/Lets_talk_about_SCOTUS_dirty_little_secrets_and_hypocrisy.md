---
title: Let's talk about SCOTUS, dirty little secrets, and hypocrisy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a4zUTEU7mD0) |
| Published | 2022/05/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Living near small towns, understanding hypocrisy and secrets, sets the stage.
- Tells a story about a woman splitting from her fiancé and lying about going to Pensacola Beach.
- Beau catches the woman in her lies, revealing her true intentions.
- Talks about small towns and the need for secrecy due to social pressures.
- Mentions the prevalence of lying about visits to certain facilities in conservative areas.
- Evangelical Protestants and Catholics make up a significant portion of visits to these facilities.
- Despite holding certain beliefs publicly, individuals keep their visits a secret due to societal pressures.
- Beau criticizes Republicans for not matching policies with the reality of what people want and do.
- Acknowledges the tough choices many women face and the importance of preserving options like safe access to healthcare.
- Concludes with the prediction that many women will keep their voting choices as another "dirty little secret" to protect future options.

### Quotes

- "It's happened a lot over the course of my life, and those are just the ones I picked up on."
- "They treat it like a dirty little secret that nobody's ever going to know about or discuss."
- "I think that the Republicans have made a grave error."
- "Not everybody gets a happy ending."
- "They're going to go somewhere, they're going to do something, and they're never going to tell us all about it."

### Oneliner

Small-town dynamics reveal the layers of secrecy and hypocrisy surrounding personal choices, especially in conservative settings, impacting decisions on healthcare and voting rights.

### Audience

Small-town residents

### On-the-ground actions from transcript

- Support and advocate for policies that protect healthcare access for all individuals, especially in conservative areas (implied).
- Encourage open and honest dialogues about sensitive topics to reduce the need for secrecy and social pressures (implied).

### Whats missing in summary

The full transcript provides a deep dive into the intricacies of small-town dynamics, secrecy, and the impact of societal pressures on personal choices, making it worth exploring in its entirety.

### Tags

#SmallTown #Hypocrisy #Secrets #ConservativeAreas #HealthcareAccess


## Transcript
Well, howdy there, internet people. It's Beau again.
I think most of y'all have picked up on the fact that I live near a small town,
and that for most of my life,
I have lived
in or around small towns.
Because of that, I know a whole lot
about hypocrisy
and secrets.
And that's what we're going to talk about today.
We're going to talk about the Supreme Court,
hypocrisy,
and dirty little secrets.
To start, I'm going to tell you a story that has been
lightly edited
to protect the innocent.
Me and a friend of mine, who I've
talked about on the channel before,
he's the guy, I said, he's the best capitalist I've ever met
because he runs his business like a socialist.
We're in a piggly wiggly,
and we see this woman we know.
A couple nights before,
she had split from her fiance
in a very public fashion.
It occurred at the local bar,
and
from what we were able to gather,
she reminded him that he wasn't supposed to be there,
because he was supposed to stop going.
And he told her
that this is the way it was, and she better get used to it.
And she
threw the ring at him,
and that was the end of it, and it was very final.
So we see her,
and we're like, hey, how are you holding up?
And she's like, I'm a little messed up,
I'm going to take a couple days and I'm going to go to Pensacola Beach.
When she said that,
I was pretty certain she was lying,
but it's not my business.
You don't want to tell me how you're feeling, where you're going, what you're doing,
that's fine.
I don't take offense to it.
My friend is a good guy.
He is a good guy.
So upon hearing this,
he's like,
you know,
my place down on 30A, it's going to be empty this week if you just want to go down there instead.
And she goes,
um,
well,
then I knew she was lying.
So I turned to him and I'm like, you said Jason could stay there.
He's going to be there this week.
And he's like, did I?
I don't remember that.
He's been talking about it for two days.
And he's like, well, you know, it's not like they don't know each other, it's big enough for the both of them.
I was like, yeah, she just split from her fianc??,
and you want her to go down and shack up on the beach with Jason. That makes sense.
He's like, yeah, it's probably not a good idea.
And at this point,
he is kind of figuring out that I'm really just telling him to be quiet.
And everything would have been fine.
But while we were talking, she was coming up with her next lie.
She's like, oh, I have to go to Pensacola anyway,
because I'm going to see a friend from high school.
You are the worst liar in history, you know,
because they went to high school together.
So the next words out of his mouth were,
oh, who are you going to see?
I'm like, dude, are you writing a book?
And he's like, I'm sorry. I'm sorry.
You know, this is none of my business.
I hope you feel better.
You know, let me know if you need anything, all of that normal stuff.
And we part ways.
Soon as she's out of earshot, he's like, dude, what was that about?
Like, I don't know, but it's obviously not our business.
The reality is I did know,
because that was the third time
I had heard that exact lie in my life.
And it never made any sense.
It never made any sense.
If you're not familiar with the geography,
due south of where we are, much, much closer,
is Creighton, all of 38, destined.
If you're not familiar with these places,
just type in most beautiful beaches in the world.
They'll be on the list.
To go to Pensacola Beach from where we are,
you have to drive past this.
You know, these areas, they have the prettiest beaches.
They have the best restaurants.
They have the best, well, pretty much everything
if you're looking to unwind for a couple of days.
You know what they don't have?
A clinic.
They don't have a clinic.
Now, over the years, I have also heard about concerts,
people going to Pensacola for a concert that didn't exist.
Found that out because I wanted tickets
and then found out it wasn't happening.
So, they lie.
They treat it like a dirty little secret,
and I have never taken offense to this
because they kind of have to.
You can't hold it against them.
These are small towns.
They are very Republican, very rural
in everything that that entails.
You tell one person, you might as well just tell everybody.
So, they go over there.
They have their appointment.
They come back, and they never talk about it again
because if they do, man.
About the only thing more socially unacceptable
than going for a lot of them is not going,
and they're in that situation.
So, they make that choice,
and they never really know who would be okay
with the choice that they made, so they just lie about it.
It's happened a lot over the course of my life,
and those are just the ones I picked up on,
and it doesn't seem like it would happen that often
in the areas where I've lived,
predominantly evangelical Protestants,
because they're one of the loudest voices
when it comes to wanting to close these facilities,
and that's true, and especially if you look at polling.
The thing is, evangelical Protestants
make up more than 10% of those visits to those facilities,
13%, and then the other large group is Catholics,
very vocal.
They make up more than 20%.
When you add it together, it comes in close,
almost 40%, sometimes over 40% depending on the year.
So, because of social pressures, they have their appointment.
They never talk about it,
and they treat it like that dirty little secret
that nobody's ever going to know about or discuss,
and you can't blame them.
They come back.
They go back to their church.
They espouse all of the same beliefs.
If you asked them about it,
they would hold the same opinion before and after their visit,
and you can say it's hypocritical, and sure, it is,
but I find it really hard to condemn them for it.
I think that the Republicans have made a grave error.
They have gone off of trying to grant a small group of people
what they say they want rather than what they really do.
The percentages don't actually match this move.
I have a feeling that a whole lot of these women understand
that their life is dramatically different
because that option was available to them
because had they not made it, their life would be
drastically different, and not everybody gets the daughter
who ends up in an Ivy League school while they own a hotel.
That's TV.
In real life, not everybody gets a happy ending.
I have a feeling that a whole lot of these women
are about to have another thing
that they view as a dirty little secret.
They're going to go somewhere, they're going to do something,
and they're never going to tell us all about it.
This time it won't be in a clinic.
This time it'll be in a voting booth
because I have a feeling that a whole lot of these women
are going to want to preserve that option
for their daughters.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}