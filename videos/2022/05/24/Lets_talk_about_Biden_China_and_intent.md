---
title: Let's talk about Biden, China, and intent....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QLqtYPWvHh0) |
| Published | 2022/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's statement about defending Taiwan led to confusion and speculation.
- White House clarified Biden "misspoke" and the US is not ready to defend Taiwan.
- Speculation arises about Biden's competence and decision-making.
- Russia's poor planning and intelligence in Ukraine led to unexpected outcomes.
- Understanding an opponent's intent is key in foreign policy.
- Lack of information about Putin's intent creates speculation and worry.
- Biden's unpredictability adds to the uncertainty in foreign relations.
- Openly stating intentions, like in the Afghanistan withdrawal, can have negative consequences.
- Masking intent is vital in foreign policy to keep adversaries guessing.
- Confusion and ambiguity can be strategic tools in international relations.

### Quotes

- "What Biden did is normal. That's what foreign policy is supposed to look like."
- "It's an international poker game where everybody's cheating."
- "This is what foreign policy looks like when you have a good team."
- "It's actually an asset because people know that he goes off script."
- "Confusion and ambiguity can be strategic tools in international relations."

### Oneliner

Biden's remarks on defending Taiwan reveal the complex dance of foreign policy, where ambiguity and unpredictability can be strategic assets.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

<!-- Skip this section if there aren't physical actions described or suggested. -->

### Whats missing in summary

Insight into the nuanced approach to foreign policy and the strategic value of ambiguity.

### Tags

#Biden #China #ForeignPolicy #InternationalRelations #Russia #Taiwan


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Biden and China.
We've been talking for a couple of weeks,
saying that because Russia is in the situation that it is,
that the US can now turn its attention to China.
And now that's happening, right?
And Biden opened up by saying,
hey, we're ready to defend Taiwan.
And a chill filled the room.
And almost immediately,
the White House put out a statement saying,
oh no, no, no, he misspoke.
We're not really ready to do that.
Now, a lot of people have taken this as a sign
that Biden doesn't know what he's doing.
That he isn't familiar with what the US is ready to do
or not ready to do.
That it's just somebody else making the decisions for him.
Okay.
Why did Russia's invasion of Ukraine go off script so badly?
Poor planning, right?
Poor planning, poor intelligence.
Intelligence being a key part here.
The holy grail of intelligence is intent.
Being able to discern your opponent's intent,
what they're going to do.
Being able to distill it down.
And that's what all of those assessments
and estimates and everything,
that's really what they're about,
is trying to guess what the other guy's gonna do.
Right?
Russia didn't do a really good job of that.
And we see the results.
Let's flip the script here for a second.
In the US media, in Western media in general,
how much time is devoted to speculation
over whether or not Putin will invade, pick the country?
Whether or not he'll do the unthinkable.
Whether or not he'll use nukes at a tactical level.
A lot.
Why?
Because we don't know his intent.
And that space that lacks that information
creates speculation, creates apprehension, creates worry.
What Biden did is normal.
That's what foreign policy is supposed to look like.
It's an international poker game where everybody's cheating.
That means everybody's lying.
Now in Biden's case,
because he does actually go off script pretty often,
it's even better because nobody really knows.
Did he go off script and accidentally tell the truth?
We don't know, right?
And that's the point.
We don't know, neither does China.
I can assure you that when it comes
to a nation like China, that was intentional.
Now, whether the US is actually ready
to defend Taiwan or not, well, I don't have a clue,
but that's the point.
This is what foreign policy looks like
when you have a good team.
You don't give away intent.
And I know there are people who are gonna say,
well, you're just defending Biden
or you're just defending Biden's foreign policy team
because you actually have spoken highly of them.
Let's do it another way.
What does it look like when you don't mask your intent,
when you're just very open about what you want?
The withdrawal in Afghanistan.
That's what it looks like.
See, Trump early on was like, we're gonna leave.
We're gonna leave, kept saying it over and over again.
So the opposition there, they knew we were gonna leave.
So they knew our intent.
So what did they do?
They didn't have to waste resources
trying to drive the US out.
They could prepare for the battle
with the national government.
They could spend those resources, I don't know,
secretly depositing a whole bunch of troops in the capital,
which is what they did.
That's why it's important to mask your intent.
You can see it.
There's examples of it both ways.
Now, I know it's easy for me to say that.
Now, in hindsight, I'll put a video down below from 2019
that says exactly the same thing about Afghanistan
because as soon as he started publicly saying
and telling the opposition that we were leaving, it was over.
What happened at the withdrawal was going to happen
no matter what.
It was set in stone at that point.
So when people try to turn this into some major issue,
understand it's intentional.
China has no clue whether or not the United States
would militarily defend Taiwan now.
They don't have a clue.
That lack of information will create speculation,
which will create hesitation, which is the goal.
Now, again, to reiterate, Biden does actually
go off script a lot.
I'm 100% certain this isn't one of those times.
This was planned.
But just do understand that he does go off script
and say things that probably weren't planned.
But on the foreign policy scene, that actually helps.
Not very useful domestically, but on the foreign policy scene,
it's actually an asset because people
know that he goes off script.
So that factors into them trying to discern what he really meant.
And it just makes it even more confusing
and creates more of a vacuum of information,
which creates more hesitation.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}