---
title: Let's talk about renaming military bases....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zk_ZZvGgSi4) |
| Published | 2022/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US military is renaming installations named after Civil War traitors by 2023.
- Congress-created commissions suggest new names for the installations, which must be confirmed by Congress.
- Fort Bragg will become Liberty, Hood will be renamed Cavazos, and Gordon will change to Eisenhower.
- Lee will be jointly renamed after General Greg and Charity Adams, the first black woman officer in the Women's Army Auxiliary Corps.
- Pickett will be renamed after Van Barfoot, a Medal of Honor recipient known for embarrassing Klansmen.
- A.P. Hill will be named after Dr. Mary Walker, an abolitionist and Medal of Honor recipient during the Civil War.
- Benning will be renamed after Hal Moore, known from the movie "We Were Soldiers."
- Rucker will be renamed after Michael Novosel, a Medal of Honor recipient who flew helicopters during Vietnam.
- Fort Polk will be named after Sergeant William Henry Johnson, a Medal of Honor recipient from World War I.
- The renaming process has widespread support and aims to address discomfort at installations named after controversial figures.

### Quotes

- "It is time for this to happen, and it has a lot of support, both across the country and within the military community."
- "There are a lot of people who don't like working at installations that are named after people that they really don't like."

### Oneliner

The US military is renaming installations named after Civil War traitors by 2023, with widespread support and a focus on honoring diverse heroes.

### Audience

Military personnel, historians, activists.

### On-the-ground actions from transcript

- Support the renaming process by advocating for the changes in your community (implied).

### Whats missing in summary

The transcript provides a detailed overview of the upcoming renaming of US military installations, shedding light on the diverse heroes who will be honored in place of controversial figures.

### Tags

#USMilitary #Renaming #CivilWar #Heroes #Support


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about the US military
renaming some of its installations.
And the process involved with that,
we'll go over some of the name changes,
provide a light biography of some
of the people who will have installations
named after them, and just kind of run through it all.
If you have no idea what I'm talking about,
the US military, after all of this time,
has decided, hey, maybe it's not such a good idea
to have major installations named after traitors
from the Civil War.
So by 2023, all of those names will be changed.
The list I'm about to read is from a commission.
These are suggestions.
They have to be confirmed by Congress.
However, the reason these commissions are created
is so Congress can wash their hands of it.
This way, the members of Congress
aren't put in politically compromising situations.
Generally speaking, when these lists come out,
this is what happens.
OK.
So Fort Bragg becomes Liberty.
Hood becomes Cavazos, who is listed
as the first Hispanic four-star general.
Gordon becomes Eisenhower.
Lee will be jointly renamed after General Greg and Charity
Adams.
Charity Adams was the first black woman officer
in the Women's Army Auxiliary Corps.
Pickett will be renamed after Van Barfoot.
Now Barfoot, Medal of Honor recipient.
However, that's probably not the reason he was chosen.
The fact that he has Native ancestry
is probably not the reason he was chosen.
He was probably chosen because he had
a habit of embarrassing Klansmen.
One of the more prominent ones occurred in 1945
when a senator who was in the Klan basically asked him,
you have any trouble with those black folk over there?
And in the most southern way possible,
he was like, nah, I found out they fight just as good
as white folk.
I'm paraphrasing here.
And then he goes on to just drive the point home,
saying, you know, a whole lot of us
have changed our opinions about stuff like that.
I have, and a whole lot of other people in the South
have too, which if you're not from the South,
you may not understand the bless your heart
nature of that statement.
A.P. Hill will be named after Dr. Mary Walker, who
was an abolitionist, a women's rights activist,
a Medal of Honor recipient during the Civil War,
and a prisoner of war.
Benning will be renamed after Hal Moore.
That's pretty fitting.
We Were Soldiers, the Mel Gibson movie,
the commander in that, that's who that is.
Rucker will be renamed after Michael Novosel.
This is another interesting story.
He joined the Army Air Corps at like age 19,
fought during World War II, went into the reserves,
came back for Korea in the Air Force,
went back into the reserves.
When Vietnam popped off, he was a lieutenant colonel.
And the Air Force was kind of like, yeah, we actually
don't need higher ranking people.
So you can just sit this one out.
So he gave up his rank and joined the Army
and wound up as a warrant officer, a CW4,
flying helicopters with Special Forces Aviation.
During Vietnam, he became a Medal of Honor recipient
for flying in repeatedly to evacuate South Vietnamese troops.
Side note, also a really nice guy.
I met him, I want to say in Mobile, like 20 years ago.
Fort Polk will be named after Sergeant William Henry Johnson.
I don't want to give too much away about this,
because this one will certainly be turned into a movie at some point.
During World War I, he and his partner, a buddy,
were on sentry duty, and they were surprised by German troops.
His buddy gets wounded, and he decides to just fight him off anyway.
Short version.
He's wounded repeatedly, and Teddy Roosevelt
referred to him as one of the five bravest Americans in the war.
However, he didn't really get any recognition for this at the time.
Not official recognition, anyway, because he was black.
He was awarded the Medal of Honor in 2015, almost 100 years later.
So these are the brief biographies of the people involved, of some of them.
This is going to be the new list.
Odds are this is going to be approved, and this is going to go into effect.
Generally speaking, Congress lets these commissions do their job,
and they don't want to try to override it, because if they get involved,
then they get sucked into the political debate,
and they're probably not going to want that,
because it is time for this to happen, and it has a lot of support,
both across the country and within the military community.
There are a lot of people who don't like working at installations
that are named after people that they really don't like.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}