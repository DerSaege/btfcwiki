---
title: Let's talk about hurricanes and supply chains....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F2HGluxjzRA) |
| Published | 2022/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Loop current active, big hurricane year expected.
- Stressed supply chains mean delays in supplies and relief post-hurricane.
- Private companies may struggle to provide relief due to supply chain issues.
- Individuals in hurricane-prone areas urged to prepare for longer recovery times.
- People in evacuation areas should also prepare for reduced supplies due to increased demand.
- Even those far from hurricanes should prepare for supply chain stress post-storm.
- Stress on supply chains nationwide after hurricanes will impact availability of goods.
- Urges viewers to plan evacuation routes, monitor weather, and ensure access to gas.
- Recommends checking home insurance policy and gathering necessary documents.
- Suggests preparing emergency supplies including water, fire, shelter, knife, first aid kit, meds, tarp, charcoal, and water.
- Encourages finding ways to charge electrical devices in advance.
- Warns of high prices during evacuation and need to be prepared for extended sheltering.
- Expects longer recovery periods post-hurricane due to stressed supply chains.

### Quotes

- "Make sure that you're prepared for that."
- "Plan your evacuation route now."
- "You need to prepare now, not once the news of the storm breaks."
- "It's going to take even longer to get back up and running."
- "Keep an eye on the weather."

### Oneliner

Be prepared for hurricanes this year - stressed supply chains mean longer recovery times and reduced supplies. 

### Audience

Residents in hurricane-prone areas

### On-the-ground actions from transcript

- Plan your evacuation route now (suggested)
- Gather emergency supplies including water, fire, shelter, knife, first aid kit, meds, tarp, charcoal, and water (suggested)
- Ensure access to gas for evacuation (suggested)
- Prepare to outlast normal period if sheltering in place (suggested)

### Whats missing in summary

Importance of charging electrical devices for emergencies

### Tags

#HurricanePreparedness #SupplyChainStress #EmergencyPreparedness #EvacuationPlanning #CommunitySafety


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about hurricanes and supply chains,
and why, even if you're not in a hurricane-prone area, you probably want to pay attention this year.
The loop current, which is a feeder for big storms, it's active. It looks like it's going to be a big year.
Our supply chains are already stressed, which means if you're in an area where hurricanes come ashore often,
you need to be ready for it to take additional time for things to get back to normal,
for supplies to show up, for relief to show up.
In recent years, a lot of the relief that came in was from private individuals or private companies,
and they were offloading supplies they had on hand.
Because supply chains are less than perfectly functioning right now, that's going to be harder for them to do.
You're going to be on your own longer.
Now, if you're not in an area that is prone to hurricanes, but you're in an area where people tend to evacuate to,
like if you're in the southeast, if you're in Birmingham or Atlanta, you need to be ready.
Because when one comes ashore, what happens? A whole bunch of people evacuate to your area.
Supply chains are already stressed.
So, when you have a whole bunch of new people show up, and they're buying stuff up because they don't have anything,
it's going to reduce supply.
So, you need to be ready for it too, even if under normal circumstances you're not,
and you don't really have a huge need to be.
On top of that, if you're nowhere near it,
understand after a hurricane, it's going to stress the supply chain across the country.
So, it's going to be harder to get stuff everywhere,
because a whole bunch of supplies, a whole bunch of products, will have been lost during the storm,
and they'll need to be replaced.
So, it's going to spread out, make the coverage of the supplies even thinner.
So, you need to prepare now, not once the news of the storm breaks.
I have a whole playlist on emergency preparedness, and a whole lot dealing specifically with hurricanes.
So, you can go back and review them all, but the short version, plan your evacuation route now.
Figure out where you're going to go if you're in a normal impact zone.
Keep an eye on the weather, and make sure you have gas to get there.
Then, look at your insurance policy for your home, get your documents together,
all of that stuff, all the normal stuff that you do.
Go ahead and do it now.
Then, make sure you have your supplies.
Include water, fire, shelter, knife, first aid kit, to include any meds you need.
The supply chains are stressed, so it's going to take longer for the pharmacies to fill back up if they lose stuff.
You might also want to get a tarp to cover any damage to a roof, something like that,
and charcoal for cooking, and water.
I can't stress that enough, because that may take some time.
It would be great if you could find a way to charge your electrical devices.
That's not going to be an option for everybody.
But, you need to have this stuff ready and be prepared.
If you're going to evacuate, be prepared for, one, it to be expensive,
because prices are high, and they're going to go up during the storm.
If you're going to shelter in place, you need to be prepared to stay longer than you normally would.
You need to be ready to outlast the normal period.
Normally, a day or two, and the first bits of relief are showing up.
Within a week or two, there's a lot of resources in the area, and you just have to find them.
Then, things start to smooth out.
It's going to take longer, because the supply chain is stressed.
So, if we have big storms, as it appears we very well might,
when they hit, it's going to take even longer to get back up and running.
Make sure that you're prepared for that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}