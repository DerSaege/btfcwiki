---
title: Let's talk about a problem at an intelligence school....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tYuc2VRU47Q) |
| Published | 2022/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Bowie Gun says:

- Addresses the need for the US military to become more "woke" by having basic respect for others.
- Describes a Marine School for Intelligence where instructors were investigated for harassment and misogynistic behavior.
- Mentions inappropriate relationships, a chat room with misogynistic attitudes, and a general tone of acceptance towards such behavior.
- Points out the negative impact of this behavior on female students, potentially discouraging them from pursuing intelligence roles.
- Warns that male students may adopt misogynistic attitudes, leading to undervaluing their female counterparts.
- Emphasizes the danger in deploying individuals who underestimate their female counterparts due to misogyny.
- Notes that women can excel in intelligence roles as they are often underestimated and can access certain information more effectively than men.
- Criticizes the lack of adequate corrective action taken in the Marine School for Intelligence.
- Raises concerns about the dehumanizing nature of militarism and its implications for civilian casualties.
- Stresses the importance of proper intelligence in minimizing civilian losses during military operations.
- Calls for the military to address dehumanizing behaviors to prevent further civilian harm.

### Quotes

- "Because their counterparts who are women, well they'll be underutilized because they're undervalued, because they're underestimated, because of the misogynistic attitude."
- "How does that happen normally? In real life, it isn't, you know, the sergeant gets hit and then the lower enlisted just lose it on an entire village. That happens, but it's super rare."

### Oneliner

Bowie Gun stresses the need for the US military to combat misogyny and dehumanization to enhance intelligence capabilities and reduce civilian casualties.

### Audience

Military personnel, advocates for gender equality

### On-the-ground actions from transcript

- Advocate for gender sensitivity training in military institutions (suggested)
- Support initiatives that address misogyny and harassment in military training (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into the harmful effects of misogyny and the importance of gender equality in intelligence training within the US military.

### Tags

#US military #Misogyny #Gender equality #Intelligence #Civilian casualties


## Transcript
Well howdy there internet people, it's Bowie Gun.
So today we're going to talk about why the US military needs to become more woke.
I know with all of the talk about how it's become too woke, that sounds odd.
Woke at this point, we're just going to use the general Republican definition, which is
having some basic respect for other people.
So at the Marine School for Intelligence, the instructors there, they wound up under
investigation and it was found that there was some harassment, there were some inappropriate
relationships, there was a chat room, just overwhelming misogynistic attitude.
The general tone was like, well that's just how it is, you got to get used to it.
Yeah that's bad, that's really bad, because the students probably picked up on this.
Now the students who are women, they're probably going to talk about it and may discourage
women from going into this field.
That's an obvious outcome from this.
Another one is that the men students will probably pick up on this misogynistic attitude
and it will carry over.
Meaning they'll probably undervalue their counterparts who are women.
And then if they deploy, this is when it becomes dangerous.
This is when it can actually get people really hurt.
Because their counterparts who are women, well they'll be underutilized because they're
undervalued, because they're underestimated, because of the misogynistic attitude.
But the reality is, because forces all over the world underestimate women, when it comes
to intelligence, women can actually do a lot of stuff men can't.
They can get access to places a whole lot easier.
Sources often feel more comfortable with them.
They can get access to sources who may have their guard up around men because of that
misogynistic attitude.
This is a really bad thing.
It's got to get addressed.
And this is before we even get to the flip side where it gets really dangerous.
Why are women effective in this role?
Because they're underestimated.
What did the instructors teach the male students to underestimate them?
So the opposition forces who use women, the Marines who graduated these students, they're
going to underestimate them.
They will be the people who become easier marks for opposition services.
This wasn't really addressed.
There was super minor corrective action, but not to the degree that it should have occurred.
And it can get people hurt because, well, they're not woke enough.
That's really what it boils down to here.
Now when this came out, the response was varied.
There were some people who were like, yeah, you've got to fix this and here are all the
reasons why.
There are some people who were just like, it's the Marines.
What did you expect?
And then there were those who said stuff like, we murder innocent people all over the world,
but this kind of language is unacceptable.
Obvious sarcasm here.
Everyone involved should be grateful they got a glance at the real face of militarism.
It is dehumanizing.
Yeah, I mean, okay.
But there's a couple parts in this.
I'm going to suggest that if your concern is innocent people, civilian loss overseas
at the hands of the US, you should probably want the military to be less dehumanizing
because if you dehumanize your team, it's probably even way easier to dehumanize the
other people, right?
Those people over there.
It seems like if that's your concern, you would want this addressed.
The other part of it is, you know, innocent people all over the world.
Okay.
How does that happen normally?
In real life, it's not like the movies.
In real life, it isn't, you know, the sergeant gets hit and then the lower enlisted just
lose it on an entire village.
That happens, but it's super rare.
That's less than 1%.
That's a tiny, tiny fraction.
How does it normally happen?
How does civilian loss normally occur?
From the air, right?
From above?
From, you know, a wedding that gets mistaken as something else or somebody delivering water,
an aid worker, stuff like that.
The list of stuff like that goes on and on and on.
Do you know what would limit that?
Proper intelligence.
Intelligence is one of those things that makes a military more capable of prosecuting a war.
And it also makes them more professional because they have more information.
This issue is bad all around because if they have less access to locations, less access
to sources, they have worse information, which means more of that civilian loss is going
to occur.
This is a dangerous thing to be occurring in an intelligence school.
It's bad everywhere, but it is super dangerous in this particular field.
It's something that the Marines definitely need to step beyond a single letter of counseling.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}