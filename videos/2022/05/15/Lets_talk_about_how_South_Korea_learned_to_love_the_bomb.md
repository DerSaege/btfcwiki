---
title: Let's talk about how South Korea learned to love the bomb....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H_w71xvwH1A) |
| Published | 2022/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- South Korea's new conservative president may escalate tensions with North Korea in the competition between the US and China.
- Around 70% of South Koreans support developing nuclear weapons for deterrence.
- The South Korean president wanted the US to put nukes back in South Korea, which was not openly agreed upon.
- The purpose of a nuclear deterrent is for it to be known, yet the US could secretly place nukes in South Korea.
- Economic incentives have been offered to North Korea to abandon its nuclear ambitions.
- After Russia's invasion of Ukraine, nuclear powers are seen as less likely targets for invasion.
- Negotiation and accommodation without nuclear weapons on the peninsula are possible, despite North Korea being portrayed negatively.
- Trump attempted negotiations with North Korea but was unsuccessful.
- Confrontation and saber-rattling are not seen as effective ways to keep nuclear weapons off the Korean peninsula.
- The details of economic packages and incentives from South Korea might influence North Korea's leadership.
- There is potential for dialogues and negotiations with North Korea, as most countries aren't "cartoon villains."
- These developments will have long-term implications for the region, involving various approaches to North Korea by neighbors and global powers.

### Quotes

- "Around 70 percent of South Koreans support the South developing nuclear weapons."
- "Negotiation and accommodation without nuclear weapons on the peninsula are possible."
- "Most countries aren't actually cartoon villains."

### Oneliner

South Korea's new president's posture may escalate tensions with North Korea, but negotiation without nuclear weapons is possible amid regional dynamics and economic incentives.

### Audience

Global policymakers

### On-the-ground actions from transcript

- Monitor developments in the Korean peninsula and advocate for peaceful negotiations (exemplified)
- Support initiatives that focus on economic incentives for denuclearization in North Korea (exemplified)

### Whats missing in summary

Insights on the historical context shaping North Korea's nuclear ambitions.

### Tags

#SouthKorea #NorthKorea #NuclearWeapons #Diplomacy #GlobalPolitics


## Transcript
Well howdy there internet people. It's Beau again. So today we are going to talk about how
South Korea learned to stop worrying and love the bomb. South Korea has a new president and
the position of that new president is going to lead to South Korea and North Korea becoming
a flashpoint in the near-peer contest between the United States and China.
The new president is a conservative. Not incredibly popular. Their election was very
similar to a lot of U.S. elections. People really weren't voting for any particular candidate. They
were voting against the ones they didn't like. I want to say he won with less than a one percent
lead. But part of his posture is to confront North Korea. Now something that may come as a surprise
to those in the United States, polling suggests that around 70 percent of South Koreans support
the South developing nuclear weapons, obtaining a nuclear deterrent of some kind, whether it be
through a sharing agreement with another power like the United States, or maybe developing them
on their own. Now the new president reached out to the U.S. and kind of asked, unofficially
mentioned it, back in September and wanted the U.S. to put nukes back into South Korea.
The U.S. at the time said no, at least openly and publicly. Keep in mind the United States does have
a capability of putting them there quietly, which is not something I have ever understood.
The purpose of a deterrent is for people to know about it.
But the South Korean president has offered a carrot to the North, promising massive economic help
if the North abandons its nuclear desires. Now how successful that's going to be, I don't know.
One thing that has come of Russia going into Ukraine, of that invasion, is a message being
sent very loudly around the world that nuclear powers, well, they're probably not going to get
invaded. People don't want to mess with them. So with that in view, the North may be very reluctant
to abandon their nuclear aspirations. At the same time, a strong economic package coming from the
South might be the incentive. We don't know. North Korea has been portrayed as a cartoon villain
for a very, very long time. I am of the opinion that there is room for negotiation, that there is
room to reach an accommodation that doesn't involve nukes being on the peninsula. How to get there?
Well, that gets really complicated. And the only person that was willing to give it a shot was
Trump, who was wholly incapable of doing it. But I'm convinced the theory is sound. Just because
he failed doesn't mean other people would. That's one of the few moves he made on the
foreign policy scene that I was like, you know what? That's not a bad idea. I don't think that
the confrontation and the bluster and the saber rattling is the route to go. I don't think that's
the way that is going to move us forward and keep nukes out of the peninsula. At this point,
we're going to have to see the details of the package and what the South is going to offer.
Sanction relief, maybe re-establishing a lot more trade, something to boost the lives of the average
person, might make the leadership in North Korea more receptive because maybe they can come out of
isolation. But that's, again, assuming they want to. And there's a lot of mystery surrounding
North Korea's true intentions. I'm of the opinion that most countries aren't actually cartoon
villains. So there's room to talk. But we're going to have to wait and see. Either way,
these developments are going to matter for years. We're going to see a North Korea that is being
approached in various ways by its neighbors and by world powers. We're going to see a South Korea
that is going to try to exert itself more internationally and try to directly confront
North Korea. That's the posture that the new president has kind of set. So we're going to
have to keep an eye on this area. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}