---
title: Let's talk about Russian analysis of Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WiRIJegvpKY) |
| Published | 2022/05/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he previously didn't understand a common question about Russian analysts and the war in Ukraine.
- Points out that good analysts, regardless of nationality, will provide similar analyses of the situation in Ukraine.
- Describes the disconnect between Western media coverage and in-depth analysis of the war in Ukraine.
- Illustrates the focus of media on dramatic but less strategically significant aspects of the conflict.
- Emphasizes the significant disparity between analysts' perspectives and what is portrayed in the news, particularly in countries where the government controls the narrative.
- Introduces Igor Gherkin as a Russian analyst with a background in the military and intelligence.
- Shares Igor Gherkin's bleak analysis of the situation in Ukraine, indicating competent defense by the enemy and challenges for the Russian side.
- Mentions that Igor Gherkin's assessment mirrors Western analyses, with slight variations.
- Notes that Ukraine doesn't necessarily have to win battles but make the cost too high for Russia to continue.
- Concludes by underlining the consistency in analyses regardless of analysts' nationalities and warns against conflating media narratives with actual analysis.

### Quotes

- "Nationalism is politics for basic people."
- "The meaty is the spin of that."
- "Ukraine doesn't have to win the battles. They just have to make it too costly to continue."

### Oneliner

Beau explains the disconnect between media portrayal and analyst perspectives on the war in Ukraine, showcasing Igor Gherkin's bleak assessment that echoes Western analyses.

### Audience

Analytical viewers

### On-the-ground actions from transcript

- Analyze international news sources to understand varying perspectives on global conflicts (suggested).
- Support independent journalism to access diverse viewpoints on geopolitical events (implied).

### Whats missing in summary

Insight into the impact of media narratives on public perception and government policies.

### Tags

#Ukraine #RussianAnalysts #MediaAnalysis #Geopolitics #NarrativeDisparity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a question
I've gotten repeatedly over the last month
and that I didn't understand.
I didn't actually understand what was being asked.
Now that I do, we're going to talk about
why I didn't understand the question
and then we're gonna answer it.
The general tone of all of the messages was,
hey, what are the Russian analysts saying
about the war in Ukraine?
How's it going from their perspective?
I didn't understand this question because nationalism is politics for basic people.
Good analysts who are being honest, who haven't fallen into any of the pitfalls that create
analysis failure, all of their analysis are going to be pretty much the same.
There's going to be small deviations, but they're all going to be the same.
The reason this question is getting asked is because of the difference between Western
media and Russian media and how they're portraying it.
So I think a good way to show the disconnect between analysis and what ends up on the news
is to use Western media.
Because I think most people are aware that Russia is creating a narrative.
So with Western media, when's the last time you heard deep analysis of the steel plant?
That's all over network news, right?
But the analysis that is being provided by those who are talking about how the war is
progressing and particularly those who are trying to create estimates, kind of seeing
into the future, they're not talking about that anymore.
Because from a long-term strategic point of view, it's not really that important.
It's important to the people who are there, don't get me wrong, but for the overall course
of the war, there's not going to be a lot of developments there.
But it's dramatic, and it gets ratings, and people are familiar with this storyline, so
the media keeps running with it.
There is a huge disconnect between analysts and what ends up on the news, even in the
best of scenarios.
When you have a country that is very much controlling the narrative, it's even worse.
That disconnect is even larger.
So that's why I didn't understand it, because if they're good, it's all going to be pretty
much the same.
To answer the question, I'm going to give you what a Russian analyst is saying.
And since we're just going to go through one, because I don't have a desire to go through
a bunch, I'm going to choose somebody that is just unimpeachable.
Somebody that there is no way anybody can be like, oh, that's just Western propaganda.
So today we're going to talk about the analysis of Igor Gherkin.
Now if you don't know who this is, well actually let me start with, if you know who this is,
there's a reason I'm using this person.
They are unimpeachably on the Russian side and we'll get to their claim to fame here
in a minute.
So Igor was Russian Army, then left, went to the FSB, went intelligence.
He was the regional commander of all of the separatist forces in the area we're talking
about.
Probably knows a little bit about it.
He commanded all of the non-state actors in the region we're talking about back in 2013-2014.
He then went on to become the defense minister of one of the little Russian puppet governments.
And I'm sure somebody's going to get mad that I said it that way.
And they had a Russian FSB officer as their defense minister.
Their puppet governments.
It doesn't matter if you don't like the term, it's the reality.
So what does Igor say?
Unfortunately the general conclusion is bleak.
In the best case scenario, the enemy will be slowly pushed out of the Donbass with large
losses for both sides of course across many weeks and possibly months. Overall
the enemy is defending competently, fiercely. It controls the situation and
its troops. This is from somebody who is a Russian nationalist, his words. He is
also wanted by the Dutch for war crimes in service of Russia. This isn't somebody
that has sympathies towards the West. The analysis is the analysis. Ideas stand
and fall on their own. That's what's happening. It is not going well for the
Russian side. And if you were to compare that analysis with Western analysis,
It's pretty much what they're saying.
You know, there's little deviations and some people go more into it.
You know, there's a lot of people talking about encirclements right now.
I've made it clear I don't really think that's going to be a big part of this.
But the overall tone is that it's not going well for Russia.
Their offensive is stalled or slowed, depending on how optimistic you want to be for them.
and that overall it's bleak. Their chances are bleak. One thing I want to
point out is that Ukraine doesn't have to win the battles. They just have to
make it too costly to continue. And you have analysts inside Russia now pointing
at the high cost. That's probably the most noteworthy part in that analysis, so the reason
this hasn't really been answered before is because they're all the same. If you had a
good analyst from Japan looking at it, their analysis would be the same. If they're being
honest, they have the information, and they're decent at what they do, it's all
going to look very, very similar unless they fall into one of the things that
creates an intelligence failure. So when you're comparing the media, just
understand the media is what's driving the narrative for the government. The
analysis? It's the analysis. The meaty is the spin of that. Anyway, it's just a
With that, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}