---
title: Let's talk about a Russian take on mobilization....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cykjxyuMSXc) |
| Published | 2022/05/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the Russian perspective on the situation in Ukraine, specifically discussing mobilization and its potential impact.
- Mentioning the Russian company Wagner, known as a private military company and special operations group.
- Sharing information from Wagner's telegram channel indicating the need for 600 to 800,000 troops to defeat Ukraine.
- Noting the challenges Russia may face in mobilizing such a large number of troops, including minimal resistance, training requirements, and equipment shortages.
- Speculating on the possibility of Russia launching a full mobilization on May 9th.
- Emphasizing that even with a massive influx of conscripts, Russia may still struggle due to lack of training, equipment, and resources.
- Pointing out Russia's difficulties in supplying and equipping troops currently present in Ukraine.
- Stating that Ukraine's strategy is to make the conflict too costly for Russia to continue rather than winning battles outright.
- Acknowledging the growing realization within Russia about the challenges and costs of the conflict.
- Suggesting that despite potential mobilization attempts, Russia may not have the necessary resources for prolonged engagement.

### Quotes

- "There will be mobilization or we will lose the war."
- "They don't have the resources they need to do this."
- "All they have to do is keep fighting."

### Oneliner

Beau delves into the Russian perspective on mobilization in Ukraine, revealing challenges and potential futility in the face of resource shortages.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Monitor developments in Ukraine and Russia (suggested)
- Support diplomatic efforts for a peaceful resolution (suggested)

### Whats missing in summary

Insights on the potential geopolitical implications of Russia's actions in Ukraine.

### Tags

#Russia #Ukraine #Mobilization #PrivateMilitaryCompany #Geopolitics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk a little bit more
about the Russian perspective on how things are progressing
in Ukraine, the likelihood of mobilization,
and whether or not it's going to matter.
Because shortly after I released the video last night
on the Russian analysis, a little bit more information
came out from a pretty unique source.
I found it entertaining to say the least. So there is a Russian company called the
Wagner and this is really an umbrella company. It's not actually, it's not a
formal company as many might picture it. It's a lot of cover companies and stuff
like that all blended together. The overall name is Wagner. They have a
telegram channel. This is what went out on it. There will be mobilization or we
will lose the war. To defeat Ukraine 600 to 800,000 people are needed. Whoever
says that a lot of people are not needed in a modern war is simply on a fool's
errand. Whatever high precision weapons there are, the infantry still has to take
can hold the ground. So right now, Wagner, which is their private military company,
and that also kind of doubles as their special operations groups, they have
caught up to what was discussed on this channel on March 9th. March 9th, I said
they needed 800,000 troops, because it's the math. That's how many you need for a
country with the population of Ukraine. So they have finally caught up to the correct math.
However, there are three additional components when using that number. The first
is that you have to be facing minimal resistance. Nothing about the Ukrainian resistance could be
be described as minimal. The troops that are sent in, they also have to be trained.
Conscripts don't count and they have to be equipped. So because of this and some
other things that have been said, there are a number of people who are
suggesting that on the 9th of May, Russia will launch a full mobilization and try
to actually get up to these numbers. I don't know if I believe that, but in the
end it doesn't matter. If they were to pour in that many conscripts, yeah, it
would make the fighting worse, but it still doesn't give them a
victory because they're not trained. They're not trained to the level they
need to be at. Aside from that, they won't be equipped. Russia is having trouble
supplying the troops that are there right now. You have reservists who are
showing up now carrying weapons from World War II. They're not going to be
able to kit them out the way they need to be to run an occupation. Aside from
that, they have to be able to maintain this for an extended period. They don't
have the money. They don't have the money, they don't have the equipment, they
don't have the training, even if they were to mobilize and bring in a bunch of
conscripts. I would point out that there are some people who are talking about
their demographics saying that even that part is super unlikely. At this point,
you're finally getting inside Russia, you are getting those who are pointing out
that it might be too costly to continue. It's not about winning the battles from
Ukraine's point of view. They don't have to win the battles. All they have to do
is keep fighting. All they have to do is make it too costly to continue. Make them
pay for every square inch and that's what they've been doing. That cold, grim
reality is finally starting to set in inside Russia. That doesn't mean that
they won't mobilize. It doesn't mean they won't try it. It just means that it's
really unlikely to succeed. They don't have the resources they need to do this.
They bit off more than they can chew. The longer it goes on, the longer they make
the attempt, the more waste occurs. So that's yet another peak inside the Russian perspective.
Once again, it's Wagner. These are not Western allies. These aren't people who are sympathetic
to the West. They're not painting a cloudy picture just to, you know, undermine the effort.
These are people who want to win. And that's what they're saying they need. And even if
they were to get it, it probably won't matter. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}