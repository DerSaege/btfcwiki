---
title: Let's talk about the statements by Justice Clarence Thomas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YxUCoygG_ao) |
| Published | 2022/05/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions Justice Clarence Thomas' statements and ideas presented in the Washington Post.
- Expresses confusion over the erosion of faith in American institutions.
- Questions which institutions are being referred to, mentioning the presidency, Congress, and the Supreme Court.
- Criticizes the Supreme Court for potentially stripping rights from half the country for partisan and religious reasons.
- Raises concerns about justices on the Supreme Court potentially lying to secure their positions.
- Argues that Americans may be losing faith in institutions due to catering to a smaller group of people.
- Criticizes the idea of protests being seen as bullying institutions rather than exercising First Amendment rights.
- Expresses skepticism about maintaining faith in institutions given recent actions undermining trust.
- States that the US may be heading towards a long road of regaining lost rights.
- Questions the logic behind reversing freedoms and limiting rights that have been in place for years.

### Quotes

- "I don't understand why something like that would be written."
- "It's not that I think he's wrong. It's that I don't understand why he's surprised."

### Oneliner

Beau questions Justice Clarence Thomas' statements, expressing concerns about the erosion of faith in American institutions and the potential loss of rights, urging for a reevaluation of recent actions.

### Audience

American citizens

### On-the-ground actions from transcript

- Challenge actions undermining trust in institutions (suggested)
- Advocate for maintaining and expanding rights for all (implied)

### Whats missing in summary

Insight into Beau's perspective and analysis 

### Tags

#Justice #AmericanInstitutions #Faith #Rights #Government


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Supreme Court Justice Clarence Thomas
and those statements and the ideas that were presented in the Washington Post.
There are a couple of concepts, a couple of ideas that came out that I have questions about.
I don't really understand what he means.
A couple of them, one in particular, is the idea that Americans are losing faith,
that faith is being eroded in American institutions, institutions of government.
And another being that those institutions can't be bullied.
And I get it, I mean, I understand what he's saying, I understand the words,
but I don't understand the sentiment.
What institutions in particular? Like the presidency?
You know, where the last guy ran around making a bunch of baseless claims about the election,
whipping people up into a frenzy, and then when the results didn't go the way he wanted,
tried to cling desperately to power anyway?
Yeah, I mean, I get it. I guess that people might lose faith in that,
especially when in Congress you have people who did the same thing who are still seated.
People who are still championing the former president for doing those things.
They might lose faith there too.
And then you have the Supreme Court, who is on the verge of stripping the rights
of people from half the country for partisan and religious reasons,
things that shouldn't really matter to the Supreme Court,
and that it certainly appears that some of the justices on the Supreme Court lied to get the job.
But I guess the people should just have faith.
I don't know, it seems unlikely.
And then I have a problem with the whole idea of bullying institutions, those protests.
Yeah, that's the First Amendment.
That's in the document that your entire job is to interpret.
That's not bullying, that's speech.
I would imagine that Americans are losing faith because the institutions are catering
to a smaller and smaller and smaller group of people.
And in order for the establishment to retain control, they continue to other groups
and push them out, reversing what is supposed to be the trend.
The idea is to fulfill all those promises that were made in the 1700s,
those that the country didn't really live up to at the time.
But the goal should be to move in that direction.
And now we have an entire political party bent on doing things that undermine faith
in those institutions and reverse the course of trying to fulfill those promises.
And when the people, we the people, speak up, oh well it's bullying by the commoners.
I have a lot of questions about that.
I don't understand why something like that would be written.
I don't understand how anybody could believe that would be received well.
We are in for a long road having to claw back and bring people back out of that other category.
And it certainly appears that people are going to lose rights that they've had for a long time.
And we're going to have to fight to get them back.
If an institution, a government, allowed something and said,
yeah this is how it is for 20 years or 50 years, and said, yes you have these freedoms.
We know we didn't get things right in the beginning, but we're trying to do it now.
So this is how it is now.
And then they went back on that?
Why would you have faith?
We're not talking about reversing something that was denying people their freedom.
We're not talking about expanding the freedoms that exist.
We're talking about reducing them, limiting them,
pushing more people out from underneath the protections after they achieve them.
Yeah, people are going to lose faith because it's the exact opposite of what people expect.
It's not that I think he's wrong.
It's that I don't understand why he's surprised.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}