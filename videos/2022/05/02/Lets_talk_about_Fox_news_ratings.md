---
title: Let's talk about Fox news ratings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nLrS9yiXoww) |
| Published | 2022/05/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses concerns about network news and ratings, particularly focusing on Fox News and its high ratings.
- He explains that Fox News has the highest ratings because it caters to the demographic seeking right-wing content.
- Beau clarifies that centrist outlets like CNN and MSNBC are not leftist but lean liberal.
- He points out that Fox's ratings are inflated due to its specific audience demographic, primarily older viewers who prefer TV news.
- Beau mentions that the high ratings of Fox News are not necessarily indicative of widespread trust or viewership.
- He criticizes Fox News for using a propaganda strategy to claim they are the most trusted due to being the most viewed.
- Beau distinguishes between centrist, liberal-leaning outlets and leftist news networks like PLN on YouTube.
- He encourages viewers to watch PLN to understand the difference between leftist and centrist news perspectives.
- Beau concludes by stating that Fox's higher ratings are normal and not a cause for alarm, as it correlates with the age demographic that consumes TV news.
- Overall, Beau aims to provide perspective on network news ratings and to debunk misconceptions about political leanings in news media.

### Quotes

- "They're the most viewed because it's a small demographic of people of a certain age who want their views reflected back to them."
- "You will never hear on CNN that the solution to your problem is dismantling capitalism."
- "This isn't a sign that more people are suddenly trusting Tucker Carlson."

### Oneliner

Beau explains why Fox News has high ratings, debunking misconceptions about political leanings in news media and reassuring viewers that it's a normal phenomenon.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Watch leftist news networks like PLN on YouTube to understand different news perspectives (suggested)
- Analyze news sources critically and seek diverse viewpoints to avoid misinformation (implied)

### Whats missing in summary

Beau's engaging delivery and insightful analysis are best experienced through watching the full video for a comprehensive understanding.

### Tags

#NetworkNews #FoxNews #Ratings #PoliticalPerspectives #MediaLiteracy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about network news and ratings and whether or not there's
anything to worry about.
Got a message from somebody saying, hey, you know, having dinner with my dad who's ridiculously
conservative and he went off on a rant talking about how Fox News is the truth and the way
you know they're the truth is that they have the highest ratings.
And I looked it up and they do have the highest ratings and, you know, I'm kind of freaking
out about this.
The word's freaking out, we're in it.
Okay, so is it true that Fox has the highest ratings?
Yes.
Yeah.
Absolutely.
Why?
Because more people like right-wing, fashy content?
Look at the market for news outlets.
You have a bunch of centrist outlets.
CNN, MSNBC, the nightly news shows.
I know somebody's going to say that these are leftists.
They're not.
They're centrist, leaning liberal.
How about that?
You have a whole bunch of them.
So all of those outlets divide up the viewers that would watch centrist, liberal-leaning
content.
If you want right-wing, fashy news delivered over a TV, where do you go?
Fox.
It's all you've got.
That's why the ratings are higher.
Typically if you add up the centrist ratings, the ratings from those outlets, those numbers,
they're normally about the same as Fox.
Fox still has a little bit of an edge though.
So is that something to worry about?
No, not really.
Because who watches Fox?
Who watches network news over a TV?
Older people.
Older people.
If you are 18 to 29, 71% of you get your news from a phone, tablet, or computer.
30 to 49, 67% of you get your news from a phone, tablet, or computer.
The age of 50, something happens.
That's no longer the most popular option.
Switches to TV.
54% get their news from TV.
65 and up, 68% get their news from TV.
What do we know about party affiliation and age?
Republicans really don't actually get a majority of people until about the age of 50.
It doesn't really mean that much.
Aside from that, I would point out that from the recent ratings, the most recent that I
could find, Fox during primetime had 2.36 million viewers.
Sounds like a lot when you say it like that.
Another way to say it would be we're talking about less than 1% of the population of the
US.
This isn't indicative of anything.
Fox likes to put these numbers out there because it's a propaganda strategy.
It's the bandwagon propaganda strategy.
And it's designed to get people exposed to it to say exactly what your dad said.
You're the most trusted because they're the most viewed.
Not really.
They're the most viewed because it's a small demographic of people of a certain age who
want their views reflected back to them.
I said centrist, and I know people are just going to go to town with that.
CNN, MSNBC, they are centrist, liberal leaning at most outlets.
They are not leftist.
You will never hear on CNN that the solution to your problem is dismantling capitalism.
That's not a thing.
If you actually want to see what a leftist news network looks like, here on YouTube,
there's something called PLN.
And I think they actually have a video dropping today or tomorrow.
A new one.
They put out one episode a month.
Go watch one of those and tell me if that is similar to what you would find on CNN or
MSNBC.
They're not leftists.
That's part of that misconception that a lot of people in the United States have when it
comes to the ideas of right, center, and left.
But this is certainly not something to freak out about.
This is pretty normal.
It's pretty standard for Fox to have higher ratings, have just a little bit of an edge
on the centrist outlets when you add them up, because generally speaking, when you're
talking about the age demographic that gets their news via TV, Republicans have an edge.
It all tracks.
This isn't a new development.
This isn't a sign that more people are suddenly trusting Tucker Carlson.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}