---
title: Let's talk about 10 propaganda techniques you'll see in the midterms....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Uq5FwK43Gc4) |
| Published | 2022/05/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains ten propaganda techniques that surface in elections, including ad hominem, ad nauseam, appeal to authority, appeal to fear, appeal to prejudice, bandwagon, inevitable victory, beautiful people, card stacking, and glittering generalities.
- Propaganda doesn't necessarily mean it's false; it can be true information presented in a manipulative way.
- Emphasizes the importance of recognizing these techniques to become less susceptible to manipulation during elections.
- Describes how appeals to authority involve endorsements and support from various groups.
- Points out the use of fear in politics to sway opinions by presenting one position as the lesser evil.
- Talks about appealing to prejudices by associating emotional value with certain concepts, like the term "woke" or "CRT."
- Mentions the bandwagon technique where individuals are pressured to join a group to be part of the "in-crowd."
- Explains how politicians often use the concept of inevitable victory to attract supporters and create an aura of success.
- Comments on the strategy of showcasing "beautiful people" to support a political cause and how it plays into societal biases.
- Describes card stacking as providing selective information to support a particular narrative while omitting critical details.
- Talks about glittering generalities, which are vague terms used to evoke positive emotions without substance.

### Quotes

- "Propaganda doesn't care whether or not it's true or not."
- "Appeal to fear generates anxiety and fear of the alternative."
- "Bandwagon: You want to be one of us, right?"
- "Inevitable victory: Making people want to be on the winning side."
- "Glittering generalities: A general term that is just supposed to make people feel better."

### Oneliner

Beau explains ten propaganda techniques used in elections to manipulate opinions, urging viewers to recognize and resist these tactics to make informed choices.

### Audience

Voters

### On-the-ground actions from transcript

- Analyze political messages critically (implied)
- Educate others about propaganda techniques (implied)
- Fact-check political claims and statements (implied)

### Whats missing in summary

In-depth examples and analysis of each propaganda technique used in elections.

### Tags

#Propaganda #Elections #PoliticalManipulation #Voting #CriticalThinking


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about ten propaganda techniques
that you are certain to see in the upcoming elections.
One thing to remember about propaganda
is that it doesn't necessarily mean it's not true.
In the U.S., when you hear the term propaganda,
generally you feel it's a lie.
It doesn't have to be a lie.
It could be something that's completely true.
Propaganda doesn't care whether or not it's true or not.
So we're going to go over ten techniques
that surface in pretty much every election.
And this way when you see it,
hopefully you'll be less susceptible to it.
Okay, first is ad hom. We all know what that is.
Mud-slinging happens in every election.
The next is ad nauseum.
Repetition. Saying something over and over
and over again.
That's ad nauseum.
Saying it again and again.
Ad nauseum.
Appeal to authority.
We normally think of this as a logical fallacy.
And it is.
But it's also used a whole lot as a propaganda technique
during elections in particular.
Those are endorsements.
This person says that this is the best candidate.
This group of business people support this person.
This environmental group supports this person.
These are appeals to authority.
Appeal to fear.
This is something that generates anxiety
and fear of the alternative.
Support my position or this horrible thing will happen.
You see this all the time,
anytime somebody talks about clean energy.
Those who support dirty energy show up and tell you,
well, you know, it really doesn't work that well.
Do you know how hard it is to get these components?
Do you know?
And it doesn't attempt to present
dirty energy as a good thing.
It's just better than the alternative.
This was a lot of people's motivation for voting for Biden,
honestly.
Appeal to prejudice.
Now, this doesn't actually have to be about race,
which again, how we picture it in the US.
This is adding emotional value and language to something
or associating something with something
that you know your audience doesn't like.
Woke.
CRT.
That's the perfect example.
They took something that most people had no clue what it was,
villainized it, and because nobody really knew what it was,
they could apply it to anything.
That's appeal to prejudice.
Bandwagon.
Everybody's doing it.
You want to be one of us, right?
You can't be part of this in-group
unless you vote for our person.
Bandwagon.
Inevitable victory.
Every politician I have ever seen does this.
When we take the White House,
introducing your next governor, inevitable victory.
Making people want to be on the winning side
and proclaiming yourself as the winner.
Beautiful people.
This is always a funny one to me.
In American politics, it's very revealing.
Politicians will often have their case made by people
that they know their constituents
will find beautiful.
It always cracks me up that if the best example of this,
what do all, not all, almost all women on Fox News look like?
All blonde hair, right?
That thing.
And it doesn't look like that on the Democratic side
when they employ the same technique, and they do.
But that's just, that's actually always cracked me up
because it acknowledges that at least those people
who make decisions like this for the Republican Party
are very aware of the racism that exists there
because they play into it pretty heavily.
Card stacking.
Now, this is where you provide a bunch of information,
but you're omitting a lot.
Most politicians do this.
Abbott, governor down there in Texas,
is certainly going to try to do this
with his little border stunt.
He will put out all kinds of information
about how successful it is, probably using
those same numbers that have already been debunked.
But he'll put them out anyway and talk
about how successful it was, never mentioning the cost,
never mentioning the conditions, never mentioning any of that.
That's card stacking.
Most politicians do this.
Glittering generalities.
Every politician does this as well.
This is a general term that is just supposed
to make people feel better.
It's a slogan.
Make America great again.
Doesn't mean anything, but it makes people feel
all warm and fuzzy, right?
If that one doesn't do it for you, hope and change.
It's the same thing.
These are glittering generalities.
All of these, these 10 things, you're
going to see them throughout the election cycle.
Again, doesn't mean that they're not true when they're used.
Just understand that when somebody
is using a propaganda technique like this,
it doesn't matter if it's true.
That doesn't factor into it.
What matters is that it resonates with the voter.
You.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}