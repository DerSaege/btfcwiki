---
title: Let's talk about SETI vs METI and who speaks for us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZmyBmcb0JN4) |
| Published | 2022/05/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Distinguishes between SETI (passive) and METI (active) in the search for extraterrestrial intelligence.
- Explains that METI involves sending messages to extraterrestrial intelligence, while SETI involves listening for signs.
- Mentions two upcoming METI projects: one from China in 2023 and another on October 4 sending a message 39 light years away.
- Raises ethical questions about contacting extraterrestrial intelligence, including the risk of a clash of civilizations.
- Questions who speaks for humanity and makes decisions about sending messages to potential extraterrestrial life.
- Points out the lack of regulations in determining who can send messages to space, as it currently rests in the hands of unaccountable scientists.
- Draws parallels between unaccountable scientists in the space messaging field and those making decisions impacting the world today.
- Encourages reflection on who truly represents and speaks for individuals and the global population.
- Calls for a pause to ponder the accountability of decision-makers, both in the space messaging realm and on Earth.
- Ends with a thought-provoking message about accountability and representation in decision-making processes.

### Quotes

- "Who speaks for everybody? Who speaks for humanity?"
- "It's really just in the hands of scientists. They're not accountable to anybody."
- "Makes you think about all of the people who are not accountable to anybody, who are making decisions for the entire world here."
- "Who's speaking for you? Who's speaking for us today? Here."
- "Have a good day."

### Oneliner

Beau questions accountability and representation in decision-making processes, drawing parallels between unaccountable scientists in space messaging and decision-makers impacting the world today.

### Audience

Decision-makers, activists, thinkers

### On-the-ground actions from transcript

- Question accountability of decision-makers and demand transparency (implied)
- Advocate for regulations and oversight in decision-making processes (implied)

### Whats missing in summary

The full transcript provides a comprehensive exploration of the ethical implications of messaging extraterrestrial intelligence and prompts reflection on accountability and representation in decision-making processes.

### Tags

#SETI #METI #extraterrestrial #intelligence #accountability


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about SETI and METI.
Most people are familiar with SETI,
the search for extraterrestrial intelligence, right?
It's passive.
People sit around and listen for signs that somewhere out there
is more, some intelligent life form.
METI isn't passive.
It's active.
It's messaging extraterrestrial intelligence.
It's actually trying to send them a message.
Now SETI is something that runs like all the time.
METI is a little bit different.
That doesn't happen so often.
Any time it does, there's all kinds of ethical questions
that come up, and scientific ones.
We have two METI projects on the horizon.
One is slated for 2023 out of China,
and they're going to be beaming a message
to a cluster of stars, a cluster.
It's like millions of stars, in hopes
of like widening the pool of potential little green men
to contact.
This project is sending a message
that could take 10,000 to 20,000 light years to get there.
So I mean, with the way the planet's going,
probably not much to worry about when
it comes to a return visit.
Another one that's going out on October 4
is sending a message to an area 39 light years away,
which means theoretically we could receive
a reply in less than 80 years.
It's wild when you think about it.
But that brings up all of the questions that really
don't get answered in this discussion
every time we do this.
There's the idea of the clash of civilizations.
And we expect, for whatever reason, that those out there,
that they would behave like we do.
So there are some who believe they'll be warlike,
and that they might use that advanced technology
and come here and just ruin our whole day.
And I guess that's a risk.
And then there are some who believe
that if they're advanced enough to have
that kind of capability, then they already
know about us anyway.
And they just don't care, which kind of makes sense to me.
I mean, if you had the ability to observe another planet
and you saw a bunch of tool using primates destroying
the planet and carving it up while taking each other out,
would you visit?
Probably not.
So you have all of this debate and discussion
about what's going to happen if they actually make contact.
And nobody really has an answer, which
I feel as though we might want to kind of look
into this a little bit more before we make a decision.
But there's a far more interesting ethical question.
Who speaks for everybody?
Who speaks for humanity?
Who gets to make this decision?
Who determines what message we send and why and where?
Who speaks for us?
As it is, there's no regulations on this.
If you had the money, you could do it yourself.
There's nothing saying that you can't.
It's really just in the hands of scientists.
They're not accountable to anybody.
And when you think about it, this group
of unaccountable people making decisions that
could impact the entire world, wow,
holds up a mirror, doesn't it?
Makes you think about all of the people who are not
accountable to anybody, who are making decisions
for the entire world here.
Not in some sci-fi adventure, not
in beaming a message off planet, but here right now.
All of these people who really don't have any accountability,
but for some reason they speak for us.
They make decisions for the entire world.
And by all accounts, if we look to the scientists,
they're not being very good stewards.
So when this topic comes up, as it does every once in a while,
it's always worth taking the moment to pause and examine
that while people are discussing these unaccountable scientists
speaking for us, who's speaking for you?
Who's speaking for you?
Who's speaking for us today?
Here.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}