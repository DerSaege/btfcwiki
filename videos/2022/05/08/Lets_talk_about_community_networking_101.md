---
title: Let's talk about community networking 101....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TZJuWScu3qQ) |
| Published | 2022/05/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Social media inquiries led to the question of starting a community network after previous videos.
- The network Beau is part of incorporated the channel to supply shelters during the public health crisis.
- Leveraging the network helped in providing supplies for hospitals and clinics when the pandemic started.
- Under normal circumstances, acquiring supplies like gowns and masks for nurses requires significant effort.
- A community network is described as a versatile and effective way to bring people together to help the community.
- It's suggested to keep the idea of the network open-ended to encourage participation.
- The ideal size for a network is around 8 to 12 people to avoid personality conflicts.
- Allowing natural splits in the group can lead to more coverage and collaboration in the community.
- Bringing different social circles together is key to creating a network that can achieve positive outcomes.
- Identifying like-minded individuals willing to commit time is the first step in starting a community network.

### Quotes

- "A community network is a versatile and effective way to bring people together to help the community."
- "Bringing different social circles together is key to creating a network that can achieve positive outcomes."
- "If you're going to try to start doing this, your first step is to sit down and think about all of the people you know who might be like-minded."

### Oneliner

Beau explains the importance of community networks, focusing on inclusivity, versatility, and collaboration to achieve positive outcomes.

### Audience

Community organizers

### On-the-ground actions from transcript

- Identify like-minded individuals willing to commit time to start a community network (implied)
- Encourage open-ended participation in the network to foster collaboration and inclusivity (implied)

### Whats missing in summary

More details on developing larger networks, securing funding, and the role of electoral politics in community networks.

### Tags

#CommunityNetworks #Collaboration #Inclusivity #LocalAction #CommunityBuilding


## Transcript
Well, howdy there, internet people, it's Bo again.
So after those videos yesterday,
my social media everywhere filled up with the question of,
well, how do you start a community network?
What is it?
You've talked about this a few times,
but you've never really explained it.
I have, however, it was years ago,
and given how many videos are on this channel
and how I title my videos,
good luck finding them.
So what we're gonna do is we're gonna go through it again.
Now, about half of you have signed up
since our public health issue stopped the world.
If you were around prior to that, you know all about this,
because a lot of the stuff that we,
the network I'm a part of, a lot of the stuff that we did,
we incorporated the channel.
We've done everything from supply shelters
the stuff that they need to a good example and the last one that was a major event where we
were leveraging everything was when the pandemic started getting supplies for hospitals and clinics
around us. If you remember in the beginning nurses didn't have the gowns they needed,
they didn't have masks, they didn't have equipment. That didn't happen around us.
We were able to leverage the network that we had in place and find the stuff that people needed.
It was nice and much easier because of the channel. Under normal circumstances,
you have to beg, borrow, and borrow quietly to accomplish stuff like that. Because of live
streams and the generosity of the people who watch this channel, we also had the option of buy,
which isn't normally available. So there is stuff on the channel about this but
I think it might be better given the growth of the channel and how old that
stuff is to just kind of run through it again. A community network is the
terminology I like to use. Now if you are heavy into leftist theory, when I start
talking about it you're gonna be like hey that's a yeah it is if you're a vet
and you're gonna be like what that's for some yeah it is the reason you will see
this template in a whole bunch of different places is because it works
it's super effective you can accomplish a lot doing this you are leveraging
everybody involved and everybody they know at one time so what is it plain and
simple, I like to keep it very open. So when people are setting one up, I
suggest they just have people commit to the idea of we're going to help each
other and make the community better. Something really simple, really open-ended.
Now, if you have a whole bunch of people who are very, very strong
advocates of any particular cause, you can narrow it down if you want, and that's
that's fine. I think that versatility is really important and it's going to become even more
important as time goes on. So, I would try to keep it open. Now, as far as size, I have found
generally 8 to 12 is a good number. When you get above 12, personality conflicts start and
And eventually the groups just kind of split, but that's fine.
Don't let any infighting occur, allow a split to occur naturally because then you have two
groups providing coverage to the same area.
You can work together, leverage each other, and it works out better for everybody.
If you only have four people, well, you go to war with the army, you have, you know.
Like most times you need a little bit more than that.
You need around eight people to get a good, a good network going because it's those eight
people and then all of the little networks they're a part of.
And whether you realize it or not, you're a part of a lot of different networks.
The key is bringing them together.
The key is bringing all of those social circles that you have together into a network that
can accomplish something good. When I was talking about this, I mentioned how the
Republican Party had used churches as turnkey community networks and one of
the very common comments was that, well, you know, we don't have anything like
that that we can do. Oh yeah, we do. We do. There's gonna be a whole video on that
on just different things that I promise exist in your area where you can find
like-minded people, or networks that already exist, so you can help them, and
in theory they would help you, and there would be this mutual assistance that
goes on. But that's the building block. The building block is the group of people
that exist outside of those turnkey networks, of those networks that already
exist of those demographic groups, those social circles, that have made the
commitment to use their skills, their friends, their networks, their powers to
to make the world better. You get that group together and you you'll be really
surprised what you'd be able to accomplish. So if you're going to try to
start doing this and you want to start one, your first step is to sit down and
think about all of the people you know who might be like-minded, who who might
be willing to commit a little bit of time each week or every other week to
this. Now, in the beginning, everything that I'm talking about, it doesn't really
relate to electoral politics. That comes as we move along. But your first step is
identifying the people who might be willing to do this and who would follow
through with it. And we will go through how to develop big larger networks. We
will go through how to get funding. We will go through all of this and we'll
just put out a video every couple of days on this topic. So I'm of the firm
belief that this is one of the most important things that you can do right
now. This is how you're gonna be able to help fix the situation and mitigate the
situation that we're going to find ourselves in.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}