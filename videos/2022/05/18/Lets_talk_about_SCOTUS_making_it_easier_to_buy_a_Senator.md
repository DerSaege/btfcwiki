---
title: Let's talk about SCOTUS making it easier to buy a Senator....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NUoKtbuyC8w) |
| Published | 2022/05/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court decision makes it easier for representatives in Congress to be bought by wealthy donors by striking down a law limiting post-election campaign repayments.
- Politicians can now use their own money to fund their campaigns, win elections, and then accept repayments from donors.
- Donors can give money to elected officials after they win, essentially putting cash in their pockets in exchange for potential favors.
- The dissenting opinion warns about the heightened risk of corruption when politicians can personally benefit from post-election repayments.
- The law that was struck down already allowed for $250,000 post-election repayments, but it wasn't enough for politicians.
- Senator Ted Cruz brought the challenge to this law, paving the way for increased corruption in politics.
- The Supreme Court's decision undermines efforts to keep money out of politics and ensures that representatives prioritize the wealthy over constituents.
- This decision is seen as a way for the political establishment to protect and enrich themselves at the expense of the country and its people.

### Quotes

- "The Supreme Court decision makes it easier for representatives in Congress to be bought by wealthy donors."
- "It is going to do everything it can to make sure the establishment can protect itself and can further enrich themselves at the expense of the country and the people."

### Oneliner

The Supreme Court's decision enables wealthy donors to buy influence in Congress by allowing post-election repayments, undermining efforts to keep money out of politics and prioritizing the interests of the political establishment over the people.

### Audience

Voters, Activists, Advocates

### On-the-ground actions from transcript

- Challenge corrupt campaign finance practices by supporting and advocating for campaign finance reform (suggested)
- Hold elected officials accountable by demanding transparency and accountability in their campaign finances (suggested)

### Whats missing in summary

The full transcript provides a detailed explanation of how the Supreme Court decision impacts political corruption and the influence of money in politics, offering insight into the potential consequences for democracy and the American people.

### Tags

#SupremeCourt #CampaignFinance #PoliticalCorruption #MoneyInPolitics #RepresentativeDemocracy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk a little bit
about how the Supreme Court just made it easier
for our representatives in Congress to be purchased,
because that's really the outcome here.
Back in 2002, a law went on the books
that limited the amount of money a politician could
use from their campaign and race after the election
to repay themselves.
So if, let's say, Senator X put $300,000 into their campaign,
well, they couldn't recoup all of that.
They couldn't raise money to repay themselves
after they had won.
OK?
This law was struck down.
So what it enables is for politicians
to put a whole bunch of their own money into their campaign
to ensure their victory.
Then afterward, somebody could come up and be like,
hey, Senator, I'd like to help you repay those loans.
I got a check for $10,000.
Let's meet at the club for dinner.
While we're there, I want to talk about something else, too.
And then they talk about a policy
that they would like to see and how
they have friends who are also supporters of the new senator
and would like to help them repay loans.
It creates a situation where an elected senator can take cash
and literally put it into their own pocket from donors.
And this occurs at a point in time
when they're able to exchange something for that money,
their vote.
The Supreme Court struck down this cap.
Now, keep in mind, the law as it stood
allowed for $250,000 to be done like this.
A quarter of a million dollars could already
be done like this.
But, you know, a quarter of a million dollars,
that's not enough for our politicians, I guess.
So the Supreme Court struck this law down.
The dissenting opinion says, repaying a candidate's loan
after he has won election cannot serve the usual purposes
of a contribution.
The money comes too late to aid in any of his campaign
activities.
All the money does is enrich the candidate personally
at a time when he can return the favor by a vote, a contract,
an appointment.
It takes no political genius to see the heightened
risk of corruption.
Yeah.
You don't have to be super smart to see
how this is going to play out.
Of course, the suit, the challenge to this law
was brought by Senator Ted Cruz.
This is going to enable corruption.
This is the political machine chipping away
at the attempts to get money out of politics,
to have our representatives represent us,
rather than those with large checkbooks.
The Supreme Court of the United States at this point
is going to do a whole lot of damage.
It is going to undermine the working class
at every opportunity.
It is going to do everything it can
to make sure the establishment can protect itself
and can further enrich themselves
at the expense of the country and the people.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}