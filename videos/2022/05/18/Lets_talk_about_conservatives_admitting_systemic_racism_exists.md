---
title: Let's talk about conservatives admitting systemic racism exists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N0jW_AmNRk4) |
| Published | 2022/05/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message from someone in Buffalo struggling with anger due to the spread of hatred by media outlets like Fox News.
- Views the problem as existing within white people, not something that people who are black can correct.
- Believes that systemic racism is the root cause of the fear and anger perpetuated by certain narratives.
- Notes that those spreading these theories, like Fox News, may not actually believe them but use them to manipulate their audience.
- Explains the fear behind embracing these theories: fear of losing societal edge and fear of being subject to racial inequality.
- Suggests that dismantling power structures upholding systemic racism could address these fears but acknowledges the resistance to giving up that societal edge.
- Expresses uncertainty about effectively countering these harmful beliefs and fears, recognizing that educating people out of them may take too long.

### Quotes

- "It's not your problem. It's a problem that exists within white people."
- "The root core of this belief and of the fear is that systemic racism is real."
- "If you believe that this is what will happen, it seems like the answer would be to get rid of the systemic racism."
- "I know why it's happening. I don't really know how to counter it, though."
- "Y'all have a good day."

### Oneliner

Beau sheds light on the roots of fear and anger perpetuated by systemic racism, questioning how to effectively counter harmful beliefs.

### Audience

Activists, Community Members

### On-the-ground actions from transcript

- Challenge harmful narratives and spread awareness about the root causes of fear and anger (implied).
- Support initiatives aimed at dismantling power structures upholding systemic racism (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the complex interplay between systemic racism, fear, and resistance to change in addressing harmful beliefs and narratives.

### Tags

#SystemicRacism #Fear #HarmfulBeliefs #MediaManipulation #CommunityAction


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a message I got.
While I can't really answer the message, not effectively anyway, I can shed some light on it.
Maybe other people can use that to guide their own actions, I guess.
Help me understand, brother. I'm from Buffalo,
and I don't understand what to do with my anger anymore because I've tried everything else.
It seems there is no way to stop these talking heads, i.e. Fox News, from spreading their hatred.
I don't know how to effectively channel your anger on this one.
Because through my lens, the way I see it, this isn't a problem that people who are black can correct.
It's not your problem. It's a problem that exists within white people.
I don't know what y'all can really do.
It's something that we have to fix.
And I'm sure that's absolutely no comfort whatsoever, given the fact that until it is fixed,
it's y'all who pay the price.
The thing is, those people on Fox News, they don't believe this.
I'm not saying they don't spread it. They absolutely do.
But they don't believe these theories. They don't believe replacement or any of that.
It's just a tool they use to manipulate their base.
That's it. None of their other positions actually line up with believing that this is true.
So I find it hard to believe that they actually believe this theory themselves.
They just use it to get people angry.
So the question is, why does it resonate?
Why do some people hear this and say, yeah, not just is that true, I should be angry about it.
It's fear. It's fear. And it's two separate reasons.
And both of them are an acknowledgement of something that they say doesn't exist.
They both acknowledge systemic racism as existing.
The first reason is that they're afraid.
Those people who believe it, they're afraid of having to compete on an even playing field.
They're worried that if the edge that society grants white people,
if that's gone, then they're going to fall even further behind.
Their life didn't turn out the way they want it to.
And if that edge that they get, if that disappears, well, they're going to be even worse off.
And other people will do better than them. Some who they view as lesser.
That's one of the fears.
The other is the fear that if demographics shift so much,
that they'll be subject to not only not having an edge,
but they'll have a system put in place against them that will hold them back.
They're worried about being on the receiving end of racial inequality.
Both of these acknowledge that systemic racism exists.
The root core of this belief and of the fear is that systemic racism is real.
And they just don't want it visited upon them.
To me, if you believe that this is what would happen,
it seems like the answer would be to get rid of the systemic racism.
I mean, that would be the smart move, right?
But a lot of them can't give up that edge.
Dismantling the power structures in the United States that uphold systemic racism,
if they do that to alleviate that second fear of it being visited upon them,
well, then they lose their edge and then they fall behind economically.
They have a lower status. That's why it exists.
That's why it resonates with some people.
I don't know how to address this really.
This seems like something that you could, in theory, over time, educate out of people.
The problem is that takes time, and as time goes by, there's more and more and more lost.
I don't have an answer to this one. I don't.
I know why it's happening. I don't really know how to counter it, though,
beyond something that, to my way of thinking, is just going to take way too long
and cost way too much.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}