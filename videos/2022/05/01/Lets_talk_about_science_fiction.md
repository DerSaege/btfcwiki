---
title: Let's talk about science fiction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1oY2LmTSAYI) |
| Published | 2022/05/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into the importance of science fiction in exploring truth and facts.
- Science fiction allows individuals to analyze ideas without the influence of societal norms and biases.
- It serves as a tool for examining commentary on the world by placing it in a different context.
- Good science fiction can inspire change in perspectives and values.
- Beau likens science fiction to gonzo journalism, focusing on getting to the truth rather than just facts.
- He mentions how works like "1984" are science fiction that provide commentary on society and its future.
- Star Trek's enduring fan base is attributed to its valid commentary on societal issues.
- Science fiction helps people escape reality and return with new perspectives.
- Beau believes that the lack of analysis and understanding in society is a significant issue.
- Analyzing deeper cues in media, like in science fiction, can lead to a better world.

### Quotes

- "Facts mixed with a little bit of fiction can often get you to truth and that's what science fiction is."
- "Good science fiction is commentary on the world around the author."
- "Science fiction can literally change the world."
- "If we want a better world, we have to analyze, we have to think deeper."
- "That type of thought, that deeper thought, is kind of the only thing that's going to save the world at this point."

### Oneliner

Beau delves into how science fiction offers a unique lens to analyze truth and societal commentary, urging deeper thought for a better world.

### Audience

Enthusiasts of science fiction

### On-the-ground actions from transcript

- Dive into science fiction literature and films to analyze societal commentary (suggested)
- Encourage others to think deeper about the media they consume (exemplified)

### Whats missing in summary

Exploration of specific examples of science fiction works that effectively provide social commentary and inspire change.

### Tags

#ScienceFiction #SocietalCommentary #Analysis #Truth #Change


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about truth and fiction and fact and being grounded and science
fiction because you seem very grounded and fact based, but when I look up a lot of the
references you make that I don't know, they're sci-fi references.
It doesn't seem to match your personality.
I know I don't really know you though.
I was just wondering if you could talk a little bit about why you find it interesting and
worth referencing in the subtle ways you do.
Information gets you to facts, right?
Facts mixed with a little bit of fiction can often get you to truth and that's what science
fiction is.
Science fiction is the examination of something by removing it from its current context.
Commentary on the world around you put into quite literally sometimes a different world.
So you lose all of your unconscious biases.
You aren't influenced by the societal norms around you because you're seeing it in a place
that's literally out of this world.
Doesn't have anything to do with what's around you so you don't feel a loyalty to a political
party or a societal norm or just peer pressure from dead people.
All of that fades away because it's just out here.
It's not part of this world.
Because of that, you're more free to examine those ideas when you're in that world.
And if they're good and they resonate and it's a work of science fiction that actually
has some value, it's going to change the way you look at the world, at that world.
And then when you are not immersed in that world anymore, you bring those values with
you and it can be very inspirational.
It's a lot like gonzo journalism.
You're telling a story that, yeah sometimes there's a little bit of liberties taken, but
it's not about the factual information at that point.
It's about getting to the truth.
And those two things aren't always the same.
Good science fiction is commentary on the world around the author.
What they saw, what they witnessed, the way they experienced certain things, and how they
see the world progressing.
Sometimes it can be inspirational, sometimes it's a warning.
In 1984, a lot of people don't think of that as science fiction.
It absolutely is.
That was commentary on the world the author saw.
And it became so enduring because it addressed themes that were ever-present and it was,
it saw pretty far into the future in a way.
It saw how society would progress.
That's what makes science fiction franchises stick around.
The reason Star Trek has been around so long and still has that avid fan base is because
the world that was seen initially, the commentary is still valid.
So it continues to have value, it continues to allow people to escape this world, be in
a different one, see similar events to what occurs here.
But because it's somewhere fictional, all the unconscious biases are gone.
So when they come back, when they step out of that book or look away from that screen,
they're not immersed in it anymore.
Those values are still there.
Science fiction can literally change the world.
Because at the end of it, even though it's generally seen as being about robots and space
and aliens, most of the commentary is just about being human and what humanity is, what
it could be, and what it shouldn't be.
I think one of the big problems in the world is that we don't spend the time to look into
something a little deeper.
We don't analyze things.
We don't look at the world and try to figure it out as much.
So much so that a lot of entertainment today doesn't even try to insert commentary or value.
It's just something to watch.
That lack of trying to analyze and understand, it bleeds over into everything else.
It's why you don't have people digging into the news, why they just accept the talking
points because they were never trained to look beyond the surface on anything.
Science fiction trains you to do that.
It's funny though, because the habit of not looking deeper into something has become so
widespread.
There are entire science fiction cult followings that don't understand the work that they're
following.
Starship Troopers is a great example of that.
There are a lot of people who watch that and think of it as a good thing.
That is definitely not the point of that show, of that work.
If we want a better world, we have to analyze, we have to think deeper.
We have to take those subtle cues and look into them like you did, like what prompted
this question.
That type of thought, that deeper thought, is kind of the only thing that's going to
save the world at this point.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}