---
title: Let's talk about pipelines and Putin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G0SHeWrScSs) |
| Published | 2022/05/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why Ukraine hasn't seized the initiative to attack critical Russian infrastructure like pipelines due to complex capabilities and balancing economic considerations.
- Points out that cutting off Russia's cash flow by attacking pipelines could harm European economies supporting Ukraine's war effort.
- Stresses the importance of maintaining economies that are aiding Ukraine and not angering them, even though going green is a national security priority.
- Dissects the question of why the West doesn't remove Putin, citing potential negative outcomes of destabilizing Russia and the uncertainty of who might come to power after Putin.
- Warns about the risks of engaging in actions to remove Putin and the potential consequences of a power struggle or breakup of Russia.

### Quotes

- "Is it more beneficial to Ukraine to have those economies functioning and pumping in what they need, or to cut off Russia's cash flow and perhaps anger the European economies?"
- "There are people who want a strongman ruler who's going to tell them what to do and just cast that image of power, even if it's hurting them."
- "If it's forced on them from outside, it will go bad."
- "That's not good."
- "It's not like the US has the same philosophical objections I do when it comes to removing a foreign head of state."

### Oneliner

Beau explains why Ukraine hasn't attacked Russian pipelines and why removing Putin isn't a viable option strategically or morally.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support initiatives that prioritize clean energy and national security. (exemplified)
- Advocate for diplomatic solutions and support efforts to maintain stability in regions of conflict. (exemplified)

### Whats missing in summary

The in-depth analysis and reasoning behind complex geopolitical decisions and their potential repercussions. 

### Tags

#Ukraine #Russia #Putin #Geopolitics #NationalSecurity


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about two questions that
have come in pretty frequently.
And they're asking why the West, or Ukraine in particular,
hasn't kind of embraced the idea that the best defense is
a strong offense.
Why haven't they seized the initiative
and gone after critical things?
So we're just going to kind of go through them,
because with the frequency that these questions come in,
they're probably being asked by a lot of people.
OK, so the first one is, why doesn't Ukraine just
go after the pipelines and cut off Russia's economy
themselves?
First, I don't know that Ukraine actually
has the capability to do that.
That's way more complicated than you might think.
OK, the other side to that is, while economic warfare has
definitely become accepted in this conflict,
and it's within bounds, so to speak,
to cut off Russia's cash flow like that, when they do that,
at the same time, they would be harming economies in Europe
that are very much funding the Ukrainian war effort right now.
So it becomes a balancing question.
Is it more beneficial to Ukraine to have those economies
functioning and pumping in what they need,
or to cut off Russia's cash flow and perhaps anger
the European economies and the countries that
didn't take their national security seriously enough
to go to renewables, much like the United States?
As I say that, the US is doing the same thing.
Just quick aside, going green, going to better energy,
cleaner energy, stop using dirty energy,
that's a national security priority.
And it's one that the US is ignoring
and has been for a very long time.
So is Europe.
So that's why.
They want to keep the economies that
are helping them functioning.
And they also don't want to anger them.
The next one is one that comes in really often.
Why doesn't the West just get rid of Putin?
I would say that's a bad idea for a whole bunch of reasons,
not just from my philosophical standpoint on stuff like this,
but from a strategic aspect of it, too.
So if the US did it, let's just say it's the US,
there would be a response from Russia.
Most people who send this message are like,
Putin's a dictator.
The people don't like him.
And sure, there are people who feel that way.
There are people who look at him as a bad person,
that they don't want leading their country.
And there's probably a lot of them.
And those are certainly the stories
that get highlighted in Western media.
However, no country is a monolith.
There are a whole bunch of people in Russia
who love their authoritarian bigot.
I mean, they view him...
People in the US should certainly understand this.
There's a segment of the population
who wants a strongman ruler who's
going to tell them what to do and just
cast that image of power, even if it's hurting them.
So that's one reason.
It wouldn't be well-received.
Now, let's say that the US was able to engage
in some super off-the-book stuff.
What happens next?
I mean, best-case scenario, it goes down.
They get past his wild security posture.
He takes his security very, very seriously.
So it would be very hard to do this anyway.
But let's say they get past it.
And they make it look like it originated
from within Russia.
So somebody else comes to power.
Best-case scenario is somebody who the US, the West,
can't predict comes to power.
What if they're worse?
What if they're worse than Putin?
And now they have an energized populace
because of the loss of the previous head of state?
That's not good.
And then worst-case scenario is that nobody comes out on top.
In that feud to take power after Putin is gone, nobody wins.
Nobody comes out a clear victor.
And fighting ensues.
And it leads to the breakup of Russia.
Now, from a Western perspective, having Russia weakened
like that, that would be a good thing
until you remember that strategic arsenal,
those nukes, those nukes would then
be in a whole bunch of different hands
if that happened to Russia.
It is better for the world for Russia to remain.
Now, I personally would like to see somebody else in charge
of it.
However, that's something that Russia has to decide.
The Russian people have to make that decision.
If it's forced on them from outside, it will go bad.
There's no way for that to go well.
Even with the best planning and making
it appear as though it was a patriotic effort from people
inside Russia, it would probably still
trigger a civil conflict there, which
then runs the risk of having somebody worse come to power
or breaking up the country and sending that arsenal
all over the place.
So that's a kind of a quick look at why those two things really
aren't on the table.
Although, it's not like the US has
the same philosophical objections
I do when it comes to removing a foreign head of state.
That's pretty clear.
But in this case, strategically, setting aside
the moral aspects, it doesn't play out well down the road.
So that's why it's off the table.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}