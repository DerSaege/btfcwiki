---
title: Let's talk about what aliens can teach us about tech companies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sRTyf-1drzk) |
| Published | 2022/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of aliens and their relationship to tech companies.
- Mentions the Fermi paradox and the theory that intelligent species tend to self-destruct before exploring space.
- Suggests that humanity's technological advancements have often been driven by war or a desire for control.
- Emphasizes the need for ethics in tech companies to move away from competitive and primitive instincts.
- Argues that tech companies should prioritize promoting equality and enlightenment for all of humanity.
- Criticizes tech figures who reject woke and egalitarian ideas, calling them con artists focused on profit rather than progress.
- Stresses the importance of an egalitarian society for the species to advance beyond current limitations.
- Warns that without philosophical advancement alongside technological progress, humanity is doomed to self-destruct.

### Quotes

- "Tech companies have to skew woke. They have to skew egalitarian."
- "If they skew towards nationalism, if they skew towards subjugation, if they skew towards old ideas of hierarchy and domination, well, those ideas will continue."
- "If tech companies are controlled by profit motive, we're doomed."
- "It's incredibly important for humanity to continue to grow at a faster rate than our technology."
- "If humanity doesn't philosophically advance along with it, it doesn't matter because that technology won't be around long."

### Oneliner

Tech companies must prioritize ethics and equality to prevent humanity from self-destruction amidst technological advancements.

### Audience

Tech professionals, activists, policymakers.

### On-the-ground actions from transcript

- Advocate for ethics and equality in tech companies (implied).
- Support initiatives and policies that prioritize the betterment of humanity over profit (implied).

### Whats missing in summary

The full transcript provides a deeper exploration of the intersection between technological progress, ethics, and societal advancement.

### Tags

#TechEthics #Egalitarianism #Humanity #Technology #FermiParadox


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to, we're gonna talk about aliens.
Yeah, we're gonna talk about aliens
and what aliens can teach us about tech companies
and the way tech companies have to be
if the species is to survive.
There is a great paradox that exists in science.
And it's the contrast between the number of intelligent
civilizations that should exist out there in space.
Because the estimates are all exceedingly high.
There should be people out there
whose estimates are all exceedingly high.
There should be tons of different civilizations
made up of intelligent life.
So you have that, and then you have the fact that,
well, where are they?
Why can't we see them?
Why can't we find them?
Why aren't they around?
It's called the Fermi paradox.
And one of the leading explanations for this,
which I put a whole lot of faith in,
is the idea that intelligent life, intelligent species,
tend to, well, destroy themselves
and wipe themselves out
before they can really get out there
and start exploring space.
And if you think about it, it makes sense
from humanity's perspective, sure.
Atom.
What's the first word that came to mind?
Probably bomb.
Right?
It's a basic building block of the world that we inhabit.
But it is inextricably tied to images
of the world being destroyed.
Because most of humanity's great technological achievements
have been driven by war.
So, to me, that explanation makes a lot of sense.
And those that weren't driven by war
were often driven by a desire to control the lessers.
A way for our betters up there to keep us common folk in line.
Subjugation. It's still competition.
So, what that means is that you have a mandate,
a requirement, that tech companies have ethics.
And tech companies try to move away
from that competitive streak,
that primitive instinct that humanity has.
Because if they don't, well, they'll foster it.
And if they foster it, we destroy ourselves.
Tech companies, particularly those that deal
with the flow of information and ideas,
have to skew woke.
They have to skew egalitarian.
They have to promote equality for all of the world.
Because if they don't, by default,
they'll continue to feed that primitive instinct.
Those old ideas.
They'll continue to reinforce tribalism
or imaginary lines on a map
in that competitive, war-like nature.
And if they do that, we're doomed.
Tech companies should be about trying to enlighten the world.
And the reason this is important is because
you have leading figures in that world right now
decrying being woke.
These are people who built their image and their idea
on changing the world,
taking us to Mars or whatever.
When you abandon the desire
to move humanity forward,
all of your technological advancements,
all of your achievements,
well, it really just turns into you being a capitalist,
a con man in a slick suit,
trying to sell a dream so you can scam investors.
That's what it turns into.
If you don't actually try to maintain those ethics,
if you don't try to create a world that is better,
if you don't want to use that technology
to make the life better for all of humanity,
you're a hack.
You're not a visionary.
You're just another con man in a suit.
The idea of an egalitarian society
is a requirement for getting off of this rock.
It is a requirement for the species to move forward.
Ideas that foster conversation,
they have to skew that way.
If they don't,
if they skew towards nationalism,
if they skew towards subjugation,
if they skew towards old ideas of hierarchy and domination,
well, those ideas will continue.
And eventually they lead to a war,
and we wipe ourselves out.
Or they become so pronounced
that some theocratic superstitious government
takes over everywhere,
and we're all just trapped under its thumb.
Or the profit motivator is there
so people ignore things that filter out civilizations,
things that cause extinction,
like climate change.
If tech companies are controlled by profit motive,
we're doomed.
If they're controlled by the desire to maintain the status quo,
we're doomed.
Because the technology will continue to advance.
If humanity does not move along with it,
if humanity does not advance,
if it doesn't become more enlightened and more egalitarian,
we're doomed.
It's incredibly important for humanity
to continue to grow at a faster rate than our technology.
If we try to maintain the status quo
and the current modes of thought as technology increases,
we will wipe ourselves out.
And there's a number of ways it could happen.
It could be a problem
until it's too late to do anything about it,
like climate change.
It could be war.
It could be a system of governance
that is so intense
that there's no escape from it.
And that system,
it doesn't want to leave the rock.
It has control here.
If you see tech leaders bemoan egalitarian ideas,
woke ideas, equality,
recognize them for what they are.
All of everything that they've ever said
is just a PR blitz.
And they're just a normal capitalist
playing on your emotions,
playing on your desire
to be part of the technological advance.
If humanity doesn't philosophically advance along with it,
it doesn't matter
because that technology won't be around long.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}