---
title: Let's talk about the military going green....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pSgN8skKZJ8) |
| Published | 2022/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Defense (DOD) has launched various climate and environmentally friendly initiatives, aiming to reduce the carbon footprint by 50% and more.
- Politicians, backed by dirty energy companies, often criticize and mock these initiatives, claiming they are impossible to achieve.
- A common criticism is the idea that electric vehicles won't outrun Chinese tanks, like a Prius versus a tank scenario.
- In Ukraine, Russian tank drivers are facing a new challenge from Ukrainian soldiers on e-bikes.
- Ukrainian soldiers use e-bikes, donated and modified by a private company, to set up ambushes against tanks effectively.
- E-bikes provide speed, quietness, and agility, making them more efficient for military operations compared to soldiers on foot.
- The real-life scenario in Ukraine showcases tanks running from electric vehicles, contrary to the skeptics' claims.
- Russia's invasion in Ukraine faced setbacks not due to Ukrainian resistance initially but because their vehicles ran out of gas.
- The future of military vehicles may involve producing their own energy or having convoy vehicles that generate energy for others.
- Those deeming things impossible should not hinder those actively working towards achieving them.

### Quotes

- "Those who say something is impossible should probably stop interrupting those who are doing it."
- "It's one of those things you can keep up or get left behind, but nobody really cares anymore."

### Oneliner

The Department of Defense is advancing environmentally friendly initiatives while critics question their feasibility; Ukraine showcases the effectiveness of e-bikes in military operations, debunking skeptics' claims.

### Audience

Environmental activists, military personnel

### On-the-ground actions from transcript

- Support environmentally friendly initiatives in your local military or community by advocating for sustainable practices (exemplified)
- Donate resources or funds to provide efficient modes of transportation for military or emergency response teams (exemplified)

### Whats missing in summary

The full transcript provides a deeper insight into the intersection of environmental sustainability and military operations, offering a unique perspective on the feasibility and effectiveness of cleaner technologies in challenging scenarios.

### Tags

#Military #Environment #Sustainability #Ukraine #ElectricVehicles


## Transcript
Well, howdy there, internet people.
It's Bob McGowan.
So today, we're going to talk about the environmentally
friendly military and the criticisms and comments that
arise any time the military brings this up.
If you don't know, DOD has launched a whole lot of climate
and environmentally friendly initiatives over the last year
or so.
I mean, some of them are pretty ambitious, reducing carbon
footprint by 50%, like just all kinds of stuff.
And any time this comes up, politicians, a certain subset
of politicians who are owned by dirty energy companies, well,
they show up to say it's impossible.
They show up to ridicule and mock it.
One of the most common is that you're not
going to be able to outrun that Chinese tank
in your electric vehicle.
That Prius just isn't going to hold up.
Ha ha.
And I get it.
I mean, for a lot of people, they view anything that hasn't happened before as impossible.
And everything is impossible until it's not.
In Ukraine, there's been an interesting development.
Russian tank drivers there are facing a new plague, a new threat that they're having a
a hard time dealing with. E-bikes.
Electric bikes, little electric dirt bikes.
So, what's happening
is these e-bikes have been donated by a private company and they've been refitted
with a little rack that will hold in any tank weapon.
So, the Ukrainian military identifies
where a couple of tanks are. The soldiers hop on their little
e-bikes because they're fast, because they're quiet, they can get out ahead of the target,
and they set up an ambush, destroy it, and then disappear.
They're a whole lot more effective this way than they are on foot because they can move
more quickly, they're faster, they're quiet, they're everything that you need in a situation
like this.
The ultimate irony being with all of the people talking about, you know, the Chinese tank
chasing the Prius, it turns out in real life it's the tank running from the electric vehicle.
There's another lesson from Ukraine when it comes to militaries embracing cleaner technologies.
Why'd Russia's invasion fail?
Why did it go bad?
Because it stalled, right?
stalled. Was it Ukrainian resistance that stalled them initially? Not really. They
ran out of gas. It's not too long into the future when vehicles will produce
their own power, their own energy. Or there will be vehicles that travel with
the convoy to produce energy for all the other vehicles. This is the future. Again,
it's one of those things you can keep up or get left behind, but nobody really
cares anymore. Those who say something is impossible should probably stop
interrupting those who are doing it. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}