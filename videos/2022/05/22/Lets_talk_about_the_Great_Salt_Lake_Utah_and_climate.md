---
title: Let's talk about the Great Salt Lake, Utah, and climate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=n2WWA4uZAC0) |
| Published | 2022/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Great Salt Lake in Utah is receding, currently 11 feet lower than when pioneers arrived, causing damage to birds and wildlife.
- For every foot the lake drops, 150 square miles of the lake bed are exposed, impacting the economy and Utah's GDP.
- Proposed solution: building a pipeline to pump in salt water from the Pacific, facing potential environmentalist backlash.
- Climate change is a significant factor in the lake's decline, with impacts being felt particularly in the western and southwestern regions.
- The prevailing attitude of trying to overcome climate change without changing our way of living is unsustainable and doomed to fail.
- Exposed lake beds pose additional risks due to arsenic content, exacerbating the already dire situation.
- Delaying necessary changes will only worsen the effects of climate change and necessitate more drastic actions in the future.
- The impacts of climate change are inevitable and will affect everyone, requiring proactive measures from legislatures across the country.
- Utah's consideration of solutions like the pipeline is a start, but real change may involve altering consumption patterns and industries in the state.
- Internal and external migration due to climate change is likely, underscoring the need to acknowledge and prepare for these realities.

### Quotes

- "The longer we put off making the changes that are necessary, the worse it's going to get."
- "We can't just keep pretending like it's not happening."
- "There are impacts from climate change that are guaranteed to happen. They won't just skip us."
- "We're just seeing the tip of the iceberg that's melting."
- "There are news stories every single day that show the impacts that are already here."

### Oneliner

The Great Salt Lake's decline due to climate change underscores the urgent need for proactive, sustainable solutions to address environmental impacts and ensure a livable future for all.

### Audience

Legislatures, policymakers, environmentalists

### On-the-ground actions from transcript

- Start considering proactive measures to address climate change impacts (implied)
- Advocate for sustainable solutions and policies in response to environmental challenges (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the environmental and economic consequences of the Great Salt Lake's decline, urging immediate action to mitigate climate change effects and transition to more sustainable practices.

### Tags

#ClimateChange #GreatSaltLake #Utah #EnvironmentalImpact #Sustainability


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk a little bit about, well,
the Great Salt Lake and Utah and the situation there
and what we might be able to learn from it.
So the lake is receding.
It's been receding for a long time.
It is currently 11 foot lower than when the pioneers arrived.
So receding shorelines, this damages birds, wildlife,
so on and so forth.
Every foot that it drops, it's 150 square miles of the lake
bed that is now exposed.
It's a big deal.
The lake itself is responsible for somewhere between $1.69
and $2.17 billion in economic activity.
It accounts for roughly 1% of Utah's GDP.
But the lake is going dry.
One of the proposed solutions to this is to build a pipeline
to the Pacific and pump in salt water.
That seems like a unique approach.
I'll wait for the environmentalists
to weigh in on this.
But I've got a feeling that that's not going
to be their preferred method.
This is another sign.
This is another impact from climate change.
And people don't want to acknowledge
what is happening all around us, particularly out west,
particularly in the southwest.
The concept, the general tone of people right now
is to try to find some way to overcome climate change
and not alter the way we're living.
That's not going to work.
That will not be successful.
And the longer we put off making the changes that
are necessary, the longer we put off
living within what the region provides,
the worse it's going to get.
Now, the exposed lake beds cause an issue in and of themselves
because there's arsenic in them.
So when there's windstorms, it blows it around.
I mean, this is all bad.
It is a bad situation.
Something needs to be done.
But we're still faced with the attitude
that we will find some way to just push right on through
and not alter the way we're doing business,
the way we're running the world, the way we're
protecting the areas around us.
It won't work.
It is doomed to fail.
And the longer we put off just accepting
that we're going to have to make significant changes,
the worse it's going to be.
And the more changes we're going to have to make because we'll
be further along.
There are impacts from climate change
that are guaranteed to happen.
They won't just skip us.
They're going to happen at this point.
We're just seeing the tip right now.
We're just seeing the tip of the iceberg that's melting.
Legislatures around the country really
need to start thinking about this.
I don't actually want to come down too hard on Utah
because at least they're thinking about it in some way.
I have a whole lot of questions about the pipeline.
But at least they're trying.
I'll give them that.
But I'm fairly certain that the real solution here
is altering consumption in the state,
altering the industries that exist within the state.
And more than likely, there's probably
going to be a number of people who end up leaving the state.
We're going to see people move because of climate change
internally within the United States and from outside.
We've got to get ready for this.
We can't just keep pretending like it's not happening.
There are news stories every single day
that show the impacts that are already here.
And we try to pretend like it's something else.
We try to pretend like if we ignore it, it'll go away.
It won't.
It'll get worse.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}