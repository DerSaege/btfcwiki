---
title: Let's talk about wheat, India, and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4k8jMr2I3sM) |
| Published | 2022/05/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the impact of the wheat supply chain disruption due to the conflict in Ukraine.
- Mentions that India produces wheat but has restricted exports, leading to potential global food insecurity.
- Notes that the Russian invasion removed a significant portion of the wheat supply.
- Predicts that prices of grains and vegetable oils will increase substantially in the US and Europe.
- Warns that in some countries, food might not even be available due to the supply chain disruptions.
- States that the United States and India might make exceptions to help areas facing food insecurity.
- Emphasizes that the situation will persist until the next crop season, leading to prolonged price increases.
- Points out the importance of world leaders addressing supply chain issues to stabilize prices.
- Acknowledges the uncertainty regarding the severity and duration of the global food crisis.
- Concludes by indicating that the effects of the disrupted wheat supply chain are now being felt globally.

### Quotes

- "Food insecurity is a global issue, and in some areas, it is more dramatic than others."
- "It's going to be until the next crop."
- "When you see prices on these items start to rise even more, well, you're going to know why."

### Oneliner

The global wheat supply chain disruption due to the Ukraine conflict leads to rising prices and potential food insecurity worldwide.

### Audience

World Leaders

### On-the-ground actions from transcript

- Monitor local food prices and availability to prepare for potential increases (implied).
- Support community food banks and organizations aiding those facing food insecurity (implied).

### Whats missing in summary

Importance of monitoring ongoing developments and collaborating with global partners to address the food supply crisis. 

### Tags

#WheatSupply #GlobalFoodInsecurity #UkraineConflict #PriceIncreases #SupplyChainDisruption


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about wheat,
the wheat supply, India, Ukraine,
and we're going to talk about how
what we talked about two months ago
is happening in a dramatic way right now.
A couple of months ago,
there was a video that I put out
talking about the wheat supply.
Ukraine is a large exporter of wheat.
The conflict there disrupted that supply,
and we knew then that there were going to be
ripple effects from it.
I think downstream effects is actually
in the title of the video.
We're seeing those in a very pronounced way right now.
India, which I think we talked about in the video,
produces wheat, but generally produces
just a little bit more than it needs.
It has enough for its population.
They have restricted exports,
so they're not going to be exporting at all commercially.
They have indicated that they might be willing
to export to countries that were in need,
and we'll get to what that means in a minute.
The Russian invasion is what triggered all of this.
It removed a massive chunk of the supply.
The demand is still the same.
Now, for the United States and Europe,
it's going to cost more.
Anything having to do with grains,
vegetable oils, stuff like that,
prices are going to go up,
and they're going to go up pretty substantially.
But that's in the US and Europe.
There are going to be countries
where it's not a matter of what it's going to cost.
It's a matter of it not being available for them.
Food insecurity is a global issue,
and in some areas, it is more dramatic than others.
It does appear that the United States
is more dramatic than others.
It does appear that India might be willing to make exceptions
to help those areas so they can actually get bread,
stuff like that.
But this is something that we don't know
how severe it's going to be
other than it's going to be severe.
If I'm not mistaken, wheat is at a record high right now.
So not going to be something that is short-lived.
It's going to be until the next crop.
So anything that you regularly purchase
that is based in grain, in vegetable oil,
anything like that, it's going to cost more.
How much it goes up depends on how well world leaders
work out some form of temporary supply chain
that it makes up for the loss of Ukraine's crop
and the instability that has spread from that.
I don't really have a prediction on how well that's going to work out.
There are a lot of supply chain issues right now.
This is just one more.
But when you see prices on these items start to rise even more,
well, you're going to know why.
This is one of those things that when we talked about it,
we're like, it's already happened, we just haven't felt it yet.
Now we're feeling it.
Anyway, it's just a thought. Thanks for your time.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}