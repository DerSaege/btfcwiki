---
title: Let's talk about when the wealthy meet climate change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hYa19YeYE-s) |
| Published | 2022/05/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Major drought in the southwestern United States leading to water usage regulations.
- Las Virginas in California, an area with high water consumption and wealthy residents, imposed water usage restrictions.
- Wealthier individuals sought exemptions for koi ponds, car washing, and lawns despite facing climate change impacts.
- Urges rethinking landscaping to fit the region's resources and adjust to climate change reality.
- Growing food to combat food insecurity and adapting landscaping to be sustainable are key solutions.
- Emphasizes that people must adjust their lifestyles to match what the region can sustain.
- Wealthier individuals may try to bypass regulations to maintain their lifestyle, impacting less fortunate communities.
- Calls for vigilance at the local level as climate change impacts worsen.
- Criticizes the surprise displayed by some individuals at being asked to reduce water consumption despite widespread awareness of climate change.
- Advocates for preparing for climate change impacts and adjusting lifestyles to reduce stress on resources.

### Quotes

- "The response from the wealthy is to try to buy their way out of it. It's not going to work. The water doesn't exist."
- "Your footprint in the area that you live has to exist within the confines of what that area can provide."
- "Lawns are going to turn brown. You're going to have landscaping that has to come from the native area."
- "Everybody knows this is coming. We have to make preparations."
- "The funny thing is, this region, it's actually really pretty when you don't mess it up with a bunch of fake landscaping."

### Oneliner

Wealthy individuals seek exemptions from water usage restrictions in drought-stricken areas, ignoring climate change impacts and stressing the importance of adjusting lifestyles to fit within regionally sustainable parameters.

### Audience

Residents, Environmentalists, Community Leaders

### On-the-ground actions from transcript

- Adjust your landscaping to be more compatible with the region you live in (suggested).
- Grow food to combat food insecurity (suggested).
- Advocate for sustainable practices in your community (implied).

### Whats missing in summary

The full transcript provides additional insights on the importance of preparing for climate change impacts, the need for vigilance at the local level, and the repercussions of wealthy individuals attempting to bypass regulations to maintain their lifestyle.

### Tags

#ClimateChange #WaterUsage #Sustainability #CommunityAction #Adaptation


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about what happens when the
wealthy meet climate change.
There is a major drought, a mega drought, going on in the
southwestern United States.
We've talked about it on the channel a few times.
This has led to water usage regulations.
And these regulations are being imposed all over the Southwest.
Now, recently in Las Virginas in California,
they had a Zoom call to talk to people from the area
and explain the water usage restrictions
that were going to go into effect.
This is an area that is incredibly wealthy, generally speaking.
It also has a lot of high water usage.
It has high water consumption.
In 2021, the average water usage per person per day was 193 gallons.
Now, statewide, the average per person per day was 91 gallons.
They're using more than double the state average.
And 70% of it is used outside to maintain lawns or something
to that effect.
It probably isn't going to come as a surprise to the people who
watch this channel.
When the wealthier people were informed of the water usage
directions, their first response was to seek an exemption for their koi ponds or
for washing their cars or for the lawn that they had installed. The reality is
not really setting in. We're dealing with climate change. All of those impacts
that scientists have warned about for years and years and decades are now upon
us. They're here. The response from the wealthy is to try to buy their way out
of it. It's not going to work. The water doesn't exist. It's not there. One thing
that people across the United States are going to have to do is adjust the way
they think about their land, their lawn. You know, I'm a huge proponent of growing
food to also assist with food insecurity. If you're not somebody who's willing to
do that. Please find a way to make your landscaping fit with the region that you
live in. If you have to water it you need something else. Your footprint in the
area that you live has to exist within the confines of what that area can
provide. That's that's one of the key parts of being sustainable and it doesn't
matter where you live in the United States, there's landscaping options that are beautiful.
You just have to fit within what the region provides.
You can't expect to have lush lawns if you chose to live at the edge of a desert.
That's not how it works.
That's why there weren't lawns there.
when it cost $8,000 to have graphs put in.
The overall attitude that was displayed and the just almost
denial of the situation is something
that those of us on the bottom need to really get ready for.
Because as these impacts strengthen
and the situation deteriorates and areas within the United
States become less hospitable, the wealthy in those areas
are going to try to game the system.
Whatever system exists to try to keep that area afloat,
they're going to try to find some way to bypass it
and maintain the lifestyle they had before they
realized that there was a problem.
This is something that you're going
to have to keep an eye on at your local level,
because it's going to be everywhere.
This was a humorous example, because there
were more than 600 questions that all revolved around news
that's been everywhere, the fact that we're
in a major drought in this country in the Southwest.
And there were people involved in this who truly seemed surprised that they were being asked to reduce water
consumption.
As much coverage as this topic, climate change in general, has received, there's no excuse for surprise.
Everybody knows this is coming.
We have to make preparations.
And one of the things that everybody can do
is adjust their footprint where they live
to be more compatible with the region they live in
and stick within the confines of the resources that
are provided by that region.
Doing that reduces stress on the overall system
and allows for a higher standard of living.
The green lawns and the non-native plants that people like to have as landscaping, that's
going to have to go away.
Lawns are going to turn brown.
You're going to have landscaping that has to come from the native area.
The funny thing is, this region, it's actually really pretty when you don't mess it up with
a bunch of fake landscaping.
Most areas in the United States have plenty of options, and you're going to have to use
them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}