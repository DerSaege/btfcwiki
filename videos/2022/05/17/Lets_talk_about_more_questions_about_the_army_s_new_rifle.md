---
title: Let's talk about more questions about the army's new rifle....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5Pc0cYxopHM) |
| Published | 2022/05/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how civilians are getting the new rifle first, clarifying that gun companies develop designs based on Department of Defense criteria and rejected designs often end up in the civilian market.
- Details the reasons behind the Army changing rifles, including the need for increased range and power to counter body armor in engagements.
- Points out the potential impact of the new rifle on domestic incidents, especially in terms of defeating body armor and altering the dynamics of confrontations.
- Responds to a comment attempting to correct misconceptions about the term "AR" in AR-15 and provides historical context about weapon weights during World War II.
- Criticizes the blaming of gun violence on gun owners, questioning the lack of personal responsibility and endorsing violence within gun culture.
- Attributes America's decline to demonizing education and nostalgia for an idealized past, urging people to embrace progress and move forward.

### Quotes

- "The Army realized that during Afghanistan, engagements were testing the limits of the 5.56 round and AR platform."
- "Y'all have been saying guns don't kill people. People kill people. I'm blaming the people."
- "It's absolutely the gun crowd's fault."

### Oneliner

Beau explains the implications of the Army's new rifle, challenges misconceptions about gun culture, and addresses America's decline due to nostalgia and ignorance.

### Audience

Gun owners, policymakers, activists

### On-the-ground actions from transcript

- Contact local policymakers to advocate for responsible gun laws and regulations (suggested)
- Join community organizations working towards reducing gun violence (implied)

### Whats missing in summary

Deeper insights into the societal impact of weapon advancements and the importance of accountability in addressing gun violence.

### Tags

#GunControl #MilitaryTechnology #Misconceptions #Responsibility #Progress


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk a little bit more
about that new thing the Army's getting.
We're going to do this because in that first video,
I relied heavily on technical data
to try to get the point across.
I don't think it worked.
A whole bunch of questions came in,
so we're going to kind of go through them.
Because even though it was 40 or 50 questions,
it was really only three questions,
just phrased differently.
And we're gonna go over those.
And the other thing is that, really,
once we go over two of them,
you're already gonna know the answer to the third.
So we're gonna go through that.
And then I'm gonna read another comment
that was sent to me and talk about it a little bit,
just so everybody can kinda end on a laugh.
Okay, so how are civilians getting it first?
And if you don't know what I'm talking about,
the Army's getting a new rifle.
This is normal.
This is normal.
What happens is DOD says, hey, gun companies,
we need a gun that does X, Y, and Z.
And gun companies come up with a design to fit that criteria.
And then those designs are tested.
Those that are rejected typically
end up produced on the civilian market.
In this case, the company that happened to win the competition
and have their rifle selected had already
made the decision to put it on the civilian market.
So that's how that's happening.
By the time I talked about it, even if they
hadn't released it early, it would already
be in the works to be released to the civilian market
under normal circumstances.
The other two questions is why is this going to change things so much and why
is the army changing rifles to begin with. If you understand why the army's
changing rifles you'll understand why it's going to change things domestically.
Basically the army realized that during Afghanistan there were a lot of
engagements that were testing the limits of what the 5.56 round, the AR platform,
what it was capable of doing. Ranges were being exceeded and stuff like that so
they needed a new round. They wanted the American warfighter to be able to reach
out and touch someone. So that's where the new bullet came from. New bullet, you
need a new platform. That's where the new rifle came from. So what the Army really
wanted was something that had increased range. Now this also has to do with us
shifting to near piers. So what they really kind of asked for was something
that had a greater range than expected opposition weapons. And they found it. By
the data that's been released it looks like in a combat scenario the American
soldier will be able to fire at somebody about a football field further, then they
will be able to fire at them. So they have to close about a hundred yards
before they're in range so they can use their weapon. That's a huge
advantage in long-range engagements. Historically, these ranges
aren't actually reached in most modern combat.
Now I know that the gun nerds are going to be down below and they're going to tell you
no it's more than a football field and that's true but that's for range conditions, perfect
conditions not necessarily combat but that's a huge advantage.
The other thing is they wanted a weapon that had enough power to be effective against body
armor because near peers are likely to provide their troops with body armor,
something that non-state actors normally don't have. So this rifle will be more
effective at countering body armor. When do these incidents domestically, when do
they stop when the cops show up? Right. That may not be the case anymore because
the weapon in hand may be more capable of defeating body armor. All of the
technical specs that I gave also it's going to be more horrifying. This is a
much more powerful weapon and it's in a compact package that's just as easy to
maneuver. So that's going to shift the dynamic because the live streams are
going to be more graphic, the footage that comes out is going to be more graphic, and
then it's not going to end the way that it has been because they may decide to engage.
And that's just going to alter the debate even more.
Okay, so now let's get to something to bring a little bit of levity to this.
You're a typical really mean word for liberal who knows nothing about guns.
I bet you think AR stands for Assault Rifle.
Actually, it stands for Armolite Rifle.
The gun crowd, as you call us, isn't going to buy the new rifle because it's too heavy
and will be too hard to carry.
You blame shootings on gun owners and that's stupid.
You're the reason America is failing and has been since liberals took over after World
War II.
Our country's peak was during World War II.
It was a country that had real men who would storm the beaches of Normandy.
You and today's men are too weak for that."
And then it goes into a profanity-laden rant questioning my orientation.
So I'm just going to run through some high points here.
Actually it stands for Armalite Rifle.
Actually it probably doesn't.
It probably just stands for Armalite.
The first two letters, AR.
The product catalog, if you look at it, everything from Armalite starts with AR, including things
that aren't rifles, like their shotguns, or their kits to be inserted into the Mk.
XIX Grenade Launcher.
They were designated with AR as well.
Actually, it's probably not that. Remember that part about the rifle. They won't buy it because it's too heavy and will
be hard to carry.
You blaming shooting on gun owners is stupid. Who should I blame them on? Non-gun owners? I mean, that doesn't make
sense.
This whole time y'all been saying guns don't kill people. People kill people. I'm
blaming the people. Y'all are the ones that fostered this culture.  Yeah, it's on you.
You're the reason America is failing and has been since liberals took over after World
War II.
Our country's peak was during World War II.
Yes, under the leadership of renowned conservative FDR.
What?
FDR is kind of viewed as the father of modern progressivism.
Just so you know.
The period that you're looking back to as America's peak was brought about by a liberal,
to use the terminology you want to use, was a country that had real men who had stormed
the beaches of Normandy.
I do not understand why storming the beaches of Normandy has become synonymous with all
of World War II, but whatever, you and today's men are too weak for that.
Fun fact from somebody who doesn't know anything about guns.
The rifle that most soldiers carried, most American soldiers carried as they stormed
the beaches of Normandy was the M1, which weighed in at nine and a half pounds.
That was its base weight, which is more than the one that you said you didn't want to buy
because it was too heavy and too hard to handle.
So I mean, I found that entertaining.
I don't think that I personally am responsible for America's decline, which is what it says
here.
You're the reason America is failing.
Now, I think America is failing because a whole lot of people want to demonize education
and they are supremely confident in their ignorance.
And they fall for nostalgia for a period in time in which they weren't even alive.
view of the 1940s and 50s, that's not based on your own experiences. It's based
on TV shows you saw when you were a kid. You watched nostalgic TV shows that
painted this period in a great light and believed that's what it was. There's no
way that you got your impression of the 1940s and 50s from reading history books
if you believe that FDR was a conservative, you got it based on this
this TV image that was manufactured for a feel-good era. That's it. That's not
real life. The world is going to move forward. It will continue to move forward.
You can keep up or get left behind and most of us don't really care anymore. It
It is the gun crowd's fault.
It's the gun crowd's fault.
This culture, the rhetoric that's constantly used,
the constant endorsement of violence for anything,
that's why this is happening.
It's absolutely the gun crowd's fault.
The idea that the gun makes the man.
I don't know who else you want to blame. From a group of people who always talk
about personal responsibility, you don't really seem to like to carry that weight
yourself. I guess it weighs more than nine pounds. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}