---
title: Let's talk about the future of the US, Russia, and Africa....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8q34HggiVME) |
| Published | 2022/05/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the future of the foreign policy of the United States, focusing on Africa and Russia.
- Mentions the creation of Africa Command by the United States in preparation for Africa becoming a priority.
- Describes a bill in Congress, HR 7311, aimed at countering Russian influence in Africa.
- Points out that while framed as countering Russia, the bill actually prepares the US for increased influence in Africa.
- Outlines the strategy of strengthening democratic institutions, human rights, and monitoring natural resources to bring African nations into the US sphere of influence.
- Notes that the US intends to use soft power to establish influence rather than military force.
- Mentions the creation of de facto agents of influence and elevating cultural leaders friendly to US interests as part of the strategy.
- Indicates that the US plans a long-term foreign policy strategy in Africa akin to its campaign in Ukraine.
- Emphasizes that while these operations may not be horrible for the countries involved, there is always a coercive element accompanying them.
- Concludes that with the bill's passage, Africa will become a hotspot for US influence and resources.

### Quotes

- "Africa is the new hotspot right then."
- "Generally speaking, these types of operations are not horrible for the countries that they're run in."
- "The United States is committed to this course."
- "The United States is going to try to implement."
- "Y'all have a good day."

### Oneliner

Beau outlines how the US is gearing up to extend influence in Africa under the guise of countering Russian activities, with a focus on soft power strategies and a long-term foreign policy vision.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Contact local representatives to express concerns about US foreign policy strategies in Africa (suggested).
- Join organizations focused on monitoring and advocating for transparency in foreign policies (exemplified).

### Whats missing in summary

Insights on the potential impacts of increased US influence in Africa and the importance of monitoring and questioning foreign policy decisions.

### Tags

#USForeignPolicy #Africa #Russia #InfluenceOperations #SoftPower


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the future
of the foreign policy of the United States.
And we're going to talk about where that future lies
geographically, where the United States is about to become
incredibly active.
Along the way, we're going to talk about Russia,
and we're going to talk about Africa.
For years on the channel, we've talked
about how Africa is going to be the new area where the world
powers are incredibly interested,
where they're all going to be playing their little world
control games.
We've discussed the little steps the United States
has taken along the way to get ready for this,
such as the creation of Africa Command.
We talked about how the deprioritization
of the Middle East will lead to Africa becoming a priority.
The question is, when does that start?
Now, right now.
There is a bill currently in Congress.
I think it already passed the House.
It's HR 7311.
It is titled the Countering Malign Russian Activities
in Africa Act.
And it lays out the strategy.
It lays out what's going to happen
and how the United States is going
to attempt to bring nations in Africa closer to the US,
to bring them into the US sphere of influence.
And it's framed, it's given a very thin veneer of really
being about countering Russia.
But it's a veneer, and you can see that in the way
the bill's laid out.
It is the sense of Congress that the United States should
regularly assess the scale and scope of the Russian Federation's
influence and activities in Africa that undermine United
States objectives and interests.
That's the introduction.
And determine how, A, to address and counter
such influence and activities effectively,
including through appropriate United States foreign assistance
programs, and B, to hold the Russian Federation accountable.
The framing of Russia is an afterthought
throughout all of this.
This is Congress telling the State Department
to get ready, that Africa is about to become the priority.
They have this thin veneer of countering Russian influence,
which it's true, no doubt, the United States is
going to set out to do that.
But to counter Russian influence,
they're going to be running their own influence operations.
Aside from that, openly stating that this
is to counter Russian influence means
that their influence operations, the US influence operations,
well, they're not really against China.
That's just a happy coincidence.
So how are they going to do it?
They're going to strengthen democratic institutions,
improve standards for human rights, labor,
and anti-corruption activities, monitor natural resources,
and extractive industries.
You knew that was going to be in there.
And that's the plainly stated strategy.
And they're going to use State Department and USA to do this.
That's the carrot.
Just remember, Africa Command was created.
There's a stick there as well.
Now, if you want to know what these types of influence
operations really look like and get in-depth on it,
over on the other channel, The Roads with Beau,
there is a video titled The Roads to Ukraine.
And it goes through Russian and US influence operations
in the country since 1991.
It appears that the United States is going
to run the exact same template, which
is to create de facto agents of influence.
They're going to create these democratic institutions.
They're going to elevate cultural leaders who
are in favor of the West.
It appears, at least from what we've seen so far,
the United States intends on using soft power
to establish this sphere of influence, which is, I mean,
it's still establishing a sphere of influence,
but it's better for it to be done with soft power
than a gun, I guess.
So if you want to know what it looks like, go there.
Because the idea is they elevate cultural icons
who are friendly to US interests.
Therefore, those people get more of a platform.
Therefore, they influence thought in the country.
Therefore, the country naturally draws closer
to the United States and away from Russia
and, coincidentally, China.
So that looks like the game plan, which, I mean,
it is still maintaining a US sphere of influence.
But I have to be honest, after the creation of Africa Command,
I don't want to say I'm happy about it, but I'm relieved
about that.
Generally speaking, these types of operations
are not horrible for the countries that they're run in.
At the same time, remember, this is the carrot.
There's always the stick that goes along with it.
And that's something that we definitely
have to remember here.
The United States is committed to this course.
In fact, in this bill, I want to say
Congress is asking for reports for the next five years
on how it's going to run.
So this is going to be a long-term foreign policy
strategy that the US is going to try to implement.
And it is.
It's going to look very similar to the campaign in Ukraine.
It's going to be an ad buy.
It's going to be a massive advertising campaign saying,
yay, United States in the West.
You don't really want to have anything
to do with Russia or China.
I mean, coincidentally, China.
They're not actually listed in the act.
But that's going to be the goal.
So the current situation, Russian aggression in Europe,
gave the United States a perfect pretext
to do what they were going to do anyway.
I mean, we have been talking about this on this channel
for literally years.
We knew this was coming.
But they gave them a way to do it and make it seem as though
it's all about protecting Africa.
And you have to get into the details to realize
it's about maintaining US influence
and monitoring natural resources and extractive industries.
So we've been talking about it so long
that I just felt like this was something that should definitely
be covered because it's no longer a hypothetical of me
saying, hey, Africa is the new hotspot.
This passes.
Africa is the new hotspot right then.
The United States will be dumping tons of resources
into influencing the events on that continent.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}