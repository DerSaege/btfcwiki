---
title: Let's talk about George Bush, Iraq, Ukraine, and mirrors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x-Vd8RPWdA4) |
| Published | 2022/05/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Compares viewing events in other countries to using a mirror instead of a cartoon villain lens.
- Points out similarities between actions taken by the United States and actions condemned in other countries.
- Mentions Russia's invasion of Ukraine as an example.
- Talks about holding up a mirror through videos to show the reflection of actions.
- Emphasizes subtlety in conveying the message so that truth is realized rather than told.
- Expresses the challenge of getting the people behind US actions to admit the similarities.
- References former President George W. Bush's slip-up in mentioning Iraq instead of Ukraine, hinting at a Freudian slip.
- Notes the significance of Bush's slip in reflecting the mirror image.
- Draws parallels between propaganda tactics used by different countries to manufacture consent for wars.
- Stresses the repetition of similar stories and justifications in different conflicts due to their effectiveness in manipulating public opinion.

### Quotes

- "Truth isn't told, it's realized."
- "The aggressing country, the invading country, they have to invade that other country because there's that really bad group."
- "Truth is always the first casualty of war."
- "His mistake probably far more effective than any video that I could put together."
- "But it might change the next one."

### Oneliner

Beau compares international actions to a mirror, reflecting on the similarities and propaganda tactics used to manufacture consent for wars.

### Audience

Global citizens

### On-the-ground actions from transcript

- Share George W. Bush's slip-up video to raise awareness (suggested)
- Encourage others to watch and share the video to prompt realization (suggested)

### Whats missing in summary

Deeper insights into the manipulation of public opinion through propaganda and the importance of recognizing patterns in international conflicts.

### Tags

#USActions #Propaganda #ManufacturingConsent #Wars #PublicOpinion


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about holding up a mirror to the United States
and how that works.
You know, when you're looking at events in other countries or actions that other
countries take,
a lot of times we view it through this cartoon villain lens,
when
a more appropriate lens would be a mirror.
Because a lot of times the actions are very similar
to actions that the United States undertakes. Actions that
we condemn
other countries for taking,
they're very much
what the U.S. does
on the international scene.
A good example of this
was Russia's invasion of Ukraine.
And if you were watching
the coverage in the beginning,
there were a number of videos where I talked about that, holding up a mirror
and seeing the reflection.
And I was not liking it.
Now,
these videos, I tried to make them as subtle as possible.
To me, this is one of those things, if you just hammer it in,
then it won't really be
acknowledged. This is one of those things that
the truth isn't told, it's realized.
So,
there's a lot of subtlety to it.
But I talked about that mirror image
a whole lot,
and I agonized over every point
that I was going to make when I was sitting there thinking about how to do the videos.
Trying to find
the right ones
that would
reach
and resonate
but wouldn't be
too
overt.
And I had to do that
because it's not like
the people behind
the US actions
would ever just come out and admit it.
Right?
I mean, we live in an echo chamber
inside of our country.
Most countries are like that.
And because of the propaganda and the way we view other nations,
they have no need to just come out and say
it is a mirror image.
You can't count on that.
That would never happen.
So last night,
footage surfaced
of former president George W. Bush,
in which he said,
the decision of one man
to watch a wholly unjustified and brutal invasion of Iraq,
I mean Ukraine.
That's what he said. I do believe that people might call that a
Freudian slip.
Probably
the most poignant Freudian slip in American history.
That
mirror image, you're certainly going to see that clip played today.
And the mirror image that it shows,
it's pronounced.
And it's also something that might help people
realize how much of the Russian framing
is
propaganda.
Because
in the modern world,
manufacturing consent for wars,
it's the same playbook
for every country.
So
the aggressing country, the invading country, they have to invade that other
country
because
there's that really bad group.
It's not all of the country,
but they're there.
And
the invading country has to get rid of them.
Are we talking about Iraq or Ukraine?
Both. You know what? They might have
weapons of mass destruction. They might have stuff they're not supposed to have.
Is it Iraq
or Ukraine?
Those elections, they're not real.
Iraq
or Ukraine?
You know what?
If we don't act,
they might invade somewhere else.
Is it Iraq
or Ukraine?
Now, I'm not actually comparing the situations. They were very different.
I'm not comparing Zelensky and Hussein.
But
I'm trying to point out that the framing,
what gets sold
through the propaganda,
in that little echo chamber,
it's pretty much always the same.
It's the same stories.
It's the same justifications,
minor tweaks.
And they use them because it works.
That's how they can manufacture consent.
The situations were different,
but the stories weren't.
And the reason the stories weren't
is because they work.
When you're talking about pretexts for war,
you have to remember that they're pretexts.
Whether or not it's true, that doesn't even factor into it.
Truth is always the first casualty
of war.
George Bush
saying this offhandedly,
correcting himself immediately, and then being like, look, I'm 75,
it
hopefully
held that mirror image up.
His mistake
probably far more effective
than any video that I could put together.
When you see that today,
maybe share it.
Maybe make sure other people see it.
Because again, this isn't something that you can
just hammer into people.
The propaganda is too thick.
They have to realize it
on their own.
And if you can put that video in front of them,
they might do it.
It won't change
what happened in Iraq.
It won't change what's happening in Ukraine.
But it might change the next one.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}