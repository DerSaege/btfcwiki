---
title: Let's talk about baby formula and Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hwuDsv2Nwls) |
| Published | 2022/05/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the shortage of baby formula in the United States caused by a plant shutdown and Trump-era trade policies.
- Republicans have been criticizing Biden for not doing enough about the baby formula shortage, despite having the power to propose legislation themselves.
- The Democratic Party introduced a bill to provide $28 million for FDA inspectors to address the formula shortage, but 192 Republicans in the House voted against it.
- Republicans seem more focused on making the American people suffer to blame Biden than on finding solutions.
- Biden has taken actions like directing the military to fly in formula and using the DPA to expedite ingredients to address the shortage.
- Beau questions why Republicans voted against the bill given the relatively small amount of money involved compared to campaign costs.
- Criticizes politicians for spending more on campaigns than on ensuring children have food.
- Points out the irony of passing legislation to restrict reproductive rights while neglecting efforts to provide safe food for children.

### Quotes

- "They'll spend more to make sure they get to sit in that seat than they will to make sure kids have food in this country."
- "But they're not going to put any effort behind making sure that child has safe food."
- "That doesn't seem very pro-life to me."

### Oneliner

Republicans block bill to address baby formula shortage, prioritizing politics over children's well-being.

### Audience

Legislative advocates

### On-the-ground actions from transcript

- Contact your representatives to advocate for legislation addressing issues like the baby formula shortage (implied).

### Whats missing in summary

The emotional impact of prioritizing political gains over the well-being of children. 

### Tags

#BabyFormulaShortage #Legislation #PoliticalPriorities #ChildWelfare #Biden #Republicans


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about baby formula
and a bill in the House of Representatives
that did pass, but we're going to talk about how it passed.
If you don't know, there's a shortage in the United States of baby formula.
This stems from two things. One, a plant being shut down
and Trump-era trade policies. That's the cause
of the shortage. Mainly, to be clear, mainly the plant being shut down.
For the last week, Republicans have been all over the news
talking about how Biden isn't doing enough. Biden isn't doing enough.
They're legislators. They actually have the
ability to propose legislation to address any issues
facing the country. It's quite literally their job.
The Democratic Party introduced a bill that would provide $28 million
to make sure that the FDA had inspectors
to get plants opened up and to inspect
formula coming in from overseas. 192 Republicans
in the House voted against it.
Not just will they not come up with solutions of their own,
they will do anything they can to block solutions that
somebody else presents, because their goal right now
is to make the American people suffer so they blame Biden.
Biden, for his part, has directed the military to fly
in formula from overseas
and has used the DPA to
kind of fast track ingredients to get to the plants.
I'm not really sure about that one, to be honest.
I was unaware that that was an issue,
but maybe they're looking downstream. We have supply chain issues.
Maybe they're trying to solve a problem before it starts.
So Biden has done more than I think they are aware of.
And the legislation that was proposed to make sure that
the formula can get to the shelves, well, they voted against it.
They have no problem being on the news every night ranting and raving.
But I guess drawing up legislation is just a step too far for them.
And it's $28 million.
They're talking about this as if it's a huge amount of money.
Why don't you look to see how much a Senate campaign costs?
In a competitive state, most times it's more than $28 million
when you take the two candidates.
They'll spend more to make sure they get to sit in that seat
than they will to make sure kids have food in this country.
They'll spend a lot of time passing legislation and installing judges
that'll make sure that you have to have that kid.
But they're not going to put any effort behind
making sure that that child has safe food.
That doesn't seem very pro-life to me.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}