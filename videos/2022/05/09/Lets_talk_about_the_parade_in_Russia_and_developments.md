---
title: Let's talk about the parade in Russia and developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=atmY8RDPkYw) |
| Published | 2022/05/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- May 9th parades in Russia are a significant source of national pride but speculation about potential announcements during this year's parade did not materialize.
- Speculation ranged from full mobilization to a formal declaration of war, but ultimately, nothing major happened except for the parade and Putin's speech framing the invasion of Ukraine as defensive.
- The media's readiness for the conflict to end contributes to the spread of speculation as there are no dynamic developments drawing viewers.
- Lack of on-the-ground sources leads to speculation spreading rapidly, indicating a need for caution regarding media reports.
- Media outlets reporting on speculation in the absence of actual developments may lead to unnecessary alarm and should be approached with skepticism.
- Beau advises against being swayed by sensationalist reporting and urges to remain calm and composed amidst uncertain situations.
- The visualization of 11,000 troops in the Moscow parade serves as a stark reminder of the human cost of war, being fewer than the lowest estimates of Russian troop losses in the conflict.
- Beau underscores the importance of recognizing the human toll of conflicts like these, particularly on civilians who often bear the brunt of the consequences.

### Quotes

- "Don't let talking heads convince you that something really, really bad is right around the corner because there's going to be a parade."
- "Keep calm and carry on type of thing right now."
- "When you see that many people and you visualize that and all of the families that were impacted, it's a good tool to visualize the human cost of wars like this."

### Oneliner

May 9th parade in Russia sparks speculation but ends with Putin's speech on Ukraine, reminding of the human cost of conflict.

### Audience

Media consumers

### On-the-ground actions from transcript

- Visualize and acknowledge the human cost of war by reflecting on the impact on families and communities (implied).

### Whats missing in summary

The emotional impact of witnessing the parade and its reminders of human loss can be best understood by engaging with the full transcript. 

### Tags

#Russia #May9thParade #Speculation #Putin #HumanCost #MediaReporting


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the parade in Russia
and what happened at it and what didn't
and why there was so much talk about it,
so on and so forth.
And we're gonna kinda go through it
and there is one visualization
that I wanna make sure everybody gets.
So the May 9th parades that occur in Russia,
they're a big deal.
They're a big deal.
It's a huge source of national pride.
Because of this, there was a lot of speculation about things that Putin might announce during
this year's parade.
And that speculation could be found in the media, it could be found all over the web,
and it ranged from full mobilization to a formal declaration of war to the use of the
unthinkable at the tactical level, all sorts of things. The thing was it was
just speculation. It was speculation. What occurred? Nothing. They had a parade.
They had a parade. Putin did give a speech and in that speech he tried to
frame the invasion of Ukraine as a defensive act. Also used some terminology
that again suggested that it was Russian soil, that kind of indicating that Ukraine didn't
have legitimacy to actually be a country.
That's kind of noteworthy, but it's not new.
There was no major development that occurred.
So why does speculation exist?
Why did people latch on to this idea that there was going to be a major announcement?
The real reason is the media is ready for this to be over.
It isn't dynamic.
Because the lines are static right now, there's not anything that's giving them huge ratings.
There's nothing that's drawing the viewer in, so they are latching on to speculation
and running with it.
problem is the lack of sources that are on the ground that most media outlets
have. They're not talking to a lot of people who are there who are in the no.
So once the speculation starts, it just spreads. It's something to definitely be
aware of as we move forward. The media is ready for there to be a development. And
And in lieu of an actual development,
they will report on speculation.
Be ready for that.
Don't let it inflame you.
Don't let it worry you.
Keep calm and carry on type of thing right now.
Don't let talking heads convince you
that something really, really bad is right around the corner
because there's going to be a parade.
Now, the one thing that I want everybody to visualize,
especially if you watch the parade or any portion of it,
if you watch it, you see this giant, long line of troops.
You see all of this equipment rolling by.
The Russian sources say that 11,000 troops
participated in the one in Moscow, which is the one
that Putin spoke at.
And the reason that's important is
because you can see a visualization.
Understand that 11,000 troops is less than the low estimates
of how many troops Russia has lost thus far.
So when you see that long line, when
you see all of those people, those
are people who aren't here anymore because
of this failed adventure.
And that's just on the Russian side.
There are Ukrainian troop losses, and then the biggest
number is going to be Ukrainian civilian losses,
because in conflicts, civilians always pay the highest price.
When you see that many people and you visualize that and all
of the families that were impacted, it's a good tool to
visualize the human cost of wars like this.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}