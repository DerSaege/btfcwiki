---
title: Let's talk about community networks and what you can do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kQWp5xRelI0) |
| Published | 2022/05/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about community networking and addresses the common question of "What can I do?" when it comes to helping out.
- Uses Hurricane Michael relief efforts as an example to demonstrate the power of community networks in disaster relief.
- Describes how people from various networks came together to provide aid after the hurricane hit Panama City.
- Explains the different roles individuals played in the relief efforts, from collecting supplies to monitoring social media for requests and road updates.
- Emphasizes the importance of utilizing one's skills and resources in crisis situations.
- Acknowledges the valuable contributions of individuals who may not physically participate but support through other means like donations.
- Stresses the significance of multiple networks collaborating to achieve greater impact in addressing challenges.
- Encourages individuals to recognize their skills and find ways to contribute to making things better for everyone, regardless of their abilities.
- Mentions the necessity of local networks in mitigating harm and solving problems faced by communities.
- Concludes by affirming that everyone's skill set is valuable and needed in community efforts.

### Quotes

- "Your skill, whatever it is, is needed."
- "It's all a matter of applying what you're good at to making things better for everybody."
- "The solution to the problems that we're going to be facing has centered where you're at."
- "There's always a way you can help."
- "What can I do? And it's specific."

### Oneliner

Beau shares insights on community networking using Hurricane Michael relief efforts to illustrate the impact of individual contributions and the necessity of leveraging diverse skills in crisis situations.

### Audience

Community members

### On-the-ground actions from transcript

- Join local community organizations to build networks and support disaster relief efforts (exemplified)
- Organize collection drives for supplies in preparation for future emergencies (suggested)
- Monitor social media for real-time needs and updates during crises (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of how diverse skills and resources can be utilized in community networking during disaster relief efforts.

### Tags

#CommunityNetworking #DisasterRelief #CommunitySupport #SkillsUtilization #Collaboration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk a little bit more
about community networking.
And we're going to focus on a question that always comes up
whenever I talk about this.
It's normally the first question I get from somebody
after I'm gone.
What can I do?
And it's specific.
The person is literally asking, what
can I, me personally, what can I do?
And they always have a reason that they think
they wouldn't be valuable.
It took me a long time to figure out
what people were actually asking when they asked this question.
Because I'm always like, I don't know, what can you do?
What's your skill set?
But it has to do with the perception of how
this stuff works.
And it has a lot to do with that spear analogy.
If you haven't seen that video, I'll put it down below.
It's very transferable to this topic.
What we're going to do is we're going to talk about disaster
relief after Hurricane Michael.
Because it's the perfect example of showing what can I do,
and also shows exactly how powerful these networks can be.
So Hurricane Michael hit Panama City pretty bad.
If you don't know, it was a bad hurricane.
I've been through a lot.
And wow, that was one of the first ones where I was like,
man, this might have been a mistake.
And we're pretty far inland.
So after the hurricane cleared my driveway,
and we go and start helping down in Panama City.
Because our electricity, all that stuff, it's coming back up.
But down in Panama City, they're in a bad way.
So what people remember about the relief efforts that
weren't National Guard, they remember people like me
and my little circle showing up, chainsaws in hand,
cutting people out of their houses,
clearing the roads, delivering supplies.
That's what people picture.
But that's just the tip of the spear.
That's just the people who were there doing the final part.
And that's the part that people normally recognize.
And that's normally what they associate with this.
So what we're going to do is we're
going to go backward from there.
We're dropping off supplies.
Where did we get them?
From these little collection points
that sprung up outside of Panama City,
outside of the impacted area.
You had nonprofits, you had churches,
you had community centers, you had private individuals
using their land as basically a warehouse.
So supplies could be picked up from there
and taken in to the impacted area.
Where'd those supplies come from?
The supplies that were there, how'd they get there?
Because it certainly didn't come from the government,
at least not in the beginning.
I know that some of them came from as far away as Ohio
because I met a guy in a giant rental truck that
had driven canned goods and diapers and stuff like that
down.
There was a group in Tennessee that
put together these little buckets
full of cleaning supplies and they had lines to dry clothes
with and stuff like that.
All of this stuff came from other networks.
The little collection points, those
were somebody's network.
They may not think of themselves as a community network,
but when the need arose, they became one.
The supplies came in from little networks
all across the country.
At least the eastern side of the country.
Some of them were churches, some of them were aid groups,
some of them were just individuals, neighborhoods
that got together and sent the supplies down.
So the supplies get there from all over the country.
They get to the collection point and I show up
and pick it up, right?
But how do I know where to go?
There were a whole bunch of them,
but one of the ones that I like to go to the most
had a woman in a wheelchair who spent all day sitting there
monitoring social media because it was intermittent,
but people could get on their phone and say,
hey, we need this at this address or on this street.
And she was monitoring that.
She was taking the phone calls.
Most importantly, the thing that helped us the most
was she was keeping track of which roads got cleared
so we knew how to get from point A to point B.
Because, I mean, most of the people in my little circle,
the quickest way to get from point A to point B
is a straight line.
And we would have cut through stuff
when there was another route, which, yeah, eventually it
has to get cleared, but these people are waiting on stuff.
And that information helped speed that process along.
There wasn't any skill that didn't get used during that.
And if you think about it, all the way up in Ohio or Tennessee,
there were probably people printing out flyers,
putting them out, letting people know that there was a collection
drive to get the stuff down here.
It's all a matter of what skills you have
and how you can use them in the situation at hand.
The people who showed up, those are the people
who were remembered, right?
But the reality is we couldn't have done any of that.
We couldn't have done any of that
without the people further back down the spear.
Because we wouldn't have had the supplies.
We wouldn't have known where to go.
And there's no way that one person can do it all.
And if it was just our little network
and we tried to divide up all of those tasks amongst ourselves,
sure, we could have done it, but it would have been a lot harder.
And the better use for people like the people in my network
is to send them into the middle of things that aren't so great.
There's always a way you can help.
Your skill, whatever it is, is needed.
And as the United States goes the direction
it certainly appears to be going, it's all going to be needed.
And it doesn't matter what it is you're good at.
It's a matter of applying what you're good at
to making things better for everybody.
That is without a doubt the most common question I get.
When this topic comes up, it's always,
well, you know, I don't really know how to do any of this.
Okay, you don't have to know how to run a chainsaw.
Do you know how to monitor a hashtag?
Because that was super useful.
Can you run a highlighter over a map?
This stuff matters.
It's not what people picture.
It's not the photos that end up in the newspaper.
But it's just as, if not more, important.
If you're somebody who is older or for whatever reason
can't be out there doing something active,
understand there's definitely a place for you.
If you don't have time, in fact, there's somebody
in our little circle who really doesn't have much time
to contribute, but they'll always cut a check
if we need something.
It's the same thing.
What your skills are, what your gifts are,
they contribute to this.
That little network of people can accomplish a lot.
And when you have multiple networks working together,
I mean, you can dramatically affect the outcome of something.
And we're going to need to start doing that.
The solution to the problems that we're going to be facing
has centered where you're at.
It's not in DC.
Yeah, eventually that's where we have to get to.
But right now, you're going to have to mitigate harm.
And these networks can help do it.
And make no mistake about it, whatever your skill set is,
it's needed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}