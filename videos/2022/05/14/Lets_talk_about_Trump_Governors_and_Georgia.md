---
title: Let's talk about Trump, Governors, and Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uvzXsI3NqpU) |
| Published | 2022/05/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is endorsing candidates, focusing on Georgia to control the state-level Republican Party apparatus.
- Despite media hype about Trump's endorsements, his candidate lost in Ohio and Nebraska.
- In Georgia, Trump's endorsed candidate is trailing the incumbent, reflecting potential waning influence.
- In West Virginia, two incumbent Republican congressional reps faced off due to district mergers.
- Trump's endorsement in West Virginia primary didn't guarantee a win, as it boiled down to turnout.
- Reports suggest Trump's name may be toxic among some Republicans, leading independents to vote against his endorsed candidate in Georgia.
- Influence of Trump's endorsements may not be as significant as media portrays, especially in major races like Georgia.

### Quotes

- "Trump's endorsement may not be what is deciding a lot of these races."
- "Trump's name, despite the media coverage, may not be carrying the influence that that coverage suggests."
- "His name is so toxic among a large segment of the normal Republican party."
- "As you hear more and more reporting talking about Trump's status and his endorsements and how influential he is, I would wait to see what happens in Georgia first."

### Oneliner

Trump's endorsements may not hold as much sway as media suggests, with Georgia's primary results being a key indicator of his influence.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Monitor the outcome of the Georgia primary elections to gauge the true impact of Trump's endorsements (suggested).

### Whats missing in summary

Insight into the potential implications of Trump's endorsement strategy beyond the immediate election outcomes.

### Tags

#Trump #Endorsements #Georgia #RepublicanParty #Influence


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about
the power of Trump's endorsement
and the lack thereof in major races.
Trump is endorsing a lot of candidates
and is paying particular attention to the state of Georgia.
The Trump machine really wants to seize control
of the state-level apparatus of the Republican Party
In order to do that, they need the governors.
The thing is, as much as the media is talking
about Trump's endorsement and his status
as a kingmaker and all of that stuff,
his candidate lost in Ohio and in Nebraska.
And when it comes to Georgia,
where Trump's wrath is just really thick
because he wants to get even
because they wouldn't play ball and find those votes,
the incumbent is leading Trump's endorsed candidate.
Trump's endorsement may not be
what is deciding a lot of these races.
The media right now is making a big deal
about what happened up in West Virginia.
There were two, there was a primary
between two congressional reps there.
The thing is, they were both incumbents.
Let's see, because of the stellar leadership of the Republican Party in West Virginia,
so many people are leaving the state that it's losing seats.
So two districts got merged and they were running against each other.
And Trump picked one of them, picked one that didn't engage in any bipartisan activity.
So I mean, he was kind of 50-50 there anyway.
That really just came down to who turned out.
That wasn't Trump doing a come from behind, endorsing somebody and making them win.
Trump's name, despite the media coverage, may not be carrying the influence that that
coverage suggests.
In fact, it may be doing the opposite in the big races, because there is now reporting
that in Georgia, there are independents who are going in to vote in the Republican primary
to make sure that his endorsed candidate loses because his name is so toxic among a large
segment of the normal Republican party.
The lead up to the Georgia primary, which is the one that Trump is really all about,
isn't voting well for him.
So as you hear more and more reporting talking about Trump's status and his endorsements
and how influential he is, I would wait to see what happens in Georgia first.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}