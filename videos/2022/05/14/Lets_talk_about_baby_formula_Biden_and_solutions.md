---
title: Let's talk about baby formula, Biden, and solutions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R7vVDUfqbOY) |
| Published | 2022/05/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the controversy surrounding the shortage of baby formula and criticism towards Biden for not doing enough.
- Criticizing the idea of government intervention in directing corporations on what to do as a merger of state and corporate power.
- Drawing parallels between advocating for government control over corporations and fascism, mentioning Benito Mussolini and corporatism.
- Pointing out that members of Congress should be introducing legislation to address issues like the baby formula shortage instead of looking to the White House for solutions.
- Suggesting that breaking up monopolies to prevent widespread issues like a recall impacting half the country could be a solution.
- Expressing concern over the trend of people looking to the White House as the ultimate solution to all problems, including inflation.
- Questioning the actions of governors and members of Congress who are quick to ask what the White House is doing about inflation without taking steps within their own states to address the issue.
- Warning against the dangers of constantly relying on the White House to solve every problem, leading to excessive control by one person.

### Quotes

- "You're literally advocating for fascism just to throw that out there."
- "They're not supposed to be looking to the White House to have dear leader tell them what to do."
- "We have to stop looking to the White House to solve every single problem that comes along."
- "It's not a good system."
- "Y'all have a good day."

### Oneliner

Beau addresses the baby formula shortage controversy, criticizes government intervention as a merger of state and corporate power, and warns against excessive reliance on the White House for solutions.

### Audience

American citizens

### On-the-ground actions from transcript

- Advocate for legislation to address issues like the baby formula shortage (implied).
- Support efforts to break up monopolies that control significant portions of the market (implied).
- Take steps within your own state to help with issues like inflation, such as reducing gas taxes (implied).

### Whats missing in summary

The nuances and detailed explanations provided by Beau in the full transcript.

### Tags

#BabyFormulaShortage #GovernmentIntervention #CorporatePower #BreakUpMonopolies #WhiteHouseReliance


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about baby formula
and solutions and quotes,
because it's the hot topic right now.
You have all kinds of powerful people
tweeting and posting and making videos or ads,
all saying Biden hasn't done anything
about the baby formula issue.
What is he going to do about the baby formula issue?
If you don't know, there's a shortage of baby formula.
And there have been some very unique takes about it,
but the overriding theme is that Biden isn't doing enough.
What are you asking for when you ask Biden
to direct from on high what corporations should do?
You want the government to tell companies what to do.
You want a merger of the state and corporate power.
The merger of state and corporate power.
Just go ahead and type that in
to your favorite search engine and hit enter.
Yeah, there was an Italian guy named Benito
who talked about this a lot.
You're literally advocating for fascism
just to throw that out there.
Corporatism was a cornerstone of the Italian brand of it.
That's how it functioned.
And the way it would work with the state
from the dictator guy up top,
well, he would send his directives down
and then the little corporate guilds would enact it,
which is exactly what people are advocating for.
You may not realize it.
You may not have known
that that's what you were advocating for
because it's sneaking in under the radar.
They've dressed it up.
You know, it's coming in carrying an American flag
and holding the Bible.
Go ahead and Google that, too.
There's one other piece to this.
The people who are screaming the most
about a baby formula shortage, who are they?
What are their titles?
Member, House of Representatives, Senator.
You know, people who in the American system of government,
you know, when you're not talking about fascism,
here under this system of government,
these are the people who are supposed
to be introducing legislation to fix problems like this.
They're not supposed to be looking to the White House
to have dear leader tell them what to do.
Now, I don't know if these people from Congress,
if they don't know what their job is,
if they themselves have already been trained
to look to the White House as some ultimate power,
or they do know that it's not the White House
who should solve this,
and they're just trying to train us common folk
to look for one person to be in charge
and for that one person to have control of everything.
See, the funny thing is, the solution to this,
if you really look at it under the American system
and capitalism in general,
when you're looking at what caused this issue,
it seems like the obvious answer would be to break up
the company that controls so much of the market
so a recall doesn't impact half the country.
You know, breaking up that monopoly,
that would probably be the solution.
Don't worry if you work for that company,
you don't have anything to worry about
because they certainly don't want to reduce corporate power.
They want to merge with it because it's the goal.
It's where they're headed.
Whether they realize this is what they're advocating for
or not, every time they tweet,
every time they post, every video they make,
it's one of the characteristics.
It's, in this case, literally the definition
that Benito gave.
Well, probably not Benito's, probably his ghostwriter.
But anyway, that should be a little concerning.
The White House isn't the solution
to all of your problems.
You have a bunch of governors asking
what the White House is going to do about inflation.
You have a bunch of members of the House of Representatives
and senators asking the same question,
even though they all know it's a global thing
happening all over the world.
They know this because they understand
the power of corporations
and they're heavily invested in them
and do their bidding almost like they've kind of merged with them.
But they're asking that question.
How many of those governors have taken any steps
within their own state to help with inflation?
I don't know.
Reduction of the gas tax,
something that everybody's talking about as a marker,
the gas prices.
A lot of states have a tax on it that could be waived
to help reduce things.
They're not looking for a solution
because they're not interested in leading.
They want a leader.
They want dear leader.
We have to stop looking to the White House
to solve every single problem that comes along
because that's going to lead us to a situation
where everything is controlled by the White House,
by one person.
And even if Joe Biden happens to be your man,
you may not like the next person.
It's not a good system.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}