---
title: Let's talk about Starbucks, unions, and you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ht0PULmvL9I) |
| Published | 2022/05/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unions are traditionally associated with blue-collar jobs like coal miners and electricians, but the service industry is seeing a rise in unionization.
- There's a misconception that unions only thrive in progressive areas, but recent events like Starbucks in Birmingham, Alabama, voting to unionize, challenge this idea.
- Unions are not limited to blue-collar workers; they are for any worker who needs representation in a hostile environment.
- The process of unionizing is not as difficult as it may seem, despite pressure from businesses against it.
- The National Labor Relations Board outlines the process for forming a union, including holding elections if 30% of workers express interest in unionizing.
- Businesses often resist unions, but collective bargaining through unions gives workers more power and representation.
- Unions are effective, as evidenced by the significant efforts businesses put into defeating them.
- If you're in a workplace where you're not treated well and lack necessary benefits, a union might be the solution.
- There are networks available to help workers navigate the unionization process, making it more accessible than perceived.
- Considering joining a union can empower workers and improve their working conditions.

### Quotes

- "Unions aren't just for blue-collar workers. Unions are for any worker where the boss is not really looking out for them."
- "Divided we beg, united we bargain."
- "If you're in a workplace and you're not treated well, a union might be the thing for you."
- "If unions weren't effective, if they didn't work, big business wouldn't spend so much money trying to defeat them."
- "There are networks that can help you do it."

### Oneliner

Unions are not just for blue-collar workers; they empower all workers in hostile environments, providing representation and collective bargaining power to improve working conditions and rights.

### Audience

Workers seeking empowerment

### On-the-ground actions from transcript

- Contact networks that assist with unionizing (suggested)
- Join a union or start the process of forming one (implied)

### Whats missing in summary

The full transcript provides detailed insights into the importance of unions, the process of unionizing, and how they empower workers in various industries.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're gonna talk about unions
and Starbucks and you.
When you think about unions,
you generally think of blue collar jobs.
You think of coal miners.
You think of welders.
You think of electricians.
You think of stuff like that, right?
You generally don't think of the service industry.
typically, but that's changing, and it's changing quickly.
There's been a wave of service industry related jobs
that have unionized around the country.
However, there's still a very prevalent attitude
that that really only happens in progressive areas,
and that's why they get through.
That's why those places, those Starbucks,
that's why they're unionized.
The Starbucks in Birmingham, Alabama
just voted to unionize.
Alabama is not really known as a bastion
of progressive thought, but it succeeded there.
Unions aren't just for blue collar workers.
Unions are for any worker where the boss is not really
looking out for them.
It's for any worker that needs representation.
It's for any worker that's in a hostile environment.
That's what unions are for.
And unionizing, it isn't really as difficult
as it may seem at times.
It seems difficult because you're
getting pressure from that business not to unionize.
There has been a pretty strong network
that is being created to help people that
trying to unionize, and it's not difficult. Over on the National Labor
Relations Board website, your right to form a union, not represented by a union
but want to be, if a majority of workers wants to form a union, they can select a
union in one of two ways. If at least 30% of workers sign cards or a petition
saying they want a union, the NLRB will conduct an election. If a majority of
those who vote choose the union, the NLRB will certify the union as your
representative for collective bargaining. An election is not the only way a union
can become your representative. And then it goes into situations in which the
business voluntarily says yeah okay this is the union. That normally doesn't
happen. Businesses put a lot of effort into defeating unions and you need to
think about why. Without that union, you don't have the representation. It's that
slogan. You know, divided we beg, united we bargain, that type of thing. When you
have collective bargaining, you have more power as an employee, as one of the
workers. If unions weren't effective, if they didn't work, big business wouldn't
spend so much money trying to defeat them. If you're in a workplace and you're
not treated well, you don't have the things that you feel you should because
you work in an industry or at a location that we as society have decided, oh we
really want that job performed, but we also want to be able to look down at the
people who do it. If you work in one of those jobs, a union might be the thing
for you. And it's not really that hard. There are networks that can help you do
it. So it might be something to think about. It might be a place where you can
active. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}