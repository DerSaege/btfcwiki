---
title: Let's talk about a comprehensive gun control strategy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SwOVUt7myLU) |
| Published | 2022/05/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Proposes a comprehensive plan to address mass incidents in the United States by setting aside ideological hangups.
- Suggests raising the age to 21 for firearm access and assigning criminal liability for unsupervised access to firearms under 21.
- Advocates for closing all domestic violence loopholes, starting with the boyfriend loophole.
- Recommends temporary prohibitions for any violent conviction and permanent prohibition for hurting animals.
- Emphasizes behavior or age-based solutions that have traditionally been upheld by the Supreme Court.
- Calls for triggering a cultural shift within the gun crowd by banning military-tied advertisements and reaching out to Hollywood.
- Proposes a campaign against misogyny and racism to address cultural factors linked to mass incidents.
- Stresses the importance of better social safety nets and Medicare for All with mental health care to relieve economic stressors and provide necessary support.
- Advocates for a multi-pronged approach that limits access for individuals displaying concerning behaviors without infringing on Second Amendment rights.

### Quotes
- "Start there. Think about the way certain products can't be advertised to kids. Eliminate that tie."
- "Come out for six weeks, or eight weeks, or whatever. Do you want to be a warrior, or a wannabe?"
- "This might be the way to go."

### Oneliner
Beau proposes a comprehensive plan to address mass incidents in the US, focusing on age-based restrictions, cultural shifts, and social support.

### Audience
Legislators, Gun Advocates

### On-the-ground actions from transcript
- Contact legislators to advocate for raising the age for firearm access to 21 (suggested).
- Join or support campaigns against misogyny and racism in schools and communities (suggested).
- Organize or participate in initiatives to improve social safety nets and access to mental health care (suggested).

### Whats missing in summary
Detailed examples on how to implement the proposed plan effectively.

### Tags
#GunControl #YouthSafety #CulturalShift #MentalHealth #Legislation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to conduct a little thought exercise,
do a little pretending, a little imagining.
And we're going to try to come up
with a step-by-step comprehensive plan
to address one of the larger problems in the United States.
We're going to do this because somebody sent me a message
saying, hey, you give us thought exercises all the time.
Here's one for you.
Why don't you set your ideological hangups aside
and try to help us come up with a plan,
a comprehensive step-by-step plan to address mass incidents.
OK.
I added a couple of additional criteria.
One, it's got to be something I think will work.
Two, it needs to be something that
can survive the Supreme Court.
And three, it has to be within the Overton window of the United
States.
My version of utopia is different than most people's.
So it has to be something that would
fit within the mainstream of the United States.
OK.
So first thing, raise the age to 21.
Raise the age to 21.
Now, you're going to get pushed back immediately
because people are going to say, you can enlist in the Army at 18
and go over and fight and not be able.
Yeah, you can carve out an exemption for that.
You can.
Assign criminal liability to those
who would provide unsupervised access to firearms
for those under the age of 21.
And then enhanced liability if the person under 21
does something with them, bad, hurt somebody.
So just allowing it, there's liability there.
And if they do something, you're on the case with them.
For the 900th time on this channel,
close all the DV loopholes.
Start with the boyfriend loophole.
Start there.
But close them all.
And you get a conviction there.
That's permanent prohibition.
The markers are way too significant there.
Temporary prohibitions for any violent conviction.
That little scuffle you got into in the parking lot,
well, now you can't own for three years, seven years,
five years.
Y'all can argue about that in the comments section.
Permanent prohibition for hurting animals.
You start here.
Now, this seems pretty simple stuff, right?
So this would impact almost all mass incidents.
I can't actually think of an exception offhand,
but I mean, I'm sure there is one.
These suggestions here, they're all behavior or age-based.
Two things that the Supreme Court has traditionally upheld.
Nobody's going to look at this and say,
oh, it's an attack on the second.
It's an attack on behavior.
None of this disproportionately impacts either the working
class, people of low means.
It doesn't disproportionately impact marginalized groups,
with maybe the exception of the DD loopholes,
because of the way police charge stuff and go
after different demographics in the US.
And that's something that needs to be addressed as well.
But you're not banning anything.
You're going after age and behavior alone.
But this is only a band-aid.
You also have the real problem, which
is that people want to do this kind of stuff.
So you have to trigger a cultural shift
within the gun crowd.
So you need steps for that.
First, ban all advertisement that ties it to the military.
Start there.
Think about the way certain products
can't be advertised to kids.
Eliminate that tie.
Reach out to Hollywood.
Make the tactical world a subject of ridicule constantly.
Just reach out, explain how important it is that we
shift thought on this issue.
The Department of Defense can provide basic training
with no obligation to serve.
Allow them to come out and go through boot.
Be brought into that warrior culture,
rather than that tactical culture.
Make it not about might makes right, but selfless service.
And you can incentivize it in some way, a tax break,
or a year of college, something.
But you get them out there, and you put them
in situations that test them, that push them a little bit,
and bring the actual warrior ethos into them,
rather than just them having the clothes and the gear.
I think that might help.
The advertising campaign for that
could also tie into the idea of ridiculing the tactical world.
Come out for six weeks, or eight weeks, or whatever.
Do you want to be a warrior, or a wannabe?
Something like that.
You need a campaign against misogyny and racism.
Pretty coordinated one.
You could reach out to Hollywood.
But the same type of stuff.
It needs to be very comprehensive.
It needs to be in schools.
It needs to be everywhere, because those two things have
a lot to do with these mass incidents.
So that's going to help with the cultural shift.
Now, the other thing you want to do
is relieve the pressure on things
that don't necessarily seem to directly relate.
We need better social safety nets.
If you look at the profiles, what you'll find
is that most of them had multiple stressors hit them
at one time, and that's what kind of built up to it.
And often, one of those stressors is economic.
So help relieve it there.
We need that anyway.
But it assists in this problem.
Another is Medicare for All with mental health care.
And make it available.
So you get a multi-pronged approach here
that limits access of those people who
are displaying the behaviors.
So you're not going after everybody.
So you're not going to get this wide pushback.
You're not banning anything.
You're not encroaching on that sacred Second Amendment.
You're helping to shift the culture.
And you're providing the tools to alleviate
some of the stress and perhaps get treatment.
And honestly, this all seems pretty easy.
I mean, none of this seems that hard.
When you compare this to some of the other things that
are suggested, it seems like we should probably try this.
This might be the way to go.
So there's your thought exercise for the day.
If you have other suggestions, put them down below.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}