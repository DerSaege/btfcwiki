---
title: Let's talk about the EU quitting Russian oil....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p0G4KTJCj7E) |
| Published | 2022/05/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The EU is preparing to implement the sixth round of sanctions against Russia, which includes a significant cut in Russian oil imports by 90% by the end of the year.
- This move will involve pushing nearly $10 billion US into the Ukrainian economy to provide support.
- The sanctions also involve cutting Russia's largest bank out of SWIFT and blocking the distribution of content from Russia's major state-owned media companies in the EU.
- Along with targeted sanctions on individuals, assets freezing, and travel restrictions, this round of sanctions aims to impact Russia's economy.
- The reduction in Russian oil imports is a key demand from Zelensky to weaken Russia's economic stability.
- The global impact of these sanctions is not isolated, as it will lead to shifts in oil trading globally.
- There are speculations that Russia might sell its excess oil at a reduced rate to new buyers, potentially impacting gas prices worldwide.
- Despite such speculations, any market instability or change often results in increased prices.
- EU countries are collaborating to facilitate Ukraine's grain export, a critical aspect for Ukraine's food security and global trade.
- Balancing the potential rise in gas prices due to sanctions with the effort to stop the war in Ukraine is a critical consideration for the global community.

### Quotes

- "You have to weigh the increased costs or the driving less against that fact."
- "It may save lives."
- "The food security aspects are very pressing."
- "This will probably drive prices up even further."
- "It's just something to keep in mind the next time you go to the gas pumps."

### Oneliner

The EU implements stringent sanctions on Russia, including a 90% cut in oil imports, aiming to impact its economy while considering global repercussions and Ukraine's food security.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support initiatives that aid Ukraine's economy and food security (suggested)
- Stay informed about the global impact of sanctions and trade shifts (suggested)

### Whats missing in summary

Insights on the long-term implications of the sanctions and the ongoing conflict in Ukraine. 

### Tags

#EU #Sanctions #Russia #Ukraine #GlobalImpact


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the next episode in the saga that is the cat and mouse
game of going after Russia's economy.
The EU, the European Union, has confirmed but hasn't officially enacted their sixth
round of sanctions.
I think it's their sixth.
And this one includes cutting Russian oil imports by 90% by the end of the year.
The remaining 10% has to do with countries that are landlocked.
This package includes almost $10 billion US that will be pushed into the Ukrainian economy
to help keep it rolling.
It cuts Russia's biggest bank out of SWIFT.
It stops Russia's three largest state-owned media companies from distributing their content
within the EU.
And then there's all the normal stuff, you know, more targeted sanctions against people,
freezing assets, stopping travel, that kind of stuff.
The oil is a big deal.
That's something that Zelensky has been asking for.
Obviously it will make it harder for Russia to continue to prop up its economy from the
impacts of the other sanctions.
Now for us and everybody else in the world, we have to remember this stuff doesn't occur
in a vacuum.
You know, they're cutting out this supply of oil, which means they have to get it from
somewhere else.
So they're going to be buying from somewhere else.
Meanwhile, Russia is going to try to find a different buyer for the oil they would normally
ship to Europe.
Now I heard somebody say that because Russia is going to be trying to offload this oil
to a new buyer, they may do it at a cut rate and it may actually help with gas prices.
I am super skeptical of that.
I don't know a whole lot about how these markets work, but the one thing I have noticed is
that pretty much any instability whatsoever or any change whatsoever means that the prices
are going to go up.
So I don't have a whole lot of faith in that assessment, but it was interesting to hear.
Now today, when you're watching this on Tuesday, the same group of EU countries is going to
be trying to figure out how to help get Ukraine's grain to market.
And that's going, I mean, it's already a big deal, but it's going to become more and more
pronounced as time goes on.
That's probably going to be a more important move as far as the rest of the world than
the oil, while the oil is incredibly important to Ukraine.
The food security aspects are very pressing.
So I'm looking forward to hearing about that.
Now when we talk about the impact to the rest of the world, when we're talking about the
oil, I mean, I am of the opinion that this will probably drive prices up even further.
But you have to temper that with understanding what they're trying to do.
I mean, they're trying to stop a war.
So you have to weigh the increased costs or the driving less against that fact.
I mean, it may save lives.
So it's just something to keep in mind the next time you go to the gas pumps and prices
have gone up yet again.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}