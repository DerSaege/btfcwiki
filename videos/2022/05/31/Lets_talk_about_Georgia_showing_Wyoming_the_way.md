---
title: Let's talk about Georgia showing Wyoming the way....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sk3RgpMHmlU) |
| Published | 2022/05/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a political strategy incident in Georgia with potential implications for the Democratic Party in Wyoming.
- Points out the crossover voting from the Democratic primary to the Republican primary in Georgia.
- Mentions how Raffensperger needed the help and avoided a runoff by 27,000 votes.
- Notes that 37,000 people who voted in the Democratic primary previously voted in the Republican primary this time.
- Connects Raffensperger's victory to his refusal to help Trump overturn the election.
- Emphasizes the impact of defeating Trumpism and the importance of participating in selecting representatives.
- Mentions the significance of Trump's endorsements and how defeating Trump in Wyoming could diminish his influence.
- Encourages considering the impact of selecting opposition candidates in the context of the country's authoritarian slide.
- Wraps up by urging listeners to think about the implications of these political dynamics.

### Quotes

- "If defeating Trumpism is important to you and everything that goes along with it, and you live in Wyoming, it's worth noting..."
- "Defeating Trump and his crowd in Wyoming, that's it for the worth of his endorsements. That's it."
- "If you're concerned about the authoritarian slide this country has been on, it's just worth remembering that this is something that happens."
- "Selecting your opposition might be a way to look at it."
- "Y'all have a good day."

### Oneliner

Beau explains a political strategy incident in Georgia with implications for defeating Trumpism and selecting opposition candidates.

### Audience

Wyoming residents

### On-the-ground actions from transcript

- Change your voter registration to participate in selecting representatives (suggested)
- Engage in voting strategically to have an impact on political outcomes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of a political strategy incident in Georgia and underscores the importance of understanding crossover voting and its implications for selecting representatives.


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about a little bit of
political strategy. We're going to talk about something that happened in Georgia, and if you're
a member of the Democratic Party and reside in Wyoming, you probably really want to pay attention
to this. Now for those that don't know, I have a policy. I don't tell people how to vote.
The only candidate I've ever endorsed on this channel was Big Bird running against Ted Cruz.
So just bear that in mind. A couple of weeks before the Georgia primary, I pointed out how it
appeared that members of the Democratic Party might be voting in the Republican primary. They
may decide to cross over vote, which is something you're allowed to do.
Now an AP investigation has determined that yeah, that happened a lot. Now Kemp didn't need the
help. The governor didn't need the help. That was a blowout. That was so embarrassing for Trump,
his candidate losing like that. Funny stuff. Anyway, but Raffensperger needed the help.
He avoided a runoff, I want to say 27,000 votes. According to the AP analysis,
there were 37,000 people who voted in the Democratic primary last time who voted in
the Republican primary this time. Raffensperger, by the way, is the person who wouldn't help Trump
find the votes to overturn the election.
It appears that that really mattered there, that it actually played a pretty big part of it
in defeating Trumpism. If defeating Trumpism is important to you and everything that goes along
with it, and you live in Wyoming, it's worth noting that Trump's status as a kingmaker in
the Republican Party at this point pretty much hinges on his ability to beat Liz Cheney in the
primary, for his candidate to defeat her. Liz Cheney is, she's the Republican Party's mansion
at this point. That's how they look at her. You can, in Wyoming, engage in that type of voting.
You can change your registration. It's just, you know, something that's worth noting that maybe
if you don't have a vested interest in the Democratic primary, maybe you just want to
make sure that you can participate in selecting those people who are going to represent you.
Defeating Trump and his crowd in Wyoming, that's it for the worth of his endorsements.
That's it. It's over. He no longer has that to hold out as a cookie to get people to do
whatever he wants, like hypothetically find the votes.
If you're concerned about the authoritarian slide this country has been on,
it's just worth remembering that this is something that happens, and in some cases, it
has a marked impact. Selecting your opposition might be a way to look at it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}