---
title: Let's talk about if the ban worked....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5ZoYEiui7iM) |
| Published | 2022/05/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Beau questions the effectiveness of the assault weapons ban and why there are conflicting opinions on its success.
- The assault weapons ban did not remove any firearms off the streets; weapons like ARs and AKs continued to be sold during and after the ban.
- The ban focused on cosmetic features of firearms rather than reducing their actual firepower, leading to the same weapons being sold but with minor alterations.
- There was a massive surge in production of rifles, especially ARs, after the ban, with spikes in production coinciding with political events like elections.
- Beau speculates that fear-mongering, social media influence, and a toxic gun culture may contribute to the increasing frequency of incidents involving firearms.
- Despite differing views on gun control, one point of agreement among pro-gun, anti-gun, and firearm experts is that the assault weapons ban failed to achieve its intended goals.

### Quotes
- "How did it not work?"
- "The assault weapons ban didn't work."
- "No matter what metric you're using, it didn't work."
- "Your statistics are all based on correlation."
- "It didn't actually stop the type of weapon that people think it stopped from going onto the street."

### Oneliner
Beau questions the effectiveness of the assault weapons ban, pointing out that it failed to remove firearms from the streets and only regulated cosmetic features, leading to continued production and sales of the same weapons.

### Audience
Advocates for gun control

### On-the-ground actions from transcript
- Educate communities on the nuances of firearms legislation and the limitations of cosmetic-based regulations (exemplified)
- Initiate open dialogues and debates within communities about effective gun control measures (exemplified)
- Advocate for comprehensive gun control policies that address the actual firepower of firearms rather than cosmetic features (implied)

### Whats missing in summary
A deeper dive into how fear-mongering, toxic gun culture, social media influence, and political events contribute to the surge in firearm production and incidents post-ban.

### Tags
#AssaultWeaponsBan #FirearmLegislation #GunControl #CosmeticRegulations #FirearmProduction


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about whether or not something
worked and why you have people who you would think
would be in favor of it and saying that it would work,
saying that it doesn't.
And we're going to go through it.
Now, I know a lot of people already
have preconceived notions of this particular piece
of legislation.
And as soon as I start talking, you're
gonna want to throw statistics up. Hold them till the end please. Okay, so here's
the message. I just saw somebody say we need to ban all semi-auto rifles because
the assault weapons ban didn't work. I thought that was just a gun talking
point. How did it not work? There are three types of people in the world when
comes to guns. Pro-gun, anti-gun and those who understand how firearms work. If you
understand how firearms work, you know there is no way that the assault weapons
ban was successful. Like I said, I know you've got your studies, just wait. To
to really grasp this, you have to understand what the assault weapons ban
and did and did not do.
So let's start with what it didn't do.
It didn't take anything off the street.
So every assault weapon, every AR, every AK
that was there in 93, well, it was there in 94, 95, and 96.
So if the prevalence of these types of weapons,
If that had something to do with the frequency of incidents,
well, you would expect it to run flat, which
is kind of what it did.
Right?
It actually trended down a little bit.
The problem comes in when you have
to explain the hundreds of thousands of new ARs
that were sold during the ban.
new ones, new production.
The assault weapons ban did not stop production of the AR platform.
It did not stop production of the AK platform or any of the other ones.
The assault weapons ban regulated firearms based on cosmetic accessories basically.
So your standard AR has a flash suppressor, a bayonet lug, and then many have a telescoping
stock.
Those were three features that were listed in the assault weapons ban as things that
if a semi-automatic rifle had these, well, then it was an assault weapon.
So what did they do?
They removed those features.
no longer had a telescoping stock, didn't have a flash suppressor, and it didn't have
a bayonet lug.
But the rifle itself, the receiver, everything else was still there.
It's a weapon with the exact same firepower, takes the same magazines, fires just as quickly,
was still being sold.
To drive the point home, go to Google and type in AR pre-ban versus VS post-ban and
click images.
What you're going to get is a series of images that have two pictures.
There's one rifle right on top of the other.
If you're not a gun person, you're going to look at those and say, those are the same
thing.
I'm looking at the same picture.
not. The the post band one will be missing the flash suppressor and the
bayonet lug. Hundreds of thousands more more on the market. Same thing with the
AK. I'm gonna put a link to a video down below. It'll be in the the comment
section. The first comment I'll pin it. It's from the 90s. It is super cheesy.
and it is definitely like a right-wing pro gun, but it's really good because
there are two scenes in it you should watch. It's a relatively short video. One
of the scenes, there are three rifles sitting there. One is a fully automatic AK.
Okay, so that's one you pull the trigger, bullets come out until you let go of the
trigger. The next one is a semi-automatic AK. Pull the trigger, one bullet comes out.
You have to release the trigger, pull it again for another bullet to come out.
Okay, that's the civilian version. That is what most people would call an assault
weapon. And then there's a hunting rifle sitting there that is also semi-automatic.
The guy shoots all three. And then there's the twist where he tells you
that the hunting rifle is an AK. It just looks like a hunting rifle. Because of
the way the AK was designed they were able to use different stocks and mask it
even more. But the same weapon with the same firepower firing just as quickly
was still available during the ban. New production, new guns coming out. That
particular video is good because if you don't know anything about guns, there's a
scene where he removes the covers from the one that looks like the fully
automatic AK, the one that you would say is an assault weapon, and the one that
looks like a hunting rifle. Pulls them up and you can look into the receiver, which
is the engine of the rifle, and you see that they're identical. You can literally
pull parts out of one and put them in the other if you wanted to. The other
scene that's in it that's worth watching is where he takes a Mini 14, which is one
that we've talked about on the channel, not quite as popular when you're talking
about the media and the coverage, and in front of your eyes he transforms it. So
because of that, because the fact that there were more guns, hundreds of
thousands of more guns of this type on the market during the ban, I don't know that anybody wants
to say that there is a causative relationship between the increase or decrease of incidents.
Because it would, by most studies, by the way most studies count it, the numbers do
trend slightly down during the ban. That would imply that more guns means less shootings.
I don't think anybody believes that. Okay, so that's before the ban, during the ban.
About after the ban? Huge, huge spike in production. Massive. Massive.
like it's it is unreal. So there's a couple years where it's normal and then I want to say it starts
in 2007 but it really heats up 2008-2009 when Obama got elected. The numbers just shoot through
through the roof of production of rifles.
And then you see spikes that occur in 2012, 2016.
I'm willing to bet there will be one in 2020 as well,
because it's a political thing.
I am certain that right now the cells of ARs
are through the roof.
People are out there buying them left and right because
what just happened and the concern is that there's going to be legislation
against it. So you have a whole bunch of new ones entering the market right now.
Now the question here is why did this happen, right? Why was there this giant
spike right afterward? During the ban there was just a massive push to equate
any kind of legislation with true tyranny and to basically cast those who
owned ARs as patriots, and you still hear this today. When I talk about the
toxic gun culture, it started during the ban. Some would say because of the ban.
I don't know that I believe that I think the ban was a catalyst and guns or gun
rights groups used it to push a bunch of bad propaganda so it increased in
popularity because of the ban so a whole bunch of people started buying more and
more and more when you look at the production of the rifles it just shoots
up about four years after the ban ends. And then it stays high but spikes around
election time. In 2016, I want to say there were just more than four million
rifles produced in the United States and I think 61% of them were ARs. That's a
lot. Okay, so as far as those statistics that you were gonna post, it's Ben and
Jerry's. I know that anytime Ben and Jerry's has a good sales month, a whole
bunch of people are gonna drown. And it's not that Ben and Jerry go out there to
their pool and hold people underwater to celebrate their record-breaking sales.
it's that it's summer. It's correlation, not causation. There isn't a causative
link because there's more guns. There were more of this type of weapon on the
market in civilian hands during the ban than before the ban. But then we have
after the ban and because of this fact people like to kind of extrapolate and
say well there's not a causative link between the massive surge in production
and the massive frequency increase that we're seeing right now. Yeah, I'm not
sure about that. It seems to me that because of the just overwhelming numbers
that are being produced now, the AR is the most popular rifle in the United
States at the moment. They're in a lot of homes and they're in a lot of places
and in a lot of hands of people that probably wouldn't have had them back in
the 80s and 90s because of that toxic culture that developed during the ban,
pushing it and propagandizing it and basically using it as a tool so you can
feel safe. They motivated people by fear and every time people talk about banning
it they go out and buy more. Why? For the same reason Republicans all have my
pillow. They're doing it to own the libs and then a few months later they realize
maybe I shouldn't have dropped 600 bucks on that rifle and it enters the
secondary market. I can't say that what's happening now isn't causative. I
can't say that it is. My gut tells me that it's a piece of a larger issue
involving increased use of social media, a lot of fear-mongering, people falling
into echo chambers and stuff like that, and it's all working together to create
the situation that we have right now. I don't think that this is a single issue
type thing. I think there are more there are more factors to this than people
want to admit. So that that's my gut though. The stuff during the ban and
before, that's hard data. You can look all that up. But afterward we see a
giant increase and it does kind of track with a pretty substantial increase in
frequency but I think there are other factors at play beyond just access to
it. Don't get me wrong the access is not helping but I think at this point
Pandora's Box is opened and if you were to switch out and make all of the ARs
disappear I think you'd have the same issue but there would be different
weapons used.
But again, that part from the end of the ban on, that's gut.
So take that with a grain of salt.
But this is why you have people on all ends of this discussion.
If they really understand firearms, about the only thing that they can agree on is that
the assault weapons ban didn't work.
This is a person who is obviously in favor of gun control.
saying to ban every semi-automatic in the U.S. And the reason they're saying to go to
that step is because the assault weapons ban was a failure because it only regulated the
cosmetic features.
Now on the other side of the spectrum, you have pro-gun people saying, well, don't re-enact
the assault weapons ban, it was a failure, why would you want to duplicate it?
So that's about the only thing that you're going to have pro-gun, anti-gun, people who
understand firearms actually agree on, is that this didn't work.
No matter what metric you're using, it didn't work.
Your statistics are all based on correlation.
In fact, most of them say correlative.
I have yet to see anything that attempts to show a causal relationship.
And it's been a long time.
You would think with as much money as in the lobbying for this, you'd probably see one
by now.
But you can't.
Because if you get into the causal aspects of it, you have to admit that there were more
of them on the street that were low intermediate power box magazine fed semi-automatic rifles
that were used.
Some of the studies that get cited are kind of intentionally misleading because they say,
well, it wasn't an assault weapon used during the ban, even though it was an AR.
It was just missing the flash suppressor and bayonet lug, telescoping stock, so it didn't
at the legal definition of an assault weapon.
I've only seen a couple that go that route.
That to me is like super shady.
So that's your answer.
How did it not work?
It didn't actually stop the type of weapon that people think it stopped from going onto
the street.
were more during the ban than before.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}