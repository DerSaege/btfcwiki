---
title: Let's talk about Russians and John Deere....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=h0kbx8zJZvQ) |
| Published | 2022/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russian troops in Ukraine looted a John Deere dealership, taking equipment back to Chechnya.
- John Deere equipment is not easily repairable by smaller operators due to technology.
- Smaller farms are opting for other companies over John Deere due to complex technology.
- The expensive equipment stolen by Russian troops is controlled via remote access by John Deere.
- Russian troops won't be able to use the high-ticket items they stole due to being locked out remotely.
- The looted equipment is worth $5 million, including harvesters worth a quarter million each.
- Only smaller tractors might be salvageable, while the expensive equipment is rendered useless.
- This act by troops violates the laws of armed conflict.
- The Russian forces' activity is easily traceable, even down to the farm in Grozny.
- Crime doesn't pay, even in a war zone.

### Quotes

- "Crime doesn't pay, not even in a war zone, I guess."
- "John Deere has locked them out of it via remote access."
- "Smaller farms going with other companies."
- "Russian forces' activity is easily traceable."
- "They had a bunch of scrap metal, maybe a few engine parts and tires."

### Oneliner

Russian troops in Ukraine loot John Deere dealership, discover they can't use the high-ticket items due to remote access lockout, rendering $5 million worth of equipment useless.

### Audience

Farmers, tech enthusiasts, activists

### On-the-ground actions from transcript

- Contact local authorities about potential thefts or suspicious activities in your area (suggested)
- Join or support organizations that work towards accountability for armed forces (suggested)

### Whats missing in summary

The full transcript provides additional context on the impact of technological advancements in farming equipment on smaller operators.

### Tags

#JohnDeere #RussianTroops #Looting #Technology #ArmedConflict


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about something humorous
and give everybody a laugh,
introduce a little bit of levity to this week.
And we're going to talk about a story that broke
while everything else was breaking.
So it kind of got overshadowed,
but it is definitely entertaining to say the least.
So some Russian troops in Ukraine,
they decided to take a bunch of equipment
from a John Deere dealership.
They took this equipment all the way back to Chechnya.
Now, if you're familiar with John Deere equipment,
you already know where this story is going.
And while you're laughing, let me clue everybody else in.
The big ticket items from John Deere,
they're the reason that John Deere's kind of falling out of favor
with smaller operators, with people who have smaller farms.
Because you can't work on that stuff yourself.
And it's just loaded down with computers,
and it's all kind of controlled via satellite.
And this has led to smaller farms going with other companies.
You know, that John Deere green,
it's not as prevalent as it used to be on smaller places.
Now the big, the giant companies,
they're still going to use it because John Deere can basically run
their harvester and their seeder and all of that stuff from space.
And I get it, it's great for them,
but for smaller places, not so much.
So these Russian troops, they loot this city,
they loot this town, they steal $5 million worth of agricultural equipment
from a John Deere dealership,
and then they haul it all the way back to Grozny, to Chechnya,
which is where they find out that they can't turn it on.
All the high ticket items, all the real expensive stuff,
they took some harvesters that are worth a quarter million dollars a piece.
They can't run any of it, and they will not be able to.
John Deere has locked them out of it via remote access.
Now, some of the smaller stuff, the smaller tractors,
they might be able to get those running.
But the big stuff, the expensive stuff,
the stuff that was really hard to move all of that way,
nah, they're never going to get that stuff running.
So basically, they had a bunch of scrap metal,
maybe a few engine parts and tires.
That's what they got out of this,
because everything else is going to be useless.
It is worth noting that this type of activity by troops
is a violation of the laws of armed conflict.
And the Russian forces happen to engage in this behavior
with stuff that is just totally tracked in every way.
In fact, they know what farm it is on in Grozny.
So crime doesn't pay, not even in a war zone, I guess.
Just something that's a little bit humorous in a week
that is full of a bunch of stuff that just is not funny.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}