---
title: Let's talk about the dreams of the American worker....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ptdpKBETXDY) |
| Published | 2022/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A group of combat vets share nightmares about their past jobs, revealing unexpected sources of trauma.
- The group finds humor in each other's stories, showing gallows humor as a coping mechanism.
- There is a misconception in the United States that PTSD only stems from war events.
- Trauma can manifest from various sources, not limited to combat situations.
- Beau's Twitter post about job nightmares sparks responses from others sharing similar experiences.
- Combat vets talk about stressful jobs like working at a cell phone store, challenging the stereotype of PTSD triggers.
- The cavalier attitudes towards American workers discussing their job conditions are concerning.
- Beau suggests that the United States needs a labor movement to improve worker conditions.
- The stress faced by tough men at places like car dealerships points to underlying issues in the American workforce.
- Complaints about job conditions shouldn't be dismissed by comparing them to deployed individuals' experiences.

### Quotes

- "The stress came from a car dealership, a restaurant, a home improvement store."
- "The United States needs a resurgence in the labor movement."
- "Trauma can come from all kinds of places."
- "PTSD really only affects war events. And it's not true."
- "Those who deployed apparently have the same complaints."

### Oneliner

Combat vets share nightmares about past jobs, challenging stereotypes on PTSD triggers, calling for a labor movement to improve worker conditions.

### Audience

American workers

### On-the-ground actions from transcript

- Support and advocate for labor movements to improve worker conditions (suggested)
- Educate others on the diverse sources of trauma and the need for empathy and understanding (implied)

### Whats missing in summary

The full transcript provides deeper insights into the unexpected sources of trauma for American workers and the importance of acknowledging and addressing these issues in society.


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
the dreams of the American worker and how they may not be what we expect. We're going
to do this because I had a conversation and I thought it was unique. Turns out it's not.
But I'm hanging out with some friends and we're all just sitting around outside and
I'm doing what I always do, which is constantly checking my phone, looking at the news. And
I'm on Twitter and I see this tweet and it says, I keep having nightmares about my last
job. Is this normal? LOL. And I read it aloud because I know it's going to get a big laugh
because everybody who's there, all of these guys, all are combat vets. So I knew they
would find it funny. So I read it out loud and they did. They burst out into laughter.
It's one of those real good gallows humor type moments. And then one of them's like,
you know, I actually have something about working at Home Depot too. You can tell he
felt weird saying it. But as soon as he's finished, one of them was like, that restaurant
I worked at down on the beach, I have dreams about, well, he had some less than polite
things to say about the guest. It's a restaurant that caters to the ultra wealthy that comes
down. And he's like, I have nightmares about them yelling back into the kitchen or yelling
at the waitresses. And then the last guy who, out of a group of people who have seen and
done wild things, he's the one that has seen and done the wildest for the longest periods.
And he's like, no hesitation. Car dealership. Lasted three weeks. That is a stressful job.
These are people who would literally rather get shot at than go back to work at these
places. And I found this amusing. I also kind of enjoyed the acknowledgement because there
is a view in the United States that PTSD really only affects war events. And it's not true.
It is not a true thing. And trauma can come from all kinds of places. And it can manifest
in a whole bunch of different ways. And it was just cool to see that acknowledgement
like universally from the people who were there. So I put it on Twitter, expecting people
to be like, yeah, that's weird. But that's not what happened. Instead, the replies filled
up with people talking about the jobs that gave them nightmares. And the really interesting
part of it to me was that there were a whole bunch of people in the replies that I recognized
who I know are also combat vets. These are people who were in the same situation as these
guys. But that's not what they were talking about. They were talking about working at
a cell phone store and stuff like that. It definitely gives a very unique look at the
life of the American worker and what they deal with. And a lot of the cavalier attitudes
that come, that surface when American workers start to talk about their conditions and how
they have to work and the way things are at their job. Because from a lot of people, it's
just like, well, you need to suck that up. That's how it works. You know, get tough.
Some of the toughest men that I know, their real stress came from a car dealership, a
restaurant, a home improvement store. That probably says that the United States needs
a resurgence in the labor movement. In a movement that is set to improve the conditions of the
worker in this country. When you have people like that openly talking about how the real
stressful job is working at a car dealership, there's probably an issue. And it's one that
gets overlooked because a lot of times when people complain about their conditions, those
who want to muffle those complaints, those who want to downplay them, they say, well,
you better talk to the people who deployed. The people who deployed, they apparently have
the same complaints. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}