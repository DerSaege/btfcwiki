---
title: Let's talk about desks, creative solutions, and realism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rtgGht7YDcc) |
| Published | 2022/05/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reacts to a new idea of desks doubling as bulletproof shields for kids in schools.
- Contemplates the practicality and feasibility of the idea.
- Describes the potential materials and features of the detachable bulletproof shield desks.
- Acknowledges the need for creative solutions to protect children in schools.
- Expresses that this idea might be the best one he's heard that could actually be implemented.
- Quotes an activist's principle of being a realist and an idealist.
- Recognizes the current political reality regarding gun control measures.
- Emphasizes the necessity for actionable and creative ideas to address school safety.
- Rejects the notion of turning every child into a warrior for their own protection.
- Calls for more innovative solutions to safeguard children.

### Quotes

- "We need creative ideas. We need to think in ways that can actually be enacted right now."
- "I refuse to believe that the best we can come up with is making sure that we turn every child into Ragnar Lodbrok just so they can survive elementary school."

### Oneliner

Beau contemplates a new idea of desks as bulletproof shields for kids, stressing the importance of creative yet feasible solutions for school safety.

### Audience

Activists, educators, parents

### On-the-ground actions from transcript

- Develop and propose practical, creative solutions for school safety (implied)
- Advocate for innovative approaches to protect children in schools (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the need for creative yet realistic solutions to enhance school safety and protect children effectively.


## Transcript
Well howdy there internet people. It's Beau again. So today we're going to talk about new ideas
and desks and reality. One of the things that I always do anytime I hear a new idea,
the first question that runs through my mind is will it work? And it's almost involuntary.
Like I start running through and trying to figure out will this actually function?
Will it make a difference? Is it practical? Is it feasible? And it is. It's just something
that happens immediately. I was sent something on Twitter and it's a photo of a desk,
like you would find in a school. One of the type with the flat writing surface up top,
the little cubby hole underneath and four legs. And it says, what about detachable desktops that
can double as bulletproof shields? They would at least help protect center mass and head
and allow kids to go mobile on mass, not just be sitting docks. We got to get creative
and give them ways to defend themselves. It'd be worth every penny.
So the first thing that goes through my mind is will it work? And I mean, yeah,
you start thinking about it. That writing surface, that flat writing surface, that can be made out of
the same thing plates are made of. The bullet resistant material that's in the plate carriers,
those worthless cops had. You could make it out of that. Inside in the cubby hole, handles,
to allow it to detach, heavy nylon strap to allow the kids to slide their arms through it
so they can hold it in front of them. And then if you could teach them to form a shield wall,
it would be pretty effective. And this is the moment when I realize what I'm actually thinking about.
The thing is, it's honestly, it's probably the best new idea that could actually be enacted
that I've heard so far. Somebody once said, and I wish I knew who because it's definitely
something that has stuck with me, but I wish I knew who it was. And I'm not going to tell you
who it was, but it's definitely something that has stuck with me. Said that an activist can't
be a perfectionist. They have to be a realist and they should be an idealist.
The realist part is that right now with this Senate and that Supreme Court,
an assault weapons ban is not going anywhere. It won't go through the Senate and it will not be
court. That's the reality. So even if you believe that that is the perfect answer,
you can't just wait for that to happen because you have to be a realist about it.
The person's right. The person who sent this is right.
We need creative ideas. We need to think in ways that can actually be enacted right now.
But I refuse to believe that the best we can come up with is making sure that we turn every child
into Ragnar Lodbrok just so they can survive elementary school.
We need new ideas. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}