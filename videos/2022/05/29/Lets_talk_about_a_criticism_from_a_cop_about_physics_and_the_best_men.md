---
title: Let's talk about a criticism from a cop about physics and the best men....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bYM6e87fWBw) |
| Published | 2022/05/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Corrects criticism of law enforcement regarding countering a rifle with a pistol.
- Shares a recent incident where a woman used a pistol to stop a shooter with an AR at a graduation party.
- Points out that shooters often have a domestic violence connection.
- Emphasizes that countering an AR with a pistol is possible and done frequently.
- Argues that it's not about gear but about the warrior mentality and commitment.
- States that the U.S. military could issue gear to everyone, but it's the commitment that matters.
- Explains that what makes elite individuals special is their ability to perform without relying solely on equipment.
- Disputes the idea that gunfights are determined by the power of the weapons involved.

### Quotes

- "Countering an AR with a pistol can be done, and it's done all the time."
- "It's not the equipment. They've proven that over and over again."
- "It's just physics. I don't even know what that means."

### Oneliner

Beau corrects misconceptions about countering a rifle with a pistol, citing real-life incidents and stressing the importance of commitment over gear in law enforcement.

### Audience

Law enforcement critics

### On-the-ground actions from transcript

- Understand the importance of commitment and warrior mentality in law enforcement (implied)

### Whats missing in summary

In-depth analysis of the impact of mindset and training over equipment in law enforcement situations

### Tags

#LawEnforcement #Criticism #RifleVsPistol #WarriorMentality #Commitment


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about physics
and corrections and criticisms and whether or not something is possible and maybe we'll
discuss the fact that perhaps I went a little too far in my criticism of law enforcement.
I'm a cop and your criticism of the cops was initially fair because they didn't enter once
they had the proper equipment. But I'm afraid I have to correct you when it comes to countering
a rifle with a pistol. The rifle has more power and can fire faster than the pistol.
Only the absolutely best trained men would ever even think about going against an AR
with only a pistol and the smart ones would decide against it. You need the right equipment.
It just doesn't happen. It's not cowardice. It's not a lack of warrior mentality. It's just physics.
The rifle is so much more powerful that it can't be countered by a pistol.
Sorry, maybe stick to stuff you know about.
Only the absolutely best trained men would ever think about going against an AR with only a pistol.
Blah, blah, blah. It just doesn't happen. Except it does. It happens, I mean, pretty regularly.
In fact, it happened this week. It happened this week. According to reporting in West Virginia,
a man goes speeding through an apartment complex. He's basically told to slow it down.
There's some kind of exchange of information. He's told to slow down. He's told to slow down.
There's some kind of exchange. He gets mad. He leaves. He comes back with his AR.
He steps out and he opens fire on a graduation party. A woman, I don't know if she was the best trained or not,
a woman pulls out her pistol and counters the threat and stops it. And because she did it quickly
and it was connected quickly, as far as I can tell from the reporting, the only person to go that day was the shooter.
It happens pretty often. Now something I'm going to start doing anytime I talk about this stuff
is pointing something out. In this case, yes, the shooter had a DV connection.
They almost always do. Okay. So yeah, it can happen. It doesn't have to be the best trained men, so on and so forth.
It's not a lack of warrior mentality. No, that's exactly what it is. It's exactly what it is.
When you talk about those best trained men, you know, people do. They focus all on the gear.
But the whole point is that that's extra. The reason they are who they are and have the reputation they have
isn't because of the gear. If that was the case, the U.S. military could just issue that gear to
everybody in the army and then everybody's elite. It's not that. It's the commitment.
It's the warrior mentality. It's the fact that they won't quit. It's literally ingrained in their
mottos and their ethos. Fight on and complete the objective. Though I be the lone survivor,
night stalkers don't quit. It's who they are. That's what it is. It's not the equipment.
They've proven that over and over again. What makes them so special is that they can do it
without the equipment if they have to. It's just physics. I don't even know what that means.
I mean, that would make sense. That would be a statement that makes sense if you thought that
gunfights got determined by the two guns firing the bullets directly at each other and the one
that has the most kinetic energy is the one that's going to win because it's going to knock the other
one out of the way. But that's not how they work. All that matters as far as the physics is that the
weapon you're using has enough energy to penetrate the other person. You're not actually fighting
the other weapon. It's not a power contest between the two. If your weapon at the range you're at
has the energy, has the capability to kill, well, it'll work. That's not how it works. You're
comparing the power of the weapons doesn't make sense because you're not actually fighting the
other bullet. You're fighting the other person. And as far as it firing faster, that's dependent
on the shooter, not the gun. I mean, I know I don't know much about this stuff, but this whole
take is wrong. It's an excuse. And the reason the excuse is so hard to let go of is because
cops have come into the whole, well, I'm going home at the end of my shift. Okay, go there and
stay there. If that's your attitude when it comes to an incident at a school, we don't want you.
It's that simple. You're not fit. That attitude of better you than me, I'm going home at the
end of my shift. This is why law enforcement has the reputation it does. This is why every time
you have a situation like that, I don't want to say every time, this is why when you have
situations like this, the response from law enforcement is often underwhelming.
It is absolutely the lack of a warrior mentality. Countering an AR with a pistol can be done,
and it's done all the time. It's pretty common because there's so many ARs out there.
You're wrong. Maybe stick to stuff you know about. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}