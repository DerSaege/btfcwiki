---
title: Let's talk about why the democrats didn't codify....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L3l34lC7qds) |
| Published | 2022/05/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Dissects the historical fact about federal legislation and Roe v. Wade, pointing out its limited impact.
- Emphasizes the importance of understanding the raw exercise of power by the Supreme Court.
- Asserts that the solution lies at the local level and not in Washington, D.C.
- Warns that the current political situation is not ordinary and stresses the need for people to realize this.
- Mentions the ongoing threats beyond abortion rights, indicating a broader agenda.
- Criticizes the focus on federal legislation as merely adding a speed bump for those in power.
- Talks about the importance of building local power structures instead of relying solely on federal actions.
- Illustrates how certain groups like the Tea Party and MAGA have influenced national parties by starting at the local level.
- Concludes by reiterating the limited impact of past legislative actions in the current political climate.

### Quotes

- "The solution to this is not in D.C. It is at the local level."
- "You're not dealing with anything more than the raw exercise of power at this point."
- "If you are not a straight white guy who happens to also be a theocratic Christian, they're coming for you."
- "This isn't politics as usual."
- "It is more important to understand the kind of fight you're actually in and that this is just the beginning."

### Oneliner

Beau explains why focusing on federal legislation for Roe v. Wade isn't the solution; local power structures are key in the ongoing political battle against regressive policies.

### Audience

Activists, Community Leaders

### On-the-ground actions from transcript

- Build and strengthen local power structures to resist regressive policies (implied).
- Motivate and mobilize the base by developing policies that resonate with normal people (implied).

### Whats missing in summary

Beau's deep analysis and historical perspective provide critical insights into the ineffective nature of federal legislation in combating regressive policies and the importance of building local power structures for lasting change.

### Tags

#FederalLegislation #RoevWade #LocalPowerStructures #PoliticalActivism #CommunityLeadership


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a certain little historical fact
that keeps getting thrown out right now
as if...
It's being presented as if it is super important
and it's something that would have altered the outcome
of a lot of current situations.
So we need to go through that
because I think with people saying this,
it shows they don't understand the situation.
This historical fact, surfacing,
demonstrates they don't understand what they're up against
and it's really important that people do.
And I've had a bunch of people make comments.
I'm going to use this one because this one has dates in it, which is useful.
Carter had a veto-proof supermajority in 1977-79.
Carter had Dem Senate and House 79-81.
Clinton had Dem Senate and House 93-95.
Obama had a supermajority in 2009.
Biden currently has Dem Senate and House.
And the question that goes along with this is,
hey, why didn't they pass federal legislation
to turn Roe into federal law?
Okay, now the question is there and it deserves an answer.
There's two reasons to that.
One, sometimes it probably just wasn't a legislative priority.
And other times, I would imagine that Democrats looked at it and were like,
hey, that's our wedge issue too.
And they already had the Supreme Court ruling.
So they didn't see it as a high priority.
Now, here's the important question.
Let's say they had.
Let's say that the federal law was put into effect.
What exactly do you think would change?
The hierarchy of this local, state, federal, Supreme Court ruling?
If they overturned Roe, whatever that was would be overturned too.
And I know that doesn't make any sense because right now,
Democrats in D.C. are pointing to that as like something they're going to do.
Yeah, that doesn't have anything to do with it actually being effective.
Just to be super clear on that.
That has to do with removing that insulation.
Republican candidates are insulated from this really unpopular decision
by the Supreme Court.
If they run it through the House and the Senate, they have to vote on it.
And that can be used in the elections.
That's what that's about.
It's not going to change anything.
Understand the Supreme Court, they'll overturn that.
You're making the assumption that the Supreme Court is operating in good faith.
They're literally reaching back to somebody from the 1600s who believed in witches.
That's who they're basing this on.
It's not in good faith.
That's the part people need to understand.
You're not dealing with anything more than the raw exercise of power at this point.
The solution to this is not in D.C.
It is at the local level.
Those local power structures are going to be the only thing
that will be able to unseat or mitigate this.
Asking why something didn't happen 40 years ago kind of indicates
that you believe it would change the outcome.
It wouldn't.
It wouldn't.
And it's important for people to realize this.
Because this isn't over.
They're not done.
They are coming for gay marriage.
They are... there are things cited in that opinion, cases cited in that opinion,
that make it pretty clear where they're headed with this.
In short version, if you are not a straight white guy who happens to also be a theocratic
Christian, they're coming for you.
It is really important that people understand that.
It is really important that people understand this isn't politics as usual.
If you want to... if you want to just look to D.C.,
then understand you might as well not do anything.
You're not dealing with normal politics.
Federal legislation on this... yeah, I mean, it'd be useful.
It would be another speed bump for them to roll over.
But make no mistake about it, they intend on rolling over them.
There's a whole bunch more coming.
You already have the governor in Texas talking about going after one that...
a ruling that made sure that everybody got access to education as far as kids.
This isn't over.
This is the opening shot.
Looking to D.C. and trying to find some way to basically say, well, the Dems didn't do
enough.
Yeah I mean, you're right, but not in the way you're thinking.
They didn't do enough because they didn't build those power structures.
They didn't develop policies that normal people are getting behind.
They didn't motivate their base.
There's a whole bunch of things they didn't do that helped lead us here.
Absolutely.
But passing the legislation isn't one of them.
That literally would not matter.
It's just an extra paper for them to write.
I would also point out that we have witnessed twice where different groups have kind of
completely altered the position of a national party.
You have both the Tea Party and then the MAGA thing, right?
Most of that was done at the local level and it filtered up.
The structure that Trump used, he just took control of something that already existed.
He didn't come in and build that.
When you say the Dems didn't do it, yeah, I mean, sure, if you're talking about the
party, but the individuals who lean that way or lean past them, they didn't build those
local structures either.
So it's important to note that this argument is historically accurate.
There were multiple times that the Democratic Party could have put protections into law,
but it is more important to note that it wouldn't have mattered.
It is more important to understand the kind of fight you're actually in and that this
is just the beginning.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}