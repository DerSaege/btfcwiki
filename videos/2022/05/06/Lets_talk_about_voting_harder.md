---
title: Let's talk about voting harder....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ITaqQu0K9SM) |
| Published | 2022/05/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing how the country ended up in its current situation, where an opinion heavily reliant on a person who believed in witches is overturning 50 years of legal precedent.
- Expressing concern over the defeatist attitude and doomerism among some individuals regarding the Democrats' failure to prevent the current situation.
- Questioning how to fight back against the right-wing successes in stripping rights away from the majority.
- Exploring the tactics used by the Republican Party, such as leveraging community networks like churches for political gains.
- Emphasizing the power of community networks in influencing election results and ultimately shaping the national landscape.
- Noting the need for a shift in mindset towards electoral politics, especially in light of recent events affecting fundamental rights.
- Urging people to organize and build local power structures as a way to counter regressive policies and protect individual freedoms.
- Stating that voting out of self-defense and organizing at the local level are key strategies in resisting harmful political agendas.
- Encouraging individuals to take action and participate in building a stronger, more resilient community network to combat oppressive policies.
- Acknowledging the long and challenging road ahead in undoing existing damage and preventing further erosion of rights.

### Quotes

- "How do we fight back? Give me something."
- "You can call that voting harder if you want to. I don't see that as an inaccurate description of what they did."
- "That strategy, you can use it too. There's nothing saying you can't."
- "It's not relying on the Democratic establishment in DC. They're not your solution."
- "That's the solution."

### Oneliner

Beau urges a shift towards community organizing and local power structures to counter regressive political agendas, advocating for voting out of self-defense and active participation to safeguard fundamental rights.

### Audience

Community activists, organizers, voters.

### On-the-ground actions from transcript

- Organize with local community members to establish strong local power structures (suggested).
- Participate actively in building resilient community networks to resist regressive policies (implied).
- Shift focus towards electoral politics and voting out of self-defense to protect fundamental rights (implied).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the current political landscape and offers a strategic approach towards community organizing and active participation as means to combat regressive policies and protect individual freedoms effectively.

### Tags

#CommunityOrganizing #VotingOutofSelfDefense #BuildingLocalPower #ResistingRegressivePolicies #ProtectingRights


## Transcript
Well, howdy there, internet people.
Lids a bow again.
So today, we are going to talk about voting harder.
We're going to talk about voting harder.
We're going to kind of analyze what happened
and how we wound up, where we're at.
We're going to see if we can figure out
what brought us to this point.
What put this country in the situation where an opinion that is heavily reliant on a guy
who believed in witches is upending 50 years of legal precedent here?
And we're going to do this because I got a message.
Bo I need some encouragement.
I'm just doom scrolling at this point and watching all the edgy people who normally
have good ideas, just talk about how Democrats betrayed us by not stopping this, and how
all is lost.
They've given up.
They're making jokes about voting harder.
Am I really going to lose my rights because of this?
I don't see it stopping here.
How do we fight back?
The left is just reverted to edgy comments about how the system is broken and it's pure
doomerism.
While they sit there and throw their hands up making jokes, I'm one of the people that
will suffer. How do we fight back? Give me something. Yeah. Yeah. It's a weird
situation. You have to analyze why they were successful, how they did this,
because that's the only way you'll be able to counter it, right? The reality is
what the Republican Party did here and how they're managing to strip the
rights away from half the country, it shouldn't have happened.
Overturning Roe is unpopular.
The overwhelming majority of Americans want Roe upheld.
So how did we end up here?
What tactic, strategy did they use to get the country in this situation?
That's where you have to start.
And it's not really that complicated.
It's just time consuming.
They started by exploiting turnkey community networks, networks of people that were already
built, churches.
They used those as voting blocks to get their candidates into local office.
Then those candidates used their position to get their hands on the levers of power
in the various states.
Then holding those levers in all of the states, they used that power to propel their candidates
on the national level, and then to insulate themselves from the decision that they know
is wildly unpopular.
They didn't try to push it through, not through legislation.
They appointed people who they knew would do what they were sent there to do.
That's how they wound up here.
That's how they wound up in a situation in which at most 30% of the country is stripping
the rights away from 70%.
That's what they did.
You can call that voting harder if you want to.
I don't see that as an inaccurate description of what they did.
That's how they accomplished it.
Those local power structures are voting blocks.
Community networks, churches, whatever kind of organization, whatever name you want to
give it, a group of people committed to an idea.
Those people have power.
That power can manifest in election results.
And that's what they focused on.
And they followed this strategy all the way to DC and then all the way to the Supreme
Court.
That's what happened.
There aren't, I would imagine there are a lot of people who are heavily, who have built
their audiences around the idea of being edgy, who in some ways are loving this because it
validates their rhetoric.
There are some people who have ideological qualms with the electoral system.
There are people who don't like voting, who view it as giving consent to be governed.
They're probably not happy about this, because generally speaking, those people are super
in favor of people being able to make their own decisions. I have a feeling that there's
going to be a lot of people who are from that part of the left who may have a change of
heart about electoral politics and it may start to fall into the whole voting out of
self-defense thing, the news is new.
I don't think many people expected this.
I know there were a lot of people who were saying that it could happen, but I think most
people looking at the polls, looking at the way the country is shaping, looking at the
fact that there aren't countries, I think there's only three countries in the entire
world that have actually rolled back family planning laws and made them more restrictive.
In like the last quarter of a century, it's not a thing.
It's very regressive.
It's something that has been shown to not be a good thing.
So I don't know that a lot of people were prepared for this and that may be why they
don't have a plan or they don't have words of encouragement because they're just as shocked
as you are.
The thing is, what the Republican Party did, how they betrayed we the people and went against
will of the people. That strategy, you can use it too. There's nothing saying you can't. And it
would be a whole lot faster for you. Because most people, the overwhelming majority of people,
want Roe upheld. The polls have shown that time and time and time again.
And yeah, I mean, call it voting harder if you want to.
But that's your solution.
That's how we wound up here.
That's what you have to counter.
And understand their focus, especially among Trump loyalists, their focus on state politics
right now, it's to do the same thing with the elections.
That's why they're spending so many resources and focusing so much on the secretaries of
states, the secretaries of state positions, because they control the elections.
They're doing the same thing again.
I know that for the overwhelming majority of the left and even the anti-authoritarian
right, participation in electoral politics, it's not cool, right?
It doesn't get clicks.
undermines your street cred or whatever. But I do believe that most are going to
are going to realize that it's voting out of self-defense. That there are a
whole lot of people like yourself that are going to pay if that rhetoric
it continues.
And you're right, it's not going to stop here.
If you look at that opinion, and you look at some of the cases that were cited, they're
coming for a lot of people.
They're going to try to strip the rights from a lot of people.
They're going to try to send this country back in a lot of different ways, and they're
being very open about it.
I don't think it's time for edgy jokes.
There are a lot of people who are going to suffer because of this.
It's
organizing.
It's building those local power structures that can compete.
It's not relying on the Democratic establishment in DC.
They're not your solution.
I know that people are blaming them right now, and I get it, I understand, because there
are a whole lot of people who believe, yeah, you know, they should have added to the court.
And I didn't have a problem with that. The second Republicans put up a justice who couldn't identify the elements of the
First Amendment.
It didn't happen. And you can sit there and point fingers at stuff that happened in the past, but that's not going to
help you.  That's not going to help all of the people who are going to find themselves with less freedom
and less rights and less ability to live as they choose because of what's
happening. If I could encourage you to do something, it's organize. It's get with
those in your local community and get that local power structure set up.
That's the solution.
There's probably a really long fight ahead for a lot of people.
It's going to take a lot of people working together to undo what has already happened
and to stop what's coming.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}