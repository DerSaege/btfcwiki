---
title: Let's talk about turnkey community networks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z0BHKvaiRE8) |
| Published | 2022/05/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Emphasizes the importance of community networks, specifically turnkey networks that need activation or mutual assistance agreements.
- Mentions the example of the Republican Party using the Evangelical Church to push their agenda and how similar power structures exist on the progressive side.
- Suggests looking into nonprofits, newly unionized places, art galleries, gay clubs, liberal churches, student groups, environmental clubs, and more as potential allies.
- Points out the value of combining networks to create a stronger collective impact beyond just voting power.
- Recommends utilizing social media to connect with like-minded individuals and expand networks.
- Advises on the benefit of not starting with a network of close friends to maximize reach and impact.
- Encourages viewers to recognize existing power structures they can collaborate with or recruit from and teases more upcoming videos on the topic.

### Quotes

- "There already are power structures that you can ally with or recruit from."
- "When you start combining your network with others, that's when that block develops."
- "It's better if everybody involved isn't close friends to begin with."
- "You show up for them, they show up for you type of thing."
- "There are more of these videos coming."

### Oneliner

Beau stresses the importance of activating existing community networks and forming alliances to create a stronger collective impact for progressive causes.

### Audience

Community organizers, activists

### On-the-ground actions from transcript

- Join a nonprofit or community organization to connect with like-minded individuals (implied).
- Establish connections with newly unionized places for mutual support (implied).
- Collaborate with art galleries, gay clubs, liberal churches, student groups, environmental clubs, and other organizations to build a network (implied).
- Utilize social media to find and connect with potential allies (implied).
- Encourage diverse connections within networks to maximize reach and impact (implied).

### Whats missing in summary

Importance of building diverse and interconnected community networks for collective impact.

### Tags

#CommunityNetworks #Activism #Allies #Progressive #Collaboration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk a little bit more
about community networks.
And we're going to focus on turnkey community networks,
networks that already exist.
They just need to be activated.
Or they're already active, and you
might want to either recruit from these groups
or maybe set up some form of agreement
where you mutually assist each other.
When the concept of turnkey community networks came up,
I was talking about the Republican Party
and how they used the Evangelical Church
to forward their agenda.
And over time, the two kind of merged.
When I brought that up, people seemed
to be under the impression that people on our side of things
don't have these, that there aren't power structures that
already exist that are friendly to our way of thinking.
And that's not true.
There are.
I'm going to give you a few.
And understand this is not an exhaustive list.
There are a whole bunch more.
Hopefully this will spark other ideas.
OK, so the first place you could look is nonprofits.
Now, generally speaking, the people
who write the big checks at nonprofits,
they probably aren't actually like-minded.
However, the people who work there,
the people who may have gone to school for six years
to get a master's to come out and work for $17 an hour,
yeah, they're probably on our side.
They're probably people who want to make the world better.
So that's one place.
The labor movement is another, particularly
newly unionized places.
Those places that are becoming unionized now,
they tend to be more progressive.
And you also know they already know
how to organize, which is an added benefit.
Now, if you're down in the South or perhaps in the Midwest,
you may find people in unions who don't actually
understand that that's left.
And they may hold very conservative social views.
So just be ready for that.
Art galleries.
Generally speaking, people who are creative,
they're open-minded.
So you will probably find allies there.
Gay clubs.
With everything going on, I would
imagine that they understand their next.
If the Republican Party gets their way,
right to marriage, stuff like that, it's in jeopardy.
So I would imagine they're going to be active on their own.
So this might be a group you want
to set up a method of you amplifying their message.
And maybe they'll help you.
Then you always have liberal churches.
We talked about the evangelical churches before.
But there are a whole bunch of churches
that don't share those views.
Those power structures already exist as well
and can be tapped into.
Student groups at universities are another really good example
because you know they're already active.
And they might actually be able to benefit
in really tangible ways from people
who aren't at the university.
At least having access to your network
may benefit them more than you having access
to theirs in a lot of ways.
So that's one that if there's one in your area,
he should probably cultivate.
And again, that's a group of people who are already active.
These are people who are already in the game.
They'll show up.
So let's see.
You have environmental clubs are good ones.
Animal rights groups.
You know, these are again people that are already active.
And there's normally one in pretty much every area.
Libraries.
Well, really anything having to do with education
because education has a well-known liberal bias.
So libraries, schools in general,
especially with the way teachers have been villainized
by the Republican Party.
Indie bookstores.
These are all really good places to find allies
and to find networks that already exist.
When you start combining your network with others,
that's when that block develops.
That's when you can accomplish a lot of stuff.
That's when that real power base starts to form.
And yeah, it can be used as a voting block
the same way the Republican Party did.
But it can be used for a lot of other stuff too
when it comes to mitigating harm
or just making the world better in general.
Another place that for some reason gets overlooked a lot
is social media.
You can generally search by location
and you can find people
and you can see their views via their profile.
Now, if you already have a network,
these are good groups to kind of ally with,
to establish a rapport with.
You show up for them, they show up for you type of thing.
If you don't have a full network,
let's say you only have four people,
these are good places to look for people
to join your network.
And remember, when you're building a network,
it's better if everybody involved
isn't close friends to begin with.
Because if everybody's close friends when it starts,
you're going to share friends,
which means your network is limited.
If everybody only kind of knows each other,
they have more friends that aren't involved in that network,
which means it has more reach
and it can be leveraged further.
So just a quick overview of this.
I assume that as I was going through this,
other things popped up in your mind.
There are tons.
It doesn't matter where you're at.
There already are power structures
that you can ally with or recruit from.
So we'll be going over more.
There are more of these videos coming.
We're doing it piece by piece
to allow questions to come in
so I know which direction to head
and to give everything time to digest.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}