---
title: Let's talk about Britney Spears being every woman in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6YXvuIET2rI) |
| Published | 2022/05/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Britney Spears' situation mirrors that of every woman in the United States right now, as she faced backlash for posting photos in her birthday suit.
- Despite being 40 years old, Britney is not trusted with bodily autonomy and is seen as needing someone else to make decisions for her.
- There is a push to return women to a legal structure where others control their choices, reminiscent of historical restrictions on women's freedom.
- The argument to overturn Roe v. Wade is not about moral reasons but about control and limiting women's autonomy.
- The double standard applied to Britney showcases society's desire to control women and restrict their agency.
- The focus on controlling women is about preserving power for mediocre men over women, rather than genuine concerns about family planning.
- The criticism and attempts to control Britney reveal deeper societal issues around gender roles and power dynamics.
- Restrictions on women's autonomy are not equally applied and are often rooted in outdated stereotypes and biases.
- The push to control women's choices extends beyond family planning and into various aspects of their lives.
- The underlying issue is about control and maintaining power dynamics that favor certain groups over others.

### Quotes

- "It's about control. It always has been. Everything else, that's just camouflage."
- "It's about returning women to a subservient class where they have to do what they're told."
- "It's about preserving mediocre men's power over women."
- "If you think it's about anything other than control, you're wrong."
- "Freedoms that should be extended to everybody at all times."

### Oneliner

Britney Spears' situation epitomizes the ongoing battle for women's autonomy and the underlying desire for control over their choices in society.

### Audience

Advocates for gender equality

### On-the-ground actions from transcript

- Challenge and push back against attempts to control and restrict women's autonomy (implied)
- Support organizations and movements that advocate for women's rights and autonomy (suggested)

### Whats missing in summary

The full transcript provides a deeper exploration of societal attitudes towards women's autonomy and the underlying power dynamics that influence decision-making.

### Tags

#BritneySpears #GenderEquality #Autonomy #Control #Women'sRights #PowerDynamics


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Britney Spears.
We're going to talk about Britney Spears
and how Britney is every woman in the United States right now.
How her situation mirrors that of every woman
in the United States right now.
Because she posted some photos,
and people didn't like those photos.
Some people looked at them and said, well, that's wrong.
Therefore, she needs to be returned to a legal structure
in which somebody else is making decisions for her.
If you don't know what I'm talking about,
she posted photos in her birthday suit.
And while we're talking about birthdays,
I need to point something out
that is really important to this conversation.
Because I know a lot of people still see her
as the girl next door who's singing,
Oops, I Did It Again.
Britney is 40 years old.
She is 40 years old,
but for some reason can't be trusted
to post photos that she wants to post.
She doesn't have bodily autonomy.
Her actions must meet a standard
that other people impose upon her.
She can't think clearly with her little baby lady brain,
I guess.
She needs a man to tell her what to do.
Tell me that doesn't mirror everything
that's going on in this country right now.
For a long time, women in this country
had a certain standard imposed upon them by law,
a legal structure that they had to exist under.
Then they get a break from it.
They're released from this legal structure.
They have freedom.
They have bodily autonomy.
They get to make their own choices.
And then because a small group of people
don't like the way they exercised their freedom
and the choices that they made,
people want to return them back to that legal structure
where somebody else gets to decide
what to do with their body.
And that's the thing,
is that those who argue in favor of overturning Roe,
they always say, it's not about that.
It's not about control.
It's not about bodily autonomy.
We have this other very moral reason
to impose this upon women.
The rest of society says that that's just an utter lie.
Everything else that happens like this, there's no harm,
but there is now a call to return her to a situation
in which she cannot make her own decisions,
that she doesn't have any choice.
It mirrors it perfectly.
I would also point out that this standard
isn't uniformly applied.
See, Brittany is that stereotype.
Because of how she came on the scene,
people look at her as that young woman
that really just doesn't have it all figured out yet,
maybe needs some guidance.
She's 40.
She is 40 years old.
And people want to return her to a legal structure
because she posted photos on the internet
that they don't approve of.
It's about control.
It always has been.
Everything else, that's just camouflage.
It's about returning women to a subservient class
where they have to do what they're told.
That's what it's about.
It's what it's always been about.
Everything else is just camouflage.
Because you can find that exact same attitude
about returning women to that second-class state
throughout all of society.
Things that have nothing to do with family planning,
it still exists.
It's never been about their actual arguments.
It's about control.
It's about preserving mediocre men's power over women.
That's what it is.
Don't let anybody tell you differently.
Because that standard isn't equally applied.
It's applied to her in this situation
because of the image that people have of her.
Because she's viewed as young
because that's when she came on the scene.
She's 40.
40 years old.
And my understanding is that she's about to be a mom,
trying to get pregnant, maybe already pregnant.
I'm not really sure, I don't actually keep up with this stuff.
Maybe the photos are kind of to show that transformation.
Maybe it's actually a celebration.
Maybe she is very, I don't know, pro-baby.
Maybe that's what it is.
But her decisions, they're wrong.
Take away her right to make those decisions.
If you think that this is actually about family planning,
you're wrong.
If you think it's about anything other than control,
you're wrong.
If you think it stops here, you're wrong.
There's going to be more and more rollbacks of freedoms
that people have come to enjoy.
Freedoms that everybody should have.
Freedoms that should be extended to everybody at all times.
Freedoms that should be, I don't know, unalienable.
But I guess we can't have that,
because there are insecure men who feel their only way
that they can have some value is to control somebody else.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}