---
title: Let's talk about civic engagement, Roe, and electoral politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EUApNV7S-Ig) |
| Published | 2022/05/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains different types of civic engagement, including capacity building, community networking, direct service, research, advocacy and education, electoralism, personal responsibility, philanthropy, and participation in association.
- Addresses the criticism of electoralism and its effectiveness in creating change.
- Points out the importance of electoralism in preventing harm and holding the line, especially in the context of the jeopardy faced by Roe.
- Challenges the notion that electoralism is a dead end for change and showcases its role in influencing laws and policies.
- Emphasizes the need for concrete steps and actions rather than just criticizing electoralism without providing alternatives.
- Raises awareness about the impact of reduced voter turnout on vulnerable populations facing potential harm.

### Quotes

- "If you want to get out there and talk about these other methods, these other types of civic engagement, and promote them, I get it."
- "Electoralism isn't the best tool for deep systemic change, but it can stop harm."
- "At the bare minimum, it holds the line."
- "Just screaming that the Democrats didn't do enough and that there's no reason to vote, that's not helping."
- "There are people who are going to be in harm's way because of this."

### Oneliner

Beau explains types of civic engagement, defends electoralism's role in preventing harm, and urges concrete actions over mere criticism.

### Audience

Activists, Voters, Community Members

### On-the-ground actions from transcript

- Promote different types of civic engagement and encourage community involvement (implied)
- Take concrete steps to address immediate needs, create long-term solutions, and mitigate harm in your community (implied)
- Advocate for causes through demonstrations, petitions, and public education efforts (implied)
- Ensure voter turnout by educating and motivating individuals to participate in elections (implied)

### Whats missing in summary

The full transcript provides detailed insights into the importance of electoralism in preventing harm and challenges the belief that it is ineffective for creating change.

### Tags

#CivicEngagement #Electoralism #PreventingHarm #ConcreteActions #CommunityInvolvement


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today is going to be a little bit different.
Normally, I tell you what prompted a conversation, and
then we talk about it.
Today, I'm going to provide the information, and then we'll
talk about what prompted it in detail.
But we're going to talk about the types of civic engagement.
We're going to run through the different types,
and we're going to address an attitude that has come up
concerning electoralism.
All right, so what are the types of civic engagement?
The first is capacity building, community networking.
We're going to start with the two I like the most.
Community networking.
This is working within a defined geographic area,
analyzing the assets and the liabilities,
the problems they have, trying to develop the infrastructure
to fix them, or set it up so the community can
solve the problem itself.
Capacity building, community networking.
The next is direct service.
And there are other terms for this,
but this is what gets the goods.
When you are talking about achieving results
and addressing immediate needs, devoting your personal time
and energy right there at the time, addressing the problems,
mitigating them if you have to, whatever you can.
That's direct service.
Another is research.
And research is developing the intelligence, the information
to know what the assets and the problems are,
making sure that all of the other forms of civic engagement
have the information they need to do
what they're trying to do.
Advocacy and education.
This is demonstrations, petitions, stuff like that.
Stuff designed to further your calls in public opinion
or educate the public about the cause,
so hopefully they come to a better conclusion.
put pressure on government or corporate entities.
Advocacy and education.
Electoralism, that's voting, campaigning, stuff like that.
Personal responsibility, this is setting the example.
Doing what's right, hoping that other people follow
at the bare minimum, at least you're not part of the problem.
Another is philanthropy, this is throwing cash at a problem.
or funding those who are engaged in a different kind
of civic engagement.
And then the last is participation in association,
which is very similar to capacity building.
But this is more on a social level.
So this is about getting the contacts to get stuff done,
rather than building the infrastructure.
So these are the types of civic engagement.
Now electoralism, because of the possibility of Roe going the way it might go, it's catching
a lot of heat right now.
And there's a lot of people that say it's pointless, it doesn't do any good.
The comment that caught my attention the most is, the point is that electoralism is a dead
end for change.
And I get this, I get it, believe me, this is a belief I used to hold.
And I still hold that electoralism is the least effective form of civic engagement.
But I don't deny that it's effective.
At the bare minimum, it holds the line, right?
When you talk about change, that's a fluid concept.
That is a fluid concept.
If this person means we're never going to get to that deep systemic change that's going
to lead us to their utopian vision, and when I say utopian, that's not an insult.
I personally believe we should be trying to get to some utopian vision.
I don't think we should just throw our hands up and say this is the best we can do.
So if you're talking about it in that light, yeah, electoralism is not super effective.
And it's very unlikely that the current system is going to give you the tools to create a
new one.
To vote in utopia, it would take forever, because at some points and times, it would
be rolled back, right? So the thing that prompted this conversation is Roe. That's
what's bringing this up, the jeopardy that Roe is in right now. When you say
that it can't create change, sure if you're talking about that deep systemic
utopian kind, electoralism isn't the best tool. If you're talking about harm
reduction, mitigation, all of that stuff, it's kind of important. And as far as it
not being able to create change at all, I just have one question. These are the
types of civic engagement with loose definitions. Which one was used to bring
about the change we are all worried about right now.
Which one was used to put Roe in jeopardy?
Electoralism.
Some of this other stuff had a part in it, but it was all geared towards using the power
of the state to change the law.
They certainly didn't run a successful advocacy and education campaign, right?
It's super unpopular to overturn Roe.
They didn't do capacity building, I mean they used churches that already existed, but in
and of itself, they didn't develop the infrastructure to solve the problems, right?
They just want to legislate it, make it go away.
Wasn't direct service, I don't really even know how that would apply here.
wasn't research, wasn't personal responsibility, I mean we went over the
numbers about where clinic visits come from. Philanthropy, no, it was electoralism.
That's what brought about this change. Sure, you may not be able to use the
ballot box to vote in the revolution and bring about that perfect society you
envisioned, but it can stop harm. I mean that's plainly obvious. You can trash the
Democratic Party, you can trash the establishment of the Democrats and say
they should have done this or that or whatever, but let's be honest, if the
Democrats had been making the appointments the last four years, if
If they had appointed those last three judges, would we be having this conversation right now?
We wouldn't.
We would not.
If I had to draw a Venn diagram of those people who are worried about Roe being overturned,
right, that'd be the big circle.
Those who say electoralism doesn't work.
who are screaming about the Democratic establishment, they're all just little
circles inside the big one. The reality is Roe was changed through electoralism,
through a perversion of that, through the raw exercise of power once their
representatives got where they wanted them to. You can say it's the least
effective. You can say these other things are far more valuable when it comes to
addressing immediate needs or or creating long-term solutions or mitigating
harm. Yeah and I'd be hard-pressed to argue against that but the reality is it
It isn't totally ineffective.
At the bare minimum, it holds the line.
And if you just rail about electoralism
and you don't provide concrete steps that people can take,
you're not helping.
What you're going to end up doing
reducing voter turnout and making it worse.
If you want to get out there and talk about these other methods, these other types of
civic engagement, and promote them, I get it.
That at least I can understand.
But just screaming that the Democrats didn't do enough and that there's no reason to vote,
that's not helping.
And those people who are in the sights of the next ruling and the next one, they're
kind of counting on you not to do that, not to make it worse.
There are people who are going to be in harm's way because of this.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}