---
title: Let's talk about Abbott costing Texas while helping New Mexico....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Xy0a5DAEqAg) |
| Published | 2022/05/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor of Texas engaged in a political stunt at the border that backed up trade from Mexico, costing Texas $4.2 billion, including $240 million in spoiled produce.
- The stunt involved unnecessary truck inspections that had already been conducted by the federal government, resulting in zero drug seizures or stopping of undocumented individuals.
- As a response, the Minister for the Economy in Mexico diverted the TMEC Corridor Railroad away from Texas, causing a loss of billions with the new route through Santa Teresa, New Mexico.
- This rail line was initially planned to run from Mexico to Canada through Texas but will now bypass Texas entirely.
- Governor Abbott's actions along the border are causing severe economic damage to Texas, leading to significant financial losses.
- The governor's actions, aimed at exaggerating a situation for political gain, have backfired by draining money from Texans during a time of increased financial strain.
- The economic repercussions of Abbott's decisions will likely impact his chances of re-election, with potential primary challengers, independents, and Democrats expected to capitalize on detailing the financial losses incurred by the state.
- The fallout from these border moves will continue to escalate, with increasing scrutiny on the financial implications and political consequences.
- The diversion of the railroad away from Texas signifies a substantial blow to the state's economy and serves as a direct result of Governor Abbott's misguided actions.
- Beau concludes by reflecting on the significant financial toll inflicted on Texas due to Governor Abbott's costly and ineffective border policies.

### Quotes

- "The point of the stunt was to perform inspections on trucks coming up from the border."
- "Rather than going through Texas and adding to the economy there, it will now be going through New Mexico."
- "This is going to weigh heavily on Governor Abbott's re-election chances."
- "It's a method of convincing Texans to other and kick down."
- "The fallout from Governor Abbott's moves along the border will continue to grow."

### Oneliner

Governor Abbott’s costly border stunt leads to billions in economic losses for Texas, diverting vital trade routes and potentially impacting his re-election prospects.

### Audience

Texans, Voters, Border Communities

### On-the-ground actions from transcript

- Contact local representatives or organizations to advocate for responsible and effective border policies (suggested)
- Support primary challengers, independents, or Democrats who prioritize sound economic decisions and community well-being (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the economic repercussions and political fallout stemming from Governor Abbott's border actions in Texas, shedding light on the significant losses incurred and potential implications for future governance.

### Tags

#Texas #BorderPolicy #GovernorAbbott #EconomicImpact #TradeRoutes


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Texas and Mexico and New Mexico and Canada
and something that links them all together.
Well, it's no longer going to link Texas, but we'll get there.
Recently, the governor of Texas engaged in a little political stunt down on the border
that backed up trade coming from Mexico.
The estimate suggests that it cost Texas $4.2 billion to include $240 million in spoiled
produce.
It's a pretty big deal.
The point of the stunt was to perform inspections on trucks coming up from the border.
Inspections that had already been performed by the federal government.
It is worth noting that as far as has been reported, this 10-day stunt produced exactly
zero in drug seizures or stopping of undocumented people.
In what appears to be a response to this, the Minister for the Economy in Mexico has
decided that a TMEC Corridor Railroad will no longer go through El Paso.
It will avoid Texas completely and it looks like it will go through Santa Teresa, New
Mexico.
This is a loss of billions again.
How much, there's not an estimate yet.
That's a lot of addition.
This rail line will run from Mexico to Canada, and rather than going through Texas and adding
to the economy there, it will now, according to the Minister, be going through New Mexico.
The fallout from Governor Abbott's moves along the border will continue to grow.
The economic damage will continue to grow.
This is a huge chunk of money that Texas is losing out on.
So the governor can act as though he's actually doing something about a situation that in
many ways he has blown out of proportion.
It's a method of convincing Texans to other and kick down.
And it might have worked had it not cost so much money, had it not taken so much money
out of the pockets of Texans in a time when the average person needs more.
They can't afford more losses.
This is going to weigh heavily on Governor Abbott's re-election chances, and as the fallout
continues to be added up, I am certain there will be ads coming from primary challengers,
from independents, from Democrats, detailing exactly how much money he has cost the state.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}