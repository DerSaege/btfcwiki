---
title: Let's talk about Trump vs the state GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sMXq1aQVmvo) |
| Published | 2022/05/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump loyalists are aiming to control the Republican party at the state level by focusing on offices related to elections.
- Their attention is particularly on secretaries of state who didn't comply with Trump's request to find votes.
- Trump's move against state power structures faces resistance from states like Tennessee, North Carolina, Georgia, and now Nevada.
- In Nevada, despite Trump's endorsement, the state Republicans endorsed different candidates, showing a divide within the party.
- The state Republican parties are pushing back against Trump's influence by choosing candidates independently.
- Some view this resistance as a patriotic act against Trump's power grab, while others see it as political self-defense to maintain their own power.
- The ongoing struggle between the state GOP and Trump's machine is leading to resource depletion for both parties.
- Ohio's Republican candidate for governor successfully fought off three pro-Trump primary challengers.
- Despite media attention on certain Trump-endorsed candidates like Vance, overall, the state-level GOP seems to be conflicting with Trump's influence.
- The battle between the state GOP and Trump's faction is significant and continues to evolve.

### Quotes

- "They're making sure that they maintain their power, and that means undercutting Trump."
- "This struggle is sapping resources from both parties."
- "The state level GOP is at odds with the Trump machine."
- "It's just a thought."
- "Y'all have a good day."

### Oneliner

Trump loyalists target state GOP for control, facing resistance as state Republicans in various states push back, leading to a resource-draining struggle.

### Audience

Political observers

### On-the-ground actions from transcript

- Support state Republican parties in their efforts to maintain independence from Trump's influence (implied).
- Stay informed about the ongoing power struggles within the Republican party (implied).

### Whats missing in summary

Further insights into the potential long-term implications of this power struggle within the Republican party.

### Tags

#Trump #RepublicanParty #StateGOP #PowerStruggle #PoliticalResistance


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk a little bit more about the ongoing power struggle between
the Trump machine and the state GOP.
Each state has its own Republican power structure and they've been kind of at odds.
And we've been covering this for a while, but quick recap.
It appears that the Trump loyalists have decided their best route forward is to take control
of the Republican party at the state level.
Paying particular attention to offices within the state that have to do with elections.
They're paying a whole lot of attention to the secretaries of state in the various states.
You could infer from that that because there were certain Republican secretaries of state
that did not play ball with Trump's request to find votes, that he wants to make sure
that he doesn't run into that issue again.
You could be forgiven for making that assumption.
This move by Trump against the state power structures has been met with some resistance.
We have talked about how the state level Republican parties in Tennessee, North Carolina, and
Georgia have all bucked Trump in some way.
Have all taken his candidates, his endorsed candidates, and done something to lessen their
viability.
You can now add Nevada to this ever-growing list.
In Nevada, Trump endorsed Lombardo and Laxalt.
They were the presumed frontrunners heading into this little meeting where the state Republicans
make their endorsements.
And the state Republicans endorsed their opponents.
This isn't the primary there.
This is the state power structure of the Republican party throwing their weight behind certain
candidates in the lead up to the primaries.
The state power structure chose to go against Trump again.
This is another state where this is happening.
Now you could see this as something that might even be altruistic, and it may be for some
people.
There may be some people in state level GOP power structures who recognize Trump for what
he is, and they may be trying to stop him.
Another way of looking at that is that if Trump was successful in doing this, he would
be pushing the state power structure out of its position.
His people would be taking over.
So while for some people this may actually be like a patriotic thing, that they may be
resisting this move because they know where it could lead, for others, almost assuredly,
it's just political self-defense.
They're making sure that they maintain their power, and that means undercutting Trump.
The good news is that this little struggle that's occurring is going to be sapping resources
from both parties, from both groups, from the state level GOP and from Trump's machine,
because every loss he racks up, it undermines him across the entire country.
It's worth noting that on top of this, the Republican candidate for governor in Ohio
fended off three pro-Trump primary challengers.
If I'm not mistaken, the only notable Trump-endorsed candidate that's gotten anywhere has been
Vance.
And I know the media is going to spend a lot of time talking about that particular candidate,
but overall, it certainly appears that the state level GOP is at odds with the Trump
machine, and this is a wide-ranging thing.
So we'll continue to follow and see how this develops.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}