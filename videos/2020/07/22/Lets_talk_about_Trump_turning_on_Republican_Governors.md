---
title: Let's talk about Trump turning on Republican Governors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1q1q2KoRfw8) |
| Published | 2020/07/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump is turning on Republican governors, senators, and representatives who supported him.
- Trump is openly and publicly distancing himself from those who backed him up.
- Governors made policy decisions based on Trump's statements; for example, wearing masks.
- Trump is now following health experts' advice after months and 140,000 deaths.
- Governors who followed Trump's advice are now stuck as Trump distances himself from them.
- Trump will likely deny giving mandates or advice to these politicians.
- Governor DeSantis had to clarify that his advice on reopening schools was just a recommendation.
- Trump has a history of not taking responsibility for his actions.
- Trump's behavior with the Portland situation shows he will sell out anyone, including his allies.
- Those enabling Trump should be prepared for him to turn on them as well.

### Quotes

- "If you cannot read the writing on the wall, the president is selling these senators, governors, and representatives out very publicly."
- "None of this should be a shock."
- "He's going to sell you out. He's already started."

### Oneliner

President Trump is turning on his Republican allies, distancing himself publicly, leaving those who followed his lead stuck and at risk of being sold out.

### Audience

Politicians, Republican allies

### On-the-ground actions from transcript

- Stand on your own to preserve your seat (implied)
- Be prepared for Trump to potentially sell you out (implied)

### Whats missing in summary

The full transcript provides deeper insights into the consequences of blindly following Trump's lead and the importance of politicians standing on their own.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about how President Donald J. Trump is turning on Republican governors,
Republican senators, and Republican representatives.
Specifically those who backed him up, his allies, those who enabled him, those who listened
to him, who followed his talking points, and then made policy decisions based on those
talking points.
And he is turning on them, openly and publicly.
A lot of governors based their policy decisions on the statements of Trump, on the statements
coming out of the White House, to back him up.
When Trump said that some people were just wearing a mask as a signal of disapproval
of him, governors fell in line.
We don't need to do that, don't worry about it.
Today, I gladly wear a mask.
Use the mask, he says.
He said that anything that could potentially help is a good thing, in case governors can't
tell, that's keeping the schools closed.
He said it was going to get worse before it gets better.
Yeah, it is, because he's just now starting to follow the advice of the health experts.
140,000 people later, months.
And these governors, senators, and representatives who backed him up, now they're just stuck.
They are stuck.
And when they say that, well, we're following the president's advice, I don't think that
they're going to get the response that they're thinking they will.
They're going to be told, I didn't say that.
Yeah, sure, there's a photo of me with the guy, but I'm in a photo with a lot of people.
Maybe I was in a room with him.
I don't know him, we're not friends.
You know, Governor DeSantis is already having to backpedal in Florida, saying, no, no, no,
no, that wasn't a mandate to open schools back up.
It was just a recommendation, now that the teachers are suing him.
That's exactly what Trump's going to say.
Now I didn't tell the governors they had to do this, it was just a recommendation.
They did it on their own.
If you cannot read the writing on the wall, the president is selling these senators, governors,
and representatives out very publicly.
I wonder how they feel right now.
None of this should be a shock.
None of this should be a shock.
This is what he's done his entire life.
He won't accept responsibility for anything, he never has.
And those people who are enabling him now, when it comes to this goon squad from Portland,
everybody making that nationwide, understand that come election time, he's going to do
the same thing to you.
He's going to sell you out.
He's going to turn on you and say, no, no, no, no, no, we just offered our services.
They took it.
They backed us up.
Yeah, no, I was just following the lead of the acting secretary of DHS.
I didn't know anything about it.
And his base, the base you're counting on to get reelected, they're going to believe
them, not you.
If you want any chance of preserving your seat, you better start standing on your own,
because he will sell you out.
He's already started.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}