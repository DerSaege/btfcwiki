---
title: Let's talk about what would happen if Trump followed the manual....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xTNqT6LcW1o) |
| Published | 2020/07/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump administration is ignoring manuals and best practices in dealing with Portland, leading to mistakes and worsening the situation.
- Public perception drives Trump's response, but it is based on incorrect assumptions on managing movements like the one in Portland.
- Immediate results demanded by the public and politicians without understanding the situation can lead to further mistakes.
- Managing the situation in Portland requires patience and a long-term commitment, as advised by manuals.
- The goal should be to prevent escalation and violence, allowing grievances to be addressed in a calm manner.
- The response of using an iron fist concept always fails and is not effective in managing grievance-driven movements.
- More advanced texts beyond basic manuals are available to policymakers, developed by outside contractors and academics like RAND.
- Strategies like "hearts and minds" and "pacification" are recommended to secure the area and address grievances.
- Development efforts should focus on improving infrastructure, providing healthcare, education, and addressing income inequality.
- Implementing measures to improve living standards and address grievances is key to calming situations like the one in Portland.

### Quotes

- "Those who make peaceful revolution impossible make violent revolution inevitable."
- "No masks, no shields, no batons, no vans."
- "If the people want change, you give it to them."
- "It's not glamorous. It's not tough. It's not violent. But it wins."
- "This isn't a new theory."

### Oneliner

Trump administration's failure in Portland stems from ignoring manuals and best practices, requiring a patient long-term commitment and focus on addressing grievances non-violently.

### Audience

Policy makers, community leaders

### On-the-ground actions from transcript

- Address grievances calmly and non-violently (exemplified)
- Focus on improving infrastructure and addressing income inequality (exemplified)
- Provide healthcare, education, and relief to the community (exemplified)

### Whats missing in summary

Full understanding of the necessity for a long-term commitment and non-violent strategies in managing grievances.

### Tags

#Portland #TrumpAdministration #Grievances #Pacification #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Portland and it going nationwide and what would happen
if the Trump administration decided to read their own manuals.
I've talked about it the last few days.
Pointed out that they're ignoring the manuals or they haven't read them.
They're ignoring best practices.
They're ignoring their experts who get paid very, very well and they're making a lot of
mistakes because of it.
Some of the mistakes are actually making the situation worse.
Along the way I've had a lot of people ask me why I'm trying to help them.
Why I'm pointing this out.
I'm obviously on the side of the people in Portland so why would I want to point them
to the manuals?
Because the manuals don't say what you think they might say.
Trump's response is based on the public perception of what people think should be done in a situation
like this.
The public perception of how to manage a movement like this.
The problem is that perception is wrong.
I don't want to put too fine a point on this but every move that the Trump administration
has made without exception is explicitly advised against.
Every move.
This happens a lot in stability operations.
The public demands immediate results.
Therefore politicians demand immediate results without understanding what's going on.
And they get their immediate results.
Here.
This is not a conventional thing.
There is no 100 hour ground war here.
This takes time.
Patience.
It's like one of those finger traps.
The harder you fight against it, the stronger it gets.
In fact, if this was to go to the next level, which Trump's actions may take it to that,
the manuals say you need to be ready to commit for six years.
It takes time.
It takes time.
What we're going to do is we're going to talk about this as if it was happening overseas
rather than in Portland.
Because when we get to the end, there's some concepts that people are familiar with them
happening overseas, but when you talk about them happening in the US, the idea just kind
of eludes us.
It seems weird.
So we're going to talk about this as if it was occurring overseas.
Okay, so we're running a stability operation.
There's people in the streets.
They've been there 60 days.
What do we do?
Nothing.
Nothing.
You don't do anything.
Your goal is to stop it from escalating.
That's your goal.
Your goal is to stop it from turning into the I word.
You don't do anything.
You're there to stop it from becoming violent.
And I know there are people who are saying it's already violent.
No, it's not.
No, it is not.
These are the same people who think they're ready for a revolution.
In that context, no, it's not.
It's a calm Tuesday night that doesn't even make the news.
If you are dealing with a grievance driven movement, which this is, you have to allow
them to speak their grievances.
It's in the manual.
So you don't do anything.
Trump's response has been a concept called iron fist, to put it in quotes.
It fails.
It always fails.
It's not what you do.
The manual that I've been referencing up until today, that's the entry level course.
That's 101.
There are more advanced texts that are available for policy makers.
The manual I've been talking about is for the average soldier.
So everybody's on the same page and has a general idea of what's going on.
The more advanced texts that are actually put out by DOD, they're not publicly available
to my knowledge.
However, in something that can only happen in the United States, most of the information
contained in them isn't developed by DOD.
It's developed by outside contractors, civilians, academics, consultants, weirdos, to stick
with the language of the channel.
One of the big ones is called RAND, and they do most of the work on this particular subject.
I'm going to put a link down below that has, it runs through all of the different strategies,
and it talks about which ones and what combinations they should be used and all of that stuff.
We're at the beginning, before it actually goes into the I word, right?
The ones that are applicable here are called hearts and minds and pacification.
Those are the ones that would work here.
Now without getting too far into it, there are differences between these two, but they're
very subtle.
For our purposes today, they're the same thing.
Those are the overriding themes.
The idea here is that you would secure the area.
By secure, I don't mean clear.
I mean just secure it.
Make sure everybody stays safe.
Limit injury.
That's the idea.
Then you develop.
We'll get to what that means in a second.
While you're doing this, because it is a grievance driven movement, you allow redress of grievance
in the manual.
You allow that to happen.
Now me, because I am sympathetic to the people in Portland, I think all of their grievances
should be addressed.
Realistically when you're just talking about managing it, you really only need to do one
or two.
You need to do a good portion of their grievances, but you don't have to do them all.
So that keeps it from growing.
Now we get to the development.
This is where people gasp.
We're used to seeing it overseas.
If we're trying to win hearts and minds, pacify, what do we do?
What do we do?
Go to schools, hospitals.
We provide health care, education, infrastructure, fresh water.
That's what we do.
When it's overseas, for geopolitical reasons, we have no problem providing people with this.
But when we're talking about it in our own country, well then it's just, oh we can't
do that.
We do it overseas all the time.
Provide relief, feed the homeless, provide health care, provide health care, help with
economic development to address income inequality, raise the standard of living.
This is all stuff we do overseas during one of these campaigns.
But for some reason, we can't do it here.
And it's a bias.
See we're willing to address those things over there because we're like, well, you know,
they've got a backward government.
Of course their police needs reform.
Of course they don't have this stuff.
But here in the US, we're number one.
We're number one.
At what?
At what?
This country actually needs a lot of the infrastructure development that happens during stability
operations overseas.
If we addressed some of that, just as it happens overseas, it calms things down.
It increases the standard of living.
It makes people happier.
These are things that would need to be done.
If they were following the manuals, it's what they'd be doing.
I know it's not glamorous.
It's not tough.
It's not violent.
But it wins.
That's how you win something like this.
No masks, no shields, no batons, no vans.
That is explicitly advised against.
You build.
Because your goal is to stop it from becoming violent.
If the people want change, you give it to them.
Why?
Because those who make peaceful revolution impossible make violent revolution inevitable.
This isn't a new theory.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}