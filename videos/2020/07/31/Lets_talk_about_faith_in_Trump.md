---
title: Let's talk about faith in Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eJyov5ZykO4) |
| Published | 2020/07/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a topic rarely discussed due to its personal nature, focusing on faith and its impact on society.
- Expressing the importance of discussing faith when it negatively impacts others.
- Exploring the perception of Islam as a fanatical faith in the United States.
- Detailing the significance of the Hajj pilgrimage in Islam and the adaptations made this year due to the pandemic.
- Comparing COVID-19 fatality rates between Florida and Saudi Arabia, reflecting the seriousness of public health measures.
- Criticizing the blind faith and sacrifices demanded by political leaders in pursuit of economic gain.
- Contrasting the values of faith and human life in the context of pandemic response.
- Drawing parallels between blind faith in political leaders and cult-like behavior.
- Warning about the dangers of blind allegiance to leaders that prioritize personal gain over public safety.

### Quotes

- "Your belief in a creator or your lack of belief is not my business."
- "They're not the fanatics. They value human life. The real fanatics are here."
- "If you are one of those people who buy into that stereotype, I want you to take a look at Hajj."
- "That fanaticism, when it comes to a political leader in the United States, many times it borders on a cult."
- "But there are the faithful. Maybe you will be rewarded."

### Oneliner

Beau addresses the impact of faith on public health, contrasting the sacrifices made by some with the dangers of blind allegiance to political leaders prioritizing personal gains over public safety.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Research the significance of religious practices like Hajj to better understand different faith perspectives (suggested).
- Advocate for prioritizing public health and safety over political or economic interests (implied).
- Encourage critical thinking and questioning blind loyalty to political leaders (implied).

### Whats missing in summary

The full transcript provides a deep insight into the intersection of faith, public health, blind allegiance, and political influence in society.

### Tags

#Faith #PublicHealth #BlindAllegiance #PoliticalLeadership #CultBehavior


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about something we don't normally talk about on this channel.
There are very few topics that we shy away from, but one I don't talk about very often
because I feel it's personal.
Your belief in a creator or your lack of belief is not my business.
It's not anybody's business.
It's not up for public debate.
Normally.
I hold to that idea pretty well, but every once in a while a faith engages in an activity
that is a detriment to everybody else.
And at that point I feel it's necessary, I feel it's an obligation to talk about it.
So that's what we're going to do tonight.
We're going to talk about the most fanatical of all faiths.
Now if you ask people in the United States that question, what's the most fanatical faith,
you're going to get an answer pretty quick.
For most people it's a stereotype, it's unfair, it's inaccurate, it's wrong.
I'm not going to do much to dispel that stereotype right now.
The people of that faith, because the stereotype is wrong, I don't think they're going to mind
because this is in pursuit of saving lives.
And they value human life.
So I don't think they're going to care.
You ask that question in the United States, somebody's going to say Islam.
Islam has five pillars, five things you have to do.
One of them is called Hajj.
It's a pilgrimage, it's a journey.
It's a big deal.
By big deal I mean if you are financially and physically capable, you have to make this
trip once in your life.
It is a big deal.
You are literally walking in the footsteps of the prophets and engaging in reenactments
along the way.
It's a big deal.
In an average year, two and a half million people do this.
This year, a thousand, hand-picked.
Under normal circumstances, one of the things that they do involves pebbles.
This year, those pebbles will be in a plastic bag and sanitized.
Drinking from wells is part of it, but not this year, bottled water.
Communal meals is a big part of it.
Individually packaged and you will be socially distanced.
This is a faith that most people in the United States would see as the most fanatical of
all.
They are making major accommodations to a pillar of their faith because of a public
health issue.
That might be why in the state of Florida, where we have a population of about 21 million,
we have lost 6,332, whereas in Saudi Arabia, where the population is 33 million, 50 percent
more, they have only lost about 2,800.
Those two things are probably related, how serious they are willing to take it.
The most dangerous people in the world are true believers.
I know of no larger fanatic than an American who has been told it is time to sacrifice
by a leader from their chosen party, especially if that sacrifice is in pursuit of the almighty
dollar.
Because the ruling class in this country have done an extraordinary job of training a large
segment of the working class that their identity is their job.
That's their value.
That's who they are.
You are measured by how much you can produce for this economic system, and if you can't,
you don't really even deserve to eat.
I'd like to point out that one of the other five pillars is charity.
But see, in the United States, we've got that edict by tweet now.
And their god, Donald Trump, has instructed them it's time.
It's time to reopen the economy.
Get back out there.
Send your kid off to school.
And there are a whole bunch of the faithful who are willing to walk into a market or a
restaurant or a church or put a backpack on their kid and send them into a school.
I hope you're getting the imagery, because from where I stand, I don't see much of a
difference.
That's faith.
That is faith in God, Emperor Trump right there.
If you are one of those people who buy into that stereotype, I want you to take a look
at Hodge.
Find out how important it really is, and then take a look at the steps they're taking to
keep people safe and compare that to the steps that your god is willing to take or not take
for the safety of your kids, their teachers, and our communities.
They're not the fanatics.
They value human life.
The real fanatics are here.
You know, that fanaticism, when it comes to a political leader in the United States, many
times it borders on a cult.
And when that leader needs a bump in the polls or doesn't want to admit they're wrong, it
turns into a death cult very, very quickly.
And that's where we're at.
This isn't an experiment.
We know what's going to happen.
But there are the faithful.
Maybe you will be rewarded.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}