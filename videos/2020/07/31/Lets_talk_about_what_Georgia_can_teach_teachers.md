---
title: Let's talk about what Georgia can teach teachers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_VWrBblE24A) |
| Published | 2020/07/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation at a summer camp in Georgia where out of 597 attendees, 260 tested positive within four days, with the highest positive tests in the 6-10 age group.
- Dispels the idea that children are not impacted by the virus as about three quarters showed symptoms.
- Advises teachers, especially in areas with districts functioning as usual, to prepare like they are going on a combat deployment, suggesting they organize their affairs and have a designated person know where all their critical documents are located.
- Urges people to always wear protective equipment, drawing a parallel to soldiers removing their back plate because it was "cool," leading to dangerous consequences.
- Encourages community members with influence to advocate for online learning as the lowest risk option according to the CDC guidelines.
- Recommends being proactive in influencing school boards and districts to prioritize safety over following guidelines that pose moderate risks.

### Quotes

- "Treat [preparations] like a combat deployment."
- "Wear your protective equipment at all times. Doesn't matter how annoying it is. Doesn't matter how hot it makes you. Wear it."
- "We know what's going to happen. We don't need to reproduce the results."
- "If you're active in your community and you have any sway over the school board, the school district in your area, now's the time to use it."
- "Even with the guidelines, they're saying it's moderate risk."

### Oneliner

Beau advises teachers in high-risk areas to prepare for teaching like a combat deployment, stresses the importance of protective gear, and urges community advocacy for online learning amidst rising COVID cases.

### Audience

Teachers, Community Members

### On-the-ground actions from transcript

- Advocate for online learning with school boards and districts (exemplified)
- Prepare critical documents and share their location with a trusted person (exemplified)

### Whats missing in summary

Importance of advocating for safety measures and preparations in high-risk areas.

### Tags

#COVID19 #Education #CommunityAdvocacy #ProtectiveGear #SafetyPrecautions


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about what the state of Georgia, a summer camp in Georgia,
can teach teachers, specifically those who are in areas where the districts have decided
to go about business as usual.
We'll talk about what happens.
First thing you need to know is that at this camp, quote, all counselors and campers attending
passed all mandatory screenings.
The next thing you need to know is that four days later, within four days, out of the 597
residents who attended the camp, 344 were tested and 260 were positive.
The age group with the most positive tests was 6-10 years old.
Says about three quarters showed symptoms.
That kind of dispels the notion that children won't be impacted by this.
Now one of the things that I have noticed, since we're talking about preparations, is
that a lot of teachers are getting their affairs in order.
Filling out wills and stuff like that.
That's a good idea.
You should do this stuff anyway, have it done ahead of time.
But, if I was a teacher, I would suggest you treat this like a combat deployment.
In addition to your will, you need to put together an envelope that is in the event
of.
Inside, you need to have a list of all of your passwords, specifically those related
to your banking.
Copies of any keys, storage sheds, lock boxes, anything like that, that people may not have
access to.
All important documents, copies of them.
Anything you need to say.
Have it all put together.
And have one person whom you trust designated to know where it's at.
Yeah, treat it like a combat deployment.
And in that sense, there was a time, this isn't an issue anymore because people have
learned, there was a time when it was the cool thing to take your back plate out.
Because you're not going to run away.
Soldiers would remove their protective equipment on the back.
It never failed that it was that guy who was looking the wrong way when running a T around
a corner and caught around in the back.
Wear your protective equipment at all times.
Doesn't matter how annoying it is.
Doesn't matter how hot it makes you.
Wear it.
Don't become complacent.
I really feel for y'all.
I really do.
Now normally I don't do this, but I just feel like I should in this case.
The shirt that I'm wearing right now, it's not one from our store.
I'm going to put a link down below because it has a lot of subtle detail related to the
topic at hand.
If you're active in your community and you have any sway over the school board, the school
district in your area, now's the time to use it.
It's not an experiment.
We know what's going to happen.
We don't need to reproduce the results.
The guidelines that are there, if they are not followed to the letter, this is what's
going to happen.
Even with the guidelines, they're saying it's moderate risk.
The lowest risk is online, doing everything online.
That's the lowest risk.
That is per the CDC.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}