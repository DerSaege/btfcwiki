---
title: Let's talk about how that tweet can't happen here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yqLqj8mgUzc) |
| Published | 2020/07/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Murphy's Law and its relevance to the President's intent.
- Points out that the diversion being ignored might be the main attack.
- Raises concerns about the President's tweets being dismissed as distractions.
- Emphasizes the importance of not overlooking the President's intent in delegitimizing the election.
- Conveys that the President's actions are about power and self-interest, not democracy.
- Warns about the potential consequences of not taking the situation seriously.
- Urges people to realize that what they think can't happen actually can.

### Quotes

- "Accept this for what it is. He's telling you his intent."
- "At the end of the day, President Trump wants the same thing that every first term president wants. A second term."
- "He does not care about democracy. He does not care about the republic. He doesn't care about your voice. He never did."
- "Because we thought the same thing they thought. It can't happen here. But it can."

### Oneliner

Beau explains Murphy's Law and warns not to overlook the President's intent in delegitimizing the election, pointing out his focus on power over democracy.

### Audience

American citizens, voters

### On-the-ground actions from transcript

- Challenge attempts to delegitimize the election (implied)
- Take a stand for democracy and the republic (implied)
- Stay informed and engaged in current events (implied)

### What's missing in summary

The emotional urgency and call to action in response to the President's potential threats to democracy.

### Tags

#MurphysLaw #President #Intent #Democracy #Election


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about what Murphy, of Murphy's Law, can teach us about the President
and his intent.
Most people are familiar with Murphy's Law, the main one.
If it can go wrong, it will go wrong.
That is certainly the most famous.
The thing is, Murphy had a bunch of laws.
Murphy was a grunt.
Most of his laws dealt with that.
They can be applied to a whole lot of situations.
And one of his laws that really kind of always stuck with me is the one that says the diversion
you are ignoring is the main attack.
Right now you have a bunch of people, a bunch of people saying not to worry about that tweet
that he doubled down on at his press conference.
Don't worry about it.
He's just rage tweeting.
He's just trying to get the liberals stirred up.
It's a diversion.
It's a distraction.
Maybe.
And then there's a bunch of people saying, well, you know, even if he did delay the elections,
he'd leave power.
That's not how it works.
See, according to the Constitution and the law, this is what's supposed to happen.
Yeah, that's what it says.
That is what it says.
Those words on paper say that.
Those words that are in the same document that are supposed to protect us from unreasonable
search and seizure, the same document that gives control of immigration to the states,
not the feds, the same one that says you can peaceably assemble, the same one that protects
the press as journalists are deliberately targeted in the street.
Those words on paper mean nothing if people don't follow them.
When you have many Republicans giving lukewarm responses to this, well, it's probably not
a good idea.
Probably doesn't cut it.
Probably isn't enough.
I get it.
I get it.
At the end of the day, it's just a tweet.
Doesn't matter.
The law says he can't do it.
I understand.
It can't happen here.
Right?
Don't take that stance.
Accept this for what it is.
He's telling you his intent.
He's trying to delegitimize the election.
That's what he's doing.
It has nothing to do with people talking about it being safe as if it's really him being
concerned about the public health issue.
It's not.
He's talking about voter fraud.
The 14th characteristic.
That's what it's about to him.
It's about delegitimizing the election.
He says it.
The safety and security thing?
That's just window dressing.
To get you to agree to a delay.
If there is a delay, there will be no election.
At the end of the day, President Trump wants the same thing that every first term president
wants.
A second term.
He's come to the conclusion that he's probably not going to get that at the polls.
So there's going to be another way to do it.
There's got to be something else he can do.
Delay it.
Declare an emergency.
Use the playbook he's been following for three years.
Don't blow this off.
This is him exposing his face.
Exposing who he is.
He does not care about democracy.
He does not care about the republic.
He doesn't care about your voice.
He never did.
And it doesn't matter what side of the aisle you're on.
He cares about power and himself.
Just like every other wannabe dictator.
This is the warning that if it isn't heated, historians are going to look back on.
And just the way we look back on the rise of others throughout history, they're going
to sit there, how did they let this happen?
Because we thought the same thing they thought.
It can't happen here.
But it can.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}