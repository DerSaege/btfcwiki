---
title: Let's talk about Houston and intelligence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PC-0El3LvUw) |
| Published | 2020/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the relationship between diplomacy and intelligence work, mentioning that every embassy engages in intelligence work and has spies.
- States that the Chinese government was spying on the US, particularly at a specific consulate.
- Suggests that the intelligence agencies' choice to make arrests rather than run a disinformation campaign was politically motivated.
- Notes that foreign intelligence services are spying on US medical research due to the public health crisis and Trump's mismanagement of it.
- Emphasizes that monitoring the research process gives other countries insight into how long the US forces could be degraded.
- Points out that the national security costs of Trump's errors in managing the crisis are significant and will have long-term consequences.
- Mentions that other nations are trying to exploit the weakness displayed by the US administration in handling the crisis.
- Concludes by stating that the concern is not about medical professionals sharing information but about disrupting the research process to make the US more vulnerable.

### Quotes

- "Every embassy ran by every country, everywhere in the world, engages in intelligence work."
- "The idea that the Chinese government was spying on us, that's a fact."
- "Trump's failure to manage this showed exactly how vulnerable the United States is to this type of warfare."

### Oneliner

Diplomacy and intelligence work are intertwined, with foreign nations exploiting vulnerabilities in US response to the crisis for their advantage.

### Audience

Policy makers, diplomats, activists

### On-the-ground actions from transcript

- Monitor and safeguard medical research facilities (suggested)
- Advocate for improved crisis management strategies (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of how foreign intelligence services exploit vulnerabilities in US response to the crisis, urging for increased vigilance and strategic improvements.

### Tags

#IntelligenceWork #ForeignEspionage #CrisisManagement #NationalSecurity #Diplomacy


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're gonna talk about Houston and intelligence.
We've got three questions coming in,
two are specific to Houston,
the third is related but kind of separate.
The first two questions are basically,
is the Trump administration's narrative on this accurate?
Is it true?
And the other question is,
is the Trump administration's narrative on this
politically motivated?
The answer to both of those is probably yes.
Um, if you were to go back in time
and listen to a diplomat advising Caesar,
you would hear that diplomat try to explain
the intent of whoever they're talking about.
Try to explain what the other side
is going to do.
That, summed up, that's intelligence work.
Intelligence work is about discerning the intent
of the opposition group.
So, diplomacy and intelligence work,
they go hand in hand and they have forever.
Every embassy ran by every country,
everywhere in the world, engages in intelligence work.
They all have spies there.
Every single one of them.
There are no exceptions to this.
Um, the history of giving a spy a diplomatic cover
goes back forever as well.
If you ever meet somebody who is an attache at an embassy,
just assume they're a spy.
That's not always true,
but you're gonna be right more than you're wrong.
Um, this is especially true if they are a legal attache.
They're pretty much all spies.
So, the idea that the Chinese government was spying on us,
that's a fact.
The idea that it was happening at that specific consulate
in the way it was being described, 99% fact.
Was it politically motivated?
Probably, probably, because this is a game.
You don't hear about spies getting arrested very often
because it's always preferable
to feed the opposition government bad information.
That's always a better course of action over arresting them.
So, the fact that the intelligence agencies
chose to make arrests rather than run
a disinformation campaign,
that was probably politically motivated.
But, yeah, it was probably happening.
The Trump administration has chosen,
we've already talked about it in that video,
let's talk about how Trump could win.
They've chosen China as the external enemy.
They're gonna ratchet up tensions any way they can.
This is a way to do that.
But that doesn't mean that the Chinese weren't spying on us.
They most certainly were.
Just the way it was handled
may have been politically motivated, probably was.
It's really unlikely that they had all of this information
but couldn't figure out a way to exploit that information
without arresting them.
That doesn't seem likely.
It's possible, but it's not really likely.
Okay, so the side question here
is why are foreign intelligence services
spying on our medical research right now
because of the public health crisis?
The current crisis and Trump's mismanagement of it,
his failure to contain it, his failure to control this,
it's not just a public health issue.
It's a national security one.
Any country that is opposition
or may become opposition to the United States
at some point in the future, they're taking notes.
They are taking notes. They're writing down.
They're learning everything they can about our response
to include the research and how that research is conducted.
They're not really concerned with the final product.
That's probably going to be shared anyway.
They want to know how it's done
because they're trying to determine a window.
There are very, very, very few countries
that can even remotely hold a candle
to the United States military going toe to toe.
They have to find some way to level the playing field.
Trump's failure to manage this
showed exactly how vulnerable the United States
is to this type of warfare.
All an opposition group has to do is wage a troll campaign,
casting doubt on medical experts and education in general.
Given our cultural bias against intelligence and education,
this is really easy.
So they run a disinformation campaign
at the same time they release something,
and we're crippled. We're hit.
Then monitoring the research process
gives them a window into how long we would be,
our forces would be degraded by this.
So that's what they're trying to figure out.
Most times when you're talking about intelligence work,
the information itself doesn't matter.
It's not about what's found out that's secret.
It's how the information was developed or obtained.
Right now, they're not really concerned
about the final product of the research.
They're trying to figure out how the research is conducted
so they can disrupt it if they need to,
so they can increase that window.
Because of the human cost of Trump's errors in this regard,
that's what everybody's focusing on.
The national security costs of Trump's errors
in managing this are huge, are huge,
and it's something that we're going to be paying for
for decades.
We're going to have to spend tons of money very publicly
so other nations know that we've improved our abilities,
when the reality is our abilities
were pretty good to begin with.
The administration just didn't use them.
So that's why they're concerned with the research.
It's not that medical professionals aren't sharing
and aren't cooperating, really.
It's more about them trying to figure out
how they can disrupt the research process
to make us more vulnerable,
because the administration showed the underbelly
of a national defense issue.
It openly displayed a weakness.
And every opposition country in the world
or countries that may become opposition,
they're trying to figure out how to exploit that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}