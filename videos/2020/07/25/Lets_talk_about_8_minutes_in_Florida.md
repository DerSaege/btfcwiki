---
title: Let's talk about 8 minutes in Florida....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C4Y_0NYDCjk) |
| Published | 2020/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the challenges of governing Florida, a state with a diverse population and significant distrust of government.
- Describes the severity of the COVID-19 situation in Florida, with hospitals overwhelmed and rising cases.
- Notes the lack of effective leadership and messaging in handling the crisis, particularly in South Florida.
- Criticizes Governor DeSantis for his ineffective leadership and alignment with Trump, pointing out his absence during the crisis.
- Emphasizes the importance of honest messaging, acknowledging the severity of the situation, and promoting safety measures like wearing masks and staying at home.
- Praises county school districts for prioritizing virtual learning over returning to normalcy.
- Urges for a change in messaging and leadership to save lives and prevent further escalation of the crisis in Florida.

### Quotes

- "It's that bad. It's not a manufactured narrative."
- "We're losing a person on average every eight minutes and 20 seconds."
- "The best leadership we have is not coming from state government. It's coming from Walmart."
- "Governor DeSantis' little speech was probably premature."
- "It's not a media fiction. It's not a manufactured narrative. It is that bad."

### Oneliner

Florida faces a severe COVID-19 crisis with overwhelmed hospitals, lacking leadership, and a need for honest messaging to save lives.

### Audience

Floridians, Public Health Advocates

### On-the-ground actions from transcript

- Support county school districts opting for virtual learning (exemplified)
- Wear masks, stay at home, practice good hygiene (implied)
- Advocate for honest messaging and leadership to address the crisis (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the COVID-19 situation in Florida, criticizing the government's response and urging for better leadership and messaging to tackle the crisis effectively.

### Tags

#Florida #COVID19 #Leadership #Messaging #GovernorDeSantis #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about leadership messaging
and eight minutes in Florida.
Stick with me on this one.
Somebody asked me, you know, because we are in the news
right now a little bit, y'all might have heard about
what's going on down here,
asked if it was really that bad.
Yeah, yeah, they're not making it up.
They're not making it up, it is.
But before we get into that, I want to acknowledge
the difficulties of governing a state like Florida.
Because there's the image of Florida,
the image that a lot of people have,
which is beaches and parasailing and restaurants and hotels.
And yeah, that's the coast, that's the coast.
From the shoreline, 20 miles inland,
pretty much the entire state.
Once you get past that 20 miles, that's not Florida.
Florida is populated by a fiercely independent people
who have more distrust of government
than Appalachia Hill folk.
So I want to acknowledge the difficulties
in governing a state like this.
That being said, where I am, yeah, it's bad.
We're hit.
We've got a pretty large number of cases.
We've already had quite a few that had the worst outcome.
There are hospitals nearby that are on divert,
can't take any more people.
So yeah, I would say it's pretty bad.
The thing is, where I am, that's not the area in the news.
We're not the area being talked about.
That's South Florida.
Where I am, nobody's really mentioning it.
It's that bad here in South Florida.
Yeah, it's bad.
It's bad.
It's not a manufactured narrative.
The reason it is this bad is not really
because DeSantis didn't issue stay-at-home orders or mask
mandates or whatever.
Because it's Florida.
Nobody would have listened to him anyway, to be honest.
I think everybody knows.
I'm not a fan of DeSantis, and you'll certainly know that
by the end of this video.
But that in Florida, not going to be as effective as it might
be in other states.
It's just different here.
What matters in Florida is leadership, messaging, and
getting out information, telling people to actually trust the
experts, not just using one quote, but telling them all of
the guidance that went along with that quote.
That's what matters here.
We're not getting that.
The closest thing we got to any real leadership came from Rick
Scott by accident.
He was being a good Republican foot soldier, out there
staying on message, on brand.
We need to get those kids back to school.
Oh, my grandkids?
Oh, no, no, no, no.
They're doing distance learning.
Ain't no way.
That's what Florida needed to hear, more than anything.
An acknowledgement that no, we're not getting back to
normal.
No, everything is not okay.
And while we're on this subject, I want to say thank
you to all of the county school districts that have
collectively raised their middle finger to Tallahassee, and who
are doing the virtual stuff.
It's a good move, and it also illustrates the point about
governing Florida.
Even the governments, the subordinate governments in
Florida won't listen.
The peoples certainly won't.
What matters is messaging.
Getting the right information out there.
The problem is, early on, the government, the state
leadership, in quotes, they declared victory.
DeSantis went out there and berated the media, as if the
media had created the studies and was personally
responsible for all of this.
They're like, you said that it was going to be two weeks.
In two weeks, Florida would be the next New York.
Well, here we are eight weeks later, and just nobody wants
to admit that I got it right, and my state's doing
wonderful.
It's challenging your narrative, so you have to create
a boogeyman, so there's black helicopters circling the
Department of Health.
He actually said stuff like this.
I'll put a clip down below.
Well we are not eight weeks out from that.
We don't have black helicopters circling the Department of
Health, but what we do have are a bunch of black Hertz's
circling South Florida.
And where is the governor?
In DC, with Trump.
That's a bold move, reminding everybody that he's just in
lockstep with Trump, following his directives, like a good
little boy.
There's no way that's going to become a campaign ad come
election time.
Governor AWOL.
I mean, I see it coming.
I don't know.
That was bad optics.
That was bad political imaging there.
If he decides to return to the state at some point, I would
strongly suggest that he acknowledge things are not
getting back to normal.
The idea is, right now the information says, well, it's
going to plateau in two weeks.
Sure, that's about the time the governor wants to force
all the kids back to school and open everything back up.
No, it's not going to plateau then.
It's going to get worse.
It would probably be a good idea for the governor to show
that he's wearing a mask.
First, he has to learn how to put one on.
That was embarrassing.
I don't know how you can live in Florida and not know how to
put on an N95.
It's like he's never done hurricane cleanup before.
But putting on a mask.
Telling people, no, everything actually
isn't back to normal.
You should probably stay home.
Wash your hands.
Don't touch your face.
If you have to go out, wear a mask, all that
stuff, and stick to it.
Stop trying to be so positive that you're not really telling
the truth.
It's bad.
It's out of hand.
When we're getting updates in tweet format about when the
next shipment of Redempsevir is, that's probably a sign
that things are a little worse than the governor's letting
on.
In a lot of communities, when you're talking about Freeport
Florida, or Ocala, Lake City, Waldo, Stark, places like
this, places people have never heard of outside of Florida,
they're not going to listen to a mandate.
They're going to listen to leadership.
And they're going to look to see what those in
power are doing.
That's why Rick Scott's accidental
statement is so important.
That's what they're going to go off of.
And we don't have that.
We don't have any leadership.
Right now, in the state of Florida, we are losing a person
on average every eight minutes and 20 seconds, which is now.
While you watch this video, somebody died.
That's how bad it is.
And we have no leadership.
The governor is AWOL.
The best leadership we have is not coming from state
government.
It's coming from Walmart, which is concerning, given the fact
that Walmart is not really known for their social
responsibility.
The state government has to stop pretending that they
weren't wrong.
That's the first step in this.
They have to get out and just admit it.
Yeah, it's bad.
We are the next New York.
Governor DeSantis' little speech was probably
premature.
Now we have to act on it.
We have to stay home.
We have to do all of the stuff we were doing before,
because we opened up too soon.
We're back to the beginning.
Forcing it to happen?
Yeah, sure, try, whatever, I guess.
It's not going to matter, not really.
Not if the messaging doesn't change.
If the governor acts like he's being forced into issuing
these orders, nobody's going to listen to him.
He doesn't even have to issue them.
And he can just say, yeah, we messed up.
This is a real thing.
Trump's wrong.
We've got to take action.
Wear your mask.
Stay at home.
All the stuff that we had to do before, that's what matters.
That's what's going to save lives.
Or he can do nothing.
He can stay AWOL.
The people of Florida will figure it out.
Once enough holes get dug, we're probably
reaching that point.
So to answer the question, yeah, it's that bad.
It's that bad in Florida.
It's not a media fiction.
It's not a manufactured narrative.
It is that bad.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}