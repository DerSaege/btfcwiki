---
title: Let's talk about opening schools, talking points, teachers, and counting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rEEPvrwR5vE) |
| Published | 2020/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the administration's push to get all students back in person as a magic trick to create an illusion of normalcy and economic improvement.
- Large school districts have refused the push for in-person learning and opted for distance learning, showing independent thinking.
- The argument that students need socialization is countered by the rigid guidelines turning schools into prison-like environments.
- Beau provides evidence from Arizona, where three teachers contracted COVID-19 during summer school, with one teacher succumbing to the virus.
- He challenges Governor DeSantis to specify the acceptable number of teachers who can die before reconsidering in-person schooling.
- Beau argues that the talking point of returning to normalcy through in-person schooling is debunked, as the risks and inability to meet safety guidelines remain.
- He suggests alternative methods like online learning to protect teachers and students from unnecessary risks.

### Quotes

- "It's what the president is really hoping to achieve here."
- "They're going to open up using distance learning, and that's smart."
- "Turns it into a prison."
- "I have evidence."
- "The talking point is gone."

### Oneliner

Beau exposes the illusion of returning to normal through in-person schooling, advocating for safety and alternative teaching methods in the face of COVID-19 risks.

### Audience

Parents, Teachers, Community

### On-the-ground actions from transcript

- Advocate for safe learning environments in schools (implied)
- Support distance learning options for students and teachers (implied)
- Challenge local leaders to prioritize health and safety in educational decisions (implied)

### Whats missing in summary

The emotional impact and urgency of prioritizing safety and evidence-based decision-making in educational policies during the ongoing pandemic.

### Tags

#Education #COVID19 #Safety #OnlineLearning #CommunityConcerns


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about talking points,
transitions, schools, and magic,
because that's what this is.
Illusion, sleight of hand.
It's what the president is really hoping to achieve here.
The push to get all the students in person,
to get them all back in person, it's a magic trick.
The hope from the administration is
that if everybody goes back, then the average American
is going to feel like things are getting back to normal,
that we're back on track, that the economy is
going to get better, that everything's going to be OK,
and we are ready for that transition to greatness.
That's the plan.
That's why the push is there.
The good news is that a whole bunch of school districts,
large ones, have already said, no, we're not doing it.
That's a bad idea.
We are not doing it.
And that's a good thing.
They're going to open up using distance learning,
and that's smart.
Apparently teachers are better educated than, well,
that's not really a surprise.
So now that these large districts have already said
they're not doing it, that talking point is gone.
The president is not going to be able to say that because there
are large school districts that aren't doing it,
that are thinking for themselves.
That means there should be no reason for governors who just
want to obey dear leader to continue to push it.
The talking point is gone.
There's no reason to risk this now.
There is no reason to risk it.
No matter what, he's not going to be able to say that.
So there's no reason to take the risk, especially now
that districts have looked at what they need to do to reopen,
and they're like, we can't do this.
And for some of them, it's literally impossible.
They don't have the money.
The whole argument here is that, well, students,
they need to be socialized.
We're robbing them of that.
Have you actually looked at what they're supposed to do when they reopen these
schools?
Turns it into a prison.
No changing classes.
Meals are to be eaten in the classroom.
Everybody's six feet apart.
Plastic dividers up everywhere.
Paint on the ground so they know how far to stay apart.
Stay in line, inmate.
It turns it into a prison.
I don't know that I want my child socialized in that manner.
The other talking point here is that, well, we don't know.
There's really no evidence that it's going to go bad,
that there will be problems.
We don't know that for sure, so let's just risk it.
I have evidence.
In Arizona, they had summer school.
Three teachers.
They end up sharing, kind of rotating through this classroom.
All of them appropriately socially distanced, wearing a mask,
hand sanitizer, everything, doing everything right.
All three of them caught it, one of them's dead.
My evidence is Kimberly Bird, a teacher for 38 years who is no longer with us.
That's my evidence.
So before we start this in Florida, I want Governor DeSantis to give us a number.
Exactly how many teachers are we cool with putting in the ground?
That way, when we reach that number, and we will, we can stop.
I wanna know exactly how many teachers we're okay with losing.
There's got to be some standard here, right?
The whole push behind this is for
a talking point that we're getting back to normal.
Talking point is gone.
The magic trick is ruined.
Everybody's gonna know.
There's no reason to do this.
We can't meet the criteria.
We can't follow the guidelines.
We are clearly still in a state where cases are rising.
The talking point is gone.
It is definitely a risk.
There is no reason to do this.
And we have an alternative.
If only there was some other way to teach children how to count.
One, two, three, ah, ah, ah.
They could count how many of their teachers they lose because of
DeSantis' policies.
And for those that don't wanna watch Big Bird, they can,
you know, go online as they had been before.
Is it ideal?
No, it's not.
But it's better than their field trips being to cemeteries.
The talking point is gone.
It is not going to appear that we're getting back to normal as bags get filled.
There's no reason to risk this.
There is no reason to risk this.
None.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}