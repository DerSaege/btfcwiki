---
title: Let's talk about Trump being right, kind of....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ePu5YHehKSM) |
| Published | 2020/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump made a statement regarding the Confederate flag in the South, calling it a free speech issue.
- Some individuals associate the Confederate flag with a romanticized image of the pre-war South, not with its history of oppression.
- Many people fail to connect the Confederate flag with its association to slavery, civil rights opposition, and systemic racism.
- The ability to ignore the history and pain associated with the Confederate flag is considered a form of white privilege.
- White privilege doesn't imply an easy life for white individuals, but rather the absence of additional challenges due to their skin color.
- Viewing the Confederate flag without acknowledging its dark history showcases a form of privilege rooted in skin color.
- White privilege doesn't negate personal struggles or hardships but indicates that those struggles aren't a result of being white.
- The privilege to romanticize the Confederate flag without considering its painful history is a form of privilege that some may find hard to grasp.

### Quotes

- "The ability to look at the Confederate flag and picture dances, picture a belle of the South, and not picture the abject horrors of slavery, that is white privilege."
- "White privilege doesn't mean that your life is easy simply because you're white. It means that it's not harder because you're white."
- "Viewing the Confederate flag without acknowledging its dark history showcases a form of privilege rooted in skin color."

### Oneliner

Trump's statement on the Confederate flag reveals how white privilege allows some to overlook its painful history.

### Audience

Community members

### On-the-ground actions from transcript

- Challenge misconceptions about symbols with historical ties (implied).
- Educate others on the history and implications of symbols (implied).

### Whats missing in summary

The full transcript delves deeper into the concept of white privilege and its manifestations in society.

### Tags

#WhitePrivilege #ConfederateFlag #History #Racism #Community


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump being right about something.
Broken clock and all that.
Doesn't happen often, but when it does, we're going to talk about it.
He gave an interview today.
It's definitely worth watching.
It was a doozy.
There are a bunch of statements in it that are probably going to make the news tonight.
The thing is, one of his statements, I think it's going to be overshadowed by a lot of
the other more Trump-esque statements.
And the thing is, I think it's the one that can teach us the most.
Because when I first heard him say it, I was like, that's not true, rolled my eyes, and
then the more I thought about it, the more I'm like, no, he's right.
He is absolutely correct.
He was asked about the whole debate over the flag here in the South.
And he said it was a free speech issue and gave all the normal stuff that went along
with it.
But then he said, well people love it, and I know people that like the Confederate flag,
and they're not thinking about slavery.
And at first, you're like, what else would they be thinking of?
And then you think about it.
Some of them genuinely aren't.
That's not what they're picturing.
They're picturing the pre-war South.
They're picturing houses with big columns and long dresses and a very genteel way of
life.
That's what they're picturing.
That's what they associate it with because of old movies.
They don't see that symbol and associate it with hundreds of years of oppression.
They don't associate it with opposition to civil rights and all of the racist policies
of the day, many of which still continue in practice today.
They don't associate it with a lack of generational wealth.
They don't associate it with the fact that it led many people in a certain demographic
to start out behind.
So they wound up in poverty, and then they can't escape poverty because of some of the
systemic issues.
So they're stuck in it.
And the fact that they're in poverty decreases their standard of living, their health care
access.
It increases the amount of unjust violence that's visited upon them by law enforcement.
It yeah.
But that's not what they're picturing.
They're picturing something else.
They can ignore all of the history that goes along with that symbol.
They have that privilege.
They can do that because of their skin tone.
They can ignore history because it didn't impact them because of their skin tone.
They have the privilege of ignoring history because of their skin tone.
You might call that white privilege.
If you can look at that symbol and not see the pain, despair, and destruction that it
has caused throughout American history going back to the 1860s, you have a privilege because
it didn't directly impact you.
And it doesn't impact you today.
That is white privilege.
White privilege doesn't mean that your life is easy simply because you're white.
It means that it's not harder because you're white.
You don't suffer a negative bias because of your skin tone.
That's what it means.
Doesn't mean that you didn't struggle.
Doesn't mean that you're not poor.
Doesn't mean that you haven't had a bad time in life.
It means that you didn't have those things because you were white.
You can still have them for other reasons, but they didn't occur because of your skin
tone.
The ability to look at the Confederate flag and picture dances, picture a bell of the
South, and not picture the abject horrors of slavery, that is white privilege.
And it's not a meaningful kind, but it's something I think people might get, might understand.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}