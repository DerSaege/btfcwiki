---
title: Let's talk about Portland again and manuals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=onmsFzEtK7U) |
| Published | 2020/07/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculated on the response to federal government actions in Portland, predicting strengthening of protests.
- Mentioned that crowd size in Portland doubled in response to federal government actions.
- Referenced a military manual, FM3-24, which covers provoking responses in chapter four, paragraph 43-ish.
- Emphasized the wealth of resources available in military manuals that can be accessed for free.
- Encouraged everyone to tap into these resources, even if they think they may not be interested.
- Stressed the importance of the federal government reading and following their own manual to avoid escalating tensions.
- Pointed out that mishandling the situation could lead to things spiraling out of control quickly.
- Suggested that the only options left are to either wait for the situation to burn out or to listen to the protesters' demands.
- Urged the federal government to avoid continuing on the current path to prevent a potentially disastrous outcome.
- Mentioned an incident with a young woman facing law enforcement, which was not in the manual.

### Quotes

- "For every person they snatched, it would bring 10 more to the cause."
- "You don't give them a security clamp down in an area where a movement has popular support."
- "The only thing that occurred that wasn't in the manual was the young woman and the mask and only the mask that decided to face down law enforcement last night."

### Oneliner

Beau speculates on Portland protests, urges federal government to follow their manual to avoid escalation, and stresses the importance of listening to protesters' demands.

### Audience

Community members, protesters.

### On-the-ground actions from transcript

- Download military manuals from archive.org or fas.org (suggested).
- Utilize resources like Project Gutenberg and LibriVox for free access to valuable information (suggested).

### Whats missing in summary

The full transcript provides detailed insights into the potential consequences of mishandling protests and the importance of adhering to established strategies to prevent escalation.

### Tags

#Portland #Protests #MilitaryManuals #CommunityResources #FederalGovernment


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Portland again
and manuals.
So yesterday, yesterday afternoon in that video,
I took some guesses as to how things were gonna play out
in the streets in Portland.
What the response to the federal government's actions
was going to be.
I said it was gonna strengthen the hand
of those in the streets.
For every person they snatched,
it would bring 10 more to the cause.
That's what happened.
According to the live streamers who have been covering it,
their estimation is the crowd size doubled.
I don't have a crystal ball, I read the manual.
I said that yesterday and I found out last night
by the time you all watch this,
that people thought I was being sarcastic or metaphorical.
Now I'm dead serious, 100% literal here.
There's a manual on this.
It is FM3-24.
The portion in question that we were talking about yesterday
as far as provoking a response,
that's covered in chapter four paragraph 43-ish,
somewhere in there.
And that's how these books are laid out.
It'll be 4-43, maybe 42.
That's how you'll find it.
It's a military manual.
You can download it for free at archive.org or fas.org,
which is Federation of American Scientists.
This is an incredibly valuable resource
because yes, it has stuff like this.
You can download manuals about this type of stuff.
The military is a massive organization though.
They need people to do everything
and they have manuals for everything.
So if you wanted a book about carpentry, they've got it.
If you wanna know how to drive a bulldozer, they've got it.
It is a massive nonfiction section.
If you were to combine that
with the Gutenberg Project and LibriVox,
you have an entire public library.
It's an amazing resource.
It's something that everybody should tap into.
Even if you don't think you have any interest in anything
the military will publish a manual on, go check it out.
I bet there's something there you wanna read.
Everything from medical texts, it's just tons.
The list is endless.
So definitely take a look.
And you already paid for it.
Your tax dollars developed this,
wrote the manuals and put it out there.
There's no reason you shouldn't have access to it.
Okay, so what's gonna happen in Portland next?
It all depends on the federal government and what they do.
Hopefully they're gonna read their own manual
and realize they're making critical errors.
Those who are in this field,
who understand this stuff well,
they don't agree on anything.
They really don't.
It's constant arguments.
However, they all agree on this.
You don't give them a security clamp down.
You do not provide a security clamp down
in an area where a movement has popular support.
All you do is escalate things.
That's it.
It never works.
It's in the book.
Now, the manual that I gave you there, that's 101.
That's introduction level.
When you see the title of it,
you'll understand how important it is
for the federal government to change their tune
because that's where this can take us.
These type of movements can spin out of control very quickly
and it can go to a whole other level.
It can get really bad, and I know there's people right now
saying it is really bad.
No, it's not.
It's uncomfortable.
It can get really bad if it's mishandled.
Currently, it is being mishandled,
and it will continue to inflame tensions
if they don't start following their own books,
their own advice, their own strategies,
which really at this point,
because this has matured through the cycle,
the only option is to A, wait and hope that it burns out
or B, bring them to the table and listen to what they want.
Those are the only two choices.
It's escalated through mismanagement too far now
because if they continue down the road that they're on,
it's going to be the title of that book,
and nobody wants that.
The only thing that occurred that wasn't in the manual
was the young woman and the mask
and only the mask that decided to face down law enforcement
last night.
That was a surprise, but everything else is in the manual.
But definitely check out those resources.
Check out archive.org, fas.org, LibriVox,
and Project Gutenberg.
You can build an entire library for nothing.
It's worth the time to take a look at it.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}