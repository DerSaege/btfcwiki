---
title: Let's talk about Trump allies pointing the finger at Schiff over Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kYM0V3bIvgo) |
| Published | 2020/07/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the typical cycle of Trump deflection when he gets in trouble, starting with "I didn't know" and moving to blaming Democrats.
- Asserts that Schiff, on the intelligence committee, likely knew about issues before the president, due to attending briefings diligently.
- States that Schiff holding hearings immediately could have endangered special operators and intelligence personnel in the field.
- Points out that intelligence agencies like the CIA work for the president, not Congress, and Schiff cannot direct them to act.
- Notes that blaming Schiff is a deflection tactic to target a disliked figure, as Republicans misunderstand the government's workings.
- Emphasizes that Trump's base is being manipulated with misinformation about intelligence operations to deflect blame effectively.
- Indicates that rumors of issues date back to the Obama administration, but concrete intelligence came during Trump's term.
- Criticizes the lack of action by the Trump administration despite intelligence reports, choosing to believe Putin without any response.
- Stresses that the options are not just ignoring the issue or going to war with Russia, dismissing these extreme notions.
- Concludes by remarking on the deliberate crafting of talking points to exploit ignorance among Trump's base regarding government operations.

### Quotes

- "Schiff knew first because he actually attends his briefings."
- "Secrets are secrets not really because of the information in them but how they're gathered."
- "This is a story that should not go away."
- "Those aren't even options really on the table in a sane world."
- "Crafting their talking points around the idea that their voters don't understand the very basics of how this stuff works."

### Oneliner

Beau explains the Trump deflection cycle, clarifies intelligence operations, and exposes manipulation of Trump's base with misinformation.

### Audience

Politically informed individuals

### On-the-ground actions from transcript

- Contact your representatives to demand accountability and transparency in intelligence operations (suggested)
- Join organizations advocating for governmental oversight and accountability (implied)

### Whats missing in summary

Detailed analysis and context on the manipulation of misinformation among Trump's base.

### Tags

#Intelligence #Trump #Government #Accountability #Misinformation


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about intelligence and how it works.
Talk about who is responsible.
And talk about how we are moving through the typical cycle of Trump deflection very, very
quickly.
So it's already started, you know, anytime Trump gets in trouble it starts with the idea
of I didn't know.
It's not real.
Even if I had known I did it perfectly and then we move to the blame Democrats phase.
We're already starting to hit that.
Because I'm already seeing articles pushing the idea that Schiff, who's on the intelligence
committee, well he knew.
He knew shortly after the president or at the same time as the president.
He knew.
You know what?
That's probably true.
That's probably true.
In fact, I'm going to go one step further.
I'm going to say that Schiff knew before the president.
Schiff knew first because he actually attends his briefings and if he can't make it he sends
a staff member.
So I'm going to suggest that Schiff's team knew before the president.
Now what?
The idea is that well he should have called hearings on it then if he wants hearings now.
No that's not how this works.
If you're Schiff and you're operating under the assumption that the president of the United
States cares about the lives of American soldiers, you wouldn't do that because you would expect
a response would be in the works.
Holding hearings on something while there are special operators or intelligence people
out in the field working on it is what those people out in the field, those people who
are at risk, that's what they would call totally uncool because it puts them at risk.
It discloses what they're doing.
We've talked about it before.
Secrets are secrets not really because of the information in them but how they're gathered.
It puts those people at risk.
No he shouldn't have held hearings on it right then.
That would have been a really bad idea.
And no he couldn't have directed the intelligence community to do something.
That's not how this works.
This deflection is counting on the fact that Republicans do not understand how the government
works.
I'm going to read you a mission statement.
Preempt threats and further U.S. national security objectives by collecting intelligence
that matters.
Producing objective all-sourcing analysis.
Conducting effective covert action as directed by the president.
The CIA works for the president.
The intelligence community works for the president.
Military intelligence works for the president.
They don't work for Congress.
They don't take orders from Congress.
Schiff cannot direct them to do anything.
That's not how the government works.
If Trump wants to deflect blame away from himself, the only person he can push it to
is the director of national intelligence, his appointee.
Congress can't control this.
They can do hearings to gather information and maybe pass legislation to direct policy.
That's it.
They do not control intelligence operations.
That's not a thing.
The attempt to deflect this to Schiff because he's a good target, even though there's a
whole bunch of other people on the intelligence committee who would have been briefed on this
at the same time.
Or in the Senate, there's a whole bunch of people who would have got this information
at the same time.
They've chosen Schiff because he's already disliked by the base, so he makes a good scapegoat.
That's it.
He could have done nothing.
There's nothing that he could have accomplished.
He can't call up Langley and be like, hey, y'all need to handle this.
They don't take orders from him.
He can't call up the Pentagon and say, hey, you know your guys in the field are saying
this happened.
He can't do anything.
The one thing that he could do is hold hearings, which would be really inadvisable because
that would put the people in the field at risk.
And Schiff, unlike the president, apparently cares about that.
This is a horrible deflection.
It's once again counting on the fact that Trump's base is ignorant.
They're actually basing their talking points around the idea that his voters are too ignorant
of how the government works to understand how silly these claims are.
If you have missed it, everybody's confirming this now.
Government elements within the people that accepted the money are confirming this now.
It's not unverified.
That's not a thing.
This is a story that should not go away.
Now something I don't think I've made this clear in videos.
The rument on this goes back, I don't know, maybe eight years into Obama's administration.
I want to be real clear on that.
The rumors about this occurring have gone back a long, long time.
However, the actual intelligence on it came in during the Trump administration and it
made it into the presidential daily brief during the Trump administration, I believe
on February 27th.
That is when some form of action would have been required.
And at this point, the administration still isn't acting because they believe Putin.
No protest, no diplomatic protests, no withdrawing support for their international projects,
nothing.
This doesn't have to be a military response.
It shouldn't be really until the other stuff is exhausted.
Once it's exhausted, then yeah, there could be some low intensity things that are done.
People are framing this as ignore it or go to war with Russia.
Those are not the options.
I mean, those aren't even options really on the table in a sane world.
Once again, that talking point is counting on the idea that Trump's base is ignorant.
That's what they're counting on now.
They are crafting their talking points around the idea that their voters don't understand
the very basics of how this stuff works.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}