---
title: Let's talk about Fauci, Trump, and a wall....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XmfuZ4q7zCU) |
| Published | 2020/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Private citizens raised money to build a wall on the border, but the wall is now unstable and may collapse into the river soon.
- Trump's reaction to negative news is to distance himself from it and reject reality.
- He mentions Trump's pattern of not taking responsibility, blaming others, and politicizing situations.
- Beau contrasts how a smart leader might respond to failure with Trump's actual response.
- The White House is discrediting Dr. Fauci, who has consistently based his opinions on the best available evidence.
- Beau criticizes Trump for prioritizing politics over dealing with the current crisis effectively.
- He points out the economic consequences of Trump's actions and the impact on other nations.
- Beau stresses the importance of acting responsibly during the ongoing crisis and not relying on guidance from authorities.
- He urges people to stay at home, wash hands, wear masks, and take necessary precautions to prevent the further spread of the virus.

### Quotes

- "His decisions, well, they're not his fault. They'll always be somebody else's fault."
- "Our obligation to humanity does not end at the border."
- "Stay at home as much as you can. Wash your hands. Don't touch your face."
- "When he gets bad news, he finds some way to distance himself from it, to reject reality and substitute his own."
- "We're all in this together."

### Oneliner

Private citizens' wall collapses, Trump distances himself, Fauci discredited, economy at risk—Beau urges action in crisis.

### Audience

American citizens

### On-the-ground actions from transcript

- Stay at home as much as you can. Wash your hands. Don't touch your face. (suggested)
- Wear a mask if you have to go out. (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's response patterns, the treatment of Dr. Fauci, and the need for individual action during the crisis.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a wall and Trump and Trump's patterns when things
don't go his way.
How he handles things, what his MO is and what that can tell us about what he's planning,
what he intends to do in the future now that the political machines inside the White House
have started to discredit and smear Dr. Fauci.
So if you don't know, a group of private citizens got together and they raised money to build
a wall down on the border, on the Rio Grande.
This area is hard to build in.
It is a difficult area to build in.
Unsurprisingly reports are now coming in that this wall that they built is, let's just say,
less than stable.
And that at some point in the relatively near future, sections of it may just fall into
the river.
Okay, these are Trump supporters.
These are his base.
These are people who gave up their time, money and land to help fulfill his dream of having
a vanity wall down on the border.
Their little project apparently didn't work.
What would a smart man do in that situation?
What would a real leader do?
What would a good politician do?
Maybe say something like, you know, these are real Americans here.
These are Americans that saw a problem and moved to fix it because they knew Congress
wasn't going to appropriate the funds and they were going to fix it on their own.
They didn't fail at building a wall.
They learned a new way not to build one.
What did he actually do?
Tweeted out about how this only happened to make me look bad.
I'm not joking.
His own base in his mind, his own supporters got together, raised money, gave up their
own land, built a wall to make him look bad.
That's his MO.
It's what he does.
When he gets bad news, he finds some way to distance himself from it, to reject reality
and substitute his own.
With the wall, it's funny.
It's entertaining because it's classic Trump.
With Fauci, it's a little different.
It's actually worrisome.
What has Fauci done this entire time?
Because he's been incredibly consistent about one thing.
He has stated that his opinion is whatever the best evidence and information at the time
says.
That's been his public opinion.
It has always lined up with the best research and best everything that was available.
That's his position.
The White House is attempting to paint this as, oh, well, he changed his mind about this
and he was less than right about this.
Yeah, he changed his mind when new information became available and that's what you're supposed
to do.
I know that seems odd in the Trump White House, but that's actually how you're supposed to
make decisions.
They're pointing to the fact that he didn't think it was going to be a big deal in the
beginning.
Yeah, neither did I.
Go back and watch the first video on it.
I'm assuming that Dr. Fauci knows the same thing I do and knows that we have plans to
deal with this sort of stuff and I imagine that he kind of assumed that the White House
would, I don't know, use them.
That didn't happen.
So his position had to change.
The worrisome part about this is that once he is done distancing himself from the only
adult in the room, from the only person with any expertise, he's telling us now that once
he's done with that, he's going to politicize this even further.
Politics will not be a factor in how he deals with this, attempts to deal with this.
It's going to be completely politicized.
He's going to try to brand his way through it and blame everybody else.
Meanwhile, things are going to get worse.
Things are going to get worse.
I imagine that the president has looked to Europe and seen how those economies are rebounding.
And here, we're still kind of in the middle of this.
I'm in Florida.
At least I hope we're in the middle of this.
I hope this is the peak.
But we've got a governor who seems to believe a lot of the same things that Trump believes.
Just reality is whatever you pretend it is.
With those economies rebounding and the U.S. running flat, it won't be long before his
base starts to question it.
Because this whole time, the reason they were willing to make these sacrifices was to save
the economy.
Because they apparently did not understand that the longer this drags on and the longer
sales are down and activity is down, the worse the economy is going to get.
Because he told them something else.
When he gets called on that, make no mistake, he will find some economic advisor to distance
himself from.
This whole thing will start all over.
It's what Trump does.
He accepts responsibility for nothing.
He will take credit for things that he had nothing to do with.
And his decisions, well, they're not his fault.
They'll always be somebody else's fault.
Because he's not a leader.
He never was.
And we are going into a time where we really need some leadership because now we're at
the point where Trump's antics, they're going to start impacting the rest of the world.
Their system of government didn't put this guy in office.
There's no reason for them to suffer.
As it drags on here and our economy gets drug down with it, we will drag down other nations'
economies.
We become the danger because we're not dealing with it while most of the world is.
We become the source of the issue.
That's why it's funny that this wall thing happened.
Because maybe Canada and Mexico should put one up.
When it comes to stuff like this, our obligation to humanity does not end at the border.
Doesn't matter what their system of governance is.
It doesn't matter what their economy is, what color they are, what flag they have, or what
color their passport is.
We're all in this together.
This thing, it does not care about any of that.
And right now we're just a petri dish because we're not acting.
Because we're letting the president distance himself from the only person the American
people trust.
From the only person in the administration that has any clue what they're talking about.
We really need to remember this.
And we need to understand that we have to act ourselves.
There is not going to be any guidance coming from D.C. or if you're in Florida, the state
capital, you know what you have to do.
Stay at home as much as you can.
Wash your hands.
Don't touch your face.
You have to go out.
And absolutely have to go out.
Wear a mask.
Otherwise we've got another year of this easy.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}