---
title: Let's talk about Trump supporters and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qhZZT0Q1g7M) |
| Published | 2020/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questioning which party in the United States truly supports the Constitution by examining if they encourage supporters to live up to its ideas.
- Testing politicians who claim to be defenders of the Constitution on whether they support and defend its contents, not just its symbol.
- Exploring different aspects of the Constitution like establishing justice, ensuring domestic tranquility, promoting general welfare, and securing liberty.
- Criticizing the response of sending in troops to maintain domestic tranquility instead of addressing underlying issues.
- Emphasizing the importance of promoting general welfare by opposing systemic issues that hinder prosperity.
- Calling out the Republican Party for valuing the symbol of the Constitution more than its principles.
- Asserting that it is a constitutional duty to address systemic racism, support fellow citizens in crises, and advocate for equality.
- Urging individuals to focus on their responsibilities rather than just their rights for the betterment of society.

### Quotes

- "You have a constitutional duty to say that black lives matter."
- "Stop being so concerned with your rights and be concerned about your responsibilities because you're letting us all down."
- "You're failing."

### Oneliner

Questioning if political parties truly uphold the Constitution by examining actions over symbols, Beau challenges individuals to prioritize responsibilities over rights for societal progress.

### Audience

American voters

### On-the-ground actions from transcript

- Join movements advocating for systemic change (implied)
- Support marginalized communities (implied)
- Prioritize community welfare over personal rights (implied)

### Whats missing in summary

Beau's passionate call for societal change through prioritizing responsibilities and actively supporting the principles of the Constitution.

### Tags

#Constitution #PoliticalParties #Responsibility #SystemicChange #Equality


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to answer a question.
Which party in the United States best supports the Constitution?
You know, one party loves to frame it as though they are the defenders of the Constitution.
Use that symbol.
Parchment paper, we the people written really large, nothing else visible.
And the question is whether or not they are really living up to the ideas.
And that's how you test it.
It's a real simple test.
Do the politicians who use this framing, who like to cast themselves as defenders of the
Constitution, do they actually encourage their supporters to live up to the ideas contained
within it?
The concepts that were laid out.
Do they encourage their supporters to support and defend the Constitution?
Or do they just care about that parchment symbol?
We the people.
We the people, in order to form a more perfect union, establish justice, ensure domestic
tranquility, provide for the common defense, promote the general welfare, and secure the
blessings of liberty.
We're going to go through it one at a time.
Establish justice.
So if, let's say hypothetically speaking, there was a segment of the population that
was disproportionately impacted by police violence, that they suffered a sentencing
disparity in the criminal justice system.
If that existed, it would be your constitutional duty to oppose that system.
It would be your constitutional obligation to attempt to fix it.
Because you want to establish justice.
If you were to say, well, he resisted, cops can do whatever they want.
You are anti-constitution.
We have a whole section about this, due process.
Cops don't get to mete out punishment in the street.
That's not how it works.
I'm sorry.
Domestic tranquility.
That's one that's come up a lot lately.
It's become relevant because we have had some less than tranquil moments.
And the response from the we the people crowd, send in the troops.
No.
Ensure domestic tranquility.
That means stopping it before it starts.
The way you do that is you actually listen to the people with the complaints and you
fix them.
So it doesn't come to that.
I know that's not the tough guy routine that people like to use, but that is what's written
down in the constitution.
That's why we have that whole section about being able to petition for a redress of grievances.
The fact that it became less than tranquil is not an indictment of those in the street.
It's an indictment of the government that failed them.
Promote the general welfare and provide for the common defense.
These two go together.
Common defense.
Republicans love that one because they tie it to the second.
That's where they take it.
Common defense.
And I get it.
But provide for the common defense means more than that.
Means I protect you and you protect me.
Wear your mask.
My mask protects you.
Your mask protects me.
It's your constitutional duty in this case to wear a mask.
To look out for your community, to not be selfish.
Promote the general welfare.
That's kind of the same thing, only to a less critical outcome.
Let's say hypothetically there was a systemic issue that stopped a portion of the population
from being able to prosper.
It would be your constitutional duty to oppose that.
You would want it changed because you want them to be able to prosper.
You want to promote the general, that's everybody, welfare.
You want everybody to do well.
And you can tie this to anything too, by the way.
This whole part right here, this is really be a good person.
Look out for your community.
Don't try to keep other people down.
Lift everybody up.
Promote the general welfare.
It's there in the Constitution.
First paragraph.
Does not seem to be the mantra of Republicans today.
And then secure the blessings of liberty.
That's the one they love the most because they take it to mean they can do anything
they want.
Now that's actually a warning.
Secure the blessings of liberty.
And it goes on to talk about how it's for us and those who come after us.
So the duty here is to not elect, I don't know, some authoritarian goon who thinks they
can rule by tweet.
To not elect somebody who calls journalists an enemy of the people any time they criticize
him.
You know, journalism, the press, being one of the only industries enshrined and protected
in the Constitution because it's that important to the functioning of a representative democracy.
The Republican Party today embodies precisely zero of the main reasons we, the people, came
together.
It's all about that symbol.
They care more about that picture of parchment paper with we, the people, written on it than
they do the contents of the actual paper.
The ideas contained within them.
The concepts that were eventually supposed to be spread to everybody when it was written,
including more and more people under that umbrella of freedom.
And any time anybody tries to get that umbrella over another group's head, well, there are
the conservatives opposing the very founding documents of this country.
You have a constitutional duty to say that black lives matter.
You have a constitutional duty to look for ways to address systemic racism.
You have a constitutional duty to look out for your fellow person during the current
health crisis.
Stop being so concerned with your rights and be concerned about your responsibilities because
you're letting us all down.
You're failing.
Anyway, hopefully more people will join we, the people, in the struggle to create the
society that was promised to us.
One where all people are created equal.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}