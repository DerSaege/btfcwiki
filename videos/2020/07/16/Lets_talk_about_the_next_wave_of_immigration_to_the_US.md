---
title: Let's talk about the next wave of immigration to the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kegksr7tYUA) |
| Published | 2020/07/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is heading towards an immigration boom due to freer immigration policies.
- A new study suggests that population decline will occur around 2060 due to factors like education and healthcare.
- Major corporations will influence legislation to encourage immigration for the sake of consumption in a capitalist economy.
- Anti-immigrant sentiment under Trump might be the last surge before a period of increased immigration.
- Immigration is not a problem; it has been used to keep the marginalized divided.
- This shift towards more welcoming immigration policies is good news.

### Quotes

- "The only aliens you should be afraid of are the ones in spaceships."
- "People coming here for a better life are not a problem. They never have been."
- "If there's one thing the US enjoys more than hating people, it's money."

### Oneliner

The US is moving towards an immigration boom due to population decline and capitalist interests, signaling a shift from anti-immigrant sentiments to more welcoming policies.

### Audience

Advocates, Activists, Citizens

### On-the-ground actions from transcript

- Advocate for inclusive immigration policies through community organizing and activism (implied).
- Support organizations working towards immigrant rights and empowerment (implied).

### Whats missing in summary

The full transcript provides a deeper exploration of the reasons behind the potential immigration boom and the societal dynamics driving this shift.

### Tags

#Immigration #PopulationDecline #Capitalism #AntiImmigrantSentiment #USPolicy


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a study, population, and how the United States is headed
towards an immigration boom.
We're going to become very welcoming to immigrants again.
We're going to have much freer immigration policies.
I know this is really hard to imagine right now with the things that are being chanted,
our current political climate, and our current president.
It is hard to picture the United States once again being a country that says send me your
huddled masses yearning to breathe free.
But it's on the way.
And it's on the way for a really weird reason.
For decades we've had people tell us about population growth and warn us even about population
growth and how eventually the population would get to the point that it was causing real
issues.
The reality is that most of that was rooted in a bunch of racist garbage.
I'll actually put a link on overpopulation down below that's really good.
We've been told this for so long that we accept it as fact.
A new study in the Lancet suggests otherwise.
It suggests because of things like education and access to health care and family planning,
knowing where babies come from, all of this stuff, we're having less babies.
Not just in the United States, but in a lot of countries.
And that come about 2060, we're going to start to see a population decline.
We're going to start losing people.
Our population will get lower.
And that sounds cool.
The thing is, the United States is a capitalist society.
We have a capitalist economy.
What does that need?
Consumption.
We need consumers.
Major corporations are not going to allow a declining population to impact their profit
margins.
So while we may not have the same number of births and have a growing population in that
manner, we can get it in another way.
And these major corporations will reach out to the senators and representatives that they
own, I mean donate to, and get them to pass legislation that encourages immigration to
the United States.
We will have much freer immigration.
And this will probably start pretty soon.
In the next 20 years or so, we'll start seeing the beginning of it.
So this surge of anti-immigrant sentiment that is occurring under Trump, it's probably
going to be the last one for quite some time.
Because if there's one thing the US enjoys more than hating people, it's money.
At the end of the day, this isn't something to be worried about.
The only aliens you should be afraid of are the ones in spaceships.
People coming here for a better life are not a problem.
They never have been.
It's just a tool those in power used to keep those in the bottom divided, to keep them
kicking down so they couldn't look up and see who the real problem was.
But it looks like that might be coming to an end.
So there's just some good news for the morning.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}