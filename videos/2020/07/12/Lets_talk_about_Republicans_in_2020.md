---
title: Let's talk about Republicans in 2020....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OJHcM8XMK-g) |
| Published | 2020/07/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans facing challenges in the 2020 election due to a significant shift from 2016.
- Republican pollsters realizing the need to spend money to secure seats even in traditionally red states.
- Voters who previously supported Trump now considering voting Democrat for the first time.
- Analysis of the uniqueness of the 2016 election compared to the upcoming one.
- Republicans urged to speak out against Trump and return to the core principles of the party.
- Concerns within the Republican Party about losing control over the House, Senate, and White House.
- The necessity for Republican senators to show courage and stand up against Trump to maintain their seats.
- Criticism of Trump's actions and their impact on the Republican Party's image.
- Speculation on the fate of Republican senators based on their response to Trump's leadership.
- Beau's curiosity about which Republican senators will demonstrate integrity and stand up against Trump.

### Quotes

- "Unique does not always mean useful."
- "Their only option is to grow a spine real fast."
- "He has the Republican Party attacking wounded war vets and defending it."
- "Your most vocal supporters are not really representative of all of the people in the Republican Party."
- "Republican senators have to decide whether or not they want to stay on that boat or not."

### Oneliner

Republicans face a challenging shift from 2016, urging senators to speak out against Trump to preserve their seats and party principles.

### Audience

Republican voters

### On-the-ground actions from transcript

- Speak out against Trump's actions openly and oppose him on critical issues (implied)
- Return to the core principles of the Republican Party by standing up for integrity (implied)

### Whats missing in summary

The full transcript provides more in-depth analysis and examples of the challenges Republicans face in the upcoming election and the necessity for them to re-evaluate their support for Trump.

### Tags

#Election #RepublicanParty #Trump #2020Election #Voters


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the 2020 election and what Republicans are going to
do because they got a dumpster fire on their hands.
A lot of internal Republican pollsters are starting to realize this isn't 2016.
They're having to spend money to keep seats in reliably red states.
In Republican states they're having to campaign to keep seats.
And it's worrying them, as it should, because it isn't 2016.
2016 was a very unusual situation.
On the Republican side, you had this maverick, this individual who was different.
And he somehow made it through the Republican primary.
And a lot of people looked at him and were like, I don't really back him, but he's unique,
let's give it a shot.
And they have come to find out that unique does not always mean useful.
They're not going to vote for him again.
The situation with the people I talked to today, all Republican voters too will be voting
Democrat for the first time ever.
On the other side of the aisle in 2016, you had Hillary Clinton.
She was different too.
She was one of the few candidates that created negative turnout.
People who showed up not really to vote for Trump, but to vote against her.
And Democrats in 2016, they were like, there's no way Trump's going to win.
So a lot of them didn't go vote.
They're not going to make that mistake again.
When voters wake up on election day in 2020, they've got a choice.
They can have water or spoiled orange juice.
Nobody really wants water with their breakfast.
But nobody's going to drink the spoiled orange juice, especially if they sipped it yesterday
and they know it's bad.
Most people would like something with a little bit of flavor.
Biden is not that.
In the words of one of the people I talked to today, at least he's a conservative Democrat.
So what are Republicans going to do?
Those in the Senate, because that's the real worry inside the Republican Party, is that
they're going to lose the House, the Senate, and the White House.
They're not going to have control over anything.
Their only option is to grow a spine real fast, because the people I talked to today,
it wasn't just Trump.
Now I'm not going to vote for Trump or Matt.
They got to grow a spine.
They have to start speaking out against the president, especially on the issues that are
dragging him down, like his lack of a response to our public health issues.
They're going to have to start opposing him openly, or they're going to be penalized when
they get to the polls.
They're crossing over to vote Democrat for the first time in their life.
What's to stop them from voting D all the way down it?
They have to start speaking out.
They have to get back to the principles of the Republican Party, because Trump sold them
all out.
He has the Republican Party attacking wounded war vets and defending it.
That's not going to fly with the silent majority he keeps talking about, those that aren't
on Twitter.
They don't like that.
Your most vocal supporters are not really representative of all of the people in the
Republican Party.
Trump's boat has a big ol' hole in it.
It's going to sink.
Republican senators have to decide whether or not they want to stay on that boat or not.
That's the only chance they have to keep their seats.
I personally don't care.
But I'm interested to see which ones actually have any courage, which ones can stand up
for their principles, and which ones are so scared and so bullied by Trump that they're
literally going to lose their Senate seat rather than speak out against him.
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}