---
title: Let's talk about using your voice, being too close, and Tanisha Pughsley....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hc4uzTduxL4) |
| Published | 2020/07/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Importance of the video for everyone, especially women.
- Impact of the topic on women when it spirals out of control.
- Focus on using your voice, being cautious, and dispelling myths.
- Learning from Tanisha Pugsley, a detective in Montgomery, Alabama.
- Description of Tanisha as a strong, law-abiding woman.
- The limitations of protective orders and their role in cases.
- Tragic outcome involving Tanisha's ex-boyfriend charged in her death.
- Challenging stereotypes of weak, dependent women in such situations.
- Emphasizing the need to recognize danger even when too close.
- Urging the use of voice, seeking help, and calling for backup in risky situations.
- Mention of shelters and resources available for individuals in need.
- Providing the national hotline number for assistance.
- Encouragement to reach out for help and not face such situations alone.

### Quotes

- "Use your voice. Call for backup."
- "You don't have to go through this alone."
- "If you knew somebody going through this, you'd want to help, right?"

### Oneliner

Beau stresses the importance of using your voice, seeking help, and dispelling myths around domestic violence, inspired by the tragic story of Tanisha Pugsley.

### Audience

Women, allies

### On-the-ground actions from transcript

- Call the national hotline for assistance (exemplified)
- Reach out to shelters for help (implied)
- Encourage others to seek help and not go through such situations alone (exemplified)

### Whats missing in summary

Full emotional impact and details of Tanisha's story can be better understood by watching the full video.

### Tags

#DomesticViolence #Empowerment #SeekHelp #Support #Community


## Transcript
Well howdy there internet people, it's Beau again.
So this video is important for everybody, everybody, but it is especially important
for women.
While this topic impacts everyone, when it spins out of control, the consequences tend
to be more severe for women.
So today, we are going to talk about using your voice.
We're going to talk about being too close.
We're going to dispel some myths.
And we're going to be able to do all of this because we're going to talk about what we
can learn from Tanisha Pugsley.
Tanisha Copp, detective in Montgomery, Alabama.
If you are not familiar with the southern United States, Montgomery is not a small southern
town.
If you are a detective in Montgomery, you can hold your own.
It's not a weak woman, not a passive woman, not a dependent woman.
She had an altercation with her ex-boyfriend.
She's a cop.
Followed the law.
The suggestion.
Got a protective order.
That protective order is a piece of paper.
It's what it is.
It's another suggestion.
It's a piece of paper.
Can it help?
Yes.
Can it deter some people?
Yes.
Can it help build a case later?
Yes.
Will it stop someone?
No.
It's a piece of paper.
Her ex-boyfriend was just charged capitally in her death.
This is a woman who was not weak.
Who could hold her own.
Had a weapon.
Followed the law.
Hurts a lot of those stereotypes, doesn't it?
Because that's who this happens to.
Weak, dependent, passive women.
You just need to run out to the store, pick you up something to handle that.
Go get a protective order.
Take some self-defense classes.
All of it was done.
All of it.
Sometimes you're too close.
Sometimes you're too close and you may not see the danger.
If she had a call and was rolling up to a domestic and was by herself, think she would
have called for backup?
Probably.
Probably.
Because I'm sure she knew these things can spin out of control in the blink of an eye.
She's too close.
Didn't think that would happen.
If you are in this situation, use your voice.
Call for backup.
If you've been watching this channel long enough, you know there are shelters nationwide
and some of them are like the marshals.
They will show up when that person least expects it.
They will pick you up.
You will disappear and they will help you put your life back together.
There are phone numbers you can call.
The national hotline is 1-800-799-7233.
That is 1-800-799-7233.
You don't have to go through this alone.
It's not a good idea to go through it alone.
Use your voice.
Call for backup.
The resources exist.
If you knew somebody going through this, you would want to help, right?
Everybody else does too.
Call.
There is no reason not to.
Use your voice.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}