---
title: Let's talk about black lives, uniforms, and causes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IzBXlc8gtOc) |
| Published | 2020/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the pushback he received after a previous video about uniforms and causes.
- Exploring the sentiment of the necessity of uniforms from a broader perspective.
- Questioning the practicality and implications of uniforms in certain situations.
- Emphasizing the importance of blending in for safety and freedom of movement.
- Identifying the diverse goals of the movement, ranging from social justice to equality for all.
- Tracing the roots of the movement back to helping, protecting, and elevating people of color.
- Warning against escalating to a next level where identifiable groups face greater risks.
- Urging against adopting uniforms that make marginalized groups easy targets.
- Rejecting the idea of sacrificing a demographic for political gains.
- Advocating for the harder but peaceful route for a better outcome.

### Quotes

- "Being able to blend in, in a situation like that, drastically increases your survivability."
- "If this goes open and escalates into something like that, they don't."
- "The end of something can often be determined by the beginning."
- "You give up, but you don't give in either."
- "It's harder. It's harder. It's going to have a better outcome."

### Oneliner

Beau addresses the implications of uniforms in movements, urging against easy targeting and advocating for the harder but peaceful route towards a better outcome.

### Audience

Activists, advocates, allies.

### On-the-ground actions from transcript

- Advocate for blending in and maintaining safety in movements (implied).
- Reject the idea of sacrificing any demographic for political gains (implied).
- Support peaceful approaches for better outcomes (implied).

### Whats missing in summary

The full transcript provides deeper insights into the risks and implications of adopting uniforms in movements, urging for thoughtful consideration and advocating against easy targeting.

### Tags

#Uniforms #SocialJustice #Equality #PeacefulResistance #MarginalizedGroups


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about uniforms and causes.
After that last video I had some people kind of push back.
Three people sent me a message.
Basically the summation is that
they feel it might be necessary.
And I get that.
I get that on a sentimental level, on an emotional level.
It seems at first glance that that would be the easiest route.
It's not, not really, but it seems that way.
I don't think these people are coming from a bad place.
I think they're coming from a good place.
But I think they may not be thinking it all the way through.
And to get them to think a little bit wider,
I asked them a question that I had to clarify
that I was being serious.
Because they all knew I knew the answer.
And they all gave me the right answer.
But that question was, if it goes to that next level,
what are the uniforms gonna be?
The three answers, the uniform of the man on the street,
if you don't blend in your bait,
no uniforms, that's stupid.
Yeah, that's right.
That's right.
Being able to blend in, in a situation like that,
drastically increases your survivability.
If you can't easily be picked out as a target,
it makes you a whole lot safer.
It allows you some freedom of movement,
allows you to get stuff done.
No uniforms.
Then I asked them what the goal was.
And I got varying answers, but they all acknowledged
where this started.
That's gonna become important here in a minute.
And when you look at the goals now,
because this movement has widened drastically,
just random off of Twitter,
where I asked this question openly,
social justice now, fight until it's right,
no authority without accountability,
make 1984 fiction again.
The poor and middle class want a seat at the table.
Treat us with respect.
Give power back to the people equally.
There's a whole bunch of very broad ideas here.
And yeah, because the movement has broadened,
it's embraced more people who are suffering inequality.
However, where did this start? How did it begin?
Hoping to help and protect and elevate people of color.
That's how this started.
If this was to go to the next level,
and it hits that point, that's what people would rally behind.
A lot of people would revert back to that.
Yes, it's gonna encompass these other goals,
but that's gonna be a main component of it.
I would hope, you know.
The opposition, though,
it's not gonna be their rallying cry.
That's gonna be what they're motivated against.
That group of people becomes the symbolic opposition
for them.
That group of people can't take their uniform off.
Not the uniform that the opposition would be looking for.
It's permanent.
And the people who would be opposed to equality for all,
treating everybody equally,
they're gonna be looking for them.
Those communities have leaders.
All three of the people who sent me a message,
they all looked like me.
Back then,
they all looked like me.
Back those communities up.
Back their plays.
They may be considering things that we're not, like this.
They can't blend in, so they're bait.
Six years, that's a rough sentence.
Rough period of time to be the lightning rod for the movement.
I don't think we're at a point where we need to do this.
I would openly advocate against it.
We need to be aware that it's a possibility,
but we need to take the steps not to do it.
I get, without a firm grasp on what happens during one of these,
yeah, it seems like it would be easier.
It's not.
But even, let's say that it is.
If even a minor piece of the movement
is still the idea that black lives matter,
we just have to do it the harder way.
Because if this goes open and this escalates
into something like that, they don't.
And we're saying that very clearly.
They're fodder.
They're the lightning rod.
We know what's going to happen.
Historically speaking, marginalized groups
that are easily identifiable by their skin tone
or their practices, like their cultural practices,
their way of dress, stuff like that, they don't fare well.
Because they're easy to pick out.
They can't blend in.
They can't adopt the uniform of the man on the street.
That next level is horrible.
It's not good.
It's nothing to advocate for ever.
It's something that you have to be pushed into.
And it's something that you should
try to avoid being pushed into.
I understand that there's a whole lot of people
who think that's the whole goal right now of the administration
is to start this.
Then why play along?
Because you think you're going to win?
Maybe.
Maybe not.
It's not a movie.
The good guys don't always win.
And I have to say that if we're going to just abandon
a pretty large demographic of people
to support our other political ends, we aren't the good guys.
We're not very much different than those
who we're saying we oppose.
If it has to go to that level, they
have to make that decision.
Because it's going to fall on them a whole lot harder
than it's going to fall on anybody else.
And I do not think that we're at that point yet.
The end of something can often be
determined by the beginning.
Yeah, it may be harder to do it the peaceful route.
So it's harder.
It's harder.
It's going to have a better outcome.
You don't give up, but you don't give in either.
And I think that's the way to go.
You give up, but you don't give in either.
And I would suggest going that route is giving in.
It's duplicating all of the errors from before.
It can be done.
It's just harder.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}