---
title: Let's talk about a lesson for America from my son....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MVAjMktiaZ4) |
| Published | 2020/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares a story about his son and habanero dip to illustrate a bigger point.
- His son, despite being warned, tries the dip and experiences the burning sensation.
- Beau uses this analogy to draw parallels to a country facing a crisis.
- He paints a grim picture of a country, Suistan, dealing with a public health crisis and oppressive regime.
- The regime in Suistan is neglectful and focused on extracting wealth.
- Beau compares the situation in Suistan to the current state in the USA under Joe Biden's administration.
- He warns about the consequences of ignoring warning signs and pushing towards irreversible outcomes.
- Beau criticizes the administration for their actions and lack of consideration for the people.
- He cautions against the path the country is heading towards, stressing the severe consequences.
- Beau ends by urging people to listen to those who have experienced similar situations and avoid the impending crisis.

### Quotes

- "You don't want this. You do not want this."
- "Because if this weekend taught us anything, it's that if this goes too far, they're not going to have to worry about a re-election."
- "There is no good guy. There is no glory. There is no happy ending to any of this."
- "That militant talk, it's all cool. Sounds good. Until you have the chip in your hand."

### Oneliner

Beau warns against the irreversible consequences of ignoring warning signs and pushing towards a crisis, drawing parallels between his son's habanero dip experience and a country in turmoil.

### Audience

Citizens, activists, voters

### On-the-ground actions from transcript

- Pay attention to warning signs and advocate for change (implied)
- Support and listen to those who have experienced similar crises (implied)
- Take action to prevent irreversible outcomes (implied)

### Whats missing in summary

The full transcript provides a detailed analogy between personal experiences and larger societal crises, urging individuals to heed warnings and take action before it's too late.

### Tags

#Crisis #WarningSigns #Prevention #CommunityAction #PoliticalAwareness


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about what we can learn from my son and habanero dip.
Last year he was four and there were a couple of us sitting around and we were eating habanero
dip and he kept saying he wanted some.
Now you really don't and he keeps going back and forth with us, eventually stomping his
feet saying he wants some.
No you really don't.
And eventually he grabs a chip, dips it in real quick and stuffs it in his mouth, ignoring
everybody who's actually experienced it, everybody who's seen it.
A split second later he realized it wasn't what he wanted.
But it didn't matter.
The burning did not stop immediately, it didn't matter how much water he drank or bread he
ate or how he tried to distract himself, he felt it.
Want you to picture hearing something on the 6 o'clock news.
Picture hearing about a country where there's a public health crisis, 150,000 gone, the
regime's leadership does not care, they're not really acting, hospitals can't get what
they need.
The security services in the country, they have a history of oppressing an ethnic minority
and right now they're out in the street wearing masks, looking like soldiers, stuffing people
into vans.
It's so bad that the UN is tweeting advice about how not to go too far.
The discontent, levels of unemployment, maybe evictions, are incredibly high and that's
led to competing groups being in the street, armed.
The regime's leadership and his less than capable family don't seem to care, they're
golfing and they really only seem interested in what they can extract from the country,
the wealth they can get out of it.
If you heard this storyline and it wasn't the USA with some backwards place, let's call
it a Suistan, you know what would be next.
First we'd start hearing little tidbits about the hard, industrious people of a Suistan,
how they're a lot like us, trying to get us to identify with them.
Because we know the next step is seeing US soldiers load onto C-130s and all of us being
told to support the troops as they go to spread democracy and bring freedom and all of that.
The administration is running ads right now talking about Joe Biden's America, what's
going to happen under Joe Biden.
But it's all happening now and just like in one of those other countries that's nothing
like the United States, the regime leadership is so out of touch with the average person
in their country and thinks so little of them, they don't believe that people are going to
realize Joe Biden's America is happening now.
Over this weekend, and the weekend isn't even over yet, there were at least three different
incidents that could have been that unintentional thing that takes this to the next level.
The weird part is there are people on social media that seem to be advocating for it, seem
to think that's a good idea, even though anyone who's ever gotten a taste of it is telling
them no, this isn't what you want.
Eventually, somebody's going to grab a chip and when it happens, there won't be enough
water or bread and circuses to put out the fire, to stop the burning, not immediately.
It's going to have to go on.
See in the United States, we're so insulated from stuff like this.
Hasn't happened here in so long and we're not close enough to any country where it's
really occurred before and we don't pay attention to what goes on in other nations anyway.
So we only have this movie image of what stuff like that, what it's like.
That's how we picture it.
The reality is it is not a two hour story arc where the good guy wins at the end.
There's no glory in it.
There's no comic relief.
It's just bad.
And if this happens, it's a six year commitment.
Everybody thinks their side's going to win.
It's unconventional.
It's irregular.
You've got no clue who's going to win.
It probably wouldn't be any of the groups that are formed now.
Because just like in every other country where it has ever happened, the initial groups rarely
make it through.
They shift.
Shifting alliances.
Shifting groups.
Groups merging.
Goals changing the entire time.
Because after about the first year, all anybody really wants is stability.
I understand that people are upset.
There's a lot of discontent and there's a lot of fear.
I get that.
But listen to those who have had the dip before.
You don't want this.
You do not want this.
This administration is dragging us and pushing us towards something we cannot come back from
simply for their own political advantage because they think it's going to help them.
It won't.
It won't.
Because if this weekend taught us anything, it's that if this goes too far, they're not
going to have to worry about a re-election.
Because once these things start, six-year commitment.
There is no good guy.
There is no glory.
There is no happy ending to any of this.
That militant talk, it's all cool.
Sounds good.
Until you have the chip in your hand.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}