---
title: Let's talk about a special 4th of July message....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cY1xXL33i3I) |
| Published | 2020/07/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Sharing a special 4th of July message regarding Independence Day and the concept of equality in the country.
- Addressing the slogan "All lives matter" and its implications when used in response to "Black Lives Matter."
- Exploring the hidden motives behind using "All lives matter" to diminish the message of equality for black individuals.
- Pointing out the systemic racism embedded in using "All lives matter" to deflect focus from specific injustices faced by black people.
- Questioning the true intent behind advocating for justice for all lives while undermining the Black Lives Matter movement.
- Emphasizing the fear that equality for black individuals may challenge the privilege and status quo enjoyed by white individuals.
- Concluding with a thought on the implications of acknowledging true equality for all individuals in society.

### Quotes

- "If the only time you say all lives matter is as a response to somebody saying that Black Lives Matter, you have to question your own motives."
- "All lives deserve justice, if that's true, then you should have no issue with the phrase Black Lives Matter because black is part of all."
- "That's not what gets said. When people say it, what they're really doing is trying to turn down the volume on that message."
- "The fear of most people who say that is that if black people get equality, well, then white folk, they're gonna have to stand on their own."
- "Happy 4th of July, and all countries matter."

### Oneliner

Beau addresses the divisive slogan "All lives matter" and its implications, questioning hidden motives behind its usage in response to "Black Lives Matter" while exposing systemic racism.

### Audience

Social Justice Advocates

### On-the-ground actions from transcript

- Support fundraisers for shelters that aid survivors of domestic violence (suggested)
- Challenge systemic racism through education and advocacy efforts (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the intricacies of systemic racism and the importance of acknowledging specific injustices faced by marginalized communities.


## Transcript
Well howdy there internet people, it's Beau again.
So today we have a very special 4th of July message.
Something specific to Independence Day,
given the fact that we live in a country
where all men are created equal.
And we're gonna talk about that.
We're gonna talk about a slogan.
Something that gets said all over the country right now,
but it only gets said at certain times.
We're gonna talk about why that happens,
what the intent is, and what people really mean
when they say it.
Hey babe.
Ooh, I'm sorry, are you working?
It's the 4th of July, I thought you weren't
gonna work tonight.
Not everybody who watches the channel
lives in the US and cares about our holidays.
Yeah, but certainly the internet people
wouldn't stop you or be mad at you
for being mostly concerned with your own holidays
and your own country.
All countries matter.
Really, okay, whatever.
Look, we have that family reunion tomorrow.
All families matter.
Whatever, it's your family.
Are you doing the fundraiser for the women's shelter?
Because that's also tomorrow.
Is it really?
Yeah.
I've been thinking about that.
I'm not sure, because men get impacted by DV as well.
I don't know why we should just do a fundraiser
for a women's shelter.
Maybe we should do a fundraiser for all shelters.
Yes, it affects men too, but isn't it worse for women
if we fix the systemic issues impacting them?
Wouldn't that help everybody?
Yeah, I mean, probably, it might.
Did you just rope me into a bit?
Maybe.
Good night, internet people.
We hear that a lot now.
All lives matter.
Cool, then you have no problem with Black Lives Matter.
If the only time you say all lives matter
is as a response to somebody saying that Black Lives Matter,
you have to question your own motives.
You have to wonder what your real intent is.
Do you really believe that all lives matter
and all lives deserve justice?
If that's true, then you have to question
what your real intent is.
All lives deserve justice, if that's true,
then you should have no issue
with the phrase Black Lives Matter
because black is part of all.
But that's not how it gets used.
That's not what gets said.
When people say it, what they're really doing
is trying to turn down the volume on that message.
They're trying to say, well, yeah, yeah, yeah,
that's all fine and good, but we have to focus on us, us,
but not you, not you specifically.
We need to be more concerned about society as a whole.
And since the country was founded,
you really weren't part of society
and we've pretty much always pretended that the whole time
that we don't need to worry about you.
If the only time you're saying all lives matter
is a way to downplay somebody else's message,
that is the systemic racism that they're talking about.
The fear of most people who say that
is that if black people get equality,
well, then white folk,
they're gonna have to stand on their own.
They're gonna have to stand on their own
individual accomplishments.
They won't have the privilege that's granted to them
by their skin tone,
by the status of those that came before them.
They're not gonna have access to all of that
because maybe all men will be created equal,
and that's how it would be seen by society at large.
So, yeah, anyway, it's just a thought.
Happy 4th of July, and all countries matter.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}