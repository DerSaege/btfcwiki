---
title: Let's talk about Trump helping auto workers with his trade deal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OpD0xZdGiF0) |
| Published | 2020/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into Trump's signature trade deal, the USMCA, aiming to bring back auto manufacturing jobs to the US.
- The deal requires 75% of car parts to be manufactured in the US, Canada, or Mexico, and 40-45% by workers earning at least $16 an hour.
- Trump's plan was to entice Japanese auto manufacturers to move to the US; however, companies are opting to raise wages in Mexico instead.
- The trade deal went into effect on July 1st, with companies in Mexico choosing to increase wages rather than relocating.
- Despite falling short of Trump's goals, the deal has a silver lining: benefiting workers, raising living standards, and boosting the Mexican economy.
- Beau acknowledges that the deal may increase car prices in the US due to higher labor costs but suggests the Mexican government might offer incentives to balance this.
- Trump is likely to tout the deal as a success on the campaign trail, but the reality differs from his claims.
- While Trump may claim to have helped auto workers, it's primarily benefiting non-American workers under the USMCA.

### Quotes

- "Trump is going to get out on the campaign trail and still talk about this as if it's a great success."
- "He says that he helped auto workers, that's a true statement. Just not American auto workers."

### Oneliner

Beau delves into Trump's USMCA trade deal aiming to boost auto jobs, but results differ from his claims, benefiting non-American workers primarily.

### Audience

Policy Analysts, Trade Experts

### On-the-ground actions from transcript

- Support policies that prioritize American workers' interests over foreign workers (implied)
- Advocate for trade deals that truly benefit American workers (implied)

### Whats missing in summary

Insights on the potential long-term impacts of the USMCA trade deal beyond immediate consequences.

### Tags

#TradeDeal #USMCA #AutoWorkers #Trump #EconomicImpact


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Trump's signature trade deal, the one he's been touting
and talking about what a success it is going to be, and how it helped autoworkers.
Okay, the name of the deal is the USMCA, that's the United States Mexico Canada Agreement.
It's NAFTA II.
Trump's goal for this was to bring back auto manufacturing jobs to the United States.
That's what he wanted to accomplish with it.
He set out to do this with two components in the agreement.
The first was that 75% of parts in cars had to be manufactured in either the United States,
Canada, or Mexico.
The second was a labor component, and it required that 40-45% of parts had to be manufactured
by employees who were making at least $16 an hour.
Now in Trump's mind, this was going to encourage Japanese auto manufacturers to move their
facilities from Mexico to the US where $16 an hour is kind of a normal wage.
And that makes sense in Trump land.
However, if you're the CEO of a successful business, it wouldn't take you long to realize
that it is cheaper just to pay your employees in Mexico $16 an hour than it is to pay to
move your factory and then pay $16 an hour.
$16 an hour in Mexico gets a really happy employee.
The trade deal went into effect July 1st.
That's exactly what's happening.
The Honda affiliated company down there, they're just raising wages.
Most of the component manufacturers down there are just raising wages.
Toyota back in February actually shifted production of one line of vehicles from Texas to Mexico.
Now they are investing in some infrastructure in the US as well.
Okay, so I know it's going to be hard to avoid mocking Trump relentlessly for this because
it's his signature trade deal and it is a failure by the standards he set for it.
But in the grand scheme of things, in comparison to Trump's other foreign policy and trade
disasters, this one's not so bad.
It's got a silver lining.
A bunch of workers are going to get paid well to do their job.
It's going to raise the standard of living down there.
It's going to help their economy.
These are all good things.
I know people are going to say that it's going to raise the price of cars in the US because
of the added labor expense down there.
Yeah, I mean maybe a little bit, but I would hope that the Mexican government would help
provide some incentive for those companies to keep their factories down there.
So that might offset it some.
Yeah by his standards, what he wanted to accomplish with it, it's a failure.
Fact.
But it's not necessarily a bad one.
You know, it's going to help people and I'm okay with that.
Now this is the reality of it.
Trump is going to get out on the campaign trail and still talk about this as if it's
a great success.
Just know that when he says that he helped auto workers, that's a true statement.
Just not American auto workers.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}