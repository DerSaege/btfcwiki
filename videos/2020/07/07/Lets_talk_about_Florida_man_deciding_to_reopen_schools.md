---
title: Let's talk about Florida man deciding to reopen schools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FdYy3w-Fxvs) |
| Published | 2020/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Florida plans to reopen schools in person in August, five days a week, despite the ongoing situation.
- The decision to reopen schools seems to be based on unclear reasoning and not on data.
- Beau criticizes the lack of understanding and decision-making skills of those in charge.
- He questions the safety guidelines provided by the Governor and contrasts them with a typical school day scenario.
- The school environment poses challenges for following safety protocols like wearing masks and social distancing.
- The decision to reopen schools now, amidst a surge, lacks a logical basis.
- Beau advocates for having distance learning options ready as a more critical measure.
- He urges Floridians to prioritize safety and make informed decisions regarding sending children to school.
- Beau warns the ruling party in Tallahassee about the potential consequences of their decision on re-election.
- He concludes by suggesting that the decision-makers may be jeopardizing their chances for re-election by ignoring public safety concerns.

### Quotes

- "So we're just gonna go through an average day at a school and compare that to the guidelines."
- "There's no data to back up this decision. None."
- "We are Florida man. We are Florida woman. We do not need Tallahassee to make the right decision."
- "If you look at that data come August and you don't feel that your child would be safe, don't send them, homeschool."
- "You are quite literally killing your chances for re-election."

### Oneliner

Florida plans to reopen schools in August without clear data or rationale, posing risks to students and communities, urging individuals to prioritize safety over government mandates.

### Audience

Parents, educators, community members

### On-the-ground actions from transcript

- Monitor local school board decisions and advocate for safer alternatives (implied)
- Stay informed about the situation in your area and make decisions based on safety (implied)

### Whats missing in summary

The emotional impact of potentially risking the health and safety of students, families, and communities by rushing to reopen schools without adequate preparation or consideration for the ongoing situation.

### Tags

#Florida #Schools #Reopening #Safety #CommunitySafety #DecisionMaking


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about Florida,
deciding now, in the middle of all of this,
to reopen schools in August in person
and have them open five days a week
and making that decision now in July
based on I don't know what, obviously not the data.
So we're gonna talk about that.
We're going to talk about the governor's guidelines
on how to stay safe.
And we're gonna go through an average day at school.
We're gonna do all of this mainly because of a quote
from a Department of Education spokesperson.
Logically, I don't think they could say schools aren't safe
if they are allowing people to be out in public.
Okay.
I would try to explain this using the graphs
that are widely available.
Some of the less than accurate ones
are even put out by the state.
But I've come to the conclusion
that the people making these decisions
probably wouldn't have scored very high on the FCAT
and may not understand this.
So we're just gonna go through an average day at a school
and compare that to the guidelines
that we have right here with Governor DeSantis's name
at the top, because this is how we would be safe,
smart, and step-by-step.
Okay. That's what it says here.
It says, public health advisory,
residents advised to wear masks in public
and socially distance, avoid crowds,
closed spaces, and close contact.
Average day at school, kid walks out, gets on the bus,
in a crowd, in a close space and close contact
where they can't socially distance.
Not even to the school yet.
Not even to the school yet.
Let's just skip that one.
They get to the school and they get off the bus
where they all grab that metal bar there by the steps.
They all touch that.
That seems like a bad idea to me.
And in the state of Florida, they go to the cafeteria,
which is another crowd in a closed space
where they can't socially distance and close contact.
And because they're eating, they're not wearing their masks.
And because we have such nutritious meal plans
here in Florida, a lot of it's finger foods.
So they're touching their face.
They're not even to class yet.
And they have broken all of the guidelines.
All.
Logically, I would say that's probably a bad idea.
So, why we're making this decision now,
it's not really a surprise.
But the president tweeted, delivered his edict
that schools should reopen in person.
So that's what Governor DeSantis is doing
because apparently he's a rubber stamp.
Even though his own guidelines that are safe, smart
and step by step say that we shouldn't do that.
We certainly shouldn't make that decision now
in the middle of a surge.
There's no data to back up this decision.
None.
Now for all we know, next month, yeah,
things could be better.
It could disappear like a miracle.
Have zero cases.
We've heard this before.
Maybe this time it'll happen.
But that's a month from now.
There's no reason to make this decision now.
Remember a month ago, the governor of Florida was bragging
about how well the state was doing.
And now we're in the middle of a surge.
A lot can happen in a month.
This seems like a bad idea.
Sure, have the schools ready,
but have the distance learning options,
have them ready too.
Those are probably more important.
Floridians, we have a national reputation to uphold here.
We are Florida man.
We are Florida woman.
We do not need Tallahassee to make the right decision.
We don't listen to the government for anything else.
Why would we start now?
If you look at that data come August
and you don't feel that your child would be safe,
don't send them, homeschool.
Now to those in Tallahassee, the ruling party there,
y'all normally need some self-interest to do anything.
So let me get it to you.
Young people have written y'all off.
They're not going to be voting for your party.
Exit polls show that traditionally anyway.
Your voters are older.
Your voters are more susceptible to this.
So as the kids spread it around, take it home to their parents
who then take it to their grandparents
who are those who are going to vote for you,
that's going to impact them.
You are quite literally killing your chances for re-election.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}