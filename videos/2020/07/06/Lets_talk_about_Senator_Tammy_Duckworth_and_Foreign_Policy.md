---
title: Let's talk about Senator Tammy Duckworth and Foreign Policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PHt5qWGST7c) |
| Published | 2020/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lieutenant Colonel Tammy Duckworth is a potential Biden VP nominee generating a lot of interest.
- Duckworth's background includes growing up in Southeast Asia and serving in the military, where she chose to fly helicopters.
- In Iraq, Duckworth's Black Hawk was hit by an RPG, resulting in her losing both legs and severely injuring one arm.
- Despite her injuries, Duckworth stayed in the military and retired as a lieutenant colonel.
- Her military record is exemplary, with accolades like the Purple Heart and Meritorious Service Medal.
- Duckworth's political positions are in line with expectations for a Biden VP nominee, except for gun control.
- Her stance on gun control might not please those on either side of the debate.
- Duckworth's expertise in foreign policy could make her a strong choice to clean up Trump's mess in that area.
- She has shown a genuine interest in auditing international affairs spending and has the necessary knowledge and experience.
- Duckworth's military background could command respect from leaders who may not take women seriously.
- While she may not be a diplomat, she has the foundation to excel in foreign policy if that's the role Biden's team is looking to fill.
- Beau believes that if the VP pick is intended to strengthen Biden's foreign policy, Duckworth could be a suitable choice.
- Ultimately, Beau sees Duckworth as a potentially strong pick if the goal is to fill a foreign policy role in the administration.

### Quotes

- "She has the knowledge, she has experience, she understands the way the world works, and she actually cares about the subject."
- "She survived an RPG. She's gonna get more respect from the leaders of countries that don't treat women as they should."
- "They've chosen well, as far as I can tell."

### Oneliner

Lieutenant Colonel Tammy Duckworth could be a strong VP choice for Biden, particularly for foreign policy, given her experience, knowledge, and resilience.

### Audience

Political analysts

### On-the-ground actions from transcript

- Support a VP nominee with a strong focus on foreign policy (implied)

### Whats missing in summary

Details on Duckworth's specific plans or actions she might take in a VP role.

### Tags

#TammyDuckworth #BidenVP #ForeignPolicy #GunControl #MilitaryService


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So tonight we're gonna talk about Lieutenant Colonel Tammy Duckworth.
We're gonna do this because in all the time I've been doing this,
I have never gotten this many questions about somebody who hasn't even been
selected yet.
And most of them are asking, is this the person that can fill the role that I
said the Biden administration was gonna have to have in order to undo Trump's mess?
And we will get to that.
If you don't know, she is apparently on the shortlist to become Biden's VP nominee.
Okay, so a little bit about her.
She was born in Thailand and growing up traveled around Southeast Asia
with her father who was doing humanitarian and diplomatic work.
She came back to the US for school, went to college, did ROTC, and
joined the military.
She chose to fly helicopters because at the time that was one of the only
roles open to women that could go into combat.
In Iraq, her Black Hawk took an RPG and
she lost both legs and severely injured one arm.
She got a waiver, stayed in the military, and
retired as a lieutenant colonel maybe five years ago.
That's a guess, I could be wrong on that.
She has a Purple Heart, Meritorious Service Medal, Air Medal, and
a bunch of other stuff.
Her military record as far as what's public, exemplary, okay?
Her voting positions, exactly what you would expect from
a vice presidential nominee under Biden, no big surprises here.
Supports carbon capture, supports the Affordable Care Act,
supports a path to citizenship for choice.
Her only real stumbling block is gun control, and
it doesn't really matter where you sit on that.
It's just not a strong topic for her.
If you are pro Second Amendment, you're not gonna like her.
If you are looking for effective gun control, you're not gonna like her.
One of the things that she put out, one of the bills she sent up
was basically more or less a carbon copy of the law that I make fun of
in the three part series on gun control because it was completely ineffective.
It focused on cosmetics more than anything else.
So it's just not a strong topic for her.
And it may be a case where she's just rolling with the party,
because I know she is a gun owner.
But to be honest, none of this really matters as a VP.
Now, the question everybody's asking,
can she be the foreign policy person that I said the Biden administration
was gonna have to have in order to clean up Trump's mess?
Yeah, yeah, she could be.
She absolutely could be.
When she first ran for Congress, one of her big things was wanting to audit
how we spent money in regards to international affairs.
That's kind of a bizarre thing to run on your first time out.
That means you actually care about this subject.
She has the knowledge, she has experience,
she understands the way the world works, and she actually cares about the subject.
So yeah, if the Biden administration, if their team is looking to have a VP with
a strong foreign policy presence,
that's really what they want them to take care of, she'd probably be a good pick.
Especially if she's backed up by professional diplomats rather than
political appointees in State Department's top positions.
Yeah, I definitely see that as a very likely scenario.
She also has an added advantage in dealing with countries
that don't always take women seriously, because she's a war vet.
Even if they don't take women seriously, she survived an RPG.
She's gonna get more respect from
the leaders of countries that don't treat women as they should.
And I don't think that that should be undervalued if that's going to be the case,
if they're going to use the vice presidency for foreign policy.
Yeah, she's not a diplomat,
but she has the base of knowledge to accomplish it.
I don't know that that's what Biden's team's doing.
I don't know if she should be VP, but if that's what they're going for,
they're looking for somebody who can fill that role because Biden is lacking in it.
They've chosen well, as far as I can tell.
Anyway, it's just a thought.
Y'all have a good night. And I'll see you next time.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}