---
title: Let's talk about Bubba Wallace, Trump, definitions, and deceptions.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ow9ZyHoOV_o) |
| Published | 2020/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States demanded a race car driver, Bubba Wallace, apologize for an incident involving his crew misinterpreting something and asking for an investigation.
- President called the incident a hoax, displaying a misunderstanding of the term.
- Beau points out the definition of a hoax as a humorous or malicious deception, contrasting it with the situation involving Bubba Wallace's crew.
- Beau criticizes the President's hiring practices as being a mistake, not a hoax.
- He gives examples of humorous deceptions by the President, like claiming to make America great again or supporting the American people.
- Beau also provides an example of a malicious deception by the President, such as manipulating COVID-19 testing numbers.
- Despite Trump asking for apologies, Beau finds it ironic since Trump rarely accepts responsibility for his actions.
- Beau concludes that Trump and the Trump brand are hoaxes perpetuated on the American people for personal gain.
- He suggests that before demanding apologies from others, Trump should apologize for his actions during his presidency.
- Beau calls for accountability from Trump, which he believes is unlikely to happen.

### Quotes

- "The fact that the President of the United States is asking for an apology from anybody is hilarious."
- "Before I care about a race car driver apologizing for anything, I'd like to hear an apology from the President of the United States for his hoax."
- "The Trump brand is a hoax. It is a malicious deception."

### Oneliner

President's misunderstanding of a hoax leads to demands for apology, while Beau criticizes Trump's actions as a malicious deception.

### Audience

Voters, activists, concerned citizens

### On-the-ground actions from transcript

- Hold leaders accountable for their actions (implied)
- Demand transparency and honesty from public figures (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how the President's actions are perceived as deceptive and calls for accountability from leadership.

### Tags

#Deception #Accountability #Trump #Politics #Hoax


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about definitions, deceptions, the administration, NASCAR, and
apologies because it's 2020 and all of this stuff connects now.
The President of the United States has taken time out of his busy schedule to demand that
a race car driver, Bubba Wallace, apologize because his crew saw something and asked for
an investigation into it and they misinterpreted what they saw.
He called it a hoax.
He said it was a hoax.
I've come to the conclusion, and it's the only reasonable conclusion anybody can come
to, that the President doesn't know what this word means.
He uses it a lot, but I've never actually seen him use it correctly.
A hoax is a humorous or malicious deception.
That's what a hoax is.
A group of people seeing something and interpreting it one way that is completely reasonable given
all of the context and everything going on at the time and reporting it and there being
an investigation conducted and then it finding out, no, that's the incorrect interpretation,
that's not a hoax.
That's not a hoax.
A humorous or malicious deception.
What happened with Bubba Wallace's crew, that's a mistake, like all of the President's hiring
practices.
It's a mistake.
A humorous deception, for an example of that, would be like when the President says that
he's going to make America great again or that he supports the American people or that
he supports the troops.
That's a hoax.
It's a humorous deception.
He knows it's not true, we know it's not true, and his actions are so contradictory to these
statements that it's absurdly funny.
That's a hoax.
One example of a malicious deception would be like slowing down testing to make the numbers
look the way you want them to after saying that it was going to magically disappear.
That's a hoax.
It is a malicious deception because it causes harm.
The fact that the President of the United States is asking for an apology from anybody
is hilarious.
The man accepts responsibility for nothing.
The fact that he is asking for an apology related to something that he believes to be
a hoax is even funnier because his entire presidency has been a hoax perpetrated on
the American people for the sake of his own ego, his brand, and his wallet.
Trump is a hoax.
The Trump brand is a hoax.
It is a malicious deception.
Before I care about a race car driver apologizing for anything, I'd like to hear an apology
from the President of the United States for his hoax.
The last three years.
Seems to be much more relevant.
If he wants to talk about accountability, he should take some responsibility, be accountable
for his own actions, which is something we all know won't happen.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}