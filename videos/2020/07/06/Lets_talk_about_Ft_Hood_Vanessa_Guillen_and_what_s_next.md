---
title: Let's talk about Ft. Hood, Vanessa Guillen, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=V3ppc0Bkxjk) |
| Published | 2020/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the case of Specialist Vanessa Guillen from Fort Hood, Texas.
- Acknowledging the efficiency of army investigators and the slow but thorough process.
- Mentioning the 300 interviews conducted by investigators.
- Emphasizing the need for political pressure to ensure a proper investigation.
- Noting that Vanessa Guillen wanted to report harassment but was afraid of retribution.
- Pointing out the improvements made by the US military in addressing harassment.
- Urging for accountability if policies weren't followed or if commands failed.
- Suggesting the Senate Armed Services Committee launch an investigation.
- Providing Twitter handles of committee members for public outreach.
- Mentioning strong allies like Elizabeth Warren and Tammy Duckworth for support.
- Stressing the importance of public support to keep the story alive and demand answers.
- Expressing concerns about the lack of security in the armory where the incident occurred.
- Calling for a clear picture of what happened beyond the basic details of the crime.
- Advocating for congressional hearings to address the larger issues beyond the crime itself.

### Quotes

- "We can't let this story fade away because there are a lot of questions."
- "The US military for years was moving ahead on this."
- "Because there are a lot of things that should have been in place that should have prevented this."
- "We need congressional hearings on this."
- "Y'all have a good night."

### Oneliner

Beau addresses the need for thorough investigation, accountability, and congressional hearings regarding Specialist Vanessa Guillen's case at Fort Hood, Texas.

### Audience

Advocates for Justice

### On-the-ground actions from transcript

- Contact members of the Senate Armed Services Committee (suggested)
- Tweet at committee members for support (implied)
- Reach out to committee members on social media (implied)
- Call committee members for action (implied)

### Whats missing in summary

Full context and emotional depth can be better understood by watching the full transcript.

### Tags

#Justice #Accountability #Military #Investigation #Congress #VanessaGuillen


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we are going to talk about Fort Hood, Texas, Specialist Vanessa Guillen and
what happens next, what should happen next anyway.
When this first happened I had a couple of people reach out and ask me to do a video
on it.
I didn't for a reason I'm about to explain.
There are two phases to this and the second phase might slow down the first phase.
The main reason I didn't want to do it is that army investigators are really good.
We've talked about it before, normal law enforcement runs closure rates from the teens to like
60%.
Military investigators run them in the 90's.
I wasn't worried about them not finding out what happened, they're just slow but that's
why their closure rates are so high.
I want to say they did 300 interviews totaling like 10,000 hours or something.
They're very methodical and it's very tedious so it takes a lot of time and the one thing
that would slow them down even more is political pressure.
But the actual crime and if you don't know what happened, a soldier at Fort Hood turned
up missing and then her body was found.
The story that goes along with it, the story itself merits an investigation and that requires
some political pressure.
One of the key elements is that she wanted to report harassment but was afraid of retribution
so she didn't.
That's part of the storyline.
That shouldn't happen anymore.
The US military has made huge strides over the last 10 years in regards to that exact
topic.
If those policies aren't being followed, weren't being followed at Fort Hood, if her command
failed her, if her command knew and didn't act, if her command had a suspicion and didn't
tell the investigators the second they showed up, hey you need to be looking at this guy,
we need to know.
It may turn out that her command did not fail her but we don't know and this isn't the civilian
world.
We don't have to accept that as an answer.
I don't know doesn't cut it.
There's a thing called the Senate Armed Services Committee.
They can launch an investigation into this and they can find out exactly how one of our
soldiers was left twisting in the wind.
So that needs to happen.
By the time y'all watch this I will have tweeted out all of their Twitter handles and all of
that stuff and either in the description or the first pinned comment below we'll have
a link that has all of their names and if you scroll all the way to the bottom in very
small print you will find a phone number.
The good news is that we have strong allies on this committee.
People that would be interested in finding out what happened.
Elizabeth Warren, Tammy Duckworth, and as much as I drag him on Twitter constantly I
think Senator Tom Cotton would actually be very interested in this.
So those would be the three that I reach out to if one of your senators isn't on the list.
It would not surprise me to find out that they've already decided to hold hearings and
conduct an investigation.
A little bit of public support for that may go a long way though.
So still contact them, tweet them, reach out to them on social media, call them, whatever.
We can't let this story fade away because there are a lot of questions and I waited
before making a video, I waited to find out what we could.
I have more questions than I have answers.
Not just about her command but they're saying this occurred in an armory and nobody knew.
That should be really unlikely.
If that's the case we have to assume that everything there is not secured.
We need to know what went down here beyond just the bare bones of the actual crime.
Because there are a lot of things that should have been in place that should have prevented
this.
Because the stories that we have to go on, they're not telling enough.
They're not giving us a clear picture and we need one.
The US military for years was moving ahead on this.
And it seems like in the last two or three years that has gone to the wayside and we
need to know why.
So I don't want this story to fade away simply because the crime itself was solved.
Because that is only half of this story.
We need congressional hearings on this.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}