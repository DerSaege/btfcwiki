---
title: Let's talk about Trump, the Founders, and a list you need for July 4th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Lk7XBcWIZIU) |
| Published | 2020/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the upcoming July 4th, Independence Day, and the President's typical actions on this day.
- He questions if the Founding Fathers' beliefs truly match up with the President's.
- Beau suggests creating a list of the President's positions to compare them with the Founders.
- He points out the President's inaction in response to various issues affecting the country.
- Beau mentions the President's anti-immigration stance, obstruction of justice, and desire to control judges.
- He criticizes the President's use of law enforcement and his interference in domestic issues.
- Beau expresses concern about the President's handling of trade, public health, and military superiority.
- He draws parallels between the President's actions and the reasons listed in the Declaration of Independence for altering or abolishing government.
- Beau reminds listeners to read the Declaration of Independence and analyze how it relates to current political situations.
- He encourages people to understand the true spirit of patriotism and the Founders' intentions.

### Quotes

- "Those attempting to alter or abolish a government that has become destructive to the ends of preserving life, liberty, and the pursuit of happiness. Those are the patriots."
- "When the streets fill with people, understand those are the people who are the ideological descendants of the Founders."
- "Just remember that tomorrow as he tries to paint himself as some patriot. They wouldn't have liked him."
- "He is more akin to the king than to those who signed the Declaration of Independence."
- "Declaration of Independence says you're wrong. You're supposed to fix it."

### Oneliner

Beau outlines how the President's actions diverge from the Founding Fathers' beliefs, urging a critical comparison on Independence Day.

### Audience

American citizens

### On-the-ground actions from transcript

- Read the Declaration of Independence to understand its relevance to current political events (suggested)
- Analyze how the President's actions relate to the reasons listed in the Declaration of Independence for altering or abolishing government (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the misalignment between the President's actions and the principles of the Founding Fathers, urging citizens to critically think about patriotism and government.

### Tags

#IndependenceDay #FoundingFathers #President #DeclarationOfIndependence #Patriotism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about tomorrow, July 4th, Independence Day.
And we're going to talk about a list that I think you need to have handy for tomorrow.
Because tomorrow will be like every other Independence Day.
The President's going to come out and talk about the Declaration of Independence, at
least the first paragraph and a half of it.
And then he'll stand in front of a giant row of flags and talk about how he's the ideological
descendant of the Founders and he believes the same things that they believed and he's
just trying to preserve what they believed.
See the thing is, I'm a pretty good student of history.
I don't know that the Founders would have agreed with him.
I don't know that the Founders would have even liked him.
What we could do is come up with a list.
A list that outlines some of his positions and see how they match up with the Founders.
I would suggest first we talk about his inaction that has left America open to internal and
external dangers.
You know, like him ignoring the bounties or him doing nothing while everybody got sick.
Maybe talk about his anti-immigration stance.
Talk about his issues with obstructing justice.
Talk about his desire to make judges dependent on his will.
Talk about sending swarms of officers out to harass us.
Law and order.
Cutting off trade through tariffs.
Exciting domestic issues, squabbles, you know, because there are good people on both sides.
Refusing to pass laws that are necessary for the public good while everybody got sick.
Not allowing governors to act on their own.
You have to look no further than DeSantis or Abbott to see that in action.
Maybe making the military superior to civil authorities.
Send in the troops.
I don't think that they would have liked this platform.
I'm certain they wouldn't have.
Because in the Declaration of Independence, in that part that nobody talks about, there's
a list of all of the reasons that our Founders felt it necessary to alter or abolish the
system of government that they had.
And that's the list in plain English.
Not all of it, but most of it.
Lines up with Trump almost to the letter.
And there's more that lines up with him too.
Just remember that tomorrow as he tries to paint himself as some patriot.
They wouldn't have liked him.
He is more akin to the king than to those who signed the Declaration of Independence.
And they wrote it down.
And yeah, for those that are curious, being anti-immigration was one of the reasons that
we separated.
Because we wanted immigration.
This country was quite literally founded on immigration.
It's in the founding documents.
When the streets fill with people, understand those are the people who are the ideological
descendants of the Founders.
Those attempting to alter or abolish a government that has become destructive to the ends of
preserving life, liberty, and the pursuit of happiness.
Those are the patriots.
Love it or leave it, no.
Declaration of Independence says you're wrong.
You're supposed to fix it.
Fix the door.
Anyway, tomorrow may be a good day, or today.
Read the Declaration of Independence, especially that list of reasons.
The long train of abuses.
See if they don't line up to Trump's talking points.
The things that he advocates are the very reasons for the American Revolution.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}