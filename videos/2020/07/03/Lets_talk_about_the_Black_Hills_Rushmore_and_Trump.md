---
title: Let's talk about the Black Hills, Rushmore, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pHmGYgbTa0o) |
| Published | 2020/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the cultural and historical significance of the Black Hills and its connection to the Constitution.
- Trump's visit to Rushmore has sparked controversy similar to his visit to Tulsa, raising questions about the timing of his appearances.
- Beau questions why the Sioux do not have the Black Hills despite the 1868 Fort Laramie Treaty granting it to them.
- The U.S. took back the Black Hills from the Sioux due to the discovery of gold, an act Beau deems unconstitutional.
- Despite a 1980 Supreme Court ruling that the taking of the land was unconstitutional, the Sioux were offered cash instead of the land's return.
- Beau explains that the Black Hills hold deep cultural and spiritual significance for the Sioux, being considered the birthplace of their culture.
- Beau stresses that it is ultimately up to the Sioux to decide the fate of their land, not outsiders like himself.
- Various proposed solutions to the issue of the Black Hills ownership have been suggested over the years, but Beau asserts that it is solely the Sioux's decision.
- Beau advocates for returning the Black Hills to the Sioux, urging respect for treaties, the Constitution, and the rightful owners of the land.
- Ending with a call to honor treaties, the Constitution, and give the land back to its rightful owners.

### Quotes

- "It is their land. Period."
- "It's their land. They get to decide."
- "Constitutionally, it's theirs. We need to honor the treaties."
- "Let's give it back."
- "Give it back. I don't know why this is a debate."

### Oneliner

Beau stresses the constitutional importance of honoring treaties and returning the Black Hills to the Sioux, respecting their cultural significance and rightful ownership.

### Audience

Advocates and Activists

### On-the-ground actions from transcript

- Contact indigenous-led organizations to support efforts for the return of sacred lands (exemplified)
- Join community initiatives that center indigenous voices and rights (exemplified)

### Whats missing in summary

Deeper insights into the cultural and spiritual significance of the Black Hills for the Sioux community.

### Tags

#BlackHills #IndigenousRights #Treaties #Constitution #CulturalSignificance #CommunityJustice


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the Black Hills.
We're going to talk about the cultural significance, the historical significance, we're going to
talk a little bit about the Constitution and how it relates to the Black Hills.
We're going to do this because Trump is going to Rushmore and this has caused a stir, just
like going to Tulsa.
Whoever is picking his locations for his appearances is either really bad at the timing or is doing
it intentionally, one of the two.
I don't know which.
We're going to start by talking about the Constitution because those people who have
an attachment to Mount Rushmore, I would imagine that they view themselves as patriots, as
people who love America and love the ideas that America is supposed to represent.
So we're going to start with the Constitution because that should be one of the most important
things.
Not a symbol, but the most important thing.
Article 6, Clause 2.
This Constitution and the laws of the United States which shall be made in pursuance thereof
and all treaties made, or which shall be made under the authority of the United States,
shall be the supreme law of the land.
Treaties are the supreme law of the land.
In 1868, the Fort Laramie Treaty gave this region to the Sioux.
Now I'm going to use the term Sioux in this just for sake of continuity.
For those that don't know, there are other terms that are way more appropriate, but right
now we're just going to stick with that.
Okay, so it was given to them.
Why don't they have it?
If it's their land and the treaty said they should have it undisturbed and it's the supreme
law of the land, why don't they have it?
Because there's gold in them their hills.
Shortly after it was given to them, the U.S. took it back because of gold.
That's why.
This was unconstitutional.
This was plainly unconstitutional.
It is their land.
Period.
If we honor the treaties and honor the Constitution, it's their land.
Now I know somebody else out there is going, here we go, another YouTube constitutional
scholar.
The Supreme Court said that taking the land back was unconstitutional.
They said this in 1980.
But rather than ordering the land returned, because it is still a tool of the settlers,
they offered a cash payment and the Sioux didn't take it.
Why?
Why wouldn't they take the money?
Because it's Eden.
It is Eden.
That's the only way I can think to explain it to non-natives.
It's Eden.
It's the birthplace of their culture.
To them, it is the birthplace of humanity.
When we say it was given to them in 1868, that is historically inaccurate.
They had been there for centuries.
Indigenous culture after indigenous culture.
And each one that came adopted the traditions and the outlook of those that came before
in relation to the Black Hills.
It has been sacred land for centuries.
And it's theirs.
It's their land.
Historically, traditionally, morally, ethically, and constitutionally, it's their land.
Now over the years, a whole bunch of people have come up with solutions to this problem.
Come up with ideas on what to do with their land.
Doesn't matter.
My opinion on what should happen doesn't matter.
I'm not Oglala.
I'm not Sioux.
It's their land.
They get to decide.
And some of these solutions, well, we'll give everything back except for Rushmore.
We'll give Rushmore back, but they have to keep it up, but they'll get to keep the revenue.
We'll remove a giant chunk of their land and put it somewhere else.
It's their land.
They get to decide what to do with it.
Here's a crazy idea.
Let's give it back.
Let's give it back and hope that they are better people.
Hope that they won't deface something that has become important to American mythology.
And that's what it is.
It's mythology.
Because the people who are standing here talking about how important it is to honor this symbol,
this symbol, that's what it is.
It's a symbol.
In order to honor that symbol, they're going to undermine the main thing the symbol's supposed
to represent.
Constitutionally, it's theirs.
We need to honor the treaties.
We need to honor the Constitution.
We need to give the land back.
Let the chips fall where they may.
They may decide to remove it.
Okay?
And are we willing to sell out the Constitution to protect some statues, to protect a carving
in the side of a mountain that never should have been there?
Or maybe they leave it up.
We don't know.
And we don't have a say in it.
It's not ours.
Let's say they do take it down.
So what?
What happens?
Does the United States cease to be?
Does it no longer mean anything?
Because the funny thing is that by keeping it up, you're undermining the United States.
You're undermining the Constitution.
It's theirs.
Give it back.
I don't know why this is a debate.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}