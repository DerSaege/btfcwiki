# All videos from July, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-07-31: Let's talk about what Georgia can teach teachers.... (<a href="https://youtube.com/watch?v=_VWrBblE24A">watch</a> || <a href="/videos/2020/07/31/Lets_talk_about_what_Georgia_can_teach_teachers">transcript &amp; editable summary</a>)

Beau advises teachers in high-risk areas to prepare for teaching like a combat deployment, stresses the importance of protective gear, and urges community advocacy for online learning amidst rising COVID cases.

</summary>

"Treat [preparations] like a combat deployment."
"Wear your protective equipment at all times. Doesn't matter how annoying it is. Doesn't matter how hot it makes you. Wear it."
"We know what's going to happen. We don't need to reproduce the results."
"If you're active in your community and you have any sway over the school board, the school district in your area, now's the time to use it."
"Even with the guidelines, they're saying it's moderate risk."

### AI summary (High error rate! Edit errors on video page)

Explains the situation at a summer camp in Georgia where out of 597 attendees, 260 tested positive within four days, with the highest positive tests in the 6-10 age group.
Dispels the idea that children are not impacted by the virus as about three quarters showed symptoms.
Advises teachers, especially in areas with districts functioning as usual, to prepare like they are going on a combat deployment, suggesting they organize their affairs and have a designated person know where all their critical documents are located.
Urges people to always wear protective equipment, drawing a parallel to soldiers removing their back plate because it was "cool," leading to dangerous consequences.
Encourages community members with influence to advocate for online learning as the lowest risk option according to the CDC guidelines.
Recommends being proactive in influencing school boards and districts to prioritize safety over following guidelines that pose moderate risks.

Actions:

for teachers, community members,
Advocate for online learning with school boards and districts (exemplified)
Prepare critical documents and share their location with a trusted person (exemplified)
</details>
<details>
<summary>
2020-07-31: Let's talk about how that tweet can't happen here.... (<a href="https://youtube.com/watch?v=yqLqj8mgUzc">watch</a> || <a href="/videos/2020/07/31/Lets_talk_about_how_that_tweet_can_t_happen_here">transcript &amp; editable summary</a>)

Beau explains Murphy's Law and warns not to overlook the President's intent in delegitimizing the election, pointing out his focus on power over democracy.

</summary>

"Accept this for what it is. He's telling you his intent."
"At the end of the day, President Trump wants the same thing that every first term president wants. A second term."
"He does not care about democracy. He does not care about the republic. He doesn't care about your voice. He never did."
"Because we thought the same thing they thought. It can't happen here. But it can."

### AI summary (High error rate! Edit errors on video page)

Explains Murphy's Law and its relevance to the President's intent.
Points out that the diversion being ignored might be the main attack.
Raises concerns about the President's tweets being dismissed as distractions.
Emphasizes the importance of not overlooking the President's intent in delegitimizing the election.
Conveys that the President's actions are about power and self-interest, not democracy.
Warns about the potential consequences of not taking the situation seriously.
Urges people to realize that what they think can't happen actually can.

Actions:

for american citizens, voters,
Challenge attempts to delegitimize the election (implied)
Take a stand for democracy and the republic (implied)
Stay informed and engaged in current events (implied)
</details>
<details>
<summary>
2020-07-31: Let's talk about faith in Trump.... (<a href="https://youtube.com/watch?v=eJyov5ZykO4">watch</a> || <a href="/videos/2020/07/31/Lets_talk_about_faith_in_Trump">transcript &amp; editable summary</a>)

Beau addresses the impact of faith on public health, contrasting the sacrifices made by some with the dangers of blind allegiance to political leaders prioritizing personal gains over public safety.

</summary>

"Your belief in a creator or your lack of belief is not my business."
"They're not the fanatics. They value human life. The real fanatics are here."
"If you are one of those people who buy into that stereotype, I want you to take a look at Hajj."
"That fanaticism, when it comes to a political leader in the United States, many times it borders on a cult."
"But there are the faithful. Maybe you will be rewarded."

### AI summary (High error rate! Edit errors on video page)

Addressing a topic rarely discussed due to its personal nature, focusing on faith and its impact on society.
Expressing the importance of discussing faith when it negatively impacts others.
Exploring the perception of Islam as a fanatical faith in the United States.
Detailing the significance of the Hajj pilgrimage in Islam and the adaptations made this year due to the pandemic.
Comparing COVID-19 fatality rates between Florida and Saudi Arabia, reflecting the seriousness of public health measures.
Criticizing the blind faith and sacrifices demanded by political leaders in pursuit of economic gain.
Contrasting the values of faith and human life in the context of pandemic response.
Drawing parallels between blind faith in political leaders and cult-like behavior.
Warning about the dangers of blind allegiance to leaders that prioritize personal gain over public safety.

Actions:

for concerned citizens,
Research the significance of religious practices like Hajj to better understand different faith perspectives (suggested).
Advocate for prioritizing public health and safety over political or economic interests (implied).
Encourage critical thinking and questioning blind loyalty to political leaders (implied).
</details>
<details>
<summary>
2020-07-30: Let's talk about pulling from Germany and slogans.... (<a href="https://youtube.com/watch?v=8K_vCcxU9TQ">watch</a> || <a href="/videos/2020/07/30/Lets_talk_about_pulling_from_Germany_and_slogans">transcript &amp; editable summary</a>)

Slogans like "bring the troops home" may lose original meaning over time, as withdrawing troops from Germany potentially leads to increased troop presence and militarization in Europe.

</summary>

"The idea behind bringing the troops home was to de-escalate, not relocate."
"The goal isn't to bring troops home. It's to increase the number."
"Peace advocates oppose this because they understand the end goal is more troops."
"Moving troops closer to Russia makes them more susceptible to a surprise attack."
"The slogan 'bring the troops home' may not mean reducing troop numbers."

### AI summary (High error rate! Edit errors on video page)

Slogans can lose their original meaning over time, like "bring the troops home" in the context of withdrawing troops from Germany.
The concept of bringing troops home was initially to de-escalate and disband, not relocate them within Europe.
Some groups, including foreign policy hawks and those wanting fewer troops, are in rare agreement due to confusion over troop movements.
Withdrawing troops from Germany is not about demilitarization but about relocating and potentially increasing troop numbers.
The move may actually lead to more German NATO troops and contribute to further militarization in Europe.
Foreign policy hawks support keeping troops in Germany as a strategic move in case of a ground incursion.
Moving troops closer to Russia may not act as a deterrent but could make them more vulnerable to surprise attacks.
Peace advocates oppose the troop movements as they see the end goal is to increase troop numbers and potentially lead to further military engagements.
Lobbying and corruption in the defense industry play a role in decisions related to troop movements.
The slogan of bringing troops home does not necessarily mean reducing troop numbers but could result in an increase.

Actions:

for foreign policy analysts,
Contact advocacy groups focused on demilitarization (suggested)
Join organizations advocating for peace and diplomacy (suggested)
Organize events to raise awareness about the implications of troop movements (implied)
</details>
<details>
<summary>
2020-07-30: Let's talk about Trump's call to suspend the elections.... (<a href="https://youtube.com/watch?v=TKBoQZfYPaI">watch</a> || <a href="/videos/2020/07/30/Lets_talk_about_Trump_s_call_to_suspend_the_elections">transcript &amp; editable summary</a>)

President Trump's call to delay elections, likened to dictator tactics, prompts Beau to urge Republicans to oppose him, warning of a threat to American freedom and values.

</summary>

"The silence from the Republican Party has been deafening."
"If you still support Donald Trump, you do not support the United States."

### AI summary (High error rate! Edit errors on video page)

President Donald Trump called for the suspension of elections, citing concerns about universal mail-in voting leading to inaccuracies and fraud.
Trump's call to delay the elections until vague preconditions are met mirrors tactics used by dictators in history to stay in power.
Beau criticizes the Republican Party for not openly opposing Trump's call to delay the elections, accusing them of undermining the Constitution.
Beau urges Republicans to stand up against Trump's actions to protect American freedom, warning that history will judge them based on their response.
He calls on Republican leadership to deny Trump the nomination, describing the situation as the most significant threat since 1860.
Beau accuses Trump of displaying fascist characteristics and attempting to subvert the Constitution through his statements on social media.
He warns that those who continue to support Trump are betraying American values and endangering the country's well-being.
Beau concludes by stating that supporting Trump is incompatible with supporting the United States, implying a moral and patriotic duty to oppose him.

Actions:

for republican party members,
Stand in open opposition to Trump's call to delay elections (suggested)
Advocate within the Republican Party leadership to deny Trump the nomination (suggested)
</details>
<details>
<summary>
2020-07-29: Let's talk about Trump's interview on foreign policy.... (<a href="https://youtube.com/watch?v=20oyDJZWJis">watch</a> || <a href="/videos/2020/07/29/Lets_talk_about_Trump_s_interview_on_foreign_policy">transcript &amp; editable summary</a>)

Beau criticizes Trump's lack of understanding in a damaging interview, stressing the need for foreign policy expertise in the next administration to repair international relationships.

</summary>

"He's inept. He's incompetent when it comes to this subject."
"That's the intent. They don't want us withdrawn."
"The next administration has a mess to clean up."
"Imagine our relationships with the countries that we don't think about."
"It's hard to withdraw from somewhere when you're constantly taking hits."

### AI summary (High error rate! Edit errors on video page)

Critiques Trump's interview, pointing out his lack of understanding on various subjects.
Trump falsely claims he never saw intelligence on Putin's bounties, despite it being in his daily brief.
Trump brags about receiving intelligence briefings 2-3 times a week, falling short of the necessary 7.
Beau notes that Trump's limited briefings hinder his ability to understand and address global issues effectively.
Trump's lack of comprehension regarding foreign policy is evident in his statements about arming the Taliban and historical events.
Beau criticizes Trump's incompetence in dealing with Putin compared to the Russian leader's intelligence and sharpness.
Beau suggests that Trump's ineptitude may inadvertently benefit the US by preventing further entanglement in conflicts.
Trump's misunderstanding of Russian intentions in Afghanistan could lead to unintended consequences if he were to respond.
Beau stresses the importance of having a knowledgeable foreign policy expert in the next administration to repair damaged international relationships.
Overall, Beau expresses concern about the chaotic state of US foreign relations under Trump's leadership.

Actions:

for foreign policy experts,
Advocate for competent foreign policy experts in positions of power (implied)
Stay informed on global issues and political developments (implied)
Support efforts to repair and strengthen international relationships (implied)
</details>
<details>
<summary>
2020-07-29: Let's really talk about dropping the MIC and defense spending.... (<a href="https://youtube.com/watch?v=fXmeMyMcupo">watch</a> || <a href="/videos/2020/07/29/Lets_really_talk_about_dropping_the_MIC_and_defense_spending">transcript &amp; editable summary</a>)

President Eisenhower's warning about the military-industrial complex reveals a broader issue of corruption influencing policy-making in various industries for profit.

</summary>

"The military-industrial complex isn't really a thing. A guy named Noam Chomsky said there's no such thing as the military-industrial complex. It's just the industrial system."
"All of this, all of this, it's just corruption."
"It's corruption. We need to start calling it what it is."
"It's just corruption. Maybe it's better to just give people the term that they know, that they can recognize."
"They were all under the influence of corruption, not the military-industrial complex."

### AI summary (High error rate! Edit errors on video page)

President Eisenhower's farewell address introduced the term "military-industrial complex" in 1961, warning about the influence of the arms industry on the government.
The arms industry profits by influencing government decisions to purchase more arms, leading to a cycle of increased militarization.
This influence results in policies that may not serve the public good, encouraging interventions and a war-based economy.
The military-industrial complex extends its influence to schools, contributing to the normalization of a militarized society.
Beau suggests curbing this influence to prevent further militarization and negative consequences for society.
Noam Chomsky argues that it's not just the military-industrial complex but a broader issue of industrial influence over policy-making for profit.
The term "military-industrial complex" adds a veneer of legitimacy to what is essentially corruption in various sectors like private prisons and big agriculture.
Lobbying and campaign contributions lead to policies that benefit large industries at the expense of the public good.
By framing these issues as corruption rather than complex terms, it becomes clearer how profit-driven decisions harm society.
Beau advocates for recognizing and addressing corruption in government decisions, particularly in defense spending, which often goes unquestioned due to pervasive influence.

Actions:

for policy reform advocates,
Advocate for transparency in government spending and policy-making (exemplified)
Support campaigns against corruption in various sectors, including defense spending (exemplified)
Educate others on the influence of profit-driven decisions on public policy (implied)
</details>
<details>
<summary>
2020-07-28: Let's talk about what the 2021 school year might be like.... (<a href="https://youtube.com/watch?v=QXt6XzjwBUI">watch</a> || <a href="/videos/2020/07/28/Lets_talk_about_what_the_2021_school_year_might_be_like">transcript &amp; editable summary</a>)

Beau instructs second grade students on safety procedures, warns about consequences of not following protocols, and limits interactions like sharing food or celebrating birthdays.

</summary>

"One of you will carry something home to your parents."
"No birthdays."
"Welcome to second grade."

### AI summary (High error rate! Edit errors on video page)

Introduces themselves as the instructor for the 2021 school year due to a lack of teachers in the school district.
Mentions being on loan from Fort Benning, Georgia, through a Department of Defense initiative.
Expresses wearing a whimsical mask to appear approachable but warns students to stay six feet away due to being "stinky-hand bags of germs."
Informs students that they will be issued either a surgical mask or a P100 respirator, with politicians' children receiving the latter.
Emphasizes the importance of following safety procedures like handwashing and not sharing food due to last year's incidents.
Warns students that one of them may carry something home to their parents if safety protocols are not followed.
Mentions the availability of forms for non-familial foster care and DNRs in the office.
Instructs students to keep personal belongings, like a box of crayons labeled with their names, to themselves.
Concludes the safety procedure meeting for second grade students and asks if there are any questions.

Actions:

for parents, teachers, school administrators,
Keep personal belongings like food and crayons to yourself (implied)
Follow safety procedures like handwashing and wearing masks (implied)
</details>
<details>
<summary>
2020-07-28: Let's talk about absurdity and getting longer.... (<a href="https://youtube.com/watch?v=YfdQTDhWCQs">watch</a> || <a href="/videos/2020/07/28/Lets_talk_about_absurdity_and_getting_longer">transcript &amp; editable summary</a>)

Beau talks about the absurdity of the administration, from Trump Jr.'s Twitter suspension to the Attorney General's complaints about prison sentences, hinting at double standards and teasing upcoming long-format content.

</summary>

"US prison sentences are far too long."
"You know you're under tyranny when the government is able to do things you can't."
"If you want to laugh instead of cry at today's events, go watch that video I released last night."

### AI summary (High error rate! Edit errors on video page)

Released a video on absurdity, then saw today's headlines reflecting absurdity of the administration.
Trump Jr. had his Twitter suspended due to false healthcare tweets.
Attorney General, overseeing mass incarceration, complains about unfair prison sentences.
Acknowledges that US prison sentences are too long but makes no effort to address the issue.
Describes tyranny as when the government can do things you can't, hinting at double standards.
Teases upcoming long-format content where he will deep dive into specific topics.
Long-format content won't replace daily videos but will be additional.
Encourages watching his previous video for a laugh amidst current absurd events.

Actions:

for viewers,
Watch Beau's video for a humorous take on current events (suggested).
Stay tuned for Beau's upcoming long-format content (suggested).
</details>
<details>
<summary>
2020-07-27: Let's talk about what happened in Florida in 8 days.... (<a href="https://youtube.com/watch?v=7QJFJsdgrRc">watch</a> || <a href="/videos/2020/07/27/Lets_talk_about_what_happened_in_Florida_in_8_days">transcript &amp; editable summary</a>)

Governor in Florida pushes to reopen schools against advice, risking children's health and economy while prioritizing politics over safety.

</summary>

"Kids transmit just as easily."
"There's no reason to reopen these schools."
"The numbers say it's a bad idea."
"This is a bad idea."
"But hey, the guy who didn't know how to put on his mask, well, he says it's
important."

### AI summary (High error rate! Edit errors on video page)

Governor in Florida is forcing schools to reopen despite knowing it's a bad idea, following Trump's direction.
In just eight days, cases involving children in Florida increased by 34%, with almost a thousand new cases daily.
Hospitalizations also increased by 23% in the same period.
Research from South Korea shows that children transmit the virus as easily as adults.
Parents were misled into thinking it's safe for kids to return to school, neglecting the risks.
Beau urges parents to opt for distance learning or homeschooling if possible.
Florida districts have homeschooling-friendly policies that parents can utilize.
The reopening of schools will further increase cases and pose a significant risk to public health.
Beau criticizes the prioritization of the economy over the well-being of children and families.
Despite warnings, some prioritize economic reasons for reopening schools, neglecting safety concerns.
Beau stresses that there is no valid reason to reopen schools and that it's a dangerous decision.
He calls out the disregard for facts and safety in favor of political allegiance.
Teachers, school administrators, and data all indicate that reopening schools is ill-advised.
Beau ends by questioning the logic behind prioritizing reopening schools against all evidence.

Actions:

for parents, educators, concerned citizens,
Opt for distance learning or homeschooling if feasible (suggested)
Research homeschooling policies in Florida districts and take necessary steps to enroll children (implied)
</details>
<details>
<summary>
2020-07-27: Let's talk about how we get off the highway to escalation.... (<a href="https://youtube.com/watch?v=xJ1BAh0ZbKw">watch</a> || <a href="/videos/2020/07/27/Lets_talk_about_how_we_get_off_the_highway_to_escalation">transcript &amp; editable summary</a>)

Beau stresses community action over waiting for politicians, advocating for grassroots movements to build a just society without violence.

</summary>

"It's always bad."
"Don't wait for him. It's going to take us to do it. Might as well start now."
"Power to the people? That's how you achieve it. Social networks and cell phones, not guns."
"Be the anvil."
"Maybe we should just build the system that we want rather than trying to fight against a system that's been there."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of getting off the current "highway" we're on, discussing different exits to take.
He presents a strategy that, although harder and taking longer, has proven successful in the past.
Beau explains the gravity of the situation through numbers, using the example of Syria to illustrate the potential loss if the current path continues.
He stresses the importance of community action and starting now to create change rather than waiting for political figures like Biden.
Beau advocates for building small community networks and making positive changes locally to gain political influence from the ground up.
He warns against blindly following political figures and instead encourages grassroots movements for real change.
Beau underlines the power of social networks and community organizing over violence in achieving a just society.
He cautions against escalating situations into violence, advocating for defensive strategies in protests.
Beau suggests using symbolic gestures like carrying the American flag in protests to gain wider public support and win the PR battle.
He concludes by proposing the idea of building a new system rather than solely fighting against the existing one.

Actions:

for community members, activists,
Build small community networks and initiate positive changes now (suggested)
Carry out local activism with friends to influence policy decisions (exemplified)
Use social networks and community organizing to gain political clout (implied)
</details>
<details>
<summary>
2020-07-26: Let's talk about black lives, uniforms, and causes.... (<a href="https://youtube.com/watch?v=IzBXlc8gtOc">watch</a> || <a href="/videos/2020/07/26/Lets_talk_about_black_lives_uniforms_and_causes">transcript &amp; editable summary</a>)

Beau addresses the implications of uniforms in movements, urging against easy targeting and advocating for the harder but peaceful route towards a better outcome.

</summary>

"Being able to blend in, in a situation like that, drastically increases your survivability."
"If this goes open and escalates into something like that, they don't."
"The end of something can often be determined by the beginning."
"You give up, but you don't give in either."
"It's harder. It's harder. It's going to have a better outcome."

### AI summary (High error rate! Edit errors on video page)

Addressing the pushback he received after a previous video about uniforms and causes.
Exploring the sentiment of the necessity of uniforms from a broader perspective.
Questioning the practicality and implications of uniforms in certain situations.
Emphasizing the importance of blending in for safety and freedom of movement.
Identifying the diverse goals of the movement, ranging from social justice to equality for all.
Tracing the roots of the movement back to helping, protecting, and elevating people of color.
Warning against escalating to a next level where identifiable groups face greater risks.
Urging against adopting uniforms that make marginalized groups easy targets.
Rejecting the idea of sacrificing a demographic for political gains.
Advocating for the harder but peaceful route for a better outcome.

Actions:

for activists, advocates, allies.,
Advocate for blending in and maintaining safety in movements (implied).
Reject the idea of sacrificing any demographic for political gains (implied).
Support peaceful approaches for better outcomes (implied).
</details>
<details>
<summary>
2020-07-26: Let's talk about a lesson for America from my son.... (<a href="https://youtube.com/watch?v=MVAjMktiaZ4">watch</a> || <a href="/videos/2020/07/26/Lets_talk_about_a_lesson_for_America_from_my_son">transcript &amp; editable summary</a>)

Beau warns against the irreversible consequences of ignoring warning signs and pushing towards a crisis, drawing parallels between his son's habanero dip experience and a country in turmoil.

</summary>

"You don't want this. You do not want this."
"Because if this weekend taught us anything, it's that if this goes too far, they're not going to have to worry about a re-election."
"There is no good guy. There is no glory. There is no happy ending to any of this."
"That militant talk, it's all cool. Sounds good. Until you have the chip in your hand."

### AI summary (High error rate! Edit errors on video page)

Beau shares a story about his son and habanero dip to illustrate a bigger point.
His son, despite being warned, tries the dip and experiences the burning sensation.
Beau uses this analogy to draw parallels to a country facing a crisis.
He paints a grim picture of a country, Suistan, dealing with a public health crisis and oppressive regime.
The regime in Suistan is neglectful and focused on extracting wealth.
Beau compares the situation in Suistan to the current state in the USA under Joe Biden's administration.
He warns about the consequences of ignoring warning signs and pushing towards irreversible outcomes.
Beau criticizes the administration for their actions and lack of consideration for the people.
He cautions against the path the country is heading towards, stressing the severe consequences.
Beau ends by urging people to listen to those who have experienced similar situations and avoid the impending crisis.

Actions:

for citizens, activists, voters,
Pay attention to warning signs and advocate for change (implied)
Support and listen to those who have experienced similar crises (implied)
Take action to prevent irreversible outcomes (implied)
</details>
<details>
<summary>
2020-07-25: Let's talk about Houston and intelligence.... (<a href="https://youtube.com/watch?v=PC-0El3LvUw">watch</a> || <a href="/videos/2020/07/25/Lets_talk_about_Houston_and_intelligence">transcript &amp; editable summary</a>)

Diplomacy and intelligence work are intertwined, with foreign nations exploiting vulnerabilities in US response to the crisis for their advantage.

</summary>

"Every embassy ran by every country, everywhere in the world, engages in intelligence work."
"The idea that the Chinese government was spying on us, that's a fact."
"Trump's failure to manage this showed exactly how vulnerable the United States is to this type of warfare."

### AI summary (High error rate! Edit errors on video page)

Explains the relationship between diplomacy and intelligence work, mentioning that every embassy engages in intelligence work and has spies.
States that the Chinese government was spying on the US, particularly at a specific consulate.
Suggests that the intelligence agencies' choice to make arrests rather than run a disinformation campaign was politically motivated.
Notes that foreign intelligence services are spying on US medical research due to the public health crisis and Trump's mismanagement of it.
Emphasizes that monitoring the research process gives other countries insight into how long the US forces could be degraded.
Points out that the national security costs of Trump's errors in managing the crisis are significant and will have long-term consequences.
Mentions that other nations are trying to exploit the weakness displayed by the US administration in handling the crisis.
Concludes by stating that the concern is not about medical professionals sharing information but about disrupting the research process to make the US more vulnerable.

Actions:

for policy makers, diplomats, activists,
Monitor and safeguard medical research facilities (suggested)
Advocate for improved crisis management strategies (suggested)
</details>
<details>
<summary>
2020-07-25: Let's talk about 8 minutes in Florida.... (<a href="https://youtube.com/watch?v=C4Y_0NYDCjk">watch</a> || <a href="/videos/2020/07/25/Lets_talk_about_8_minutes_in_Florida">transcript &amp; editable summary</a>)

Florida faces a severe COVID-19 crisis with overwhelmed hospitals, lacking leadership, and a need for honest messaging to save lives.

</summary>

"It's that bad. It's not a manufactured narrative."
"We're losing a person on average every eight minutes and 20 seconds."
"The best leadership we have is not coming from state government. It's coming from Walmart."
"Governor DeSantis' little speech was probably premature."
"It's not a media fiction. It's not a manufactured narrative. It is that bad."

### AI summary (High error rate! Edit errors on video page)

Explains the challenges of governing Florida, a state with a diverse population and significant distrust of government.
Describes the severity of the COVID-19 situation in Florida, with hospitals overwhelmed and rising cases.
Notes the lack of effective leadership and messaging in handling the crisis, particularly in South Florida.
Criticizes Governor DeSantis for his ineffective leadership and alignment with Trump, pointing out his absence during the crisis.
Emphasizes the importance of honest messaging, acknowledging the severity of the situation, and promoting safety measures like wearing masks and staying at home.
Praises county school districts for prioritizing virtual learning over returning to normalcy.
Urges for a change in messaging and leadership to save lives and prevent further escalation of the crisis in Florida.

Actions:

for floridians, public health advocates,
Support county school districts opting for virtual learning (exemplified)
Wear masks, stay at home, practice good hygiene (implied)
Advocate for honest messaging and leadership to address the crisis (exemplified)
</details>
<details>
<summary>
2020-07-24: Let's talk about what a climate study can teach us about the media.... (<a href="https://youtube.com/watch?v=RouTiIyBsI0">watch</a> || <a href="/videos/2020/07/24/Lets_talk_about_what_a_climate_study_can_teach_us_about_the_media">transcript &amp; editable summary</a>)

Beau sheds light on media manipulation in reporting climate studies and urges personal verification of critical information to maintain focus on climate policy goals.

</summary>

"They can paint a picture that is factually accurate, but it's not true."
"If it's something you care about, if it's something that's important to you, make sure you go look at it."
"At the end of the day, this is not great news, but it's also not going to change climate policy, really."

### AI summary (High error rate! Edit errors on video page)

Explains how a climate study can reveal media manipulation or reinforcement of narratives.
Notes the confusion surrounding a new climate study due to media reporting.
Describes the updated findings of the study, indicating a more precise temperature range of 2.6 to 3.9 degrees Celsius.
Points out how news outlets can spin the study to fit different narratives, either catastrophic or downplaying the impact.
Emphasizes that the 2.6 to 3.9 degrees Celsius range is the most likely scenario at a 66% chance.
Acknowledges that the outcomes are based on the assumption that action is taken to combat climate change.
Mentions the potential consequences of a three-degree temperature increase, such as environmental disasters.
Advises the audience to verify information themselves rather than solely relying on media interpretations.
States that the study results, while not optimistic, should not alter climate policy goals of staying below 2 degrees Celsius.
Encourages maintaining the original climate policy goal despite its seeming unlikelihood.

Actions:

for climate activists, media consumers,
Verify information yourself (exemplified)
</details>
<details>
<summary>
2020-07-24: Let's talk about Trump's convention, a quote, and context.... (<a href="https://youtube.com/watch?v=qQCJ16XXG6M">watch</a> || <a href="/videos/2020/07/24/Lets_talk_about_Trump_s_convention_a_quote_and_context">transcript &amp; editable summary</a>)

Trump's convention went virtual, while guidelines push for school reopenings with unrealistic expectations, prompting Beau to call for innovation in the education system.

</summary>

"This is going to be really hard."
"It's time to innovate."
"We need something new."

### AI summary (High error rate! Edit errors on video page)

Trump's convention in Jacksonville was moved virtually due to crowd limitations.
Simultaneously, the administration pushes for schools to reopen, focusing on a quote about opening schools.
The quote implies the critical importance of opening schools but overlooks the associated guidelines and resources.
Guidelines suggest measures like regular hand washing, wearing masks, and disinfecting classrooms and buses.
Schools are expected to implement social distancing, provide supplies, and broadcast health announcements.
The guidelines propose unrealistic expectations, such as maintaining distance in classrooms and buses.
Students are required to bring their own food and water, contradicting the issue of student poverty.
Beau questions the feasibility of the guidelines and expresses concerns about the lack of leadership in ensuring safety.
He criticizes the reliance on schools for addressing poverty and stresses the need for innovation in the education system.
Beau calls for a new approach to education that does not burden teachers and administrators with unrealistic demands.

Actions:

for educators, parents, activists,
Donate supplies like soap, hand sanitizer, and disinfectant wipes to schools (implied)
Advocate for adequate funding for schools and support teachers (implied)
Collaborate with local communities to address educational challenges (implied)
</details>
<details>
<summary>
2020-07-23: Let's talk about what it looks like when they don't read the manual.... (<a href="https://youtube.com/watch?v=wMSVFSCj7Zo">watch</a> || <a href="/videos/2020/07/23/Lets_talk_about_what_it_looks_like_when_they_don_t_read_the_manual">transcript &amp; editable summary</a>)

Beau explains the progression of movements from discontent to intensity, detailing the split into five elements and the roles within, offering insights into global events.

</summary>

"It's almost never intended on the part of the establishment, on the part of the government. They don't mean to do it, but they do."
"A lot of times it's kind of imperceptible to the people inside of it, until it's not."
"Those who become soldiers, these are typically people without families. These are typically people who maybe they have a record."

### AI summary (High error rate! Edit errors on video page)

Explains the transition from peaceful protests to more intense movements in the streets.
Describes the typical progression where an overreaction by authorities provokes a reaction.
Talks about how individuals join movements not just for grievances but also moral reasons.
Outlines the cycle of escalation and unintended incidents leading to splits within the movement.
Details the five elements that movements typically split into: leadership, soldiers, underground, auxiliary, and mass base.
Defines each element with examples like soldiers being those who pick up arms.
Mentions differences in terminology in older manuals where auxiliary and mass base were combined as sympathizers.
Emphasizes the importance of understanding these dynamics for insight into global events and transitions.
Gives a hypothetical example of how different groups like Green Berets or activists fit into these elements.
Concludes with the idea that this knowledge is valuable for understanding events both domestically and internationally.

Actions:

for activists, organizers, researchers,
Analyze and understand the dynamics of movements and protests (suggested)
Foster understanding of global events and transitions (suggested)
</details>
<details>
<summary>
2020-07-23: Let's talk about the most important part of the manual for Trump.... (<a href="https://youtube.com/watch?v=7qLp88n6s5k">watch</a> || <a href="/videos/2020/07/23/Lets_talk_about_the_most_important_part_of_the_manual_for_Trump">transcript &amp; editable summary</a>)

Be ready to control the narrative and stop a potential war by leveraging social media influence to prevent escalation.

</summary>

"If you have the means, you have the responsibility."
"Lead yourself. Force multiplication."
"We have to stop a war."
"Just be ready to control the narrative."
"No rational person wants this."

### AI summary (High error rate! Edit errors on video page)

Overview of discussing manuals and strategies for managing intense situations.
Critiques the Trump administration's strategy based on their own manuals.
Mention of the likelihood of escalation into a more intense movement.
Refusal to provide a how-to guide for campaigns that may incite violence.
Encourages viewers to understand the theory and take responsibility.
Emphasizes the nature of escalated situations as PR campaigns with violence.
Calls for a proactive PR campaign to control the narrative before escalation.
Breaks his usual rule of not calling for action due to the importance of the message.
Utilizes recurring themes from his channel to urge viewers to take action.
Shares the potential reach and impact of spreading the message through social media.

Actions:

for online activists and social media users.,
Share the message through social media to control the narrative (implied).
Explain the situation and question motives on social media (implied).
Encourage others to understand and prevent escalation (implied).
</details>
<details>
<summary>
2020-07-23: Let's talk about the US, third countries, and thanking Canada.... (<a href="https://youtube.com/watch?v=CVSVTQMD_KY">watch</a> || <a href="/videos/2020/07/23/Lets_talk_about_the_US_third_countries_and_thanking_Canada">transcript &amp; editable summary</a>)

Beau explains the implications of the overturned Safe Third Country Agreement between the US and Canada, urging for policy reform to honor international agreements and ensure asylum seekers' rights.

</summary>

"The US is not a safe third country anymore."
"It's not a detriment. It's not something that's going to throw open the borders of Canada or anything like that."
"This will save lives."

### AI summary (High error rate! Edit errors on video page)

Explains the Safe Third Country Agreement between the US and Canada, governing immigration policies, particularly for asylum seekers.
Notes that if someone arrived in the US first, they couldn't claim asylum in Canada at a traditional crossing.
Mentions a dangerous loophole where individuals could cross into Canada through unmanned areas.
Points out the danger of such crossings, especially for those traveling with children.
Canadian court declared the agreement unconstitutional due to the US not being a safe third country anymore.
Emphasizes that the US has been ignoring treaties and breaking its own laws related to asylum seekers.
States that issues with asylum seekers predate the Trump administration, with Trump exacerbating the situation.
Indicates the need for reforming policies beyond just returning to the status quo.
Stresses the importance of honoring agreements entered into by the country.
Expresses gratitude towards the Canadian courts for acknowledging the issues and pushing for necessary reforms.
Acknowledges that the fight for asylum seekers' rights is not over despite the court ruling.
Hopes that the Canadian government won't appeal the decision and will recognize the US's failure to adhere to international law.
Appreciates Canada stepping up to potentially save lives through their actions.
Concludes by noting the ongoing work required beyond removing Trump from office.

Actions:

for policy reform advocates,
Advocate for policy reform to ensure the protection of asylum seekers (implied).
</details>
<details>
<summary>
2020-07-22: Let's talk about what would happen if Trump followed the manual.... (<a href="https://youtube.com/watch?v=xTNqT6LcW1o">watch</a> || <a href="/videos/2020/07/22/Lets_talk_about_what_would_happen_if_Trump_followed_the_manual">transcript &amp; editable summary</a>)

Trump administration's failure in Portland stems from ignoring manuals and best practices, requiring a patient long-term commitment and focus on addressing grievances non-violently.

</summary>

"Those who make peaceful revolution impossible make violent revolution inevitable."
"No masks, no shields, no batons, no vans."
"If the people want change, you give it to them."
"It's not glamorous. It's not tough. It's not violent. But it wins."
"This isn't a new theory."

### AI summary (High error rate! Edit errors on video page)

Trump administration is ignoring manuals and best practices in dealing with Portland, leading to mistakes and worsening the situation.
Public perception drives Trump's response, but it is based on incorrect assumptions on managing movements like the one in Portland.
Immediate results demanded by the public and politicians without understanding the situation can lead to further mistakes.
Managing the situation in Portland requires patience and a long-term commitment, as advised by manuals.
The goal should be to prevent escalation and violence, allowing grievances to be addressed in a calm manner.
The response of using an iron fist concept always fails and is not effective in managing grievance-driven movements.
More advanced texts beyond basic manuals are available to policymakers, developed by outside contractors and academics like RAND.
Strategies like "hearts and minds" and "pacification" are recommended to secure the area and address grievances.
Development efforts should focus on improving infrastructure, providing healthcare, education, and addressing income inequality.
Implementing measures to improve living standards and address grievances is key to calming situations like the one in Portland.

Actions:

for policy makers, community leaders,
Address grievances calmly and non-violently (exemplified)
Focus on improving infrastructure and addressing income inequality (exemplified)
Provide healthcare, education, and relief to the community (exemplified)
</details>
<details>
<summary>
2020-07-22: Let's talk about Trump turning on Republican Governors.... (<a href="https://youtube.com/watch?v=1q1q2KoRfw8">watch</a> || <a href="/videos/2020/07/22/Lets_talk_about_Trump_turning_on_Republican_Governors">transcript &amp; editable summary</a>)

President Trump is turning on his Republican allies, distancing himself publicly, leaving those who followed his lead stuck and at risk of being sold out.

</summary>

"If you cannot read the writing on the wall, the president is selling these senators, governors, and representatives out very publicly."
"None of this should be a shock."
"He's going to sell you out. He's already started."

### AI summary (High error rate! Edit errors on video page)

President Trump is turning on Republican governors, senators, and representatives who supported him.
Trump is openly and publicly distancing himself from those who backed him up.
Governors made policy decisions based on Trump's statements; for example, wearing masks.
Trump is now following health experts' advice after months and 140,000 deaths.
Governors who followed Trump's advice are now stuck as Trump distances himself from them.
Trump will likely deny giving mandates or advice to these politicians.
Governor DeSantis had to clarify that his advice on reopening schools was just a recommendation.
Trump has a history of not taking responsibility for his actions.
Trump's behavior with the Portland situation shows he will sell out anyone, including his allies.
Those enabling Trump should be prepared for him to turn on them as well.

Actions:

for politicians, republican allies,
Stand on your own to preserve your seat (implied)
Be prepared for Trump to potentially sell you out (implied)
</details>
<details>
<summary>
2020-07-21: Let's talk about how Trump made Biden the most powerful man in the US.... (<a href="https://youtube.com/watch?v=0jWW-JfInpc">watch</a> || <a href="/videos/2020/07/21/Lets_talk_about_how_Trump_made_Biden_the_most_powerful_man_in_the_US">transcript &amp; editable summary</a>)

Joe Biden's newfound power and Trump's misunderstandings have shifted the political landscape, setting the stage for potential chaos.

</summary>

"Joe Biden is now the most powerful man in the United States."
"This tough guy rhetoric that everybody likes to use, it's getting out of hand."
"Had he read the manual, he [Trump] would understand that the people in the street pretty much always win."

### AI summary (High error rate! Edit errors on video page)

Joe Biden, despite not being Beau's preferred candidate, is currently the most powerful man in the United States.
Trump inadvertently gave Biden this power by not understanding how politics works in the system he wants to implement.
Biden can alter the political landscape with just one speech or tweet, thanks to Trump's actions.
Trump is considering deploying his forces nationwide after their controversial presence in Portland.
The threat of a nationwide deployment comes after a team member suggested shooting those who follow them.
Despite this, there has been no investigation or calls for law and order from the president.
Trump's approach has eroded trust in his administration and its reports.
Biden is now seen as the anti-establishment candidate against Trump's authoritarian tactics.
If Biden were to pardon those arrested by Trump's forces, it could render them ineffective and boost Biden's support.
Biden has the power to incite chaos with a single tweet, contrary to Trump's fearmongering narrative.

Actions:

for voters, activists, political observers,
Organize peaceful demonstrations to show opposition to authoritarian tactics (implied)
Support transparency and accountability within law enforcement agencies (implied)
Stay informed and engaged with political developments to understand power dynamics better (implied)
</details>
<details>
<summary>
2020-07-20: Let's talk about what my wife can teach the Gov of Missouri.... (<a href="https://youtube.com/watch?v=ZwZBQyAl9iY">watch</a> || <a href="/videos/2020/07/20/Lets_talk_about_what_my_wife_can_teach_the_Gov_of_Missouri">transcript &amp; editable summary</a>)

Beau illustrates the stark contrast between his wife's cautious approach to COVID-19 and Governor Parson's reckless disregard for public health, putting political allegiances over people.

</summary>

"He chose to protect Trump's fragile little ego."
"These governors are selling out their duty, their citizens, and their country."
"This man is not capable of running a house with a nurse in it, let alone a state."

### AI summary (High error rate! Edit errors on video page)

Beau contrasts the actions of his wife, a nurse, with the governor of Missouri, Mike Parson, in handling the spread of COVID-19.
His wife follows a strict routine of disinfecting and isolating herself after work to prevent spreading the virus at home.
Beau's wife sends a text before coming home so he can prepare their children to avoid immediate contact with her.
She changes clothes, showers, and disinfects all personal items upon arrival to ensure safety.
Beau criticizes Governor Parson for downplaying the risks of COVID-19 spread in schools and homes.
He accuses Parson of prioritizing Trump's image over public health by pushing for a premature return to normalcy.
Beau questions how many children the governor is willing to sacrifice to protect Trump's ego.
He warns against dangerous messaging that misleads people into thinking the pandemic is over.
Beau links Florida's spike in cases to its governor's premature celebration and lack of protective measures.
He condemns leaders who prioritize political allegiance over the well-being of their citizens and country.

Actions:

for missouri residents,
Follow strict disinfection and isolation protocols after potentially exposing yourself to COVID-19 (implied)
Advocate for evidence-based public health measures in schools and communities (implied)
</details>
<details>
<summary>
2020-07-20: Let's talk about how Trump might win.... (<a href="https://youtube.com/watch?v=RKUVcAo3Cvw">watch</a> || <a href="/videos/2020/07/20/Lets_talk_about_how_Trump_might_win">transcript &amp; editable summary</a>)

Trump's re-election strategy focuses on powerful nationalism, militarized law enforcement support, and suppressing opposition, resembling characteristics of fascism.

</summary>

"He's literally running a campaign, a referendum on fascism."
"It's no longer a question of it can't happen here or even admitting that it can happen here. It is happening here right now."

### AI summary (High error rate! Edit errors on video page)

Trump's re-election strategy involves powerful displays of American symbols like Rushmore and the flag to make people comfortable and safe by showing control within his loyal circle.
The campaign will heavily focus on supporting the troops, which will now include militarized law enforcement, as part of a law and order platform.
National security issues, particularly related to China, will be a recurring theme throughout the campaign.
Trump will intertwine religious imagery and rhetoric, reinforcing traditional family roles and creating a narrative dominated by strong men.
He aims to undercut labor unions, especially teachers' unions, to protect corporate power.
Trump's strategy involves identifying the opposition as the radical left and promising mass arrests and long prison sentences.
He plans to suppress the vote through actions like attacking mail-in voting and shutting down polling places in areas favoring Democrats.
Trump attempts to control the narrative by discrediting any opposition, labeling them as fake news and threatening lawsuits against them.
The campaign strategy shows characteristics of fascism, such as powerful nationalism, disdain for human rights, identification of enemies, supremacy of the military, and rampant sexism.
Corporate power is protected, while labor power is suppressed, with an overall focus on control, corruption, and fraudulent elections.

Actions:

for voters, activists,
Contact local election officials to ensure fair access to voting (suggested)
Join or support organizations advocating for voter rights and access (exemplified)
Organize community efforts to increase voter turnout in areas facing suppression tactics (implied)
</details>
<details>
<summary>
2020-07-19: Let's talk about Morgan Bullock and cultural exchange.... (<a href="https://youtube.com/watch?v=YkiO-WI5htY">watch</a> || <a href="/videos/2020/07/19/Lets_talk_about_Morgan_Bullock_and_cultural_exchange">transcript &amp; editable summary</a>)

Beau introduces cultural appreciation, appropriation, and exchange through Morgan Bullock's story, challenging accusations of cultural appropriation based on skin color and advocating for more understanding between cultures.

</summary>

"The world would be better off if there were more people like Morgan Bullock who took the time to understand another culture."
"All it takes is a little bit of understanding."

### AI summary (High error rate! Edit errors on video page)

Introducing the topic of cultural appreciation, appropriation, and exchange through the story of Morgan Bullock, an Irish dancer.
Morgan Bullock caused a stir by performing traditional Irish steps to modern music on TikTok, sparking accusations of cultural appropriation.
Cultural appropriation involves taking from another culture without understanding or respect, tokenizing it for personal gain, which is not what Morgan did.
Morgan spent half her life studying Irish dance, implying a deep understanding and respect for the culture.
Cultural appreciation involves understanding, respecting, and participating in a culture, which Morgan demonstrated through her dance.
Cultural exchange is about transforming something from a culture in a non-mocking, respectful way while still understanding its origins.
Beau argues that cultural exchange is vital for civilization to progress, citing the example of Arabic numerals in the United States.
People accused Morgan of cultural appropriation because she is black, not because of genuine concerns from the Irish community.
Beau points out that the Irish Embassy supported Morgan, showing that the issue was predominantly with white Americans looking to play the victim.
Beau advocates for more understanding between cultures, referencing the positive impact of collaborations between black and Irish dancers in the past.

Actions:

for cultural enthusiasts,
Support and celebrate individuals like Morgan Bullock who genuinely appreciate and understand different cultures (exemplified)
</details>
<details>
<summary>
2020-07-18: Let's talk about Portland again and manuals.... (<a href="https://youtube.com/watch?v=onmsFzEtK7U">watch</a> || <a href="/videos/2020/07/18/Lets_talk_about_Portland_again_and_manuals">transcript &amp; editable summary</a>)

Beau speculates on Portland protests, urges federal government to follow their manual to avoid escalation, and stresses the importance of listening to protesters' demands.

</summary>

"For every person they snatched, it would bring 10 more to the cause."
"You don't give them a security clamp down in an area where a movement has popular support."
"The only thing that occurred that wasn't in the manual was the young woman and the mask and only the mask that decided to face down law enforcement last night."

### AI summary (High error rate! Edit errors on video page)

Speculated on the response to federal government actions in Portland, predicting strengthening of protests.
Mentioned that crowd size in Portland doubled in response to federal government actions.
Referenced a military manual, FM3-24, which covers provoking responses in chapter four, paragraph 43-ish.
Emphasized the wealth of resources available in military manuals that can be accessed for free.
Encouraged everyone to tap into these resources, even if they think they may not be interested.
Stressed the importance of the federal government reading and following their own manual to avoid escalating tensions.
Pointed out that mishandling the situation could lead to things spiraling out of control quickly.
Suggested that the only options left are to either wait for the situation to burn out or to listen to the protesters' demands.
Urged the federal government to avoid continuing on the current path to prevent a potentially disastrous outcome.
Mentioned an incident with a young woman facing law enforcement, which was not in the manual.

Actions:

for community members, protesters.,
Download military manuals from archive.org or fas.org (suggested).
Utilize resources like Project Gutenberg and LibriVox for free access to valuable information (suggested).
</details>
<details>
<summary>
2020-07-17: Let's talk about what Trump and Republicans know but won't say in public.... (<a href="https://youtube.com/watch?v=14aO7DKUlws">watch</a> || <a href="/videos/2020/07/17/Lets_talk_about_what_Trump_and_Republicans_know_but_won_t_say_in_public">transcript &amp; editable summary</a>)

The Republican National Convention in Florida drastically reduces attendance while maintaining misleading messages on COVID-19 risks.

</summary>

"Nobody wants to be a tough guy until it's time to go through the door."
"They're banking on it not getting out of hand until after the election."
"He has to know how stupid he looks, moving the entire convention to satisfy his ego."

### AI summary (High error rate! Edit errors on video page)

The Republican National Convention in Florida will have 2,500 attendees, with a cap of 7,000 for guests.
Originally scheduled in North Carolina with an anticipated 50,000 attendees, now reduced to 2,500 attendees.
Republicans moved the convention due to attendance concerns but have drastically reduced numbers themselves.
Despite taking precautions for their own safety, Republicans continue to push for normalcy and downplay risks to their supporters.
Beau notes the indifference to human suffering and the disconnect between actions taken and messages being conveyed.
Republicans seem more focused on post-election outcomes rather than the current health crisis.
Trump's ego played a significant role in decisions around convention attendance and location.
The convention is portrayed as more of a party or show rather than a serious decision-making event.
Beau questions the disregard for science in the face of holding a large-scale event during a pandemic.

Actions:

for concerned citizens,
Hold accountable political leaders who prioritize ego over public health (exemplified)
Advocate for science-based decision-making in public events (exemplified)
</details>
<details>
<summary>
2020-07-17: Let's talk about Portland and Trump's move.... (<a href="https://youtube.com/watch?v=EDEv3Ey-Tfg">watch</a> || <a href="/videos/2020/07/17/Lets_talk_about_Portland_and_Trump_s_move">transcript &amp; editable summary</a>)

Trump's actions in Portland suggest he's supporting the movement, but his administration's tactics are backfiring, provoking sympathy and growth for the cause.

</summary>

"Every one of you they snatch brings ten more to your cause."
"Trump has done more to grow the movement in Portland than any activist has."
"Security clampdowns, these kind of measures, backfire always."
"It appears to be just a method of harassing the movement in the streets."
"Government overreactions can solidify movements."

### AI summary (High error rate! Edit errors on video page)

Trump's recent actions suggest he has joined the movement in Portland by actively helping recruit for it.
The administration's handling of the situation in Portland goes against established tactics for dealing with movements.
The use of unmarked vehicles for snatch and grabs in Portland is seen as a form of harassment by the Trump administration.
The arrests and detainments appear to be aimed at intimidating the movement in the streets rather than for legitimate reasons.
Security clampdowns like this are strategically flawed and tend to backfire by generating fear and sympathy for the movement.
Historical examples, such as Ireland in 1916, demonstrate how government overreactions can solidify movements and lead to significant changes.
Overreactions to protests can crystallize movements by creating sympathizers out of fear and anger towards the government.
Provoking a government overreaction is a known tactic for some militant groups seeking revolutionary change.
The protesters in Portland have not done anything to warrant the security clampdown they are facing.
Mainstream figures like Dan Rather have pointed out the counterproductive nature of the federal actions in Portland.

Actions:

for activists, protesters, community members,
Connect with local activist groups to understand how best to support and amplify the movement (suggested).
Share information and updates about the situation in Portland on social media to raise awareness (suggested).
Stand in solidarity with those affected by the security clampdown in Portland by attending or organizing peaceful protests (exemplified).
</details>
<details>
<summary>
2020-07-16: Let's talk about the next wave of immigration to the US.... (<a href="https://youtube.com/watch?v=kegksr7tYUA">watch</a> || <a href="/videos/2020/07/16/Lets_talk_about_the_next_wave_of_immigration_to_the_US">transcript &amp; editable summary</a>)

The US is moving towards an immigration boom due to population decline and capitalist interests, signaling a shift from anti-immigrant sentiments to more welcoming policies.

</summary>

"The only aliens you should be afraid of are the ones in spaceships."
"People coming here for a better life are not a problem. They never have been."
"If there's one thing the US enjoys more than hating people, it's money."

### AI summary (High error rate! Edit errors on video page)

The United States is heading towards an immigration boom due to freer immigration policies.
A new study suggests that population decline will occur around 2060 due to factors like education and healthcare.
Major corporations will influence legislation to encourage immigration for the sake of consumption in a capitalist economy.
Anti-immigrant sentiment under Trump might be the last surge before a period of increased immigration.
Immigration is not a problem; it has been used to keep the marginalized divided.
This shift towards more welcoming immigration policies is good news.

Actions:

for advocates, activists, citizens,
Advocate for inclusive immigration policies through community organizing and activism (implied).
Support organizations working towards immigrant rights and empowerment (implied).
</details>
<details>
<summary>
2020-07-15: Let's talk about the Lincoln Project and manipulating Trump.... (<a href="https://youtube.com/watch?v=85zX6thjQBI">watch</a> || <a href="/videos/2020/07/15/Lets_talk_about_the_Lincoln_Project_and_manipulating_Trump">transcript &amp; editable summary</a>)

The Lincoln Project strategically targets Trump's vulnerabilities through untraditional ads to influence his decisions, raising concerns about his susceptibility to manipulation.

</summary>

"The commander in chief is making decisions because somebody made fun of him on Twitter."
"He's a laughingstock and he can be goaded over Twitter."
"He needs to go."
"There's a reason we're losing every foreign policy engagement."
"Regardless of how you feel about him, he's getting manipulated over Twitter."

### AI summary (High error rate! Edit errors on video page)

The Lincoln Project is a group of Republicans aiming to ensure Donald Trump loses to the Democratic candidate.
Their goal is not just for the next election, but to prevent a second term for Trump to save the Republican Party.
If Biden wins, he will have to clean up Trump's messes, which may take time to show effects.
Trump, on the other hand, lacks the capability to handle his own messes efficiently.
The Lincoln Project's ads are not traditional political ads but are aimed at goading Trump into making mistakes.
They understand that Trump is erratic, dangerous, and easily manipulated.
By appealing to Trump's insecurities, the Lincoln Project aims to influence his decisions.
Trump's response to the ads, like distancing himself from Dr. Fauci, shows he can be manipulated easily.
Despite being effective in the past, the effectiveness of the Lincoln Project may diminish as Trump becomes aware of their tactics.
Regardless of political affiliation, it is concerning that the president can be swayed by tweets and lacks the capacity for critical decision-making.

Actions:

for concerned citizens,
Contact Republican representatives urging them to prioritize country over party (implied)
Support efforts to hold elected officials accountable for their actions (implied)
Stay informed and engaged in political developments to understand the impact of manipulation on decision-making (implied)
</details>
<details>
<summary>
2020-07-15: Let's talk about sending your kid back to school if you have doubts.... (<a href="https://youtube.com/watch?v=wQtmhbIl1C8">watch</a> || <a href="/videos/2020/07/15/Lets_talk_about_sending_your_kid_back_to_school_if_you_have_doubts">transcript &amp; editable summary</a>)

Parents facing conflicting information from the government should trust their instincts on their child's safety and not be bullied into risky decisions.

</summary>

"Where there's doubt, there is no doubt."
"Do not let the state, the government, bully you into risking your child."
"It's time for us to lead ourselves."
"Don't let your political party tell you to not believe what you see with your own eyes."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Parents are struggling with the decision to send their kids back to school due to conflicting information from the government.
Trust your instincts: if you doubt your child's safety at school, don't send them.
The government doesn't prioritize your child's well-being; make decisions based on your own judgment.
The politicization of the situation has led to bending reality for political gain, even attempting to control information about school safety.
Don't trust information coming from an administration with a history of falsehoods; doubt is valid in this context.
Economic concerns shouldn't outweigh doubts about your child's safety—find alternative solutions if needed.
Take charge of your decisions; there's a lack of leadership at both federal and state levels.
Stand firm against pressure and make choices based on what you see and believe is right.

Actions:

for parents, caregivers,
Trust your instincts on your child's safety (implied)
If you doubt your child's safety at school, don't send them (suggested)
Find alternative solutions if economic concerns arise (implied)
Take charge of your decisions regarding your child's safety (implied)
</details>
<details>
<summary>
2020-07-14: Let's talk about opening schools, talking points, teachers, and counting.... (<a href="https://youtube.com/watch?v=rEEPvrwR5vE">watch</a> || <a href="/videos/2020/07/14/Lets_talk_about_opening_schools_talking_points_teachers_and_counting">transcript &amp; editable summary</a>)

Beau exposes the illusion of returning to normal through in-person schooling, advocating for safety and alternative teaching methods in the face of COVID-19 risks.

</summary>

"It's what the president is really hoping to achieve here."
"They're going to open up using distance learning, and that's smart."
"Turns it into a prison."
"I have evidence."
"The talking point is gone."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the administration's push to get all students back in person as a magic trick to create an illusion of normalcy and economic improvement.
Large school districts have refused the push for in-person learning and opted for distance learning, showing independent thinking.
The argument that students need socialization is countered by the rigid guidelines turning schools into prison-like environments.
Beau provides evidence from Arizona, where three teachers contracted COVID-19 during summer school, with one teacher succumbing to the virus.
He challenges Governor DeSantis to specify the acceptable number of teachers who can die before reconsidering in-person schooling.
Beau argues that the talking point of returning to normalcy through in-person schooling is debunked, as the risks and inability to meet safety guidelines remain.
He suggests alternative methods like online learning to protect teachers and students from unnecessary risks.

Actions:

for parents, teachers, community,
Advocate for safe learning environments in schools (implied)
Support distance learning options for students and teachers (implied)
Challenge local leaders to prioritize health and safety in educational decisions (implied)
</details>
<details>
<summary>
2020-07-14: Let's talk about Trump being right, kind of.... (<a href="https://youtube.com/watch?v=ePu5YHehKSM">watch</a> || <a href="/videos/2020/07/14/Lets_talk_about_Trump_being_right_kind_of">transcript &amp; editable summary</a>)

Trump's statement on the Confederate flag reveals how white privilege allows some to overlook its painful history.

</summary>

"The ability to look at the Confederate flag and picture dances, picture a belle of the South, and not picture the abject horrors of slavery, that is white privilege."
"White privilege doesn't mean that your life is easy simply because you're white. It means that it's not harder because you're white."
"Viewing the Confederate flag without acknowledging its dark history showcases a form of privilege rooted in skin color."

### AI summary (High error rate! Edit errors on video page)

Trump made a statement regarding the Confederate flag in the South, calling it a free speech issue.
Some individuals associate the Confederate flag with a romanticized image of the pre-war South, not with its history of oppression.
Many people fail to connect the Confederate flag with its association to slavery, civil rights opposition, and systemic racism.
The ability to ignore the history and pain associated with the Confederate flag is considered a form of white privilege.
White privilege doesn't imply an easy life for white individuals, but rather the absence of additional challenges due to their skin color.
Viewing the Confederate flag without acknowledging its dark history showcases a form of privilege rooted in skin color.
White privilege doesn't negate personal struggles or hardships but indicates that those struggles aren't a result of being white.
The privilege to romanticize the Confederate flag without considering its painful history is a form of privilege that some may find hard to grasp.

Actions:

for community members,
Challenge misconceptions about symbols with historical ties (implied).
Educate others on the history and implications of symbols (implied).
</details>
<details>
<summary>
2020-07-13: Let's talk about Trump supporters and the Constitution.... (<a href="https://youtube.com/watch?v=qhZZT0Q1g7M">watch</a> || <a href="/videos/2020/07/13/Lets_talk_about_Trump_supporters_and_the_Constitution">transcript &amp; editable summary</a>)

Questioning if political parties truly uphold the Constitution by examining actions over symbols, Beau challenges individuals to prioritize responsibilities over rights for societal progress.

</summary>

"You have a constitutional duty to say that black lives matter."
"Stop being so concerned with your rights and be concerned about your responsibilities because you're letting us all down."
"You're failing."

### AI summary (High error rate! Edit errors on video page)

Questioning which party in the United States truly supports the Constitution by examining if they encourage supporters to live up to its ideas.
Testing politicians who claim to be defenders of the Constitution on whether they support and defend its contents, not just its symbol.
Exploring different aspects of the Constitution like establishing justice, ensuring domestic tranquility, promoting general welfare, and securing liberty.
Criticizing the response of sending in troops to maintain domestic tranquility instead of addressing underlying issues.
Emphasizing the importance of promoting general welfare by opposing systemic issues that hinder prosperity.
Calling out the Republican Party for valuing the symbol of the Constitution more than its principles.
Asserting that it is a constitutional duty to address systemic racism, support fellow citizens in crises, and advocate for equality.
Urging individuals to focus on their responsibilities rather than just their rights for the betterment of society.

Actions:

for american voters,
Join movements advocating for systemic change (implied)
Support marginalized communities (implied)
Prioritize community welfare over personal rights (implied)
</details>
<details>
<summary>
2020-07-13: Let's talk about Fauci, Trump, and a wall.... (<a href="https://youtube.com/watch?v=XmfuZ4q7zCU">watch</a> || <a href="/videos/2020/07/13/Lets_talk_about_Fauci_Trump_and_a_wall">transcript &amp; editable summary</a>)

Private citizens' wall collapses, Trump distances himself, Fauci discredited, economy at risk—Beau urges action in crisis.

</summary>

"His decisions, well, they're not his fault. They'll always be somebody else's fault."
"Our obligation to humanity does not end at the border."
"Stay at home as much as you can. Wash your hands. Don't touch your face."
"When he gets bad news, he finds some way to distance himself from it, to reject reality and substitute his own."
"We're all in this together."

### AI summary (High error rate! Edit errors on video page)

Private citizens raised money to build a wall on the border, but the wall is now unstable and may collapse into the river soon.
Trump's reaction to negative news is to distance himself from it and reject reality.
He mentions Trump's pattern of not taking responsibility, blaming others, and politicizing situations.
Beau contrasts how a smart leader might respond to failure with Trump's actual response.
The White House is discrediting Dr. Fauci, who has consistently based his opinions on the best available evidence.
Beau criticizes Trump for prioritizing politics over dealing with the current crisis effectively.
He points out the economic consequences of Trump's actions and the impact on other nations.
Beau stresses the importance of acting responsibly during the ongoing crisis and not relying on guidance from authorities.
He urges people to stay at home, wash hands, wear masks, and take necessary precautions to prevent the further spread of the virus.

Actions:

for american citizens,
Stay at home as much as you can. Wash your hands. Don't touch your face. (suggested)
Wear a mask if you have to go out. (suggested)
</details>
<details>
<summary>
2020-07-12: Let's talk about using your voice, being too close, and Tanisha Pughsley.... (<a href="https://youtube.com/watch?v=hc4uzTduxL4">watch</a> || <a href="/videos/2020/07/12/Lets_talk_about_using_your_voice_being_too_close_and_Tanisha_Pughsley">transcript &amp; editable summary</a>)

Beau stresses the importance of using your voice, seeking help, and dispelling myths around domestic violence, inspired by the tragic story of Tanisha Pugsley.

</summary>

"Use your voice. Call for backup."
"You don't have to go through this alone."
"If you knew somebody going through this, you'd want to help, right?"

### AI summary (High error rate! Edit errors on video page)

Importance of the video for everyone, especially women.
Impact of the topic on women when it spirals out of control.
Focus on using your voice, being cautious, and dispelling myths.
Learning from Tanisha Pugsley, a detective in Montgomery, Alabama.
Description of Tanisha as a strong, law-abiding woman.
The limitations of protective orders and their role in cases.
Tragic outcome involving Tanisha's ex-boyfriend charged in her death.
Challenging stereotypes of weak, dependent women in such situations.
Emphasizing the need to recognize danger even when too close.
Urging the use of voice, seeking help, and calling for backup in risky situations.
Mention of shelters and resources available for individuals in need.
Providing the national hotline number for assistance.
Encouragement to reach out for help and not face such situations alone.

Actions:

for women, allies,
Call the national hotline for assistance (exemplified)
Reach out to shelters for help (implied)
Encourage others to seek help and not go through such situations alone (exemplified)
</details>
<details>
<summary>
2020-07-12: Let's talk about Republicans in 2020.... (<a href="https://youtube.com/watch?v=OJHcM8XMK-g">watch</a> || <a href="/videos/2020/07/12/Lets_talk_about_Republicans_in_2020">transcript &amp; editable summary</a>)

Republicans face a challenging shift from 2016, urging senators to speak out against Trump to preserve their seats and party principles.

</summary>

"Unique does not always mean useful."
"Their only option is to grow a spine real fast."
"He has the Republican Party attacking wounded war vets and defending it."
"Your most vocal supporters are not really representative of all of the people in the Republican Party."
"Republican senators have to decide whether or not they want to stay on that boat or not."

### AI summary (High error rate! Edit errors on video page)

Republicans facing challenges in the 2020 election due to a significant shift from 2016.
Republican pollsters realizing the need to spend money to secure seats even in traditionally red states.
Voters who previously supported Trump now considering voting Democrat for the first time.
Analysis of the uniqueness of the 2016 election compared to the upcoming one.
Republicans urged to speak out against Trump and return to the core principles of the party.
Concerns within the Republican Party about losing control over the House, Senate, and White House.
The necessity for Republican senators to show courage and stand up against Trump to maintain their seats.
Criticism of Trump's actions and their impact on the Republican Party's image.
Speculation on the fate of Republican senators based on their response to Trump's leadership.
Beau's curiosity about which Republican senators will demonstrate integrity and stand up against Trump.

Actions:

for republican voters,
Speak out against Trump's actions openly and oppose him on critical issues (implied)
Return to the core principles of the Republican Party by standing up for integrity (implied)
</details>
<details>
<summary>
2020-07-11: Let's talk about music, a theme for 2020, and them hearing us now.... (<a href="https://youtube.com/watch?v=NiTf93deP9I">watch</a> || <a href="/videos/2020/07/11/Lets_talk_about_music_a_theme_for_2020_and_them_hearing_us_now">transcript &amp; editable summary</a>)

Beau talks about music's power to capture history, introducing a song that resonates with the theme of standing up against injustice and corruption in 2020.

</summary>

"I bet you can hear us now."
"Back to how it was before, we're gonna stand our ground."
"A system corrupted by lies."
"Blatant aggression, it seems their obsession."
"We ain't gonna take any more."

### AI summary (High error rate! Edit errors on video page)

Music captures moments in history better than anything, often not heard until years later because songwriters write passionately about their surroundings, not for an audience.
Songs connect us to specific periods in time, playing out the idea of capturing a moment in every movie about the past.
Introduces a song by Tom Burton, "I Bet You Can Hear Us Now," as a fitting theme for 2020.
The lyrics of the song address standing up against injustice, corruption, and violence by those in power, particularly targeting law enforcement.
Talks about the systemic issues, oppression, and violence faced by communities, calling for change and standing ground against it.

Actions:

for music enthusiasts and advocates for social justice.,
Join protests against injustice and corruption (implied).
Support community-led initiatives for change (implied).
</details>
<details>
<summary>
2020-07-11: Let's talk about Trump vs the Pediatricians.... (<a href="https://youtube.com/watch?v=wL6gij70OKc">watch</a> || <a href="/videos/2020/07/11/Lets_talk_about_Trump_vs_the_Pediatricians">transcript &amp; editable summary</a>)

Viva Litzbow challenges the Trump administration's stance on school reopening, advocating for evidence-based decisions led by health experts and educators over political influence.

</summary>

"Science should drive decision-making on safely reopening schools."
"Health experts do [decide when schools open], not the president."
"The presidency has too much power. The Trump administration has shown this completely."
"Eventually there will be another Trump."
"The president attempting to control everything is going to make things worse."

### AI summary (High error rate! Edit errors on video page)

Trump administration insisted on schools reopening in the fall, threatening funding withdrawal if not in person.
American Academy of Pediatricians' guidance emphasized the importance of safe reopening with CDC-aligned recommendations.
Trump disagreed with CDC guidelines for schools, planning to pressure experts to change their guidance.
American Academy of Pediatricians issued a statement co-signed by other organizations, prioritizing safe reopening based on science, not politics.
They emphasized the need for evidence-based decision-making and leaving school reopening decisions to health experts and educators.
The statement rebuked the President's stance and called for federal resources to ensure safe education.
Withholding funding from schools not opening full-time in person was criticized as putting students and teachers at risk.
The President's involvement in determining school reopening was deemed inappropriate; health experts should lead decision-making.
The presidency's excessive power and the need to limit it, especially in light of the current administration's actions, were emphasized.
The push to reopen schools was labeled as political, lacking a balanced nationwide approach to reopening based on varying threats.
Adaptability and trust in health experts and educators were prioritized over the President's directives.

Actions:

for education advocates, parents, policymakers,
Advocate for evidence-based decision-making in school reopening plans and prioritize safety for students, teachers, and staff (implied)
Support federal funding initiatives to ensure safe education during the reopening process (implied)
Advocate for limiting presidential power to prevent political influences in critical decision-making processes (implied)
</details>
<details>
<summary>
2020-07-10: Let's talk about how you can create change without being in the street.... (<a href="https://youtube.com/watch?v=UDfp_nRqaxU">watch</a> || <a href="/videos/2020/07/10/Lets_talk_about_how_you_can_create_change_without_being_in_the_street">transcript &amp; editable summary</a>)

Beau explains how individuals can contribute to deep systemic change by shifting societal thought and pushing unacceptable ideas out of the Overton Window, even without physically participating in protests.

</summary>

"The legislation is just a tool to help [change societal thought]."
"You don't need combat boots and a mask to be involved in this campaign."
"Gotta push it all the way out. Gotta get it out of the window completely."
"Make it unacceptable in every way. Every form of covert racism."

### AI summary (High error rate! Edit errors on video page)

Explains how individuals can create deep systemic change within the United States even if they can't physically participate in street protests.
Emphasizes the importance of diversity of tactics, strategy, and shifting the Overton Window to achieve the grand goal of ending unacceptable things.
Points out that disrupting business through street protests grabs the attention of the government, which is closely tied to business interests.
Acknowledges that while small concessions from the government are helpful, the ultimate goal is changing societal thought.
Defines the Overton Window as the acceptable range of political thought and stresses the need to shift it to make certain ideas unacceptable.
Encourages individuals to use their influence on social media and in everyday interactions to change thought and challenge covert racism.
Urges people who can't physically participate in protests to still play a vital role in changing societal perceptions and pushing unacceptable ideas out of the Overton Window.
Suggests using hobbies, such as creating art or memes, to contribute to shifting the Overton Window and promoting social change.
Stresses that any action taken to shift societal thought is valuable, regardless of the form it takes.
Advocates for engagement in letter writing, signing petitions, contacting legislators, and supporting bail funds as ways to support street actions and create change.

Actions:

for activists, allies, community members,
Use your social media accounts to influence thought by discussing privilege and the need for a police database (implied).
Call out covert racism and challenge unacceptable ideas in everyday interactions (implied).
Support bail funds and other organizations involved in street actions (implied).
Write letters to the editor, sign petitions, contact legislators, and vote with your dollars to support social change (implied).
</details>
<details>
<summary>
2020-07-10: Let's talk about Trump, tax exempt status, and schools.... (<a href="https://youtube.com/watch?v=jCrgv8MVpDg">watch</a> || <a href="/videos/2020/07/10/Lets_talk_about_Trump_tax_exempt_status_and_schools">transcript &amp; editable summary</a>)

President Trump's desire to revoke universities' tax-exempt status sparks a call for auditing all institutions, including right-wing Christian schools, to ensure educational integrity and financial transparency.

</summary>

"Our children must be educated not indoctrinated."
"I think opening this door is fantastic."
"I think it's a good idea."
"How Christian schools get away with teaching people to not love their neighbor."
"Y'all have a nice day."

### AI summary (High error rate! Edit errors on video page)

President Trump desires to ensure universities are run correctly, despite his own controversial university's past payment to students.
Trump believes universities teach left-leaning ideas, leading him to suggest revoking their tax-exempt status—a concept not supported by tax code or the constitution.
Beau suggests auditing the tax-exempt status of all universities, including far-right Christian schools involved in political activities.
Beau sees potential in Trump's proposal, as it could uncover inconsistencies in how funds are used by universities.
Trump's tweets call for a reexamination of universities' tax-exempt status if they are deemed to propagate propaganda.
Beau criticizes Trump's lack of understanding and excessive use of capitalized letters and punctuation in his tweets.
Beau acknowledges the importance of reviewing universities' financial practices but points out that student-led organizations may not impact tax exemptions.
Beau supports the idea of scrutinizing educational content in universities, especially in right-wing Christian schools.
Beau questions how Christian schools reconcile teachings that may contradict the principle of loving one's neighbor.
Beau concludes by suggesting that the Trump administration should spend time in a college to better understand the situation.

Actions:

for policy advocates,
Advocate for transparent financial practices in universities (implied)
Support auditing educational content in institutions (implied)
</details>
<details>
<summary>
2020-07-09: Let's talk about Trump, leadership, flexibility, and consistency.... (<a href="https://youtube.com/watch?v=Vjzh5GZ1HP0">watch</a> || <a href="/videos/2020/07/09/Lets_talk_about_Trump_leadership_flexibility_and_consistency">transcript &amp; editable summary</a>)

Beau advises President Trump to lead by example and prioritize the safety of children, teachers, and school staff over political events, urging for consistency and decisiveness in leadership.

</summary>

"Be a leader. Be first through the door."
"No flexibility on this one."
"The entire country is watching."
"It's time for the President to lead."
"You can't have extra precautions."

### AI summary (High error rate! Edit errors on video page)

Shares advice on leadership from a warrant officer: leaders must be willing to take risks and be first through the door to inspire followers.
Recalls the officer's experience in Panama, Grenada, and Vietnam as a real leader.
Criticizes President Trump's approach to leadership, referencing his handling of the Republican National Convention in Jacksonville.
Trump insists on holding the convention despite rising COVID-19 cases in Florida, showing inflexibility.
Trump demands schools to reopen in Florida before August 24th, threatening to pull funding if they don't comply.
Beau urges Trump to lead by example and move the convention earlier without additional safety measures to demonstrate that it's safe.
Emphasizes that Trump's decision will reveal his priorities: politicians or children, teachers, and school staff.
Asserts that Trump needs to show consistency in his leadership, even if it means maintaining a stance of "abject cowardice."
Concludes by calling for decisive action from the President.

Actions:

for politically aware citizens,
Contact local representatives to advocate for safe school reopening plans (implied)
Organize community efforts to support schools with necessary resources for safe reopening (implied)
</details>
<details>
<summary>
2020-07-08: Let's talk about a story out of Mississippi and systemic issues.... (<a href="https://youtube.com/watch?v=ZLGbBYrqpro">watch</a> || <a href="/videos/2020/07/08/Lets_talk_about_a_story_out_of_Mississippi_and_systemic_issues">transcript &amp; editable summary</a>)

Beau sheds light on systemic racism and voter suppression through the comments of an election commissioner in Mississippi, urging for a new mindset towards inclusivity and representation.

</summary>

"If you are differentiating between any group of human beings and people, you're wrong. You're racist, you're sexist, you're a bigot of some sort, I guarantee it."
"We're building a new South. We don't need this. We've rejected this. We're past this. We're getting away from it."
"This shows one of the critical flaws in representative democracy."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of an election commissioner in Mississippi and the importance of sharing stories to understand systemic issues.
Sheds light on a Facebook post by Gail Harrison Welch, an election commissioner, where she differentiated between "the blacks" and "people" regarding voter registration concerns.
Explains how this differentiation perpetuates systemic racism and voter suppression to maintain institutional power.
Raises the issue of gerrymandering and the manipulation of voting districts to silence minority voices.
Criticizes Welch's attempt to explain her comments privately after posting them publicly, suggesting it raises further doubts about her actions behind closed doors.
Questions Welch's statement about trying to motivate people to vote but points out the underlying racial undertones in her initial comments.
Emphasizes the need to reject any form of differentiation between groups of people based on race or ethnicity.
Urges for a new mindset in the South that moves away from racial discrimination and towards inclusivity and representation.
Raises concerns about how Welch's mindset may have influenced her actions during her 20-year tenure as an election commissioner.
Points out the upcoming election where the people of Jones County, Mississippi may need to be informed about Welch's history and attitudes towards voter registration.

Actions:

for mississippi residents,
Inform the people of Jones County, Mississippi about the upcoming election and the history of the election commissioner (suggested).
Advocate for fair voting practices and representation in your community (implied).
</details>
<details>
<summary>
2020-07-08: Let's talk about Florida, beds, nurses, plans, and boxers.... (<a href="https://youtube.com/watch?v=Q-XRACaIlvs">watch</a> || <a href="/videos/2020/07/08/Lets_talk_about_Florida_beds_nurses_plans_and_boxers">transcript &amp; editable summary</a>)

Beau talks about Florida's crisis, urging a change in plan to combat the shortage of ICU beds and nurses, and stresses community action amid government failure.

</summary>

"Every boxer will tell you that when you step into that ring you have a plan until you get punched in the face."
"We are Floridians. We know that after a hurricane the government's pretty much useless."
"We have to lead ourselves."
"Stay at home. Do what you can to stay at home."
"If you have a boss that tells you not to wear a mask or that you can't do it in their store or whatever, send me a message."

### AI summary (High error rate! Edit errors on video page)

Talking about Florida again due to unforeseen events unfolding.
Governor DeSantis plans to reopen schools in August for in-person learning despite concerning numbers.
Florida is facing a critical shortage of ICU beds and nurses.
Despite the crisis, the leadership is reluctant to change their plan.
Beau draws a comparison between facing a crisis and being a boxer getting punched in the face.
Urges for a change in strategy to avoid getting knocked out by the current situation.
Emphasizes the need to listen to medical experts and follow simple guidelines like staying at home and wearing masks.
Criticizes the government's failure to provide effective leadership during the crisis.
Calls for community action and solidarity in the absence of government support.
Stresses the importance of individuals taking responsibility for their actions to combat the situation effectively.

Actions:

for community members,
Stay at home, follow health guidelines (implied)
Wear a mask and take necessary precautions at work (implied)
Support each other by standing up against unsafe practices (implied)
</details>
<details>
<summary>
2020-07-07: Let's talk about Trump helping auto workers with his trade deal.... (<a href="https://youtube.com/watch?v=OpD0xZdGiF0">watch</a> || <a href="/videos/2020/07/07/Lets_talk_about_Trump_helping_auto_workers_with_his_trade_deal">transcript &amp; editable summary</a>)

Beau delves into Trump's USMCA trade deal aiming to boost auto jobs, but results differ from his claims, benefiting non-American workers primarily.

</summary>

"Trump is going to get out on the campaign trail and still talk about this as if it's a great success."
"He says that he helped auto workers, that's a true statement. Just not American auto workers."

### AI summary (High error rate! Edit errors on video page)

Beau dives into Trump's signature trade deal, the USMCA, aiming to bring back auto manufacturing jobs to the US.
The deal requires 75% of car parts to be manufactured in the US, Canada, or Mexico, and 40-45% by workers earning at least $16 an hour.
Trump's plan was to entice Japanese auto manufacturers to move to the US; however, companies are opting to raise wages in Mexico instead.
The trade deal went into effect on July 1st, with companies in Mexico choosing to increase wages rather than relocating.
Despite falling short of Trump's goals, the deal has a silver lining: benefiting workers, raising living standards, and boosting the Mexican economy.
Beau acknowledges that the deal may increase car prices in the US due to higher labor costs but suggests the Mexican government might offer incentives to balance this.
Trump is likely to tout the deal as a success on the campaign trail, but the reality differs from his claims.
While Trump may claim to have helped auto workers, it's primarily benefiting non-American workers under the USMCA.

Actions:

for policy analysts, trade experts,
Support policies that prioritize American workers' interests over foreign workers (implied)
Advocate for trade deals that truly benefit American workers (implied)
</details>
<details>
<summary>
2020-07-07: Let's talk about Florida man deciding to reopen schools.... (<a href="https://youtube.com/watch?v=FdYy3w-Fxvs">watch</a> || <a href="/videos/2020/07/07/Lets_talk_about_Florida_man_deciding_to_reopen_schools">transcript &amp; editable summary</a>)

Florida plans to reopen schools in August without clear data or rationale, posing risks to students and communities, urging individuals to prioritize safety over government mandates.

</summary>

"So we're just gonna go through an average day at a school and compare that to the guidelines."
"There's no data to back up this decision. None."
"We are Florida man. We are Florida woman. We do not need Tallahassee to make the right decision."
"If you look at that data come August and you don't feel that your child would be safe, don't send them, homeschool."
"You are quite literally killing your chances for re-election."

### AI summary (High error rate! Edit errors on video page)

Florida plans to reopen schools in person in August, five days a week, despite the ongoing situation.
The decision to reopen schools seems to be based on unclear reasoning and not on data.
Beau criticizes the lack of understanding and decision-making skills of those in charge.
He questions the safety guidelines provided by the Governor and contrasts them with a typical school day scenario.
The school environment poses challenges for following safety protocols like wearing masks and social distancing.
The decision to reopen schools now, amidst a surge, lacks a logical basis.
Beau advocates for having distance learning options ready as a more critical measure.
He urges Floridians to prioritize safety and make informed decisions regarding sending children to school.
Beau warns the ruling party in Tallahassee about the potential consequences of their decision on re-election.
He concludes by suggesting that the decision-makers may be jeopardizing their chances for re-election by ignoring public safety concerns.

Actions:

for parents, educators, community members,
Monitor local school board decisions and advocate for safer alternatives (implied)
Stay informed about the situation in your area and make decisions based on safety (implied)
</details>
<details>
<summary>
2020-07-06: Let's talk about Senator Tammy Duckworth and Foreign Policy.... (<a href="https://youtube.com/watch?v=PHt5qWGST7c">watch</a> || <a href="/videos/2020/07/06/Lets_talk_about_Senator_Tammy_Duckworth_and_Foreign_Policy">transcript &amp; editable summary</a>)

Lieutenant Colonel Tammy Duckworth could be a strong VP choice for Biden, particularly for foreign policy, given her experience, knowledge, and resilience.

</summary>

"She has the knowledge, she has experience, she understands the way the world works, and she actually cares about the subject."
"She survived an RPG. She's gonna get more respect from the leaders of countries that don't treat women as they should."
"They've chosen well, as far as I can tell."

### AI summary (High error rate! Edit errors on video page)

Lieutenant Colonel Tammy Duckworth is a potential Biden VP nominee generating a lot of interest.
Duckworth's background includes growing up in Southeast Asia and serving in the military, where she chose to fly helicopters.
In Iraq, Duckworth's Black Hawk was hit by an RPG, resulting in her losing both legs and severely injuring one arm.
Despite her injuries, Duckworth stayed in the military and retired as a lieutenant colonel.
Her military record is exemplary, with accolades like the Purple Heart and Meritorious Service Medal.
Duckworth's political positions are in line with expectations for a Biden VP nominee, except for gun control.
Her stance on gun control might not please those on either side of the debate.
Duckworth's expertise in foreign policy could make her a strong choice to clean up Trump's mess in that area.
She has shown a genuine interest in auditing international affairs spending and has the necessary knowledge and experience.
Duckworth's military background could command respect from leaders who may not take women seriously.
While she may not be a diplomat, she has the foundation to excel in foreign policy if that's the role Biden's team is looking to fill.
Beau believes that if the VP pick is intended to strengthen Biden's foreign policy, Duckworth could be a suitable choice.
Ultimately, Beau sees Duckworth as a potentially strong pick if the goal is to fill a foreign policy role in the administration.

Actions:

for political analysts,
Support a VP nominee with a strong focus on foreign policy (implied)
</details>
<details>
<summary>
2020-07-06: Let's talk about Ft. Hood, Vanessa Guillen, and what's next.... (<a href="https://youtube.com/watch?v=V3ppc0Bkxjk">watch</a> || <a href="/videos/2020/07/06/Lets_talk_about_Ft_Hood_Vanessa_Guillen_and_what_s_next">transcript &amp; editable summary</a>)

Beau addresses the need for thorough investigation, accountability, and congressional hearings regarding Specialist Vanessa Guillen's case at Fort Hood, Texas.

</summary>

"We can't let this story fade away because there are a lot of questions."
"The US military for years was moving ahead on this."
"Because there are a lot of things that should have been in place that should have prevented this."
"We need congressional hearings on this."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Addressing the case of Specialist Vanessa Guillen from Fort Hood, Texas.
Acknowledging the efficiency of army investigators and the slow but thorough process.
Mentioning the 300 interviews conducted by investigators.
Emphasizing the need for political pressure to ensure a proper investigation.
Noting that Vanessa Guillen wanted to report harassment but was afraid of retribution.
Pointing out the improvements made by the US military in addressing harassment.
Urging for accountability if policies weren't followed or if commands failed.
Suggesting the Senate Armed Services Committee launch an investigation.
Providing Twitter handles of committee members for public outreach.
Mentioning strong allies like Elizabeth Warren and Tammy Duckworth for support.
Stressing the importance of public support to keep the story alive and demand answers.
Expressing concerns about the lack of security in the armory where the incident occurred.
Calling for a clear picture of what happened beyond the basic details of the crime.
Advocating for congressional hearings to address the larger issues beyond the crime itself.

Actions:

for advocates for justice,
Contact members of the Senate Armed Services Committee (suggested)
Tweet at committee members for support (implied)
Reach out to committee members on social media (implied)
Call committee members for action (implied)
</details>
<details>
<summary>
2020-07-06: Let's talk about Bubba Wallace, Trump, definitions, and deceptions..... (<a href="https://youtube.com/watch?v=ow9ZyHoOV_o">watch</a> || <a href="/videos/2020/07/06/Lets_talk_about_Bubba_Wallace_Trump_definitions_and_deceptions">transcript &amp; editable summary</a>)

President's misunderstanding of a hoax leads to demands for apology, while Beau criticizes Trump's actions as a malicious deception.

</summary>

"The fact that the President of the United States is asking for an apology from anybody is hilarious."
"Before I care about a race car driver apologizing for anything, I'd like to hear an apology from the President of the United States for his hoax."
"The Trump brand is a hoax. It is a malicious deception."

### AI summary (High error rate! Edit errors on video page)

President of the United States demanded a race car driver, Bubba Wallace, apologize for an incident involving his crew misinterpreting something and asking for an investigation.
President called the incident a hoax, displaying a misunderstanding of the term.
Beau points out the definition of a hoax as a humorous or malicious deception, contrasting it with the situation involving Bubba Wallace's crew.
Beau criticizes the President's hiring practices as being a mistake, not a hoax.
He gives examples of humorous deceptions by the President, like claiming to make America great again or supporting the American people.
Beau also provides an example of a malicious deception by the President, such as manipulating COVID-19 testing numbers.
Despite Trump asking for apologies, Beau finds it ironic since Trump rarely accepts responsibility for his actions.
Beau concludes that Trump and the Trump brand are hoaxes perpetuated on the American people for personal gain.
He suggests that before demanding apologies from others, Trump should apologize for his actions during his presidency.
Beau calls for accountability from Trump, which he believes is unlikely to happen.

Actions:

for voters, activists, concerned citizens,
Hold leaders accountable for their actions (implied)
Demand transparency and honesty from public figures (implied)
</details>
<details>
<summary>
2020-07-05: Let's talk about why people are moving left.... (<a href="https://youtube.com/watch?v=2g0qUxgwHmo">watch</a> || <a href="/videos/2020/07/05/Lets_talk_about_why_people_are_moving_left">transcript &amp; editable summary</a>)

Why the surge in black leftists and rejection of capitalism stems from challenging systemic issues of race and class in the US, pushing for egalitarian philosophies and systemic changes.

</summary>

"White privilege denial contributes to pushing black Americans towards leftist ideologies."
"Black leftists are willing to fight for all lives."
"Rejecting capitalism is a common trend among those advocating for environmental protection."

### AI summary (High error rate! Edit errors on video page)

The US political spectrum is skewed to the right, with even Democrats considered center-right globally.
Leftism, defined as rejecting capitalism, is gaining traction in the US.
There is a surge in black leftists, challenging traditional right-wing ideologies.
White privilege denial contributes to pushing black Americans towards leftist ideologies.
Issues of race and class in the US prompt people to seek egalitarian philosophies.
Black leftists are advocating for systemic changes that impact them daily.
The economic system is being challenged by those seeking systemic change.
Black leftists are willing to fight for all lives, leading to a surge in leftists across different groups.
Rejecting capitalism is a common trend among those advocating for environmental protection.
The current form of capitalism in the US fails to lift black Americans out of poverty.

Actions:

for activists, political analysts,
Challenge systemic issues of race and class by advocating for egalitarian philosophies and systemic changes (implied).
Stand up against white privilege denial and contribute to pushing for leftist ideologies (implied).
Advocate for environmental protection and challenge capitalism by rejecting its harmful practices (implied).
</details>
<details>
<summary>
2020-07-04: Let's talk about a special 4th of July message.... (<a href="https://youtube.com/watch?v=cY1xXL33i3I">watch</a> || <a href="/videos/2020/07/04/Lets_talk_about_a_special_4th_of_July_message">transcript &amp; editable summary</a>)

Beau addresses the divisive slogan "All lives matter" and its implications, questioning hidden motives behind its usage in response to "Black Lives Matter" while exposing systemic racism.

</summary>

"If the only time you say all lives matter is as a response to somebody saying that Black Lives Matter, you have to question your own motives."
"All lives deserve justice, if that's true, then you should have no issue with the phrase Black Lives Matter because black is part of all."
"That's not what gets said. When people say it, what they're really doing is trying to turn down the volume on that message."
"The fear of most people who say that is that if black people get equality, well, then white folk, they're gonna have to stand on their own."
"Happy 4th of July, and all countries matter."

### AI summary (High error rate! Edit errors on video page)

Sharing a special 4th of July message regarding Independence Day and the concept of equality in the country.
Addressing the slogan "All lives matter" and its implications when used in response to "Black Lives Matter."
Exploring the hidden motives behind using "All lives matter" to diminish the message of equality for black individuals.
Pointing out the systemic racism embedded in using "All lives matter" to deflect focus from specific injustices faced by black people.
Questioning the true intent behind advocating for justice for all lives while undermining the Black Lives Matter movement.
Emphasizing the fear that equality for black individuals may challenge the privilege and status quo enjoyed by white individuals.
Concluding with a thought on the implications of acknowledging true equality for all individuals in society.

Actions:

for social justice advocates,
Support fundraisers for shelters that aid survivors of domestic violence (suggested)
Challenge systemic racism through education and advocacy efforts (implied)
</details>
<details>
<summary>
2020-07-03: Let's talk about the Black Hills, Rushmore, and Trump.... (<a href="https://youtube.com/watch?v=pHmGYgbTa0o">watch</a> || <a href="/videos/2020/07/03/Lets_talk_about_the_Black_Hills_Rushmore_and_Trump">transcript &amp; editable summary</a>)

Beau stresses the constitutional importance of honoring treaties and returning the Black Hills to the Sioux, respecting their cultural significance and rightful ownership.

</summary>

"It is their land. Period."
"It's their land. They get to decide."
"Constitutionally, it's theirs. We need to honor the treaties."
"Let's give it back."
"Give it back. I don't know why this is a debate."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the cultural and historical significance of the Black Hills and its connection to the Constitution.
Trump's visit to Rushmore has sparked controversy similar to his visit to Tulsa, raising questions about the timing of his appearances.
Beau questions why the Sioux do not have the Black Hills despite the 1868 Fort Laramie Treaty granting it to them.
The U.S. took back the Black Hills from the Sioux due to the discovery of gold, an act Beau deems unconstitutional.
Despite a 1980 Supreme Court ruling that the taking of the land was unconstitutional, the Sioux were offered cash instead of the land's return.
Beau explains that the Black Hills hold deep cultural and spiritual significance for the Sioux, being considered the birthplace of their culture.
Beau stresses that it is ultimately up to the Sioux to decide the fate of their land, not outsiders like himself.
Various proposed solutions to the issue of the Black Hills ownership have been suggested over the years, but Beau asserts that it is solely the Sioux's decision.
Beau advocates for returning the Black Hills to the Sioux, urging respect for treaties, the Constitution, and the rightful owners of the land.
Ending with a call to honor treaties, the Constitution, and give the land back to its rightful owners.

Actions:

for advocates and activists,
Contact indigenous-led organizations to support efforts for the return of sacred lands (exemplified)
Join community initiatives that center indigenous voices and rights (exemplified)
</details>
<details>
<summary>
2020-07-03: Let's talk about Trump, the Founders, and a list you need for July 4th.... (<a href="https://youtube.com/watch?v=Lk7XBcWIZIU">watch</a> || <a href="/videos/2020/07/03/Lets_talk_about_Trump_the_Founders_and_a_list_you_need_for_July_4th">transcript &amp; editable summary</a>)

Beau outlines how the President's actions diverge from the Founding Fathers' beliefs, urging a critical comparison on Independence Day.

</summary>

"Those attempting to alter or abolish a government that has become destructive to the ends of preserving life, liberty, and the pursuit of happiness. Those are the patriots."
"When the streets fill with people, understand those are the people who are the ideological descendants of the Founders."
"Just remember that tomorrow as he tries to paint himself as some patriot. They wouldn't have liked him."
"He is more akin to the king than to those who signed the Declaration of Independence."
"Declaration of Independence says you're wrong. You're supposed to fix it."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the upcoming July 4th, Independence Day, and the President's typical actions on this day.
He questions if the Founding Fathers' beliefs truly match up with the President's.
Beau suggests creating a list of the President's positions to compare them with the Founders.
He points out the President's inaction in response to various issues affecting the country.
Beau mentions the President's anti-immigration stance, obstruction of justice, and desire to control judges.
He criticizes the President's use of law enforcement and his interference in domestic issues.
Beau expresses concern about the President's handling of trade, public health, and military superiority.
He draws parallels between the President's actions and the reasons listed in the Declaration of Independence for altering or abolishing government.
Beau reminds listeners to read the Declaration of Independence and analyze how it relates to current political situations.
He encourages people to understand the true spirit of patriotism and the Founders' intentions.

Actions:

for american citizens,
Read the Declaration of Independence to understand its relevance to current political events (suggested)
Analyze how the President's actions relate to the reasons listed in the Declaration of Independence for altering or abolishing government (suggested)
</details>
<details>
<summary>
2020-07-02: Let's talk about Trump allies pointing the finger at Schiff over Russia.... (<a href="https://youtube.com/watch?v=kYM0V3bIvgo">watch</a> || <a href="/videos/2020/07/02/Lets_talk_about_Trump_allies_pointing_the_finger_at_Schiff_over_Russia">transcript &amp; editable summary</a>)

Beau explains the Trump deflection cycle, clarifies intelligence operations, and exposes manipulation of Trump's base with misinformation.

</summary>

"Schiff knew first because he actually attends his briefings."
"Secrets are secrets not really because of the information in them but how they're gathered."
"This is a story that should not go away."
"Those aren't even options really on the table in a sane world."
"Crafting their talking points around the idea that their voters don't understand the very basics of how this stuff works."

### AI summary (High error rate! Edit errors on video page)

Explains the typical cycle of Trump deflection when he gets in trouble, starting with "I didn't know" and moving to blaming Democrats.
Asserts that Schiff, on the intelligence committee, likely knew about issues before the president, due to attending briefings diligently.
States that Schiff holding hearings immediately could have endangered special operators and intelligence personnel in the field.
Points out that intelligence agencies like the CIA work for the president, not Congress, and Schiff cannot direct them to act.
Notes that blaming Schiff is a deflection tactic to target a disliked figure, as Republicans misunderstand the government's workings.
Emphasizes that Trump's base is being manipulated with misinformation about intelligence operations to deflect blame effectively.
Indicates that rumors of issues date back to the Obama administration, but concrete intelligence came during Trump's term.
Criticizes the lack of action by the Trump administration despite intelligence reports, choosing to believe Putin without any response.
Stresses that the options are not just ignoring the issue or going to war with Russia, dismissing these extreme notions.
Concludes by remarking on the deliberate crafting of talking points to exploit ignorance among Trump's base regarding government operations.

Actions:

for politically informed individuals,
Contact your representatives to demand accountability and transparency in intelligence operations (suggested)
Join organizations advocating for governmental oversight and accountability (implied)
</details>
<details>
<summary>
2020-07-01: Let's talk about how to watch this channel and 300,000 subscribers.... (<a href="https://youtube.com/watch?v=xzmbnUD09tw">watch</a> || <a href="/videos/2020/07/01/Lets_talk_about_how_to_watch_this_channel_and_300_000_subscribers">transcript &amp; editable summary</a>)

Beau clarifies his channel's purpose, strategies, community engagement, charity work, and Patreon without hiding content behind paywalls, ending with a message of hope for the future.

</summary>

"Context is what can get you to truth."
"Short, digestible pieces of information are more effective."
"I want this channel to help foster community and reach out to the people who need to hear it the most."
"I don't put anything behind a paywall."
"Hang in there. The long national nightmare will end eventually."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the milestone of reaching 300,000 subscribers and explains the purpose of his channel.
He clarifies why he doesn't include sources in his videos, mentioning the importance of context over facts alone.
Beau explains his strategy of breaking down information into shorter videos rather than longer ones for better engagement.
He reveals how he uses techniques like the sunk cost fallacy and vague titles to draw in viewers and convey his messages effectively.
Beau talks about fostering community through his channel and reaching out to those who need to hear his messages the most.
He distinguishes his channel from a charity while discussing the charitable work they do, including providing PPE and setting up facilities for healthcare workers.
Beau mentions his Patreon, where he offers benefits like Discord access and t-shirt discounts without putting exclusive content behind a paywall.
He shares about the challenges of conducting interviews for podcasts due to the current situation with his kids at home.
Beau explains his daily uploading schedule, advises against turning on notifications if you're a light sleeper, and ends with a message of hope for the future.

Actions:

for creators, activists, viewers,
Support community initiatives by donating PPE or setting up facilities for frontline workers (exemplified).
Follow Beau's example of using funds effectively in charitable work (exemplified).
Engage with community members on platforms like Instagram and Twitter to see the impact of charitable work (exemplified).
</details>
<details>
<summary>
2020-07-01: Let's talk about Senate Republicans and a Trump tweet.... (<a href="https://youtube.com/watch?v=_xGUA46rdrg">watch</a> || <a href="/videos/2020/07/01/Lets_talk_about_Senate_Republicans_and_a_Trump_tweet">transcript &amp; editable summary</a>)

Beau explains Trump's distracting tweet, Senate Republicans' actions, and the need to focus on critical issues rather than being swayed by social media distractions.

</summary>

"We have to stop allowing Trump's edgy tweets to control the narrative."
"He still has done nothing about the bounties."
"We've got to take control of the narrative here."
"We can't afford to become distracted right now."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains President Trump's tweet threatening to veto a defense authorization bill over an amendment by Senator Warren related to renaming military bases.
Trump's tweet is culturally insensitive, historically inaccurate, and strategically distracting.
Trump is unlikely to veto the bill and will sign it, despite his tweet suggesting otherwise.
Senate Republicans are rolling intelligence funding into the bill but removing a provision requiring campaigns to report foreign assistance offers.
Points out the inconsistency in allowing elected officials to accept foreign assistance without reporting it.
Emphasizes the need to focus on significant issues like the bounties matter instead of being swayed by Trump's tweets.
Urges for control over the narrative and to not let distractions hinder addressing critical matters.

Actions:

for voters, concerned citizens,
Contact elected officials to demand transparency and accountability regarding foreign assistance offers (implied).
Stay informed about critical political issues and hold elected officials accountable (implied).
</details>
