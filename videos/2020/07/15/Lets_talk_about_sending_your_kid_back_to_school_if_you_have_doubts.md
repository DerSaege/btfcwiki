---
title: Let's talk about sending your kid back to school if you have doubts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wQtmhbIl1C8) |
| Published | 2020/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Parents are struggling with the decision to send their kids back to school due to conflicting information from the government.
- Trust your instincts: if you doubt your child's safety at school, don't send them.
- The government doesn't prioritize your child's well-being; make decisions based on your own judgment.
- The politicization of the situation has led to bending reality for political gain, even attempting to control information about school safety.
- Don't trust information coming from an administration with a history of falsehoods; doubt is valid in this context.
- Economic concerns shouldn't outweigh doubts about your child's safety—find alternative solutions if needed.
- Take charge of your decisions; there's a lack of leadership at both federal and state levels.
- Stand firm against pressure and make choices based on what you see and believe is right.

### Quotes

- "Where there's doubt, there is no doubt."
- "Do not let the state, the government, bully you into risking your child."
- "It's time for us to lead ourselves."
- "Don't let your political party tell you to not believe what you see with your own eyes."
- "Have a good day."

### Oneliner

Parents facing conflicting information from the government should trust their instincts on their child's safety and not be bullied into risky decisions.

### Audience

Parents, Caregivers

### On-the-ground actions from transcript

- Trust your instincts on your child's safety (implied)
- If you doubt your child's safety at school, don't send them (suggested)
- Find alternative solutions if economic concerns arise (implied)
- Take charge of your decisions regarding your child's safety (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the challenges parents face in deciding whether to send their kids back to school amidst conflicting information and political pressures.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about decisions, decision making, information, and children.
There's a whole lot of people right now, a whole lot of parents saying that they're having
trouble making a decision as to whether or not to send their kids back to school.
Why is this decision difficult? That's the question.
If this was any other scenario, would it be difficult?
Right now the reason people are having trouble is because an institution that they've been
conditioned to believe is telling them something that is at odds with reality.
The state, the government, is telling them something's okay when they know it's not.
Take this scenario and put it in any other situation.
Some dude in a suit in your state capitol or in DC says it's okay for your kid to do
this but you feel that it may not be safe.
What do you do? That person does not matter, right?
Their opinion does not matter.
Nothing changed just because that person is a government employee.
If you're having trouble making this decision, there's no decision to be made.
Where there's doubt, there is no doubt.
When it comes to safety, the safety of your children, if you doubt they will be safe at
school, don't send them.
Period. Full stop.
There's no decision to be made.
The government as an institution does not care about your child.
They don't care about your kid's life.
Period.
As this became politicized, the Republican party is now in the position of trying to
bend reality to make it seem like everything's okay.
They have to do that so hopefully they can get reelected.
And now the President of the United States is attempting to circumvent the CDC and take
control of the numbers, the information that you would use to determine whether or not
it's safe for your child to go.
He wants to take control of that.
He wants to remove that from the CDC.
The easiest way to determine whether or not this is a good idea is to go back and look
at how often the President is fact-checked and he's proven to be a liar.
I would not trust information coming from this administration.
I certainly would not trust it if I felt my kid's life depended on it.
Where there is doubt, there is no doubt.
Any rational person is going to doubt information coming out of this administration.
There's no doubt.
If you're having trouble making this decision, there's no decision to be made.
It means that you have doubts about your child's safety.
Do not let the state, the government, bully you in to risking your child.
There have been times in history where people just followed the orders of the state.
It almost never works out well because the state does not care about your life, does
not care about your kid's life.
I know there are some people out there that are having trouble because of economic considerations.
They don't know how they're going to handle it once the school year starts, watching their
kids.
You found a way to do it in the summer.
Maybe it's just time to extend those plans.
If you have doubts about your kid's safety, you're going to have to.
You're going to have to figure something out because it's very clear the government doesn't
care.
There are lawyers in Florida giving teachers free wills.
If you have doubts about whether or not it is safe to send your kid back to school, don't
send them.
It's time for us to lead ourselves.
We don't have leadership at the federal level.
Most states don't have leadership at the state level.
You have to make this decision for yourself.
Do not let them bully you in to it.
Don't let your political party tell you to not believe what you see with your own eyes.
It's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}