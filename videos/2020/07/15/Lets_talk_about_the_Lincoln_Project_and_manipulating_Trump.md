---
title: Let's talk about the Lincoln Project and manipulating Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=85zX6thjQBI) |
| Published | 2020/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Lincoln Project is a group of Republicans aiming to ensure Donald Trump loses to the Democratic candidate.
- Their goal is not just for the next election, but to prevent a second term for Trump to save the Republican Party.
- If Biden wins, he will have to clean up Trump's messes, which may take time to show effects.
- Trump, on the other hand, lacks the capability to handle his own messes efficiently.
- The Lincoln Project's ads are not traditional political ads but are aimed at goading Trump into making mistakes.
- They understand that Trump is erratic, dangerous, and easily manipulated.
- By appealing to Trump's insecurities, the Lincoln Project aims to influence his decisions.
- Trump's response to the ads, like distancing himself from Dr. Fauci, shows he can be manipulated easily.
- Despite being effective in the past, the effectiveness of the Lincoln Project may diminish as Trump becomes aware of their tactics.
- Regardless of political affiliation, it is concerning that the president can be swayed by tweets and lacks the capacity for critical decision-making.

### Quotes

- "The commander in chief is making decisions because somebody made fun of him on Twitter."
- "He's a laughingstock and he can be goaded over Twitter."
- "He needs to go."
- "There's a reason we're losing every foreign policy engagement."
- "Regardless of how you feel about him, he's getting manipulated over Twitter."

### Oneliner

The Lincoln Project strategically targets Trump's vulnerabilities through untraditional ads to influence his decisions, raising concerns about his susceptibility to manipulation.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Contact Republican representatives urging them to prioritize country over party (implied)
- Support efforts to hold elected officials accountable for their actions (implied)
- Stay informed and engaged in political developments to understand the impact of manipulation on decision-making (implied)

### Whats missing in summary

Insight into the implications of Trump's susceptibility to manipulation on national security and foreign policy decisions.

### Tags

#LincolnProject #Trump #PoliticalAds #Manipulation #RepublicanParty


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Lincoln Project.
Had a lot of questions about them, but they really boil down to two main questions.
First, who are they?
Are they the good guys?
What's their intent?
Why are they doing this?
And the second is about their ads.
Their ads don't make any sense.
We'll get to that.
They do, they make perfect sense, as long as you don't look at them as political ads.
At least the type that we see in this country.
If you're overseas and you don't know who the Lincoln Project is, or you're here and
you don't know who they are, it's a group of Republicans who are going all in on making
sure that the Republican candidate for president, Donald Trump, loses.
They want to make sure the Democratic candidate wins, the opposition party.
Now people want to know why.
Some of them, I think they have a pure motive.
I think they understand exactly how erratic and dangerous the president is for the United
States.
I'm pretty sure they understand this because of their ads.
And then I think some of them have a more self-serving motive, which is they want to
preserve the Republican Party.
And they know that a second term for Donald Trump could actually end the Republican Party.
And I don't mean just for the next election, I mean permanently.
If Biden wins, all of Trump's policies, you know, when the government enacts something,
it's a slow machine.
It takes time.
Most policies really don't begin to show effect until the following term.
So if Biden is in office, he has to clean up all of Trump's messes.
And he's running against the clock because he has to do it before they spin out of control.
And if he fails from the Republican point of view, well, he can just blame the Democrats.
However, if Trump gets a second term, Trump has to clean up his own messes.
And he's incapable of that.
He doesn't have the skill set to deal with some of the messes that he's created.
And he doesn't have the staying power.
This isn't something he can just declare bankruptcy on and walk away like he has everything else
in his life.
And people are worried.
Within the Republican Party, they're worried that if Trump has to deal with these messes,
he won't be able to.
The voters will see that and they will blame the entire Republican Party because the Republican
Party enabled him.
I think that voters can rightfully blame the Republican Party for this.
So they're trying to get him out.
That's what's happening.
Okay, so that's their intent.
What about their ads?
Why don't their ads make any sense?
They don't have a clear political vision.
They don't have a clear philosophy.
They don't seem to be appealing to voters.
What are they doing?
They're not trying to do any of that.
They understand that he's erratic and dangerous and easily manipulated.
They're trying to goad him into making mistakes.
And it's working.
It is working.
One of the more recent videos that they put out that went viral was the one where it was
aimed directly at Trump, talking directly to him, very overt about trying to get inside
his head, saying, everybody's against you.
They're all laughing at you.
They're all whispering about you.
Everybody's leaking.
You can't trust anyone, Donald.
It's everyone.
And it's funny.
I would imagine that the Lincoln Project was actually alluding to the fact that like 60%
of their funding is coming from small donations of less than like 200 bucks, the kind that
are really not tracked to that well, that I believe are probably originating in the
House and the Senate, Republicans in the House and the Senate who want Trump gone, but don't
want to come out publicly against him because they need his base during the next election.
I think that's what the Lincoln Project was actually getting at.
But Trump's not that bright.
He doesn't see stuff like that.
He doesn't think anything through.
He sees what's right in front of his face.
So he looks at the high profile people in his administration.
Who's most likely to betray me?
Well, the one that's speaking out against you, of course.
How long was it between that ad and him starting to distance himself from Dr. Fauci?
Not very long.
The president of the United States is getting manipulated via tweet.
I know some people are saying, well, he could have fired Fauci.
That would have been dangerous.
Having a president that can be manipulated over Twitter is dangerous.
So it's working or it was working.
I actually think they're done and we'll get to that in a second.
And yeah, this is dangerous.
It's something that they have to be right on the money with.
It can't be too hot.
It can't be too cold.
They got to be perfect.
We're talking Goldilocks here.
And they're doing a good job of tempering what they say.
In another recent one, they had a seal come out, you know, the epitome of a tough guy
in the United States.
And basically call him spineless for his lack of a response to the bounties.
I don't think it was a coincidence.
In fact, I tweeted about it at the time, but I didn't want to say too much because I knew
I was going to make this video.
I don't think it was a coincidence that the tough guy image was broken for just a second
just to make sure that Donald Trump understood that there were diplomatic and economic responses
as well.
It didn't have to be a military one because the president is dangerous because he is erratic.
And if that had been the one that went viral and pushed his buttons and goaded him for
all we know, he could have launched a strike on Chelyabinsk.
So they're doing a good job at tempering it.
Yeah, the Fauci thing almost got out of hand, but overall, I think they've done a good job.
I didn't make this video because they were doing a good job.
I'm willing to make it now because I think they're done.
I think somebody in the administration shortly before the denials started getting issued
about trying to smear Fauci grabbed a giant box of crayons and broke it down for the president
and explained what was going on to him.
So I don't think they're going to be as effective as they have been in the past.
Now I could be wrong about that because it is Trump and he is an emotional person.
So it may still work, even if he knows what they're doing.
But we'll have to see.
Now if you are a supporter of the president, please understand that regardless of how you
feel about him, he's getting manipulated over Twitter.
The commander in chief is making decisions because somebody made fun of him on Twitter.
That man is not fit to be in that office.
He's not.
Doesn't matter if you support him or not.
You have to acknowledge that.
Because if a group of Republicans can tweet him and have him make policy changes or staffing
changes, imagine what a foreign intelligence service could do with training and resources.
There's a reason we're losing every foreign policy engagement.
The president of Mexico said he didn't have an opinion on the wall because the president
of Mexico knows that Trump's completely ineffectual.
How embarrassing is that?
Make America great.
He's a laughingstock and he can be goaded over Twitter.
He needs to go.
Now I don't know if Biden will be able to clean up his messes, but I know that he can't.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}