---
date: 2023-02-16 09:28:30.128000+00:00
dateCreated: 2023-02-16 02:15:49.900000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about how you can create change without being in the street....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UDfp_nRqaxU) |
| Published | 2020/07/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how individuals can create deep systemic change within the United States even if they can't physically participate in street protests.
- Emphasizes the importance of diversity of tactics, strategy, and shifting the Overton Window to achieve the grand goal of ending unacceptable things.
- Points out that disrupting business through street protests grabs the attention of the government, which is closely tied to business interests.
- Acknowledges that while small concessions from the government are helpful, the ultimate goal is changing societal thought.
- Defines the Overton Window as the acceptable range of political thought and stresses the need to shift it to make certain ideas unacceptable.
- Encourages individuals to use their influence on social media and in everyday interactions to change thought and challenge covert racism.
- Urges people who can't physically participate in protests to still play a vital role in changing societal perceptions and pushing unacceptable ideas out of the Overton Window.
- Suggests using hobbies, such as creating art or memes, to contribute to shifting the Overton Window and promoting social change.
- Stresses that any action taken to shift societal thought is valuable, regardless of the form it takes.
- Advocates for engagement in letter writing, signing petitions, contacting legislators, and supporting bail funds as ways to support street actions and create change.

### Quotes

- "The legislation is just a tool to help [change societal thought]."
- "You don't need combat boots and a mask to be involved in this campaign."
- "Gotta push it all the way out. Gotta get it out of the window completely."
- "Make it unacceptable in every way. Every form of covert racism."

### Oneliner

Beau explains how individuals can contribute to deep systemic change by shifting societal thought and pushing unacceptable ideas out of the Overton Window, even without physically participating in protests.

### Audience

Activists, Allies, Community Members

### On-the-ground actions from transcript

- Use your social media accounts to influence thought by discussing privilege and the need for a police database (implied).
- Call out covert racism and challenge unacceptable ideas in everyday interactions (implied).
- Support bail funds and other organizations involved in street actions (implied).
- Write letters to the editor, sign petitions, contact legislators, and vote with your dollars to support social change (implied).

### Whats missing in summary

The full transcript provides detailed insights into how individuals can make a significant impact on societal change through various actions, including influencing thought, challenging unacceptable ideas, and supporting street actions.

### Tags

#SystemicChange #OvertonWindow #SocialActivism #Influence #CommunityEngagement


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So tonight we are going to talk about how you personally can help create deep systemic
change within the United States, even if you can't be out in the streets.
In order to do that, we are going to talk about diversity of tactics, the actual strategy
of what we're hoping to accomplish, and the Overton Window.
Right now, it seems like the only way to create change is to be in the streets, because that's
what's getting the most attention.
That is what the media is focusing on.
But what's the goal here?
What is the grand goal?
We want to end certain things.
We want to get rid of them.
We want them to not be acceptable anymore.
Right?
That's the goal.
The people in the street, the boots on the ground, the [[tip of the spear]], they are disrupting
business.
People being out in the street, that's bad for business.
When business gets impacted, the government notices, because in the United States, business
and government are inextricably tied.
The government, the establishment, wants to get back to the status quo, business as usual.
They want to keep making money.
Because of that, they're willing to make small concessions when it comes to legislation.
It seems as though the people in the street are achieving the most results, because they're
getting these small concessions.
These small concessions are important, but it's not the whole fight.
It's not the whole fight.
The Civil Rights Act has been around a long time, but we still have these problems.
If legislation alone, if politicians writing something down on a piece of paper and saying,
this is how it is now, if that actually changed society, we wouldn't have anybody in prison.
It's a fiction.
It helps, because it helps guide things.
But it doesn't actually change society.
If you want to change society, you have to change thought.
You have to change the way people think.
You have to shift something called the Overton Window.
The Overton Window is short version.
It is the acceptable range of political thought within a country at any given time, as viewed
by the average person.
That's the Overton Window.
There was a time when owning another human being was inside the Overton Window.
It's not anymore.
We have shifted it away from that.
That's no longer acceptable thought.
What we have to do is keep shifting it.
So being the HR person who takes that resume where the name really doesn't sound right
and puts it at the bottom of the stack, that's unacceptable.
That person gets fired.
We have to change thought, the way people think about things.
All the forms of covert racism that exist, they have to be moved outside the realm of
acceptable thought, because right now they're not.
Right now they're on the edge.
People excuse it.
Well, they're just old.
They're from a rural area.
Well they grew up here.
Well it was just a joke.
No.
It's outside the realm of acceptable thought.
If you're one of those people who for whatever reason can't make it and be there physically,
you're probably more important than you think.
Because what are the reasons?
Geography, right?
Can't get to a major city?
There's nobody around that thinks the way you do.
You're in an industry.
You have a job that would find that too controversial.
You're in contact with the people that have to be reached.
You're behind enemy lines.
So don't feel because you can't be there in person that you can't make a difference.
You may be able to make more of a difference than you might imagine, because the end goal
is changing thought.
The legislation is just a tool to help do that.
So those small concessions from government, do they help?
Yeah, and some of them would help a lot.
But it's not the end goal.
It's not the end goal.
Law is what it is.
It doesn't actually change society.
If it did, nobody would be in prison.
The government doesn't have as much authority as people like to think it does.
But you do, because you can help shift that acceptable thought.
Your social media account, not the one you use for your political stuff, but your normal
one.
The one that all your friends and family are on.
The one you don't talk about this stuff on because you don't want to hear what they have
to say.
That's the most important account to use.
That's the one that you need to explain what privilege is.
That's the one you need to use to do that.
That's the one where you need to talk about the need, why we need a police database.
That's where it has to be done, because you can influence thought.
And maybe you don't change anybody's mind.
But I'm willing to bet wherever you are, there's somebody else who thinks just like you, who
also thinks just like you in regards to, there's nobody around that thinks the way I do.
Sometimes when you're saying something, you're not doing it to change somebody else's opinion.
You're doing it to let other people know they're not alone.
Because then they'll reach out.
Somebody has to stand up first.
You can do that.
And then you can use your hobbies.
If you're an artist, think about those propaganda posters during World War II.
They helped move the window.
They helped get everybody committed to a cause.
You can do that.
Make memes.
You know, I know that's funny to hear me say.
But on some people, it works.
Diversity of tactics.
Anything you're doing to help shift that window is good.
It doesn't matter if some seasoned boots on the ground activist is like, ah, you know,
you're an internet activist or whatever.
Yeah fine, whatever.
Doesn't matter.
If you're helping to move that window, you're in the fight.
You don't need combat boots and a mask to be involved in this campaign.
The goal here, the end goal, is to change thought.
You can do that.
Even if you're limited.
Even if you can't get to these major events.
You can help shift thought.
Sometimes it's just you saying that, hey, this is wrong.
You know?
When you see one of those covert racist jokes, call it out.
Ask people to explain it to you.
I have a video on this.
Ask them to explain why it's funny.
And then watch them try to weasel their way out of it.
Eventually you'll grow to like it.
I do, anyway.
When people talk about crime rates and try to link them to skin tone, ask them directly.
Are you saying that being this skin tone makes you a criminal?
Makes you more likely to be a criminal?
Well no, no, no, no, no.
That's not what I was saying.
Because it's already on the edge of political thought.
Gotta push it all the way out.
Gotta get it out of the window completely.
That's how we get that deep change.
So don't feel like you can't be involved.
You can.
You can also write letters to the editor.
Sign petitions.
Call your legislator.
Whatever.
All of the normal stuff.
You can, if you really want to help with the street actions, two things that are important.
Number one, bail funds.
Number two, most of the people that are there, they're part of some other organization.
And a whole bunch of people got together at that organization and decided to go.
Like they're a food pantry.
They still have to keep their normal operations up and running.
So maybe your job finds it too controversial for you to be out in the street.
But they'd probably be okay with you working at a food pantry.
That frees one of them up to be in the street.
There are ways to do it.
And don't forget to vote with your dollars.
You know, reward those companies that take a stand.
Penalize those that don't.
Make it unacceptable in every way.
Every form of covert racism.
It's unacceptable.
And act as if it already is.
Shift that window.
Because that's what's going to create long-term change.
The legislation that's relying on a government fiction that says they have a lot more authority
and a lot more power than they really do.
In many ways, legislation is a suggestion.
It's a suggestion.
You have to change the way people think.
And if you're one of those people who can't get out in the streets, odds are you are the
most important person when it comes to changing thought.
Because you're in direct contact with those who are on the edge of that window.
And they just need a little shove to get those ideas outside of the acceptable realm.
You can do that.
You can do that more than I can.
Because people know my views.
And if they don't like them, they don't talk to me.
So I have no ability to reach in that manner.
You do.
Anyway.
Go change the world.
It's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}