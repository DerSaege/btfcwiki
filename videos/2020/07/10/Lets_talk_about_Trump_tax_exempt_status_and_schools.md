---
title: Let's talk about Trump, tax exempt status, and schools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jCrgv8MVpDg) |
| Published | 2020/07/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump desires to ensure universities are run correctly, despite his own controversial university's past payment to students.
- Trump believes universities teach left-leaning ideas, leading him to suggest revoking their tax-exempt status—a concept not supported by tax code or the constitution.
- Beau suggests auditing the tax-exempt status of all universities, including far-right Christian schools involved in political activities.
- Beau sees potential in Trump's proposal, as it could uncover inconsistencies in how funds are used by universities.
- Trump's tweets call for a reexamination of universities' tax-exempt status if they are deemed to propagate propaganda.
- Beau criticizes Trump's lack of understanding and excessive use of capitalized letters and punctuation in his tweets.
- Beau acknowledges the importance of reviewing universities' financial practices but points out that student-led organizations may not impact tax exemptions.
- Beau supports the idea of scrutinizing educational content in universities, especially in right-wing Christian schools.
- Beau questions how Christian schools reconcile teachings that may contradict the principle of loving one's neighbor.
- Beau concludes by suggesting that the Trump administration should spend time in a college to better understand the situation.

### Quotes

- "Our children must be educated not indoctrinated."
- "I think opening this door is fantastic."
- "I think it's a good idea."
- "How Christian schools get away with teaching people to not love their neighbor."
- "Y'all have a nice day."

### Oneliner

President Trump's desire to revoke universities' tax-exempt status sparks a call for auditing all institutions, including right-wing Christian schools, to ensure educational integrity and financial transparency.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Advocate for transparent financial practices in universities (implied)
- Support auditing educational content in institutions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's proposal regarding universities' tax-exempt status and prompts critical thinking about educational content and financial transparency in academic institutions.

### Tags

#PresidentTrump #TaxExemptStatus #UniversityFunding #Education #FinancialTransparency


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about President Trump and his desire to make sure that America's
universities are run correctly.
Because we all know that when we think of a correctly run university, we think President
Trump and his university that had to pay students.
His complaint I guess is that universities are teaching left leaning ideas and therefore
they should have their tax exempt status yanked.
That's not a thing.
That's not how the tax code, the constitution, any of this stuff works.
Once again the president is showing his own utter incompetence.
But I think he's on to something.
I think we should audit the tax exempt status of every university in the country.
Now the ones he's talking about, it's not going to impact them.
But once we start doing this, it'll open the door to audit all of the universities.
All of them.
Including all of those far right Christian schools that use funds to get their students
to political events, endorse candidates, and actively get involved in politics.
That will make them lose their tax exempt status.
I think we're on to something here.
Once again he's failing so hard he's winning.
This is a real issue.
So let's address it.
His actual tweets by the way.
Too many universities and school systems are about radical left indoctrination, not education.
Therefore I am telling the treasury department to reexamine their tax exempt status and or
funding which will be taken away if the propaganda or act against public policy continues.
Our children must be educated not indoctrinated.
Again full of all the random capitalized letters and punctuation that isn't needed.
I think that it would be a great idea for the Trump administration to spend any time
in a college.
But this idea, it's good.
We do need to review it.
We need to take a hard look at where the money from these universities that are tax exempt
where it's going.
What he's talking about these organizations on campus, they're set up by students.
They have nothing to do with the university administration and therefore would not impact
their tax exempt status.
However, that's not true of the right wing Christian schools.
So I think opening this door is fantastic.
I think it's a good idea.
And if we're going to actually look at the content of what they're teaching and what
they're supposed to be teaching, I would like to know how Christian schools get away with
teaching people to not love their neighbor.
Anyway, it's just a thought. Y'all have a nice day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}