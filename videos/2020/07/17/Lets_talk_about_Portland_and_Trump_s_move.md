---
title: Let's talk about Portland and Trump's move....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EDEv3Ey-Tfg) |
| Published | 2020/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's recent actions suggest he has joined the movement in Portland by actively helping recruit for it.
- The administration's handling of the situation in Portland goes against established tactics for dealing with movements.
- The use of unmarked vehicles for snatch and grabs in Portland is seen as a form of harassment by the Trump administration.
- The arrests and detainments appear to be aimed at intimidating the movement in the streets rather than for legitimate reasons.
- Security clampdowns like this are strategically flawed and tend to backfire by generating fear and sympathy for the movement.
- Historical examples, such as Ireland in 1916, demonstrate how government overreactions can solidify movements and lead to significant changes.
- Overreactions to protests can crystallize movements by creating sympathizers out of fear and anger towards the government.
- Provoking a government overreaction is a known tactic for some militant groups seeking revolutionary change.
- The protesters in Portland have not done anything to warrant the security clampdown they are facing.
- Mainstream figures like Dan Rather have pointed out the counterproductive nature of the federal actions in Portland.

### Quotes

- "Every one of you they snatch brings ten more to your cause."
- "Trump has done more to grow the movement in Portland than any activist has."
- "Security clampdowns, these kind of measures, backfire always."
- "It appears to be just a method of harassing the movement in the streets."
- "Government overreactions can solidify movements."

### Oneliner

Trump's actions in Portland suggest he's supporting the movement, but his administration's tactics are backfiring, provoking sympathy and growth for the cause.

### Audience

Activists, Protesters, Community Members

### On-the-ground actions from transcript

- Connect with local activist groups to understand how best to support and amplify the movement (suggested).
- Share information and updates about the situation in Portland on social media to raise awareness (suggested).
- Stand in solidarity with those affected by the security clampdown in Portland by attending or organizing peaceful protests (exemplified).

### Whats missing in summary

The full transcript dives deeper into historical examples and the practical implications of government responses to movements, providing a comprehensive understanding of the current situation in Portland and its broader implications.

### Tags

#Trump #Portland #Protests #GovernmentOverreach #Activism


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about how Trump has joined the movement.
Trump is now on the side of those in the street.
That's the only thing we can take away from his most recent actions, and
that his entire administration is behind this, because nobody apparently stopped him.
So they must at least secretly hold some loyalty to the Defund the Police movement,
specifically the area in Portland.
The reason I say this is because the most recent actions there are counter to
everything, everything that is known about how to handle these type of movements.
So the only thing I can take away from it,
other than just assuming that he's completely incompetent and inept, and
we all know that's not true, is that he has decided to actively help
the movement in Portland recruit, because it's in the manual not to do what he's doing.
If you don't know what's going on, apparently the feds
are using unmarked vehicles to conduct snatch and grabs in Portland.
They're just rolling up, yanking people in.
I know people wanna talk about the legality and the constitutionality of it.
The Trump administration does not care about the Constitution,
and they don't care about staying within the law.
So I'm gonna skip over that.
We're gonna go straight to the practical aspects of this.
These arrests, detainments, whatever,
they appear to be just a form of harassment.
There are a few extreme circumstances which this would be warranted.
I have found precisely zero evidence suggesting that's the case.
It appears to be just a method of harassing the movement in the streets.
That's really what it boils down to.
It looks like they're just snatching up people who are dressed in all black.
In the cases I've been able to actually track down what's happened,
they've let them go pretty much as soon as they ask for a lawyer.
This security clampdown, which is what this is, is wrong.
Not just morally, not just ethically, not just legally, not just constitutionally.
Tactically and strategically, it's in the manual, don't do this.
When you have a defined geographic area, let's say the city of Portland, and
a movement has popular support within that geographic area,
you don't conduct a security clampdown on them.
Because it just creates people who sympathize with their movement.
In more militant organizations, it is actually part of the goal,
it is part of the strategy to provoke the government into an overreaction,
into a security clampdown.
Because what happens is when the government overreacts,
the average civilian, the person that is not affiliated with the movement,
they get caught up in it.
They're scared.
They don't know if they're next.
It generates a climate of fear.
They become afraid of the government, therefore, they sympathize with
the movement.
Then, eventually, something goes wrong.
Something happens.
Could be one of these vans accidentally hitting somebody.
It could be them snatching up somebody who is beloved in the community.
It could be one of these people resisting, and it going loud.
Any of this stuff that happens, it gets blamed on the security clampdown.
Therefore, it gets blamed on the government.
Therefore, all those people who started to sympathize with the movement,
they join.
They're no longer sympathetic.
They're active participants, and they take to the streets.
This, the understanding of this theory started more than 100 years ago.
This isn't new.
This isn't new.
People have understood this for 100 years now.
Security clampdowns, these kind of measures, backfire always.
They don't work.
They've never worked.
So what we're really going to talk about is what Portland can learn from Ireland.
In 1916, a movement in Ireland took to the streets.
Easter.
They occupied government buildings.
Now this was a more militant movement, but the same thing happened.
Government overreaction, like we're talking artillery.
When it was over, the reaction, the overreaction continued.
They took the leaders, and they literally put them on the wall.
Those who they thought were ringleaders, they put them on the wall.
Boom.
There was one guy, Eamon de Valera, who was slated to be put on the wall, but because
he had an American passport, he got a pass.
Because of that overreaction, the movement in Ireland solidified.
It cemented.
Wasn't very long before we had the Republic of Ireland.
Up until then, it had been occupied by the British.
That crystallization of the movement, all of those sympathizers, they were created because
of the government overreaction.
Since then, militant groups, especially those who are looking for subrevolutionary or revolutionary
change, they intentionally try to provoke a government overreaction.
In this case, the protesters have done nothing in Portland to warrant a security clampdown.
When you look at the list of reasons, it's like graffiti and broken windows.
That warrants roving bands snatching up protesters.
That doesn't play well with the general populace.
To prove this, you can go to Dan Rather's tweet on it.
Dan Rather.
It does not get more mainstream than Dan Rather.
Look at what his followers are saying in the replies.
Look at the terms they're using to describe the federal agents.
Trump has done more to grow the movement in Portland than any activist has, and he did
it in like two days.
If you are caught up in this, I understand it is uncomfortable.
It is unnerving if you've never been snatched before.
It's scary.
It won't be the second time, don't worry.
It does appear that as soon as you ask for a lawyer, they let you go because they don't
really have a reason to hold anybody.
If you are part of this movement, just accept this for what it is.
The legal arguments, the constitutional arguments, they don't matter.
Look at it from the practical aspect, to still a phrase from Devalera, every one of you they
snatch brings ten more to your cause.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}