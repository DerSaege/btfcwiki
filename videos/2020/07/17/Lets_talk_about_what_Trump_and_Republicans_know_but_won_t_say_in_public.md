---
title: Let's talk about what Trump and Republicans know but won't say in public....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=14aO7DKUlws) |
| Published | 2020/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican National Convention in Florida will have 2,500 attendees, with a cap of 7,000 for guests.
- Originally scheduled in North Carolina with an anticipated 50,000 attendees, now reduced to 2,500 attendees.
- Republicans moved the convention due to attendance concerns but have drastically reduced numbers themselves.
- Despite taking precautions for their own safety, Republicans continue to push for normalcy and downplay risks to their supporters.
- Beau notes the indifference to human suffering and the disconnect between actions taken and messages being conveyed.
- Republicans seem more focused on post-election outcomes rather than the current health crisis.
- Trump's ego played a significant role in decisions around convention attendance and location.
- The convention is portrayed as more of a party or show rather than a serious decision-making event.
- Beau questions the disregard for science in the face of holding a large-scale event during a pandemic.

### Quotes

- "Nobody wants to be a tough guy until it's time to go through the door."
- "They're banking on it not getting out of hand until after the election."
- "He has to know how stupid he looks, moving the entire convention to satisfy his ego."

### Oneliner

The Republican National Convention in Florida drastically reduces attendance while maintaining misleading messages on COVID-19 risks.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Hold accountable political leaders who prioritize ego over public health (exemplified)
- Advocate for science-based decision-making in public events (exemplified)

### Whats missing in summary

The full transcript provides a deeper insight into the disconnect between actions taken for safety and the contradictory messaging on COVID-19 risks.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about North Carolina, Florida, trust and indifference.
The agenda, the itinerary, the rough sketch for the Republican National Convention here
in Florida, it's been released.
It's going to happen next month.
The overwhelming majority of the time it will have 2,500 attendees, that is 2,500 attendees.
On the last night, for some period of time at least, they will allow guests.
But it will be capped at 7,000.
Now if you're in Florida or you're paying attention to what's going on down here, you're
like that doesn't really seem like a good idea because it's not.
But that's what they're doing.
I want to point something out though.
This was originally scheduled to happen in North Carolina.
The convention was going to happen in North Carolina and they moved it because the governor
of North Carolina was concerned about attendance, how many people were going to be there.
So they moved it.
The CDC said that Republicans were touting 50,000 attendees.
50,000, now it's 2,500.
Now I'm sure Republicans will try to spin that.
Well they all wouldn't have been there at the same time or whatever, fine.
Cut the number in half.
25,000 to 2,500.
Ten percent.
In reality it's five.
What does that mean?
Means Republicans have changed.
They've changed.
They recognize the danger now.
They see the need to take precautions.
They want to protect themselves.
Makes sense.
What hasn't changed?
Their talking points.
What they're telling the American people.
They moved their convention because they weren't going to be allowed to have a big crowd.
And now they've limited the crowd size themselves to five percent, ten percent if you want to
go with whatever spin comes out.
But while they've taken those actions for themselves, they're still telling their supporters,
those people who trust them, in this case with their life, that everything's fine.
We're getting back to normal.
Your kids need to go to school.
You need to get back to work.
Sacrifice yourself for the economy.
I would do it, but I'm needed in D.C. in the rear.
You guys on the front lines.
Y'all have fun though.
I wish I was there with you.
Remember all that tough talk?
Nobody wants to be a tough guy until it's time to go through the door.
I don't know that I have ever seen this level of indifference to human suffering.
I've seen some messed up stuff.
They have reduced their attendance by 95 percent, 90 percent, whatever.
But they're still telling their supporters the same thing.
Don't worry about a mask.
Be all right.
They're just hoping that their supporters don't have to go to too many funerals between
now and November.
They're banking on it not getting out of hand until after the election.
They know it's going to.
If things don't change, they know it's going to.
But they're more concerned about this.
Can you imagine how hard this was for Republicans to convince Trump to agree to shrink the crowd
size like this?
Especially after he took to Twitter, moved the whole convention just to satisfy his ego.
They had to convince him that this was okay, which means Trump knows too.
But his talking points haven't changed.
He's still saying the same thing.
But when it comes to them and their safety, well then, we have to take the precautions.
I can't imagine that battle.
He has to know how stupid he looks, moving the entire convention to satisfy his ego and
then still only getting 2,500 people because it's that bad.
And there's no denying it anymore.
I think that they need to rethink this.
Go ahead.
Have your full festival.
It's what it is.
It's a party.
It's not like you're really deciding anything there.
It's just a show like everything else.
I don't see why there's any reason to let science get in the way of it.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}