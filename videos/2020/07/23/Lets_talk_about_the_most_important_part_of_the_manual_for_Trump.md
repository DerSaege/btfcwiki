---
title: Let's talk about the most important part of the manual for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7qLp88n6s5k) |
| Published | 2020/07/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of discussing manuals and strategies for managing intense situations.
- Critiques the Trump administration's strategy based on their own manuals.
- Mention of the likelihood of escalation into a more intense movement.
- Refusal to provide a how-to guide for campaigns that may incite violence.
- Encourages viewers to understand the theory and take responsibility.
- Emphasizes the nature of escalated situations as PR campaigns with violence.
- Calls for a proactive PR campaign to control the narrative before escalation.
- Breaks his usual rule of not calling for action due to the importance of the message.
- Utilizes recurring themes from his channel to urge viewers to take action.
- Shares the potential reach and impact of spreading the message through social media.

### Quotes

- "If you have the means, you have the responsibility."
- "Lead yourself. Force multiplication."
- "We have to stop a war."
- "Just be ready to control the narrative."
- "No rational person wants this."

### Oneliner

Be ready to control the narrative and stop a potential war by leveraging social media influence to prevent escalation.

### Audience

Online activists and social media users.

### On-the-ground actions from transcript

- Share the message through social media to control the narrative (implied).
- Explain the situation and question motives on social media (implied).
- Encourage others to understand and prevent escalation (implied).

### Whats missing in summary

Full understanding of Beau's message on preventing escalation through proactive narrative control and social media influence.

### Tags

#PreventEscalation #ControlTheNarrative #SocialMediaInfluence #StopPotentialWar #Responsibility


## Transcript
Well howdy there internet people, it's Beau again.
So tonight's important, so stick with me.
So far in this series of videos, the ones that reference the manual, we've talked about
how these things start, we've talked about the best practices to manage one before it
starts and how to manage one once it starts.
We've talked about the Trump administration's current strategy and how it is wrong on every
level according to their own manuals, their own documents.
We've talked about the likelihood of this causing a street movement to transition into
something more intense.
We've pretty much covered it all.
The only thing we haven't talked about is what happens after it transitions, once it
becomes more intense, once it becomes full-fledged.
And to be honest, I'm not going to do a video describing how to wage a campaign like that,
mainly because with my luck somebody will turn that into a how-to guide and go do a
bunch of horrible things.
And I want no part of that.
But see, at this point, if you've watched those videos, you've got a good grasp on this
subject.
You understand the theory as well as most soldiers.
And that has some responsibility with it.
At the end of the day, we don't need descriptions of how these things work.
We know they're bad.
We can look to the other countries.
Some of these countries have had these campaigns going on for 20 years.
We don't need to cover that.
But what's important is the understanding of one last little bit.
At the end of the day, when these things escalate and they go full-fledged, they're a PR strategy.
PR campaign with violence.
That's it.
You can church it up however you want, wrap it in whatever flag you want to, drape it
in whatever cause you believe in.
It is a PR campaign.
That's it.
Nothing more.
That's what it's all about.
Why don't we do the PR campaign now?
Before it starts.
Call the narrative now.
If you've watched this channel for any length of time, you know I don't do calls to action.
I don't know what video we're at now.
800?
Maybe two.
The entire time.
I don't do it.
I don't do it mainly because I've done the math on it and it's a little intimidating.
But we're going to do one tonight because it's important and everything lines up.
The recurring themes on this channel, Rule 303, if you have the means, you have the responsibility.
Evolution over revolution.
Good ideas generally don't require force.
Lead yourself.
Force multiplication.
These are things that come up over and over again and they all converge right now.
On average, this series of videos that are referencing the manual, they get watched by
about 150,000 people each.
And that's big.
I mean, that's bigger than a Trump rally.
But that's not enough to control the narrative, right?
It is.
It is.
The average Facebook account has 338 friends.
You do the math on that.
That's 50 million people.
That is 50 million people.
Now granted, your social media account, maybe it doesn't have that many.
Maybe it has more.
And you're not going to reach everybody on your account, but some of the people you reach
are going to reach others.
So by the time it's all said and done, if everybody takes this message to heart, we're
talking about 50 million people.
A pretty good chunk of the population can be influenced by it.
We have to stop a war.
If this spins out of control, that's what it turns into.
You understand this material now.
You can explain where they're wrong, why they're wrong.
You have the links to the documents.
Y'all should have known something was up when I started providing links.
You know where these stormtroopers are headed next.
And you know right now, they're not in the news.
They're not on fire, these cities.
There's not massive events going on.
They're not out of control.
So if that happens after his forces arrive, what does that mean?
Their fault.
They escalated it.
We have to use our social media accounts.
Put in your own words.
Nothing inorganic, no hashtag, nothing like that.
Just be ready to control the narrative.
Explain what's happening.
And get people to question why he's going against all of the manuals, all of the experts.
What is he hoping to accomplish by provoking a response?
What is his goal?
There's got to be one.
Nobody can be this wrong without some other reason.
Nobody is this bad at it.
I mean, he actually has to overcome safeguards to be this wrong.
There's got to be a motive.
There's got to be a reason.
I don't know what it is, and I'm not going to speculate.
But nobody's this wrong by accident.
So controlling the narrative, yeah, that's a tall order.
But the math is on our side.
The end goal is to stop a war.
And stop this from spinning out of control.
It's certainly worth trying, because no rational person wants this.
Anybody who's ever seen it, studied it, or even glanced at it on the news knows this
isn't something we want.
We don't want it here.
We don't want it anywhere else, really.
It's bad.
And yeah, maybe we get a better system on the other side.
That's a big maybe.
I'm not okay with not trying to stop it.
So yeah, I didn't mean to dump too much on you today, but it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}