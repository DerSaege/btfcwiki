---
title: Let's talk about the US, third countries, and thanking Canada....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CVSVTQMD_KY) |
| Published | 2020/07/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Safe Third Country Agreement between the US and Canada, governing immigration policies, particularly for asylum seekers.
- Notes that if someone arrived in the US first, they couldn't claim asylum in Canada at a traditional crossing.
- Mentions a dangerous loophole where individuals could cross into Canada through unmanned areas.
- Points out the danger of such crossings, especially for those traveling with children.
- Canadian court declared the agreement unconstitutional due to the US not being a safe third country anymore.
- Emphasizes that the US has been ignoring treaties and breaking its own laws related to asylum seekers.
- States that issues with asylum seekers predate the Trump administration, with Trump exacerbating the situation.
- Indicates the need for reforming policies beyond just returning to the status quo.
- Stresses the importance of honoring agreements entered into by the country.
- Expresses gratitude towards the Canadian courts for acknowledging the issues and pushing for necessary reforms.
- Acknowledges that the fight for asylum seekers' rights is not over despite the court ruling.
- Hopes that the Canadian government won't appeal the decision and will recognize the US's failure to adhere to international law.
- Appreciates Canada stepping up to potentially save lives through their actions.
- Concludes by noting the ongoing work required beyond removing Trump from office.

### Quotes

- "The US is not a safe third country anymore."
- "It's not a detriment. It's not something that's going to throw open the borders of Canada or anything like that."
- "This will save lives."

### Oneliner

Beau explains the implications of the overturned Safe Third Country Agreement between the US and Canada, urging for policy reform to honor international agreements and ensure asylum seekers' rights.

### Audience

Policy reform advocates

### On-the-ground actions from transcript

- Advocate for policy reform to ensure the protection of asylum seekers (implied).

### Whats missing in summary

The full video provides a deeper understanding of the historical context and implications of the Safe Third Country Agreement, along with the ongoing challenges faced by asylum seekers in the US and Canada.

### Tags

#US #Canada #AsylumSeekers #ImmigrationPolicy #InternationalAgreements


## Transcript
Well, howdy there, Internet of People.
It's Beau again.
So today we're going to talk about the US, Canada, and safe third countries.
Because the US isn't one anymore.
I'm so glad.
If you've been watching this channel a while, you might remember me just railing about something
called the Safe Third Country Agreement.
It was an agreement between the United States and Canada.
And it governed immigration policies.
It governed specifically asylum seekers.
And the way it worked was that if you arrived in the United States, you set foot in the
US first, you couldn't claim asylum in Canada at a traditional crossing.
Now you could go, there was a loophole in it, you could go to some unmanned area and
cross into Canada that way.
That wasn't always safe, especially if you were traveling with kids, a lot of those areas
are rough.
So it became dangerous.
This agreement was just struck down by a Canadian court.
They said it was unconstitutional on their side.
The basis for that was that the United States is no longer a safe third country.
They're right.
They're right.
For asylum seekers, it is not.
It hasn't been for a while.
In the US, we have a lot of agreements that we have signed onto, a lot of treaties.
As we've talked about before, treaties are the law of the land.
It's backed up by the Constitution.
And we've been ignoring them.
We've been breaking our own laws repeatedly.
Something I was really happy to see the Canadian courts acknowledge openly was that a lot of
these issues predate the Trump administration.
Yeah, Trump made them worse, a lot worse.
As somebody who was very open in their opposition to Obama's immigration policies, Trump's made
them infinitely worse.
However, that acknowledgement from the Canadian court means a lot because when we finally
start to reform our policies and bring them back in line, we're not just going to be able
to go back to the status quo.
Not if we want acceptance in the international community again.
We're going to have to bring them past what they were, which is where they need to be.
We need to honor the agreements that this country entered into.
A lot of asylum seekers have been illegally and unconstitutionally denied asylum in the
United States, or they've been detained, or they've been punished, all of which is unconstitutional,
all of which is illegal under our own laws.
And we've been doing it for years, more than three.
Some of these policies extend back to, I mean, there's actually a couple that extend back
to Bush, but they became worse under Obama.
And then it took on a very ugly face under Trump.
The Canadian court openly acknowledging this and explicitly saying it predates the Trump
administration is going to require us to get our act together.
So I cannot thank the Canadian courts enough for that.
Now the fight isn't over.
This is a huge win, but the fight isn't over.
The Canadian government can, in theory, appeal this and try to fight it.
I don't know the ins and outs of their legal system the way I do the U.S.'s.
However, I do know their moral system, and I would hope that they don't.
I hope they don't appeal it.
I hope they understand that the United States is not a safe third country anymore.
It is behaving like a failed state.
It is violating international law when it comes to the treatment of asylum seekers.
So it appears that our neighbors to the north are going to start picking up our slack.
And as somebody who has been in that particular argument for a long time, I cannot express
my gratitude enough.
This is going to save lives, period.
This will save lives.
And it's not a detriment.
It's not something that's going to throw open the borders of Canada or anything like that.
The asylum process is pretty rigid to begin with.
So the U.S. making it even more so and trying to deter asylum seekers, not only is it illegal,
it's immoral.
And I'm really happy to see the Canadian government stepping up in this manner.
Anyway, this should serve as a reminder that, yeah, we need to get Trump out of there.
But even once he's gone, we've got a lot of work to do.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}