---
title: Let's talk about what it looks like when they don't read the manual....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wMSVFSCj7Zo) |
| Published | 2020/07/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the transition from peaceful protests to more intense movements in the streets.
- Describes the typical progression where an overreaction by authorities provokes a reaction.
- Talks about how individuals join movements not just for grievances but also moral reasons.
- Outlines the cycle of escalation and unintended incidents leading to splits within the movement.
- Details the five elements that movements typically split into: leadership, soldiers, underground, auxiliary, and mass base.
- Defines each element with examples like soldiers being those who pick up arms.
- Mentions differences in terminology in older manuals where auxiliary and mass base were combined as sympathizers.
- Emphasizes the importance of understanding these dynamics for insight into global events and transitions.
- Gives a hypothetical example of how different groups like Green Berets or activists fit into these elements.
- Concludes with the idea that this knowledge is valuable for understanding events both domestically and internationally.

### Quotes

- "It's almost never intended on the part of the establishment, on the part of the government. They don't mean to do it, but they do."
- "A lot of times it's kind of imperceptible to the people inside of it, until it's not."
- "Those who become soldiers, these are typically people without families. These are typically people who maybe they have a record."

### Oneliner

Beau explains the progression of movements from discontent to intensity, detailing the split into five elements and the roles within, offering insights into global events.

### Audience

Activists, organizers, researchers

### On-the-ground actions from transcript

- Analyze and understand the dynamics of movements and protests (suggested)
- Foster understanding of global events and transitions (suggested)

### Whats missing in summary

Insights into the importance of understanding movement dynamics and transitions for broader context and global events.

### Tags

#Protests #MovementDynamics #GlobalEvents #Leadership #Community


## Transcript
Well howdy there internet people, it's Bo again.
Because of the last few videos, I've gotten a lot of questions.
You know, we've been talking about the manuals and we've been talking about what it's like
to try to manage a situation like this, where you have a bunch of people in the streets
and we're talking about best practices.
People have been asking what it's like from the other side, because a lot of people don't
see how easy it is to transition from a group of people with a lot of discontent, who are
just out in the streets, into something a little more intense.
And that's what we're going to talk about today.
We're going to talk about it from the inside, what it looks like if you are part of that
movement, so you can understand how it occurs and how that transition happens, because a
lot of times it's kind of imperceptible to the people inside of it, until it's not.
Historically, every one of these situations is different, but they're all the same.
The specifics are different, but the general storyline is pretty much always the same.
There's a bunch of people in the street, there's an overreaction, the powers that be, the establishment
in that country uses the iron fist, and it provokes a reaction.
More people join, and these people are angrier.
These people, the people that show up after the iron fist gets deployed, it's counterintuitive,
but they tend to be more extreme than the people who were there in the beginning, because
they're not showing up for grievance.
They're showing up because of a moral injury.
They saw something that they just could not stand, so then they show up and they join.
What happens at that point is that things escalate, and more than likely, if the government
is set on that track, if the establishment in the country is set on using the iron fist,
they continue.
And eventually, there's an accident.
There's an overreaction.
It's almost never anything intended.
Historically speaking, it's never something that is planned out.
It's something, if you're in the United States, think of how our revolution started, the Boston
Massacre, right?
If you're in the UK, think of Bloody Sunday.
It is what it is.
That's how it happens.
It's almost never intended on the part of the establishment, on the part of the government.
They don't mean to do it, but they do.
Something happens and it spirals out of control.
Once that happens, the movement in the street splits, but not really.
It's still unified, but it divides into five elements.
Those five elements are leadership, soldiers, underground, auxiliary, and mass base.
Now, this is current terminology.
If you've got an older edition of one of these manuals, I'll fill you in.
The leaders are exactly what they sound like.
They don't do much.
They provide ideological guidance and loose objectives.
That's it.
The soldiers are anybody.
This can be defined as anybody who picks up arms, okay?
Anybody.
And it doesn't matter what kind.
Those are the soldiers.
Different editions of these manuals use different terms.
So when you're looking at it, don't look for the term.
Look for the definition where it's talking about taking up arms.
Those are soldiers.
This varies from year to year because different terms come in and out of vogue.
I think the current term they use is just combatant.
Then you have the underground.
Now the underground are people who help with logistics, transport, housing, propaganda
operations, okay?
They don't actually pick anything up, though.
They're not out there.
They're the logistical side of it.
Then you have auxiliaries.
Auxiliaries are people who never engage in any form of actual operation, but maybe they
provide information that they saw accidentally.
A good example would be like a shepherd who happens to be near a base and sees something
and tells them.
They may run courier operations.
Most of the time, these people are less ideologically motivated and more motivated by money or ego.
They want to be involved, but they don't want to risk anything.
Then you have mass base, which are just the average people who agree with them but don't
do anything.
They don't get involved in any way, shape, or form other than the normal functions of
government.
In the United States, these would be people who signed petitions and called their representatives
while all of this was going on.
That's the mass base.
If you have a much older version of one of these manuals, you only have four.
You only have four elements.
Auxiliary and mass base used to be one thing called sympathizers.
And then it split.
They split it into people that took some action to directly benefit that movement and those
who didn't.
So if yours is a little different, that's why.
This happened 15 years ago.
So if you've got one of the older ones, you may not have five elements.
You may have four.
And some of them, I think they have different terms for auxiliary as well or for underground.
But those are the base definitions.
So that movement that was in the street, it splits and it turns into the people decide
where they fit in best in these five categories.
They still stay together in the sense of they're in pursuit of the same goal as typically outlined
by whoever the leadership becomes.
But they're different, obviously.
They have different levels of commitment.
They have different motivations.
And they also tend to be different as far as their station in life.
Those who become soldiers, these are typically people without families.
These are typically people who maybe they have a record.
They don't have anything tying them down.
Because in this situation, once it progresses to this level, you don't go to the event and
then go home.
You're mobile until it's over.
And as we discussed yesterday, these things take a lot of time.
Minimum commitment, if it goes to something like this, is six years.
And that's the minimum.
They can go on a whole lot longer, as we've seen recently.
So this is the organizational structure in the sense of the different types of people
involved.
They adopt different command structures, which could be based on cells, or they could try
to organize into a more regular military with rank and stuff like that.
It all depends on the culture of the country and the area that they're operating in.
In more rural areas, they tend to become more formalized.
In urban areas, they tend to just be small groups, tightly knit groups.
And they tend to operate independently.
They get ideas from the leadership, but they may not have any direct contact with them,
which makes it very hard for their opposition.
Now hopefully, you're never going to need this information, but it's important to know,
not necessarily for what's going on right now, but when normalcy returns and the United
States empire goes on about its business, this gives you a better understanding of what's
happening in these other countries and how those movements transitioned into what we
see when it finally hits our news.
A lot of these people, and Portland is a good example, right now you've got Green Berets,
Navy vets, Marines out in the streets.
Those will probably become the soldiers.
And you have those activists who were there from the beginning, they would probably become
the underground.
The moms, the wall of moms, they become the auxiliary.
Those people who show up and get their selfie taken, they're the mass base.
That's how it works.
So you can see a direct line and a direct transition.
Again, I don't necessarily think you're going to need this information here, but since people
are interested in it, and it will foster understanding in foreign events later, it seemed like a
good time to bring it up.
Anyway, it's just the thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}