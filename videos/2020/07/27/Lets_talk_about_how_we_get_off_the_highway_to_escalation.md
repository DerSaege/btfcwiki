---
title: Let's talk about how we get off the highway to escalation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xJ1BAh0ZbKw) |
| Published | 2020/07/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of getting off the current "highway" we're on, discussing different exits to take.
- He presents a strategy that, although harder and taking longer, has proven successful in the past.
- Beau explains the gravity of the situation through numbers, using the example of Syria to illustrate the potential loss if the current path continues.
- He stresses the importance of community action and starting now to create change rather than waiting for political figures like Biden.
- Beau advocates for building small community networks and making positive changes locally to gain political influence from the ground up.
- He warns against blindly following political figures and instead encourages grassroots movements for real change.
- Beau underlines the power of social networks and community organizing over violence in achieving a just society.
- He cautions against escalating situations into violence, advocating for defensive strategies in protests.
- Beau suggests using symbolic gestures like carrying the American flag in protests to gain wider public support and win the PR battle.
- He concludes by proposing the idea of building a new system rather than solely fighting against the existing one.

### Quotes

- "It's always bad."
- "Don't wait for him. It's going to take us to do it. Might as well start now."
- "Power to the people? That's how you achieve it. Social networks and cell phones, not guns."
- "Be the anvil."
- "Maybe we should just build the system that we want rather than trying to fight against a system that's been there."

### Oneliner

Beau stresses community action over waiting for politicians, advocating for grassroots movements to build a just society without violence.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Build small community networks and initiate positive changes now (suggested)
- Carry out local activism with friends to influence policy decisions (exemplified)
- Use social networks and community organizing to gain political clout (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the urgency to take action and build a new system rather than relying on existing political figures or waiting for change.

### Tags

#CommunityAction #GrassrootsMovements #PoliticalChange #SocialJustice #ProtestActions


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about how to get off the highway we're on.
There are exits and we're going to talk about a couple of them.
I'm going to give you a strategy that will work.
It's harder, it will take longer, but it's a strategy we've talked about on this channel
before and the reason I keep going back to it is because it works.
It's successful.
Since I started talking about this, the possibility of escalation, I've had a lot of people who
have been there, experienced it, tasted the chip, so to speak, and they told me I've been
doing a really bad job of conveying exactly how bad it is.
And they've given me suggestions on how to do that.
The problem is those suggestions do not fit this channel.
They do not fit this format.
This isn't a channel where I would go into descriptive detail.
It just doesn't fit.
But numbers, numbers can often put things in perspective.
So that's what we're going to do real quick.
I'm going to explain why we need to get off this highway.
Okay, everybody's familiar with Syria.
They had an escalation there.
Depending on who you ask, the number of lost is 384,000 to 586,000.
It's a wide range.
We're going to go in the middle of it, be around 480,000.
Then we're going to go a little lower.
We'll say 450,000, that way nobody can accuse me of inflating the numbers.
So 450,000.
That's three times the number that's been lost to what's happening now, and everybody's
kind of losing faith in the government.
450,000.
But that's only part of the story, because in 2010, the population there was 21 million.
The population in the United States is 15 times that.
So if you were to scale that and bring it to the United States on scale, the number
of lost would be 6,750,000.
The reality is it would be worse than that, and we'll get to why here in a minute.
But that's also only part of the story.
Because see, I said in 2010, the population was 21 million.
In 2018, it was 16 million.
Where'd all those extra people go?
Four million people displaced, lost their homes.
If you were to scale that, you would have 60 million people that had to leave their
homes.
So the total, 6.75 million gone, 60 million without homes.
That's the reality of it.
It's not a little thing.
It's a pretty big ordeal.
And the reality is, in the United States, it would be even worse, because we are a more
developed country.
We have different skill sets.
Most people in the United States cannot survive without a grocery store.
And most of those, they're not going to be open during this.
It's bad.
It's always bad.
So how do we get off the highway?
What do you want?
What do you want?
You're one of the people on the street.
You're one of the supporters.
What do you want?
You want a fair system.
You want an egalitarian system, a system that works for everybody.
A system where those on the bottom have a say.
So why are we waiting for Biden?
He's not going to fix all this.
Yeah.
Is he better than Trump?
Will he put a kinder, gentler face on it?
Will he make some reforms?
Yeah, sure.
Absolutely.
But he's still very much an establishment figure, and he's going to defend that system.
If you want that egalitarian society where everybody gets a fair shake, we've got to
start now.
Don't wait for him.
It's going to take us to do it.
Might as well start now.
Small community networks, small groups of you and your friends, active in your community,
actually making positive changes.
That gets noticed by the community.
That gives that network political clout, the ability to influence policy decisions, leadership
from the ground up, how it's supposed to be.
Eventually, the people in that community will come to rely on that community network more
than they do the establishment.
There you go.
Who has the real power?
You want power to the people?
That's how you achieve it.
Social networks and cell phones, not guns.
That's what would work.
And if you want that society, I would suggest it might be a bad idea to take those who are
most willing, most committed, those who would put themselves on the line and thrust them
into a situation where hierarchy and following orders is what keeps them alive.
That might undermine those beliefs.
It's hard to make the transition back from that.
If that happens, there's no guarantee that that concept, that philosophy, that ideology
would even make it through it.
Might morph into something else.
You can normally see the end of something in the beginning of it.
And if the beginning of it is top-down hierarchy using force, that's what the end's going
to be.
That's what the end's going to be.
And that's not what we want, right?
We want a just society.
We want a fair society.
Something where everybody gets a say.
We have to build it.
You cannot reform the establishment systems that exist into that.
You have to build another power structure at the same time.
Now the other side to this is people are saying, are you saying get out of the street?
No!
You have a right to assemble.
You have a right to protest.
You have a right to ask for redress of grievances.
What I'm saying is do not cross that line into going on the offensive.
Stay on defense.
And I know there's somebody out there, military mind, you have to seize the initiative.
Not in this.
Not in this.
If it escalates, it's a PR campaign with violence, right?
It's about being on defense.
At the end of the day, the anvil always breaks the hammer.
Be the anvil.
It's a PR campaign.
There's no reason the violence has to go with it.
You know, there's something that's been mentioned over and over again, and I don't understand
why it's not happening.
If I told you that carrying a New York Yankees banner would get the American people behind
you when you're out there in the street, you'd probably do it.
You know, if there was bona fide evidence that that was true, you'd probably do it.
If you carry the American flag, it will go a long way in the PR fight.
And I know there's people, no, that's not what we want.
Represents nationalism.
Represents oppression.
I don't believe in it anymore because they messed me over when I got back from Iraq.
I don't, you know, it didn't keep its promises.
I get it.
Fine.
Fine.
You don't have to believe in it.
Okay?
But understand that if you're carrying that and some masked goon draws a bead on you and
that image gets out to the American people, they're going to fall behind you.
They're going to support you.
If one of those distraction devices catches an American flag on fire, who are the people
going to support?
It's a PR campaign, right?
And I know there's a lot of ideologues out there.
I'm not willing to make that compromise.
Then you are not ready for an escalation because once it escalates, it's nothing but compromise.
Moral compromise, ethical compromise, all the bad ones.
We got to get off this highway.
If you're troubled by that idea, you're not ready for it.
You're not ready for 6.75 million, 60 million displaced.
It's not a movie and it just gets worse and worse and worse.
And even after going through it, there's no guarantee that your side wins.
Maybe we should just build the system that we want rather than trying to fight against
a system that's been there.
Be to build a competing system.
Build one that we can be proud of, one that doesn't require compromise.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}