---
title: Let's talk about what happened in Florida in 8 days....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7QJFJsdgrRc) |
| Published | 2020/07/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor in Florida is forcing schools to reopen despite knowing it's a bad idea, following Trump's direction.
- In just eight days, cases involving children in Florida increased by 34%, with almost a thousand new cases daily.
- Hospitalizations also increased by 23% in the same period.
- Research from South Korea shows that children transmit the virus as easily as adults.
- Parents were misled into thinking it's safe for kids to return to school, neglecting the risks.
- Beau urges parents to opt for distance learning or homeschooling if possible.
- Florida districts have homeschooling-friendly policies that parents can utilize.
- The reopening of schools will further increase cases and pose a significant risk to public health.
- Beau criticizes the prioritization of the economy over the well-being of children and families.
- Despite warnings, some prioritize economic reasons for reopening schools, neglecting safety concerns.
- Beau stresses that there is no valid reason to reopen schools and that it's a dangerous decision.
- He calls out the disregard for facts and safety in favor of political allegiance.
- Teachers, school administrators, and data all indicate that reopening schools is ill-advised.
- Beau ends by questioning the logic behind prioritizing reopening schools against all evidence.

### Quotes

- "Kids transmit just as easily."
- "There's no reason to reopen these schools."
- "The numbers say it's a bad idea."
- "This is a bad idea."
- "But hey, the guy who didn't know how to put on his mask, well, he says it's
  important."

### Oneliner

Governor in Florida pushes to reopen schools against advice, risking children's
health and economy while prioritizing politics over safety.

### Audience

Parents, educators, concerned citizens

### On-the-ground actions from transcript

- Opt for distance learning or homeschooling if feasible (suggested)
- Research homeschooling policies in Florida districts and take necessary steps to enroll children (implied)

### Whats missing in summary

The emotional impact and urgency of protecting children and communities by making informed and responsible decisions.

### Tags

#Florida #Schools #COVID19 #Children #Safety #Economy #Government


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So we talked about eight minutes in Florida, now we're going to talk about eight days in
Florida because there have been some developments.
If you are unaware of the situation here, the governor is forcing the schools to reopen,
even those who know it's a bad idea, mandating they reopen.
He's doing this because Trump told him to.
Now Trump has since softened on this a little bit, but not really.
He goes back and forth.
He knows he made a mistake, but he's not enough of a leader to admit it.
So while every once in a while you get a statement from him that seems to say, oh, maybe we shouldn't
do this, he still pushes for it.
The governor is marching in lockstep.
So in two weeks, unless something changes, the schools are going to open in a lot of
districts.
So what has happened?
What's happened in the last eight days?
We have gone from 23,170 cases involving children, starting from the very beginning, the very
beginning to 31,150, a 34% increase in eight days, 7,980 cases, almost a thousand cases
a day.
That first number, 23,000, was from the very beginning, 8,008 days.
This is because there was a messaging change, the idea that we're returning to normal.
They're going to have to go back to school so parents are relaxed.
Yeah go play.
Let's go back to school shopping.
Eight days.
Eight days we had that increase.
And I know there are people saying that, well, yeah, so they get it, but it doesn't really
impact them.
It's not like they get, you know, really sick or anything.
In the same eight days, we have had a 23% increase in hospitalizations.
It's not true.
It will impact them.
The latest research out of South Korea says that children transmit just as easily as adults.
This happened because parents trusted government officials who have a long record of showing
they don't care about the kids.
They do not care.
They care about the economy.
We're going to get to that aspect of it in a minute.
In the state of Florida, a lot of districts have distance learning capabilities.
Use them.
Use them.
If your district doesn't, you can't get your kid enrolled.
In the state of Florida, all it takes is a letter to the superintendent to pull your
kid out of school and start homeschooling.
There are some requirements after that.
They involve keeping a binder of your child's educational activities and stuff like that.
It's not difficult.
Look into it.
It is not hard.
The requirements are not hard.
We are a very homeschool-friendly state.
Who knew?
Because I think that may be the best idea.
This is with the schools still closed.
This is just with a change in messaging.
Once the governor forces those schools to reopen and kids are back, these numbers are
going to go up.
They're all going to say, oh, nobody could have predicted this.
Here's the warning sign.
This is the flashing red light that this is a really bad idea.
Since nobody really cares about the kids, let's just go ahead and go to what people
care about the economy.
Kids transmit just as easily.
So kids in these petri dishes of schools will be taking this home to their parents, who
will then spread it around their workplace.
The Florida economy will grind to a halt.
We are terrorism.
How many people go on vacation at a leper colony?
The governor is going to destroy the state's children and its economy in one fell swoop.
Sadly, people seem more concerned about the economy.
I know there are some people saying, for economic reasons, kids have to go back to school for
the parents.
That's a tough decision to have to make.
That is a tough decision to have to make.
Maybe if you look through the homeschooling stuff, you can work out some other alternative.
They do seem relatively simple.
Seems easy.
And then there's the idea that kids have to go back to school because that's the only
meal they're going to get.
And that sounds great, except the people saying this have absolutely no interest in solving
the issue of poverty.
They just want to push it off on the school.
And I'd like to point out that in the guidance from the CDC, it says that kids should bring
their own food.
There is no reason to reopen these schools.
It's a bad idea.
They're going to pretend like they didn't know.
Here's the warning sign.
We're not even open yet.
Just a change in messaging altered the outcome.
This is not a partisan thing.
It's not a partisan thing unless you ignore the facts on the ground.
Unless your political party has indoctrinated you to the point where you will disbelieve
what you know to be true.
You're going to believe the party over your own eyes.
And you're going to risk your kids over it.
I don't know, bury them with a stuffed elephant, I guess.
This is a bad idea.
There's no reason to reopen these schools.
The teachers don't want it.
The school administrators don't want it.
The numbers say it's a bad idea.
But hey, the guy who didn't know how to put on his mask, well, he says it's important.
Let's go with that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}