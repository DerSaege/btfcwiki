---
title: Let's talk about what a climate study can teach us about the media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RouTiIyBsI0) |
| Published | 2020/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how a climate study can reveal media manipulation or reinforcement of narratives.
- Notes the confusion surrounding a new climate study due to media reporting.
- Describes the updated findings of the study, indicating a more precise temperature range of 2.6 to 3.9 degrees Celsius.
- Points out how news outlets can spin the study to fit different narratives, either catastrophic or downplaying the impact.
- Emphasizes that the 2.6 to 3.9 degrees Celsius range is the most likely scenario at a 66% chance.
- Acknowledges that the outcomes are based on the assumption that action is taken to combat climate change.
- Mentions the potential consequences of a three-degree temperature increase, such as environmental disasters.
- Advises the audience to verify information themselves rather than solely relying on media interpretations.
- States that the study results, while not optimistic, should not alter climate policy goals of staying below 2 degrees Celsius.
- Encourages maintaining the original climate policy goal despite its seeming unlikelihood.

### Quotes

- "They can paint a picture that is factually accurate, but it's not true."
- "If it's something you care about, if it's something that's important to you, make sure you go look at it."
- "At the end of the day, this is not great news, but it's also not going to change climate policy, really."

### Oneliner

Beau sheds light on media manipulation in reporting climate studies and urges personal verification of critical information to maintain focus on climate policy goals.

### Audience

Climate activists, Media consumers

### On-the-ground actions from transcript

- Verify information yourself (exemplified)

### What's missing in summary

The full transcript provides a detailed analysis of how media can skew climate study results and the importance of verifying information independently.


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
what a climate study can teach us about how the media can manufacture a narrative or back
up a narrative that they've already put in place. We're going to do this because I had
a couple people ask me about a new climate study and they can't figure out if it's good
news or bad news because of the reporting. So I'm like, okay. So I got to look at the
study. The study's really clear. The abstract, well, not the abstract. The abstract is actually
really confusing. The plain language version down at the bottom, that's really clear. Basically,
since 1979, we've had a range of how much we expect to warm. 1.5 to 4.5 degrees Celsius.
That's been the expected range. And this is based on how much carbon dioxide is in the
air and it doubling. A lot's happened since 1979. They have new methodology. They have
a more accurate range. The new range is 2.6 to 3.9 degrees Celsius. For Americans, that's
like 5 to 7 degrees-ish. So if you're a news outlet and you're wanting to paint a picture
that is favorable to your side, you can report on this by saying, hey, we're all going to
fry. The best case scenarios, they're all off the table. Or you can say climate alarmists,
all those doomsday scenarios, we're not going to go extinct. Both of these are kind of true.
Now it should be noted, this is the most likely range at 66%. There's a 66% chance it'll fall
within this range. The further away from the range you get, the less of a chance. So you
can paint this as good news or bad news and it's accurate in the sense of that statement
is true. However, it doesn't tell the whole story. It doesn't even come close. And when
I looked at some of the reporting, most of them chose good news or bad news and then
they mentioned the other part. But it was kind of an afterthought. It certainly wasn't
part of the headline. So what is it? Is it good news or is it bad news? First thing to
note is this is assuming we act and we do something about climate change. The other
part to this is 2.6 to 3.9 degrees. Three degrees, that's a disaster movie. That's the
Colorado River running dry at times, sections of the planet becoming uninhabitable. It's
not good. But hey, we probably won't go extinct, which, I mean, that's always good, you know.
But again, that's assuming we act. At the end of the day, the real lesson here is a
lot like that video earlier. If you get a quote, just a small quote from the media or
they're interpreting a study for you, make sure you go look at it. If it's something
you care about, if it's something that's important to you, make sure you go look at it because
they can paint a picture that is factually accurate, but it's not true. And that's what
a lot of outlets did with this. At the end of the day, this is not great news, but it's
also not going to change climate policy, really. The goal was to keep it below 2 degrees. That
seems really unlikely, but it should still be the goal, you know. We don't want to bump
up the goal simply because it's less likely to happen. That should be what we're striving
for because even at 2 degrees, we're going to fill it. Anyway, it's just a thought. Y'all
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}