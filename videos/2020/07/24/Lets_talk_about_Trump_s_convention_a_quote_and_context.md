---
title: Let's talk about Trump's convention, a quote, and context....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qQCJ16XXG6M) |
| Published | 2020/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's convention in Jacksonville was moved virtually due to crowd limitations.
- Simultaneously, the administration pushes for schools to reopen, focusing on a quote about opening schools.
- The quote implies the critical importance of opening schools but overlooks the associated guidelines and resources.
- Guidelines suggest measures like regular hand washing, wearing masks, and disinfecting classrooms and buses.
- Schools are expected to implement social distancing, provide supplies, and broadcast health announcements.
- The guidelines propose unrealistic expectations, such as maintaining distance in classrooms and buses.
- Students are required to bring their own food and water, contradicting the issue of student poverty.
- Beau questions the feasibility of the guidelines and expresses concerns about the lack of leadership in ensuring safety.
- He criticizes the reliance on schools for addressing poverty and stresses the need for innovation in the education system.
- Beau calls for a new approach to education that does not burden teachers and administrators with unrealistic demands.

### Quotes

- "This is going to be really hard."
- "It's time to innovate."
- "We need something new."

### Oneliner

Trump's convention went virtual, while guidelines push for school reopenings with unrealistic expectations, prompting Beau to call for innovation in the education system.

### Audience

Educators, parents, activists

### On-the-ground actions from transcript

- Donate supplies like soap, hand sanitizer, and disinfectant wipes to schools (implied)
- Advocate for adequate funding for schools and support teachers (implied)
- Collaborate with local communities to address educational challenges (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the impractical guidelines for reopening schools amidst the COVID-19 pandemic, calling for a critical reevaluation of the education system and leadership in ensuring safety and innovation.

### Tags

#Trump #SchoolReopenings #EducationSystem #Safety #Innovation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump's convention, you know the one in Jacksonville,
Republican National Convention, and we're going to talk about some guidelines that got
released that aren't being talked about because a quote came out with them and everybody's
looking at the first sentence of the quote, the release.
So the convention in Jacksonville, it's been canned.
Now if you don't know the full back story, this was going to happen in North Carolina.
They moved it because they didn't want big crowds up there so they moved it to Jacksonville.
Then they realized it was probably not a good idea to have big crowds so they limited crowd
size and now it's just scrapped all together and it's going to be done virtually.
Cool.
Trump says he's doing it to send the right message, you know, to kind of convey some
leadership.
Cool.
I'm with that.
And that sounds good.
The problem is at the exact same time this is happening, his administration is still
pushing for all the schools to reopen.
And a quote came out today and everybody's focusing on the first sentence of that quote.
It is critically important for our public health to open schools this fall.
That's pretty conclusive.
It is critically important and this is the quote that's going around.
Let's add some context to this because there's more to the quote.
The CDC resources released today will help parents, teachers, and administrators make
practical, safety-focused decisions as this school year begins.
Okay.
So what do those resources say?
It's critically important, but you need to look at the resources and those resources
give you guidelines.
Hand washing should occur pretty much all the time for no less than 20 seconds each
for each student.
I'm sure there's going to be enough sinks and everything for that.
That's not going to be a problem at all.
You know, we have been underfunding teachers for years.
They will figure it out.
They'll build sinks.
You know, we don't need to worry about this.
They got it.
They need to have adequate supplies of soap and hand sanitizer because, you know, hand
sanitizer is not in short supply.
Again, maybe they can do a science project and make their own.
They'll adapt.
It's what teachers do.
They're all supposed to wear masks.
We got grown adults who won't wear their masks and when they don't and they throw a temper
tantrum and end up on YouTube, what do we say?
They're acting like a child.
But we expect the actual children to do what they're supposed to.
They need other supplies in the classroom.
Hand sanitizer again, paper towels, tissue, disinfectant wipes.
You know, because those again, it's not like those are in short supply.
I'm sure there will be no problem getting them once every school in the country is trying
to get their hands on them.
But again, maybe they can just make their own.
They'll figure something out.
No worries.
This is stuff that teachers need under normal circumstances.
It has to be donated by parents.
And now they're going to need a whole lot more.
The schools are supposed to broadcast regular announcements on stopping the spread, like
some dystopian science fiction novel.
And in the same guidelines, they're supposed to take time to remind students and give them
skills to not think about it.
I don't know how that's going to work.
They're supposed to disinfect playgrounds.
I'm assuming with Lysol, because that's everywhere.
I'm sure they can figure something out.
Janitors are resourceful too, they'll figure it out.
No reason to worry.
They also need to disinfect the buses.
Now I don't know how they're going to do this as children are using them.
These are shared things, but that's another thing.
While they're talking about disinfecting this stuff, they're also saying you can't use anything
that is communal and there's supposed to be no sharing at all, like anything.
You're supposed to open windows in the classrooms, so you get outside air in.
However, you can't do that if a kid has asthma.
Here's one.
Students should bring their own water and food.
One of the big things this entire time, one of the big talking points was, you know, this
may be the only meal a student gets.
And rather than addressing the actual issue of poverty, we rely on the school.
But now, by the guidelines, no, they need to bring their own.
Kind of undercuts that talking point a lot.
They're supposed to social distance, six feet at all times.
I don't know how that's supposed to work either.
There's not enough room in a classroom to do that with the number of kids that we have
packed into classrooms because they're underfunded.
So we're going to have to open up more classrooms, build more schools, hire more teachers, because
it's not like there's a teacher shortage or anything.
They're supposed to find the money to put up plastic dividers at sinks and anywhere
that students may be near each other.
They're supposed to stay six feet apart even in lines.
And I don't know if you've ever watched children in a line, but that seems ambitious.
Supposed to skip rows on buses to maintain distance on buses, which means we need more
buses and more bus drivers and more money because we're going to run the same routes
more often.
We have to close the dining halls.
I guess that kind of goes without saying because kids are supposed to bring their own food.
There's going to be daily health checks.
Teachers also need a medical degree now.
They'll be doing that.
I guess the school nurse could process every student that comes in.
And they need backup staffing for the teachers for the inevitable occasion when they get
sick.
Now this is a huge list of stuff.
Seems pretty impractical.
This is a fraction of one of the pages.
There are more.
This was just the stuff for school administrators and not even all of that.
This doesn't really seem feasible.
It doesn't seem feasible.
Now I'm sure that the state government and the county government, they will be completely
honest with everybody about how well they're keeping people safe.
I can't think of any situation in which a state government or county government just
didn't tell the truth and a whole bunch of people got sick.
You know, I actually, when I said that I was specifically thinking of Flint and now that
I've said it out loud I realize that that applies to a whole bunch of stuff.
They actually have a really bad track record with this.
But don't worry, it'll be different this time with no money and higher requirements.
Even though they weren't able to do the base requirements a lot of times.
Now in this same release that that quote got pulled from, there's high risk, moderate risk,
and low risk.
High risk is business as usual.
Moderate risk is this, what I just described.
That's moderate risk and all the other stuff that I didn't mention.
This is moderate risk.
What is lowest risk?
Students and teachers engage in virtual only classes, activities, and events.
That quote, only using that first line, that's not bad journalism.
That's straight up propaganda.
They had this.
It's all part of the same thing, but they're only using that first quote.
This doesn't seem feasible to me, not realistically.
I'm not a teacher, but I know a little bit about logistics.
This is going to be really hard.
Our system, our school system, it doesn't work.
It doesn't work.
We're trying to force this system to remain open when in reality it wasn't that great
to begin with.
Necessity is at the mother of invention.
We need something else.
It's time to innovate.
It's time to move past a system that has failed and that the failure is not the fault of the
teachers or the administrators.
It's our fault for not forcing the politicians to fund them.
We need something new.
We need something new.
This doesn't seem like it's going to hold because we're not getting the leadership that
we actually need.
Even when he pretends to lead, this is going on behind the scenes.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}