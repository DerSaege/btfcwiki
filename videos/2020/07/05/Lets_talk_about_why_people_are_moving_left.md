---
title: Let's talk about why people are moving left....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2g0qUxgwHmo) |
| Published | 2020/07/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US political spectrum is skewed to the right, with even Democrats considered center-right globally.
- Leftism, defined as rejecting capitalism, is gaining traction in the US.
- There is a surge in black leftists, challenging traditional right-wing ideologies.
- White privilege denial contributes to pushing black Americans towards leftist ideologies.
- Issues of race and class in the US prompt people to seek egalitarian philosophies.
- Black leftists are advocating for systemic changes that impact them daily.
- The economic system is being challenged by those seeking systemic change.
- Black leftists are willing to fight for all lives, leading to a surge in leftists across different groups.
- Rejecting capitalism is a common trend among those advocating for environmental protection.
- The current form of capitalism in the US fails to lift black Americans out of poverty.

### Quotes

- "White privilege denial contributes to pushing black Americans towards leftist ideologies."
- "Black leftists are willing to fight for all lives."
- "Rejecting capitalism is a common trend among those advocating for environmental protection."

### Oneliner

Why the surge in black leftists and rejection of capitalism stems from challenging systemic issues of race and class in the US, pushing for egalitarian philosophies and systemic changes.

### Audience

Activists, Political Analysts

### On-the-ground actions from transcript

- Challenge systemic issues of race and class by advocating for egalitarian philosophies and systemic changes (implied).
- Stand up against white privilege denial and contribute to pushing for leftist ideologies (implied).
- Advocate for environmental protection and challenge capitalism by rejecting its harmful practices (implied).

### Whats missing in summary

Full context and depth on the history and reasons behind the surge in black leftists and rejection of capitalism.

### Tags

#USPolitics #Leftism #BlackAmericans #SystemicChange #Capitalism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about two questions that I've gotten from opposite ends of the
political spectrum, but they're the same question at the end of the day.
Boils down to why.
Why is this happening?
We know that it is happening, but why is it happening?
Before we start, quick recap.
We've talked about it in depth on this channel before, but for those who missed it, in the
US, our normal spectrum of political debate is so far to the right that even those who
we call left in the US, the Democrats, are center right anywhere else in the world.
We're that far to the right in the US.
We don't have a lot of real leftists in politics today.
It's a gross oversimplification, but for the purposes and context of today, you can define
leftism as rejecting capitalism.
That's where it starts.
That's where the line is between left and right.
So we now have large numbers of people who are willing to do that in the US.
And specifically, we have a lot of black folk who are now leftists.
Now a white leftist sent me a message.
He's like, I've met more black leftists in the last six months than I have in my entire
life.
What is going on?
Now to him, this is exciting news, but he doesn't know why it's happening.
Right wing question, why are all of them becoming Marxist, communist, Leninist, socialist, whatever
ist, that they want to assign to them?
And by them, of course, black Americans.
So now they're doubly scary.
Not just are they black, they're this scary left wing ideology that he doesn't understand.
But again, it's why.
First thing I want to point out is that black leftism has a long history in this country.
A lot of the movements that get associated with black America as far as the civil rights
period, they were leftists.
But why is it happening now?
Why is there this surge?
We're going to have to ask them to get a solid answer, but outside observer here, right wing
talking points about race.
The right wing trolls that talk about race, that's what has greatly contributed to that
move to move them left past Democrats into actual leftist territory.
Think about it.
Let's say white privilege.
What is the number one response from a white person who wants to turn down the volume on
that message?
You know, I work 40 hours a week and I'm still dirt floor poor.
Proof white privilege doesn't exist.
Checkmate.
The black person that hears this does not suddenly go, well, white dude's poor.
I must have imagined everything I've ever experienced in my entire life.
They have a different realization.
They think more along the lines of, so you work 40 hours a week, you're still dirt floor
poor and you're okay with that?
So we have a race issue and a class issue in the United States.
So then they start looking for egalitarian philosophies, which move them left.
No gender, race, or class.
That's how a lot of them end up there.
They're already looking for deep systemic change to address issues that impact them
daily.
You know, when you're talking about, well, we got to change law enforcement, the entire
criminal justice system, the way we vote, banking, all of this stuff that negatively
impacts us.
Changing the economic system isn't really a lot to add, you know, in the grand scheme
of things, it is what it is.
At the end of the day, you're seeing more black leftists because they hear stuff like
that and they're willing to do the fighting for white folk.
I guess they believe all lives matter.
I think that's where it's coming from.
And then the surge in white leftists and all of these other groups, I actually think it
has a lot to do with the same thing.
The talking points on healthcare.
Well you should just work more.
I was working 40 hours a week.
I got sick, now I can't afford my insulin.
Well you know, you just need to work more.
Ties to the economic system.
So they reject capitalism.
They move left.
Environmentalist, the person who just wanted you to recycle.
Well no, we need to save the economy, that's why we have to do this.
So we need to do all this environmental degradation.
Well if it's the earth or capitalism that has to go, capitalism is the one that has
to go for them and they move left.
This is especially true about the form of capitalism we have in the United States.
You know, people want black Americans to turn to capitalism to lift them out of poverty.
Why would they do that when every example that they're given from white Americans proves
that capitalism hasn't done anything for them?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}