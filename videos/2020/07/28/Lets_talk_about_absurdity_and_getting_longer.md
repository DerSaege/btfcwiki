---
title: Let's talk about absurdity and getting longer....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YfdQTDhWCQs) |
| Published | 2020/07/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Released a video on absurdity, then saw today's headlines reflecting absurdity of the administration.
- Trump Jr. had his Twitter suspended due to false healthcare tweets.
- Attorney General, overseeing mass incarceration, complains about unfair prison sentences.
- Acknowledges that US prison sentences are too long but makes no effort to address the issue.
- Describes tyranny as when the government can do things you can't, hinting at double standards.
- Teases upcoming long-format content where he will deep dive into specific topics.
- Long-format content won't replace daily videos but will be additional.
- Encourages watching his previous video for a laugh amidst current absurd events.

### Quotes

- "US prison sentences are far too long."
- "You know you're under tyranny when the government is able to do things you can't."
- "If you want to laugh instead of cry at today's events, go watch that video I released last night."

### Oneliner

Beau talks about the absurdity of the administration, from Trump Jr.'s Twitter suspension to the Attorney General's complaints about prison sentences, hinting at double standards and teasing upcoming long-format content.

### Audience

Viewers

### On-the-ground actions from transcript

- Watch Beau's video for a humorous take on current events (suggested).
- Stay tuned for Beau's upcoming long-format content (suggested).

### Whats missing in summary

The full video provides a deeper exploration of the absurdity in current events and offers insight into the double standards within the administration.

### Tags

#Absurdity #TrumpAdministration #DoubleStandards #LongFormatContent #PoliticalSatire


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about absurdity.
Late last night I released a video that I thought was just over the top absurd.
And then I looked at today's headlines.
This administration has devolved to the point where people who make jokes by being ridiculously
absurd can no longer do so because the administration with no sense of humor and no hint of
irony will do something more absurd within the next 24 hours.
Donald Trump Jr. had his Twitter taken away because of, let's just say, some less than
scientific tweets that he chose to make about healthcare in the middle of this.
That should be an issue.
This is a man that has untold amounts of influence within this administration and was tweeting
out something so false, so absurd, that Twitter suspended his ability to tweet.
It was that bad.
That should be a sign.
At the same time, you have the Attorney General, who is in charge of the US's mass incarceration
program, lamenting unfair prison sentences.
You can't make this stuff up.
This entire thing with Stone is unique in the sense that on some level, they're right.
They're right.
US prison sentences are far too long.
At the same time, they're making no effort to address the issue.
They just want their friends, their cronies, their buddies exempted from it.
One of the handy definitions of tyranny, an informal one, you know you're under tyranny
when the government is able to do things you can't.
When there's two sets of rules, that certainly seems to apply.
Now we've been teasing long format content for a while.
That will be coming very soon.
If you see a video starting with, let's really talk about subject whatever, that's what it
is.
The way these are going to be formatted, I think, we'll see once we actually start recording,
is the first segment will be a video just like this.
So if you don't want to stick around for the longer content, you don't really miss anything.
You get a good general overview.
And then there will be a break, and then there will be a deep dive into one element of that
video.
It's very likely that you'll start seeing these appear occasionally in the next few
days.
Now it's not going to replace the channel.
We're still going to do these videos on a daily basis the way we always have.
This is going to be additional content.
We have other long format stuff coming as well.
They will be significantly different, so different that they may end up on their own channel.
They may be separated from this.
We'll see how that develops as it goes along.
If you want to laugh instead of cry at today's events, go watch that video I released last
night and realize that our real world is more absurd than anything people who like satire
can dream up.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}