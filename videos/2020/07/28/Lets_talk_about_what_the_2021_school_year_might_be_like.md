---
title: Let's talk about what the 2021 school year might be like....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QXt6XzjwBUI) |
| Published | 2020/07/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces themselves as the instructor for the 2021 school year due to a lack of teachers in the school district.
- Mentions being on loan from Fort Benning, Georgia, through a Department of Defense initiative.
- Expresses wearing a whimsical mask to appear approachable but warns students to stay six feet away due to being "stinky-hand bags of germs."
- Informs students that they will be issued either a surgical mask or a P100 respirator, with politicians' children receiving the latter.
- Emphasizes the importance of following safety procedures like handwashing and not sharing food due to last year's incidents.
- Warns students that one of them may carry something home to their parents if safety protocols are not followed.
- Mentions the availability of forms for non-familial foster care and DNRs in the office.
- Instructs students to keep personal belongings, like a box of crayons labeled with their names, to themselves.
- Concludes the safety procedure meeting for second grade students and asks if there are any questions.

### Quotes

- "One of you will carry something home to your parents."
- "No birthdays."
- "Welcome to second grade."

### Oneliner

Beau instructs second grade students on safety procedures, warns about consequences of not following protocols, and limits interactions like sharing food or celebrating birthdays.

### Audience

Parents, Teachers, School Administrators

### On-the-ground actions from transcript

- Keep personal belongings like food and crayons to yourself (implied)
- Follow safety procedures like handwashing and wearing masks (implied)

### Whats missing in summary

The detailed tone and expressions used by Beau while addressing the students can best be understood by watching the full transcript. 

### Tags

#School #SafetyProcedures #DepartmentOfDefense #Instructor #Children


## Transcript
Well, howdy there school children.
Welcome to the 2021 school year.
I will be your instructor this year.
I am on loan via Department of Defense initiative
from Fort Benning, Georgia.
I am here due to the unexpected lack of teachers
in your school district.
The Department of Defense agreed to this,
seeing it as a good opportunity to reach out to young people,
especially since that angry young congresswoman
banned us from recruiting at these locations.
We're going to begin the school year
by going through some basic safety procedures.
You may have noticed I am wearing a mask.
As advised by my command, it is a whimsical mask
to show that I am approachable and you can talk to me.
By approachable, I mean that you little soft-skulled,
stinky-hand bags of germs
better stay six feet away from me at all times.
You will stay six feet away from each other at all times.
You will also be issued a mask.
It will either be a surgical mask or a P100 respirator.
Most of you will receive a surgical mask.
The P100 respirators are reserved
for the children of politicians.
Do not read this aloud.
Most of them will be attending via distance learning.
You will be required to bring your own food and water.
The container it comes in will be labeled,
you will not share food.
There will be announcements made every hour on the hour
reminding you to sanitize your hands.
Currently, you'll be able to ignore those announcements.
We don't have any hand sanitizer.
All right, let's see.
Special considerations, birthdays.
On the off chance that one of you makes it to your birthday,
you will not be allowed to bring a cake.
Your parents cannot send anything.
We can't blow on food and then share it.
That's what happened last year.
That's why I'm here.
No birthdays.
Okay, you're gonna have to wash your hands all the time.
This is all common-sense stuff, kids.
All right, to impress upon you the importance
of following these safety procedures,
I would like you to look to your left.
Now look to your right.
One of you will carry something home to your parents.
On the off chance that that child is you,
the forms to be placed in non-familial foster care
are in the office, right next to the DNRs and DNI.
Look, go ahead and pick up these forms,
have them filled out ahead of time.
Save us all the trouble.
Okay, there's a box of crayons on your desk in front of you.
It's labeled with your name.
Can't share anything, all right?
Keep it to yourself.
All right, welcome to second grade.
That concludes our safety procedure meeting.
Any questions?

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}