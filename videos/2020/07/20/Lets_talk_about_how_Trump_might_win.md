---
title: Let's talk about how Trump might win....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RKUVcAo3Cvw) |
| Published | 2020/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's re-election strategy involves powerful displays of American symbols like Rushmore and the flag to make people comfortable and safe by showing control within his loyal circle.
- The campaign will heavily focus on supporting the troops, which will now include militarized law enforcement, as part of a law and order platform.
- National security issues, particularly related to China, will be a recurring theme throughout the campaign.
- Trump will intertwine religious imagery and rhetoric, reinforcing traditional family roles and creating a narrative dominated by strong men.
- He aims to undercut labor unions, especially teachers' unions, to protect corporate power.
- Trump's strategy involves identifying the opposition as the radical left and promising mass arrests and long prison sentences.
- He plans to suppress the vote through actions like attacking mail-in voting and shutting down polling places in areas favoring Democrats.
- Trump attempts to control the narrative by discrediting any opposition, labeling them as fake news and threatening lawsuits against them.
- The campaign strategy shows characteristics of fascism, such as powerful nationalism, disdain for human rights, identification of enemies, supremacy of the military, and rampant sexism.
- Corporate power is protected, while labor power is suppressed, with an overall focus on control, corruption, and fraudulent elections.

### Quotes

- "He's literally running a campaign, a referendum on fascism."
- "It's no longer a question of it can't happen here or even admitting that it can happen here. It is happening here right now."

### Oneliner

Trump's re-election strategy focuses on powerful nationalism, militarized law enforcement support, and suppressing opposition, resembling characteristics of fascism.

### Audience

Voters, Activists

### On-the-ground actions from transcript

- Contact local election officials to ensure fair access to voting (suggested)
- Join or support organizations advocating for voter rights and access (exemplified)
- Organize community efforts to increase voter turnout in areas facing suppression tactics (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of Trump's re-election strategy and the concerning similarities to fascism, urging vigilance and action against authoritarian tactics.

### Tags

#Trump #Reelection #Fascism #VoterRights #PoliticalStrategy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about how Trump plans to win,
what his re-election strategy is,
what are the things that he's doing so far
that we can kind of pick apart
and we can see his whole campaign coming now.
He's shown us enough.
And it's a good campaign, to be honest.
It's won in the past, doing it in this exact manner.
And it seems to be the template that he's following.
So we're going to go through it
so we can kind of know what to expect to see.
The first thing is going to be
powerful displays of America.
All right?
I mean, like all of the symbols of America,
of the United States, Rushmore,
the American flag all the time, that sort of thing.
That's going to be an overwhelming,
continuing theme throughout his election,
throughout his campaign.
And while he's doing this,
he wants to show that his family
and his close allies, well, they're in control.
They've got it.
This tight-knit circle where everybody's loyal to Trump.
They're in control.
It's going to make people feel safe, right?
So you're going to see Ivanka
and all of those people that are just
staunch Trump supporters,
you're going to see them constantly
throughout the campaign.
This is all going to be backed up
by a support the troops type of rhetoric.
But troop is going to be more broadly defined this time.
It's going to include militarized law enforcement as well.
We're already starting to see it
from some of his administration officials.
When they're talking about supporting our men and women
in uniform on the front lines,
and they're talking about the cops in Portland.
So that's going to be a big part of it.
The military will reign supreme, okay?
And he has to do this and broaden it to include the cops
because he's going to run on a law and order platform.
He's actually used those terms.
He's going to run on a law and order platform,
focused on crime and punishment.
It's going to be a big recurring theme.
He's going to do all of this at the same time
as he's hammering on national security issues
over and over and over again.
And it appears that he's going to focus on
China, China, China.
Everything is going to get attributed to China.
And that's going to be the main foreign opposition
as far as national security.
He's going to intertwine a lot of religious imagery
and rhetoric, okay?
We've heard him say they're going to destroy religion.
And we've got that photo op with him in the Bible.
This is going to be more common.
He's going to continue to do this.
And then he's going to have corporate leaders
back him up from out of nowhere, like Goya, okay?
You're going to see that.
Why are they going to do it?
Because corporate power is protected under Trump.
The deregulation benefits them.
So they're going to back him up so he stays in power.
This is the pro-Trump angle.
This is how we make people feel safe and comfortable
and know that he's in control.
That's one side of it.
The other side is, well, it wouldn't be Trump
if there wasn't opposition.
That's how he always markets himself.
Duality, good and bad, right?
Okay, so we have to identify the bad guys first.
And it appears that during this campaign,
it is going to be the radical left, which is good for him
because it's something that's so broadly defined,
nobody really knows what it is.
So that's going to benefit him in a lot of ways.
And because he's running on a law and order platform,
he's going to take that and kind of use that opposition,
the radical left, and he's going to promise mass arrests
and long prison sentences,
all of the stuff like he did with the monuments.
We can expect to see that type of rhetoric
continue all the way until November.
He's going to try to reinforce tradition,
traditional family roles.
He'll probably come out, well, he already has come out,
against family planning, stuff like that.
He's going to focus on creating an America
that is once again dominated by strong men.
That's the plan.
We've already seen it.
He has to undercut labor, and this is a tricky one for him,
but he's come up with a really unique way of doing it.
He has to undercut labor because his policies
are protecting corporate power,
so he has to find some way to kind of downplay labor.
The problem is a lot of these labor unions, they're powerful,
so he has to be smart about this one.
He can't come at them directly.
Now, he may say that he supports Union X or whatever.
The only union that you're going to see him actively support
through action is going to be the police union.
He's going to try to downplay the rest,
some of them powerful, like the teachers' union.
So how do you get people to turn against teachers?
Reopen the schools.
So then you have to choose between Trump and the teachers,
and then at the end of the day, you have to choose
between Trump and the teachers' union.
So we've got that battle pitted,
and then we're going to say,
well, we have to audit colleges and universities
because they may be teaching anti-American stuff.
Recent interview, he said that schools
were teaching students to hate America.
This is going to be a common theme.
It's something that's going to continue till November.
We're going to see these sorts of attacks
go against different labor unions, different labor groups,
and they may not always be quite as overt as this
and easy to see.
Now, anybody who calls him out on it, fake news.
He's got this down.
He has this down already.
Controlling mass media, he's got it.
Anybody who comes out against him,
whether it be a journalist or somebody just asking a question
or polls, they're fake news.
Disregard them.
Don't listen to them.
I'm going to tweet out the outlets that are good,
and anybody that goes too far,
I'm going to threaten them with a lawsuit.
So he's going to attempt to control the narrative that way
rather than the traditional way of creating photo ops, stuff
like that.
He's going to attempt to, I don't want to say censor,
because as of yet, he hasn't used the power of government
to do it.
But he's going to try to minimize
the effect of journalists.
And then the tricky part is he has to suppress the vote,
because he is unpopular, and he knows that.
His ideas are unpopular.
He has to make sure that the other side doesn't
turn out in big numbers.
So he's going to find a way to go after mail-in voting.
Republicans will shut down polling places
in areas that favor Democrats.
All this is already happening.
In fact, this entire list is already happening.
And then he did refuse to say that he would concede
the election if he lost.
So we may end up with, I don't want to say fraudulent,
but that's where we're at.
That's going to be the campaign.
There's no reason to worry about that last part
until it gets here, because after all, this is just
a 14-point campaign strategy.
It's literally fascism.
If you were listening to this, and you are a Trump supporter,
and you're thinking, hey, this sounds like a good idea.
I like all this.
This would all appeal to me.
I got some bad news for you.
14 characteristics of fascism.
Powerful and continuing nationalism.
Rushmore.
Disdain for recognition of human rights.
And actually, in the summary here,
it talks about long prison sentences and mass rapes.
Long prison sentences and mass rapes, all that stuff.
Identification of enemies and scapegoats.
Supremacy of the military.
Support the troops, buddy.
Rampant sexism.
That's reinforcing those traditional roles.
Controlled mass media.
Obsession with national security.
China, China, China.
Religion and government intertwined.
There is nothing more fascistic than using militarized police
to clear a demonstration so you can take
a picture holding a Bible.
Corporate power being protected.
Labor power being suppressed.
Disdain for intellectuals and the arts
going against the teachers.
Obsession with crime and punishment.
Law and order.
Rampant cronyism and corruption.
Like, nepotism definitely falls under cronyism.
Corruption, constantly going to your own hotels
on the taxpayer dime.
And then fraudulent elections through all the stuff
I named.
He's literally running a campaign,
a referendum on fascism.
This isn't theory anymore.
There's an older video of mine where I said, hey,
this is what he's going to do.
He's done it.
Here it is.
Plain as day, using the same list.
This is his campaign strategy.
If you are a Trump supporter, please
remember that there were a bunch of Germans
and a bunch of Italians, people who just loved their country.
They loved what they heard.
And they didn't see the danger.
They didn't understand what was happening
because they had fallen for somebody who
was charismatic on some level.
I hope it sounds familiar because it's happening right
now.
It's no longer a question of it can't happen here
or even admitting that it can happen here.
It is happening here right now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}