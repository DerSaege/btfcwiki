---
title: Let's talk about what my wife can teach the Gov of Missouri....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZwZBQyAl9iY) |
| Published | 2020/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau contrasts the actions of his wife, a nurse, with the governor of Missouri, Mike Parson, in handling the spread of COVID-19.
- His wife follows a strict routine of disinfecting and isolating herself after work to prevent spreading the virus at home.
- Beau's wife sends a text before coming home so he can prepare their children to avoid immediate contact with her.
- She changes clothes, showers, and disinfects all personal items upon arrival to ensure safety.
- Beau criticizes Governor Parson for downplaying the risks of COVID-19 spread in schools and homes.
- He accuses Parson of prioritizing Trump's image over public health by pushing for a premature return to normalcy.
- Beau questions how many children the governor is willing to sacrifice to protect Trump's ego.
- He warns against dangerous messaging that misleads people into thinking the pandemic is over.
- Beau links Florida's spike in cases to its governor's premature celebration and lack of protective measures.
- He condemns leaders who prioritize political allegiance over the well-being of their citizens and country.

### Quotes

- "He chose to protect Trump's fragile little ego."
- "These governors are selling out their duty, their citizens, and their country."
- "This man is not capable of running a house with a nurse in it, let alone a state."

### Oneliner

Beau illustrates the stark contrast between his wife's cautious approach to COVID-19 and Governor Parson's reckless disregard for public health, putting political allegiances over people.

### Audience

Missouri residents

### On-the-ground actions from transcript

- Follow strict disinfection and isolation protocols after potentially exposing yourself to COVID-19 (implied)
- Advocate for evidence-based public health measures in schools and communities (implied)

### Whats missing in summary

The full transcript provides a detailed account of the personal sacrifices and precautions individuals must take to prevent the spread of COVID-19, contrasting it with the irresponsible leadership prioritizing political interests over public health.

### Tags

#COVID-19 #PublicHealth #Governance #Leadership #Missouri


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about what the governor
of Missouri, Mike Parson, can learn from my wife, a nurse.
She's been dealing with what all nurses
have been dealing with lately,
because it's everywhere, because of people like the governor.
The most important thing that he can learn,
I think can be illustrated by her daily routine
when she gets off work.
Before she leaves the hospital, she sends me a text.
And that text is to let me know she's on her way,
so I can get prepared for what's about to happen.
More importantly, I can prepare the kids
for what's about to happen.
I can get them their Kindle, snack, juice, whatever,
and get them into a room with their older brother.
Because see, the younger kids,
they wanna hug mommy as soon as she gets home,
but they can't.
They can't.
That's not a good idea.
That's not safe.
Now, by the time I get this done, she's probably home.
I go to the door, let her know that the kids are secured.
Her shoes are off, disinfected.
They stay outside, because walking around can pick it up,
and then it can be brought back in on shoes.
And with little kids on the floor, that's bad, you know?
So she comes inside, shoes off, goes straight to the shower.
Clothing goes to the wash.
She's taking a shower, her personal belongings,
anything that entered the hospital,
purse, keys, sunglasses, whatever,
they're getting disinfected.
Because you know, kids like to stick stuff in their mouth.
So we have to make sure that all of that is clean.
Because if you bring something into a home,
it can spread, it can be transmitted.
I know this sounds really basic,
but I feel like it needs to be said because of this quote.
These kids have to get back to school.
They're at the lowest risk possible.
And if they do get it, which they will,
and they will when they go to school,
they're not going to the hospitals.
They're not going to have to sit in doctor's offices.
They're going to go home and they're going to get over it.
Before or after they give it to somebody else, governor,
the acknowledgement here is that it will spread in schools.
He apparently gets that part,
but he doesn't understand that it will spread
once it gets home.
Once they go home, give it to other people.
So this is completely ignoring the parents,
their coworkers, the teachers,
the support staff at the school.
We're not worried about those people
because most young people will be okay.
And I want to stress that most.
I want to know exactly how many kids
the governor of Missouri is willing to, I don't know,
let die to protect Trump's fragile little ego,
because that's what this is about.
We have to convince everybody that we've returned to normal
when we haven't, because Trump said it.
And nobody wants to tell Trump he's wrong.
Nobody wants to tell the emperor he has no clothes.
It didn't magically disappear.
Convincing people that we're back to normal
when we're not is really dangerous.
Look at Florida.
That's what happened.
The governor took his little victory lap
and cases shot up because everybody thought it was okay,
because he refused to protect his citizens
because just like the governor of Missouri,
he chose to protect Trump's fragile little ego.
That's more important.
These governors are selling out their duty.
They're selling out their citizens,
and they're selling out their country.
This man is not capable of running a house
with a nurse in it.
He's certainly not capable of running a state.
This is dangerous.
The acknowledgement that it is going to spread in schools
is in his statement,
but apparently he either doesn't understand
that it'll spread once it gets home
or he believes that the people of Missouri
are too uneducated to understand that.
Much like a lot of the ruling class in this country,
he views those people who he has a duty to look out for
as below him, as expendable,
as uneducated, unwashed masses.
It's appalling.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}