---
title: Let's talk about Trump's call to suspend the elections....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TKBoQZfYPaI) |
| Published | 2020/07/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Donald Trump called for the suspension of elections, citing concerns about universal mail-in voting leading to inaccuracies and fraud.
- Trump's call to delay the elections until vague preconditions are met mirrors tactics used by dictators in history to stay in power.
- Beau criticizes the Republican Party for not openly opposing Trump's call to delay the elections, accusing them of undermining the Constitution.
- Beau urges Republicans to stand up against Trump's actions to protect American freedom, warning that history will judge them based on their response.
- He calls on Republican leadership to deny Trump the nomination, describing the situation as the most significant threat since 1860.
- Beau accuses Trump of displaying fascist characteristics and attempting to subvert the Constitution through his statements on social media.
- He warns that those who continue to support Trump are betraying American values and endangering the country's well-being.
- Beau concludes by stating that supporting Trump is incompatible with supporting the United States, implying a moral and patriotic duty to oppose him.

### Quotes

- "The silence from the Republican Party has been deafening."
- "If you still support Donald Trump, you do not support the United States."

### Oneliner

President Trump's call to delay elections, likened to dictator tactics, prompts Beau to urge Republicans to oppose him, warning of a threat to American freedom and values.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Stand in open opposition to Trump's call to delay elections (suggested)
- Advocate within the Republican Party leadership to deny Trump the nomination (suggested)

### Whats missing in summary

The emotional intensity and urgency conveyed by Beau's plea for Republicans to take a stand against Trump can be fully grasped by watching the full transcript.


## Transcript
Well howdy there internet people, it's Beau again.
So the day we all knew was coming is here.
The President of the United States, Donald Trump, has called for the suspension of the
elections via tweet.
He says, with universal mail-in voting, not absentee voting, which is good, 2020 will
be the most inaccurate and fraudulent election in history.
It will be a great embarrassment to the USA.
Delay the election until people can properly, securely, and safely vote.
Yep, he put a question mark at the end of it to give himself some plausible deniability.
But there's the call.
There it is.
The thing that everybody said he'd never do.
But there it is.
It's one of the 14 characteristics.
Everybody said he was going to do this, starting years ago.
But people marched behind him.
This is the same call put out by every too-big dictator in history.
We need to delay the elections until these vague preconditions get satisfied.
And I'll determine when they are, while I stay in power.
The silence from the Republican Party has been deafening.
If you are not in open opposition to this, you are in on it.
This is undermining the Constitution.
At this point, there is no way to support this president and support the Constitution.
To support the Republic.
This is the same rhetoric used by others in the past.
There's a reason everybody knew what he was going to do before he did it.
And here we are.
Fourteenth characteristic.
If you're a Republican, if you swore an oath, now's your moment to shine.
You have been handed the opportunity to protect American freedom.
If you don't take it, it's on you.
And you will be remembered for it.
The Republican leadership needs to deny the president the nomination.
This is it.
The United States has not faced a threat like this since 1860.
At some point, people will have to wake up.
They will have to acknowledge they were wrong about him.
That he is exactly what all of the experts said he was when they watched his rhetoric.
You heard the call the entire time.
He's a fascist.
Now if you go back and look at those 14 characteristics, he's met them all.
He's trying to subvert the Constitution via tweet.
And the Republican leadership says nothing.
The country is in more peril than people realize.
Those who wave their American flags as they wave their rights and support this man are
selling out everything they pretend they believe in.
If you still support Donald Trump, you do not support the United States.
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}