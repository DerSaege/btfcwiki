---
title: Let's talk about pulling from Germany and slogans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8K_vCcxU9TQ) |
| Published | 2020/07/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Slogans can lose their original meaning over time, like "bring the troops home" in the context of withdrawing troops from Germany.
- The concept of bringing troops home was initially to de-escalate and disband, not relocate them within Europe.
- Some groups, including foreign policy hawks and those wanting fewer troops, are in rare agreement due to confusion over troop movements.
- Withdrawing troops from Germany is not about demilitarization but about relocating and potentially increasing troop numbers.
- The move may actually lead to more German NATO troops and contribute to further militarization in Europe.
- Foreign policy hawks support keeping troops in Germany as a strategic move in case of a ground incursion.
- Moving troops closer to Russia may not act as a deterrent but could make them more vulnerable to surprise attacks.
- Peace advocates oppose the troop movements as they see the end goal is to increase troop numbers and potentially lead to further military engagements.
- Lobbying and corruption in the defense industry play a role in decisions related to troop movements.
- The slogan of bringing troops home does not necessarily mean reducing troop numbers but could result in an increase.

### Quotes

- "The idea behind bringing the troops home was to de-escalate, not relocate."
- "The goal isn't to bring troops home. It's to increase the number."
- "Peace advocates oppose this because they understand the end goal is more troops."
- "Moving troops closer to Russia makes them more susceptible to a surprise attack."
- "The slogan 'bring the troops home' may not mean reducing troop numbers."

### Oneliner

Slogans like "bring the troops home" may lose original meaning over time, as withdrawing troops from Germany potentially leads to increased troop presence and militarization in Europe.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Contact advocacy groups focused on demilitarization (suggested)
- Join organizations advocating for peace and diplomacy (suggested)
- Organize events to raise awareness about the implications of troop movements (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of withdrawing troops from Germany and the potential consequences of slogans losing their original meaning.

### Tags

#TroopWithdrawal #ForeignPolicy #Demilitarization #NATO #PeaceAdvocacy


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
slogans and how sometimes a slogan, especially if it's been around a long time, will get
so entrenched in our vocabulary that we forget the original meaning. When a slogan comes
out, it doesn't encompass the whole idea. It's something to represent a much larger
idea. Sometimes a slogan has been around for so long and the situation has changed so much
that if you just listen to the slogan, you undermine the idea that that slogan was supposed
to represent. And this is exemplified in the discussion over withdrawing troops from Germany.
You have a whole bunch of people who are very on the record in regards to their desire to
curtail US military activity, who don't want the troops removed, and it is confusing
a whole lot of people. The idea, bring the troops home, we've heard this for decades,
it's been a slogan. When the slogan emerged, the idea was that if we brought the troops
home we would be less likely to engage in military adventurism and get involved in stuff
that we really don't need to. That's kind of the idea behind it. De-escalate. Bring
the troops home. De-escalate. That was the idea. The whole idea of bringing the troops
home is that they would come home and be disbanded. Not that we would just bring them home and
they'd hang out at Fort Rucker. So people who still stick with that mindset that we
need less troops, they're opposing the Trump decision. The Trump decision to withdraw.
And this has confused a whole lot of people. Because in one moment you have those who want
to de-escalate the militaries all over the world, foreign policy hawks, and those people
who just will oppose anything that Trump does, all lined up, all in agreement. Foreign policy
hawks and those who want less troops, they rarely agree on anything. But in this moment
they do. And it's mainly because the idea of yanking troops from Germany doesn't mean
what a lot of people believe it means. They think that these troops are going to come
home to the United States. A whole lot of people think that's what's going to happen.
It's not. Six thousand of them have already been reassigned, basically. They're going
so far away from Germany, they're going to Belgium. Not very far at all. Or they're going
to Italy. They're not leaving Europe. It's not a move to demilitarize in any way, shape,
or form. They're just relocating them. About six thousand of them will be coming home,
but I know at least part of that is an armored unit that will come home and then start a
rotation back to Europe. It's not actually about demilitarizing. None of these troops
are coming home and their positions being eliminated. We're not lowering our troop numbers
total. Now that slogan, bring the troops home, that originated in a time where we had to
have troops elsewhere to get involved in something quickly. The situation today is different.
The United States can put boots on the ground anywhere in the world in a matter of hours.
And it's not necessarily because of foreign bases. It's because we've learned to force
project. So, pulling these troops out does not actually lower troop numbers. On the contrary,
if we take Trump at his word, how advisable that is is up to you, if we take him at his
word, and what he's saying his intent is, is actually what his intent is, and it's not
just a gift to Putin. The goal is more troops. We're removing troops from Germany as punishment
in order to gain leverage. So, Germany increases its military expenditures. More troops, more
NATO troops, more troops to be used in adventurism because it's part of NATO. And when the U.S.
gets involved in something, Germany will probably help out. So, while many see themselves as
being anti-war, so they're cheering this on, they didn't follow it through. They just heard
that slogan. We're pulling troops from Germany. The idea is to move about 12,000. There's
going to be double that still remaining there, and they're not coming home. They're being
repositioned to make way, in theory, for more troops. No troop reduction, but if he gets
his way, there will be more German NATO troops. Backing him moving these troops, removing
them from Germany, that's not a peace move. It's not. It helps expand NATO. It helps expand
the militarization of Europe. It's not moving in the right direction. Now, the foreign policy
hawks, the reason they agree with this is an entirely different set of circumstances.
Germany is central. We want troops there, just in case there ever is going to be a ground
incursion there. That's really why we want them. Because most of Eastern Europe, by our
doctrines, we're just writing it off. It's a buffer zone. We're really letting them into
NATO, so if Russia was to move, they could steamroll over them, and they would buy us
enough time to get our forces ready in Germany. Moving the forces closer to Russia makes them
more susceptible to a surprise attack, not less. It's not really a deterrent. It's spreading
the troops out, making them less effective in a conventional fight. So the foreign policy
hawks, those who want a strong US military presence in Europe, they want the troops to
remain in Germany. The peace advocates, those who think beyond the slogans, they're opposing
this because they understand the end goal is more troops, which, as we talked about
in the recent video, what good are troops if you don't use them? And the lobbying that
the defense industry does, it's not really that much different in Germany. It's still
going to happen. There's still money to be made. There's still corruption. Is it as bad
as the US? No. But the US, well, we can drag them into it. It's more troops. That's why
it's being opposed by people who would normally be all about bringing the troops home, because
the goal isn't to bring them home. It's to increase the number. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}