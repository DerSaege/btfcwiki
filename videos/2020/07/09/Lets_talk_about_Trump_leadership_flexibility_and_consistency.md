---
title: Let's talk about Trump, leadership, flexibility, and consistency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Vjzh5GZ1HP0) |
| Published | 2020/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares advice on leadership from a warrant officer: leaders must be willing to take risks and be first through the door to inspire followers.
- Recalls the officer's experience in Panama, Grenada, and Vietnam as a real leader.
- Criticizes President Trump's approach to leadership, referencing his handling of the Republican National Convention in Jacksonville.
- Trump insists on holding the convention despite rising COVID-19 cases in Florida, showing inflexibility.
- Trump demands schools to reopen in Florida before August 24th, threatening to pull funding if they don't comply.
- Beau urges Trump to lead by example and move the convention earlier without additional safety measures to demonstrate that it's safe.
- Emphasizes that Trump's decision will reveal his priorities: politicians or children, teachers, and school staff.
- Asserts that Trump needs to show consistency in his leadership, even if it means maintaining a stance of "abject cowardice."
- Concludes by calling for decisive action from the President.

### Quotes

- "Be a leader. Be first through the door."
- "No flexibility on this one."
- "The entire country is watching."
- "It's time for the President to lead."
- "You can't have extra precautions."

### Oneliner

Beau advises President Trump to lead by example and prioritize the safety of children, teachers, and school staff over political events, urging for consistency and decisiveness in leadership.

### Audience

Politically aware citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for safe school reopening plans (implied)
- Organize community efforts to support schools with necessary resources for safe reopening (implied)

### What's missing in summary

The emotional impact and urgency conveyed by Beau's call for President Trump to prioritize the safety of children and demonstrate true leadership by example. 

### Tags

#Leadership #Consistency #Flexibility #SchoolReopening #COVID19


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about leadership, consistency, flexibility, and the President
of the United States, Donald Trump.
Years ago, I was talking to a warrant officer, and he was just giving me general advice.
He told me that if you wanted to be a good leader, if you wanted to inspire those who
were looking to you for leadership, that every once in a while, you had to be first through
the door.
You had to be the one to take the risk.
You had to be first through the door.
You could not ask them to do anything they hadn't seen you do first.
And that if you did this, those under you would follow you through any door.
Great advice.
Great advice.
And I knew what he was talking about.
He was very well respected, and he had been through the door first a number of times.
He was in Panama, Grenada, and Vietnam.
That was his advice on leadership.
And he was a real leader.
The President of the United States was recently asked about the Republican National Convention
there in Jacksonville, and what they were going to do now that the numbers are going
up.
He answered with his typical word salad in the beginning, talking about North Carolina
and how they didn't want to let him use the arena and all of that stuff.
The important part is when he says, we went to Florida, and when we went, when we signed
a few weeks ago, it looked good.
And now all of a sudden, it's spiking up a little bit, and that's going to go down.
It really depends on timing.
Look, we're very flexible.
We could do a lot of things, but we're very flexible in regard to his convention on August
24th in Florida.
Schools must open in the fall.
All capital letters, exclamation points.
In Florida, that's before August 24th.
Not a whole lot of flexibility there.
In fact, if they don't do it, if they don't follow his edict by tweet, well, we may just
have to pull their funding.
No flexibility for them.
No that ain't going to work.
The President needs to have his convention, the Republican National Convention, needs
to take place in Jacksonville.
In fact, they need to move up the date.
They need to have this convention before the President tries his little experiment on this
nation's children.
This is the opportunity for the President to be first through the door, to show us that
he can be a leader.
They need to move it up, and none of that fancy testing and safety precautions.
No rapid testing, no temperature checks.
Needs to be the exact same conditions you will find in a public school's cafeteria,
hallways, classroom, or bus.
Show us that it's okay.
Be a leader.
Be first through the door.
No flexibility on this one.
The entire country is watching, and if he chooses to alter his plans for Jacksonville,
he's saying very clearly that he values a bunch of politicians more than this nation's
children, teachers, and the support staff at schools.
There's no other way to read that.
No flexibility on this one.
It's time for the President to lead.
I know it's a little late in his administration to do so, but if he doesn't do it, at least
we do get a consistent approach throughout his entire administration, one of abject cowardice.
You can't change that date.
You can't have extra precautions.
You can't socially distance beyond what can happen in a school.
No.
No flexibility on this one, sir.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}