---
title: Let's talk about how to watch this channel and 300,000 subscribers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xzmbnUD09tw) |
| Published | 2020/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the milestone of reaching 300,000 subscribers and explains the purpose of his channel.
- He clarifies why he doesn't include sources in his videos, mentioning the importance of context over facts alone.
- Beau explains his strategy of breaking down information into shorter videos rather than longer ones for better engagement.
- He reveals how he uses techniques like the sunk cost fallacy and vague titles to draw in viewers and convey his messages effectively.
- Beau talks about fostering community through his channel and reaching out to those who need to hear his messages the most.
- He distinguishes his channel from a charity while discussing the charitable work they do, including providing PPE and setting up facilities for healthcare workers.
- Beau mentions his Patreon, where he offers benefits like Discord access and t-shirt discounts without putting exclusive content behind a paywall.
- He shares about the challenges of conducting interviews for podcasts due to the current situation with his kids at home.
- Beau explains his daily uploading schedule, advises against turning on notifications if you're a light sleeper, and ends with a message of hope for the future.

### Quotes

- "Context is what can get you to truth."
- "Short, digestible pieces of information are more effective."
- "I want this channel to help foster community and reach out to the people who need to hear it the most."
- "I don't put anything behind a paywall."
- "Hang in there. The long national nightmare will end eventually."

### Oneliner

Beau clarifies his channel's purpose, strategies, community engagement, charity work, and Patreon without hiding content behind paywalls, ending with a message of hope for the future.

### Audience

Creators, Activists, Viewers

### On-the-ground actions from transcript

- Support community initiatives by donating PPE or setting up facilities for frontline workers (exemplified).
- Follow Beau's example of using funds effectively in charitable work (exemplified).
- Engage with community members on platforms like Instagram and Twitter to see the impact of charitable work (exemplified).

### Whats missing in summary

Beau's detailed insights and personal touch can be best experienced by watching the full video.

### Tags

#ChannelPurpose #CommunityEngagement #CharitableWork #Transparency #Hope


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about 300,000 subscribers and how to watch this channel.
So every 100,000 subscribers or so I've done Frequently Asked Questions.
Somebody recently asked me to do a video explaining how to watch this channel and explaining some
of the weird things I do on it.
Now seems the appropriate time to do that before I get too far into it.
Thank you for all of the support.
It's really cool.
If you want to know more about the background of the channel, how it started, a little bit
more about me, whatever you can go to, let's talk about 100,000 subscribers or let's talk
about 200,000 subscribers.
Okay so how to watch it.
The number one thing that people ask about is why don't you put sources.
That's not me being lazy.
On the internet today you can find a source to back up anything, literally anything.
I can show you graphs that attempt to demonstrate that the amount of ice cream bought can predict
whether or not people will drown in pools.
Generally I think that facts can get you facts.
That's cool.
But context is what can get you to truth.
So if you end up searching it for yourself, you'll find more information that will give
you a clearer picture.
This isn't supposed to be a channel where I tell you how to think.
So I don't do it.
If it is something really obscure, the link to the study or whatever will be the pinned
comment down in the comment section.
People have asked me to do longer videos and I watch the whole conversation on Facebook
unfold about the reason I don't do it.
That's not it.
I do hate ad breaks but that's actually not why I have short videos.
I try to use the techniques that I advocate.
Short, digestible pieces of information are more effective.
So rather than having one 30 minute video, I will have three 10 minute videos and they
will come together in a story arc.
It's just more effective.
It helps people get the information.
So that's the reason I do it that way.
Because of that, at times a video is narrow in scope.
So if I'm talking about the food stamp program in the US, I may not bring up that the Panthers
had an effective version of their own in that video.
But there will be another one on it.
We're trying to figure out a way to organize them to make them easier to find.
Curious George.
So if the patch on my hat is ever upside down, I'm using something called the sunk cost fallacy.
If you can get somebody nodding their head and agreeing for the first minute or so, they'll
listen to the whole video even if they hear stuff they don't like.
So I have videos where I'm drawing people in to get them to listen to something that
I think is really important.
So if you ever hear a video start off with something that just does not sound like it
should be coming from me, that's probably what's going on.
Look at the patch on my hat.
I also do it when I'm doing satire.
When I'm just being incredibly sarcastic.
Titles my titles are vague.
Yeah they are.
I do that so people don't scroll past it just based on the title.
I want this channel to help foster community and I want it to reach out to the people who
need to hear it the most.
And if I say, yay taking down statues is good in the title, they won't listen to it.
So I don't phrase it that way.
I want them to tune in.
On that same note, I've had a lot of people ask me how I feel about the fact that a bunch
of people in the comment section are like, you know I've scrolled past 7 of your videos
before I ever clicked because I thought I knew what you were going to say.
It doesn't bother me.
I can't count on that on one hand and then get mad at it on the other.
For every person that says I scrolled past a bunch, there's a whole bunch of people who
clicked on it because they thought they were going to hear what they wanted to hear based
on how I look.
And those are the people that need to hear it the most.
So it does not bother me.
Are we a charity?
No, we are not a charity.
Our live streams do very well because we do a whole lot of charitable work.
But we are not a charity.
Do not try to claim a donation to us on your taxes.
We are not.
No.
If you follow us on Instagram or Twitter, then yeah.
You'll see the effects.
We donate PPE.
We recently set up a break room for a rural hospital that had everything separated off
and the nurses really couldn't leave otherwise they risked cross-contamination.
So we set up a break room for them.
We do stuff like that.
We have supplied domestic violence shelters.
We've done a bunch of stuff like that.
You can see the photos and the stories on Twitter and Instagram.
We'll do a video about it, but not always.
That's the reason they do well is because I try to use the money the way people would
want it used.
Our t-shirts used to, off top, 50% of it would go to a various charity.
We stopped doing it that way because people would buy shirts they didn't like because
it benefited a charity or they wouldn't buy a shirt they did like because it benefited
a charity they didn't like.
So we basically took control of it ourselves and it's proven to be a whole lot more effective.
Had we not done that, we wouldn't have been able to get all that PPE when everybody needed
it because we would have had the money.
So let's see.
Patreon.
Yes, I do have a Patreon.
My Patreon is a little bit different.
There's no content.
There's no real exclusive content that you'll get.
I don't put anything behind a paywall.
I can't sit here in good conscience and argue about the class problems in the United States
and then put stuff behind a paywall.
I don't think anybody that supports this channel enough to become a Patreon sponsor would want
me to do that anyway.
There's a Discord.
You get discounts on t-shirts.
There's polls and stuff like that occasionally.
There is some stuff but there's no exclusive content.
Sometimes you'll get content early.
The big draw was going to be the tour this summer but now everybody has to stay at home
so we're trying to figure out something else.
People have asked about podcasts.
We do interviews normally.
Right now all my kids are at home and they can't go anywhere.
So it's hard to do interviews and stuff like that because a little girl may come in and
Elsa has to be turned on right now or something along those lines.
All of the audio from these videos gets uploaded.
You can find it on Spotify or iTunes or wherever.
I do not tell people to like, comment, subscribe because I figure if you want to you will anyway.
I would be careful turning on notifications.
If you are a light sleeper and it pushes through to your phone, if a YouTube notification creates
a noise you may not want to ring the bell for me because I upload at various times and
they wake you up in the middle of the night.
Just know that in the last two years I think I have missed uploading like six days.
I upload every day at least once, sometimes two or three times a day.
So if you want to catch everything just check back every day.
That's rain so that's going to be pretty much the end of this video.
We're going to make it through this.
I know it's getting wild and it's going to get crazier before the election.
Just hang in there.
The long national nightmare will end eventually.
Anyway it's just a thought.
Thank you again for the support and I will try to be in the comment section today even
more than usual trying to answer any questions that you all have.
Thanks again. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}