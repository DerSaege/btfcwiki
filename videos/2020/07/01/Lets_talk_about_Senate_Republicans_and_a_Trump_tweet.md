---
title: Let's talk about Senate Republicans and a Trump tweet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_xGUA46rdrg) |
| Published | 2020/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains President Trump's tweet threatening to veto a defense authorization bill over an amendment by Senator Warren related to renaming military bases.
- Trump's tweet is culturally insensitive, historically inaccurate, and strategically distracting.
- Trump is unlikely to veto the bill and will sign it, despite his tweet suggesting otherwise.
- Senate Republicans are rolling intelligence funding into the bill but removing a provision requiring campaigns to report foreign assistance offers.
- Points out the inconsistency in allowing elected officials to accept foreign assistance without reporting it.
- Emphasizes the need to focus on significant issues like the bounties matter instead of being swayed by Trump's tweets.
- Urges for control over the narrative and to not let distractions hinder addressing critical matters.

### Quotes

- "We have to stop allowing Trump's edgy tweets to control the narrative."
- "He still has done nothing about the bounties."
- "We've got to take control of the narrative here."
- "We can't afford to become distracted right now."
- "Y'all have a good day."

### Oneliner

Beau explains Trump's distracting tweet, Senate Republicans' actions, and the need to focus on critical issues rather than being swayed by social media distractions.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Contact elected officials to demand transparency and accountability regarding foreign assistance offers (implied).
- Stay informed about critical political issues and hold elected officials accountable (implied).

### Whats missing in summary

The full transcript provides detailed insights on the strategic distractions caused by Trump's tweets and Senate Republicans' actions, urging for a focus on significant matters beyond social media controversies.

### Tags

#Trump #SenateRepublicans #ForeignAssistance #PoliticalDistractions #Accountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about what Senate Republicans have done, and
we are going to talk about an incredibly on-brand tweet from President Trump.
When I read this, you're gonna come up with all the reasons
in your head that it's on-brand, but there's another reason.
We're gonna talk about how it relates to what the Senate Republicans have done,
because that's the point of this tweet.
Okay, now as I read this, just understand, yes, it's a Trump tweet.
The structure is all off.
It has all of the random capitalized letters and
exclamation points you would expect.
Okay, so I will veto the defense authorization bill if the Elizabeth
Pocahontas Warren of All People Amendment, which will lead to the renaming,
plus other bad things of Fort Bragg, Fort Robert E.
Lee, and many other military bases from which we won two world wars is in the bill.
Sure, no you won't.
He's not gonna do that.
He will not veto this.
He'll sign it.
He will, even if Senator Warren's amendment is in it.
He will sign it.
You're probably thinking it's on-brand because it's culturally insensitive and
racist.
Yeah, that's on-brand.
You may be thinking it's on-brand because it's historically inaccurate.
Yes, Fort Bragg did not become Fort Bragg until after World War I.
In fact, it was Camp Bragg at the very end of World War I.
You may be thinking it's on-brand because the only time he ever decides to attempt
to show that he has a spine is when he is siding with people who are currently
fighting against or did fight against the US military.
Yeah, also on-brand for him.
You may think it's on-brand because it makes absolutely no sense to withhold
funding from DOD right after bounties became public knowledge.
Also on-brand.
But what's really on-brand about this is that he has no intention of doing this.
And that he tweeted this as a distraction to get us to talk about all of those
other things because he doesn't want us talking about what Senate Republicans
have done.
Senate Republicans have decided they're going to roll in the intelligence funding
into this, but only after they have stripped out the portion that says
campaigns must report offers of foreign assistance.
So what does that tell you?
Number one, you know Trump's going to sign it.
Number two, Senate Republicans are co-signing him selling out the country.
They know that he's going to get offers of foreign assistance and he's going to
take them.
And they don't want him to have to report it.
Imagine us doing this with anything else.
Imagine our security clearances.
Imagine if security clearances would still be granted if somebody had taken
10 grand from a foreign national and not reported it.
Wouldn't happen.
But it'll be okay for elected officials to do it.
That's what he doesn't want us talking about.
We have to stop allowing Trump's edgy tweets to control the narrative.
Because that's what he does.
We need to focus on what's important.
He still has done nothing about the bounties.
And Senate Republicans are setting it up so he can once again accept offers of
foreign assistance.
So he can get help from foreign nationals.
And it won't be a problem.
He won't have to report it.
He's going to sign this.
He needs it.
Because the only way he can win is to cheat.
Now if you don't know why you shouldn't call people Pocahontas, I will put a
video down below.
The story that most people know, the love story, that's not the real story.
It's a term that should be avoided.
We've got to take control of the narrative here.
We have to stop letting a child with a tweet tantrum control the nation and paralyze it.
There's a lot of important stuff going on.
We can't afford to become distracted right now.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}