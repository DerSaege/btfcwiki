---
title: Let's talk about how Trump made Biden the most powerful man in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0jWW-JfInpc) |
| Published | 2020/07/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Joe Biden, despite not being Beau's preferred candidate, is currently the most powerful man in the United States.
- Trump inadvertently gave Biden this power by not understanding how politics works in the system he wants to implement.
- Biden can alter the political landscape with just one speech or tweet, thanks to Trump's actions.
- Trump is considering deploying his forces nationwide after their controversial presence in Portland.
- The threat of a nationwide deployment comes after a team member suggested shooting those who follow them.
- Despite this, there has been no investigation or calls for law and order from the president.
- Trump's approach has eroded trust in his administration and its reports.
- Biden is now seen as the anti-establishment candidate against Trump's authoritarian tactics.
- If Biden were to pardon those arrested by Trump's forces, it could render them ineffective and boost Biden's support.
- Biden has the power to incite chaos with a single tweet, contrary to Trump's fearmongering narrative.

### Quotes

- "Joe Biden is now the most powerful man in the United States."
- "This tough guy rhetoric that everybody likes to use, it's getting out of hand."
- "Had he read the manual, he [Trump] would understand that the people in the street pretty much always win."

### Oneliner

Joe Biden's newfound power and Trump's misunderstandings have shifted the political landscape, setting the stage for potential chaos.

### Audience

Voters, Activists, Political Observers

### On-the-ground actions from transcript

- Organize peaceful demonstrations to show opposition to authoritarian tactics (implied)
- Support transparency and accountability within law enforcement agencies (implied)
- Stay informed and engaged with political developments to understand power dynamics better (implied)

### Whats missing in summary

More insights on the implications of Trump's actions on civil unrest and democracy.

### Tags

#Politics #PowerDynamics #Authoritarianism #JoeBiden #DonaldTrump


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to go through the looking glass, because the country has, we're on our
way.
We're going to talk about what politics is like in countries that have this system of
government that Trump wants.
I think a lot of this is going to be funny coming from me because I think most of you
know that Joe Biden is not my man.
However even I cannot deny that Joe Biden is now the most powerful man in the United
States.
And I don't mean that in the sense of he's likely to be elected president, therefore
he's powerful.
I mean today, right now, as a private citizen, he's the most powerful man in the United States,
more so than Trump.
And Trump gave him that power.
In his hurry to cast himself as this strong, tough guy leader, Trump apparently neglected
to learn how politics works in that kind of system, in the system that he wants to bring
about in the United States.
Because he created a situation in which Joe Biden can give one speech, or one tweet, and
forever alter the political landscape of the United States.
The president is threatening to roll out his goon squad from Portland, nationwide.
You know, because it was so effective there, at quelling dissent.
So effective that you now have moms and grandmas out in the street.
Crowd sizes have grown, so these gatherings are larger than the president's Tulsa rally.
That's how effective they are.
But the president wants to roll them out nationwide, and he's making this statement, talking about
doing this right after the entire country sees a member of this task force say, if you
follow us, you're going to get shot.
I would like to see the statute authorizing that.
I'm fairly certain that's illegal.
In fact, I'm fairly certain that just saying that is illegal.
But we all saw it.
We all saw the footage.
You know what we didn't see?
An investigation.
We didn't see the president calling for law and order then.
We certainly didn't see any arrests.
The American people saw that.
What does that say about that task force?
What can we infer from it?
That they can't be trusted.
That they don't have any credibility.
That unlike the military, when Trump tried this exact same stunt with them, that they
won't honor their oaths.
He tried this before and got so much pushback he had to cancel it.
That's what the American people see, somebody that can't be trusted, a group of people who
cannot be trusted, which means their reports, to include their arrest reports, can't be
trusted.
The president has cast himself as the opposite of Joe Biden.
Fine, whatever.
The thing is, the president, no matter how much he wants to pretend that he's a maverick
and an outsider, he's the establishment now.
He's been in power for years.
The president has successfully cast Joe Biden as the anti-establishment candidate.
Those people who saw that footage, those people who saw what went on in Portland, those people
who have been paying attention maybe for the first time over the last few months, they're
looking to Joe Biden for leadership.
They see him as what stands between totalitarian rule and the American way of life.
Is that an accurate read?
Is Joe Biden anti-establishment?
No, of course not.
That's silly.
But that's not how a lot of people are going to see it, because they're not going to look
at it as far as establishment.
They're going to look at it as fascism and the system we currently have now.
Is Joe Biden the difference between that?
Yeah.
And I am not a supporter of Joe Biden.
So that gave Joe Biden an unprecedented amount of power.
You have a lot of distrust.
You have people in the streets.
You have an authoritarian squad running around that is completely unaccountable.
And you have a lot of opposition to the establishment party of Trump.
If Joe Biden was to walk out tomorrow and say, if elected due to the lack of transparency
and the apparent corruption within this task force, I have no choice but to pardon anybody
they arrest, what happens?
That task force becomes just like President Trump, completely ineffective at everything.
What good is secret police if they're not scary, if they can't haul you off, if you
know that they're only going to be able to hold you a couple months?
It would definitely get him votes.
It would increase the number of people in the streets, those people who see themselves
as loyal to Joe Biden.
It would probably cause people to take to the streets in cities where they're not right
now, because they have less fear of repercussion.
They have the backing of somebody, somebody who's anti-establishment.
That's what the American people see.
This is how politics works in a country that has had their constitution subverted after
three and a half years.
This is how politics works.
Joe Biden, with a tweet, could send the country into utter chaos.
And it's funny because that's what Trump tries to fearmonger.
Joe Biden wants this.
This is Joe Biden's America.
No, dude, that footage is from your administration.
And if Joe Biden wanted it, if he wanted cities in flames, he could do it now with almost
no effort.
This tough guy rhetoric that everybody likes to use, it's getting out of hand.
It's been getting out of hand.
And everybody needs to take a step back because where we're headed, most of those people who
think they want to go there, they don't.
You really don't.
This is how it starts.
Mass demonstrations, secret police, establishment and anti-establishment.
This is what causes true strife.
Trump is setting the stage for it.
And I know there's a lot of people who think he's doing this intentionally.
The thing is, had he read the manual, he would understand that the people in the street pretty
much always win.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}