---
title: Let's really talk about dropping the MIC and defense spending....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fXmeMyMcupo) |
| Published | 2020/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Eisenhower's farewell address introduced the term "military-industrial complex" in 1961, warning about the influence of the arms industry on the government.
- The arms industry profits by influencing government decisions to purchase more arms, leading to a cycle of increased militarization.
- This influence results in policies that may not serve the public good, encouraging interventions and a war-based economy.
- The military-industrial complex extends its influence to schools, contributing to the normalization of a militarized society.
- Beau suggests curbing this influence to prevent further militarization and negative consequences for society.
- Noam Chomsky argues that it's not just the military-industrial complex but a broader issue of industrial influence over policy-making for profit.
- The term "military-industrial complex" adds a veneer of legitimacy to what is essentially corruption in various sectors like private prisons and big agriculture.
- Lobbying and campaign contributions lead to policies that benefit large industries at the expense of the public good.
- By framing these issues as corruption rather than complex terms, it becomes clearer how profit-driven decisions harm society.
- Beau advocates for recognizing and addressing corruption in government decisions, particularly in defense spending, which often goes unquestioned due to pervasive influence.

### Quotes

- "The military-industrial complex isn't really a thing. A guy named Noam Chomsky said there's no such thing as the military-industrial complex. It's just the industrial system."
- "All of this, all of this, it's just corruption."
- "It's corruption. We need to start calling it what it is."
- "It's just corruption. Maybe it's better to just give people the term that they know, that they can recognize."
- "They were all under the influence of corruption, not the military-industrial complex."

### Oneliner

President Eisenhower's warning about the military-industrial complex reveals a broader issue of corruption influencing policy-making in various industries for profit.

### Audience

Policy reform advocates

### On-the-ground actions from transcript

- Advocate for transparency in government spending and policy-making (exemplified)
- Support campaigns against corruption in various sectors, including defense spending (exemplified)
- Educate others on the influence of profit-driven decisions on public policy (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of how industrial influence for profit corrupts policy-making and the importance of addressing this corruption to ensure decisions serve the public good.


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about a term,
a term that most people have heard but maybe not everybody understands,
and we're really going to talk about it. Okay, so a term exists and it came into popular usage
after January 17, 1961 when President Eisenhower gave his farewell address. In his speech,
a vital element in keeping the peace is our military establishment. Our arms must be mighty,
ready for instant action, so that no potential aggressor may be tempted to risk his own
destruction. This conjunction of an immense military establishment and a large arms industry
is new in the American experience. The total influence, economic, political, even spiritual,
is felt in every city, every state house, every office of the federal government. We recognize
the imperative need for this development, yet we must not fail to comprehend its grave implications.
Our toil, resources, and livelihood are all involved. So is the very structure of our society.
In the councils of government, we must guard against the acquisition of unwarranted influence,
whether sought or unsought, by the military-industrial complex. The potential
for the disastrous rise of misplaced power exists and will persist. The military-industrial complex.
When that term gets thrown out, people oftentimes don't really know what it means.
Just this shady, smoke-filled room with a whole bunch of guys, cigars at a table drinking brandy
talking about how to take over the world. That's not what it is. The key word in that speech
isn't military-industrial. It's influence. It's influence. The complex as it is,
is arms manufacturers making profit, then using that profit to influence government,
to purchase more arms for the military. The military then gets the arms, and this is where
it turns bad. All of this is normal business in the United States. Not necessarily good,
but it's normal. Here's the thing about arms. If you are selling arms,
let's take ammo, for example. You sell ammo. You make ammo. In order to sell more,
if the military is your client, they have to use it. In order to stay profitable, they have to use
it. There's only so much they're going to use with training, and ammo has a really long shelf life.
So how are they going to use it? This is why you have defense firms funding think tanks.
These think tanks often encourage adventurism, intervening in countries where we really maybe
shouldn't. It harms public policy. It's against the public good. That influence gets used
as a reason for war. It pushes the war narrative. That war-based economy,
have to keep it moving. Ammo, rockets, it's all got to keep getting built.
Because it becomes so pervasive, it's everywhere. It's in our schools. It's everywhere.
And it contributes to the militarization of our society as a whole because we
become used to it. Up until the 1900s, the US military ran their own armories.
And then after World War II, well, then it became an industry. It became a huge industry
with a lot of money and a lot of influence. That influence runs contrary to the public good.
Now, obviously, one of the key things we should do is start to curtail that influence.
We have to shut it down. Otherwise, it will continue. The country will continue to militarize,
and we'll be in this situation for much, much longer, and it will keep getting worse. We will
devolve into Sparta. And while I'm sure there's a bunch of macho guys out there that think that's a
good idea, it's not. Curtailing that influence. A congressperson recently suggested banning
recruitment of young people, active recruitment from the military, keeping them out of schools,
off of school, off of work, off of school. And that's a big deal.
And if you're going to ban a lot of schools off of certain social media platforms,
that's probably a good place to start. Get them out of the indoctrination that war is good,
because that's part of it. On some levels, it's spiritual. It's become part of our national
identity. If we allow it to stay there, it will continue to erode the good things that this country
has done. Anyway, it's just a thought. Y'all have a good day. Now, in theory,
there's going to be a break here, and then we're going to come back and talk about how
the military-industrial complex isn't actually a thing, not the way most people picture it.
Welcome back. So, Eisenhower said this, right? Eisenhower recognized the pervasive influence
that was given to certain industries, the military-industrial complex. He gave it a name.
He recognized it in the military because before he was President Eisenhower, he was General
Eisenhower. He watched it develop. He saw it there. Now, there's an unsubstantiated rumor
in the initial drafts of this speech that it wasn't military-industrial complex. It was
military-industrial-congressional complex. And while we don't know if that's true, it's a rumor,
or whether or not it is, it's more accurate. It makes the term make more sense. But even that
alone, it's still not accurate. He recognized it in the military because he was familiar with it.
But this idea of industry using its profits to create influence, to influence policy-making,
which is designed to increase their profits, not necessarily for the public good, it's pervasive.
It's everywhere. So military-industrial complex isn't really a thing. A guy named Noam Chomsky
said there's no such thing as the military-industrial complex. It's just the industrial
system. He's right. He's right. In and of itself, there's nothing wrong with a large industry,
a large method of production. That's not... There's nothing wrong with that.
The problem comes when that influence becomes used to put their profits over people,
their profits over the public good. This influence is everywhere, and Congress is a big part of it.
The prison-industrial complex, that's a term getting thrown around a lot right now.
It's the same thing. Private prisons need their beds full, so they lobby to have longer sentences.
They put in their contracts that certain X number of people must be arrested.
Contrary to the public good, we have tons of studies saying that long incarceration periods
don't work. They don't do any good. They just cost people money, cost the taxpayers money,
cost people their lives. But it makes those private prison companies make more money.
Contrary to the public good, that's the big piece of it. That's the part that matters.
Big agriculture, same thing. There's nothing wrong with giant farms.
There's nothing wrong with it. Well, there might be from an environmental standpoint. But
the idea of large industry, there's no issue. The problem comes when those profits get used
to influence Congress and then enact policies that are contrary to the public good so those
large companies can make more money. It's not the military-industrial complex. It's not
the prison-industrial complex. It is not big agriculture. It's just corruption.
When we give it these other names, we add legitimacy to these campaign contributions,
to these lobbying events. It's corruption. We need to start calling it what it is.
When Eisenhower came up with this, he wasn't trying to add a veneer. He was really trying
to give us a warning. But we didn't listen. We focused on that term. And it became
a joke. It became, in most people's minds, it's a conspiracy theory. It's something that those
radical people say and nobody really knows what it means. Plain language is really important.
The military-industrial complex, the prison-industrial complex, all of this,
all of this, it's just corruption. Yeah, you sound like you have a firmer grasp
on what you're talking about if you use these terms. It's definitely grounded in history.
There's a reality there. But if you're trying to sway people's opinion, maybe it's better to just
call it corruption. Maybe it's better to just give people the term that they know, that they can
recognize. Company X makes money because of policy decisions from the government. Company X then
spends money that goes into the coffers of those who make the decisions. That's corruption. Yeah,
it's a bigger cycle here. It's not a direct bribe. But it certainly corrupts people. That's why you
have billions, billions for defense spending in that recent stimulus package. That's why it's
there, because it's so pervasive nobody even considers it. And yeah, Republicans, rank-and-file
Republicans are coming out against it now. It seems like everybody in the Republican Party
is against it, so I'm not sure how it wound up in there. The thing is, all of that spending,
if the political imagery wasn't bad, if it was in any other bill, it would pass. They'd vote for it,
because they've all gotten their contributions. They were all under the influence of corruption,
not the military-industrial complex. It's just corruption. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}