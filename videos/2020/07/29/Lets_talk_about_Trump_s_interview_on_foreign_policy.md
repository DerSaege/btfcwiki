---
title: Let's talk about Trump's interview on foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=20oyDJZWJis) |
| Published | 2020/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques Trump's interview, pointing out his lack of understanding on various subjects.
- Trump falsely claims he never saw intelligence on Putin's bounties, despite it being in his daily brief.
- Trump brags about receiving intelligence briefings 2-3 times a week, falling short of the necessary 7.
- Beau notes that Trump's limited briefings hinder his ability to understand and address global issues effectively.
- Trump's lack of comprehension regarding foreign policy is evident in his statements about arming the Taliban and historical events.
- Beau criticizes Trump's incompetence in dealing with Putin compared to the Russian leader's intelligence and sharpness.
- Beau suggests that Trump's ineptitude may inadvertently benefit the US by preventing further entanglement in conflicts.
- Trump's misunderstanding of Russian intentions in Afghanistan could lead to unintended consequences if he were to respond.
- Beau stresses the importance of having a knowledgeable foreign policy expert in the next administration to repair damaged international relationships.
- Overall, Beau expresses concern about the chaotic state of US foreign relations under Trump's leadership.

### Quotes

- "He's inept. He's incompetent when it comes to this subject."
- "That's the intent. They don't want us withdrawn."
- "The next administration has a mess to clean up."
- "Imagine our relationships with the countries that we don't think about."
- "It's hard to withdraw from somewhere when you're constantly taking hits."

### Oneliner

Beau criticizes Trump's lack of understanding in a damaging interview, stressing the need for foreign policy expertise in the next administration to repair international relationships.

### Audience

Foreign policy experts

### On-the-ground actions from transcript

- Advocate for competent foreign policy experts in positions of power (implied)
- Stay informed on global issues and political developments (implied)
- Support efforts to repair and strengthen international relationships (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's interview, shedding light on his lack of understanding in foreign policy matters and the potential consequences of his actions.

### Tags

#Trump #ForeignPolicy #Putin #USRelations #Intelligence


## Transcript
Well howdy there internet people, it's Beau again.
So we're going to talk about that interview.
We're going to talk about Trump's interview.
Have you ever watched somebody do their best to pretend to understand a subject, and the
more they talk, the more you realize they don't have a clue what they're talking about?
That's that interview.
If I had to describe that interview, that's what it was.
There are a number of things that people are bringing up that definitely need attention,
because he got some things wrong, but there were some other things that aren't catching
as much attention that maybe should.
You know, he was asked directly about whether or not he ever confronted Putin about some
of the allegations that are going on about the bounties.
He said no because it never reached his desk.
That's not true.
It was in his presidential daily brief.
We know that, okay?
It's not true.
That's fine.
We get it.
We've seen it applied 21,002 times now.
Fine.
He goes on to say that he gets intelligence briefings two or three times a week.
A lot of meetings.
That's what he says.
Seven.
He should get a minimum of seven.
Presidential daily brief, it's in the name.
It's in the name.
He should get a minimum of seven intelligence briefings a week.
Bragging about going to two or three is like bragging about passing a cognitive test.
It's not something anybody who understood it would do.
Seven.
That's the minimum.
That's the minimum you would need to be effective because, as he says, the world is angry all
over the world.
There's a lot of information to process and it changes daily.
That's why there's a presidential daily brief.
Seven is the minimum acceptable number.
He was questioned about whether he confronted Putin about arming the Taliban because the
bounties are in dispute.
He didn't do that either.
He said he wanted to talk about proliferation, which is fine.
That's an important topic.
But apparently the president doesn't understand his own moves, much less Putin's.
The president has withdrawn from nuclear proliferation agreements.
In fact, all of the ones that Putin didn't like, the US took the step to withdraw.
My guess is that he was looking through old footage of Reagan and believed that it might
help him because it helped Reagan getting into disarming.
But if you spent your entire administration scrapping those treaties, it just makes you
look like you don't know what you're doing, which is because he doesn't know what he's
doing.
He then goes on to say that we supplied weapons when they were fighting Russia.
That's not true, actually.
I get the theory that he's saying there, but that's an inaccurate statement.
We supplied weapons when they were fighting the Soviets.
It was a completely different geopolitical situation, literally different countries.
If we were to use that line of thinking, we could also say we supplied tanks and Thompsons
to the Soviets during World War II.
Things change over time.
You can't do that, especially when the countries involved have changed forms of government
over the years.
Every statement that he made displayed a complete lack of understanding of foreign policy, every
single one of them.
He's inept.
He's incompetent when it comes to this subject.
At the end of the day, we can't hold him to too high a standard, though.
We need to keep in mind Putin was one of the greatest intelligence officers of the 20th
century.
Trump is a failed game show host.
Putin is smart, sharp, has a deep understanding of these subjects.
Witty, he's a good leader.
Trump is none of those things.
This isn't a fair discussion.
The idea that Trump would be able to handle Putin on any level is laughable.
So we can't hold Trump to too high a standard when it comes to his outcomes because he demands
to deal with him directly.
We don't know why.
And it's even, Trump is placing himself at even more of a disadvantage by only getting
two or three intelligence briefings a week, not just as he not understand it, not want
to protect American forces, he makes no effort to get to the point where he could understand
it.
Now at the end of the day, there is one important thing that we have to remember here.
Trump said that, you know, the arming and then Russia getting bogged down there is what
wound up breaking up the Soviet Union.
And because of that, that's why the current Russia doesn't want to get involved.
Again, a gross misunderstanding of what happened.
Today, Russia, the current Russian government is on the opposite side of where the Soviet
government was.
They're supplying the unconventional forces.
The US is on the side of where the Soviets were.
One thing that we do need to keep in mind as this plays out is that in this case, Trump's
ineptitude, his inaction, his unwillingness to question his boss, all of these things
are playing in our favor in the long run.
The goal of the Russian operation in Afghanistan is to keep the US bogged down.
That's their intent.
We've talked about it before.
Intent is what is important.
That's the intent.
They don't want us withdrawn.
They want us there expending our energy so we can't focus on Europe, so we can't focus
on the areas they care about.
It's hard to withdraw from somewhere when you're constantly taking hits.
That's the Russian goal in supplying the opposition there.
So the fact that the president is inept, is overmatched, is actually helping us.
It's not helping the individual soldier over there, but it is helping us in the long run
because if Trump understood the situation, he might respond.
By responding, might embed us even further.
I know people are going to say, well, he's trying to leave.
That doesn't mean anything.
He very quickly changes course when it comes to foreign policy.
Look at the proliferation treaties he's talking about because he doesn't know what he's doing.
When the next administration comes in, they have to have somebody on the team, high up,
that really understands foreign policy.
It is incredibly important.
This is a relationship with a world power.
It's in complete disarray.
Imagine our relationships with the countries that we don't think about, with countries
that aren't in the forefront of Trump's mind.
Those relationships have not been managed or nourished in years.
The next administration has a mess to clean up.
They need to get somebody on board that is equal to the task.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}