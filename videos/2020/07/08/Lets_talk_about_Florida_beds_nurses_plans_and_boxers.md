---
title: Let's talk about Florida, beds, nurses, plans, and boxers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q-XRACaIlvs) |
| Published | 2020/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Florida again due to unforeseen events unfolding.
- Governor DeSantis plans to reopen schools in August for in-person learning despite concerning numbers.
- Florida is facing a critical shortage of ICU beds and nurses.
- Despite the crisis, the leadership is reluctant to change their plan.
- Beau draws a comparison between facing a crisis and being a boxer getting punched in the face.
- Urges for a change in strategy to avoid getting knocked out by the current situation.
- Emphasizes the need to listen to medical experts and follow simple guidelines like staying at home and wearing masks.
- Criticizes the government's failure to provide effective leadership during the crisis.
- Calls for community action and solidarity in the absence of government support.
- Stresses the importance of individuals taking responsibility for their actions to combat the situation effectively.

### Quotes

- "Every boxer will tell you that when you step into that ring you have a plan until you get punched in the face."
- "We are Floridians. We know that after a hurricane the government's pretty much useless."
- "We have to lead ourselves."
- "Stay at home. Do what you can to stay at home."
- "If you have a boss that tells you not to wear a mask or that you can't do it in their store or whatever, send me a message."

### Oneliner

Beau talks about Florida's crisis, urging a change in plan to combat the shortage of ICU beds and nurses, and stresses community action amid government failure.

### Audience

Community members

### On-the-ground actions from transcript

- Stay at home, follow health guidelines (implied)
- Wear a mask and take necessary precautions at work (implied)
- Support each other by standing up against unsafe practices (implied)

### Whats missing in summary

The full transcript provides additional context on the failures of leadership during the crisis and the necessity for individual and community action.

### Tags

#Florida #COVID19 #CommunityAction #PublicHealth #Leadership


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about Florida again and boxers.
What boxers can teach us about plans.
You know, we're talking about Florida again because something happened.
Something that was completely unforeseen, unpredictable.
Nobody could have imagined this happening except for everyone who did.
Unlike the only people that didn't see this coming were the people responsible for making
sure that it didn't come.
Yesterday we were talking about how Governor DeSantis has decided now that it's time to
reopen schools in August in person.
We're talking about how the numbers suggest that maybe that's not the best course of action.
Maybe that's not the best plan.
But see, he has a plan.
He has that plan and he wants to stick to it.
That plan is real simple.
We're going to put all this behind us.
We're going to get back to our routine.
We are going to put this issue to bed.
The problem is we can't do that because we're running out of beds.
We have 962 beds left, ICU beds left in the state.
You have some hospitals that are straight out of beds.
It's clear from that that what we're doing is not working.
Every boxer will tell you that when you step into that ring you have a plan until you get
punched in the face.
Florida we have been punched in the face.
We have to adjust our plan or we're going to get knocked out.
The bed shortage isn't even the biggest issue.
We're running out of nurses.
They're having to shuffle nurses around the state to meet the demands.
Major metropolitan areas have hospitals with no ICU beds left.
962 left in the state and that number is actually lower by now.
They're locked into that plan and they don't want to change.
They don't want to admit they were wrong.
They don't want to admit that they've been punched in the face.
We are Floridians.
We know that after a hurricane the government's pretty much useless.
They're not going to help.
We're on our own for a few days and during that period we have to make the best decisions
we can for ourselves, our families, and our communities.
That's where we're at right now.
We didn't suffer a natural disaster.
We suffered a political one that has caused far more damage because of ego.
Hurricane DeSantis.
Following the politicians' advice on this, following their plan to stay safe, it's clearly
not working.
Maybe instead of following the advice of the guy who couldn't put his mask on, we should
follow the advice of the doctors.
And I know they like to pretend that there's a bunch of debate.
There's not.
There's a pretty clear consensus on this.
We need to do what has worked in other places.
Stay at home.
Wash your hands.
Don't touch your face.
If you have to absolutely go out for some reason, wear a mask.
This isn't hard.
That's the plan.
That's the plan we should all be working towards.
I would suggest that adding another point of transmission at schools when we can't handle
it as it is, is probably not a good idea.
We should probably alter that plan.
I know there are a lot of people that think that we should just let it run its course.
You know, get everybody exposed and then we'll all be okay.
Yeah, there's some acceptable losses that are going to have to happen that way.
The problem with that theory is that study after study after study is showing that the
antibodies don't last very long.
So if we don't stop transmission, once they wear off, you might get it again.
We don't know, but we can't develop a plan based on that or we're going to get punched
in the face again because we don't know.
What we do know is that staying at home, washing your hands, not touching your face, wearing
a mask, these things work.
They help.
What we're doing now isn't working.
And as people talk about acceptable losses, I want to put our national situation into
focus real quick.
We have lost more people to this than we did to combat in Vietnam or combat in Korea or
combat in World War I or all of those combat deaths added together.
This is out of control and it didn't have to be.
We didn't have to make these decisions.
They made these decisions under the pretext of saving the economy.
You know, I'm not an economist, but I would suggest that having the state, the country,
whatever, half shut down for a year because we refuse to do what's necessary and everybody
just stay at home is going to be more damaging than everybody staying at home for a month.
The longer this goes on, the worse the economy is going to get.
You can't wait for a government order.
You don't need a government order.
Stay at home.
Do what you can to stay at home.
And I understand their economic concerns.
Take every possible precaution you can.
Everything that you can do, do it.
Wear a mask at work.
Do everything you can.
If you have a boss that tells you not to wear a mask or that you can't do it in their store
or whatever, send me a message.
We've got to back each other up and we have to act as a community because the government
has failed us.
We have no leadership.
We have to lead ourselves.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}