---
title: Let's talk about a story out of Mississippi and systemic issues....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZLGbBYrqpro) |
| Published | 2020/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of an election commissioner in Mississippi and the importance of sharing stories to understand systemic issues.
- Sheds light on a Facebook post by Gail Harrison Welch, an election commissioner, where she differentiated between "the blacks" and "people" regarding voter registration concerns.
- Explains how this differentiation perpetuates systemic racism and voter suppression to maintain institutional power.
- Raises the issue of gerrymandering and the manipulation of voting districts to silence minority voices.
- Criticizes Welch's attempt to explain her comments privately after posting them publicly, suggesting it raises further doubts about her actions behind closed doors.
- Questions Welch's statement about trying to motivate people to vote but points out the underlying racial undertones in her initial comments.
- Emphasizes the need to reject any form of differentiation between groups of people based on race or ethnicity.
- Urges for a new mindset in the South that moves away from racial discrimination and towards inclusivity and representation.
- Raises concerns about how Welch's mindset may have influenced her actions during her 20-year tenure as an election commissioner.
- Points out the upcoming election where the people of Jones County, Mississippi may need to be informed about Welch's history and attitudes towards voter registration.

### Quotes

- "If you are differentiating between any group of human beings and people, you're wrong. You're racist, you're sexist, you're a bigot of some sort, I guarantee it."
- "We're building a new South. We don't need this. We've rejected this. We're past this. We're getting away from it."
- "This shows one of the critical flaws in representative democracy."

### Oneliner

Beau sheds light on systemic racism and voter suppression through the comments of an election commissioner in Mississippi, urging for a new mindset towards inclusivity and representation.

### Audience

Mississippi residents

### On-the-ground actions from transcript

- Inform the people of Jones County, Mississippi about the upcoming election and the history of the election commissioner (suggested).
- Advocate for fair voting practices and representation in your community (implied).

### Whats missing in summary

Deeper insights into the impact of systemic racism on voter registration and the importance of addressing racial biases in public office.

### Tags

#SystemicRacism #VoterSuppression #Mississippi #ElectionCommissioner #Inclusivity #Representation


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about an election commissioner in Mississippi.
We're going to do this because we have a story.
We have a story.
A lot of the stuff that we talk about on this channel, they're big picture concepts.
We've got statistics and numbers and facts and studies and all of this stuff, it's abstract
for a lot of people.
Everybody learns that way.
A lot of people need a story.
They need something that they can look at and follow and then break it apart and look
at each little piece and then they get it.
Or you could show them charts and statistics all day.
One of the things that we've been talking about a lot on this channel is how certain
systemic issues are everywhere.
They invade every part of our society and because they're so prevalent it's hard to
pick out individual stories.
We have that.
Okay.
Gail Harrison Welch posted this publicly on Facebook.
I'm an election commissioner in Jones County, Mississippi.
I'm concerned about voter registration in Mississippi.
The blacks are having lots of events for voter registration.
People in Mississippi need to get involved too.
Thank you for all you do.
Did you catch that?
The blacks.
People.
That's a pretty marked difference.
I mean that's pretty pronounced.
I'm an election commissioner.
I'm concerned about voter registration.
This is what's happening.
So one of the main conditions of systemic racism is voter suppression, negating the
vote of minority groups.
It's exactly what's happening in this post.
The concern is that, quote, the blacks are going to have a lot more voters than the whites.
If you are using fear of a minority group to motivate another group, it's so they can
maintain that institutional power, that systemic power.
This is the reason voting districts look the way they do.
They're gerrymandered.
They draw them up to divide up the votes.
So black voices or minority voices of any kind don't get heard.
And if they can't do it that way, they can do it this way.
Downplaying voter registration for one group, upplaying it for another.
They can remove polling stations from minority communities so it's harder for them to vote.
This is all part of something that occurs, and it is systemic.
Now to be fair, she did try to explain her comments.
And she said in an interview on Monday, she meant to say this privately, didn't mean to
post it publicly on Facebook, meant to send it in a private message just so everybody
knows and is aware that doesn't make it better if you are in an office of public trust and
you are making statements in private that you wouldn't want to make publicly about that
office of public trust.
It just leads people to believe that even more is going on behind closed doors.
She said, we've always in the past had whites really participating in registering to vote.
So many people don't seem to be concerned about voting.
Okay.
The concern, once again, is that, going back to that first statement, concerned about voter
registration in Mississippi, the blacks are having lots of events for voter registration.
That's the concern.
And she does say at one point, this was an error on my part, but I can't tell honestly
because of how the interview was done, whether or not she's talking about what she said or
the fact that she said it publicly.
I can't tell which one was the error.
And then she goes on to say, I was just trying to strike a match under people and get them
to vote, to get everybody to vote.
The problem with this statement is that first comment showed a difference between the blacks
and people.
So saying that you're trying to get the people to vote, that doesn't mean anything because
it may not include them.
There are a lot of people trying to excuse this and justify it or whatever.
Sure, it's possible this was just some turn of phrase that got taken out of context.
I mean, it's possible.
I personally don't see it as likely, but it's possible.
Sure.
Whatever.
It's 2020.
A senator that has part of her county in his district, Juan Barnett, said, it's those kind
of things that people say until somebody brings it to their attention.
Okay so let me do that now.
If you are differentiating between any group of human beings and people, you're wrong.
You're racist, you're sexist, you're a bigot of some sort, I guarantee it.
Don't do that.
Shouldn't say anything like that.
You shouldn't think anything like that either.
We're building a new South.
We don't need this.
We've rejected this.
We're past this.
We're getting away from it.
This woman has held this office for 20 years.
Twenty years.
It is fair to ask how many times in those 20 years that thought process, differentiating
between the blacks and people, impacted what she did in that office of public trust.
That is a fair question to ask.
Now 20 years.
Incidentally, she's up for election.
That might be something the people of Jones County, Mississippi need to know.
This line of thought's been around for a long time, trying to subjugate a voice, trying
to take that voice away from them so they don't have a say in it.
And then we can continue to look down on them.
Well maybe it's because nobody can speak up for their interests because of gerrymandered
districts, because of voter suppression, because of removing polling places, because of motivating
the white vote out of fear of the blacks.
Maybe that's why their communities don't get the attention they deserve because we make
sure they don't because they're not represented.
This shows one of the critical flaws in representative democracy.
Anyway, this line of thought's been around for a long time and it needs to go away.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}