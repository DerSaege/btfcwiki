---
title: Let's talk about Trump vs the Pediatricians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wL6gij70OKc) |
| Published | 2020/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Viva Litzbow says:

- Trump administration insisted on schools reopening in the fall, threatening funding withdrawal if not in person.
- American Academy of Pediatricians' guidance emphasized the importance of safe reopening with CDC-aligned recommendations.
- Trump disagreed with CDC guidelines for schools, planning to pressure experts to change their guidance.
- American Academy of Pediatricians issued a statement co-signed by other organizations, prioritizing safe reopening based on science, not politics.
- They emphasized the need for evidence-based decision-making and leaving school reopening decisions to health experts and educators.
- The statement rebuked the President's stance and called for federal resources to ensure safe education.
- Withholding funding from schools not opening full-time in person was criticized as putting students and teachers at risk.
- The President's involvement in determining school reopening was deemed inappropriate; health experts should lead decision-making.
- The presidency's excessive power and the need to limit it, especially in light of the current administration's actions, were emphasized.
- The push to reopen schools was labeled as political, lacking a balanced nationwide approach to reopening based on varying threats.
- Adaptability and trust in health experts and educators were prioritized over the President's directives.

### Quotes

- "Science should drive decision-making on safely reopening schools."
- "Health experts do [decide when schools open], not the president."
- "The presidency has too much power. The Trump administration has shown this completely."
- "Eventually there will be another Trump."
- "The president attempting to control everything is going to make things worse."

### Oneliner

Viva Litzbow challenges the Trump administration's stance on school reopening, advocating for evidence-based decisions led by health experts and educators over political influence.

### Audience

Education advocates, parents, policymakers

### On-the-ground actions from transcript

- Advocate for evidence-based decision-making in school reopening plans and prioritize safety for students, teachers, and staff (implied)
- Support federal funding initiatives to ensure safe education during the reopening process (implied)
- Advocate for limiting presidential power to prevent political influences in critical decision-making processes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the politicization of school reopening and the importance of relying on experts for evidence-based decision-making.

### Tags

#SchoolReopening #HealthExperts #PoliticalInfluence #Education #FederalFunding


## Transcript
Well howdy there internet, Viva Litzbow again.
So today we are going to talk about Trump vs. the pediatricians.
The Trump administration, President Trump himself tweeted out that schools must open
in the fall or maybe we'll take their funding away.
Has to be in person, all of this stuff.
To defend this, everybody pointed to the American Academy of Pediatricians and what they said
and they're like, see they said they should return to school.
It's not what they said.
Either you just read the headline or you read a news article that spun what they said to
support the president.
What they said was that should be the goal and then they provided a laundry list of guidance
that would make it safe.
That guidance pretty much lined up with the CDC's guidance because contrary to a lot of
people's opinion, there is a consensus among experts on how best to deal with this.
The Trump administration, I disagree with the CDC on their very tough and expensive
guidelines for opening schools.
While they want them open, they are asking schools to do very impractical things.
I will be meeting with them.
So the president in his infinite wisdom is going to tell the experts what to do and he's
going to pressure them to change their guidance to make it less safe for children.
That's what he's going to do.
Because the American Academy of Pediatricians, I'm assuming, got tired of people spinning
what they said, spinning their statement, they issued a new statement.
And this is co-signed by the American Federation of Teachers, the National Education Association,
a whole bunch of people.
And it does start off with the same thing, you know, we should get kids back to school.
It's important.
That's the goal.
Of course it is.
It's everybody's goal.
They want to do it safely.
See that's the key part.
They want to do it safely.
Returning to school is important for the healthy development and well-being of children.
But we must pursue reopening in a way that is safe for all students, teachers, and staff.
Science should drive decision-making on safely reopening schools.
Public health agencies must make recommendations based on evidence, not politics.
We should leave it to health experts to tell us when the time is best to open up school
buildings and listen to educators and administrators to shape how we do it.
A one-size-fits-all approach is not appropriate for return to school decisions.
That's a pretty sharp rebuke of what the president said.
They flat out said he was wrong.
And unlike the news articles, I'll put the link to the statement so you can read it yourself.
Here's another bit to this that's entertaining.
President said he would withhold funding.
The same statement goes on.
We call on Congress and the administration to provide the federal resources needed to
ensure that adequate funding does not stand in the way of safely educating and caring
for children in our schools.
Withholding funding from schools that do not open in person full-time would be a misguided
approach, putting already financially strapped schools in an impossible position that would
threaten the health of students and teachers.
Really doesn't get more clear than that.
So if you have been pointing to the academy as the definitive proof because they said
they should open, which isn't what they said, I'm very curious to see how you're going to
deny that this is important.
This was your evidence last week because you didn't read it or misread it.
They're really clear on this.
The president has exactly zero business in determining when schools open.
Health experts do.
One of the issues with the presidency is that people have begun to see that office as more
than just the president of the Congress.
That's really what it is.
That's what it's supposed to be, the executive branch.
He's not a ruler.
That office was never meant to be that.
He's not supposed to know everything, not supposed to have his fingers in everything,
not supposed to direct everything, because one person can't know everything.
And that's when you're talking about a qualified individual.
When you're talking about Trump, who's bragging about passing a cognitive test, maybe there
should be more input from others.
The presidency has too much power.
The Trump administration has shown this completely.
It needs to be curtailed.
That needs to be one of the most important things on the agenda of whoever is in office
next, to limit their own power.
Because eventually there will be another Trump.
There will be another person who believes they are a stable genius.
That they have infinite wisdom.
When in reality, they don't know much.
Make no mistake about it, this push to reopen schools, it's political.
There's no logical reason to set a nationwide standard for when schools reopen when the
threat isn't balanced everywhere.
It's going to change.
We have to be adaptable.
If not, it's going to make things worse.
The president's tweets are going to make things worse.
The president attempting to control everything is going to make things worse.
I personally would trust everybody who signed on to this a whole lot more than I would trust
the president.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}