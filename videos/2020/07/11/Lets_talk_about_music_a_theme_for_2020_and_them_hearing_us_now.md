---
title: Let's talk about music, a theme for 2020, and them hearing us now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NiTf93deP9I) |
| Published | 2020/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Music captures moments in history better than anything, often not heard until years later because songwriters write passionately about their surroundings, not for an audience.
- Songs connect us to specific periods in time, playing out the idea of capturing a moment in every movie about the past.
- Introduces a song by Tom Burton, "I Bet You Can Hear Us Now," as a fitting theme for 2020.
- The lyrics of the song address standing up against injustice, corruption, and violence by those in power, particularly targeting law enforcement.
- Talks about the systemic issues, oppression, and violence faced by communities, calling for change and standing ground against it.

### Quotes

- "I bet you can hear us now."
- "Back to how it was before, we're gonna stand our ground."
- "A system corrupted by lies."
- "Blatant aggression, it seems their obsession."
- "We ain't gonna take any more."

### Oneliner

Beau talks about music's power to capture history, introducing a song that resonates with the theme of standing up against injustice and corruption in 2020.

### Audience

Music enthusiasts and advocates for social justice.

### On-the-ground actions from transcript

- Join protests against injustice and corruption (implied).
- Support community-led initiatives for change (implied).

### Whats missing in summary

The emotional impact of music in reflecting and addressing societal issues.

### Tags

#Music #SocialJustice #Injustice #Power #Corruption #Community


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about music and how it captures a point in history sometimes,
a moment in time, better than anything.
The sad part about this phenomenon is that a lot of times the songs, we don't really
get to hear them until 10 years or 20 years later because the songwriter, they wrote about
what they saw around them and it was something they were passionate about.
Wasn't really for an audience, so people didn't get to hear it until much, much later.
This is something that happened several times throughout history.
And the idea that a moment can capture a point in time like that, it's played out in every
movie about the past ever.
If you believed movies you would think that every Huey in Vietnam was blasting CCR the
entire time because those songs connect us to that period in time and they capture the
moment.
So there's a song that I heard recently and I think it is very fitting.
It could be the theme of 2020 from where I'm standing.
So we're going to listen to it.
You are about to hear Tom Burton perform his song, I Bet You Can Hear Us Now.
I bet you can hear us now, as we're pouring out into the streets and we're gonna tear
it down.
We're all marching to the same beat and we won't be turned around.
Back to how it was before, we're gonna stand our ground.
We ain't gonna take any more.
Look back through history, witness the misery imposed by those from the top.
Be a world leader, supremacy feed us the judges, the courts, or the cops.
Epidemic extortion of epic proportion, a system corrupted by lies.
Using violence for silence and total compliance is justice from death to our cries.
I bet you can hear us now, as we're pouring out into the streets and we're gonna tear
it down.
We're all marching to the same beat and we won't be turned around.
Back to how it was before, we're gonna stand our ground.
We ain't gonna take any more.
They claim they're prestigious, their actions egregious, their power is what they preserve.
A place for bad apples held holy as chapels, they're killing the people they serve.
Blatant aggression, it seems their obsession, they're killing three people a day.
The good cops who speak up have unions that speak up, they're fired for what they might
say.
I bet you can hear us now, as we're pouring out into the streets and we're gonna tear
it down.
We're all marching to the same beat and we won't be turned around.
Back to how it was before, we're gonna stand our ground.
We ain't gonna take any more.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}