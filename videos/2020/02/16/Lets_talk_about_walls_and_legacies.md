---
title: Let's talk about walls and legacies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S_VM-KApNrg) |
| Published | 2020/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- August 13th, 1961: the Berlin Wall went up, seen as a symbol of oppression dividing a city, country, and world.
- The Berlin Wall separated West Berlin from East Germany, symbolizing oppression until it fell in 1989.
- Trump's wall on the southern border is questioned for its potential legacy like the Berlin Wall.
- The wall's potential legacy may not be as weighty due to hardened hearts and prioritization of money over morality.
- The effectiveness of Trump's wall is doubted as reports show various ways people easily breach it.
- People are actively choosing to go to the wall because it's easier to use it than to go around it, indicating its ineffectiveness.
- The wall being defeated even before completion raises concerns about its actual utility.
- Cutting through the wall with saws and scaling it with rebar-made ladders are some tactics used to breach it easily.
- The wall might be remembered not as a symbol of oppression but as a testament to incompetence and fiscal irresponsibility.
- Beau predicts Trump's wall will be associated with his legacy of wasteful spending and inefficiency.

### Quotes

- "The wall is not built yet. If you want a good gauge of how easily the wall is defeated, understand they're choosing to go to the wall."
- "I don't see this wall being recorded as a great symbol of oppression, I think it's going to be recorded as a massive symbol of incompetence."
- "It will be a symbol of massive fiscal irresponsibility."

### Oneliner

Beau questions the legacy of Trump's border wall, foreseeing it as a symbol of incompetence and fiscal irresponsibility rather than oppression.

### Audience

Border wall critics

### On-the-ground actions from transcript

- Contact local representatives to voice opposition to the construction of the border wall (suggested).
- Support organizations advocating for immigration reform and more effective border security measures (exemplified).

### Whats missing in summary

The full transcript provides more in-depth analysis on the potential legacy of Trump's border wall, touching on themes of oppression, morality, effectiveness, and wasteful spending.

### Tags

#BorderWall #Legacy #Incompetence #FiscalIrresponsibility #Oppression


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we're gonna talk about walls and legacies.
On August 13th, 1961, when the Berlin Wall went up, it almost immediately became seen
as a symbol of oppression.
It divided a city, it divided a country, it divided the world in a lot of ways.
wall separated much, much more than just breaking West Berlin off from the rest of East Germany
and creating that little enclave.
It became oppression, physically manifested, and it stood and it carried that image until
1989 when it came down, the question we have to ask is whether or not the wall on the southern
border, Trump's wall, whether or not that's going to have that same legacy.
It's hard to guess how history is going to record something.
I don't believe Trump's wall will have that image.
I don't believe the Southern Iron Curtain is going to carry the same weight as the Berlin
Wall.
Not because it's any less evil, but because our hearts have grown harder and other things
have become more important, money.
has become more important than morality in a lot of ways and how we judge things.
What do we know about the wall, I mean the parts that have been built?
We know they're using these to cut through them.
We know they're going under them.
We know that they've blown down.
We know they're just scaling them.
We just found out they're taking rebar, making ladders, and hooking them to the top, using
it to climb over, dropping another one on the other side, going down, and that they're
leaving the ladders in place because the rebar matches the texture and color of the wall.
So Porter Patrol has a hard time spotting it.
And yeah, that shows how easily the wall is defeated, but there's something that people
aren't catching. The wall is not built yet. The wall is not built yet. If you
want a good gauge of how easily the wall is defeated, understand they're choosing
to go to the wall. They don't have to. They're choosing to go to the wall
because it's easier to use the wall than it is to go around it. That's a
bad sign when you're talking about the effectiveness of a fortification. When
the opposition is choosing that route it means that it is completely
ineffective. In this case it may actually be aiding them because what they have
realized is that once they use the
Recept saw to cut through they put the
piece back so there's just a really easy
hole to get in and out of as long as
you remember where you put it. Because
Border Patrol can't see it because of how
the wall is made. So I don't see this
wall being recorded as a great symbol of
of oppression, I think it's going to be recorded as a massive symbol of
incompetence, Trump's legacy, what he wanted, that giant vanity wall, that
thing that would be forever associated with him.
Oddly enough, I think it's going to be like the rest of his reputation.
It will be a symbol of massive fiscal irresponsibility.
This thing cost billions and is so bad at doing what it's designed to do, people are choosing to go to it to get
through.
through. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}