---
title: Let's talk about what California can learn from the French Foreign Legion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=05kxe2IH00g) |
| Published | 2020/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparing the state of California to the French Foreign Legion, suggesting lessons to be learned.
- The Foreign Legion is known for giving individuals a new identity and a fresh start after leaving.
- Despite some members having criminal backgrounds, they put their lives on the line and are changed by their experiences.
- Mentioning Dien Bien Phu as a significant battle that marked the end of French involvement in Vietnam.
- Advocating for California's AB 2147 bill, which aims to create a California firefighter legion for inmates who are trained as firefighters.
- Emphasizing the benefits of allowing these inmates to clear their records and become firefighters, reducing recidivism.
- Expressing belief in the positive impact of providing second chances through skill-building opportunities.
- Stressing the importance of implementing this legislation as a way to help both the community and the state.
- Acknowledging past legislative challenges in California but underlining the potential of this bill to be a significant benefit.
- Concluding with a call to action for California not to miss out on this beneficial legislation.

### Quotes

- "Imagine being able to cut down recidivism."
- "If you can trust an inmate with an axe, probably not a danger."
- "There's no downside to this."
- "This is something that's helping the community and it's helping the state while it's helping them."
- "This seems like a no-brainer."

### Oneliner

Beau suggests California learn from the Foreign Legion and implement AB 2147 to offer inmates a second chance through firefighting, reducing recidivism and benefiting the community and state.

### Audience

California policymakers, activists

### On-the-ground actions from transcript

- Support the implementation of AB 2147 for the creation of a California firefighter legion (suggested).
- Advocate for legislation that provides second chances and skill-building opportunities for inmates (exemplified).

### Whats missing in summary

The full transcript provides more context on the history and impact of the French Foreign Legion, as well as Beau's personal reflections on the importance of second chances and community benefits.


## Transcript
Well howdy there internet people, it's Beau again. So tonight we're going to talk
about what the state of California can learn from the French Foreign Legion.
The Foreign Legion has a mythology all of its own and it has an image and
popular culture. Thing is it's been around for centuries, centuries. It is
It's said that when a man leaves the Legion, he's a new man.
That is quite literally true.
In fact, before he leaves, because he has a new name and a clean slate, a second chance.
Most Americans are familiar with the Legion through bad movies or maybe from Yan Bin Fu.
The idea of the ideal Legionnaire is that he is never a criminal but seldom an angel.
Reality a lot of them are criminals.
But they put their lives on the line.
it changes them. It doesn't just change their name. It has anything. When you face danger
like that, it changes you. Dien Bien Phu, for example, was a just vicious battle in
what became Vietnam. It marked the end of French involvement and set the stage for American
involvement in Vietnam.
The battle was lost because the French believed the Vietnamese did not have
anti-aircraft, and they did, and that was a pretty catastrophic failure of
assessment right there.
But the point is, these men who have this image of these hardened killers,
these people that are just criminals.
They walk out with a clean slate.
The Legion has existed for centuries.
If there was a lot of problems with former Legionnaires breaking the law again, I would
imagine the French would do away with it, but they haven't.
In California right now, there's a bill, AB 2147.
Bill's whole purpose is to create a California firefighter legion.
Right now, there are inmates who are trained as firefighters, and they help fight the wildfires.
This bill would allow them to expunge their records quickly and then get hired on with
state firefighting units, become firefighters for the state.
I think California would be remiss if it doesn't do this.
Imagine being able to cut down recidivism.
walking out having paid their debt to society and then some, and they're walking out with
a skill set that can get them a decent job, which means they won't be in poverty, which
means they'll be less likely to commit crime, which will keep them out of jail.
This seems like a no-brainer.
If you can trust an inmate with an axe, probably not a danger.
In the absence of real criminal justice reform, this is amazing.
I personally don't believe that somebody should have to put their life on the line
to get a second chance.
However, given the current situation, if the option is available and it's voluntary,
it is what it is, and this is real service.
This is something that's helping the community and it's helping the
state while it's helping them.
There's no downside to this.
California has had some trouble with legislation in the past.
This is one they don't want to miss.
This is something that could be a great benefit to the state.
Anyway, it's just a thought.
It's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}