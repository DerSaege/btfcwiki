---
title: Let's talk about driving Dixie down....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ni3HWJxE6kw) |
| Published | 2020/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the history of the Dixie Highway, a highway network in the United States running from Michigan to Miami, created in the early 1900s.
- Notes that Carl G. Fischer, the mind behind the highway, previously worked on the Lincoln Highway, suggesting that the choice of the name Dixie was not to glorify it.
- Mentions that the term Dixie refers to the region south of the Mason-Dixon line where slavery was legal.
- Talks about how the Dixie Highway did not gain the same tourism attention as other famous highways like Route 66.
- Acknowledges that historical events tied to Dixie, such as opposition to civil rights, make it a controversial symbol.
- Miami recently renamed its portion of the Dixie Highway to the Harriet Tubman Highway, which Beau praises as a positive step forward.
- Speculates on the economic and cultural benefits that could arise if other jurisdictions along the highway also choose to rename their sections.
- Suggests that renaming the highway could attract more tourists and revenue to rural communities along the route.
- Expresses curiosity about how different areas will respond to the renaming, seeing it as a test of whether they choose to honor a dark past or a heroic figure.
- Encourages moving forward by letting go of glorifying a period in American history associated with slavery and racism.

### Quotes

- "Miami has just elected to rename theirs, it will now be the Harriet Tubman Highway and I think that is just awesome on so many levels."
- "Economically, it makes sense. Morally, it makes sense. Historically, it makes sense."
- "There's no reason to keep this when it could be used to demonstrate that we do have a new South, that the country is moving forward."

### Oneliner

Beau explains the history of the Dixie Highway and advocates for renaming it to honor progress and reject a dark past.

### Audience

History enthusiasts, community activists

### On-the-ground actions from transcript

- Advocate for renaming controversial symbols in your community (exemplified)
- Support initiatives that celebrate diversity and progress in your area (exemplified)

### Whats missing in summary

The full transcript provides a detailed historical context and rationale for renaming the Dixie Highway, which may not be fully captured in this summary.

### Tags

#DixieHighway #History #Renaming #Progress #CommunityEngagement


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about driving Dixie down.
For those that don't know, there is a highway network
in the United States called the Dixie Highway.
It runs from Michigan to Miami, north-south.
This highway system came to being in the 1900s,
early 1900s, 1915-ish, around there.
It was the brainchild of Carl G. Fischer whose first project
was the Lincoln Highway running east-west.
It's pretty clear that somebody whose first project
was the Lincoln Highway is probably not choosing
the name Dixie to glorify it.
It was just the region it was headed.
For those overseas who may not know,
Dixie is the area of the country
that was south of the Mason-Dixon line.
The Mason-Dixon line was the dividing line between the north and the south.
South of the Mason-Dixon line, well that's where slavery was legal.
Now this came into being in the early 1900s.
This wasn't named this in the 1950s and 1960s when a lot of things went up that had to do
with Dixie that were really not historical based.
It was more about opposition to civil rights and just sending a signal, you all better
stay in your place.
And that's, just so you know, a lot of the Confederate monuments that people are talking
about that are getting torn down, that's when they went up.
They didn't actually go up like right after the Civil War.
It was a hundred years later and it was really just as a symbol to black Americans in the
South.
So why am I talking about all this?
What is the Dixie Highway?
what does this matter today? It's a historic network of roads because it was
built by different jurisdictions, okay? But it doesn't get tourism like Route 66
or Route 1 or any of the other ones. This one just doesn't get it. I can't
imagine why. Can you imagine people stopping to take photos along this route
today glorifying the Dixie Highway. Probably not. Nobody wants to bring up
images of the Little Rock Nine or the Alabama bus boycott. It's just not
something that people want to do. It carries a very negative connotation.
Miami has just elected to rename theirs, it will now be the Harriet Tubman Highway and I think that is just awesome on
so many levels.  One, with them renaming the route, that's it. They were the destination. So they've taken control of
it.
it. Now we have to see whether the other jurisdictions that own different pieces of the highway along the
way will follow suit. If they do, this could turn out to be an economic godsend to a lot of these
communities. There aren't a lot of tourist destinations in rural Georgia, but this could
bring them money. You know, a lot of people, myself, I, when I have the opportunity, I will
won't drive Route 66 just because it's more interesting than the highway. Could
happen here too if people let go of the past, if people chose to make it something
worthy of driving. It's kind of cool because we're going to get to see which
areas of the south are still super super racist because there is no downside to
this. Economically, it makes sense. Morally, it makes sense. Historically, it makes sense.
The only reason to hold on to it would be glorifying a really horrible period in American
history. It's the only reason. There's no other reason to do it. They can glorify that
Horrible era or they can glorify a hero of American history. That's their choice.
And I'm very curious to see how it plays out. I really am.
I'm bringing this up because this is going to be one of those things that for
the next five years people are going to argue about. But the key points to take
away and to have in your pocket when this argument shows up is that no, it's
not a historic thing from the Confederacy. It's not. It wasn't built until long after
the Confederacy was gone. It wasn't even a thing that intended to glorify the South
or Southern heritage. Just the region it was headed. That's it. There's no reason to keep
this when it could be used to demonstrate that we do have a new South, that the country
is moving forward.
And I do find a bit of humor in the idea that it is beginning where the road ended, in Miami.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}