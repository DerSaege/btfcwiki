---
title: Let's talk about Bernie, Trump, and Russians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E7t45Tdm548) |
| Published | 2020/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the debate surrounding Russian interference in elections and their potential support for Bernie and Trump.
- Explains that countries try to influence other countries' leadership to advance their foreign policy.
- Mentions historical examples of countries supporting leaders in other nations based on shared interests.
- Points out that Russian organized crime is strategic and plays chess, not poker like Italian organized crime.
- Suggests that Russians may support both sides in an election to achieve their goals.
- Emphasizes that understanding what the Russians want is key, not projecting motives onto them.
- States that the Russians are interested in influencing foreign affairs, not degrading American democracy.
- Argues that Bernie being the peace candidate benefits the Russians in terms of foreign policy.
- Raises the importance of candidates maintaining integrity and not being influenced by foreign support.
- Mentions U.S. counterintelligence's role in monitoring candidates to prevent direct foreign influence.

### Quotes

- "Countries try to influence other countries' leadership to advance their foreign policy."
- "Understanding what the Russians want is key, not projecting motives onto them."
- "Bernie being the peace candidate benefits the Russians in terms of foreign policy."
- "The real question is whether candidates have the integrity to not be influenced by foreign support."
- "U.S. counterintelligence's role is to monitor candidates to prevent direct foreign influence."

### Oneliner

Beau explains Russian interference, potential support for Bernie and Trump, and the importance of candidates' integrity in not being influenced.

### Audience
Voters, Political Analysts

### On-the-ground actions from transcript

- Monitor candidates for foreign influence (implied)

### Whats missing in summary
An in-depth analysis of the impact of Russian interference on democratic processes and potential solutions.

### Tags

#RussianInterference #Elections #BernieSanders #Trump #ForeignInfluence #Integrity


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about Bernie, Trump, and Russians.
Dun dun dun!
Everybody be scared.
So the debate right now is would the Russians really do this?
Of course they would.
Of course they would.
Would they try to interfere in our elections?
Of course they would.
influence them. Every country does and we try to do it to other countries. Of
course. Would they really support Bernie? Probably. I mean that that makes sense.
Why? For the same reason they supported Trump. The effect from their point of
view isn't going to be any different. Not really. Trump's a little bit better for
them, but not by much. The reality is intelligence agencies, they attempt to
influence other countries leadership. That's what they do. They try to advance
their nation's foreign policy. That's their job. Having somebody of similar
interests in the leadership of another country, that's beneficial to them. Let's
Let's do it this way.
Why were the Russians in Afghanistan?
Leadership questions, 78, I think.
There was a coup that brought communist power.
They wanted to back them.
Why were we in Panama?
Leadership questions.
Why were we in Iraq?
Leadership questions.
What's the issue with North Korea?
Leadership questions.
If we will go to war over it, we will certainly devote some covert resources.
The idea that it's not happening is just saying, I do not understand the international
stage.
Of course it's happening.
Why would they support Bernie?
Why would the Russians support Bernie?
The Republicans right now have two answers to this.
One is that he's a socialist, first he's actually not, none of his policies that he's
stated are socialist.
That, whatever, even let's just say he is, it's not the Soviet Union anymore, it's
Russia, and Russia isn't socialist.
That doesn't even make sense.
The other is that he would be easier for Trump to beat.
You're thinking like an American.
One of the smartest things I've ever heard about organized crime was a guy who spent
a lot of time studying them and he said the difference between the Italian mafia, Italian
organized crime and Russian organized crime could be summed up by the fact that Italian
organized crime sat around and played poker and the Russians sat around and played chess.
The Russians don't gamble.
They come up with multiple avenues to get what they want.
They would support both sides.
So what do they want?
That's the question.
We keep trying to force our motives onto them.
What do they actually want?
Do they want to degrade our democracy?
No, they don't care.
They want to influence our stance in foreign affairs.
They want to have somebody that is beneficial to them when it relates to foreign affairs.
They do not care what happens domestically.
Bernie is the peace candidate, right?
He's the guy speaking out against the military and industrial complex, he's the guy saying
he wants to reduce our footprint overseas, he's the guy saying he wants to reduce the
frequency with which we force project.
Yeah, of course, I mean, all that's good for the Russians.
It's also good for the American people.
have to be either or. Why did they support Trump? Because on the international
stage, he's incompetent. He can be led around by the nose, which is beneficial to
them. That's it. There's no great mystery here. This happens in every election.
It's nothing shocking, really. The real question at the end of the day is
is whether or not the candidates themselves have the integrity to not play ball with them,
to not be influenced by that support, to not allow their policies to be changed by that
support, to not, well I'd sit there and listen.
That's the real question.
It deals with the integrity of the candidates involved.
U.S. counterintelligence, it doesn't matter how many resources you grant them, they're
They're not going to be able to stop other nations from trying to influence the election.
They're not going to be able to do that.
The best they can do is keep tabs on the candidates themselves and make sure that they're not
being influenced directly because then that is a national security threat.
Now they've done a pretty good job of this over the years.
They probably won't do very well this election because, you know, the Senate stopped measures
to help them and then their leadership was just gutted.
So they're probably going to do really bad at it this time around.
But I would suggest not dismissing the idea that the Russians support Bernie.
He's the peace candidate.
The same reason you support him, if you do, is the reason they do.
I would suggest that there are a couple of other Democratic candidates who, if they had
a chance, they'd also be getting support from Russia.
And there probably are a couple others that are getting support.
It hasn't been identified yet because I can think of a couple more candidates that would
be getting passive support.
Now this doesn't mean that they're working with them.
It doesn't mean that they're meeting them in hotels.
It doesn't mean that they're promising them anything.
It just means that the Russians would like them in a position of authority because their
foreign policy isn't combative.
That's kind of a no-brainer.
It really boils down to, well, like Richard Nixon said, is your president a crook?
Anyway, it's just a thought.
Now have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}