---
title: Let's talk about Trump, the Rosenbergs, Cohn, McCarthy, and Stone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nRpfU_idUw0) |
| Published | 2020/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the interconnectedness of historical figures like the Rosenbergs, Roy Kahn, Joe McCarthy, Roger Stone, and Donald Trump.
- The Rosenbergs were highly effective spies for the Soviets in the 20th century, recruiting other spies and providing high-value information.
- Roy Kahn, one of the prosecutors in the Rosenberg case, was known for his hard-right stance and association with Joe McCarthy during the Red Scare.
- Kahn also worked with Roger Stone on Reagan's campaign and later represented Donald Trump in New York.
- Treason in the United States is narrowly defined, making it difficult to charge individuals with this offense.
- The Rosenbergs were not charged with treason but with conspiracy to commit espionage, carrying severe penalties.
- In the U.S., treason requires allegiance to the United States and an act of war or support for a war effort.
- Treason could involve a scenario where a person in public office aids a foreign government's act of war against the U.S.
- The Pentagon considers cyber attacks as potential acts of war, raising concerns about today's security landscape.

### Quotes

- "Treason in the United States is extremely specific."
- "Foreigners, people who do not owe allegiance to the United States, can't be charged with treason."
- "Acts of war don't happen that often."
- "The Pentagon has argued that cyber attacks are an act of war."
- "It's worth noting that the Pentagon has argued that cyber attacks are an act of war."

### Oneliner

Beau delves into the historical connections between spies, prosecutors, and politicians, shedding light on the narrow definition of treason in the United States and the implications of cyber attacks as potential acts of war.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Research historical events and figures mentioned (suggested)
- Stay informed about cybersecurity issues and potential threats (suggested)

### Whats missing in summary

Insights on the long-term impact of espionage and political connections in shaping historical narratives. 

### Tags

#Espionage #Treason #HistoricalConnections #Cybersecurity #PublicOffice


## Transcript
Well, howdy there, internet people.
It's a bubble again.
So tonight we're going to talk about the Rosenbergs, Roy
Kahn, Joe McCarthy, Roger Stone, and Donald Trump.
Because oddly enough, all of them are connected.
They are all part of one historical chain.
OK.
So let's start with the Rosenbergs.
Who were they?
Simply the probably the most effective spies of the 20th century, I mean realistically.
They were a husband and wife team.
They spied for the Soviets against the United States.
And they were just amazing, I mean seriously.
Not just did they hand over information, all spies do that.
They recruited other spies.
And the information they handed over was high value.
We're not talking about little bits and pieces, little bits of human intelligence that doesn't
matter.
No, we're talking about serious stuff like the plans to our jets, the internal workings
of components to some of our most devastating weaponry.
They handed over secrets related to the A-bomb.
They were eventually caught.
Roy Kahn was one of the prosecutors on that case.
Now, as we're about to find out, this is a hard right guy.
And he would definitely charge them to the max.
They were not charged with treason.
If you followed this channel for any length of time,
you've probably seen me in the comments section saying,
treason in the United States is extremely specific.
It's actually really hard to get charged
treason in the U.S. There's a lot of things that have to line up. Okay, so Roy
Kahn also worked for Joe McCarthy. Joe McCarthy of the guy who personified the
Red Scare. He worked for him during the hearings and McCarthy pretty much saw
Soviet spies, well, everywhere. Everybody was a communist. After that, he worked
with Roger Stone on Reagan's campaign. Roy Kahn and Roger Stone, there's the accusation
that they engaged in, let's just call it unethical behavior. He was also a representative for
a certain real estate developer when he got to New York, Donald Trump. That's Roy Kahn.
So this is a man that would definitely sink the Rosenbergs as hard as he could. Didn't
charge them with treason. I think they went down for conspiracy to commit
espionage is what they went down for. Either way the point being it didn't
really matter to them both carrying pretty stiff penalties. In the United
States, U.S. Code 18-2381. Treason. Okay. Whoever owing allegiance to the
United States levies war against them or adheres to their enemies giving them aid
and comfort within the United States or elsewhere is guilty of treason. Two key
things. Foreigners, people who do not owe allegiance to the United States, can't be
charged with treason. The other part of this levies war. It has to be during an
act of war or in support of an actual war effort. Simple espionage isn't
enough to for treason.
Okay, so now that we understand why, what would be an act of treason?
An act of treason would be if somebody owed allegiance to the United States, let's say
they were in public office, and a foreign government was committing an act of war.
And that person in public office used their position to degrade the United States' ability
to fight back against that act of war, to aid it, to give them comfort, to help.
That would be something that a prosecutor could present as treason.
It's very rare that that happens because acts of war don't happen that often.
It's worth noting that the Pentagon has argued that cyber attacks are an act of war.
Something that should give everybody pause, given today's climate.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}