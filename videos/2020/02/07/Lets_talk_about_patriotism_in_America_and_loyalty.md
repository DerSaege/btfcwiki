---
title: Let's talk about patriotism in America and loyalty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_8t6PJx0Ox0) |
| Published | 2020/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- American patriotism has become muddled in the United States, often confused with another term.
- American patriots in 1776, such as those at Yorktown, Lexington, and Concord, had no loyalty to the United States, as it didn't exist until 1787.
- The loyalty of American patriots was to the people, the countryside, and the principles, not to politicians or governance bodies.
- American patriots throughout history understood that governments are a creation of the people and sometimes need correction.
- Patriotism is not blindly obeying the government; it's about holding onto principles and correcting the system when needed.
- Nationalism, not patriotism, is blindly following government orders and giving up rights.
- The government's appeal to patriotism to gain control is akin to a child wanting cake for dinner - it's a manipulation tactic.
- Individuals hold the power in a democracy, and the government appeals to patriotism because they need the people.

### Quotes

- "Patriotism is not blindly obeying the government; it's about holding onto principles and correcting the system when needed."
- "Nationalism, not patriotism, is blindly following government orders and giving up rights."
- "The government's appeal to patriotism to gain control is akin to a child wanting cake for dinner - it's a manipulation tactic."

### Oneliner

American patriotism is about loyalty to people and principles, not blind obedience to the government; nationalism is the opposite.

### Audience

American citizens

### On-the-ground actions from transcript

- Correct the system when needed (implied)
- Uphold principles over blind obedience (implied)
- Understand your power in a democracy (implied)

### Whats missing in summary

The full transcript provides a deep dive into the historical origins of American patriotism and the importance of loyalty to principles over blind obedience to government.

### Tags

#AmericanPatriotism #Government #Nationalism #Principles #Democracy


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about patriotism,
specifically American patriotism,
because patriotism means different things
in different parts of the world.
In the United States, the term has become muddled.
It has been confused with another term and they're not
the same, and it's very easy to illustrate
that they aren't the same, but it's uncomfortable
people to realize this at times. American patriotism started in the 1760s. However,
using that period would take us away from our point. So we're going to use a different date.
We're going to use 1776. I think that everybody would agree that American
patriots existed in 1776, that goes without saying.
Here's the thing, those who fought at Yorktown, Lexington, Concord, not a single one of them
held any loyalty whatsoever to the United States.
None.
It's an indisputable historical fact.
None of them held any loyalty to the U.S.
The reason is because it didn't exist.
The United States didn't come into being until 1787, 1788 really, that's when it was ratified.
When they spoke of patriotism, they didn't mean loyalty to a country the way we think
of it.
When they spoke of country, they meant the country, the countryside, those around them,
the people, their neighbors, that's who they held loyalty to, an idea, an idea, a principle.
what patriotism is. They held no loyalty to some politician, to some body of
governance. They held no loyalty to D.C. It didn't exist. The United States didn't
exist.
The other thing that American patriots throughout our history have all shared
is that they understood that governments in general, well, they're a child of the
people and sometimes children need to be corrected. That's what an American
patriot is. Yeah, in 1776 it was the Tree of Liberty and all of that. More recently
it was I have a dream. Martin Luther King was a person who held his loyalty to the people,
those around him, his neighbors. He understood that government was a child of the people
and he corrected it. As we evolve, we understand that violence is not necessary for everything
And that ideas travel faster than bullets.
So when you hear people talk about patriotism, and their version is obeying the government
and doing whatever those in power say, they're not patriots.
That's the opposite of patriotism.
That's nationalism, and that's what the United States has fostered.
the greatest trick the powers that be have pulled.
They've tied your principles to their wishes.
As we move forward over the next year, understand that when those in D.C. appeal to your patriotism,
get you to sign away your rights, to give them a little bit more control, to do things
you know are wrong and violate your principles.
Understand that's a lot like a child wanting to eat cake for dinner every night.
You're the adult.
Government is a child of the people.
You are the adult.
get to decide. You have the power. The reason they are appealing to you is because you have
the power. They need us. We don't need them. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}