---
title: Let's talk about pettiness and something that is clear satire....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9MbaAb-O66E) |
| Published | 2020/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes the American political landscape as resembling "mean girls" with pettiness and revenge.
- Mentions a possible document from within the administration, offering insight into the President's mindset.
- Points out the President's creative skills in graphic arts and his primary target, Joe Biden.
- Notes a list of reasons the President dislikes Biden, including references to Rudy Giuliani and John Bolton.
- Mentions the anger towards Adam Schiff and describes the treatment of AOC and Ted Cruz.
- Talks about Mitch McConnell's relationship with Moscow and Mitt Romney's integrity.
- Suggests potential envy towards those admired turning into rivals.
- Mentions the author's behavior towards the Constitution and anger towards Nancy Pelosi and other world leaders.
- Raises questions about the authenticity of the document and its significance.

### Quotes

- "American political landscape resembling 'mean girls' with pettiness and revenge."
- "It's big if true."
- "The President's arch enemies and allies rated in a mysterious notebook."

### Oneliner

Beau dives into a document possibly from within the administration, shedding light on the President's mindset towards political figures, resembling a high school drama with pettiness and revenge.

### Audience

Political observers, concerned citizens.

### On-the-ground actions from transcript

- Read the document to understand potential insights into the President's mindset (suggested).
- Pay attention to the behaviors and attitudes of political figures mentioned for a deeper understanding (suggested).

### Whats missing in summary

The nuances and detailed analysis of the relationships and attitudes between the President and various political figures.

### Tags

#AmericanPolitics #President #Document #Insights #PoliticalFigures #PettyBehavior


## Transcript
Well howdy there internet people, it's Beau again.
Tonight we're going to talk about the American political landscape.
There are many suggesting that it has devolved into mean girls
with pettiness
running rife
and with the administration, the president in particular, suggesting that he may
take revenge
and make people pay the price for opposing him during the impeachment.
We have stumbled upon what may be
a document from within the administration,
possibly made by the president's own hand,
if true,
would provide us some great insight
into the mind of the leader of the free world.
Let's take a look.
The president has always been a very creative man,
very skilled in the graphic arts.
Learned that through his years of successful business
and pitching ideas.
The primary target for the President's wrath appears to be, still, Joe Biden.
There's a list of reasons the President dislikes the man, if this was in fact made by the President.
I would draw your attention to the upper right hand corner in the note that says, talk to
Rudy about getting even.
Rudy himself has not been spared the anger contained within this notebook.
You'll notice that someone has attached googly eyes to the attorney.
Notes include needs to STFU and the to-do list, talk about burner phones, only call
me when you're alone, and revoke passport.
There aren't many people in the United States who have the authority to revoke a passport.
One of the President's arch enemies is Shifty Schiff, who has, by the author of this document,
harassed me bigly over a perfect phone call and must pay the price.
is often referred to as the made-up fake transcript person, known for being very unfair.
The president's former advisor, John Bolton, also known as Loudmouth Bolton, has been rated
a star in the notebook and is referred to as the Deep State CIA guy.
As reading the numbered complaints about Mr. Bolton, we must remember that this administration
demands utmost loyalty from those within it.
This is possibly one of the more shocking pages in the notebook.
It appears that whoever was behind this document interprets AOC's refusal to attend Trump
events as standing him up.
The subtext here is pretty clear and something that we should possibly pay attention to.
Senator Cruz, who is a reluctant ally of the President, at least publicly, has apparently
scared the President in many ways.
There is the rumor that Senator Cruz is in fact the Zodiac Killer.
And then there's something about the Kennedy assassination in here, I'm not really sure.
Mitch McConnell is another public ally of the President, also rating a star.
However it appears that Mr. McConnell may have developed a better relationship with
Moscow than the President, and the President has in turn, well, you see.
Senator Mitt Romney is being lauded in the press right now as one of the few men to maintain
his integrity throughout this whole thing.
The author of the notebook, possibly the president, seems to think otherwise.
It's not uncommon to grow to detest those that you try to emulate and try to be like.
your heroes become your rivals.
Yeah I got nothing on that one.
Apparently, sad mad, faxed someone, the author of this document, a copy
the Constitution every hour for the first hundred days of his office.
And he is mean, something we should all pay close attention to.
The gullible grandma may be in for promotion, perhaps the ambassadorship in Ukraine.
Speaker of the House, Nancy Pelosi seems to have drawn the most anger from the author
of the book and it uh well again you can read it yourself
the troubling handwriting here I'll read this world leaders not my friends Erdogan for bullying
me out of Syria. Kim Little Kim Rocketman doesn't return my calls. Trudeau, Ivanka and Melania.
Boris stole my fashion. Friends with Trudeau and French Guy. Tim Apple for never meeting me.
Is this an authentic document? Well, we'll never know. But it's big if true.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}