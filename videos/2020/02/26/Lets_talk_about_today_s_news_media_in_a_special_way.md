---
title: Let's talk about today's news media in a special way....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KuppkXTXx-I) |
| Published | 2020/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Introducing a new experiment on the channel, discussing the media and its portrayal of news.
- Criticizing the current media for bringing in biased experts without disclosure and portraying non-experts as experts.
- Contrasting today's media mission with its historical purpose of conveying information, providing context, and instilling calmness.
- Expressing concern over the media's sensationalism and partisanship in shaping news.
- Imagining how today's media might have covered critical historical events like the Cuban Missile Crisis.
- Presenting a fictional news segment from October 22, 1962, focusing on the Cuban missile crisis.
- Featuring guests advocating for aggressive actions against Cuba, criticizing President Kennedy's pacifist approach.
- Mocking the media's reliance on unqualified individuals presented as experts based on irrelevant factors like accents.
- Showcasing fear-mongering tactics by the media, spreading misinformation about nuclear threats to instill panic.
- Concluding with a commercial break parody and a segment with a Bay of Pigs veteran advocating for violent intervention in Cuba.

### Quotes
- "Nothing to fear, but people who are slightly different than us."
- "Murder on, man, murder on."
- "We owe them that much. I agree with the Murder on Technology's representative."
- "We need to spread democracy."
- "Indeed, true patriots in this country know this soft-handed Eastern elite attitude is just going to lead to the destruction of the United States."

### Oneliner
Beau conducts a unique media experiment critiquing sensationalism, partisanship, and fear-mongering in news reporting during a fictionalized Cuban Missile Crisis broadcast.

### Audience
Media consumers

### On-the-ground actions from transcript
- Fact-check news sources (implied)
- Analyze media bias and conflicts of interest (implied)
- Advocate for accurate and ethical journalism (implied)

### Whats missing in summary
The full transcript provides an in-depth look at how media sensationalism and bias can shape public perception of critical events, urging viewers to critically analyze news sources and narratives.

### Tags
#MediaCritique #Sensationalism #Bias #Journalism #FictionalNews #CubanMissileCrisis


## Transcript
Well howdy there internet people, it's Beau again.
So tonight's going to be a little bit different.
We're going to do something on this channel tonight that we've
never done before, anything even remotely like it.
We're going to conduct a little experiment.
And I have some other people who you may know helping me out
with this.
Tonight we are going to talk about the news
like we normally do.
However, tonight we're actually going to talk about the news,
the media itself.
You know, today's media, they often bring in experts
who have a clear conflict of interest
with the subject matter that's being talked about,
and they don't really disclose it.
They often bring in people who are not experts,
but they portray them as if they are.
And it seems as though the mission of today's media,
rather than what it used to be,
you know, it used to be to convey information,
provide context, and alongside all of that, convey calm.
There's a lot of scary stuff in the world.
There's no reason for everything to be terrifying.
Put it in perspective.
But it's become so partisan, and so, well, just out of whack,
that everything has to be shaped,
as it's almost the end of the world.
What would happen if today's media was around
during the points in time when the fate of the world
actually did hang in the balance.
Let's take a look.
Welcome, America.
It's Mick Beauregard, and tonight is October 22, 1962.
We have some troubling news from the island of Cuba tonight,
but stay tuned after our update for the latest
on the blockbuster film, Lawrence of Arabia,
and an update on the Belgians in the Congo.
As outlined in President Kennedy's address
earlier tonight, it appears the Soviet Union
has installed nuclear missile launchers on the island in Cuba.
This comes on the hills of Gary Powers being shot down in his U-2 spy plane over the Soviet
Union and the Bay of Pigs invasion, which was an invasion carried out by Cuban exiles
in an attempt to liberate the island from the Cuban dictator and communist black leader
Fidel Castro.
A leader who at this very moment is threatening the very existence of the United States.
We can only assume these missiles were placed there for first strike capability.
This attack was thwarted by the fine work of our U.S. intelligence services, saving
the lives, millions of lives, at least for the moment.
Tonight, we'll be bringing in several guests to discuss the president's actions or inactions.
First up is Racy Delaware, a D.C. insider with close connections to the administration.
Thanks, Mick.
Yes, the president's complete inaction is reminiscent of his reluctance to truly commit
our boys to help the poor people of Vietnam against the aggression of dictator Ho Chi
Minh. It's time for this president to stop trying to achieve peace through talking and
push the button. Let's get this show started. He should start both. Now, today. We fought
the Germans and Japanese at the same time. Surely we can handle Cuba and Vietnam. They'd
be home before Christmas.
Yes, yes. Red China backing Vietnamese leader Ho Chi Minh is certainly a worry. But tonight,
let's just focus on Castro.
Sorry, Mick. I just love this country so much. I wish the Democrats did too.
Anyway, my friend Denise knows Kelly, who does Jackie's hair, and she said that Kelly
said that Jackie said that Bobby said the president was too preoccupied with Maryland's
death to even consider a response to Castro. He's just talking about quarantining the
island. It's time for him to man up. If we had a Republican in the White House, this
wouldn't be happening.
That's reporting reminiscent of the great gossip columnist Walter Winchell there, Racy.
Thanks for the insight. President Kennedy's decision to remain a pacifist and just quarantine
the island has angered many in the defense community. We have a statement
from director of government ourselves for murder on technology but tonight
we'll just be calling him a defense expert to avoid any appearance of
impropriety. What do I have to say? The same thing we've been saying since this
peacenik was elected. He wants to end the arms race. That's great but it's just not
possible. Russians are different than we are. They only respect force. This little
quarantine shows exactly how weak this president is. Does he think Khrushchev is just gonna
talk to him and they'll come to some kind of an agreement? We've only got one option,
a one option only. The third option that my good friend Mac presented to the president.
We attack and we attack now. I know of a little company that produces a low-altitude bomber
that could fly right under the radar, take them out with limited fuss. That's what has
to be done. But this president doesn't have the stomach for it. I know what you're gonna
Indeed, we need to think of the children. If we are going to have a nuclear war, we
need to have it now. It's not like these launch sites are for putting space monkeys
For another perspective on the inevitable coming war, we have a British journalist.
She typically reports on royal scandals for the tabloids and has no subject matter experience
whatsoever.
However, she does have a British accent, which leads most American viewers to believe that
she's an expert with a double-doctorate of some sort.
Tonight, rather than disclosing that, we'll just be referring to her as an investigative
reporter.
Catherine, what are your sources telling you?
My sources, which I can't name, tell me that your government is only telling you part of
the truth.
There might already be nuclear weapons inside of the United States right now.
I was told that they were manufactured in Ukraine and they're ready to crowd strike
at any moment now.
They might already be in your city today.
You are at far greater risk than you might imagine.
And I was told that Atlanta is one of the primary targets, Mick.
You might want to remember that and move out of your area as soon as possible.
The most important thing of all is not to panic as you're building your fallout shelters.
Be leery of anyone that doesn't look like you.
The president would like you to remain calm, but fear is what keeps you safe.
That's right people.
Nothing to fear, but people who are slightly different than us.
We're gonna take a quick commercial break here
and come back with a special interview
from a Bay of Pigs veteran.
Are you a veteran?
Were you asked to leave the service
with an other than honorable discharge?
Did you receive a dishonorable discharge?
Here at Murder on Technologies, none of that matters.
We can place you in a contract position
and get you right back into the job you were in
in next to no time.
Remember, the U.S. taxpayer spent tens of thousands,
if not hundreds of thousands of dollars,
training you to become an instrument of war.
Just because you've been a little too free
with that trading doesn't mean anything.
This is the American way.
It's capitalism at its finest.
Murder on, man, murder on.
Welcome back.
Now we have a very special guest
who we can't name for security reasons.
He's a veteran of the Bay of Pigs invasion
and is going to explain to us why this time
our efforts to teach Cuba our peaceful ways by force
will work. I understand he is currently, definitely not at this exact moment training more Cuban
exiles to once again invade the country. You've appeared many times as an expert since your
harrowing escape from Cuba when you selflessly tried to free the Cuban people from themselves
and were employed by Christians in action. Why will this time be different?
Thanks, Mick. We're on the range today, so please bear with gunfire in the background.
Listen, I suppose I could be considered an expert on these matters, and as an expert,
I would tell you that the Cuban people want democracy. They want what we have.
They want it so bad that we have to go in there and murder people to give it to them.
We owe them that much. I agree with the Murder on Technology's representative.
We have to go in there and take that whole damn island.
If the Soviet Union is going to force them to take nuclear weapons,
then we have no other choice but to go in and invade them.
Cuban people are living under the boot of an authoritarian regime.
We need to spread democracy.
I've been there.
I've seen the babies being taken out of incubators, WMDs, their state sponsors.
The United States would never do this to another sovereign nation.
Could you imagine if they left missiles in Turkey?
Oh wait.
We need to invade them before they invade us.
They might have already.
They're sneaky.
They're sneaky sneaks.
And those doves in Washington listening to Kennedy are just going to get us killed.
And anyway, what do they know about war?
Indeed, true patriots in this country know this soft-handed Eastern elite attitude is
just going to lead to the destruction of the United States.
We need to act, and need to act now.
JFK needs to act before he gets us all blown away.
Up next is Pox and Prims talking about the first real boy band in the United States as
British Beatlemania sweeps the nation.
In the meantime, enjoy this stock footage of the new craze sweeping the nation, the
hula hoop.
informed on America's most trusted network for what might pass for news.
Good night, America, and God bless.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}