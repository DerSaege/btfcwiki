---
title: Let's talk about the 1960s, ideas, and Woodstock....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NNNW7K7V4VE) |
| Published | 2020/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Woodstock was intended to be a money-making venture, not a free concert.
- The original Summer of Love happened in 1967 in San Francisco, not at Woodstock in 1969.
- Half a million people showed up at Woodstock when only 50,000 were expected, causing chaos.
- Woodstock faced logistical issues, leading to it becoming a free concert due to the overwhelming number of attendees.
- Jimi Hendrix, the headliner, played to a much smaller crowd as most people had already left by the time he performed.
- Woodstock organizers didn't make a profit until the 1980s from residual income, facing financial losses initially.
- Woodstock is iconic not because of free love or revolution but because it gave rise to punk rock.
- The event showcased ideas of the '60s that couldn't be packaged and sold, surviving through people walking through the gates without buying tickets.
- Despite the lack of visible security or real authority, Woodstock remained peaceful and became a cultural phenomenon, proving the validity of its ideas.
- Woodstock was the last great victory of a movement that couldn't be marketed, remembered for its essence of love and community.

### Quotes

- "Woodstock was intended to be a money-making venture, not a free concert."
- "Woodstock is iconic not because of free love or revolution but because it gave rise to punk rock."
- "The event showcased ideas of the '60s that couldn't be packaged and sold, surviving through people walking through the gates without buying tickets."

### Oneliner

Woodstock's chaotic journey from a commercial venture to a cultural phenomenon, proving the validity of '60s ideas through unity and music.

### Audience

History enthusiasts, music lovers

### On-the-ground actions from transcript

- Attend or organize community music events to foster unity and share ideas (suggested)
- Host gatherings where people can share music, ideas, and food in a peaceful, inclusive setting (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of how Woodstock, initially planned as a profit-making venture, transformed into a cultural phenomenon despite facing logistical challenges and financial losses, ultimately symbolizing unity and love within a chaotic yet peaceful setting.

### Tags

#Woodstock #1960s #Music #Culture #Community #Unity


## Transcript
Well, howdy there internet people, let's bow again. So tonight we're gonna talk about the 60s and music and
Woodstock
Mainly Woodstock what it was what it was meant to be what it became
Why it's viewed as an icon in this cultural moment?
You know, we see it looking back because of the way it gets portrayed in movies and how
it all gets blended together as everything by decades.
We see it as the summer of love, and that's what it was, right?
No.
Summer of love was two years before on the other side of the country.
That was San Francisco.
Hayton Ashbery.
1969 was when some enterprising individuals attempted to package that movement, package
the flower children, turn them into a product, capitalize on it.
Woodstock was set out to be a money-making venture.
It's what it was.
It was a concert.
It wasn't intended to be a free concert.
Tickets were sold.
The ticket price wasn't cheap, and today's money would be about $150 a ticket.
Woodstock wasn't held in Woodstock.
The town of Woodstock was one of the initial locations that they were considering it fell
through, but they kept that name because that name was associated with Bob Dylan, who lived
in Woodstock. The concert was 70 miles away. They kept that name to keep that association
even though Bob Dylan was never actually going to play there. The concert itself was just
an unmitigated disaster. It really was. They had planned for roughly 50,000 people. About
About half a million showed up, so at some point they just couldn't handle the tickets.
They couldn't handle the gates.
So it became a free concert because they just took it.
The traffic backing up had the governor, Governor Rockefeller, calling the promoters telling
me he's going to send out 10,000 National Guardsmen.
The money that they did make on the tickets, that had to get spent using helicopters to
fly in food, it was just a disaster.
Even the line-up.
Jimi Hendrix had a thing in his contract that said he had to be the final act.
He was the headliner, but he had to be the last act.
So even though there were roughly half a million people there at one point, he played to a
crowd of about 30,000 because everybody else had already left. Creedence Clearwater Revival,
they wound up playing at like 3 a.m. Everybody was asleep. It was a disaster. Even from the
standpoint of the people who put the concert on, they lost money. They lost money. They
didn't actually make, turn a profit until I want to say the 80s, off of residual income
from merchandise, stuff like that.
With all of this in mind, why is it an icon?
Why is it this iconic moment?
Because of free love, right?
Because the revolution, that revolution that happened because of birth control and the
The pill... no.
No.
Unmarried women didn't have the right to the pill until 1972, the Supreme Court gave it
to them.
I want to say at the time of Woodstock, and I could be off a little bit here, but roughly
half the states didn't allow unmarried women to get the pill.
So it wasn't that.
It was what Woodstock gave us.
Maybe.
I probably had something to do with it.
Woodstock gave us punk rock.
I know, that doesn't make sense.
The Who played at Woodstock and they got a lot of publicity.
Don't get me wrong, they had plenty before.
But they got more at Woodstock.
And The Who, their song My Generation,
it was the mod anthem.
But listen to it, you can hear the roots of punk in it.
and Woodstock helped to catapult them but surely there was something more
there is
they didn't get packaged
the ideas of the sixties
which are a lot of the ideas that
end up getting advocated on this channel they survived
Nobody was able to market it, package it, and sell it.
They tried, but they weren't buying.
They just walked through the gates.
And then there was half a million people, almost half a million people,
for three days. No visible security,
no real authority, just people
sharing music,
ideas, food, a lot of other substances.
no real violence. It wasn't chaos.
To look back is a beautiful defining moment.
That's what it was. That's why it became a cultural phenomenon.
Because it proved the ideas valid on some level.
And yeah, you can say there was definitely outside support in many ways and yeah, that's true.
But that little town wound up becoming, I want to say the fifth largest city in the
state of New York for those couple of days with no real police, just people, no fear,
love. And it was all started because somebody tried to market it. It was the
last great victory of that movement. That's why it's remembered that way. You
know Hunter S Thompson said that the high water mark was Hayton Ashbury was
the summer of love. Looking back with the benefit of hindsight, I think he called it
a little too soon. Because even though people were trying to market that culture, that subculture,
They couldn't, not really, because the subculture wasn't playing along.
That is why Woodstock became important.
That's why it is in history books.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}