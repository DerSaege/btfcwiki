---
title: Let's talk about Trump saying the quiet part aloud....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fWanhxcNZTE) |
| Published | 2020/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the "quiet part" that remains unsaid in normal and political discourse to avoid discomfort.
- Points out how the President of the United States has a habit of saying the quiet part aloud at inappropriate times.
- Mentions the recent instance in New Delhi where the President openly talked about US military involvement in Syria being primarily about oil.
- Criticizes the act of using military force to extract wealth from a nation as a form of colonialism or pillaging, which goes against international law.
- Expresses concern over the President's statements indicating military alliances with Russia, Iran, Iraq, and Syria against non-state actors in the Middle East.
- Raises alarm about the potential long-lasting repercussions of such alliances and the implications for US dominance in the region.
- Condemns the President's actions that may lead to dividing countries in the region along oil interests rather than ideological lines.
- Stresses the importance of acknowledging Russian interference, especially when the President advocates for actions that benefit Russia.
- Urges for attention to be paid to these concerning developments and the potential consequences of such foreign policy decisions.

### Quotes

- "It's called pillaging, not just for pirates, and it is against international law."
- "He's pushing the most powerful non-state actor into Russia's arms."
- "It's dividing them, creating an oil curtain rather than an iron curtain."

### Oneliner

Beau Gyan explains how the President's habit of saying the unsaid aloud, especially regarding military involvement and alliances, raises concerns about potential repercussions and Russian interference.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Contact policymakers to express concerns about potential military alliances that could undermine US interests (implied)
- Stay informed about international developments and advocate for transparent foreign policy decisions (implied)

### Whats missing in summary

Insight into the broader context of US foreign policy decisions and their impact on global alliances and stability.

### Tags

#ForeignPolicy #USPresident #RussianInterference #MilitaryAlliances #GlobalStability


## Transcript
Well, howdy there, internet people, it's Bo Gyan.
So tonight we're going to talk about saying the quiet part aloud.
You know, in normal conversation, there are some things that just go unsaid.
You don't have to say it.
It's the quiet part of the sentence.
Everybody knows it, so there's no reason to say it aloud.
It might make people feel uncomfortable.
So it just goes without being said.
The same thing happens in politics and international relations.
There are some things you just don't say, even if they're true.
It's the quiet part.
Our president, the president of the United States, is a master of saying the quiet part
aloud at the worst possible moment.
We're three years into it, so we're just kind of used to it.
I'm like, yep, he did it again.
shocking but whatever and well he did it again in New Delhi. Recent address he
said we've taken the oil and the soldiers we have there are the ones
guarding the oil we have the oil so that's all we have there that's uh yeah
that's that's something he's talking about Syria and that's the quiet part
you don't say that you do not say that yeah it's true when you're talking about
US military involvement any major powers military involvement in that region yep
that's what it's about it's about oil pretty much always even when Nasser was
causing all the trouble in the Suez nationalized the canal we didn't say oh
oh it's about the oil we said no shipping rights impacts on trade the oil
trade but you don't say that you don't make it about oil the reason you don't
is because if you're using military force to extract wealth from a nation
that's colonialism and that is something that has been looked down upon as of
late and in modern times if you are using military force to extract wealth
and natural resources from a country, and you're not attempting to set up a subservient government.
It's called pillaging, not just for pirates, and it is against international law.
He's kind of admitting to it in this little run-on sentence.
Hopefully that is not something the Department of Defense is involved in.
Last time he made a statement like this, they were very quick to come out and say,
no, we're not doing that in fact and we will not, but he has said it again.
And that should make people wonder. It makes me wonder. That wasn't his only
statement in this little address that should give people pause. For the last few
months we've talked about his foreign policy and I've made jokes. I've made
jokes, even the Deep Goat series, those little bits.
I have often pointed out that many of his foreign policy decisions seem like they're
pushing our allies into the arms of Russia and that many of his foreign policy decisions
benefit Russia more than they benefit the United States.
I always gave them the benefit of the doubt that it was just sheer incompetence causing
this to happen.
that there was any direct intention to undermine US dominance overseas.
He said something, he said a quiet part out loud, something that this definitely should
have remained quiet, because if this is his thought process, it's very troubling.
I'm of mixed minds on it myself, I definitely think we need to leave the region, however
I don't think we should set up another nation as the new imperial overlord."
You know that organization he said that he totally defeated, 100% defeated, and experts
all over the world were like, no, you didn't actually defeat them, you kind of made them
more dangerous.
Well he's realized that.
And his solution, Russia should do it, Iran should do it, Iraq should do it, Syria should
do it.
He's advocating that those nations enter into an open military alliance against a non-state
actor over there.
This would also include the Kurds.
It would have to.
The most powerful non-state actor in the Middle East.
So he is pushing non-state actors and nations that are semi-favorable to us, Iraq was supposed
to be our newfound friend over there, to buddy up to Russia through Iran.
Iraq, Syria, the Kurds, they're supposed to buddy up to the Ayatollah in Iran and then
to Russia.
That completely undermines the US position over there.
He's obviously not thinking long term about this if his concern is the oil, because our
little pockets of troops guarding the oil, taking the oil, pillaging against international
law to hear him tell it, they'd be pretty quickly overrun once this alliance is cemented.
I know nobody wants to talk about it.
It's the quiet part now, because it has implicated both parties.
You can't ignore a Russian interference.
You can't, especially not with Trump.
Not after this.
I gave him the benefit of the doubt.
You go back through the videos, I never actually say that it's intentional.
I really do chalk it up to incompetence.
He said it was.
He's advocating for it.
He's pushing the most powerful non-state actor into Russia's arms.
What that will do, by the way, with the countries that he selected, and we've talked about this
before, there's the Shia bloc and the Sunni bloc.
There's the northern countries of the region, which generally line up with Iran, and then
those that line up with Saudi Arabia.
It's dividing them, creating a oil curtain rather than an iron curtain.
This is definitely part of that landmine diplomacy of his, because somebody in the future is
going to step on this.
If this goes through, if these nations come under the thumb of Moscow, there's going
to be long lasting repercussions.
Doesn't matter how uncomfortable it is, we cannot ignore Russian interference anymore.
with the President of the United States advocating this.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}