---
title: Let's talk about Bernie, his women, and the military....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=W6dBEXuuNIQ) |
| Published | 2020/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Bernie is receiving support from unlikely places, including active duty military.
- Bernie leads all candidates, including Trump, in donations from active duty military by 50%.
- This shift surprises many as the military has traditionally supported the Republican party.
- The abandonment of the Kurds by Trump had a significant impact on military support for him and the Republicans.
- Many active duty military members who were Trump supporters turned away after the abandonment of the Kurds.
- Military personnel work closely with the Kurds, and leaving them behind did not sit well with many.
- Another reason for military support of Bernie is related to systemic issues like income inequality.
- People join the military for better opportunities and benefits, such as universal healthcare and housing.
- Seeing the benefits in Bernie's platform makes military personnel question why these are not available to all Americans.
- Bernie's appeal to active duty military lies in his platform advocating for universal healthcare and other social benefits.

### Quotes

- "Bernie leads all candidates to include Trump in donations from active duty military."
- "Leaving them twisting in the wind, leaving a man behind, so to speak, did not sit well with them."
- "A lot of people join the military today because of systemic issues, income inequality."
- "It's a really bad mindset these women did not get where they are by you know worrying about the fact that some man can think better than their little baby lady brains."
- "If you have a problem with what they say, I suggest you take it up."

### Oneliner

Bernie garners support from unexpected quarters like active duty military due to Trump's actions, systemic issues, and appeal of his platform; criticism of Bernie's surrogates should not be gender-based.

### Audience

Democratic voters, Bernie supporters, Military personnel

### On-the-ground actions from transcript

- Address systemic issues like income inequality in your community (implied)
- Advocate for universal healthcare and social benefits for all (implied)

### Whats missing in summary

The full transcript provides more in-depth insights into the reasons behind the military's support for Bernie and the importance of addressing systemic issues like income inequality and healthcare access.

### Tags

#BernieSanders #MilitarySupport #SystemicIssues #IncomeInequality #DemocraticParty


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about the bane of the Democratic Party.
We're going to talk about Bernie.
Because Bernie is getting some support from some very unlikely places.
Seemingly unlikely places.
They seem unlikely unless you actually think about it.
Stone ran a report saying that Bernie leads all candidates to include Trump in donations
from active duty military.
And not by a little bit, by like 50%.
He's leading by a lot.
And this surprised a lot of people.
The military has traditionally been a bedrock of support for the Republican party and that
appears to be changing and people want to know why, there are a couple of reasons
and from where I'm sitting. The first would be that the president and the
Republican Party in general greatly underestimated the impact that selling
out the Kurds because Trump couldn't handle a phone call would have on that
that support that they normally enjoy.
I personally knew two, maybe three dozen active duty military from one community.
And they were all Trump supporters.
Most of them had bumper stickers.
The day before he did that, the day after I knew one, not a joke, one, everybody else,
that was the line in the sand.
were done with him. A lot of people within the military worked hand-in-hand
with the Kurds and yeah wasn't the same Kurds, not always, but that that
camaraderie was still there. Leaving them twisting in the wind, leaving a man
behind, so to speak, did not sit well with them. That's one reason. Another reason
goes to military recruitment. A lot of people join the military today because
of systemic issues, income inequality, and there are a lot of people who enlist
that are just looking for a way out, looking to build a better life. And they
join the military because they have benefits dangled in front of them. And
then they get there and they see all of the planks of his platform and even some
more radical than his platform. At work they see universal health care, they see
everybody with housing they see food security and they realize that the
reason the rest of the United States doesn't have it perhaps is that if they
did then DOD would lose a really powerful recruitment tool because people
want that and maybe they don't believe that everybody should have to sign up
up and be willing to write that check just to get health care.
That may have something to do with it.
Those two reasons are foremost when I'm looking at it as far as active duty supporting Bernie.
Now we're on the topic of Bernie, though, I have to point out that I've seen a lot of
people over the last 24 hours say that he needs to rein in his surrogates or control
his women, wow, because some politicians who support him have said things about establishment
and see that, well, I guess didn't go over well with everybody.
I would ask that if it was a man, if the politician that said that was a man, if you would approach
them directly or you would ask somebody else to rule them in, it's a really bad look.
It's a really bad look.
really bad optics because it's a really bad mindset these women did not get
where they are by you know worrying about the fact that some man can think
better than their little baby lady brains and letting somebody else make
decisions for them if you have a problem what they say I suggest you take it up
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}