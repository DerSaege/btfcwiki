---
title: We talked about Easter eggs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WGQbEDLKxzI) |
| Published | 2020/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tells the story of St. Augustine and Alexander the Great to illustrate moral ambiguity between a pirate and an emperor.
- Criticizes the equating of legality with morality, pointing out that using force to plunder is the same regardless of legality.
- Questions the lack of progress on Trump's promised border wall and the methods used to acquire land for it.
- Addresses pastors planning to instigate conflict at the Pulse Nightclub on its anniversary and criticizes their behavior.
- Comments on the president's ultimatum and threats, comparing it to actions in Nazi Germany.
- Mentions China quarantining a city and stresses the importance of waiting for demographic information on patients.
- Reports from the Mexican border about Trump declaring a national emergency over asylum seekers.
- Shares a story about refugees building a tunnel with the help of NBC News.
- References Kennedy's emotional reaction to refugees escaping to West Berlin through a tunnel.
- Analyzes the political situation and the lack of expected outcomes in the 2020 election.
- Draws a parallel with Sherlock Holmes' story about a silent dog to suggest hidden knowledge in current events.

### Quotes

- "It's the same thing."
- "We are in occupied territory."
- "The dog isn't barking."
- "He should have barked."
- "Our senatorial dog recognizes the guilty person."

### Oneliner

Beau questions morality, criticizes political actions, and draws parallels with historical events while reporting from various locations.

### Audience

Activists, Political Observers

### On-the-ground actions from transcript

- Contact local representatives to express concerns about immigration policies (implied).
- Support refugee assistance organizations or initiatives (exemplified).

### Whats missing in summary

Beau's insightful commentary on current events and historical parallels can be further explored by watching the full transcript. 

### Tags

#StAugustine #MoralAmbiguity #BorderWall #Refugees #PoliticalAnalysis


## Transcript
We're going to talk about St. Augustine.
St. Augustine told a story about Alexander the Great.
He called a pirate.
He had the pirate brought before him.
And Alexander was like, I'm going to paraphrase here,
dude, what are you doing?
You're disturbing the piece of the seas.
And the pirate's like, dude, what are you doing?
You're disturbing the piece of the whole planet.
But because I do it with one ship, I'm a thief, a pirate, a robber.
Because you have a fleet, well, you're an emperor.
Pretty smart, isn't it?
Morally, there's no difference.
If you're attempting to use force to plunder, there's no difference.
It's the same thing.
That's why it gets very disturbing when people equate legality and morality.
When people say, well, it's legal.
Do it legally.
It means nothing.
Means nothing.
What's the difference between a pirate and a privateer?
Ones authorized by the state.
They do the exact same thing.
Hey man, where's the wall?
Wait a minute, what wall?
Trump's wall.
Nineteen hundred and fifty-four miles of great, big, beautiful wall.
Where is it?
There was already a wall there.
Where's the new wall?
You built the new wall where the old wall was, 78 miles.
You're telling me that in three years, there's not one new mile of wall, not one.
allocated, took money from DOD, disrupted the entire government process for three years
and not a single mile a new wall.
What are y'all doing?
And don't get mad at me.
I don't make a stupid promise.
My viewers want to see a wall.
I'm not here trying to be a good presidential supporter, not like I'm being extorted into
it or anything.
Uh-huh, mm-hmm, see y'all are taking land,
trying to seize land under the Declaration and Taking Act.
That's the one where you can take ranchers' land
without even discussing paying them
until after you're already using it, right?
Yeah, yeah, so I guess Mexico didn't pay for the wall, huh?
Hey, man, can you come get me?
I might be lost out here.
Well, howdy there, Internet people, it's Bo again.
I had intended on doing something funny this morning,
but I started trying to get through some of my messages
and there's a whole lot of people asking me
to dissect the Fullerton incident.
The message that caught my attention is,
Bo, what happened in Fullerton?
If anybody can tell us what this cop did wrong, it's you.
Okay, if you are new to my videos,
I'm a very strong critic of police departments
and tend to rip apart their actions
when they use bad tactics.
There are some pastors, and I use that term loosely,
that plan on coming down here to Florida
and going to the Pulse Nightclub
on the anniversary of the attack
and instigating something, taking them up on their dare,
quote, picking a fight, starting an argument,
after threatening people, and their videos,
if you look at through their videos in the past,
They are ridiculous.
Like many criminals before him, the president has issued a threat, an ultimatum.
Comply in two weeks or we go after the families.
That's the American way, I guess.
The one thing I think everybody should take away from this is that, no, it's not the same as Nazi Germany.
History doesn't repeat.
It rhymes, and this is certainly carrying the tune.
We are in occupied territory.
The front line is everywhere.
Anyway, it's just a thought.
If you are not aware, China has quarantined a pretty large city.
And that can be unnerving.
before we produce our response, we
should wait to get the demographic information
of the first cluster of patients, those that
didn't recover.
Because that's going to tell us a whole lot about anything new.
Because in the beginning, they don't really
know how to treat it.
They're kind of guessing.
So those are going to be the ones that
are going to be hit the hardest.
We have that information.
OK, one was 40 to 50, one was 50 to 60, five were 60 to 70,
two were 70 to 80, and eight were 80 and older.
More than half had diagnosed preexisting conditions
and serious ones like Parkinson's, coronary artery
disease, stuff like that, stuff that could complicate
any efforts to treat.
more than half had known pre-existing based on that this probably isn't the
doomsday pandemic it looks like it's affecting those that are already in some
way compromised now this is assuming that we are getting all of the
information that's available with it they are telling us everything that they
know. Normally they do because they want to produce an abundance of caution.
Howdy there internet people, it's Beau again reporting live from the Mexican border for
the fifth column. President Trump has declared a national emergency due to an invasion. We've
been down here most of the night. We have not seen any signs of any opposition forces.
The Pentagon has yet to tell us exactly which country is invading. However, we have seen
have seen what appears to be refugees.
I'm sorry, say again, it sounded like you said that was the invasion?
It was a family of six, confirmed?
He declared a national emergency over people crossing the border to legally claim asylum.
Who sent me down here?
You get that idiot Justin on the phone.
I am so done with this network.
We got friends on the other side of the border.
We want to build a tunnel.
We need your help.
Engineering student, he's a refugee himself.
So he says, okay.
And they start looking, and they find a factory, and inside that factory they can put the entrance
to their tunnel.
Nobody will see it, it's perfect.
They start digging, it doesn't take them long to realize they need help, so they recruit
a couple more engineering students.
And they've been digging for weeks now, trying to get to a house on the other side of the
border that belongs to one of their friends. They're just going to pop up in the middle
of it. He doesn't know they're doing this, by the way. And weeks go by, they still haven't
even made it to the border. They realize they need better tools. They need more diggers.
short, they need money. NBC News decides to pay for the tools, decides to fund building
this tunnel. Can you believe that? And they've got to be real careful though, because there's
a lot of patrols, border patrol running up and down that area. And NBC, at no danger
to their selves, of course. They're back on the other side, the exit to the tunnel now,
filming this empty hole. And they're filming and filming and waiting, having no idea what's
going on on the other side, the risk these people are under. And all of a sudden, a woman
pops out, covered in mud. Then a baby. Then refugees pour out of this hole for an hour.
It said that when Kennedy saw the footage, he cried.
He cried as those people made it to safety in West Berlin, made it to freedom, defeated
that wall.
If what the Republicans have been saying is true is true, they can completely cement a
2020 victory.
They can destroy this narrative, show that the Democrats just politicized this whole
thing and went in a landslide. Instead, that's not what's happening. That's not what's happening
at all. The dog isn't barking. In one of Sherlock Holmes' stories revolves around a horse getting
stolen and there was a dog in the area, a guard dog. And when Holmes goes through and
he does all of his interviews, nobody mentions the dog.
When they're recounting the events of the night, nobody mentions the dog.
It's the curious incident of the dog in the nighttime.
Dog didn't do anything curious in the nighttime.
That's what's curious.
He should have barked.
The dog didn't bark because he knew the guilty person.
I'm fairly certain that our senatorial dog recognizes the guilty person.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}