---
title: Let's talk about the Superbowl....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kcTJM-JTeEc) |
| Published | 2020/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Jan says:

- Analyzing the Super Bowl halftime show and its significance as a highly-watched television event.
- Mention of Shakira's performance as a teachable moment.
- Critique on how cultural differences are framed during such events.
- Comparison of primal guttural utterances across different cultural groups.
- Acknowledgment of commonalities in celebratory expressions among diverse groups.
- Addressing misconceptions around Shakira's heritage and the use of the song "Born in the USA."
- The appropriateness of displaying the Puerto Rican flag during the performance.
- Mention of the depiction of "kids in cages" during the show.
- Concerns over the lack of immediate action following the portrayal of pressing issues.
- Call for reflection on societal acceptance of critical issues presented through entertainment.

### Quotes

- "It's common, we all do it."
- "We all have stuff like that."
- "There are a lot of working class Americans, Puerto Rico is part of the United States."
- "I got a real Hunger Games vibe from it."
- "We've just kind of accepted that that's what it is now."

### Oneliner

Beau Jan breaks down the Super Bowl halftime show, urging for unity over differences and action on pressing societal issues like "kids in cages."

### Audience

Viewers, Super Bowl fans

### On-the-ground actions from transcript

- Educate others on the cultural significance of different expressions (implied)
- Advocate for the recognition and support of marginalized communities like Puerto Rican veterans (implied)
- Raise awareness about and take action against the issue of children in cages (implied)

### Whats missing in summary

In-depth analysis of cultural perceptions and stereotypes surrounding entertainment events like the Super Bowl halftime show.

### Tags

#SuperBowl #CulturalUnity #SocialIssues #Advocacy #CommunityEngagement


## Transcript
Well, howdy there, internet people.
It's Bo Jan.
So today, we're going to talk about sports ball.
We're gonna talk about the Super Bowl halftime show and
the stuff that goes on and it is important because this is
one of the most highly watched television events.
So the little things that occur are worth discussing.
And there was a lot of little things that happened that
provoked discussion.
So the first, of course, would be Shakira doing that.
And I think it's cool that it became a teachable moment.
I wish it hadn't have been framed the way it was.
Because even though it was used to kind of bridge that gap
and explain that cultural phenomenon,
It was still kind of shown as this weird thing that this other group does.
And we, as good, red-blooded, civilized Americans, we don't have anything like that, do we, huh?
Somebody just set it back to the screen.
like Pavlov's dog, that same primal guttural utterance can be used to request acknowledgement,
to say yes I understand, yes I understand but I'm not happy about it, yes I understand
and I'm really excited about it. It can be used to display a lot of different things,
by the tone. It's not weird. Every group has something like that. And I wish that when
stuff like this happened, we didn't use it to show how different we were, but we used
it to show how similar we are. I mean, if we were watching a different game, you certainly
could have heard or eagle and for those outside of Alabama that's two words or
eagle same thing this primal scream to show excitement and to say that we don't
have something a direct correlation to that most times I've seen what she did
used in real life, it was kind of like a welcome, an excited welcome of sorts.
And if you don't think we have anything that is a direct comparison, I would suggest you
think back to the last time you saw two women in their early 20s run into each other unexpectedly.
Oh my god, and then that high-pitched squeal.
It's common, we all do it.
are the same in almost every way. Tones and the way it's done is different. But we all
have stuff like that. I would like to say that there's this idea that there are people
that are mad that she did it and they normally look and sound like me. I've heard nobody
say anything like that, not even remotely close. The main topic of discussion is the
the fact that apparently a whole lot of people did not know the woman who used belly dancing
throughout pretty much her entire career was half Lebanese.
That's the topic of discussion, I don't know anybody that's mad about that.
There's also a conversation about the song Born in the USA being used the way it was
because that wasn't patriotic.
First I would point out that if you believe Born in the USA is a patriotic song, you have
never listened to it. That entire song is about how the system, the establishment,
the powers that be just screw over veterans in the working class. That is
not a I'm super proud of this country song. That's not what it is. I would
suggest that displaying the Puerto Rican flag at that point is extremely
appropriate because there are a lot of Puerto Rican veterans who get left out.
There are a lot of working class Americans, Puerto Rico is part of the United States,
I know those who normally watch this channel already know that, but it is and they get
left out all the time, it's an incredibly appropriate thing.
And then we have the big one, the kids in cages.
I think it was bold, I think it was cool.
The problem is that's what I saw, something cool, something bold.
But I got a real Hunger Games vibe from it, because this is the most watched television
event pretty much, maybe the most watched, and there wasn't an immediate reaction to
fix it.
We've just kind of accepted that that's what it is now, and I think that's something
and we should be very concerned about and very leery of.
Anyway, it's just a thought.
y'all have a good night!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}