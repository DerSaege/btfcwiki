---
title: Let's talk about the economics of being prepared and masks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cyPjeE0keoQ) |
| Published | 2020/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocates for preparedness and calmness in the face of potential emergencies.
- Demonstrates the difference between N95 masks and surgical masks for protection.
- Points out the exploitation of fear leading to price gouging of masks.
- Emphasizes the importance of being prepared before emergencies occur.
- Mentions the affordability of preparedness supplies under normal circumstances.
- Recommends preparing for various emergencies, not just the current situation.
- Suggests getting masks and other essentials for pandemic preparedness.
- Urges not to strain emergency services by being unprepared.
- Recommends taking the current situation as a teachable moment for readiness.
- Mentions that alternative filters for masks are still widely available.
- Advises against mistaking surgical masks for the protection level of N95 masks.
- Expresses confidence in authorities handling the current situation effectively.

### Quotes

- "Don't panic."
- "It's a teachable moment to show that you should get prepared for when there is an emergency."
- "Under normal circumstances an N95 mask at any hardware store will cost you a dollar maybe two."
- "You're taking yourself out of the equation because you have what you need."
- "I don't think any of this is necessary."

### Oneliner

Beau advocates preparedness and calmness, differentiates mask types, warns against price exploitation, and urges readiness in a teachable moment.

### Audience

Prepared individuals

### On-the-ground actions from transcript

- Stock up on emergency supplies like masks, Purell, and essentials (suggested).
- Get N95 filters for masks used for painting and similar tasks (suggested).

### Whats missing in summary

The full transcript provides detailed insights on the importance of preparedness, mask distinctions, and staying calm during emergencies.

### Tags

#Preparedness #EmergencyPreparedness #MaskProtection #PriceGouging #CommunitySafety


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about the economics
of being prepared, and masks.
But before we get into that, I want to tell you
that I spent today planting trees
that will take two or three years to fruit.
That's just my way of saying
I still think everybody should remain calm.
this is not doomsday okay I really I've been paying attention to this they seem
to have a very very good handle on it despite all of the headlines saying
otherwise I don't foresee this becoming a major issue could be wrong but I don't
see it becoming one but I am a huge advocate of being prepared and because
Because of that, I'm in a position where I can demonstrate something because I found
out something that wasn't very surprising in general, but it was surprising it was happening
this early.
This is an N95 mask.
It is form-fitting, fits to your face.
There's a little piece up here that bends and clamps down on your nose.
It form-fits to your face.
That's a key part of this.
we get any further I want to point out this is a surgical mask. This does not
form fit to your face. It does not offer the same type of protection. It's better
than nothing but so is wrapping a bandana around your face. They are not the
same. Under normal circumstances an N95 mask at any hardware store will cost you
a dollar maybe two. Right now a whole lot of places are sold out but you can still
get them on eBay for $13 to $14.
People saw an opportunity to exploit fear and they're taking it.
This is why it's important to be prepared before it happens.
This is something that literally a dollar or two under normal circumstances and just
It's the threat has driven the price up to $13 or $14.
Imagine what that price would be if there actually was an outbreak in the United States.
You wouldn't be able to get your hands on them.
This is why it's important to have your stuff ready beforehand.
If you can do that and you're in a position where you can be prepared, not just does it
give you peace of mind.
that's one of the reasons I'm not concerned about it because I have what I
need. But it helps free up resources for those people who are in trouble. It's
kind of like putting your own mask on on an airliner before you help anybody else.
You're taking yourself out of the equation because you have what you need.
You're not somebody else that would be straining emergency services for those
people who genuinely don't have the means to prepare for themselves or that were in
a situation that got overtaken by events and can't get out without help.
You don't want to strain a system when it's already overworked.
If you were to try to set up like a family of four for pandemic preparedness two months
ago 30 bucks 40 bucks get masks the Purell all the stuff that you would need
it's not it wouldn't cost much at all judging by what I've seen so far as far
as the way the prices are going the family for probably a hundred and thirty
hundred forty dollars and keep in mind there's not actually an emergency it's
It's just the perception that one might happen.
So if you have the means, take this as a teachable moment.
Now is the time to prepare for whatever the situation may be.
Whether it be a hurricane or an earthquake, whatever's in your area, get prepared for
it now.
Now, because I was getting trees today and happened to be in a place that had them, I'd
like to point out these are apparently not sold out everywhere. These are filters
or these are masks, respirators used for painting and stuff like that. You can get
N95 and up filters for this and they're still cheap and they're still widely
available. So there's your alternative but a surgical mask and you can find
this information on the CDC website is not the same thing. It does not offer the same
amount of protection, so don't make that mistake. It's not the same. But again, I want to reiterate
as many times as I can, I don't think any of this is necessary. It's a teachable moment
to show that you should get prepared for when there is an emergency. It looks like they're
already figuring out which antivirals work and they're doing a really good job of keeping
this in check. Don't panic. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}