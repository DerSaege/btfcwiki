---
title: Let's talk about Hemingway, icebergs, a different Bernie, and the unsaid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=r2Co3kv2rns) |
| Published | 2020/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of the iceberg theory in storytelling, where the storyteller can omit details as long as they know it should be there, and the audience fills in the gaps.
- Mentions the theory of the death of the author, where once a story is public, only the audience's interpretation matters, not the author's intentions.
- Talks about the significance of subtext in storytelling, especially when there are topics that can't be openly discussed due to social norms, censorship, or the climate of the time.
- Describes the context of 1984 in American history, marked by trade issues, returning soldiers, homeless vets, crack, AIDS, and high crime rates in New York City.
- Recounts the story of Bernie Getz, who shot four teens on a subway, sparking public debate on whether he was a hero or a psycho.
- Explains how gaps in information lead people to fill in the blanks with their own biases and preconceived notions, often creating false narratives.
- Warns about the dangers of filling in gaps with misinformation and how it can distort one's beliefs.
- Draws parallels between the events of 1984 and the present day, urging caution in interpreting information and avoiding self-delusion.
- Encourages critical thinking and awareness of the narratives we construct based on incomplete information.
- Ends with a reminder to be mindful of how we interpret and fill in gaps in information to avoid falling into self-deception.

### Quotes

- "Subtext and those omissions, they can be great. They can be very powerful for illustrating the truth."
- "People don't look at little bits of data and try to figure it out. They grasp for what they can find that will fill in the blanks to create a story they like."

### Oneliner

Beau introduces the iceberg theory in storytelling, warns of the dangers of filling gaps with misinformation, and urges critical thinking amid incomplete information.

### Audience

Story Consumers

### On-the-ground actions from transcript

- Question narratives and seek out multiple perspectives to avoid filling gaps with biased information (suggested).
- Encourage critical thinking and fact-checking when forming opinions based on incomplete information (implied).

### Whats missing in summary

The full transcript delves into the power of storytelling through subtext, the impact of societal contexts on narratives, and the need for critical analysis in interpreting information.

### Tags

#Storytelling #CriticalThinking #Subtext #Misinformation #Bias #Narratives


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we are going to talk about
Icebergs,
Bernie Getz,
and The Unsaid.
There's a theory in storytelling.
It has been around since storytelling first started,
but it was Ernest Hemingway who gave it a name.
And he called it the iceberg theory, iceberg principle.
The idea behind it, simply put, is that you can delete or omit any detail, any event,
anything out of a story as long as you, the storyteller, knows it's supposed to be there.
As long as you know it's supposed to be there and you're conscious of that, the rest of
the details, the rest of the action will clue the reader or listener in, and they'll fill
in that gap, creates that subtext.
It's a theory I definitely believe in.
We talked about it when we were talking about Disney's Icy Princess, Queen, I guess, Elsa.
We did that video and in the comments section after I said what I saw, somebody's like,
you know, I thought it was about somebody who was neuro-atypical.
Maybe, certainly can be.
There's also a theory called the death of the author.
What that basically means is once the storyteller puts it
out there for public consumption, their intentions,
their beliefs, what they planned, doesn't matter anymore.
Only what's interpreted by the public is what matters.
That's why subtext at times can be a little dangerous.
Hemingway was a master of subtext.
Subtext is great because a lot of times there are things you just can't talk about you can't say it out loud
Sometimes it's because social norms dictate that you don't talk about those subjects
Sometimes it's because there's outright censorship
Sometimes it's just the climate of the time
In 1984, not the story the year, it was a scary time. It was a scary time. There aren't many
points in American history that are like it. There was ongoing trade issues. People were
worried about trade deficits, foreign debts, there was returning soldiers just
come home. Our government being our government that means we had a lot of
uncared for homeless vets. Crack had just shown up, AIDS had just shown up. In New
York City, crime rate was pretty high, so can't think of many times like that in
American history where you have that that set of circumstances and that
brings us to Bernie Getz. He's a guy on a subway and depending on the information
you got at the time, when it first happened. Four teens, older teens, 17, 18, 19, somewhere
in there. I'm not exactly sure of their ages. You have to look it up. There's a whole bunch
of information on this. Well, either they came up to him to panhandle or they came up
to him to mug him. One of the two. Now, we know from later that at least one of the four
admitted, yeah, that they were going to mug him. They thought he would be an easy target.
They were wrong. He stood up, bang, bang, bang, bang, bang. Injured, four of them. And
then fled, took off. And when he took off, created that omission. Nobody knew what happened.
So it had to be filled in with something.
And that's when it started.
Public opinion went back and forth, back and forth.
Was he a hero or a psycho?
It all depended on what you heard.
They were teens.
I can't believe that.
Oh they all had criminal records.
Oh they had sharpened screwdrivers.
know before he fired that last time he said here have another. Now that's the
stuff that went out and there's more but a lot of that's not true. A lot of it was
reported as fact but not true. People were filling in the gaps because of the
omission. They needed to fill in the gaps with something and they tended to fill
it in with stuff that supported their opinion, their preconceived notion, what
they thought was going to happen. It's something that still happens today when
And there is an absence of information.
People don't look at lighter pictures.
They don't look at little bits of data and try to figure it out.
They grasp for what they can find that will fill in the blanks to create a story they
like.
That's something we should be aware of, because we are in a time just like 1984, both of them.
Subtext and those omissions, they can be great.
They can be very powerful for illustrating the truth.
They can also be very dangerous if you allow them, if you allow it, to delude yourself
and fill in the blanks in a way that allows you to not challenge what you believe.
Anyway it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}