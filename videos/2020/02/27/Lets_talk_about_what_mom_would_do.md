---
title: Let's talk about what mom would do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xGYnEFgDpGg) |
| Published | 2020/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Gyan says:
- Explains the importance of parental figures for young military recruits, who are often teenagers leaving home for the first time.
- Recounts a story about a young soldier seeking medical advice, where the doctor's approach was reminiscent of a caring mother's guidance.
- Shares valuable advice on dealing with cold season, reminiscent of what a wise mother might suggest.
- Emphasizes the significance of basic self-care practices during illness, like rest, hydration, and monitoring symptoms.
- Advises on maintaining hygiene by cleaning high-touch surfaces and designating a recovery room at home.
- Encourages individuals to manage minor illnesses at home before seeking medical help.
- Acknowledges the role of medical professionals while advocating for the reassuring calm that a mother's guidance provides.

### Quotes

- "What would your mom tell you to do?"
- "Your mom sounds like a really smart lady."
- "Probably also encourage you to wash your hands, don't touch your face, get a lot of hand sanitizer around."

### Oneliner

Beau Gyan explains the vital role of parental figures for military recruits and shares valuable advice on self-care during illness, reminiscent of a caring mother's guidance.

### Audience

Military recruits and individuals seeking practical self-care advice.

### On-the-ground actions from transcript

- Ensure you have necessary supplies for cold season like food, soup, Pedialyte, and hand sanitizer (implied).
- Clean high-traffic surfaces in your home regularly to prevent the spread of illnesses (implied).
- Designate a specific room for sick individuals to rest and recover (implied).

### Whats missing in summary

The full transcript provides personal anecdotes and detailed examples of how parental guidance and simple self-care practices can positively impact individuals' well-being during challenging times.

### Tags

#Military #ParentalGuidance #SelfCare #Illness #Hygiene


## Transcript
Well howdy there internet people, it's Bo Gyan.
So today we're going to talk about what your mom would do.
Moms are normally pretty smart.
And one of the things that people don't consider when they
picture somebody joining the military is that they're teens
heading away from home for the first time.
Not just are they joining the military, but they're leaving
home for the first time.
So there's a whole bunch of things that they're going to
encounter that they may need reassurance with.
They may need a little bit of guidance.
And since they're away from home,
they may not have that immediate access to it.
That's why a lot of people will tell you that half of an NCO's
real job is being a parental figure to their troops.
And this plays out in other ways, too.
So years ago, I was at a hospital on post, and there's this young kid in there, he's
like 19, he's got a cold, he has a cold, he has a bad cold, goes to the doc, doc comes
in, yep, you got a cold, and the kid's like, well, are you going to give me any meds for
it, you know, what should I do, and the doctor responded in such a way that was just so condescending
it could only come from a medical professional, but at the same time, years later I remember
it because it was the most effective thing I've ever seen. Doc's like, what would your
mom tell you to do? The kid's like, well, stay home, get lots of rest, drink lots of
Fluid, soup, use Tylenol if I get a fever.
If it gets really bad, call somebody, you know,
call you guys.
Your mom sounds like a really smart lady.
Let's start with that.
Very wise advice.
And what else might a mom tell you to do during cold season?
Go ahead and make sure you have all that stuff, food, soup that you might need, Pedialyte,
Gatorade, stuff to keep you hydrated, cold medicine, Tylenol.
Probably also encourage you to wash your hands, don't touch your face, get a lot of hand sanitizer
around.
High traffic areas in the home, like doorknobs, high touch stuff, doorknobs, remotes, stuff
like that.
Make sure they're washed, they're cleaned.
Maybe designate a room in her mind, at least, that if people feel bad, that's where they
should go.
Probably her room, you know, because it has a bathroom as well.
So people that don't feel well, they can just hang out in there, watch TV, relax, recover.
Unless it gets bad, take care of it yourself unless it gets bad.
You don't want to strain a system that doesn't need to be strained.
A lot of times people picture the worst case, but the worst case is just that, it's the
worst case.
Most people don't get the worst case, most people get something they can take care of
at home. Something we should remember because while medical professionals do
their job and they work from an abundance of caution, I think there still
needs to be some moms out there conveying calm.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}