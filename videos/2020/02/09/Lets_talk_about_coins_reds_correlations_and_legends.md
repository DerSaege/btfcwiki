---
title: Let's talk about coins, reds, correlations, and legends....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iQaOpp7m0JU) |
| Published | 2020/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring legends as windows into past beliefs and fears.
- The legend surrounding U.S. coins: Roosevelt dime and Kennedy half dollar.
- Red Scare paranoia in the 1960s linked to coded messages on coins.
- Debunking the socialist infiltration myth associated with the coins.
- Differentiating between social Democrats and socialists in modern U.S. politics.
- Correlation between American Republicans and Irish socialists.
- Warning against drawing shallow conclusions without deeper investigation.
- Robert Lincoln's presence at events of three assassinated presidents.
- Americans falling for repeated myths due to romantic nationalism.
- Historical trend of the world moving towards the left.
- Viewing past historical figures like Washington and Adams as far-left liberals.

### Quotes

- "They are a link to the past."
- "That habit of never scratching and getting below the surface of how something appears is why Americans in general, but specifically the right wing, fall for the same stuff over and over again."
- "So if you look at it from a long enough timeline, you may see things that appear to be infiltration. But the reality is, it's just a legend."
- "Those terms weren't used at the time, but that's what they were."
- "It's just the course of human events."

### Oneliner

Exploring legends on U.S. coins, debunking socialist infiltration myths, and understanding historical political shifts.

### Audience

History enthusiasts, political analysts.

### On-the-ground actions from transcript

- Challenge myths and misinformation in political discourse (implied).
- Encourage deeper investigation into historical and political narratives (implied).
  
### Whats missing in summary

Deeper insights into historical figures' political ideologies and the impact of myths on modern political beliefs.

### Tags

#Legends #Myths #Socialism #PoliticalShifts #History


## Transcript
Well, howdy there, internet people, it's Bill again.
So tonight we're gonna talk about coins,
reds, definitions, correlations, all kinds of things.
But mainly, we're gonna talk about legends.
Love legends, love old legends,
because they put you inside the mind of the people of the time,
let you know what they thought,
let you know what they were afraid of,
let you know how those beliefs created the scars of history that we still deal
with today. They are a link to the past. A good
example of this is our coins. There's a legend surrounding U.S. coins,
specifically two of them. One the Roosevelt dime, the other the Kennedy
half dollar. Keep in mind both these guys were kind of liberal, right? That's
important. Now at the neck of Roosevelt on the dime you'll see the letters JS
and at the neck of Kennedy on the half dollar you'll see what appears to be a
hammer and sickle. Now in the 1960s because of the Red Scare and how deeply
that impacted the United States, this was proof positive that the Soviets had
infiltrated the US government to the deepest levels and were sending coded
messages back to Moscow. The JS was said to have stood for
Joseph Stalin. In reality it stood for John Sinek, the guy who designed the dime
and the hammer and sickle is really a G&R. Gilroy Roberts, the guy who designed
the half dollar. But that legend exists. It was believed. It still has to be
debunked today. Because of those scars that were created, the very idea
socialist, that term, it now gets leveled as an insult. You're a socialist and maybe
Maybe if you're far right, fine, it's an insult.
But it perpetuated this idea that they will infiltrate, that they will sneak in, that
nobody's being honest, and that they're out to get us.
The reality is, we don't have any, well I don't want to say we don't have any, we do
not have any major political figures in the United States advocating for socialism, none.
Bernie, no, AOC, no, not at all.
you hear a candidate start talking about worker control of the means of
production or seizing the means of production, stuff like this, they're not
socialists. They're not even leftist, really. They're to the left of what
exists in the United States, but they're not really leftists until they cross
into that territory. That's the key phrase you need to look for.
and AOC are social Democrats. The policies they advocate are social
Democrat and again there's that correlation just like with the two
liberals and the symbols. There's that correlation. Social Democrat, socialists.
They got to be the same thing. They're not. They're not. To flip that around I
want you to picture an American Republican and all that that
entails. And then picture an Irish one. Many, historically I would say most, were
literal socialists. The words don't mean anything. It's just a correlation. It's
not causation. This habit in the American people of not looking deeper into things
things, leads them to see 2 and 2 and come up with 29, it happens a lot.
This is how French theories develop.
And I can show you how they get fostered.
What if I told you that one person was at the scene of three different presidents being
taken out?
And not just was he there, but in the weeks before one of them, the shooter's brother
saved his life.
Talking about Robert Lincoln, he had just shown up at the theater and then his life
had been saved by Booth's brother just a few weeks before, stopping him from getting run
over.
Over the years he was there for Garfield and McKinley, why?
Is it because he had something to do with it?
He did have some shady ties, especially to what was the military industrial complex at
the time.
Or was it because he was a beltway insider before there was a beltway?
He was a DC insider, so he was at these events.
The correlation doesn't necessarily mean that he did it, that he had anything to do
with it.
That habit of never scratching and getting below the surface of how something appears
is why Americans in general, but specifically the right wing, fall for the same stuff over
and over again because they tend to believe in romantic ideas of nationalism.
They fall for anything that threatens that, anything that is seen to threaten their mythology.
They believe because it's based on fear.
The world moves left, historically.
That's what's happened throughout time and it will continue to happen.
So if you look at it from a long enough timeline, you may see things that appear to be infiltration.
But the reality is, it's just a legend.
And historically, things move left.
have understand that from the systems of the time, Washington, Adams, Payne, these guys,
they were far left.
Those terms weren't used at the time, but that's what they were.
They were the liberals, the far, far liberals.
It's just the course of human events.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}