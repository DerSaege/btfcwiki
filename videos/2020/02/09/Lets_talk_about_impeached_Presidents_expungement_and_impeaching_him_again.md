---
title: Let's talk about impeached Presidents, expungement, and impeaching him again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oZiRHTPZCDU) |
| Published | 2020/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the ongoing questions about the impeached president and the possibility of expunging his impeachment.
- Clarifies that the president will always be considered impeached regardless of acquittal, citing historical examples.
- Points out that expunging the impeachment from the House record may backfire and further the perception of a cover-up.
- Emphasizes that expunging the impeachment won't erase it from history but merely add a footnote.
- Affirms that the House can impeach the president again for any reason, even trivial ones like wearing a tan suit.
- Notes that the House has sole power over impeachment and can proceed without alleging a specific crime.
- Criticizes senators who may not have been impartial during the impeachment proceedings.
- Encourages people to read the Constitution to understand the impeachment process better.

### Quotes

- "He will always be an impeached president forever. That never that's never going to change."
- "The House can impeach for anything. The House has sole power over impeachment."
- "All of this is very, very simple if you actually read the Constitution."

### Oneliner

Beau explains the permanence of impeachment, potential futility of expunging, and the House's power to impeach for any reason.

### Audience

Constitutional enthusiasts

### On-the-ground actions from transcript

- Read and understand the Constitution (exemplified)

### Whats missing in summary

The full transcript provides a detailed explanation of impeachment, history, and the constitutional process for those seeking clarity.

### Tags

#Impeachment #Constitution #HousePower #History #Patriotism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna answer three questions
that keep popping up.
The first is, is the president impeached?
He was acquitted, is he still an impeached president?
Is he still an impeached president?
The next is, can the Republicans expunge his impeachment?
And then the third is, can the House impeach him again?
Okay, so, as far as is he an impeached president, yes.
He will always be an impeached president.
The acquittal doesn't matter when it comes to whether or not he was impeached.
If you want evidence of this, you have to go no further than any article detailing the
history of impeachment in the United States.
Who are the impeached presidents?
Johnson in 1868 and Bill Clinton in 1998. Nixon actually wasn't impeached. He resigned while there
was an impeachment inquiry going on. So he wasn't impeached. But Clinton and Johnson both were.
They're mentioned, right? Both of them were acquitted. It doesn't matter. He's still going
to be an impeached president forever. That never that's never going to change.
Now to the idea that the republicans will retake the house at some point and
expunge his impeachment. Yeah, I mean they can do that. There's nothing
prohibiting that in the Constitution, but I don't know what they're going to
expunge it from. This isn't a criminal proceeding. It's not like when you go in to apply for a job
there's a box to check, have you ever been impeached? The record of this is American history.
I would suggest, given the fact that there are many people who believe Republicans engaged in a cover-up,
the idea of expunging it and getting it out of the House record, I would suggest that that would just
further the notion that Republicans are engaging in a cover-up. More importantly,
it's not going to take it out of the history books. It's just going to add a
footnote. He was impeached and then when Republicans
retook the House, they expunged it off of something. But
it's not going to take it out of the history books. We aren't quite that
totalitarian yet. You can't edit history. This happened.
Okay, can they impeach him again?
Can the House impeach him again?
Yes, yes.
Somebody's gonna say double jeopardy.
First, this isn't a criminal proceeding.
The President is not being held at life and limb.
In fact, let's say that he was impeached and convicted.
There's a passage in the Constitution
that allows him to still be tried in a criminal proceeding.
So I would not suggest that double jeopardy applies on that. I think that
they may have to impeach him for something else. I don't think they'd
be able to do abuse of power again. They would have to impeach him for something
else. However, understand that the house can impeach him for literally anything.
And when I say that, I don't think people understand the house can impeach him for
literally anything. They can impeach him for wearing a tan suit or for wearing
too much spray tan. Either one. The House can impeach for anything. The House has
sole power over impeachment. It's that simple. They don't actually have to
allege a crime. If there is a crime, a high crime, misdemeanor, bribery, treason,
they're listed in the Constitution, it is the Senate's obligation to remove the
president if he is found guilty or she is found guilty. So, yes, they can impeach him again.
Now people are going to say, well, that's partisan. Yeah, it is. It's the lower house.
Yeah, the House is not even in the Constitution. The House doesn't even have to be under oath
during impeachment proceedings. The witnesses are, but the House members aren't. When he
gets to the Senate, then in the Constitution it says they'll be under oath.
So I would suggest as far as constitutionally, the real question here is, were senators impartial?
Did they fulfill their constitutional obligation?
I would suggest, given the fact that some of them said, yeah, he was guilty, but we
still voted not guilty, that they probably weren't.
I would suggest that many announce their verdict before the proceeding started that they did
not fulfill their constitutional obligations.
I would say that was a miscarriage of the Constitution.
All of this is very, very simple if you actually read the Constitution.
As far as impeachment goes, there might be 400 or 500 words on it, max.
It's not a long read.
You can go to different websites and search for the word impeachment.
can find it. The whole document isn't that long, and as many of the people who are making
these claims fashion themselves as patriots, I would suggest you take the time to read
it because you look kind of silly, to be honest. This isn't arguable. I'm not arguing these
points. I'm explaining why you're wrong because there's no argument to be had here. It's black
It's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}