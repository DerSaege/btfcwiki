---
title: Let's talk about dancing left and right....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ajy-_QZUouM) |
| Published | 2020/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of dancing and its significance in the political discourse.
- Two friends talking to a Cuban refugee who escaped Castro realize their privilege when he mentions having a place to escape to.
- The Cuban refugee's statement about losing freedom here being the last stand on Earth resonates deeply.
- Beau mentions Ronald Reagan's rhetoric about America being the place people are supposed to escape to.
- Beau criticizes the Democratic Party for continuously moving to the center to chase votes, lacking backbone and principle.
- He draws parallels between Reagan's promotion of Barry Goldwater and the current political landscape.
- Beau warns Republicans about dancing further right and selling out the country's values for loyalty to a leader.
- He urges for reflection on principles to bring the country closer together and resist division created by those in power.
- Beau advocates for America as a place for people to escape to, not for walls or aggression.

### Quotes

- "We're the place for people to escape to. We're not the place for walls. We're not the place for SWAT teams."
- "If everybody in this country stands on principle, we will be closer together than we can possibly imagine."
- "You have gone from what this country is supposed to stand for."
- "Dancing further and further right, kicking you along with that authoritarian boot."
- "We don't know how lucky we are."

### Oneliner

Beau talks about dancing in politics, warns against moving too far to the right, and calls for standing on principle to unite the country.

### Audience

Americans

### On-the-ground actions from transcript

- Stand on principle and resist division created by political leaders (implied)
- Advocate for America as a place for people to escape to, not for walls or aggression (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the historical context and political dynamics surrounding the idea of America as a place for freedom.

### Tags

#Politics #Freedom #Principle #Unity #America


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about dancing.
Dancing left and dancing right.
Talk about it, because everybody's doing it.
Not too long ago, two friends of mine
were talking to a Cuban refugee, a businessman
who'd escaped Castro.
And in the midst of his story, one of my friends
turned to the other and said, we don't know how lucky we are.
And the Cuban stopped and said, how lucky you are.
I had a place to escape to.
And in that sentence, he told us the entire story.
If we lose freedom here, there's no place to escape to.
This is the last stand on Earth.
I know.
Idealistic.
Silly, even.
heartfelt. Not something you find in today's discourse very often when you're
talking about politics.
Open borders nonsense.
Lefty garbage.
Liberal snowflake tears.
That's Ronald Reagan.
That's Ronald Reagan reminding everybody that this is the place where people are
supposed to escape to.
Liberal garbage. I understand.
More importantly, that is Ronald Reagan promoting Barry Goldwater of the infamous Southern Strategy.
This is rhetoric that would have appealed to segregationists and racists of the 1960s.
Maybe not appealed to them, but they would have been okay with it.
That's who they're campaigning for.
This sentiment is to the left of many in the Democratic Party today, and if you are a Democrat
that should give you pause because it shows clearly exactly what happens when
every time the Republicans move further to the right, the Democrats just dance
over for compromise in the center, chasing votes rather than standing on
principle, rather than making an argument and bringing people to them.
They show no backbone and they move to the center, which becomes further and
further right. You do that long enough and you have Ronald Reagan promoting
Barry Goldwater to your left. If you're a Republican, I got another story for you.
In the 1500s in a town called Stroudsburg, everybody just started dancing.
Nobody knows why. There's theories, food poisoning that led to hallucinations, all
kinds of stuff. It wasn't. It was mass hysteria. Put a video down below about it.
Pretty interesting. The point is, that's what the Republican Party has you doing.
Dancing further and further right, kicking you along with that authoritarian
boot. You're selling out everything this country stood for to prove your loyalty
to a man who has no loyalty to you for what? A red hat? If you hear that speech,
You hear that passage and you think what a liberal snowflake lefty and then you
discover it's Ronald Reagan and you don't pause and you don't realize how
far you have gone from what this country is supposed to stand for. It's something
that requires a little bit of reflection. If we both, if everybody in this country
stands on principle. We will be closer together than we can possibly imagine.
Those in power like to keep us divided. This is a sentiment that most Democrats
today would agree with. We're the place for people to escape to. We're not the
place for walls. We're not the place for SWAT teams. We're the place for people to
escape too.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}