---
title: Let's talk about why the House was partisan and the Senate can't be....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4fnOOAuGjiI) |
| Published | 2020/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the difference between the House of Representatives and the Senate in the US government.
- Points out the constitutional obligation of the Senate to be impartial and fair.
- Emphasizes that the Senate failed to uphold the Constitution during impeachment.
- Calls out those who claim the Senate didn't remove the guilty party, stating the Constitution mandates removal.
- Notes a growing trend of people not familiar with the Constitution, despite having "We the People" in their profile pictures.
- Stresses that the Senate and the House of Representatives are designed to be different, with unique roles and responsibilities.
- Argues that those justifying the Senate's actions are essentially admitting they haven't read the Constitution.

### Quotes

- "The Senate had a constitutional obligation to be impartial. They betrayed that obligation."
- "The relevant passage says, shall be removed, not can be, not could be if you want to, not could be if it won't hurt your reelection chances, shall be."
- "The Senate failed to uphold the Constitution. Period. Full stop."
- "The Houses, the House of Representatives, and the Senate, they're not the same. They're designed to be different."
- "Anytime you say it, all you're doing is telling everybody around you that you've never read the Constitution."

### Oneliner

Beau explains the constitutional obligation for Senate impartiality, condemning its failure during impeachment, and clarifies the distinct roles of the Senate and House of Representatives.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Read and familiarize yourself with the Constitution (suggested)
- Advocate for accountability in government actions (implied)

### Whats missing in summary

Explanation on the importance of upholding constitutional obligations and promoting governmental accountability. 

### Tags

#Constitution #Senate #HouseOfRepresentatives #Impeachment #Government


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna answer a question.
We're gonna answer a question that keeps popping up,
mainly from Republicans, and it makes sense
that they don't really get the answer to this.
The question is, why is it that the House
was able to be partisan, and the Senate is supposed
to be impartial and fair and objective?
Where did that crazy idea come from?
It's a legit question from some people,
because they may not know, so we're going to answer it.
The US government has three co-equal branches,
the legislative, the judicial, and the executive.
They're all supposed to be equal.
However, the House of Representatives and the Senate
are not equal.
The House of Representatives was designed
to be the place for the young upstarts,
and the Senate was supposed to be for older and wiser people,
allow things to cool out, let cooler heads prevail
there. That was the idea. This is spelled out in the Constitution. You're going to notice
a pretty big difference here. Article 1, Section 2, Clause 5. The House of Representatives
shall choose their speaker and other officers and shall have the sole power of impeachment.
1 section 3 clause 6 the Senate shall have the sole power to try all
impeachments sounds familiar sounds about the same see except the Senate has
a little bit more in it when sitting for that purpose they shall be on oath or
affirmation why is it different because it's literally different in the
Constitution it's spelled out differently it's designed to be different
The Senate had a constitutional obligation to be impartial. They betrayed that obligation.
In my eyes, they betrayed the Constitution of the United States.
It's real simple. It's right there.
It does not surprise me that many people who have We the People as their profile picture have never read the
Constitution, though.  seems to be a growing trend.
The other argument I would like to lay to rest real quick is the idea that, well, he
was guilty, but we didn't want to remove him.
That also flies in the face of the Constitution.
The relevant passage says, shall be removed, not can be, not could be if you want to, not
could be if it won't hurt your reelection chances, shall be.
be is legalese. It means must be. The Senate failed to uphold the Constitution. Period.
Full stop. It's spelled out. That's why it was allowed to be different. Because it's
supposed to be different. Because the Houses, the House of Representatives, and the Senate,
they're not the same. They're designed to be different. They're designed, in theory,
give everybody a voice. The Senate bears the most responsibility. You can try to
spin this however you want, but anytime you say it, all you're doing is telling
everybody around you that you've never read the Constitution. Anyway, it's just a
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}