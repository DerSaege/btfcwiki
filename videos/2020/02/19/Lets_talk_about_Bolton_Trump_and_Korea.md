---
title: Let's talk about Bolton, Trump, and Korea....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0LGwIs7wpao) |
| Published | 2020/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gives a historical overview of the Korean conflict from 1910 onwards, including the involvement of major powers like the Soviet Union and the US.
- Talks about the surprising invasion of South Korea by North Korea in 1950.
- Mentions the role of Stalin in potentially green-lighting the invasion to entangle the US in a conflict in Asia.
- Describes the back-and-forth military actions between the US, UN forces, and North Korea.
- Explains how the US involvement in the Korean conflict was not solely for South Korea's freedom but also to realign North Korea from communism to capitalism.
- States that North Korea seeks nuclear weapons as a deterrent rather than for offensive use.
- Criticizes John Bolton's hawkish foreign policy approach and suggests Trump's economic approach might be more effective in bringing countries out of isolation.
- Emphasizes the importance of engagement in trade to prevent wars and create prosperity.

### Quotes

- "You don't need to worry about the person that wants 10. You need to worry about the person that wants one, because they're going to use it."
- "Trump's approach of bringing them out through economics is probably right."
- "If everybody's engaged in trade, there's less chance of a war."
- "It's probably right. He just can't execute it."
- "Until then, our best bet is to just try to delay them obtaining a weapon."

### Oneliner

Beau explains the historical context of the Korean conflict, criticizes hawkish foreign policies, and suggests economic engagement to prevent wars and create prosperity.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Engage in trade with countries to prevent conflicts and foster prosperity (suggested).
- Advocate for diplomatic approaches over hawkish foreign policies (implied).

### Whats missing in summary

The full transcript provides a detailed historical analysis of the Korean conflict and offers insights on foreign policy strategies and approaches towards North Korea.

### Tags

#KoreanConflict #ForeignPolicy #Diplomacy #Trade #NorthKorea


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about Bolton,
and Trump, and Korea, and our diplomatic efforts there,
because they're coming back into the conversation.
People are talking about what we should do.
In order to know that, we should probably take a look
at what we have done.
It might give us a good blueprint.
It might give us a little bit of insight
into how all of this shaped up to begin with.
And that might help.
So that is going to form the basis of our little history
lesson that we're going to start off with.
All right, so 1910, Japan annexes Korea.
Becomes part of Japan.
Now at the Yalta Conference, which
when the major Allied powers played masters of the universe and just divided
up the world at the end of World War II. It was decided that the northern portion
of Korea, north of the 38th parallel, well that would go to the Soviets and the
South would go to the Americans. And that's pretty much what happened. And
everything was fine for a few years after the war. By 1949 though most Soviet
troops and American and British troops they were gone. June of 1950 the North
invades surprised pretty much everybody except for Joseph Stalin according to
one argument. There is a widespread belief, one that I hold, that he knew that
it was going to go down and that he green lit it. It's like yeah do that
because he wanted the Americans involved in a conflict in Asia to kind of
keep him tangled up, keep him busy. He also predicted the eventual Chinese
involvement. He did this because he wanted a freer hand in Europe. He wanted
to be able to consolidate power. Not just was he worried about his former allies,
he was concerned about resistance within communist black countries. And that
That turned out to be a pretty accurate assessment on his part in 1956.
There was the Hungarian Revolution in Budapest and he was pretty spot on with what he thought
was going to happen.
But it is unlikely that the North would have invaded without at least a nod from Stalin.
Okay, so since they took them by surprise,
and nobody was expecting this,
the communist forces, the North,
made very, very, very quick gains.
And this obviously irritated the US.
The US went to the UN.
At this time, the Soviet Union was boycotting the UN
because they wouldn't admit China.
That means that the Soviet Union could not exercise
its veto on the Security Council.
So, the UN authorized the use of military force.
The US and allies went in.
Pretty quickly, they pushed the Koreans back, the North Koreans back.
Then, the Chinese intervened and pushed the UN and US forces back south.
Eventually, the US and UN forces pushed back
and we end up right around the 38th parallel,
meaning all of this has been for nothing.
Eisenhower enters, and he wants peace.
Now, prior to this, it was under Harry Truman.
Eisenhower wants peace, pushes for it.
It takes two years with fighting still going on,
and the line's not changing much,
there is an armistice reach at Panmunjom. So that's kind of how the actual war
went down. That's what happened as far as the involvement of the Soviet
Union, Red China, North Korea, South Korea, all of that. Now there is the idea,
and it's something that we need to dispel, that the U.S. was there for South Korea's
freedom, that sounds good.
The reality is this entire time the country was run by Sigmund Rhee and his regime was
marked by massive corruption, unbelievable corruption.
His reign came to an end when an election went way too much in his favor considering
how disliked he was.
People believed it was rigged.
And that belief turned into action.
People took to the streets and as they were headed to his home, surrounding his home,
he was being flown out of the country with his wife.
a DC-4 owned by Civil Air Transport. That's Air America. It's the CIA. And he flew to, I think,
Honolulu and lived out his days there. I think on our dime, if I'm not mistaken.
That's how it went down. This was the soft colonialism that occurred after World War II.
Now, during the Cold War, and this is important, during the Cold War, North Korea was under
the umbrella of China and the Soviet Union when it came to mutually assured destruction.
The end of the Cold War, that didn't exist anymore.
So they want a deterrent, why?
Because they're on the list.
We've talked about getting even since this happened.
So there's that worry on their part.
That's where they're coming from.
They want a deterrent.
The senators and the pundits who talk about, well, North Korea may give it to these people
and they may do something if they get one.
That's not going to happen.
And that's not going to happen.
The reality is they want a strong deterrent.
They don't want one.
They want a whole bunch.
There's a long-standing saying when it comes to nuclear weapons.
You don't need to worry about the person that wants 10.
You need to worry about the person that wants one,
because they're going to use it.
He just wants a deterrent.
Now, as far as weapons of mass destruction goes,
they have chemical.
They have biological.
They have that.
They haven't used it.
So the fear mongering is probably pretty unwarranted.
Now, the real reason the US doesn't want them to have it is because we want to realign them.
We want to change them from a communist country to a capitalist one.
That's the reason, that's the real reason, and if they have that deterrent, well, we
can't invade them at will, if we so choose to.
The reason we haven't is because they've spent all of this time developing a pretty
good doctrine, it starts conventional and then goes unconventional, it's not one that
we want.
want to fight that war. It's the reality of it. That's the reason nobody's ever done it.
So that's where they're coming from and the US foreign policy, the hawkish foreign policy
that John Bolton promotes is wrong. It's what's been promoted for half a century and it's
never worked. This is going to surprise pretty much everybody that's watched this channel
for any length of time.
Trump's approach of bringing them out through economics is probably right.
He's probably correct.
He's just not somebody who can do it because he doesn't understand what they want.
He goes over there talking about, well, we're going to build great hotels and they don't
care.
That's not what they need.
That's not what they want.
Now if you're looking at it from a historical perspective, John Bolton's policy, when it
comes to bringing countries that are in isolation, they're out of the international community,
when it comes to bringing them back into the fold and bringing them back into the international
community. How many times has that worked? I can think of one. I can think of one. And
that success is Iraq. That's not a really great success. How many times have we been
able to bring a country out of international isolation via economics? It's a list too
long for me to go through.
Trump is probably right.
He just can't execute it.
And because he doesn't really have experienced foreign policy people on hand,
nobody can guide him through it.
Long-term, that's probably the approach that needs to be used.
If everybody's engaged in trade,
there's less chance of a war.
They're gonna feel more comfortable.
They're not gonna need that deterrent.
That's the idea.
They're gonna feel more prosperous.
He, the ruler, whoever it is at the time,
will be more seated, more in power,
because his people won't be unhappy.
it worked with a lot of countries and that's probably the route we should go short term we
just need to make sure that nobody starts a war that nobody wants to fight and we can't win and
isn't going to accomplish the goal anyway until then our best bet is to just try to delay them
obtaining a weapon that's it and if they do it's not the end of the world this
isn't a crazy madman regime despite the fear-mongering media they've had the
ability to wreak havoc for a very very long time through other means they never
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}