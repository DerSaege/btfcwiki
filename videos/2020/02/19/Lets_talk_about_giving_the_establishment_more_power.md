---
title: Let's talk about giving the establishment more power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jSdpj6lA2V8) |
| Published | 2020/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes an op-ed suggesting giving elites more control in choosing the president.
- Establishment faces difficulty with preferred candidates and wants more influence.
- Proposes ranked choice voting combined with non-binding exit polls for elites.
- Calls out this proposal as oligarchy rather than democracy.
- Advocates for direct democracy where popular vote determines primary candidates.
- Suggests a process where candidates collect signatures, debate, and face elimination rounds.
- Emphasizes the need for year-round involvement to avoid last-minute choices and poor leaders.
- Stresses the importance of removing power from party elites and involving the rank and file.
- Believes in including third-party candidates with a million signatures to remove party affiliation.
- Concludes by urging for active citizenship and caution against letting elites dictate decisions.

### Quotes

- "That's oligarchy. That's not democracy."
- "Democracy is advanced citizenship."
- "Letting the elites do your thinking for you, that's how we wound up here."

### Oneliner

Beau criticizes proposals for elite control, advocates for direct democracy, and stresses active citizen involvement to avoid oligarchy.

### Audience

Voters, democracy advocates

### On-the-ground actions from transcript

- Collect a million signatures for presidential candidacy in January (suggested)
- Be actively involved year-round in political processes (implied)

### Whats missing in summary

Detailed breakdown of the proposed primary reform process

### Tags

#Democracy #PrimaryReform #DirectDemocracy #ActiveCitizenship #Oligarchy


## Transcript
Well, howdy there, internet people, it's Bo again.
It's time to give the elites a bigger say in choosing the president.
That's an op-ed headline, the Washington Post right underneath the slogan, democracy dies
in the darkness, because that's funny.
Is the article as bad as the headline?
Yes, yes it is.
Basically, the establishment is having trouble getting the
candidates that they would like.
So they need to rework the system so the elites have even
more control over who us common folk get to vote on.
Because the endorsement scheme, you know, the rock or
roll or coal wars that we have going on, that isn't working
in their favor right now.
And the outside candidates are still gaining ground, so they
need to change that.
What's proposed?
A ranked choice voting scheme, which is sometimes a good
thing, but it's combined with like an exit poll on various
issues that is, of course, public, but not binding.
So the elites don't actually have
to do what the people want them to do
in our representative democracy.
And of course, the elites would get
to choose who we get to rank choice vote on.
Just to be real clear, that's oligarchy.
That's not democracy, just to let you know.
That's how you end up with the choices of Jack Johnson
John Jackson. There's going to be no meaningful difference between the candidates and it's
going to be an oligarchy. I get it though. Parties need primary reform. Sure, we can
go with that. Let's just keep it simple. Let's say March 15th, date doesn't matter.
Everybody, Republican and Democratic alike,
goes and heads to the polls,
and they vote in the primary nationwide at one time.
Popular vote wins.
Those are your candidates.
That's direct democracy.
That's a democracy, that's how that would work.
But I do understand that that still allows the elites
a little bit more control than I'd like because they're going to get to pick who the primary
candidates are. So a little bit more complicated suggestion would be, let's say, the month of
January. Any person who wants the job of being president, they've got the month of January to
collect a million signatures. If they do that, they're on the debate stage and the debate stage
includes Republicans and Democrats. So that debate takes place in February and
about a week after the debate, once everybody's read the commentary and
figured out what they liked and what they didn't, you vote again.
Bottom 50% of the candidates are gone, regardless of party. And this includes
the incumbent, by the way. They need to be on the debate stage too. And then that
That process just repeats until you have two candidates left, or four, and do rank choice
voting if that's really how we want to do this.
And I know some people worry, well what if my party doesn't get a candidate in the last
two or the last four?
Too bad.
Part of the problem right now is that we are trusting our betters, the elites, to make
the decisions for us and then once every four years we go in and vote for whoever has the
right letter after their name.
It's not about substance, it's not democracy, these people aren't representing us.
That's part of the problem.
So if you want your candidate, you need to be involved year round.
See you can't just wing it at the last moment.
That's how we ended up with a candidate, a president, like the one we have now, a national
embarrassment.
We do need to work on our democracy.
We do need to adjust the way the primaries are conducted.
But if we want to maintain our democracy, we need to remove power from the party elites
and put it back into the hands of the rank and file.
And I'm saying this as somebody who isn't a Democrat and isn't a Republican.
Incidentally, if you did the petition thing, all those third party candidates, well they
get to be on the stage too if they get a million signatures.
It's almost like it would remove party affiliation from the presidency.
Democracy is advanced citizenship.
One day every four years, that doesn't cut it.
Letting the elites do your thinking for you, that's how we wound up here.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}