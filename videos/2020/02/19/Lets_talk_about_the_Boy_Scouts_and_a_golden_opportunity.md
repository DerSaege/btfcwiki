---
title: Let's talk about the Boy Scouts and a golden opportunity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BqwrZb7Ia9o) |
| Published | 2020/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Boy Scouts started with solid values of service to community, self-reliance, and doing your best.
- Over time, nostalgia took over, the organization lost focus, and it became politically controversial.
- Many former Scouts value the moral code instilled in them by the organization.
- Beau suggests remaking the Scouts into a modern American image with inclusive values and less nationalism.
- He proposes creating a decentralized form of scouting led by parents in the community, focusing on core values and flexibility.
- The core issue with the Boy Scouts was the overrun of nationalism and a paramilitary structure.
- Beau believes that an organization dedicated to creating strong young people is needed, focusing on internal growth over external achievements.

### Quotes

- "The core values need to be instilled."
- "It should be more about fostering internal growth than putting patches on a sash."
- "This is something that somebody watching this can do."

### Oneliner

Beau suggests remaking the Boy Scouts into a modern, inclusive organization focused on core values and internal growth, rather than external achievements.

### Audience

Parents, community members

### On-the-ground actions from transcript

- Create a decentralized form of scouting led by parents in the immediate community (implied)
- Incorporate STEM learning into scouting activities (implied)

### Whats missing in summary

The full transcript provides detailed insights into the history and challenges faced by the Boy Scouts, as well as a comprehensive vision for its transformation into a more inclusive and relevant organization.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about the Boy Scouts.
Got to talk about the Boy Scouts.
The Boy Scouts are like many things in American history.
They, it was a good idea.
A copy of a copy of a copy.
Nostalgia takes over.
And it loses its focus, and it loses its meaning.
You know, the idea was to instill service to community, self-reliance, doing your best,
helping out when you need to.
That was the idea.
Those are still solid ideas.
Those are still things that should be instilled in young people.
was it hit its heyday in a period of a bunch of nationalism, a lot of nationalism.
And that doesn't fit with today's modern American, you know, and as nostalgia took
over in the late 80s, it was harkening back to Studebaker's and Edsel's, that era.
Like those cars, well, it became obsolete because it didn't keep up with the times.
trying to create Davy Crockett's King of the Wild Frontier and live up to that myth rather
than advancing.
And then when it tried to advance, those that held on to that nostalgia, well they made
it politically controversial.
And that was one of the many nails in the coffin of the Scouts.
I'm sure y'all have seen the bankruptcy news regarding the Scouts.
That doesn't actually mean that it's done, by the way.
But it's yet another nail in the coffin, and they're on their way out.
Because of the behavior of a lot of those in Scouts, many don't want to have their children
associated with it. And then those that try to move the organization forward, they
run into political controversy by those who want to harken back. Treat it like
the 1950s. So this is a golden opportunity for somebody all the time in
comment section, what can I do? This. This is the opportunity for somebody to remake the scouts
in the modern American image. And by remake the scouts I don't mean actually reform scouting,
I mean create an organization that more accurately reflects today's moral norms. More inclusive,
less nationalistic, but those key ideas, service to community, self-reliance, all
of that still needs to be there. It's still a valuable tool and this is
proven by the fact that there are hundreds of thousands of former scouts
who look back at what they learned there and know that it helped shape their moral code.
It doesn't have to be paramilitarily structured. It doesn't have to have uniforms. That was part of the problem.
problem. As time went on, it became more about checking boxes in the little Cub Scout book,
earning badges, than it was about instilling the core values. There's no reason for it
to have a large hierarchy. Somebody, some enterprising individual, could take the U.S.
Army Survival Guide and the Soldier's Guide to Basic Tasks, break them apart, those that
are appropriate, turn them into lessons, incorporate all of the other, the core ideas, the core
tenants put it online and create a decentralized form of scouting where
it's the parents in the immediate community, service to community, that are
actually the scout leaders or what would be the scout leaders. That eliminates or
greatly reduces some of the issues that it had. It also allows the individual
groups to judge for themselves what's appropriate and what's inappropriate.
It allows them to supplement with STEM learning. It allows for a lot more flexibility
disability, as decentralization typically does.
The message behind scouting has always been solid and it will always be solid.
The problem is the core values got overrun with nationalism and that, there is no other
word for it, that paramilitary structure.
That was the problem.
And then as it tried to advance, it tried to shed some of that, the funders weren't
having it.
And here we are.
A organization that's dedicated to creating strong young people is needed.
is. Yeah, it should be more inclusive from the outset. Yeah, it should be less
nationalistic from the outset. It should be more about fostering internal growth
than putting patches on a sash. But the core values are there. The core values
need to be instilled. This is something that somebody watching this can do. And quite literally
thousands of lives.
Anyway, it's just thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}