---
title: Let's talk about how Andrew Yang can still win....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NvvKGQVBM1k) |
| Published | 2020/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Andrew Yang's success despite ending his run for the Democratic nomination.
- Andrew Yang presented himself as good at math and knew he was a long shot for winning.
- Yang saw himself as a messenger candidate, aiming to introduce new ideas like universal basic income.
- Hundreds of thousands of people are now familiar with the concept of universal basic income because of Yang.
- Yang's campaign succeeded in getting people to acknowledge the flaws in the current system and the need for systemic change.
- Beau views Yang's campaign as a success in introducing radical ideas, even if he didn't win the nomination.
- Questions arise for Yang's supporters about what to do next after his campaign ends.
- Beau encourages Yang's supporters not to give up on the idea of systemic change just because Yang is no longer running.
- Yang may have intended to create a movement for systemic change rather than solely aiming for the nomination.
- Beau stresses the importance of continuing to push for change even without a specific political leader to rally behind.

### Quotes

- "You don't need a political candidate to fall behind and follow everything they do and support no matter what."
- "If there's anything you can learn from that campaign, it's that radical ideas and new thought can come from anywhere."
- "You don't need a leader. You've got the thought. I run with it."
- "But for that percentage of his yang gang that looked a little deeper into it and got into the fact that this is something that has to happen, not necessarily UBI but some form of systemic change, you now have the means."
- "You can't stop. You can't become politically inactive simply because your candidate didn't get the nomination."

### Oneliner

Beau reminds Andrew Yang's supporters that systemic change doesn't depend on one leader and encourages them to continue pushing for change even after Yang's campaign ends.

### Audience

Yang supporters

### On-the-ground actions from transcript

- Continue advocating for systemic change (implied)
- Embrace radical ideas and push for new thought (implied)
- Stay politically active and engaged in driving change (implied)

### Whats missing in summary

The full transcript provides a detailed reflection on Andrew Yang's impact in introducing radical ideas and the need for systemic change, encouraging supporters to carry on advocating for change beyond his campaign.

### Tags

#AndrewYang #SystemicChange #RadicalIdeas #PoliticalActivism #Leadership


## Transcript
Well, hi there, internet people.
It's Bo again.
So tonight, we're going to talk about Andrew Yang
and how he won.
And a weird night to talk about that, given the fact that he
just ended his run for the Democratic nomination.
But he won.
Andrew Yang kind of put himself out there as somebody
who's good at math.
I don't think that he had any delusions about whether or not he was going to win the nomination.
He knew he was a long shot.
He knew he wasn't a viable candidate.
His idea, outside looking in, was he was a messenger candidate.
He was trying to bring a new idea to the forefront.
He succeeded.
have hundreds of thousands of people now familiar with the idea of universal
basic income. That in and of itself to me means nothing. Beyond that there are
hundreds of thousands of people who without him would not have embraced a
radical idea. That's a radical concept and in order to embrace that idea they
had to admit something to themselves. They had to admit that the current
system isn't sustainable. They had to admit that the system as it is, is flawed and will
fail. He did that. He got that message out. You know, people pointing to this campaign
as a failure because they're looking at it from a political standpoint as in election
politics. Well, he went on podcast and embraced meme culture and it didn't get him anywhere.
I don't know about that. If creating
hundreds of thousands of people
who have accepted the idea that we need systemic change, if that's a failure,
I hope one day to fail as hard as he did.
I would say that it was a huge success in that regard.
But see, now you got another question.
It's to those people.
those people. All right, your messiah, he's not going to be the candidate. Probably won't
run for the nomination in four years. It's over. But you've been introduced to that radical
idea. That idea that this system is flawed and will fail without systemic change. What
do you do now? Do you give up because you don't have Yang? Because you don't have a
leader? You can. I don't think many would fault you. But for that percentage of his
yang gang that looked a little deeper into it and got into the fact that this is something
that has to happen, not necessarily UBI but some form of systemic change, you know now
have the means. You have the responsibility. You can't stop. You can't become politically
inactive simply because your candidate didn't get the nomination. I don't think your candidate
ever intended to get the nomination. I think he intended to create you.
You don't need a political candidate to fall behind and follow everything they do and support
no matter what.
If there's anything you can learn from that campaign, it's that radical ideas and new
thought can come from anywhere.
Nobody, nobody pictured Andrew Yang coming to the Democratic Party and introducing this
idea as something that could happen.
But it did.
You don't need a leader.
got the thought. I run with it. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}