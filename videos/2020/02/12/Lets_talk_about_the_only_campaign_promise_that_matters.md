---
title: Let's talk about the only campaign promise that matters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ir3D5WUFfgU) |
| Published | 2020/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the importance of a specific campaign promise from candidates.
- Acknowledges President Trump's impact on exposing the disproportionate power of the executive branch.
- Emphasizes the danger of unchecked executive power.
- Stresses the need to restore balance among the branches of government.
- Argues that without this restoration, other campaign promises are essentially meaningless.
- Advocates for returning executive powers to the legislative and judicial branches.
- Calls for a president who prioritizes restoring normalcy over wielding power.
- Points out the risks of concentrating power in the executive branch.
- Questions the efficacy of pursuing policy agendas without addressing the imbalance of power.
- Urges for a president with the integrity to relinquish excessive power.

### Quotes

- "He's shown that the Constitution is not being upheld."
- "That Constitution, it's a piece of paper unless it's adhered to. It doesn't matter. It means nothing."
- "We need a president who has the integrity to give that power up."

### Oneliner

The campaign promise we need: restore balance of power to save democracy and make other promises matter. 

### Audience

Voters

### On-the-ground actions from transcript

- Demand accountability from candidates to restore balance of power (implied).
- Advocate for checks and balances within the government (exemplified).
- Support candidates committed to relinquishing excessive executive power (suggested).

### Whats missing in summary

The full transcript delves into the critical need for presidential candidates to prioritize restoring the balance of power among government branches to safeguard democracy effectively. Viewing the entire speech provides a comprehensive understanding of the urgency for this pivotal campaign promise in ensuring a functional and accountable government.

### Tags

#BalanceOfPower #Democracy #ExecutiveBranch #CampaignPromise #ChecksAndBalances


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about the campaign promise we need to hear.
The one that is possibly the most important campaign promise that any candidate can make.
And it's one that should be being made right now over and over again, and we're not hearing
it from anybody.
Right now everybody's focused on Trump out.
can beat Trump. Fine. Yeah, that's important. That needs to happen. Okay. But we also have
to acknowledge the one great thing that President Trump accomplished. And he did. He pulled
the curtain back. He showed exactly how much power the executive branch has taken from
the legislative and judicial branches over the years. He's demonstrated it. He's shown
everybody that we don't have three co-equal branches of government anymore.
He's shown that they're rubber stamps.
He's shown that the Constitution is not being upheld.
That power that he is wielding like a toddler who's found his father's pistol is dangerous.
The only thing that is protecting this country right now is the fact that Trump has all
show. If this was a thinking man, this country would be over. We're lucky in the fact that
he's not. He doesn't understand unintended consequences. And that's pretty much the only
thing protecting us right now. That Constitution, it's a piece of paper unless it's adhered
to. It doesn't matter. It means nothing. The campaign promise that we need to hear, and
from every candidate who is seeking that office is that they're going to use that executive
power that the current president has abused to pretty much nullify this entire presidency
and then give that power up.
Return it to the legislative branch.
Return it to the judicial branch.
Turn those branches back into co-equal parts of the government.
If that's not done, nothing else matters.
You can campaign for Medicare for all.
You can campaign for anything.
It doesn't matter because if the president has that level of authority, it doesn't matter
what gets passed.
It can be undone.
There is no future.
four-year terms of a country subjected to the whims of whoever won a popularity contest.
Honestly that's the only campaign promise that matters.
Anything else is just window dressing.
We need a president who doesn't actually want power.
We need a president who wants to return the country to some form of normalcy.
And I'm sure that there are people out there who are saying, no, we want them to use that
power to enact our agenda.
That's great, it'll be undone.
The country will get nowhere until a thinking man with Trump's moral flexibility takes that
office and then we'll get somewhere we don't want to be.
Those offices were never supposed to be our rulers.
They're our employees.
They're not acting like it now.
And they don't have to because all of the power has been concentrated.
The checks and balances you were taught about in the third grade, they don't exist.
They're gone.
We need a president who has the integrity to give that power up.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}