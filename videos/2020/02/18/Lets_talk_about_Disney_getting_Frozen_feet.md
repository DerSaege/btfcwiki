---
title: Let's talk about Disney getting Frozen feet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TXPVsRhB0Yw) |
| Published | 2020/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau watches both Disney Frozen films back to back with his daughter, noticing the lack of closure in the second film.
- He points out that Disney always includes subtext in their storytelling, like in Peter Pan, and meticulously plans everything as an organization.
- Beau describes the plot of the first Frozen movie, focusing on the two princess sisters, Elsa and Anna, and the secrets and conflicts between them.
- Elsa, the older sister, has powers to control ice and snow but also harbors another secret hinted at in the films.
- In the second film, Elsa hears a mysterious voice calling her out into the world, uncovering family secrets along the way.
- Despite various hints and subtext throughout both films, Beau notes that Elsa's true nature is never overtly acknowledged.
- He expresses his frustration at the lack of acknowledgment of Elsa's potential queerness and the importance of representation for young audiences.
- Beau suggests that Disney, as a powerful entity, could be a force for social change by portraying Elsa's difference more explicitly in future films.

### Quotes

- "I mean, come on. But it's never acknowledged. And it bothers me."
- "The fact is Disney is a massive machine. Disney could be an engine for social change."
- "Because the fact is Disney is a massive machine. Disney could be an engine for social change."

### Oneliner

Beau watches Frozen films with his daughter, pointing out Disney's subtle subtext and the unacknowledged queer potential of Elsa, urging for more explicit representation in future films.

### Audience

Disney Fans, LGBTQ+ Advocates

### On-the-ground actions from transcript

- Advocate for more diverse and inclusive representation in media through petitions and letters to production companies (implied)

### Whats missing in summary

Beau's emotional investment in seeing Elsa's potential queerness acknowledged in Disney's storytelling.

### Tags

#Disney #Frozen #Representation #Queer #Subtext #SocialChange


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about how Disney got frozen feet.
I know I'm a little late to this conversation,
but tonight I watched both films back to back with my daughter.
And I never actually sat through the second one.
And I just assumed that it provided a little bit of closure
to something I think pretty much everybody that watched the first one
noticed. Well, maybe not everybody, but a lot of people noticed.
Disney as an organization always has subtext. Peter Pan, for example,
it had a lot of subtext. They planned everything, even their construction
projects.
When Disneyland
went up in the fifties,
everything was planned. Down to the last detail, pulled influences from all
over the world.
They did have one
slight issue because of a plumber strike. They had to choose between working
toilets or running water in the fountains. They wisely chose to have the toilets
working. But overall the company plans every detail about everything. It's
always been there. There's always subtext. It hasn't always been good
subtext you know looking back on it but it's always been there now if you
haven't seen the films there are gonna be some mild spoilers in this the first
film the first first movie there's two princesses one well she's a little bit
different. She is a little bit different. She's a little colder. Frigid even might
be how some people would describe someone like this. She has parents who
are accepting of her differences in the beginning and then of course there's an
incident and they go to see what might be considered religious leaders and then
And from that point forward, she's locked in a closet, well, not a closet, I mean that's
a little too on the nose, she's locked in a room.
Now the younger sister, Anna, she has run of the castle, but she's still locked away.
A mishap befalls the parents, Elsa, the older sister, well she becomes queen.
When the coronation happens, they open up the castle.
She still has to keep that secret though, has to keep it concealed, and don't want
people to know.
Now her younger sister, she falls in love with the first dude she meets, like literally
the first guy.
This rubs Elsa the wrong way, of course, I mean as might happen if your younger sibling
found true love and you were still trying to find yourself. This leads to an argument.
And in the middle of this argument Elsa's secret is exposed to the room. People being
what they are, fear what they don't really understand and they ridicule. She leaves.
As often happens when someone you love leaves, the whole town gets a little bit colder. Elsa
that takes off into the hills, her only companion
is her imaginary Canadian boyfriend from childhood,
a snowman who wants nothing from her,
her only male counterpart, only male influence.
Not a romantic influence, of course.
I mean, they didn't put a beard on him, surprisingly.
Now it being a Disney movie, of course the younger sister comes to her and everybody
goes back and saves the day and all is right with the world.
At the end of that film, if you're actually watching it, not, you know, just watching
it for the kids, if you're actually paying attention to it, you know Elsa can move ice
and snow, but you know she's different in another way too.
It's plain.
It's there.
And the assumption is that the second film would confirm that, because what good is subtext
if it isn't eventually acknowledged?
So the second film starts off with them as kids again.
Elsa's playing with some dolls and Anna comes in and makes the prince and princess kiss
and Elsa says, ew, because she's a little different.
And then it's them older.
There she is.
safe with her close friends and family, those people who know her secret, and she's comfortable
with that.
But she hears this female voice calling, calling her out into the world.
She's a little scared to take that trip.
But she goes and her and her sisters, where a pact, they're going to do everything together.
And along the way, they uncover some family secrets, you know, like her grandfather was
not nice to people like her.
And eventually Elsa has to go down into a cave of a rather provocative shape which her
sister does not come with her because I imagine that would be a little weird.
And when she gets down there and of course saves the day there, for some unknown reason
ice crystals begin to glow in almost a complete rainbow of colors, but it's never acknowledged.
The world gets set right, again, but there's never that overt nod.
There's never that acknowledgement of the subtext that existed through both films.
I mean, come on, the only character that Elsa ever had a moment with of any kind was another
woman at a campfire, and as they're sitting there looking at each other, of course, the
giants walk by and the earth moves. I mean, come on. But it's never acknowledged. And
it does. It bothers me. Partly because I enjoy subtext and eventually it has to be acknowledged
otherwise it's a lot like those water fountains in a perfect scene. It's something that's
left undone. The other reason I think it irritates me is that there's a whole lot of little
girls out there that could probably use a nod from Elsa.
Yeah I can understand Disney's point of view for not wanting to put it out there too overtly
and anger the religious right, but come on, give her a rainbow flag tattoo on her ankle
or something in Frozen 3, give us something.
Because the fact is Disney is a massive machine.
Disney could be an engine for social change.
is still a lot of bigotry and Disney having Elsa acknowledge how she's
different could help a whole lot of people well let it go anyway it's just a
Fart. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}