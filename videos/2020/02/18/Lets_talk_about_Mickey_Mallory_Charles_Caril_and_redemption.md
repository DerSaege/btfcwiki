---
title: Let's talk about Mickey, Mallory, Charles, Caril, and redemption....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q_QHHV5PZiE) |
| Published | 2020/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a story of possible redemption, uncertain of the outcome.
- Mentions Mickey and Mallory Knox from a spree on Route 666, comparing to historical counterparts Charles Starkweather and Carol Fugate.
- Describes Carol as 14 and Charles as 19 during a week-long spree that took 10 lives.
- Notes conflicting narratives about Carol's role, with one suggesting she went along to protect her family.
- Reveals Carol served 18 years for her involvement, now 76, seeking a pardon backed by a victim's granddaughter.
- Argues that if it happened today, Carol wouldn't have been tried, being treated as a victim witness.
- Suggests the pardon may not change much but could be a recognition of how the situation might be handled today.
- Tomorrow, the petition for Carol's pardon goes before the board, awaiting the outcome.
- Raises the idea of giving a platform to hear the story of the 14-year-old involved in past crimes.
- Ends with a reflective thought and wishes the audience a good night.

### Quotes

- "Maybe it's redemption, maybe it's something else."
- "She is now 76 years old and she's petitioning for a pardon."
- "We'll have to wait and see what happens."
- "Y'all have a good night."

### Oneliner

Beau introduces a story of potential redemption involving Carol Fugate seeking a pardon at 76, sparking reflections on justice for a 14-year-old involved in past crimes.

### Audience

Advocates, Justice Seekers

### On-the-ground actions from transcript

- Support initiatives advocating for fair justice for individuals involved in crimes at a young age (implied).

### Whats missing in summary

The emotional weight and historical context behind Carol Fugate's story can be fully grasped by watching the full transcript. 

### Tags

#Redemption #Justice #Pardon #TrueCrime #History


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we've got a story of redemption, maybe.
Maybe it's redemption, maybe it's something else.
We don't really know.
Time makes people forget things.
So maybe we're not real sure about what tomorrow may bring.
It might bring redemption for somebody.
Well, this may not be remembered.
Everybody remembers Mickey and Mallory, right?
Mickey and Mallory Knox, Traveling Route 666 on a spree.
Oh, God, creatures do it in one way or another.
So here's the crazy thing.
They have a historical counterpart.
Circumstances may have been a little bit different, though.
That couple, Charles Starkweather and Carol Fugate.
There are multiple narratives about what went down, but what we do know is that Carol,
she was 14 and he was 19.
The narrative that is going to be tested tomorrow is that while she was only long for the ride
to protect her family.
She thought that if she went with him,
well, they'd be spared.
The reality is they were already gone.
But she may not have known that.
That spree lasted about a week, took 10.
One the year before, an additional one the year before,
but wasn't known about.
Carol got life at the age of 14.
She served 18 years and then was released.
She is now 76 years old and she's petitioning for a pardon.
This pardon is backed by the granddaughter of one of the victims.
The idea is that if this were to have occurred today, Carole would be treated as a victim
witness, wouldn't have been tried at all.
She would have been used to ensure Starkweather's conviction, Charles Starkweather, of the Starkweather
homicide.
At this point, she's 76 years old.
Her time is served long since.
The pardon itself means nothing except maybe to those around today, now, that if this situation
were to occur today, we would go the extra step and perhaps hear out the story of the
14-year-old that accompanied the 19-year-old on a road trip of mayhem.
So tomorrow is the day it goes up before the board, and we'll have to wait and see what
happens.
Anyway, that's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}