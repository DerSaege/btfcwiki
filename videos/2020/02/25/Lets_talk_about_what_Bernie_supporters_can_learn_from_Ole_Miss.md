---
title: Let's talk about what Bernie supporters can learn from Ole Miss....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b04Qh0Qu-9w) |
| Published | 2020/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls an incident at Ole Miss where 40 black students staged a demonstration in 1962 by sitting at different tables in the cafeteria, demanding minor requests.
- The establishment did not grant their requests, resulting in mass arrests.
- Beau underscores the importance of understanding the American Civil Rights Movement's strategic and philosophical lessons for those seeking deep systemic change in the present United States.
- Points out the need for long-term commitment and readiness for an enduring campaign for change.
- Mentions the necessity of movements being centered around ideas rather than individuals to avoid vulnerability.
- Emphasizes the vulnerability of movements that focus on personalities, as they can easily be dismantled through various means.
- Notes that movements built around personalities often crumble when the individual is targeted.
- Argues that movements need to be about ideas, policies, and deep systemic change to be successful.
- States that the American Civil Rights Movement's success stemmed from focusing on the idea and the dream rather than individuals.
- Encourages rallying people behind ideas for systemic change and justice rather than personalities.

### Quotes

- "It's got to be about the ideas."
- "If it's about anything else, you're setting yourself up for failure."
- "You make it about a person, you make it about an individual, some personality, you will lose."

### Oneliner

Beau stresses the importance of long-term commitment and focusing on ideas, not individuals, for successful movements seeking deep systemic change.

### Audience

Activists, Social Justice Advocates

### On-the-ground actions from transcript

- Rally people behind ideas for systemic change and justice (implied).

### Whats missing in summary

The full transcript provides a detailed historical context of a civil rights demonstration at Ole Miss, showcasing the importance of long-term commitment and movement centered around ideas for successful systemic change. Viewing the entire transcript offers a deeper understanding of the strategic lessons from the American Civil Rights Movement. 

### Tags

#CivilRights #SystemicChange #MovementBuilding #LongTermCommitment #Ideas


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about what Americans today
can learn from the American Civil Rights Movement,
two big strategic and philosophical lessons
that the American Civil Rights Movement can teach those today
in the United States who want deep, systemic change.
Fifty years ago yesterday at Ole Miss, for those overseas,
that's a university in Mississippi,
40 students, they lined up outside the cafeteria
waiting for it to open.
When it opened, they went inside, they got their trays,
They sat down at different tables, all over the cafeteria.
They ate slowly and waited.
Man, when those white students came in, they were mad.
It was a demonstration.
And they had some demands, they had some requests.
Those black students that sat down at those tables,
they had requests.
Nothing major.
But given the time that it was, of course,
the establishment did not grant those requests.
They were met with mass arrests.
That moment was covered pretty well in the news yesterday.
There were a lot of little articles remembering it.
But I only saw one that put it into context,
that really put it together,
so there was a lesson that could be learned.
At Ole Miss in 1962, eight years before,
there was a riot, a real one.
Lasted two days, hundreds injured.
I want to say two fatalities, could be wrong on that,
because that was the beginning of integration.
The marshals had to come out, the National Guard, eight years before, and they're still
having to make requests.
There is no defining battle when you're talking about deep systemic change.
It's an ongoing campaign, and it takes a long time.
The wills of justice, the wills of change, they move very, very slowly.
are reluctant to change. Even at a university, eight years after a riot. So
that's one. Those today who want deep systemic change, they've got to be in it
for the long haul. They've got to be ready for a long campaign. It's not going
to end in November guys. It's got to be something that you're committed to. The
other thing is that it has to be about an idea of movement. That idea that
people can rally behind. That idea that moves people. Can't be about a person.
Cannot be about a person. Yeah, you can have major leaders. You can have large
personalities and major figures. You can have a Malcolm X and you can have a
Dr. King. You can have whoever you want, but they have to be about the idea, not
Not the other way around.
The people who follow them have to follow them because of the ideas that they espouse.
Not because of their personality.
When you make a movement about a person, it becomes incredibly vulnerable.
If you do that, a bullet in Memphis can end it all.
It has to be about the idea because the idea will outlive any person.
If they can't get them at the polls, they know there's other ways to get rid of a major
figure.
If they can't defeat them politically, they can smear them.
They can arrange accidents.
There's all sorts of things that have been done throughout American history to neutralize
large movements that want systemic change.
It cannot be about a person.
When you look at authoritarian regimes,
do you notice most of them don't last very long?
Yeah, they cause a lot of problems while they're here,
but most of them don't really outlive
that main cult of personality
because it's about the cult of personality, it's about the person.
The people following them don't actually believe the stuff that they're spouting.
They're following that personality.
You can't become that.
It's got to be about the ideas.
It's got to be about the policies.
It's got to be about that deep systemic change.
If it's about anything else, you're setting yourself up for failure.
You will lose.
It cannot be about a person.
That is something the American Civil Rights Movement greatly understood.
And because they recognized that and they focused on the idea, on the dream, that's
why they were successful.
Because it didn't matter what the functionaries of the establishment did to any individual.
The idea, the movement kept going.
You make it about a person, you make it about an individual, some personality, you will
lose.
You've got to avoid that.
There are plenty of ideas that people can rally behind.
There are enough people that want that deep systemic change, that want justice.
Just got to rally them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}