---
title: Let's talk about the mainstream....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A6WijixQAS0) |
| Published | 2020/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines the mainstream as widely held beliefs and agreed-upon facts by the majority.
- Questions the purpose of possessing radical ideas outside the mainstream.
- Encourages mainstreaming radical ideas for broader acceptance.
- Suggests that shifting the mainstream narrative can lead to a better world.
- Points out the challenge of communicating radical ideas effectively to the general public.
- Advocates for making radical ideas accessible and palatable for wider acceptance.
- Emphasizes the importance of speaking a common language to convey ideas effectively.

### Quotes

- "If you hold a radical idea, it's because at least at some point really wanted everybody to believe that so you could have a better world."
- "Sometimes you've got to speak the language of the people that you're talking to."

### Oneliner

Beau explains the importance of mainstreaming radical ideas for a better world and the need to communicate them effectively to the general public.

### Audience

Activists, Ideological Warriors

### On-the-ground actions from transcript

- Make efforts to communicate radical ideas in accessible and understandable terms (implied).
- Work towards mainstreaming radical ideas by making them more palatable to a broader audience (implied).

### Whats missing in summary

The transcript addresses the importance of not just holding radical ideas but mainstreaming them for broader acceptance and impact. It also underscores the necessity of effective communication to shift widely held beliefs.

### Tags

#Mainstream #RadicalIdeas #Communication #Activism #Ideology


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about the mainstream.
What is it?
What is it really?
It's become a term we use to criticize widely held beliefs.
And in a way, it becomes a signal to others
that we are more enlightened.
That may not be the best approach.
The mainstream in general are the agreed upon facts, the facts and ideas that the overwhelming
majority of people can agree upon.
That is what becomes the mainstream.
The thing is, let's say you possess a radical idea or two, assuming if you're watching
this channel you probably do.
You probably have views outside of the mainstream.
What is your goal for those ideas?
What do you want to do with them?
You just want to hold them and therefore possess some sense of superiority.
Because yeah, a mainstream narrative, a mainstream idea, fact, it loses nuance, it loses subtlety
the finer points get washed away so everybody can agree on it or the majority of people
can agree on it.
But if you have one of those radical tributaries, your goal at the end of the day shouldn't
it be to mainstream the idea to get more people to believe it?
If it is something that you believe in, and it's something that you truly hold as something
that is good and true and right, you would want that idea to become mainstreamed.
You would want more people to agree with it.
You would want it to become part of those agreed upon facts.
Because once that happens, then it can shift the course of the river, so to speak.
Because that element, that idea that you possess, well, now more and more people possess it.
I think one of the primary issues with bringing new ideas to the rest of the world is that
But on some level we may not want to actually bring them there.
Some people may like the idea that they have that enlightenment that others don't.
That they understand the nuance and the subtlety.
But I would suggest that if you hold a radical idea, it's because at least at some point
really wanted everybody to believe that so you could have a better world.
With this in mind, it may not be a good idea to constantly bash the mainstream, bash the
widely held beliefs.
One of the things that I see a lot is activists who are on the front lines, and generally
they are the ideological warriors of any radical idea.
They're steeped in theory, and when they talk about it, they talk in theory.
They talk about things in terms that becomes jargon.
that most people don't really understand. That's praxis. What's it mean? If you're
talking about a blue-collar union worker, when he hears that, what's he going to do?
The use of terminology from within your ideological in-group may not always be best for that ideology.
If you want your ideas to become accepted, they have to be accessible, and if you hide
them in language that nobody understands outside of that end group, it'll never happen.
If you truly believe in your values, you want them to be mainstreamed.
You want your ideas to become more palatable, more accessible for those who don't devote
their lives to philosophy or politics.
Sometimes you've got to speak the language of the people that you're talking to.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}