---
title: Let's talk about becoming the out-group....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iNHy_BYwTPs) |
| Published | 2020/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the demographic shift in the United States and addresses the fear associated with becoming a minority in one's own country.
- Criticizes the inherently racist sentiment behind not wanting to be a minority in America.
- Points out that the concern about becoming a minority showcases systemic racism in the country.
- Advocates for treating everyone equally and fairly rather than resisting demographic changes.
- Addresses the argument of wanting everybody to speak English and questions its significance.
- Mentions the gradual demographic shift over decades and the evolution of languages spoken in the country.
- Emphasizes that resistance to change is often rooted in the history of using differences to marginalize others.
- Encourages letting go of fears and biases to build a better society and world.
- Expresses openness to diversity and multilingualism, envisioning a future where differences fade away.

### Quotes

- "It is inherent racism."
- "Try to treat everybody equally. Try to treat people fairly."
- "There's nothing to fear."
- "We will eventually all look like Brazilians and what a great day that will be."
- "I'm totally cool with pressing one for English."

### Oneliner

Beau addresses the fear of becoming a minority in America, calling out the inherent racism and advocating for equality and acceptance of demographic shifts towards a diverse future.

### Audience

Americans

### On-the-ground actions from transcript

- Embrace diversity and treat everyone equally (implied)
- Be open to different languages and cultures (implied)
- Advocate for inclusivity and fairness in your community (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of systemic racism and the importance of embracing diversity for a better future.

### Tags

#DemographicShift #Racism #Equality #Diversity #Inclusion


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're gonna talk about becoming the outgroup.
The demographics of the United States are changing
and they will continue to change.
This is one of those things on a long enough timeline,
there are certain things that are just going to happen.
It is what it is.
There's no stopping it.
There are a lot of people that have a fear
of this demographic shift.
that people, they're worried what's gonna happen to them
when it happens, and it's something we should talk about.
There are a lot of people that say things like,
well, I don't wanna become a minority in my own country.
There's a lot, there's a lot in that statement.
There really is, and then we're gonna just kinda
go through it piece by piece.
The most obvious part is that it is inherently racist.
There's no other way to put it.
First in my own country, in our own country.
That alone suggests the United States is for white people and white people alone.
It's our country.
All of those citizens of other races, well, they're not really American.
That's kind of messed up.
There's also the idea that gets presented along with this is, I don't want to become
a minority in my own country, why?
Why is that a concern?
When you say this, you admit that there is systemic racism.
If there wasn't, becoming a minority wouldn't be a concern, now would it?
It's an admission that the United States does not treat minority groups equally.
Maybe rather than trying to stop the inevitable, it would be a better idea to try to work to
treat everybody equally.
Try to treat people fairly.
Call me crazy.
And then there are sub-arguments when you bring that up, people are like, well no, no
see, I just want everybody to speak English, why, seriously, why does that matter?
This doesn't happen overnight, you know, the demographic shift will take decades and
it will occur and yes Spanish will become a more popular language in the
United States almost like pretty much every country having multiple languages
being spoken. It's not a big deal it's only a big deal if you make it a big
deal. The reason in the United States it becomes a worry is because people used
that, white people, used that to outgroup others for so long. I don't want to have
to press one for English. It doesn't matter. The reason people see that as such a horrible
thing is because it was used to vilify others. This is one of those situations in which the
fates are kind of holding up a mirror to you. If you've said these things, if you're concerned
about these things. Understand it is inherent racism. There's no other way to
do it. There's no other way to hold these views and suggest it's anything but. You
can try to downplay it or spin it, but at the end of the day your concern is
either that a new majority group will treat white people the way white people
treated others, or it's an admission that today's society in the United States
does not treat minority groups equally and that you're okay with that on a
long enough timeline we will all fall in love eventually. Skin tones, facial
features, everything will change.
We will eventually all look like Brazilians and what a great day that will
be, it will be harder for politicians and those seeking to pit us against each
other, to derive power from our differences when they no longer exist until
Until then, it's a source of power for those who want to keep others down.
Don't feed into that.
There's nothing to fear.
I think that we can build a better America and a better world by letting some of this
go.
I'm totally cool with pressing one for English.
I'm okay with my kids learning Spanish.
Their grandkids,
they may speak it at home.
It's okay.
This isn't the end of the world.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}