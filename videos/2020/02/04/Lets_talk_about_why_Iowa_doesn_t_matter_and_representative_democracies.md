---
title: Let's talk about why Iowa doesn't matter and representative democracies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3d1PWr1_Ygw) |
| Published | 2020/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the significance of what happened in the Iowa caucus behind the scenes, stressing its irrelevance in the bigger picture.
- Emphasizes that the appearance of fair elections is vital for a representative democracy to function.
- Points out that faith in three things is necessary for a representative democracy to work effectively.
- Expresses skepticism about the average voter's ability to avoid being manipulated into voting against their interests.
- Raises doubts about elected officials actually representing the best interests of the people who elected them.
- Mentions the importance of believing that your vote matters for the democratic system to operate smoothly.
- Criticizes attempts from both the right and the Democratic Party to undermine democracy, intentionally or inadvertently.
- Asserts that the American experiment of representative democracy is failing due to a lack of belief in voter education and political integrity.
- Advocates for focusing on strengthening relationships with neighbors as a way to improve the country.
- Concludes by suggesting that even if the current system fails, there is hope to build something better for all.

### Quotes

- "Just the appearance matters."
- "You and your neighbors. That's what you need to work on strengthening right now."
- "If the American experiment is indeed dead, we'll be alright."

### Oneliner

Beau questions the essence of behind-the-scenes political events, stresses the importance of appearances in democracy, and advocates for strengthening community ties to build a better future.

### Audience

Citizens, Community Members

### On-the-ground actions from transcript

- Strengthen relationships with neighbors (implied)
- Advocate for transparency and accountability in elections (implied)
- Educate yourself and others on democratic processes (implied)

### Whats missing in summary

The full transcript provides a thought-provoking analysis of the challenges facing representative democracy and the potential for building a better system through community engagement and empowerment.

### Tags

#Democracy #Politics #CommunityEngagement #AmericanExperiment #VoterEducation


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about why it doesn't really matter
what happened in the Iowa caucus behind the scenes, it doesn't.
Big picture, it does not matter.
There's the idea, and it's widely held now, it's mainstream,
that the Democratic establishment rigged it or is trying to.
It doesn't matter whether or not that's true.
Big picture, it does not matter.
Just the appearance matters.
Because representative democracies run on faith.
They run on faith, on belief, nothing more.
And in order for a representative democracy to function,
you have to have faith in three things.
The first is that the average voter is smart enough not to get conned.
We all know what our own interests are.
Do you believe that the average voter is smart enough to not be tricked into
voting against them.
That's something you have to believe for a representative democracy to function.
You have to believe that the average voter is capable of not being conned.
Do you believe that?
Probably not.
The next thing is that you have to believe that when the elected person gets to wherever
they're going, whether it be the state capitol or DC, that they're actually going to represent
the interests of those people who elected them, that they're not going to be bought
off by campaign contributions, large corporations, promises of jobs afterward, party apparatus,
they're not going to do that, that they're actually going to represent the
best interest of the people. Do you believe that? Probably not, because
there's not a lot of evidence to suggest that it's true. And what's the third
thing? That your vote matters.
The idea, you know, that your vote counts and it's not who counts the vote
that's really important. Without those three things the whole system falls
apart and when you have an incident like what happened in Iowa that casts doubt
on that, when it's literally the only leg that is still kind of standing, it
it undermines the entire idea of representative democracy.
We have a large problem, because you have those on the right who
are clearly trying to undermine democracy
through authoritarian measures.
And then you have those in the Democratic Party
who maybe it's unintentionally, maybe they're just incompetent, maybe they don't understand
the stakes, maybe they're just as corrupt.
And they're undermining democracy.
This idea, this representative democracy, this American experiment, it's failing.
It's failing.
Because you don't believe the average voter is educated enough to not get tricked into
voting against their own interests.
You probably don't.
And if you do believe that, odds are you can be tricked into voting against your own interests.
You don't believe that politicians, when they get up there, actually represent you and can't
be bought off.
I can name just a couple out of the hundreds up there that I believe have any kind of integrity
or loyalty to those people who elected them.
And now
we're not even sure
if the vote matters.
You know, there was that study.
The U.S. isn't a democracy, it's an oligarchy.
The more time
passes since that study, the more it shapes up to be true.
You know, people talk about revolution and reform, talk about all you want, but at the
end of the day, I'm going to go back to something that I really kind of point to a lot, it's
you and your neighbors.
That's what you need to work on strengthening right now.
And if you do that, we'll have a strengthened country.
And if it does just completely fall apart, if the system does collapse, if it is over,
if the American experiment is indeed dead, we'll be alright.
We can build something better.
We can build something that wasn't engineered from the beginning to be for the benefit of
the few.
Anyway it's just a thought.
Y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}