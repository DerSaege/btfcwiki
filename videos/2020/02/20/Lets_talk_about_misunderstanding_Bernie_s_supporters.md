---
title: Let's talk about misunderstanding Bernie's supporters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8qyHbdVsUho) |
| Published | 2020/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Bernie is the compromise, not the radical position, for millions of Americans seeking deep systemic change.
- The Democratic Party misunderstands Bernie's supporters, viewing him as too radical when he is actually the compromise.
- Bernie represents proactive food security measures, while the establishment fails to grasp this.
- If Bernie is denied the nomination despite having the lead and support, his supporters may refuse to back the chosen candidate.
- Bernie supporters see him as the compromise, and not supporting him could lead to further radicalization among marginalized communities under Trump.
- The Democratic establishment's view of Bernie as too far left reveals the party's shift to the right.
- Failure to embrace compromise may escalate political tensions towards radicalism.
- Bernie advocates policies similar to those in other western nations, dispelling the notion of his radicalism.

### Quotes

- "Bernie is the compromise, not the radical position."
- "The Democratic Party misunderstands Bernie's supporters."
- "Failure to embrace compromise may escalate political tensions towards radicalism."
- "Bernie advocates policies similar to those in other western nations."
- "Be Democratic during the nomination."

### Oneliner

Bernie is the compromise, not the radical position; failure to recognize this may lead to further political tensions and radicalization among marginalized communities.

### Audience

Progressive voters

### On-the-ground actions from transcript

- Support proactive food security measures in your community (implied)
- Advocate for policies that benefit marginalized communities (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Bernie's position as a compromise, challenging misconceptions about his radicalism and urging support for systemic change. Watching the full transcript offers a comprehensive understanding of the Democratic Party's failure to grasp Bernie's supporters' perspectives. 

### Tags

#BernieSanders #DemocraticParty #Radicalism #PoliticalTensions #SystemicChange


## Transcript
Well, howdy there internet people, it's Bo again.
So tonight we're gonna talk about Bernie
and his supporters and misunderstanding his supporters
because of something that came up during the debates.
The nomination process came up and I realized
that the Democratic establishment
is grossly misunderstanding Bernie's supporters.
To get into that and to provide a real clear comparison, I'm going to tell you about my views on food security.
I'm a huge proponent of proactive measures when it comes to food security.
I think that at low income housing, all of the landscaping should be edible.
I think it should be fruit trees and berry bushes.
should be fruit trees and berry bushes.
I think raised beds should be everywhere.
I think when you walk around town
and you see those decorative trees in the sidewalk,
that they should be fruit trees.
I think that there should be a place
where people can go to get seeds
and compost and stuff like that.
I think that if we're going to have
a government this expansive, call me crazy.
It should promote the general welfare.
I think that was an idea that was tossed out at one point.
That's my position.
It's my actual position.
We don't have any of that.
None of that's reality.
The reality is we have EBT.
Fine.
It's a compromise.
It's not what I want.
It's not radical enough for me.
But sure.
OK.
Every once in a while, I run into somebody who's like, we
need to get rid of all those programs.
I can't take it anymore. I can't take it anymore. Fine. Do it. Because frankly you don't have the stones.
Because I know that there's not a country on this planet that is more than three days worth of miss meals away from
revolution.
Go for it. Go for it. Starve the poor. That's what you do. See how that works out for you.
And I leave the conversation.
There's no sense in continuing it because they don't have the base of knowledge to
engage in it.
So you have the position, the radical position.
You have the compromise and then you have the extreme that you just can't deal with.
Those are three points on this scale.
There are other points in between, but those are the three we're going to be talking about.
Okay, so how does this relate to Bernie?
There are millions of Americans who want deep systemic change that hold radical ideas.
Proactive food security measures.
Here's the part that the Democratic Party does not understand.
them Bernie is the compromise. He's EBT. He's not the radical position. The
Democratic Party doesn't understand this. The establishment doesn't understand
this because they don't know their constituents. They don't talk to him
anymore. They talk to lobbyists. The idea that Bernie is too radical is kind of
laughable on a lot of levels.
So actual position, Bernie the compromise, and then Bloomberg
as an example.
No point in continuing the conversation.
The reason this becomes important
is because of that bit about how the nomination is conducted.
There's two ways that can go.
If Bernie walks in and he does not have the lead,
he doesn't have the massive supporters,
he doesn't have the most delegates,
he doesn't have a plurality of support,
and the Democratic establishment chooses
to go with another candidate, most of them
will still get a pretty big chunk of Bernie supporters.
Not all of them, because some of them,
They're just Trump-lite, but many will look at it in the sense of harm reduction.
This candidate, not radical enough for me, not even a decent compromise, but better than
Trump.
That's how some of them will look at it.
However, if he walks in to that convention and he has the lead and has the most delegates
and has a plurality of support, and the Democratic establishment uses superdelegates
or some other means to take the nomination from him and give it to somebody else,
they will leave the conversation.
Because all that does is reinforce the idea that there is no compromise.
We have to have deep systemic change or nothing.
And they will take it and they will look at it and they will understand that if they
refuse to support whoever the candidate is, that more than likely we'll get four
more years of Trump, and those four more years of Trump, because of his habit of
wanting to publicly punish the marginalized for the humor of his base,
will take those marginalized people and radicalize them even further.
Pass the compromise.
pass Bernie.
That's how they'll look at it.
They'll stay home.
Now there's still a chance that a lot of Democratic candidates can beat Trump without their support.
But remember, they were certain they could in 2016.
And when Bernie supporters en masse went third party or just stayed home, it handed the election
election to Trump, they will do it again because to them, Bernie is the compromise, not the
radical position.
That's the missing part.
That's the part that the DNC just doesn't seem to get.
The idea that the Democratic establishment looks at Bernie and says, he's too far left,
too radical. All that shows is that the Democratic Party has gone right, hard.
Bernie is not a leftist. He's not. He's more left than a lot of candidates, but
he's not a leftist. He's not a radical. The radicals who support him do so out
out of compromise.
And I know there are people who are saying, so they'll stay home and they'll let those
marginalized people just be further victimized by Trump.
That's coming from a position of privilege.
That's ableist, that's accelerationist, yeah, it's all of those things.
And they will do it.
just because it's those things doesn't mean that people aren't thinking that way.
Historically this scenario has played out many times.
In the early 1900s in Ireland, the Irish Resistance brought the British Empire to its knees.
And the British wanted a little compromise.
And one group within the Irish Resistance, they're like, no, that's too much.
We're not doing it.
up the north, and civil war followed.
More recently, it happened in China.
The rulers didn't know their constituents because they didn't talk to them.
A group of young people who wanted deep systemic change took to the streets.
Bam.
China's under martial law.
I don't think it would get that serious this time.
But if failure to even get to the compromise continues, yeah, that's the
route it'll go. It'll eventually head that direction. I don't think it's this
election or the next, but that's the direction it will head. Because as the
party that is supposed to be the left-leaning party in the US moves
further right, understand the real progressives, the real radicals, they continue to progress
and they come up with new ideas that aren't being embraced out of fear of appearing to
be too radical.
When at this point Bernie is really just advocating the stuff that pretty much every other western
nation has, it's not radical and the democratic party calling it that
further feeds people to the right.
There are a whole bunch of people who will take the opportunity to
radicalize the marginalized with four more years of Trump over Bloomberg
Trump-lite. There are a couple other candidates who could probably count on
getting some support from Bernie, from his supporters, but not many, because he
is the compromise, not the radical. I think the best thing that the Democratic
Party could do during the nomination is be Democratic. I think anything other
Other than that is going to lead to issues.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}