---
title: Let's talk about Republics and Democracies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0bHdU-d_dFw) |
| Published | 2020/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the ongoing debate between democracy and republic in the US.
- Mentions that Americans are brought up to question their national identity, leading to confusion.
- Clarifies that the US is a representative democracy, not a direct democracy.
- Points out that when people say "republic" in the US context, they are essentially stating the absence of a monarch.
- Compares the US government system to the UK's parliamentary system and China's republic.
- Talks about the protection from tyranny of the majority in the US constitution.
- Mentions the concept of tyranny of the minority where a small number of elected officials can make decisions for the majority.
- Raises the question of whether elected officials truly represent the people once in office.
- Suggests that certain aspects of the constitution protect the ruling class and disenfranchise many.
- Concludes by hinting at the need to reexamine beliefs about democracy in light of recent events.

### Quotes

- "The US is a democracy because it is. Right now in the comment section somebody is saying, no, we're a republic."
- "Major portions of the Constitution are there strictly to protect the ruling class and to disenfranchise many people."
- "Governments don't fit into buckets like that."
- "It may be time to revisit some of those things that we hold to be true."

### Oneliner

Beau explains the democracy vs. republic debate in the US and questions if the constitution truly protects all citizens.

### Audience

Citizens, Voters, Activists

### On-the-ground actions from transcript

- Question the representation and decision-making processes of elected officials (implied).
- Revisit beliefs about democracy and its functioning in the current context (suggested).

### Whats missing in summary

Deeper insights into the impact of historical constitutional protections on present-day democracy.

### Tags

#Democracy #Republic #Constitution #Government #Tyranny #Representation


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about democracies and republics.
We're gonna talk about this because, no joke,
at least twice a month, somebody in the UK
or Canada or Australia sends me a message and asks,
why do Americans have this conversation
over and over again in the comments section?
It doesn't make any sense.
The simplest answer to that is your school systems are better than ours.
The longer answer is it's semantics and it's part of our national identity.
It's something that we're brought up to believe that eventually we're going to have to question.
And basically, I often say the United States is a democracy because it is.
is. Right now in the comment section somebody is saying, no, we're a republic.
That's the conversation that confuses them. Because it doesn't actually make any sense.
Okay, the US is a democracy. It is a representative democracy. It is not a direct democracy.
When people say, it's a republic, they're trying to express something, however, that
phrase doesn't express it.
In its simplest terms, when you say, we're a republic, what you're loudly proclaiming
is that England has a new queen and we don't.
We don't have a monarch, we don't have a king.
That's what it means.
That's what it means.
It doesn't mean all of the other stuff that you may want it to express.
We'll do it this way to illustrate it.
The UK has a parliamentary system, representative democracy.
They're a constitutional monarchy.
They're not a republic.
China is a republic.
Which one are we closer to?
Which one are we more closely related to as far as our system of government?
The UK.
Governments don't fit into nice neat little buckets like that.
There's a lot of blurry lines.
But saying that we're a republic certainly does not express what most people are trying
to express when they say that.
What they're really trying to say is that we are a constitutional representative democracy
the federal system that has checks and balances, and therefore we are protected from the tyranny
of the majority that comes along with direct democracy.
That's really what they're trying to say.
That's a whole lot of words to say it, but that's what they're trying to say.
In the United States, there is this idea of the tyranny of the majority, that if we have
a direct democracy that 51% of people can oppress the other 49% of people.
Yeah sure, if the democracy threshold is set at 51%, there's no law saying that it has
to be, it can be 60 or 75% if need be, but that's what it's trying, that's what they're
trying to express.
We are protected from this because of our constitution, our minority groups are protected.
the Constitution protects them as it did for black Americans during slavery and segregation
and the natives and the Japanese during World War II.
It doesn't actually work.
That's made up.
It's not a real thing.
That concept is just false.
It's been proven to be inaccurate throughout all of American history.
But taking it a step further, the worry is that the majority of people, so let's just
say that everybody in the U.S. can vote, so 360 million people, so we would need 180 million
and one to achieve that 51% threshold, that that majority can oppress the other 49.
And that's bad.
So instead of that, what we've done is we have chosen to trust our betters in D.C. to
make the decisions for us and instituted a tyranny of the minority, where if the requisite
number from the House and the Senate and the President agree, it happens.
So we're talking about less than 500 people can institute tyranny over the other 360 million.
No they're elected.
They have to represent us.
Do they?
It doesn't seem like it, but after they are elected, what can we do?
So if the day after the election, once they're in office, the day they take office, they
decide to institute some form of tyranny what can we do? Well, we'll rise up. Now
you're back to direct democracy. This idea that the Constitution offers these
protections, it's something that we're going to have to question eventually.
We're going to have to acknowledge at some point that major portions of the
Constitution are there strictly to protect the ruling class and to
disenfranchise many people. And we have to acknowledge that we the people didn't
actually mean we the people at the time. It meant a certain set of people, not
everybody. But I would suggest that this conversation is pointless because
governments don't fit into buckets like that. The UK, for example, it's a kingdom.
Is it more like us or Saudi Arabia? You can't group them like that. It doesn't
work. You have to go off of the whole character of the country rather than
just little bits and pieces of what makes it up. And we're going to go into
this in another video soon when we talk about capitalism and communism because there's the
same thing happens there. But I would, with all of the questions and all of the problems that
we've had with our democracy as of late, it may be time to revisit some of those things that we hold
to be true, we're protected based on this and examine whether or not we actually are
protected.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}