---
title: Let's talk about how we learned stop worrying and love colonization....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v850HR3EQd0) |
| Published | 2020/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States, historically anti-colonial, has become a colonial power.
- Colonialism involves extracting wealth, imposing cultural similarities, and using military force.
- Today, corporations plant logos instead of flags, still backed by military force.
- American belief in the right to colonize leads to shock when facing resistance.
- Support for troops should mean keeping them out of harm's way, not as fodder for profit.
- Despite protests, the American people embrace and love the idea of colonialism.
- American empire expansion mirrors past colonial entities' behaviors.
- Territorial disputes on Earth could lead to fear and hostility towards "aliens" among us.
- Beau suggests meeting extraterrestrial beings to learn, as we have much to discover.
- The expanding empire contradicts the principles of the American founders.

### Quotes

- "We have become exactly what the founders opposed."
- "The only alien I'm scared of are the ones up there."

### Oneliner

The United States, once anti-colonial, has transformed into a colonial power, backed by military force, extracting wealth and imposing cultural similarities, leading to acceptance and love for colonialism among the American people.

### Audience

Global citizens

### On-the-ground actions from transcript

- Question colonial practices and beliefs (suggested)
- Advocate for keeping troops out of harm's way (implied)
- Foster understanding and acceptance of diversity (suggested)

### Whats missing in summary

The full transcript delves into the paradox of America's history as an anti-colonial entity turning into a colonial power, urging reflection on the consequences and responsibilities of such actions.

### Tags

#Colonialism #AmericanEmpire #MilitaryForce #CulturalImperialism #AntiColonialism


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about how we learn to stop worrying and love colonization.
The United States throughout our history, we have been an anti-colonial entity, right?
No taxation without representation.
That whole concept, that whole idea that we were just beleaguered colonies who found our
independence. It is part of the American mythology. It's what we are. Yet along the way, we became
a colonial power. And I'm not just talking about the 1800s and 1900s. I'm talking about
now, today. And we've just learned to accept it. What is colonialism? What is it? What's
That's the definition.
What happens?
You set up within an area that is not part of your national territory and you extract
the wealth.
You try to make them more like you, save them from themselves.
And normally this is backed up by military force.
It's what colonialism is.
Granted, today, we don't show up and plant a flag and say this is ours now.
We allow our corporations to show up and plant their logos.
But it's still backed up by military force.
We've grown so accustomed and so in love with the idea and so dedicated to the belief
that we not only can, but have the absolute right to do it.
That when they protest at one of our embassies,
we act shocked.
When they fight back, we're just appalled.
If that's true and you don't believe
that the colonized people have a right to do that,
and I got some crazy stuff to tell you
some of the stuff the Founding Fathers did in the early days. A protest at an
embassy was pretty low on the list to be honest, but we've grown accustomed to it.
We believe it's our right. We are shocked and dismayed when our troops,
thousands of miles from our borders, in countries that don't want us there, get
attacked and the idea of, well, we have to support the troops. Support the troops.
Get them out of harm's way. Get them out of harm's way. Stop letting them become
fodder for major corporations' profit margins. That would be supporting the
troops in my mind, but here we are every day and we just accept it.
We don't accept it.
We love it.
The American people in general love this.
We're all over the world, the American empire.
We are a colonial empire.
And it's sad because we've learned nothing throughout history.
We are at the point in history where we can colonize Mars or we can stay here and fight
over rocks, minerals.
Just like every other colonial entity, as it grows we begin to be upset at the aliens
among us, those from other places.
They show up and well they just don't, they're not like us.
And so they suffer the wrath that we visit upon their countries.
That's how they got here.
Our conduits work both ways.
So we become terrified of the alien among us because we're terrified of them over there.
The only alien I'm scared of are the ones up there.
Let's go meet them, see what they're like, maybe they can teach us, because we've still
got a lot to learn.
As we applaud the ever-expanding empire, it's just something we should keep in mind.
have become exactly what the founders opposed. Anyway, it's just a thought. Y'all
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}