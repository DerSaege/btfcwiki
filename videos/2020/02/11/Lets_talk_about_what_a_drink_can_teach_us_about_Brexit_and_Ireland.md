---
title: Let's talk about what a drink can teach us about Brexit and Ireland....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7CYhtFeqP_A) |
| Published | 2020/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the background of Northern Ireland being part of the United Kingdom and the Republic of Ireland not being part of the UK.
- Describes the potential consequences of Brexit on the border between Northern Ireland and the Republic, especially the risk of a hard border with checkpoints.
- Talks about the concerns related to the romanticization of conflicts and the potential for violence due to triggering issues like a hard border.
- Mentions the request from his friends to rename a popular drink in the US called a "car bomb" to an "Irish Slammer" out of sensitivity towards the Irish community.
- Points out the historical significance of bars in supporting different sides of conflicts and the need to avoid unintentionally supporting violence.
- Compares the perceptions of conflicts in Ireland between the United States and Ireland, with the US having a more romanticized view.
- Emphasizes the importance of understanding nuance in conflicts that have deep historical roots and real impacts on people.
- Stresses that rhetoric, imaginary lines, and conflicts like Brexit should not be worth people dying over and militarizing borders.
- Urges the Irish American community in the US to be cautious in their actions and words regarding conflicts like those in Ireland.
- Suggests that allowing the people directly involved to decide on matters like borders can prevent unnecessary violence and conflicts.

### Quotes

- "It's sparked by a request from some of my friends."
- "There's a whole lot of nuance to it."
- "It's not worth people dying over."
- "If you're in the U.S. and you are a part of that Irish American community, unless you're going to go there and fight, shut up."
- "Believe me, I understand."

### Oneliner

Beau explains the nuances of conflicts like Brexit, Ireland, and romanticization, urging caution and understanding to prevent violence and support peace.

### Audience

Irish American community members

### On-the-ground actions from transcript

- Rename the drink "car bomb" to "Irish Slammer" to show sensitivity and respect towards the Irish community (suggested).
- Avoid unintentionally supporting conflicts by being cautious with actions and words (implied).

### Whats missing in summary

The full transcript provides a comprehensive understanding of the historical context, potential consequences, and nuances of conflicts like Brexit and Ireland, urging for sensitivity, understanding, and peace.

### Tags

#Brexit #Ireland #Conflict #IrishAmericanCommunity #Violence #Peace


## Transcript
Well, howdy there, Internet Viva Lidsbo again.
So tonight, it's going to be a weird one.
It's sparked by a request from some of my friends.
So tonight, we're going to talk about what a drink can teach
us about Brexit, Ireland, nuance, romanticizing things,
just a whole bunch of stuff.
all boiled down to a drink.
OK, so some background information
before we get started for Americans who may not
have been keeping up with this.
Northern Ireland is part of the United Kingdom.
The United Kingdom has left the European Union.
They're out, gone.
The Republic of Ireland is not part of the United Kingdom.
Now, this leads to a whole lot of questions
that have very serious, very serious consequences in regards to the border between Northern
Ireland and the Republic.
It has more or less been open, it's been a soft border and the majority of those people
on that imaginary line, they want it to remain that way.
And we're not just talking about trade groups
and economic groups who engage in cross-border trade.
A lot of groups, because of the rhetoric used
to achieve Brexit, there is a big concern
that it will become a hard border,
that it will become checkpoints, perhaps even
militarized, that for a lot of groups, especially those groups who the Irish American community
has romanticized in a lot of ways, that is quite literally a triggering issue.
That is something that can cause violence.
A hard border will cause violence.
the dangers of imaginary lines becoming a little too real. So, let's just have to do with a drink.
A very popular drink in the United States is whiskey, cream, and stout. What's it called?
OK, so it's been called a car bomb without issue for the last 20 years or so.
Nobody's really kind of raised an eyebrow when you say that.
My friends, stereotypes being what they are, are most are, you know,
first generation Americans and yeah, big surprise, they own bars.
They want to take that drink off the menu if you want to.
take that drink off the menu. If you want that, it is now called an Irish Slammer.
You're going to see this request made a whole lot over the next month.
According to them, it's going to be on social media. They're going to try to
rename that drink and get rid of it. Now there's a whole bunch of reasons for
this. One is kind of odd. The Irish American community asking to be more
sensitive to about a subject that's not something that happens often but there
there are practical applications to this too one they don't want to even remotely
be seen as supporting any violence over this which is odd given some of their
histories but the there's a real concern that they don't they don't want to feed
into that. The other aspect to it is in the United States there has been a long
history of bars supporting one side or the other and not just with empty words
but sometimes with money. One of my favorite things to do when I have a
friend who's actually from Ireland who comes over is to take them to an Irish
bar in the U.S. around 1 a.m. on a Saturday night when a whole bunch of people who have
never set foot in Ireland are singing a soldier song, Boys of the Old Brigade, Men Behind
the Wire, stuff like that.
It is shocking to them because over there, generally, things have healed, tones have
softened a whole lot more than in the United States, which in many cases, tones have hardened.
And that tends to be the case when anything gets romanticized.
If you're romanticizing something and you weren't actually a part of it, if you weren't
one of the people dodging the actual rubber bullets, not just the songs or the actual
car bombs, yeah, it's easy to look at it as black and white.
The reality is that's a conflict that went on for 800 years.
There's a whole lot of nuance to it.
Yeah, some issues are black and white.
You can look at it in the sense of imperialism and colonialism, and yeah, sure.
But that doesn't matter much to people who were actually impacted.
One of the interesting things that I've noticed is that many of those who were active in the
more recent flare-ups, their tones have softened a lot and they tend to appreciate the nuance.
In fact, I'm going to put a song that doesn't get sung a lot in the United States down in
the comments, and that was written by somebody who was active, and if you listen to it, there's
a lot of nuance.
There's a lot of understanding.
something that we need to be aware of and not just in the sense of ordering a
drink on St. Patrick's Day but in the sense of this is the whole reason this
is coming about is rhetoric. It's an imaginary line. It's a line drawn on a
map. It's not worth people dying over. It's not worth militarizing. You know this
This is one of those things, if you let the people there decide what's going to happen,
you're not going to have this issue.
There's still going to be the nationalist tendencies among some, but that, a hard border
is just handing a recruitment tool to them.
It is handing a cause for more violence when there doesn't have to be one.
Brexit was about catching votes.
There's no reason for people to die over that.
And if you're in the U.S. and you are a part of that Irish American community, unless you're
going to go there and fight, shut up.
Just shut up.
I mean, they have enough to deal with from their own government.
They don't need cheerleaders egging it on.
That conflict, like many, like most, like almost all, it's the civilian that ends up
paying the price.
And it's not something that we should support, even unintentionally.
The romantic elements are there, and if you're part of the Irish-American community, especially
the more recent wave, you probably have Republican loyalties.
Believe me, I understand.
I know the songs, too.
We grew up with them.
We're entering a new age and self-determination notwithstanding, it's a little different
today.
The reasons for violence are lessening.
And truthfully, this may precipitate a peaceful, united Ireland, if it's given the chance.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}