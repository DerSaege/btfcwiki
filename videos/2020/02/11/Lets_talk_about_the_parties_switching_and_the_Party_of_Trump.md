---
title: Let's talk about the parties switching and the Party of Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-APCt4soM_I) |
| Published | 2020/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Delving into the transformation of the Republican Party from Lincoln to Trump.
- Exploring the historical context of the party switch.
- Using a timeline approach to understand the shift.
- Noting the change in electoral votes between 1960 and 1964 due to the Civil Rights Movement.
- Detailing the primary candidates in 1964 to show the party stances on segregation.
- Pointing out Goldwater's role in voting against the Civil Rights Act in 1964.
- Addressing accusations of intentionally altering the platform to attract racist voters.
- Referencing Strom Thurman's Dixiecrat movement in 1948 as a pivotal moment.
- Connecting the shift in the Republican Party to campaign strategies targeting the racist vote.
- Encouraging individuals to prioritize being a good person over blind party loyalty.

### Quotes

- "It's real clear, it's history, it's not something you can really debate."
- "Maybe it's time for people to stop worrying about their party and start worrying more about being a good person."
- "Their policies and the way they try to move those policies forward is racist. There's no big mystery here."

### Oneliner

Exploring the Republican Party's transformation from Lincoln to Trump through historical context, campaign strategies, and societal perceptions of racism.

### Audience

History enthusiasts, Political analysts, Voters

### On-the-ground actions from transcript

- Reassess party loyalty and prioritize personal values (implied)

### Whats missing in summary

A deeper dive into the impacts of historical shifts on current political landscapes.

### Tags

#RepublicanParty #History #CivilRights #CampaignStrategies #Racism #PartyLoyalty


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight, we're going to delve into a hotly debated topic,
because it's debated for some reason.
I'm not really sure why.
It's a question that pops up a lot.
And when people try to answer it, and I think this is the
reason it ends up getting debated, they try to answer it
by talking about campaign strategies and abstract things
like that.
However, it's a historical thing.
So rather than do it that way, let's just use a timeline.
Then we can see whether or not it happened
and why it happened.
And then we can talk about some of the other stuff.
The question is, how did the party of Lincoln
become the party of Trump?
Did the Republican Party and the Democratic Party
switch places, at least on some issues?
So rather than talking about the abstract,
So let's do a timeline.
Did it happen?
Look at the electoral votes for 1960, for the presidential election in 1960.
Look at all those blue states in the south.
Crazy!
That's something you wouldn't expect to see.
And you can go back a few elections and see that that trend holds.
Now look at 1964.
They're red.
So yes, it happened.
How and why?
What was going on in those states between 60 and 64?
The Civil Rights Movement.
Fair enough.
That's correlation though.
One of the things we talk about on this channel a lot is that correlation does not equal causation.
So we need to go a little bit further.
Who were the Democratic candidates for the primary for president in 64?
main ones Johnson and Wallace yeah that Wallace ardent segregationist and he
lost he lost he lost to the guy who signed the Civil Rights Act in 1964 okay
so that shows that the Democratic Party rejected at least that form of racism
segregation is bad. Alright, what about the Republicans? Who'd they run? Goldwater
who voted against the Civil Rights Act of 1964 and campaigned on state's rights.
It shows where the switch occurred and why. And that's good, it's pretty clear
there, but we need to be ironclad on this, right? I mean this is pretty heavy
the accusation saying that the Republican Party intentionally altered
their platform to go after the racist vote, we need to show that that's really
kind of the only reason that the South would stop voting Democrat. How are we
going to show that? Pull up the Electoral College the votes for 1948. Weird. The
The South didn't vote blue, didn't vote red either, who to vote for?
Strom Thurman, a segregationist under a third party, the Dixiecrats.
Yeah that's why it happened, that's how it happened.
It's real clear, it's history, it's not something you can really debate.
That's how the party of Lincoln turned into the party of Trump.
That's why the Republican party gets seen as the party of racists.
They altered their campaign strategy and platform to help them.
When you go out of your way to include them, that's how you get that reputation.
Sorry.
I would suggest that maybe it's time for people to stop worrying about their party and start
worrying more about being a good person.
A lot of Republicans that I know, they make excuses for their party's platform.
When it comes to the tangible, to the real, to the things they see themselves, a good
example is welfare, food stamps, stuff like that, right?
You talk to a Republican in abstract terms and you hear stuff like welfare queen and
stuff like that.
You talk to them about somebody that they know, well, that person needs help because
contrary to a lot of imagery, Republicans, the average Republican voter isn't evil.
They understand that sometimes people need help, but they have that image from that platform.
When you talk to them about individuals and you talk to them about people rather than
groups, a lot of the things the Republican Party opposes, they're actually four.
And then you conduct this little experiment.
The people that they think are okay and just need a little help, they're struggling.
Do they always look like them?
A lot of times it's true.
and the image they have of the Welfare Queen, well, they don't look like them.
And then you go back to how it originated, what this topic is really about.
Why is the Republican Party seen as the party of racists?
Because a lot of times, their rhetoric, their policies, and the way they try to move those
policies forward is racist. There's no big mystery here. The average Republican
may not be racist, but they kind of decided it wasn't a deal-breaker for
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}