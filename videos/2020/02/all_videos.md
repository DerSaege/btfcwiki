# All videos from February, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-02-28: Let's talk about the comments section and revisiting Rule 303.... (<a href="https://youtube.com/watch?v=ULJoGp69Ihc">watch</a> || <a href="/videos/2020/02/28/Lets_talk_about_the_comments_section_and_revisiting_Rule_303">transcript &amp; editable summary</a>)

Beau shares the Rule 303: If you have the means, you have the responsibility to act, encouraging individuals and communities to help those in need, especially during challenging times in the absence of solid leadership.

</summary>

"If you have the means at hand, you have the responsibility to act."
"It's up to us as individuals. It's up to us as communities."
"You do what you can when you can for as long as you can."
"There's probably somebody out there that could use your help."
"If you see somebody that needs help, you probably should. If you can."

### AI summary (High error rate! Edit errors on video page)

Unwritten rule of internet journalism: never read the comment section under your work to avoid negativity and criticism.
Shared a heartwarming experience of a viewer transcribing the entire video for a hearing-impaired person.
Explained Rule 303, stating that if you have the means, you have the responsibility to act.
Encourages helping others, especially during tough times in the US due to lack of solid leadership.
Emphasizes the importance of individuals and communities stepping up to help those in need.
Acknowledges the potential burnout from helping others and advises to do what you can sustainably.
Calls for collective action and support during challenging times when leadership is lacking.

Actions:

for online citizens,
Help those in need in your community by offering support and assistance (exemplified).
Step up to assist individuals facing challenges during tough times (exemplified).
</details>
<details>
<summary>
2020-02-28: Let's talk about Utah and a Valentine's Day dance gone wrong.... (<a href="https://youtube.com/watch?v=0VdApBcpgGs">watch</a> || <a href="/videos/2020/02/28/Lets_talk_about_Utah_and_a_Valentine_s_Day_dance_gone_wrong">transcript &amp; editable summary</a>)

An 11-year-old girl's discomfort at a school dance sparks debate on consent and entitlement, prompting reflections on what schools should be teaching.

</summary>

"Consent is a very basic premise of society."
"She said no. That's the end of the conversation."
"There is no reason for a principal to try to persuade or suggest or ask that she say yes."
"This is a really simple concept."
"If you're not teaching that, what can you possibly be teaching at this school?"

### AI summary (High error rate! Edit errors on video page)

Incident at a Valentine's Day dance in Rich County, Utah, involving an 11-year-old girl named Aislinn.
Two versions of events: Aislinn refused a dance, principal insisted she dance with the boy; school denies forcing anyone but encourages saying yes.
Aislinn felt uncomfortable with the boy and outlined reasons to the press.
Beau believes the boy is not to blame; it was on Aislinn to communicate her discomfort to the school.
School's actions send wrong messages about entitlement and consent, leaving boys with misguided ideas.
Beau criticizes the school's policy and the need for a policy review, insisting that the issue should just go away.
Emphasizes the importance of consent and how it should be a fundamental concept taught in schools.

Actions:

for school administrators, educators, parents.,
Contact the school to express concerns about their policy and handling of the situation (suggested).
Advocate for better education on consent and respect in schools (implied).
Initiate community dialogues on consent and healthy boundaries in education settings (implied).
</details>
<details>
<summary>
2020-02-27: Let's talk about what mom would do.... (<a href="https://youtube.com/watch?v=xGYnEFgDpGg">watch</a> || <a href="/videos/2020/02/27/Lets_talk_about_what_mom_would_do">transcript &amp; editable summary</a>)

Beau Gyan explains the vital role of parental figures for military recruits and shares valuable advice on self-care during illness, reminiscent of a caring mother's guidance.

</summary>

"What would your mom tell you to do?"
"Your mom sounds like a really smart lady."
"Probably also encourage you to wash your hands, don't touch your face, get a lot of hand sanitizer around."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of parental figures for young military recruits, who are often teenagers leaving home for the first time.
Recounts a story about a young soldier seeking medical advice, where the doctor's approach was reminiscent of a caring mother's guidance.
Shares valuable advice on dealing with cold season, reminiscent of what a wise mother might suggest.
Emphasizes the significance of basic self-care practices during illness, like rest, hydration, and monitoring symptoms.
Advises on maintaining hygiene by cleaning high-touch surfaces and designating a recovery room at home.
Encourages individuals to manage minor illnesses at home before seeking medical help.
Acknowledges the role of medical professionals while advocating for the reassuring calm that a mother's guidance provides.

Actions:

for military recruits and individuals seeking practical self-care advice.,
Ensure you have necessary supplies for cold season like food, soup, Pedialyte, and hand sanitizer (implied).
Clean high-traffic surfaces in your home regularly to prevent the spread of illnesses (implied).
Designate a specific room for sick individuals to rest and recover (implied).
</details>
<details>
<summary>
2020-02-27: Let's talk about Hemingway, icebergs, a different Bernie, and the unsaid.... (<a href="https://youtube.com/watch?v=r2Co3kv2rns">watch</a> || <a href="/videos/2020/02/27/Lets_talk_about_Hemingway_icebergs_a_different_Bernie_and_the_unsaid">transcript &amp; editable summary</a>)

Beau introduces the iceberg theory in storytelling, warns of the dangers of filling gaps with misinformation, and urges critical thinking amid incomplete information.

</summary>

"Subtext and those omissions, they can be great. They can be very powerful for illustrating the truth."
"People don't look at little bits of data and try to figure it out. They grasp for what they can find that will fill in the blanks to create a story they like."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of the iceberg theory in storytelling, where the storyteller can omit details as long as they know it should be there, and the audience fills in the gaps.
Mentions the theory of the death of the author, where once a story is public, only the audience's interpretation matters, not the author's intentions.
Talks about the significance of subtext in storytelling, especially when there are topics that can't be openly discussed due to social norms, censorship, or the climate of the time.
Describes the context of 1984 in American history, marked by trade issues, returning soldiers, homeless vets, crack, AIDS, and high crime rates in New York City.
Recounts the story of Bernie Getz, who shot four teens on a subway, sparking public debate on whether he was a hero or a psycho.
Explains how gaps in information lead people to fill in the blanks with their own biases and preconceived notions, often creating false narratives.
Warns about the dangers of filling in gaps with misinformation and how it can distort one's beliefs.
Draws parallels between the events of 1984 and the present day, urging caution in interpreting information and avoiding self-delusion.
Encourages critical thinking and awareness of the narratives we construct based on incomplete information.
Ends with a reminder to be mindful of how we interpret and fill in gaps in information to avoid falling into self-deception.

Actions:

for story consumers,
Question narratives and seek out multiple perspectives to avoid filling gaps with biased information (suggested).
Encourage critical thinking and fact-checking when forming opinions based on incomplete information (implied).
</details>
<details>
<summary>
2020-02-26: Let's talk about today's news media in a special way.... (<a href="https://youtube.com/watch?v=KuppkXTXx-I">watch</a> || <a href="/videos/2020/02/26/Lets_talk_about_today_s_news_media_in_a_special_way">transcript &amp; editable summary</a>)

Beau conducts a unique media experiment critiquing sensationalism, partisanship, and fear-mongering in news reporting during a fictionalized Cuban Missile Crisis broadcast.

</summary>

"Nothing to fear, but people who are slightly different than us."
"Murder on, man, murder on."
"We owe them that much. I agree with the Murder on Technology's representative."
"We need to spread democracy."
"Indeed, true patriots in this country know this soft-handed Eastern elite attitude is just going to lead to the destruction of the United States."

### AI summary (High error rate! Edit errors on video page)

Introducing a new experiment on the channel, discussing the media and its portrayal of news.
Criticizing the current media for bringing in biased experts without disclosure and portraying non-experts as experts.
Contrasting today's media mission with its historical purpose of conveying information, providing context, and instilling calmness.
Expressing concern over the media's sensationalism and partisanship in shaping news.
Imagining how today's media might have covered critical historical events like the Cuban Missile Crisis.
Presenting a fictional news segment from October 22, 1962, focusing on the Cuban missile crisis.
Featuring guests advocating for aggressive actions against Cuba, criticizing President Kennedy's pacifist approach.
Mocking the media's reliance on unqualified individuals presented as experts based on irrelevant factors like accents.
Showcasing fear-mongering tactics by the media, spreading misinformation about nuclear threats to instill panic.
Concluding with a commercial break parody and a segment with a Bay of Pigs veteran advocating for violent intervention in Cuba.

Actions:

for media consumers,
Fact-check news sources (implied)
Analyze media bias and conflicts of interest (implied)
Advocate for accurate and ethical journalism (implied)
</details>
<details>
<summary>
2020-02-26: Let's talk about the 1960s, ideas, and Woodstock.... (<a href="https://youtube.com/watch?v=NNNW7K7V4VE">watch</a> || <a href="/videos/2020/02/26/Lets_talk_about_the_1960s_ideas_and_Woodstock">transcript &amp; editable summary</a>)

Woodstock's chaotic journey from a commercial venture to a cultural phenomenon, proving the validity of '60s ideas through unity and music.

</summary>

"Woodstock was intended to be a money-making venture, not a free concert."
"Woodstock is iconic not because of free love or revolution but because it gave rise to punk rock."
"The event showcased ideas of the '60s that couldn't be packaged and sold, surviving through people walking through the gates without buying tickets."

### AI summary (High error rate! Edit errors on video page)

Woodstock was intended to be a money-making venture, not a free concert.
The original Summer of Love happened in 1967 in San Francisco, not at Woodstock in 1969.
Half a million people showed up at Woodstock when only 50,000 were expected, causing chaos.
Woodstock faced logistical issues, leading to it becoming a free concert due to the overwhelming number of attendees.
Jimi Hendrix, the headliner, played to a much smaller crowd as most people had already left by the time he performed.
Woodstock organizers didn't make a profit until the 1980s from residual income, facing financial losses initially.
Woodstock is iconic not because of free love or revolution but because it gave rise to punk rock.
The event showcased ideas of the '60s that couldn't be packaged and sold, surviving through people walking through the gates without buying tickets.
Despite the lack of visible security or real authority, Woodstock remained peaceful and became a cultural phenomenon, proving the validity of its ideas.
Woodstock was the last great victory of a movement that couldn't be marketed, remembered for its essence of love and community.

Actions:

for history enthusiasts, music lovers,
Attend or organize community music events to foster unity and share ideas (suggested)
Host gatherings where people can share music, ideas, and food in a peaceful, inclusive setting (implied)
</details>
<details>
<summary>
2020-02-26: Let's talk about Trump saying the quiet part aloud.... (<a href="https://youtube.com/watch?v=fWanhxcNZTE">watch</a> || <a href="/videos/2020/02/26/Lets_talk_about_Trump_saying_the_quiet_part_aloud">transcript &amp; editable summary</a>)

Beau Gyan explains how the President's habit of saying the unsaid aloud, especially regarding military involvement and alliances, raises concerns about potential repercussions and Russian interference.

</summary>

"It's called pillaging, not just for pirates, and it is against international law."
"He's pushing the most powerful non-state actor into Russia's arms."
"It's dividing them, creating an oil curtain rather than an iron curtain."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the "quiet part" that remains unsaid in normal and political discourse to avoid discomfort.
Points out how the President of the United States has a habit of saying the quiet part aloud at inappropriate times.
Mentions the recent instance in New Delhi where the President openly talked about US military involvement in Syria being primarily about oil.
Criticizes the act of using military force to extract wealth from a nation as a form of colonialism or pillaging, which goes against international law.
Expresses concern over the President's statements indicating military alliances with Russia, Iran, Iraq, and Syria against non-state actors in the Middle East.
Raises alarm about the potential long-lasting repercussions of such alliances and the implications for US dominance in the region.
Condemns the President's actions that may lead to dividing countries in the region along oil interests rather than ideological lines.
Stresses the importance of acknowledging Russian interference, especially when the President advocates for actions that benefit Russia.
Urges for attention to be paid to these concerning developments and the potential consequences of such foreign policy decisions.

Actions:

for foreign policy analysts,
Contact policymakers to express concerns about potential military alliances that could undermine US interests (implied)
Stay informed about international developments and advocate for transparent foreign policy decisions (implied)
</details>
<details>
<summary>
2020-02-25: Let's talk about what Bernie supporters can learn from Ole Miss.... (<a href="https://youtube.com/watch?v=b04Qh0Qu-9w">watch</a> || <a href="/videos/2020/02/25/Lets_talk_about_what_Bernie_supporters_can_learn_from_Ole_Miss">transcript &amp; editable summary</a>)

Beau stresses the importance of long-term commitment and focusing on ideas, not individuals, for successful movements seeking deep systemic change.

</summary>

"It's got to be about the ideas."
"If it's about anything else, you're setting yourself up for failure."
"You make it about a person, you make it about an individual, some personality, you will lose."

### AI summary (High error rate! Edit errors on video page)

Recalls an incident at Ole Miss where 40 black students staged a demonstration in 1962 by sitting at different tables in the cafeteria, demanding minor requests.
The establishment did not grant their requests, resulting in mass arrests.
Beau underscores the importance of understanding the American Civil Rights Movement's strategic and philosophical lessons for those seeking deep systemic change in the present United States.
Points out the need for long-term commitment and readiness for an enduring campaign for change.
Mentions the necessity of movements being centered around ideas rather than individuals to avoid vulnerability.
Emphasizes the vulnerability of movements that focus on personalities, as they can easily be dismantled through various means.
Notes that movements built around personalities often crumble when the individual is targeted.
Argues that movements need to be about ideas, policies, and deep systemic change to be successful.
States that the American Civil Rights Movement's success stemmed from focusing on the idea and the dream rather than individuals.
Encourages rallying people behind ideas for systemic change and justice rather than personalities.

Actions:

for activists, social justice advocates,
Rally people behind ideas for systemic change and justice (implied).
</details>
<details>
<summary>
2020-02-22: Let's talk about Trump, the Rosenbergs, Cohn, McCarthy, and Stone.... (<a href="https://youtube.com/watch?v=nRpfU_idUw0">watch</a> || <a href="/videos/2020/02/22/Lets_talk_about_Trump_the_Rosenbergs_Cohn_McCarthy_and_Stone">transcript &amp; editable summary</a>)

Beau delves into the historical connections between spies, prosecutors, and politicians, shedding light on the narrow definition of treason in the United States and the implications of cyber attacks as potential acts of war.

</summary>

"Treason in the United States is extremely specific."
"Foreigners, people who do not owe allegiance to the United States, can't be charged with treason."
"Acts of war don't happen that often."
"The Pentagon has argued that cyber attacks are an act of war."
"It's worth noting that the Pentagon has argued that cyber attacks are an act of war."

### AI summary (High error rate! Edit errors on video page)

Exploring the interconnectedness of historical figures like the Rosenbergs, Roy Kahn, Joe McCarthy, Roger Stone, and Donald Trump.
The Rosenbergs were highly effective spies for the Soviets in the 20th century, recruiting other spies and providing high-value information.
Roy Kahn, one of the prosecutors in the Rosenberg case, was known for his hard-right stance and association with Joe McCarthy during the Red Scare.
Kahn also worked with Roger Stone on Reagan's campaign and later represented Donald Trump in New York.
Treason in the United States is narrowly defined, making it difficult to charge individuals with this offense.
The Rosenbergs were not charged with treason but with conspiracy to commit espionage, carrying severe penalties.
In the U.S., treason requires allegiance to the United States and an act of war or support for a war effort.
Treason could involve a scenario where a person in public office aids a foreign government's act of war against the U.S.
The Pentagon considers cyber attacks as potential acts of war, raising concerns about today's security landscape.

Actions:

for history enthusiasts,
Research historical events and figures mentioned (suggested)
Stay informed about cybersecurity issues and potential threats (suggested)
</details>
<details>
<summary>
2020-02-22: Let's talk about Bernie, Trump, and Russians.... (<a href="https://youtube.com/watch?v=E7t45Tdm548">watch</a> || <a href="/videos/2020/02/22/Lets_talk_about_Bernie_Trump_and_Russians">transcript &amp; editable summary</a>)

Beau explains Russian interference, potential support for Bernie and Trump, and the importance of candidates' integrity in not being influenced.

</summary>

"Countries try to influence other countries' leadership to advance their foreign policy."
"Understanding what the Russians want is key, not projecting motives onto them."
"Bernie being the peace candidate benefits the Russians in terms of foreign policy."
"The real question is whether candidates have the integrity to not be influenced by foreign support."
"U.S. counterintelligence's role is to monitor candidates to prevent direct foreign influence."

### AI summary (High error rate! Edit errors on video page)

Talks about the debate surrounding Russian interference in elections and their potential support for Bernie and Trump.
Explains that countries try to influence other countries' leadership to advance their foreign policy.
Mentions historical examples of countries supporting leaders in other nations based on shared interests.
Points out that Russian organized crime is strategic and plays chess, not poker like Italian organized crime.
Suggests that Russians may support both sides in an election to achieve their goals.
Emphasizes that understanding what the Russians want is key, not projecting motives onto them.
States that the Russians are interested in influencing foreign affairs, not degrading American democracy.
Argues that Bernie being the peace candidate benefits the Russians in terms of foreign policy.
Raises the importance of candidates maintaining integrity and not being influenced by foreign support.
Mentions U.S. counterintelligence's role in monitoring candidates to prevent direct foreign influence.

Actions:

for voters, political analysts,
Monitor candidates for foreign influence (implied)
</details>
<details>
<summary>
2020-02-21: Let's talk about driving Dixie down.... (<a href="https://youtube.com/watch?v=Ni3HWJxE6kw">watch</a> || <a href="/videos/2020/02/21/Lets_talk_about_driving_Dixie_down">transcript &amp; editable summary</a>)

Beau explains the history of the Dixie Highway and advocates for renaming it to honor progress and reject a dark past.

</summary>

"Miami has just elected to rename theirs, it will now be the Harriet Tubman Highway and I think that is just awesome on so many levels."
"Economically, it makes sense. Morally, it makes sense. Historically, it makes sense."
"There's no reason to keep this when it could be used to demonstrate that we do have a new South, that the country is moving forward."

### AI summary (High error rate! Edit errors on video page)

Explains the history of the Dixie Highway, a highway network in the United States running from Michigan to Miami, created in the early 1900s.
Notes that Carl G. Fischer, the mind behind the highway, previously worked on the Lincoln Highway, suggesting that the choice of the name Dixie was not to glorify it.
Mentions that the term Dixie refers to the region south of the Mason-Dixon line where slavery was legal.
Talks about how the Dixie Highway did not gain the same tourism attention as other famous highways like Route 66.
Acknowledges that historical events tied to Dixie, such as opposition to civil rights, make it a controversial symbol.
Miami recently renamed its portion of the Dixie Highway to the Harriet Tubman Highway, which Beau praises as a positive step forward.
Speculates on the economic and cultural benefits that could arise if other jurisdictions along the highway also choose to rename their sections.
Suggests that renaming the highway could attract more tourists and revenue to rural communities along the route.
Expresses curiosity about how different areas will respond to the renaming, seeing it as a test of whether they choose to honor a dark past or a heroic figure.
Encourages moving forward by letting go of glorifying a period in American history associated with slavery and racism.

Actions:

for history enthusiasts, community activists,
Advocate for renaming controversial symbols in your community (exemplified)
Support initiatives that celebrate diversity and progress in your area (exemplified)
</details>
<details>
<summary>
2020-02-20: Let's talk about misunderstanding Bernie's supporters.... (<a href="https://youtube.com/watch?v=8qyHbdVsUho">watch</a> || <a href="/videos/2020/02/20/Lets_talk_about_misunderstanding_Bernie_s_supporters">transcript &amp; editable summary</a>)

Bernie is the compromise, not the radical position; failure to recognize this may lead to further political tensions and radicalization among marginalized communities.

</summary>

"Bernie is the compromise, not the radical position."
"The Democratic Party misunderstands Bernie's supporters."
"Failure to embrace compromise may escalate political tensions towards radicalism."
"Bernie advocates policies similar to those in other western nations."
"Be Democratic during the nomination."

### AI summary (High error rate! Edit errors on video page)

Bernie is the compromise, not the radical position, for millions of Americans seeking deep systemic change.
The Democratic Party misunderstands Bernie's supporters, viewing him as too radical when he is actually the compromise.
Bernie represents proactive food security measures, while the establishment fails to grasp this.
If Bernie is denied the nomination despite having the lead and support, his supporters may refuse to back the chosen candidate.
Bernie supporters see him as the compromise, and not supporting him could lead to further radicalization among marginalized communities under Trump.
The Democratic establishment's view of Bernie as too far left reveals the party's shift to the right.
Failure to embrace compromise may escalate political tensions towards radicalism.
Bernie advocates policies similar to those in other western nations, dispelling the notion of his radicalism.

Actions:

for progressive voters,
Support proactive food security measures in your community (implied)
Advocate for policies that benefit marginalized communities (implied)
</details>
<details>
<summary>
2020-02-20: Let's talk about Republics and Democracies.... (<a href="https://youtube.com/watch?v=0bHdU-d_dFw">watch</a> || <a href="/videos/2020/02/20/Lets_talk_about_Republics_and_Democracies">transcript &amp; editable summary</a>)

Beau explains the democracy vs. republic debate in the US and questions if the constitution truly protects all citizens.

</summary>

"The US is a democracy because it is. Right now in the comment section somebody is saying, no, we're a republic."
"Major portions of the Constitution are there strictly to protect the ruling class and to disenfranchise many people."
"Governments don't fit into buckets like that."
"It may be time to revisit some of those things that we hold to be true."

### AI summary (High error rate! Edit errors on video page)

Explains the ongoing debate between democracy and republic in the US.
Mentions that Americans are brought up to question their national identity, leading to confusion.
Clarifies that the US is a representative democracy, not a direct democracy.
Points out that when people say "republic" in the US context, they are essentially stating the absence of a monarch.
Compares the US government system to the UK's parliamentary system and China's republic.
Talks about the protection from tyranny of the majority in the US constitution.
Mentions the concept of tyranny of the minority where a small number of elected officials can make decisions for the majority.
Raises the question of whether elected officials truly represent the people once in office.
Suggests that certain aspects of the constitution protect the ruling class and disenfranchise many.
Concludes by hinting at the need to reexamine beliefs about democracy in light of recent events.

Actions:

for citizens, voters, activists,
Question the representation and decision-making processes of elected officials (implied).
Revisit beliefs about democracy and its functioning in the current context (suggested).
</details>
<details>
<summary>
2020-02-19: Let's talk about the Boy Scouts and a golden opportunity.... (<a href="https://youtube.com/watch?v=BqwrZb7Ia9o">watch</a> || <a href="/videos/2020/02/19/Lets_talk_about_the_Boy_Scouts_and_a_golden_opportunity">transcript &amp; editable summary</a>)

Beau suggests remaking the Boy Scouts into a modern, inclusive organization focused on core values and internal growth, rather than external achievements.

</summary>

"The core values need to be instilled."
"It should be more about fostering internal growth than putting patches on a sash."
"This is something that somebody watching this can do."

### AI summary (High error rate! Edit errors on video page)

The Boy Scouts started with solid values of service to community, self-reliance, and doing your best.
Over time, nostalgia took over, the organization lost focus, and it became politically controversial.
Many former Scouts value the moral code instilled in them by the organization.
Beau suggests remaking the Scouts into a modern American image with inclusive values and less nationalism.
He proposes creating a decentralized form of scouting led by parents in the community, focusing on core values and flexibility.
The core issue with the Boy Scouts was the overrun of nationalism and a paramilitary structure.
Beau believes that an organization dedicated to creating strong young people is needed, focusing on internal growth over external achievements.

Actions:

for parents, community members,
Create a decentralized form of scouting led by parents in the immediate community (implied)
Incorporate STEM learning into scouting activities (implied)
</details>
<details>
<summary>
2020-02-19: Let's talk about giving the establishment more power.... (<a href="https://youtube.com/watch?v=jSdpj6lA2V8">watch</a> || <a href="/videos/2020/02/19/Lets_talk_about_giving_the_establishment_more_power">transcript &amp; editable summary</a>)

Beau criticizes proposals for elite control, advocates for direct democracy, and stresses active citizen involvement to avoid oligarchy.

</summary>

"That's oligarchy. That's not democracy."
"Democracy is advanced citizenship."
"Letting the elites do your thinking for you, that's how we wound up here."

### AI summary (High error rate! Edit errors on video page)

Criticizes an op-ed suggesting giving elites more control in choosing the president.
Establishment faces difficulty with preferred candidates and wants more influence.
Proposes ranked choice voting combined with non-binding exit polls for elites.
Calls out this proposal as oligarchy rather than democracy.
Advocates for direct democracy where popular vote determines primary candidates.
Suggests a process where candidates collect signatures, debate, and face elimination rounds.
Emphasizes the need for year-round involvement to avoid last-minute choices and poor leaders.
Stresses the importance of removing power from party elites and involving the rank and file.
Believes in including third-party candidates with a million signatures to remove party affiliation.
Concludes by urging for active citizenship and caution against letting elites dictate decisions.

Actions:

for voters, democracy advocates,
Collect a million signatures for presidential candidacy in January (suggested)
Be actively involved year-round in political processes (implied)
</details>
<details>
<summary>
2020-02-19: Let's talk about Bolton, Trump, and Korea.... (<a href="https://youtube.com/watch?v=0LGwIs7wpao">watch</a> || <a href="/videos/2020/02/19/Lets_talk_about_Bolton_Trump_and_Korea">transcript &amp; editable summary</a>)

Beau explains the historical context of the Korean conflict, criticizes hawkish foreign policies, and suggests economic engagement to prevent wars and create prosperity.

</summary>

"You don't need to worry about the person that wants 10. You need to worry about the person that wants one, because they're going to use it."
"Trump's approach of bringing them out through economics is probably right."
"If everybody's engaged in trade, there's less chance of a war."
"It's probably right. He just can't execute it."
"Until then, our best bet is to just try to delay them obtaining a weapon."

### AI summary (High error rate! Edit errors on video page)

Gives a historical overview of the Korean conflict from 1910 onwards, including the involvement of major powers like the Soviet Union and the US.
Talks about the surprising invasion of South Korea by North Korea in 1950.
Mentions the role of Stalin in potentially green-lighting the invasion to entangle the US in a conflict in Asia.
Describes the back-and-forth military actions between the US, UN forces, and North Korea.
Explains how the US involvement in the Korean conflict was not solely for South Korea's freedom but also to realign North Korea from communism to capitalism.
States that North Korea seeks nuclear weapons as a deterrent rather than for offensive use.
Criticizes John Bolton's hawkish foreign policy approach and suggests Trump's economic approach might be more effective in bringing countries out of isolation.
Emphasizes the importance of engagement in trade to prevent wars and create prosperity.

Actions:

for foreign policy analysts,
Engage in trade with countries to prevent conflicts and foster prosperity (suggested).
Advocate for diplomatic approaches over hawkish foreign policies (implied).
</details>
<details>
<summary>
2020-02-18: Let's talk about Mickey, Mallory, Charles, Caril, and redemption.... (<a href="https://youtube.com/watch?v=Q_QHHV5PZiE">watch</a> || <a href="/videos/2020/02/18/Lets_talk_about_Mickey_Mallory_Charles_Caril_and_redemption">transcript &amp; editable summary</a>)

Beau introduces a story of potential redemption involving Carol Fugate seeking a pardon at 76, sparking reflections on justice for a 14-year-old involved in past crimes.

</summary>

"Maybe it's redemption, maybe it's something else."
"She is now 76 years old and she's petitioning for a pardon."
"We'll have to wait and see what happens."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Introduces a story of possible redemption, uncertain of the outcome.
Mentions Mickey and Mallory Knox from a spree on Route 666, comparing to historical counterparts Charles Starkweather and Carol Fugate.
Describes Carol as 14 and Charles as 19 during a week-long spree that took 10 lives.
Notes conflicting narratives about Carol's role, with one suggesting she went along to protect her family.
Reveals Carol served 18 years for her involvement, now 76, seeking a pardon backed by a victim's granddaughter.
Argues that if it happened today, Carol wouldn't have been tried, being treated as a victim witness.
Suggests the pardon may not change much but could be a recognition of how the situation might be handled today.
Tomorrow, the petition for Carol's pardon goes before the board, awaiting the outcome.
Raises the idea of giving a platform to hear the story of the 14-year-old involved in past crimes.
Ends with a reflective thought and wishes the audience a good night.

Actions:

for advocates, justice seekers,
Support initiatives advocating for fair justice for individuals involved in crimes at a young age (implied).
</details>
<details>
<summary>
2020-02-18: Let's talk about Disney getting Frozen feet.... (<a href="https://youtube.com/watch?v=TXPVsRhB0Yw">watch</a> || <a href="/videos/2020/02/18/Lets_talk_about_Disney_getting_Frozen_feet">transcript &amp; editable summary</a>)

Beau watches Frozen films with his daughter, pointing out Disney's subtle subtext and the unacknowledged queer potential of Elsa, urging for more explicit representation in future films.

</summary>

"I mean, come on. But it's never acknowledged. And it bothers me."
"The fact is Disney is a massive machine. Disney could be an engine for social change."
"Because the fact is Disney is a massive machine. Disney could be an engine for social change."

### AI summary (High error rate! Edit errors on video page)

Beau watches both Disney Frozen films back to back with his daughter, noticing the lack of closure in the second film.
He points out that Disney always includes subtext in their storytelling, like in Peter Pan, and meticulously plans everything as an organization.
Beau describes the plot of the first Frozen movie, focusing on the two princess sisters, Elsa and Anna, and the secrets and conflicts between them.
Elsa, the older sister, has powers to control ice and snow but also harbors another secret hinted at in the films.
In the second film, Elsa hears a mysterious voice calling her out into the world, uncovering family secrets along the way.
Despite various hints and subtext throughout both films, Beau notes that Elsa's true nature is never overtly acknowledged.
He expresses his frustration at the lack of acknowledgment of Elsa's potential queerness and the importance of representation for young audiences.
Beau suggests that Disney, as a powerful entity, could be a force for social change by portraying Elsa's difference more explicitly in future films.

Actions:

for disney fans, lgbtq+ advocates,
Advocate for more diverse and inclusive representation in media through petitions and letters to production companies (implied)
</details>
<details>
<summary>
2020-02-17: Let's talk about the wholesome 1950s.... (<a href="https://youtube.com/watch?v=XCDoYkAcvRg">watch</a> || <a href="/videos/2020/02/17/Lets_talk_about_the_wholesome_1950s">transcript &amp; editable summary</a>)

Beau dismantles the myth of the wholesome 1950s, revealing it as a politically correct version of history that never truly existed.

</summary>

"When people say, make America great, and they're looking to the 1950s, they're looking back to a mythology, they're looking back to something that never existed."
"The 1950s were not all leave it to Beaver."
"It wasn't real. It was a censored, politically correct version of the times."

### AI summary (High error rate! Edit errors on video page)

Talks about the myth of the wholesome 1950s and the desire to reach back to that era.
Mentions how certain political slogans reference the 1950s as a time to "make America great" based on movies' portrayal.
Points out the reality behind the wholesome image, including icons like Marilyn Monroe and themes in Broadway productions.
Explains how the production code in movies enforced a certain image by prohibiting controversial topics.
Argues that books provide a more accurate view of the 1950s, mentioning works like "Catcher in the Rye" and "On the Road."
Emphasizes that the 1950s were not all as wholesome as portrayed, referencing global works like "Dr. Zhivago."
Concludes that the nostalgia for the 1950s is based on a myth and a politically correct version of history.

Actions:

for history enthusiasts, truth-seekers,
Read books from the 1950s era to gain a more accurate understanding of the time (suggested).
Challenge nostalgic narratives about historical eras by seeking diverse perspectives (exemplified).
</details>
<details>
<summary>
2020-02-17: Let's talk about Jeff Bezos, space, climate, and journeys forward.... (<a href="https://youtube.com/watch?v=7YbUxb0Ghms">watch</a> || <a href="/videos/2020/02/17/Lets_talk_about_Jeff_Bezos_space_climate_and_journeys_forward">transcript &amp; editable summary</a>)

Bezos pledges $10 billion for climate change; addressing it is a challenge propelling humanity forward with no downside.

</summary>

"The journey to combat it, to reduce carbon, all of this stuff, the technologies that are going to come from it, are going to propel mankind forward."
"There is no downside to attempting to mitigate climate change."
"This is this generation, this time period, this is its space race, this is its World War II."

### AI summary (High error rate! Edit errors on video page)

Bezos is allocating $10 billion to combat climate change, sparking debate on its intent and impact.
Draws parallels between addressing climate change and historical challenges like the space race.
Mentions the indirect benefits of past challenges, like space race innovations that became everyday technology.
Emphasizes that tackling climate change will lead to technological advancements and propel humanity forward.
Suggests that even if one doesn't believe in climate change, the journey to combat it will result in positive developments.
Argues that investing in fighting climate change is necessary due to the potential benefits and cost savings in the long run.
Compares the current battle against climate change to past significant events that pushed humanity forward.
Urges for action and sees addressing climate change as a challenge that humanity must undertake for the betterment of all.
Views combating climate change as an investment in advancing humanity with no real downside.

Actions:

for global citizens, environmental activists,
Invest in renewable energy sources (implied)
Support initiatives combating climate change (implied)
Advocate for sustainable practices in daily life (implied)
</details>
<details>
<summary>
2020-02-16: Let's talk about what California can learn from the French Foreign Legion.... (<a href="https://youtube.com/watch?v=05kxe2IH00g">watch</a> || <a href="/videos/2020/02/16/Lets_talk_about_what_California_can_learn_from_the_French_Foreign_Legion">transcript &amp; editable summary</a>)

Beau suggests California learn from the Foreign Legion and implement AB 2147 to offer inmates a second chance through firefighting, reducing recidivism and benefiting the community and state.

</summary>

"Imagine being able to cut down recidivism."
"If you can trust an inmate with an axe, probably not a danger."
"There's no downside to this."
"This is something that's helping the community and it's helping the state while it's helping them."
"This seems like a no-brainer."

### AI summary (High error rate! Edit errors on video page)

Comparing the state of California to the French Foreign Legion, suggesting lessons to be learned.
The Foreign Legion is known for giving individuals a new identity and a fresh start after leaving.
Despite some members having criminal backgrounds, they put their lives on the line and are changed by their experiences.
Mentioning Dien Bien Phu as a significant battle that marked the end of French involvement in Vietnam.
Advocating for California's AB 2147 bill, which aims to create a California firefighter legion for inmates who are trained as firefighters.
Emphasizing the benefits of allowing these inmates to clear their records and become firefighters, reducing recidivism.
Expressing belief in the positive impact of providing second chances through skill-building opportunities.
Stressing the importance of implementing this legislation as a way to help both the community and the state.
Acknowledging past legislative challenges in California but underlining the potential of this bill to be a significant benefit.
Concluding with a call to action for California not to miss out on this beneficial legislation.

Actions:

for california policymakers, activists,
Support the implementation of AB 2147 for the creation of a California firefighter legion (suggested).
Advocate for legislation that provides second chances and skill-building opportunities for inmates (exemplified).
</details>
<details>
<summary>
2020-02-16: Let's talk about walls and legacies.... (<a href="https://youtube.com/watch?v=S_VM-KApNrg">watch</a> || <a href="/videos/2020/02/16/Lets_talk_about_walls_and_legacies">transcript &amp; editable summary</a>)

Beau questions the legacy of Trump's border wall, foreseeing it as a symbol of incompetence and fiscal irresponsibility rather than oppression.

</summary>

"The wall is not built yet. If you want a good gauge of how easily the wall is defeated, understand they're choosing to go to the wall."
"I don't see this wall being recorded as a great symbol of oppression, I think it's going to be recorded as a massive symbol of incompetence."
"It will be a symbol of massive fiscal irresponsibility."

### AI summary (High error rate! Edit errors on video page)

August 13th, 1961: the Berlin Wall went up, seen as a symbol of oppression dividing a city, country, and world.
The Berlin Wall separated West Berlin from East Germany, symbolizing oppression until it fell in 1989.
Trump's wall on the southern border is questioned for its potential legacy like the Berlin Wall.
The wall's potential legacy may not be as weighty due to hardened hearts and prioritization of money over morality.
The effectiveness of Trump's wall is doubted as reports show various ways people easily breach it.
People are actively choosing to go to the wall because it's easier to use it than to go around it, indicating its ineffectiveness.
The wall being defeated even before completion raises concerns about its actual utility.
Cutting through the wall with saws and scaling it with rebar-made ladders are some tactics used to breach it easily.
The wall might be remembered not as a symbol of oppression but as a testament to incompetence and fiscal irresponsibility.
Beau predicts Trump's wall will be associated with his legacy of wasteful spending and inefficiency.

Actions:

for border wall critics,
Contact local representatives to voice opposition to the construction of the border wall (suggested).
Support organizations advocating for immigration reform and more effective border security measures (exemplified).
</details>
<details>
<summary>
2020-02-15: Let's talk about dancing left and right.... (<a href="https://youtube.com/watch?v=ajy-_QZUouM">watch</a> || <a href="/videos/2020/02/15/Lets_talk_about_dancing_left_and_right">transcript &amp; editable summary</a>)

Beau talks about dancing in politics, warns against moving too far to the right, and calls for standing on principle to unite the country.

</summary>

"We're the place for people to escape to. We're not the place for walls. We're not the place for SWAT teams."
"If everybody in this country stands on principle, we will be closer together than we can possibly imagine."
"You have gone from what this country is supposed to stand for."
"Dancing further and further right, kicking you along with that authoritarian boot."
"We don't know how lucky we are."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of dancing and its significance in the political discourse.
Two friends talking to a Cuban refugee who escaped Castro realize their privilege when he mentions having a place to escape to.
The Cuban refugee's statement about losing freedom here being the last stand on Earth resonates deeply.
Beau mentions Ronald Reagan's rhetoric about America being the place people are supposed to escape to.
Beau criticizes the Democratic Party for continuously moving to the center to chase votes, lacking backbone and principle.
He draws parallels between Reagan's promotion of Barry Goldwater and the current political landscape.
Beau warns Republicans about dancing further right and selling out the country's values for loyalty to a leader.
He urges for reflection on principles to bring the country closer together and resist division created by those in power.
Beau advocates for America as a place for people to escape to, not for walls or aggression.

Actions:

for americans,
Stand on principle and resist division created by political leaders (implied)
Advocate for America as a place for people to escape to, not for walls or aggression (implied)
</details>
<details>
<summary>
2020-02-14: Let's talk about the stakes and strategy of the 2020 election.... (<a href="https://youtube.com/watch?v=ucnCZI3Q4ms">watch</a> || <a href="/videos/2020/02/14/Lets_talk_about_the_stakes_and_strategy_of_the_2020_election">transcript &amp; editable summary</a>)

Remind people of the stakes, focus on fighting for principles over personalities, and strategize wisely amidst political battles.

</summary>

"We have to think of things in the affirmative."
"Principle over personality."
"We have to think of things in the affirmative."
"If we personify the evil that exists in one person, we become very easy to manipulate."

### AI summary (High error rate! Edit errors on video page)

Remind people of the stakes amidst the constant influx of news to refocus on what we're fighting for rather than against.
Points out the history of a New York billionaire who has minimized civil rights, ignored income inequality, advanced surveillance and police state, and scapegoated minorities.
Raises the question of whether the billionaire in question is Trump or Bloomberg, showcasing the danger of personifying what we're fighting against.
Suggests a military analogy where allowing opposing forces to weaken each other could be a strategic move before targeting the ultimate opposition.
Advocates for letting Sunday, Sunday, Sunday, the Battle of the Billionaires play out to potentially weaken the larger opposition force.
Considers the possibility of Bloomberg winning and the implications of him securing the Democratic nomination.
Expresses concern over voters' inability to see through propaganda and focus on principles rather than personalities.
Warns against blindly following the "vote blue no matter who" mantra and the dangers it poses in allowing bad actors to secure nominations.
Proposes a long-term strategy of allowing Bloomberg to weaken Trump before dealing with him in the primaries.
Emphasizes the importance of remembering what is being fought for and prioritizing principles over personalities.

Actions:

for voters, political activists,
Strategize and prioritize focusing on principles over personalities in political decision-making (implied).
Stay informed about political candidates' histories and priorities to make informed voting choices (implied).
</details>
<details>
<summary>
2020-02-14: Let's talk about the pendulum (Roger Stone Part 2).... (<a href="https://youtube.com/watch?v=YQxlsw6_EMI">watch</a> || <a href="/videos/2020/02/14/Lets_talk_about_the_pendulum_Roger_Stone_Part_2">transcript &amp; editable summary</a>)

Beau urges us to question the status quo, push for criminal justice reform, and create a better future rather than settling for past norms.

</summary>

"We have to start thinking about this stuff now."
"If we are going to create a just society, fixing the failures in our criminal justice system should be pretty near the top of the list."

### AI summary (High error rate! Edit errors on video page)

The country is shifting back from the right and we have a chance to create something better rather than just returning to normal.
Questioning our beliefs is necessary, particularly regarding long prison sentences and their effectiveness in deterring crime.
Criminal justice reform is imperative and requires new ideas and heavy implementation.
When certain currently illegal substances become legal, the approach towards those involved must change to focus on treatment and community service for non-violent offenders.
Planning for the future of criminal justice reform needs to start now, not when the momentum has subsided.
Beau acknowledges provoking reactions to draw attention to the excessive sentences within the system.
Fixing the failures in the criminal justice system should be a top priority in creating a just society.

Actions:

for advocates for justice reform.,
Start questioning beliefs on the effectiveness of long prison sentences now, suggested.
Push for criminal justice reform by advocating for new ideas and heavy implementations, implied.
Plan for a future where the failures of the criminal justice system are rectified, exemplified.
</details>
<details>
<summary>
2020-02-14: Let's talk about Valentine's Day, tradition, and ancient Woodstock.... (<a href="https://youtube.com/watch?v=ysVLefWw8As">watch</a> || <a href="/videos/2020/02/14/Lets_talk_about_Valentine_s_Day_tradition_and_ancient_Woodstock">transcript &amp; editable summary</a>)

Beau questions the origins of Valentine's Day, delves into its history, and urges reevaluation of traditions in times of social change.

</summary>

"We don't question it. we've just always done it this way, so we don't change."
"When you examine traditions sometimes, you realize that they may not be worth continuing."
"Now is the time to examine those traditions and that mythology."

### AI summary (High error rate! Edit errors on video page)

Beau starts off by addressing the audience on Valentine's Day, mentioning the various ways people celebrate the day.
The videos throughout the week had themes centered around tradition, radical ideas, and questioning the importance of holding on to traditions.
Valentine's Day is rooted in tradition, with people exchanging chocolates without really knowing the full history behind the day.
The popular theory behind Valentine's Day involves a priest named Valentine who defied Emperor Claudius II by marrying soldiers in secret, leading to his beheading and subsequent canonization as a saint.
The romantic aspect of Valentine's Day didn't emerge until Shakespearean times, and the industrial revolution saw Hallmark commercializing the day with cards.
February 14th was chosen by the church to coincide with a pagan festival involving music, love, nudity, and rituals like drawing names for temporary romantic partners.
Beau questions the continuation of certain traditions, suggesting that some may be outdated or have lost their original meaning.
He stresses the importance of examining and reevaluating the traditions and mythology prevalent in the U.S., particularly in times of social upheaval.
Despite the discomfort that may arise from challenging established traditions, Beau believes it is necessary for societal renewal and progress.
Beau warns that without proactive choices and planning from the people, those in power can easily introduce new traditions or uphold the status quo.

Actions:

for curious individuals,
Examine and question traditions (suggested)
Reassess cultural myths and practices (suggested)
Plan for societal renewal (implied)
</details>
<details>
<summary>
2020-02-13: Let's talk about you, hope, and my key chain.... (<a href="https://youtube.com/watch?v=PPcEBpnQlDg">watch</a> || <a href="/videos/2020/02/13/Lets_talk_about_you_hope_and_my_key_chain">transcript &amp; editable summary</a>)

Beau's diverse audience united by pursuit of freedom faces authoritarian threats but can find hope in symbols transformed through history towards freedom.

</summary>

"We are lucky in the sense that there are a lot of people in this country who realize what's around the corner."
"But no matter what, the pattern of world history moves towards freedom."
"They're going to become symbols of hope and symbols of motivation for the next generation of people who will speak up."

### AI summary (High error rate! Edit errors on video page)

Describes his diverse audience as people supporting or in pursuit of freedom, regardless of demographics.
Expresses concern over news articles suggesting the president will become more authoritarian after being acquitted.
While waiting in a drive-thru, he plays with his keychain, a rock with blue and pink paint, as a symbol of hope.
Recalls how the rock used to symbolize tyranny and oppression but now represents hope and motivation when broken up and scattered.
Draws parallels to the Berlin Wall coming down, representing the end of authoritarianism and the shift towards freedom.
Mentions the importance of active individuals who speak out to turn the tide against authoritarianism.
Emphasizes the need to speak out now to prevent symbols of authoritarianism from growing bigger.
Acknowledges that symbols created by Trump may represent authoritarianism but hopes they will be seen as reminders of how close society came to that reality.
Believes that history moves towards more freedom for people, and any symbols created will eventually become symbols of hope for future generations who speak up.

Actions:

for activists, freedom advocates,
Speak out against authoritarianism (implied)
Actively work to turn the tide against oppressive systems (implied)
</details>
<details>
<summary>
2020-02-13: Let's talk about the Roger Stone and what we aren't talking about.... (<a href="https://youtube.com/watch?v=z-8ivmEaVek">watch</a> || <a href="/videos/2020/02/13/Lets_talk_about_the_Roger_Stone_and_what_we_aren_t_talking_about">transcript &amp; editable summary</a>)

Beau is conflicted over the oversimplification of the Stone case and argues against excessive sentences for nonviolent crimes, calling for amending federal sentencing guidelines.

</summary>

"Nine years for lying to Congress and telling other people to lie to Congress is unjust."
"It creates two sets of laws. Those laws for people who are politically connected and those for everyone else."
"The sentencing guidelines need to be amended, and that's what this should show."
"I don't think it's right to cheerlead for a nine-year sentence for a non-violent crime."
"This can be used to highlight a lot and hopefully fix a lot."

### AI summary (High error rate! Edit errors on video page)

Beau is conflicted about the media's oversimplification of the Stone case.
Federal sentencing is based on a point system that determines the guideline range in months.
Roger Stone's guidelines suggest seven to nine years due to enhancements, which Beau finds excessive.
Beau believes lengthy incarceration for nonviolent offenders is unjust, regardless of his opinion of the person.
The administration's attempted intervention in Stone's sentencing process is unethical, if not illegal.
Beau questions why the president didn't attempt to change the guidelines if he found the sentence unfair.
He believes the real issue is the need to amend federal sentencing guidelines for nonviolent crimes.
Beau criticizes the existence of disproportionately long sentences for victimless crimes compared to violent crimes.
He advocates for using the Stone case to address broader issues and push for necessary changes in sentencing guidelines.

Actions:

for advocates for criminal justice reform,
Advocate for the amendment of federal sentencing guidelines to address disproportionate sentences (suggested)
Push for changes in the criminal justice system to ensure fairness and equity (implied)
</details>
<details>
<summary>
2020-02-13: Let's talk about a foreign policy report card for Trump.... (<a href="https://youtube.com/watch?v=dLbvg81NSig">watch</a> || <a href="/videos/2020/02/13/Lets_talk_about_a_foreign_policy_report_card_for_Trump">transcript &amp; editable summary</a>)

Beau breaks down Trump's foreign policy track record country by country, showcasing weakened alliances, chaos in hot spots, and the urgent need for skilled diplomacy to repair the damage.

</summary>

"Trump in three years has weakened America's position on the international stage so much that the Philippines is shopping for another ally."
"He managed to lose that war as well."
"We have not had a president this bad at foreign policy."
"We need a secretary of state that is just amazing."
"His landmine diplomacy has taken the United States from its position that it attained after World War II."

### AI summary (High error rate! Edit errors on video page)

International relations don't happen in a vacuum, with one action impacting another, leading to consequences.
Trump's foreign policy endeavors are scrutinized country by country, with the Philippines being in focus due to recent events.
The Philippines is no longer an ally, a move Beau sees as a positive due to Trump's inability to handle China effectively.
Trump's lack of skill in managing spheres of influence is a concern for Beau, who sees harm reduction in the Philippines not being an ally.
Beau questions why the Philippines was okay with losing the US as an ally, indicating a weakening of America's international standing under Trump.
In Europe, traditional allies are ignoring the US due to Trump, but the situation is seen as repairable with a new administration.
Trump's actions in Syria led to chaos and strengthened Iran's regional power.
In Iraq, traditional allies from Western Europe are trying to smooth things over, but those from Eastern Europe are hesitant due to concerns about Russia.
Trump's actions in Afghanistan have worsened an already challenging situation, making negotiations difficult and potentially leading to a Taliban resurgence.
Beau criticizes Trump's foreign policy track record, especially the failed "deal of the century," stressing the need for a skilled Secretary of State in the next administration.

Actions:

for political analysts, policymakers,
Contact local representatives to advocate for a strong diplomatic approach (implied)
Join international relations organizations to stay informed and engaged (implied)
</details>
<details>
<summary>
2020-02-12: Let's talk about the only campaign promise that matters.... (<a href="https://youtube.com/watch?v=Ir3D5WUFfgU">watch</a> || <a href="/videos/2020/02/12/Lets_talk_about_the_only_campaign_promise_that_matters">transcript &amp; editable summary</a>)

The campaign promise we need: restore balance of power to save democracy and make other promises matter.

</summary>

"He's shown that the Constitution is not being upheld."
"That Constitution, it's a piece of paper unless it's adhered to. It doesn't matter. It means nothing."
"We need a president who has the integrity to give that power up."

### AI summary (High error rate! Edit errors on video page)

Addresses the importance of a specific campaign promise from candidates.
Acknowledges President Trump's impact on exposing the disproportionate power of the executive branch.
Emphasizes the danger of unchecked executive power.
Stresses the need to restore balance among the branches of government.
Argues that without this restoration, other campaign promises are essentially meaningless.
Advocates for returning executive powers to the legislative and judicial branches.
Calls for a president who prioritizes restoring normalcy over wielding power.
Points out the risks of concentrating power in the executive branch.
Questions the efficacy of pursuing policy agendas without addressing the imbalance of power.
Urges for a president with the integrity to relinquish excessive power.

Actions:

for voters,
Demand accountability from candidates to restore balance of power (implied).
Advocate for checks and balances within the government (exemplified).
Support candidates committed to relinquishing excessive executive power (suggested).
</details>
<details>
<summary>
2020-02-12: Let's talk about how Andrew Yang can still win.... (<a href="https://youtube.com/watch?v=NvvKGQVBM1k">watch</a> || <a href="/videos/2020/02/12/Lets_talk_about_how_Andrew_Yang_can_still_win">transcript &amp; editable summary</a>)

Beau reminds Andrew Yang's supporters that systemic change doesn't depend on one leader and encourages them to continue pushing for change even after Yang's campaign ends.

</summary>

"You don't need a political candidate to fall behind and follow everything they do and support no matter what."
"If there's anything you can learn from that campaign, it's that radical ideas and new thought can come from anywhere."
"You don't need a leader. You've got the thought. I run with it."
"But for that percentage of his yang gang that looked a little deeper into it and got into the fact that this is something that has to happen, not necessarily UBI but some form of systemic change, you now have the means."
"You can't stop. You can't become politically inactive simply because your candidate didn't get the nomination."

### AI summary (High error rate! Edit errors on video page)

Talking about Andrew Yang's success despite ending his run for the Democratic nomination.
Andrew Yang presented himself as good at math and knew he was a long shot for winning.
Yang saw himself as a messenger candidate, aiming to introduce new ideas like universal basic income.
Hundreds of thousands of people are now familiar with the concept of universal basic income because of Yang.
Yang's campaign succeeded in getting people to acknowledge the flaws in the current system and the need for systemic change.
Beau views Yang's campaign as a success in introducing radical ideas, even if he didn't win the nomination.
Questions arise for Yang's supporters about what to do next after his campaign ends.
Beau encourages Yang's supporters not to give up on the idea of systemic change just because Yang is no longer running.
Yang may have intended to create a movement for systemic change rather than solely aiming for the nomination.
Beau stresses the importance of continuing to push for change even without a specific political leader to rally behind.

Actions:

for yang supporters,
Continue advocating for systemic change (implied)
Embrace radical ideas and push for new thought (implied)
Stay politically active and engaged in driving change (implied)
</details>
<details>
<summary>
2020-02-11: Let's talk about what a drink can teach us about Brexit and Ireland.... (<a href="https://youtube.com/watch?v=7CYhtFeqP_A">watch</a> || <a href="/videos/2020/02/11/Lets_talk_about_what_a_drink_can_teach_us_about_Brexit_and_Ireland">transcript &amp; editable summary</a>)

Beau explains the nuances of conflicts like Brexit, Ireland, and romanticization, urging caution and understanding to prevent violence and support peace.

</summary>

"It's sparked by a request from some of my friends."
"There's a whole lot of nuance to it."
"It's not worth people dying over."
"If you're in the U.S. and you are a part of that Irish American community, unless you're going to go there and fight, shut up."
"Believe me, I understand."

### AI summary (High error rate! Edit errors on video page)

Explains the background of Northern Ireland being part of the United Kingdom and the Republic of Ireland not being part of the UK.
Describes the potential consequences of Brexit on the border between Northern Ireland and the Republic, especially the risk of a hard border with checkpoints.
Talks about the concerns related to the romanticization of conflicts and the potential for violence due to triggering issues like a hard border.
Mentions the request from his friends to rename a popular drink in the US called a "car bomb" to an "Irish Slammer" out of sensitivity towards the Irish community.
Points out the historical significance of bars in supporting different sides of conflicts and the need to avoid unintentionally supporting violence.
Compares the perceptions of conflicts in Ireland between the United States and Ireland, with the US having a more romanticized view.
Emphasizes the importance of understanding nuance in conflicts that have deep historical roots and real impacts on people.
Stresses that rhetoric, imaginary lines, and conflicts like Brexit should not be worth people dying over and militarizing borders.
Urges the Irish American community in the US to be cautious in their actions and words regarding conflicts like those in Ireland.
Suggests that allowing the people directly involved to decide on matters like borders can prevent unnecessary violence and conflicts.

Actions:

for irish american community members,
Rename the drink "car bomb" to "Irish Slammer" to show sensitivity and respect towards the Irish community (suggested).
Avoid unintentionally supporting conflicts by being cautious with actions and words (implied).
</details>
<details>
<summary>
2020-02-11: Let's talk about the parties switching and the Party of Trump.... (<a href="https://youtube.com/watch?v=-APCt4soM_I">watch</a> || <a href="/videos/2020/02/11/Lets_talk_about_the_parties_switching_and_the_Party_of_Trump">transcript &amp; editable summary</a>)

Exploring the Republican Party's transformation from Lincoln to Trump through historical context, campaign strategies, and societal perceptions of racism.

</summary>

"It's real clear, it's history, it's not something you can really debate."
"Maybe it's time for people to stop worrying about their party and start worrying more about being a good person."
"Their policies and the way they try to move those policies forward is racist. There's no big mystery here."

### AI summary (High error rate! Edit errors on video page)

Delving into the transformation of the Republican Party from Lincoln to Trump.
Exploring the historical context of the party switch.
Using a timeline approach to understand the shift.
Noting the change in electoral votes between 1960 and 1964 due to the Civil Rights Movement.
Detailing the primary candidates in 1964 to show the party stances on segregation.
Pointing out Goldwater's role in voting against the Civil Rights Act in 1964.
Addressing accusations of intentionally altering the platform to attract racist voters.
Referencing Strom Thurman's Dixiecrat movement in 1948 as a pivotal moment.
Connecting the shift in the Republican Party to campaign strategies targeting the racist vote.
Encouraging individuals to prioritize being a good person over blind party loyalty.

Actions:

for history enthusiasts, political analysts, voters,
Reassess party loyalty and prioritize personal values (implied)
</details>
<details>
<summary>
2020-02-10: Let's talk about how we learned stop worrying and love colonization.... (<a href="https://youtube.com/watch?v=v850HR3EQd0">watch</a> || <a href="/videos/2020/02/10/Lets_talk_about_how_we_learned_stop_worrying_and_love_colonization">transcript &amp; editable summary</a>)

The United States, once anti-colonial, has transformed into a colonial power, backed by military force, extracting wealth and imposing cultural similarities, leading to acceptance and love for colonialism among the American people.

</summary>

"We have become exactly what the founders opposed."
"The only alien I'm scared of are the ones up there."

### AI summary (High error rate! Edit errors on video page)

The United States, historically anti-colonial, has become a colonial power.
Colonialism involves extracting wealth, imposing cultural similarities, and using military force.
Today, corporations plant logos instead of flags, still backed by military force.
American belief in the right to colonize leads to shock when facing resistance.
Support for troops should mean keeping them out of harm's way, not as fodder for profit.
Despite protests, the American people embrace and love the idea of colonialism.
American empire expansion mirrors past colonial entities' behaviors.
Territorial disputes on Earth could lead to fear and hostility towards "aliens" among us.
Beau suggests meeting extraterrestrial beings to learn, as we have much to discover.
The expanding empire contradicts the principles of the American founders.

Actions:

for global citizens,
Question colonial practices and beliefs (suggested)
Advocate for keeping troops out of harm's way (implied)
Foster understanding and acceptance of diversity (suggested)
</details>
<details>
<summary>
2020-02-09: Let's talk about impeached Presidents, expungement, and impeaching him again.... (<a href="https://youtube.com/watch?v=oZiRHTPZCDU">watch</a> || <a href="/videos/2020/02/09/Lets_talk_about_impeached_Presidents_expungement_and_impeaching_him_again">transcript &amp; editable summary</a>)

Beau explains the permanence of impeachment, potential futility of expunging, and the House's power to impeach for any reason.

</summary>

"He will always be an impeached president forever. That never that's never going to change."
"The House can impeach for anything. The House has sole power over impeachment."
"All of this is very, very simple if you actually read the Constitution."

### AI summary (High error rate! Edit errors on video page)

Explains the ongoing questions about the impeached president and the possibility of expunging his impeachment.
Clarifies that the president will always be considered impeached regardless of acquittal, citing historical examples.
Points out that expunging the impeachment from the House record may backfire and further the perception of a cover-up.
Emphasizes that expunging the impeachment won't erase it from history but merely add a footnote.
Affirms that the House can impeach the president again for any reason, even trivial ones like wearing a tan suit.
Notes that the House has sole power over impeachment and can proceed without alleging a specific crime.
Criticizes senators who may not have been impartial during the impeachment proceedings.
Encourages people to read the Constitution to understand the impeachment process better.

Actions:

for constitutional enthusiasts,
Read and understand the Constitution (exemplified)
</details>
<details>
<summary>
2020-02-09: Let's talk about coins, reds, correlations, and legends.... (<a href="https://youtube.com/watch?v=iQaOpp7m0JU">watch</a> || <a href="/videos/2020/02/09/Lets_talk_about_coins_reds_correlations_and_legends">transcript &amp; editable summary</a>)

Exploring legends on U.S. coins, debunking socialist infiltration myths, and understanding historical political shifts.

</summary>

"They are a link to the past."
"That habit of never scratching and getting below the surface of how something appears is why Americans in general, but specifically the right wing, fall for the same stuff over and over again."
"So if you look at it from a long enough timeline, you may see things that appear to be infiltration. But the reality is, it's just a legend."
"Those terms weren't used at the time, but that's what they were."
"It's just the course of human events."

### AI summary (High error rate! Edit errors on video page)

Exploring legends as windows into past beliefs and fears.
The legend surrounding U.S. coins: Roosevelt dime and Kennedy half dollar.
Red Scare paranoia in the 1960s linked to coded messages on coins.
Debunking the socialist infiltration myth associated with the coins.
Differentiating between social Democrats and socialists in modern U.S. politics.
Correlation between American Republicans and Irish socialists.
Warning against drawing shallow conclusions without deeper investigation.
Robert Lincoln's presence at events of three assassinated presidents.
Americans falling for repeated myths due to romantic nationalism.
Historical trend of the world moving towards the left.
Viewing past historical figures like Washington and Adams as far-left liberals.

Actions:

for history enthusiasts, political analysts.,
Challenge myths and misinformation in political discourse (implied).
Encourage deeper investigation into historical and political narratives (implied).
</details>
<details>
<summary>
2020-02-07: Let's talk about putting yourself in the position and widely held beliefs.... (<a href="https://youtube.com/watch?v=VHsUU62zsZ0">watch</a> || <a href="/videos/2020/02/07/Lets_talk_about_putting_yourself_in_the_position_and_widely_held_beliefs">transcript &amp; editable summary</a>)

Beau addresses the prevalence of sexual assault, challenges victim-blaming rhetoric, and calls out widely held but erroneous beliefs that perpetuate harm and injustice.

</summary>

"It's about power."
"Lawyers and high-profile cases say stuff like this publicly because they know it's a widely held belief."
"Widely held beliefs are often wrong."

### AI summary (High error rate! Edit errors on video page)

Exploring widely held beliefs that are wrong and the consequences they create.
Referencing a lawyer in a high-profile case who claimed to have never been assaulted due to not putting herself in vulnerable positions.
Disputing the notion that staying away from unknown people can prevent assault.
Citing statistics showing that a significant number of assaults are committed by partners, family members, or other known individuals.
Pointing out the fallacy of victim-blaming based on being drunk or with someone unknown.
Revealing that a substantial percentage of assault incidents go unreported for years or not at all.
Criticizing the rhetoric that shifts blame onto victims and protects powerful perpetrators.
Drawing attention to the prevalence of sexual assault and the prioritization of success over addressing victimization.
Contrasting the public's concern over coronavirus with the underreporting of sexual assault due to power dynamics.
Condemning the victim-blaming culture perpetuated by high-profile individuals and the media.
Emphasizing the need to challenge and correct widely held but erroneous beliefs surrounding assault and victimization.

Actions:

for advocates, activists, allies,
Challenge victim-blaming narratives publicly and within your social circles (implied)
Support survivors of assault and believe their experiences (implied)
Advocate for policies and cultural shifts that prioritize addressing victimization over protecting perpetrators (implied)
</details>
<details>
<summary>
2020-02-07: Let's talk about pettiness and something that is clear satire.... (<a href="https://youtube.com/watch?v=9MbaAb-O66E">watch</a> || <a href="/videos/2020/02/07/Lets_talk_about_pettiness_and_something_that_is_clear_satire">transcript &amp; editable summary</a>)

Beau dives into a document possibly from within the administration, shedding light on the President's mindset towards political figures, resembling a high school drama with pettiness and revenge.

</summary>

"American political landscape resembling 'mean girls' with pettiness and revenge."
"It's big if true."
"The President's arch enemies and allies rated in a mysterious notebook."

### AI summary (High error rate! Edit errors on video page)

Describes the American political landscape as resembling "mean girls" with pettiness and revenge.
Mentions a possible document from within the administration, offering insight into the President's mindset.
Points out the President's creative skills in graphic arts and his primary target, Joe Biden.
Notes a list of reasons the President dislikes Biden, including references to Rudy Giuliani and John Bolton.
Mentions the anger towards Adam Schiff and describes the treatment of AOC and Ted Cruz.
Talks about Mitch McConnell's relationship with Moscow and Mitt Romney's integrity.
Suggests potential envy towards those admired turning into rivals.
Mentions the author's behavior towards the Constitution and anger towards Nancy Pelosi and other world leaders.
Raises questions about the authenticity of the document and its significance.

Actions:

for political observers, concerned citizens.,
Read the document to understand potential insights into the President's mindset (suggested).
Pay attention to the behaviors and attitudes of political figures mentioned for a deeper understanding (suggested).
</details>
<details>
<summary>
2020-02-07: Let's talk about patriotism in America and loyalty.... (<a href="https://youtube.com/watch?v=_8t6PJx0Ox0">watch</a> || <a href="/videos/2020/02/07/Lets_talk_about_patriotism_in_America_and_loyalty">transcript &amp; editable summary</a>)

American patriotism is about loyalty to people and principles, not blind obedience to the government; nationalism is the opposite.

</summary>

"Patriotism is not blindly obeying the government; it's about holding onto principles and correcting the system when needed."
"Nationalism, not patriotism, is blindly following government orders and giving up rights."
"The government's appeal to patriotism to gain control is akin to a child wanting cake for dinner - it's a manipulation tactic."

### AI summary (High error rate! Edit errors on video page)

American patriotism has become muddled in the United States, often confused with another term.
American patriots in 1776, such as those at Yorktown, Lexington, and Concord, had no loyalty to the United States, as it didn't exist until 1787.
The loyalty of American patriots was to the people, the countryside, and the principles, not to politicians or governance bodies.
American patriots throughout history understood that governments are a creation of the people and sometimes need correction.
Patriotism is not blindly obeying the government; it's about holding onto principles and correcting the system when needed.
Nationalism, not patriotism, is blindly following government orders and giving up rights.
The government's appeal to patriotism to gain control is akin to a child wanting cake for dinner - it's a manipulation tactic.
Individuals hold the power in a democracy, and the government appeals to patriotism because they need the people.

Actions:

for american citizens,
Correct the system when needed (implied)
Uphold principles over blind obedience (implied)
Understand your power in a democracy (implied)
</details>
<details>
<summary>
2020-02-05: Let's talk about what Trump can learn from toy designers.... (<a href="https://youtube.com/watch?v=W85G2sthZNQ">watch</a> || <a href="/videos/2020/02/05/Lets_talk_about_what_Trump_can_learn_from_toy_designers">transcript &amp; editable summary</a>)

Toy designers teach reusing molds, but President's mine reversal poses dangers; unnecessary, obsolete, and risks harm to civilians and US forces.

</summary>

"This is a horrible idea."
"They're unnecessary. They're obsolete."
"To supply the opposition. To endanger civilians."
"DOD has made a lot of dumb moves over the years."
"The brass at the Pentagon apparently does not understand something that every E2 in the world knows and that even toy designers know."

### AI summary (High error rate! Edit errors on video page)

Talks about G.I. Joe and Cobra, and how toy designers reused vehicle molds for new storylines.
Compares this toy designer tactic with the President's decision to reverse a policy on using mines.
Explains the policy on non-persistent mines that degrade or stop working after a set number of times.
Questions the relevance of using mines in modern warfare scenarios.
Expresses concerns about the dangers and risks associated with using mines that can be repurposed by opposition forces.
Criticizes the decision to lift the ban on mines, labeling it as a horrible idea.
Points out the indiscriminate nature of mines and their potential harm to civilians and US forces.
Mentions the lack of necessity for mines in modern warfare and suggests there are better tools available.
Speculates on the possible reasons behind reversing the mine policy, including personal gain or political reasons.
Condemns the decision and criticizes the Pentagon for not understanding the risks associated with mines.

Actions:

for advocates for peace and disarmament,
Advocate for peace and disarmament by raising awareness in your community (implied).
Support organizations working towards banning the use of landmines (suggested).
Contact lawmakers to express opposition to the use of mines in warfare (suggested).
</details>
<details>
<summary>
2020-02-05: Let's talk about the acquittal and the spirit of America.... (<a href="https://youtube.com/watch?v=kGlTWg_Lfd4">watch</a> || <a href="/videos/2020/02/05/Lets_talk_about_the_acquittal_and_the_spirit_of_America">transcript &amp; editable summary</a>)

The current administration, Senate decisions, and political system are critiqued for deviating from the spirit of the United States, risking lives and losing the country's ideals.

</summary>

"Party over country."
"We are not the land of the free or the home of the brave, because we deny freedom out of cowardice."
"We're losing the plot and we're losing the war because this is a war for the very soul of this country."
"We need real change not just a change of personality."
"It's just going to be an act, completely performative, meaning nothing."

### AI summary (High error rate! Edit errors on video page)

Criticizes the current administration for deviating from the spirit of the United States.
Believes that the President will be acquitted due to partisanship overriding conscience.
Blames senators for enabling the President's actions and violating the country's spirit.
Mentions the Human Rights Watch report on El Salvador, linking Senate decisions to deaths there.
Expresses concern over the focus on Trump rather than the system that enables him.
Warns against the Democratic Party's reluctance for substantial change in candidates.
Stresses the importance of running a candidate focused on real change to prevent Trump's reelection.
Questions the significance of voting blue if policies remain unchanged.
Emphasizes the need for genuine change to uphold the country's ideals and promises.
Condemns sending people back to dangerous situations and the performative nature of American values.

Actions:

for american citizens,
Advocate for candidates focused on real change in elections (implied)
Support policies that prioritize substantial change and system alteration (implied)
</details>
<details>
<summary>
2020-02-05: Let's talk about Mr. Rogers, goldfish, strawberries, and dreams.... (<a href="https://youtube.com/watch?v=B4Kzl_Q57NE">watch</a> || <a href="/videos/2020/02/05/Lets_talk_about_Mr_Rogers_goldfish_strawberries_and_dreams">transcript &amp; editable summary</a>)

Beau shares Mr. Rogers' story, showcases self-reliance through growing food, and encourages overcoming obstacles to pursue dreams.

</summary>

"Let nothing stand in your way."
"There's always a way."
"Don't give up on your dreams."

### AI summary (High error rate! Edit errors on video page)

Sharing a heartwarming story about Mr. Rogers and his dedication to a blind girl worried about the fish being fed on his show.
Demonstrating self-reliance by growing his own food with a Bag-O-Blooms Strawberry Kit.
Encouraging viewers to overcome obstacles and pursue their dreams, no matter the challenges they face.
Providing tips on growing plants even in small spaces like balconies without the need for weeding.
Emphasizing the simplicity of growing plants in bags with sunlight and moisture requirements.
Mentioning alternative methods like using Ziploc bags or shoe organizers to grow plants effectively.
Inspiring confidence and self-reliance in oneself to tackle obstacles and achieve goals.
Advocating for supporting and encouraging others to pursue their dreams and not give up.

Actions:

for gardeners and dreamers,
Start growing your own food in small spaces like balconies using bags or containers (exemplified)
Encourage and support others in pursuing their dreams (exemplified)
</details>
<details>
<summary>
2020-02-04: Let's talk about why Iowa doesn't matter and representative democracies.... (<a href="https://youtube.com/watch?v=3d1PWr1_Ygw">watch</a> || <a href="/videos/2020/02/04/Lets_talk_about_why_Iowa_doesn_t_matter_and_representative_democracies">transcript &amp; editable summary</a>)

Beau questions the essence of behind-the-scenes political events, stresses the importance of appearances in democracy, and advocates for strengthening community ties to build a better future.

</summary>

"Just the appearance matters."
"You and your neighbors. That's what you need to work on strengthening right now."
"If the American experiment is indeed dead, we'll be alright."

### AI summary (High error rate! Edit errors on video page)

Questions the significance of what happened in the Iowa caucus behind the scenes, stressing its irrelevance in the bigger picture.
Emphasizes that the appearance of fair elections is vital for a representative democracy to function.
Points out that faith in three things is necessary for a representative democracy to work effectively.
Expresses skepticism about the average voter's ability to avoid being manipulated into voting against their interests.
Raises doubts about elected officials actually representing the best interests of the people who elected them.
Mentions the importance of believing that your vote matters for the democratic system to operate smoothly.
Criticizes attempts from both the right and the Democratic Party to undermine democracy, intentionally or inadvertently.
Asserts that the American experiment of representative democracy is failing due to a lack of belief in voter education and political integrity.
Advocates for focusing on strengthening relationships with neighbors as a way to improve the country.
Concludes by suggesting that even if the current system fails, there is hope to build something better for all.

Actions:

for citizens, community members,
Strengthen relationships with neighbors (implied)
Advocate for transparency and accountability in elections (implied)
Educate yourself and others on democratic processes (implied)
</details>
<details>
<summary>
2020-02-04: Let's talk about the mainstream.... (<a href="https://youtube.com/watch?v=A6WijixQAS0">watch</a> || <a href="/videos/2020/02/04/Lets_talk_about_the_mainstream">transcript &amp; editable summary</a>)

Beau explains the importance of mainstreaming radical ideas for a better world and the need to communicate them effectively to the general public.

</summary>

"If you hold a radical idea, it's because at least at some point really wanted everybody to believe that so you could have a better world."
"Sometimes you've got to speak the language of the people that you're talking to."

### AI summary (High error rate! Edit errors on video page)

Defines the mainstream as widely held beliefs and agreed-upon facts by the majority.
Questions the purpose of possessing radical ideas outside the mainstream.
Encourages mainstreaming radical ideas for broader acceptance.
Suggests that shifting the mainstream narrative can lead to a better world.
Points out the challenge of communicating radical ideas effectively to the general public.
Advocates for making radical ideas accessible and palatable for wider acceptance.
Emphasizes the importance of speaking a common language to convey ideas effectively.

Actions:

for activists, ideological warriors,
Make efforts to communicate radical ideas in accessible and understandable terms (implied).
Work towards mainstreaming radical ideas by making them more palatable to a broader audience (implied).
</details>
<details>
<summary>
2020-02-04: Let's talk about becoming the out-group.... (<a href="https://youtube.com/watch?v=iNHy_BYwTPs">watch</a> || <a href="/videos/2020/02/04/Lets_talk_about_becoming_the_out-group">transcript &amp; editable summary</a>)

Beau addresses the fear of becoming a minority in America, calling out the inherent racism and advocating for equality and acceptance of demographic shifts towards a diverse future.

</summary>

"It is inherent racism."
"Try to treat everybody equally. Try to treat people fairly."
"There's nothing to fear."
"We will eventually all look like Brazilians and what a great day that will be."
"I'm totally cool with pressing one for English."

### AI summary (High error rate! Edit errors on video page)

Talks about the demographic shift in the United States and addresses the fear associated with becoming a minority in one's own country.
Criticizes the inherently racist sentiment behind not wanting to be a minority in America.
Points out that the concern about becoming a minority showcases systemic racism in the country.
Advocates for treating everyone equally and fairly rather than resisting demographic changes.
Addresses the argument of wanting everybody to speak English and questions its significance.
Mentions the gradual demographic shift over decades and the evolution of languages spoken in the country.
Emphasizes that resistance to change is often rooted in the history of using differences to marginalize others.
Encourages letting go of fears and biases to build a better society and world.
Expresses openness to diversity and multilingualism, envisioning a future where differences fade away.

Actions:

for americans,
Embrace diversity and treat everyone equally (implied)
Be open to different languages and cultures (implied)
Advocate for inclusivity and fairness in your community (implied)
</details>
<details>
<summary>
2020-02-03: Let's talk about the economics of being prepared and masks.... (<a href="https://youtube.com/watch?v=cyPjeE0keoQ">watch</a> || <a href="/videos/2020/02/03/Lets_talk_about_the_economics_of_being_prepared_and_masks">transcript &amp; editable summary</a>)

Beau advocates preparedness and calmness, differentiates mask types, warns against price exploitation, and urges readiness in a teachable moment.

</summary>

"Don't panic."
"It's a teachable moment to show that you should get prepared for when there is an emergency."
"Under normal circumstances an N95 mask at any hardware store will cost you a dollar maybe two."
"You're taking yourself out of the equation because you have what you need."
"I don't think any of this is necessary."

### AI summary (High error rate! Edit errors on video page)

Advocates for preparedness and calmness in the face of potential emergencies.
Demonstrates the difference between N95 masks and surgical masks for protection.
Points out the exploitation of fear leading to price gouging of masks.
Emphasizes the importance of being prepared before emergencies occur.
Mentions the affordability of preparedness supplies under normal circumstances.
Recommends preparing for various emergencies, not just the current situation.
Suggests getting masks and other essentials for pandemic preparedness.
Urges not to strain emergency services by being unprepared.
Recommends taking the current situation as a teachable moment for readiness.
Mentions that alternative filters for masks are still widely available.
Advises against mistaking surgical masks for the protection level of N95 masks.
Expresses confidence in authorities handling the current situation effectively.

Actions:

for prepared individuals,
Stock up on emergency supplies like masks, Purell, and essentials (suggested).
Get N95 filters for masks used for painting and similar tasks (suggested).
</details>
<details>
<summary>
2020-02-03: Let's talk about the Superbowl.... (<a href="https://youtube.com/watch?v=kcTJM-JTeEc">watch</a> || <a href="/videos/2020/02/03/Lets_talk_about_the_Superbowl">transcript &amp; editable summary</a>)

Beau Jan breaks down the Super Bowl halftime show, urging for unity over differences and action on pressing societal issues like "kids in cages."

</summary>

"It's common, we all do it."
"We all have stuff like that."
"There are a lot of working class Americans, Puerto Rico is part of the United States."
"I got a real Hunger Games vibe from it."
"We've just kind of accepted that that's what it is now."

### AI summary (High error rate! Edit errors on video page)

Analyzing the Super Bowl halftime show and its significance as a highly-watched television event.
Mention of Shakira's performance as a teachable moment.
Critique on how cultural differences are framed during such events.
Comparison of primal guttural utterances across different cultural groups.
Acknowledgment of commonalities in celebratory expressions among diverse groups.
Addressing misconceptions around Shakira's heritage and the use of the song "Born in the USA."
The appropriateness of displaying the Puerto Rican flag during the performance.
Mention of the depiction of "kids in cages" during the show.
Concerns over the lack of immediate action following the portrayal of pressing issues.
Call for reflection on societal acceptance of critical issues presented through entertainment.

Actions:

for viewers, super bowl fans,
Educate others on the cultural significance of different expressions (implied)
Advocate for the recognition and support of marginalized communities like Puerto Rican veterans (implied)
Raise awareness about and take action against the issue of children in cages (implied)
</details>
<details>
<summary>
2020-02-02: We talked about Easter eggs.... (<a href="https://youtube.com/watch?v=WGQbEDLKxzI">watch</a> || <a href="/videos/2020/02/02/We_talked_about_Easter_eggs">transcript &amp; editable summary</a>)

Beau questions morality, criticizes political actions, and draws parallels with historical events while reporting from various locations.

</summary>

"It's the same thing."
"We are in occupied territory."
"The dog isn't barking."
"He should have barked."
"Our senatorial dog recognizes the guilty person."

### AI summary (High error rate! Edit errors on video page)

Tells the story of St. Augustine and Alexander the Great to illustrate moral ambiguity between a pirate and an emperor.
Criticizes the equating of legality with morality, pointing out that using force to plunder is the same regardless of legality.
Questions the lack of progress on Trump's promised border wall and the methods used to acquire land for it.
Addresses pastors planning to instigate conflict at the Pulse Nightclub on its anniversary and criticizes their behavior.
Comments on the president's ultimatum and threats, comparing it to actions in Nazi Germany.
Mentions China quarantining a city and stresses the importance of waiting for demographic information on patients.
Reports from the Mexican border about Trump declaring a national emergency over asylum seekers.
Shares a story about refugees building a tunnel with the help of NBC News.
References Kennedy's emotional reaction to refugees escaping to West Berlin through a tunnel.
Analyzes the political situation and the lack of expected outcomes in the 2020 election.
Draws a parallel with Sherlock Holmes' story about a silent dog to suggest hidden knowledge in current events.

Actions:

for activists, political observers,
Contact local representatives to express concerns about immigration policies (implied).
Support refugee assistance organizations or initiatives (exemplified).
</details>
<details>
<summary>
2020-02-02: Let's talk about Bernie, his women, and the military.... (<a href="https://youtube.com/watch?v=W6dBEXuuNIQ">watch</a> || <a href="/videos/2020/02/02/Lets_talk_about_Bernie_his_women_and_the_military">transcript &amp; editable summary</a>)

Bernie garners support from unexpected quarters like active duty military due to Trump's actions, systemic issues, and appeal of his platform; criticism of Bernie's surrogates should not be gender-based.

</summary>

"Bernie leads all candidates to include Trump in donations from active duty military."
"Leaving them twisting in the wind, leaving a man behind, so to speak, did not sit well with them."
"A lot of people join the military today because of systemic issues, income inequality."
"It's a really bad mindset these women did not get where they are by you know worrying about the fact that some man can think better than their little baby lady brains."
"If you have a problem with what they say, I suggest you take it up."

### AI summary (High error rate! Edit errors on video page)

Bernie is receiving support from unlikely places, including active duty military.
Bernie leads all candidates, including Trump, in donations from active duty military by 50%.
This shift surprises many as the military has traditionally supported the Republican party.
The abandonment of the Kurds by Trump had a significant impact on military support for him and the Republicans.
Many active duty military members who were Trump supporters turned away after the abandonment of the Kurds.
Military personnel work closely with the Kurds, and leaving them behind did not sit well with many.
Another reason for military support of Bernie is related to systemic issues like income inequality.
People join the military for better opportunities and benefits, such as universal healthcare and housing.
Seeing the benefits in Bernie's platform makes military personnel question why these are not available to all Americans.
Bernie's appeal to active duty military lies in his platform advocating for universal healthcare and other social benefits.

Actions:

for democratic voters, bernie supporters, military personnel,
Address systemic issues like income inequality in your community (implied)
Advocate for universal healthcare and social benefits for all (implied)
</details>
<details>
<summary>
2020-02-01: Let's talk about why the House was partisan and the Senate can't be.... (<a href="https://youtube.com/watch?v=4fnOOAuGjiI">watch</a> || <a href="/videos/2020/02/01/Lets_talk_about_why_the_House_was_partisan_and_the_Senate_can_t_be">transcript &amp; editable summary</a>)

Beau explains the constitutional obligation for Senate impartiality, condemning its failure during impeachment, and clarifies the distinct roles of the Senate and House of Representatives.

</summary>

"The Senate had a constitutional obligation to be impartial. They betrayed that obligation."
"The relevant passage says, shall be removed, not can be, not could be if you want to, not could be if it won't hurt your reelection chances, shall be."
"The Senate failed to uphold the Constitution. Period. Full stop."
"The Houses, the House of Representatives, and the Senate, they're not the same. They're designed to be different."
"Anytime you say it, all you're doing is telling everybody around you that you've never read the Constitution."

### AI summary (High error rate! Edit errors on video page)

Explains the difference between the House of Representatives and the Senate in the US government.
Points out the constitutional obligation of the Senate to be impartial and fair.
Emphasizes that the Senate failed to uphold the Constitution during impeachment.
Calls out those who claim the Senate didn't remove the guilty party, stating the Constitution mandates removal.
Notes a growing trend of people not familiar with the Constitution, despite having "We the People" in their profile pictures.
Stresses that the Senate and the House of Representatives are designed to be different, with unique roles and responsibilities.
Argues that those justifying the Senate's actions are essentially admitting they haven't read the Constitution.

Actions:

for political enthusiasts,
Read and familiarize yourself with the Constitution (suggested)
Advocate for accountability in government actions (implied)
</details>
