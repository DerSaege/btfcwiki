---
title: Let's talk about Utah and a Valentine's Day dance gone wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0VdApBcpgGs) |
| Published | 2020/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Incident at a Valentine's Day dance in Rich County, Utah, involving an 11-year-old girl named Aislinn.
- Two versions of events: Aislinn refused a dance, principal insisted she dance with the boy; school denies forcing anyone but encourages saying yes.
- Aislinn felt uncomfortable with the boy and outlined reasons to the press.
- Beau believes the boy is not to blame; it was on Aislinn to communicate her discomfort to the school.
- School's actions send wrong messages about entitlement and consent, leaving boys with misguided ideas.
- Beau criticizes the school's policy and the need for a policy review, insisting that the issue should just go away.
- Emphasizes the importance of consent and how it should be a fundamental concept taught in schools.

### Quotes

- "Consent is a very basic premise of society."
- "She said no. That's the end of the conversation."
- "There is no reason for a principal to try to persuade or suggest or ask that she say yes."
- "This is a really simple concept."
- "If you're not teaching that, what can you possibly be teaching at this school?"

### Oneliner

An 11-year-old girl's discomfort at a school dance sparks debate on consent and entitlement, prompting reflections on what schools should be teaching.

### Audience

School administrators, educators, parents.

### On-the-ground actions from transcript

- Contact the school to express concerns about their policy and handling of the situation (suggested).
- Advocate for better education on consent and respect in schools (implied).
- Initiate community dialogues on consent and healthy boundaries in education settings (implied).

### Whats missing in summary

The emotional impact on Aislinn and the broader implications on teaching consent effectively in schools.


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a story that sounds like it's out of the 1950s.
But it's not.
There was no Rock Around the Clock playing, Buddy Holly was not on stage.
Happened in Rich County, Utah.
And it's at a Valentine's Day dance.
Now there's two versions of events, as there often are in things like this.
The student, an 11-year-old girl named Aislinn, says a young boy asked her to dance and she
said no.
And then the principal told her that we don't say no here and made her dance with him.
That's her version.
The school said that nobody was forced to do anything, yet they do ask children to say
Yes.
And that all a student has to do is talk to them.
OK, so those are the two versions of events.
She said that the young boy made her feel uncomfortable.
Four reasons that she has outlined or her mother has
outlined to the press.
I'm not going to repeat them here because there's no way of
knowing whether or not that's true.
And, to be honest, the boy's not really to blame here.
All she had to do was talk to the school.
She said no.
That's the end of the conversation.
The school does not have the authority to give a boy the right to put his hands on a girl.
There is no conversation to be had.
There is no reason for a principal to try to persuade or suggest or ask that she say
yes.
That is not part of the curriculum.
Things like this leave boys with the idea that they are entitled to something.
And that's the reason for the policy that they don't want anybody left out.
Their feelings in regards to this don't grant them access to somebody else's body.
This is why they feel entitled.
Now at the end of the day, the school said they will have a policy review.
There's no need for that.
This just needs to go away.
There's no conversation to be had.
She said no.
Now the upsetting part about this is that this is the last dance of the year for them,
Which means they're probably not going to do anything unless there's some kind of pressure
put on them because they have no reason to all be forgotten over the summer until it
happens again and again and understand the school is liable for this.
This is the policy.
The policy is to grant young boys access to young girls' bodies.
That's the policy, well, to ask that it's okay.
Even after, according to her, she said no.
She doesn't have to give a reason.
She doesn't have to explain herself.
She doesn't have to talk to the administration.
She said no.
This is a really simple concept.
Consent is a very basic premise of society.
If you're not teaching that, what can you possibly be teaching at this school?
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}