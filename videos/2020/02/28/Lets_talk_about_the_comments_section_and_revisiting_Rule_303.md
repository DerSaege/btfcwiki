---
title: Let's talk about the comments section and revisiting Rule 303....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ULJoGp69Ihc) |
| Published | 2020/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unwritten rule of internet journalism: never read the comment section under your work to avoid negativity and criticism.
- Shared a heartwarming experience of a viewer transcribing the entire video for a hearing-impaired person.
- Explained Rule 303, stating that if you have the means, you have the responsibility to act.
- Encourages helping others, especially during tough times in the US due to lack of solid leadership.
- Emphasizes the importance of individuals and communities stepping up to help those in need.
- Acknowledges the potential burnout from helping others and advises to do what you can sustainably.
- Calls for collective action and support during challenging times when leadership is lacking.

### Quotes

- "If you have the means at hand, you have the responsibility to act."
- "It's up to us as individuals. It's up to us as communities."
- "You do what you can when you can for as long as you can."
- "There's probably somebody out there that could use your help."
- "If you see somebody that needs help, you probably should. If you can."

### Oneliner

Beau shares the Rule 303: If you have the means, you have the responsibility to act, encouraging individuals and communities to help those in need, especially during challenging times in the absence of solid leadership.

### Audience

Online citizens

### On-the-ground actions from transcript

- Help those in need in your community by offering support and assistance (exemplified).
- Step up to assist individuals facing challenges during tough times (exemplified).

### Whats missing in summary

The full transcript conveys the importance of stepping up to help others and take responsibility, particularly during times of need and lack of leadership.

### Tags

#InternetJournalism #Rule303 #Responsibility #CommunitySupport #Leadership


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we're going to talk about an unwritten rule of
internet journalism, the comments section, and we're
going to revisit rule 303.
We're going to talk about that again.
So the unwritten rule is that you never, under any
circumstance, read the comment section under your work. If you are publishing on
the internet, you never read your own comment section. If you do, what you're
going to find is every grammar expert in the world congregating to critique your
work. You will find everybody with a differing opinion explaining what a
horrible person you are, you will find just a whole bunch of stuff that is bad
for morale and bad for productivity. Most people don't read their own comment
sections, especially if they're writing. I read the comment section of these
videos. I try to read them all actually, but last night I ran across a comment
that was from somebody who was hearing impaired and they were just expressing
their frustration that the closed captioning was messing up and I
commented explaining yes there's a glitch you know we are aware of it there's
nothing we can really do about it it's it's on the other end but I kept an eye
on the video waiting for the closed captioning to show up and when it did I
I went back to the comment and what I found was that one of you had transcribed the entire
video and put it below.
That's pretty cool.
That is pretty cool.
That is rule 303.
Now that term gets thrown around a lot on the channel but I realized that video is old.
A lot of people who have just arrived may not know that, know what it is.
I'll link the original video that explains where the term came from and how it kind of
evolved below, but the general gist of it is if you have the means at hand, you have
the responsibility to act.
Now normally, given where the term originated, this is used to describe something that is
typically necessary, but not necessarily good. It's not necessarily a good thing.
I love the fact that now it's being used to describe things that are good and
that are good by themselves. Not good in relation to other stuff. We're probably
headed into a couple of rough months in the US and it might be a good idea for
everybody to remember that if you have the means at hand you have the
responsibility to act. If you see somebody that needs help you probably
should. If you can. Somebody mentioned recently that you know that assigns a
lot of responsibility to people. It does, but I mean it needs to be tempered with
you do what you can when you can for as long as you can. You know, you can't burn
yourself out helping others, but we don't have solid leadership in this country
and we have a couple of events that are converging that we probably could use
solid leadership. We just don't have it. That means it's up to us. It's up to us as
individuals. It's up to us as communities. So as things progress and if they get
rough, just remember there's probably somebody out there that could use your
help and if you have the means it'd be a good time to step up to the plate anyway
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}