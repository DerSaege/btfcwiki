---
title: Let's talk about the Roger Stone and what we aren't talking about....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z-8ivmEaVek) |
| Published | 2020/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is conflicted about the media's oversimplification of the Stone case.
- Federal sentencing is based on a point system that determines the guideline range in months.
- Roger Stone's guidelines suggest seven to nine years due to enhancements, which Beau finds excessive.
- Beau believes lengthy incarceration for nonviolent offenders is unjust, regardless of his opinion of the person.
- The administration's attempted intervention in Stone's sentencing process is unethical, if not illegal.
- Beau questions why the president didn't attempt to change the guidelines if he found the sentence unfair.
- He believes the real issue is the need to amend federal sentencing guidelines for nonviolent crimes.
- Beau criticizes the existence of disproportionately long sentences for victimless crimes compared to violent crimes.
- He advocates for using the Stone case to address broader issues and push for necessary changes in sentencing guidelines.

### Quotes

- "Nine years for lying to Congress and telling other people to lie to Congress is unjust."
- "It creates two sets of laws. Those laws for people who are politically connected and those for everyone else."
- "The sentencing guidelines need to be amended, and that's what this should show."
- "I don't think it's right to cheerlead for a nine-year sentence for a non-violent crime."
- "This can be used to highlight a lot and hopefully fix a lot."

### Oneliner

Beau is conflicted over the oversimplification of the Stone case and argues against excessive sentences for nonviolent crimes, calling for amending federal sentencing guidelines.

### Audience

Advocates for Criminal Justice Reform

### On-the-ground actions from transcript

- Advocate for the amendment of federal sentencing guidelines to address disproportionate sentences (suggested)
- Push for changes in the criminal justice system to ensure fairness and equity (implied)

### Whats missing in summary

Deep dive into the implications of unfair sentencing and the need for systemic reform.

### Tags

#FederalSentencing #RogerStone #CriminalJusticeReform #UnjustSentences #Advocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about something
I am of two minds of,
something I'm kinda conflicted on in a way, but not really.
It's why I haven't done a video on it,
because there are multiple parts to this,
yet it's been boiled down in the media to one thing.
Make sure you watch this video till the end,
because we're going to talk about the Stone case,
Roger Stone.
Okay, so let me tell you how federal sentencing is supposed to work.
There's a chart.
There's points.
That's really how it works.
There's a point system.
And those sentencing guidelines put out by the United States Sentencing Commission, you
add up the number of points you go over to the person's criminal history, and that gives
you a guideline range in months.
That's what prosecutors recommend.
That's what judges sentence to, almost always.
outside of that is very, very rare.
That is how it is supposed to work under the current system.
Roger Stone's guidelines because of enhancements put him at roughly seven to nine years.
I do not believe that nonviolent offenders should be subjected to lengthy periods of
incarceration.
That doesn't change just because I think the guy's a goon.
His political stance has nothing to do with that.
I believe that nine years for lying to Congress and telling other people to lie to Congress
is unjust.
It's unfair.
In a perfect world, I don't even think without the enhancements, the guidelines for him without
the enhancements, he'd be looking at like three years.
In a perfect world, I think that's too much.
But we're not living in a perfect world, and at least that would seem kind of just.
Not really to me, but at least it makes sense.
Seven to nine years seems excessive.
Now, the other side to this is that the administration attempted to meddle in it.
That's wrong.
Had some people tell me it's illegal, I don't know that it is.
It's definitely unethical.
But here's my real issue, and this is what I think we should be talking about more so
than Stone himself.
In the president's tweet, he said that this was unfair.
He's right.
Nine years for that is, is unfair.
Think about everything that's happened to you and changed in your life since 2011.
That's how long he'd be gone.
That seems a bit excessive.
And the president realizes it's unfair, but rather than using the power of his administration to attempt to change the
guidelines, he just wants to intervene for his buddy.
buddy. That to me is more criminal than what Stone did. In my eyes, that is more of a wrongdoing.
Because it's an admission that this is happening to other people, but we're not going to do
anything to fix it. And it creates two sets of laws. Those laws for people who are politically
connected and those for everyone else.
If you look at the federal sentencing guidelines, you will see decades long sentences for non-violent
victimless crimes.
It's unjust.
It is unjust.
You know there is a problem when somebody who is caught with a certain substance, pick
who harmed no one, gets a longer sentence than someone who committed the worst kind of violent crime.
The sentencing guidelines need to be amended, and that's what this should show.
That's what we should use this to highlight.
Yeah, you can talk about the corruption in the administration, that's fair game,
but I don't think it's right to cheerlead for a nine-year sentence for a non-violent crime.
simply because his politics are garbage.
Anyway, there's multiple issues at play here and I think we need to address them
all instead of trying to just boil it down to something we can get behind
because this can be used to highlight a lot and hopefully fix a lot.
But anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}