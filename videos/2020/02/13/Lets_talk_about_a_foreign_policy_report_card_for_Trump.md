---
title: Let's talk about a foreign policy report card for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dLbvg81NSig) |
| Published | 2020/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- International relations don't happen in a vacuum, with one action impacting another, leading to consequences.
- Trump's foreign policy endeavors are scrutinized country by country, with the Philippines being in focus due to recent events.
- The Philippines is no longer an ally, a move Beau sees as a positive due to Trump's inability to handle China effectively.
- Trump's lack of skill in managing spheres of influence is a concern for Beau, who sees harm reduction in the Philippines not being an ally.
- Beau questions why the Philippines was okay with losing the US as an ally, indicating a weakening of America's international standing under Trump.
- In Europe, traditional allies are ignoring the US due to Trump, but the situation is seen as repairable with a new administration.
- Trump's actions in Syria led to chaos and strengthened Iran's regional power.
- In Iraq, traditional allies from Western Europe are trying to smooth things over, but those from Eastern Europe are hesitant due to concerns about Russia.
- Trump's actions in Afghanistan have worsened an already challenging situation, making negotiations difficult and potentially leading to a Taliban resurgence.
- Beau criticizes Trump's foreign policy track record, especially the failed "deal of the century," stressing the need for a skilled Secretary of State in the next administration.

### Quotes

- "Trump in three years has weakened America's position on the international stage so much that the Philippines is shopping for another ally."
- "He managed to lose that war as well."
- "We have not had a president this bad at foreign policy."
- "We need a secretary of state that is just amazing."
- "His landmine diplomacy has taken the United States from its position that it attained after World War II."

### Oneliner

Beau breaks down Trump's foreign policy track record country by country, showcasing weakened alliances, chaos in hot spots, and the urgent need for skilled diplomacy to repair the damage.

### Audience

Political analysts, policymakers

### On-the-ground actions from transcript

- Contact local representatives to advocate for a strong diplomatic approach (implied)
- Join international relations organizations to stay informed and engaged (implied)

### Whats missing in summary

Insight into the potential long-term impacts of Trump's foreign policy decisions and the importance of diplomatic finesse in repairing international relationships.

### Tags

#InternationalRelations #TrumpAdministration #ForeignPolicy #Diplomacy #SecretaryOfState


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about international relations.
I often say that these things don't occur in a vacuum when
I'm talking about international relations.
One thing impacts something else that impacts something else.
And there's a lot of consequences
to even small decisions.
I had somebody ask me for examples of that,
because I use that phrase a lot, but I've never really sat down
and said, this happens, then this happens,
connected at all. Fair enough. So tonight we're going to go through Trump's foreign policy endeavors.
We're going to give a little report card, just go country by country to everywhere that he
has involved himself, and see how each location is doing after Mr. Art of the Deal got involved.
Okay, we're going to start with the Philippines because that is the one that everybody's talking
about right now because it just happened. The Philippines is no longer an ally.
By traditional doctrine, yeah, that's important that they should be. We should
have done what we could to to keep them.
I personally disagree. I don't think it's that important.
I actually think it's kind of a happy accident. I'm kind of glad it happened.
As we're about to see as we go through the other countries, Trump is not equal to the
task when it comes to dealing with China in a foreign policy setting.
When we're talking about spheres of influence, he's just not up to that, period.
He can't do it.
The way I'm looking at it is harm reduction.
The Philippines not being an ally is just one less way he can accidentally start a war.
That's really my take on it.
The media keeps asking the question, what are we losing?
How is the US losing out with the Philippines withdrawing?
That's the wrong question.
If you want to understand the importance of it, that's the wrong question.
The question isn't, what are we losing?
The question is, why was the Philippines okay with losing us?
We're the United States.
Everybody wants to be our ally because we'll go to war over anything.
But they don't want to be our ally.
What does that tell you?
Trump in three years has weakened America's position on the international stage so much
that the Philippines is shopping for another ally.
How did he weaken it?
We're about to go through it.
Now I will say this before we go, before we get off the Philippines.
The reason that they withdrew their stated reason had to do with the administration denying
a visa to somebody, that visa should have been denied.
I think that was the right move.
Normally, when you do something like that, there is a diplomacy detachment sent over
and they smooth things over.
Apparently that didn't happen, but their stated cause, which isn't their real cause,
That wasn't a mistake on Trump's part, on the administration's part.
That visa should have been denied.
Okay, so what about Europe?
Our main allies, our traditional allies over there, the ones that made up the core of NATO
before the end of the Cold War, they're still with us.
They're ignoring Trump.
They're ignoring the United States right now because he's Trump.
When we get a new administration, they'll come back.
That's an easily repairable situation, so I don't think that we should focus too much
on that.
Yeah, he's messed stuff up there, but it's not a huge deal.
It's easily fixable.
When we get to the newer members of NATO, that's not entirely true, but we will show
some examples of that here in a minute.
Okay, so let's get to the hot spots.
Let's get to the spots where it really matters.
he really showed his skill level. Syria. This was the location where he proved he
knew more than all of the generals and advisors and against everybody's advice
and counsel he pulled our troops back and agreed to allow Turkey to enter
Syria. Sold out the Kurds in the process losing us one of the most powerful
non-state actors in the region.
Okay so what's happened since then?
Nothing good and it is now on the verge of turning into a regional war.
Turkey and Syria are actively shooting at each other.
The US and Syrian backed forces and possibly Syrian troops directly by some reports are
shooting at each other.
Russia is watching and laughing and the whole thing is a powder keg that could turn into
a regional war at literally any moment. That was an unmitigated failure. We got
nothing and lost everything. One country over, Iraq. When he pulled off his little
hit, they got upset. Go figure. They've asked us to leave. Things are kind of
calming down but not really. They will never be what we spent a decade trying
trying to get them to be, because now the people that are poised to take power, they
are buddied up to Iran.
Trump lost Iraq.
Understanding that he's on thin ice, he asks NATO to step in and help out.
Our traditional allies, those in Western Europe, they're like, okay, we'll conduct some extra
training exercises with the Iraqis.
to smooth things over. Those European allies that are from Eastern Europe,
they're not doing anything because they don't want to relocate assets to Iraq
when they have Russia up against their border. What if they get attacked? Well
they shouldn't have to worry about that because they have the US as an ally but
for all they know we will leave them twisting in the wind while we try to get
political dirt on a domestic rival. These things don't exist in a vacuum. They don't
want to help now. And I can't say a blame to be honest.
Okay, Iran, one country over. The hit, it was in the beginning with the information
we were given in the beginning, it was a draw at best, didn't do anything, but whatever.
Now that all of the information has come out, we understand that the US got one of theirs,
they wounded a hundred of ours, they are looking at it as a win, and they stood the US down.
little exercise served to strengthen Iran's hand. It makes them more of a regional superpower.
One country over. Afghanistan. Now this one is not entirely Trump's fault. He inherited
a horrible situation and somehow found a way to make it worse. The US has been trying to
leave Afghanistan. They've been trying to get out and the problem is the national
government's pretty weak and if we just abandon them well it it looks bad. It's
really what it boils down to. Trump handicapped his negotiators. He did. He
walked out and said we want to leave. Well great now they have no leverage. We
We talked about it when it happened and we were like, they're never going to be able
to get a decent deal now because the Taliban has no reason to negotiate.
They just have to wait us out.
The way that deal currently stands, the United States is basically just begging them to reduce
violence for a little bit.
Can't even get a temporary ceasefire.
That's how much he undermined his own negotiators through his public statements and his tweets.
We will just end up just selling out the national government and the Taliban will take over
again.
He managed to lose that war as well.
In his defense, that one probably wasn't entirely his fault.
It probably would have gone down that way no matter what.
He just ensured that it did.
Okay so that's it.
That's his foreign policy endeavors.
Unless you want to go ahead and count the deal of the century, the amazing peace deal
that was so great and so peaceful, it triggered riots and everybody rejected it out of hand.
That's his foreign policy.
We have not had a president this bad at foreign policy.
you're waiting for like since whatever there's not just ever we have not had
somebody that's bad at this the next administration is going to need a
secretary of state that is just amazing I understand most of the candidates that
are coming up now they're kitchen table candidates and that's fine but they have
to have a secretary of state that has a true diplomat because some of this is
going to be very complex to fix and it's going to take a lot of finesse and it's
going to have to be done or the effects of Trump will just snowball and at the
end of the day his landmine diplomacy has taken the United States from its
position that it attained after World War II, as just a world superpower, and
turned it into a nation that other countries don't even care if they lose
the Alliance, that's what he's done in three years, we need a secretary of
state that is just amazing otherwise the impacts from Trump will last decades
because it's gonna have to be fixed immediately after he leaves anyway it's
just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}