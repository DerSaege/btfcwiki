---
title: Let's talk about you, hope, and my key chain....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PPcEBpnQlDg) |
| Published | 2020/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes his diverse audience as people supporting or in pursuit of freedom, regardless of demographics.
- Expresses concern over news articles suggesting the president will become more authoritarian after being acquitted.
- While waiting in a drive-thru, he plays with his keychain, a rock with blue and pink paint, as a symbol of hope.
- Recalls how the rock used to symbolize tyranny and oppression but now represents hope and motivation when broken up and scattered.
- Draws parallels to the Berlin Wall coming down, representing the end of authoritarianism and the shift towards freedom.
- Mentions the importance of active individuals who speak out to turn the tide against authoritarianism.
- Emphasizes the need to speak out now to prevent symbols of authoritarianism from growing bigger.
- Acknowledges that symbols created by Trump may represent authoritarianism but hopes they will be seen as reminders of how close society came to that reality.
- Believes that history moves towards more freedom for people, and any symbols created will eventually become symbols of hope for future generations who speak up.

### Quotes

- "We are lucky in the sense that there are a lot of people in this country who realize what's around the corner."
- "But no matter what, the pattern of world history moves towards freedom."
- "They're going to become symbols of hope and symbols of motivation for the next generation of people who will speak up."

### Oneliner

Beau's diverse audience united by pursuit of freedom faces authoritarian threats but can find hope in symbols transformed through history towards freedom.

### Audience

Activists, Freedom Advocates

### On-the-ground actions from transcript

- Speak out against authoritarianism (implied)
- Actively work to turn the tide against oppressive systems (implied)

### Whats missing in summary

The full transcript delves into the symbolism of historical events like the fall of the Berlin Wall and the importance of active resistance against authoritarianism.

### Tags

#Freedom #Authoritarianism #Symbols #Hope #Activism


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about my key chain.
And you guys.
Ha ha.
So had somebody ask me a question today,
I couldn't answer.
Like, who's your audience?
Like, what?
Like, the demographics, who are they?
Most people can say, you know, they're 24 to 35,
predominantly male, predominantly this race, you know.
I can't do that.
pretty wide cross-section. The only answer I could come up with was those
people supporting or in pursuit of freedom. That's about all y'all have in
common to be honest. It's a very diverse group of people generally leaning
liberal-ish, left-ish, but not always, and it's like, man, well, you're going to have
a lot to work with over the next year because we've been talking about the news articles
that came out today, talking about how the president is going to become more authoritarian.
He's been emboldened by being acquitted and that basically the Senate won't hold him accountable.
afraid to. And it got me thinking, you know. I was like, man, that is a little rough. That's
a little rough of a load. And as I'm in the drive-thru making my $7 installment on my
heart attack, I'm playing with my keychain because food's taking a little while. And
my keychain, by the way. It's a rock. It's got some blue paint on it, pink and blue paint on it.
And it kind of gave me a little bit of hope. There was a time when this rock was a symbol
of everything that was wrong in the world, a symbol of tyranny, symbol of oppression.
And we're probably headed into a period in the United States where we're gonna
have to deal with a lot of authoritarianism. But at the same time, now,
today, this rock, it doesn't symbolize that anymore. An image of this all put
together, oh yeah, it still symbolizes all the bad things, everything that's wrong with
the world. But when it's broken up, scattered all over the world, takes on a different meaning.
You know, the Berlin Wall coming down, it was perfect in so many ways. It was literally
freedom breaking through was literally an end to that type of authoritarianism and it
literally crashed through the wall. It was gorgeous. Perfect. And now pieces of that
wall. They're scattered all over the world, not as symbols of tyranny, but as symbols
of hope and motivation. Germany endured that for decades before it came to an end.
The entire time, there were people who were active. There were people who spoke out. There
There were people who did what they could to turn the tide.
We are lucky in the sense that there are a lot of people in this country who realize
what's around the corner.
And we can speak out now.
We can hopefully stop the symbols from becoming quite as big as this.
There will certainly be symbols that Trump creates over the rest of his tenure that will
be viewed as symbols of authoritarianism.
Hopefully they will be viewed as symbols of how close we came rather than the time we
we did become an authoritarian state.
But no matter what, the pattern of world history moves towards freedom.
It moves towards more freedom for more people.
And eventually, whatever symbols he creates, they're going to become keychains.
They're going to become symbols of hope and symbols of motivation for the next generation
of people who will speak up.
Anyway it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}