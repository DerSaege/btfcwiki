---
title: Let's talk about what Trump can learn from toy designers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=W85G2sthZNQ) |
| Published | 2020/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about G.I. Joe and Cobra, and how toy designers reused vehicle molds for new storylines.
- Compares this toy designer tactic with the President's decision to reverse a policy on using mines.
- Explains the policy on non-persistent mines that degrade or stop working after a set number of times.
- Questions the relevance of using mines in modern warfare scenarios.
- Expresses concerns about the dangers and risks associated with using mines that can be repurposed by opposition forces.
- Criticizes the decision to lift the ban on mines, labeling it as a horrible idea.
- Points out the indiscriminate nature of mines and their potential harm to civilians and US forces.
- Mentions the lack of necessity for mines in modern warfare and suggests there are better tools available.
- Speculates on the possible reasons behind reversing the mine policy, including personal gain or political reasons.
- Condemns the decision and criticizes the Pentagon for not understanding the risks associated with mines.

### Quotes

- "This is a horrible idea."
- "They're unnecessary. They're obsolete."
- "To supply the opposition. To endanger civilians."
- "DOD has made a lot of dumb moves over the years."
- "The brass at the Pentagon apparently does not understand something that every E2 in the world knows and that even toy designers know."

### Oneliner

Toy designers teach reusing molds, but President's mine reversal poses dangers; unnecessary, obsolete, and risks harm to civilians and US forces.

### Audience

Advocates for peace and disarmament

### On-the-ground actions from transcript

- Advocate for peace and disarmament by raising awareness in your community (implied).
- Support organizations working towards banning the use of landmines (suggested).
- Contact lawmakers to express opposition to the use of mines in warfare (suggested).

### Whats missing in summary

The full transcript provides detailed insights into the risks and consequences of using mines in modern warfare, urging for peaceful and safer alternatives.

### Tags

#Disarmament #Landmines #ModernWarfare #PeaceAdvocacy #PoliticalDecisions


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna talk about what toy designers
can teach the president.
So in the late 80s, G.I. Joe, that was a big thing.
That was a big thing.
And it was little action figures,
and then they had vehicles that went along with them.
You had G.I. Joe, and they were pitted against Cobra.
Now as the franchise kind of fell in popularity, I don't know if it was because they were running out of money or they
just couldn't come up with new ideas or what it was, but they decided to reuse some of the molds for the vehicles.
vehicles and they released the Cobra vehicles with new paint jobs and they
gave them to GI Joe and it was called Tiger Force and the storyline was that
GI Joe had captured these vehicles and they were now using them. Fair enough
because that happens in real life. In real life opposition forces they take
each other's equipment and they repurpose it for their own uses. That's a
real life thing. So what's that have to do with the president? The president and
DoD in their infinite wisdom have decided to reverse a policy stopping
the use of mines. The policy states that if they're going to be used they have to
be non-persistent mines. Now what that means in English is that they degrade
And they stop working after a set number of times. There's two ways that this can be done. It can be done chemically
By attacking one specific component. I'm not gonna get into the interior workings of them or
The one they're leaning towards is that the battery just runs out so it doesn't work anymore
They'll also be equipped with a remote control that can make them self-destruct
Okay, all that sounds pretty good. I mean they said it and it goes they go dead in
two weeks. Walk along, step on it, click, nothing happens. Fair enough, fair enough.
However, this is not the 1900s. There is no real use for these on a modern field.
There's not. The study that kind of prompted this had to do with a great
power scenario. So us versus Russia or China. And the idea was that we could use
these to funnel opposition wherever we wanted them to go. And that's the way they
work. They're denial of area devices. You put them out and people can't go there.
It's really kind of simple idea. The thing is you don't win the next war by
preparing for the last. These aren't effective anymore and we have much
better devices to deny area. Tons. Some are unmanned. So the need for them isn't
isn't there.
But let's go back to the toy designers real quick.
So you're going to put a bunch of these out and then the batteries are going to go dead.
Now on today's fields, lines don't exist, but let's just pretend that they do.
Let's play along with this scenario.
They change very quickly.
So the US would either move past or move back from these in short order.
The remote control is probably going to be pretty much useless, but they will degrade
over time.
So you have all of these out there, they are now inert.
They won't work if somebody steps on them.
And that's cool, except for the fact that all the components inside still function.
How long do you think it's going to be before the opposition repurposes them?
If you were part of the recent unpleasantness over there, who was just a great catch?
You went out to snatch somebody, you were sent out, who was the best person to catch?
What did he make?
Did he use commercially available stuff or was it improvised?
We're handing them the tools they need to harm civilians and U.S. forces.
This is a horrible idea.
It's a horrible idea.
And understand even while they are short-lived, during that time they are indiscriminate.
When they are active, they are indiscriminate.
They don't know it's a device that sits on the ground.
It has no clue whether that's a combat boot or a barefoot child that just stepped on it.
Most countries have completely prohibited the use of these things.
They are unnecessary.
They're obsolete.
They are obsolete.
This isn't something we need, and it can only hurt us.
I would suggest that the only real reasons, because I racked my brain trying to come up
with any real, realistic scenarios in which these would be necessary, or even beneficial,
and there are some where maybe it kind of helps, but there's better tools for that.
So the only scenarios I can come up with for this occurring and this ban being lifted and
this policy being reversed is that somebody's been offered a really cush retirement job
or the president realized that Obama was instrumental in prohibiting their use, except outside of
Korea.
That seems like a really, really stupid reason to put people in harm's way.
To supply the opposition.
To endanger civilians.
DOD has made a lot of dumb moves over the years.
This will go in that book.
And the brass at the Pentagon apparently does not understand something that every E2 in
the world knows and that even toy designers know.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}