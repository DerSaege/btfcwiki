---
title: Let's talk about Mr. Rogers, goldfish, strawberries, and dreams....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=B4Kzl_Q57NE) |
| Published | 2020/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Sharing a heartwarming story about Mr. Rogers and his dedication to a blind girl worried about the fish being fed on his show.
- Demonstrating self-reliance by growing his own food with a Bag-O-Blooms Strawberry Kit.
- Encouraging viewers to overcome obstacles and pursue their dreams, no matter the challenges they face.
- Providing tips on growing plants even in small spaces like balconies without the need for weeding.
- Emphasizing the simplicity of growing plants in bags with sunlight and moisture requirements.
- Mentioning alternative methods like using Ziploc bags or shoe organizers to grow plants effectively.
- Inspiring confidence and self-reliance in oneself to tackle obstacles and achieve goals.
- Advocating for supporting and encouraging others to pursue their dreams and not give up.

### Quotes

- "Let nothing stand in your way."
- "There's always a way."
- "Don't give up on your dreams."

### Oneliner

Beau shares Mr. Rogers' story, showcases self-reliance through growing food, and encourages overcoming obstacles to pursue dreams.

### Audience

Gardeners and dreamers

### On-the-ground actions from transcript

- Start growing your own food in small spaces like balconies using bags or containers (exemplified)
- Encourage and support others in pursuing their dreams (exemplified)

### Whats missing in summary

The emotional connection and personal motivation that Beau's storytelling adds to the message.

### Tags

#SelfReliance #Dreams #OvercomingObstacles #Gardening #Support


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're gonna talk about Mr. Rogers,
dreams and not letting anything stand in your way.
I heard a story about Mr. Rogers once,
I don't know if it's true, it could be an urban legend.
But I never looked into it because I like the story.
The reason that he narrated and always said
he narrated and always said that he was feeding the fish. It's because he got a
letter from a blind girl who was worried about the fish being fed because I guess
he said it once and then didn't say it again. So from that point forward he
always made sure it was part of the show. Right now I've got something called a
Bag-O-Blooms Strawberry Kit. This isn't a sponsorship thing. It's $5.99. I got it at
tractor supply. Inside there's a point. Inside there's a plastic bag with holes. Big holes
for the plants, little holes for the drainage at the bottom, and strawberries. Strawberry
plants. I've got a bag of potting soil here on the counter.
So one of the reasons I love growing your own food is self-reliance.
Breed self-reliance.
And when I did that video about planting your yard, there was a comment there.
Most people, when they have comments about it, it's, I don't have time, I don't have
space.
This person didn't have space.
They have a balcony, but it gets sunned, but they're blind and worried about weeding.
These bags don't require weeding, and they work on a balcony, and it's been about two
minutes since I started putting this together. It doesn't take much time either. Eight strawberry
plants. If you don't want to get the kit because they don't have the fruit you want or the
vegetable you want to do it with, you can use Ziploc bags, trash bags, do the same thing.
All it needs is soil in the bag to be hung in sunlight matching whatever the plant needs.
Sometimes it's going to be direct sun, so it needs like six hours a day.
And moisture, that's it, the plant has no idea it's not in the ground and that it's
in a bag.
It'll work.
I've seen people do this with these.
I've seen people do it in containers on patios.
I've seen people use shoe organizers that hang on the back of doors that have the pockets
in them, that poke little holes in the fabric, and then use those.
And I've actually seen people use those and have the bottom done with watermelons, because
the vines that come off with the fruit on them, they don't need to be in the dirt, just
the roots.
So they're shoe organizers.
You can do it with that.
I know this video is really for one person, but at the same time, I think everybody can
take something away from it.
You may have something you want to fruit.
You may have some dream.
Let nothing stand in your way.
You may have society throwing obstacles in your path left and right.
You may have a physical issue.
There's always a way.
But given the fact that one of the reasons I advocate this so much is to instill self-reliance
and build that confidence in yourself.
I think that if somebody leaves a comment like that, somebody should tell them that
the goldfish is being fed.
Don't give up on your dreams.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}