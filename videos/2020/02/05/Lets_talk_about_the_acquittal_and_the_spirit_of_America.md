---
title: Let's talk about the acquittal and the spirit of America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kGlTWg_Lfd4) |
| Published | 2020/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the current administration for deviating from the spirit of the United States.
- Believes that the President will be acquitted due to partisanship overriding conscience.
- Blames senators for enabling the President's actions and violating the country's spirit.
- Mentions the Human Rights Watch report on El Salvador, linking Senate decisions to deaths there.
- Expresses concern over the focus on Trump rather than the system that enables him.
- Warns against the Democratic Party's reluctance for substantial change in candidates.
- Stresses the importance of running a candidate focused on real change to prevent Trump's reelection.
- Questions the significance of voting blue if policies remain unchanged.
- Emphasizes the need for genuine change to uphold the country's ideals and promises.
- Condemns sending people back to dangerous situations and the performative nature of American values.

### Quotes

- "Party over country."
- "We are not the land of the free or the home of the brave, because we deny freedom out of cowardice."
- "We're losing the plot and we're losing the war because this is a war for the very soul of this country."
- "We need real change not just a change of personality."
- "It's just going to be an act, completely performative, meaning nothing."

### Oneliner

The current administration, Senate decisions, and political system are critiqued for deviating from the spirit of the United States, risking lives and losing the country's ideals.

### Audience

American citizens

### On-the-ground actions from transcript

- Advocate for candidates focused on real change in elections (implied)
- Support policies that prioritize substantial change and system alteration (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current political landscape in the United States, urging for significant change to uphold the country's ideals and prevent further deterioration.

### Tags

#UnitedStates #Politics #Change #DemocraticParty #HumanRights


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about the spirit of the United States.
The promises, what it was supposed to be.
What the idea is.
And it's an idea that we still
pretend that we hold to.
We still think we're that country. We still believe our own
advertising to the world.
We're not that and we haven't been that for a very long time and this administration is making it worse and worse.
Today, probably by the time you watch this, the President of the United States will have been acquitted.
He will have been acquitted of crimes that many who voted not guilty
say that they know he did, but they voted not guilty out of partisanship.
They chose party over conscience.
Party over the spirit of the United States.
Party over country.
Their names will forever be linked to Him.
But beyond that, they're responsible for Him.
Everything that He's done and everything that He will do is on their hands.
They did it.
It's fitting that while our senators violate the spirit of the United States, violate their
oaths, that the report from Human Rights Watch has come out.
One of the ideas of the United States was that we would take the huddled masses, those
yearning to breathe free.
Human Rights Watch tracked what happened to those sent back to El Salvador, those people
who were here and stated they had a credible fear if they were returned home.
And those who today voted not guilty, many of them, said they were trying to game the
system and using a loophole.
A hundred of them are dead.
Those deaths are on the hands of those in the Senate.
Those that will come after are on the hands of those in the Senate who enabled this today.
And there are much worse things than death sometimes.
They're detailed in the report, I'm not going to detail it here.
That's one country, one small country, where they can actually find the people or what's
left of them.
The United States is, in a lot of ways, losing the plot.
Trump out, yeah, that's a goal, but it's a short-term goal.
That's not the actual problem.
The problem is not one guy.
It's a system that enables him.
As the democratic establishment tries to force candidates that are centrist, that are mainstream,
They don't actually want change, substantial change, the kind of change that will save
lives and they try to force those candidates onto the American people.
They're losing the plot and they're losing the war because this is a war, it is a war
for the very soul of this country.
There's not much left.
If we let this go, if this continues, we're just going to turn into any other oligarchy.
The advertising is false.
We are not the land of the free or the home of the brave, because we deny freedom out
of cowardice.
We're not holding up to that anymore.
If the Democratic Party does not run a candidate who is about substantial change, real change,
and altering the system, they will lose.
And we'll get four more years of Trump.
But it doesn't really matter.
Because if the DNC just wants to run a candidate who is Trump-lite and puts a kinder, gentler
face on the behavior doesn't matter anyway.
The idea of vote blue no matter who, yeah it sounds good to get rid of Trump.
But if these policies are going to remain in effect, why does it matter?
Why does it matter?
We are literally sending people back to their deaths.
And the Senate, members of the Senate, well I know, I believe they approve their case
but I'm going to vote not guilty anyway because of your election.
You understand that if those in the Republican Party had shown some backbone and said, yes,
was wrong, a chorus of senators could have drowned out any cult of personality, but they
chose not to.
They chose to enable this and allow it to go on.
One country, people are dying.
And understand the reason I'm saying this about the Democratic Party running a kindler,
gentler more eloquent version of Trump if the policies are the same it doesn't
matter some of these people were sent home sent to their deaths under Obama we
need real change not just a change of personality and if we don't get it we're
going to lose this country we're going to lose all the ideals that it
represents, everything that you're promised, everything you believe in, those chills that
you get when you hear the national anthem and look at the flag, it's going to be
more than worthless.
Because it's just going to be an act, completely performative, meaning nothing.
As people literally die, clawing, trying to get to the land of the free and the home of
the brave.
No, we're too scared to let you in.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}