---
title: Let's talk about the stakes and strategy of the 2020 election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ucnCZI3Q4ms) |
| Published | 2020/02/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Remind people of the stakes amidst the constant influx of news to refocus on what we're fighting for rather than against.
- Points out the history of a New York billionaire who has minimized civil rights, ignored income inequality, advanced surveillance and police state, and scapegoated minorities.
- Raises the question of whether the billionaire in question is Trump or Bloomberg, showcasing the danger of personifying what we're fighting against.
- Suggests a military analogy where allowing opposing forces to weaken each other could be a strategic move before targeting the ultimate opposition.
- Advocates for letting Sunday, Sunday, Sunday, the Battle of the Billionaires play out to potentially weaken the larger opposition force.
- Considers the possibility of Bloomberg winning and the implications of him securing the Democratic nomination.
- Expresses concern over voters' inability to see through propaganda and focus on principles rather than personalities.
- Warns against blindly following the "vote blue no matter who" mantra and the dangers it poses in allowing bad actors to secure nominations.
- Proposes a long-term strategy of allowing Bloomberg to weaken Trump before dealing with him in the primaries.
- Emphasizes the importance of remembering what is being fought for and prioritizing principles over personalities.

### Quotes

- "We have to think of things in the affirmative."
- "Principle over personality."
- "We have to think of things in the affirmative."
- "If we personify the evil that exists in one person, we become very easy to manipulate."

### Oneliner

Remind people of the stakes, focus on fighting for principles over personalities, and strategize wisely amidst political battles.

### Audience

Voters, Political Activists

### On-the-ground actions from transcript

- Strategize and prioritize focusing on principles over personalities in political decision-making (implied).
- Stay informed about political candidates' histories and priorities to make informed voting choices (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on the danger of personifying political opponents and the importance of strategic thinking in political movements.

### Tags

#Stakes #PoliticalActivism #PrinciplesOverPersonalities #StrategicThinking #VotingChoices


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about the stakes.
We're gonna talk about the stakes.
I think it's important to remind people
of the stakes every once in a while.
We get inundated with so much news
and so much stuff to be concerned about,
new things every day,
that we often forget what we're fighting for.
We just remember what we're fighting against.
And I think it might be time to remind people of that.
So let's take stock.
What do we have?
We've got this person who has a history of minimizing civil rights, a history of doing
nothing to address income inequality, a history of advancing the surveillance state and the
police state, a history of scapegoating minorities.
That New York billionaire is a problem.
Now the question is, do you know who I'm talking about?
Or is it a little confusing, is it muddled?
Am I talking about Trump or am I talking about Bloomberg?
And the fact that based on that description, you can't tell, that's an issue.
Because we've become so concerned with Trump out, with that short-term goal, we've personified
what we're fighting against and that's a danger because then it becomes real easy
to end up supporting the exact same thing. We have to remember things in the
affirmative, what we're fighting for. Now the question is what do we do? What do we
do from here? There are some people who say we have to start opposing Bloomberg
and get him out of the primaries right now. It's imperative and I understand
logic. I'm gonna present an alternate strategy. Let's say you're in the
military and you are on an advanced modern battlefield. You have multiple
opposition groups, multiple groups you are pitted against. You walk over a ridge
and you see opposition group A over here with ten people and opposition group B
over here with five people and they are going at it. What do you do? Do you try to
to engage them all or do you wait? Yeah they're all your opposition but right now
they're fighting each other. One of two things will happen either the larger
force will take out the smaller force or the smaller force will pull off a
miracle victory. Either way at the end of it doesn't matter who wins they'll be
weakened and be easier to go after.
I would suggest that we all sit back and enjoy Sunday, Sunday, Sunday, the Battle of the
Billionaires.
And then if Trump doesn't do it, Bloomberg gets taken out in the primaries.
The concern with that is that Bloomberg wins.
And it's possible.
possible. You know in a recent video when I was talking about the campaign promise that matters
I said a thinking man and there were a couple people that were like you always use gender neutral terms
I can't believe you did that women can be dictators too, and that's true
They can't but I wasn't speaking in general terms. I had somebody specific in mind
Bloomberg is that thinking man?
Bloomberg is that thinking man he's smarter
He is smarter. He knows not to say the quiet part out loud
He's a little more polished. In a lot of ways that makes him more dangerous. But
here's the question, and the important one really, if Bloomberg can win the
Democratic nomination with that history, what does that say? It says it's already
lost. It says it doesn't matter because it's inevitable. We will be headed to
fascism or at the very least a formalized corporate oligarchy rather
than the ad hoc one we have today. If Bloomberg can win the Democratic Party's
nomination, there is no opposition party because the voters can't see through the
propaganda. They can't focus on principle rather than personality.
This is the danger of vote blue no matter who. If people believe that, all it
takes is for a bad actor to get somebody in the party, to get them the
nomination. So the important thing to me would be to remember that, but long-term
strategy, I would suggest we allow Bloomberg to pursue his old-man hobby of
running for president and to spend millions dragging Trump and weakening the
larger opposition force and then deal with Bloomberg in the primaries.
We have to remember what we're fighting for.
We have to think of things in the affirmative.
If we personify the evil that exists in one person,
we become very easy to manipulate.
Principle over personality.
Anyway, it's just a thought.
Y'all have a good day. Thanks for watching!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}