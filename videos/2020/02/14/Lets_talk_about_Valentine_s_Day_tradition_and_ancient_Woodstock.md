---
title: Let's talk about Valentine's Day, tradition, and ancient Woodstock....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ysVLefWw8As) |
| Published | 2020/02/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts off by addressing the audience on Valentine's Day, mentioning the various ways people celebrate the day.
- The videos throughout the week had themes centered around tradition, radical ideas, and questioning the importance of holding on to traditions.
- Valentine's Day is rooted in tradition, with people exchanging chocolates without really knowing the full history behind the day.
- The popular theory behind Valentine's Day involves a priest named Valentine who defied Emperor Claudius II by marrying soldiers in secret, leading to his beheading and subsequent canonization as a saint.
- The romantic aspect of Valentine's Day didn't emerge until Shakespearean times, and the industrial revolution saw Hallmark commercializing the day with cards.
- February 14th was chosen by the church to coincide with a pagan festival involving music, love, nudity, and rituals like drawing names for temporary romantic partners.
- Beau questions the continuation of certain traditions, suggesting that some may be outdated or have lost their original meaning.
- He stresses the importance of examining and reevaluating the traditions and mythology prevalent in the U.S., particularly in times of social upheaval.
- Despite the discomfort that may arise from challenging established traditions, Beau believes it is necessary for societal renewal and progress.
- Beau warns that without proactive choices and planning from the people, those in power can easily introduce new traditions or uphold the status quo.

### Quotes

- "We don't question it. we've just always done it this way, so we don't change."
- "When you examine traditions sometimes, you realize that they may not be worth continuing."
- "Now is the time to examine those traditions and that mythology."

### Oneliner

Beau questions the origins of Valentine's Day, delves into its history, and urges reevaluation of traditions in times of social change.

### Audience

Curious individuals

### On-the-ground actions from transcript

- Examine and question traditions (suggested)
- Reassess cultural myths and practices (suggested)
- Plan for societal renewal (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of Valentine's Day traditions, shedding light on their historical roots and urging critical examination of established practices.

### Tags

#ValentinesDay #Traditions #CulturalHistory #SocialChange #Mythology


## Transcript
Well, howdy there internet people, it's Bo again. Happy Valentine's Day or
Singles Awareness Day or whatever it is you're celebrating if you're celebrating anything at all
And that kind of fits
Most of this week the videos have been kind of themed around
Tradition and radical ideas and
and whether or not we should hold to traditions.
And here we are, ending on Valentine's Day.
Why are people getting chocolates today?
Nobody has a clue, right?
Yeah, it's tradition.
It's tradition.
It's what we've always done.
We don't question it.
we've just always done it this way, so we don't change.
So the history of this, the truth is nobody knows.
Nobody really knows exactly why this is happening,
why Valentine's Day is a thing.
The popular theory, and there are a couple,
but the one that has the most weight behind it
is there was a guy named Valentine,
And Emperor Claudius II had banned soldiers from having wives.
Well, Valentine, he was a priest in Rome, and he didn't like that.
So he married them in secret.
Emperor didn't like that.
So he had him beheaded.
And he became a saint, and we get chocolates.
There's a little bit more to that.
it really wasn't all about love
at uh... initially
uh... there was another reason we'll get to that but
the uh...
the the love the romantic aspect to it didn't really come around to
Shakespearean times
and Shakespeare made it about love
then the industrial revolution hit
and Hallmark made it about cards
that's what happened
before then there there's a
there's a prelude, the church chose the day,
February 14th, to be smack dab in the middle
of a pagan festival that was on the 13th, 14th, and 15th.
It was three days of music, love, nudity,
and it was basically pagan woodstock.
The men would beat the women with bloody hides to help their fertility and then they would
draw names out of a bucket and that was your boyfriend or girlfriend for the period of
the festival.
Sometimes they'd get married.
And we get chocolates.
It seems kind of weird, right?
When you examine traditions sometimes, you realize that they may not be worth continuing.
You may realize that they're a little outdated.
You may realize that they have absolutely nothing to do with what you thought they had
to do with.
The U.S. has a lot of traditions.
We have a lot of mythology, and we need to examine it.
We're headed into a period, and we're probably already in the period, of social upheaval.
And that's scary to some.
And I get that.
But at the end of upheaval comes renewal.
And that's when we're going to get to decide where we're headed for the next 20 or 30 years,
if we know.
Otherwise it's going to be a return to the status quo.
So now is the time to examine those traditions and that mythology.
Those things that we hold dear and some of it is not going to be comfortable.
But we have to do it.
somebody with a little bit of power can just come in and plant something right
in the middle of our festival and that's what will get remembered that's what
will be the new tradition that's what will be carried down if the people on
the bottom. Don't make the choices and have a plan. I assure you the people on
the top two. Anyway, that's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}