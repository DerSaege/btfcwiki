---
title: Let's talk about the pendulum (Roger Stone Part 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YQxlsw6_EMI) |
| Published | 2020/02/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The country is shifting back from the right and we have a chance to create something better rather than just returning to normal.
- Questioning our beliefs is necessary, particularly regarding long prison sentences and their effectiveness in deterring crime.
- Criminal justice reform is imperative and requires new ideas and heavy implementation.
- When certain currently illegal substances become legal, the approach towards those involved must change to focus on treatment and community service for non-violent offenders.
- Planning for the future of criminal justice reform needs to start now, not when the momentum has subsided.
- Beau acknowledges provoking reactions to draw attention to the excessive sentences within the system.
- Fixing the failures in the criminal justice system should be a top priority in creating a just society.

### Quotes

- "We have to start thinking about this stuff now."
- "If we are going to create a just society, fixing the failures in our criminal justice system should be pretty near the top of the list."

### Oneliner

Beau urges us to question the status quo, push for criminal justice reform, and create a better future rather than settling for past norms.

### Audience

Advocates for justice reform.

### On-the-ground actions from transcript

- Start questioning beliefs on the effectiveness of long prison sentences now, suggested.
- Push for criminal justice reform by advocating for new ideas and heavy implementations, implied.
- Plan for a future where the failures of the criminal justice system are rectified, exemplified.

### Whats missing in summary

The emotional urgency and call to action that Beau conveys throughout his message.

### Tags

#JusticeReform #QuestionBeliefs #CriminalJustice #CreateChange #CommunityService


## Transcript
Well, howdy there, internet people, it's Bo again.
So now that I have everybody's attention,
let's move on to what we're talking about.
And that is what we're talking about.
But there's a little more to it.
Right now, the country has swung to the right,
and it's starting to swing back.
As it swings back, we say stuff like, well,
we want to return to normal.
We want to return to the way things
were before Trump, the pre-Trump era.
But we need to stop for a second.
Is that really what we want?
Were things really the way we wanted them
under Bush, under Obama, under Clinton?
Yeah, they're better than Trump,
but is that what we want?
Or do we use that momentum when that pendulum
starts to swing back to go a little bit further,
to create something better?
That's what I would suggest we should do.
And in order to do that,
We have to question things that we believe.
The reality is there is absolutely no evidence to suggest that long prison
sentences
do anything to deter crime.
That evidence does not exist.
And that's just one facet of the things we need to work on.
We need to look at, we need to come up with new ideas about.
Criminal justice reform needs to take place and it needs to be pretty heavy.
We have a lot that we have to do there.
I would suggest that even though when it swings back,
I would assume that some substances
that are currently illegal will become legal, others won't.
What are we gonna do with those people?
Those people that are selling?
We gonna lock them up, continue the war on drugs in a way,
even though it's failed?
Or do we come up with something new?
Perhaps take the money that would be spent locking them up
and reinforcing the idea of might makes right.
Take that money and put it into treatment.
And then have those people, non-violent offenders,
doing community service there.
We have to have a blueprint to move forward.
We have to start thinking about this stuff now.
We can't wait until the momentum's gone.
When it swings back is the time to start talking about this stuff.
and it's the time to act.
But in order to come up with that blueprint,
we have to start questioning this stuff now, today.
And yeah, I chose stone because I was fairly certain
it was going to get the reaction it did.
And to be honest, though, to be very clear,
I do believe that's an excessive sentence.
I do.
However, I probably could have framed it a little bit better
if I didn't want to provoke a little bit of a reaction on this one.
It's probably one of the most important things we can do right now.
As we wait for everything to settle, for the dust to settle, for
the stars to align, and for
things to start moving back towards the arc of justice,
we need to come up with a plan.
We have to figure out what we're going to do, and I would suggest
If we are going to create a just society,
fixing the failures in our criminal justice system
should be pretty near the top of the list.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}