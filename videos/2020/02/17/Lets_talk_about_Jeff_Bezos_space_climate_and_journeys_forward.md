---
title: Let's talk about Jeff Bezos, space, climate, and journeys forward....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7YbUxb0Ghms) |
| Published | 2020/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Bezos is allocating $10 billion to combat climate change, sparking debate on its intent and impact.
- Draws parallels between addressing climate change and historical challenges like the space race.
- Mentions the indirect benefits of past challenges, like space race innovations that became everyday technology.
- Emphasizes that tackling climate change will lead to technological advancements and propel humanity forward.
- Suggests that even if one doesn't believe in climate change, the journey to combat it will result in positive developments.
- Argues that investing in fighting climate change is necessary due to the potential benefits and cost savings in the long run.
- Compares the current battle against climate change to past significant events that pushed humanity forward.
- Urges for action and sees addressing climate change as a challenge that humanity must undertake for the betterment of all.
- Views combating climate change as an investment in advancing humanity with no real downside.

### Quotes

- "The journey to combat it, to reduce carbon, all of this stuff, the technologies that are going to come from it, are going to propel mankind forward."
- "There is no downside to attempting to mitigate climate change."
- "This is this generation, this time period, this is its space race, this is its World War II."

### Oneliner

Bezos pledges $10 billion for climate change; addressing it is a challenge propelling humanity forward with no downside.

### Audience

Global citizens, environmental activists

### On-the-ground actions from transcript

- Invest in renewable energy sources (implied)
- Support initiatives combating climate change (implied)
- Advocate for sustainable practices in daily life (implied)

### Whats missing in summary

Detailed examples of specific byproducts resulting from addressing climate change.

### Tags

#ClimateChange #Innovation #SpaceRace #Investment #Technology


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Bezos,
Amazon's pledge for $10 billion, and the journeys,
the journey that comes along with it.
So Bezos is putting out $10 billion
for a fund to help combat climate change.
Cool, cool.
People are already saying that it's a PR move,
and it probably is on some level.
It doesn't mean that he's not putting up 10 bill for a bill.
There are a lot of people who are opposed to the idea of addressing climate change for
various reasons.
But I want to point to the space program.
When we think of the space program, when we think of the space race, what do we think
of?
Sputnik, the Soviet satellite.
We think of Neil Armstrong, John Glenn, Michael Collins, famous astronauts, and it culminates
in the moon shot, putting a spacecraft on the moon.
That's what we think of in the space race.
But what about all the byproducts?
The stuff that we don't realize is actually part of the space race, like your cordless
vacuum that was invented because of the space race.
NASA needed batteries, and well, that's where it came from.
The CMOS sensor that's in the camera this is filmed on
has its origins in the space race.
All those cool prosthetics that exist today,
their roots are in the robotics of the space race.
Those silly memory foam mattresses, space race.
firefighting equipment that has saved thousands of lives. Space race. That's where it comes
from. Every time humanity decides to do something, not because it is easy but because it is hard,
it takes a giant leap forward. Even if you disagree with the ideas behind climate change,
If you just refuse to see what's happening, understand that the journey to combat it,
to reduce carbon, all of this stuff, the technologies that are going to come from it, are going
to propel mankind forward.
It's going to make humanity better.
It's a challenge.
Every great challenge produces byproducts like that.
If you don't want to do it to save the planet, do it in your own self-interest.
Understand without Sputnik, there's no cell phones.
Not really.
Not the way we have them today.
There's no satellite TV.
There's no camera in your phone, microprocessors, all of this stuff was spurred along because
of the space race.
The battle against climate change will do the same thing.
Byproducts are likely to include cheaper energy, cleaner energy, maybe self-sufficient energy
where you wouldn't even have a bill.
That's just one.
Cleaner water, which means a healthier populace.
The fight is one that needs to be undertaken.
Even if you don't agree, if you think it's caused by something else, if you think it's
just all hogwash, it doesn't matter.
The journey, the attempt will create things that will make our lives better.
There is no downside to attempting to mitigate climate change.
There's not.
We're going to spend a bunch of money.
Yeah, yeah, we are.
But the reality is that climate change is already costing us billions, probably trillions.
So it'll probably save money in the long run.
But let's say you don't believe it.
It's an investment.
in investment, it's venture capital that's put out there to move humanity forward.
I don't see the problem in it.
This is one of those things where if we do it, there's no real downside.
And if we don't, there will be.
There will be.
Even if you don't believe it, there will be.
There was a set of articles published in I think the Guardian, excerpts from a book,
showing, best case and worst case, I'll put them down below.
This is this generation, this time period, this is its space race, this is its World
War II.
This is its great challenge.
thing it has to overcome to keep this species moving.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}