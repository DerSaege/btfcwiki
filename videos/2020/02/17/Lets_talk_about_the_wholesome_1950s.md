---
title: Let's talk about the wholesome 1950s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XCDoYkAcvRg) |
| Published | 2020/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the myth of the wholesome 1950s and the desire to reach back to that era.
- Mentions how certain political slogans reference the 1950s as a time to "make America great" based on movies' portrayal.
- Points out the reality behind the wholesome image, including icons like Marilyn Monroe and themes in Broadway productions.
- Explains how the production code in movies enforced a certain image by prohibiting controversial topics.
- Argues that books provide a more accurate view of the 1950s, mentioning works like "Catcher in the Rye" and "On the Road."
- Emphasizes that the 1950s were not all as wholesome as portrayed, referencing global works like "Dr. Zhivago."
- Concludes that the nostalgia for the 1950s is based on a myth and a politically correct version of history.

### Quotes

- "When people say, make America great, and they're looking to the 1950s, they're looking back to a mythology, they're looking back to something that never existed."
- "The 1950s were not all leave it to Beaver."
- "It wasn't real. It was a censored, politically correct version of the times."

### Oneliner

Beau dismantles the myth of the wholesome 1950s, revealing it as a politically correct version of history that never truly existed.

### Audience

History enthusiasts, truth-seekers

### On-the-ground actions from transcript

- Read books from the 1950s era to gain a more accurate understanding of the time (suggested).
- Challenge nostalgic narratives about historical eras by seeking diverse perspectives (exemplified).

### Whats missing in summary

The full transcript provides a deep dive into the myth vs. reality of the wholesome 1950s, urging listeners to question nostalgic narratives and seek a more nuanced understanding of history.

### Tags

#1950s #Nostalgia #MythVsReality #History #Books


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about a bygone era,
the wholesome 1950s.
That's an image that people have.
George Santayana died, goodbye, in 1952.
It's kind of fitting that he died then.
He's the guy, those who cannot remember the past
condemned to repeat it. And it makes sense. Now that saying has been said in a bunch of
different ways over the years, but when you're talking about the 1950s, man, it makes sense.
Because we have that image. We have that image of this wholesome, leave it to beaver type
life. And people want to reach back to it. In fact, there are certain political slogans
today that reference it, and that's the idea when you ask people what they mean.
You know, when you see that red hat and you say, when was it?
You want to make America great.
When are you talking about?
Most times they say the 1950s because they have this idea, this image that comes from
movies.
But what if there's a reason that image exists and it wasn't reality?
We need to remember that the same era that gave us the wholesome Doris Day gave us Marilyn
Monroe and Bridgette Bardell, who today are kind of seen as the first icons of, well let's
just say the less than wholesome.
You look to Broadway and you have South Pacific in 1949 addressing interracial relationships.
have the King and I. And when you look at it today, you're like, man, that's kind of
racist. Because it is. But some of the themes that were being addressed when you look at
them in context of the times and substitute Siam for other locations in Southeast Asia
where the former colonial powers might have been trying to get a foothold, it kind of
That takes on a new meaning.
The wholesome 1950s gave us James Dean and Rubble without a Cause.
See those are just a couple.
The movies overall are super wholesome because there was a thing called the production code.
And the production code didn't allow discussion of any controversial topics at all.
You couldn't make fun of religion.
You couldn't talk about certain substances unless it was in a bad light.
You couldn't talk about family planning or extramarital activities.
You couldn't do any of that.
That's why that image exists.
But the thing is, the fact that those rules existed, saying you couldn't talk about those
things shows that it wasn't reality.
Because if it wasn't going on in the real world, there would be no reason to prohibit
it from being in movies, because it wouldn't show up.
Art does imitate life.
The best place to look if you want to get a real view of that period isn't movies,
it's books.
We have Catcher in the Rye, 1951, became a symbol of being non-conformist, why?
Because the 1950s was shoving it down your throat, you've got to be this way.
People didn't like it.
They became rebels without causes and that book was highly denigrated by many of those
in wholesome communities, hence shortly thereafter Peyton Place came out and just put a big old
mirror on the hypocrisy.
The books are where it's at.
The books will tell you about the 1950s.
Jack Kerouac, you know, that's associated with the 60s and 70s.
On the road, 1957.
The 50s were not all leave it to Bieber.
And it wasn't confined to the U.S. Boris Pastronov, Dr. Zhivago was finished the same year, 1957.
And that counterculture narrative that kind of casts a bland eye on the October Revolution
was so counter to the narrative, the established narrative in the Soviet Union that the CIA
the book and tried to get it out there because it would undermine the Soviets.
Because the narrative in films, that's what they wanted people to think it was like.
That's what they wanted it to be like and they exercised a great deal of control over
it but that's not what it was really like.
And then when you step just outside of the 50s, into the 60s, 1961 you have Stranger
in a Strange Land that openly questioned, well, everything, almost all social norms.
Some of them, in my opinion, don't need to be questioned, but hey, whatever, the question
can be asked.
So when people say, make America great, and they're looking to the 1950s, they're looking
back to a mythology, they're looking back to something that never existed.
It wasn't real.
It was a censored, politically correct version of the times.
It wasn't reality.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}