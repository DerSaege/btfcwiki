---
title: Let's talk about Michigan, lost causes, and a sense of purpose....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ud2yfIXJAe0) |
| Published | 2020/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts by discussing the desire to belong, a sense of purpose, insurmountable odds, lost causes, and a specific incident in Michigan.
- He shares his experience of being on Twitter and the sudden shift in mood caused by a question about the motivations of individuals in a difficult situation.
- Some individuals may have ended up in challenging circumstances because they sought belonging and purpose, leading to severe consequences.
- The romanticization of battling insurmountable odds in the U.S. has sometimes been misconstrued as resorting to violence, neglecting the importance of building and living.
- Beau acknowledges the allure of facing challenges and the satisfaction of overcoming them but stresses that building and living are more difficult yet fulfilling endeavors.
- He criticizes how working-class individuals were manipulated by those in power to serve their interests without realizing the repercussions.
- The exploitation of people’s desire for purpose by societal and political structures is condemned by Beau.
- Advocating for building a just society and supporting those at the bottom, Beau suggests that true fulfillment lies in constructing a positive legacy rather than engaging in destructive actions.
- By working towards building a better world for the marginalized, individuals can face significant challenges and make a lasting impact without resorting to violence.
- Beau concludes by encouraging listeners to find purpose in helping those who have lost hope and making a difference in their lives without resorting to destructive means.

### Quotes

- "Building, living is harder, much more fulfilling too."
- "Build something for them. Work to build a just society."
- "You don't have to destroy anything. Build something better."
- "You don't have to hurt anybody. You just have to build something better."
- "You can help bring them there. Isn't that better?"

### Oneliner

Beau stresses the importance of building a just society and finding purpose in helping others rather than resorting to destructive actions.

### Audience

Social activists, community builders.

### On-the-ground actions from transcript

- Build something for those at the bottom (suggested).
- Work towards creating a just society (implied).
- Support marginalized communities in building a better world (exemplified).

### Whats missing in summary

Beau's emotional delivery and emphasis on the transformative power of constructive actions are best experienced by watching the full transcript.

### Tags

#Purpose #CommunityBuilding #SocialJustice #ConstructiveAction #Empowerment


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about wanting to belong, a sense of purpose, insurmountable
odds, lost causes, and that thing up in Michigan.
We're going to do this because last night I was doing what a whole lot of the country
was doing.
I was hanging out on Twitter, laughing, joking, just doom scrolling, looking at the mug shots.
Dozens of people in that thread, hundreds, everybody having a good time.
And then somebody came along and just stomped on the vibe.
They did that by asking a question, simple question.
How many of these guys you think are in this situation now wound up there simply because
they wanted to belong to something?
They wanted a sense of purpose.
Yeah, yeah, that's probably true.
That is probably true for some, not all, but for some of them.
Doesn't matter, it's too late for them, they've made their bed, they're going to have to deal
with the consequences and those consequences appear to be pretty severe.
However, there may be other people on that road with that desire for a sense of purpose
and it is that sense of purpose that turned into that plan because here in the US we have
romanticized that hopeless battle, going up against those insurmountable odds.
Believe me, I get it, I get it.
But somewhere that got equated with pulling a trigger and the imagery became, you know,
being pinned down and outnumbered and all that.
I do, I do understand that desire, that rush you get from facing those insurmountable odds
and maybe coming out on the other side of it and setting things right.
I get it, I do.
Thing is, if you're on that road long enough, you will, if you make it, you'll figure out
that destroying, fighting, dying, it's easy, it's easy.
Building, living is harder, much more fulfilling too.
You're also less likely to get co-opted and that's what happened here.
Bunch of working class, working poor got co-opted, got energized by rhetoric from the establishment,
from those in power.
They wound up doing their betters bidding without realizing it, doing something that
benefits them politically or they thought it would.
And those at the top, those they were serving, I don't care about them, their biggest concern
is the fact that some of their friends that make seven, eight, nine, ten figures a year
may have to pay just a little bit more in taxes.
Somebody exploited that desire for a sense of purpose because our society doesn't always
give people that.
You know, the thing is, if you want a sense of purpose, you want to fight those hopeless
battles, you want insurmountable odds, start trying to look out for those on the bottom.
Start trying to build something for them.
Build, you don't have to destroy anything.
Build something for them.
Work to build a just society.
Believe me, when you start trying to build something for those on the bottom, you're
facing those insurmountable odds.
You wake up every day outnumbered, outgunned, outmanned, outfinanced, and behind enemy lines.
If that is the rush you seek, it doesn't get better than that.
It is never ending.
Aside from that, you get to build.
You get to do something that leaves a positive mark rather than becoming the butt of a joke,
an internet meme.
If you do it right, if you succeed, if you are successful, and you build that better
world, you don't have to destroy anything, those other systems, those other establishments,
they will decay on their own from disuse, and you will have completed your revolution
without firing a shot.
You don't have to hurt anybody.
You just have to build something better.
If you are one of those who is looking for a sense of purpose, that's it, and if you
do it, you're building something for somebody like you.
Because if you want to engage in a hopeless battle, at some point in time, on some level,
you lost hope.
There are people out there who are there now who have no hope, and you can help give them
to them.
You can help bring them there.
Isn't that better?
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}