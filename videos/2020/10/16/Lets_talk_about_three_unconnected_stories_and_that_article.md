---
title: Let's talk about three unconnected stories and that article....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7Io8KG2ZkHA) |
| Published | 2020/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces three seemingly unconnected stories to shed light on how things may not be as they seem.
- Using Coca-Cola as an example, Beau explains the importance of having a product people want and the infrastructure to deliver it.
- Beau shares a personal experience with a knife that broke upon delivery, illustrating the importance of a product's quality.
- He talks about showcasing the idea that cost doesn't always equate to value in certain industries.
- Beau mentions a beverage company that stopped delivering a product with poor ingredients, showing responsibility.
- He recalls trying to order a controversial book at a bookstore and facing resistance due to its content.
- Beau contrasts how certain actions are considered normal, while others, like social media censorship, spark controversy.
- He questions the framing of social media giants reducing the volume of an article criticizing Biden as censorship.
- Beau explains that free speech doesn't guarantee a platform or promotion for every idea and stresses individual responsibility.
- He concludes by stating that bad ideas and information are not entitled to promotion or support.

### Quotes

- "Nobody owes bad ideas, bad information, a social safety net."
- "The market decided we don't want them."

### Oneliner

Beau introduces three stories to challenge how things are framed, discussing responsibility, censorship, and the market deciding what ideas are promoted.

### Audience

Consumers, critical thinkers.

### On-the-ground actions from transcript

- Challenge the framing of information and narratives (implied).
- Support responsible actions by companies and individuals (implied).
- Promote critical thinking and individual responsibility (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how narratives are framed and the importance of individual responsibility in promoting ideas.

### Tags

#Framing #Responsibility #Censorship #MarketDecides #CriticalThinking


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
three seemingly unconnected stories,
a newspaper article, a recent newspaper article,
and hopefully shed some light
on whether or not things are
the way they're being framed by some,
because they may not be.
But before we get into the stories,
Let's get some basic concepts set.
Why does Coca-Cola rule the world?
Why can you get Coca-Cola pretty much anywhere?
Now, you can talk about their business practices,
but that's not actually what I'm getting at.
I'm getting at the fact that they have a product that
people want and the infrastructure to get it there.
It's really that simple when you really kind of break it
down.
I don't know if y'all know this,
but I used to review equipment,
outdoor equipment, stuff like that.
Had a knife sent to me once.
And when I opened the box, it was broke.
That's a really bad sign.
And if it can't stand up to shipping,
it's not gonna stand up to outdoor use.
Not for real.
Couple months later, I wanted to do
something kind of showcasing.
The idea that cost doesn't always equate to value in this particular industry because a lot of times people buy this
stuff for emergencies, they never actually use it.  They put it in a bag and it stays there.
So I wanted to showcase that, so I wanted that particular knife because I had heard from other people that it fell apart
really easily.
really easily. Looked online, called around, could not find a vendor selling it.
The only place I could get it was from the manufacturer and they had recently
stopped producing it because the vendors wouldn't carry it because it fell apart
really easily. Makes sense. There was a beverage company, not Coca-Cola, but a
lot like them. They had their own trucks and everything and they had a deal with
a company that made energy drinks. They deliver their products to the stores and
all that stuff. They found out that the energy drink company was using, well
let's just say, not great ingredients. So the beverage company did the
responsible thing. Stop delivering it. Makes sense. Had to write a paper once
and in order to write this paper I had to read a pretty controversial book.
Somebody's Diaries. I walk into this bookstore and I know it's not going to
be on the shelves but I figured they'd order it for me. I walk up to the desk
I was going to ask them, and if you're familiar, if you can guess what book I'm talking about,
me looking like me asking for that book, definitely got some side eyes, and I asked them to order it,
and they were like, no, and it wasn't even just like, you know, no, we're not going to order that.
It was kind of like, no, get out, you know, fair enough.
They don't want to be associated with it, which makes sense because it's kind of garbage.
of that's normal. All of that is normal. Nobody that is like a right-wing, you
know, the market decides type of person is gonna have a problem with any of that.
That's normal. Nobody's gonna complain, doesn't send up any red flags. It's
normal. It's always entertaining to me to watch the the market will decide people
turn into straight commies the second the conversation turns to social media
or their ideas. With everything else oh the market decides but when it comes to
to their poor ideas, well, they want welfare, they want somebody
to provide the infrastructure for them, they want somebody to
promote their bad ideas, that article that had the volume
turned down on it by a couple of social media giants, people are
framing that as if it's some form of censorship, and that
They're stopping criticism of Biden.
Go to either one of those platforms and criticize Biden, or better yet, look for
articles that are far more critical of Biden than that one.
That's probably not it.
That didn't even make sense.
It doesn't, it doesn't hold up the idea that they're censoring it for that reason.
They even allow articles talking about that article on it.
Just not that article.
It's interesting.
Maybe it has bad ingredients.
Maybe it falls apart real easily.
Maybe it's garbage.
Maybe they were warned that one of the people involved in getting this information had been
targeted for a disinformation campaign because the White House knew why
wouldn't these social media giants know there's something specific to that
article it's not stopping criticism of Biden now I know the idea is free
speech and all that free speech means you're allowed to say whatever you want
within the parameters of a healthy society doesn't mean that somebody has
to give you a microphone or a megaphone or put you on a stage or promote your
work on their platform. It's not what it means. Nobody has to provide the
infrastructure for your product. It's up to you.
At the end of the day, nobody owes bad ideas, bad information, a social safety net.
Nobody needs to provide welfare to bad ideas.
The market decided we don't want them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}