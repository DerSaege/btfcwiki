---
title: Let's talk about Trump, Biden, Mr. Rogers, and the town halls....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1PubsTRMtTc) |
| Published | 2020/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Both Biden and Trump held town halls at the same time, causing an uproar.
- Biden's town hall was civil but somewhat boring, with no major surprises.
- Biden claimed to have always hoped to make the criminal justice system fair, which Beau found questionable.
- Trump's town hall was chaotic, with him often avoiding direct answers and claiming not to know or remember things.
- Trump's refusal to denounce a vocal supporter group raised eyebrows, especially since the FBI labeled them a threat.
- Trump's team compared Biden's town hall to watching Mr. Rogers, which Beau interpreted positively as seeking unity.
- Beau praises Mr. Rogers for his courage in addressing social issues and contrasts his leadership style with Trump's administration.
- Beau suggests that the Trump team positioning themselves against Mr. Rogers may not resonate well with American voters.

### Quotes

- "I'm not sure positioning yourself as [Mr. Rogers'] opposition is going to play out well with the American voters."
- "They don't admit they were wrong."
- "I understand it, but y'all need to keep Mr. Rogers' name out your mouths."

### Oneliner

Beau analyzes Biden and Trump's town halls, contrasts their styles, and praises Mr. Rogers for embodying unity in challenging times.

### Audience

Voters, Political Observers

### On-the-ground actions from transcript

- Contact local representatives to advocate for criminal justice reform (implied)
- Educate others on the importance of unity and empathy in leadership (implied)

### Whats missing in summary

Insights into the impact of political town halls on public perception and the role of empathy in leadership.

### Tags

#TownHalls #Biden #Trump #MrRogers #Unity


## Transcript
Well howdy there internet people it's Beau again. So uh today we're going to talk about the town
halls. We'll talk about Biden's. We'll talk about Trump's. And then we're going to talk about Mr.
Rogers. If you're on Twitter you know why. Um if you don't know last night both candidates held
town halls. There was a big uproar because they were at the same time and all of that.
But they were both exactly what you would expect to be honest. I mean Biden's was very civil,
kind of boring to be completely honest. No big surprises. He said a couple of things that were
less than ideal. Nothing major but a few. The one that raised my eyebrows and I'm sure everybody
will have their particular moment but for me it was when he said something to the effect of
he had always hoped and worked to make the criminal justice system fair or something along
those lines. Yeah I'm old enough to know that's not true. But the rest of that statement, the rest
of that answer showed that he has definitely evolved on the topic since then. I think maybe
a better lead-in to that answer would have been that he's looking forward to fixing his mistakes,
cleaning up his mess, something along those lines. However he's a politician you know.
They don't uh they don't own their mistakes typically. They don't admit they were wrong.
That was it. I mean it was stuff like that. Nothing huge. It was Joe Biden being Joe Biden.
Like I said kind of boring to be completely honest. And then there was the Trump town hall
which is also exactly what you would expect. A complete dumpster fire. Lots of interrupting.
Lots of him not actually answering the question. Lots of him saying he didn't know or couldn't
remember or whatever. My particular favorite moment was when he was asked to condemn or denounce
a group that has been rather vocal in their support of him. And he said he didn't know
anything about them. Which I mean I get it. Again he's a politician. He doesn't want to
denounce a group that is vocal in their support of him. However as president of the United States
it would be a complete and utter failure to not know anything about a group that the FBI has
labeled a threat. Especially if your entire campaign is about law and order and keeping
the suburbs safe. How are you going to protect the country when you don't know what the threats are?
But again there's plenty to choose from when it comes to the the Trump town hall.
And that's really all I'm going to say about that because y'all are going to watch the clips.
You can pick any random Biden clip and any random Trump clip and compare them. It's pretty clear.
I want to talk about something else though because during the town halls somebody on Trump's team,
they compared the Joe Biden town hall to watching an episode of Mr. Rogers.
Good? That's not an insult. That is not an insult. I'm not going to put the person's name
out there simply because I'm certain they have already received enough backlash. I sent messages
myself. Yeah that's not an insult. I know it may seem odd or even weird from somebody on the Trump
team to see that and you know see somebody try to unite rather than divide. Somebody who's
looking for the helpers instead of looking for somebody to blame. I get it. It's odd but I would
suggest that's exactly what the country needs right now. Go back and look at the videos. I am not
overly enthusiastic about a Biden presidency but it's a good thing that the Trump team is
trying to unite. But if his opposition views him as Mr. Rogers, maybe Biden is my man. I mean I get it.
I get it from the Trump side of things. You know that cardigan wearing weakling and all of that.
However, I'd give just about anything to see President Biden dip his feet in the kiddie pool
with a representative of every demographic that the current administration has marginalized and
attacked. And if you don't get the reference, that's probably why you don't understand that
Mr. Rogers showed more courage than anybody in the entire Trump administration. See in 1969,
there was an episode where him and this other guy, they dipped their feet into a kiddie pool together
and then they shared a towel. That was it. Not a huge thing except he was black and this was at a
time even though segregation was kind of supposed to be over, it wasn't really happening. And he
addressed it by leading, setting the example. And that's just one thing. The entire show
softly addressed social issues and he led the country more so than President Trump ever has.
I get that the Trump administration doesn't actually know anything about the ideas of America,
but Mr. Rogers is kind of an American icon. I'm not sure positioning yourself as his opposition
is going to play out well with the American voters. And I get it. The Trump administration
has sullied the Constitution, the basic principles of this country, the foundational elements of
democracy. I understand it, but y'all need to keep Mr. Rogers' name out your mouths.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}