---
title: Let's talk about why Trump's decisions and feedback loops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b-JE25WXH6s) |
| Published | 2020/10/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analogy of radar stations near schools and Trump's decision-making.
- Feedback loops: action, measurement, emotional resonance, options.
- Trump's characteristics: huge ego, poor consumer of information.
- Trump's feedback loop: influenced by extreme viewpoints.
- Explanation of Trump's decisions through feedback loops.
- Example of Trump's response to debate polls.
- Food boxes disliked by many but praised by right-wing media.
- Trump's response to law and order discourse.
- Influence of feedback loop on Trump's campaign.
- Downplaying of public health issue by Trump and his followers.
- Theory: Trump easily manipulated due to feedback loop.
- Importance of being aware of feedback loops in personal life.

### Quotes

- "Are the feedback loops that you are in, are they encouraging growth and good behavior?"
- "He's just easily manipulated."
- "Take note of and watch out for in your life."
- "The sign of an enlightened mind is the ability to entertain an idea without accepting it."
- "If it's just a thought, y'all have a good day."

### Oneliner

Beau explains how Trump's decisions are influenced by feedback loops, showcasing the impact of extreme viewpoints and emotional resonance.

### Audience

Voters, citizens, activists

### On-the-ground actions from transcript

- Monitor your sources of information and ensure they encourage growth and good behavior (suggested)
- Stay informed about current events from diverse and reliable sources (implied)
- Encourage critical thinking and awareness of feedback loops in your community (implied)

### Whats missing in summary

Detailed analysis of how feedback loops can shape decision-making processes.

### Tags

#Trump #DecisionMaking #FeedbackLoops #MediaInfluence #CriticalThinking


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about
why Trump makes poor decisions.
Have you ever been driving near a school
and seen one of those radar stations
that tells you how fast you're going?
There's no cop near it.
It's just the radar and the flashing light
telling you how fast you're going.
What do you do when you see one of those?
Almost everybody slows down.
Why?
It's not new information.
It's on your dashboard.
Why do you slow down?
It's feedback loop.
The information was presented to you in a way that resonated
emotionally.
Feedback loop has four stages.
The action, that action gets measured, stored, gets presented back in a way that emotionally
resonates, and it highlights options, in this case, slow down.
It can be used to create good behavior.
Can also be used to create negative behavior.
What do we know about Trump?
I mean, like really, what do we know?
you think Trump, you think ego. Man has a huge ego, right? Name in gold everywhere.
He's a stable genius with infinite wisdom. He's infallible. Huge ego. What else do
we know? He's a poor consumer of information. People who watch this
channel are gonna say, yeah, he's retweeting Fox and other far-right
outlets all the time. Yeah, that's true. That is true. But his intelligence
briefings. We've had people talk about how he kind of drifts off at them. Doesn't
always attend them. He's a poor consumer of information. And then, yeah, he does
have that diet of far-right internet and TV news. What if the reason he makes bad
decisions is because there's two feedback loops colliding. We've talked
about it on this channel before how a lot of pundits try to out sensationalize
each other and it just gets wilder and wilder in their defense of Trump or in
whatever it is that they're covering they try to come up with a take that's
It's going to get those clicks, going to get those views, going to sensationalize more
and more and more.
It's a cycle.
That's one feedback loop that exists.
Given what we know about President Trump, that may play a very large part in his decision-making.
He undertakes an action.
That action is measured and stored by the news outlet, by the pundit.
It's relayed back to him because he watches it.
Because these people are generally trying to show how loyal they are to the president
and cheerlead him on, they take positions that are more extreme than the president in
In an attempt to make him seem less extreme, and they highlight an option.
They give him that option to be even more extreme, to take it a step further.
They're reinforcing the idea that everything he's doing is right, so he continues along
that path.
That's his feedback loop.
his feedback loop and it explains so much. It explains why after that debate, he said
he wasn't going to change anything because the polls showed that he won. No, they didn't.
The only polls that showed him winning were the ones where, like, vote on the internet
polls. Not scientific in the least, but that's the information he consumes. And we don't
know that he knows the difference, to be honest.
We have no clue that he's aware that those polls are not accurate.
So he thinks he did a good job.
He thinks he won.
That's why I didn't want any changes to the debate.
Because in his feedback loop, he won.
Take it to some other stuff.
the food boxes that the government is sending out.
These things are pretty much despised by everybody.
However, because his cheerleaders in the right-wing media have heralded it as this great thing,
he actually put a letter in them saying that he was responsible for them.
emailed a letter to voters saying he was responsible for something that they don't like.
But he doesn't know this because he's in that feedback loop.
That's what he sees.
Carries through through other stuff.
Shortly after a majority of Americans decided seeing a station alight was at least somewhat
justified the president started talking about law and order because in the
right-wing media it was cheered on even though the majority of Americans do not
like the way law enforcement is behaving right now. As this feedback loop goes on
It actually ends up working against his campaign because he ends up catering to a smaller and
smaller, a more extreme group of people.
It's probably why he's dropping in the polls the way he is.
Let's talk about one other thing.
The president initially downplayed the public health thing, and then his cheerleaders took
it even further and he followed suit.
Part of the reason the president is in the state that he is in is because of this feedback
loop, because they fed into it. They rejected everybody who knows what they're talking
about to cheer him on. It put him where he's at right now, because it reinforced those
negative decisions he was making. It's an interesting theory. I don't know that it's
true. I don't. I don't know that it's accurate, really, but the sign of an
enlightened mind is the ability to entertain an idea without accepting it.
With this in mind, it may be that Trump isn't evil. He's just easily manipulated.
I mean, it doesn't change what needs to happen in November. I would suggest that
an easily manipulated commander in chief is probably not a good thing, but it's
definitely a something to take note of and it's also something to watch out for
in your life. Are the feedback loops that you are in, are they encouraging growth
and good behavior? Making yourself better? Expanding who you are? Or are they
negative feedback loops? Are they feeding you praise for things you shouldn't be
doing? That you know you shouldn't be doing more than likely, but the loop that
you're in is cheering you on.
Anyway, if it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}