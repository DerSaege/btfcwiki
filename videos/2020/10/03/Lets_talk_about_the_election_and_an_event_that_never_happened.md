---
title: Let's talk about the election and an event that never happened....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ESHxkY9IdpE) |
| Published | 2020/10/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- In an alternate historical timeline set in 1977 Western Europe, NATO faces a crisis where members of Parliament loyal to the Soviets are elected.
- NATO's dilemma is preventing these members from being seated without undermining democracy.
- The Soviets threaten military intervention if their members are not seated, leaving NATO paralyzed.
- Individuals in a stay-behind organization, part of a Cold War network, are trained to resist potential Soviet occupation.
- Despite the crisis never materializing, these organizations existed across Europe and engaged in clandestine activities.
- Beau mentions the importance of waiting for directives rather than reacting impulsively to situations.
- He contrasts this patient approach with the modern trend of instant reactions fueled by sensationalism and the 24-hour news cycle.
- Beau encourages society to adopt a more thoughtful and measured response to events rather than succumbing to emotional reactions.

### Quotes

- "They were trained not to respond at every provocation, to wait for that code word, to respond rather than react."
- "The reality is we have turned into a country that thrives on the 24-hour news cycle and hot takes."
- "In most cases, it's better to wait to see what happens when the dust settles and respond rather than react."

### Oneliner

In an alternate historical crisis, Beau explains the importance of waiting for directives and responding thoughtfully rather than reacting impulsively in today's world dominated by sensationalism and instant reactions.

### Audience

Global citizens

### On-the-ground actions from transcript

- Establish a network for preparedness and response in case of crises (implied)
- Train individuals to respond thoughtfully rather than react impulsively to events (implied)

### Whats missing in summary

The full transcript provides a deep reflection on historical crises, the importance of patience, and the dangers of impulsive reactions in today's society.

### Tags

#HistoricalCrisis #ColdWar #ResponseOverReaction #Sensationalism #CommunityPreparedness


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about an event that never occurred.
We're going to talk about a history lesson that's never been given.
We're going to go through an alternate historical timeline.
We're going to do this because there's a lot of apprehension right now.
And I think hearing this may set some people's mind at ease.
Let them relax a little bit.
So to set the scene, we're going to say it's 1977.
You live in Western Europe.
NATO country.
Three days ago there was an election for Parliament.
For whatever reason, just a little more than half of the members of Parliament that were
elected, well they're loyal to the Soviets.
NATO doesn't know what to do.
They want to stop these people from being seated.
Because if they are seated, they may withdraw from NATO and join Warsaw Pact.
But they can't really do that politically.
The optics on that are really bad.
They can't move in to a representative democracy and stop democracy.
Doesn't make sense.
And that's what all their propaganda is geared around.
So they're unsure of what to do.
The Soviets are definitely watching, waiting to see how NATO responds.
And when NATO blinks, when they freeze, well it opens up the door for the Soviets.
And two days ago, the Soviets said that if those members of Parliament aren't seated,
they'll send in troops to enforce the will of the people.
NATO is at a loss.
They're posturing.
They can't really do anything.
It's at this point that the people of your country, a lot of them, start leaving.
They're trying to get out.
But not you.
Somebody has to stay and resist the onslaught, right?
If not you, who?
Truth be told, you knew it was going to be you if this happened.
But you just wait.
This morning, when you look out the window, there are T-72s rolling down the streets.
Soviet troops walking alongside of them.
The reason you knew that you were going to stay and you knew it was going to be you is
because it's your job.
Literally.
You're part of a stay-behind organization, a stay-behind network.
These things existed all over Europe during the Cold War.
There were groups of people who were sitting around waiting for this eventuality, for a
scenario similar to this.
There were certain things that could bring them into action.
And this was one of them.
Never happened.
But they were there.
So you are a highly trained person.
And the opposition's right outside.
What do you do?
You go out there and take them on, right?
The world's upside down.
You've got to do something.
No, you take the bullets out your gun, you're not doing anything.
You're sitting around, you're watching, you're waiting for the world powers to decide what's
going to happen.
You're waiting for a code word.
While you're waiting, for probably weeks, you're watching the opposition, watching them
set up checkpoints, do patrols, figure out what they're doing, see which ones are scared
because you know they're dangerous, which ones are showing a lot of bravado, because
they're probably careless.
And this is all information you might need, depending on what others decide.
This never happened.
But these organizations existed all over Europe.
The reason we know about them, the reason they're public, is because some of them, particularly
in Italy, well, they waited around so long, they got a little bored.
And let's just say they engaged in some extracurricular activities that made the news.
That's when the existence of these things broke.
I wanted to talk about this for two reasons.
The first is, there's a lot of apprehension about what's going to happen after the election.
I just want to remind people that the United States government has continuity of government
plans for other countries and has had them for decades.
They certainly have them for the United States.
Most elected officials don't even know they exist, but they do.
The other reason I wanted to talk about it is because that highly trained person who
looked out their window, they waited.
They waited.
They were trained not to respond at every provocation, to wait for that code word, to
respond rather than react.
It's something that we as a society might want to take to heart.
It's a good lesson.
The reality is we have turned into a country that thrives on the 24-hour news cycle and
hot takes.
People saying a lot, but really saying nothing.
Sensationalism appeals to emotion.
Things that evoke a reaction.
In most cases, it's better to wait to see what happens when the dust settles and respond
rather than react.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}