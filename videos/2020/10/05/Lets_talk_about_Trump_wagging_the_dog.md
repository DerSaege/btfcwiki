---
title: Let's talk about Trump wagging the dog....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fDr3E4b9K4k) |
| Published | 2020/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he doesn't buy into the idea that Trump manufactured a situation.
- Questions the effectiveness of using this as a talking point.
- Suggests that focusing on Trump's inability to protect the country due to potential health effects might be more effective.
- Points out that regardless of the truth, what matters is that it was on TV and many believe it.
- Advocates for using Trump's story against him rather than pointing out his past lies.
- Emphasizes the importance of reaching undecided voters and how certain talking points can sway them.
- Urges to focus on undermining Trump's main lines of attack against Biden.

### Quotes

- "It's because I think it's a bad talking point."
- "It was on TV. We all saw it."
- "I don't think it's an effective talking point for the people who are undecided at this point."
- "I think that is a more effective line."
- "I think that's a more effective talking point."

### Oneliner

Beau explains why the claim that Trump manufactured a situation isn't a good talking point, suggesting focusing on Trump's inability to protect the country due to potential health effects may be more effective, especially for reaching undecided voters.

### Audience

Voters, political activists

### On-the-ground actions from transcript

- Roll with effective talking points (suggested)
- Use Trump's story against him (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the ineffectiveness of a particular talking point regarding Trump, and suggests alternative strategies for reaching undecided voters and countering Trump's attacks against Biden.

### Tags

#Politics #Election #Trump #Biden #TalkingPoints


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about why I'm not rolling with the talking point that maybe
he manufactured this whole thing.
That maybe Trump just made it all up.
And why I don't think you should either.
Had a number of people message me and say that they were disappointed that I didn't
really entertain this idea.
Now I did point out that there were some people who were skeptical of his claims.
But I guess I didn't go far enough into it.
It's because I think it's a bad talking point.
Because I think it is a bad talking point.
I'm going to explain why, I'm going to give you a number of reasons, the most important
will be at the end.
First, who are you trying to reach with this statement?
People who don't know he's a liar.
If they know he's a liar, they're probably not voting for him.
Anybody who would believe this, they're already against him.
So it's wasted breath.
Aside from that, what's your evidence?
Prove it.
You can't, it boils down to timing.
He had just had a scandal and then this happens.
Name a time during Trump's administration where he hadn't just had a scandal.
Means nothing in and of itself.
I don't think it's an effective talking point for the people who are undecided at this point.
I don't think it's going to reach anybody.
Aside from that, I would point out that many people reference the movie Wag the Dog in
their message more than I would imagine.
You're missing the most important line of that movie.
I saw it on TV.
It's over.
The demographic that is still undecided, odds are that's where they get their news.
That is where they gather their information.
And they saw it on the news.
They're going to believe it.
They're going to believe it.
It might be more effective to roll with it.
I certainly believe it is because Trump's two main lines of attack on the Biden campaign
are one, slow Joe.
You know, he's old and decrepit and he can't think straight.
Two is law and order.
The idea that Trump is going to protect the country.
Did you know that people who catch this, a large number of them, and there's studies
on this, have long-term neurological side effects?
They can't think straight.
They can't focus.
They feel like they're in a fog.
I don't know if somebody who might have that, I don't know if they should be president.
We need somebody who can think straight right now.
Turns his own line of attack back on him.
Undermines it.
Seems more effective to me.
Law and order.
He's going to protect the country.
He couldn't protect his own house with an army because he can't accurately assess risk.
And if he can't accurately assess risk, how can he protect the United States?
He's in that situation because he couldn't accurately assess risk.
How's he going to protect the suburbs?
I think that is a more effective line.
I think that's a more effective talking point.
Whether or not this really happened, it doesn't matter.
It was on TV.
We all saw it.
Those who are undecided at this moment, they believe it.
A whole lot of people have doubts.
A whole lot.
But those two talking points, the fact that it cuts into his main lines of attack against
Biden are probably more of a help to Biden than whatever sympathy bump or distraction
is a help to Trump.
So I would just roll with it.
I would just roll with it.
I would suggest that those are far more effective than pointing out that Trump has lied for
the 265,917th time.
If all of his other dishonesties have not impacted that particular voter, they're not
going to do it now, especially when you can't prove it.
It's just a claim.
I would focus on using his story against him.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}