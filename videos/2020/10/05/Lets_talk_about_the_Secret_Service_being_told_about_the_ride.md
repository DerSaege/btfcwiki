---
title: Let's talk about the Secret Service being told about the ride....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=i9Xa5xm0A0Y) |
| Published | 2020/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is asked to accompany a positive press member in a sealed vehicle for a quick ride around the block.
- The decision is dictated by the president, despite the unnecessary risk involved.
- Beau questions the need for such a risk, especially considering the positive status of the press member.
- Despite concerns, Beau is reminded that the president's wishes must be followed unquestioningly.
- Beau references a Tennyson quote about unnecessary risks not ending well, which is dismissed.
- Precautions are promised, but Beau doubts their effectiveness given the current situation in front of Walter Reed.
- Beau points out that instead of risky stunts, the focus should be on protecting the president's safety and dignity.
- Beau questions the president's ability to understand and mitigate risks, given his track record.
- The president's irresponsibility and inability to protect those around him are emphasized by Beau.
- Beau ends by urging a national discourse on whether the country can afford another term of such recklessness.

### Quotes

- "He put lives at risk for a propaganda stunt, to waive it people."
- "He can't protect his staff. He can't protect his house with an army of advisors and experts."
- "We have to decide whether or not we, as a country, are willing to accept this kind of recklessness for another four years."

### Oneliner

Beau questions the president's risky decisions and urges a national debate on his irresponsibility and ability to protect the country.

### Audience

Citizens, Voters

### On-the-ground actions from transcript

- Start a national discourse on the president's actions and their implications (implied)
- Engage in informed political debates and decision-making (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the risks and implications of the president's decisions, prompting reflection on leadership qualities and public safety.

### Tags

#Risk #PoliticalLeadership #Responsibility #NationalDebate #PublicSafety


## Transcript
Well howdy there secret service people.
Looking at the roster to see who's up and it looks like it's Bo again.
No, that ain't what the roster says. I'm off.
It's just a quick run.
I'm not supposed to be here today.
Yeah, well,
take a look around. We're kind of short-handed.
Fine, what is it?
Just a quick greet and grip.
Cool, who?
The press.
Is there a problem?
Well, I mean, yeah, kinda.
Isn't he positive?
Yes.
And you want me...
You want me to get in a sealed vehicle with him?
Yeah, look, it's just a lap around the block.
Doesn't that seem like a completely unnecessary risk?
Well, the president wants to do it, so we gotta do it.
Plus, not yours to reason why.
Tennyson quote. That's nice.
You do know that that quote is about a
completely unnecessary ride and most of them don't make it back, right?
Oh, I'm so sorry.
Is Alfred Tennyson signing your paychecks?
Look, precautions will be taken.
If precautions were taken, we wouldn't be standing in front of Walter Reed right now.
What?
Nothing. I didn't say anything.
Look, he wants to do it.
Instead of staging press events?
Shouldn't you be worrying about your job?
My job is to provide for the safety, security, and dignity of the president of the United States.
Yeah, well, how's protecting his dignity been working out?
I'm sorry, what?
I didn't say anything. Look,
I brought you a mask.
What, you need a gown too?
The filter's expired and I have a beard. This isn't gonna seal.
Suck it up.
That's kinda what I'm worried about.
Just do it.
The president of the United States put lives at risk.
He put lives at risk for a propaganda stunt, to waive it people.
He put the lives
of people who are willing to take a bullet for him at risk.
They're willing to take a bullet for him
and he won't take a nap for them.
The president has shown
over the last four years painstakingly
that he is completely incapable of assessing risk.
If he can't assess risk, how can he mitigate it?
That's his big campaign promise, right?
He's gonna protect everybody.
He can't protect his staff. He can't protect his house with an army of advisors and experts.
How is he gonna protect the country or the suburbs?
At the end of the day,
the president's irresponsibility that he clearly shows at every opportunity
needs to be a topic of national discussion.
We have to decide whether or not we, as a country,
are willing to accept this kind of
recklessness
for another four years.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}