---
title: Let's talk about Trump in the last few weeks of the 2020 election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mXnFhNlha-E) |
| Published | 2020/10/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the 2020 election and what to expect from the incumbent in the next few weeks.
- Describes the incumbent, Donnie, as a class bully with a rich background and lack of accomplishments.
- Mentions Donnie's lack of participation in class projects and failure to implement policies.
- States that it's too late for Donnie to propose policies or develop leadership skills.
- Comments on Donnie's endorsements, including support from the school resource officer and the school rival.
- Views the upcoming election as a referendum on Donnie's administration.
- Speculates on possible desperate tactics Donnie might employ, such as pushing a disliked student against lockers or spreading rumors.
- Anticipates Donnie resorting to making baseless claims due to his low chances of winning.
- Advises preparing for Donnie's potential unfounded claims and reminding others of his history of fabrication.

### Quotes

- "It's a referendum on Donnie's administration or lack thereof."
- "He's probably going to rely on what's always worked for him, making stuff up."
- "We should be ready for some explosive claims that are completely baseless and unfounded."

### Oneliner

Beau covers the 2020 election dynamics at Anytown High School, predicting desperate tactics from the incumbent and advising readiness for baseless claims. 

### Audience

Students, Voters, School Community

### On-the-ground actions from transcript

- Remind fellow students of the incumbent's history of fabrication (implied)
- Stay vigilant against potential baseless claims and misinformation (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the dynamics of an election scenario at a high school, shedding light on potential tactics and challenges faced by the incumbent.

### Tags

#Election #HighSchool #Incumbent #DesperateTactics #BaselessClaims


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about the 2020 election, what we can expect to see over the
next few weeks from the incumbent.
And to cover it properly, we're here at Anytown High School to cover the student body election.
Okay, so you have the incumbent, Donnie.
Class bully, rich kid whose daddy always bailed him out of trouble.
He's the incumbent, he's partnered up with the one kid who like audibly prays in the
hallway and even most people of his own faith don't want to be around him.
Don't know how that happened.
But they're working together and they're trying to get another term.
They're trying to stay as class president and vice president.
Now if you talk to the other schools in the school district, those that we've worked with
and stuff like that, you talk to their class presidents, they kind of say that these guys
didn't participate in any class projects and didn't pull their weight when it comes to
the school district at large.
When it comes to inside the school, he really didn't put forth any policies.
He tried to get a new fence put up on the south field, but it's already falling down
and didn't get it finished.
So there's that.
There was a time he got everybody sick and tried to blame it on the exchange student.
He doesn't have any policy really.
So it's probably a little late in the game to expect that.
We're not going to see any policy proposals come forth from him.
Nobody would believe him even if he put any forward.
So that's off the table.
It's also too late in the game for him to try to be a statesman and develop some kind
of leadership skills.
So we're probably not going to see that either.
So he didn't do anything for the school outside of the school, didn't do anything for the
school inside of the school.
What about endorsements?
Well the school resource officer likes him.
The one kid that's not allowed to come here anymore, he's tall, got banned, he endorsed
him.
Our school rival, they endorsed him.
We've had a rivalry going back since like 1945.
They're very supportive of him.
But I don't know that that's going to be enough.
I don't know that's going to be enough to carry him through to another term because
that's really what this is.
It's a referendum on Donnie's administration or lack thereof.
So what can we expect to see over the next three weeks?
What can he really do?
You've got the wild card option where he walks out in the hallway, finds some little kid
nobody likes, maybe E&R missing, and pushes him up against the lockers.
Get everybody to rally around him, show how big and bad he is.
That's one option, but it's a little late for that.
He should have done that a couple weeks ago if he's going to do it.
So I don't know that we're going to see that.
Now he might send his little bully buddies out on election day to remind everybody who
they're supposed to vote for.
He'll probably do that.
He'll probably do that.
But more than likely, he's probably going to rely on what's always worked for him, making
stuff up.
He's probably going to start some rumors.
Maybe not about his political opposition themselves, maybe not about them, but about people connected
to them.
Maybe there was this girl that they used to hang around.
She sent some text messages, and my buddy Billy has them.
And we're going to release them right before the election.
Really embarrass her.
He's not running against Hillary or her text messages.
So I don't know that that's going to be really relevant.
I don't think anybody's going to care about that.
Now he may just attempt to make stuff up.
We have to consider that a pretty high probability because he does it every day.
So with the polls showing what they are showing, and he's currently got like a 9% chance of
winning I guess, he's probably going to get pretty desperate.
And he's probably just going to start lashing out.
That's what all people like this do.
So we should be ready for some explosive claims that are completely baseless and unfounded.
I would imagine they'd show up sometime in the next two weeks.
I would just be ready to remind your fellow students about this individual's long, long
history of just making stuff up.
And at the end of the day, I honestly can't think of anything that he could make up about
his political opposition that is worse than the stuff that he's done.
So that's where we're at here at Anytown High School.
We'll find out what happens in a couple weeks.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}