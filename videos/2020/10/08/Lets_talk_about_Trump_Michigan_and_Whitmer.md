---
title: Let's talk about Trump, Michigan, and Whitmer....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wH7ZYj2k3D4) |
| Published | 2020/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of discussing Michigan and what it foreshadows.
- Criticizes the divisive and extreme rhetoric used by certain individuals in the country to energize their base.
- Points out the refusal of the president and vice president to commit to a peaceful transfer of power.
- Emphasizes the significance of committing to a peaceful transfer of power beyond legal battles.
- Raises concerns about the potential consequences of not committing to a peaceful transfer of power, suggesting it sends a message that force is acceptable.
- References the attempted kidnapping of a governor as an outcome of such rhetoric.
- Stresses the need for the president to condemn such actions and commit to unity for the country's sake.
- Asserts that the current president has escalated division in America for personal gain.
- Urges for a peaceful transfer of power to prevent further incidents and unite the country.
- Calls for a landslide defeat for the current president in the upcoming election.

### Quotes

- "Force is on the table. The attempted snatch of a governor. To spark it."
- "He has brought it to its knees."
- "It's not a game. It's not a movie."

### Oneliner

Beau stresses the critical need for the president to commit to a peaceful transfer of power to prevent further division and potential violence in the country.

### Audience

American voters

### On-the-ground actions from transcript

- Contact local community organizations to organize peaceful unity events (implied)
- Join advocacy groups promoting peaceful political discourse (implied)
- Urge elected officials to publicly commit to a peaceful transfer of power (implied)
  
### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of divisive rhetoric and the urgent need for a commitment to a peaceful transfer of power to avoid further turmoil and division in the country.

### Tags

#Michigan #DivisiveRhetoric #PeacefulTransferOfPower #Election #Unity #PoliticalResponsibility


## Transcript
Well howdy there internet people, it's Beau again.
So I guess we're gonna talk about Michigan, right?
Kinda have to, seems important.
So we're gonna do that, we're gonna talk about
what almost happened, why it almost happened,
and what it foreshadows.
See, there are people in this country
who are attempting to energize their base,
and that's a thing, it's normal.
It occurs every time there's an election.
The problem is they're using rhetoric
that is so divisive, so extreme in nature,
that it's energizing people to do something else.
And they've subjected the populace
to this rhetoric for four years.
For four years.
This is what it leads to.
This is what it leads to.
And in my opinion, there are two people
that hold a whole lot more responsibility
for this than others.
The president and the vice president.
Because they refuse to commit
to a peaceful transfer of power.
See, when the average American hears that,
I think that they're picturing that
cool, congratulatory phone call
from the loser to the winner.
And that committing to a peaceful transfer of power
means that they're not gonna go through legal battles.
That's not the question.
The question is, will you commit
to a peaceful transfer of power?
And the answer is no.
They won't commit to it, so the answer is no.
Avoiding the question doesn't mean anything.
The answer is no.
Because they won't commit to it.
That sends a message to go along
with all of that rhetoric.
Force is on the table.
Here you go.
The attempted snatch of a governor.
To spark it.
To get that show on the road.
And there's a lot of people who think that's a good idea.
Yeah, okay.
See, in the US, we've been insulated from it for so long.
To a lot of people, that's just something
that happens in movies,
or something that happens over there.
It's not real.
But see, I think a lot of Americans remember
when it happened to contractors over there.
Remember the footage?
See, that's a pretty plausible outcome from these events.
That's something that could have occurred.
How would you feel when it's an American on American soil,
on their knees, bag over their head, begging,
and it's being done to them by another American?
What's the reaction gonna be?
It's a very real possibility.
And, yeah, they failed this time, but they're allowed to.
They're allowed to.
They're allowed to hit and miss.
Their opposition, those trying to stop them,
they have to hit every single time.
Because it only takes once.
It only takes once to cause things to spin out of control,
to cause things to hit that point and overturn.
Because once it starts, it's really hard to stop.
Because the back and forth starts,
and it's small, little groups.
We need to take a step back.
And, sadly, the person who can accomplish
that step back the fastest is somebody
who has shown for years that they don't actually
have the ability to lead.
They don't even understand that their statements
are the cause of this.
Yeah, that rhetoric started before this administration,
but they certainly ratcheted it up.
Trump ratcheted it up, pitted American against American
for his own personal gain.
And it's gonna be him.
If we want to walk this back in the next 30 days,
and I strongly think it should be, it's gotta be him.
He has to come out and condemn this
and commit to a peaceful transfer of power.
Because if he doesn't, if he just comes out
and condemns it, it doesn't mean anything.
Because they'll take that as, oh, well,
he had to say that for political reasons,
but you notice he says force is still on the table.
He has to commit to a peaceful transfer of power.
He has to own this.
Try to unite the country instead of divide.
He can redeem his whole presidency by doing this.
Because right now, he's set the stage
with all of this rhetoric of make America great,
apparently by undermining all of America's core principles,
everything that has helped this country.
It's gotta be him.
Sadly, he's a man who has spent his entire administration
defending the indefensible
and refusing to condemn the things
that need to be condemned.
He has to commit to a peaceful transfer of power.
Or we may have another incident like this.
We may have something else happen like this.
And they might get it right.
They might succeed.
Nobody wants that.
Nobody who's actually familiar with it wants that.
And it's really hard to stop once it starts.
The President of the United States has to come out
and commit to a peaceful transfer of power.
It's not a game. It's not a movie.
That's where we're at.
And where we're headed is into some very dark water.
If you watch this channel long enough,
you know I am not a fan of electoral politics.
Voting is the least effective form of civic engagement.
Trump has to lose in a landslide.
I am not a partisan person.
I'm not a Democrat.
I have never said that a president
could be the downfall of the country.
I do not know if the US will make it
if the President is reelected,
because he doesn't have the ability to clean up
the mess he has created through his rhetoric.
It is incredibly important that Trump lose.
Big.
And if you are part of the Trump administration,
you have any influence,
he has to commit to a peaceful transfer of power.
He has to tone this down, and that's the way to do it.
Send the message that force is off the table,
because the message he is sending by refusing to commit
to a peaceful transfer of power
is that using force to seize or retain power is okay.
People hear what he's not saying.
This president hasn't made America great.
He has brought it to its knees.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}