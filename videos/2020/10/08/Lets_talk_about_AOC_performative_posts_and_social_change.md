---
title: Let's talk about AOC, performative posts, and social change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bsOEH7vXH3A) |
| Published | 2020/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into a social media post by AOC that draws comparisons between the Panthers' food distribution network and a project AOC was involved in.
- He encourages people to look into the Black Panthers beyond the media's portrayal, noting their radical nature and effective community networks.
- Critiques of AOC's post revolved around its phrasing, framing, and being labeled as performative activism.
- Beau argues that the debate over the most effective method of social change is ongoing and complex, with no definitive answer.
- He suggests that performative posts, even if criticized, play a valuable role in drawing attention to social issues and making them socially acceptable.
- Changing societal thinking is key to societal change, and performative posts can contribute to this shift by making causes desirable and attracting support.
- Beau acknowledges that not everyone can directly participate in activism but sharing social media posts can still contribute to social change.
- By exposing thousands to the idea of solving local issues through community networks, AOC's post may have inspired direct engagement in the future.
- Beau stresses the importance of a diversity of tactics in creating social change and the role of performative actions in shaping societal thought.
- He concludes by underlining the significance of social media as a powerful tool for changing societal perspectives and the necessity of utilizing it for positive change.

### Quotes

- "If you want to change society, you have to change the way people think."
- "Those posts, performative or not, are incredibly valuable because our goal is to change society."
- "Performative posts are incredibly valuable."
- "Our goal is to change society. We have to change the way people think."
- "Social media is an incredibly, incredibly valuable tool."

### Oneliner

Beau explains the value of performative activism in shaping societal thought and sparking social change through exposure to new ideas and community networks.

### Audience

Social Media Users

### On-the-ground actions from transcript

- Share social media posts about community initiatives and social issues to raise awareness and shape societal perspectives (implied).
- Encourage community engagement and involvement in local projects to address social issues and create positive change (implied).

### Whats missing in summary

The full transcript provides a nuanced exploration of the role of performative activism in shaping societal attitudes and driving social change, advocating for a diversity of tactics and the power of community networks in addressing local issues.

### Tags

#PerformativeActivism #SocialChange #CommunityNetworks #BlackPanthers #SocietalThought


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about an AOC tweet.
I guess it started on Instagram.
We're gonna talk about a social media post.
We're gonna do this because some of my favorite things
collided in it, community networks, logistics,
and the discussion over what the most effective method
of social change is.
This happened because there were some critiques
about the post. Okay, so let me set the scene here. It was a series of photographs that
drew a comparison between the Panthers and their food distribution network and a project
that AOC was involved in. It's what it was. Let me stop here. If you do not know a lot
about the Panthers, if your image of them is shaped by the media and movies definitely
look into them.
Your view of them may not be accurate.
In some ways they are probably far, far more radical than what you might imagine.
And then some of the negative things that you may believe about them, you'll find out
they're not true.
If you have a community network, if you're a part of one,
the logistics involved with the Black Panthers food
distribution projects, that is something to aspire to.
It is amazing.
So definitely look into it.
There's a whole bunch of reasons to do this.
OK, so back to the post.
Some of the critiques were basically
that it wasn't phrased right.
Some of it was that it wasn't framed
as radically as the Panthers deserve.
But the one I want to talk about is that it was just
performative.
It was worthless.
It was just a social media post.
Performative.
This is a discussion that happens a lot, this debate
over what the most effective way of creating social
change is.
The reality is nobody knows.
Nobody actually has an answer to this.
If anybody did, we wouldn't have any problems in the world.
If something consistently worked to create social change,
we would just constantly use that.
Doesn't work that way.
There is no right answer.
Beyond that, I would suggest that the strongest critics
of performative posts and performative activism
need them the most.
If you're one of those people who believes in directly
engaging because it gets the goods,
I'm going to lay out a scenario for you.
You and 10 of your friends, you go lock arms
in front of some factory, right?
You've blocked the gate.
You've got it stopped.
What do the security services have to do to defeat you?
If this is all you've got, wait.
Wait.
They don't have to do anything.
They just have to wait.
Because eventually, you're going to get tired.
You're going to need food.
You're going to have to go home.
you had to go to work, whatever.
And if there's no media there,
they can do whatever they want.
However, take that same scenario,
people locked arms in front of a factory
because it's doing whatever wrong,
and now add a few people showing up every hour
with their cell phones, take a picture
and put it on social media, performative people.
Spread your message.
It draws attention to your cause.
It makes it socially acceptable.
It might make it socially desirable, at which point you win.
Because then, more people show up.
Even if it is initially just to get the clout from being there or whatever, they show up.
Some of them may lock arms.
Some of them may relieve others so they can go home, get something to eat, take a break,
whatever.
performative posts are incredibly valuable. A very, very smart person once
said that if you want to change society, you have to change the law. I disagree. If
you want to change society, you have to change the way people think. And when
those social media posts make something socially desirable, it achieves that. And
And then once you change the way people think, either you don't need a law or the legislation
will follow because enough people think it, enough people will support it.
You get that broad base.
For a lot of people, those performative posts, that's all they can do, literally, or sharing
social media posts.
Some people do not have the ability for whatever reason to get out there in the trenches, but
they can do this.
can contribute to social change in their own way. I would suggest, partially because of the critiques
that her social media post may have inspired people. It showed what was possible with networks
like this, if you get the logistics to go with it. That may encourage people to do that, and those
people become those that directly engage. It's a cycle. It is a cycle. I don't see
it as worthless just exposing people to this idea and she exposed thousands to the idea
of solving the issues at the local level. I think that's incredibly valuable because
that is what creates those people who directly engage.
If we plan to win, if you want to create social change, we have to understand that a diversity
of tactics is imperative because a lot of times the performative stuff becomes the icon,
the thing everybody remembers. Picture the 1960s and 70s, the peace, love, and all of the upheaval
that went with it, right? All of that social change that was occurring. Define it. Come up with one
thing, one image, one word that can conjure up all of those images. A concert does it, right? Woodstock.
A concert brings up all of that imagery, a concert that really didn't have anything
to do with it.
It was performative, literally performers, but it captured that era.
It gave those people who aren't politically active something to associate the rest of
the imagery with, something to associate the beliefs and the cause, those are your selfie
people today.
They're more important than you might think because they shape thought and that's the
goal.
Even if you are one of those people who likes to directly engage, I would assume that at
some point you would like to not have to.
The way you do that is by changing thought.
Social media is an incredibly, incredibly valuable tool for that because people get
the likes, the hearts, whatever.
It provides a way of reinforcing the idea that this is good.
can be turned to a negative from the other side. I would suggest that it is such a powerful
tool, such a powerful weapon of social change, that we can't ignore it. We can't just let
the other side have it. Those posts, performative or not, are incredibly valuable because our
Our goal is to change society.
We have to change the way people think.
Anyway it's just a thought.
Y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}