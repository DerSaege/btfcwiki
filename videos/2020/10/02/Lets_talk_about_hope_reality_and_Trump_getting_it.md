---
title: Let's talk about hope, reality, and Trump getting it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Z2ZhQUeUPQk) |
| Published | 2020/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States tested positive for COVID-19, impacting many others.
- Expresses sincere hope for their full recovery and that they go through it without incident.
- Points out the disparity between working class Americans and those with access to top healthcare.
- Criticizes the administration for prioritizing forcing people back to work over their safety during the pandemic.
- States that the office of the Presidency is more significant than working at a regular job.
- Acknowledges that the President testing positive won't shield him from rightful criticism.
- Notes the bad timing of the President's diagnosis so close to the election.
- Emphasizes the seriousness of the situation and the need for everyone to take precautions seriously.
- Encourages basic preventive measures like washing hands, wearing masks, and staying at home.

### Quotes

- "I truly do hope they make it through this."
- "Wash your hands. Don't touch your face. Stay at home."
- "The office of the Presidency is more significant than working at Winn-Dixie."

### Oneliner

The President's COVID diagnosis doesn't shield him from criticism; everyone should take precautions seriously.

### Audience

General public

### On-the-ground actions from transcript

- Wash your hands, don't touch your face, stay at home, wear a mask (suggested)

### Whats missing in summary

The emotional impact of the President's diagnosis and the importance of taking the pandemic seriously.

### Tags

#COVID-19 #President #Health #Precautions #Election


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about hope and reality.
Because reality just came crashing in on some people.
If you do not know, the President of the United States has got it.
I hope the President, the First Lady, and all the others who were impacted are fine.
Hope they make a full recovery.
I hope that they go through this without incident.
I hope they are asymptomatic the entire time.
And I am sincere when I say that.
Because nobody anywhere on this planet should have to go through what more than 200,000
Americans have.
With that said, the reality is, when this was still new, still unknown, working class
Americans who did not have the luxury of having millions spent on their safety and security,
who did not have access to the finest health care anywhere in the world, well they were
deemed essential.
They were told to go back to work.
When Americans lost their jobs, this administration's primary concern was trying to force them to
go back to work, force them to find another job, attempted to provide the minimum amount
of assistance to get them back where they belong, because it's really important.
I'm going to suggest that the office of the Presidency is more important than working
at Winn-Dixie.
So with that in mind, while I wish the President and the First Lady the best, this little vacation
will not change the story, not on this channel.
It will not shield him from criticism that is rightfully earned.
At the end of the day, we're about a month away from the election.
Yeah, this is bad timing for him.
It's bad timing for this to happen, but it is what it is.
Politics on the national level, it's tough, and he knew what he signed up for.
I truly do hope they make it through this.
I also hope that it serves as a warning to those who may not be taking it seriously,
who may have a cavalier attitude towards it.
Wash your hands.
Don't touch your face.
Stay at home.
If you have to go out, wear a mask.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}