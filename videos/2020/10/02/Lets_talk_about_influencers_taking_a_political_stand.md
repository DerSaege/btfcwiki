---
title: Let's talk about influencers taking a political stand....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z_dkjVS6Clg) |
| Published | 2020/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Influencers facing pressure to take political stands to retain audience.
- All topics have political implications, from travel to food to fashion.
- Encourages influencers to condemn bigotry and amplify voices of marginalized.
- Advocates for self-education on issues related to their content.
- Emphasizes the importance of not attacking those with less power.
- Pushes for uplifting others instead of tearing them down.
- Being a good person is key, rather than being a political expert.

### Quotes

- "Everything's political. Everything is political."
- "You don't have to become a political expert. Just try to be a good person."
- "Never kick down. Only punch up."
- "Just have to talk about the things that relate directly to your genre."
- "If you take the moral stands, you don't have to come out and endorse a candidate."

### Oneliner

Influencers are urged to acknowledge the political undercurrents in all topics, condemn bigotry, amplify marginalized voices, and uplift others, promoting goodness over political expertise.

### Audience

Influencers, content creators

### On-the-ground actions from transcript

- Condemn bigotry and police comments on your platform (suggested)
- Speak up for marginalized voices using your platform (suggested)
- Self-educate on issues related to your content (suggested)
- Never attack those with less power; focus on uplifting (suggested)

### Whats missing in summary

Beau's engaging and thoughtful delivery.

### Tags

#Influencers #Politics #SocialResponsibility #UpliftOthers #SelfEducation


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about everything being political.
We're going to talk about influencers and politics.
Apparently some outlet released an article that suggested that if influencers do not
take a political stand over the next 30 days, they risk losing large portions of their audience
never to get them back.
I don't know what outlet did it, but it obviously had some reach because within the
space of an hour I had four influencers that I know reach out and say, what am I supposed
to do?
I didn't want to do anything political, I just wanted to talk about this subject.
I feel you.
I feel you.
Everything's political.
Everything is political.
Whatever your topic is, whatever your little genre is, it's political on some level.
You just wanted to do a travel channel.
Cool.
And that brings up a lot of things about economic disparity and the United States' let's
just say kind of lopsided immigration policies.
I just wanted to do a food channel.
Talk about food.
Post photos of food to Instagram.
I'm the foodie, that's it.
Which again brings up income inequality, food insecurity, and health care because eating
right can keep you healthy.
I just wanted to do something about fashion.
Take selfies and post cute pictures.
Yeah, and that brings up labor, income inequality, objectification, normative beauty standards,
all that stuff.
It's political.
Cars.
Yeah, pollution, labor, the tariffs.
Health care.
Just wanted people to be fit.
Yeah, brings up those same normative beauty standards.
There's a lot of people who don't have access to health care, don't have the time to exercise
because they're struggling to survive.
Just like gadgets.
Just like to play with high tech stuff which brings up mining and the exploitation of the
third world, colonization and all of that stuff.
In an unjust society, everything is political.
Everything is political.
But have no fear.
This isn't as hard as it sounds.
You do not have to go full bore.
You don't have to change your channel.
Just work some simple stuff in.
First, condemn bigotry of any kind.
Start there, real easy.
Condemn bigotry.
Don't just say it.
Act a little bit on it.
Police your comments.
If you're on YouTube, use the hide user from channel feature.
As soon as somebody says something bigoted, just boop, they're gone.
Channel never sees it.
That person doesn't even know you did it.
They still come watch your videos and leave their comments yelling into the void.
Doesn't hurt you at all, but enhances the user experience.
Here's the part that may be a little bit more difficult.
Speak up for the people who don't have a voice.
You have a platform.
Use it.
That's what people are expecting.
Doesn't have to be all the time.
In fact, if you did it all the time, people would probably not like it.
But a brief mention every once in a while shows that you're human.
Shows you're a person.
So you take care of those two things and then self-educate.
Learn a little bit about the issues surrounding whatever your genre is and then remember this
one little piece of advice.
Never kick down.
Only punch up.
Those people who have less power, less influence than you do, they're never going to be the
source of your problem.
Ever.
Under any circumstance.
That's never the real source.
The real source is somewhere else.
Remember that and you're fine.
You don't have to become a political expert.
Just try to be a good person.
That's it.
You don't have to worry about this.
If you take the moral stands, you don't have to come out and endorse a candidate.
Just have to talk about the things that relate directly to your genre.
And don't kick down.
Try to uplift.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}