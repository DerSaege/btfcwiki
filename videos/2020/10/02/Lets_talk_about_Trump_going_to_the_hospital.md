---
title: Let's talk about Trump going to the hospital....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uuZgYATHO8w) |
| Published | 2020/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is on his way to Walter Reed with the White House claiming everything is fine.
- There's skepticism due to the administration's lack of truthfulness.
- The importance of Trump fully recovering or having no symptoms is not out of sympathy but to avoid the survival of "Trumpism."
- It's vital for Trump to lose at the polls to reject his policies and not let them be seen as martyrdom.
- Rejecting Trump at the polls is rejecting the hate, divisiveness, and authoritarian rule he represents.
- The rejection of Trump by all marginalized demographics in the country is significant.
- Beau isn't overly concerned about who wins in the election as long as Trump loses.
- Surviving a negative outcome for Trump prevents the people from rejecting his policies collectively.
- The defeat of Trump at the polls is critical for the nation's soul to reject everything he stands for, not just his person.
- Beau stresses the importance of American voters defeating Trump rather than a public health issue.

### Quotes

- "It's incredibly important that the president fully recovers or suffers no symptoms."
- "If we don't show at the polls that Trump's policies are anti-American, that Trump's policies are the cause of the division, they'll continue."
- "I want him to survive probably more than anybody because I think that's more important than anything."
- "It's not just a person anymore. It's Trumpism."
- "I want the American voters to."

### Oneliner

Beau stresses the critical importance of rejecting Trump at the polls to prevent the survival of "Trumpism" and its divisive policies.

### Audience

American voters

### On-the-ground actions from transcript

- Reject Trump's policies by voting against him in the upcoming election (implied)
- Encourage others to vote against Trump to reject hate, divisiveness, and authoritarian rule (implied)
- Advocate for marginalized communities by supporting their rejection of Trump (implied)

### Whats missing in summary

The emotional urgency and moral imperative conveyed by Beau can best be felt by watching the full transcript. 

### Tags

#RejectTrump #Election2020 #Trumpism #CriticalVote #CommunityAction


## Transcript
Well howdy there internet people, it's Beau again.
So as I record this, Trump is on his way to Walter Reed.
The White House is saying that everything's fine.
Everything's under control, there is nothing to worry about.
Just a minor thing.
Some people are skeptical.
They have reason to be.
This administration is not known for its truthfulness.
Fair enough.
There are a lot of people who are hoping for an outcome.
There are a lot of people who are hoping for a negative outcome.
I'm going to suggest that it is incredibly important that the president fully recovers
or suffers no symptoms.
It's not out of sympathy, to be honest.
If Trump suffers a negative outcome, what happens?
Yeah he goes away.
Fine.
Trumpism doesn't.
Trumpism will survive him.
It is incredibly important that Trump lose at the polls.
This yeah, sure, it's an immediate end to some of his policies.
Got it.
I understand that sentiment.
However his supporters are still there.
His supporters still believe that they have the majority of Americans behind them.
They still believe they have a mandate.
It is incredibly important for this country to see his policies rejected.
If Pence takes over, if Pence is the candidate, it doesn't mean anything if he loses.
It just shows that people didn't want Pence.
We have to show that people don't want Trump, that people don't want his policies.
We can't let it go down a martyr.
I know I'm saying this like we have some say in this matter.
We don't.
However I just want people to be aware that if we don't show at the polls that Trump's
policies are anti-American, that Trump's policies are the cause of the division, they'll continue.
There will be people who still think that that's the right way to go about it.
I also feel that it is incredibly important for all of the demographics in this country
that have been marginalized, that have been slighted by this man, to see him rejected
by their neighbors.
That's what's important to me.
I'm not a huge fan of electoral politics.
I don't care who wins as long as Trump loses, as long as it's a referendum on his policies
and they are flatly rejected by the American people.
If he suffers a negative outcome, we don't have that opportunity.
We don't have the opportunity as a people to reject the hate, to reject the divisiveness,
to reject the authoritarian rule.
And there will be people out there who will turn this into a situation in which he's proclaimed
a hero because, see, even when it was affecting him, he didn't try to infringe on our rights
by asking us to wear a mask.
He's got to be defeated at the polls.
I want him to survive probably more than anybody because I think that's more important than
anything.
If this country is to get back on track, Trump has to have a positive outcome and then he's
got to be defeated on November 3rd.
Can't let him take an easy way out, as if we have some say in the matter again.
It's important for this country, for the soul of this nation, to reject everything that
is Trump.
It's not just a person anymore.
It's Trumpism.
It's this belief in might makes right.
It's this belief that those at the top can squash those on the bottom without repercussion.
I don't want the public health issue to get him.
I want the American voters to.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}