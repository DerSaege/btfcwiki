---
title: Let's talk about Trump, Joe Biden, and mud....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Am0CypZFIuc) |
| Published | 2020/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump campaign's strategy: releasing information about Biden every other day to create a parallel with hopes of decreasing voter enthusiasm around Biden.
- The strategy aims to target emotional voters on election day, suppress the vote, and undermine democracy.
- Trump campaign's late deployment of unreliable information to avoid debunking before the election.
- Beau assumes all allegations about Biden are true, focusing on the pragmatic math and the lesser of two evils concept.
- Even if all allegations were true, Biden is still seen as the better option compared to Trump for the country.
- Beau stresses the importance of stopping the damage being done by Trump rather than praising Biden.
- The mental exercise is to assume allegations are true, do the math, and realize Biden remains the lesser of two evils.

### Quotes

- "It doesn't matter what they say. It doesn't matter if it's easily debunked."
- "Biden will always end up being the lesser of two evils."
- "It doesn't matter who replaces them. You just want to stop the damage being done."

### Oneliner

Beau says to assume allegations are true, do the math, and realize Biden remains the lesser evil compared to Trump, stressing the urgency to stop the damage.

### Audience

Voters

### On-the-ground actions from transcript

- Challenge misinformation by fact-checking and sharing accurate information (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Trump campaign's strategy and why it's vital to focus on stopping the damage being done rather than praising Biden.

### Tags

#TrumpCampaign #Biden #ElectionStrategy #VoterEnthusiasm #StopTheDamage


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Joe Biden.
We're going to talk about Joe Biden, the Trump campaign, the Trump campaign's current strategy,
what they hope to achieve by deploying this strategy, and why they are deploying this
strategy so late in the game.
And after all that, we're going to talk about why it doesn't matter.
It certainly appears that the Trump campaign's current strategy is to release information
about Biden every other day until the election in the hopes of creating a parallel, making
people draw an equivalency.
They have identified the fact that many Americans are tired of the constant drama that accompanies
a Trump administration.
They want to create the image that a Biden administration will be the same way.
By doing that, they hope to decrease voter enthusiasm around Biden.
That appears to be the strategy.
Why are they deploying it this late in the game?
My read on it is that the information is not really reliable.
That it creates a false narrative.
It is something that is going to be easily debunked.
The problem is, it's so close to the election, will it be debunked in time?
If this was something that was verifiable and credible, they would have released it
before early voting started.
But they didn't.
They're waiting until now because it appears as though they're just targeting those emotional
voters who happen to be voting on election day.
They want to decrease their turnout, suppress the vote, undermine democracy and all that.
That's how it appears.
And they don't want it debunked before it happens.
I'll be honest, I am personally really, really tired of debunking Trump's lies after years
of doing it.
So today I want to go in a different direction.
Today I want to try something new.
It's all true.
Everything the Trump administration, the Trump campaign has alleged or will allege about
Joe Biden is true.
All of it.
To be clear, I haven't seen any evidence that convinces me that any of it is true.
But right now we're just going to assume that it's all true.
In fact, I have emails.
I don't have emails.
But we're going to pretend that I do.
And these emails outline a conversation between Joe Biden and Hunter Biden because apparently
Hunter Biden is running for office as well.
And they plan on going into the Treasury, putting masks on and taking a hundred million,
no a billion dollars out of the Treasury and putting it in their pockets.
Assume that's true.
Again, I have no evidence that any of these allegations are true.
However, Joe Biden is a politician.
I am like super comfortable assuming that all politicians are corrupt.
That is a generalization that has rarely let me down in my life.
So right now it's all true.
Everything the Trump administration has alleged or will allege is true.
And there are mythical emails showing that they plan on taking a billion dollars the
day after they take office.
Okay, except all of that is fact and let's move on with the campaign.
First issue.
Joe Biden takes the public health issue seriously.
Trump does not.
Because of Trump's ineffective and ruinous leadership, 225,000 holes have been dug.
Because Biden takes it seriously, we can safely assume that by the time it's all said and
done, he will have saved 225,000 people.
Do the math.
Do the math on that billion dollars.
I should have stuck with a hundred million because I'd already done the math on that.
Four thousand four hundred and something dollars per life saved.
I'm okay with that.
If that's what it's going to cost to get somebody in there who's actually going to do the job
and save American lives, I'm okay with that.
I think most Americans would be okay with a billion dollar price tag to save a quarter
of a million lives.
The point here is that at the end of the day, it doesn't matter.
There is no allegation the Trump administration, the Trump campaign can make about Joe Biden
that is going to make him, make Biden be the one who is worse for the country.
That's not going to happen.
If you are pragmatic and do the math and just assume that Biden is ridiculously corrupt
to an impossible level, he's still the lesser of two evils.
This isn't praise for Joe Biden.
I've said it before.
He's not my man.
I don't think he's going to be a great president.
I just expect him to put a tourniquet on.
That's it.
Even with that incredibly low standard and adding in the idea that he is incredibly corrupt,
he's still the lesser of two evils.
He is still better for the country than Trump.
It's not praise for Biden.
It's showing how bad Trump is.
So my suggestion is for the rest of the election season to just assume that everything the
Trump administration says is true when it comes to Biden.
It's all true.
And then do the math.
Just as a mental exercise, assume it's true and then do the pragmatic math.
And when he still ends up being the lesser of two evils, that should show you how important
it is for Trump to lose, how bad Trump is for this country.
It doesn't matter what they say.
It doesn't matter if it's easily debunked.
It doesn't matter if you've already seen it debunked.
It doesn't matter if the evidence is flimsy or it came from a foreign intelligence service.
Just believe it for a split second and say, OK, this is true and do the pragmatic math.
At the end of the day, I'm willing to bet that Joe Biden will always be the lesser of
two evils.
I am sick and tired of people who appear to have the moral and ethical compass that this
administration appears to have slinging mud.
They don't have any room to.
Even if you were to up that number to $10 billion, you would still sit there and be
like, well, at least he didn't have people arguing in court that children didn't need
toothbrushes or beds.
There is no allegation the Trump campaign can make that is going to make Biden be the
one who's worse for the country.
Biden will always end up being the lesser of two evils.
It's not that he's that good.
It's that Trump is that bad.
So it's a mental exercise to run through your mind.
And when you realize that it doesn't matter what allegations are made, that Biden still
ends up being the lesser of two evils, if anything, that should show you how important
it is for Trump to lose.
And it should increase voter enthusiasm.
Because Trump is that bad.
He is that bad.
There's no amount of mudslinging coming from his campaign that is going to alter how bad
he is.
I honestly, I can't think of many allegations that would alter that equation.
Biden will always be the lesser of two evils.
Biden will always be better for the country.
Not because he's good, but because Trump's that bad.
And when you have somebody that bad, it doesn't matter who replaces them.
You just want to stop the damage being done.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}