---
title: Let's talk about being tired and settling into a routine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vjddKxBcxB4) |
| Published | 2020/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the national exhaustion and feeling of being tired.
- Mentions the cliche of characters being close to retirement in movies and its truth.
- Talks about staying cautious and avoiding routines in high-risk environments.
- Compares the current national situation to being at the end of a tour.
- Emphasizes the need to continue taking precautions despite feeling exhausted.
- Urges everyone, especially medical providers, to mitigate risks and not become complacent.
- Points out that the situation is not routine and stresses the importance of following safety measures.
- Mentions the need to stay vigilant until a safe treatment or vaccine is available.
- Reminds about the ongoing public health crisis and the necessity to keep fighting.
- Advises being prepared for emergencies like hurricanes and not underestimating the current situation.

### Quotes

- "It's not over and it's not routine."
- "Mitigate all the risk you can because there's a whole bunch of people out there who aren't."
- "Don't become overconfident. Don't settle into a routine."

### Oneliner

Beau addresses national exhaustion, urges continued caution, and stresses the importance of not becoming complacent in the ongoing crisis.

### Audience

General public

### On-the-ground actions from transcript

- Mitigate risks and continue following safety measures (exemplified)
- Be prepared for emergencies like hurricanes (exemplified)

### Whats missing in summary

The full transcript provides a detailed reminder of the importance of staying vigilant and cautious, despite exhaustion, in the ongoing crisis.

### Tags

#Exhaustion #NationalCrisis #Precautions #PublicHealth #EmergencyPreparedness


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about
being tired, just being exhausted.
Because I think a lot of people are there, nationally, I think that's where we're at.
How many times have you been watching a movie
and you heard the line, oh he was three days to retirement.
He was about to rotate home.
Yeah, it's used a lot because it's an
easy gimmick to get you to feel for the character.
At the same time that cliche exists
because there's an element of truth to it.
In
fields where you always are in a state of caution,
towards the end you
tend to become lax.
In the contracting community there's a saying, in the fight until you're on the
flight
to you are literally on the plane home,
you exercise personal security.
Because if you don't, you settle into a routine and
in that world routines are bad, it makes you predictable.
You don't want that.
You want to exercise all the precaution that you can
even though you're in a state
of caution.
If you don't, that is when bad things happen.
You have to stay in it until the end.
Right now
on a national level
we are at the end of our tour.
It's the way it seems.
For a lot of people I'm sure that's how it feels.
The last four years have been unforgiving.
The last year in particular
it's been endless scandals.
It's been a public health issue that is out of control and shows no signs of
coming under control.
It is endless fires, hurricanes, unrest,
an economy in tatters.
Yeah, got it.
But
it's not over
and it's not routine.
It isn't routine.
I know that it's hard
to take precautions when you are constantly in a state of caution.
But you have to. You don't really have a choice on this one.
When you are out and about
do everything you can to mitigate risk
because there are still people out there that do not get it. They're still having
to explain that this is worse than the flu.
If you are a medical provider
do everything that you're supposed to.
Make sure that you have everything on that you're supposed to. Don't get lax.
Don't settle into a routine.
I understand that it's every day
but it's not routine.
This is not a routine situation.
Mitigate all the risk you can because there's a whole bunch of people out
there who aren't.
They aren't doing their part.
At some point
this long national nightmare will end
but we're not there yet.
We are not there yet.
And
I'm seeing people get very comfortable
because of poll numbers and stuff like that.
Those numbers don't mean anything.
They mean nothing.
That's what could happen
if everybody follows through.
It's just like the projections where
everything's getting better as far as the public health thing.
That's what could happen
if everybody follows through with what they're supposed to do.
However,
we know that as soon as those numbers come out people get complacent
and they think it's over.
It's not.
That particular issue does not end
until there is a safe,
available, reliable treatment or vaccine.
It doesn't end until then.
It doesn't stop.
We have to stay in the game, stay in the fight,
until we are on the flight.
We don't even have our ticket yet.
Everybody needs to continue to exercise
the precautions they need to.
We've got a big hurricane
brewing up in the Gulf.
If you are anywhere in that very wide cone,
make sure that you're ready.
Make sure you're ready. You don't want to complicate an already exhausting situation
by not being ready.
Everybody
at this point needs to realize
it's not over.
It's not over.
Don't become overconfident.
Don't settle into a routine.
Don't think
that
it's in the bag.
Because the second you think that,
that's when you end up in one.
It will end.
Make sure you're around to see it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}