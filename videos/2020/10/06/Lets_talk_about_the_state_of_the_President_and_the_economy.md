---
title: Let's talk about the state of the President and the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=visSYGjQgow) |
| Published | 2020/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's unusual behavior raises concerns after his recent illness, leading to questionable decisions.
- Federal Reserve Chair warns of dire consequences without stimulus, calling the scenario tragic.
- Despite this, Trump abruptly halts stimulus negotiations until after the election, prioritizing politics over economy.
- Beau questions if Trump's actions indicate neurological impacts from his recent hospitalization.
- Trump's decision to delay stimulus may harm the economy and damage his political standing.
- Beau suggests that Trump's behavior is out of character and potentially influenced by health issues.
- Concerns arise about having a president who may not be thinking clearly or rationally in tough times.
- Beau points out that Trump's decision-making process appears flawed and lacking in listening to others.
- The situation could lead to disastrous outcomes and needs national attention and discourse.
- Ultimately, Beau expresses worry about the future under a leader making erratic decisions.

### Quotes

- "If the economy tanks, it's Donald Trump's fault."
- "We cannot have a president who is behaving out of character."
- "We've got tough times ahead."
- "It may get worse."
- "It could lead to some pretty grim scenarios."

### Oneliner

Trump's concerning decision to delay stimulus post-hospitalization raises questions about his judgment, putting the economy at risk and his leadership capabilities in doubt.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Have national level discourse on the implications of a leader behaving erratically (suggested)
- Stay informed and engaged in political decisions and their potential impact on the economy (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's recent decision-making, raising concerns about his health and leadership capabilities. Watching the full transcript can offer a deeper understanding of the potential impacts of erratic behavior on national and economic stability.

### Tags

#USPolitics #Economy #Trump #Leadership #HealthConcerns


## Transcript
Well, howdy there internet people.
It's Bob McGowan.
So today we had to talk about the state of the economy and the state of the
president, because some behavior was exhibited today that I'm not sure is
typical of him and we do know that after going through what he went through,
there are a lot of people who come back from it and they aren't operating
at full capacity and he displayed some judgment today and did some things
today that just don't make sense.
Not for Trump, not for Trump.
And that's how you have to view this.
Not as, don't look at it in policy consideration, think about it as Trump.
Okay.
So to give you the full run-up into what's occurred, the, uh, pal who's
the Federal Reserve Chair.
He said that if there wasn't a stimulus,
that it could lead to a weak recovery,
creating unnecessary hardship for households and businesses.
He described that scenario as tragic.
He went on to say that if they didn't get the stimulus,
it would cause a long period of unnecessarily slow progress
that could continue to exacerbate existing disparities in our economy.
Pretty grim. Pretty grim coming from the guy who in theory is supposed to know.
That happened this morning. This afternoon, the president,
I have instructed my representatives to stop negotiating until after the election when
immediately after I win, we will pass a major stimulus bill that focuses on hard-working
Americans in small business." Okay, so the day he gets the news that a lack of
stimulus is going to be just devastating to the economy, the president says, well
we're not gonna have one till after the election. Set aside policy considerations
as to whether or not you think the stimulus is a good idea or not. The day
the American people were told that we need a stimulus or the economy is going
to go bad, Trump says, you're not going to get one until after the election.
Does that sound like Trump in his entire time in office?
Has he ever put a policy consideration above what is going to help him politically?
No, he hasn't.
I, uh, I take the president to task for policy decisions all the time.
I have never insulted his political abilities, never.
This is a bad move politically.
That's not something Trump does.
He knows his base.
This is a bad move for him.
We have to wonder whether or not he had some neurological impacts during his recent vacation
at Walter Reed.
We know that it's a possible outcome, it is actually incredibly frequent when it comes
to people who were hospitalized.
We have to consider this now.
It's not just a talking point, it's not just a joke.
He hadn't been back that long and he is already doing things that are way out of character
for him.
Trump doesn't care about policy.
He never has.
And he doesn't actually care about debt.
He's the king of debt.
I mean, look at his finances.
doesn't care about the money, it's not like it's his money, it's not like he's
paying taxes and funding this, it's all about politics to him. It always has been.
And here the American people get a reminder, if the economy tanks it's
Donald Trump's fault. He pulled out of the negotiations literally right after he
was told we have to have this and they're not going to revisit it until
until after the election, either the president has just completely taken leave of abilities
that he had, or he's given up and doesn't want to win.
Those seem to be the most likely options to me, because this downturn can happen tomorrow,
And with him pulling out and saying that they're definitely not going to get it for a month,
that may cause it.
That could definitely impact the markets, the thing he likes to take credit for.
That's the other side to this.
Not just is it not Trump politically.
He's damaging the one thing that he feels like he can take credit for right now, which
is the economy.
He's a failure across the board policy-wise everywhere else.
the economy, in my opinion, he's a failure as well, however, to his base, he's not.
If it goes south now, it was his decision.
He withdrew.
He said, no, we're not going to do this.
That's not just a normal Trump bad decision.
That to me seems like he doesn't have all of his faculties.
That he is not operating as the Trump that we knew.
he may be in that fog. Neurological damage from this is a very real thing. It is incredibly
common and it appears that it may be coming into play already. It's something that needs
to be discussed on the national level. It's something we have to talk about because it
It may get worse.
We cannot have a president who is behaving out of character, who is behaving in an emotional
or irrational way.
We've got tough times ahead.
We need somebody who is capable of making the right decisions based on their own input
or at least listening to others.
Aside from him not thinking clearly,
he's not known for listening to others either.
This could be a really, really bad recipe.
It could lead to some pretty grim scenarios.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}