---
title: Let's talk about computers, emails, Biden, Trump, and verification....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=q9xH-8c4Qgs) |
| Published | 2020/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recaps a video from last year about the Democratic primaries and the narrative surrounding allegations.
- Points out the importance of understanding the timeline in relation to the allegations.
- Mentions the main allegation against Vice President Biden involving elevating a family member.
- Expresses desire to question President Trump or his advisors about similar family-related concerns.
- Notes that the emails discussed have not been verified or authenticated yet.
- Criticizes the White House for acting on unvetted intelligence regarding the emails.
- Draws attention to the lack of response from the administration on intelligence concerning US troops.
- Summarizes the story about a computer repair shop, a laptop, and obtained emails involving Steve Bannon and Rudy Giuliani.
- Comments on the credibility of the source and distribution of the emails, hinting at potential disinformation.
- Considers the possibility that the emails might be genuine but focuses more on the likelihood of a disinformation campaign.

### Quotes

- "The reporting was so bad I couldn't let it slide."
- "I believe the activities that Hunter Biden engaged in, me personally, I think they're shady. I think they're unethical. But they're not illegal."
- "But it's unlikely. It is unlikely that after all of this time, magically it's going to show up right before the election."

### Oneliner

Beau recaps past allegations, questions credibility of unverified emails, and suggests a potential disinformation campaign.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Investigate the credibility of information before acting on it (implied).

### Whats missing in summary

Beau's detailed analysis and commentary on the narrative surrounding the unverified emails and their potential implications.

### Tags

#Politics #Disinformation #Election #Allegations #Integrity


## Transcript
Well howdy there internet people, it's Beau again.
So yeah, we're going to talk about the emails, the computer, the laptop, that whole event
again.
We're going to talk about this narrative one more time because we kind of have to.
Okay if you don't know, last year during the Democratic primaries I put out a video.
The video detailed the timeline surrounding these allegations.
I did this and I was not a supporter of Joe Biden.
Didn't want him to win the primary.
However the reporting was so bad I couldn't let it slide.
When you look at the timeline, the story falls apart.
Some of the allegations are impossible without a time machine.
It's important to understand the timeline when you're talking about this.
Now the meat of the allegation is that Vice President Biden used his position to elevate
a family member.
That's really what it boils down to and this is bad.
I wanted to reach out to President Trump and ask him how he felt about that.
But I'm a small time YouTuber, that wasn't likely.
Just like I wanted to reach out to Senior Advisor to the President Ivanka Trump or Senior
Advisor to the President Jared Kushner and ask them if they believed President Trump
would elevate family members who are completely unqualified simply because they were family
members.
But alas, that's not possible.
One other thing I want to get into before we go too far into this is the emails that
are being discussed really haven't been verified and authenticated yet.
So it's less than vetted intelligence.
Not properly vetted, not 100% verified.
Yet the White House and friends, well they're ready to act on this.
This is super important.
We all need to pay attention to it right now.
Golly gee whiz, I wish they had that attitude when the less than 100% verified intelligence
was about US troops and perhaps somebody targeting them.
But that didn't happen because the administration doesn't care about losers and suckers.
They only care about themselves.
Okay, so to the actual story, and it is a story.
The Post is saying that a computer repair shop run by a pro-Trump person apparently,
received a laptop from an unidentified person who never came back to pick it up.
And on this laptop there were these emails that the Post found out about and was able
to obtain via Steve Bannon and Rudy Giuliani.
Because this just screams credibility.
It's not like this is how active measure campaigns are conducted.
It's not like this is how disinformation is distributed, except it is.
In fact, there are experts on this who are screaming to get on the news.
Some have already given interviews because this is straight out of a textbook.
This is how you disseminate information while hiding the source of the information.
You give it to an unaffiliated third party who is motivated to distribute it.
My gut tells me that's what we're going to find out happened.
One of the experts described this as a standard tactic in disinformation operations.
Yeah, this is right out of a textbook.
Now to be fair, because I try to be fair, the emails haven't been authenticated and
verified.
So there is a slim possibility that they're genuine.
I don't think that what's contained in them is quite the smoking gun that the president
believes.
But, I mean, I guess it is a possibility.
I'm not really focused on that.
I'm more focused on the likelihood that it is a campaign.
Which leaves me with the question of whether or not the White House and friends fell for
a disinformation campaign or they're perpetrating one.
Because that seems a lot more likely to me.
I'll put the link to the video on the timeline below.
At the end of the day, as I said in that, I believe the activities that Hunter Biden
engaged in, me personally, I think they're shady.
I think they're unethical.
But they're not illegal.
And that's really focused on him kind of leasing out his name to be on a board of directors.
Not the allegations that are being made.
Because so far, there is no verifiable, authenticated evidence that this is true.
Is it something we should look into?
Sure.
Said that back then.
But it's unlikely.
It is unlikely that after all of this time, magically it's going to show up right before
the election.
This is kind of like the video I put out.
These are the text messages.
This is the just, you know, huge claim that's going to occur.
Said it was going to happen and here it is.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}