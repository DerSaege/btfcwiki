---
title: Let's talk about Trump's comments and a dress rehearsal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GQcmv8zDLO4) |
| Published | 2020/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump bragged about his exceptional healthcare treatment with 14 doctors, pointing out the stark contrast with the accessibility issues faced by most people.
- The inequality in healthcare access is glaring, emphasized by Trump and other politicians receiving privileged treatment.
- Trump, who previously downplayed the severity of the situation, was able to access top-notch healthcare and then boasted about it.
- Another politician's precautionary medical care raises questions about political connections and wealth influencing access to healthcare.
- The actions of politicians like Trump and others in handling the pandemic forecast their approach to other critical issues like climate change and economic priorities.
- Beau compares the current situation with privileged individuals easily accessing healthcare to a dress rehearsal for the future impact of climate change, stressing the need for urgent action.
- He advocates for political pressure on the upcoming administration to prioritize addressing healthcare disparities and mitigating the impacts of climate change.
- While it may be too late to avoid all negative impacts of climate change, Beau urges for immediate action to mitigate the worst effects.
- The next administration needs to focus on addressing healthcare inequalities and climate change, as they currently do not prioritize these issues without external pressure.
- Beau underscores that without intervention, the privileged few will have access to resources and precautions that the general population lacks, leading to severe consequences.

### Quotes

- "He's like, yeah it was great, we had like 14 doctors standing around."
- "It's a dress rehearsal for climate change."
- "We're past the point of being able to avoid it completely, but we can still mitigate the worst of it."

### Oneliner

Trump and politicians' privileged healthcare access showcases inequalities, serving as a warning for climate change impacts and the urgent need for mitigation.

### Audience

Activists, policymakers, voters

### On-the-ground actions from transcript

- Pressure the upcoming administration to prioritize healthcare disparities and climate change mitigation (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how politicians' access to healthcare during the pandemic can predict their approach to critical issues like climate change, urging for immediate action and political pressure on future administrations.

### Tags

#Healthcare #Inequality #ClimateChange #PoliticalPressure #UrgentAction


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the President's statements, Trump's statements, our politicians'
treatments, what they were able to get and how that compares to everybody else, what
we can forecast from that, and address rehearsal.
So if you don't know, Trump was commenting on his treatment.
He's like, yeah it was great, we had like 14 doctors standing around.
After flying there via helicopter to a world-renowned facility and all of that, that's fantastic,
that's great, it's too bad most people don't have access to that kind of healthcare.
And that's the takeaway, for a lot of people that is the takeaway.
It highlights the inequalities in our healthcare system and it's a valid takeaway.
Yeah absolutely, draw attention to that, it needs to be highlighted.
And the President, who constantly downplayed, to use his term, lied to the American people,
to use mine, he was able to get that.
And then actually had the gall to brag about it.
That's pretty amazing.
Another politician, he checked in as a precautionary measure.
Man, that's cool.
How politically connected, how much money do you have to have to check in as a precautionary
measure?
It's amazing.
And again, it highlights the healthcare issue.
It also forecasts the same people who downplayed this, the same people who told you not to
wear a mask, are the same people who are telling you that we don't need to cut emissions,
that climate change isn't a big deal, that we don't need to worry about it, the economy
is more important.
All of this sounds familiar, right?
We have to go on with life as normal.
Because when the worst of it comes, when it finally hits home and impacts them, well,
they're going to be able to get on a helicopter.
They have precautionary measures they'll be able to take.
We won't.
This is a dress rehearsal for climate change.
What you see happening now is what is going to happen then.
The only difference is it's going to be a lot more than 200,000 people.
It's going to be much, much more severe.
Hopefully here soon we will have a new group of people in charge who theoretically are
going to want to demonstrate that they are the exact opposite of the administration that's
currently in power.
One of the primary objectives we need to give them through political pressure is to address
this because if we're to continue that analogy right now, it's over there still.
We got a couple of cases here, but not many.
We still have time to mitigate, to avoid the worst of it.
We're still going to get hit.
Understand if we start today, we're still going to suffer negative impacts from it.
We're past the point of being able to avoid it completely, but we can still mitigate the
worst of it.
That needs to be something that the next administration is really focused on because at the end of
the day, it's not going to be at the top of their list unless we push them because when
the time comes, they're going to have precautionary measures.
They're going to have access to things we don't.
So they're not thinking about it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}