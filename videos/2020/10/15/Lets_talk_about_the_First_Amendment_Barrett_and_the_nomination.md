---
title: Let's talk about the First Amendment, Barrett, and the nomination....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZzEbLlPKdkc) |
| Published | 2020/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses concern over the nomination and the First Amendment.
- Barrett, a nominee for the Supreme Court, failed to name the five freedoms protected by the First Amendment.
- Compares this to basic knowledge needed for other job interviews.
- Questions how Barrett can interpret the Constitution without knowing its content.
- Emphasizes that this is not a minor issue but a significant disqualifier for the position.
- Criticizes media and Supreme Court justices for misinterpretation of basic constitutional concepts.
- Asserts that Barrett's lack of basic constitutional knowledge makes her unfit for the Supreme Court.
- Argues that any vote to confirm her is not based on qualifications but on party allegiance.
- Concludes that giving an unqualified nominee a lifetime appointment undermines the Constitution.

### Quotes

- "If you can't answer the basic questions about the Constitution, you can't interpret it."
- "This needs to be the end of the hearings. She is unfit for the Supreme Court."
- "If you're going to interpret the Constitution, you have to know what's in it."
- "Any vote to confirm is really just a vote for party."
- "Knowingly giving somebody a lifetime appointment that isn't qualified."

### Oneliner

Barrett's failure to identify basic constitutional freedoms deems her unfit for a Supreme Court lifetime appointment, making any vote to confirm a party allegiance rather than a qualification assessment.

### Audience

Voters, activists, concerned citizens

### On-the-ground actions from transcript

- Contact your senators to express opposition to confirming unqualified nominees (suggested)
- Educate others on the importance of basic constitutional knowledge for Supreme Court justices (implied)

### Whats missing in summary

The full transcript provides detailed arguments on why Barrett's lack of basic constitutional knowledge should disqualify her from a Supreme Court appointment.

### Tags

#SupremeCourt #FirstAmendment #Constitution #Nomination #Qualifications


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the nomination and the First Amendment.
I waited to make this video, this news broke a while ago, and I've just been kind of mulling
it over because I was worried that I was overreacting.
And the more I thought about it, the more I realized, no, I am not overreacting.
Not at all.
If you don't know, Barrett, who is up for a lifetime appointment on the Supreme Court,
was asked to name the five freedoms protected by the First Amendment.
She couldn't.
That's it, the hearings are over.
That's it.
This isn't a little thing.
I know, some people, well, it just slipped her mind.
Okay, sure, let's put this in any other context.
You go apply at McDonald's and you don't know that bread is part of a hamburger, you're
not going to get the job.
You go to be a mechanic and you don't know that there's a cooling system, you're not
going to get the job.
This is an interview for the highest court in the land, the sole job of which is to interpret
the Constitution.
How can she interpret the Constitution if she doesn't know what's in it?
This isn't an entry-level position where there's on-the-job training.
This is a lifetime appointment to the Supreme Court.
She was asked point-blank what freedoms the First Amendment protected and she couldn't
answer.
She couldn't answer.
And for the record to the talking heads out there, you don't have a right to receive redress.
That's not what that says.
Even in the discussion after the question that she failed, the media is not even getting
it right.
You don't have a right for redress.
You have a right to petition for a redress of grievances.
You have a right to ask.
You don't have a right to have your problem remedied.
That's not what the Constitution says.
If the media and Supreme Court justices can't answer basic questions about the Constitution,
that might be why it's constantly misinterpreted.
This isn't a little thing.
This needs to be the end of the hearings.
She is unfit for the Supreme Court.
Period.
There's no other way to spin this.
This is go or no-go, pass or fail.
You can't identify basic things that she should have learned in civics.
I cannot understand why the hearings didn't end at that moment.
That's it.
There's nothing more to be said on this subject.
She doesn't have the base qualifications for the job.
What she would do in a certain case doesn't matter.
Her stances don't matter.
She doesn't know the Constitution well enough to have the job.
That's it.
She doesn't have the qualifications.
If you're going to interpret the Constitution, you have to know what's in it.
She doesn't.
I don't see any reason to go forward with this.
Anybody who votes to confirm isn't doing it because they believe she's qualified.
If you can't answer the basic questions about the Constitution, you can't interpret it.
Any vote to confirm is really just a vote for party.
Knowingly giving somebody a lifetime appointment that isn't qualified.
Knowingly subverting the Constitution because there's no way that she can accurately interpret
it if she doesn't know what's in it.
I don't think I'm overreacting on this.
I think this is one of those pass-fail moments.
You're pregnant or you're not.
She doesn't know what's in it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}