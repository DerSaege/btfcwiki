---
title: Let's talk about Omaha, Trump, logistics, and taking a walk....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lKYOKdmYzIY) |
| Published | 2020/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President held a rally in freezing temperatures in Omaha, with logistics including buses to transport attendees, mostly older Trump supporters.
- After the rally, President left, leading to a shortage of buses to return everyone, leaving elderly people in freezing temperatures.
- Medics had to treat people for hypothermia due to the lack of buses.
- Beau criticizes the President's inability to handle rally logistics, let alone running a country, citing past failures and lack of responsibility.
- Despite the incident, Beau acknowledges that some of those left behind may still support the President and deflect blame.
- Beau questions why people hold the President to a lower standard than they do for other leaders in different scenarios.
- He points out the importance of good leadership in taking care of the people entrusted to them.
- Beau contrasts the President's actions with those of a good leader like Queen Elsa from "Frozen."
- He urges people to think critically about renewing the President's "contract" in the upcoming election.
- Beau criticizes the President's administration as lacking substance, being purely a show driven by personal gain.

### Quotes

- "Queen Elsa showed more leadership than the President of the United States."
- "He's a national embarrassment."
- "Your team doesn't care about you. They will literally leave you behind."
- "It's a show. It's branding. It's about ratings and numbers."
- "The man is not fit to run a carnival."

### Oneliner

President's rally logistics failure in Omaha leaves elderly supporters in freezing temperatures, prompting Beau to question his leadership abilities and urge critical thinking for the upcoming election.

### Audience

Voters, Critical Thinkers

### On-the-ground actions from transcript

- Critically analyze leadership qualities of political candidates (suggested)
- Encourage responsible and empathetic leadership in your community (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the President's mishandling of logistics during a rally and raises concerns about his leadership capabilities and the upcoming election.

### Tags

#Election #Leadership #CriticalThinking #Logistics #Responsibility


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Omaha and logistics and leadership and taking a
walk when you need to.
If you don't know, the President of the United States held a rally in Omaha in freezing temperatures.
Part of the logistics for this rally included providing buses to get from the parking area
way over to where the rally was being held and it was a hike.
Well once the President had used these people, gotten his photo op, got the praise and applause
he feels he deserves, he left.
And there were not enough buses to return everybody.
I want you to think about the demographics that typically support Trump.
They're older.
He left a bunch of elderly people outside in freezing temperatures.
You know normally when I say that the establishment leaves the working class out in the cold,
it's figurative.
Here it was literal.
Medics had to treat people for hypothermia.
This President cannot handle the logistics of a rally.
He certainly cannot handle the logistics of running a country as if we needed more evidence
to support this.
We have four years of unmitigated failure to look back on.
And I get it.
The thing is I know that some of those people that were treated, they're still going to
vote for him.
They're going to find some way to deflect blame.
But the reality is that was his show.
He was the leader.
He's responsible.
Even though this President accepts responsibility for nothing.
As he has said many times.
I get it.
You want to defend your guy.
Red versus blue.
Donkey versus elephant.
I got it.
Put this in any other situation.
Let's say a bunch of elderly people got left out in freezing temperatures by a nursing
home.
Who are you blaming?
The bus driver?
Mental management?
Or the director of that nursing home?
Who's getting sued?
When you realize that you hold a school board or a nursing home director or whoever else
you want to apply this scenario to.
I mean, let's be honest.
If somebody did this with dogs, they'd probably get arrested.
When you realize you hold those people to a higher standard than you hold the President
of the United States to, simply because he's a member of your party, you should probably
realize that maybe your judgment has been skewed by a team mentality.
The thing is, your team doesn't care about you.
They will literally leave you behind.
A good leader takes care of their people.
A good leader, if they realize that they are a danger to the people that they are entrusted
with leading, well, she'll take a walk, happily, singing a song.
And that story, getting frozen, was symbolic.
Queen Elsa showed more leadership than the President of the United States.
It's probably time for this President to take a walk.
And in just a few short days, you have the opportunity to decide whether or not his contract
gets renewed.
Think you need to cancel this show.
And that's what it is.
It's a show.
There's no substance to it.
There's no substance to this administration at all.
It's a show.
It's branding.
It's about ratings and numbers.
What can benefit him personally, and it doesn't matter who gets hurt in the process.
The man is not fit to run a carnival.
And somehow, he was able to con people into letting him run the country.
He's a national embarrassment.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}