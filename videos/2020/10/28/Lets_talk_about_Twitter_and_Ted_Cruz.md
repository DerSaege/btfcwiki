---
title: Let's talk about Twitter and Ted Cruz....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qR_mJZKPgaw) |
| Published | 2020/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ted Cruz attempted to hold Twitter accountable for not carrying information he deems necessary on their platform.
- Beau explains that the government dictating what speech should occur on a private platform contradicts the First Amendment.
- He mentions the distinction between a platform and a publisher, acknowledging the government's role but asserting that it shouldn't override freedom of speech.
- Beau uses a restaurant analogy to demonstrate how Twitter, as a company, aims to ensure a positive user experience by moderating content.
- He criticizes the far right's desire to control information access and suggests they create their own platforms.
- Beau questions Senator Cruz's true intentions, implying that he may prioritize spreading misinformation to manipulate voters rather than genuine information access.
- He argues that the Republican Party seeks equal access only for their "bad ideas" used for manipulation.
- Beau stresses the danger of concentrating power in social media networks and the need to prevent government control over speech.
- He proposes the idea of a state-funded social media network for spreading information without manipulation.
- Beau warns against allowing the government to control information exchange, portraying it as a form of socialism detrimental to democracy.

### Quotes

- "You can't tell somebody what they have to allow on their property."
- "The government can't do this. You don't get to force somebody to carry government ideas."
- "It's too powerful for that. This is big government. It is socialism."

### Oneliner

Beau asserts the dangers of government control over speech and the manipulation of information on social media, advocating for the preservation of freedom of expression and caution against authoritarian practices.

### Audience

Social media users

### On-the-ground actions from transcript

- Advocate for maintaining freedom of speech online (implied)
- Support platforms that prioritize user experience and positive community interactions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of government control over social media platforms and the importance of upholding freedom of expression in the digital age. Viewing the entire transcript offers a comprehensive understanding of Beau's arguments against censorship and manipulation in online spaces. 

### Tags

#FreedomOfSpeech #SocialMedia #GovernmentControl #Misinformation #DigitalDemocracy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Ted Cruz and Twitter.
If you don't know, Ted Cruz attempted to take Twitter to task for not carrying the
information he believes should be carried.
Ted Cruz is Senator Cruz, a member of the government.
The idea that the government can tell somebody what speech has to occur on their platform
is antithetical to the First Amendment.
I understand there's been a whole lot of people up on Capitol Hill recently that do not understand
the First Amendment, and that's fine.
It's not, but we're going to pretend that it is for this conversation.
The government's position is to draw a distinction between a platform and a publisher, and that
makes sense on a lot of levels.
It really does.
I get it.
However, that distinction does not supersede the First Amendment.
You cannot tell somebody what they have to allow on their property.
You can't tell them what speech has to occur.
We have a whole amendment about this.
Assembly, speech, press.
Let's put this into any other scenario.
Let's say that Senator Cruz owns a restaurant, and I go in and order a meal.
Sit down, eat my meal.
And as I stand up, every so often, I yell out some smear about Senator Cruz.
I'm willing to bet I will be asked to leave.
Me doing that would interfere with the experience of the other guests.
That appears to be what Twitter is doing.
They're trying to guarantee a positive experience for their users.
That's kind of their responsibility as a company.
That's what they're supposed to do.
When it comes to anything else, the market will decide.
When it comes to health care, housing, food, the market will take care of it.
We don't need to worry about people's wages as they get exploited.
No all that will be fine.
Rugged individualism.
Pull yourself up by your bootstraps.
The far right that wants to control the access to information, they are more than welcome
to set up their own social media networks.
In fact, they have on numerous occasions.
But let's be honest, posting something there is a lot like it not being posted on Twitter.
Nobody is going to read it.
Because the market decided.
We don't want your bad ideas.
It's that simple.
I understand that Twitter, all social media networks, have an immense amount of power.
Which is exactly why it can't be controlled by the government.
The reality is, I don't believe that Ted Cruz really cares about access to information.
Doesn't care about accurate information.
I don't believe that.
Maybe he does, but I don't believe it.
It seems to me, he wants to guarantee the information that is necessary to misinform,
manipulate, and play low information voters is widely accessible.
That's what it seems like to me.
Because it's the only way the Republican Party can continue to get working class Americans,
those struggling in poverty, to vote against their own interests to support them.
It's entertaining that when it comes to social media, all of a sudden, the Republican Party
is all about community control of the means of production, distribution, and exchange.
Social media socialism.
That's what they want.
But when it comes to health care, the market will decide.
Wages, the market will decide.
Food, the market will decide.
The only thing that it seems that they want equal access for everybody for is their bad
ideas.
The information they need to manipulate people.
You can't tell somebody what they have to do on their property.
You can't tell them the speech they have to allow.
They have to promote.
That article wasn't censored.
It's still available, I think.
You can go look at it.
You just can't get to it from Twitter.
That's what happened.
You couldn't access it.
You couldn't spread it via their platform, their network.
That's not censorship.
That article is still out there.
It's still widely available.
It's just that those in power want that, the contents of that spread widely because it
will help them maintain power.
Because rather than focusing on policy ideas that will help their voters, they just want
to smear somebody.
It's how it appears to me.
I could be wrong.
I don't think I am, though.
At the end of the day, the government does not have control over speech.
We have to keep it that way.
I understand.
It's concentrating a whole lot of power in very few hands.
It's creating an alternative power structure that is separate from the government.
That was literally the intention of the First Amendment.
That was the whole idea.
I find it funny that the party that constantly attempts to cast itself as the defenders of
the Constitution doesn't seem to understand it on any level.
The government can't do this.
You don't get to force somebody to carry government ideas, and that's what it is.
The current party wants this idea carried.
It's antithetical to the First Amendment.
It's wrong.
And yeah, I'll be the first to admit, there are a lot of things that have been reduced
in circulation on different social media networks that I don't think that was good.
But it's not my property.
I can't tell them what to do.
I can only use their property to the extent they allow me.
That's how it works.
Maybe what Senator Cruz should do is write a bill to set up some form of, I don't know,
some form of public service that allows information to be spread like that.
Maybe a state-funded social media network.
And then they can have all the propaganda and bad ideas out there they want, because
that's what it's about.
It's what it always seems like it's about, control.
They want to control those people who have limited time to access good information.
They have limited time because they're working, struggling to survive.
They can't research on their own.
And they want an emotional lever to manipulate those people to vote against their own interests
so they can keep them in that state so they will support the establishment politicians.
So much so, and this tool is so powerful, that the Republican Party was able to convince
a large portion of Americans that a billionaire who constantly rubbed elbows with the establishment
of both parties was somehow not part of the establishment.
That's how powerful this tool is.
And that's why it cannot come under government control.
It's too powerful for that.
This is big government.
It is socialism.
It's socialism on the level of the government, the governing organization, is going to be
able to control the production, distribution, and exchange.
Which that argument can be made, Senator Cruz, and you probably find a whole lot of support.
But they're going to want health care and food first.
They're going to want people taken care of before they worry about taking care of your
political future.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}