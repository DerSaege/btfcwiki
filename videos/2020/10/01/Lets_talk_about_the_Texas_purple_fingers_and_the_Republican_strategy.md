---
title: Let's talk about the Texas, purple fingers, and the Republican strategy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p4jEU00kEBg) |
| Published | 2020/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People in failed states risked their lives to vote, seeking a voice in their country's future.
- They had to ensure orderly voting locations and convince others it was worthwhile.
- Intimidation by "goons" was a real concern for voters trying to make a change.
- Texas's Republican strategy limits ballot drop-off locations to sow confusion and discourage voting.
- The president's rhetoric undermines election integrity, suggesting votes may not be counted.
- Poll watchers are seemingly being used to surveil and intimidate voters.
- The GOP appears to be mirroring tactics of failed states to suppress votes deliberately.
- Framing the strategy as a public health measure is seen as an excuse to limit voting access.
- Beau finds the strategy embarrassing and infuriating, hindering the democratic process.
- Voters are advised to stay informed about changing locations and budget extra time to vote.
- Beau urges people to question the credibility of sources spreading misinformation.
- When facing intimidation at polling places, Beau suggests wearing nondescript clothing and avoiding confrontation.
- The goal appears to be preventing a high voter turnout to avoid a potential landslide against them.
- Republicans seem prepared to contest election results in courts rather than respecting the democratic process.
- Beau sees this as a deliberate attempt to undermine the core principles of representative democracy in the U.S.

### Quotes

- "People in failed states risked their lives to vote, seeking a voice in their country's future."
- "The GOP appears to be mirroring tactics of failed states to suppress votes deliberately."
- "Republicans seem prepared to contest election results in courts rather than respecting the democratic process."

### Oneliner

People risked their lives for democracy, but the GOP mirrors failed state tactics to suppress votes, undermining the democratic process.

### Audience

Voters, Activists, Citizens

### On-the-ground actions from transcript

- Stay informed about changing voting locations and budget extra time to vote (implied)
- Question the credibility of sources spreading misinformation
- Wear nondescript clothing, avoid confrontation, and vote peacefully (implied)

### Whats missing in summary

The full transcript provides a deep insight into the deliberate efforts to suppress voting rights and undermine democracy, urging people to stay vigilant and protect their right to vote.

### Tags

#VotingRights #RepublicanStrategy #DemocraticProcess #ElectionIntegrity #SuppressionTactics


## Transcript
Well howdy there, Internet of People.
It's Beau again.
So today we're going to talk about Texas specifically, the Republican election strategy generally,
and purple fingers.
Y'all remember this?
Some of you do.
Y'all remember that?
It was a cool time, right?
Felt like that country was finally turning the corner, like things were going to get
better and the photos of those people with that ink on their finger, man, they were happy.
Because they felt like they finally had a voice, and a lot of them took a pretty big
risk to get that ink on their finger.
Those people who wanted that election to go off, who wanted that to happen, what did they
have to worry about?
What did they have to deal with?
First thing was they had to make sure that people knew where to go, that it wasn't chaos.
They had to make sure it was orderly and simple.
It was a simple process.
The next thing they had to do was make sure that people thought it was going to matter,
that they were going to have some voice, no matter how small, how minimal, they were going
to have some voice in what was going to happen to their country.
And then they had to worry about the goons, people showing up and intimidating, attempting
to dissuade them from voting.
That's what it took to get that ink on somebody's finger in a failed state.
Now let's talk about the Republican strategy.
In Texas today, very short notice, the governor has decided that, hey, you know what, we're
only going to allow one drop-off location for the ballots per county.
So if you live in Austin, you better pay attention, because the place where you dropped off your
ballot last time, that's probably not where you're supposed to go.
Seems like the goal is to sow confusion, make it hard, dissuade people from voting, make
them give up.
Then you've got the rhetoric from the president, undermining the integrity of the election
at every turn.
It's going to be rigged.
Oh, you know, your vote may not even be counted.
May take months.
It may not get counted at all.
Maybe you just shouldn't do it.
Your voice doesn't really matter.
And then we've got the goons, the poll watchers.
It certainly appears that the Republican Party is deploying the strategy of the opposition
over there.
That should give everybody pause.
They're having to worry about the same things a failed state had to worry about.
And it's by design, by one of our own political parties attempting to suppress the vote.
Because that is certainly how it appears.
They're framing this in Texas as a public health thing.
Yeah, because a bunch of doctors got together and was like, hey, you know what would help
us manage this?
Let's send everybody to one location and, you know, like, create a crowd.
That'll help out a whole lot.
That didn't happen.
It's not a reason, it's an excuse.
The goal is to sow confusion.
The goal is to dissuade people, to make them give up.
I have a real issue with this.
I have a real issue with this strategy being used.
I remember how happy these people were, how they looked.
And now the party that wants to make America great again is trying to create the same situation
that they had to overcome to get some semblance of a representative democracy.
It's embarrassing and it's infuriating.
This is the strategy.
This is the attempt.
This is what's going to make America great.
Making sure that people don't have a voice.
To deal with this, let's just keep it simple.
As far as changing the locations, you got to stay aware because they're going to do
it.
They're going to try to sow confusion.
Budget some extra time to vote because something's going to happen.
They're going to make sure of it.
Certainly appears that way.
Looks like it's a pretty thought out strategy because while we're talking about Texas, this
is happening everywhere.
So budget some extra time.
When it comes to the rhetoric from the president and the others saying stuff like this, just
consider the source.
They haven't told the truth about anything else in the last four years.
What makes you think they're telling the truth now?
When it comes to the goons, wear nondescript clothing.
Wave at them when you go in.
Don't make any statements.
Just go in, vote, and leave.
There's a reason you vote in secret.
These people risked a whole lot, a whole lot, to overcome the Republican strategy for this
election.
Definitely the way it appears.
It seems very concerted.
It seems planned that the whole goal is to deny the people a voice because they know
that the more people who show up, the more likely that landslide is.
And they don't want a representative democracy.
They want to take it to the courts because they think they can win there.
They are trying to subvert the republic, the representative democracy that we're supposed
to have in this country.
They are waging an all-out assault on the fundamental principles of this country.
I guess that's how we make America great.
Make it look like that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}