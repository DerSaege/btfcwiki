---
title: Let's talk about Trump, Biden, swing states, and polls....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=V-mE7LFGOLw) |
| Published | 2020/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of swing states and their importance in U.S. elections.
- Mentions that swing states are where Democratic and Republican votes are close.
- Lists the swing states for the upcoming election.
- Points out the importance of voter turnout in determining the election outcome.
- Stresses the significance of states like Nevada, Wisconsin, and Pennsylvania for Biden to secure a landslide victory.
- Recalls the surprise in the 2016 election due to negative voter turnout and polling inaccuracies.
- Emphasizes the need for voters, especially in swing states, to participate in the election.
- Talks about the limited paths to victory for Trump based on current polling.

### Quotes

- "Swing states are states where the population of likely Democratic votes and likely Republican votes are close."
- "If you're a Democrat in those states and you want that landslide, you need to go vote."
- "If people don't go vote, the polling doesn't matter."
- "Georgia, it's amazing that Georgia's in play."
- "They're not really set in stone to go to a particular party because of the population and the way they vote."

### Oneliner

Beau explains swing states, their impact on elections, key states for Biden, and the importance of voter turnout in determining outcomes.

### Audience

Voters

### On-the-ground actions from transcript

- Go vote in swing states (suggested)
- Keep track of polling data (suggested)

### Whats missing in summary

In-depth analysis of specific polling data and trends.

### Tags

#SwingStates #USPolitics #Election2020 #VoterTurnout


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to answer two questions
that have the same answer, different questions,
but they're incredibly related.
Okay, so the first question is, European here,
what's a swing state is what the question is.
It's phrased in a very colorful way, though.
And the second question is,
what states are most important for Biden to secure
to get the landslide?
The answers are the same.
The states involved are the same.
To understand what a swing state is,
you have to know the United States
works on something called the Electoral College.
The way this works is if you get the popular vote
in any state, you get all the electors for that state.
You need 270 electoral votes to win the presidency.
There are exceptions to this.
I want to say Maine and Nebraska have it divided,
divided up into areas.
So if you get all of, or the popular vote in one area,
you'll get the electoral votes for that area,
and then there's another.
But in most states, it's statewide.
Some states say for Trump, West Virginia and Wyoming,
they're going to vote for him.
He has those states on lock.
It would be incredibly surprising
for either of those states to go to Biden.
For Biden, Hawaii and California,
like he doesn't need to campaign in these states.
He probably has 60 to 70% of the vote.
So he's going to win the popular vote.
He's going to get the electoral votes from there.
Swing states are states where the population
of likely Democratic votes
and likely Republican votes are close.
They can swing to either side.
Currently, the swing states are,
now by some polls, Texas might swing to Biden.
I would be amazed if that happened.
That would be really surprising to me.
Taking that one out,
the swing states are Georgia, Ohio, North Carolina,
Arizona, Florida, Pennsylvania, Wisconsin, and Nevada.
Those are the swing states.
Which ones are most important for Biden to secure
to get that landslide?
The same states.
If you're a Democrat in those states
and you want that landslide, you need to go vote.
I would suggest most important for Biden
would be Nevada, Wisconsin, and Pennsylvania.
Those are the ones, if he wins those,
which he is favored to win, he gets over that 270.
Now, again, this is all based on polling.
And if people don't go vote, the polling doesn't matter.
And there are a lot of things that can influence the polling
and how accurate it is.
I have the strong belief that the reason 2016
was such a surprise to so many people
was because there was a high amount
of negative voter turnout.
People who showed up not to vote for Trump,
but to vote against Clinton.
And a whole bunch of people looked at the polls
and saw Clinton way ahead.
And people in swing states were like,
yeah, I don't need to go vote.
She's got this in the bag.
And then she didn't.
I think those are the two things that led to a lot
of the confusion and a lot of the shock when Trump won.
So again, those states.
Georgia, it's amazing that Georgia's in play.
Just being in the South, the fact that Georgia
might go to Biden is really kind of surprising.
But Georgia, Ohio, North Carolina, Arizona,
Florida, Pennsylvania, Wisconsin, Nevada.
Those are the most important for Biden to win
if he wants the landslide.
As far as a possibility of Trump winning, by current polling,
there's not a lot of paths to victory for him.
He would have to flip one, two, three, four, five states that
are currently favored to go to Biden.
However, if people don't show up, that's possible.
So that's what swing states are.
I know from past experience that a lot of Europeans
believed that it was, they were called that because these were
the states that candidates would swing through on the campaign
trail, which is true.
That it is.
But it's because they can go to either party.
They're not really set in stone to go to a particular party
because of the population and the way they vote.
So there you go.
If you want to keep up on the polling, which
I suggest you don't, but if you want to,
there is a website called FiveThirtyEight that actually
puts out a little chart that would probably mirror pretty
closely what I just said.
It changes every day, but it has like a little snake,
like a little winding path to get to 270 votes.
And it helps people who are visual learners
understand how it works and what the swing states are.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}