---
title: Let's talk about the US Constitution and a slogan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=s_kq5bOcSQs) |
| Published | 2020/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes a conservative talking point about restoring the Constitution by giving Trump more power.
- Explains that the Constitution is essentially a permission slip from states to the federal government.
- Points out that the Constitution established a representative democracy with checks and balances.
- Questions the logic of restoring a document (Constitution) that failed to prevent one branch from gaining too much power.
- Argues that if you believe the Constitution failed, it might be better to create a new one instead of restoring it.
- Emphasizes that giving more power to one branch of government (like Trump) goes against the principles of checks and balances.
- Stresses the importance of reading and understanding the Constitution rather than blindly following it as an icon.
- Suggests that the Constitution allows for change through a legal process instead of resorting to extreme measures.
- Advocates for using the existing machinery for change within the Constitution to address issues rather than resorting to violence.
- Proposes ideas like ranked choice voting and abolishing the electoral college to address power consolidation issues.

### Quotes

- "The whole idea was to protect against tyranny. So if you are saying that it failed to do that, there's no purpose in restoring it."
- "If any branch was able to gain too much power, the Constitution failed."
- "If you believe the Constitution is a failure, perhaps you want a new one."
- "It's normally accompanied by some pretty extreme rhetoric."
- "You don't need to reach into your gun cabinet if the Constitution has an issue."

### Oneliner

Beau questions the logic of restoring a Constitution that failed to prevent power imbalances and suggests considering new ways to address governance issues instead.

### Audience

Citizens, Voters, Activists

### On-the-ground actions from transcript

- Use the existing machinery for change within the Constitution to address issues (implied).

### Whats missing in summary

Exploration of how the Constitution can be effectively adapted and improved through legal processes and civic engagement.

### Tags

#Constitution #ChecksAndBalances #PowerBalance #LegalProcesses #CivicEngagement


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a talking point, a slogan, a slogan I have heard all
my life.
I remember hearing this in the 80's.
Didn't make any sense to me then either.
It's a conservative talking point that gets drug out every once in a while and it goes
through various versions.
The current version is that we need to restore the Constitution by giving Trump more power
so he can do it.
Sometimes this is phrased as restore the republic as well.
It gets drug out.
Over the years there were different boogeymen.
Sometimes it's Democrats in the Congress.
Sometimes it's activist judges on the Supreme Court.
Sometimes it's a Democratic President.
Whoever it is is actively attempting to subvert the Constitution and we have to stop that
because the Constitution is sacred and we have to restore the Constitution, restore
the republic.
Got it.
What is the Constitution?
When I have asked this question, when this little slogan comes up, I always get the same
deer in the headlight look.
What is it?
It's permission slip from the states to the feds.
This is what you're allowed to do.
What it is.
Aside from that, yeah, it set up a representative democracy that was a republic.
It enshrined property rights.
It established, you know, some basic freedoms that were supposed to be unalienable.
But what are you taught in civics?
What's the most important thing that the Constitution did?
Even today, as lacking as civics classes are, you learn this at a pretty early age.
It established three co-equal branches of government and checks and balances, right?
So what it did, executive, legislative, and judicial, three co-equal branches of government
and checks and balances.
So none of them could get too much power.
So follow me on this.
When you say we have to restore the Constitution because Branch X got too much power, what
you are saying is we have to restore a document that failed to do its job.
Because it failed to do its job, we have to go back to it.
That doesn't really make a whole lot of sense, to be honest.
If you believe the Constitution is a failure, perhaps you want a new one.
I don't know why you would want to restore the one that failed.
Doesn't make sense.
That's why it's important to actually read the documents you hold up as icons.
Aside from that, the current version of this, giving Trump more power so he can restore
the Constitution makes even less sense.
We want to give a branch of government more power.
The one thing the Constitution was pretty explicit about.
We don't want that co-equal branches of government.
We want to subvert the Constitution and give Trump more power so he can restore a document
that is designed to limit his power.
Sounds like you've been tricked.
Sounds like you may have never read the document or you didn't understand it and somebody came
along and used the fact that you view it as an icon, as something that is sacred, something
that can't be changed, used that to motivate you, to manipulate you.
The idea of restoring the Constitution doesn't make sense.
If you're going to give a person more power, you are subverting the Constitution.
If any branch was able to gain too much power, the Constitution failed.
Aside from that, to me the Constitution is actually a pretty good framework.
It had a lot of problems.
It had a lot of problems in the beginning.
It still has some.
However, one of the genius parts about the Constitution is that it includes the machinery
for change.
If something was wrong, it can be fixed within the Constitution through a legal process.
The reason I say that is because when this slogan gets brought up, when people start
chanting about restoring the Constitution, it's normally done by the type of folk who
got picked up in Michigan recently.
It's normally accompanied by some pretty extreme rhetoric.
That doesn't make sense.
If you have read the document, if you understand it, you understand the machinery for change
to deliver on the promises made, you know, that one day we the people would actually
mean we the people, all of us.
It was included.
That machinery is there.
You don't need to reach into your gun cabinet if the Constitution has an issue.
You need to use the machinery that exists.
If it is that far out, if it is that faulty, if it has failed, if you believe that, it
makes no sense to restore it because it didn't do its job.
The whole idea was to protect against tyranny.
So if you are saying that it failed to do that, there's no purpose in restoring it.
It's time to get a new one.
Or we could use the machinery for change and maybe alter the way things are done.
Include ranked choice voting.
I don't know, get rid of the electoral college.
Get rid of the things that consolidate power in the hands of the few because that's the
real issue, right?
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}