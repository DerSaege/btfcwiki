---
title: Let's talk about Trump's endorsements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JWgtwghlRfg) |
| Published | 2020/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring Donald Trump's endorsements and the different perspectives behind them.
- Mentioning the opposition's endorsement of Trump due to their belief they can manipulate him.
- Not focusing on the easy targets and low hanging fruit of Trump's endorsements.
- Acknowledging two groups who sincerely believe Trump represents their interests.
- Detailing the left's belief that Trump's failure will pave the way for a revolution.
- Describing the right's shift from anti-government to pro-Trump foot soldiers.
- Not understanding the ideological transition on the right but recognizing their sincere beliefs.
- Both groups on the left and right see a Trump victory as beneficial for their goals.
- Pointing out that Trump's strongest supporters are those who want to see the country divided.
- Ending with a reflection on the impact of Trump's supporters and their goals.

### Quotes

- "They're people who believe they are looking out for the best interests of this country."
- "Both sides believe that. It's interesting to me that the president's strongest supporters are those people who want to watch the country tear itself apart."

### Oneliner

Exploring the sincere but misguided beliefs behind Donald Trump's endorsements, revealing how both left and right supporters see his victory as aiding their goals.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Support political candidates who genuinely represent your values and interests (implied)
- Engage in constructive political discourse with those who have differing views (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the diverse motivations behind Trump's endorsements and encourages reflection on the impact of such divided support.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Donald Trump's endorsements.
And I know, y'all are waiting for the jokes.
You're waiting for me to lead in to talking about how the opposition of this country for
like two decades kind of endorsed him and said that he was wise and sane because they
can play him like a fiddle.
Or maybe bring up the fact that pretty much every nation on the planet that is in opposition
to the United States wants him to remain president.
I mean that would be something worth bringing up I guess.
Or the niece, I mean there's that too.
But that's not actually what I want to talk about.
Mainly because that's low hanging fruit and I think there's going to be a whole bunch
of people talking about it.
There's another group of people who support Donald Trump.
Two groups really.
And these groups, however misguided they may be, they believe that Donald Trump best represents
their interests.
They believe that he is the president who in a second term can give them what they desire.
And these are people who as wrong as they are, are very sincere in their beliefs.
They think that's the way to go.
They're people who believe they are looking out for the best interests of this country.
They're wrong but they believe that.
And it's bipartisan.
It's people on the left and the right who support Donald Trump if they believe that
armed conflict is necessary.
See on the left it's those people who believe the United States is too far gone.
That it can't be fixed through normal means.
They believe that Donald Trump is going to be a large enough failure to facilitate it,
to make it more palatable to stage the glorious revolution or whatever.
They support Trump.
He likes to cast himself as the opposition to the left.
The reality is the left that he thinks, I guess he believes they're Democrats, but they're
much further to the left than Democrats.
They support him.
They want him to win because he's so bad that it keeps people mobilized.
And they believe that he is so incompetent that he will drive the country down, making
it easier for them.
And then on the right it's those people who were like staunchly against the government
just a few years ago but now all of a sudden are all for it and want to be like foot soldiers
for the empire.
I don't really understand that transition but I am certain that means that they are
on really solid ideological ground.
But either way, both these groups are sincere in their beliefs.
They're wrong but they believe it.
And both of them want Donald Trump to win because it helps further their goals.
Will be easier to remake the country under Donald Trump.
Both sides believe that.
It's interesting to me that the president's strongest supporters are those people who
want to watch the country tear itself apart.
Anyway it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}