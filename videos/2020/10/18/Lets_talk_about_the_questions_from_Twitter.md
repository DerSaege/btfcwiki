---
title: Let's talk about the questions from Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xA0x7Ul_t7M) |
| Published | 2020/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau held a live stream where he answered questions from Twitter, but ran out of time and didn't get to address any.
- Beau decided to answer some of the questions in an off-the-cuff manner.
- He discussed the media's use of soft language like "meddling" instead of more accurate terms like "Russian intelligence operation."
- Beau shared his views on why some Americans still support Donald Trump, citing reasons like reluctance to admit being wrong and resonating with certain talking points.
- He talked about having civil, fact-based debates with those who disagree with you, focusing on sticking to objective facts.
- Beau expressed his belief that political engagement due to Trump's presidency could lead to a more progressive outlook in the country.
- He addressed the possibility of Trump's followers turning violent if he loses the election.
- Beau discussed the progressive stance on Biden's lack of progressiveness, acknowledging that while Biden may not be ideal, he is still better than the alternative.
- He shared his thoughts on whether Trump will leave the U.S. if he loses the election, expressing skepticism about potential legal consequences for Trump.
- Beau talked about the American perspective on public healthcare and the resistance due to the fear of government control.
  
### Quotes

- "It's a choice of language in which I think the media outlets are trying to elicit an emotional reaction when a rational one would probably better serve the country."
- "Most people watching this channel, Joe Biden doesn't represent the change you want. However, he's definitely better than the change you're going to get from Trump."
- "I think everybody should have a wide skill set. I don't think and understand when I say this I'm not necessarily talking about because what may happen in a few months."

### Oneliner

Beau provides off-the-cuff responses to questions on media terminology, Trump's support, civil debates, Biden's progressiveness, and public healthcare.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Reach out to people on social media platforms like Twitter to start community building efforts. (implied)
- Stay politically engaged and push for progress regardless of election outcomes. (implied)

### Whats missing in summary

Insights on how to maintain political engagement post-election and foster inclusive representation in political platforms.

### Tags

#Media #Trump #Biden #PublicHealthcare #PoliticalEngagement #CommunityBuilding


## Transcript
Well, howdy there, internet people.
It's Bo again.
So earlier today, I did a live stream.
And prior to doing it, I asked for questions on Twitter.
During the live stream, I got to exactly zero of them
before time ran out.
So I'm going to answer some of them now.
Have a laptop in front of me.
I haven't looked at the questions yet.
So you're still getting kind of an off-the-cuff answer.
OK.
So, let's see, why do you think the media chooses soft language, i.e.
Russian meddling over interference, Trump's corruption for trips to his
golf course become conflicts of interest?
I think they're trying to elicit an emotional reaction.
The term meddling is vague and it can allow you to assume the worst.
The problem is what we're dealing with in a lot of cases is the worst.
I think it would be more effective to say Russian intelligence operation, rather than Russian meddling.
It's a choice of language in which I think the media outlets are trying to elicit an emotional reaction
when a rational one would probably better serve the country.
Why do so many Americans still support Donald Trump? I don't have a clue. I don't have a clue.
I think a lot of people are reluctant to admit they were wrong initially, and I think some
people listen to the talking points, and those talking points resonate with them, rather
than look at what he actually accomplishes or fails to accomplish.
How do friends, neighbors react to your opinions, since they're often public, and how do you
approach a civil fact-based conversation with those who don't agree with you.
I've always been very open about my political opinions.
By this stage in my life, most people I interact with know what they are and they don't bring
it up normally if they disagree because I don't mind talking to people about it and
And I will carry on a conversation about it.
And most times, people don't really want a conversation.
They don't want a rational discussion about events.
They want reinforcement of their own beliefs.
And that's not something I'm known for.
So normally, people don't start those conversations with me.
I mean, you described how to do it.
How do you approach a civil fact-based conversation
with those who don't agree with you.
When you're talking to people who don't agree with you, stay civil as much as
humanly possible, and stick to the facts, things that you can objectively prove.
It's easier now that you have a cell phone in your pocket and you can be like,
no, you're wrong.
Here you go.
Can we acknowledge that Trump has brought about renewed civic discourse?
Yeah, absolutely.
I truly believe that if we realize that Biden is not a savior and is just a stopgap, and
we continue the amount of political engagement that we currently have in this country because
of Trump, oddly enough, he may be a net good.
He may be responsible for pushing people to more progressive outlooks because he did rip
the facade off of the American dream and revealed the nightmare that's underneath for a whole
lot of people.
So I think that that would, I think in a way he may be a good thing as long as he can be
stopped before he comes a really bad thing, you know?
If Trump does in fact lose, do you think his followers will turn violent?
I think some will, yeah.
I think it'll be sporadic and short-lived.
During the live stream, somebody brought up the years of lead and I, yeah, I really hope
it doesn't turn into that.
I think it would be a short flare-up, and I think the response would be pretty significant.
So I don't, I'm hopeful that it wouldn't be a protracted thing, but we'll have to
wait and see.
There is a, no matter how small, it is a realistic possibility that things get out of hand.
Why are some progressives so focused on how Joe Biden isn't progressive enough for them
they'd rather see Trump re-elected than vote for Biden.
There are people who want to accelerate things.
I'm not one of them.
I think there's a lot of people who don't want to delude themselves into thinking that
Joe Biden represents their interests, because he doesn't.
Most people watching this channel, Joe Biden doesn't represent the change you want.
However, he's definitely better than the change you're going to get from Trump.
I think that he's more easily countered.
I think it would be easier to push more progressive policies forward.
I don't think he's progressive, but I don't think that he would stand in the way of progress.
So I think for those who draw attention to the fact that he isn't progressive, I think
it's them wanting to remind people that this doesn't end on, you know, at the inauguration.
We have to continue to stay politically engaged.
And I'm definitely one of those people.
Let's see, 45 said he would leave the United States if Biden was allowed to win.
Yeah, good.
But you know, and it goes on, this goes on to talk about possible indictments and stuff
like that.
He's already got some waiting overseas.
That's the thing.
So I don't know if, I don't know what he's going to do.
he leaves good. I don't have a lot of faith in the American justice system
bringing a former president to trial. So short of that, you know, don't go away
mad, just go away. Do I think Trump will actually leave the United States if he
I don't know.
See, realistically, the idea that's being floated by a lot of people is that he wants
to do this to get away from prosecution.
I think it was an offhand comment that he made, and I don't think he's...
I think he's too arrogant to believe he would lose in court.
So I think he would actually stay and try to fight.
You know?
EU dude here. How can Americans seem not to comprehend, see the benefits of public free
universal health care? Okay, so your average American, most Americans understand that that's
like super important. The problem is those with money don't because it doesn't benefit
them. I would be willing to bet that most Americans, I actually don't remember seeing
any polls on this, but anecdotally, I would suggest that most Americans are very clear
on the fact that everybody needs health care, and I think that most Americans would be okay
with using tax dollars for that.
For those who are in opposition, I think a lot of it has to do with the idea of letting
the government have control of something, because that's part of the American mythology
is that we're very individualistic.
We're not really, but we like to pretend that we are,
and therefore we don't want the government
to have control over healthcare.
That's the idea, anyway.
Help me convince a 31-year-old male
who wants to ride in Nader to vote for Biden.
I don't actually, I don't tell people how to vote.
I don't endorse candidates.
I don't even tell people that if they don't believe in voting, they should vote.
I understand the desire to try to convince everybody to vote for your chosen candidate.
The reality is their interests may not line up.
They may not be able to stomach that.
It may not be something they can do in good conscience.
At the end of the day, I know that people like to say, a vote for anybody but Biden
is a vote for Trump.
That's not true.
Any vote that isn't for Trump is a vote against him.
I'm a huge fan of the, in fact, I participated in it the other day, a huge
fan of the strategy to convince small government conservatives to vote for
Joe Jorgensen, because it's pulling votes away from Trump and therefore would
make that landslide between the popular vote totals of Trump and Biden even more
pronounced.
This is the opposite.
This is taking one from Biden.
But, I mean, there's a reason voting's done in secret, I don't like doing that.
I think it is important that Trump lose in a landslide, and that's really all I feel
that I can ethically say.
Do you think a lot of the early voting is done by Trump supporters following his command
to vote multiple times.
I don't know.
I think that there are a whole lot of people who are genuinely just tired of Trump.
And I think that may be motivating at some.
Do you think Hunter Biden is objectively cooler than Trump's sons?
Well, yeah.
I mean, even if scandals are cooler.
So, yeah, absolutely.
How do we keep focus on social economic equality once Trump is out of office?
I don't think that's going to be a problem because Biden isn't magic.
All of the issues that currently exist, they're still going to be there and they're
going to be exacerbated and laid bare by the public health thing.
So I think they're going to be very apparent for a lot of people.
I'm just hoping that people still stay politically engaged.
That's my big fear.
Like my one fear about a Biden win is that people believe everything's going to go back
and everything's going to be fine.
The reality is it wasn't fine before for a whole lot of people.
This just exposed it.
Now we have to fix it.
Do you think the brass interprets the election results for themselves and if so, and Biden
wins and tries anything, will guard units and federal agents follow NCA?
That's a tough question to be honest.
I don't, I don't know.
I think that that would, if it devolves to that point first, we're in a lot of trouble.
If it gets to that point where commanders are having to make those decisions, it's
a sign that things are going to get really bad.
And as far as I'm concerned, if I were you guys and it came to that point and commanders
were making the decision whether or not to side with Biden or Trump, that's not a cue
to become politically active, that's a cue to get out.
That's a cue to leave, because it is going to get bad.
At the end of the day, I think it would be commander by commander basis.
If it falls to that point, it's going to be done by individual commands, and it would
probably split."
Yeah, I can't read that.
What is your ideal country?
My ideal country?
I have a video called Let's Talk About the World I Want or something like that.
That's my ideal country.
maximum amount of freedom and the maximum amount of cooperation between people.
Let's see, there are a lot of stories floating around about possible GOP plans to nullify
popular vote in key states by using faithless electors. I would,
I think that Republicans have wisen to the concept that Trump is dragging them down.
I don't know that they'd go along with it.
I don't know why I continue to have faith in these people.
I have no logical reason to believe that all of a sudden they would grow a backbone other
than the fact that now it's in their political interests. So, it's a possibility, but I
don't think it's likely. Why don't you follow me back? My social media follows and
friends and stuff like that. It's almost random, so I wouldn't read too much into
that. If you can think of any, what advice would you give to progressives who want to run for any
political office? Hide your power level. I would not come out swinging for the fences and saying
the most radical thing you can think of.
You can talk about it, you can say you support it, but I wouldn't suggest saying, hey,
this is the legislation I'm going to introduce.
Let those people who are on the ideological front lines carry that.
Your goal as a legislator would be to get to that point as quickly as possible with
and small enough steps that will keep you in office.
if that's the goal you're going for and your idea is to progress things, we want you in
office as long as possible.
So you don't want to propose legislation that might turn your constituency against you.
Okay, so I'm sure there's going to be a couple questions about this.
This is, I'd love to hear your take on Trump's claim about the Marshalls in Portland and
what they did.
I don't believe what Trump says, period.
When he says something, I don't accept it as fact.
I don't want to accept something as fact simply because I believe it may be true.
And this time it's something that to me seems more plausible.
It's still Trump.
He's still a liar.
I can't accept his word, you know.
He said that they didn't want to arrest him, that they were going there to do this,
and made it sound planned.
I need evidence to back that up.
Trump's word is worth nothing to me, and that's whether or not I believe it's something
that's possible or him saying something that I don't think is possible.
It means nothing.
It carries no weight to me.
like or dislike? I guess that would depend on what it's going on, but
generally dislike. What is your favorite book? All of them. All of them. See, that's
a hard question. That's like asking for a favorite movie. It depends on what
you're... what you're in the mood for, what you're trying to learn, stuff like that.
I just I don't I'd never have an answer to that question. When it comes to the
question of stacking the court, what are your thoughts on just removing the last
two justices? Yes, yeah. Now I'll be honest, up until they decided to nominate
somebody who couldn't identify the elements of the First Amendment, I
actually wasn't in favor of anything radical. Once, not just did they nominate
her, but now it seems like they don't care that she doesn't know what's in the Constitution,
all bets are off.
If they want to add judges, they want to reduce judges, yeah, you're not going to hear a complaint
from me.
They acted in bad faith, so I no longer care.
They took the gloves off as far as undermining the Constitution.
All right, fine.
What's the clearest physical surrounding indication that we're right for fascism?
The fact that people don't realize that it's kind of already here.
I think that has a lot of warning signs in it.
How do we reduce political division after the election?
Facts.
After the election, we can't use emotional rhetoric anymore.
We have to use cold, hard truth.
This is what Trump told you.
Here's the truth.
He was wrong about this thing that can be objectively proven.
His opinions are probably wrong too.
I think we're going to have to focus on things that were just completely
indisputable and let the chips fall where they may from there.
What is your beard grooming routine?
I don't have one.
Like Dove soap, I don't, the reason I have a beard, it's not a fashion statement.
I'm just totally, I'm like half retired and don't want to do anything.
So it's really kind of that simple.
Let's see.
What would you say to people who vote Republican to protect their second amendment rights and
justify with the to guard against tyranny argument?
Yeah.
How's that worked out this time?
As far as the second amendment argument, I think the strongest argument to put forth
when somebody says, oh, I'm going to vote for Trump because of the second amendment,
I would point out that the Obama-Biden administration was more pro-gun than Trump ever was.
And that's objective fact, that can be proven.
The Obama administration put military surplus arms on the market.
The Obama administration expanded concealed carry, a whole bunch of stuff.
I would stick to that.
Trump's record on guns is actually pretty bad.
I don't know why people think he's a savior of the Second Amendment, he's not.
He has expressed open support for things that under normal circumstances, the Second Amendment
crowd would just immediately reject.
How bad of a crime do you believe bad cops would have to commit to finally make those
mentally enslaved to authority wake up?
I don't think there's such a thing.
I don't think that's what's going to do it.
There have been plenty of really bad incidents involving law enforcement.
If it was something, if it was them doing something negative that was going to wake
people up, it already would have happened.
I mean, there's countless incidents and none of them have worked.
It's going to take education.
Let's see, I already answered that.
Can we place term limits on Congress?
Yeah, but you've got to get Congress to do it, and that seems super unlikely.
Here's a list of things I'm curious about.
Please pick and choose at your discretion.
What do you feel like people misunderstand about messages you try and get across the
most?
I don't know.
I have been accused of many things in my life.
Being inarticulate is not one of them.
I think that, I hope, I make my points pretty clearly.
What's your biggest but most grounded fear for the next six months?
That Biden wins and everybody thinks everything's fine all of a sudden, and we lose the political
engagement that we have.
What platform?
YouTube.
It was on YouTube, but you've definitely missed that.
Another comment drawing to, oh, same person, okay.
Let's see, do you think we can realistically get rid of the Electoral College and if so
how?
If the Democrats win, they might want to.
However, I would caution people, I think it's a good idea to get rid of the Electoral College
at this point, however, I think we need a higher threshold.
If we're going to get rid of the Electoral College, we should probably require a presidential
candidate to get way more than just 50% of the vote.
It can't just be a simple majority, maybe make it 60.
And that would help safeguard those in cities from the rural vote and those in rural areas
from the city vote.
That way no matter how the demographics play out in the future, it's unlikely that it's
It's going to divide more than that.
Removal of the Electoral College.
Thoughts on the People's Party.
It's a good start.
It's a stepping stone as long as it doesn't fall apart immediately after the election.
That's one of the things about third parties is they don't put in the effort to build
the ground game when there's not an election.
the pre-fund mandate on the Postal Service. Yes, that's a good idea. I'm not sure.
Let's see. What do you think of Trump saying he'll leave the country?
Yeah. I think it's... Oh, do I think it was a whistle to Putin or other allies?
I don't know. I don't know. I would hope that he understands that, let's just say
that everything's true. He's an agent of influence, and Putin is an ex-intelligence
officer. At the end of the day, when the agent of influence no longer has influence, it doesn't
do the intelligence officer any good to protect them, to be honest. If that's the
scenario that's playing out, Putin probably isn't going to help. That's the danger of
being a reality TV show host going toe to toe with one of the 20th century's greatest
intelligence officers. You're probably out of your league. What are the most likely outcomes
after a Biden win? I would hope that he tries to stick status quo, which is what he's promised
is probably what he's going to try to do is just return things to Clinton Bush Obama.
That's what he's going to try to do.
I would hope that there is a Senate and House that's going to push him further.
Can you talk more about how to start the community building stuff when you have no relationships
and the community around you.
Okay, so you are on Twitter.
There's a tweet that I have penned that has every state,
and basically click on your state, like below it,
you can click on your state,
and then scroll all the way down.
There's a whole bunch of people in that state.
Maybe there's somebody near you.
Reach out to them over Twitter, exercise good caution.
Those people are not vetted at all, okay?
And so, just, it's a stranger from the internet and all that.
Maybe discuss the Reddit thread that talks about Canada sending troops to the border
to help refugees after the election.
I didn't know that was a Reddit thread, but yeah, I mean, realistically, it's a possibility.
again, I'm hoping none of this is likely. What do you think comes after a Trump
administration if he wins a second term, a third, and then a fourth? If he gets a
second term, I don't think there's going to be a... I don't think it's going to
proceed as usual after that.
Fully expect to encounter Trump poll watchers on November 3rd.
What are your suggestions on how to handle that situation?
Wear nondescript clothing.
Don't engage.
If you have the ability, if you're somebody who looks like me,
wave and smile.
Just most of them have no clue what.
This isn't a country where intimidation at the polls
occurs in large numbers.
Most of these guys are going to have no clue what they're
really supposed to be doing.
They're out there playing GI Joe dress up.
They're looking for guys dressed in all black, that
type of thing.
I don't.
I think the overwhelming majority of them that show up
are just going to be there to stand around.
I would try to avoid them.
And if not, defuse the situation.
If you can't, defuse it any way you have to.
Any way you have to.
And that means if it takes saying, yeah, voted for Trump,
do it.
You don't owe them honesty.
They're not being honest with you.
Their goal there is to intimidate you.
They just don't know how.
How certain are you of a Biden-Harris win in the blue wave for other elections?
I am reasonably confident that Biden is going to win.
at the polls in the different states and what it takes to win get 270, 270 electoral votes,
I think that's incredibly likely.
Whether or not he's going to win in big enough numbers to create that landslide where
we don't have legal battles and stuff like that afterward, that I'm not sure of.
Um, I do think he'll get 270 though.
Let's see.
What do you think is in store for us from now until January of Biden wins?
Trump lame duck.
He could do a lot of damage.
I don't, I don't know.
Um, a soup recipe.
Somebody asked me to put out like a redneck cooking book.
And I might actually do that at some point if I ever get time.
What's your favorite fast food?
Taco Bell or Crystal.
Should corp packing be a goal?
Again, I don't have an objection to it anymore.
Who's your favorite Ninja Turtle?
Donatello, the one that built stuff.
Let's see, super salad, salad, what branch of the military did you serve in?
This comes up all the time.
I was never active duty.
I was a private consultant, contractor, whatever term you want to use.
What's the best piece of advice you've ever received?
You're not going to make it?
Had somebody not given me that advice, probably wouldn't be standing here right now.
One of those things where accepting the inevitable prevents it.
How do we respond to Trumpism once he's out of office?
I would suggest getting rid of anybody who helped, who pushed policy.
Making sure that they can't get another position.
They can't get another government job.
aren't re-elected. I do believe that we have to engage in
detrumpification. There are going to be some people who were just following
orders and at this point there are very few of those who I think warrant
criminal prosecution. There are a couple but not many. But the idea would be to
get rid of the the ideology that enabled him and that takes more education than
anything. Let's see, what are reputable sources for researching validating news
stories? That's going to depend on the news story. A lot of times I like to go
to trade magazines. You can find out a lot of information about various things
by looking at magazines that are or outlets online that are written
specifically for a group for a trade that that's a good place to get a lot of
information that doesn't show up in mainstream news additionally what types
of information do you trust? Trust but verify everything. I don't remember if you ever
went into this in your vids, but what got you into journalism? So I was a consultant,
a contractor. My skills were gathering information, documenting stuff, distilling it, explaining
it and shooting a gun. You remove the gun and I'm a journalist. That's what happened.
It was a skill set I had and it was something that I was interested in because I thought
it could affect change and that's how I wound up here.
I don't trust Trump to step down if he loses.
I don't think many people do.
Let's see.
Black men seem to be finally waking up to the fact that no party actually cares
about them and are starting to demand representation in these party platforms
if they want our vote.
What can Dems do after not including us much up to now to get these votes?
You know, I was actually thinking about this today, it's interesting to me and it's something
I'm probably going to delve into in a future video, how when you look at the parties, generally
speaking it's white folk talking about what's best for black folk and you don't have a lot
of black folk in the conversation, that's probably not a good way to solve the problem.
I think that, I think any party that wants to actually earn votes from black people should
probably have black people in their upper echelons.
And I don't mean those who are, I mean people that are representative of the black population.
I don't think that, you know, the person who went to Yale who, you know, had a trust
fund when they were born is necessarily going to have the answers.
I think you need people from the ground level.
And again, this was something I started thinking about earlier
today.
And there's probably going to be a long video about this
coming up, as soon as I can kind of get my thoughts together
on that one.
OK.
Let's see.
Should the US rewrite its constitution
and turn to having a parliamentary system.
I don't know.
I mean, it's,
when you talk about giant ideas like that,
the devil's in the details.
You know?
I mean, that could be a good thing or a bad thing,
depending on how it shakes out.
Talk about ranked choice voting.
Generally, I think it's a good idea.
I think that we need to make it
easier for people to vote their conscience
and not have negative outcomes because of that.
And I also think we need it to be harder to become elected.
I think that raising the threshold, where 50% isn't
enough, you need more than that.
So you know from the beginning that, at the very least,
the person can build a coalition.
The person can get people behind them.
I think that's important.
Are you going live on Twitter or YouTube?
YouTube, a while ago, sorry.
But see, I didn't read these because I
wanted to be able to, anyway.
Can a former president permanently leave the US
after leaving office?
Yeah.
Yeah, they can.
Already talked about that.
Let's see.
What made you give up weapons?
professionally and or in your personal life.
I know there's a lot of theories on this.
And there's one in particular that people seem to think it's
related to something else.
It's not.
I'd actually started getting rid of all of my stuff before
then.
I had some PTSD issues.
And I basically had a friend at one point, like, yeah, I'm
taking this with me.
It was at that point that I probably realized I should start getting rid of all of them.
What's your favorite film?
Again, I don't know.
Favorite band, same thing.
What's the worst thing you think Donald Trump will do if he gets a second term?
a third term? What's your full political journey? Way too long to get into in this video. Let's
see. Do I? Wow. I'm going to rephrase that. Do I think that citizens should start training
in various ways. I think that everybody should have a wide skill set. I don't
think and understand when I say this I'm not necessarily talking about because
what may happen in a few months. I think generally everybody should every
community should be pretty self-sufficient and that's all the time
having nothing to do with the election. Any plans for a discord server for
subscribers if you are a patreon supporter we have a discord that's like
one of the few things patreon subscribers get so because I don't want
to put any content behind a paywall I want everybody to be able to access it
whether they have money or not and on the second channel if you're a patreon
sponsor just so you know you'll get early access to the videos when they start
coming out? Has Trump committed treason yet? I don't know. If he interfered, if the
bounty thing is legitimate and he interfered with that, that would be
adhering to an enemy. So maybe, but there's a lot we have to find out first.
Treason in the United States, I'm sure you all get tired of hearing this, but it
is incredibly specific. You almost have to try to commit treason in the
It's one of the few, it may be the only crime actually outlined in the Constitution and
defined in the Constitution.
How are state and local politics and attitudes changed over the past few months with the
governor?
No, people here, if you don't know, I'm in a red part of Florida and people here are
still referring to him as that idiot in Tallahassee. He is incredibly unpopular so that hasn't
changed. Can you make a video on how you or anyone could reach Trump supporters? After
the election I plan on doing a lot of them. There are going to be a lot of videos that
an upside-down patch where I'm leading in with something to draw people in, exploiting
that sunk cost fallacy, I don't think it's going to do any good to do it over the next
few days.
I don't think anybody's mind is really going to be changed anytime soon.
I'm seeing a lot of political analysis posts, which is cool, but win or lose, how
do we keep the moment for change going?
Some are worried a Biden win makes us complacent.
Yeah, me.
Some are worried a Trump win.
Well, you know, how do we keep eyes on the future?
We.
We do it.
We have to lead by example.
Those of us that are worried about that eventuality, we have to push harder than anybody else.
We have to keep people motivated and make sure they understand it doesn't end just
because Trump's out of office.
All the problems are still there, in fact more than when he took over.
How many t-shirts do you own and does your wife do your laundry?
A lot?
I don't know.
I've never counted.
a lot of t-shirts, and no, we, my wife and I split domestic chores, like it's not
like she does the dishes and I do the laundry, it's just when it needs to get
done, it gets done.
What do you think the chances of Trump fleeing the country are?
I think he's too arrogant.
I think he'll try to fight, um, any charges because he'll want to win.
Okay.
Thoughts on the long-term damage of the lockdown.
Um, I would hope that any new administration is going to understand
that it's not even the lockdown, even in areas where there wasn't one, like
here, there were economic impacts. I would hope that any administration coming in, anybody
understands that there was widespread economic damage due to the public health issue and
that it's going to have to be remedied and that remedy has to start at the bottom. If
they do it at the top, it is not going to work and I think they know that. And if they
don't they're going to end up finding out because there are a lot of issues
that are right below the surface right now and if they don't act it's going to
that they'll bubble over and they'll see the impacts in their stock portfolios
which oftentimes that's all they care about okay so there's a whole bunch of
answers same closing so the second channel is coming and it's gonna be long
format content, documentary style stuff, some of it's gonna be on the road, some
of it's gonna be like normal documentaries, and that's coming very
very soon, and other than that, you know, thanks for hanging in there with me.
Sorry I couldn't get to these in the live stream, but I had a limited amount of
time to work with.
Anyway, it's just a thought.
Y'all have a good night!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}