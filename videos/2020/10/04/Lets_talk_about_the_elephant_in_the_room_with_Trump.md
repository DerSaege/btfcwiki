---
title: Let's talk about the elephant in the room with Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KIoU-6hbQiM) |
| Published | 2020/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump administration's mismanagement spotlighted existing inequities in the US.
- President Trump prioritizes his interests over the country's best interest.
- Lack of temporary power transfer despite President's distractions.
- The focus on campaign over discharging duties raises concerns on who's running the country.
- Secret Service's primary duty is executive safety, yet the President overrides their advice.
- Concerns arise that the President is more focused on re-election than national safety.
- Pence hasn't taken over due to image concerns, leaving doubts on effective leadership.
- President's mismanagement is becoming increasingly apparent.
- Beau questions the President's ability to lead the entire country if he struggles with White House safety.
- Suggests that if the President managed the country well, campaigning wouldn't be a top priority.

### Quotes

- "The mismanagement that has occurred during this administration simply put a floodlight on a lot of the inequities that already existed."
- "If the president cannot provide for the safety, security, and dignity of the White House with a team of experts with a $2.25 billion budget, he probably can't do it for the entire country."
- "I think he's in one of those feedback loops where he's getting bad advice."

### Oneliner

Trump administration's mismanagement spotlights existing inequities, raising concerns about President's priorities and ability to lead effectively.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Question leadership priorities and demand accountability (implied)
- Stay informed on political actions and decisions (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the current administration's mismanagement and its impact on national interests, urging for a focus on effective leadership and accountability.

### Tags

#TrumpAdministration #Leadership #Mismanagement #Accountability #Safety


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about the elephant in the room.
We've talked about it numerous times on this channel. A lot of the issues that have arisen and
come into public conversation during the Trump administration are not really the Trump
administration's fault. The mismanagement that has occurred during this administration simply put
a floodlight on a lot of the inequities that already existed in the United States, existed
before him. And in a bizarre turn of events, that mismanagement led to the state that he's in and
that state that he's in has put a floodlight on his mismanagement. One of the overriding issues
with the Trump administration is President Trump's desire and practice of putting himself,
his interests, his image, and his re-election chances over what's in the best interest
of the country. That's how we wound up here. That's how he wound up where he is.
I think it would be fair to say that the president is currently not capable of fully
discharging the duties of his office. He's got a lot on his plate. He's distracted. And that's
fine. It's completely understandable. However, there's been no temporary transfer of power.
Aside from that, aside from the completely understandable distractions
that exist because of what he's going through, he's focusing on his campaign.
Between the understandable distractions and his campaign, who's running the country?
Who's discharging the duties of his office?
They're sitting there creating PR photos and then mishandling that.
And that campaign, his entire campaign is the idea that he is going to provide for the safety,
security, and dignity of the United States. That's his thing. He's going to keep everybody safe,
tells them what to be afraid of, and says he'll protect them from it. That's his campaign.
The reality is the Secret Service has a budget of about two and a quarter billion dollars a year.
Their primary duty is to provide for the safety, security, and dignity of the executive.
In order for him to end up where he is right now, he had to overwhelm the
order for him to end up where he is right now. He had to override them.
To go against their advice, their experts, ignore their procedures and their protocols.
He can do it because he's the chief executive.
He had people telling him it wasn't a good idea, but he ignored it because he believes he knows
better. At this moment, the nation doesn't really have a president. They have somebody with that
title who is not fully discharging the duties of their office and who is more concerned with
their re-election chances. That's why Pence hasn't taken over, is because he's afraid of the image
and how that will impact him. He's more concerned with the campaign. I'm going to suggest that if
the president cannot provide for the safety, security, and dignity of the White House
with a team of experts with a $2.25 billion budget, he probably can't do it for the entire country.
I think he's in one of those feedback loops where he's getting bad advice.
I think the longer this goes on, the more apparent his mismanagement is going to be.
The more floodlight is going to be directed at these issues. I would also suggest that if the
president was running the country half as well as he pretends to be, he wouldn't need to campaign.
It certainly wouldn't be at the top of his agenda. He would have more
important things to do, like the duties of his office.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}