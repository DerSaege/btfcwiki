---
title: Let's talk about Trump and adapting to a mystery....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3fFG6pRL4t8) |
| Published | 2020/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans face a mystery in explaining their higher impact by something compared to Democrats.
- The theories Republicans have come up with to explain their situation are not grounded in reality.
- Democrats were given a briefing that Republicans apparently did not hear, leading to different responses to the situation.
- The briefing emphasized washing hands, not touching faces, staying home, wearing masks, and social distancing.
- Republicans may have chosen not to hear the briefing or were explicitly told not to pay attention to it.
- Democrats, as a general rule, tried to lead by example by wearing masks in public.
- Republicans, on the other hand, often did not wear masks as a display of loyalty to Trump.
- Republicans tend to be influenced more by anecdotal information rather than studies and statistics.
- Beau urges Republicans to adapt to the new information they have received for the sake of their well-being.
- He stresses the importance of changing one's thinking in response to new information, especially when it can be a matter of life and death.

### Quotes

- "The proper thing to do when you receive new information is to change your way of thinking, to adapt to it."
- "Adaptation may literally be the difference between life and death."
- "Democrats were attempting to lead by example."
- "Republicans are generally not swayed by studies and statistics."
- "Are you going to adapt?"

### Oneliner

Republicans face a reality-check on their response to a situation compared to Democrats, urging adaptation over loyalty.

### Audience

Republicans

### On-the-ground actions from transcript

- Wash your hands, don't touch your face, stay at home, wear a mask, and socially distance (suggested).
- Brush up on the leaked briefing information available on government websites (suggested).
- Adapt to new information received for the sake of well-being (suggested).

### Whats missing in summary

The full transcript provides detailed insight into the contrasting responses of Republicans and Democrats to a situation, stressing the importance of adaptation and changing one's mindset based on new information. 

### Tags

#Republicans #Adaptation #Loyalty #Information #Masks


## Transcript
Well howdy there internet people. It's Beau again. So today we have some information for
Republicans. A certain subset of Republicans. We're going to start off with a quote.
It is not the strongest of the species that survives nor the most intelligent. It is the
most adaptable to change. That quote is attributed to Charles Darwin. I say attributed because,
fun fact, there's no real evidence that he ever said that. It appears to be a blend of things he
said over the years. You could say that quote evolved. Right now Republicans have a Scooby-Doo
mystery on their hands. They're trying to explain why they are more heavily impacted by something
than Democrats. And they have come up with some theories. And those theories are just awful.
Awful. They're not grounded in reality in any way shape or form.
The reality is, and I have the documents to prove this, that Democrats were given a briefing
that Republicans apparently just didn't hear. They didn't know, I guess. And I don't know if
this briefing was top secret compartmentalized information or what. But we're going to go
through it now so everybody knows. Wash your hands. Don't touch your face. Stay at home.
If you have to go out, wear a mask. Socially distance. I don't know why Republicans didn't
hear this. Maybe they didn't want to. Maybe they were told not to hear it. But they certainly
didn't act on it. If that briefing is too complex, it has been leaked from the secure mountain
in which it was given in. And it's on pretty much every government website. I would suggest
people brush up on it. And I know Republicans are going to point to the instances in which
some Democrats were caught without their mask. Think about the way that is said,
caught without their mask. Because as a general rule, Democrats were attempting to lead. And by
lead, I mean lead by example. So when they were in public around a lot of people, they were wearing
a mask as a general rule. Whereas with Republicans, the rule was not to wear one, because that's how
you show your loyalty to Trump. There are studies and statistics that can confirm the information
in this briefing. However, Republicans are generally not swayed by studies and statistics.
They're swayed by anecdotal information. You've got it now. You have it. Here is your anecdotal
information. Here's your story that you can use to highlight this. So now the question,
are you going to adapt? Or are you going to allow the loyalty and trust you placed in a man who has
admitted he lied to the American people while hundreds of thousands were lost, get the
way of that? You have new information. The proper thing to do when you receive new information is to
change your way of thinking, to adapt to it. In this case, adaptation may literally be the
difference between life and death. Please take it seriously. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}