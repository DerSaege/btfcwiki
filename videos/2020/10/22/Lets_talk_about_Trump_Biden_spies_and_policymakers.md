---
title: Let's talk about Trump, Biden, spies, and policymakers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7e9tVGGppvY) |
| Published | 2020/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the importance of journalism and freedom of the press.
- Addresses the issue of people seeking facts that only reinforce their preconceived ideas.
- Explains the purpose of spies in gathering information for policymakers to make informed decisions.
- Emphasizes the power of knowledge and the role of journalists in keeping the public informed.
- Advocates for being well-informed to guide policy decisions in a democracy.
- Stresses the significance of journalism in free societies where people are meant to lead.
- Compares journalists to spies feeding information to the public.
- Urges the audience to pay attention to international news to form their own opinions before spin begins.
- Points out the influence of money in shaping the narrative and the importance of being informed to counter it.
- Concludes with a reminder of the importance of staying informed about global events for future decision-making.

### Quotes

- "Journalists, they're your spies, feeding you information so you have a better grasp of things that you can't experience firsthand."
- "Learning about events that are occurring overseas and understanding them ahead of time before the politicians can put a spin on it, that can help save lives."
- "Because you're probably going to need to know about it in February. And by then, the spin will have already started."
- "That's why journalism is so important in free societies, in societies where the people are supposed to lead."
- "And in this case, money is power."

### Oneliner

Beau explains the vital role of journalism in informing the public to guide policy decisions, comparing journalists to spies feeding critical information.

### Audience

Journalism enthusiasts, policymakers, activists

### On-the-ground actions from transcript

- Stay informed by following a diverse range of news sources (implied).
- Actively listen to various news bureaus to get a clearer picture of events (implied).

### Whats missing in summary

Importance of staying informed to counter the influence of money and power on shaping narratives.

### Tags

#Journalism #FreedomOfThePress #KnowledgeIsPower #PolicyDecisions #Information #GlobalEvents


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a question I received last night.
It's a good question, it's a valid question,
but to be honest when I first saw it, it threw me through a loop.
To me it was like watching somebody ask a farmer why they grow food.
People have to eat, you know.
From this side,
the answer seems very apparent. It's self-evident.
But the more I thought about it, the more I realized it wasn't.
Not really.
And the question gets to the heart
of a lot of the problems we have in journalism in the United States right now.
You have a whole bunch of people
who want facts, but they want those facts presented in a way that reinforces their
preconceived ideas.
You have some people that don't even care about the facts, they just want their
preconceived ideas reinforced
by somebody who
they feel has some kind of authority because they're on the cable news show.
A lot of journalists play into this,
but that's not why freedom of the press was enshrined in the Bill of Rights.
Why do we have spies?
Every country in the world has spies. Why do we have spies?
Asking that on this channel is funny. I'm sure somebody right now is typing
to get rid of democratically elected governments and install one that was
friendly to us.
And yeah, that happens.
But that's a byproduct.
That's a byproduct. That's not the goal. The goal is to get information, put it in a
report,
and give it to policymakers
so they can be better informed, so they can make better decisions, because
knowledge is power.
If they're really good at their job,
they can take a bunch of information,
sift through it,
determine the opposition's intent, and give policymakers a forecast
of what's going to happen.
Knowledge is power.
If the United States was healthy
and functioning properly,
you're a policymaker.
Your vote,
your calls, your petitions,
they influence policy
if things were working right.
That means you
need to be better informed
so you can make better decisions,
because knowledge is power.
That's why the freedom of the press is so important
in any country, any system of government,
where the people
are supposed to lead.
All this came up because I retweeted something about Nigeria.
And the guy was like, you know,
I feel for him, but what can I do with this?
Realistically, right now, nothing.
This foreign policy team is not going to get involved in this.
Highly unlikely.
And to be honest, if I was a Nigerian,
I wouldn't want them to,
because they're going to make a bad situation worse.
However, Biden,
we already know he wants to get involved in foreign policy again.
He wants to get the U.S. back on the stage.
That's why he put together that giant team of advisors
before he was ever even elected.
He wants to.
Which means come January,
hopefully, this information is going to be useful.
Because these people, they get elected,
they're going to put forth policies.
They're going to talk about them in the press.
And they're going to want to know
how the people feel about it.
You need the information so you can be better informed,
so you can help guide policy.
That's why journalism is so important
in free societies,
in societies where the people are supposed to lead.
They have to be better informed so they can make the decisions.
That's why it's protected.
That's why it's enshrined in the Bill of Rights.
Journalists, they're your spies, feeding you information
so you have a better grasp of things
that you can't experience firsthand.
You can't be everywhere at once.
Your embassies are the different news bureaus.
And just like embassies,
you need to be listening to all of them,
at least a large portion of them.
Some may have been compromised,
but you need to listen to a whole lot of them
because different pieces, different places
pick up different parts of the story,
give you a clearer picture.
Learning about events that are occurring overseas
and understanding them ahead of time
before the politicians can put a spin on it,
that can help save lives
because you can be better informed
before they realize they need to propagandize it.
So at the end of the day,
why do you need to know about Nigeria right now?
Because you're probably going to need to know about it in February.
And by then, the spin will have already started.
And the spin is going to be determined by who has the power.
And in this case, money is power.
The companies that stand to make a lot of money
will determine the spin
unless people know what's going on already.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}