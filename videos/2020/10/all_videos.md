# All videos from October, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-10-31: Let's talk about Trump's rallies, criticism, and a piece of mail.... (<a href="https://youtube.com/watch?v=R1urToCymQ4">watch</a> || <a href="/videos/2020/10/31/Lets_talk_about_Trump_s_rallies_criticism_and_a_piece_of_mail">transcript &amp; editable summary</a>)

Beau addresses criticism of President Trump's rallies, revealing the predictability of harm when safety is disregarded, questioning the President's care for citizens, and spotlighting his priorities over supporters' well-being.

</summary>

"All it would have taken for this to happen to me is for me to completely disregard everybody's safety for my own personal gain."
"I personally cannot think of anything scarier for Halloween than the realization that the President of the United States does not care about the citizens of the United States."
"Either he's not really that bright or he does not care about the lives of his most ardent supporters."
"He only cares about what he can get out of them."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

On Halloween, Beau addresses predictability, criticism, and President Trump's rallies.
Following a tweet criticizing President Trump's rallies for causing harm, Beau received hate mail.
The email criticized Beau for canceling events due to safety concerns while criticizing the President.
Beau acknowledges that if he had held events like the President's rallies, similar outcomes could have occurred.
He points out the predictability of harm when safety is disregarded for personal gain.
Beau underscores the importance of caring for people's safety and lives in events.
The email demonstrates a lack of concern from the President for citizens' well-being.
Beau questions the President's fitness for office based on his apparent disregard for safety.
He suggests that the President values his own interests, like photo ops, over supporters' well-being.
Beau concludes by expressing concerns about the President's priorities and actions.

Actions:

for activists, concerned citizens,
Contact local officials to ensure event safety protocols are in place (suggested)
Prioritize safety in all community events and gatherings (exemplified)
</details>
<details>
<summary>
2020-10-30: Let's talk about wolves and declaring victory.... (<a href="https://youtube.com/watch?v=vNlNDANJu0o">watch</a> || <a href="/videos/2020/10/30/Lets_talk_about_wolves_and_declaring_victory">transcript &amp; editable summary</a>)

Gray wolves' delisting poses risks of undoing years of conservation efforts, with uncertain outcomes ahead.

</summary>

"I don't think we're there yet."
"I have doubts, I have concerns, and it seems premature to me."
"I think it's at the point where it's guaranteed that it's going to stay there."
"I don't fault anybody for saying, hey, it's kind of a both sides issue."
"We did bring this animal back from the brink."

### AI summary (High error rate! Edit errors on video page)

Gray wolves are being delisted, coming off the Endangered Species Act list.
Beau has a bias from raising money to protect gray wolves.
Gray wolves were nearly extinct, but successful efforts have brought stable populations back.
Concerns arise as gray wolves' population is not stable in all areas.
If wolves are delisted, the responsibility falls to states, which could lead to different outcomes.
Some worry states might allow sport hunting and decimate the wolf population.
The current success story could be lost if states don't manage the wolf population effectively.
Beau compares the situation to public health policies being removed, leading to negative consequences.
Beau believes the situation is not yet certain and the final chapter hasn't been written.
Organizations are already taking action through petitions and lawsuits to protect the wolves.
States with wolf populations tend to follow science, offering hope for a positive outcome.
Beau expresses doubts and concerns about the premature delisting of gray wolves.
Beau anticipates more news on this issue post-election due to expected lawsuits.
Beau doesn't think it's a good idea to delist the wolves yet, despite being close to success.
Beau understands the reluctance of outlets to take a hard stand due to the proximity to a possible positive outcome.

Actions:

for conservationists, wildlife enthusiasts,
Sign petitions and support lawsuits by organizations protecting gray wolves (suggested).
Stay informed about the issue and advocate for science-based conservation efforts (implied).
</details>
<details>
<summary>
2020-10-30: Let's talk about my Biden Trump election prediction.... (<a href="https://youtube.com/watch?v=y-PbSFZASR0">watch</a> || <a href="/videos/2020/10/30/Lets_talk_about_my_Biden_Trump_election_prediction">transcript &amp; editable summary</a>)

Beau analyzes the election dynamics, points out emotional reactions, and advocates for reducing the overwhelming power of the presidency.

</summary>

"The only thing that isn't a guess is that the presidency has too much power."
"Nobody should have the power to upend the country the way Trump has."

### AI summary (High error rate! Edit errors on video page)

Analyzes the upcoming presidential election dynamics and predictions.
Mentions the unpredictability of polls based on past experiences.
Points out the emotional reactions people have towards the election.
Stresses that the presidency holds too much power for one person.
Advocates for decentralizing the power of the US government.
Suggests that the outcome of the election depends on voter turnout rather than polls or predictions.

Actions:

for american voters,
Rally for decentralization of power in the US government (suggested)
Encourage voter turnout and participation in elections (suggested)
Advocate for limitations on the power of the executive branch (suggested)
</details>
<details>
<summary>
2020-10-28: Let's talk about Twitter and Ted Cruz.... (<a href="https://youtube.com/watch?v=qR_mJZKPgaw">watch</a> || <a href="/videos/2020/10/28/Lets_talk_about_Twitter_and_Ted_Cruz">transcript &amp; editable summary</a>)

Beau asserts the dangers of government control over speech and the manipulation of information on social media, advocating for the preservation of freedom of expression and caution against authoritarian practices.

</summary>

"You can't tell somebody what they have to allow on their property."
"The government can't do this. You don't get to force somebody to carry government ideas."
"It's too powerful for that. This is big government. It is socialism."

### AI summary (High error rate! Edit errors on video page)

Ted Cruz attempted to hold Twitter accountable for not carrying information he deems necessary on their platform.
Beau explains that the government dictating what speech should occur on a private platform contradicts the First Amendment.
He mentions the distinction between a platform and a publisher, acknowledging the government's role but asserting that it shouldn't override freedom of speech.
Beau uses a restaurant analogy to demonstrate how Twitter, as a company, aims to ensure a positive user experience by moderating content.
He criticizes the far right's desire to control information access and suggests they create their own platforms.
Beau questions Senator Cruz's true intentions, implying that he may prioritize spreading misinformation to manipulate voters rather than genuine information access.
He argues that the Republican Party seeks equal access only for their "bad ideas" used for manipulation.
Beau stresses the danger of concentrating power in social media networks and the need to prevent government control over speech.
He proposes the idea of a state-funded social media network for spreading information without manipulation.
Beau warns against allowing the government to control information exchange, portraying it as a form of socialism detrimental to democracy.

Actions:

for social media users,
Advocate for maintaining freedom of speech online (implied)
Support platforms that prioritize user experience and positive community interactions (implied)
</details>
<details>
<summary>
2020-10-28: Let's talk about Omaha, Trump, logistics, and taking a walk.... (<a href="https://youtube.com/watch?v=lKYOKdmYzIY">watch</a> || <a href="/videos/2020/10/28/Lets_talk_about_Omaha_Trump_logistics_and_taking_a_walk">transcript &amp; editable summary</a>)

President's rally logistics failure in Omaha leaves elderly supporters in freezing temperatures, prompting Beau to question his leadership abilities and urge critical thinking for the upcoming election.

</summary>

"Queen Elsa showed more leadership than the President of the United States."
"He's a national embarrassment."
"Your team doesn't care about you. They will literally leave you behind."
"It's a show. It's branding. It's about ratings and numbers."
"The man is not fit to run a carnival."

### AI summary (High error rate! Edit errors on video page)

President held a rally in freezing temperatures in Omaha, with logistics including buses to transport attendees, mostly older Trump supporters.
After the rally, President left, leading to a shortage of buses to return everyone, leaving elderly people in freezing temperatures.
Medics had to treat people for hypothermia due to the lack of buses.
Beau criticizes the President's inability to handle rally logistics, let alone running a country, citing past failures and lack of responsibility.
Despite the incident, Beau acknowledges that some of those left behind may still support the President and deflect blame.
Beau questions why people hold the President to a lower standard than they do for other leaders in different scenarios.
He points out the importance of good leadership in taking care of the people entrusted to them.
Beau contrasts the President's actions with those of a good leader like Queen Elsa from "Frozen."
He urges people to think critically about renewing the President's "contract" in the upcoming election.
Beau criticizes the President's administration as lacking substance, being purely a show driven by personal gain.

Actions:

for voters, critical thinkers,
Critically analyze leadership qualities of political candidates (suggested)
Encourage responsible and empathetic leadership in your community (implied)
</details>
<details>
<summary>
2020-10-27: Let's talk about the Supreme Court confirmation and power.... (<a href="https://youtube.com/watch?v=8wapGhUGxAc">watch</a> || <a href="/videos/2020/10/27/Lets_talk_about_the_Supreme_Court_confirmation_and_power">transcript &amp; editable summary</a>)

Beau criticizes the lack of qualifications in the new Supreme Court justice, urges Democrats to exercise power for real change, and stresses the importance of systemic change originating from the House and Senate.

</summary>

"She's not qualified for the job, period."
"It's really that simple."
"If the Democratic party has the House, the Senate, and the White House and utilizes that power to make things better for the average person."
"The Democratic Party seems very unwilling to exercise power, very unwilling to wield the power when they have it."
"Deep, systemic change and it's going to have to come from the House and the Senate."

### AI summary (High error rate! Edit errors on video page)

Criticizes the new Supreme Court justice for her lack of qualifications and inability to answer basic questions about the Constitution during her confirmation hearing.
Points out that the Republican Party is willing to wield power to achieve their goals, regardless of qualifications or consequences.
Suggests that if Democrats win the House, Senate, and White House, they should use their power decisively to make significant changes, including restructuring the courts and passing progressive policies.
Expresses disappointment in both Republican and Democratic politicians for either selling out the country or not fighting hard enough.
Urges the Democratic Party to exercise power and implement deep, systemic changes to energize and retain support from voters.
Warns against the Democratic Party following the Republican Party's shift towards the far right and calls for real change to prevent becoming like the current Republicans.
Emphasizes the need for Democrats to take action to nullify the Supreme Court justice's vote due to her lack of understanding of basic freedoms protected by the Constitution.
Concludes by stressing the importance of real change and the necessity for House and Senate action to drive it, rather than relying on the White House.

Actions:

for politically engaged individuals,
Rally support for deep systemic changes by engaging with local political organizations and campaigns (implied)
Advocate for nullifying the Supreme Court justice's vote through contacting elected representatives and supporting legislative actions (implied)
</details>
<details>
<summary>
2020-10-27: Let's talk about Mitch McConnell's great fiction.... (<a href="https://youtube.com/watch?v=aZMQ467p9Ug">watch</a> || <a href="/videos/2020/10/27/Lets_talk_about_Mitch_McConnell_s_great_fiction">transcript &amp; editable summary</a>)

Mitch McConnell and the Senate prioritized a Supreme Court nominee over providing stimulus relief, revealing their true priorities and lack of care for working Americans.

</summary>

"They care about power and keeping working-class commoners in their place."
"The rich keep getting richer and the poor keep getting poorer."
"You co-sign them. You say, hey, I'm okay with what they're doing."
"You've been conditioned by that fiction to believe that the Republican Party is the party of patriotism."
"They confirmed a Supreme Court nominee who could not tell you what the First Amendment was."

### AI summary (High error rate! Edit errors on video page)

Mitch McConnell and the Senate pushed through a Supreme Court nominee instead of providing stimulus relief to Kentuckians.
The nominee couldn't name the components of the First Amendment during the hearing.
McConnell created a fiction that the Republican Party cares about the Constitution and working Americans.
The new justice may not intentionally undermine the Constitution because she doesn't know what's in it.
Republicans prioritize power and keeping working-class people down over helping them.
McConnell and other senators serve the rich rather than their constituents.
Senators were more focused on confirming Trump's nominee than on helping the economy.
The nominee couldn't answer basic civics questions but will be interpreting the Constitution.
Republicans care more about big companies than the common folk.
Voters who re-elect these senators enable their actions and lack of accountability.

Actions:

for voters, working americans,
Hold senators accountable for their actions (implied)
Pay attention to your representatives' priorities and hold them responsible (implied)
</details>
<details>
<summary>
2020-10-26: Let's talk about the shelves this winter.... (<a href="https://youtube.com/watch?v=mkp8KD1j3Vs">watch</a> || <a href="/videos/2020/10/26/Lets_talk_about_the_shelves_this_winter">transcript &amp; editable summary</a>)

Beau predicts potential shortages in the upcoming months and advises stocking up on essentials to reduce strain and be prepared.

</summary>

"I have been kicking myself for the last eight months over that."
"If you have the means, if you can get ahead now, you can reduce some of that strain."
"Go ahead and get what you need now so you're not one of the people out there straining the system then."
"Just set it back and carry on as normal."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Talks about inventories, stocks, and a potential upcoming situation.
Mentions discussing the economics behind masks eight months ago.
Explains the difference between N95 masks, surgical masks, cloth masks, and respirators.
Regrets not taking the previous situation seriously enough.
Predicts possible shortages in the upcoming months, especially during winter.
Advises preparing ahead by stocking up on essentials like canned food, paper towels, and toilet paper.
Encourages having a stash for emergencies regardless of where you live.
Emphasizes the importance of reducing strain by being prepared.
Suggests getting what you need now to avoid being in a rush during shortages.
Recommends taking oneself out of the equation by stocking up in advance.

Actions:

for prepared individuals,
Stock up on essentials like canned food, paper towels, toilet paper, and other shelf-stable items (suggested).
</details>
<details>
<summary>
2020-10-26: Let's talk about NYPD and having always done it that way.... (<a href="https://youtube.com/watch?v=Xg8xaroPO4s">watch</a> || <a href="/videos/2020/10/26/Lets_talk_about_NYPD_and_having_always_done_it_that_way">transcript &amp; editable summary</a>)

An officer's suspension for political display sparks debate, revealing resistance to change within NYPD and the need for moral over political stances.

</summary>

"Just because something has always been done that way is not a justification to continue doing it."
"When you got sheriffs and deputies in the old south telling you to ease up on the black folk, you got a problem."
"The SBA is part of the problem in NYPD."
"It's the cops who don't want to get better."
"Perhaps they should be leading the charge to advance and get better instead of holding it back."

### AI summary (High error rate! Edit errors on video page)

An officer in the NYPD was suspended for displaying "Trump 2020" on their squad car, sparking controversy over political neutrality.
The Sergeant's Benevolent Association (SBA) criticized the suspension, claiming it was unnecessary and that NYPD officers have a history of making political statements.
Beau argues against the idea of maintaining the status quo just because it's always been done that way, especially in a politically charged environment like the current election.
The SBA equated endorsing a political candidate with taking a knee with Floyd demonstrators, showing a lack of understanding between political matters and moral issues.
Beau suggests that all in-custody deaths should be condemned, regardless of past practices or norms.
He questions why Donald Trump is viewed as a racist, pointing out how his allies portray him and how it feeds into devaluing black lives.
Beau contrasts the actions of deputies and sheriffs in the deep south who took a knee in solidarity, even though most are Trump supporters, to show that it's a matter of right vs. wrong, not politics.
The SBA represents an institutional memory that hinders progress in law enforcement by resisting necessary reforms and improvements.
Beau challenges the NYPD to lead reform efforts rather than being an obstacle to change and advancement within law enforcement.

Actions:

for community members, activists,
Contact local law enforcement agencies to advocate for unbiased and apolitical conduct (suggested).
Organize community dialogues on the distinction between political matters and moral issues (exemplified).
</details>
<details>
<summary>
2020-10-25: Let's talk about Trump, Joe Biden, and mud.... (<a href="https://youtube.com/watch?v=Am0CypZFIuc">watch</a> || <a href="/videos/2020/10/25/Lets_talk_about_Trump_Joe_Biden_and_mud">transcript &amp; editable summary</a>)



</summary>

"It doesn't matter what they say. It doesn't matter if it's easily debunked."
"Biden will always end up being the lesser of two evils."
"It doesn't matter who replaces them. You just want to stop the damage being done."

### AI summary (High error rate! Edit errors on video page)

Trump campaign's strategy: releasing information about Biden every other day to create a parallel with hopes of decreasing voter enthusiasm around Biden.
The strategy aims to target emotional voters on election day, suppress the vote, and undermine democracy.
Trump campaign's late deployment of unreliable information to avoid debunking before the election.
Beau assumes all allegations about Biden are true, focusing on the pragmatic math and the lesser of two evils concept.
Even if all allegations were true, Biden is still seen as the better option compared to Trump for the country.
Beau stresses the importance of stopping the damage being done by Trump rather than praising Biden.
The mental exercise is to assume allegations are true, do the math, and realize Biden remains the lesser of two evils.

Actions:

for voters,
Challenge misinformation by fact-checking and sharing accurate information (implied).
</details>
<details>
<summary>
2020-10-24: Let's talk about voter turnout in 2020 and what it tells us.... (<a href="https://youtube.com/watch?v=CVNaUG0HrNg">watch</a> || <a href="/videos/2020/10/24/Lets_talk_about_voter_turnout_in_2020_and_what_it_tells_us">transcript &amp; editable summary</a>)

Predicted high voter turnout in the US draws historical parallels to past elections, raising concerns about suppression efforts and the importance of upholding democratic values.

</summary>

"To oppose high voter turnout, to attempt to suppress the vote, is to undermine the very ideas this country was founded on."
"Those who believe in the ideas that America is supposed to embody, the idea that the people are the leader, they should want high voter turnout."
"There is a systemic effort by one party to suppress the vote."
"If they're willing to sell out a pillar of what's supposed to make up America, well they will certainly sell out you."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Predicted voter turnout is incredibly high, with 150 million people expected to vote, around 65% turnout in the US.
Historical parallels to the 1908 election where high turnout was seen, with similar strategies and concerns about crowd sizes.
One candidate in 1908 didn't seek establishment support, campaigned against the elite, and embraced new technology like stumping.
Teddy Roosevelt, a popular figure, supported Taft for the presidency, resulting in a high voter turnout and Taft's win.
Experts suggest a potential landslide in the upcoming election due to high voter enthusiasm and mail-in voting accessibility.
In a representative democracy like the US, high voter turnout should be encouraged to uphold the democratic ideals.
Beau points out the importance of identifying those who try to suppress the vote, as it goes against the fundamental principles of America.
He warns about the dangers of individuals undermining democratic values for personal gain and urges vigilance against such efforts.

Actions:

for american voters,
Investigate efforts to suppress the vote and support organizations working against voter suppression (exemplified)
Encourage voter engagement and support initiatives that facilitate accessible voting methods (exemplified)
</details>
<details>
<summary>
2020-10-24: Let's talk about Trump, rounding a corner, and people counting on you.... (<a href="https://youtube.com/watch?v=LZNvGFdaYdI">watch</a> || <a href="/videos/2020/10/24/Lets_talk_about_Trump_rounding_a_corner_and_people_counting_on_you">transcript &amp; editable summary</a>)

Beau warns against trusting the administration's handling of COVID-19, urging heroism through mask-wearing to protect lives and maintain precautions.

</summary>

"Your country is calling. They need you to wear a mask."
"We cannot trust this administration to make the right calls."
"You want to be a hero? Wear a mask."
"We have to stay in the fight until it's over."
"It's really that simple."

### AI summary (High error rate! Edit errors on video page)

President Trump mentioned "rounding a corner" in debates, but Beau views it differently.
The US recently hit its highest daily new cases ever: 80,005.
Experts predict six-figure daily cases soon.
Trump hasn't attended task force meetings in months, relying on Scott Atlas instead of Fauci's advice.
Atlas spread misinformation about masks' effectiveness, leading Twitter to remove his tweet.
A new study shows wearing masks and following guidelines can save lives.
If 95% of Americans comply, 130,000 lives can be saved by February.
People resistant to wearing masks often want to appear tough or brave.
Beau urges everyone to wash hands, avoid touching faces, stay home, wear masks, and practice social distancing.
Despite fatigue setting in, precautions must continue as 130,000 lives depend on it.

Actions:

for general public,
Wear a mask, wash hands, avoid touching face, stay home, and practice social distancing to protect lives (implied).
</details>
<details>
<summary>
2020-10-23: Let's talk about civil dialogue, 3 steps, and the holidays.... (<a href="https://youtube.com/watch?v=fH_j7hSyCug">watch</a> || <a href="/videos/2020/10/23/Lets_talk_about_civil_dialogue_3_steps_and_the_holidays">transcript &amp; editable summary</a>)

Beau addresses the importance of civil dialogues, outlining a three-step screening process to determine if engaging in discourse is warranted, productive, or beneficial.

</summary>

"Not all ideas are owed civil dialogues."
"Bad ideas aren't owed a platform."
"Nobody is owed your time."

### AI summary (High error rate! Edit errors on video page)

Addressing the importance of civil dialogues in response to queries on dealing with criticism and engaging in civil dialogues with individuals who may not seem receptive.
Advocating for a three-step screening process to determine if a person is entitled to a civil discourse.
Emphasizing that not all ideas are entitled to civil dialogues, especially when they cross a certain line of being harmful or atrocious.
Sharing a personal example of engaging in a productive civil discourse on YouTube regarding reparations.
Advising not to entertain bad faith arguments or individuals who aim to create trouble without good intentions.
Encouraging individuals to critically analyze if engaging in civil dialogues is a productive use of time.
Suggesting strategies for engaging in civil dialogues with friends or acquaintances holding different beliefs, while questioning the potential productivity of such engagements.
Concluding by reiterating that nobody is owed dialogues, especially if the idea being discussed is atrocious or unproductive.

Actions:

for engage in civil dialogues.,
Analyze if engaging in civil dialogues is productive (suggested).
Determine if the idea being discussed is harmful or atrocious (implied).
</details>
<details>
<summary>
2020-10-23: Let's talk about Trump, Lincoln, you, and the debate.... (<a href="https://youtube.com/watch?v=S6Gdd2lbhEk">watch</a> || <a href="/videos/2020/10/23/Lets_talk_about_Trump_Lincoln_you_and_the_debate">transcript &amp; editable summary</a>)

Beau predicts Trump's reliance on sensationalized claims to sway low-information voters emotionally, urging individuals to be prepared to counter misinformation for a decisive rejection of Trumpism in the upcoming elections.

</summary>

"Because if it was true and something that could be verified, he would have released it months ago when it could knock Biden out of the race."
"Be ready to counter it."
"We want him to lose big."
"You have to be ready to counter it."
"Those people exist."

### AI summary (High error rate! Edit errors on video page)

Beau gives his take on the recent debate involving President Trump and his campaign strategy, including his personal style and branding.
Trump managed to behave himself initially during the debate, adhering to certain rules.
Trump boasted about his contributions to black people, comparing himself to President Lincoln, despite historical facts proving otherwise.
Beau criticizes Trump's reliance on sensationalized claims to appeal to his base rather than discussing his policies or record.
Beau predicts another sensationalized claim from Trump against his opposition, aimed at swaying low-information voters emotionally.
Beau urges individuals to be prepared to counter any false claims and strive for a significant rejection of Trumpism in the upcoming elections.
He stresses the importance of individuals being ready to debunk misinformation, especially for those swayed by emotions and lacking in verification.
Beau encourages viewers to play their part in ensuring a decisive rejection of Trump by a majority of Americans.
He underlines the role of individuals in countering false claims and preventing the influence of emotional manipulation in the political sphere.

Actions:

for individuals,
Be ready to counter false claims when they arise (implied)
</details>
<details>
<summary>
2020-10-22: Let's talk about Trump, Biden, spies, and policymakers.... (<a href="https://youtube.com/watch?v=7e9tVGGppvY">watch</a> || <a href="/videos/2020/10/22/Lets_talk_about_Trump_Biden_spies_and_policymakers">transcript &amp; editable summary</a>)

Beau explains the vital role of journalism in informing the public to guide policy decisions, comparing journalists to spies feeding critical information.

</summary>

"Journalists, they're your spies, feeding you information so you have a better grasp of things that you can't experience firsthand."
"Learning about events that are occurring overseas and understanding them ahead of time before the politicians can put a spin on it, that can help save lives."
"Because you're probably going to need to know about it in February. And by then, the spin will have already started."
"That's why journalism is so important in free societies, in societies where the people are supposed to lead."
"And in this case, money is power."

### AI summary (High error rate! Edit errors on video page)

Questions the importance of journalism and freedom of the press.
Addresses the issue of people seeking facts that only reinforce their preconceived ideas.
Explains the purpose of spies in gathering information for policymakers to make informed decisions.
Emphasizes the power of knowledge and the role of journalists in keeping the public informed.
Advocates for being well-informed to guide policy decisions in a democracy.
Stresses the significance of journalism in free societies where people are meant to lead.
Compares journalists to spies feeding information to the public.
Urges the audience to pay attention to international news to form their own opinions before spin begins.
Points out the influence of money in shaping the narrative and the importance of being informed to counter it.
Concludes with a reminder of the importance of staying informed about global events for future decision-making.

Actions:

for journalism enthusiasts, policymakers, activists,
Stay informed by following a diverse range of news sources (implied).
Actively listen to various news bureaus to get a clearer picture of events (implied).
</details>
<details>
<summary>
2020-10-22: Let's talk about Trump and the election announcement.... (<a href="https://youtube.com/watch?v=xQN2gm2AWbo">watch</a> || <a href="/videos/2020/10/22/Lets_talk_about_Trump_and_the_election_announcement">transcript &amp; editable summary</a>)

Beau questions the alignment between the president's tactics and foreign adversaries, urging supporters to reconsider their stance on democracy.

</summary>

"If his talking points about American democracy match up with those people, those nations, who want to subvert it, do you really think he believes in the republic, in representative democracy and the Constitution, when all he does is try to undermine it?"
"Maybe a desperate attempt by a desperate adversary."

### AI summary (High error rate! Edit errors on video page)

Special announcement on election resilience.
Reassurance to calm and unify.
Emphasis on confidence in vote count.
FBI director warns against unverified claims.
National intelligence director debunks fraudulent ballot claims.
Foreign operations to undermine voter confidence by Iran and Russia.
President using similar tactics as foreign adversaries.
Questioning where talking points originated.
Alignment in talking points to disrupt election.
Call for supporters to reconsider their stance.
Reflection on president's commitment to democracy.
Comparing president's tactics to desperate adversaries.
Implications on American democracy.
Provoking thought on supporting the president.
Ending with a contemplative note.

Actions:

for voters,
Reexamine support for the president (implied)
</details>
<details>
<summary>
2020-10-21: Let's talk about 500 kids and a message to suburban moms.... (<a href="https://youtube.com/watch?v=mNzVP8B01Wg">watch</a> || <a href="/videos/2020/10/21/Lets_talk_about_500_kids_and_a_message_to_suburban_moms">transcript &amp; editable summary</a>)

500 kids separated from parents in 2017, still not reunited; America's moral fabric questioned under fear-mongering campaign, with urgent need for action.

</summary>

"What happened because of it."
"We have a decision to make in this country."
"The President has destroyed the moral fiber of this country."
"We got work that needs to be done."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

500 kids have been separated from their parents in 2017 and haven't been reunited since, some for more than half their lives.
Finding the parents of these children seems unlikely as they've been bounced around the system for years.
The children may now have addresses like row and plot numbers, making it harder to locate them.
Beau questions if America has truly been made great again, considering the lack of respect on the international stage and the moral authority.
The fear-mongering campaign may have created a nation afraid of everything, leading to actions like separating families.
The forcible transfer of children by the United States violates treaties backed up by the Constitution.
The President's actions have torn families apart through creating deterrents or displaying negligence towards public health issues.
Each of the 500 stories of separated families should be heard and covered extensively to understand the consequences of apathy.
Beau stresses the need to make a decision about the country's direction soon, as the lack of respect from other nations is evident.
The President's actions have eroded the moral fabric of the nation, with the lack of media coverage on reuniting families showcasing a significant downfall.

Actions:

for every concerned citizen.,
Contact organizations working to reunite families (suggested)
Spread awareness about the issue through social media and community networks (suggested)
Join protests or advocacy campaigns demanding family reunification (suggested)
</details>
<details>
<summary>
2020-10-20: Let's talk about Trump, executive power, the Heisman, and the Pulitzer.... (<a href="https://youtube.com/watch?v=v9wx6df5ZaM">watch</a> || <a href="/videos/2020/10/20/Lets_talk_about_Trump_executive_power_the_Heisman_and_the_Pulitzer">transcript &amp; editable summary</a>)

Beau recounts a historical football incident to urge America to learn from its current crisis and enact necessary changes.

</summary>

"He broke America's jaw."
"Are we going to be as responsive as the NCAA?"
"Does the country need their jaw broken again?"

### AI summary (High error rate! Edit errors on video page)

Recounts a 1950s football game involving Johnny Bright, a black player destined for the Heisman.
Bright was intentionally hit in a racially motivated incident, impacting college athletics.
The incident was captured in photos, becoming a symbol for the civil rights movement.
The imagery energized the civil rights movement by making the mistreatment visible.
The NCAA changed rules and implemented safety measures in response to the incident.
Beau draws parallels between this historical event and America's current political situation.
Trump's actions are likened to breaking America's jaw on film, forcing the country to confront its issues.
Beau questions whether the country will learn from this incident or require further crises to prompt change.

Actions:

for americans,
Implement changes in response to current political issues (suggested)
Advocate for safeguards to protect against abuses of power (implied)
</details>
<details>
<summary>
2020-10-19: Let's talk about trust in the polls.... (<a href="https://youtube.com/watch?v=jgLL8m3b3iA">watch</a> || <a href="/videos/2020/10/19/Lets_talk_about_trust_in_the_polls">transcript &amp; editable summary</a>)

Beau explains the dynamics of trusting polls, addressing past inaccuracies and adjustments made by pollsters, advocating for both trust and skepticism in poll results based on people's actions matching their poll responses.

</summary>

"Education and credentialing are not the same thing."
"Pollsters create their forecast based on what people say during the polls."
"If people don't do what they say, then the poll, it's going to be wrong."
"Yes, trust the polls because they fixed the statistical issue, but no, don't trust them because they don't actually mean anything."
"It's really that simple."

### AI summary (High error rate! Edit errors on video page)

Addresses the topic of trusting polls and the consistent lead of Biden in polls.
Mentions the doubts surrounding poll accuracy due to past incidents like Clinton's lead.
Points out that pollsters previously undercounted uneducated voters, particularly white voters without a college degree.
Expresses disagreement with the term "uneducated voters" and the correlation between education and susceptibility to fear-mongering.
Talks about how pollsters have adjusted their methods by weighting uneducated voters more heavily.
Notes the decrease in the percentage of whites without a college degree in the voting populace.
Emphasizes that polls are not magical crystal balls and rely on people's actions matching their poll responses.
Suggests that people need to act according to the poll results for them to be accurate.
Concludes by advising to trust the polls due to statistical adjustments but also not to trust them entirely.

Actions:

for voters, poll-watchers,
Follow through on reported poll demographics when voting (implied).
</details>
<details>
<summary>
2020-10-19: Let's talk about reaching your Trump supporting relative.... (<a href="https://youtube.com/watch?v=wJUVMMgsOFc">watch</a> || <a href="/videos/2020/10/19/Lets_talk_about_reaching_your_Trump_supporting_relative">transcript &amp; editable summary</a>)

Beau addresses the struggle of reaching out to Trump loyalists, stressing the importance of the messenger and acknowledging the challenges of changing deeply held beliefs.

</summary>

"Sometimes it's not the arguments you're using. Sometimes it's not your, sometimes there is no combination for you."
"You can't look at this as a failure on your part. You did everything you could."
"There is no combination. There's no pattern that's going to work on every person. It doesn't exist."
"Some men you just can't reach."
"It may just be that you're not the right person to make that argument."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the common question he receives about how to reach loved ones who are loyal to Trump and seem unreachable despite efforts to change their minds.
He points out that many individuals asking for help in reaching out to their pro-Trump family members or friends often express a sense of failure because they couldn't convince them otherwise.
Beau shares a personal anecdote about a commenter who used to vehemently support Trump but, after watching a video collaboration Beau did with another channel, reached out to apologize and express a change of heart.
He suggests that sometimes the messenger, not the message, is what matters in getting through to someone who holds strong political beliefs.
Beau notes that trying to argue based on the principles someone held five years ago might not be effective, as Trump loyalists today are often driven by loyalty to him rather than traditional principles.
He warns against reinforcing the idea of Trump as a savior by pointing out how much the person has changed, as it could backfire and strengthen their loyalty to him.
Beau acknowledges that there may be no universal combination of arguments that work on every person, citing his own relatives with Trump signs in their yard.
He recognizes that some individuals may be too deeply entrenched in the cult of personality around Trump to be swayed by logical arguments, suggesting that external factors beyond one's control might be needed to break that loyalty.

Actions:

for those struggling to reach pro-trump family members or friends.,
Have open, empathetic dialogues with pro-Trump individuals (suggested).
Recognize when to step back and accept that some people may be beyond your influence (exemplified).
</details>
<details>
<summary>
2020-10-18: Let's talk about the questions from Twitter.... (<a href="https://youtube.com/watch?v=xA0x7Ul_t7M">watch</a> || <a href="/videos/2020/10/18/Lets_talk_about_the_questions_from_Twitter">transcript &amp; editable summary</a>)

Beau provides off-the-cuff responses to questions on media terminology, Trump's support, civil debates, Biden's progressiveness, and public healthcare.

</summary>

"It's a choice of language in which I think the media outlets are trying to elicit an emotional reaction when a rational one would probably better serve the country."
"Most people watching this channel, Joe Biden doesn't represent the change you want. However, he's definitely better than the change you're going to get from Trump."
"I think everybody should have a wide skill set. I don't think and understand when I say this I'm not necessarily talking about because what may happen in a few months."

### AI summary (High error rate! Edit errors on video page)

Beau held a live stream where he answered questions from Twitter, but ran out of time and didn't get to address any.
Beau decided to answer some of the questions in an off-the-cuff manner.
He discussed the media's use of soft language like "meddling" instead of more accurate terms like "Russian intelligence operation."
Beau shared his views on why some Americans still support Donald Trump, citing reasons like reluctance to admit being wrong and resonating with certain talking points.
He talked about having civil, fact-based debates with those who disagree with you, focusing on sticking to objective facts.
Beau expressed his belief that political engagement due to Trump's presidency could lead to a more progressive outlook in the country.
He addressed the possibility of Trump's followers turning violent if he loses the election.
Beau discussed the progressive stance on Biden's lack of progressiveness, acknowledging that while Biden may not be ideal, he is still better than the alternative.
He shared his thoughts on whether Trump will leave the U.S. if he loses the election, expressing skepticism about potential legal consequences for Trump.
Beau talked about the American perspective on public healthcare and the resistance due to the fear of government control.

Actions:

for political enthusiasts,
Reach out to people on social media platforms like Twitter to start community building efforts. (implied)
Stay politically engaged and push for progress regardless of election outcomes. (implied)
</details>
<details>
<summary>
2020-10-18: Let's talk about Trump's endorsements.... (<a href="https://youtube.com/watch?v=JWgtwghlRfg">watch</a> || <a href="/videos/2020/10/18/Lets_talk_about_Trump_s_endorsements">transcript &amp; editable summary</a>)

Exploring the sincere but misguided beliefs behind Donald Trump's endorsements, revealing how both left and right supporters see his victory as aiding their goals.

</summary>

"They're people who believe they are looking out for the best interests of this country."
"Both sides believe that. It's interesting to me that the president's strongest supporters are those people who want to watch the country tear itself apart."

### AI summary (High error rate! Edit errors on video page)

Exploring Donald Trump's endorsements and the different perspectives behind them.
Mentioning the opposition's endorsement of Trump due to their belief they can manipulate him.
Not focusing on the easy targets and low hanging fruit of Trump's endorsements.
Acknowledging two groups who sincerely believe Trump represents their interests.
Detailing the left's belief that Trump's failure will pave the way for a revolution.
Describing the right's shift from anti-government to pro-Trump foot soldiers.
Not understanding the ideological transition on the right but recognizing their sincere beliefs.
Both groups on the left and right see a Trump victory as beneficial for their goals.
Pointing out that Trump's strongest supporters are those who want to see the country divided.
Ending with a reflection on the impact of Trump's supporters and their goals.

Actions:

for political observers, voters,
Support political candidates who genuinely represent your values and interests (implied)
Engage in constructive political discourse with those who have differing views (implied)
</details>
<details>
<summary>
2020-10-16: Let's talk about three unconnected stories and that article.... (<a href="https://youtube.com/watch?v=7Io8KG2ZkHA">watch</a> || <a href="/videos/2020/10/16/Lets_talk_about_three_unconnected_stories_and_that_article">transcript &amp; editable summary</a>)

Beau introduces three stories to challenge how things are framed, discussing responsibility, censorship, and the market deciding what ideas are promoted.

</summary>

"Nobody owes bad ideas, bad information, a social safety net."
"The market decided we don't want them."

### AI summary (High error rate! Edit errors on video page)

Beau introduces three seemingly unconnected stories to shed light on how things may not be as they seem.
Using Coca-Cola as an example, Beau explains the importance of having a product people want and the infrastructure to deliver it.
Beau shares a personal experience with a knife that broke upon delivery, illustrating the importance of a product's quality.
He talks about showcasing the idea that cost doesn't always equate to value in certain industries.
Beau mentions a beverage company that stopped delivering a product with poor ingredients, showing responsibility.
He recalls trying to order a controversial book at a bookstore and facing resistance due to its content.
Beau contrasts how certain actions are considered normal, while others, like social media censorship, spark controversy.
He questions the framing of social media giants reducing the volume of an article criticizing Biden as censorship.
Beau explains that free speech doesn't guarantee a platform or promotion for every idea and stresses individual responsibility.
He concludes by stating that bad ideas and information are not entitled to promotion or support.

Actions:

for consumers, critical thinkers.,
Challenge the framing of information and narratives (implied).
Support responsible actions by companies and individuals (implied).
Promote critical thinking and individual responsibility (implied).
</details>
<details>
<summary>
2020-10-16: Let's talk about Trump, Biden, Mr. Rogers, and the town halls.... (<a href="https://youtube.com/watch?v=1PubsTRMtTc">watch</a> || <a href="/videos/2020/10/16/Lets_talk_about_Trump_Biden_Mr_Rogers_and_the_town_halls">transcript &amp; editable summary</a>)

Beau analyzes Biden and Trump's town halls, contrasts their styles, and praises Mr. Rogers for embodying unity in challenging times.

</summary>

"I'm not sure positioning yourself as [Mr. Rogers'] opposition is going to play out well with the American voters."
"They don't admit they were wrong."
"I understand it, but y'all need to keep Mr. Rogers' name out your mouths."

### AI summary (High error rate! Edit errors on video page)

Both Biden and Trump held town halls at the same time, causing an uproar.
Biden's town hall was civil but somewhat boring, with no major surprises.
Biden claimed to have always hoped to make the criminal justice system fair, which Beau found questionable.
Trump's town hall was chaotic, with him often avoiding direct answers and claiming not to know or remember things.
Trump's refusal to denounce a vocal supporter group raised eyebrows, especially since the FBI labeled them a threat.
Trump's team compared Biden's town hall to watching Mr. Rogers, which Beau interpreted positively as seeking unity.
Beau praises Mr. Rogers for his courage in addressing social issues and contrasts his leadership style with Trump's administration.
Beau suggests that the Trump team positioning themselves against Mr. Rogers may not resonate well with American voters.

Actions:

for voters, political observers,
Contact local representatives to advocate for criminal justice reform (implied)
Educate others on the importance of unity and empathy in leadership (implied)
</details>
<details>
<summary>
2020-10-15: Let's talk about the First Amendment, Barrett, and the nomination.... (<a href="https://youtube.com/watch?v=ZzEbLlPKdkc">watch</a> || <a href="/videos/2020/10/15/Lets_talk_about_the_First_Amendment_Barrett_and_the_nomination">transcript &amp; editable summary</a>)

Barrett's failure to identify basic constitutional freedoms deems her unfit for a Supreme Court lifetime appointment, making any vote to confirm a party allegiance rather than a qualification assessment.

</summary>

"If you can't answer the basic questions about the Constitution, you can't interpret it."
"This needs to be the end of the hearings. She is unfit for the Supreme Court."
"If you're going to interpret the Constitution, you have to know what's in it."
"Any vote to confirm is really just a vote for party."
"Knowingly giving somebody a lifetime appointment that isn't qualified."

### AI summary (High error rate! Edit errors on video page)

Expresses concern over the nomination and the First Amendment.
Barrett, a nominee for the Supreme Court, failed to name the five freedoms protected by the First Amendment.
Compares this to basic knowledge needed for other job interviews.
Questions how Barrett can interpret the Constitution without knowing its content.
Emphasizes that this is not a minor issue but a significant disqualifier for the position.
Criticizes media and Supreme Court justices for misinterpretation of basic constitutional concepts.
Asserts that Barrett's lack of basic constitutional knowledge makes her unfit for the Supreme Court.
Argues that any vote to confirm her is not based on qualifications but on party allegiance.
Concludes that giving an unqualified nominee a lifetime appointment undermines the Constitution.

Actions:

for voters, activists, concerned citizens,
Contact your senators to express opposition to confirming unqualified nominees (suggested)
Educate others on the importance of basic constitutional knowledge for Supreme Court justices (implied)
</details>
<details>
<summary>
2020-10-15: Let's talk about computers, emails, Biden, Trump, and verification.... (<a href="https://youtube.com/watch?v=q9xH-8c4Qgs">watch</a> || <a href="/videos/2020/10/15/Lets_talk_about_computers_emails_Biden_Trump_and_verification">transcript &amp; editable summary</a>)

Beau recaps past allegations, questions credibility of unverified emails, and suggests a potential disinformation campaign.

</summary>

"The reporting was so bad I couldn't let it slide."
"I believe the activities that Hunter Biden engaged in, me personally, I think they're shady. I think they're unethical. But they're not illegal."
"But it's unlikely. It is unlikely that after all of this time, magically it's going to show up right before the election."

### AI summary (High error rate! Edit errors on video page)

Recaps a video from last year about the Democratic primaries and the narrative surrounding allegations.
Points out the importance of understanding the timeline in relation to the allegations.
Mentions the main allegation against Vice President Biden involving elevating a family member.
Expresses desire to question President Trump or his advisors about similar family-related concerns.
Notes that the emails discussed have not been verified or authenticated yet.
Criticizes the White House for acting on unvetted intelligence regarding the emails.
Draws attention to the lack of response from the administration on intelligence concerning US troops.
Summarizes the story about a computer repair shop, a laptop, and obtained emails involving Steve Bannon and Rudy Giuliani.
Comments on the credibility of the source and distribution of the emails, hinting at potential disinformation.
Considers the possibility that the emails might be genuine but focuses more on the likelihood of a disinformation campaign.

Actions:

for critical thinkers,
Investigate the credibility of information before acting on it (implied).
</details>
<details>
<summary>
2020-10-15: Let's talk about Trump's comments and a dress rehearsal.... (<a href="https://youtube.com/watch?v=GQcmv8zDLO4">watch</a> || <a href="/videos/2020/10/15/Lets_talk_about_Trump_s_comments_and_a_dress_rehearsal">transcript &amp; editable summary</a>)

Trump and politicians' privileged healthcare access showcases inequalities, serving as a warning for climate change impacts and the urgent need for mitigation.

</summary>

"He's like, yeah it was great, we had like 14 doctors standing around."
"It's a dress rehearsal for climate change."
"We're past the point of being able to avoid it completely, but we can still mitigate the worst of it."

### AI summary (High error rate! Edit errors on video page)

Trump bragged about his exceptional healthcare treatment with 14 doctors, pointing out the stark contrast with the accessibility issues faced by most people.
The inequality in healthcare access is glaring, emphasized by Trump and other politicians receiving privileged treatment.
Trump, who previously downplayed the severity of the situation, was able to access top-notch healthcare and then boasted about it.
Another politician's precautionary medical care raises questions about political connections and wealth influencing access to healthcare.
The actions of politicians like Trump and others in handling the pandemic forecast their approach to other critical issues like climate change and economic priorities.
Beau compares the current situation with privileged individuals easily accessing healthcare to a dress rehearsal for the future impact of climate change, stressing the need for urgent action.
He advocates for political pressure on the upcoming administration to prioritize addressing healthcare disparities and mitigating the impacts of climate change.
While it may be too late to avoid all negative impacts of climate change, Beau urges for immediate action to mitigate the worst effects.
The next administration needs to focus on addressing healthcare inequalities and climate change, as they currently do not prioritize these issues without external pressure.
Beau underscores that without intervention, the privileged few will have access to resources and precautions that the general population lacks, leading to severe consequences.

Actions:

for activists, policymakers, voters,
Pressure the upcoming administration to prioritize healthcare disparities and climate change mitigation (implied)
</details>
<details>
<summary>
2020-10-14: Let's talk about record-setting voter turnout and mandates.... (<a href="https://youtube.com/watch?v=5jOOZrOqCLM">watch</a> || <a href="/videos/2020/10/14/Lets_talk_about_record-setting_voter_turnout_and_mandates">transcript &amp; editable summary</a>)

Early voting with record turnout may signal rejection of Trump, but post-election pressure needed for meaningful change.

</summary>

"The election is a moment, not a movement."
"If you want change, you have to have movement."

### AI summary (High error rate! Edit errors on video page)

Early voting has begun in some places with record voter turnout, potentially signaling a rejection of Trump and Trumpism.
There is uncertainty whether the high turnout represents enthusiastic Trump voters or a rejection of him.
Assuming the polls are accurate, this could be the beginning of a landslide against Trump.
Elected representatives may interpret high numbers as approval, leading to a lack of motivation to act on issues beyond simply not being Trump.
Beau suggests using social media to pressure politicians by tying their inaction to adopting Trump's policies if they fail to advance positive change.
The focus should be on creating movement post-election, as just voting is a moment, not a movement.
Politicians walking in with high numbers may not be responsive to constituents' needs as they may believe they have a mandate to act as they please.
It is critical to push politicians from day one to address issues and not rest on the mandate of not being Trump.

Actions:

for voters, activists, citizens,
Hold elected representatives accountable for advancing policies beyond just not being Trump (implied).
Use social media to pressure politicians by tying their inaction to adopting Trump's policies (implied).
</details>
<details>
<summary>
2020-10-14: Let's talk about Trump, Pelosi, McConnell, and the stimulus.... (<a href="https://youtube.com/watch?v=6v8ogLQNqOc">watch</a> || <a href="/videos/2020/10/14/Lets_talk_about_Trump_Pelosi_McConnell_and_the_stimulus">transcript &amp; editable summary</a>)

Senate Republicans pivot to distance themselves from Trump on stimulus negotiations, prioritizing political moves over aiding Americans in need.

</summary>

"They're going to try to appeal to the American people by denying the American people the cash payments most need pretty badly."
"Once again, when it comes to something that helps the average American, those at the top are just playing politics."

### AI summary (High error rate! Edit errors on video page)

Explains the current situation with Mitch McConnell, Nancy Pelosi, and Donald Trump regarding the stimulus package.
Pelosi wants a large stimulus package, while McConnell prefers a piecemeal approach focusing on big companies.
Trump initially called off negotiations but then changed his stance due to public backlash.
Senate Republicans are trying to distance themselves from Trump as the elections approach.
They are focusing on differentiating from Trump on the stimulus and Supreme Court nominations.
McConnell is concerned about losing control of the Senate and is strategizing to appeal to voters.
Senate Republicans aim to show they are not just blindly supporting Trump's decisions.
The priority seems to be political posturing rather than genuinely helping Americans in need.

Actions:

for voters,
Call or email your representatives to express your opinion on the stimulus package negotiations (suggested).
Stay informed on the developments surrounding the stimulus package and hold elected officials accountable for their decisions (implied).
</details>
<details>
<summary>
2020-10-13: Let's talk about Trump in the last few weeks of the 2020 election.... (<a href="https://youtube.com/watch?v=mXnFhNlha-E">watch</a> || <a href="/videos/2020/10/13/Lets_talk_about_Trump_in_the_last_few_weeks_of_the_2020_election">transcript &amp; editable summary</a>)

Beau covers the 2020 election dynamics at Anytown High School, predicting desperate tactics from the incumbent and advising readiness for baseless claims.

</summary>

"It's a referendum on Donnie's administration or lack thereof."
"He's probably going to rely on what's always worked for him, making stuff up."
"We should be ready for some explosive claims that are completely baseless and unfounded."

### AI summary (High error rate! Edit errors on video page)

Talks about the 2020 election and what to expect from the incumbent in the next few weeks.
Describes the incumbent, Donnie, as a class bully with a rich background and lack of accomplishments.
Mentions Donnie's lack of participation in class projects and failure to implement policies.
States that it's too late for Donnie to propose policies or develop leadership skills.
Comments on Donnie's endorsements, including support from the school resource officer and the school rival.
Views the upcoming election as a referendum on Donnie's administration.
Speculates on possible desperate tactics Donnie might employ, such as pushing a disliked student against lockers or spreading rumors.
Anticipates Donnie resorting to making baseless claims due to his low chances of winning.
Advises preparing for Donnie's potential unfounded claims and reminding others of his history of fabrication.

Actions:

for students, voters, school community,
Remind fellow students of the incumbent's history of fabrication (implied)
Stay vigilant against potential baseless claims and misinformation (implied)
</details>
<details>
<summary>
2020-10-12: Let's talk about and to small government conservatives.... (<a href="https://youtube.com/watch?v=itCkHc6EdS8">watch</a> || <a href="/videos/2020/10/12/Lets_talk_about_and_to_small_government_conservatives">transcript &amp; editable summary</a>)

Addressing small government conservatives torn between Trump and Biden, Beau advocates for voting based on principles and introduces Joe Jorgensen as a candidate who closely resonates with their values.

</summary>

"You're going to walk into that voting booth and you're going to vote for Trump simply because he has an R next to his name and you don't want to do that."
"You're going to vote for him simply because he has an R next to his name and that's gonna be the other response."
"All it takes is informing yourself."

### AI summary (High error rate! Edit errors on video page)

Addressing small government conservatives who are uncertain about the upcoming election.
Small government conservatives prioritize down-ballot races and struggle with the presidential choices.
They can't fully support Trump due to his failures and authoritarian tendencies.
Similarly, they can't back Biden as he represents liberal policies.
The primary concern for small government conservatives is about money and limited government intervention.
They seek candidates with strong business credentials and aim to run the country like a business.
Economic issues, such as reducing national debt and corporate subsidies, are key priorities.
Their stance on social issues is often tied to economic considerations rather than social concerns.
Beau suggests Joe Jorgensen as a candidate who closely aligns with small government conservative values.
Encourages them to vote based on principles and conscience rather than party loyalty.

Actions:

for small government conservatives,
Inform yourself about alternative candidates like Joe Jorgensen (suggested).
Vote based on principles and conscience rather than party loyalty (implied).
</details>
<details>
<summary>
2020-10-12: Let's talk about a moment and movement.... (<a href="https://youtube.com/watch?v=9vAiG4LJ-Jk">watch</a> || <a href="/videos/2020/10/12/Lets_talk_about_a_moment_and_movement">transcript &amp; editable summary</a>)

Beau reminds us that real change comes from continuous forward movement and community involvement, not just isolated moments like milestones or elections.

</summary>

"Real change, real movement that you helped accomplish."
"Change isn't really going to come from D.C. It's going to come from us."
"We still have a lot to do, because there's a lot that's going to have to be cleaned up, regardless of the outcome."
"Y'all did most of the heavy lifting on all that."
"We can't just stop."

### AI summary (High error rate! Edit errors on video page)

This video marks the 1000th one on YouTube, but it's not necessarily an accomplishment.
The channel has made tangible changes by supplying essentials to hospitals, supporting shelters, aiding in hurricane relief, and funding schools in the global south.
Beau credits the viewers for their involvement and heavy lifting in achieving these accomplishments.
Despite the significance of milestones like November 3rd, true change comes from continuous forward movement, not isolated moments.
Beau stresses the importance of not stopping after specific dates but maintaining momentum to create real change in the world.
He expresses gratitude towards viewers for their contributions in making real change happen globally.

Actions:

for viewers, community members,
Keep up the momentum for real change (implied)
Continue creating tangible impact in the world (implied)
Support community efforts for positive change (implied)
</details>
<details>
<summary>
2020-10-11: Let's talk about the US Constitution and a slogan.... (<a href="https://youtube.com/watch?v=s_kq5bOcSQs">watch</a> || <a href="/videos/2020/10/11/Lets_talk_about_the_US_Constitution_and_a_slogan">transcript &amp; editable summary</a>)

Beau questions the logic of restoring a Constitution that failed to prevent power imbalances and suggests considering new ways to address governance issues instead.

</summary>

"The whole idea was to protect against tyranny. So if you are saying that it failed to do that, there's no purpose in restoring it."
"If any branch was able to gain too much power, the Constitution failed."
"If you believe the Constitution is a failure, perhaps you want a new one."
"It's normally accompanied by some pretty extreme rhetoric."
"You don't need to reach into your gun cabinet if the Constitution has an issue."

### AI summary (High error rate! Edit errors on video page)

Criticizes a conservative talking point about restoring the Constitution by giving Trump more power.
Explains that the Constitution is essentially a permission slip from states to the federal government.
Points out that the Constitution established a representative democracy with checks and balances.
Questions the logic of restoring a document (Constitution) that failed to prevent one branch from gaining too much power.
Argues that if you believe the Constitution failed, it might be better to create a new one instead of restoring it.
Emphasizes that giving more power to one branch of government (like Trump) goes against the principles of checks and balances.
Stresses the importance of reading and understanding the Constitution rather than blindly following it as an icon.
Suggests that the Constitution allows for change through a legal process instead of resorting to extreme measures.
Advocates for using the existing machinery for change within the Constitution to address issues rather than resorting to violence.
Proposes ideas like ranked choice voting and abolishing the electoral college to address power consolidation issues.

Actions:

for citizens, voters, activists,
Use the existing machinery for change within the Constitution to address issues (implied).
</details>
<details>
<summary>
2020-10-11: Let's talk about Trump, Biden, swing states, and polls.... (<a href="https://youtube.com/watch?v=V-mE7LFGOLw">watch</a> || <a href="/videos/2020/10/11/Lets_talk_about_Trump_Biden_swing_states_and_polls">transcript &amp; editable summary</a>)

Beau explains swing states, their impact on elections, key states for Biden, and the importance of voter turnout in determining outcomes.

</summary>

"Swing states are states where the population of likely Democratic votes and likely Republican votes are close."
"If you're a Democrat in those states and you want that landslide, you need to go vote."
"If people don't go vote, the polling doesn't matter."
"Georgia, it's amazing that Georgia's in play."
"They're not really set in stone to go to a particular party because of the population and the way they vote."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of swing states and their importance in U.S. elections.
Mentions that swing states are where Democratic and Republican votes are close.
Lists the swing states for the upcoming election.
Points out the importance of voter turnout in determining the election outcome.
Stresses the significance of states like Nevada, Wisconsin, and Pennsylvania for Biden to secure a landslide victory.
Recalls the surprise in the 2016 election due to negative voter turnout and polling inaccuracies.
Emphasizes the need for voters, especially in swing states, to participate in the election.
Talks about the limited paths to victory for Trump based on current polling.

Actions:

for voters,
Go vote in swing states (suggested)
Keep track of polling data (suggested)
</details>
<details>
<summary>
2020-10-10: Let's talk about the medical professionals, Trump, and the bird.... (<a href="https://youtube.com/watch?v=6dxBxsNxjnQ">watch</a> || <a href="/videos/2020/10/10/Lets_talk_about_the_medical_professionals_Trump_and_the_bird">transcript &amp; editable summary</a>)

Scientific American and New England Journal of Medicine criticize US leaders for inadequate response to public health crisis, urging Trump's removal.

</summary>

"We should not abet them and enable the deaths of thousands more Americans by allowing them to keep their jobs."
"They have taken a crisis and turned it into a tragedy."
"Rather than just continuing to ignore the people who may know what they're talking about, we might want to heed their advice."

### AI summary (High error rate! Edit errors on video page)

Scientific American endorsed Joe Biden, breaking from tradition.
New England Journal of Medicine, typically apolitical, criticized current political leaders for their response to the public health crisis.
The response by the nation's leaders has been consistently inadequate.
The crisis has revealed a failure in leadership in the United States.
The journal pointed out that the country's testing rates are lower than countries like Kazakhstan, Zimbabwe, and Ethiopia.
Some governors handled the crisis well regardless of party affiliations.
Many politicians politicized the use of masks.
The journal did not endorse Biden but emphasized the need for Trump to lose the election.
Despite Trump's endorsements from law enforcement, the real issues in the US are related to health and science, not crime.
Few respected scientific or medical figures are endorsing the president, with many suggesting he needs to be replaced.
The New England Journal of Medicine's statement was signed by 35 editors, indicating a serious criticism of the US's handling of the crisis.

Actions:

for voters, health advocates,
Pay attention to statements from respected scientific and medical experts, and advocate for necessary changes (implied).
Vote in upcoming elections based on candidates' responses to public health crises and trust scientific expertise (implied).
</details>
<details>
<summary>
2020-10-10: Let's talk about Republican Senators turning on Trump.... (<a href="https://youtube.com/watch?v=48PuG2SG7Ds">watch</a> || <a href="/videos/2020/10/10/Lets_talk_about_Republican_Senators_turning_on_Trump">transcript &amp; editable summary</a>)

Republicans posturing for election, American people urged to see through the distancing routine and hold accountable those who enabled Trump.

</summary>

"People in the Senate aren't supposed to be yes men. They're supposed to advocate for their constituents."
"We're going to have to de-Trumpify, go through de-Trumpification of anybody who enabled this."
"Can't forget that. It's up to the American people to see through this distancing routine."

### AI summary (High error rate! Edit errors on video page)

Republicans are finally calling out the president and distancing themselves, mainly as election posturing.
McConnell and Cornyn criticized the president's handling but didn't sound the alarm earlier.
Senators are suddenly remembering their duty as they anticipate a significant defeat.
The legislative branch should act as a real oversight, not a rubber stamp.
Beau won't easily forgive those who enabled Trump, even in future elections.
The American people need to see through the political tactics and hold a grudge against those who enabled Trump.
The sudden distancing by Republicans seems insincere, especially as Biden leads in swing states.

Actions:

for american voters,
Hold your elected officials accountable for their actions (exemplified)
Advocate for true representation in government (implied)
Support opposition candidates against those who enabled Trump (implied)
</details>
<details>
<summary>
2020-10-09: Let's talk about about Trump and the Nobel Prize.... (<a href="https://youtube.com/watch?v=ooH29L8GkgI">watch</a> || <a href="/videos/2020/10/09/Lets_talk_about_about_Trump_and_the_Nobel_Prize">transcript &amp; editable summary</a>)

Donald J. Trump did not win the Nobel Peace Prize; instead, it went to the UN's World Food Program for feeding almost 100 million people, sparking a call to revamp US foreign policy towards global emergency response.

</summary>

"Donald J. Trump did not win the 2020 Nobel Peace Prize."
"100 million people got to eat. That's pretty amazing."
"Maybe we can help solve problems before anybody's calling 911."

### AI summary (High error rate! Edit errors on video page)

Donald J. Trump did not win the 2020 Nobel Peace Prize, which was awarded to the UN's World Food Program for their work in ending food insecurity and hunger.
The World Food Program operates in 88 countries and assists almost 100 million people, especially critical this year due to conflicts and supply chain disruptions.
They aim for sustainability while addressing immediate needs, tackling hunger used as a weapon and preventing conflicts caused by food insecurity.
The organization's efforts focus on sustainable solutions that eliminate resource-based conflicts by ensuring everyone is fed.
Despite criticisms, the World Food Program's mission and effectiveness in feeding 100 million people are commendable.
Beau suggests using the organization as a blueprint for a revamped US foreign policy, shifting towards global emergency response rather than policing.
By emulating the World Food Program's approach, the US could potentially prevent conflicts before they escalate to emergency levels.
Beau anticipates President Trump's reaction to not winning the Peace Prize, expecting potential insults or attacks on the organization via Twitter.

Actions:

for global citizens,
Follow the World Food Program's lead in addressing hunger and conflict (suggested).
Advocate for a shift in US foreign policy towards global emergency response (suggested).
</details>
<details>
<summary>
2020-10-09: Let's talk about Michigan, lost causes, and a sense of purpose.... (<a href="https://youtube.com/watch?v=ud2yfIXJAe0">watch</a> || <a href="/videos/2020/10/09/Lets_talk_about_Michigan_lost_causes_and_a_sense_of_purpose">transcript &amp; editable summary</a>)

Beau stresses the importance of building a just society and finding purpose in helping others rather than resorting to destructive actions.

</summary>

"Building, living is harder, much more fulfilling too."
"Build something for them. Work to build a just society."
"You don't have to destroy anything. Build something better."
"You don't have to hurt anybody. You just have to build something better."
"You can help bring them there. Isn't that better?"

### AI summary (High error rate! Edit errors on video page)

Beau starts by discussing the desire to belong, a sense of purpose, insurmountable odds, lost causes, and a specific incident in Michigan.
He shares his experience of being on Twitter and the sudden shift in mood caused by a question about the motivations of individuals in a difficult situation.
Some individuals may have ended up in challenging circumstances because they sought belonging and purpose, leading to severe consequences.
The romanticization of battling insurmountable odds in the U.S. has sometimes been misconstrued as resorting to violence, neglecting the importance of building and living.
Beau acknowledges the allure of facing challenges and the satisfaction of overcoming them but stresses that building and living are more difficult yet fulfilling endeavors.
He criticizes how working-class individuals were manipulated by those in power to serve their interests without realizing the repercussions.
The exploitation of people’s desire for purpose by societal and political structures is condemned by Beau.
Advocating for building a just society and supporting those at the bottom, Beau suggests that true fulfillment lies in constructing a positive legacy rather than engaging in destructive actions.
By working towards building a better world for the marginalized, individuals can face significant challenges and make a lasting impact without resorting to violence.
Beau concludes by encouraging listeners to find purpose in helping those who have lost hope and making a difference in their lives without resorting to destructive means.

Actions:

for social activists, community builders.,
Build something for those at the bottom (suggested).
Work towards creating a just society (implied).
Support marginalized communities in building a better world (exemplified).
</details>
<details>
<summary>
2020-10-08: Let's talk about Trump, Michigan, and Whitmer.... (<a href="https://youtube.com/watch?v=wH7ZYj2k3D4">watch</a> || <a href="/videos/2020/10/08/Lets_talk_about_Trump_Michigan_and_Whitmer">transcript &amp; editable summary</a>)

Beau stresses the critical need for the president to commit to a peaceful transfer of power to prevent further division and potential violence in the country.

</summary>

"Force is on the table. The attempted snatch of a governor. To spark it."
"He has brought it to its knees."
"It's not a game. It's not a movie."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of discussing Michigan and what it foreshadows.
Criticizes the divisive and extreme rhetoric used by certain individuals in the country to energize their base.
Points out the refusal of the president and vice president to commit to a peaceful transfer of power.
Emphasizes the significance of committing to a peaceful transfer of power beyond legal battles.
Raises concerns about the potential consequences of not committing to a peaceful transfer of power, suggesting it sends a message that force is acceptable.
References the attempted kidnapping of a governor as an outcome of such rhetoric.
Stresses the need for the president to condemn such actions and commit to unity for the country's sake.
Asserts that the current president has escalated division in America for personal gain.
Urges for a peaceful transfer of power to prevent further incidents and unite the country.
Calls for a landslide defeat for the current president in the upcoming election.

Actions:

for american voters,
Contact local community organizations to organize peaceful unity events (implied)
Join advocacy groups promoting peaceful political discourse (implied)
Urge elected officials to publicly commit to a peaceful transfer of power (implied)
</details>
<details>
<summary>
2020-10-08: Let's talk about AOC, performative posts, and social change.... (<a href="https://youtube.com/watch?v=bsOEH7vXH3A">watch</a> || <a href="/videos/2020/10/08/Lets_talk_about_AOC_performative_posts_and_social_change">transcript &amp; editable summary</a>)

Beau explains the value of performative activism in shaping societal thought and sparking social change through exposure to new ideas and community networks.

</summary>

"If you want to change society, you have to change the way people think."
"Those posts, performative or not, are incredibly valuable because our goal is to change society."
"Performative posts are incredibly valuable."
"Our goal is to change society. We have to change the way people think."
"Social media is an incredibly, incredibly valuable tool."

### AI summary (High error rate! Edit errors on video page)

Beau dives into a social media post by AOC that draws comparisons between the Panthers' food distribution network and a project AOC was involved in.
He encourages people to look into the Black Panthers beyond the media's portrayal, noting their radical nature and effective community networks.
Critiques of AOC's post revolved around its phrasing, framing, and being labeled as performative activism.
Beau argues that the debate over the most effective method of social change is ongoing and complex, with no definitive answer.
He suggests that performative posts, even if criticized, play a valuable role in drawing attention to social issues and making them socially acceptable.
Changing societal thinking is key to societal change, and performative posts can contribute to this shift by making causes desirable and attracting support.
Beau acknowledges that not everyone can directly participate in activism but sharing social media posts can still contribute to social change.
By exposing thousands to the idea of solving local issues through community networks, AOC's post may have inspired direct engagement in the future.
Beau stresses the importance of a diversity of tactics in creating social change and the role of performative actions in shaping societal thought.
He concludes by underlining the significance of social media as a powerful tool for changing societal perspectives and the necessity of utilizing it for positive change.

Actions:

for social media users,
Share social media posts about community initiatives and social issues to raise awareness and shape societal perspectives (implied).
Encourage community engagement and involvement in local projects to address social issues and create positive change (implied).
</details>
<details>
<summary>
2020-10-07: Let's talk about Trump saving the Faith.... (<a href="https://youtube.com/watch?v=ue18_iP1vdI">watch</a> || <a href="/videos/2020/10/07/Lets_talk_about_Trump_saving_the_Faith">transcript &amp; editable summary</a>)

Beau questions the manipulation of faith for political gain and asserts Christianity's resilience against such tactics.

</summary>

"There is a concerted effort by a group of people to use your faith to manipulate you."
"Christianity is not in danger. It has survived a lot."
"It's going to get stomped out because people believed politicians and were led astray."

### AI summary (High error rate! Edit errors on video page)

Addressing Eric Trump's claim of his father saving Christianity, Beau questions the validity of this statement.
Beau points out the lack of evidence for Christianity being in danger from legal or legislative battles.
He criticizes the fear-mongering and manipulative tactics used to sway people's emotions.
Beau questions if Trump exemplifies Christian teachings like loving thy neighbor and helping the needy.
He warns about the danger of blending religion and the state, urging faith leaders to address this issue.
Beau stresses that Christianity has survived various challenges and won't be destroyed by politicians.
He cautions against blindly following leaders who don't embody the values of the faith.
Beau calls for a rebuke of the notion that Christianity is under threat from politicians manipulating faith for their agenda.

Actions:

for faith leaders and believers,
Address the issue of blending religion and state within your community (suggested)
Encourage critical thinking and discernment regarding political manipulation of faith (implied)
</details>
<details>
<summary>
2020-10-06: Let's talk about the state of the President and the economy.... (<a href="https://youtube.com/watch?v=visSYGjQgow">watch</a> || <a href="/videos/2020/10/06/Lets_talk_about_the_state_of_the_President_and_the_economy">transcript &amp; editable summary</a>)

Trump's concerning decision to delay stimulus post-hospitalization raises questions about his judgment, putting the economy at risk and his leadership capabilities in doubt.

</summary>

"If the economy tanks, it's Donald Trump's fault."
"We cannot have a president who is behaving out of character."
"We've got tough times ahead."
"It may get worse."
"It could lead to some pretty grim scenarios."

### AI summary (High error rate! Edit errors on video page)

Trump's unusual behavior raises concerns after his recent illness, leading to questionable decisions.
Federal Reserve Chair warns of dire consequences without stimulus, calling the scenario tragic.
Despite this, Trump abruptly halts stimulus negotiations until after the election, prioritizing politics over economy.
Beau questions if Trump's actions indicate neurological impacts from his recent hospitalization.
Trump's decision to delay stimulus may harm the economy and damage his political standing.
Beau suggests that Trump's behavior is out of character and potentially influenced by health issues.
Concerns arise about having a president who may not be thinking clearly or rationally in tough times.
Beau points out that Trump's decision-making process appears flawed and lacking in listening to others.
The situation could lead to disastrous outcomes and needs national attention and discourse.
Ultimately, Beau expresses worry about the future under a leader making erratic decisions.

Actions:

for voters, concerned citizens,
Have national level discourse on the implications of a leader behaving erratically (suggested)
Stay informed and engaged in political decisions and their potential impact on the economy (implied)
</details>
<details>
<summary>
2020-10-06: Let's talk about being tired and settling into a routine.... (<a href="https://youtube.com/watch?v=vjddKxBcxB4">watch</a> || <a href="/videos/2020/10/06/Lets_talk_about_being_tired_and_settling_into_a_routine">transcript &amp; editable summary</a>)

Beau addresses national exhaustion, urges continued caution, and stresses the importance of not becoming complacent in the ongoing crisis.

</summary>

"It's not over and it's not routine."
"Mitigate all the risk you can because there's a whole bunch of people out there who aren't."
"Don't become overconfident. Don't settle into a routine."

### AI summary (High error rate! Edit errors on video page)

Addresses the national exhaustion and feeling of being tired.
Mentions the cliche of characters being close to retirement in movies and its truth.
Talks about staying cautious and avoiding routines in high-risk environments.
Compares the current national situation to being at the end of a tour.
Emphasizes the need to continue taking precautions despite feeling exhausted.
Urges everyone, especially medical providers, to mitigate risks and not become complacent.
Points out that the situation is not routine and stresses the importance of following safety measures.
Mentions the need to stay vigilant until a safe treatment or vaccine is available.
Reminds about the ongoing public health crisis and the necessity to keep fighting.
Advises being prepared for emergencies like hurricanes and not underestimating the current situation.

Actions:

for general public,
Mitigate risks and continue following safety measures (exemplified)
Be prepared for emergencies like hurricanes (exemplified)
</details>
<details>
<summary>
2020-10-05: Let's talk about the Secret Service being told about the ride.... (<a href="https://youtube.com/watch?v=i9Xa5xm0A0Y">watch</a> || <a href="/videos/2020/10/05/Lets_talk_about_the_Secret_Service_being_told_about_the_ride">transcript &amp; editable summary</a>)

Beau questions the president's risky decisions and urges a national debate on his irresponsibility and ability to protect the country.

</summary>

"He put lives at risk for a propaganda stunt, to waive it people."
"He can't protect his staff. He can't protect his house with an army of advisors and experts."
"We have to decide whether or not we, as a country, are willing to accept this kind of recklessness for another four years."

### AI summary (High error rate! Edit errors on video page)

Beau is asked to accompany a positive press member in a sealed vehicle for a quick ride around the block.
The decision is dictated by the president, despite the unnecessary risk involved.
Beau questions the need for such a risk, especially considering the positive status of the press member.
Despite concerns, Beau is reminded that the president's wishes must be followed unquestioningly.
Beau references a Tennyson quote about unnecessary risks not ending well, which is dismissed.
Precautions are promised, but Beau doubts their effectiveness given the current situation in front of Walter Reed.
Beau points out that instead of risky stunts, the focus should be on protecting the president's safety and dignity.
Beau questions the president's ability to understand and mitigate risks, given his track record.
The president's irresponsibility and inability to protect those around him are emphasized by Beau.
Beau ends by urging a national discourse on whether the country can afford another term of such recklessness.

Actions:

for citizens, voters,
Start a national discourse on the president's actions and their implications (implied)
Engage in informed political debates and decision-making (implied)
</details>
<details>
<summary>
2020-10-05: Let's talk about Trump wagging the dog.... (<a href="https://youtube.com/watch?v=fDr3E4b9K4k">watch</a> || <a href="/videos/2020/10/05/Lets_talk_about_Trump_wagging_the_dog">transcript &amp; editable summary</a>)

Beau explains why the claim that Trump manufactured a situation isn't a good talking point, suggesting focusing on Trump's inability to protect the country due to potential health effects may be more effective, especially for reaching undecided voters.

</summary>

"It's because I think it's a bad talking point."
"It was on TV. We all saw it."
"I don't think it's an effective talking point for the people who are undecided at this point."
"I think that is a more effective line."
"I think that's a more effective talking point."

### AI summary (High error rate! Edit errors on video page)

Explains why he doesn't buy into the idea that Trump manufactured a situation.
Questions the effectiveness of using this as a talking point.
Suggests that focusing on Trump's inability to protect the country due to potential health effects might be more effective.
Points out that regardless of the truth, what matters is that it was on TV and many believe it.
Advocates for using Trump's story against him rather than pointing out his past lies.
Emphasizes the importance of reaching undecided voters and how certain talking points can sway them.
Urges to focus on undermining Trump's main lines of attack against Biden.

Actions:

for voters, political activists,
Roll with effective talking points (suggested)
Use Trump's story against him (suggested)
</details>
<details>
<summary>
2020-10-04: Let's talk about the elephant in the room with Trump.... (<a href="https://youtube.com/watch?v=KIoU-6hbQiM">watch</a> || <a href="/videos/2020/10/04/Lets_talk_about_the_elephant_in_the_room_with_Trump">transcript &amp; editable summary</a>)

Trump administration's mismanagement spotlights existing inequities, raising concerns about President's priorities and ability to lead effectively.

</summary>

"The mismanagement that has occurred during this administration simply put a floodlight on a lot of the inequities that already existed."
"If the president cannot provide for the safety, security, and dignity of the White House with a team of experts with a $2.25 billion budget, he probably can't do it for the entire country."
"I think he's in one of those feedback loops where he's getting bad advice."

### AI summary (High error rate! Edit errors on video page)

Trump administration's mismanagement spotlighted existing inequities in the US.
President Trump prioritizes his interests over the country's best interest.
Lack of temporary power transfer despite President's distractions.
The focus on campaign over discharging duties raises concerns on who's running the country.
Secret Service's primary duty is executive safety, yet the President overrides their advice.
Concerns arise that the President is more focused on re-election than national safety.
Pence hasn't taken over due to image concerns, leaving doubts on effective leadership.
President's mismanagement is becoming increasingly apparent.
Beau questions the President's ability to lead the entire country if he struggles with White House safety.
Suggests that if the President managed the country well, campaigning wouldn't be a top priority.

Actions:

for voters, concerned citizens,
Question leadership priorities and demand accountability (implied)
Stay informed on political actions and decisions (suggested)
</details>
<details>
<summary>
2020-10-04: Let's talk about Trump and adapting to a mystery.... (<a href="https://youtube.com/watch?v=3fFG6pRL4t8">watch</a> || <a href="/videos/2020/10/04/Lets_talk_about_Trump_and_adapting_to_a_mystery">transcript &amp; editable summary</a>)

Republicans face a reality-check on their response to a situation compared to Democrats, urging adaptation over loyalty.

</summary>

"The proper thing to do when you receive new information is to change your way of thinking, to adapt to it."
"Adaptation may literally be the difference between life and death."
"Democrats were attempting to lead by example."
"Republicans are generally not swayed by studies and statistics."
"Are you going to adapt?"

### AI summary (High error rate! Edit errors on video page)

Republicans face a mystery in explaining their higher impact by something compared to Democrats.
The theories Republicans have come up with to explain their situation are not grounded in reality.
Democrats were given a briefing that Republicans apparently did not hear, leading to different responses to the situation.
The briefing emphasized washing hands, not touching faces, staying home, wearing masks, and social distancing.
Republicans may have chosen not to hear the briefing or were explicitly told not to pay attention to it.
Democrats, as a general rule, tried to lead by example by wearing masks in public.
Republicans, on the other hand, often did not wear masks as a display of loyalty to Trump.
Republicans tend to be influenced more by anecdotal information rather than studies and statistics.
Beau urges Republicans to adapt to the new information they have received for the sake of their well-being.
He stresses the importance of changing one's thinking in response to new information, especially when it can be a matter of life and death.

Actions:

for republicans,
Wash your hands, don't touch your face, stay at home, wear a mask, and socially distance (suggested).
Brush up on the leaked briefing information available on government websites (suggested).
Adapt to new information received for the sake of well-being (suggested).
</details>
<details>
<summary>
2020-10-03: Let's talk about why Trump's decisions and feedback loops.... (<a href="https://youtube.com/watch?v=b-JE25WXH6s">watch</a> || <a href="/videos/2020/10/03/Lets_talk_about_why_Trump_s_decisions_and_feedback_loops">transcript &amp; editable summary</a>)

Beau explains how Trump's decisions are influenced by feedback loops, showcasing the impact of extreme viewpoints and emotional resonance.

</summary>

"Are the feedback loops that you are in, are they encouraging growth and good behavior?"
"He's just easily manipulated."
"Take note of and watch out for in your life."
"The sign of an enlightened mind is the ability to entertain an idea without accepting it."
"If it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analogy of radar stations near schools and Trump's decision-making.
Feedback loops: action, measurement, emotional resonance, options.
Trump's characteristics: huge ego, poor consumer of information.
Trump's feedback loop: influenced by extreme viewpoints.
Explanation of Trump's decisions through feedback loops.
Example of Trump's response to debate polls.
Food boxes disliked by many but praised by right-wing media.
Trump's response to law and order discourse.
Influence of feedback loop on Trump's campaign.
Downplaying of public health issue by Trump and his followers.
Theory: Trump easily manipulated due to feedback loop.
Importance of being aware of feedback loops in personal life.

Actions:

for voters, citizens, activists,
Monitor your sources of information and ensure they encourage growth and good behavior (suggested)
Stay informed about current events from diverse and reliable sources (implied)
Encourage critical thinking and awareness of feedback loops in your community (implied)
</details>
<details>
<summary>
2020-10-03: Let's talk about the election and an event that never happened.... (<a href="https://youtube.com/watch?v=ESHxkY9IdpE">watch</a> || <a href="/videos/2020/10/03/Lets_talk_about_the_election_and_an_event_that_never_happened">transcript &amp; editable summary</a>)

In an alternate historical crisis, Beau explains the importance of waiting for directives and responding thoughtfully rather than reacting impulsively in today's world dominated by sensationalism and instant reactions.

</summary>

"They were trained not to respond at every provocation, to wait for that code word, to respond rather than react."
"The reality is we have turned into a country that thrives on the 24-hour news cycle and hot takes."
"In most cases, it's better to wait to see what happens when the dust settles and respond rather than react."

### AI summary (High error rate! Edit errors on video page)

In an alternate historical timeline set in 1977 Western Europe, NATO faces a crisis where members of Parliament loyal to the Soviets are elected.
NATO's dilemma is preventing these members from being seated without undermining democracy.
The Soviets threaten military intervention if their members are not seated, leaving NATO paralyzed.
Individuals in a stay-behind organization, part of a Cold War network, are trained to resist potential Soviet occupation.
Despite the crisis never materializing, these organizations existed across Europe and engaged in clandestine activities.
Beau mentions the importance of waiting for directives rather than reacting impulsively to situations.
He contrasts this patient approach with the modern trend of instant reactions fueled by sensationalism and the 24-hour news cycle.
Beau encourages society to adopt a more thoughtful and measured response to events rather than succumbing to emotional reactions.

Actions:

for global citizens,
Establish a network for preparedness and response in case of crises (implied)
Train individuals to respond thoughtfully rather than react impulsively to events (implied)
</details>
<details>
<summary>
2020-10-02: Let's talk about influencers taking a political stand.... (<a href="https://youtube.com/watch?v=z_dkjVS6Clg">watch</a> || <a href="/videos/2020/10/02/Lets_talk_about_influencers_taking_a_political_stand">transcript &amp; editable summary</a>)

Influencers are urged to acknowledge the political undercurrents in all topics, condemn bigotry, amplify marginalized voices, and uplift others, promoting goodness over political expertise.

</summary>

"Everything's political. Everything is political."
"You don't have to become a political expert. Just try to be a good person."
"Never kick down. Only punch up."
"Just have to talk about the things that relate directly to your genre."
"If you take the moral stands, you don't have to come out and endorse a candidate."

### AI summary (High error rate! Edit errors on video page)

Influencers facing pressure to take political stands to retain audience.
All topics have political implications, from travel to food to fashion.
Encourages influencers to condemn bigotry and amplify voices of marginalized.
Advocates for self-education on issues related to their content.
Emphasizes the importance of not attacking those with less power.
Pushes for uplifting others instead of tearing them down.
Being a good person is key, rather than being a political expert.

Actions:

for influencers, content creators,
Condemn bigotry and police comments on your platform (suggested)
Speak up for marginalized voices using your platform (suggested)
Self-educate on issues related to your content (suggested)
Never attack those with less power; focus on uplifting (suggested)
</details>
<details>
<summary>
2020-10-02: Let's talk about hope, reality, and Trump getting it.... (<a href="https://youtube.com/watch?v=Z2ZhQUeUPQk">watch</a> || <a href="/videos/2020/10/02/Lets_talk_about_hope_reality_and_Trump_getting_it">transcript &amp; editable summary</a>)

The President's COVID diagnosis doesn't shield him from criticism; everyone should take precautions seriously.

</summary>

"I truly do hope they make it through this."
"Wash your hands. Don't touch your face. Stay at home."
"The office of the Presidency is more significant than working at Winn-Dixie."

### AI summary (High error rate! Edit errors on video page)

President of the United States tested positive for COVID-19, impacting many others.
Expresses sincere hope for their full recovery and that they go through it without incident.
Points out the disparity between working class Americans and those with access to top healthcare.
Criticizes the administration for prioritizing forcing people back to work over their safety during the pandemic.
States that the office of the Presidency is more significant than working at a regular job.
Acknowledges that the President testing positive won't shield him from rightful criticism.
Notes the bad timing of the President's diagnosis so close to the election.
Emphasizes the seriousness of the situation and the need for everyone to take precautions seriously.
Encourages basic preventive measures like washing hands, wearing masks, and staying at home.

Actions:

for general public,
Wash your hands, don't touch your face, stay at home, wear a mask (suggested)
</details>
<details>
<summary>
2020-10-02: Let's talk about Trump going to the hospital.... (<a href="https://youtube.com/watch?v=uuZgYATHO8w">watch</a> || <a href="/videos/2020/10/02/Lets_talk_about_Trump_going_to_the_hospital">transcript &amp; editable summary</a>)

Beau stresses the critical importance of rejecting Trump at the polls to prevent the survival of "Trumpism" and its divisive policies.

</summary>

"It's incredibly important that the president fully recovers or suffers no symptoms."
"If we don't show at the polls that Trump's policies are anti-American, that Trump's policies are the cause of the division, they'll continue."
"I want him to survive probably more than anybody because I think that's more important than anything."
"It's not just a person anymore. It's Trumpism."
"I want the American voters to."

### AI summary (High error rate! Edit errors on video page)

Trump is on his way to Walter Reed with the White House claiming everything is fine.
There's skepticism due to the administration's lack of truthfulness.
The importance of Trump fully recovering or having no symptoms is not out of sympathy but to avoid the survival of "Trumpism."
It's vital for Trump to lose at the polls to reject his policies and not let them be seen as martyrdom.
Rejecting Trump at the polls is rejecting the hate, divisiveness, and authoritarian rule he represents.
The rejection of Trump by all marginalized demographics in the country is significant.
Beau isn't overly concerned about who wins in the election as long as Trump loses.
Surviving a negative outcome for Trump prevents the people from rejecting his policies collectively.
The defeat of Trump at the polls is critical for the nation's soul to reject everything he stands for, not just his person.
Beau stresses the importance of American voters defeating Trump rather than a public health issue.

Actions:

for american voters,
Reject Trump's policies by voting against him in the upcoming election (implied)
Encourage others to vote against Trump to reject hate, divisiveness, and authoritarian rule (implied)
Advocate for marginalized communities by supporting their rejection of Trump (implied)
</details>
<details>
<summary>
2020-10-01: Let's talk about the Texas, purple fingers, and the Republican strategy.... (<a href="https://youtube.com/watch?v=p4jEU00kEBg">watch</a> || <a href="/videos/2020/10/01/Lets_talk_about_the_Texas_purple_fingers_and_the_Republican_strategy">transcript &amp; editable summary</a>)

People risked their lives for democracy, but the GOP mirrors failed state tactics to suppress votes, undermining the democratic process.

</summary>

"People in failed states risked their lives to vote, seeking a voice in their country's future."
"The GOP appears to be mirroring tactics of failed states to suppress votes deliberately."
"Republicans seem prepared to contest election results in courts rather than respecting the democratic process."

### AI summary (High error rate! Edit errors on video page)

People in failed states risked their lives to vote, seeking a voice in their country's future.
They had to ensure orderly voting locations and convince others it was worthwhile.
Intimidation by "goons" was a real concern for voters trying to make a change.
Texas's Republican strategy limits ballot drop-off locations to sow confusion and discourage voting.
The president's rhetoric undermines election integrity, suggesting votes may not be counted.
Poll watchers are seemingly being used to surveil and intimidate voters.
The GOP appears to be mirroring tactics of failed states to suppress votes deliberately.
Framing the strategy as a public health measure is seen as an excuse to limit voting access.
Beau finds the strategy embarrassing and infuriating, hindering the democratic process.
Voters are advised to stay informed about changing locations and budget extra time to vote.
Beau urges people to question the credibility of sources spreading misinformation.
When facing intimidation at polling places, Beau suggests wearing nondescript clothing and avoiding confrontation.
The goal appears to be preventing a high voter turnout to avoid a potential landslide against them.
Republicans seem prepared to contest election results in courts rather than respecting the democratic process.
Beau sees this as a deliberate attempt to undermine the core principles of representative democracy in the U.S.

Actions:

for voters, activists, citizens,
Stay informed about changing voting locations and budget extra time to vote (implied)
Question the credibility of sources spreading misinformation
Wear nondescript clothing, avoid confrontation, and vote peacefully (implied)
</details>
<details>
<summary>
2020-10-01: Let's talk about both sides.... (<a href="https://youtube.com/watch?v=BJ5MC63JWrU">watch</a> || <a href="/videos/2020/10/01/Lets_talk_about_both_sides">transcript &amp; editable summary</a>)

Beau talks about bias, Trump, and Biden, stressing common sense and objective assessment.

</summary>

"I don't believe the country can survive another four years of Trump."
"There is a lot of injustice in the world. I am not taking the side of the oppressor."
"That's not bias. That's applying the same standard."
"Biden is objectively less harmful."
"I just want him to stop the decline into being a failed state."

### AI summary (High error rate! Edit errors on video page)

Addressing comments on bias towards Trump and Biden.
Defining bias as unfair favoritism.
Stating his role on the channel is to apply common sense to current situations.
Pointing out Trump's alignment with divisive and hateful individuals.
Expressing that Biden is objectively less harmful.
Anticipating critiquing Biden's actions soon.
Acknowledging Biden's leadership as a private citizen.
Sharing a personal encounter at a convenience store discussing the debates.
Not believing the country can survive another four years of Trump based on his actions.
Distinguishing between bias and applying consistent standards.
Rejecting neutrality in the face of injustice.

Actions:

for viewers, political observers,
Apply common sense to current situations (implied)
Critique leaders objectively based on actions (implied)
Engage in political discourse with awareness and understanding (implied)
</details>
