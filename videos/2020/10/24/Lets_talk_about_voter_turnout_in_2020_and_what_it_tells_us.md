---
title: Let's talk about voter turnout in 2020 and what it tells us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CVNaUG0HrNg) |
| Published | 2020/10/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Predicted voter turnout is incredibly high, with 150 million people expected to vote, around 65% turnout in the US.
- Historical parallels to the 1908 election where high turnout was seen, with similar strategies and concerns about crowd sizes.
- One candidate in 1908 didn't seek establishment support, campaigned against the elite, and embraced new technology like stumping.
- Teddy Roosevelt, a popular figure, supported Taft for the presidency, resulting in a high voter turnout and Taft's win.
- Experts suggest a potential landslide in the upcoming election due to high voter enthusiasm and mail-in voting accessibility.
- In a representative democracy like the US, high voter turnout should be encouraged to uphold the democratic ideals.
- Beau points out the importance of identifying those who try to suppress the vote, as it goes against the fundamental principles of America.
- He warns about the dangers of individuals undermining democratic values for personal gain and urges vigilance against such efforts.

### Quotes

- "To oppose high voter turnout, to attempt to suppress the vote, is to undermine the very ideas this country was founded on."
- "Those who believe in the ideas that America is supposed to embody, the idea that the people are the leader, they should want high voter turnout."
- "There is a systemic effort by one party to suppress the vote."
- "If they're willing to sell out a pillar of what's supposed to make up America, well they will certainly sell out you."
- "Y'all have a good day."

### Oneliner

Predicted high voter turnout in the US draws historical parallels to past elections, raising concerns about suppression efforts and the importance of upholding democratic values.

### Audience

American voters

### On-the-ground actions from transcript

- Investigate efforts to suppress the vote and support organizations working against voter suppression (exemplified)
- Encourage voter engagement and support initiatives that facilitate accessible voting methods (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of historical voter turnout, the significance of mail-in voting, and the dangers of voter suppression tactics.

### Tags

#VoterTurnout #HistoricalParallels #VoterSuppression #Democracy #MailInVoting


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the predicted voter turnout, what we can learn from that,
and some historical parallels that, well, I find entertaining.
Okay so if you don't know, they are predicting incredibly high turnout.
They're predicting 150 million people vote.
That's about 65%.
That's about 65% turnout for those overseas.
That's really high here.
It's probably not in your country, but that's incredibly high in the US.
The last time we had turnout like that was in 1908.
And the situation really wasn't that different on some levels.
There were some differences such as the incumbent party and the Republicans hadn't gone after
the racist vote yet.
They hadn't executed the southern strategy.
That didn't happen for another half century or so.
But one candidate, he had a couple of failed presidential bids before, like Trump in 2000.
But in 1908 he used a different strategy.
He didn't try to get the establishment of the party behind him.
He went after local people and he campaigned against the elite.
And he did it in a unique way.
He embraced the technology of the time.
They didn't have Twitter, but there were new modes of travel.
And he started stumping.
He changed the way politicians politic by doing this.
And along the way he became very concerned about his crowd sizes.
Opposing him, you had kind of a boring guy who was picked by the establishment of the
party.
Teddy Roosevelt was incredibly popular.
He probably could have won a third term if he wanted.
But he didn't.
He went to Taft and was like, hey, we want you to run.
I think you'd be the good candidate at the convention.
Of course, Taft wins.
You know, their guy had blessed him, so to speak.
Taft picked up a couple of Teddy's progressive policies and ran on those.
Incredibly high voter turnout.
Taft got 321 electoral votes.
That's good for today.
Back then you only needed 242 to win.
He got 51% of the popular vote, whereas Bryan, the other guy, got 43.
The remaining percentages were made up of third parties, like the Socialist Party and
I want to say the Prohibition Party.
Could be wrong on that.
Might want to look that one up.
Not really that different.
And because of this example, a lot of experts today are suggesting that we're going to
see a landslide because of this.
And that may be true.
People are enthusiastic about their candidate or enthusiastic about the other guy leaving,
so they're getting out and voting.
That may be true.
That may be an element of this.
But I would also suggest that because of mail-in voting, it's easier.
If it's accessible, maybe people will engage in that civic activity.
I would also point out that in a representative democracy like we have in the U.S., people
should want high voter turnout.
Those who believe in the ideas that America is supposed to embody, the idea that the people
are the leader, they should want high voter turnout.
To oppose high voter turnout, to attempt to suppress the vote, is to undermine the very
ideas this country was founded on.
It might be worth looking into who's trying to suppress the vote now.
Who doesn't want people voting?
Who's filing lawsuits?
Who's trying to limit mail-in voting?
Because those people, not only do they know they don't have support of the people, they're
actively undermining the ideas in the Constitution, the ideas of America for their own gain.
I don't know that people like that should be in office.
Because at the end of the day, that's what they're doing.
There is a systemic effort by one party to suppress the vote.
It's something we should pay attention to.
Because if they're willing to sell out a pillar of what's supposed to make up America, well
they will certainly sell out you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}