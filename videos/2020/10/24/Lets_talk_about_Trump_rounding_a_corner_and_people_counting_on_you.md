---
title: Let's talk about Trump, rounding a corner, and people counting on you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LZNvGFdaYdI) |
| Published | 2020/10/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump mentioned "rounding a corner" in debates, but Beau views it differently.
- The US recently hit its highest daily new cases ever: 80,005.
- Experts predict six-figure daily cases soon.
- Trump hasn't attended task force meetings in months, relying on Scott Atlas instead of Fauci's advice.
- Atlas spread misinformation about masks' effectiveness, leading Twitter to remove his tweet.
- A new study shows wearing masks and following guidelines can save lives.
- If 95% of Americans comply, 130,000 lives can be saved by February.
- People resistant to wearing masks often want to appear tough or brave.
- Beau urges everyone to wash hands, avoid touching faces, stay home, wear masks, and practice social distancing.
- Despite fatigue setting in, precautions must continue as 130,000 lives depend on it.

### Quotes

- "Your country is calling. They need you to wear a mask."
- "We cannot trust this administration to make the right calls."
- "You want to be a hero? Wear a mask."
- "We have to stay in the fight until it's over."
- "It's really that simple."

### Oneliner

Beau warns against trusting the administration's handling of COVID-19, urging heroism through mask-wearing to protect lives and maintain precautions.

### Audience

General public

### On-the-ground actions from transcript
- Wear a mask, wash hands, avoid touching face, stay home, and practice social distancing to protect lives (implied).

### Whats missing in summary

The full video provides additional context and a deeper understanding of the importance of continued vigilance against COVID-19.

### Tags

#COVID-19 #PublicHealth #MaskWearing #Precautions #Trust #Administration


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about President Donald Trump and rounding a corner, because
he said that in the debates.
We're rounding a corner.
Maybe.
I mean, I see that as an analogy but not the way he meant it.
So we're going to talk about rounding a corner and how we can stop from rounding that corner.
If you don't know, if you missed the news yesterday, yesterday was our highest number
of daily new cases.
Period.
That's the end of the sentence.
Not this week, not this month, not from some arbitrary point in the past, ever.
Highest number here in the US.
80,005.
The experts are predicting six figure days soon.
Rounding a corner, huh?
To me it makes sense, because corners are dangerous.
It's a good spot to walk into an ambush if you don't know what's around it.
And make no mistake about it, Trump does not know what's around the corner, because as
we recently found out from Fauci, he hasn't attended a task force meeting in months.
He's not listening to Fauci and the other experts anymore.
He is reportedly relying on the advice of Scott Atlas.
You may recognize that name.
He's the person who Twitter removed one of his tweets because they said it was misinformation.
It was misinformation about this very thing.
He was saying that masks weren't really effective.
In related news, a new study just came out.
If 95% of Americans wear a mask in public, do what they're supposed to do as far as distancing
and all the non-pharmaceutical stuff, 130,000 lives can be saved by February.
If compliance with that drops to 85%, 95,000 lives will be saved.
I don't know that we actually have 85% compliance with wearing a mask and doing the stuff you're
supposed to.
The people who don't are often those who want to show that they're tough, want to show that
they're brave.
These are the same people who talk about, well, if my country called, I'd go to the
front lines.
Your country is calling.
They don't need you on the front lines.
They need you to wear a mask.
There's 130,000 people counting on you.
Please wash your hands.
Don't touch your face.
Stay at home.
If you have to go out, wear a mask.
Do the distancing you're supposed to.
I know for a lot of people, I've heard that over and over and over again, and people ask,
why do I keep saying it?
Everybody on the channel knows.
Only 60% of people who watch any given video are subscribed.
With the average number of views, that means about 50,000 people are hearing that for the
first time, at least from me.
I feel obligated to say it every once in a while.
Aside from that, we've been at this a long time.
There's probably some fatigue setting in.
Can't let your guard down.
Can't relax.
Can't stop taking precautions.
130,000 people are counting on you.
We have to stay in the fight until it's over.
We don't just get to stop.
So please, you want to be a hero?
You want to protect your country?
Wear a mask.
It's really that simple.
We cannot trust this administration to make the right calls.
It's not going to go away like a miracle.
We're headed into winter.
It's going to get worse.
And we're already in a bad place.
No matter what, when winter comes, the numbers are going to go up.
We're already at a record high.
We can't allow that.
We have to maintain the precautions.
We have to get better at the precautions.
We have to do what 130,000 people are counting on us doing.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}