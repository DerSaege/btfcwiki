---
title: Let's talk about the Supreme Court confirmation and power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8wapGhUGxAc) |
| Published | 2020/10/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the new Supreme Court justice for her lack of qualifications and inability to answer basic questions about the Constitution during her confirmation hearing.
- Points out that the Republican Party is willing to wield power to achieve their goals, regardless of qualifications or consequences.
- Suggests that if Democrats win the House, Senate, and White House, they should use their power decisively to make significant changes, including restructuring the courts and passing progressive policies.
- Expresses disappointment in both Republican and Democratic politicians for either selling out the country or not fighting hard enough.
- Urges the Democratic Party to exercise power and implement deep, systemic changes to energize and retain support from voters.
- Warns against the Democratic Party following the Republican Party's shift towards the far right and calls for real change to prevent becoming like the current Republicans.
- Emphasizes the need for Democrats to take action to nullify the Supreme Court justice's vote due to her lack of understanding of basic freedoms protected by the Constitution.
- Concludes by stressing the importance of real change and the necessity for House and Senate action to drive it, rather than relying on the White House.

### Quotes

- "She's not qualified for the job, period."
- "It's really that simple."
- "If the Democratic party has the House, the Senate, and the White House and utilizes that power to make things better for the average person."
- "The Democratic Party seems very unwilling to exercise power, very unwilling to wield the power when they have it."
- "Deep, systemic change and it's going to have to come from the House and the Senate."

### Oneliner

Beau criticizes the lack of qualifications in the new Supreme Court justice, urges Democrats to exercise power for real change, and stresses the importance of systemic change originating from the House and Senate.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Rally support for deep systemic changes by engaging with local political organizations and campaigns (implied)
- Advocate for nullifying the Supreme Court justice's vote through contacting elected representatives and supporting legislative actions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current political landscape, urging for significant changes to be made by the Democratic Party to maintain power and drive real systemic change.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So, yeah, we have a new Supreme Court justice.
You know, the one that couldn't,
couldn't tell you what freedoms were protected
by the First Amendment during the hearing.
She's now in the Supreme Court
because that seems like a really good idea.
I'm sure nothing can go wrong there.
She's not qualified for the job, period.
She's not qualified to pass basic civics.
She could not answer that question.
When you are applying for the highest court in the land, there shouldn't even be a pause
when asked that question.
And she couldn't respond.
She had to get help.
The reason she is now sitting on the Supreme Court is because the Republican Party is not
afraid to wield power when they obtain it.
It's really that simple.
It helps that they don't have much of an ideological makeup.
The fact that power is kind of their motivation is helpful, but that's why Republicans were
willing to do this. If polling and turnout projections are accurate there
is a very likely possibility that the Democrats will walk away with the House,
the Senate, and the White House after this election. If that happens they have
to wield the power scorched earth I am not talking about oh well we're gonna do
an investigation no you're gonna restructure the courts plural not just
the Supreme Court see all those judges and all those federal districts but
they're like really overworked we need some more judges we might want to divide
up those districts.
What has happened during this four years is pretty much unforgivable.
There are a whole lot of people with R's next to their name that have just sold out the
country, in my opinion.
There are a lot of people with D's next to their name who I don't feel fought hard enough
to be completely honest. If the Democratic Party gets the House, the
Senate, and the White House, they have a mandate. They can make changes if they
have the will to enact them. They can do a lot of stuff. They can help out with
healthcare. They can fund all sorts of things. They can restructure the courts.
They get infrastructure packaged through. Maybe do some environmental regulations.
I understand that there are a lot of politicians with these next to their
name who are not ideologically motivated and that's fine. But if the Democratic
party has the House, the Senate, and the White House and utilizes that power to make things
better for the average person.
They will retain the House, the Senate, and the White House.
That's part of the problem is that the Democratic Party seems very unwilling to exercise power,
very unwilling to wield the power when they have it.
In this case, they can hammer enough stuff through to actually get the progressives on
their side.
Maybe get them to show up and vote next time.
Like out of wanting to support the Democratic Party rather than just trying to stop Trump.
It's funny because this is the Republican Party crumbling.
We see it.
It is shifting to a far, far right that is unidentifiable by the rank-and-file Republicans
when they sit down and look at policy.
If they sit down and look at policy.
The Democratic Party cannot just follow suit and become the new Republican Party, become
the new Conservative Party.
That's what's being rejected, hopefully.
And if it is rejected, and those projections line up with reality, the only way the Democratic
Party is going to stay in power is if they push for real change.
Deep change.
Systemic change.
The kind of change that will energize people.
them back into the political process, get them to engage beyond when it's
self-defense, because that's what this really boils down to.
There's not a lot of people voting for Biden.
They're voting against Trump.
The I'm not Trump campaign seems to have worked, worked better than I expected
going by polling, but is it going to be enough?
to maintain that support?
No. There's going to have to be change.
Deep, systemic change and it's going to have to come from the House and the Senate
because it's not going to originate in the White House.
That Supreme Court confirmation is appalling.
It is appalling. The second she was unable to answer that question, those
hearings should have been over. There's no way you give a lifetime appointment to
somebody who can't answer a basic question about the Constitution. You
don't do it, but they did because their ideology is power. Their ideology is
power and being supported by people who don't understand policy who just like to
watch the exercise of power en route for a winning team.
That's it.
Strong man authoritarianism.
If the democratic party wants to maintain the base of support, it currently seems to
be enjoying, they're going to have to actually do something and doing whatever
it takes to nullify her vote should be at the top of the list.
Because anybody who doesn't understand the basic freedoms of this country probably shouldn't
be interpreting the Constitution and ruling on whether or not we have them.
It's funny, I had a much more fond memory of judging Amy before now.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}