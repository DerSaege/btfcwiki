---
title: Let's talk about Mitch McConnell's great fiction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aZMQ467p9Ug) |
| Published | 2020/10/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mitch McConnell and the Senate pushed through a Supreme Court nominee instead of providing stimulus relief to Kentuckians.
- The nominee couldn't name the components of the First Amendment during the hearing.
- McConnell created a fiction that the Republican Party cares about the Constitution and working Americans.
- The new justice may not intentionally undermine the Constitution because she doesn't know what's in it.
- Republicans prioritize power and keeping working-class people down over helping them.
- McConnell and other senators serve the rich rather than their constituents.
- Senators were more focused on confirming Trump's nominee than on helping the economy.
- The nominee couldn't answer basic civics questions but will be interpreting the Constitution.
- Republicans care more about big companies than the common folk.
- Voters who re-elect these senators enable their actions and lack of accountability.

### Quotes

- "They care about power and keeping working-class commoners in their place."
- "The rich keep getting richer and the poor keep getting poorer."
- "You co-sign them. You say, hey, I'm okay with what they're doing."
- "You've been conditioned by that fiction to believe that the Republican Party is the party of patriotism."
- "They confirmed a Supreme Court nominee who could not tell you what the First Amendment was."

### Oneliner

Mitch McConnell and the Senate prioritized a Supreme Court nominee over providing stimulus relief, revealing their true priorities and lack of care for working Americans.

### Audience

Voters, Working Americans

### On-the-ground actions from transcript

- Hold senators accountable for their actions (implied)
- Pay attention to your representatives' priorities and hold them responsible (implied)

### Whats missing in summary

The full transcript provides detailed insights into the prioritization of power over helping working-class Americans by Republican senators.

### Tags

#MitchMcConnell #Senate #SupremeCourt #StimulusRelief #WorkingAmericans


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about
Mitch McConnell
and his leadership
in the Senate
and their great achievement.
They uh... last night
managed to ram through
a Supreme Court nominee.
Got her confirmed. Good job guys.
Congratulations and all of that.
And then they adjourned.
So people in Kentucky who are waiting on that stimulus, it's not coming.
It's not coming.
They decided
that putting somebody on the Supreme Court
who could not name the components of the First Amendment at the hearing
was more important.
Somebody who did not have the base qualifications for the job.
That was more important
than helping you out with the stimulus.
That was more important
than helping the small businesses out.
That's what mattered.
See McConnell has created this fiction,
this idea
that uh...
the Republican Party cares about the Constitution,
that they care about the working American.
In one night
they proved that wasn't true.
Because you can't put somebody on the highest court in the land
whose sole purpose
is to interpret the Constitution
when they don't know what's in it.
The good news is
the new justice will...
well, at least she won't intentionally undermine the Constitution.
I say that because there's no way she can intentionally do it because she doesn't
know what's in it.
So they do not care about the Constitution. They never have.
They care about power and
keeping
working-class commoners in their place.
That's why they don't want you to have a stimulus check.
That's why they don't want your small business
to get any help.
Because they need to keep you in debt.
They need to keep you down.
So they can sit there and say, work harder.
Don't worry, the American dream's right around the corner for you.
Which is another fiction.
It's not.
The rich keep getting richer
and the poor keep getting poorer.
Why? Because the rich
own those senators.
This isn't limited to McConnell
because unless you saw your senator
out there saying that this was a bad idea, that they shouldn't do something
that most Americans don't want them to do,
and they should focus on the thing that
most Americans do want them to do,
and you didn't see that, by the way,
because it didn't happen from the Republican Party,
but
if you didn't see that,
your senator's in on it.
Your Republican senator
played a hand in this.
Now here's the thing about it.
Had they done what they were supposed to,
hey, okay, cool,
and then they could have taken care of the nominee.
They could have focused on what mattered,
and then they could have pushed through Trump's appointment.
Not a big deal.
It's not like they were actually vetting her.
It's not like they were doing their duty.
They were just going to rubber stamp anybody he sent up.
That was proven.
So why did it take so long?
A fiction.
A show.
To trick the common folk into thinking they care. They don't.
She couldn't answer
a question on a test from a sixth grade civics course.
But now she's going to be interpreting the Constitution,
determining the limits
of your rights.
Yeah, good move.
That was definitely more important than helping out the economy, right?
Helping out the little guy.
Because they don't care about it.
That's not their goal. That's their fiction.
That's the line
they sell to working-class Americans.
They just wrap it in the American flag and pretend it's true. It's not. It's never been true.
They don't care about you.
If they did,
the priorities would have been reversed.
We would have got some kind of action on relief.
And that's the economic side of it.
That's the part they've been saying they care about.
They don't care about the two hundred twenty five thousand that are gone.
They've made that real clear.
They care about the economy. At least that's their line.
But when push comes to shove,
they really don't care about the economy.
Well, at least not your economy.
They're more than willing to give the big companies
the help they need.
Just not the little guy.
Not you.
Not the person that
votes for them
and gives them that power.
When you vote for these people,
you co-sign them.
You say, hey, I'm okay with what they're doing,
especially if you're re-electing them.
You've told them repeatedly
that you're not going to pay attention to what they do.
So as long as they
sell you an image
of an American flag and an apple pie,
well, they've got your vote.
And they can do whatever they want to you.
And you're not going to do anything about it.
You're not going to say anything about it.
Because you've been conditioned
by that fiction
to believe that the Republican Party
is the party of patriotism.
Yet, they confirmed
a Supreme Court nominee
who could not tell you what the First Amendment was.
While they let the country
twist in the wind.
Like that flag they pretend they care about.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}