---
title: Let's talk about my Biden Trump election prediction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=y-PbSFZASR0) |
| Published | 2020/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzes the upcoming presidential election dynamics and predictions.
- Mentions the unpredictability of polls based on past experiences.
- Points out the emotional reactions people have towards the election.
- Stresses that the presidency holds too much power for one person.
- Advocates for decentralizing the power of the US government.
- Suggests that the outcome of the election depends on voter turnout rather than polls or predictions.

### Quotes

- "The only thing that isn't a guess is that the presidency has too much power."
- "Nobody should have the power to upend the country the way Trump has."

### Oneliner

Beau analyzes the election dynamics, points out emotional reactions, and advocates for reducing the overwhelming power of the presidency.

### Audience

American voters

### On-the-ground actions from transcript

- Rally for decentralization of power in the US government (suggested)
- Encourage voter turnout and participation in elections (suggested)
- Advocate for limitations on the power of the executive branch (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the upcoming presidential election, the impact of emotional reactions, and a call to limit the power of the presidency for the well-being of the country.


## Transcript
Well howdy there internet people, it's Beau again.
So today I'm going to answer a message I got because I thought it was funny.
It said that I know you don't endorse candidates, but can you give us a prediction about the
presidential election and tell us who's going to win?
Sure why not.
Okay, so let's use a logic here.
Clinton is leading in all the polls, all the polls that matter, he's leading by a pretty
good margin.
So clearly, can't choose Trump as the victor.
But then again, the polls were wrong last time, right?
Clinton was leading in all those polls and she lost.
So clearly, we cannot choose Biden.
But then again, the polls were adjusted to more heavily weigh uneducated voters, their
term.
So clearly, we can't choose Trump.
But see then again, Trump's voters are motivated by fear.
And they kind of feel like they're losing their grip on the country.
And when people are motivated by fear, they're highly motivated.
So they may turn out in huge numbers.
So clearly, we can't choose Biden.
But then again, Biden leads by a bigger margin in the states that actually matter in the
swing states, you know, where your vote actually counts.
So clearly, we can't choose Trump.
I would imagine that when I went through that, people had a ups and downs.
They were excited and not excited.
They had an emotional reaction on some level to this.
Happy or sad.
I think an important takeaway here is that the office of the presidency has too much
power.
The election of one person should not create an emotional reaction on this level in this
many people.
The power of the US government is not decentralized enough.
It should be inconceivable that one person can impact the country this much.
I think that may be the most important takeaway from this election.
Now as far as who's going to win in my prediction, using that logic, it's actually really, really
simple.
Whoever has the most supporters show up and vote for him.
It's that simple.
The polls don't mean anything.
The predictions don't mean anything.
They're guesses.
They're informed guesses, they're educated guesses.
They can be informed by the state of the economy or any of the factors that people use to forecast
the results.
But at the end of the day, they're just guesses.
The only thing that isn't a guess is that the presidency has too much power.
I think we all know that now.
So it's probably something that we should be working on remedying.
We should try to figure out a way to limit the power of the executive branch because
nobody should create this much disturbance.
Nobody should have the power to upend the country the way Trump has.
The presidency's possible re-election should instill fear in their own citizens.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}