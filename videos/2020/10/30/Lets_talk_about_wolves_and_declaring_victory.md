---
title: Let's talk about wolves and declaring victory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vNlNDANJu0o) |
| Published | 2020/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gray wolves are being delisted, coming off the Endangered Species Act list.
- Beau has a bias from raising money to protect gray wolves.
- Gray wolves were nearly extinct, but successful efforts have brought stable populations back.
- Concerns arise as gray wolves' population is not stable in all areas.
- If wolves are delisted, the responsibility falls to states, which could lead to different outcomes.
- Some worry states might allow sport hunting and decimate the wolf population.
- The current success story could be lost if states don't manage the wolf population effectively.
- Beau compares the situation to public health policies being removed, leading to negative consequences.
- Beau believes the situation is not yet certain and the final chapter hasn't been written.
- Organizations are already taking action through petitions and lawsuits to protect the wolves.
- States with wolf populations tend to follow science, offering hope for a positive outcome.
- Beau expresses doubts and concerns about the premature delisting of gray wolves.
- Beau anticipates more news on this issue post-election due to expected lawsuits.
- Beau doesn't think it's a good idea to delist the wolves yet, despite being close to success.
- Beau understands the reluctance of outlets to take a hard stand due to the proximity to a possible positive outcome.

### Quotes

- "I don't think we're there yet."
- "I have doubts, I have concerns, and it seems premature to me."
- "I think it's at the point where it's guaranteed that it's going to stay there."
- "I don't fault anybody for saying, hey, it's kind of a both sides issue."
- "We did bring this animal back from the brink."

### Oneliner

Gray wolves' delisting poses risks of undoing years of conservation efforts, with uncertain outcomes ahead.

### Audience

Conservationists, Wildlife Enthusiasts

### On-the-ground actions from transcript

- Sign petitions and support lawsuits by organizations protecting gray wolves (suggested).
- Stay informed about the issue and advocate for science-based conservation efforts (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the risks associated with delisting gray wolves and the importance of continued conservation efforts to ensure their long-term survival. Viewing the full video can provide a deeper understanding of the nuances involved in this complex conservation issue.

### Tags

#GrayWolves #Conservation #Delisting #EndangeredSpecies #WildlifeProtection


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
wolves and declaring victory. We're going to do this because I got a question.
Beau, since I'm certain you will know, what's going on with the wolves? Is this a good idea
or a bad idea? I've looked and nobody outside of those involved is taking a hard stand,
even in PR. Is this a good idea and have we won or is this another bad idea in a long
string of bad ideas from the Trump administration involving the environment? Yeah, sounds about
right. Okay, so disclosure notice. The reason this person is certain that I'm going to know
something about it is that I've raised money to protect gray wolves. So I'm not exactly
objective on this one. I have a bias. So the facts of what's going on. The gray wolf is
being delisted. It's coming off of the list that is protected by the Endangered Species
Act. Is it a good idea? Okay, I'm going to go ahead and say no, it's not. From my standpoint,
it's not. But I am going to try to at least give you the other side to this so you can
kind of make up your own mind on it. For, I don't know, 40, 50 years, people have struggled
to bring this animal back from the brink and it is a success story. There are stable populations
in the upper Rockies and around the Great Lakes and it seems like a win and it could
be very easily seen that way and be painted that way and be framed that way. However,
the gray wolf pretty much existed almost everywhere in the U.S. and now it exists in little pockets.
There are some areas such as the Pacific Northwest where it's starting to make inroads but it
doesn't have a stable population yet. The worry is that if the wolf is taken off of
the list, if it's delisted, the responsibility falls to the states and we have no idea what
the states are going to do. They could manage it perfectly. That is a possibility. They
could do their part and make sure that the comeback continues. Or they could open it
up to sport and decimate the population in a few years and erase 50 years of work. That's
also a possibility. The reason nobody is taking a hard stand on it is because we're right
there. We're at the finish line. If this conversation was taking place 10 years from
now, I don't think you'd have much of an objection because I believe that in another
10 years it would be over. It would be a great success story. But as it stands right now,
I don't think that final chapter has been written yet. It's still not certain that everything
will be fine. I think the best example I can give, the best comparison, is our current
public health thing. You have a problem. You introduce policies, the numbers start going
the way you want them to go. Some politician comes along, maybe influenced by economic
interests and removes those policies. The numbers start going the other way. That's
the worry and it's a very real worry. Again, I don't think that it's time yet. At the
same time, I understand why outlets are reluctant to take a hard stand because it is, we're
right there. We're right at the finish line. I don't think we've crossed it yet. A few
more years and that would probably change. It's a matter of whether you're willing to
risk it or you want to be certain you've won. I think it would be a shame to waste 50 years
or so of work that has brought this animal back. It's nowhere near everywhere that it
used to be, but it probably never will be. I think that in a few years, though, we could
be certain that the populations would remain and that we had won. Right now, I'm not.
The only good news about this is twofold. One, the organizations that are out there
protecting the wolf, they are already sending emails about petitions and lawsuits to people
involved. I've already got some. The other good bit of news is the states that have those
populations that exist but aren't exactly stable yet, they tend to be the states that
listen to science. So it is theoretically possible that we still get a happy ending
to this story. I just, I have doubts, I have concerns, and it seems premature to me. It
seems premature. At the end of the day, once the election is over, I imagine this will
be in the news a lot because there will be lawsuits, guaranteed. But that's where it
is. Do I think it's a good idea? No. No. I don't think we're there yet. I think we're
really close. At the same time, we are so close, I can't fault anybody, any outlet
that isn't actively involved in this, for saying, hey, it's kind of a both sides issue
because we are so close. And it would be very easy for a lobbyist firm or a PR firm to paint
it as if it's already happened, as if we've already won, we've already made this great
achievement. And in a lot of ways we have. You know, we did bring this animal back from
the brink. But I don't think it's at the point where it's guaranteed that it's going
to stay there. And I think that should be the goal. So there you go. I tried to be objective,
but at the end of the day, yeah, fun to the organizations that are helping. Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}