---
title: Let's talk about the shelves this winter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mkp8KD1j3Vs) |
| Published | 2020/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about inventories, stocks, and a potential upcoming situation.
- Mentions discussing the economics behind masks eight months ago.
- Explains the difference between N95 masks, surgical masks, cloth masks, and respirators.
- Regrets not taking the previous situation seriously enough.
- Predicts possible shortages in the upcoming months, especially during winter.
- Advises preparing ahead by stocking up on essentials like canned food, paper towels, and toilet paper.
- Encourages having a stash for emergencies regardless of where you live.
- Emphasizes the importance of reducing strain by being prepared.
- Suggests getting what you need now to avoid being in a rush during shortages.
- Recommends taking oneself out of the equation by stocking up in advance.

### Quotes

- "I have been kicking myself for the last eight months over that."
- "If you have the means, if you can get ahead now, you can reduce some of that strain."
- "Go ahead and get what you need now so you're not one of the people out there straining the system then."
- "Just set it back and carry on as normal."
- "Anyway, it's just a thought."

### Oneliner

Beau predicts potential shortages in the upcoming months and advises stocking up on essentials to reduce strain and be prepared.

### Audience

Prepared individuals

### On-the-ground actions from transcript

- Stock up on essentials like canned food, paper towels, toilet paper, and other shelf-stable items (suggested).

### Whats missing in summary

Importance of being proactive and prepared for potential shortages. 

### Tags

#Preparedness #StockingUp #Shortages #Essentials #EmergencyPreparedness


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about inventories, stocks, and something that I see on the horizon.
A couple other people have noticed it, I've noticed some articles about it.
If you've been watching this channel a while, you remember about 8 months ago I went through
and explained the economics behind masks, and I talked about the various kinds and how
effective they were.
Now keep in mind this is before it got here.
The reason I talked about it, because if you watched that long ago, you know I wasn't
really that concerned.
I didn't think it was going to come here because I didn't think the politicians were
going to get in the way of the people that actually know what they're doing.
Let it never be said that I can't take a joke.
Okay, so I went through and explained that N95 masks had gone from a dollar to thirteen
dollars just at the idea that it might come here.
And I said that if it did come here, you wouldn't be able to get them.
I explained the difference between an N95, a surgical, cloth, and a respirator, said
it might be a good idea to get it even though I wasn't worried about it.
The fact that I put that little caveat in there, saying that I wasn't incredibly worried
about it, I have been kicking myself for the last eight months over that.
So I'm not going to do that this time.
My best guess is that in another month, maybe two, we're going to see another round of
empty shelves.
Won't last long, shouldn't anyway, but come winter you're probably going to see some
shortages again.
We've had lower production, we're going to have higher demand because of the holidays,
and things are already a little strained.
So now, if you have the means, if you can get ahead now, you can reduce some of that
strain.
If you can get what you need now and just set it back, you can help other people make
sure they get what they need then.
So I'm talking about canned food, stuff like that.
Go ahead and get some of it, put it back.
I'm a huge advocate of having a stash anyway, just because I live in Florida and hurricanes
and whatnot.
Doesn't matter where you live, there is something that can interrupt the supply chain in your
area.
So stuff is good for years, the cans are good for years, get some, set it back.
Get some paper towels, toilet paper, any of the stuff you think you might need, just lay
on a little bit extra so you have it, so you don't become one of those people that is out
there trying to get whatever is available.
You want to reduce that strain.
So if you can fill your needs now ahead of time, before the strain becomes apparent,
do it.
Worst case scenario, it takes you a while to get through your stuff and you didn't need
it.
Worst case scenario is you actually help and you make sure that there is one less person
out there trying to obtain stuff when it happens.
Again, I see this as something that is likely.
I'm not saying it's going to happen.
I don't want to trigger a panic.
But because I was kind of like, don't worry about it, I'm not going to say that anymore.
I do see that as something that is possible, something that I think is likely to happen,
that we're going to see some shortages come winter.
If you have the ability, take yourself out of that equation now.
Go ahead and get what you need now so you're not one of the people out there straining
the system then.
Just set it back and carry on as normal.
Do your normal shopping.
But this way, if there is a shortage, you have that little stash set back.
So your comfort food, stuff like that, anything that is shelf stable and will last, go ahead
and pick it up if you have the means.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}