---
title: Let's talk about NYPD and having always done it that way....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Xg8xaroPO4s) |
| Published | 2020/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An officer in the NYPD was suspended for displaying "Trump 2020" on their squad car, sparking controversy over political neutrality.
- The Sergeant's Benevolent Association (SBA) criticized the suspension, claiming it was unnecessary and that NYPD officers have a history of making political statements.
- Beau argues against the idea of maintaining the status quo just because it's always been done that way, especially in a politically charged environment like the current election.
- The SBA equated endorsing a political candidate with taking a knee with Floyd demonstrators, showing a lack of understanding between political matters and moral issues.
- Beau suggests that all in-custody deaths should be condemned, regardless of past practices or norms.
- He questions why Donald Trump is viewed as a racist, pointing out how his allies portray him and how it feeds into devaluing black lives.
- Beau contrasts the actions of deputies and sheriffs in the deep south who took a knee in solidarity, even though most are Trump supporters, to show that it's a matter of right vs. wrong, not politics.
- The SBA represents an institutional memory that hinders progress in law enforcement by resisting necessary reforms and improvements.
- Beau challenges the NYPD to lead reform efforts rather than being an obstacle to change and advancement within law enforcement.

### Quotes

- "Just because something has always been done that way is not a justification to continue doing it."
- "When you got sheriffs and deputies in the old south telling you to ease up on the black folk, you got a problem."
- "The SBA is part of the problem in NYPD."
- "It's the cops who don't want to get better."
- "Perhaps they should be leading the charge to advance and get better instead of holding it back."

### Oneliner

An officer's suspension for political display sparks debate, revealing resistance to change within NYPD and the need for moral over political stances.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Contact local law enforcement agencies to advocate for unbiased and apolitical conduct (suggested).
- Organize community dialogues on the distinction between political matters and moral issues (exemplified).

### Whats missing in summary

The full transcript provides additional insights on the importance of challenging institutional norms and advocating for ethical standards in law enforcement.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about doing stuff the way we've always done it.
We're going to talk about the NYPD.
We're going to talk about the Sergeant's Benevolent Association, and we're going
to talk about that giant mess up there.
If you don't know, an officer up there was suspended because they blasted Trump
2020 out of their squad car. I mean, yeah, that kind of seems
appropriate. You know, generally speaking, it is kind of expected
that law enforcement remain apolitical, especially when
using taxpayer vehicles. However, the sergeants benevolent
association up there had a different idea. They refer to
the suspension as unnecessary and way over the top? Go figure, they don't want
their officers held accountable. They also went on to say that NYPD has a
history of officers making political and outside of the norm statements and
ignoring it. Okay, let's assume that's true. That's wrong, that needs to change.
Just because something has always been done that way is not a justification to
continue doing it. This is a hotly contested election that's really well
known. Y'all got a memo about it recently. It would probably not do well for the
NYPD to become a partisan organization. They went on to take members of NYPD's
administration to task because they took a knee with demonstrators out there
about Floyd and that they obviously weren't remaining apolitical. So to be
clear, the SBA up there believes that endorsing a political candidate in a
taxpayer-funded vehicle somehow equates to taking a knee with the Floyd
demonstrators. I would suggest that, and I'm going to use their terminology, not
the terminology I would like to use, I would suggest an in-custody death is not
a political matter. It's a moral one. It's one of bad standards and bad training.
Maybe just because you've always done it that way. I would further suggest that
the SBA should endorse the idea that all in-custody deaths are bad, even if you've always done
it that way.
I would point out that these are the same folks that always wonder why, why is Donald
Trump seen as a racist?
Maybe it's because his allies constantly cast him as the opposition to a movement that
says black people have value.
There's really no other way to read that.
He's seen as a racist because his allies constantly paint him as a racist, probably
because a lot of them are racists.
I would point out that casting that as a political issue feeds into the idea that black people
aren't people, that they're just a hashtag, they're score, because you've always done
it that way.
I would further point out that I'm down here in the old south, in the deep
south, even down here, deputies and sheriffs took a knee because
it's not a political issue.
Almost all of them are Trump supporters, but they took a knee because it was
wrong and I'm going to take a line from Trey Crowder here, when you got
sheriffs and deputies in the old south telling you to ease up on the black
folk, you got a problem. The SBA is part of the problem in NYPD. They're part of
that institutional memory that influences officers and gets them to
continue things that aren't best practice just because they've always
been done that way. Upholds all of the problems that exist in modern law
enforcement because they're reluctant to change even when it results in something
that is in arguably a bad thing. The administration up there isn't the issue.
It's the cops who don't want to get better. The NYPD is very fond of saying
they are the best law enforcement agency in the world. They are the finest. Then
And perhaps they should be leading the charge to advance and get better instead of holding
it back and acting like an anchor on reforms that need to happen.
Anyway, if it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}