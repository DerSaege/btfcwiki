---
title: Let's talk about record-setting voter turnout and mandates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5jOOZrOqCLM) |
| Published | 2020/10/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Early voting has begun in some places with record voter turnout, potentially signaling a rejection of Trump and Trumpism.
- There is uncertainty whether the high turnout represents enthusiastic Trump voters or a rejection of him.
- Assuming the polls are accurate, this could be the beginning of a landslide against Trump.
- Elected representatives may interpret high numbers as approval, leading to a lack of motivation to act on issues beyond simply not being Trump.
- Beau suggests using social media to pressure politicians by tying their inaction to adopting Trump's policies if they fail to advance positive change.
- The focus should be on creating movement post-election, as just voting is a moment, not a movement.
- Politicians walking in with high numbers may not be responsive to constituents' needs as they may believe they have a mandate to act as they please.
- It is critical to push politicians from day one to address issues and not rest on the mandate of not being Trump.

### Quotes

- "The election is a moment, not a movement."
- "If you want change, you have to have movement."

### Oneliner

Early voting with record turnout may signal rejection of Trump, but post-election pressure needed for meaningful change.

### Audience

Voters, Activists, Citizens

### On-the-ground actions from transcript

- Hold elected representatives accountable for advancing policies beyond just not being Trump (implied).
- Use social media to pressure politicians by tying their inaction to adopting Trump's policies (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential implications of high voter turnout and the need for continued pressure on elected representatives beyond just rejecting Trump.

### Tags

#VoterTurnout #Politics #PressurePoliticians #ElectionStrategy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about mandates, voter turnout, and what that means.
If you don't know, early voting has started in a couple of places and records are being
set as far as voter turnout.
That's good, theoretically.
Assuming the polls are accurate, this is the beginning of that landslide that is rejecting
Trump and Trumpism.
We don't actually know that's what's happening though because there is the possibility that
these are just super enthusiastic Trump voters.
That is a possibility.
However for the purposes of this conversation, we are going to assume that the polls are
accurate and that we're going to have that landslide.
That's good.
That's good.
It's the beginning of the United States, the American people, firmly rejecting Trump and
Trumpism.
And that's going to lead to a whole bunch of new people up there.
And this is all good on one hand.
We're getting rid of that element.
The downside to this is that whether you truly support any particular candidate or not, at
the end of the day, you must admit they are politicians.
That's what they are.
You can't forget that.
And they are walking in with presumably good numbers.
They're going to take those poll results as approval, which is good.
They have a mandate.
The problem is what's the mandate?
To not be Trump.
That's all they have to be.
In their mind, is not Trump.
So they may not really be motivated to do much.
So how do we motivate these new elected representatives?
We use that mandate.
They're not Trump.
That's what they're saying.
That's the idea.
A lot of this is not policy-based.
It's about getting rid of Trump.
Cool.
So to advance any particular issue, because again, the election is a moment, not a movement,
doesn't actually change anything.
Nothing inherently changes if Trump is gone.
The issues, whether it be climate or jobs or criminal justice reform or whatever, it
does not matter.
If your elected representative refuses to act, refuses to advance a policy to make things
better, your terminology to move them along should probably be, they're adopting Trump's
policy.
Because to them, their mandate, those numbers, rest on the fact that they're not Trump.
So if they don't act at all, they're adopting Trump's policy.
If they're only willing to return things to the status quo, they just want to move it
to the policy that gave us Trump.
This is the terminology you can use on social media.
This is the way that we can motivate politicians who may not really feel any pressure.
Because if the voter turnout is indicative of what we think it is, and the polls are
right, they're going to be walking in with high numbers, which means they're not going
to feel the need to be responsive.
As much as right now we feel like they're on our team because they're getting rid of
Trump, they're not.
They're not.
They're going to want what every first-term politician wants, a second term.
And chipping away at that mandate of just simply not being Trump is the fastest way
to motivate them.
If they don't want to act on something, they are adopting Trump's policy.
If they only want to return it to the status quo and not actually improve on it, they want
the policy that gave us Trump.
Tie everything back, at least for the first couple of years, you'll be able to tie everything
back to Trump because that's what they're going to feel like gave them their success.
They're the anti-Trump candidate because, again, it doesn't matter if you like them
or not.
At the end of the day, they're politicians.
They're walking in with high numbers, according to everything we're seeing right now.
That high numbers does not normally lead politicians to be very active.
It doesn't lead them to be very responsive to their constituents because they feel they
have a mandate to do whatever they want.
So from day one, as soon as it starts, that's where we have to go.
We have to push them because the issues don't go away simply because there's new people
up there.
The election itself is just a moment.
If you want change, you have to have movement.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}