---
title: Let's talk about Trump, Pelosi, McConnell, and the stimulus....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6v8ogLQNqOc) |
| Published | 2020/10/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the current situation with Mitch McConnell, Nancy Pelosi, and Donald Trump regarding the stimulus package.
- Pelosi wants a large stimulus package, while McConnell prefers a piecemeal approach focusing on big companies.
- Trump initially called off negotiations but then changed his stance due to public backlash.
- Senate Republicans are trying to distance themselves from Trump as the elections approach.
- They are focusing on differentiating from Trump on the stimulus and Supreme Court nominations.
- McConnell is concerned about losing control of the Senate and is strategizing to appeal to voters.
- Senate Republicans aim to show they are not just blindly supporting Trump's decisions.
- The priority seems to be political posturing rather than genuinely helping Americans in need.

### Quotes

- "They're going to try to appeal to the American people by denying the American people the cash payments most need pretty badly."
- "Once again, when it comes to something that helps the average American, those at the top are just playing politics."

### Oneliner

Senate Republicans pivot to distance themselves from Trump on stimulus negotiations, prioritizing political moves over aiding Americans in need.

### Audience

Voters

### On-the-ground actions from transcript

- Call or email your representatives to express your opinion on the stimulus package negotiations (suggested).
- Stay informed on the developments surrounding the stimulus package and hold elected officials accountable for their decisions (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the political maneuvers surrounding the stimulus package negotiations and how they impact the average American.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Mitch McConnell, Nancy Pelosi, and Donald Trump.
And the stimulus.
Because wow.
Okay so if you haven't been following it, you know there's supposed to be another round
of stimulus.
If you haven't been following what's going on, they couldn't agree on a number.
The White House team, the Senate team, and the House team, they couldn't agree on how
much.
Pelosi wants a whole bunch.
So much that Trump tweeted out, that's it, talks are over, we're not going to talk about
it anymore.
And then almost immediately when he realized that that was really unpopular among the American
people, he switched and said that he was willing to continue negotiating.
McConnell is wanting to push a policy forward that is piecemeal.
Helping out, it appears like it's going to be helping out the big companies and leaving
direct payments to everybody off the table.
Trump is now saying go big or go home.
In other words, he's kind of caved to Pelosi.
That's what's happened.
Now it is at this moment that Senate Republicans have decided to grow a spine.
And it's not that they actually care about this issue in particular, it's that they have
weeks left to differentiate themselves from Trump.
They see the poll numbers too.
They know that the president's coattails are not very long right now.
So they have to show the American people in some way that they are different than Trump.
That they're not going to cosign everything even though they did it the entire time.
The only two things that are really capturing the media's attention right now and therefore
the attention of the American people, the Supreme Court nomination and the stimulus.
They're not going to deny a conservative judge on the Supreme Court.
So the only option they have is to try to differentiate themselves from the president
on the stimulus.
This is all political.
It's them trying to show that they're different.
That they aren't just a rubber stamp for the president, which they have been this entire
time.
And they've got weeks left to show that, well, we can be different because his poll numbers
are down.
It's what it boils down to.
McConnell is worried about losing control of the Senate.
It's a legitimate worry.
So this is their method.
This is how they're going to do it.
They're going to try to appeal to the American people by denying the American people the
cash payments most need pretty badly.
It's a bold strategy.
I think it's one that will go down in flames personally, but we'll have to wait and see
how it plays out.
At the end of the day, the important thing to remember here is that once again, when
it comes to something that helps the average American, those at the top are just playing
politics.
There's no debate over whether or not the large companies are going to get assistance.
Just whether working class Americans are.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}