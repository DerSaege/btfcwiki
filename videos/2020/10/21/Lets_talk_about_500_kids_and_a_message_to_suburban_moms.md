---
title: Let's talk about 500 kids and a message to suburban moms....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mNzVP8B01Wg) |
| Published | 2020/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 500 kids have been separated from their parents in 2017 and haven't been reunited since, some for more than half their lives.
- Finding the parents of these children seems unlikely as they've been bounced around the system for years.
- The children may now have addresses like row and plot numbers, making it harder to locate them.
- Beau questions if America has truly been made great again, considering the lack of respect on the international stage and the moral authority.
- The fear-mongering campaign may have created a nation afraid of everything, leading to actions like separating families.
- The forcible transfer of children by the United States violates treaties backed up by the Constitution.
- The President's actions have torn families apart through creating deterrents or displaying negligence towards public health issues.
- Each of the 500 stories of separated families should be heard and covered extensively to understand the consequences of apathy.
- Beau stresses the need to make a decision about the country's direction soon, as the lack of respect from other nations is evident.
- The President's actions have eroded the moral fabric of the nation, with the lack of media coverage on reuniting families showcasing a significant downfall.

### Quotes

- "What happened because of it."
- "We have a decision to make in this country."
- "The President has destroyed the moral fiber of this country."
- "We got work that needs to be done."
- "Y'all have a good day."

### Oneliner

500 kids separated from parents in 2017, still not reunited; America's moral fabric questioned under fear-mongering campaign, with urgent need for action.

### Audience

Every concerned citizen.

### On-the-ground actions from transcript

- Contact organizations working to reunite families (suggested)
- Spread awareness about the issue through social media and community networks (suggested)
- Join protests or advocacy campaigns demanding family reunification (suggested)

### Whats missing in summary

The emotional weight and urgency conveyed in Beau's delivery can best be experienced by watching the full transcript.


## Transcript
Well howdy there internet people, it's Bo again.
So uh...
500 kids, huh?
500 kids.
More than.
And we can't find their parents.
Separated
in 2017. Years.
Years.
They've been bounced around the system.
Some of them more than half their lives.
I'll be honest, I don't think we're going to find their parents.
Put yourself in their shoes.
In the parents' shoes.
You did something
in hopes of giving your kids a better life.
Only to have them taken.
Because they
crossed a line without a hall pass.
Never to be seen again.
What would you do?
Could you bear it?
I don't think we're going to find their addresses.
I would imagine some of their addresses are
row and plot numbers.
So uh...
my question
in regards to all this is whether or not we've made America great again.
If that's happened.
Are we respected
on the international stage?
Do we have the moral authority
that we once had?
That we thought we had?
Certainly these kids can't be blamed.
So did we make America great again?
Or did his campaign
of targeted fear-mongering
create a nation
that was so afraid of everything
that we wanted to hide behind a wall
because we were terrified
of toddlers?
So he could create a deterrent.
Take punitive action.
Which by the way
we're not allowed to do.
We have a treaty that's backed up by the Constitution
that says we can't do this.
It certainly appears
as though the United States has engaged in the forcible
transfer
of children.
Google that term.
And he's saying we have to make America great all over again.
I don't know that we can bear it.
I don't know that we can stand it.
This is just one
of the things that we've uncovered.
There's more.
There is more.
Make no mistake about it.
You know I've said I don't think the President's good very much.
He's really good
at ripping families apart.
Whether it be through
his desire
to create a deterrent
or through
apathy
and negligence
when it comes to the public health thing.
This uh...
this news story
is five hundred
news stories.
Should be.
Each one should be covered.
We should hear
what
happened to our apathy, our willingness to turn away.
What happened because of it.
And I think we need to hear in the next two weeks.
Because we have a decision to make
in this country
and I don't know
that people really get it.
We're not respected.
People aren't looking up to the United States again.
They're wondering how far down this road we're going to go.
Because historically
there's more to come.
It just gets worse.
This President
has destroyed
the moral fiber of this country.
The fact that this isn't around-the-clock news
as we try to reunite these families
shows how far we've fallen.
We got work that needs to be done.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}