---
title: Let's talk about Trump, executive power, the Heisman, and the Pulitzer....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v9wx6df5ZaM) |
| Published | 2020/10/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a 1950s football game involving Johnny Bright, a black player destined for the Heisman.
- Bright was intentionally hit in a racially motivated incident, impacting college athletics.
- The incident was captured in photos, becoming a symbol for the civil rights movement.
- The imagery energized the civil rights movement by making the mistreatment visible.
- The NCAA changed rules and implemented safety measures in response to the incident.
- Beau draws parallels between this historical event and America's current political situation.
- Trump's actions are likened to breaking America's jaw on film, forcing the country to confront its issues.
- Beau questions whether the country will learn from this incident or require further crises to prompt change.

### Quotes

- "He broke America's jaw."
- "Are we going to be as responsive as the NCAA?"
- "Does the country need their jaw broken again?"

### Oneliner

Beau recounts a historical football incident to urge America to learn from its current crisis and enact necessary changes.

### Audience

Americans

### On-the-ground actions from transcript

- Implement changes in response to current political issues (suggested)
- Advocate for safeguards to protect against abuses of power (implied)

### Whats missing in summary

The full transcript provides deeper insights into the historical context of racial tensions and how past events can inform present-day responses.

### Tags

#CivilRights #RacialJustice #PoliticalChange #CommunityAction #America


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what the entire country can learn from a football game
that took place in the 1950s.
And now that I say that aloud, just stick with me.
The game took place on October 20th, I think, 1951.
There was a guy playing, Johnny Bright.
Johnny was destined for the Heisman.
He was going to get it, he was favored to get it, but he didn't have it yet.
Had he got it, he would have been the first black guy to get the Heisman.
But on that day something happened.
He took a hit.
Not a normal hit you take playing football.
One that was intentionally malicious.
Now the guy who hit him claims that it wasn't racially motivated.
He says that he intentionally did it, but he did it because Bright's team had injured
someone on his team the year before and he was just trying to get even.
There is contemporary reporting from the period that suggests otherwise.
Bright for his part, he says, there's no way it couldn't have been racially motivated.
What I like about the whole deal now, and what I'm smug enough to say, is that getting
a broken jaw has somehow made college athletics better.
It made the NCAA take a hard look and clean up some things that were bad.
He went on to have a career.
I think the Eagles tried to pick him up, but he went to Canada to play, but he didn't get
the Heisman because he got injured and was out.
But somebody else got a Pulitzer.
Somebody took six photos, quick succession, showed what happened.
White player roughing up a black player on the field.
It became imagery for the civil rights movement.
But see here's the thing about it.
A black college athlete in the 1950s, of course they were being mistreated.
Of course they were being mistreated on the field.
This wasn't new.
Everybody knew it.
Everybody was aware that it was going on, at least on some level.
What changed things was that there was evidence.
There was photographic evidence.
It was out in the open.
There was nothing that could be said.
You couldn't turn a blind eye to it.
It happened and there it was.
Because of that, these images energized the civil rights movement at the time because
at that point in time, that was really what they were trying to do is just be seen, let
people know that bad things were happening and just put it in their face.
This helped do that.
Aside from that, it made the NCAA, as he said, change some rules.
Changed rules about blocking and require helmets to have masks.
They put up some guards to make sure that it didn't happen again.
So what does this have to do with the U.S., right?
I think that everybody in the country knows that the executive branch is taking too much
power.
Have been for a real long time.
They're aware of it on some level that the presidency is becoming too powerful.
Donald Trump just punched America in the jaw on film.
That's what happened.
Out in the open.
Couldn't deny it.
Shoved in your face.
There it is.
This is what's going on.
He broke America's jaw.
Doesn't have to be career ending, though, for the country.
The country can go on to have a career.
The country can survive this.
But more importantly, are we going to be as responsive as the NCAA?
Are we going to change some rules?
Put up some guards to make sure this doesn't happen again?
Put some things in place to protect ourselves?
Or do we need to get hit in the face again?
Does the country need their jaw broken again?
Or can we learn from it now that everybody's seen it?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}