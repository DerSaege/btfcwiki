---
title: Let's talk about Trump's rallies, criticism, and a piece of mail....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R1urToCymQ4) |
| Published | 2020/10/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- On Halloween, Beau addresses predictability, criticism, and President Trump's rallies.
- Following a tweet criticizing President Trump's rallies for causing harm, Beau received hate mail.
- The email criticized Beau for canceling events due to safety concerns while criticizing the President.
- Beau acknowledges that if he had held events like the President's rallies, similar outcomes could have occurred.
- He points out the predictability of harm when safety is disregarded for personal gain.
- Beau underscores the importance of caring for people's safety and lives in events.
- The email demonstrates a lack of concern from the President for citizens' well-being.
- Beau questions the President's fitness for office based on his apparent disregard for safety.
- He suggests that the President values his own interests, like photo ops, over supporters' well-being.
- Beau concludes by expressing concerns about the President's priorities and actions.

### Quotes

- "All it would have taken for this to happen to me is for me to completely disregard everybody's safety for my own personal gain."
- "I personally cannot think of anything scarier for Halloween than the realization that the President of the United States does not care about the citizens of the United States."
- "Either he's not really that bright or he does not care about the lives of his most ardent supporters."
- "He only cares about what he can get out of them."
- "Y'all have a good day."

### Oneliner

Beau addresses criticism of President Trump's rallies, revealing the predictability of harm when safety is disregarded, questioning the President's care for citizens, and spotlighting his priorities over supporters' well-being.

### Audience

Activists, Concerned Citizens

### On-the-ground actions from transcript

- Contact local officials to ensure event safety protocols are in place (suggested)
- Prioritize safety in all community events and gatherings (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of disregarding safety for personal gain and questions the ethics of leadership prioritizing personal interests over supporters' well-being. Viewing the full transcript offers a deeper understanding of these critical observations.

### Tags

#PresidentTrump #Predictability #Criticism #Halloween #SafetyConscious #CommunityPriorities


## Transcript
Well howdy there internet people, it's Beau again.
Happy Halloween or whatever holiday you may be celebrating, if any.
Since it is a special day, we're going to do something special.
I have a treat for everyone today.
Today we're going to talk about predictability, criticism, and President Trump's rallies.
Over the course of my career, I have received thousands of pieces of hate mail.
I normally don't respond.
I don't think I've ever made a video about one.
But this, this deserves to be read aloud because there's something important in it.
Little bit of back story here, recently I tweeted out about the President's rallies
and how there was hypothermia in Omaha and the people passing out in Tampa and the people
getting sick everywhere.
And I criticized that action.
The night I did that, I got this message.
I do not see how some failed journalist and third-rate idiot vlogger, that's me, can stand
there and criticize our President for the unfortunate injuries sustained at his rallies
when you yourself canceled yours out of fear.
Had you had your tour, the same thing would have happened to you.
Use your head.
It could have been you with the bad press.
Yeah.
Absolutely.
This is correct.
Had I had the tour, the same thing would have happened to me.
Yeah.
Because it's a completely predictable outcome.
All it would have taken for this to happen to me is for me to completely disregard everybody's
safety for my own personal gain.
That's it.
That's all it would have taken.
Go ahead with it because it's a completely predictable outcome.
You have events right now.
I don't have rallies, but if you have events indoors, high likelihood of somebody getting
sick.
You have them outdoors, people could succumb to inclement weather.
Yes.
It could have been me.
All I had to do was not care about people's safety, about their lives, of the people who
support me the most.
That's all it would take.
I, a third-rate vlogger, could see that coming.
The person who wrote this email, who did not apparently see the connection there, they
could see it coming because it would have happened to you.
It would have happened to me.
Not might.
Would have.
Because it's a completely predictable outcome.
I personally cannot think of anything scarier for Halloween than the realization that the
President of the United States does not care about the citizens of the United States.
And that's what this email shows.
It's a completely predictable outcome.
It would have happened.
Yeah.
I would suggest that if the President didn't know it was going to happen, he's unfit for
office.
I would suggest if he did know and went ahead with them, he's unfit for office.
Either he's not really that bright or he does not care about the lives of his most ardent
supporters.
He only cares about what he can get out of them.
In this case, a photo op.
I think that might be something to keep in mind right now.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}