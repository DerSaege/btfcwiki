---
title: Let's talk about civil dialogue, 3 steps, and the holidays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fH_j7hSyCug) |
| Published | 2020/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the importance of civil dialogues in response to queries on dealing with criticism and engaging in civil dialogues with individuals who may not seem receptive.
- Advocating for a three-step screening process to determine if a person is entitled to a civil discourse.
- Emphasizing that not all ideas are entitled to civil dialogues, especially when they cross a certain line of being harmful or atrocious.
- Sharing a personal example of engaging in a productive civil discourse on YouTube regarding reparations.
- Advising not to entertain bad faith arguments or individuals who aim to create trouble without good intentions.
- Encouraging individuals to critically analyze if engaging in civil dialogues is a productive use of time.
- Suggesting strategies for engaging in civil dialogues with friends or acquaintances holding different beliefs, while questioning the potential productivity of such engagements.
- Concluding by reiterating that nobody is owed dialogues, especially if the idea being discussed is atrocious or unproductive.

### Quotes

- "Not all ideas are owed civil dialogues."
- "Bad ideas aren't owed a platform."
- "Nobody is owed your time."

### Oneliner

Beau addresses the importance of civil dialogues, outlining a three-step screening process to determine if engaging in discourse is warranted, productive, or beneficial.

### Audience

Engage in civil dialogues.

### On-the-ground actions from transcript

- Analyze if engaging in civil dialogues is productive (suggested).
- Determine if the idea being discussed is harmful or atrocious (implied).

### Whats missing in summary

The full transcript provides detailed insights on the importance of critically assessing the entitlement to civil dialogues and the productivity of engaging in such discourse.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about civil dialogue.
Because I got two questions in the last week that are very different, but
they kind of have the same answer.
One was from somebody else on YouTube.
And it was basically, hey, you know, how do you deal with criticism and
engage people who are criticizing you in civil dialogue when they don't seem to
want it?
And the other was from somebody on Twitter.
Is it possible to maintain civil dialogue with a friend, Baptist minister,
who told me I would be judged for my vote for
my candidate because my candidate supports choice?
For me, there's kind of a three step screening process to this.
First, is the person entitled to dialogue?
Stop there first, okay?
Forget about the civil part.
Are they entitled to dialogue?
Did they come in good faith?
Or are they just there to criticize and stir up drama and
get clicks on YouTube or to assert their moral authority in real life?
Is it dialogue that's owed?
You know, if it's just some random person on YouTube you don't know and
doesn't impact you, they're not owed anything.
If it's your friend who happens to be a Baptist minister, well, maybe yeah, okay.
The next part is, are they owed civil dialogue?
Because that's something entirely different.
Not all ideas are owed civil dialogue.
I can sit down and talk about immigration policy,
something that is near and dear to my heart, all day civilly,
until one of the arguments becomes this group of people is somehow lesser.
Well, then they're not entitled to civil dialogue anymore.
There's a line where the idea becomes so
atrocious that it's probably harmful to entertain it civilly.
And to say, no, no, well, let's talk about this.
No, there's nothing to discuss at that point.
And then the third thing for me is, is it a productive use of time?
Okay, now on YouTube, sometimes it can be.
It can be.
I made a video about reparations once.
And a descendant of a slave made one critiquing it.
And it was basically like, hey, this is where you're wrong.
I wound up seeing it.
And sure enough, I looked at what he said.
And yeah, go figure,
the descendant of a slave had a better grasp on reparations than the white guy.
I mean, who could have seen that coming?
So I made a video back, and then he made one back to me.
This probably didn't change anybody's mind.
But at the very least, people got to see a descendant of a slave
talk to a guy who looks and sounds like me civilly,
and have the guy who looks and sounds like me actually listen.
That had value.
That was a productive use of time,
demonstrating that that conversation could occur.
So to me, it was worth doing.
And all of the arguments were in good faith and presented well.
Sometimes you'll get videos made that are really just there to stir up trouble,
to create drama in the hopes of drawing people to the other channel.
I mean, if you want to engage, you can, but you don't owe that person anything.
Somebody who comes in bad faith with logical fallacies, or
is just there to libel or slander or whatever,
you don't owe them anything.
They're not entitled to dialogue.
They're not entitled to your time.
They're certainly not entitled to civil dialogue.
Now, to the guy with the minister friend,
to me, this isn't in good faith, to be completely honest.
I'm fairly certain that this isn't doctrine.
But he's your friend or she's your friend, and you want to engage.
You want to have this conversation.
I mean, it's possible to have this civilly.
I think the best route would be to say,
okay, my guy is bad in your eyes because of this sin that he condones or whatever.
But if this is the standard and we get judged by voting for somebody who has sinned,
then Baptists can't vote because it is doctrine that everybody's a sinner
and that everybody will sin.
I don't think that this was made in good faith, to be completely honest.
But if it's your friend and you want to try, that's a route to go civilly with it.
But then you have to ask that other question.
Is it going to be productive?
Are you going to be able to sway the opinion of a Baptist minister?
Probably not.
I mean, I don't know the person, but I've known a few Baptist ministers in my day,
and that's probably not something you're going to be able to accomplish.
It's kind of like trying to reach the 80-year-old racist guy.
He's been here that long, probably not going to change.
And is it really worth your time?
Is it a productive use of time?
So when it comes to this question about civil dialogue, first, nobody's owed dialogue.
And if they have it, if the idea is atrocious, they're not entitled to civil dialogue.
And engaging in civil dialogue with ideas that are just horrible may actually be bad
because you're helping to legitimize them.
And then, is it a productive use of your time?
Nobody is owed your time.
If you want to be civil with somebody who is kind of on the border with some atrocious idea,
sure, but only because it's to your strategic advantage.
They're not owed that.
Bad ideas aren't owed a platform.
I mean, that's really it.
That's really the only advice I have.
I didn't do this simply because two came in so quickly together,
and it seemed like a topic maybe other people were trying to deal with,
especially coming up to the holidays where we're probably all going to be around people
who may hold different ideas.
I would run through that same thing.
Are they owed dialogue?
Are they owed civil dialogue?
Or is it a productive use of your time?
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}