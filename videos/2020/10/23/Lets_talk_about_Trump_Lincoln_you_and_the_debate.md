---
title: Let's talk about Trump, Lincoln, you, and the debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S6Gdd2lbhEk) |
| Published | 2020/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau gives his take on the recent debate involving President Trump and his campaign strategy, including his personal style and branding.
- Trump managed to behave himself initially during the debate, adhering to certain rules.
- Trump boasted about his contributions to black people, comparing himself to President Lincoln, despite historical facts proving otherwise.
- Beau criticizes Trump's reliance on sensationalized claims to appeal to his base rather than discussing his policies or record.
- Beau predicts another sensationalized claim from Trump against his opposition, aimed at swaying low-information voters emotionally.
- Beau urges individuals to be prepared to counter any false claims and strive for a significant rejection of Trumpism in the upcoming elections.
- He stresses the importance of individuals being ready to debunk misinformation, especially for those swayed by emotions and lacking in verification.
- Beau encourages viewers to play their part in ensuring a decisive rejection of Trump by a majority of Americans.
- He underlines the role of individuals in countering false claims and preventing the influence of emotional manipulation in the political sphere.

### Quotes

- "Because if it was true and something that could be verified, he would have released it months ago when it could knock Biden out of the race."
- "Be ready to counter it."
- "We want him to lose big."
- "You have to be ready to counter it."
- "Those people exist."

### Oneliner

Beau predicts Trump's reliance on sensationalized claims to sway low-information voters emotionally, urging individuals to be prepared to counter misinformation for a decisive rejection of Trumpism in the upcoming elections.

### Audience

Individuals

### On-the-ground actions from transcript

- Be ready to counter false claims when they arise (implied)

### Whats missing in summary

Beau's detailed analysis and insights on the debate, Trump's strategies, and the importance of countering misinformation can be best understood by watching the full transcript.

### Tags

#Politics #Debate #Trump #Misinformation #Elections


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the debate, President Trump and his personal style and
brand, how he's made it politically all this time, his campaign strategy, what we can expect
next, and Abraham Lincoln.
Okay, so if you don't know, the debate went okay in the beginning.
Trump managed to behave himself.
He stuck to some rules that we expect elementary school students to stick to, which is good.
It's always nice to see personal growth in someone.
And then he said that he had done more for black people than any president since Lincoln.
Sure, why not?
It wasn't the first president to invite a black man to come have dinner at the White
House.
It wasn't the president who desegregated the military.
It wasn't the president who helped end segregation and pushed to get the Civil Rights Act passed.
It was President Trump.
Because he said so.
Because he signed a relatively small amount of commutations and some executive orders,
most of which don't have any teeth.
But that's how he survives politically.
That's how he makes it politically.
As long as he makes these sensationalized claims, his base, they believe him.
So, okay, yeah, it was him.
Because they don't really think about it.
These are not people who want verification.
They're not people who delve into subjects.
They have emotional reactions.
And as long as he can continue to provoke those emotional reactions, he doesn't have
to talk about his policies, which he doesn't really have, or his record, which isn't really
great.
He can just focus on these claims, this branding effort that he has.
Because that is his style and it's been very consistent, we know what's coming next.
Just like we knew before it came out that there were going to be text messages in the
satire video, the emails.
We knew it was coming.
And we knew it was going to be exactly what it is.
There's probably, I would give it like 90% chance, going to be another sensationalized
claim about his opposition coming in the next few days.
And it's going to be false.
And people have asked, like, how do you know it's going to be false?
How can you be so sure?
Because if it was true and something that could be verified, he would have released
it months ago when it could knock Biden out of the race.
He's saving it to the end because he knows it will eventually be debunked.
So he wants that to occur after people have voted.
He's aiming for that same demographic of people.
Those people who are swayed by emotion, who don't look into things, who don't care to
verify anything.
Low information voters.
There are people who have broken away from Trump who could be swayed to move back by
a sensationalized claim.
We need to be ready to counter whatever this claim is.
You need to be ready.
You have friends on your social media who you know don't look into things, who are swayed
by their emotions.
When this news breaks, whatever it is, be ready to counter it.
Be ready to say, no, it's not true.
And the reason it's important is because we don't just want Trump to lose.
We want him to lose big.
We need that landslide.
We need a majority of Americans to reject Trumpism.
The country needs that.
And you can play your part in it.
So be ready for it.
Whatever it is, it should be good.
I can't wait, to be honest.
And be ready to counter it.
Because it's going to depend heavily on you as an individual.
The media, just like with the last thing, they'll debunk it quick enough.
But there's going to be those people that just get caught up in the moment, get swayed
by that emotion.
You have to be ready to counter it.
Because understand, there are people who believe Donald Trump has done more for black people
than any president since Lincoln.
Those people exist.
You probably have some on your social media.
They're out there.
And those are the type of people who can be swayed back to voting for him.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}