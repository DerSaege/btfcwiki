---
title: Let's talk about and to small government conservatives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=itCkHc6EdS8) |
| Published | 2020/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing small government conservatives who are uncertain about the upcoming election.
- Small government conservatives prioritize down-ballot races and struggle with the presidential choices.
- They can't fully support Trump due to his failures and authoritarian tendencies.
- Similarly, they can't back Biden as he represents liberal policies.
- The primary concern for small government conservatives is about money and limited government intervention.
- They seek candidates with strong business credentials and aim to run the country like a business.
- Economic issues, such as reducing national debt and corporate subsidies, are key priorities.
- Their stance on social issues is often tied to economic considerations rather than social concerns.
- Beau suggests Joe Jorgensen as a candidate who closely aligns with small government conservative values.
- Encourages them to vote based on principles and conscience rather than party loyalty.

### Quotes

- "You're going to walk into that voting booth and you're going to vote for Trump simply because he has an R next to his name and you don't want to do that."
- "You're going to vote for him simply because he has an R next to his name and that's gonna be the other response."
- "All it takes is informing yourself."

### Oneliner

Addressing small government conservatives torn between Trump and Biden, Beau advocates for voting based on principles and introduces Joe Jorgensen as a candidate who closely resonates with their values.

### Audience

Small government conservatives

### On-the-ground actions from transcript

- Inform yourself about alternative candidates like Joe Jorgensen (suggested).
- Vote based on principles and conscience rather than party loyalty (implied).

### Whats missing in summary

The full transcript includes detailed reasoning and insights for small government conservatives struggling with their presidential vote choice.

### Tags

#SmallGovernmentConservatives #PresidentialElection #Principles #JoeJorgensen #Voting


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we're going to talk about and to
small government conservatives.
And we're going to do this because you guys keep sending
me messages.
It's really what it boils down to at this point.
I just feel obligated.
You win.
The messages are all pretty much the same.
They really are.
You want to go vote for the down ballot races.
Those are the ones you care about.
And when it comes to president, you don't know what to do.
You're in a weird spot because neither one of them is your man.
You can't vote for Trump again because he's not small government.
He's a failure in every way.
And you're worried he's an authoritarian because he is.
Y'all are right about that.
You can't vote for Biden because not just is he not small government,
He's also a liberal.
Yeah, I get it.
I understand you guys pretty well.
It's like 60% of the population around me.
The thing is, y'all are all pretty much the same.
I mean, you really are.
Small government conservative is code for I want the
government out of my way so I can make some money.
For you guys, it really is about money.
At the end of the day, that's what it's about.
And y'all all want the same thing.
Y'all all have the same policy considerations
because you are at least honest in your pursuing self-interest.
And because of that, you want the same things in a candidate.
You want somebody that's incredibly well credentialed
because that matters to you guys.
You want somebody with a master's in business administration
and a PhD in industrial psych because you believe
the country should be run like a business,
Only this time, you want somebody who actually knows how to run a business.
Everything's about the economy.
You want to get rid of the national debt.
You want to get rid of corporate subsidies because it gives the bigger companies an unfair
advantage against small business, which is you.
You want to get rid of the barriers for small business.
Y'all are very self-interested, okay?
Again, I'm not saying any of this to be mean.
But even when it comes to social issues, it's really about money.
Yeah you're against mass incarceration, but it's not because you care about the people
getting locked up, it's because you know that locking up a bunch of non-violent offenders
is a giant waste of money.
You want a good immigration policy.
Not because you care about refugees, you don't.
You know that a good immigration policy is good for the economy.
You want to get rid of tariffs for the same reason.
You're very predictable and you're right.
You're absolutely right.
Neither Trump nor Biden is your man.
The thing is, your man is running.
Her name is Joe Jorgensen.
Go look at her platform.
Everything that small government conservatives say they want is her platform.
It's not my platform, but it's yours.
It really is.
I mean, down to the letter.
At the end of the day, if you don't go look at her platform, if you don't inform yourself
about the other options, what's going to happen?
You're going to walk into that voting booth and you're going to vote for Trump simply
because he has an R next to his name and you don't want to do that.
If you took the time to send a message to somebody asking about this particular issue,
it's because your principles are more important than your party.
That's why you were concerned because neither one of them reflect your principles.
You feel obligated to vote because that's part of your principles.
It's your civic duty.
The thing is, your duty is to vote your conscience.
Vote for somebody who reflects your principles.
And at the end of the day, it's her.
I know you guys.
I know what you want.
And yeah, she's a libertarian, but she'll be on the ballot.
No, she doesn't have a chance of winning.
Not really.
Not realistically.
However, after Trump, the Republican Party knows it has to change.
If she gets enough votes, that's the direction they'll shift.
Wouldn't it be nice if your party, the small government party, was actually, you know,
pursuing small government policies again?
Isn't that really what you want?
I mean, you're not opposed to a protest vote.
If you were, you wouldn't have sent the message.
You just won't do it for Biden.
And we both know why it's got in control.
That's your red line to hard.
No, you won't do it.
Go look at Jorgensen again, inform yourself.
You have other options if you are one of those Republicans who
just cannot cast that protest vote for Biden, and you're looking for somebody
who reflects your principles, you want to walk out of that voting booth with a
clean conscience, not supporting an authoritarian who is the exact opposite
of everything that you say you believe in, you need to go look at her platform.
I have a feeling that you're going to align more closely with that than
you could possibly imagine.
And I want to stress this again, it's not my platform, but for you guys, it's
perfect down to every detail.
The, I mean, and again, you know how bad Trump is, you know how bad he is, you
know, he's a failure, you know that you're going to have to have somebody in
that has the ability to clean up his mess. He sure can't do it and if you just
go in there without being informed you're gonna revert back to habit.
You're gonna vote for him simply because he has an R next to his name and that's
gonna be the other response. Well I don't want to throw away my vote on a third
party, as opposed to voting for somebody simply because of a letter. At least this
way you can help guide your party. You can walk out of that voting booth with a
clean conscience. All it takes is informing yourself. Anyway, it's just a
thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}