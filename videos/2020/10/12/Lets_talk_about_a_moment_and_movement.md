---
title: Let's talk about a moment and movement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9vAiG4LJ-Jk) |
| Published | 2020/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- This video marks the 1000th one on YouTube, but it's not necessarily an accomplishment.
- The channel has made tangible changes by supplying essentials to hospitals, supporting shelters, aiding in hurricane relief, and funding schools in the global south.
- Beau credits the viewers for their involvement and heavy lifting in achieving these accomplishments.
- Despite the significance of milestones like November 3rd, true change comes from continuous forward movement, not isolated moments.
- Beau stresses the importance of not stopping after specific dates but maintaining momentum to create real change in the world.
- He expresses gratitude towards viewers for their contributions in making real change happen globally.

### Quotes

- "Real change, real movement that you helped accomplish."
- "Change isn't really going to come from D.C. It's going to come from us."
- "We still have a lot to do, because there's a lot that's going to have to be cleaned up, regardless of the outcome."
- "Y'all did most of the heavy lifting on all that."
- "We can't just stop."

### Oneliner

Beau reminds us that real change comes from continuous forward movement and community involvement, not just isolated moments like milestones or elections.

### Audience

Viewers, Community Members

### On-the-ground actions from transcript

- Keep up the momentum for real change (implied)
- Continue creating tangible impact in the world (implied)
- Support community efforts for positive change (implied)

### Whats missing in summary

The full video provides a deeper dive into the channel's accomplishments and the importance of ongoing community-driven change efforts.

### Tags

#CommunityInvolvement #ContinuousChange #Gratitude #RealImpact #GlobalSupport


## Transcript
Well howdy there internet people, it's Beau again.
So this video is going to start off sounding like one of those milestone videos that we
often see here and it is in a way, but it's also not.
So stick with me.
This is our 1000th video on YouTube.
And I want to stop here real quick and just say thank you for hanging in there with me
all this time, especially if you were one of those who were around in the beginning
listening to me through one speaker.
I really appreciate it.
It's a cool little moment.
It's an accomplishment that we've done, right?
Not really.
Not really an accomplishment.
It's definitely a moment that we can pause and say, hey we did all this cool stuff.
But it's not an accomplishment.
This video in and of itself is no different than the ones that preceded it.
Doesn't affect any real change.
Nothing changes because of this video, because of this moment.
We can stop and acknowledge all of the cool stuff that we've done and the stuff that's
still to come.
But in and of itself, this is just another video.
I mean, this channel has done a lot.
There's been real movement with this channel.
Real things have been accomplished out there.
Tangible change.
This channel has supplied masks, Tyvek suits, respirators, stuff like that to hospitals
during this giant mess.
Stuff they couldn't get their hands on.
We supplied a DV shelter.
Got the kids their Christmas gifts.
Gave them the cleaning supplies they needed.
Hurricane relief.
Helped fund schools in the global south.
A lot of you have started your own channels, adding your voice to public discourse, helping
to shape what we're talking about.
All of this is great.
Those are accomplishments.
That's real movement.
That's something that can affect real change in the world.
Video's a moment.
And yeah, it's cool.
It's cool we can definitely acknowledge all of this cool stuff that we, and I do mean
we, did.
Y'all did most of the heavy lifting on all that.
But it's just a moment.
Doesn't change anything inherently.
November 3rd is a moment.
Doesn't matter who wins.
Even if it goes the way most people watching this channel want it to go, nothing inherently
changes.
All of the issues that we've identified, all the issues we've worked to address, they're
still going to be there.
It's the movement.
It's that forward movement, the change in the real world, that tangible change out there.
That's what matters.
That's what's going to propel us forward.
Not the individual moment.
We can't stop on November 3rd or at the end of January.
We have to keep up that momentum, keep up that forward movement, keep creating real
change, keep impacting things.
We can't just stop.
Take the moment, celebrate the moment, say, hey, we've got a little bit more breathing
room to achieve some real movement, to create some real change.
But change isn't really going to come from D.C.
It's going to come from us.
So we're 22 days out, I think.
Just keep that in mind as we move forward.
That doesn't end on November 3rd or at the end of January.
We still have a lot to do, because there's a lot that's going to have to be cleaned up,
regardless of the outcome.
One way, it's just a little bit easier.
Anyway, I do want to thank you guys again.
I mean, I actually didn't know until tonight that this was that video.
But I really do appreciate it.
I appreciate y'all being involved, because, again, y'all did all the heavy lifting on
that stuff.
Y'all made it possible.
I was just there to help facilitate.
And I really appreciate it.
And there are people literally all over the world that appreciate it.
Real change, real movement that you helped accomplish.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}