---
title: Let's talk about Trump saving the Faith....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ue18_iP1vdI) |
| Published | 2020/10/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Eric Trump's claim of his father saving Christianity, Beau questions the validity of this statement.
- Beau points out the lack of evidence for Christianity being in danger from legal or legislative battles.
- He criticizes the fear-mongering and manipulative tactics used to sway people's emotions.
- Beau questions if Trump exemplifies Christian teachings like loving thy neighbor and helping the needy.
- He warns about the danger of blending religion and the state, urging faith leaders to address this issue.
- Beau stresses that Christianity has survived various challenges and won't be destroyed by politicians.
- He cautions against blindly following leaders who don't embody the values of the faith.
- Beau calls for a rebuke of the notion that Christianity is under threat from politicians manipulating faith for their agenda.

### Quotes

- "There is a concerted effort by a group of people to use your faith to manipulate you."
- "Christianity is not in danger. It has survived a lot."
- "It's going to get stomped out because people believed politicians and were led astray."

### Oneliner

Beau questions the manipulation of faith for political gain and asserts Christianity's resilience against such tactics.

### Audience

Faith leaders and believers

### On-the-ground actions from transcript

- Address the issue of blending religion and state within your community (suggested)
- Encourage critical thinking and discernment regarding political manipulation of faith (implied)

### Whats missing in summary

Beau's engaging and thought-provoking delivery

### Tags

#Christianity #Politics #Manipulation #FaithLeaders #Resilience


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about something I do not talk about on this channel.
It's funny because somebody actually recently mentioned that I do not talk about this on
this channel.
But I feel obligated to.
Eric Trump was on a radio show in North Dakota last week and he said something that just
kind of raised my eyebrows and I feel like it needs to be addressed.
He was listing off things that he perceived to be accomplishments of his father, Donald
Trump.
And in that list of accomplishments he said that he literally saved Christianity.
Quote, quote.
I feel like I missed a news story or two.
I didn't know that that was in danger.
I went back and I looked, couldn't find any legal or legislative battle that was threatening
the faith.
Just not a thing.
Seemed really odd.
Now of course he's talking about saving it from the Democratic Party.
The Democratic Party that can't squash Bernie, they're going to ban, yeah.
They get gun control passed.
But they got the power to do this.
That doesn't seem real likely.
Seems almost imaginary.
Seems made up.
Seems like a fear-mongering manipulative tactic.
Seems like using deceit to sway people's emotions.
There is no legal or legislative battle over this.
That's not a thing.
It doesn't exist.
So if it's not that, and Trump really did save it, it had to be like saving the soul
of the faith, right?
There's only one way you can do that.
But aside from that, I'd want to know exactly what the Democratic Party did to make you
question your faith.
Because the only way that Christianity can actually be destroyed is if those who believe
in it turn away.
Because it's not buildings.
That's not what the faith is, right?
So I'm curious what Joe Biden did to make you doubt your faith.
I'm willing to bet that didn't happen either.
But let's say that he did.
Trump saved you from it.
I'm assuming the way he did that was by exemplifying the teachings, right?
By telling people to love their neighbor.
Telling people to be kind to the stranger in their land.
Heal the sick.
Feed the poor.
Does any of this sound like Donald Trump?
And if you are honest with yourself, you will have to say no.
And when you acknowledge that this is made up, you have to also acknowledge that there
is a concerted effort by a group of people to use your faith to manipulate you.
And man, if only there was a text that warned about that.
I don't think that the Democratic Party is quite this powerful.
I would suggest that the faith has survived forces far more powerful than Joe Biden.
This is just made up.
It's just an attempt to manipulate.
It's an attempt to blend religion and the state.
That's a concern, isn't it?
Wouldn't it bother you if somebody used your faith to justify their actions when their
actions are not in line with the faith?
It should.
It really should.
I would suggest that if you are a faith leader, this is something you should address.
Because if you don't, it's going to become more and more concrete.
It's going to become more and more tangible.
And your church will become subservient to the whims of a man.
A man who, to my knowledge, has never really exemplified anything, anything related to
the faith.
That seems like a really dangerous position to put your flock in.
This statement needs to be sharply rebuked.
Christianity is not in danger.
It has survived a lot.
It is not going to be stomped out by some politicians.
Doesn't even make any sense.
It's going to get stomped out because people believed politicians and were led astray.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}