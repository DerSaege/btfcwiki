---
title: Let's talk about the medical professionals, Trump, and the bird....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6dxBxsNxjnQ) |
| Published | 2020/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Scientific American endorsed Joe Biden, breaking from tradition.
- New England Journal of Medicine, typically apolitical, criticized current political leaders for their response to the public health crisis.
- The response by the nation's leaders has been consistently inadequate.
- The crisis has revealed a failure in leadership in the United States.
- The journal pointed out that the country's testing rates are lower than countries like Kazakhstan, Zimbabwe, and Ethiopia.
- Some governors handled the crisis well regardless of party affiliations.
- Many politicians politicized the use of masks.
- The journal did not endorse Biden but emphasized the need for Trump to lose the election.
- Despite Trump's endorsements from law enforcement, the real issues in the US are related to health and science, not crime.
- Few respected scientific or medical figures are endorsing the president, with many suggesting he needs to be replaced.
- The New England Journal of Medicine's statement was signed by 35 editors, indicating a serious criticism of the US's handling of the crisis.

### Quotes

- "We should not abet them and enable the deaths of thousands more Americans by allowing them to keep their jobs."
- "They have taken a crisis and turned it into a tragedy."
- "Rather than just continuing to ignore the people who may know what they're talking about, we might want to heed their advice."

### Oneliner

Scientific American and New England Journal of Medicine criticize US leaders for inadequate response to public health crisis, urging Trump's removal.

### Audience

Voters, Health Advocates

### On-the-ground actions from transcript

- Pay attention to statements from respected scientific and medical experts, and advocate for necessary changes (implied).
- Vote in upcoming elections based on candidates' responses to public health crises and trust scientific expertise (implied).

### Whats missing in summary

The full transcript provides detailed insights on the New England Journal of Medicine's unprecedented criticism of US political leaders' response to the public health crisis, urging for a change in leadership based on health and science priorities.

### Tags

#PublicHealth #USLeadership #Election2020 #ScientificExpertise #PoliticalEndorsements


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
something that's going to sound really familiar, but it's not. Last month we discussed how
Scientific American broke with, I don't know, almost 200 years of tradition and endorsed a
political candidate when they endorsed Joe Biden. Yesterday the New England Journal of Medicine,
which is a pretty prestigious medical journal, didn't endorse a candidate, but they had some
things to say, and this is a journal that is notoriously apolitical. When it comes to the
response to the largest public health crisis of our time, our current political leaders have
demonstrated that they are dangerously incompetent. We should not abet them and enable the deaths of
thousands more Americans by allowing them to keep their jobs.
The response of our nation's leaders has been consistently inadequate.
This crisis has produced a test of leadership. Here in the United States, our leaders have failed that test.
They have taken a crisis and turned it into a tragedy. It goes on, those are some select quotes.
It also talks about how our testing is below that of Kazakhstan, Zimbabwe, and Ethiopia.
It's pretty pointed. It is pretty pointed. It talks about how some governors did all right,
some didn't, and that that didn't have anything to do with party.
It also talks about how many politicians politicized the use of masks.
At the end of the day, this is, again, it's not an endorsement of Biden.
It's kind of just saying that Trump needs to lose, which is a sentiment I can get behind.
The Trump campaign likes to tout their endorsements from law enforcement. It's a big deal to them.
I want to point out that our actual problems here in the United States relate to health and science.
We don't actually have a giant crime wave sweeping the country. We do have a public health crisis.
We do have issues that require scientific expertise.
I don't see a lot of respected scientific or medical people endorsing the president.
In fact, I see the exact opposite. They're saying he needs to go.
Rather than just continuing to ignore the people who may know what they're talking about,
we might want to heed their advice. We might want to heed their advice.
In case I didn't mention it, the New England Journal of Medicine, they don't endorse candidates either.
But this situation is so bad that they felt they needed to speak up, and it was signed by 35 of their editors.
It's a pretty stunning rebuke of the handling of this issue in this country,
and it's something we should probably pay attention to.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}