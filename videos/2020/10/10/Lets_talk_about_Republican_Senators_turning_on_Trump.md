---
title: Let's talk about Republican Senators turning on Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=48PuG2SG7Ds) |
| Published | 2020/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are finally calling out the president and distancing themselves, mainly as election posturing.
- McConnell and Cornyn criticized the president's handling but didn't sound the alarm earlier.
- Senators are suddenly remembering their duty as they anticipate a significant defeat.
- The legislative branch should act as a real oversight, not a rubber stamp.
- Beau won't easily forgive those who enabled Trump, even in future elections.
- The American people need to see through the political tactics and hold a grudge against those who enabled Trump.
- The sudden distancing by Republicans seems insincere, especially as Biden leads in swing states.

### Quotes

- "People in the Senate aren't supposed to be yes men. They're supposed to advocate for their constituents."
- "We're going to have to de-Trumpify, go through de-Trumpification of anybody who enabled this."
- "Can't forget that. It's up to the American people to see through this distancing routine."

### Oneliner

Republicans posturing for election, American people urged to see through the distancing routine and hold accountable those who enabled Trump.

### Audience

American voters

### On-the-ground actions from transcript

- Hold your elected officials accountable for their actions (exemplified)
- Advocate for true representation in government (implied)
- Support opposition candidates against those who enabled Trump (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of Republican distancing from President Trump and the call for accountability from the American people.

### Tags

#Republicans #Accountability #ElectionPosturing #AmericanPeople #DeTrumpification


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Republicans finally starting to call the president out,
distance themselves, and why that's happening, and why it doesn't matter.
It shouldn't matter at all.
It's posturing obviously because of the election.
All of a sudden you've got McConnell saying that, well he hadn't been to the White House
in months because he didn't think that the president was taking the proper precautions.
You got Cornyn saying that, well the president was out over his skis, you know, he didn't
really know how to deal with it.
Yeah, that sounds good.
That definitely makes you sound smarter than Trump, like you're more capable than him.
I got a question Senator McConnell, if you knew for months that the president wasn't
up to the task, why weren't you sounding the alarm?
You stayed quiet as tens of thousands of holes got dug.
I don't think that the simple distancing routine is going to work this time.
That's what's going on, right?
Because as Senator Cruz said, y'all are looking at a defeat of Watergate proportions.
All of a sudden people remember their duty.
Yeah Senator Tillis in North Carolina, you had to keep me in the Senate so I can act
as a check against President Biden.
First already assuming that Trump's going to lose.
But see, there's that other part to this.
Why would anybody in North Carolina trust that he would do that?
I mean, he didn't act as a check against President Trump, which is his job.
The legislative branch is a co-equal branch of government.
It's not supposed to be a rubber stamp.
People in the Senate aren't supposed to be yes men.
They're supposed to advocate for their constituents, those people who sent them there.
They're supposed to represent them.
But that didn't happen because they put party over country.
They didn't sound the alarm.
They didn't serve as a check.
I don't know that just simple distancing is going to work this time.
It shouldn't.
It won't with me, I mean to be honest, if you were one of those people who enabled Trump,
even if you win re-election six years from now, I'm going to remember it.
I will be supporting whoever your opposition is.
We're going to have to de-Trumpify, go through de-Trumpification of anybody who enabled this.
Because there are 200,000 Americans who won't have a chance to express their disapproval.
Didn't go there for months.
Glad you kept yourself safe, Senator.
This election has a lot riding on it in a lot of different ways.
But I think one of the big tests, it doesn't really have anything to do with the candidates.
It has to do with the American people and whether the American people are going to see
through the normal political tactics.
In my opinion, they should hold a grudge the way these guys held Trump's coattails all
these years.
Enabled him.
Everything that went wrong, they allowed it to happen.
Can't forget that.
It's up to the American people to see through this distancing routine.
Oh, the president's suddenly unpopular.
We really don't even like the guy because now we realize that Biden's leading in every
swing state and has turned Georgia and Texas into possible swing states.
All of a sudden you care about what the people think, but you haven't ever since Trump was
elected.
I hope the American people remember that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}