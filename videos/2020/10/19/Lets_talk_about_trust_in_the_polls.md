---
title: Let's talk about trust in the polls....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jgLL8m3b3iA) |
| Published | 2020/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Addresses the topic of trusting polls and the consistent lead of Biden in polls.
- Mentions the doubts surrounding poll accuracy due to past incidents like Clinton's lead.
- Points out that pollsters previously undercounted uneducated voters, particularly white voters without a college degree.
- Expresses disagreement with the term "uneducated voters" and the correlation between education and susceptibility to fear-mongering.
- Talks about how pollsters have adjusted their methods by weighting uneducated voters more heavily.
- Notes the decrease in the percentage of whites without a college degree in the voting populace.
- Emphasizes that polls are not magical crystal balls and rely on people's actions matching their poll responses.
- Suggests that people need to act according to the poll results for them to be accurate.
- Concludes by advising to trust the polls due to statistical adjustments but also not to trust them entirely.

### Quotes

- "Education and credentialing are not the same thing."
- "Pollsters create their forecast based on what people say during the polls."
- "If people don't do what they say, then the poll, it's going to be wrong."
- "Yes, trust the polls because they fixed the statistical issue, but no, don't trust them because they don't actually mean anything."
- "It's really that simple."

### Oneliner

Beau explains the dynamics of trusting polls, addressing past inaccuracies and adjustments made by pollsters, advocating for both trust and skepticism in poll results based on people's actions matching their poll responses.

### Audience

Voters, Poll-watchers

### On-the-ground actions from transcript

- Follow through on reported poll demographics when voting (implied).

### Whats missing in summary

Beau's engaging and informative delivery style is missing in the summary.

### Tags

#Polls #Trust #Election #Voters #StatisticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the polls and whether or not you can trust the polls.
The polls have consistently shown Biden in the lead by, actually like a lot, the statistical
analysis of the polls shows that Biden has an incredibly high chance of winning.
There's even evidence to suggest that the landslide is pretty likely.
However, Democrats, y'all have heard this before, right?
You know, Clinton was in the lead and we all know what happened.
So there's a lot of doubt about whether or not anybody should trust these polls.
To get into this, we have to address what happened back then.
When I say this, it's not a joke.
I'm really not joking when I say this.
Pollsters undercounted uneducated voters.
That's what happened.
Their term, not mine.
They undercounted white voters who did not have a college degree.
They didn't weight them heavily enough.
They have fixed that, theoretically.
I want to stop here and point something out.
I think that's a really bad term.
You can not go to college and be educated.
You can go to college and not be educated.
Education and credentialing are not the same thing.
I want to put that out there.
But aside from that, I would like to just throw out my own little theory.
I don't think it has anything to do with education level.
I think that's just representative of something else.
If you go to college, if you go to a university, you are more likely to have traveled.
You are more likely to have broadened your horizons.
You are more likely to have been exposed to other kinds of people.
That makes you less susceptible to Trump's particular brand of fear-mongering.
That's what I think it is.
That's why I think that disparity exists, but the pollsters say otherwise.
Either way, they have fixed that problem, so they say.
They're weighting uneducated voters more heavily.
At the same time, it should be noted that whites without a college degree make up a
smaller percentage of the voting populace now than they did four years ago.
So yes, trust the polls, but also no, because polling isn't magic.
It's not a crystal ball.
Doesn't actually see into the future.
Pollsters create their forecast based on what people say during the polls.
If people don't do what they say, then the poll, it's going to be wrong.
The forecast will be wrong.
One of the things that I think may have played into 2016, in addition to negative voter turnout,
which is people showing up to vote against Clinton, I think there were a lot of people
who were like, Clinton's up in the polls, I'm going to eat a snack and take a nap.
She's going to win.
And she didn't.
If you want Biden to be up by seven points like he is in the polls, you have to do what
your demographic reported in the polls.
It's really that simple.
So yes, trust the polls because they fixed the statistical issue, but no, don't trust
them because they don't actually mean anything.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}