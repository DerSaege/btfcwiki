---
title: Let's talk about reaching your Trump supporting relative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wJUVMMgsOFc) |
| Published | 2020/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the common question he receives about how to reach loved ones who are loyal to Trump and seem unreachable despite efforts to change their minds.
- He points out that many individuals asking for help in reaching out to their pro-Trump family members or friends often express a sense of failure because they couldn't convince them otherwise.
- Beau shares a personal anecdote about a commenter who used to vehemently support Trump but, after watching a video collaboration Beau did with another channel, reached out to apologize and express a change of heart.
- He suggests that sometimes the messenger, not the message, is what matters in getting through to someone who holds strong political beliefs.
- Beau notes that trying to argue based on the principles someone held five years ago might not be effective, as Trump loyalists today are often driven by loyalty to him rather than traditional principles.
- He warns against reinforcing the idea of Trump as a savior by pointing out how much the person has changed, as it could backfire and strengthen their loyalty to him.
- Beau acknowledges that there may be no universal combination of arguments that work on every person, citing his own relatives with Trump signs in their yard.
- He recognizes that some individuals may be too deeply entrenched in the cult of personality around Trump to be swayed by logical arguments, suggesting that external factors beyond one's control might be needed to break that loyalty.

### Quotes

- "Sometimes it's not the arguments you're using. Sometimes it's not your, sometimes there is no combination for you."
- "You can't look at this as a failure on your part. You did everything you could."
- "There is no combination. There's no pattern that's going to work on every person. It doesn't exist."
- "Some men you just can't reach."
- "It may just be that you're not the right person to make that argument."

### Oneliner

Beau addresses the struggle of reaching out to Trump loyalists, stressing the importance of the messenger and acknowledging the challenges of changing deeply held beliefs.

### Audience

Those struggling to reach pro-Trump family members or friends.

### On-the-ground actions from transcript

- Have open, empathetic dialogues with pro-Trump individuals (suggested).
- Recognize when to step back and accept that some people may be beyond your influence (exemplified).

### Whats missing in summary

The full transcript provides nuanced insights into the challenges of engaging with Trump loyalists and the importance of recognizing personal limitations in changing deeply entrenched beliefs.


## Transcript
Well howdy there internet people, it's Beau again.
So today, I want to talk about the most frequently asked question I've gotten over the last month.
And I want to point something out about it.
The question is, you know, my cousin, brother, uncle, husband, father, grandfather, whoever,
madly in love with Trump.
I can't break him free.
I've tried everything.
I don't know what to do.
They're not the same person they were five years ago.
You know, help me reach this person.
And the thing that strikes me about almost every one of these messages or every one of
these comments is that the person sending or leaving it, it seems as though they feel
like they failed.
Like they couldn't figure out the combination, the pattern, the right series of arguments
to use to break this person that they care about free.
I want to point something out.
There was a person who commented on this channel all the time, like every day.
Hundreds of comments over hundreds of videos.
Every one of them telling me how wrong I was and just praising Trump at every turn.
A while back I did a like a joint production thing with the channel Re-Education.
Two weeks after that video went public, two weeks, that guy sent me a message saying that
he had started watching that channel and that he feels bad, wanted to apologize.
He realizes he was on a dark path and that he had seen the error of his ways and all
of that.
Complete redemption story.
This guy had been watching me for months, months, two weeks watching Re-Education completely
changed.
Sometimes it's not the arguments you're using.
Sometimes it's not your, sometimes there is no combination for you.
There was something about me, my delivery, my style, whatever that could not get through
to this person.
The other channel cut quickly.
It may not be that you use the wrong arguments.
It may not be that you don't know the pattern.
It may be that you're not the right messenger.
That it has to come from somebody that for whatever reason they relate to on a different
level.
It really might be that simple.
You can't look at this as a failure on your part.
You did everything you could.
It may just be that you're not the right person to make that argument.
I would also point out the person that you knew five years ago is gone.
You're probably, you may be, basing your arguments on the principles that they once had.
Those principles for Trump loyalists today, at this stage in the game, they're probably
gone.
That person that you knew doesn't exist anymore.
The principle today is loyalty to Trump.
At this stage in the game, it's a cult of personality.
They're going to try to defend him no matter what.
You could attempt to show them how much they've changed and how their principles, things they
really used to believe in, they've kind of abandoned.
But understand there's a risk with that because that could just reinforce the idea that dear
leader has shown them the light and led them to a better path.
There is no combination.
There's no pattern that's going to work on every person.
It doesn't exist.
If it did, I wouldn't have relatives that have Trump signs in their yard, and I do.
You also have to accept the fact, and it's a sad thing, but some men you just can't reach.
You're not going to be able to do it for some people.
If after this long they still have just that amazing level of faith, they're in that cult
of personality.
It's going to take something probably outside of your control to break them free of that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}