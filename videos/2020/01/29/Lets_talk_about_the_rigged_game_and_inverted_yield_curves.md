---
title: Let's talk about the rigged game and inverted yield curves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fupdWlCyib8) |
| Published | 2020/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of an inverted yield curve with the 10-year and three-month yields.
- An inverted yield curve suggests a shaky outlook on the economy and historically precedes recessions.
- Notes that capitalism is a rigged game, favoring certain players who start ahead.
- Wealthy individuals tend to protect their assets during recessions, exacerbating the economic downturn.
- People with fewer assets can find opportunities during recessions due to their flexibility.
- Gives examples of individuals who made successful moves during past recessions, such as buying property at low prices.
- Small businesses can thrive during recessions by understanding the need to be conservative and avoid overextension.
- Recommends making strategic moves during a recession when competition is weak, following the buy low, sell high principle.
- Encourages treating employees well and striving for a fairer economic system.
- Beau concludes by suggesting that now might be the time for certain individuals to make their move in the economic landscape.

### Quotes

- "Capitalism is a rigged game."
- "Freedom is just another word for nothing left to lose."
- "Buy low, sell high. It applies to everything."

### Oneliner

Beau explains the significance of an inverted yield curve, the rigged nature of capitalism, and how individuals can make strategic moves during recessions to seize opportunities.

### Audience

Entrepreneurs, small business owners

### On-the-ground actions from transcript

- Strategically plan and make moves during economic downturns to take advantage of opportunities (suggested)
- Treat employees well and work towards a fairer economic system (suggested)

### Whats missing in summary

The full transcript provides detailed examples and insights into navigating economic challenges and opportunities during recessions.

### Tags

#InvertedYieldCurve #EconomicOpportunities #Capitalism #RiggedGame #SmallBusinessOwners


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about the inverted yield curve
again.
We've talked about it before, so we're
just going to do a real simple explanation and then move on.
OK, an inverted yield curve, when people say that,
what they generally mean is they're
talking about the 10-year and the three-month.
When the yield for the 10-year falls
below the yield for the three-month,
That is an inverted yield curve.
And it makes sense.
If you're going to give money to somebody,
and you're going to let them hold it for a specified amount
of time, and they're going to give you that money plus
interest back, you would figure you
should get more for giving it to them for 10 years
than you would for giving it to them for three months.
It makes complete sense.
Sounds weird, but it makes sense.
What that means when that happens
that investors have a shaky outlook on the future of the economy. And when I say that it tends to
indicate we're headed into a recession, I mean that every single recession since World War II
has been preceded by this. And it just happened. Now, in that time since World War II, there have
been one or two false positives, but it just happened, and it happened in October.
That also matches with what we've seen recently, the last few recessions, the curve inverts,
it corrects, it inverts, it corrects, and then eventually the recession starts.
It's a warning sign.
So what does all that mean?
I've said before, I'm not an economist.
And this is certainly not investment advice.
This is just me telling you what I've noticed.
People say that capitalism is a rigged game.
And it is.
It is.
Certain players in the game start off a dozen, two dozen
spaces ahead.
And it certainly seems like if you're one of the ones that
start off ahead, you also get extra rolls of the dice to get
even further ahead.
There's also something else.
I tend to notice that it's those who started off ahead
in the game that know the rules to the game.
And that makes it even harder.
You know, we've all heard the phrase, buy low, sell high.
I mean, it's common sense, right?
But one of the things about financial anything
is that emotion comes into it
what wealthy people do
during a recession
they protect what they have
most times
they protect what they have they have stuff to lose
they have assets property
they have something to lose
so they become very conservative
with their investments
oddly enough that tends to make the recession worse
uh...
What about people on the lower end?
People living paycheck to paycheck, maybe got a little
bit squirreled away.
What do they do?
Their biggest fear is losing their job, getting laid off.
And it's a real fear.
It is, because it happens.
But one thing I have noticed over the years is that that
lack of assets. It gives them freedom, in a way, during a recession. You know, there's
that saying, freedom is just another word for nothing left to lose. When you're down
on the economic ladder during a recession, you can make moves others can't. Almost every
single successful person I know, somebody that made it big, who was once living paycheck
paycheck, made their move, whatever their move was, during a recession, buying low.
I know a single mom who bought her first house right after the housing market
crashed and she was able to buy a house she never would have been able to afford
a little bit before because property values dropped. Then when the property
values rebounded, she had tens of thousands of dollars of equity in that
home. It was worth more than she paid for it. So even though that money was just on
paper, it gave her the ability to make more moves. A lot of my friends started
businesses during the last recession and they did it because they understood and
This especially applies to businesses that are needed and are dominated by small businesses.
They understand that businesses tend to overextend.
So what happens is, you know, these businesses that have been around for a while, they brought
on more employees.
They have higher liability insurance.
They're paying more on workers' comp.
have more overhead and then the recession hits. They're doing a lot of
this on credit during a good economy but once that recession hits and one of their
clients can't afford it anymore, whatever their service is, the dominoes
start falling. Their credit dries up. They can't make payments. They end up laying
people off and then when they do that sure they let go some of the new people
but they're also really looking at the bottom line, so they let go some of the highest paid people
which tend to know the most about the business, which means the output of the business suffers,
which means their clients are unhappy, which means they may go looking for somebody else to do that service,
which ended up being my friends. That's how they made it.
it. And this doesn't apply for everything. If you want to sell designer outdoor gear a recession
is not the time to start your business. A travel agency, stuff like that. But stuff that's needed,
landscaping, maintenance, locksmithing, stuff that just has to happen. That's the time. That's the
So if you're one of those people who has that freedom and you've got that dream, this might
be the time to make your move.
When the recession hits, it might be the time to make your move because your competition
is going to be at its weakest.
And that's the part of capitalism that doesn't get explained, is that buy low, sell high.
Yeah, it applies to stocks, it applies to everything.
It applies to everything.
And if you can buy in to your business when it's low, when the economy rebounds, you've
got nowhere to go but up.
And I've seen it happen time and again.
Again, I'm not an economist, it's not investment advice,
but it's something I've noticed over and over and over again.
So if you are ready to get it while you can
and tell the other older companies to move over,
this might be the time to make your move.
Just remember, if you do it, it's a rigged game.
Treat your employees better than you were treated.
Try to make the game a little more fair.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}