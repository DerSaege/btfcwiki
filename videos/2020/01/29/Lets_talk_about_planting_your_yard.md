---
title: Let's talk about planting your yard....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EVV1WNGD8Lk) |
| Published | 2020/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocating for planting your yard, especially with spring approaching, to grow your own food and involve kids in the process.
- Emphasizing the importance of food security, self-reliance, and fostering healthier eating habits through growing fruits and vegetables.
- Mentioning that even with limited space, gardening can be done in containers like trash cans or small areas.
- Sharing personal experiences of giving out excess produce to neighbors to foster community spirit.
- Encouraging reconnecting with neighbors and building a strong community to combat division and political distractions.
- Stating the benefits of gardening go beyond just saving money on food.
- Stressing the simplicity of gardening by choosing plants suitable for the USDA growing zone and planting at the right time.
- Sharing the joy and satisfaction that comes from experimenting with different plants and enjoying the process of gardening.
- Suggesting replacing decorative shrubs with practical plants like raspberries for added security.
- Urging everyone to plant their yard as a beneficial and enjoyable activity.

### Quotes

- "Plant your yard."
- "Food doesn't always come out of a window in a brown paper bag with golden arches on it."
- "Those people in your neighborhood, they're your neighbors. Nothing more. They're not the enemy."
- "This is something everybody should do if they can."
- "Y'all have a good night."

### Oneliner

Beau advocates for planting your yard to grow food, foster community spirit, and combat division, urging everyone to reconnect with neighbors and cultivate self-reliance through gardening.

### Audience

Gardeners, community builders

### On-the-ground actions from transcript

- Plant fruits or vegetables in your yard or containers (suggested)
- Share excess produce with neighbors to foster community spirit (suggested)
- Connect with neighbors and build a strong community bond (suggested)

### Whats missing in summary

The full transcript provides detailed insights and personal anecdotes that enrich the importance and benefits of gardening for food security, community building, and self-reliance.

### Tags

#Gardening #CommunityBuilding #FoodSecurity #SelfReliance #NeighborlyRelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about
one of my favorite subjects, planting your yard.
And I just realized I'm doing this
right after that recession video.
There's no hidden meaning there.
I don't think anybody's gonna have to survive in this matter.
I don't think it's gonna get that bad.
It's just near springtime.
And this is something I like to advocate
remind people of early so they can do it so growing your own food in your yard
raspberries blackberries we've already got a bunch growing got these because my
kids enjoyed doing it we'll plant those tomorrow it'll take us we've got a
couple more, 10-15 minutes, and then they'll watch them grow. And it's an
experience for everybody involved, and it's beyond that, and it's beyond the
food security aspect of it. Yeah, you know, you've got food in your yard if things
go really bad, and you end up snacking on this instead of garbage. Your kids get to
see that you know food doesn't always come out of a window in a brown paper
bag with golden arches on it. And then they end up wanting to eat fruit and
vegetables because they grow it. Beyond even all of that there's the
added fostering of self-reliance in yourself. Because you see what you can do.
You know, it's the U.S. We're all the sons of the pioneers and it's a thing that we're
losing.
A lot of people say that they don't have the yard for it.
You probably do.
And some people have no yard.
A lot of this can be done in containers, I've grown blackberry in containers.
You can do potatoes in a trash can.
It's just a matter of wanting to do it and wanting that experience.
So you have the food security, you have the time spent with your kids, you have the eating
healthier, the exercise that goes along with it, the fostering of self-reliance, then you
end up with a fostering of community.
I've got a couple of lemon trees that are just mutants.
They produce more lemons than any five families could ever use.
So we give them out to the neighbors, and it fosters that community spirit.
These are neighbors that I don't talk to nearly as much as I should, but it's there.
There's that little exchange, and I think that's something we're missing.
And it's not nostalgia, nobody talks to their neighbors anymore.
It's beyond that.
It helps realize a lot of things.
turn on the TV, you read the news, whatever. The world's gonna end. We're at
our throats. We're on the verge of civil war. Red team, blue team, go. No we're not. Not
really. Those are people in power keeping us divided. Those people in your
neighborhood, they're your neighbors. Nothing more. They're not the enemy.
It helps foster that community spirit, and if that community spirit grows strong enough,
it doesn't matter what the idiots in DC tell us to do, because if your community is strong
enough, you can ignore it, it doesn't matter, it ceases to be.
There's a lot of reasons to do this, and the least of which is actually the 30, 40 bucks
you're going to save on food a year.
You end up liking it.
You know, it starts off, in my case, I'm going to plant a couple apple trees.
Next thing you know, you've got a dozen fruit trees, grapes, pineapples, all kinds of stuff,
on half of it just to see if you could and it's not hard. The real secret is
finding plants that are in your USDA growing zone. There's a chart. You get the
right plants and you plant them at the right time. That's really all there is to
it. A lot of times people make it harder than it is or people want to grow things
that don't necessarily grow where they live. There's always something that you
can do with this and the more you experiment with it, the more you enjoy it.
You know, and I know people that have pulled out those decorative shrubs by
their house and planted raspberries. You know, the purpose of those bushes to keep
people away from your windows. And I promise you, if you don't know, you've never gone
into a raspberry bush. Nobody wants to. And if they do, they will leave their DNA on those
thorns, I promise. I don't advocate a whole lot and say, this is something everybody should
do if they can. This is something everybody should do if they can. Plant your yard. Anyway,
just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}