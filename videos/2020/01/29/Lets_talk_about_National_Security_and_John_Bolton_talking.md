---
title: Let's talk about National Security and John Bolton talking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MfuXFI1lK3U) |
| Published | 2020/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes John Bolton's former chief of staff for criticizing Bolton's decision to write a tell-all during a presidential reelection campaign.
- Believes it is the perfect time for officials to share information with the American people during such critical moments.
- Emphasizes the importance of transparency in government and the right of the American people to know how decisions are made.
- Suggests that keeping secrets in national security is not about hiding information but protecting the means and methods through which information is obtained.
- Questions the actions of the Trump administration, citing examples like withholding military aid for political gain and compromising national security.
- Expresses skepticism towards claims of caring about national security within the Trump administration.
- Urges for transparency and the release of information that could potentially influence elections.
- Challenges the idea of using secrecy for political purposes, stating it goes against democratic principles.

### Quotes

- "secrecy, the very word secrecy, is repugnant in a free and open society."
- "John Bolton has information that the American people need to know."
- "Whatever it is, let it come out and let the chips fall where they may come election time."

### Oneliner

Beau challenges the need for secrecy in national security, advocating for transparency and the right of the American people to be informed, particularly during critical times like elections.

### Audience

Americans, Voters, Officials

### On-the-ground actions from transcript

- Advocate for transparency in government (suggested)
- Stay informed about national security issues and demand accountability from officials (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of national security, transparency in government, and the importance of informing the public during critical political moments. Viewing the entire speech offers a comprehensive understanding of Beau's viewpoints on these critical issues.

### Tags

#NationalSecurity #Transparency #GovernmentAccountability #ElectionInfluence #PoliticalTransparency


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about national security.
We're going to talk about national security
because John Bolton's former chief of staff,
who I'm not going to name, penned an op-ed where he criticized
John Bolton's decision to discuss this and write a tell-all.
I don't understand the need for a former national
Security Advisor to publish a tell-all book critical of a president he served,
especially during a presidential reelection campaign that will determine
the fate of the country. Well, let me start by saying I strongly disagree. That
seems like the perfect time to write it. Contrary to what is believed in DC,
you're not rulers, you're employees. Your bosses, the American people, certainly
have a right to know how you come to a decision, Chief of Staff. Now let's go to
the national security aspect of this. Yeah, keeping secrets is a big part of it.
A whole, whole lot of it relates to keeping secrets. However, it's not the
information that is kept secret for its own sake. If the US discovers the ins and
outs of an opposition country's aircraft, the details of that aircraft, well that's
kept secret. Why? Is it because we don't want people to know that we know? Well
that doesn't make sense. It's because we don't want people to know how we know
No, means and methods, that's what matters.
It would make more sense from a national security perspective as far as defense to disclose
every detail we know about that aircraft.
So all of our allies can then work on finding the weaknesses.
But we don't really do that, do we?
We keep it secret because however that information came to us is what's really important.
That's what we're protecting.
So with that in mind, understand everybody knows the means and methods.
The fact that the president seeks expert advice, to use your term, that's not a secret.
But I would also differ with that.
There is no defense expert that advised the president to withhold military aid from a
frontline ally in the middle of a war to get political dirt on a domestic rival.
That didn't happen. That did not occur. There is no defense expert that gave
that advice. Keeping something secret for political purposes, that's antithetical
to the idea of democracy. Now, let's go on to the idea that people in the Trump
administration are concerned about national security. I differ with that as
well. The entire record of the Trump administration suggests otherwise from
selling out the Kurds to diverting funding from DOD to pay for that vanity
wall that everybody says won't work, degrading military readiness, selling out indigenous
allies, selling out key allies in Europe.
None of this screams, I care about national security to me.
Doesn't even make sense.
The objection is hollow, almost like it's political.
Now I would like to point out that this quote, I don't understand the need for a former
national security advisor to publish a tell-all critical of the president he served during
an election because it might influence the election and all that.
This was written in an op-ed at Fox News.
I don't see the reason for a chief of staff to write a tell-all about the national security
advisor he served in the middle of an election campaign on a partisan outlet.
It's almost like that chief of staff is attempting to hand Fox News readers, I guess they exist,
talking points because the truth hurts.
secrecy, the very word secrecy, is repugnant in a free and open society.
John Bolton has information that the American people need to know.
Whatever it is, let it come out and let the chips fall where they may come election time.
The fact that this information is this concerning kind of shows that it was wrong, doesn't it?
That if it could influence the election that much, then maybe it should.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}