---
title: Let's talk about John Bolton rendering the impeachment moot....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kr_z_ve-Nlc) |
| Published | 2020/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the confusion surrounding John Bolton and how he may have made the impeachment irrelevant.
- Describes John Bolton as a true believer in American dominance and protecting the American way of life.
- Points out that some view John Bolton as a bad actor who has caused harm over the years.
- Talks about John Bolton's consistent actions and how he can rationalize anything as a true believer.
- Mentions that Trump's actions endangered U.S. national security during the impeachment.
- Recounts the plan in the 80s involving Eastern European countries as a buffer against Russia.
- Explains how withholding military aid while those countries were fighting could undermine their trust in the U.S.
- States that John Bolton is meticulous and everything in his book is verifiable.
- Suggests that Republican senators wouldn't want witnesses in the impeachment trial based on Bolton's testimony.
- Predicts that the release of Bolton's book before the election could spell trouble for Trump and the Republican senators.

### Quotes

- "True believers are the most dangerous people on the planet because they can rationalize anything."
- "John Bolton is nothing if he is not meticulous."
- "If this is the testimony you're going to get. Objection, Judge Roberts. This is devastating to my case."
- "Trump is done. He's gone."
- "Is one person worth destroying the entire party?"

### Oneliner

John Bolton's meticulous revelations in his book may render impeachment irrelevant, putting Republican senators in a tough spot about their loyalty to Trump and the party.

### Audience

Politically active citizens

### On-the-ground actions from transcript

- Spread awareness about John Bolton's revelations and their implications (implied)
- Encourage political engagement and reflection on party loyalty (implied)

### Whats missing in summary

The detailed analysis and insights provided by Beau may be missed in a brief summary.

### Tags

#JohnBolton #Impeachment #AmericanDominance #PoliticalStrategy #RepublicanSenators


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about John Bolton, the book,
the contradiction that is John Bolton in a lot of people's
minds right now because they just don't understand,
and how he may have rendered the impeachment irrelevant
in a lot of ways.
A lot of people right now are confused by John Bolton
Because if you don't believe the same things that he does,
his actions don't make any sense.
John Bolton, to a lot of people, is a very bad actor.
He's somebody who's done a lot of harm over the years.
See, John Bolton is a true believer.
My read on him.
He's a very consistent person in his actions.
He believes in American dominance, in American might, in protecting the American way of life.
And he truly believes that he's doing that.
He's a true believer and as we've talked about in other videos, true believers are the most
dangerous people on the planet because they can rationalize anything.
A lot of people are confused by his resume, which is colorful, to say the least.
Some of the things he's accused of pale in comparison to some minor unethical stuff in Ukraine.
I mean, when you compare the two, you're like, why would he not want to be involved in that?
It's because, by my read, he doesn't care about the fact that it was unethical.
unethical, he cares about the fact that it undermined U.S.
national security.
This is the part of the impeachment that I don't think is getting enough attention.
Trump's actions endangered U.S. national security.
When the Soviet Union started looking shaky, plans were drawn up.
This is in the 80s.
There were a lot of people who sat in on these meetings.
One of them would be John Bolton.
The idea was that when Warsaw Pact dissolved, that's their version of NATO, when the
Soviet alliance failed, the U.S. would pick up all those Eastern European countries, bring
them into the fold.
Why?
And then he makes sense.
If the Soviet threat is gone, why do we want these Eastern European countries on our side?
As a buffer.
As a buffer zone, that's really what they were there for.
The Soviet threat was gone, but that did not mean that Russia would not re-emerge as a
world power, of course they would.
They're a massive country.
They're going to be an extremely powerful opposing force, and if your interest and the
thing you truly believe in is American dominance, you want to be prepared for that.
So you pick up all of these Eastern European countries, basically to use them as a buffer
in case a conventional land war kicked off in Europe,
and that buffer would protect the countries
we actually care about, if you live in one of those countries.
Sorry, that was the plan.
Those countries would slow the Russians down long enough
for us to respond.
That's the idea.
So what would undermine that?
What would undermine the illusion that we actually care about these nations?
Withholding military aid while they're fighting would be at the top of the list.
Especially if it was over something minor, some domestic squabble in the US,
getting dirt on a political rival.
That, if you are a true believer in somebody whose sole mission in life
is guaranteeing American dominance, that would probably be pretty disheartening.
John Bolton is a very consistent person.
He could probably justify anything in pursuit of American dominance because to him that's
protecting you and I.
Not my take on it, but that's his.
So it should come as no surprise that he was willing to disclose this and will continue
disclosing this over and over again.
So how did this impact the impeachment?
John Bolton is nothing if he is not meticulous.
There's not going to be anything in that book that is not 100% verifiable.
There's just not.
My understanding is that there are dozens of pages devoted to this incident.
of pages.
So the Republican senators, they don't want witnesses.
Why would you?
If this is the testimony you're going to get.
Objection, Judge Roberts.
This is devastating to my case.
You wouldn't want these people talking.
It would just secure Trump's conviction.
So you want to vote against it and hopefully keep it quiet, but now news of the book is
out and that book will be published between now and the election.
If you're a Republican Senator, you have to realize that more than likely Trump is done.
Trump is done.
As this comes out, it's done.
He's gone.
So you have to ask whether or not you want to go with him.
against the witnesses and you're a Democratic opponent in the next election, we'll get
to point out that you either are an idiot or you sold out your country and put party
over your country.
Puts them in a hard spot.
If I was a Democratic strategist, I would make them take the vote, but not care how
it turns out. Because if they vote against it and they don't allow witnesses, odds are they're just
handing the Senate to the Democrats in 2020. If they vote to allow the witnesses, well, at least
they did their part. Yeah, they may have to vote to convict Trump. But, is Trump the Republican
party. You've put your party over principles, it appears. Is one person worth destroying the
entire party? That's the question they have to ask. This book will come out between now and the
election and it will be talked about constantly. And those Republicans who went to the wall went
to the mat did everything they could to fight for Trump are going to look horrible. This confirms
everything, doesn't it? It was all political. He withheld military aid from an ally to get dirt
on a domestic political rival, undermined U.S. national security, destroyed a decades-long
strategy because he was afraid he couldn't beat Biden. That doesn't inspire a lot of confidence
in and of itself. So you don't have to think John Bolton is a good guy. I know
right now there's a lot of people just really confused by that. He's not. I mean
by the standards that most people have who are watching this channel. No, he's
not. But he's internally consistent. Outside looking in, he's been consistent
since the 80s. This really shouldn't have been a surprise, but anyway, it's just a
thought. Y'all have a good night. I hope some of y'all caught the skeleton back
there because I spent way too long looking for that to give y'all a five
second YF. Anyway, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}