---
title: Let's talk about what a Native story can teach congress....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=izfACc5zwcQ) |
| Published | 2020/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a story he heard around a campfire about a young boy forced to move from his village in winter.
- Describes the harsh journey of the boy and his village being moved by soldiers, indifferent to their suffering.
- Depicts the heartbreaking scenes of mothers struggling to keep their infants warm during the forced relocation.
- Narrates how the boy, disillusioned by the elders' words, sneaks off with his friends, becoming part of the Cherokee remaining in the southeastern United States.
- Mentions the Cherokee Rose flower, symbolizing the tears of mothers and the gold that drove them out during the Trail of Tears.
- Links the spread of the Cherokee Rose as an invasive plant to the dark moments in American history.
- Criticizes Congress for laying the foundation of such dark events and the executive branch for exploiting them.
- Points out how Congress often prioritized personal gains over the well-being of the country.
- Emphasizes the recurring theme of American dark points stemming from the shortsightedness and self-interest of those in power.
- Suggests a lesson from the story for current Capitol Hill officials and acknowledges the grim reality behind the Trail of Tears' name.

### Quotes

- "It was a trail of tears and death."
- "Most of America's dark points are because of stuff like that."
- "Sometimes, they're just an innate part of American culture."

### Oneliner

Beau recounts a poignant tale of a young boy's forced relocation and draws parallels to the dark moments in American history due to government shortsightedness and self-interest.

### Audience

Legislators, policymakers, historians

### On-the-ground actions from transcript

- Learn about the history of forced relocations like the Trail of Tears and their impact (suggested)
- Support initiatives that aim to preserve and honor the stories and cultures of indigenous peoples (suggested)

### Whats missing in summary

The emotional depth and impact of hearing Beau's storytelling firsthand.

### Tags

#TrailOfTears #AmericanHistory #GovernmentAccountability #IndigenousRights #SelfInterest #Injustice


## Transcript
Well, howdy there, internet people, it's Bill again.
So tonight I'm gonna tell you a story.
It's a story that I heard years ago around a campfire.
And I've looked it up since then,
trying to find the source material on it.
And like many stories like this, you really can't.
In fact, I can't find any story told from this perspective.
I can find the story, but it's always told
from the viewpoint of the mother in the story.
In fact, I can't even find one told
mentioning a group of young people.
But that's the version I'm gonna tell you
because even though it's not a traditional telling
as far as I can tell, I like it better.
So there's this young boy.
He's 12, 13 years old.
and he's on a journey. He's leaving everything he knows headed to some place
he doesn't know and has no connection to. His whole village is going with him
because they're being forced to move. They have no choice in the matter. It's
winter. It is winter. There are ice flows in the river. The soldiers that are
moving them. They don't care that it's winter. They have winter clothing. They
don't care about the people that they're moving. And this young boy watches just
distraught as everything he knows is destroyed. And he watches as mothers try
in vain to keep their infants warm and he's sitting there watching as a group
of mothers who had lost their children cry and one of them gets up and goes to
talk to the elders and the elders tell her you know your tears those white
tears, they'll spring forth something that will keep us connected to this area.
And this young boy, he's just like, at this point he's jaded.
He has seen it.
He doesn't believe that a sign or a symbol is going to matter, doesn't really even believe
that there is a creator anymore.
He's at that point, just disillusioned with it all.
So he bucks the elders, him and a group of his friends sneak off that night.
They become the Cherokee that remain in the southeastern United States.
later and he's walking along that very same route and he sees these flowers and
they're white and have a gold center and it is obvious that the white petals are
the tears of the mothers and that that gold center is the gold that was the
reason they got driven out 60,000 people were moved during the Trail of Tears 4,000 died
195 years ago yesterday because I'm getting to this late Congress brought about the Indian
territory and that was the building block that's what let all this happen the thing
is the Cherokee Rose, that flower, it spread all over the southeast. It was an invasive plant.
And the dark moments in American history spread the same way.
More often than not, it was Congress who didn't see what was coming or didn't care.
They put the building blocks there, and then the executive branch, well, they used them.
Those in Congress were more concerned about lining their own pockets.
their party, whatever, more concerned about stuff like that
than right or wrong, or even the good of the country.
They focused instead on the immediate gains,
rather than being the statesmen we often pretend they are.
Most of America's dark points are because of stuff like that.
Sometimes, they're just an innate part of American culture.
They were there at the founding and they will be there until we have the courage to get rid of them.
But most times, it's because Congress couldn't see what was coming down the road.
There's probably a lesson in that story for those who are on Capitol Hill today.
Incidentally, the Trail of Tears gets its name from a quote.
And the quote wasn't a trail of tears.
It was a trail of tears and death.
But we like to leave that last part out.
it a little more palatable I guess. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}