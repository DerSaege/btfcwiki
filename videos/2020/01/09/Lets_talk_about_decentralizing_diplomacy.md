---
title: Let's talk about decentralizing diplomacy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pvOQhAQx3RY) |
| Published | 2020/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines diplomacy as managing international relations by a country's representatives abroad, but notes this definition is changing.
- Describes the transformation happening in the United States as the conservative movement is aging out and being replaced by younger, more liberal conservatives.
- Points out that traditional systems of diplomacy are being undermined by technology and a changing world.
- States that everyone is now a diplomat in the age of the internet, representing their individual nations online.
- Emphasizes the importance of understanding each other and finding common ground to prevent conflicts.
- Shares that people from 121 countries watched his channel in the last week, indicating a global audience.
- Stresses the power of individuals connecting across borders and realizing their commonalities.
- Expresses optimism about the decentralization of diplomacy due to increased access to information and communication.
- Encourages online users to be mindful of representing their nations positively in their social media interactions.
- Concludes by reflecting on the changing landscape of diplomacy in the digital age and wishing viewers a good night.

### Quotes

- "We're all diplomats now because we all have access."
- "Americans don't want war. Iranians don't want war."
- "If we can do that, we have successfully eliminated a power structure that the ruling elite held over us."
- "Diplomacy is being decentralized and it is amazing."
- "Y'all are watching trying to figure us out, good luck."

### Oneliner

Beau explains the changing face of diplomacy in the digital age, urging global understanding and representing nations positively online to decentralize power structures.

### Audience

Online users, Global citizens

### On-the-ground actions from transcript

- Connect with people from different countries online to foster mutual understanding and empathy (implied).
- Represent your nation positively in social media interactions to contribute to decentralized diplomacy (implied).

### Whats missing in summary

Beau's engaging delivery and nuanced insights on the evolving nature of diplomacy in the digital era can be best experienced through watching the full transcript.

### Tags

#Diplomacy #Decentralization #GlobalUnderstanding #DigitalAge #Representation


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about decentralizing diplomacy.
Talk about something positive for a change.
It's been a couple of days.
What is diplomacy?
Google will tell us that diplomacy
is the profession, activity, or skill
of managing international relations, typically
by country's representatives abroad.
It's a good definition, decent enough,
but it's a little dated and it's changing.
It is changing.
Most of us in the United States are aware,
painfully aware, that we are witnessing a transformation.
We are witnessing the last gasps, we're witnessing the end of the conservative
movement as it exists now.
The demographic that makes up the current conservative movement, especially
those that are in control of it, they are permanently aging out of voting.
They will cease to be.
Younger conservatives who are more liberal
than older conservatives will then take the reins.
So the new conservative movement will be more liberal
than the older one.
That's how it's happened throughout all of history.
See, that movement, it's reaching back.
It's reaching back for something that really never existed,
idea of it was there. They're reaching back for it because the world moved on and they
stayed the same. Technology changed and they didn't adapt. And because of that we are seeing
this upsurge, this very vocal gasp as they try to cling on, hang on.
We see it in the United States because it's up close.
The same thing is happening on the international stage because there are things that are undermining
the traditional systems.
world moved on and those systems those that we talked about this week and we're
like it shouldn't be this way but it's the way it is. The world moved on and
those systems didn't change. Technology advanced and those systems didn't adapt.
This definition inaccurate anymore. We're all diplomats now because we all have
access. We didn't used to, but we all represent our individual nations on the
internet. It doesn't seem like it matters, but it does. I loved seeing posts.
Americans don't want war. Iranians don't want war. I loved it because it
showed that commonality.
The analytics tell me that in the last seven days, people from 121 countries watched this
channel, all of the countries involved.
There were 60 hours spent watching this by people in Iraq.
Now some of that are my friends, people I know, but trust me, they don't like me enough
to watch me for 60 hours, just normal people there.
And that's a lot like those social media posts, may not seem to matter, but it does.
The more an Iranian in Shiraz watches a redneck in Florida and realizes that we have more
in common than he has with his leader in Tehran or I have with my leader in DC, makes it that
much harder to pit us against each other.
That big worry, you know, during this recent unpleasantness, was we were going to have
a third something.
A third one was coming.
We had two in the 1900s.
I didn't use the term on this channel, I'm not going to start now.
We had two in the 1900s.
People were worried that this would spin out of control and we'd have a third.
I didn't see it as likely, didn't see it as likely, because it's different now.
Back then, who knew about them, the others, the ones that they would pit us against?
The diplomats, those that traveled there, that's it.
And if they told the newspapers all the evil things they were going to do and how horrible
and different and backwards they were, the newspapers would tell us.
We hear it enough and all of a sudden it's praise the Lord and pass the ammunition.
Can't do that now because we all have access.
We all have access.
We can talk to each other and we can begin to understand each other and that is a key
part of diplomacy.
To understand where the other is coming from.
their position.
As we move into this new decade, it would probably be very wise for everybody to remember
that 60 something percent of you represent the United States when you are online, when
you are making your social media posts.
And all of the other people from all of these other countries that are watching, you represent
your nations as well.
Y'all are watching trying to figure us out, good luck.
If we can do that, we have successfully eliminated a power structure that the ruling elite held
over us. Diplomacy is the purview of the ruling class. It always has been, but it's not now.
Because we have access, diplomacy is being decentralized and it is amazing. Anyway, it's
It's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}