---
title: Let's talk about what we can learn from unwanted house guests....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MDDZbtvTB18) |
| Published | 2020/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unwanted house guests teach valuable lessons.
- Friends and neighbors often recognize bad house guests before you do.
- Good neighbors will be direct about warning you.
- Imagine having an unwanted house guest who refuses to leave.
- The neighbor warns that the house guest won't leave when the time comes.
- The neighbor tries to help by making it uncomfortable for the house guest.
- The house guest ends up physically confronting the neighbor.
- As a responsible homeowner, you need to address the situation.
- The house guest refuses to talk about leaving and demands obedience.
- The house guest sees talking as weakness and believes control is in their hands.
- Failing to address the issue of the unwanted guest can lead to more problems.
- The outcome depends on the actions of the people in the house.
- There's a growing belief among the house guest that talking about leaving is a mistake.
- Not discussing leaving could turn the household against the house guest.

### Quotes

- "Unwanted house guests teach valuable lessons."
- "Failing to talk about leaving is the latest mistake in a long string of mistakes."
- "Talking is weakness."

### Oneliner

Unwanted house guests teach lessons; failing to address them can lead to more problems and turn the household against them.

### Audience

Homeowners

### On-the-ground actions from transcript

- Address uncomfortable situations with unwanted guests (exemplified)
- Have direct and honest communication with problematic individuals (exemplified)

### Whats missing in summary

The importance of addressing uncomfortable situations promptly to prevent further issues.

### Tags

#UnwantedHouseGuests #HomeownerTips #ConflictResolution #Responsibility #Communication


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about unwanted house guests
and what they can teach us.
I think most people have dealt with an unwanted house guest
at some point in time in their life,
and if you haven't, you probably will.
Generally, your friends, your neighbors, people around you,
They know their bad houseguests before you do.
And a lot of times they'll tell you, you know, this person,
they've worn out their welcome in a lot of places.
You know this person has a pretty bad habit of holding up
their end of the deal.
You know this person has kind of left a lot of people
And if you have a neighbor like that, oftentimes, they'll tell you and they'll be real direct
about it if you've got a good neighbor.
So I'm going to give you a scenario.
Let's say you've got an unwanted house guest.
You've grown kind of accustomed to them, but you really don't want them there to begin
with.
And you've got a neighbor who's telling you, they're bad.
They've sold out their friends, they've sold out their house, they've sold out their
sold out their friends, they've done a lot of bad things over the last few years, and
when the time comes, they're not going to leave. Mark my words. And the neighbor sincerely
believes this. The neighbor may also say other things that aren't necessarily true. Eventually
your neighbor starts trying to help you. It's the way they see it. And they make it uncomfortable
for your house guest, then all of a sudden, your neighbor standing there talking to the
house guest, you've invited him over and your house guest lays him out.
You being a responsible homeowner, whoa, whoa, whoa, whoa, whoa, this can't go on here.
We don't want this in my house.
We need to sit down and talk about you leaving.
And your house guest tells you, no, we're not going to talk about me leaving.
In fact, if we're going to have any discussion at all, it's going to be about us moving forward
and becoming better friends and you doing what I say.
How do you view the assessment of your neighbor who got knocked out?
Pretty accurate now, right?
Certainly seems that way.
In fact, that incident is so dramatic that you may even believe the stuff that isn't
true now.
Because you got to see something.
You got to see that side.
No, we're not going to talk about it.
We're in control.
It's in my house now.
That scenario has played out.
It has played out.
Now, where we go from here?
I don't know.
It's entirely up to the other people in the house,
the people that live in that house,
and what they decide to do, and how they decide to react or respond,
and whether or not the neighbor's going to help them,
and the neighbor's friends.
There seems to be a growing consensus
in the mind of the house guest, that talking is weakness.
It's not, it's not.
Failing to talk about leaving
is the latest mistake in a long string of mistakes.
mistakes. And it may very well turn the whole household against us. I mean against the houseguests.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}