---
title: Let's talk about what two sayings can teach us about a plane....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=49NaJq2k8oE) |
| Published | 2020/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the saying "Never attribute to malice that which can adequately be explained by stupidity" in relation to a recent incident.
- Mentions the saying "Rangers lead the way" to illustrate how quick reactions can lead to mistakes in conflict situations.
- Points out that conflicts have far-reaching impacts beyond just the immediate situation.
- Emphasizes that blaming others for tragedies is not the solution as everyone plays a role in supporting violence.
- Recalls the downing of Flight 655 from Iran in 1988, showing how similar tragedies have occurred due to violent actions.
- Urges for a reduction in violent rhetoric and support to prevent future tragedies.
- Raises awareness about the constant cycle of tragedies caused by violence and the need for a change in mindset.
- Stresses the importance of recognizing the humanity of all individuals involved in conflicts to prevent further suffering.
- Calls for a shift away from violence and towards peace to avoid repeating past mistakes.
- Encourages reflection on the continuous tragedies caused by violence and the need for a collective change in mindset.

### Quotes

- "Civilians always suffer the most."
- "We have got to tone down the rhetoric."
- "Knowing who to blame doesn't matter."
- "In conflict, civilians always suffer the most."
- "It's something we really should take to heart."

### Oneliner

Beau explains how attributing malice to tragedies can overlook human error and stresses the need to reduce violent rhetoric to prevent civilian suffering.

### Audience

Global citizens

### On-the-ground actions from transcript

- Tone down violent rhetoric and support (implied)
- Recognize the humanity of all individuals involved in conflicts (implied)
- Take steps to prevent future tragedies caused by violence (implied)

### Whats missing in summary

The full transcript provides a deep dive into the interconnectedness of conflicts, civilian suffering, and the necessity of shifting away from violence towards peace.

### Tags

#Conflict #Violence #Humanity #Peace #Tragedy


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're gonna talk about two sayings,
and how those two sayings can help explain
something that everybody's speculating on right now,
everybody's pointing fingers over,
a recent incident, something that just happened,
and can help bring it into focus a little bit.
First, never attribute to malice
can adequately be explained away by stupidity. Handlen's razor, Heinlein's razor, there's
much variation of this. Saying has been around for a while, origins are unclear. The earliest
one that I am aware of is in a Jane West book in the early 1800s. So that's one. Just because
something appears to be malicious doesn't necessarily mean that it is. It could just be
incompetence in fact it most likely is the other saying definitely a more
American saying Rangers lead the way probably seen it on the back of pickup
trucks t-shirts Rangers are some of our special operations guys that's their
that's their saying it's their motto indicates that they are typically among
the first in any conflict. What do they do? What do they excel at? What are they just
amazing at doing? Taking over airports, airfield seizure. How do they get there? Big planes.
Something pops up on radar. Not really sure what it is. Tension's that high. A reaction
instead of a response, this is what you get. It is probably that simple. We don't have
all of the information yet, but that's more than likely what happened.
Conflicts. Conflicts do not exist in a vacuum. They're not isolated from everything else.
impact everything around them from the economy to food security to healthcare to incidents
like this, it's all interconnected, it's why we probably shouldn't support them for
any little thing and they shouldn't be undertaken lightly because stuff like this can happen.
People right now are trying to figure out who to blame. It doesn't matter, it
really doesn't. It might provide the family some closure, but at the end of the
day, knowing who to blame, the U.S., Iran, the UK, Operation Boot, depends on how
far on back you want to go. It doesn't matter. We all have our hands in this
because we all tend to support violence to solve any problem. This is the result.
In conflict, civilians always suffer the most. And I know somebody six foot two
red white and blue out there saying we'd never do that. July 3rd, 1988. Flight
655 out of Iran. Poof. Gone. 290. 260. I think it's 290. Gone in an instant. We did the
exact same thing. We did the exact same thing. We can learn from this. We can. We can learn
that our rhetoric and our support for these kind of actions leads to this kind of stuff.
But we most likely won't.
And it will continue.
The reason I know that this tragedy won't change anything, most likely, not anytime
soon anyway, is because it is a tragedy today, it was a tragedy yesterday, it was a tragedy
the day before, and the day before, and the day before, going back 19 years.
There is a slight difference.
This time some of the people involved, the collateral, and now all of a sudden that word
seems really harsh, right?
Because we can identify with these people.
They're not others, right?
The collateral for the last 19 years, they weren't others either.
They were people.
It is a tragedy, and it's something we really should take to heart, especially in the United
States where you have a bunch of people constantly saying, oh, well, it's time.
We need to do this.
The boogaloo.
You're ready, yeah, that's fine.
Your niece ready, your uncle, your brother, nephew.
Because odds are it's going to be them, not the person that's ready.
Civilians always suffer the most.
We have got to tone down the rhetoric.
We have got to tone down the support for violence.
Or we'll have another tragedy tomorrow.
Anyway it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}