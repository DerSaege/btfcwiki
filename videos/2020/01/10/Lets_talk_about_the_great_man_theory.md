---
title: Let's talk about the great man theory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8S-P1zA2H8M) |
| Published | 2020/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the "great man theory" and how it may influence recent decisions.
- Describes how the theory posits that certain individuals are born great leaders, destined to shape their own destiny.
- Compares the theory to the concept of divine right, where kings believed they were destined to rule based on traits and genetics.
- Raises questions about the theory's reliance on divine providence and a predetermined plan shaping governance.
- Suggests that the theory hinges on the great person shaping the world before being influenced by it.
- Points out how the current administration's interactions with world leaders and followers resonate with the great man theory.
- Argues that the theory can be shattered by understanding social sciences and acknowledging social pressures.
- Links the phenomenon of Trump's rise to a global social phenomenon driven by older demographics feeling unsettled by rapid changes.
- Notes the irony of those who believe in their own destiny being unaware of their participation in a broader social phenomenon.
- Advocates for being prepared for the end of this phenomenon to drive positive change towards interconnectedness and wider acceptance.

### Quotes

- "Some people are just born great, they're born to lead, and they're heroes of a sort when they're born."
- "The ultimate irony is that those who believe they're above it are not aware of the social phenomenon."
- "We need to be ready to jumpstart the drive for more interconnectedness, wider social norms that accept more and more people."

### Oneliner

Beau explains the "great man theory," linking it to Trump's rise as a global social phenomenon and advocating for readiness to drive positive change towards interconnectedness.

### Audience

Political analysts, activists

### On-the-ground actions from transcript

- Prepare for the end of the current phenomenon and work towards promoting interconnectedness and wider social acceptance (implied).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the "great man theory" and its implications on current political dynamics, urging proactive steps towards societal progress. 

### Tags

#GreatManTheory #Trump #PoliticalAnalysis #SocialPhenomenon #Interconnectedness


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about the great man theory.
We're going to talk about it because somebody brought it up,
suggesting that the Card administration may believe
this theory and that that may have influenced
recent decision. And to be honest, I don't believe that the administration has
ever even heard of this theory, but I also believe they believe it. At least
the president does. It explains a whole lot about his actions in dealing with
world leaders. The theory is that some people are just born great, they're born
to lead, and they're heroes of a sort when they're born they have all of the
traits necessary to rise to power and this means that they deserve to be in
power because they shaped their own destiny. They took it and they got there
on their own. They and they alone can make America great again. Of course the
administration believes that it's their entire campaign to be honest. It's very
comparable to the idea of divine right that the kings of old believed and that
they used as their authority to rule. It's very very similar it's just based
in pseudoscience. God didn't come down and make them king. Their traits, their
skills, their genetics, whatever gave them the ability to lead and to become
great. Philosophers will tell you this theory assumes two things. One is that
all of the traits were present at the time of their birth and the other is
that the world needed them to be great and the reason that alone presents its
own issues because in order to believe in this theory you have to believe in
the mystical you have to believe in divine providence you have to believe
that there is a plan of some sort that brings about different forms of
governance at different times. And all of these great people throughout history
they were there as part of some plan. They were there to bring something about.
And that was what the person was really trying to do was explain history.
And they wanted to boil it down to the decisions of just a few.
And that's what really mattered.
Yeah there were other influences, but it was these great people that truly shaped the world.
Fair enough.
going to add something else to what philosophers say and I feel that this
also has to be true for this theory to be true and that is that the great
person has to make the world whatever it is they're going to make it before the
world can shape them at all they have to be immune to social pressures if they're
not, then the whole theory goes out the window. Because they were
influenced by the world around them, they're not really great, they weren't
destined for anything, they were just pushed out to the front. That to me seems
a little more likely that some people are just what the times demand. Now we can
see this play out in a lot of recent decisions. In the way the administration
interacts with world leaders. We can see this idea that it's just a few people
that rule the world. We can see this in theories that surround the president.
That the great man theory is present in his followers. Even if they've never heard
it, they believe it. Even if they've never named it the great man theory, they believe
that there is some figure, a singular person who they can follow and that that person is
the hero and they will fix everything for them. It's there. It's there in the campaign.
It's there in their followers, it's there, it's very defining for this administration.
So, how do we completely shatter this theory?
It's actually really easy. Social sciences are in fact science, it's not just a name.
In order for this to be true, they can't succumb to social pressures.
They can't be influenced by that.
The problem is the entire phenomenon of Trump is a social phenomenon.
It's based on a social pressure.
We've talked about it on this channel before.
As the world has changed, older demographics didn't change with it, and they're reaching
back, they're seeing things that they believed would always be true, now they're not true.
In fact, things are going the other way.
The static status quo of the 50s to the 90s, that's gone.
And now the whole world is becoming more and more interconnected.
A lot of cultural norms are changing.
It has to be unsettling for them.
So they need a hero.
And that hero is Trump.
And that's easy for me to say, talking about the United States, that's just him saying
that, that doesn't really mean anything. The problem is that this phenomenon is global.
It's worldwide. If this was really just Trump's doing and he was the great man, we wouldn't
have other, I'll use that term, but populist right-wing figures springing up all over the
world. What's happening here, the social phenomenon that's happening here, it's happening
everywhere. The older demographics are watching the world change and they're
freaking out and they're reaching back. That's why there are so many many
trumps springing up. It's not a great man. They're riding out of social phenomena.
The ultimate irony of this is that the fact that they believe that they're above it,
the fact that they believe that it is truly destiny and fate, means that they're not
aware of the social phenomenon.
They believe they're making it themselves.
And in a way, that's the only way you can really succumb to the social phenomenon, which
is hilarious to me. If you understand that it's occurring, you're kind of
inoculated against it. That's why people who are older but were more versed in
politics and philosophy, they're not falling for it. They were inoculated.
They understand how this works and I would assume that if they had any
guidance for us they would be telling us to be ready because this phenomenon has
an expiration date and we need to be there and we need to be ready to go as
soon as it ends and jumpstart the drive for more interconnectedness, wider social
norms that accept more and more people.
More decentralization of power.
Creating a world where we don't need to create fictitious heroes to save the day.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}