---
title: Let's talk about 4-D chess....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S5s2YZ48aBo) |
| Published | 2020/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces himself, acknowledging the recent increase in subscribers and warns that despite his appearance, he is a strong supporter of the president.
- He talks about how the president operates in a way that may seem contradictory but is actually playing a game of 4D chess.
- Beau gives an example of how the president's statements on gun control were perceived by his base and how it ties into mental health care.
- He explains the difference between red flag gun laws and mental health care plans, suggesting that the president's base has been tricked into supporting something they initially opposed.
- Beau describes the president as a brand and a walking meme, whose base follows a cult of personality rather than his actual beliefs.
- He concludes by implying that the president will deliver on his promises through strategic maneuvers, including tax breaks and the construction of a border wall.

### Quotes

- "He's a brand. He's a walking meme."
- "His base is pretty much in direct opposition to his actual beliefs."
- "He just tricked everybody because he's smart like that, stable genius and all."
- "He will 4D his chest and everybody will get what they want."
- "Mexico's going to build the wall."

### Oneliner

Beau reveals the president's strategic 4D chess moves, manipulating his base through branding and misdirection, ultimately promising to fulfill his supporters' desires.

### Audience

Supporters of the president

### On-the-ground actions from transcript

- Question the motives and strategies of political figures (implied)

### Whats missing in summary

In-depth analysis of the psychological and strategic manipulation employed by political figures like the president.

### Tags

#Politics #Trump #4DChess #MentalHealth #Supporters


## Transcript
Well howdy there internet people, it's Bo again.
So I'm gonna start off tonight because we picked up like a pretty good number of subscribers
over the last week or so.
Some of y'all ain't never seen me like this.
And I just wanted to go ahead and give you a warning.
I mean, you gotta understand, yeah, last week's one thing, but deep down I'm just a normal
country boy.
like My Proof 100, My Chicken Fried,
and I am a huge, huge supporter of the president.
And so tonight, I'm gonna go ahead
and tell you a little bit about him
and explain something y'all just, y'all been misreading.
See, he says one thing, he does something else,
and y'all say he's flip-flopping or whatever.
That's not it, and I gotta break that down.
See, he's just, he's playing a different game.
So tonight we're going to talk about chess, but not any kind of chess, we're going to
talk about 4D chess, a special kind of chess that only he knows how to play, and his followers,
they know how to play it too.
So as an example, remember back in August, all those liberals just crying about gun control,
asking for gun control.
You know at some point he said something about how he was in favor of red flag gun laws
I mean his base went wild they went crazy like no you can't do that
And he's all like wink wink 4d chess, and they're like gotcha brother cuz they know how to play it, too
and
See his base this whole time they've been saying it was not gun control. That's not what we need we need mental health
care
And here we are, last 45 days, what's he been talking about? Mental health care.
See, 4D chess, he just tricked everybody because he's smart like that, stable
genius and all. See, now the reason his base is opposed to red flag gun laws is
that, see the way they work is somebody says you're mentally ill and dangerous
or whatever, and the law shows up, and they take your guns, and then in a short time you
get to see a judge, and the judge says whether or not you can get him back.
And that just doesn't seem like how you should deal with an inalienable right.
Seeing that his mental health care plan is a little bit different.
The way that works is it's about involuntarily housing people that are dangerous of mental
ill.
So, somebody says you're crazy, you're mentally ill, you're dangerous, and the law shows up,
and they take you.
And then, in a short time, you get to see a judge and determine whether or not...
So it's not a red flag gun law.
That's a red flag you law, guys.
Don't worry, wink wink, just going to be used on homeless people, right?
He's tricked his base, again, and they fell for it, again.
He just repackaged the plan.
I mean, I'm not familiar with the regulations at every asylum out there, but I'm fairly
certain they're not going to let these guys take their guns with them.
Call me crazy.
So he has his base now supporting a more extreme version of what they opposed because he rebranded
it.
And that's what Trump is.
He's a brand.
He's a walking meme.
a brand. That's it. That's it. And he's very good at branding things. The thing is, his
base is pretty much in direct opposition to his actual beliefs. So he has to trick them
constantly and they fall for it constantly because they're following a cult of personality.
They're following a person, rather than their principals, so they get tricked, and that
is what has happened again.
So anyway, I'm certain that if you continue to follow the president, he will 4D his chest
and everybody will get what they want.
All of his supporters will eventually get the tax breaks and everything that he promised.
Mexico's going to build the wall.
And all of the things that he's promised
are going to come true.
Wink, wink.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}