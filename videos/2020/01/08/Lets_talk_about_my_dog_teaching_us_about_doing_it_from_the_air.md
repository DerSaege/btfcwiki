---
title: Let's talk about my dog teaching us about "doing it from the air"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ayyi15PBN7o) |
| Published | 2020/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau uses the example of his dog, Baroness, to illustrate the concept of doing something from the air with ease and precision.
- He questions why the strategy of taking out command and control from the air hasn't been widely implemented despite having the capability for a long time.
- The transcript delves into historical instances, like George Bush Jr.'s presidency, where this strategy wasn't employed, raising the question of why.
- Beau mentions the deterrent capabilities of Iran and how low-tech and high-tech deterrents play a significant role in international relations.
- He brings up the concept of state sponsors and their influence in funding organizations with significant force projection capabilities.
- The mention of General Soleimani and his successor, Connie, sheds light on the continuity of leadership and influence in such organizations.
- The transcript concludes by reiterating the idea of taking out command and control as a strategic move, questioning why it hasn't been executed before.

### Quotes

- "She is a literal dog of war."
- "Why hadn't anybody done it? We've had the ability to do this for a very, very long time."
- "They fund these groups. What does money come with? Strings, right?"
- "What's a brilliant strategy, again? Take out command and control."
- "It's just a thought."

### Oneliner

Beau questions why the strategy of taking out command and control from the air hasn't been widely implemented despite its capability, using his dog as an analogy.

### Audience

Policy makers, strategists, activists

### On-the-ground actions from transcript

- Analyze the implications of using the strategy of taking out command and control in conflicts (suggested)
- Engage in informed debates and dialogues about the effectiveness and consequences of such military strategies (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of historical events, military strategies, and the concept of deterrence in international relations.

### Tags

#MilitaryStrategy #ConflictResolution #InternationalRelations #Deterrence #Leadership


## Transcript
Well howdy there internet people, it's Bo again. So tonight we're gonna talk about
what Baroness, my dog, can teach us about doing it from the air. That brilliant
strategy being proposed all over social media right now. We're just gonna do it
from the air. Be real easy, real clean, real clinical, no fuss, no muss. We're
gonna do it from the air. Baroness, she has free rein inside the fence. She'd
She'd never hurt a person without encouragement, but that does mean that occasionally a rabbit
doesn't make it.
That's unfortunate.
I don't like it.
But it's kind of the cost of having a dog like her.
She is a literal dog of war.
When she's on this, she's incredibly well trained.
If you were to come on this property in the middle of the night, you would meet her and
I.
And I'd be holding this.
And if I dropped it, you better hope you can run faster than a rabbit.
And there is a point to this, this brilliant strategy.
Why hadn't anybody done it?
We've had the ability to do this for a very, very long time.
Why hasn't anybody done it?
Do it from the air.
When you say that, say what you mean.
Take out command and control.
Don't church it up.
Take out their leadership, right?
Tensions with Iran for 40 years and nobody's thought of this.
George Bush Jr., always down to use military force, needed no provocation whatsoever, he
didn't think of this.
Maybe he did, but he was just surrounded by really good advisors.
Condoleezza Rice, Colin Powell, very smart people.
In fact, together all of his advisors, probably too smart for their own good.
People then understood that maybe this isn't such a brilliant strategy, that these things
don't occur in a vacuum.
You certainly cannot accuse George Bush of not having the stomach to order something
like this.
But he didn't.
Take it a step further.
Why didn't we do it to the Soviet Union?
Do it from the air.
Take out command and control.
Take out their leadership.
Why didn't we do it?
answer is because they could make us glow in the dark right and let me stop
right there say congratulations you've just made Iran's argument for why they
want what they want they had a deterrent and high-tech deterrent there are other
kinds of deterrents you're doing the pirates and emperors thing if you haven't
seen that video go back and watch it let's talk about pirates and emperors
Low-tech deterrents exist. Iran has one.
A big one. The administration knows this because they call them this all the time.
They're state sponsors, right? State sponsors.
I bet you recognize some of these names. They're state sponsors.
You know these organizations have huge,
huge force projection capabilities. They can reach out.
They've done it for decades.
Now, understand, these are just names of organizations that they fund that I thought people might
recognize.
The list is dozens long.
It's their deterrent.
They have this as a deterrent.
And these organizations, well, in their fence, in their area, they pretty much do what they
want.
and occasionally that means they catch a rabbit.
Iran doesn't like that, but it's kind of the cost
of having dogs of war.
They fund these groups.
What does money come with?
Strings, right?
A leash, if you will.
Up until recently that leash was held by a guy named General Soleimani.
Now, it's held, luckily for us, held by a guy named Connie,
who's basically a Soleimani clone.
And that's a good thing for us, to be honest.
But it does show that the strike
that caused all of this tension
was completely without worth.
Changed nothing.
But he's holding that leash now.
What's a brilliant strategy, again?
Take out command and control.
take out their leadership
there's a reason nobody's done this
it's just a thought
and y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}