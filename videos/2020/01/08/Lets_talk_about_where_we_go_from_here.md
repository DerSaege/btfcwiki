---
title: Let's talk about where we go from here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q33-EerGS-M) |
| Published | 2020/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A response, not a reaction, was calculated and deliberate.
- The United States faces limited and unfavorable paths forward.
- The lack of key officials like Secretary of the Navy and Director of National Intelligence hinders decision-making in a crisis.
- The absence of a clear plan and exit strategy from the President of the United States is concerning.
- The situation with Iran could escalate to a full-scale war if not de-escalated promptly.
- The option of a proportional response has its limitations and risks, including the potential for American soldiers being captured in Iran.
- Going nuclear or invading Iran are disastrous options that will damage the US globally.
- The US's influence is at stake, and a token response coupled with withdrawal from Iraq may be the only viable choice.
- American foreign policy has been mismanaged, costing lives and resources over the years.
- The conflict with Iran is not just about the US but involves other nations' troops caught in the middle.

### Quotes

- "They chose to start it in a conventional manner to prove a point."
- "The President of the United States has wasted more than a decade and a half of American foreign policy, a decade and a half of American lives."
- "It's not about branding and putting a veneer on things."
- "The United States could not pacify Iraq to the point where it could leave without other nation states involved."
- "Try to have a good night."

### Oneliner

The President's lack of strategy risks dragging the US into a prolonged conflict with Iran, with limited viable options and global repercussions.

### Audience

Concerned citizens, policymakers

### On-the-ground actions from transcript

- Contact elected officials to advocate for diplomatic solutions and de-escalation (implied)
- Support organizations working towards peacebuilding and conflict resolution (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current situation with Iran and the potential consequences of different courses of action. Watching the full video will give a comprehensive understanding of the speaker's perspective and recommendations.

### Tags

#USForeignPolicy #IranConflict #De-escalation #GlobalRepercussions #Diplomacy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So here we are.
They have responded.
And it is important to note that they responded.
They didn't react.
This was calculated.
They took their time.
They did it when they wanted to at a time of their choosing.
They're ready for a reaction, which is what they'll get.
So where do we go from here?
What are the paths forward for the United States?
There's not many and none of them are good.
Go ahead and get that out of the way.
If you're the president of the United States right now,
you'd want to have your closest advisors,
your best informed people at your side
to give you the information you don't have
and that you don't even know you don't have.
With the country that we're dealing with,
The straits of Hormuz are going to be really important as this progresses.
So you're going to want to go ahead and call in the Secretary of the Navy.
The problem is, you've got such an ego that you don't have one.
Well, you've got to call in the Director of National Intelligence.
You need him too.
You don't have one.
Homeland Security Secretary, you don't have one.
in a conflict scenario you want continuity of command. You hear it when
you're talking about another country and the news is babbling on about US war
plans we're gonna take out command and control. It's really important. We don't
have one. It's lacking, sorely. Nobody knows what's going on, nobody knows what
to do. Because the President of the United States, as stated before, doesn't
have a plan. Mr. I don't need an exit strategy. Well you do now. You do now. Well I guess
you don't. You'll be safe in DC. They do. The fact that they responded is really important.
That can't be overstated. They chose this time. They're ready for the reaction. They're
They're ready for what's to come.
This was them saying they are ready to go to war.
This wasn't a minor thing.
The fact that it was conventional forces on conventional forces is saying, bring it.
And right now on Twitter, on Facebook, there's all of these people, oh yeah, it's time.
Iran's in trouble.
We got to go.
It's go time.
Posting memes of the Avengers, yeah.
I've heard all this before when we were going to leave Iraq in a year, we're still there.
Heard it in Afghanistan, we're still there.
They are not goat people.
They are not stupid.
You're underestimating them.
Their doctrine is based on insurgency.
We can respond.
We can do a proportional response, but that's it.
That's the limit of what we can do in any logical sense.
Anything beyond that, if we step it up a notch at all, it's going to go into a full-scale
war and we will be on the ground in Iran.
Nobody wants that.
And here's the problem with the proportional response, the tip for tat back and forth.
Eventually what happens?
Pilot gets shot down.
Now you have an American soldier in Iran.
This needs to de-escalate now.
The President of the United States needs to understand that this isn't the business world.
It's not about branding.
This is real life and real death.
Our options are to go ahead and continue back and forth with the tit for tat.
We can do that.
Eventually, it will spiral out of control.
That is option one.
Option two is to literally go nuclear, which is horrible.
It's going to turn the entire world against us, and that's really it.
Our influence all over the world is over.
We can invade, go full scale and be there for a quarter of a century and still lose.
Or we can do a limited response to show that we can't be messed with a token response
and withdrawal from Iraq because it's over.
It is over.
The United States could not pacify Iraq to the point where it could leave without other
nation states involved.
Now you have them.
Iran's just the first.
There will be more.
The President of the United States has wasted more than a decade and a half of American
foreign policy, a decade and a half of American lives, and as we keep saying that, America
versus Iran, that's the cool Twitter hashtag, it's not, that's not accurate, it's just
those people over there, it's not all of us, and it's not just U.S. troops.
same installations, there were British troops there, there were Aussie troops, caught in
the middle of it.
They're suffering because we elected an idiot.
This isn't the business world.
It's not about branding and putting a veneer on things.
Here somebody will call your bluff and it's just happened.
options and the things that the President of the United States said he
was going to do, he can't realistically do, not without plunging the United
States into a war that will last 25 years and not have a positive outcome.
This isn't going to be a conventional fight. They chose to start it in a
conventional manner to prove a point because right now in their eyes they
have the moral authority and in most of the world's eyes they probably do. If this
was any nation other than Iran, they're not going to get the support that any
other nation would because it is Iran. Because there's 40 years of bad blood but
this is what Americans need to understand. They've been on our hit list
since 79 right? Why do you think we never acted? Because it's a bad idea. Now to the
troops, I know you guys, you're ready to go and I understand, I really do. Half a
league, half a league, half a league onward right? It's not yours to reason why. I get
it, don't use your position, don't use your uniform to support this any more than
you have to, and I know you're going to have to on some levels, and it has nothing
to do with you, because at this point, a kid who isn't born yet will be wearing that
uniform 20 years from now in the same place anyway it's just a thought y'all
try to have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}