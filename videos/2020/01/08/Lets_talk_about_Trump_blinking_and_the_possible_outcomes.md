---
title: Let's talk about Trump blinking and the possible outcomes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NAeTxVGPOkY) |
| Published | 2020/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President appears to be seeking total de-escalation after mentioning turning things over to NATO and stating that the Middle East may not be needed.
- Gina Haspel, also known as Bloody Gina, is likely the one who reached out to the president and made him understand the assessments.
- Beau addresses whether Trump weakened America, explaining that the deterrent was already known and that the public's lack of awareness weakened our self-image.
- Iran should be treated as a regional superpower, with diplomacy being the key approach rather than rhetoric about attacking.
- Beau expresses concern about the potential proliferation of sponsoring organizations due to the effectiveness of deterrence.
- Diplomacy and using available tools to alter the landscape are emphasized as necessary approaches, as military action could lead to disaster.
- Beau believes that Trump may have inadvertently helped America by avoiding escalation, although his unpredictability leaves room for uncertainty.

### Quotes

- "We're not invincible."
- "Hopefully, now that people are aware the deterrent exists, we can start treating Iran the way we should have the entire time."
- "It doesn't matter whether or not we like them. That's the way it is."
- "We're not gonna do it militarily, not without an utter disaster."
- "In 15 minutes he's gonna tweet we're going to war."

### Oneliner

President appears to seek de-escalation, Gina Haspel may have influenced, America's self-image weakened, diplomacy over military action emphasized, and concerns about potential proliferation raised by Beau.

### Audience

Political analysts

### On-the-ground actions from transcript

- Contact political representatives to advocate for diplomatic approaches to international conflicts (implied)
- Educate others on the importance of understanding global power dynamics and deterrence strategies (generated)

### Whats missing in summary

The full transcript provides detailed insights into the dynamics of international relations and the importance of diplomacy in mitigating conflicts.

### Tags

#InternationalRelations #Diplomacy #Iran #USForeignPolicy #De-escalation


## Transcript
Well, howdy there, internet people.
It's Bo again.
So as of this moment, it appears the president blinked.
And that's a good thing.
That's a good thing.
As of this moment, as of the conference, it appears he's
looking for total de-escalation.
He mentioned there were a lot of little key phrases in
there, turning things over to NATO, we don't really need the
Middle East, all of this stuff leads me to believe we're
headed towards a total de-escalation.
His token response was sanctions.
Good.
Fantastic.
Hopefully, he'll stick to this.
As we have seen this week, he doesn't always
stick to what he says.
People are asking if whether or not
I think somebody got to it, reached in there,
and finally was able to reach him and explain
what was going on.
It's probable, and I got a feeling I know who it was.
probably Gina Haspel. If you watched enough of these videos you know we don't
see eye-to-eye on a lot of things. However, I have no doubt that she
understands the threats and Gina Haspel's nickname is Bloody Gina. If
there's somebody who is fierce enough to get through and grab him by the ear and
make him look at the assessments, it's her.
Um, speaking of assessments, this week, a lot of people have pointed out the
accuracy of mine and I know it's YouTube, so I'm supposed to pretend like I'm just
some mythical genius.
The reality is tens of thousands of people can make those assessments, um,
with that same level of accuracy.
None of this is a surprise to people who this is their field of study.
And that leads us to the next question.
Did Trump weaken America?
it this week in the United States? No. No. It didn't. Everybody knew this. Everybody
in this field was aware of this. They knew of this deterrent. That's the whole point
of a deterrent. Everybody has to know it exists. That's why you had people like Soleimani.
No. No. We don't fund those groups. What, do you think I'm a madman? Am I? And then
publicly transfer money to them. It's a deterrent. You have to know it exists. And everybody
dead. What the American public doesn't know is what makes them the American public. The
American public weren't really aware of this because it's never framed as a deterrent.
It's framed as just evil. And yeah, okay, it is on some level, sure, but it is a deterrent.
That's what it's there for.
And now, hopefully, the American people understand that it exists.
In my mind, that can only be a good thing.
He didn't weaken America.
He weakened our self-image.
We're not invincible.
Yes, we have the greatest war machine the world's ever known.
But all machines have weaknesses, and Iran has their finger on a button that goes to
one of those weaknesses.
But hopefully, now that people are aware the deterrent exists, we can start treating Iran
the way we should have the entire time.
They're a regional superpower.
They are.
It doesn't matter whether or not we like them.
That's the way it is.
This should have always been a cold war with an attempt at diplomacy at all cost.
Rhetoric about attacking should never come onto the table because they have that deterrent
and we're not going to do it and they know we're not going to do it.
The only downside to this is that maybe other countries
realize how effective it is
and then we have
proliferation
of sponsoring these organizations.
I was worried during the Cold War with that deterrent.
Yeah, it's a worry
but we bring them to the table.
We go back to diplomacy.
We go back to what the last president was trying to do.
Did he still have shortcomings? Sure.
Whatever. But it was a first step
and it was working. And now we have to go back to that. Because
John Wayne attitudes aside
they have the deterrent. Hopefully we can kill some of the rhetoric.
We can use the tools that are available
to alter the landscape, to bring them out.
That's what has to happen.
We're not gonna do it militarily,
not without an utter disaster.
So no, I don't think he weakened America.
I think he may have accidentally helped it
through no planning of his own.
But again, it's Trump.
So for all we know, in 15 minutes he's gonna tweet
were going to war. I would hope that Haspel put just the fear of God in them and explained
that escalating this to the point where they use their deterrent guarantees he doesn't
get reelected because it's going to be a disaster. And I'm hoping that's what went down. Is
Is it?
We don't know.
Because we have a tweeter-in-chief.
Anyway, well, thanks for hanging in there on the ride with me this week.
I'm going to go get some sleep.
It's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}