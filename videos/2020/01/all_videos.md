# All videos from January, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-01-31: Let's talk about Ex-Marines and the impeachment.... (<a href="https://youtube.com/watch?v=q7exZB1GJh8">watch</a> || <a href="/videos/2020/01/31/Lets_talk_about_Ex-Marines_and_the_impeachment">transcript &amp; editable summary</a>)

Beau examines the lasting impact of being a Marine, the significance of upholding oaths, and questions political integrity and representation in the U.S. Senate.

</summary>

"Once a Marine, always a Marine."
"Nobody can look at this and say that it was impartial."
"What makes you think they won't lie to you about the economy?"
"Has the United States, the people of the United States, has their honor and integrity fallen this far that they tolerate it?"
"They don't care about you; these people aren't your representatives."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of "Once a Marine, always a Marine" and why it holds true.
Touches on the significance of oaths taken by individuals in government or non-government entities.
Mentions Edward Snowden and John Karyaku as examples of individuals who upheld their oath by blowing the whistle.
Talks about the serious nature of taking oaths among people with honor and integrity.
Draws parallels between the Marine Corps' iconic knife, the K-Bar, and the symbolism behind it.
Questions the actions of a large group of people in the U.S. Senate who seemingly violated their oath to remain impartial.
Expresses concerns about politicians prioritizing political advantage over truth, justice, and the Constitution.
Raises doubts about whether politicians truly represent the interests of the people they serve.
Criticizes politicians for potentially lying to the public about various issues, including war, social security, and the economy.
Challenges the American people to question the integrity of their representatives and their tolerance for political misconduct.

Actions:

for active citizens,
Contact your representatives and demand accountability for upholding their oaths (implied).
</details>
<details>
<summary>
2020-01-30: Let's talk about what Y2K can teach us about climate change.... (<a href="https://youtube.com/watch?v=9elHSOMvn_M">watch</a> || <a href="/videos/2020/01/30/Lets_talk_about_what_Y2K_can_teach_us_about_climate_change">transcript &amp; editable summary</a>)

Y2K crisis parallels climate change urgency, urging action before it's too late and history repeats costly mistakes.

</summary>

"Dealing with climate change is going to be expensive. Fact."
"We spent almost half a trillion dollars to deal with a date so it wouldn't become an issue."
"Our failures are known and our successes are not."
"It's not a hoax. It's real."
"The alternative is that people know we failed."

### AI summary (High error rate! Edit errors on video page)

Y2K was a computer crisis 20 years ago due to shortened year dates causing potential issues in 2000.
The fear of Y2K was real as computers could have thought it was 1900 instead of 2000, impacting critical infrastructure.
Despite spending almost half a trillion dollars on fixing the Y2K problem, some issues persist due to short-term solutions.
Beau draws parallels between the Y2K crisis and climate change, where temporary fixes don't address the root problem.
He warns that time is running out for climate change action, with substantial impacts already visible.
Beau points out the trust placed in politicians over scientists, urging a reconsideration of this choice for the future.
Dealing with climate change will be costly, but history shows significant investments can prevent major issues.
The hope is for future generations to view the handling of climate change as an overblown concern, similar to Y2K.

Actions:

for climate activists, policymakers,
Invest in sustainable solutions for climate change (suggested)
Advocate for scientific truth in policymaking (suggested)
Support initiatives addressing climate change impacts (suggested)
</details>
<details>
<summary>
2020-01-29: Let's talk about the rigged game and inverted yield curves.... (<a href="https://youtube.com/watch?v=fupdWlCyib8">watch</a> || <a href="/videos/2020/01/29/Lets_talk_about_the_rigged_game_and_inverted_yield_curves">transcript &amp; editable summary</a>)

Beau explains the significance of an inverted yield curve, the rigged nature of capitalism, and how individuals can make strategic moves during recessions to seize opportunities.

</summary>

"Capitalism is a rigged game."
"Freedom is just another word for nothing left to lose."
"Buy low, sell high. It applies to everything."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of an inverted yield curve with the 10-year and three-month yields.
An inverted yield curve suggests a shaky outlook on the economy and historically precedes recessions.
Notes that capitalism is a rigged game, favoring certain players who start ahead.
Wealthy individuals tend to protect their assets during recessions, exacerbating the economic downturn.
People with fewer assets can find opportunities during recessions due to their flexibility.
Gives examples of individuals who made successful moves during past recessions, such as buying property at low prices.
Small businesses can thrive during recessions by understanding the need to be conservative and avoid overextension.
Recommends making strategic moves during a recession when competition is weak, following the buy low, sell high principle.
Encourages treating employees well and striving for a fairer economic system.
Beau concludes by suggesting that now might be the time for certain individuals to make their move in the economic landscape.

Actions:

for entrepreneurs, small business owners,
Strategically plan and make moves during economic downturns to take advantage of opportunities (suggested)
Treat employees well and work towards a fairer economic system (suggested)
</details>
<details>
<summary>
2020-01-29: Let's talk about planting your yard.... (<a href="https://youtube.com/watch?v=EVV1WNGD8Lk">watch</a> || <a href="/videos/2020/01/29/Lets_talk_about_planting_your_yard">transcript &amp; editable summary</a>)

Beau advocates for planting your yard to grow food, foster community spirit, and combat division, urging everyone to reconnect with neighbors and cultivate self-reliance through gardening.

</summary>

"Plant your yard."
"Food doesn't always come out of a window in a brown paper bag with golden arches on it."
"Those people in your neighborhood, they're your neighbors. Nothing more. They're not the enemy."
"This is something everybody should do if they can."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Advocating for planting your yard, especially with spring approaching, to grow your own food and involve kids in the process.
Emphasizing the importance of food security, self-reliance, and fostering healthier eating habits through growing fruits and vegetables.
Mentioning that even with limited space, gardening can be done in containers like trash cans or small areas.
Sharing personal experiences of giving out excess produce to neighbors to foster community spirit.
Encouraging reconnecting with neighbors and building a strong community to combat division and political distractions.
Stating the benefits of gardening go beyond just saving money on food.
Stressing the simplicity of gardening by choosing plants suitable for the USDA growing zone and planting at the right time.
Sharing the joy and satisfaction that comes from experimenting with different plants and enjoying the process of gardening.
Suggesting replacing decorative shrubs with practical plants like raspberries for added security.
Urging everyone to plant their yard as a beneficial and enjoyable activity.

Actions:

for gardeners, community builders,
Plant fruits or vegetables in your yard or containers (suggested)
Share excess produce with neighbors to foster community spirit (suggested)
Connect with neighbors and build a strong community bond (suggested)
</details>
<details>
<summary>
2020-01-29: Let's talk about National Security and John Bolton talking.... (<a href="https://youtube.com/watch?v=MfuXFI1lK3U">watch</a> || <a href="/videos/2020/01/29/Lets_talk_about_National_Security_and_John_Bolton_talking">transcript &amp; editable summary</a>)

Beau challenges the need for secrecy in national security, advocating for transparency and the right of the American people to be informed, particularly during critical times like elections.

</summary>

"secrecy, the very word secrecy, is repugnant in a free and open society."
"John Bolton has information that the American people need to know."
"Whatever it is, let it come out and let the chips fall where they may come election time."

### AI summary (High error rate! Edit errors on video page)

Criticizes John Bolton's former chief of staff for criticizing Bolton's decision to write a tell-all during a presidential reelection campaign.
Believes it is the perfect time for officials to share information with the American people during such critical moments.
Emphasizes the importance of transparency in government and the right of the American people to know how decisions are made.
Suggests that keeping secrets in national security is not about hiding information but protecting the means and methods through which information is obtained.
Questions the actions of the Trump administration, citing examples like withholding military aid for political gain and compromising national security.
Expresses skepticism towards claims of caring about national security within the Trump administration.
Urges for transparency and the release of information that could potentially influence elections.
Challenges the idea of using secrecy for political purposes, stating it goes against democratic principles.

Actions:

for americans, voters, officials,
Advocate for transparency in government (suggested)
Stay informed about national security issues and demand accountability from officials (implied)
</details>
<details>
<summary>
2020-01-28: Let's talk about what a Native story can teach congress.... (<a href="https://youtube.com/watch?v=izfACc5zwcQ">watch</a> || <a href="/videos/2020/01/28/Lets_talk_about_what_a_Native_story_can_teach_congress">transcript &amp; editable summary</a>)

Beau recounts a poignant tale of a young boy's forced relocation and draws parallels to the dark moments in American history due to government shortsightedness and self-interest.

</summary>

"It was a trail of tears and death."
"Most of America's dark points are because of stuff like that."
"Sometimes, they're just an innate part of American culture."

### AI summary (High error rate! Edit errors on video page)

Recounts a story he heard around a campfire about a young boy forced to move from his village in winter.
Describes the harsh journey of the boy and his village being moved by soldiers, indifferent to their suffering.
Depicts the heartbreaking scenes of mothers struggling to keep their infants warm during the forced relocation.
Narrates how the boy, disillusioned by the elders' words, sneaks off with his friends, becoming part of the Cherokee remaining in the southeastern United States.
Mentions the Cherokee Rose flower, symbolizing the tears of mothers and the gold that drove them out during the Trail of Tears.
Links the spread of the Cherokee Rose as an invasive plant to the dark moments in American history.
Criticizes Congress for laying the foundation of such dark events and the executive branch for exploiting them.
Points out how Congress often prioritized personal gains over the well-being of the country.
Emphasizes the recurring theme of American dark points stemming from the shortsightedness and self-interest of those in power.
Suggests a lesson from the story for current Capitol Hill officials and acknowledges the grim reality behind the Trail of Tears' name.

Actions:

for legislators, policymakers, historians,
Learn about the history of forced relocations like the Trail of Tears and their impact (suggested)
Support initiatives that aim to preserve and honor the stories and cultures of indigenous peoples (suggested)
</details>
<details>
<summary>
2020-01-27: Let's talk about John Bolton rendering the impeachment moot.... (<a href="https://youtube.com/watch?v=kr_z_ve-Nlc">watch</a> || <a href="/videos/2020/01/27/Lets_talk_about_John_Bolton_rendering_the_impeachment_moot">transcript &amp; editable summary</a>)

John Bolton's meticulous revelations in his book may render impeachment irrelevant, putting Republican senators in a tough spot about their loyalty to Trump and the party.

</summary>

"True believers are the most dangerous people on the planet because they can rationalize anything."
"John Bolton is nothing if he is not meticulous."
"If this is the testimony you're going to get. Objection, Judge Roberts. This is devastating to my case."
"Trump is done. He's gone."
"Is one person worth destroying the entire party?"

### AI summary (High error rate! Edit errors on video page)

Explains the confusion surrounding John Bolton and how he may have made the impeachment irrelevant.
Describes John Bolton as a true believer in American dominance and protecting the American way of life.
Points out that some view John Bolton as a bad actor who has caused harm over the years.
Talks about John Bolton's consistent actions and how he can rationalize anything as a true believer.
Mentions that Trump's actions endangered U.S. national security during the impeachment.
Recounts the plan in the 80s involving Eastern European countries as a buffer against Russia.
Explains how withholding military aid while those countries were fighting could undermine their trust in the U.S.
States that John Bolton is meticulous and everything in his book is verifiable.
Suggests that Republican senators wouldn't want witnesses in the impeachment trial based on Bolton's testimony.
Predicts that the release of Bolton's book before the election could spell trouble for Trump and the Republican senators.

Actions:

for politically active citizens,
Spread awareness about John Bolton's revelations and their implications (implied)
Encourage political engagement and reflection on party loyalty (implied)
</details>
<details>
<summary>
2020-01-26: Let's talk about a misreading of intent and a soon-to-be household name.... (<a href="https://youtube.com/watch?v=xuufeeqsd9A">watch</a> || <a href="/videos/2020/01/26/Lets_talk_about_a_misreading_of_intent_and_a_soon-to-be_household_name">transcript &amp; editable summary</a>)

Beau presents Muqtada al-Sadr, a powerful Iraqi cleric making strategic moves amidst protests, urging the US to reconsider its presence for a peaceful resolution.

</summary>

"Muqtada al-Sadr will probably become a household name."
"The U.S. never should have been there."
"He's at the head of a very large movement."
"There's no reason for us to be there now."
"He is handing this administration in a gift-wrapped package, an easy out."

### AI summary (High error rate! Edit errors on video page)

Introducing Muqtada al-Sadr, a powerful cleric in Iraq, making strategic moves to elevate his position.
Muqtada al-Sadr has a significant following, with a powerful family background and a history of political and ideological shifts.
He has been involved in various movements, currently positioning himself as a nationalist and a populist.
During the US occupation, he led the Mahdi and engaged in activities supporting Sunnis in Fallujah.
Recent protests in Iraq against corruption and US presence have involved his supporters, who later pulled out before Iraqi Security Services intervened.
Muqtada al-Sadr's recent statement about peaceful protests is being misinterpreted in Western media, omitting the temporary nature of his stance.
He demands the removal of US forces, closure of bases, and airspace to warplanes while allowing diplomatic missions.
Despite calling for peaceful options, he has reactivated his armed organization, indicating potential regrouping rather than surrender.
Beau believes al-Sadr is sincere in seeking peaceful resolutions but suggests that failure to respond positively could lead to resumed active resistance.
Al-Sadr's ability to mobilize a large movement raises questions about US presence in Iraq, with Beau stating that the US never should have been there.

Actions:

for policy makers, activists,
Support organizations advocating for the withdrawal of US forces from Iraq (exemplified).
Join local protests or movements against foreign presence and corruption in your community (suggested).
</details>
<details>
<summary>
2020-01-26: Let's talk about Elizabeth Cochran, pinky, Nellie Bly and the white rabbit.... (<a href="https://youtube.com/watch?v=ZvzTsLG8iOw">watch</a> || <a href="/videos/2020/01/26/Lets_talk_about_Elizabeth_Cochran_pinky_Nellie_Bly_and_the_white_rabbit">transcript &amp; editable summary</a>)

Beau shares the remarkable life of Elizabeth Mary Jane Cochran, known as Pinky, who fearlessly pursued journalism, exposed injustices, and embraced adventures wholeheartedly.

</summary>

"Because when she saw the white rabbit and had a chance for an adventure, she took it."
"She followed the rabbit. She went where it led her."

### AI summary (High error rate! Edit errors on video page)

Introduces the story of Elizabeth Mary Jane Cochran, also known as Pinky, who pursued a career in journalism.
Cochran conducted investigative reporting on factory conditions, particularly focusing on women in factories.
Facing pushback from factories, she was reassigned to cover gardening and fashion shows, which she found unsatisfactory.
Cochran decided to become a foreign correspondent and traveled to Mexico, but had to flee after advocating for an imprisoned journalist.
Upon returning, she was relegated to covering fluff pieces but eventually quit.
She joined the New York World, Pulitzer's paper, and made a significant impact with her investigative work.
Cochran orchestrated being committed to an asylum on Blackwells Island to expose the neglect and brutality faced by women there.
Her experiences led to reforms being implemented.
Despite her impactful work, Cochran continued to seek new adventures, traveling around the world in 72 days and becoming an industrialist with patents.
Cochran's willingness to embrace opportunities for adventure and follow them wherever they led was a key factor in her extraordinary life.

Actions:

for history enthusiasts, aspiring journalists,
Advocate for reforms in institutions or systems that neglect or mistreat marginalized groups (exemplified)
Embrace new opportunities for adventure and growth (exemplified)
</details>
<details>
<summary>
2020-01-25: Let's talk about locks, lessons, and Texas.... (<a href="https://youtube.com/watch?v=iJoWeXSCpo4">watch</a> || <a href="/videos/2020/01/25/Lets_talk_about_locks_lessons_and_Texas">transcript &amp; editable summary</a>)

Beau questions the enforcement of a dress code targeting Black students with locks, challenging the validity and lessons taught through such policies.

</summary>

"It doesn't matter how hard you work, what you do, what you achieve. Somebody can take it away from you because of the way you look."
"I hate to break it to the school. I don't think there's a black kid in this country that needs that lesson taught to them because they see it every day from people like you."
"Discipline for discipline's sake, that's just authoritarian nonsense."
"The expectation is for a black student to try to be white and that's a really really bad expectation on a whole lot of levels."
"I can't think of any reason for it to exist."

### AI summary (High error rate! Edit errors on video page)

Questioning the enforcement of a dress code that targets students with locks, particularly Black students.
Examining the underlying issues and lessons being taught through these policies.
Pointing out the inconsistency in enforcing the dress code based on race.
Challenging the idea that certain hairstyles are distracting or unprofessional.
Criticizing the expectation for Black students to conform to white standards.
Expressing disbelief in the validity of the lessons being taught through this enforcement.
Describing discipline for the sake of discipline as authoritarian and nonsensical.
Emphasizing the message being sent to these young men about discrimination based on appearance.
Inviting others to share their thoughts on the lessons being taught in this situation.
Concluding with a powerful message about the impact of such policies on the students.

Actions:

for students, educators, activists,
Challenge and push back against discriminatory dress codes in schools (suggested)
Advocate for inclusive and non-discriminatory policies regarding hairstyles and clothing (suggested)
</details>
<details>
<summary>
2020-01-24: Let's talk about Trump's water rule.... (<a href="https://youtube.com/watch?v=zYyXEodsy64">watch</a> || <a href="/videos/2020/01/24/Lets_talk_about_Trump_s_water_rule">transcript &amp; editable summary</a>)

Beau stresses the importance of clean water and criticizes Trump's rollback of protections, warning of dire consequences and the need for future correction.

</summary>

"Water is life, clean water is good."
"This is going to be a mistake."
"Your water supply is tainted, your crops will die."
"People will die. This is ridiculous."
"We have a complete incompetent person in the White House."

### AI summary (High error rate! Edit errors on video page)

Water is life and clean water is good.
The Clean Water Act has protected America's waterways since 1972.
President Obama expanded the Clean Water Act to include smaller bodies of water based on scientific studies.
Trump has rolled back protections on waterways further than ever before.
The rule to undo protections was developed by political appointees, not based on scientific consensus.
Environmentalists believe Trump's actions will lead to deaths and polluted waterways.
The undoing of regulations allows developers to pollute for profit.
This decision goes against the Clean Water Act and endangers water supplies and crops.
Trump's actions will need to be undone by the next president to prevent further harm.

Actions:

for environmental activists, concerned citizens,
Contact local representatives to advocate for clean water protections (suggested)
Join environmental organizations working to safeguard waterways (implied)
</details>
<details>
<summary>
2020-01-24: Let's talk about Joe Rogan endorsing Bernie Sanders.... (<a href="https://youtube.com/watch?v=AJKxmMAOZJ8">watch</a> || <a href="/videos/2020/01/24/Lets_talk_about_Joe_Rogan_endorsing_Bernie_Sanders">transcript &amp; editable summary</a>)

Beau explains why Bernie should embrace Joe Rogan's endorsement, leveraging the split in Rogan's audience to draw voters away from Trump.

</summary>

"Your allies aren't perfect. They don't have to ride with you the whole way."
"On a battlefield, they will literally bring people in and their whole mission in life is to turn them against each other."
"Nobody wakes up knowing everything. Everybody has to have a path to follow."
"Let us talk about some issues that you may have. You've learned to look at this in a different way."
"When? Anyway, it's just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Joe Rogan endorsed Bernie Sanders, causing upset among different groups, including Democrats and supporters of various candidates.
Rogan has a history of saying controversial and harmful things, as well as platforming individuals who could cause harm.
Some believe Bernie should reject Rogan's endorsement, but Beau disagrees.
Beau argues that Rogan's endorsement may sway some of his audience away from supporting Trump.
The split in Rogan's audience creates internal conflict, potentially benefiting Sanders' campaign.
Beau acknowledges the discomfort around Rogan's problematic past but sees the endorsement as a strategic move to draw people away from Trump.
He criticizes Bloomberg for his approach to the campaign and suggests focusing on the bigger picture of defeating Trump.
Beau encourages viewing allies like Rogan pragmatically, as long as they are moving in the same direction politically.
He advocates for engaging with Rogan's audience positively to bring them over to new ideas, rather than pushing them away.
Beau stresses the importance of unity and strategic alliances in achieving victory, even if it means setting aside personal dislike.

Actions:

for political activists,
Engage positively with individuals from different political backgrounds to bridge gaps (suggested)
Focus on bringing people over to new ideas rather than pushing them away (implied)
</details>
<details>
<summary>
2020-01-23: Let's talk about China, pandemics, and conveying calm.... (<a href="https://youtube.com/watch?v=hj39fDQava0">watch</a> || <a href="/videos/2020/01/23/Lets_talk_about_China_pandemics_and_conveying_calm">transcript &amp; editable summary</a>)

Beau conveys calm amidst pandemic concerns, stressing the importance of caution and trust in authorities while debunking media sensationalism.

</summary>

"There's no reason to panic."
"So just stay calm, do what you do, and let them do what they do."
"I don't see anything to really panic about."
"I really don't see this as the giant pandemic everybody's worried about."
"It's guaranteed we are going to have a bad pandemic at some point in the future."

### AI summary (High error rate! Edit errors on video page)

China has quarantined a large city, which can be unnerving.
Attended a pandemic prevention conference where worst-case scenarios were discussed to instill caution.
Presenter emphasized the importance of waiting for demographic information on patients.
Majority of patients with the virus were older and had pre-existing conditions.
Virus seems to affect those who are already compromised.
Not everyone infected had a fever, which is concerning as fever is a common screening method.
Prevention methods include washing hands frequently, staying away from sick individuals, and sneezing properly.
Surveillance and early action are key in controlling new threats.
Media sensationalism can lead to panic and desensitization to real pandemics.
Beau believes it is not yet time to panic or wear gas masks.

Actions:

for community members,
Wash hands frequently, stay away from sick individuals, and sneeze properly (suggested)
Trust in authorities and follow their guidelines (implied)
</details>
<details>
<summary>
2020-01-22: Let's talk about why Billy Joe jumped off the Tallahatchie Bridge.... (<a href="https://youtube.com/watch?v=EFd9QZG0L7c">watch</a> || <a href="/videos/2020/01/22/Lets_talk_about_why_Billy_Joe_jumped_off_the_Tallahatchie_Bridge">transcript &amp; editable summary</a>)

Unveiling the mystery behind "Ode to Billy Joe," Beau reveals a darker truth - it's not about what was thrown off the bridge, but the apathy and lack of care surrounding Billy Joe's tragic act.

</summary>

"It's not the scandal or the gossip that we focus on. It's true."
"The plot isn't about him jumping off the bridge; it's about the lack of caring at the table."
"Life is never without the means to dismiss itself, but that seems like a really, really bad reason to do it."

### AI summary (High error rate! Edit errors on video page)

Unveiling the mystery behind the deeper meaning of the song "Ode to Billy Joe" by Bobby Gentry, not Janis Joplin.
The song's intrigue lies in the unknown object thrown off the Tallahatchie Bridge.
Despite theories ranging from a wedding ring to a baby, the true meaning is darker and not about the object thrown.
The plot of the song isn't about Billy Joe jumping off the bridge; it centers on the unnoticed pain and lack of caring among those close to him.
It's not the act of jumping off the bridge that matters; it's the indifference and lack of concern from others.
Life offers ways to dismiss itself, but seeking attention through drastic actions is not a valid reason.
The profound message lies in the apathy displayed by those around, rather than the sensationalism of the event.
Bobbi Gentry purposefully left the meaning open-ended, focusing on the nonchalant nature of the family's dinner table talk after the incident.
The disinterest shown by those closest to Billy Joe is a harsh reality mirrored in society's tendency to focus on scandal and gossip rather than genuine care.
The deeper point of the song revolves around the aftermath of drastic actions and the lack of understanding and compassion from loved ones.

Actions:

for story enthusiasts, music lovers,
Reach out to loved ones and offer genuine care and support (exemplified)
Encourage open and honest communication within families and friendships (exemplified)
</details>
<details>
<summary>
2020-01-22: Let's talk about the climate-migrant paradox.... (<a href="https://youtube.com/watch?v=x868wZY3_U0">watch</a> || <a href="/videos/2020/01/22/Lets_talk_about_the_climate-migrant_paradox">transcript &amp; editable summary</a>)

Americans must choose between owning the libs by destroying the environment or addressing climate change to prevent a future influx of climate change refugees.

</summary>

"Do they enjoy owning the libs more than they hate brown people?"
"We have to make major changes. We have to address our infrastructure."
"Those who are going to be most heavily impacted by climate change are in locations that aren't really responsible."
"Might want to think about dropping the bumper sticker mentality."
"A little bit of legal precedent protecting those in the developing world."

### AI summary (High error rate! Edit errors on video page)

Talks about a man from a country at risk of disappearing due to rising sea levels who fled to New Zealand and claimed he had to stay.
The UN decided he could return because his country disappearing is 10 to 15 years away, not immediate danger.
Mentions Americans who deny climate change and want to "own the libs" will have to face climate change refugees.
States that Americans must choose between owning the libs by destroying the environment and hating brown people.
Raises the question of whether people value owning the libs more than addressing climate change.
Indicates the need for major changes in infrastructure, consumption, and addressing climate change.
Mentions legal precedent protecting developing nations from actions of developed countries.
Notes that those most impacted by climate change are often in locations with small carbon footprints.
Suggests that people impacted by climate change may seek more habitable locations.
Urges moving beyond bumper sticker mentalities and working on real solutions to address climate change.

Actions:

for climate change activists,
Address infrastructure and consumption (implied)
Work on real solutions to climate change (implied)
</details>
<details>
<summary>
2020-01-22: Let's talk about holding government accountable and Flint.... (<a href="https://youtube.com/watch?v=UCWhHjVxhVA">watch</a> || <a href="/videos/2020/01/22/Lets_talk_about_holding_government_accountable_and_Flint">transcript &amp; editable summary</a>)

The Supreme Court's refusal to hear Flint water crisis cases may signal accountability for officials, prompting reflections on cover-ups, potential future actions, and voter dissatisfaction with the current administration.

</summary>

"It's not the crime, it's the cover up."
"Justice delayed is justice denied."
"Americans are not happy with the current administration and those protecting it."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court refused to hear cases related to the Flint water crisis, opening the door for officials involved to be held accountable.
Government officials can be held accountable when the government stops protecting them.
Beau questions if accountability will also happen in other high-profile cases.
He suggests that the cover-up is often more significant than the crime itself.
Beau hints at potential accountability actions even before the current administration leaves office.
Government officials often live in an echo chamber, only listening to those who agree with them.
Americans are unhappy with the current administration and its protectors.
Beau mentions the possibility of another impeachment in the house.
He points out that justice delayed is justice denied, drawing a parallel to the Flint water crisis.
Beau concludes by reflecting on the potential consequences for senators during election time.

Actions:

for voters, activists,
Contact elected officials to demand accountability for government actions (implied)
Stay informed and engaged in political processes (implied)
</details>
<details>
<summary>
2020-01-21: Let's talk about what Sherlock Holmes, the impeachment, and dogs.... (<a href="https://youtube.com/watch?v=JclKjpmhj2Q">watch</a> || <a href="/videos/2020/01/21/Lets_talk_about_what_Sherlock_Holmes_the_impeachment_and_dogs">transcript &amp; editable summary</a>)

Beau questions why Republicans in the Senate are not aggressively dismantling the impeachment narrative despite the weak evidence, drawing parallels to Sherlock Holmes' silent dog. He speculates that they may recognize the guilt but choose not to expose it.

</summary>

"Sometimes a negative fact can lead you to the right question."
"I'm fairly certain that our senatorial dog recognizes the guilty person."

### AI summary (High error rate! Edit errors on video page)

Explains the power division between the House and Senate in impeachment proceedings.
Democrats bet the 2020 election on impeaching the president with their best evidence.
Polls show a significant portion of Americans support impeaching and removing the president.
Mitch McConnell and the Republicans in the Senate have the power over the trial.
Beau questions why Republicans are not aggressively dismantling the impeachment narrative.
Draws parallels to a Sherlock Holmes story about a silent dog revealing a negative fact.
Suggests that the Senate's behavior indicates they may recognize the guilt but are not exposing it.
Questions why the Republicans are not seizing the chance to destroy their political opponents with weak evidence.
Speculates that the Senate is intentionally downplaying and hiding proceedings.
Beau concludes with a thought on the Senate's behavior.

Actions:

for political observers,
Watch and stay informed about the impeachment trial proceedings (implied)
Engage in political discourse and analysis with others to understand different perspectives (implied)
</details>
<details>
<summary>
2020-01-21: Let's talk about the load carriers of history and Doris Miller.... (<a href="https://youtube.com/watch?v=MUu-JitGePU">watch</a> || <a href="/videos/2020/01/21/Lets_talk_about_the_load_carriers_of_history_and_Doris_Miller">transcript &amp; editable summary</a>)

Exploring the unsung heroes of history, Beau recounts the bravery of Doris Miller and the historic naming of an aircraft carrier after him, symbolizing progress and hope.

</summary>

"Naming an aircraft carrier after an enlisted person, that's a huge deal in and of itself."
"It's a big deal. This is a big deal on a whole bunch of different levels."
"There are still signs of progress, signs of hope."
"Today they're naming one after an enlisted black guy. It's a cool development."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Exploring the unsung heroes of history who carried the load and shaped events.
Common people often go unrecognized for their contributions, overshadowed by leaders like presidents.
Doris Miller, a cook during Pearl Harbor, stepped up to carry the wounded captain and assist in the defense.
Despite lacking training, Miller operated an anti-aircraft gun effectively, engaging the enemy.
After running out of ammo, Miller turned to aiding the wounded, showcasing bravery and selflessness.
Miller's heroic actions saved lives on a day filled with chaos and danger.
He was recognized for his bravery but later lost his life in action during World War II.
Miller's legacy lives on as an aircraft carrier is named after him, a rare honor for an enlisted person.
The decision to name a carrier after Miller signifies progress and change in institutions like the Navy.
This historic moment marks the first time a black person has had an aircraft carrier named in their honor.

Actions:

for history enthusiasts, advocates for recognition of unsung heroes.,
Research and share stories of lesser-known historical figures who made significant contributions (suggested)
Advocate for more diverse representation and recognition in historical commemorations (exemplified)
</details>
<details>
<summary>
2020-01-21: Let's talk about the end of capitalism and Davos.... (<a href="https://youtube.com/watch?v=NMdZSbfjLNI">watch</a> || <a href="/videos/2020/01/21/Lets_talk_about_the_end_of_capitalism_and_Davos">transcript &amp; editable summary</a>)

Beau introduces the World Economic Forum's focus on the "peak decade" with insights on tariffs, central banks, peak oil, climate change, income inequality, and the potential shift towards socialism and protectionism.

</summary>

"This really is a let's talk about it video."
"The people who attend these things are pretty smart, and they find a way to cushion the negative effects of all of this."
"It's just going to be bad."
"Some of the brightest economic minds in the world are pretty much at this point saying that socialism is inevitable for a whole lot of countries."
"I cannot wait to see the comments section, especially the economists that follow this."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of the World Economic Forum at Davos and its focus on the "peak decade."
Mentions the negative impact of tariffs, with studies showing that American citizens, importers, and consumers bear the cost.
Predicts the decline of central banks' effectiveness in handling financial crises.
Foresees peak oil in 2035 leading to industry failures and job losses.
Talks about the expected decline in car production due to urban infrastructure and a shift to electric and renewable energy.
Addresses climate change and its economic implications, including strains on infrastructure.
Points out the demographic shift towards more older people than younger, impacting social safety nets like social security.
Mentions a Stanford professor predicting a population decline due to economic factors affecting birth rates.
Raises concerns about income inequality and the potential end of capitalism, with CEOs focusing on stakeholders over shareholders.
Suggests a shift towards socialism in business practices as a response to income inequality.
Predicts the end of globalization with countries turning more protectionist.
Forecasts a choice between state capitalist systems or open society systems with free markets for countries like the United States.

Actions:

for economists, policymakers, activists,
Attend or follow updates from economic forums for insights and potential solutions (implied)
</details>
<details>
<summary>
2020-01-19: Let's talk about exhausting options and Virginia.... (<a href="https://youtube.com/watch?v=JNEjh3qcn1I">watch</a> || <a href="/videos/2020/01/19/Lets_talk_about_exhausting_options_and_Virginia">transcript &amp; editable summary</a>)

Beau questions the wisdom of extreme measures in gun advocacy and suggests a more strategic, nuanced approach to address cultural and legislative issues in Virginia.

</summary>

"Straight to the most extreme option. Probably not a good idea."
"Maybe the best idea is to pull a fast one and everybody just show up without their stuff."
"It may be time to address a cultural issue within your community."
"This doesn't seem like it was well thought out."
"I guess it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau questions the motives behind resorting to extreme options in Virginia, attributing it to a growing trend in the country.
He warns against jumping straight to the most extreme measures, noting that it may reinforce the very ideas that led to the legislation in the first place.
Beau points out the potential consequences of escalating the situation, either through peaceful or loud means.
He suggests that a more effective approach might be to show up without weapons, as a show of force without escalating tensions.
Beau underscores the importance of considering the long-term implications of actions, especially in relation to gun ownership and government involvement.
He brings attention to the need for a cultural shift within the community regarding the perception of firearms.
Beau expresses concerns about the lack of foresight in the current course of action and its potential impact on gun rights advocacy.
He questions the wisdom of applauding officials who tie gun ownership to government service, potentially undermining the fight for individual rights.
Beau advocates for a more strategic and thoughtful approach to advocacy, rather than immediate and extreme reactions.
He concludes by suggesting a more nuanced and calculated strategy in addressing the issues at hand.

Actions:

for gun advocates,
Show up without weapons to demonstrate strength and unity (suggested)
Advocate for a cultural shift within the community regarding firearms (implied)
</details>
<details>
<summary>
2020-01-19: Let's talk about cars, buses, and allies.... (<a href="https://youtube.com/watch?v=nxoHtj9p1A0">watch</a> || <a href="/videos/2020/01/19/Lets_talk_about_cars_buses_and_allies">transcript &amp; editable summary</a>)

Beau explains the importance of allies moving towards a common goal like passengers on a city bus, focusing on progress over arguments.

</summary>

"Your allies aren't perfect. They're never going to believe the same thing that you do."
"If you're headed in the same direction your allies, yeah, there will be spats, there will be arguments, but they should be pretty short-lived."
"It's a long journey, it's going to take a long time to get there."

### AI summary (High error rate! Edit errors on video page)

Observes people debating allies instead of working towards a common goal.
Defines allies as those generally moving in the same direction towards a shared destination.
Compares allies to being on a city bus where people can join and leave at different stops.
Emphasizes the importance of focusing on progress and maintenance rather than arguments.
Acknowledges that allies won't always agree but should minimize conflicts due to greater goals.
Encourages understanding that personal journeys may affect how far allies are willing to go.
Stresses the long journey towards a shared utopia and the need for patience.
Urges prioritizing actions that contribute to reaching the destination over theoretical debates.
Reminds that allies are not perfect and disagreements may arise but should not hinder progress.

Actions:

for community members,
Maintain the community bus: Ensure the vehicle (transcript implied) is well-equipped for the journey towards a shared goal.
</details>
<details>
<summary>
2020-01-18: Let's talk about the US, UK, Canada, and contingency plans.... (<a href="https://youtube.com/watch?v=geBetDLDxGM">watch</a> || <a href="/videos/2020/01/18/Lets_talk_about_the_US_UK_Canada_and_contingency_plans">transcript &amp; editable summary</a>)

Contingency planning, historical war strategies, and Trump's impact on U.S. readiness and national security, unpacked by Beau.

</summary>

"Contingency plans are our bread and butter."
"War is a failure of diplomacy."
"It's not the fault of the next person. It's Trump's fault."

### AI summary (High error rate! Edit errors on video page)

Contingency plans are prevalent in the Senate, leading to a power struggle with nobody able to predict the outcomes.
The United States is known for its contingency planning, especially by the Department of Defense, aimed at advancing national interests.
Historical examples show the U.S. had plans for war with various countries, like invading Canada as a strategic move against the UK.
Allies like the UK also had plans to attack the U.S., showcasing the importance of consistent defense planning.
Beau criticizes Trump's unpredictable nature, which has damaged U.S. readiness and strained relationships with allies.
Trump's focus on domestic rivals rather than national interests has implications for diplomacy and national security.
Beau warns of the lasting impact of Trump's actions on future presidents and U.S. readiness.

Actions:

for policy analysts, historians, voters,
Analyze and understand historical contingency plans to learn from past strategies (implied)
Advocate for consistent defense planning and diplomatic efforts for national security (implied)
Support policies that prioritize advancing national interests over domestic rivalries (implied)
</details>
<details>
<summary>
2020-01-18: Let's talk about Darkness, edge, art, and whether or not it matters.... (<a href="https://youtube.com/watch?v=71m4MnWuweU">watch</a> || <a href="/videos/2020/01/18/Lets_talk_about_Darkness_edge_art_and_whether_or_not_it_matters">transcript &amp; editable summary</a>)

Beau dives into Eminem's new song, recognizing its artistic depth and the importance of sparking societal change, particularly driven by younger generations.

</summary>

"He's holding a mirror up to reality, just letting people see it."
"Sometimes there's justice and sometimes there's just us."
"We have to change the culture."
"It's not gonna be people his and my age."
"Because it's not going to make a difference."

### AI summary (High error rate! Edit errors on video page)

Beau starts off by discussing Eminem's new song and his own views on Eminem and his music.
Despite not being a fan of Eminem, Beau acknowledges the genius in Eminem's lyrics and the power they hold.
Beau talks about how he was encouraged by friends to listen to Eminem's new song, so he gave it a shot.
He mentions catching on to various double meanings in the lyrics, particularly referencing a high-end brand, Heckler.
Beau appreciates the artistic elements in Eminem's song, like the Easter eggs, visual cues, and the use of confusion to convey a message.
He points out a portion of the song where he believes there's a deeper meaning implied by Eminem.
Beau praises Eminem for holding a mirror up to reality through his art and sparking meaningful conversations.
He expresses agreement with the need for serious societal discourse initiated by Eminem's work.
Beau comments on the importance of younger generations driving change and making difficult decisions to reshape society.
He concludes by reflecting on the enduring nature of societal issues and the role of future generations in addressing them.

Actions:

for music enthusiasts, social activists,
Engage in meaningful dialogues about societal issues (implied)
</details>
<details>
<summary>
2020-01-16: We talked about what happens behind closed doors and helped out.... (<a href="https://youtube.com/watch?v=hZ-UW3u_Hw4">watch</a> || <a href="/videos/2020/01/16/We_talked_about_what_happens_behind_closed_doors_and_helped_out">transcript &amp; editable summary</a>)

Beau sheds light on the pervasive nature of intimate partner violence, the challenges faced by survivors in leaving abusive relationships, and the vital role of community support and organizations in providing safety and assistance.

</summary>

"Women are 500 times more likely to be killed if they're in one of these relationships when they're just leaving."
"Violence isn't always the answer."
"Odds are you have an old cell phone sitting in a junk drawer, mail it to them. It means nothing to you, could save someone's life."
"Community support and donations can help provide essentials and comfort to survivors."
"Individual actions, even small ones, can contribute to collective goals in supporting survivors of domestic violence."

### AI summary (High error rate! Edit errors on video page)

Personal connections drive involvement in the fight against intimate partner violence.
Domestic violence affects everyone regardless of race, gender, or class.
Intimate partner violence accounts for 15% of all violent crime in the U.S.
Half of women killed in the U.S. are killed by a current or former intimate partner.
Financial control by abusers makes it hard for victims to leave.
Leaving an abusive relationship is the most dangerous time for victims.
Domestic violence shelters provide a safe haven for those leaving abusive relationships.
Support organizations like Shelter House of Northwest Florida provide vital assistance to survivors.
These organizations also help alleviate traumatic experiences, such as rape kits and fresh clothes for survivors.
Animal shelters by these organizations help remove barriers to leaving abusive situations.
Donations, especially old cell phones, can make a significant impact on survivors' ability to seek help and support.
Community support and donations can help provide essentials and comfort to survivors, like Christmas presents for teens in shelters.
PTSD triggers like scents can influence traumatic memories, suggesting potential avenues for support.
Individual actions, even small ones, can contribute to collective goals in supporting survivors of domestic violence.

Actions:

for supporters of survivors,
Donate money or old cell phones to organizations like Shelter House of Northwest Florida (suggested)
Support organizations that provide essentials and comfort to survivors (exemplified)
Volunteer time or resources to assist domestic violence shelters (implied)
</details>
<details>
<summary>
2020-01-16: Let's talk about what happens if you can't dance to the movement.... (<a href="https://youtube.com/watch?v=Ax2ohRVz-GY">watch</a> || <a href="/videos/2020/01/16/Lets_talk_about_what_happens_if_you_can_t_dance_to_the_movement">transcript &amp; editable summary</a>)

Beau addresses how to stay resilient amid exposure to negativity, urging the importance of joy in activism and self-care, quoting Emma Goldman on not denying life for a cause, and preparing viewers for upcoming challenges.

</summary>

"If I can't dance to it, it's not my revolution."
"The world's pretty messed up. But you have to take care of yourself."
"If something comes into being full of anger, it's going to be angry."

### AI summary (High error rate! Edit errors on video page)

Addresses the question of how to keep going and not get worn down in the face of constant exposure to the worst of humanity through the news.
Emphasizes the importance of finding happiness and joy amidst the pursuit of a better world.
Encourages finding joy in simple things like raindrops on a tin roof, music, or being around people who bring happiness.
Advocates for balance between political activism and personal joy by quoting Emma Goldman on not denying life and joy for a cause.
Stresses the significance of maintaining a positive mindset and experiencing joy to sustain oneself in the fight for a better future.
Urges self-care and the understanding that taking care of oneself is vital to staying effective in making a difference.
Quotes Emma Goldman on the interconnectedness of aims, purposes, methods, and tactics in achieving goals.
Mentions a part two to the Maddow interview and addresses Russia, advising not to panic as it's a normal restructuring.
Encourages viewers to have fun and do things that make them happy as they prepare for what seems like a challenging road ahead.

Actions:

for activists, advocates, supporters,
Surround yourself with people who bring you joy and remind you of the good in the world (implied).
Take time to do things that make you happy and recharge your batteries (implied).
</details>
<details>
<summary>
2020-01-15: Let's talk about what Princess Diana can teach us about diplomacy.... (<a href="https://youtube.com/watch?v=hnbJDxM3G_4">watch</a> || <a href="/videos/2020/01/15/Lets_talk_about_what_Princess_Diana_can_teach_us_about_diplomacy">transcript &amp; editable summary</a>)

Exploring Princess Diana's impact on diplomacy, contrasting it with the Trump administration's "landmine diplomacy," and calling for future administrations to humanize those impacted by current foreign policies.

</summary>

"She had walked through social minefields."
"She humanized them, brought them into the discussion."
"The decisions that this administration is making on the world stage, a lot of them, they won't be around to deal with the mess."

### AI summary (High error rate! Edit errors on video page)

Exploring Princess Diana's impact on diplomacy and humanitarian efforts.
Princess Diana's influence as a trendsetter beyond fashion.
How Princess Diana's actions in Angola drew attention to landmines.
The success of Princess Diana's call to end the use of landmines.
The devastating impact and hidden danger of landmines.
Comparing the Trump administration's foreign policy to "landmine diplomacy."
The need for future administrations to address the impacts of current foreign policies.
The importance of humanizing those impacted by foreign policies.
Mentioning countries impacted by the current administration's foreign policy.
Beau's call for future administrations to navigate the "landmines" left by the current administration.

Actions:

for diplomats, policymakers, activists.,
Reach out to countries impacted by current foreign policies, humanize the affected individuals (exemplified).
Advocate for the removal of landmines and support initiatives to address their devastating impact (exemplified).
</details>
<details>
<summary>
2020-01-15: Let's talk about what Parnas gave up.... (<a href="https://youtube.com/watch?v=m-Pe7IljLXM">watch</a> || <a href="/videos/2020/01/15/Lets_talk_about_what_Parnas_gave_up">transcript &amp; editable summary</a>)

Beau dives into the House Intel Committee release, uncovering alarming allegations and stressing the need for transparency and accountability in the face of potential misconduct and security breaches.

</summary>

"There are some pretty serious allegations that could be made from this."
"It certainly appears that somebody is conducting surveillance on a US ambassador. That's pretty wild."
"It certainly appears shady. I believe it was. But we need more."
"Interesting times, isn't it?"
"Anyway, it's just a thought. Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Analyzing the release from the House Intel Committee, Beau points out the significance of the information and urges everyone to read through it.
Beau expresses the gravity of the allegations presented in the release and stresses the importance of not letting them be overlooked.
The transcript delves into specific notes and messages found within the released documents, shedding light on potential misconduct and abuse of power.
There's a focus on the communication between individuals regarding surveillance of a US ambassador, raising concerns about security breaches.
The involvement of Giuliani as the president's personal attorney in matters beyond the usual scope draws attention to potential misuse of power.
The transcript hints at possible foreign involvement and surveillance of a US embassy, sparking questions about the intentions and implications behind such actions.
Amidst speculation and uncertainty, Beau underlines the need for further investigation and clarification to fully comprehend the situation at hand.
The ongoing surveillance and monitoring of the ambassador's movements indicate a serious breach of security protocols.
The release of the House Intel Committee prompts Beau to encourage thorough understanding and engagement with the information provided.
Beau acknowledges the shady and concerning nature of the revelations, prompting a call for transparency and accountability in addressing the allegations.

Actions:

for political enthusiasts, concerned citizens,
Reach out to representatives or investigative bodies for further inquiries into the allegations (suggested)
Stay informed and engaged with updates on the situation (implied)
</details>
<details>
<summary>
2020-01-13: Let's talk about wallabies and the good in people.... (<a href="https://youtube.com/watch?v=qLo0hkAuAyE">watch</a> || <a href="/videos/2020/01/13/Lets_talk_about_wallabies_and_the_good_in_people">transcript &amp; editable summary</a>)

Humanity's best emerges in unity during crises, but sustaining that spirit beyond disasters remains a challenge.

</summary>

"The spirit that exists after things go bad, it's the best that humanity has to offer."
"Most people want a just world and they're willing to work for it."
"I just can't wait until we do the same thing for people."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the positive side of humanity coming together during challenging times, like after Hurricane Michael when everyone on the coast united to help each other.
He mentions the incredible diversity of people who banded together regardless of their backgrounds.
The spirit of cooperation and support usually lasts about six months after a crisis before fading away.
Beau expresses a desire to understand why this unity fades and hopes to find a way to sustain it without the need for constant existential threats.
Shifting gears, Beau shares the heartwarming story of people in Australia helping wallabies by airdropping thousands of kilograms of sweet potatoes and carrots to their colonies affected by fires.
He admires the willingness of people to go above and beyond to create a just world, even risking their lives by flying over fires to aid the wallabies.
Beau expresses a wish for the same level of care and support to be extended to humans as well, reflecting on the need for such compassion to continue beyond natural disasters.

Actions:

for community members,
Support relief efforts in your community (exemplified)
Volunteer to aid those affected by disasters (exemplified)
</details>
<details>
<summary>
2020-01-13: Let's talk about the pursuit of knowledge and the status quo.... (<a href="https://youtube.com/watch?v=9032goyLAps">watch</a> || <a href="/videos/2020/01/13/Lets_talk_about_the_pursuit_of_knowledge_and_the_status_quo">transcript &amp; editable summary</a>)

Beau explains historical fears of science and regulations against innovation driven by those in power fearing disruptions to their control.

</summary>

"When we have our betters telling us that certain technologies or certain fields of study aren't worth pursuing, we should always ask why."
"A lot of laws and regulations that are aimed at science, that's what it's about."
"It's about those in power, the establishment, the status quo, not really understanding it and therefore being afraid of it."
"The establishment is slow to change."
"It's just them attempting to preserve their place in the world above."

### AI summary (High error rate! Edit errors on video page)

Explains the fear of science and the historical example of King Henry IV's law against multipliers in 1404.
Multipliers were alchemists who claimed to turn base metals into gold, causing concern among the rich and powerful.
The law was passed to prevent the potential destabilization of the status quo if alchemy actually worked.
Laws and regulations aimed at science are often driven by those in power fearing the unknown and potential disruptions to their control.
Innovation has been stifled throughout history due to regulations designed to slow the pursuit of knowledge until legislators understand it.
Legislators tend to learn slowly, leading to a slow acceptance of change in established systems.
Breakthroughs have often occurred through less-than-legal means due to research being blocked by regulations.
Regulations and prohibitions on new technologies or fields of study tend to persist once established, simply because "that's how it is."
The law against multipliers was eventually repealed after prominent names in science, like Isaac Newton, spoke out against it.
Beau encourages questioning authorities who discourage certain technologies or fields of study, as it may be a tactic to maintain their power.

Actions:

for science enthusiasts, advocates for innovation.,
Advocate for increased support and funding for scientific research (implied).
Challenge authority figures or institutions discouraging exploration of certain fields of study (implied).
</details>
<details>
<summary>
2020-01-13: Let's talk about Casablanca and perception.... (<a href="https://youtube.com/watch?v=BiynNSGYYD0">watch</a> || <a href="/videos/2020/01/13/Lets_talk_about_Casablanca_and_perception">transcript &amp; editable summary</a>)

Casablanca's perception versus reality lesson applies to contemporary global events, where managing perceptions can be a matter of life and death.

</summary>

"Perception is often more important than what actually happened."
"The perception is what saves lives or costs them."
"The perception of the last couple of weeks from outside the United States, it's not good, it's not good."

### AI summary (High error rate! Edit errors on video page)

Casablanca is perceived as one of the greatest films ever made, even by those who have never seen it.
The most memorable quote from Casablanca, "Play it again, Sam," is not actually in the movie.
In January 1943, Roosevelt, Churchill, and de Gaulle met in Casablanca to talk about the situation in Europe.
Roosevelt issued a proclamation demanding unconditional surrender, but British intelligence was already working on deals behind the scenes.
The proclamation was made for the benefit of the Soviet guy who couldn't attend the meeting.
Perception often outweighs reality, as demonstrated in historical events like these.
The perception of events can shape outcomes and have significant impacts.
The importance of managing perception applies to contemporary issues as well.
The recent events have raised questions about the true intentions of the United States in various global locations.
The actions and decisions of the current administration are viewed through a transactional lens, impacting perceptions globally.

Actions:

for global citizens,
Question perceptions and seek the truth behind official narratives (suggested)
Advocate for transparent and ethical decision-making in international relations (implied)
</details>
<details>
<summary>
2020-01-12: Let's talk about starting your YouTube channel.... (<a href="https://youtube.com/watch?v=BgZ_nSqFNDs">watch</a> || <a href="/videos/2020/01/12/Lets_talk_about_starting_your_YouTube_channel">transcript &amp; editable summary</a>)

Beau dives into practical advice for new YouTubers, stressing authenticity, relatability, and community building over generic recommendations.

</summary>

"Be authentic."
"Nobody cares about you."
"Ignore the money."

### AI summary (High error rate! Edit errors on video page)

Beau dives into the topic of starting a YouTube channel, a question he receives frequently.
Viewers often find generic advice on starting a channel not applicable to their specific content.
Beau advises content creators to prioritize authenticity in their videos to connect with viewers.
He stresses the importance of staying within your expertise and relating content back to what you know well.
Beau challenges the idea of maintaining a consistent posting schedule, especially for news-related content.
He bluntly states that initially, viewers do not care about the creator; they come for the content.
Being relatable and admitting to mistakes is seen as a key factor in building a connection with the audience.
Beau advises against sensationalism and clickbait titles, focusing on creating engaging but genuine content.
He simplifies the YouTube algorithm's goal: keeping viewers engaged on the platform for as long as possible.
Using Easter eggs and creating a community-centric platform are encouraged by Beau.
Having a clear goal for your channel is emphasized over simply aiming for success.
Beau advises creators to offer unique perspectives and avoid replicating existing content.
Experimenting lightly with changes in content is suggested to maintain viewer engagement.
Ignoring the allure of making money on YouTube is advised, with an emphasis on having a genuine purpose for creating content.

Actions:

for new youtubers,
Build a community-centered platform (suggested)
Offer unique perspectives in your content (suggested)
Experiment lightly with changes in content to maintain viewer engagement (suggested)
</details>
<details>
<summary>
2020-01-12: Let's talk about how there can be only one and a question from a mom.... (<a href="https://youtube.com/watch?v=pZCgR1xS7iE">watch</a> || <a href="/videos/2020/01/12/Lets_talk_about_how_there_can_be_only_one_and_a_question_from_a_mom">transcript &amp; editable summary</a>)

Beau helps a mother address her son's nationalist leanings by recommending the TV show "Highlander" to provide a different perspective on warrior ethos and nationalism.

</summary>

"Nationalism is inextricably tied to that journey of warrior-dom, it's there in the beginning and it fades as you know."
"Sometimes fiction is what can get to truth, and that's what he's going to need to know."
"I think it may be perfect for this."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a heavy email he received from a mother concerned about her son's increasing interest in nationalism and warrior ethos, fearing he may follow in his late father's footsteps who was lost in the military.
The mother expresses discomfort with her son's deep interest in history and martial arts, possibly influenced by his father's military background.
Concerned about her son adopting hateful ideologies, the mother seeks Beau's help in guiding him away from nationalist tendencies.
Beau suggests introducing the son to a TV show character, Duncan MacLeod from "Highlander," to provide a different perspective on warrior ethos and nationalism.
He describes how Duncan's character evolves from a hotheaded warrior to someone who questions nationalism and patriotism.
Beau believes that through watching this show with his mother, the son can learn valuable lessons and potentially shift his mindset away from hateful ideologies.
He acknowledges that facts alone may not be effective in changing the son's views, suggesting that fiction like the TV show could help him understand deeper truths.
Beau concludes by offering his assistance and expressing hope that this alternative approach may make a positive impact on the son.

Actions:

for parents, concerned individuals.,
Watch "Highlander" with individuals showing nationalist tendencies (suggested).
</details>
<details>
<summary>
2020-01-10: Let's talk about what we can learn from unwanted house guests.... (<a href="https://youtube.com/watch?v=MDDZbtvTB18">watch</a> || <a href="/videos/2020/01/10/Lets_talk_about_what_we_can_learn_from_unwanted_house_guests">transcript &amp; editable summary</a>)

Unwanted house guests teach lessons; failing to address them can lead to more problems and turn the household against them.

</summary>

"Unwanted house guests teach valuable lessons."
"Failing to talk about leaving is the latest mistake in a long string of mistakes."
"Talking is weakness."

### AI summary (High error rate! Edit errors on video page)

Unwanted house guests teach valuable lessons.
Friends and neighbors often recognize bad house guests before you do.
Good neighbors will be direct about warning you.
Imagine having an unwanted house guest who refuses to leave.
The neighbor warns that the house guest won't leave when the time comes.
The neighbor tries to help by making it uncomfortable for the house guest.
The house guest ends up physically confronting the neighbor.
As a responsible homeowner, you need to address the situation.
The house guest refuses to talk about leaving and demands obedience.
The house guest sees talking as weakness and believes control is in their hands.
Failing to address the issue of the unwanted guest can lead to more problems.
The outcome depends on the actions of the people in the house.
There's a growing belief among the house guest that talking about leaving is a mistake.
Not discussing leaving could turn the household against the house guest.

Actions:

for homeowners,
Address uncomfortable situations with unwanted guests (exemplified)
Have direct and honest communication with problematic individuals (exemplified)
</details>
<details>
<summary>
2020-01-10: Let's talk about what two sayings can teach us about a plane.... (<a href="https://youtube.com/watch?v=49NaJq2k8oE">watch</a> || <a href="/videos/2020/01/10/Lets_talk_about_what_two_sayings_can_teach_us_about_a_plane">transcript &amp; editable summary</a>)

Beau explains how attributing malice to tragedies can overlook human error and stresses the need to reduce violent rhetoric to prevent civilian suffering.

</summary>

"Civilians always suffer the most."
"We have got to tone down the rhetoric."
"Knowing who to blame doesn't matter."
"In conflict, civilians always suffer the most."
"It's something we really should take to heart."

### AI summary (High error rate! Edit errors on video page)

Explains the saying "Never attribute to malice that which can adequately be explained by stupidity" in relation to a recent incident.
Mentions the saying "Rangers lead the way" to illustrate how quick reactions can lead to mistakes in conflict situations.
Points out that conflicts have far-reaching impacts beyond just the immediate situation.
Emphasizes that blaming others for tragedies is not the solution as everyone plays a role in supporting violence.
Recalls the downing of Flight 655 from Iran in 1988, showing how similar tragedies have occurred due to violent actions.
Urges for a reduction in violent rhetoric and support to prevent future tragedies.
Raises awareness about the constant cycle of tragedies caused by violence and the need for a change in mindset.
Stresses the importance of recognizing the humanity of all individuals involved in conflicts to prevent further suffering.
Calls for a shift away from violence and towards peace to avoid repeating past mistakes.
Encourages reflection on the continuous tragedies caused by violence and the need for a collective change in mindset.

Actions:

for global citizens,
Tone down violent rhetoric and support (implied)
Recognize the humanity of all individuals involved in conflicts (implied)
Take steps to prevent future tragedies caused by violence (implied)
</details>
<details>
<summary>
2020-01-10: Let's talk about the great man theory.... (<a href="https://youtube.com/watch?v=8S-P1zA2H8M">watch</a> || <a href="/videos/2020/01/10/Lets_talk_about_the_great_man_theory">transcript &amp; editable summary</a>)

Beau explains the "great man theory," linking it to Trump's rise as a global social phenomenon and advocating for readiness to drive positive change towards interconnectedness.

</summary>

"Some people are just born great, they're born to lead, and they're heroes of a sort when they're born."
"The ultimate irony is that those who believe they're above it are not aware of the social phenomenon."
"We need to be ready to jumpstart the drive for more interconnectedness, wider social norms that accept more and more people."

### AI summary (High error rate! Edit errors on video page)

Explains the "great man theory" and how it may influence recent decisions.
Describes how the theory posits that certain individuals are born great leaders, destined to shape their own destiny.
Compares the theory to the concept of divine right, where kings believed they were destined to rule based on traits and genetics.
Raises questions about the theory's reliance on divine providence and a predetermined plan shaping governance.
Suggests that the theory hinges on the great person shaping the world before being influenced by it.
Points out how the current administration's interactions with world leaders and followers resonate with the great man theory.
Argues that the theory can be shattered by understanding social sciences and acknowledging social pressures.
Links the phenomenon of Trump's rise to a global social phenomenon driven by older demographics feeling unsettled by rapid changes.
Notes the irony of those who believe in their own destiny being unaware of their participation in a broader social phenomenon.
Advocates for being prepared for the end of this phenomenon to drive positive change towards interconnectedness and wider acceptance.

Actions:

for political analysts, activists,
Prepare for the end of the current phenomenon and work towards promoting interconnectedness and wider social acceptance (implied).
</details>
<details>
<summary>
2020-01-10: Let's talk about 4-D chess.... (<a href="https://youtube.com/watch?v=S5s2YZ48aBo">watch</a> || <a href="/videos/2020/01/10/Lets_talk_about_4-D_chess">transcript &amp; editable summary</a>)

Beau reveals the president's strategic 4D chess moves, manipulating his base through branding and misdirection, ultimately promising to fulfill his supporters' desires.

</summary>

"He's a brand. He's a walking meme."
"His base is pretty much in direct opposition to his actual beliefs."
"He just tricked everybody because he's smart like that, stable genius and all."
"He will 4D his chest and everybody will get what they want."
"Mexico's going to build the wall."

### AI summary (High error rate! Edit errors on video page)

Beau introduces himself, acknowledging the recent increase in subscribers and warns that despite his appearance, he is a strong supporter of the president.
He talks about how the president operates in a way that may seem contradictory but is actually playing a game of 4D chess.
Beau gives an example of how the president's statements on gun control were perceived by his base and how it ties into mental health care.
He explains the difference between red flag gun laws and mental health care plans, suggesting that the president's base has been tricked into supporting something they initially opposed.
Beau describes the president as a brand and a walking meme, whose base follows a cult of personality rather than his actual beliefs.
He concludes by implying that the president will deliver on his promises through strategic maneuvers, including tax breaks and the construction of a border wall.

Actions:

for supporters of the president,
Question the motives and strategies of political figures (implied)
</details>
<details>
<summary>
2020-01-09: Let's talk about decentralizing diplomacy.... (<a href="https://youtube.com/watch?v=pvOQhAQx3RY">watch</a> || <a href="/videos/2020/01/09/Lets_talk_about_decentralizing_diplomacy">transcript &amp; editable summary</a>)

Beau explains the changing face of diplomacy in the digital age, urging global understanding and representing nations positively online to decentralize power structures.

</summary>

"We're all diplomats now because we all have access."
"Americans don't want war. Iranians don't want war."
"If we can do that, we have successfully eliminated a power structure that the ruling elite held over us."
"Diplomacy is being decentralized and it is amazing."
"Y'all are watching trying to figure us out, good luck."

### AI summary (High error rate! Edit errors on video page)

Defines diplomacy as managing international relations by a country's representatives abroad, but notes this definition is changing.
Describes the transformation happening in the United States as the conservative movement is aging out and being replaced by younger, more liberal conservatives.
Points out that traditional systems of diplomacy are being undermined by technology and a changing world.
States that everyone is now a diplomat in the age of the internet, representing their individual nations online.
Emphasizes the importance of understanding each other and finding common ground to prevent conflicts.
Shares that people from 121 countries watched his channel in the last week, indicating a global audience.
Stresses the power of individuals connecting across borders and realizing their commonalities.
Expresses optimism about the decentralization of diplomacy due to increased access to information and communication.
Encourages online users to be mindful of representing their nations positively in their social media interactions.
Concludes by reflecting on the changing landscape of diplomacy in the digital age and wishing viewers a good night.

Actions:

for online users, global citizens,
Connect with people from different countries online to foster mutual understanding and empathy (implied).
Represent your nation positively in social media interactions to contribute to decentralized diplomacy (implied).
</details>
<details>
<summary>
2020-01-08: Let's talk about where we go from here.... (<a href="https://youtube.com/watch?v=Q33-EerGS-M">watch</a> || <a href="/videos/2020/01/08/Lets_talk_about_where_we_go_from_here">transcript &amp; editable summary</a>)

The President's lack of strategy risks dragging the US into a prolonged conflict with Iran, with limited viable options and global repercussions.

</summary>

"They chose to start it in a conventional manner to prove a point."
"The President of the United States has wasted more than a decade and a half of American foreign policy, a decade and a half of American lives."
"It's not about branding and putting a veneer on things."
"The United States could not pacify Iraq to the point where it could leave without other nation states involved."
"Try to have a good night."

### AI summary (High error rate! Edit errors on video page)

A response, not a reaction, was calculated and deliberate.
The United States faces limited and unfavorable paths forward.
The lack of key officials like Secretary of the Navy and Director of National Intelligence hinders decision-making in a crisis.
The absence of a clear plan and exit strategy from the President of the United States is concerning.
The situation with Iran could escalate to a full-scale war if not de-escalated promptly.
The option of a proportional response has its limitations and risks, including the potential for American soldiers being captured in Iran.
Going nuclear or invading Iran are disastrous options that will damage the US globally.
The US's influence is at stake, and a token response coupled with withdrawal from Iraq may be the only viable choice.
American foreign policy has been mismanaged, costing lives and resources over the years.
The conflict with Iran is not just about the US but involves other nations' troops caught in the middle.

Actions:

for concerned citizens, policymakers,
Contact elected officials to advocate for diplomatic solutions and de-escalation (implied)
Support organizations working towards peacebuilding and conflict resolution (implied)
</details>
<details>
<summary>
2020-01-08: Let's talk about my dog teaching us about "doing it from the air".... (<a href="https://youtube.com/watch?v=ayyi15PBN7o">watch</a> || <a href="/videos/2020/01/08/Lets_talk_about_my_dog_teaching_us_about_doing_it_from_the_air">transcript &amp; editable summary</a>)

Beau questions why the strategy of taking out command and control from the air hasn't been widely implemented despite its capability, using his dog as an analogy.

</summary>

"She is a literal dog of war."
"Why hadn't anybody done it? We've had the ability to do this for a very, very long time."
"They fund these groups. What does money come with? Strings, right?"
"What's a brilliant strategy, again? Take out command and control."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau uses the example of his dog, Baroness, to illustrate the concept of doing something from the air with ease and precision.
He questions why the strategy of taking out command and control from the air hasn't been widely implemented despite having the capability for a long time.
The transcript delves into historical instances, like George Bush Jr.'s presidency, where this strategy wasn't employed, raising the question of why.
Beau mentions the deterrent capabilities of Iran and how low-tech and high-tech deterrents play a significant role in international relations.
He brings up the concept of state sponsors and their influence in funding organizations with significant force projection capabilities.
The mention of General Soleimani and his successor, Connie, sheds light on the continuity of leadership and influence in such organizations.
The transcript concludes by reiterating the idea of taking out command and control as a strategic move, questioning why it hasn't been executed before.

Actions:

for policy makers, strategists, activists,
Analyze the implications of using the strategy of taking out command and control in conflicts (suggested)
Engage in informed debates and dialogues about the effectiveness and consequences of such military strategies (suggested)
</details>
<details>
<summary>
2020-01-08: Let's talk about Trump blinking and the possible outcomes.... (<a href="https://youtube.com/watch?v=NAeTxVGPOkY">watch</a> || <a href="/videos/2020/01/08/Lets_talk_about_Trump_blinking_and_the_possible_outcomes">transcript &amp; editable summary</a>)

President appears to seek de-escalation, Gina Haspel may have influenced, America's self-image weakened, diplomacy over military action emphasized, and concerns about potential proliferation raised by Beau.

</summary>

"We're not invincible."
"Hopefully, now that people are aware the deterrent exists, we can start treating Iran the way we should have the entire time."
"It doesn't matter whether or not we like them. That's the way it is."
"We're not gonna do it militarily, not without an utter disaster."
"In 15 minutes he's gonna tweet we're going to war."

### AI summary (High error rate! Edit errors on video page)

President appears to be seeking total de-escalation after mentioning turning things over to NATO and stating that the Middle East may not be needed.
Gina Haspel, also known as Bloody Gina, is likely the one who reached out to the president and made him understand the assessments.
Beau addresses whether Trump weakened America, explaining that the deterrent was already known and that the public's lack of awareness weakened our self-image.
Iran should be treated as a regional superpower, with diplomacy being the key approach rather than rhetoric about attacking.
Beau expresses concern about the potential proliferation of sponsoring organizations due to the effectiveness of deterrence.
Diplomacy and using available tools to alter the landscape are emphasized as necessary approaches, as military action could lead to disaster.
Beau believes that Trump may have inadvertently helped America by avoiding escalation, although his unpredictability leaves room for uncertainty.

Actions:

for political analysts,
Contact political representatives to advocate for diplomatic approaches to international conflicts (implied)
Educate others on the importance of understanding global power dynamics and deterrence strategies (generated)
</details>
<details>
<summary>
2020-01-07: Let's talk about my endorsements.... (<a href="https://youtube.com/watch?v=HHDx2xRh2DA">watch</a> || <a href="/videos/2020/01/07/Lets_talk_about_my_endorsements">transcript &amp; editable summary</a>)

Beau refuses to endorse politicians, warning against cults of personality and advocating for supporting policies over individuals.

</summary>

"Ideas stand and fall on their own."
"You'll never see me endorse a politician."
"My political endorsement for 2020? You and me."

### AI summary (High error rate! Edit errors on video page)

Refuses to endorse politicians due to his worldview and the danger of creating cults of personality.
Believes endorsing politicians is dangerous as it leads to blindly supporting the person, not policies.
Warns against following politicians anywhere just because of personal support.
Emphasizes the importance of ideas standing on their own merit, regardless of the source.
Points out the danger in defending policies you don't truly support because of loyalty to a candidate.
Cautions against falling into the trap of blindly supporting a candidate instead of their specific policies.
Advocates for supporting policies and ideas rather than political figures.
Rejects the idea of being ruled by politicians and encourages leading ourselves in politics.

Actions:

for voters, political activists,
Lead ourselves in politics (implied)
Support policies over individuals (implied)
</details>
<details>
<summary>
2020-01-07: Let's talk about asteroids, Australia, Mexico, and misfits.... (<a href="https://youtube.com/watch?v=8JkUEKsaBEU">watch</a> || <a href="/videos/2020/01/07/Lets_talk_about_asteroids_Australia_Mexico_and_misfits">transcript &amp; editable summary</a>)

Beau describes how real-life crises mirror asteroid movie plots, urging proactive community action to prevent escalating disasters.

</summary>

"When confronted with a global threat, we'll band together, all work together, and we will solve that problem."
"We can stop listening to the guy with the bad plan at any moment."
"We're at that point where we've got to make the decision."
"We can start banding together now to stop the problem rather than react to it."
"I put my faith in the misfits."

### AI summary (High error rate! Edit errors on video page)

Describes the familiar plot of asteroid movies where a global threat leads people to band together to solve the problem.
Draws parallels between the asteroid movies and real-life situations like the devastating fires in Australia.
Talks about the unprecedented scale and intensity of the fires in Australia, caused by climate change.
Mentions the need for help in Australia and the diverse group of firefighters coming together from around the world.
Addresses the role of the oil and coal industry in contributing to climate change and the denial surrounding its impact.
Points out the fragility of society and how natural disasters can quickly reveal it.
Emphasizes the importance of community organizing and coming together with neighbors to face challenges proactively.
Advocates for taking action now to solve problems before they escalate.
Urges people to stop waiting for a crisis to occur before banding together and making necessary changes.
Poses the choice between passively waiting for problems to worsen or actively demanding solutions and working together to prevent crises.

Actions:

for global citizens,
Organize community meetings to prepare for potential crises (implied)
Advocate for sustainable practices in your community (implied)
Support and volunteer with local firefighting efforts (exemplified)
Educate others on the impact of climate change (implied)
</details>
<details>
<summary>
2020-01-06: Let's talk about spheres of influence and international affairs.... (<a href="https://youtube.com/watch?v=ItRfOP0vaB4">watch</a> || <a href="/videos/2020/01/06/Lets_talk_about_spheres_of_influence_and_international_affairs">transcript &amp; editable summary</a>)

Beau provides a critical analysis of international affairs, discussing the shift in influence from the US to Russia under President Trump's foreign policy, urging the need for experienced diplomats to mitigate the damage done.

</summary>

"This doesn't really sound like what you believe."
"A never-ending chain of Russian influence stretching from Syria to Afghanistan."
"President Trump has one of the worst foreign policies in American history."
"The Democrats need to understand that President Trump destroyed everything that this country fought and bled for."
"This isn't anti-imperialism. It's assisting somebody else's imperialist ambitions."

### AI summary (High error rate! Edit errors on video page)

Explains the current state of international affairs focusing on spheres of influence.
Shares his perspective on how the world operates in terms of maintaining influence over smaller countries.
Compares his beliefs with the actions taken by larger countries to maintain influence.
Details the influence of larger countries in Syria, Iraq, Iran, and Afghanistan before and after Trump took office.
Describes the repercussions of current foreign policy decisions on Russian influence stretching from Syria to Afghanistan.
Criticizes President Trump's foreign policy legacy and its impact on American influence globally.
Emphasizes the need for experienced diplomats to undo the damage caused by the current administration.

Actions:

for politically engaged citizens,
Seek out and support experienced diplomats for key positions in foreign policy (implied)
Advocate for a comprehensive understanding of international relations and the Middle East and South America (implied)
</details>
<details>
<summary>
2020-01-06: Let's talk about drafting fear and reading your contract.... (<a href="https://youtube.com/watch?v=DCE6368MAB8">watch</a> || <a href="/videos/2020/01/06/Lets_talk_about_drafting_fear_and_reading_your_contract">transcript &amp; editable summary</a>)

Beau explains the process leading up to selective service reintroduction, detailing warning signs and military tactics to avoid a draft, ultimately suggesting its unlikelihood and efforts to make it palatable to the public.

</summary>

"Your strongest ally in this may be surprising. It's DOD."
"This is the point where you need to start paying attention."
"So it's not a fear that at this moment you should really have."
"I think they want to avoid it at all costs."
"Have a good night."

### AI summary (High error rate! Edit errors on video page)

Explains the process leading up to the reintroduction of selective service and military recruitment.
Focuses on the steps taken before selective service is brought back, indicating the warning signs to look out for.
Mentions that the Department of Defense (DOD) is not keen on selective service returning, as they prefer volunteer service over conscription.
Talks about tactics used by the military to avoid a draft, such as re-enlistment bonuses and retention efforts.
Describes scenarios like the back-door draft and inter-service draft, which are not official drafts but serve a similar purpose.
Mentions reserve units and the ability to involuntarily recall individuals back to service.
Explains how enlistment bonuses are used to incentivize new recruits, especially targeting low-income areas.
Talks about lowering standards for recruitment, both secretly and openly, when there is a shortage of personnel.
Indicates that selective service is unlikely in the current times, and even if it were to return, efforts will be made to make it more acceptable to the public.
Concludes by stating that the avoidance of selective service is preferred, especially under a commander-in-chief with draft-dodging allegations.

Actions:

for military-age individuals,
Stay informed on military recruitment processes (implied)
Monitor warning signs for selective service reinstatement (implied)
Advocate for transparent recruitment standards (implied)
</details>
<details>
<summary>
2020-01-05: Let's talk about how the President can win the unwinnable.... (<a href="https://youtube.com/watch?v=fG_Lb1e40I0">watch</a> || <a href="/videos/2020/01/05/Lets_talk_about_how_the_President_can_win_the_unwinnable">transcript &amp; editable summary</a>)

Beau analyzes the President's potential strategies, warns against unwinnable military approaches, and advises caution in escalating tensions with Iran.

</summary>

"He reacted instead of responded."
"They are steadfast in their ideology."
"It's really hard to read how angry they are about Soleimani."
"We don't know what their plan is because they won't tell us."
"We don't know what the President's plan is because he doesn't have one."

### AI summary (High error rate! Edit errors on video page)

Analyzing the potential strategies of the President in a critical situation.
Mentioning the unwinnable nature of various military approaches in Iraq and Iran.
Warning about the dangers of engaging in a tit-for-tat strategy with Iran.
Proposing an alternative strategy for the President to claim victory and win the 2020 election.
Expressing concern about the President's likely course of action being driven by ego rather than strategic wisdom.
Emphasizing the similarity in thinking between the US and Iran.
Advising caution in traveling due to escalating tensions.
Acknowledging the uncertainty surrounding Iran's potential actions and plans.

Actions:

for concerned citizens,
Stay informed about the situation and potential risks associated with escalating tensions (implied).
</details>
<details>
<summary>
2020-01-05: Let's talk about Trump losing two wars at once.... (<a href="https://youtube.com/watch?v=aIOHtJ37naE">watch</a> || <a href="/videos/2020/01/05/Lets_talk_about_Trump_losing_two_wars_at_once">transcript &amp; editable summary</a>)

The parliament of Iraq's resolution marks a pivotal shift as they urge the U.S. to leave, leading to implications for American influence in the region and Trump's handling of foreign affairs.

</summary>

"One decision lost two wars."
"He lost the war with Iran and Iraq."
"They managed to go back in time and take 15 years of military expenditure and waste it."

### AI summary (High error rate! Edit errors on video page)

The parliament of Iraq passed a resolution urging the United States to leave, recognizing the looming proxy war between the U.S. and Iran.
The resolution demands foreign troops to have no access to Iraqi land, waters, or air.
Trump's actions have led to losing the wars in both Iran and Iraq simultaneously.
The decision to use Iraqi airspace for an assassination further strained relations with Iraq.
This move jeopardizes the U.S.'s position and influence in the region, as Iraq now seeks to distance itself from American involvement.
Beau criticizes Trump's handling of the situation, turning years of military and diplomatic efforts into a farce.
The establishment's goal of turning Iraq into an ally has now been undermined by recent events.
Beau expresses disbelief at how Trump has managed to worsen the situation in Iraq, even beyond its original contentious beginnings.
The outcome of these actions appears to have left the U.S. in a position where it cannot achieve a favorable resolution.
The resolution passed by Iraq signifies their desire to sever ties with the U.S., indicating a significant shift in their relationship.

Actions:

for foreign policy analysts, activists,
Contact local representatives to advocate for a reevaluation of U.S. foreign policy in the Middle East (suggested).
Join organizations working towards diplomatic solutions in the region (implied).
</details>
<details>
<summary>
2020-01-04: Let's talk about updates, plans, and replacements.... (<a href="https://youtube.com/watch?v=MbLB1opzHFQ">watch</a> || <a href="/videos/2020/01/04/Lets_talk_about_updates_plans_and_replacements">transcript &amp; editable summary</a>)

Trump's gamble targeting Soleimani didn't work, leading to increased tension with Iran and uncertain future actions, including potential targeting of American civilians.

</summary>

"We have reports that there was yet another strike in addition to the one on Soleimani."
"Morally, tactically, strategically, it's all the same thing."
"Not a lot of, it's kind of dry, sorry, but anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Trump's unprecedented gamble of targeting Soleimani didn't pay off as expected.
Ismail Khani, Soleimani's replacement, has a similar ideology and background.
There has been a massive escalation in tension with no change in Iranian doctrine.
Reports suggest another strike post-Soleimani, but details are unclear.
Speculation surrounds the recent events, with nothing confirmed yet.
Beau questions if the US actions imply a state of war with Iran.
The approach to destabilize the opposition seems different from typical militant tactics.
Beau compares US and Iranian actions in international relations.
He anticipates no de-escalation despite calls from allies.
Beau predicts a potential increase in operations targeting American civilians by Iran.

Actions:

for foreign policy analysts,
Monitor developments and stay informed on the situation (implied)
Advocate for peaceful resolutions and diplomacy in international conflicts (implied)
</details>
<details>
<summary>
2020-01-03: Let's talk about us and them speaking the same language.... (<a href="https://youtube.com/watch?v=AtAyJt850Dk">watch</a> || <a href="/videos/2020/01/03/Lets_talk_about_us_and_them_speaking_the_same_language">transcript &amp; editable summary</a>)

Both the US and Iranian governments manipulate their people for war, while armchair supporters fail to acknowledge the true costs of conflict.

</summary>

"You can't underestimate your opposition. That is a fatal error."
"War isn't a spectator sport, gentlemen."
"They're both doing what they can to energize their people. They need us. We don't need them."

### AI summary (High error rate! Edit errors on video page)

Describes a storm approaching in the US and another place, with governments pumping people up for inevitable events.
Points out similarities in how both governments are treating their people and using propaganda.
Clarifies that he respects General Soleimani as a dangerous tool of Iranian foreign policy, even though he doesn't agree with him.
Warns against underestimating one's opposition, stressing the fatal consequences of doing so.
Criticizes the US administration for escalating tensions with Iran unnecessarily.
Commends the security details at the embassy for handling the situation calmly and professionally.
Expresses frustration at how the US administration's actions played into Iranian propaganda.
Criticizes those who cheer for war without considering the real consequences or volunteering to fight themselves.
Calls out the hypocrisy of politicians who support war but wouldn't personally participate in combat.
Concludes by pointing out the similarities in behavior between the US and Iranian governments and the manipulation of people for political purposes.

Actions:

for citizens, policymakers, activists,
Challenge war narratives and propaganda by engaging in critical discourse with peers (implied)
Advocate for peaceful resolutions and diplomacy in international conflicts (implied)
Support organizations working to prevent war and violence through peaceful means (implied)
</details>
<details>
<summary>
2020-01-03: Let's talk about the will of the people and a united America.... (<a href="https://youtube.com/watch?v=kfYtJQpDPWI">watch</a> || <a href="/videos/2020/01/03/Lets_talk_about_the_will_of_the_people_and_a_united_America">transcript &amp; editable summary</a>)

Exploring unity, blame, and personal responsibility while critiquing political representation and advocating for empathy and love over division.

</summary>

"77% of Americans agree on this topic."
"They're doing it because it's a base, it's a base."
"Love thy neighbor. Let's start there."
"Atheists are inadvertently following the teachings of Jesus more closely than those people that scream that they're Christian."
"The government has no business in this, none, none."

### AI summary (High error rate! Edit errors on video page)

Exploring the will of the people and unity in America, addressing blame and personal responsibility.
Analyzing the divisive portrayals by media despite widespread agreement on issues like Roe versus Wade.
Only 13% of Americans want to overturn Roe versus Wade, while 77% agree with it.
Republicans making a mistake by not truly representing the will of the people on this issue.
Criticizing politicians for catering to a small base rather than following the teachings of Jesus.
Pointing out the hypocrisy in anti-abortion stances and lack of support for policies aiding the less fortunate.
Calling out the Republican Party for clinging to outdated ideas and policies.
Emphasizing the exercise of power and government intrusion in personal matters.
Advocating for following the teachings of Jesus, promoting love and empathy over blame and division.

Actions:

for americans,
Follow the teachings of Jesus in promoting love and empathy in your community (implied).
Advocate for policies that benefit the less fortunate and prioritize social welfare (implied).
Challenge politicians who do not represent the true will of the people on critical issues (implied).
</details>
<details>
<summary>
2020-01-03: Let's talk about it from their point of view.... (<a href="https://youtube.com/watch?v=Ux9GI5nxMVU">watch</a> || <a href="/videos/2020/01/03/Lets_talk_about_it_from_their_point_of_view">transcript &amp; editable summary</a>)

Less than 24 hours after warning of escalating tensions, Beau delves into Soleimani's surprising background, expressing concerns over the unprecedented fallout of his assassination and the potential for dangerous escalations.

</summary>

"This is going from 0 to 120 miles an hour."
"Martyrdom is what he sought, and we gave it to him."
"I don't foresee the IRGC, specifically, or Iran in general, taking this lying down."
"Congress should not be cheerleading this on."
"It doesn't matter that General Soleimani was a bad actor; the fallout from this is going to be pretty big."

### AI summary (High error rate! Edit errors on video page)

Less than 24 hours ago, Beau warned of tensions escalating, but didn't expect the situation to reach this level so quickly.
Soleimani, viewed as a hero in Iran, had a background that surprised many, including helping the U.S. in Afghanistan before stopping due to George Bush's actions.
Soleimani, promoted to major general, worked alongside the U.S. in Iraq and Syria, credited with strategic successes in the region.
Beau believes the U.S. and Iran are more similar than people realize, both idolizing military leaders and engaging in questionable tactics for their perceived ends.
Soleimani's assassination by the U.S. is seen as a significant and unprecedented event that could lead to dangerous escalations and consequences.
Beau expresses concern that Iraq may become the battleground for a conflict between Iran and the U.S., urging Congress to intervene and prevent further escalation.

Actions:

for congress, activists, policymakers,
Contact Congress to urge them to intervene and prevent further escalation (suggested)
Stay informed about the situation in the Middle East and advocate for peaceful resolutions (exemplified)
</details>
<details>
<summary>
2020-01-02: Let's talk about white hats in Texas.... (<a href="https://youtube.com/watch?v=0dNu1qLhaKk">watch</a> || <a href="/videos/2020/01/02/Lets_talk_about_white_hats_in_Texas">transcript &amp; editable summary</a>)

Beau explains how a Texas event is being exploited to push a false narrative on gun ownership and heroism, stressing the critical role of training for safety and effectiveness in handling such situations.

</summary>

"It's a myth that training is required to use a gun effectively."
"This idea is incredibly dangerous."
"The debate in this country about whether or not people should be armed, about whether or not civilians should have access."
"If you're going to purchase a firearm, you must train."
"People with training hesitate."

### AI summary (High error rate! Edit errors on video page)

Explains how an event in Texas is being used to propagate a false idea about gun ownership and heroism.
Points out the misconception that training is not necessary to use a gun effectively and safely.
Contrasts the dangerous myth with the reality of the trained individual who stopped the incident in Texas.
Emphasizes the importance of training, planning, and preparedness in ensuring safety and saving lives.
Acknowledges the hero's responsibility and preparedness in handling the situation effectively.
Argues against the belief that simply owning a gun without training or a plan is beneficial.
Stresses the necessity of training for those who choose to be armed and the importance of having a plan in place.
Disputes the idea that the object itself (the gun) is the solution, underlining the critical role of training.
Warns about the risks of owning a firearm without proper training and preparedness.
Encourages individuals to invest in training and preparation rather than solely relying on owning a gun.

Actions:

for gun owners, activists,
Train with firearms for safety and effectiveness (implied)
</details>
<details>
<summary>
2020-01-02: Let's talk about over there.... (<a href="https://youtube.com/watch?v=OdVzibg-izg">watch</a> || <a href="/videos/2020/01/02/Lets_talk_about_over_there">transcript &amp; editable summary</a>)

Beau dives into recent events, analyzes Trump's response, speculates on Iranian influence, praises embassy security, and warns of escalating tensions with Iran.

</summary>

"The embassy didn't fall because Iran didn't want it to."
"The security detail at that embassy, they're the winners, they're the heroes here."
"We need to be very cognizant of it."

### AI summary (High error rate! Edit errors on video page)

Talking about a recent event involving strikes and protests in a particular area.
Emphasizing the lack of political gain for any side in the event.
Exploring the security concerns and the concept of the Green Zone.
Addressing the response from Trump as relatively average in the context of his foreign policy decisions.
Speculating on Iranian influence based on attempts to contact experts on Iran.
Describing the chain of events leading to the protest after US strikes.
Comparing the strikes to current counter-terrorism strategy and questioning its effectiveness.
Rejecting the idea of this event being Trump's Benghazi and explaining why.
Praising the security detail at the embassy for handling the situation effectively and avoiding escalation.
Predicting more tensions between the US and Iran, potentially leading to heightened conflict before the election.

Actions:

for foreign policy observers,
Stay informed on the situation and developments (suggested)
Monitor geopolitical tensions and escalations (suggested)
Advocate for peaceful resolutions and diplomacy (implied)
</details>
