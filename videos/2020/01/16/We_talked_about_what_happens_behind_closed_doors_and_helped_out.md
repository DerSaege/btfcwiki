---
title: We talked about what happens behind closed doors and helped out....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hZ-UW3u_Hw4) |
| Published | 2020/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Personal connections drive involvement in the fight against intimate partner violence.
- Domestic violence affects everyone regardless of race, gender, or class.
- Intimate partner violence accounts for 15% of all violent crime in the U.S.
- Half of women killed in the U.S. are killed by a current or former intimate partner.
- Financial control by abusers makes it hard for victims to leave.
- Leaving an abusive relationship is the most dangerous time for victims.
- Domestic violence shelters provide a safe haven for those leaving abusive relationships.
- Support organizations like Shelter House of Northwest Florida provide vital assistance to survivors.
- These organizations also help alleviate traumatic experiences, such as rape kits and fresh clothes for survivors.
- Animal shelters by these organizations help remove barriers to leaving abusive situations.
- Donations, especially old cell phones, can make a significant impact on survivors' ability to seek help and support.
- Community support and donations can help provide essentials and comfort to survivors, like Christmas presents for teens in shelters.
- PTSD triggers like scents can influence traumatic memories, suggesting potential avenues for support.
- Individual actions, even small ones, can contribute to collective goals in supporting survivors of domestic violence.

### Quotes

- "Women are 500 times more likely to be killed if they're in one of these relationships when they're just leaving."
- "Violence isn't always the answer."
- "Odds are you have an old cell phone sitting in a junk drawer, mail it to them. It means nothing to you, could save someone's life."
- "Community support and donations can help provide essentials and comfort to survivors."
- "Individual actions, even small ones, can contribute to collective goals in supporting survivors of domestic violence."

### Oneliner

Beau sheds light on the pervasive nature of intimate partner violence, the challenges faced by survivors in leaving abusive relationships, and the vital role of community support and organizations in providing safety and assistance.

### Audience

Supporters of survivors

### On-the-ground actions from transcript

- Donate money or old cell phones to organizations like Shelter House of Northwest Florida (suggested)
- Support organizations that provide essentials and comfort to survivors (exemplified)
- Volunteer time or resources to assist domestic violence shelters (implied)

### Whats missing in summary

The full transcript provides a comprehensive perspective on intimate partner violence, survivor challenges, community support, and practical ways individuals can contribute to supporting survivors.

### Tags

#IntimatePartnerViolence #CommunitySupport #DomesticViolence #Survivors #SupportActions


## Transcript
A lot of the people who get involved in this fight get involved in it for personal reasons.
I got involved because my best friend in high school, Amber, was killed by her husband.
Domestic violence affects everybody, every race, every sex, every gender, everybody.
It affects everybody, every class.
What do we call it?
Intimate partner violence, I think, is the current term.
You may not think that this is a big problem in the U.S., but it is.
15% of all violent crime is intimate partner violence.
Half of all women who are killed in the U.S. are killed by a current or former intimate partner.
Those numbers, they're in the thousands.
I think 1,600 to 2,400 were the, was the range each year.
That's pretty significant.
from 2003 to 2008, 78% of the women killed at work
were murdered by a former or current intimate partner.
Three quarters, think about all the ways
you can get murdered at work,
three quarters of them were killed
because of domestic violence.
And then the stats for murder-suicide.
Don't remember them exactly,
but I wanna say it's 80 to 90%.
Thinking that maybe using
A sanitized terminology for this isn't the right thing to do.
Why can't women just leave?
It's not as simple as just leaving.
Women are 500 times more likely to be killed if they're in one of these relationships when
they're just leaving.
Why can't they go to a friend's house?
Because the abused knows the abuser is violent.
You're not going to visit that on your extended family or friends.
He's going to come looking for you.
Twenty percent of those people killed in murder-suicides are a family member or friend of someone suffering
intimate partner violence.
If you're going to leave, you need money.
And the abuser typically controls the finances.
It's part of the pattern.
The economic considerations come into play.
to deal with the stigma of being on food stamps. You may have kids and that makes finding a place
to go harder. Then you have the social conditioning that goes along with it as far as stigmas.
You're going to be a single mom. You may have pets. You can't leave the pet because the abuser is
going to kill it. A lot of women have been trained. You got to fix your man. You need to stay and fix
it. Then you can have the fairy tale. No fairy tale has a black eye in it.
So tonight we're going to talk about honor killings.
You don't know what one of these is.
It's when a woman does something that somehow disgraces the man, typically infidelity.
So he takes a club, beats her to death, and then throws her body in a well.
Right now, everybody's thinking of Afghanistan or some other place that ends in a stand.
That story is about Tanya Lynn, happened in Georgia.
The horrifying thing about that case is that the Supreme Court of Georgia gave him a retrial,
gave the murderer a retrial on the grounds that the trial judge did not allow enough
testimony about her alleged infidelity. The Supreme Court of Georgia believed
that that might have altered the verdict. Make no mistake, the fact that he killed
her was not in dispute. The fact that he dumped her body was not in dispute. Just
that it might have been okay. Might have been less of a crime because at some
point in the past she had slept with someone else. If that's not an honor
killing I don't know what is honor killings and it all stems from the same
idea that the property disobeyed the owner. It's what it is. That is what it is.
That's where it comes from. We need to call it what it is because this is toxic
masculinity and that's the problem. That's the problem. The problem is that
that fragility, that insecurity in yourself is violent. That's why this is
such an issue. That's why this hyper masculinity, this false masculinity is
such a worry in modern society because it breeds violence. So the message is the
The story is there's a husband and wife, husband has a substance abuse issue, came home, beat
up the wife.
Her brother sent the message.
The husband is currently in jail.
The question was basically, should I put boots on this guy when he gets out?
Should I beat him up?
If you're not going to kill him and you're going to use violence, what are you going
to do?
a little bit, right? These guys have fragile little egos. That ego is going to take a beating
too. Who's he going to take it out on? It's the opposite of protection, the male protector.
The problem is we've begun to associate protection with violence. It's not necessarily the case.
that society has said and conditioned people to believe is the wrong thing.
You know, you've been socially conditioned to use violence to solve problems, and this
isn't just the person that sent the message, this is a lot of people.
And I know there are people out there that think I'm a pacifist of some sort.
I heard that the other day and it kind of surprised me.
I'm not a pacifist.
I've just seen enough violence to know that violence isn't always the answer.
It's not a bad thing to not be violent.
That's a good thing.
In this situation, if protection is the goal, what's the option?
Get her out.
When somebody's leaving an abusive relationship, that's the single most dangerous time.
That's when things can really spin out of control.
are 500 times more likely to be killed if they're in one of these relationships when
they're just leaving.
These organizations, these domestic violence shelters, they facilitate that.
They lower that risk.
So a lot of these networks, they operate like safe house networks.
Woman makes contact, the abused makes contact, they roll out, pick them up, take them to
secure safe house, secure them, and then let them restart their life because in a lot of ways it's
a complete reset and that's one of the reasons I support them the way I do. These shelters allow
people to go to them and they're really like safe houses. It's very hard to find out
where the people are. One of the organizations that I am familiar with and that I support is
is the shelter house of Northwest Florida.
They're extremely forward-thinking.
They also make these kits that are put at hospitals
for rape victims.
Inside the kit is, there's a few things,
but to me, the most important is there's fresh clothes.
You know, if you go to a hospital
after being sexually assaulted and undergo a rape kit,
now you're in a gown.
You don't want to put those other clothes back on.
And these kits can help take a traumatic experience
that gets prolonged by the medical procedures
and help alleviate that.
They also have an animal shelter,
because one of the reasons that people don't leave
when they're in a situation like that is,
well, they're worried that the abuser
will do something to the animal.
So, Shelter House, we'll get rid of that reason,
and they started an animal shelter too.
There's gonna be a link on whatever social media platform
you're seeing this on, either in the description
or in the comments, there will be a link.
In that, you'll find a way to donate to confit you,
whether it be money, which of course they need,
or an old cell phone.
You know, if the abuser controls the finances,
That cell phone gets cut off.
No communication, can make it hard to get a job, can make it hard to contact those people
who do care about you.
It's something that can be done.
Odds are you have an old cell phone sitting in a junk drawer, mail it to them.
It means nothing to you, could save someone's life.
And then there's methods to sponsor a family, there's all kinds of things, there's certainly
something for you.
You don't know we did a live stream on YouTube and all the money that was donated, sent in,
was earmarked for a specific purpose.
That specific purpose, these five bags back here.
We wanted to get Christmas presents for some teens that are in a bad way.
They're in a shelter.
So each one of these bags has a tablet, a Netflix card, a case for the tablet, earbuds,
and a board game of some kind.
This gives them a space of their own, a place that they can kind of retreat to to process
what's going on.
We accomplished that goal pretty quickly because a whole bunch of people came together.
People got in the comments, in the chat, commenting, increased interaction, brought more people
in, people donated, everybody, whether you sent money or not, donated your time and it
made this happen.
While I was buying this stuff, I started wondering why I had to.
One of the things you learn about PTSD is that scent is a very, very strong trigger
of memory.
If a company has a new scent, it seems like the ideal place to scent it, to get it out
on the market.
Not just, or most of these places are charitable institutions, so it would be tax deductible.
It's advertising, you're doing something good, and you're building brand loyalty.
They're not, because that's the world we live in.
At the same time, we also live in a world with the internet people.
did this in about an hour. It shows the value of individual action in pursuit of a collective
goal and I can't thank you enough. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}