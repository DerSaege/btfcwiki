---
title: Let's talk about Trump losing two wars at once....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aIOHtJ37naE) |
| Published | 2020/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The parliament of Iraq passed a resolution urging the United States to leave, recognizing the looming proxy war between the U.S. and Iran.
- The resolution demands foreign troops to have no access to Iraqi land, waters, or air.
- Trump's actions have led to losing the wars in both Iran and Iraq simultaneously.
- The decision to use Iraqi airspace for an assassination further strained relations with Iraq.
- This move jeopardizes the U.S.'s position and influence in the region, as Iraq now seeks to distance itself from American involvement.
- Beau criticizes Trump's handling of the situation, turning years of military and diplomatic efforts into a farce.
- The establishment's goal of turning Iraq into an ally has now been undermined by recent events.
- Beau expresses disbelief at how Trump has managed to worsen the situation in Iraq, even beyond its original contentious beginnings.
- The outcome of these actions appears to have left the U.S. in a position where it cannot achieve a favorable resolution.
- The resolution passed by Iraq signifies their desire to sever ties with the U.S., indicating a significant shift in their relationship.

### Quotes

- "One decision lost two wars."
- "He lost the war with Iran and Iraq."
- "They managed to go back in time and take 15 years of military expenditure and waste it."

### Oneliner

The parliament of Iraq's resolution marks a pivotal shift as they urge the U.S. to leave, leading to implications for American influence in the region and Trump's handling of foreign affairs.

### Audience

Foreign policy analysts, Activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for a reevaluation of U.S. foreign policy in the Middle East (suggested).
- Join organizations working towards diplomatic solutions in the region (implied).

### Whats missing in summary

Insights on the potential consequences for regional stability and future U.S. diplomatic efforts in the Middle East.

### Tags

#Iraq #US #ForeignPolicy #Trump #ProxyWar


## Transcript
Well, howdy there internet people, it's Bo again.
So as suggested might happen last night, the parliament of Iraq passed a resolution telling
the United States to get out.
These resolutions are non-binding, but the prime minister asked for it.
So they did see the writing on the wall, realized that they were about to become the battleground
for the proxy war between the United States and Iran, and they want no part of it.
The resolution states that foreign troops should have no access to their land, waters, or air.
What that means in English is that Trump managed to lose the war with Iran
and the Iraq war at the same time.
It doesn't matter what happens with Iran now.
they have more influence over Iraq and everything after December of 2003 when
Saddam was caught, everything in Iraq was about turning Iraq into an ally and now
not just do they not want to us there they don't want to even let us use their
airspace. One decision lost two wars. Well at least he finally has better
numbers than Obama.
I don't know that any president's ever done that.
So that's, that's where we're at.
He managed to turn more than a decade and a half of endless struggle for what I
believe to be a ridiculous reason, um, into a complete farce.
I did not believe that any president, even Trump, could make the Iraq war worse.
And I was wrong.
I was wrong.
Yeah, it was all a waste.
It's all for nothing.
Even understand the war itself was on false pretenses and all of that stuff, right?
Take all of that away.
The establishment viewpoint was to go long-term and turn it into an ally, turn the country of Iraq into an ally.
That was the idea. That's why we spent all of this time there by the establishment theory.
Now even that is a failure because of one decision. He lost the war with Iran and Iraq.
It doesn't matter what happens now, it doesn't matter if he launches a dozen
airstrikes against Tehran, levels Tehran.
It doesn't matter.
They won.
They managed to go back in time and take 15 years of military expenditure and waste it.
They won.
There is no way for Trump to win this now.
Now, even if Iraq doesn't actually force the issue and push the United States out, they're
not our friend, they're not our ally, and they're never going to be.
That resolution passed.
They want us gone.
They don't want us to have access to their airspace.
We're not going to be friends.
Why?
Because we abused that access.
used that access to their airspace to commit an assassination on a foreign
national who incidentally we now know was traveling to meet the Iraqi prime
minister. So yeah there we go making America great again one day at a time I
guess. So I'm gonna go play Legos with my kids or something. Build something.
Hopefully they won't destroy it. Like children often do when they react rather
than respond. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}