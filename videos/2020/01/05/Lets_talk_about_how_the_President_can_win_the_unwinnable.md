---
title: Let's talk about how the President can win the unwinnable....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fG_Lb1e40I0) |
| Published | 2020/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the potential strategies of the President in a critical situation.
- Mentioning the unwinnable nature of various military approaches in Iraq and Iran.
- Warning about the dangers of engaging in a tit-for-tat strategy with Iran.
- Proposing an alternative strategy for the President to claim victory and win the 2020 election.
- Expressing concern about the President's likely course of action being driven by ego rather than strategic wisdom.
- Emphasizing the similarity in thinking between the US and Iran.
- Advising caution in traveling due to escalating tensions.
- Acknowledging the uncertainty surrounding Iran's potential actions and plans.

### Quotes

- "He reacted instead of responded."
- "They are steadfast in their ideology."
- "It's really hard to read how angry they are about Soleimani."
- "We don't know what their plan is because they won't tell us."
- "We don't know what the President's plan is because he doesn't have one."

### Oneliner

Beau analyzes the President's potential strategies, warns against unwinnable military approaches, and advises caution in escalating tensions with Iran.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Stay informed about the situation and potential risks associated with escalating tensions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the geopolitical situation, outlining potential risks and advising against harmful strategies in dealing with Iran.

### Tags

#Geopolitics #USForeignPolicy #Iran #Election2020 #MilitaryStrategy


## Transcript
Well howdy there internet people, it's Bill again.
Hopefully this isn't gonna become an all the time thing
because I have some other stuff I'd like to talk about,
but we are going to talk about it over there again.
We're gonna talk about it because the president
has bumbled his way into a golden moment
for himself and his re-election campaign.
If he's smart, he can win 2020 in a landslide.
So tonight we're gonna talk about
could do if he was smart. We're going to talk about what he's probably going to do. We're going
to talk about reactions, responses, victory conditions, and the unwinnable. I'm also going
to give a little travel advisory. Since that last video, pretty much everything contained in that
video has come to pass, or concrete signs have appeared showing that they will come to pass.
The most important of which is the decision by Iran to use rockets in Iraq.
That was kind of a signal from them saying, yep, you're right.
Iraq is the battleground for the proxy war.
People are talking about that saying, ah, look, it was completely ineffective.
Iran is not the United States.
They don't do shock and awe.
They degrade and demoralize.
That's the first of many.
That is the first of many.
Now because the president has so few options, what he's going to want to do is do tit for
tat and go back and forth with them, which is basically playing into their hands.
That's how they roll.
That's what they plan on.
That's what they count on.
They don't care.
They'll succeed.
They'll win that way.
Using that strategy is unwinnable.
His Twitter threat, despite being
illegal, that whole targeting of locations
of cultural significance, aside from that,
it's playing into their hands.
They have the initiative in that case.
That's an unwinnable strategy.
Fighting the proxy war in a more sustained fashion in Iraq is unwinnable.
We've got more than a decade and a half of animosity directed towards the United States there.
The local populace will not be on our side, and in very short order, the Iraqi government will ask us to leave if it
gets hot.  So that gets seen as an Iranian victory. More importantly, what are the victory
conditions? To make Iran stop? They won't. There's no way to win it that way. It's
unwinnable. Moving into Iran, going directly there, into the line's den, is
unwinnable. Our own war games show that. People are talking a whole lot in various
comment sections about how low-tech they are and how it'll be over super-fast
and all that yellow ribbon and flag waving stuff.
The reality is in our own war games we lose.
Maybe if we were willing to commit a quarter of a century
to attempting to pacify it, maybe, but probably not.
Probably end up like Iraq or Afghanistan.
So that's also unwinnable.
And Iran has force projection capabilities
in a very different way.
The US has the ability to put its forces wherever it wants,
anywhere in the world, in 24 hours.
Iran has proxy forces everywhere.
Proxy forces everywhere.
So it's going to be real hard to predict them because it's multiple networks, multiple
chains.
Our intelligence isn't that good.
Now a travel advisory, while we're on this topic, a travel advisory that should have
gone out that I have not seen is that if you are an American citizen, or if you are a Westerner,
that could be mistaken for American citizen.
away from any property associated with the Trump brand.
Because just as the president wants to attack culturally significant targets, symbolic targets,
what do you think would really send a message from their side?
As we've said before, we speak the same language, we think the same way.
The U.S. and Iran are a whole lot alike.
I think this is horrible.
I think the president's threat was horrible.
I don't support it in any way, shape, or form
because I remember what it was like when it was our buildings
of cultural significance that came tumbling down.
So he can't win, tip for tat.
Can't win on the ground in Iraq.
Can't win in Iran.
He's in an unwinnable situation because he reacted instead
of responded.
So what's his golden moment?
What can he do?
He can lose in his own way.
He can choose the victory conditions in hindsight.
One of the good things about having no plan is that nobody can tell you that your plan
was wrong.
He can walk out tomorrow and say, our plan was to eliminate Iran's stranglehold on Iraq.
We feel we have done that with the elimination of Soleimani.
We're leaving and pull all of our troops home.
Tick or tape parades, the whole nine yards.
We won.
It's over.
God Emperor Trump, 2020, and he'd win.
You know, I know, the military establishment knows that that's not what happened, that
got beat, but his base, they only believe what he says.
He would win in a landslide.
But he's probably not going to do that.
He's probably not going to do that, and it's sad because it's the best thing for him, it's
It's the best thing for our troops and it's the best thing for our foreign policy.
But he won't do it because his ego will be in the way.
What he's probably going to do is engage in the tit for tat strategy because I'm sure
he understands that politically sending a whole bunch of troops to Iraq is just not
... it's going to hurt him in the polls and that's what he cares about, the American lives,
poll numbers. I'm certain his advisors will tell him that going into Iran is a
horrible idea and he will also understand that that will drag on and it
will hurt him in the polls. The tit-for-tat thing, I don't think that he
believes we can lose. I think he believes that he can out escalate them, cause more
damage. But he's thinking like an American. Iran doesn't care. That's not how they look
at it. It's not about who can inflict the most, who wins. It's who can take the most.
They are steadfast in their ideology. In fact, every attack will make them cling to it more.
So that will be a losing proposition for him, but I think that's the route he's going to take,
and that's what we need to be ready for. I would be careful traveling. I don't believe that Iran
or any of their proxies will make a hit against civilians in the United States. I don't foresee
that happening yet, the longer this drags on, it becomes more and more likely.
But again, I could be wrong.
It's really hard to read how angry they are about Soleimani.
They're unlike our president, they understand what operational security is,
and they're not really telegraphing very much.
It's rhetoric that is very similar to what they've said for a very long time.
So, we don't know.
There's a whole lot we don't know.
We don't know what their plan is because they won't tell us.
We don't know what the President's plan is because he doesn't have one.
Anyway, it's just a thought.
y'all have a good night!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}