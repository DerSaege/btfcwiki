---
title: Let's talk about the pursuit of knowledge and the status quo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9032goyLAps) |
| Published | 2020/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the fear of science and the historical example of King Henry IV's law against multipliers in 1404.
- Multipliers were alchemists who claimed to turn base metals into gold, causing concern among the rich and powerful.
- The law was passed to prevent the potential destabilization of the status quo if alchemy actually worked.
- Laws and regulations aimed at science are often driven by those in power fearing the unknown and potential disruptions to their control.
- Innovation has been stifled throughout history due to regulations designed to slow the pursuit of knowledge until legislators understand it.
- Legislators tend to learn slowly, leading to a slow acceptance of change in established systems.
- Breakthroughs have often occurred through less-than-legal means due to research being blocked by regulations.
- Regulations and prohibitions on new technologies or fields of study tend to persist once established, simply because "that's how it is."
- The law against multipliers was eventually repealed after prominent names in science, like Isaac Newton, spoke out against it.
- Beau encourages questioning authorities who discourage certain technologies or fields of study, as it may be a tactic to maintain their power.

### Quotes

- "When we have our betters telling us that certain technologies or certain fields of study aren't worth pursuing, we should always ask why."
- "A lot of laws and regulations that are aimed at science, that's what it's about."
- "It's about those in power, the establishment, the status quo, not really understanding it and therefore being afraid of it."
- "The establishment is slow to change."
- "It's just them attempting to preserve their place in the world above."

### Oneliner

Beau explains historical fears of science and regulations against innovation driven by those in power fearing disruptions to their control.

### Audience

Science enthusiasts, advocates for innovation.

### On-the-ground actions from transcript

- Advocate for increased support and funding for scientific research (implied).
- Challenge authority figures or institutions discouraging exploration of certain fields of study (implied).

### Whats missing in summary

The full transcript provides a detailed historical context and analysis of the fear of science, showcasing how regulations can inhibit innovation and progress. It delves into the motivations behind these regulations and the importance of questioning authority in such matters.

### Tags

#Science #Innovation #Regulations #History #Power #QuestioningAuthority


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about the fear of science
and what multiplication can teach us about it
and why the establishment in general tends to fear science.
On this day, January 13th, in 1404, King Henry IV signed into law the act against multipliers.
He didn't have any problem with mathematicians.
Multipliers were alchemists, people that could turn base metals into gold.
In theory, this was a field of study at the time.
The law was passed for two reasons.
The first being that rich folk who were friends with the government, that's how you got rich
back then, well some of them were getting duped, they were getting tricked.
The alchemist, the multiplier would show up and say, hey, give me some gold and I'll double
it for you.
And at first, these are smart people, they would only give them like five pieces.
And sure enough, the guy would come back with ten and give it to them.
And then, well, they'd do it with a larger amount, and that person would just disappear
with their money.
That was one reason.
That was a slight annoyance.
But there was a bigger reason.
They were worried that it would work.
At some point, somebody would really figure it out, and that that would have a destabilizing
effect on the status quo, just like today, money was power.
And if somebody was able to literally create money out of thin air, well,
they would have unlimited power.
A lot of laws and regulations that are aimed at science, that's what it's about.
It's about those in power, the establishment, the status quo, not really understanding it
and therefore being afraid of it and being very worried that it's going to upset the
status quo.
A lot of innovation has been stifled over the years because of regulations like this.
of legislators attempting to slow the pursuit of knowledge until they can
catch up and understand it. And as we've seen, legislators as a species tend to
learn slowly. The establishment is slow to change. That's why a lot of
breakthroughs that occur you find out that they used well less than legal
means to do it because a lot of times they're stifled they're blocked research
gets blocked and this still goes on today different different things
obviously but once a regulation or a prohibition to get some kind of new
technology or field of study gets put into place, it stays in place a long time
because it just becomes part of the establishment. This is how it is and
that's the worst reason to keep doing something. We've always done it this way
but it doesn't stop people from falling into that trap. This law was on the books
until some of the biggest names in science, to include Isaac Newton, spoke
out against it.
When we have our betters telling us that certain technologies or certain fields of study aren't
worth pursuing, we should always ask why.
Because many times, it's just them attempting to preserve their place in the world above
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}