---
title: Let's talk about Casablanca and perception....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BiynNSGYYD0) |
| Published | 2020/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Casablanca is perceived as one of the greatest films ever made, even by those who have never seen it.
- The most memorable quote from Casablanca, "Play it again, Sam," is not actually in the movie.
- In January 1943, Roosevelt, Churchill, and de Gaulle met in Casablanca to talk about the situation in Europe.
- Roosevelt issued a proclamation demanding unconditional surrender, but British intelligence was already working on deals behind the scenes.
- The proclamation was made for the benefit of the Soviet guy who couldn't attend the meeting.
- Perception often outweighs reality, as demonstrated in historical events like these.
- The perception of events can shape outcomes and have significant impacts.
- The importance of managing perception applies to contemporary issues as well.
- The recent events have raised questions about the true intentions of the United States in various global locations.
- The actions and decisions of the current administration are viewed through a transactional lens, impacting perceptions globally.

### Quotes

- "Perception is often more important than what actually happened."
- "The perception is what saves lives or costs them."
- "The perception of the last couple of weeks from outside the United States, it's not good, it's not good."

### Oneliner

Casablanca's perception versus reality lesson applies to contemporary global events, where managing perceptions can be a matter of life and death.

### Audience

Global citizens

### On-the-ground actions from transcript

- Question perceptions and seek the truth behind official narratives (suggested)
- Advocate for transparent and ethical decision-making in international relations (implied)

### Whats missing in summary

The full transcript delves into the importance of managing perceptions in historical and current events, urging critical thinking and awareness of the impact of public perception on global dynamics.

### Tags

#Perception #Reality #GlobalRelations #Ethics #CriticalThinking


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna talk about Casablanca
and perception.
Casablanca is widely perceived as one of the greatest films
ever made.
And this perception is so ingrained in American culture
that people who have never seen it will tell you
It is their favorite film.
Fact.
It's viewed as something that had a huge impact
on American society.
It's the perception.
So much so that its most memorable quote,
play it again, Sam, it's not in the movie.
That's not from the movie.
Perception.
sometimes the perception is more important
than the reality.
Now of course we're not actually going to talk about the film.
We are going to talk about Casablanca though.
January 14th, tomorrow,
1943.
Roosevelt, Churchill, and de Gaulle sit down at a hotel
in Casablanca.
And they're talking about the situation in Europe.
Now, the Soviet guy, he was supposed to be there,
but he was a little busy at the time.
He was kind of tied up, so he couldn't make it.
They issued a proclamation, a demand,
mainly coming from Roosevelt.
And this bears out in the fact that he used the language of grant.
And it was a proclamation saying that we would only
accept unconditional surrender.
no deals to be made." Tough guy line right there. And that's cool, but what's the reality?
That's the perception, that's what went out, that's what was in the news rules. The reality
is British intelligence already, already working out a deal. Had guys within the government
the guy with the mustache had already made contact with some of them and they
were like you know get rid of them get rid of that guy get rid of the mustache
and we'll deal with this we can work out a deal the boss of our OSS our
intelligence at the time he referred to it as a piece of paper to be scrapped
without much ado the second Germany wanted peace. See, back then intelligence
actually worked, you know, under direct supervision of the president, prime
minister. So why was this proclamation made? It wasn't true in any sense of the
word. So why was it made? For the benefit of the guy who wasn't there. See, as
Americans, and to a lesser degree those in the UK, we did that. It was Audie Murphy,
General Patton, and Montgomery. Those three guys took care of the whole thing.
The reality is we really needed that Eastern Front because if that wasn't
there, all of those resources could be turned our way. So we wanted to kind of
lock them in and say don't make a separate peace deal, even though we were
trying to or willing to. We wanted to make sure that front stayed open
because our long-range goal was to take the Germans and just spin them around
and use them to stop the Soviet advance. Perception is often more important than
what actually happened. So let's take that lesson and apply it to today. Yeah the
perception is we left our friends a couple months ago and in that case the
The perception is the reality, that is really what happened.
But we knew how that was going to be perceived all over the world.
And we talked about what was likely to happen, and it happened this week.
You know they're going to leave you too, right?
We created that by allowing that perception and reality.
What about the events of recently?
Us being the unwanted house guest again?
Nation saying you need to leave and us saying no?
Sure, it doesn't matter, just this one country, but what's the perception of all of those
other places we're guests?
Are we really there to help them?
Or are those bases really just outposts of the empire?
Has that perception shifted because of what we did?
I would guess that it has.
This is the problem with this administration.
He's a businessman.
He is a businessman.
He views things in a transactional nature.
The problem is, that's not the way everything works.
These things don't occur in a vacuum.
The perception is what's important.
The perception is what saves lives or costs them.
The perception of the last couple of weeks from outside the United States, it's not
good, it's not good.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}