---
title: Let's talk about wallabies and the good in people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qLo0hkAuAyE) |
| Published | 2020/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the positive side of humanity coming together during challenging times, like after Hurricane Michael when everyone on the coast united to help each other.
- He mentions the incredible diversity of people who banded together regardless of their backgrounds.
- The spirit of cooperation and support usually lasts about six months after a crisis before fading away.
- Beau expresses a desire to understand why this unity fades and hopes to find a way to sustain it without the need for constant existential threats.
- Shifting gears, Beau shares the heartwarming story of people in Australia helping wallabies by airdropping thousands of kilograms of sweet potatoes and carrots to their colonies affected by fires.
- He admires the willingness of people to go above and beyond to create a just world, even risking their lives by flying over fires to aid the wallabies.
- Beau expresses a wish for the same level of care and support to be extended to humans as well, reflecting on the need for such compassion to continue beyond natural disasters.

### Quotes

- "The spirit that exists after things go bad, it's the best that humanity has to offer."
- "Most people want a just world and they're willing to work for it."
- "I just can't wait until we do the same thing for people."

### Oneliner

Humanity's best emerges in unity during crises, but sustaining that spirit beyond disasters remains a challenge.

### Audience

Community members

### On-the-ground actions from transcript

- Support relief efforts in your community (exemplified)
- Volunteer to aid those affected by disasters (exemplified)

### Whats missing in summary

The full transcript provides a deeper insight into the temporary but powerful unity that arises in communities during crises and the importance of extending that same level of care and support to people beyond immediate emergencies.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about wallabies, eventually,
and the good in humanity and how it comes out at the worst
times, after being a part of just way too many relief
efforts.
One of the things that I've realized and one of the things
I've kind of seen as a redeeming quality when things go bad, is how people pull together.
And I mean really pull together.
After Michael here, everybody pulled together.
And I mean everybody, and I mean the entire coast.
Because we knew help wasn't coming.
We'd seen what had happened elsewhere.
We knew we were on our own.
And it was great.
It was just a fantastic feeling.
The entire coast, and I mean everybody, just an impossibly diverse group of people, ethnic,
religious, ideological, everybody banded together and did everything that they could for everybody
that they could.
Was fantastic.
And this phenomenon, I've seen it play out over and over again, it lasts about six months.
After that it all fades.
And it doesn't matter how many times I've done it, I never understand why it stops.
I just don't get it.
I can't wait until we can isolate and figure out why it does stop.
Because that spirit that exists after things go bad, it's the best that humanity has
to offer.
But we can't have an existential threat all the time, we'll wear out.
So if there was just some way to master that cooperation and keep it going it would be
fantastic.
Now on to the wallabies.
In Australia right now they've got fires, a lot of them.
It's really bad.
And the species wallabies, they're, if you don't know, they're like little kangaroos,
little marsupial guys.
The species isn't doing well to begin with.
And the fires are certainly going to exacerbate that problem.
So to help them, people are literally loading thousands of kilograms of sweet potatoes and
carrots into helicopters and then dumping them out over their colonies.
They live in little groups.
It's just amazing to me.
It's one of those moments that you can see it.
You can see that most people want a just world and they're willing to work for it.
They are willing to work for it.
They're willing to fly over fire to do it.
Tell kangaroos, well wallabies.
And I don't want to take away from that when I say this because it's wonderful.
It definitely needs to happen.
isn't an either or. What is happening should definitely continue to happen. I just can't
wait until we do the same thing for people. Anyway, it's just a thought. Y'all have a
good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}