---
date: 2023-05-03 03:47:23.909000+00:00
dateCreated: 2023-02-16 01:51:35.772000+00:00
description: Comparison of how the Y2K problem was handled and how it is viewed today, and how that compares to how we are handling climate change, and how people of the future will see what we did about climate change.
editor: markdown
published: true
tags: climate change, computers, future
title: Let's talk about what Y2K can teach us about climate change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9elHSOMvn_M) |
| Published | 2020/01/30|
| Theme     | Climate Change |
| Status    | article incomplete |

## Human summary

Beau compares how the Y2K problem was handled and how it is viewed today, and how that compares to how we are handling climate change, and how people of the future will see what we did about climate change.

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Y2K was a computer crisis 20 years ago due to shortened year dates causing potential issues in 2000.
- The fear of Y2K was real as computers could have thought it was 1900 instead of 2000, impacting critical infrastructure.
- Despite spending almost half a trillion dollars on fixing the Y2K problem, some issues persist due to short-term solutions.
- Beau draws parallels between the Y2K crisis and climate change, where temporary fixes don't address the root problem.
- He warns that time is running out for climate change action, with substantial impacts already visible.
- Beau points out the trust placed in politicians over scientists, urging a reconsideration of this choice for the future.
- Dealing with climate change will be costly, but history shows significant investments can prevent major issues.
- The hope is for future generations to view the handling of climate change as an overblown concern, similar to Y2K.

### Quotes

- "Dealing with climate change is going to be expensive. Fact."
- "We spent almost half a trillion dollars to deal with a date so it wouldn't become an issue."
- "Our failures are known and our successes are not."
- "It's not a hoax. It's real."
- "The alternative is that people know we failed."

### Oneliner

Y2K crisis parallels climate change urgency, urging action before it's too late and history repeats costly mistakes.

### Audience

Climate activists, policymakers

### On-the-ground actions from transcript

- Invest in sustainable solutions for climate change (suggested)
- Advocate for scientific truth in policymaking (suggested)
- Support initiatives addressing climate change impacts (suggested)

### Whats missing in summary

Importance of learning from past crisis responses to prevent repeating costly mistakes in addressing current global challenges.

### Tags

#Y2K #ClimateChange #CrisisManagement #Urgency #CommunityAction


## Transcript
Well, howdy there internet people, It's Beau again.

So tonight we're going to talk about Y2K.

If you don't remember or aren't old enough yet, 20 years ago, that was the thing that was just going to get us all.

See, what happened when computers first came around, they didn't have a lot of memory. So programmers looked for a way to cut corners any way they could. One of those ways was shortening the year in the date to two digits, 98 instead of 1998.

Wasn't a big issue at the time. Then somebody realized that when it rolled around to 2000, the computers would think it was 1900, and that was going to cause issues.

As an example, I want you to imagine opening a savings account in 1998. Two years later, the computer would be compiling interest on that account from 1900. That's a good outcome for most people, except for the banks. The downside to that would be a prescription that expired it was written.

The worry was that these would have kind of a domino effect and eventually impact critical infrastructure and they talked about it for years, doom and gloom all the time.

2000 rolled around and nothing happened and everybody kind of thought it was overblown. People think it wasn't a big deal and that's when you run into one of the big paradoxes of crisis management, of the security industry in general.

Your failures are known. Your successes are not.

The reality is that we spent almost half a trillion dollars to fix this problem, so it wouldn't become a problem.

Now some of that money was spent just kicking the can down the road a little bit. Some of the programmers in, again, an attempt to kind of cut corners, they didn't move over to a four-digit date system. They just went in and told the computer that from 00 to 20, well, that was in the 2000s.
Everything else was in 1900s, and that's why this month we've had some problems.

But it demonstrated that the fear was real. You had a lot of glitches, and some were pretty significant.

In those programmers defense I would suggest that most of them did not believe their system or software would still be in use 20 years later. Some of them were wrong but luckily for the most part they were right and these glitches have been isolated.

This whole thing can teach us a whole lot about climate change. Since the topic came up, we've been doing little things to get the can down the road. Things that make us feel better, things that make it seem like we're doing something, but we're not actually solving the problem, and we're running of time. 

We're in December of 1999, and we're still arguing about whether or not we should do anything. We are running out of time, and the impacts are substantial. There's no doubt about that. I was looking at some of the projections for sea level rise. It's cool for me. Cool for me. Where I'm at right now, it's going to be about a tenth of a mile from the coast, quarter of a mile from the coast.

That's cool.

Problem is, I'm 30 miles inland right now. That's 30 miles of people that are going to be really upset.

Granted that's worst case. That is worst case, but even in a lot of the moderate, even best case scenarios, we end up losing a lot of things we've come to know and love. Like Miami and New Orleans. 

We're already seeing issues. Crabs in the Pacific. The waters become so acidic that it's dissolving their shells. We're seeing the impacts. It's not a hoax. It's real.

I remember a time when scientists were generally trusted. Today we believe in politicians over scientists. One of these groups is known for pursuing truth and facts. The other is known as lying manipulators and those are the ones we're trusting with our future. We might want to rethink that and we need to understand that yes, dealing with climate change is going to be expensive. Fact. But we spent almost half a trillion dollars to deal with a date so it wouldn't become an issue. Now 20 years later we act like it was overblown, like it wasn't really a concern because our failures are known and our successes are not.

I would really hope that in 2100, people look back at climate change and think, man, what were they thinking, it wasn't that big of a deal. That would be the best case scenario. The alternative is that people know we failed.

Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
Black shirt with the text: "IT'S JUST A THOUGHT." in large white san serif lettering with identical but smaller text also in capitals "BEAU OF THE FIFTH COLUMN"
## Easter Eggs on Shelf
{{EasterEgg}}