---
title: Let's talk about Darkness, edge, art, and whether or not it matters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=71m4MnWuweU) |
| Published | 2020/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts off by discussing Eminem's new song and his own views on Eminem and his music.
- Despite not being a fan of Eminem, Beau acknowledges the genius in Eminem's lyrics and the power they hold.
- Beau talks about how he was encouraged by friends to listen to Eminem's new song, so he gave it a shot.
- He mentions catching on to various double meanings in the lyrics, particularly referencing a high-end brand, Heckler.
- Beau appreciates the artistic elements in Eminem's song, like the Easter eggs, visual cues, and the use of confusion to convey a message.
- He points out a portion of the song where he believes there's a deeper meaning implied by Eminem.
- Beau praises Eminem for holding a mirror up to reality through his art and sparking meaningful conversations.
- He expresses agreement with the need for serious societal discourse initiated by Eminem's work.
- Beau comments on the importance of younger generations driving change and making difficult decisions to reshape society.
- He concludes by reflecting on the enduring nature of societal issues and the role of future generations in addressing them.

### Quotes

- "He's holding a mirror up to reality, just letting people see it."
- "Sometimes there's justice and sometimes there's just us."
- "We have to change the culture."
- "It's not gonna be people his and my age."
- "Because it's not going to make a difference."

### Oneliner

Beau dives into Eminem's new song, recognizing its artistic depth and the importance of sparking societal change, particularly driven by younger generations.

### Audience

Music enthusiasts, social activists

### On-the-ground actions from transcript

- Engage in meaningful dialogues about societal issues (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Eminem's new song, Beau's insights on its artistic elements, and the call for societal change driven by younger generations.

### Tags

#Eminem #Art #SocietalChange #YouthActivism #MusicInfluences


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about darkness and edge and art.
If you don't know, Eminem released a new song, and it is wild.
I should start off by saying I'm not a fan of Eminem or his music.
Normally, I view him and his music pizza cutter.
knowledge, no point. Not my thing. At the same time, I do understand that there is a
level of genius in his lyrics. He's one of those people you look at and you're like,
man, if we could only get him to use his powers for good instead of evil. My friends are aware
of my taste in music, so I was really surprised to have literally half a dozen tell me today
And I had to listen to this.
OK, whatever.
Quite a few of you guys, as well.
So I did what I was told to do.
I listened to it.
I didn't watch the video.
Put it on, let it play, went on about what I was doing.
And I heard the Hunter S. Thompson reference.
I'm like, that's got to be why my friends want
me to listen to this.
I didn't understand why a lot of you wanted me to.
And two minutes, 20 seconds in, I hear the word hecklers.
And I realize he has his curious George patch on upside down.
He's talking about one thing, and it
seems like he's talking about something else.
He's drawing people in.
And all of the other things that have double meanings
all came into focus.
If you're not familiar with that community or those products,
Heckler is a brand.
It is a high dollar brand.
It is Ferrari.
At the risk of offending fanboys of another brand,
they really are perfection right out of the box.
So that's what caught my ear.
And at that point, I was like, oh, man.
So I pulled it back up, rewound it,
started watching the video. How could I not love it? How could I not love it? The
Easter eggs, the visual cues, the desire to induce confusion to make a point. Make
people think you're talking about one thing and then something else. It's
It's great, it really is.
The funny thing is,
around 344 to 355,
there's a part where I think everybody is presuming
that it doesn't have a double meaning
when the entire rest of the song does.
If you're watching the video,
And that portion seems like it's the person in the video saying it.
I think it is, but I think it's also Eminem saying it.
I think he's saying, I don't have the answers to this, and in the end this isn't going
to matter.
And he's talking about the song in the video.
He's just showing and demonstrating one of the most powerful forms of art, and this is
art.
He's holding a mirror up to reality, just letting people see it.
He's doing it in a decidedly M&M way with a lot of edge, but that's what he's doing,
and it's powerful, no doubt.
I think he's right.
It's not going to matter.
It's not going to matter.
I don't know that I agree with what he implied, I don't even know if he implied his beliefs,
what I inferred his beliefs to be from this video.
But when I do agree with him on, we do need to have a serious conversation about it.
Again, how could I not like this?
This whole video is, let's talk about it, and I'm not talking about the MGK thing, I'm
about this issue. And the thing is, what are people talking about? The video. Not the issue.
I think he knew that was going to happen. I think he understood that his knack for edge
is just going to allow people to blow this off as him being him, when he is trying to
provoke conversation.
A lot of what he said in there, and I don't know if he researched it or just happened
to be right, a lot of it's true as far as, you know, couldn't find a motive.
The most recent research suggests there isn't one, so multiple stressors acting on somebody
who has managed to blend their grievances and paint one institution or one group as
the source of all of their problems.
That's what the most recent research suggests and yeah so you don't have a motive and I
think the location and the event that he chose to mimic I think that was a good choice because
it was the one that was just the most unbelievable.
I mean that literally, there were people who couldn't,
didn't want to believe that one person
could do that because it was so shocking.
You know, people creating theories, there had to be more.
And if you go to, let's talk about making things quieter,
it actually explains all the sides behind the sounds.
And yeah, I mean, that's how it works.
you have multiple sounds from one shot.
I would suggest that the ending of It's Not Gonna Matter, followed by Get Out the Vote,
is a little self-defeating.
or somebody who likes to push the envelope.
It's the only part of it I didn't like.
Getting out the vote when you're talking
about a problem this big, there's no savior.
It's us.
Sometimes there's justice and sometimes there's just us.
There's no massive government solution to this.
It's gonna be us.
We have to change the culture.
And the thing is, I say us, but it's not gonna be us.
It's not gonna be people his and my age.
It's gonna be people younger than us,
people that have had to live through this,
be impacted by it.
They're gonna have to make the choices, the hard ones.
They're gonna have to decide
whether they want to adjust the culture
adjust society
or adjust the laws
those are their options
and those options
will uh...
they'll decide on them long after
we have uh...
met our old friend
anyway
until that day comes
We can just look forward to more of the same.
Because it's not going to make a difference.
Anyways, just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}