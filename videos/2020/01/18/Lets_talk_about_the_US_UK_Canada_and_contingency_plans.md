---
title: Let's talk about the US, UK, Canada, and contingency plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=geBetDLDxGM) |
| Published | 2020/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contingency plans are prevalent in the Senate, leading to a power struggle with nobody able to predict the outcomes.
- The United States is known for its contingency planning, especially by the Department of Defense, aimed at advancing national interests.
- Historical examples show the U.S. had plans for war with various countries, like invading Canada as a strategic move against the UK.
- Allies like the UK also had plans to attack the U.S., showcasing the importance of consistent defense planning.
- Beau criticizes Trump's unpredictable nature, which has damaged U.S. readiness and strained relationships with allies.
- Trump's focus on domestic rivals rather than national interests has implications for diplomacy and national security.
- Beau warns of the lasting impact of Trump's actions on future presidents and U.S. readiness.

### Quotes

- "Contingency plans are our bread and butter."
- "War is a failure of diplomacy."
- "It's not the fault of the next person. It's Trump's fault."

### Oneliner

Contingency planning, historical war strategies, and Trump's impact on U.S. readiness and national security, unpacked by Beau.

### Audience

Policy analysts, historians, voters

### On-the-ground actions from transcript

- Analyze and understand historical contingency plans to learn from past strategies (implied)
- Advocate for consistent defense planning and diplomatic efforts for national security (implied)
- Support policies that prioritize advancing national interests over domestic rivalries (implied)

### Whats missing in summary

The full transcript provides a detailed historical perspective on U.S. contingency planning, the impact of Trump's actions on national security, and the importance of consistent defense strategies.

### Tags

#ContingencyPlanning #USHistory #NationalSecurity #TrumpAdministration #Diplomacy


## Transcript
Well, howdy there, Internet people, it's Beau again.
So tonight, we're gonna talk about contingency plans.
Seems like everybody's got one right now,
especially in the Senate.
Rand Paul has one, Schumer has one, Graham has one,
even those outside, Pelosi certainly has one,
Lev Parnas definitely had one.
Everybody has a contingency plan.
And that's why nobody can tell you what's going on,
because there's a battle for power.
a whole bunch of if this, then that type of things.
And there's so many, it's impossible to guess.
But while we can't really forecast those proceedings,
we can use that to learn about something else.
See, from the outside looking at it,
I would imagine that contingency plans, as far as the United
states are concerned, that's our bread and butter. That's what we do. You know,
that's how we deal with everything. We have a plan for everything. The
Department of Defense may not appear to be successful if you look at specific
engagements, but overall what's the goal? Advanced national interests. They're
really good at that because they have a plan for everything and they've been
doing this ever since we became a world power. When did we become a world power?
At the end of World War I. That's when we really came into our own. That position
was cemented during World War II. At that point we were one of the the tough kids
on the block. What did we do in between those two wars? Developed contingency
plans. We plan to go to war with everybody and I mean everybody. For those
sitting in the UK, our plan to go to war with you was called a War Plan Red. No
joke. See at the end of World War I, the United States found itself in a new
position. We had a fleet, a real one for a change, which meant that we may run afoul
of the British because the United Kingdom has a long history of using its fleet to advance
its national interests.
So eventually those two paths may cross.
So we couldn't really deal with it directly because of distance.
So our plan was pretty simple.
We're going to go after them indirectly.
We were just going to invade Canada.
I'm not joking right now.
We had a plan to invade Canada.
The thought behind it was that was how the United Kingdom would respond.
They would move troops there and then south.
So we were just going to take it over.
With specific attention paid to Halifax because that's the port that was imagined they would
use to bring in troops.
So it became really important to deal with Halifax.
important that we came up with a really crazy plan to deal with that. Gas. Yeah.
And this, to be clear, wasn't just like some guys in a room with some maps, some
one-off hypothetical. No. We went ahead and started building airfields, pretending
they were civilian airports, in case we ever had to do this. People in the UK
right now they're like really yeah really yeah but don't you know get too
uh too shocked y'all have one too yeah and by the way y'all were just gonna
write off Canada unless it could hold its own for like six months or something
y'all were just gonna leave it to the United States let it become part of the
US you didn't figure you'd be able to muster enough troops fast enough to
even worry about it so it just became a loss you're gonna write it off leave
twisted in the wind which makes that Halifax thing a whole lot worse because
it would have been completely unnecessary. The UK's war plan for dealing
with the United States was to go after our far-flung possessions like the
Philippines, places like that. They're just gonna kind of vacate this
hemisphere over here. They're just gonna get out. And right now, poor Canadians,
The U.S., gas, the Brits were just gonna leave us, I mean what? Don't act all innocent,
y'all had one too. Yeah, I think it was called Defense Scheme 1 or something
like that because it was pretty high priority. A bunch of light, fast-moving,
flying columns, if you will. We're gonna drive south into the United States, go as
deep as they could, destroying infrastructure along the way, and then
retreat back to Canada, unless in some situations like Seattle, they thought maybe they could
hold it.
But for the most part, it was just to destroy as much as possible.
And there was no plan to defend Halifax, which again, made that whole thing just that much
creepier.
What's the point of all of this?
These were allies, allies, and we had plans to do this.
They had plans to do it to us because defense needs consistency, it needs plans for everything.
We need to be aware, we need to know what's going on.
How do you think that meshes with Trump's habit of ruling by tweeting, signing off strategic
partners that we've had forever, just riding them off, getting rid of them.
How many contingency plans do you think involved them?
Probably a lot that now we can't count, can't use those, we got to come up with new ones.
Or deciding that we weren't going to vacate when asked.
Now we have to plan to do that in every country because now it's a possibility.
local partners becomes incredibly hard because we never know what Twitter is going to say.
It becomes more and more difficult.
When you understand that, you'll understand how much the President of the United States
has undermined U.S. readiness.
His landmine diplomacy affects other things.
It affects the defense structure as well because that's what defense is or is a continuation
of politics by other means, when diplomacy fails, you end up with war.
It's what it is.
War is a failure of diplomacy.
And when the diplomatic efforts are concentrated on besting your domestic rivals instead of
advancing U.S. national interests, well, it creates a problem.
This little thing in Ukraine, on its face it may not seem like a big deal, it may just
seem like run-of-the-mill corruption, but the reality is it literally undermined U.S.
national security.
And Trump's less-than-predictable nature has severely damaged U.S. readiness, because
our allies don't know what to expect, so they have to develop contingency plans too.
And they can't count on us because they don't know what we're going to do.
I would not want to be the next president of the United States.
I wouldn't want to clean up after this guy.
We need to enter that into our national memory.
When all of these tough situations occur, it's not the fault of the next person.
It's Trump's fault.
He's the one that did this.
She's the one that undermined U.S. readiness.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}