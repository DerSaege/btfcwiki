---
title: Let's talk about my endorsements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HHDx2xRh2DA) |
| Published | 2020/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Refuses to endorse politicians due to his worldview and the danger of creating cults of personality.
- Believes endorsing politicians is dangerous as it leads to blindly supporting the person, not policies.
- Warns against following politicians anywhere just because of personal support.
- Emphasizes the importance of ideas standing on their own merit, regardless of the source.
- Points out the danger in defending policies you don't truly support because of loyalty to a candidate.
- Cautions against falling into the trap of blindly supporting a candidate instead of their specific policies.
- Advocates for supporting policies and ideas rather than political figures.
- Rejects the idea of being ruled by politicians and encourages leading ourselves in politics.

### Quotes

- "Ideas stand and fall on their own."
- "You'll never see me endorse a politician."
- "My political endorsement for 2020? You and me."

### Oneliner

Beau refuses to endorse politicians, warning against cults of personality and advocating for supporting policies over individuals.

### Audience

Voters, political activists

### On-the-ground actions from transcript

- Lead ourselves in politics (implied)
- Support policies over individuals (implied)

### Whats missing in summary

The detailed examples and explanations Beau provides on why endorsing politicians can be dangerous and the importance of focusing on policies rather than personalities.

### Tags

#PoliticalEndorsements #CultsOfPersonality #SupportPoliciesNotPeople #LeadOurselves #IdeasMatter


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about my political endorsements.
Had somebody on Twitter ask me, and they're like,
I don't think I've ever seen you endorse a politician.
It's because I don't.
And they asked whether or not it was because of my worldview
or there was some wider reason that I didn't.
And the answer is both.
My worldview certainly plays into it.
But beyond that, I think endorsing politicians
is dangerous.
think it's dangerous, because what you're really saying is that you endorse the
person, and that's how cults of personality are created, and those are
dangerous, because once you start supporting Trump, you'll follow wherever
he takes you. You will, and we got to see it in action last week as things
developed and his stance on things seemed to change. People followed and
supported it no matter what he did. His supporters would just, yeah, okay. When the Iraqi Parliament
asked us to leave Iraq, his supporters, those within his cult of personality were out there
like, yay, see, he promised to get us out of Iraq and he did it. And then when he said
that we were going to stay without skipping a beat, they're like, see, we need to stay.
She's defending America because they support the person and not any individual policy.
And that's dangerous.
That's dangerous because once that happens, they can lead you anywhere.
They can take you anywhere.
Ideas stand and fall on their own.
Doesn't matter who brings it to you.
Doesn't matter if it's somebody with an Ivy League degree or a high school dropout.
If the idea is valid, it's valid.
It doesn't matter where it comes from.
You can't discount an idea because of its source, and you can't support one because
of its source either.
And once they trick you into saying, well, I support this candidate, you try to defend
their policies, even if you don't support it.
I've seen it happen over and over again.
I know a whole bunch of people who are very pro-immigration, and they supported Obama.
They believed in hope and change.
And when his immigration policy was not what we thought it would be, they continued to
support it.
Well he has to, for political reasons, it doesn't matter, it's not really your principle.
It's not what you believe in.
Now somewhere out there, there may be somebody who completely supports everything that Bernie
Sanders stands for.
In that case, yeah.
You can say, I support Bernie Sanders, I am co-signing.
his platform completely.
But if you can't co-sign the whole platform, you can't co-sign all of his policies and
all of his actions, you don't really support Sanders.
You support most of his ideas, most of his policies.
And this is true for Andrew Yang or Elizabeth Warren or anybody else.
We've had enough of cults of personality running this country.
And those cults are dangerous no matter what form they take.
Somebody sent me a link to a question on Quora about me, which I found funny.
But one of you wrote a response that basically said it included something along the lines
of, fact checking and arguing with me is greatly encouraged, vigorously encouraged, something
of that effect.
And I loved that that was in there.
Because I do.
I can't be right about anything, about everything.
I can't.
can. Somebody out there certainly knows more about something than I do. That's a
fact. I won't be right about everything. And when somebody brings something up
you can look to the last video. And this was just about the way I phrased
something. But they were right. They were very right and it gave the wrong
impression. I don't argue with it because the idea is what's important, not who
brought it, I pinned the comment. It needs to be seen because they're right.
I shouldn't have phrased it that way.
Ideas are what's important. Policies are what's important.
Not politicians. You'll never see me endorse a politician.
I may endorse policies. I may generally like some.
But, at the end of the day, we've had enough of being ruled.
We've had enough supporting people who are going to make decisions for us.
My political endorsement for 2020?
You and me.
It's time to lead ourselves.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}