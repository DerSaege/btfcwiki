---
title: Let's talk about asteroids, Australia, Mexico, and misfits....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8JkUEKsaBEU) |
| Published | 2020/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes the familiar plot of asteroid movies where a global threat leads people to band together to solve the problem.
- Draws parallels between the asteroid movies and real-life situations like the devastating fires in Australia.
- Talks about the unprecedented scale and intensity of the fires in Australia, caused by climate change.
- Mentions the need for help in Australia and the diverse group of firefighters coming together from around the world.
- Addresses the role of the oil and coal industry in contributing to climate change and the denial surrounding its impact.
- Points out the fragility of society and how natural disasters can quickly reveal it.
- Emphasizes the importance of community organizing and coming together with neighbors to face challenges proactively.
- Advocates for taking action now to solve problems before they escalate.
- Urges people to stop waiting for a crisis to occur before banding together and making necessary changes.
- Poses the choice between passively waiting for problems to worsen or actively demanding solutions and working together to prevent crises.

### Quotes

- "When confronted with a global threat, we'll band together, all work together, and we will solve that problem."
- "We can stop listening to the guy with the bad plan at any moment."
- "We're at that point where we've got to make the decision."
- "We can start banding together now to stop the problem rather than react to it."
- "I put my faith in the misfits."

### Oneliner

Beau describes how real-life crises mirror asteroid movie plots, urging proactive community action to prevent escalating disasters.

### Audience

Global citizens

### On-the-ground actions from transcript

- Organize community meetings to prepare for potential crises (implied)
- Advocate for sustainable practices in your community (implied)
- Support and volunteer with local firefighting efforts (exemplified)
- Educate others on the impact of climate change (implied)

### Whats missing in summary

The full transcript provides a deep dive into the parallels between fictional asteroid movies and real-life crises, urging proactive community action to address climate change and prevent escalating disasters.

### Tags

#CommunityAction #ClimateChange #CrisisPrevention #Proactive #Collaboration


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight, we're going to talk about asteroids, and
Australia, and Mexico, and what those three
things can teach us.
You know, every time there's one of those movies, an
asteroid movie.
Doesn't even have to be an asteroid, something where
there is a object headed towards Earth, and it's going
to alter the landscape of the planet forever.
It's pretty much always the same plot line, really.
There's minor variations, but it's pretty much always the same.
There's the discovery, and then there's
that moment of disbelief and shock
and trying to figure it out.
Then you've got that guy with the one bad plan
that everybody listens to, and you've
got the group of impossibly diverse misfits
working behind the scenes to actually solve the problem.
Eventually, the guy with the bad plan, well, that fails.
But not before making things exponentially worse.
And then the misfits end up solving the problem.
And the idea is that when confronted with a global
threat, we'll band together.
all work together and we will solve that problem. When I watch those movies I have
to admit that somewhere deep inside man I can't wait for that asteroid to see
all those divisions just melt away. But sometimes we don't see the asteroid
coming. You know, in Australia right now, we get to watch this same idea play out
on a smaller scale. They're having fires and we can't even compare them to
California wildfires. They're even crazier than those. They're so big they're
developing their own weather patterns. They're causing phenomena I've never
even heard of. I don't know what a fire cloud was before this. Flames from these
fires are hitting 230 feet. It's hard for a lot of people to visualize distance
like that. Picture a 10-story wall of flame and then double it because that's
how big it is. Huge. Apocalyptic. And this is why they need help and they're going to
need help even once the fires are out to recover from it. And they're getting it. You got firefighters
coming in from all over the world to lend a hand. That impossibly diverse group of misfits.
widely believed that these fires are the result of climate change. See that's our
asteroid movie, the real-life asteroid movie and right now we're all in that
phase where we're listening to the guy with the bad plan who's funded by the
the oil industry or the coal industry. We all know that it's real. You have to. You
have to understand that pumping this stuff out in these quantities is bad. You
would not sit in the garage with all the windows and doors closed with your car
running. It's a bad idea. It's not that much of a stretch to understand that the
garage is the planet with the atmosphere sealing everything in and your car is
your car. This isn't good. It's not good. We know it's real. But see we like to
pretend especially here in the developed world that these things won't they won't
impact us, because we've never seen anything like that for the most part.
But then you look to Australia, that is the developed world, and you see what they're
dealing with, and you can see it in natural disasters all the time, and there's this
shock when people begin to understand how fragile society really is.
It doesn't take much to send it off kilter.
So we deny it.
We act like it's not real.
We act like it's not going to affect us.
Or that we'll be okay.
That's another one that we hear about a lot.
Well I'll be fine.
I got a water source and can grow my own food.
Sure, sure you can bud.
You know, in Mexico, when that bad guy's kid got arrested, there were technicals in the
street, firefights, and that's what would happen.
Those with power in a situation like that, oh, they're coming for that water source,
buddy.
And right now you're saying, oh, see, you know, my neighbors, they'll use that water
source too.
me out. I love that. Band together with your neighbors. All about community
organizing. I think it's a great idea and it's good. That impossibly diverse group
of misfits. The question is why do we have to wait for it to get so bad to do
that? Why can't we start banding together now? You really want to wait until you're
They're having to pull guard duty to protect water.
Seems a little silly.
Not when there are alternatives.
Not when the technology exists to solve it now, before it gets bad.
We don't actually have to follow the script.
We can stop listening to the guy with the bad plan at any moment.
of the Misfits.
We're at that point where we've got to make the decision.
We've got to decide.
Are we going to ride it out?
Do we need to get gas masks for everybody?
Everybody need to have canned food on hand?
Are we going back to the Cold War attitude?
Just got to get ready for it because we know the government is going to mess it up.
Or we start demanding that something happens, en masse.
We can start banding together now to stop the problem rather than react to it.
We see it coming, just like everything last week in those videos.
We know it's coming, and if we sit down and think about it, we see the dominoes fall,
we know how it's going to play out.
I don't know why we feel the desire to wait just to see how bad it'll get.
Have they made so many of those movies that we actually want to live one out?
Anyway, I put my faith in the misfits.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}