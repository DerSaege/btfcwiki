---
title: Let's talk about Joe Rogan endorsing Bernie Sanders....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AJKxmMAOZJ8) |
| Published | 2020/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Joe Rogan endorsed Bernie Sanders, causing upset among different groups, including Democrats and supporters of various candidates.
- Rogan has a history of saying controversial and harmful things, as well as platforming individuals who could cause harm.
- Some believe Bernie should reject Rogan's endorsement, but Beau disagrees.
- Beau argues that Rogan's endorsement may sway some of his audience away from supporting Trump.
- The split in Rogan's audience creates internal conflict, potentially benefiting Sanders' campaign.
- Beau acknowledges the discomfort around Rogan's problematic past but sees the endorsement as a strategic move to draw people away from Trump.
- He criticizes Bloomberg for his approach to the campaign and suggests focusing on the bigger picture of defeating Trump.
- Beau encourages viewing allies like Rogan pragmatically, as long as they are moving in the same direction politically.
- He advocates for engaging with Rogan's audience positively to bring them over to new ideas, rather than pushing them away.
- Beau stresses the importance of unity and strategic alliances in achieving victory, even if it means setting aside personal dislike.

### Quotes

- "Your allies aren't perfect. They don't have to ride with you the whole way."
- "On a battlefield, they will literally bring people in and their whole mission in life is to turn them against each other."
- "Nobody wakes up knowing everything. Everybody has to have a path to follow."
- "Let us talk about some issues that you may have. You've learned to look at this in a different way."
- "When? Anyway, it's just a thought, y'all have a good night."

### Oneliner

Beau explains why Bernie should embrace Joe Rogan's endorsement, leveraging the split in Rogan's audience to draw voters away from Trump.

### Audience

Political activists

### On-the-ground actions from transcript

- Engage positively with individuals from different political backgrounds to bridge gaps (suggested)
- Focus on bringing people over to new ideas rather than pushing them away (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of leveraging strategic alliances in politics and engaging with diverse audiences positively to achieve broader goals.

### Tags

#JoeRogan #BernieSanders #PoliticalAlliances #ElectionStrategy #Unity


## Transcript
Well, howdy there, internet people, it's Bo again.
So, I guess we gotta talk about Joe Rogan, Bernie Sanders.
If you don't know, Joe Rogan endorsed kind of Bernie Sanders,
said he'd probably vote for him.
He told this to his audience last,
I heard the number of people that saw it
was 11 million people.
There are people that are upset about this,
different groups of people. The first group are Democrats, some Democrats, some that support
other candidates, some that support Bernie. Why would they be upset about him catching
an endorsement, kind of an endorsement? Because Joe Rogan has said some messed up stuff over
the years, and by over the years I mean 2015, 2016, 2017, and like yesterday. He's probably
saying something messed up right now. In addition to saying things that have
caused active harm to people, he has platformed individuals that I would
suggest cause people to be actively targeted. He has a lot of, I hate the term,
problematic issues. So there are some that feel that Bernie should reject this
half-hearted endorsement. I'm gonna say no. No. I don't. I don't think that's a
good idea. And the reason I don't think that is a good idea is the other people
that are upset. I want you to think about the people who would listen to Joe
Rogan's podcast. You have two kinds of people. Those who like to hear
interesting and weird conversations and those people that agree with the messed
up stuff that he says. Those people who agree with the messed up stuff that he
He says, well, they heard that, and if they are the type that would agree with comments
that are bigoted in some ways, at some times, in a lot of ways, who do you think they're
voting for?
But they just heard somebody they tune into regularly say that they're going to vote
for Sanders. If you were part of the communities that he slights, and that he goes after at times,
I would rephrase what he said. In your mind, he didn't say, vote for Bernie Sanders. He said,
don't vote for Trump. It's the same sentence. It's the same thing. That's what he told his audience.
That's what was said, that's what they heard.
And now his audience is split.
They have to make a choice.
Do they stand by Donald Trump?
Do they stand by Joe Rogan?
And that audience is going to argue.
Red on red fire, look at this like a battlefield.
You've got opposition forces fighting each other.
That's a win, because they're going to argue.
And those who take Joe Rogan's side,
they're going to become more and more entrenched
in that position as that argument goes on, because it will be an argument,
it's not a discussion.
So they're just going to dig their heels in.
And what they're going to walk away from it with is, don't vote for Trump, that's
a win in this situation, if you were talking about it and looking at it as if
it's a battlefield and it is, Bernie took something from the opposition and
gave them nothing. That's a win. Yeah, it makes some of his allies uncomfortable. And
there's a lot to be uncomfortable about. Rogan has said some messed up stuff over the years.
If you don't know, I mean you do now, there are very few, if you have watched this channel
for any length of time, you know what causes are close to my heart.
There are very, very few.
I think the shelters are the only thing.
He hasn't actually spoke out and kind of undermined.
He's not somebody I would consider a friend.
We have very differing views.
In this instance, that endorsement can only help.
It can only help pull people away from Trump.
That's it.
That's all it can do unless it succeeds in creating red on red fire on this side of the
battlefield and Democrats start attacking each other.
But the Sanders campaign took something and left nothing.
And there are those who are saying, well, it's legitimizing Rogan.
Rogan has a massive audience.
Part of the problem with the United States is that the audience often determines whether
or not somebody is legitimate.
He has an audience that regardless of whatever he says, he's already legitimized.
That was the problem with him bringing people like Milo or the frog guy on his show.
It helped build them.
And yeah, that's bad.
That's really bad.
produced active harm. In this case he told that same audience that tunes in
to listen to people like that, that he was probably gonna vote for Sanders.
More importantly, he said he wasn't gonna vote for Trump. He didn't say it out loud,
but they all heard it. They all heard it. If you want to talk about somebody who
is actively helping the opposition, you need to go talk to Bloomberg.
He's pumping millions into running ads on Fox News, and he is being cheered on by the
Democratic establishment because they're not looking at it like a battlefield.
Where's that money going to go?
money is going to eventually pay for investigative reporters to do opposition research on democratic
candidates in the hopes of trolling Trump.
That doesn't seem to be an effective use of that money.
But what do I know?
As far as Joe Rogan and this endorsement, I want to go back to that video about allies.
Your allies aren't perfect.
They don't have to ride with you the whole way.
All they have to do is get on at one stop and get off at the next.
They're just going to ride that bus with you for a little bit.
You don't have to be friends as long as you're moving in the same direction.
Rogan at this moment is moving in the direction of most of the progressive side of the United
States.
Like it or not, he is.
And in addition to hopping on the bus for a couple of stops, he's handing out bus
passes to millions.
You don't have to like him.
You don't.
You don't have to co-sign him.
You don't even have to tolerate him.
But if you're going to go after him, there's no reason to tie it to the fact that he told
his audience he's not voting for Trump.
Why undermine that?
He told millions of people who would probably be voting for Trump not to.
Attacking that seems like a really bad idea.
Pushing them away seems like a really bad idea.
It should be the idea to pull as many people from his base as possible, to get them away,
to break them away, to introduce them to new ideas, and to turn them on each other.
On a battlefield, the U.S. military, if it is an advanced battlefield with multiple opposition
forces, they will literally bring people in and their whole mission in life is to turn
them against each other, because it's that important.
It is that important when it comes to achieving victory.
You don't have to like him.
And yeah, you can talk about all of the horrible stuff that he has said.
Rogan has said some messed up stuff.
That's not off limits.
But there's no reason to push away the people that he's trying to bring over.
If you're going to talk about it, you're going to criticize it.
Do it from a position of, we're glad you're coming over.
Let us talk about some things that you need to fix.
Let us talk about some issues that you may have.
You've learned to look at this in a different way.
Now we need to look at some other stuff in a different way.
Nobody wakes up knowing everything.
Nobody's born knowing everything.
Everybody has to have a path to follow.
He's doing it.
Maybe.
Or maybe he's just going for popularity.
Who cares?
If he's being self-serving, fine.
Let him.
Capitalize on it.
When?
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}