---
title: Let's talk about Trump's water rule....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zYyXEodsy64) |
| Published | 2020/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Water is life and clean water is good.
- The Clean Water Act has protected America's waterways since 1972.
- President Obama expanded the Clean Water Act to include smaller bodies of water based on scientific studies.
- Trump has rolled back protections on waterways further than ever before.
- The rule to undo protections was developed by political appointees, not based on scientific consensus.
- Environmentalists believe Trump's actions will lead to deaths and polluted waterways.
- The undoing of regulations allows developers to pollute for profit.
- This decision goes against the Clean Water Act and endangers water supplies and crops.
- Trump's actions will need to be undone by the next president to prevent further harm.

### Quotes

- "Water is life, clean water is good."
- "This is going to be a mistake."
- "Your water supply is tainted, your crops will die."
- "People will die. This is ridiculous."
- "We have a complete incompetent person in the White House."

### Oneliner

Beau stresses the importance of clean water and criticizes Trump's rollback of protections, warning of dire consequences and the need for future correction.

### Audience

Environmental activists, concerned citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for clean water protections (suggested)
- Join environmental organizations working to safeguard waterways (implied)

### Whats missing in summary

The emotional impact of Beau's frustration and urgency in protecting clean water.

### Tags

#CleanWater #EnvironmentalProtection #TrumpAdministration #PublicHealth #CommunityAction


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about water, it's raining.
Water, water is good.
Water is life, clean water is good.
America's waterways have been protected since 1972
by something called the Clean Water Act.
President Obama expanded the definition
of what was covered by the Clean Water Act.
That decision was not taken lightly,
I will admit there were issues with it, but it was based on 1,200 scientific studies.
Basically, we realized that waterways are connected, and the smaller streams, the
smaller bodies of water that were not protected, feed into the larger ones. So
it doesn't do any good to protect the main river if all of the feeder streams
are being polluted because they just roll into the river. Makes sense. I mean
this is really simple. Trump's undone this and in the process rolled the
protections back further than they have ever been. If this rule goes into effect
America's waterways will be less protected than they have been at any
point in the last 50 years.
This should come as no surprise to those who know how President Trump runs his EPA, those
people he appoints.
Although he is no longer there, Pruitt, one of Trump's appointees to lead the EPA, is
self-described as being a leading advocate against the EPA.
No surprise they're trying to undo any form of regulation to protect the environment.
The rule was developed strictly by political appointees.
It was such a travesty within the EPA that 44 employees filed a complaint with their
inspector general saying that this rule had no basis in scientific consensus and did not
take into account watersheds basically the connectivity of water. This comes at
a time when Americans are just freaking out over a virus. A virus that really
isn't present in the United States in any meaningful way as far as we know but
there's panic. I talked to a few of my friends who are environmentalists. They
have varying opinions on how bad it's going to be. The one thing they all agree
on is that this is going to kill people. Period. This is going to kill people. It
is going to pollute waterways. We're gonna have a whole bunch of flints
because that's what this does. It reverts a lot of things to the local level. So
So, if a developer or manufacturer can pay off the local county commission, well, that's
pretty much the end of it.
Maybe not for that county, maybe one for downstream, which is kind of why this rule was put into
place.
That's why the Clean Water Act exists.
And it's been unwound, so Trump's developer buddies can make some money.
They can poison your kids to make a little bit of cash.
It's really what it boils down to.
It's all about the pursuit of profit.
This decision goes into effect while the President is attending
a conference where one of the main themes is to revert to
stakeholder capitalism rather than shareholder capitalism.
It certainly appears that the President is incapable of
learning anything
i know there are a lot of farmers that are applauding this
guys understand
this doesn't just get rid of obama's rule
it goes way way further than that
your water supply is tainted
your crops will die
this doesn't help you
It's one of those throwing the baby out with the bath water things.
This is going to be a mistake.
This is going to be something that is going to have to be immediately undone by the next
president.
And if not, people will die.
This is ridiculous.
We have a complete incompetent person in the White House.
who just refuses to be educated and keeps those who can educate him out of the conversation.
Anyway, it's just a thought.
and y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}