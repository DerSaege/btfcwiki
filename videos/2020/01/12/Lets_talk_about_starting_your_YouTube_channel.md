---
title: Let's talk about starting your YouTube channel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BgZ_nSqFNDs) |
| Published | 2020/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into the topic of starting a YouTube channel, a question he receives frequently.
- Viewers often find generic advice on starting a channel not applicable to their specific content.
- Beau advises content creators to prioritize authenticity in their videos to connect with viewers.
- He stresses the importance of staying within your expertise and relating content back to what you know well.
- Beau challenges the idea of maintaining a consistent posting schedule, especially for news-related content.
- He bluntly states that initially, viewers do not care about the creator; they come for the content.
- Being relatable and admitting to mistakes is seen as a key factor in building a connection with the audience.
- Beau advises against sensationalism and clickbait titles, focusing on creating engaging but genuine content.
- He simplifies the YouTube algorithm's goal: keeping viewers engaged on the platform for as long as possible.
- Using Easter eggs and creating a community-centric platform are encouraged by Beau.
- Having a clear goal for your channel is emphasized over simply aiming for success.
- Beau advises creators to offer unique perspectives and avoid replicating existing content.
- Experimenting lightly with changes in content is suggested to maintain viewer engagement.
- Ignoring the allure of making money on YouTube is advised, with an emphasis on having a genuine purpose for creating content.

### Quotes

- "Be authentic."
- "Nobody cares about you."
- "Ignore the money."

### Oneliner

Beau dives into practical advice for new YouTubers, stressing authenticity, relatability, and community building over generic recommendations.

### Audience

New YouTubers

### On-the-ground actions from transcript

- Build a community-centered platform (suggested)
- Offer unique perspectives in your content (suggested)
- Experiment lightly with changes in content to maintain viewer engagement (suggested)

### Whats missing in summary

Practical tips and insights on navigating the world of YouTube content creation with authenticity, relatability, and community building at its core.

### Tags

#YouTube #ContentCreation #Authenticity #CommunityBuilding #Advice


## Transcript
Well, howdy there, internet people.
It's Beau again.
So tonight we're going to talk about how to start your
YouTube channel.
It's a question I get asked way more than I thought I would.
And the back story to the question is always the same.
I've watched all the videos on YouTube about how to start a
YouTube channel, and none of that stuff works for me.
And that makes sense, because if you're asking me, odds are
you want to build a channel similar
in some way to this one.
You want to discuss these topics.
And if you're talking about stuff that is news, most of that advice does not apply to
you.
Most of the advice that comes in those, this is how you start your YouTube channel, has
nothing to do with you.
In fact, it's all wrong and will hurt you in a lot of ways.
Those videos are framed for people doing generalized YouTube content.
It's not what you're doing.
Okay so watch them because they give you good insight into how YouTube works, but ignore
most of the advice. Okay so my advice rule one be authentic and that's funny if you know how this
channel started because this channel started as a joke with me way overplaying my hillbilly-ness
but what I would point out is that once I stopped downplaying the hillbilly is when it actually
became successful. Viewers can see that authenticity. You may think you're a great actor but I am
living proof that viewers can see that. You want to be as authentic as possible. Now obviously
your public face and your private face are going to be different but you want them to
be as close together as you can and they will see that. I promise you.
Number two, stay in your lane.
That doesn't mean you can't make a video outside of what you're passionate about or an expert
on or whatever.
It means always find a way to relate it back to something you know a lot about.
That way you're more comfortable and you're more confident the viewer will see that.
It also helps you be right more often.
You're not going to be right all the time, but it'll help you be right more often.
Number three, posting schedule.
In all of those YouTube videos about how to be a YouTuber, it says maintain a consistent
posting schedule, it's like super important for the algorithm and all of that.
Don't do it, that doesn't apply to us.
If you're going to be discussing news, you get that information out as soon as you can
get an accurate read on it.
As soon as you have your take on it, and it's a good one, put it out right then, don't
wait.
Because if you wait, it's going to be stale.
Number four, this is one that's a hard pill for a lot of people to swallow.
Nobody cares about you.
Nobody cares about you.
A lot of people, when they first start out, they want to do videos about themselves and
explain who they are.
Nobody cares.
And I know that seems counterintuitive, but they don't.
They come to you for content.
Later on, people will start asking, who are you?
That's when you make those videos.
I didn't do mine until I had 100,000 subscribers.
They're coming to you for content.
If your content is good, they'll keep coming back.
Let's see, number five, be relatable.
And what that means is admit that you are flawed.
You're going to make mistakes.
You've made mistakes in the past.
You're going to make them in the future.
It's OK.
Be relatable.
Admit that.
Don't try to present yourself as infallible.
Because if you do, eventually you're
gonna make a mistake and then that's it. Your image, your mystique is blown. So
just be who you are. People that watch this genre, they're just like you who's
gonna make these videos. You're trying to grow. We're all trying to grow. Otherwise
we wouldn't be watching stuff like this. Part of growing is making mistakes. Just
just admit it. If somebody corrects you, pin the comment. If it's a big enough
enough mistake, make a video explaining it.
Number six, don't be sensational.
This is one people like to argue with, too.
Well, this video is real sensational,
and it got a million views.
Yeah, it did, but look at the channel.
You want long-term channel growth, right?
You don't just want one video that did really well,
and now people make fun of you for it.
So don't be sensational.
You don't have to have clickbaity titles that
are saying the world's gonna end.
You can find a way to make your titles interesting
without forecasting doom all the time.
Also, who wants to wake up and do that?
Just don't do that, that's just bad.
Number seven, the algorithm is not a thing,
at least not the way most people picture it
when they talk about it.
It's not something out there that's
some kind of esoteric nonsense.
It's real simple.
The algorithm has one job,
that is to keep the viewer on YouTube
as long as humanly possible. That's all it does. Once you understand that, it becomes
really easy to look at your analytics and help grow your channel. I can tell you that
more than likely, this video will get half of its views that it's going to get by the
end of the week, within the first four hours after it's posted. Because that's always
the case. Occasionally there are exceptions. It pops viral afterward. But normally that's
what happens for me and that's because of you guys. Y'all are going to interact in
the comment section and talk about it and that keeps you on YouTube longer. So YouTube
will continue to recommend that video as long as you guys are active in the comment section.
It's really that simple.
Don't look at it as an enemy, look at it as a tool once you understand what it's there
to do for YouTube.
Number eight, use Easter eggs.
Flipped upside down Curious George, t-shirts that match your content, maybe you put something
on the shelves behind you occasionally.
Don't use mine, but do something like that because, at least for me, the marketing stuff
that I want to use is stuff that actually adds value to the viewers, you guys.
Because that helps build a community and we're going to come back to why that's important
to me here in a second.
But even if that's not your goal, you want stuff that people will talk about and you
want stuff that will help promote you without being over-commercialized.
So definitely use Easter Eggs, but not mine, come up with your own.
And number nine, each video needs to stand on its own as a piece of content and fit into
an overall arc, easier said than done.
Ever come into a TV series halfway through, you don't know what's going on?
Nobody likes that.
So each video needs to be standalone.
But if you're producing a lot of content, you want it to fit into an arc.
You want it to fit into an arc.
So your longtime viewers get a story.
Now I make this mistake, especially with rapidly developing events like last week, if you came
into like the third or fourth video talking about that, you were completely lost.
My bad, I'm flawed.
It happens.
But generally you don't want to do that.
You want each piece to be able to stand alone.
Let's see, where am I at?
Number 10.
This is the big one.
Identify what you want your platform to do.
Mentioned earlier, build a community.
That's the goal of this.
That's the goal of this channel is to build a community that can then enter the real world
and build community based organizations.
We talk about a lot on here.
That's actually the goal of the channel.
If your channel doesn't have a goal, it's going to be real hard to figure out where
to go.
Having a successful YouTube channel isn't a goal, that's a wish.
up with a goal that you can work towards and that way you can measure against it and it
helps keep you motivated, helps keep me motivated anyway.
So that's the idea and if you're doing that you can make decisions like the Easter eggs.
It benefits the community which is the actual goal of the channel.
So you've got to figure out your goal and it may not be that, it could be a whole bunch
It could just be to bring awareness to something.
But you've just got to figure it out.
And I would do that before you really start producing content.
Number 11, only make a video if you have something you can add to the conversation.
If you're just going to make a video that's like everybody else's video on it, it's going to get lost.
Especially if you're new.
The popular person is going to get the traction, not you.
is going to get the traction, not you.
And so you want to bring a fresh take to any discussion
that you approach.
And if you don't have an original thought to add to it,
step back, ask a wider question, or move in and ask
a more nuanced question, or relate it
to something in history.
Again, don't take all of my stuff, but you get the idea.
Find some way to approach it from a fresh angle.
That way the viewer gets some value from it.
Okay, number 12, experiment lightly.
When people start watching a YouTube channel, especially if the whole goal is
to build a community, there's a relationship that develops.
You interact in the comments, you actually get to know the people watching,
and they get to know you.
When you make changes, it can be unsettling,
even something as simple as a background.
So use the background once, and then go back to your normal routine.
Bring it back again.
Do stuff slowly.
Don't make a whole bunch of changes at once.
They'll lose you in the recommendations because the thumbnail won't look the same either.
In fact, you can look and see that videos filmed here tend to get more views than those
on the background, especially that first week until people see it pop up in their recommendations
a few times.
And they're like, oh, I know that guy.
Generally, people recognize the background
before they recognize me, because I
look like every bearded bald hat wearing guy on YouTube.
Number 13, ignore the money.
You're not going to get rich doing this.
I mean, I know that some people have,
but you're not going to be Ryan, more than likely.
Ignore the money.
You can eat out of living doing this.
It's a lot more work than it seems.
But do it for whatever your goal is.
Have a goal.
Have a reason to do it.
And you'll be a whole lot more successful than just trying
to do it for a job, because it won't work that way.
So there's my 13 pieces of advice for new YouTubers,
I guess.
And so now I can just refer people to this,
rather than answer the questions.
Anyway, I hope everybody got some value out of it.
It's just a thought.
I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}