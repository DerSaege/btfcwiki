---
title: Let's talk about how there can be only one and a question from a mom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pZCgR1xS7iE) |
| Published | 2020/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a heavy email he received from a mother concerned about her son's increasing interest in nationalism and warrior ethos, fearing he may follow in his late father's footsteps who was lost in the military.
- The mother expresses discomfort with her son's deep interest in history and martial arts, possibly influenced by his father's military background.
- Concerned about her son adopting hateful ideologies, the mother seeks Beau's help in guiding him away from nationalist tendencies.
- Beau suggests introducing the son to a TV show character, Duncan MacLeod from "Highlander," to provide a different perspective on warrior ethos and nationalism.
- He describes how Duncan's character evolves from a hotheaded warrior to someone who questions nationalism and patriotism.
- Beau believes that through watching this show with his mother, the son can learn valuable lessons and potentially shift his mindset away from hateful ideologies.
- He acknowledges that facts alone may not be effective in changing the son's views, suggesting that fiction like the TV show could help him understand deeper truths.
- Beau concludes by offering his assistance and expressing hope that this alternative approach may make a positive impact on the son.

### Quotes

- "Nationalism is inextricably tied to that journey of warrior-dom, it's there in the beginning and it fades as you know."
- "Sometimes fiction is what can get to truth, and that's what he's going to need to know."
- "I think it may be perfect for this."

### Oneliner

Beau helps a mother address her son's nationalist leanings by recommending the TV show "Highlander" to provide a different perspective on warrior ethos and nationalism.

### Audience

Parents, concerned individuals.

### On-the-ground actions from transcript

- Watch "Highlander" with individuals showing nationalist tendencies (suggested).

### Whats missing in summary

The emotional depth and context of the mother's plea for help in guiding her son away from nationalist ideologies.

### Tags

#Parenting #Nationalism #TVShowRecommendation #YouthEducation #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
So I'm going to start off by saying,
if you sent me a message via the comment form on the website
and the subject of that message is anywhere near you,
turn this off now.
Ha ha ha.
Kind of indicated he doesn't watch this,
but just wouldn't want them to hear it in the earshot.
So I'm going to read you the email
and I'm going to provide my thoughts
as I read it for the first time
just to add a little bit of levity
because this is a pretty heavy email.
Hi Bo, you remind me a lot of my husband
and I think my son would love you.
I'm already really uncomfortable
with where I think this is headed.
it. My son is very interested in history and martial arts. He spends every waking
moment on those subjects. He's always been like this. I am not interested in
playing Mr. Miyagi. I got no interest in it whatsoever. His father was in the army
and I think this is his connection to him because he doesn't really remember
it. We lost him over there. The problem is that my son is starting to show a lot
signs. My husband was a patriot. He might have been a little nationalist but that
was all gone after his first time over and that's really really common. He never
used the words other guys used for them. He cared about the people there and
understood the difference between them and his opposition. I'm incredibly
Worried my son will join in a few years. I can't stand the thought. I can't lose him, too
Especially with so much hate in his heart. He uses those words. I
Know you've mentioned you live in Florida, but can't remember where I'm in Miami. Could you talk to him in person or a
video?
If I thought it would do any good I would meet him
But this certainly seems to me like a developing warrior ethos combined with the memory of his father.
I'm all about lost causes, but I am not up to the task of this.
Nationalism is inextricably tied to that journey of warrior-dom, it's there in the beginning and it fades as you know.
Once it starts, the only way out is through, really.
I don't think he needs to meet me.
I think he needs to meet a guy named Duncan MacLeod of the Klan MacLeod.
He's a fictional character from a TV show.
It's not a particularly great TV show when you're talking about the writing or the special
effects or anything like that, but it explores a lot of themes that I think your son would
be very interested in, and it's all about history and martial arts.
For all I know, it was your husband's favorite TV show.
More importantly, for all your son knows, it was your husband's favorite TV show.
I would watch it with him and try to point out any similarities between Duncan and your
husband.
It's available on a couple of different streaming services, I checked.
The name of the show is Highlander, by the way.
The storyline is Duncan can't die and it's told through a series of flashbacks from the
modern era, well 30 years ago modern and 400 years ago or 200 years ago and it goes back
and forth and you get to see his journey of being a warrior play out and yeah it's very
cheesy and there's a whole lot to it that's not great, but I think it may be
perfect for this. 400 years ago he was a hothead. He just like everybody else when
when they start down that road. A few decades later he's standing on a field
with people that will never leave that field talking to somebody else telling
Some days guys don't care about your flag or your cause.
He serves in a bunch of different countries, militaries, helps undermine that nationalism.
And as it progresses, eventually the nationalism is gone.
Eventually even the patriotism is gone.
Eventually it's just him engaging in this kind of stuff because of his own moral compass
And because, as a lot of people will tell you, eventually it just becomes a game.
Once it's there, it's there.
And a lot of the stuff loses its meaning, but the game, so to speak, is still there.
And that's funny in its own way in relation to the show, because the overall plot line
that a game is taking place. I think it would do a lot of good. I think it would do a lot
of good. He can experience this vicariously because of his age, and I think it might help.
If it doesn't reach back out, I'll come up with something else. I can give him facts,
but facts are just that, they're facts. If he's into history, he already knows the facts.
Sometimes fiction is what can get to truth, and that's what he's going to need to know.
He's going to need to know some of the truth behind what he wants and what he thinks.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}