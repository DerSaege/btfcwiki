---
title: Let's talk about cars, buses, and allies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nxoHtj9p1A0) |
| Published | 2020/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Observes people debating allies instead of working towards a common goal.
- Defines allies as those generally moving in the same direction towards a shared destination.
- Compares allies to being on a city bus where people can join and leave at different stops.
- Emphasizes the importance of focusing on progress and maintenance rather than arguments.
- Acknowledges that allies won't always agree but should minimize conflicts due to greater goals.
- Encourages understanding that personal journeys may affect how far allies are willing to go.
- Stresses the long journey towards a shared utopia and the need for patience.
- Urges prioritizing actions that contribute to reaching the destination over theoretical debates.
- Reminds that allies are not perfect and disagreements may arise but should not hinder progress.

### Quotes

- "Your allies aren't perfect. They're never going to believe the same thing that you do."
- "If you're headed in the same direction your allies, yeah, there will be spats, there will be arguments, but they should be pretty short-lived."
- "It's a long journey, it's going to take a long time to get there."

### Oneliner

Beau explains the importance of allies moving towards a common goal like passengers on a city bus, focusing on progress over arguments.

### Audience

Community members

### On-the-ground actions from transcript

- Maintain the community bus: Ensure the vehicle (transcript implied) is well-equipped for the journey towards a shared goal.

### Whats missing in summary

The full transcript provides a detailed analogy on how allies should be viewed in terms of shared goals and progress.

### Tags

#Allies #Community #SharedGoals #Progress #Unity


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about what cars and buses can teach us about our allies.
Lately I have noticed a whole lot of people who are generally moving in the same direction,
spending more time debating each other and arguing with each other over finer points
they spend working to get to their destination or telling people headed the
other direction to turn around. Your allies aren't perfect. They're never
going to believe the same thing that you do. If they did believe exactly as you do
they're not your ally, they're on your team. They're part of your army so to
speak. Allies, by definition, are people that are aligned with you, that generally
are moving in the same direction. Your final destination is your utopia, where
you want to end up. As long as people are moving in that direction, they're your
ally. What people seem to want right now is for everybody to pile in a car
together and just drive to that destination. It's not how it works because
in addition to people having varying views they're at different places in
their personal journey. So at this point in time they may not be ready to go all
the way with you or they may see things differently. You may see
things differently. In five years, your destination may change. Your vision of
utopia may be different than it was five years ago. I would hope so. So instead of
looking at it like a car, look at it like a city bus. Your utopia is your dream,
your destination. That's where you want to get to. And along the way, there's a
whole bunch of stops and people can get on and off as they choose. They may get
on and ride five stops with you and get off. They don't want to go all the way
yet and it may just be because of their personal journey. They may not be ready
to go all the way yet. Maybe in another year another bus comes by and they ride
a few stops further, and then another bus, and eventually they catch up. Or maybe your
destination changes along the way. But arguing with people who are on the same bus with you,
headed in the same direction, moving to the same general area, probably not as useful as debating
those who are going the wrong way. It's also probably a better use of your time
to perform maintenance on the bus, fill it up with gas, whatever fits into the
analogy, work towards actually making sure you can get to that destination
rather than arguing about the theoretical and the hypothetical of well
Well, why don't you want to go as far as I do?
Or why are you slightly to the left?
Or slightly to the right of where I want to be?
If you're headed in the same direction your allies, yeah, there will be spats, there will
be arguments, but they should be pretty short-lived because you have other things to do, you got
bigger things on your plate.
It's a long journey, it's going to take a long time to get there.
They have time to get off the bus for a little bit, catch back up later.
And you have time to do the same thing.
Your allies are not perfect, they never will be.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}