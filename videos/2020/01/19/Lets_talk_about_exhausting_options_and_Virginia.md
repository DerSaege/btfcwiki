---
title: Let's talk about exhausting options and Virginia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JNEjh3qcn1I) |
| Published | 2020/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau questions the motives behind resorting to extreme options in Virginia, attributing it to a growing trend in the country.
- He warns against jumping straight to the most extreme measures, noting that it may reinforce the very ideas that led to the legislation in the first place.
- Beau points out the potential consequences of escalating the situation, either through peaceful or loud means.
- He suggests that a more effective approach might be to show up without weapons, as a show of force without escalating tensions.
- Beau underscores the importance of considering the long-term implications of actions, especially in relation to gun ownership and government involvement.
- He brings attention to the need for a cultural shift within the community regarding the perception of firearms.
- Beau expresses concerns about the lack of foresight in the current course of action and its potential impact on gun rights advocacy.
- He questions the wisdom of applauding officials who tie gun ownership to government service, potentially undermining the fight for individual rights.
- Beau advocates for a more strategic and thoughtful approach to advocacy, rather than immediate and extreme reactions.
- He concludes by suggesting a more nuanced and calculated strategy in addressing the issues at hand.

### Quotes

- "Straight to the most extreme option. Probably not a good idea."
- "Maybe the best idea is to pull a fast one and everybody just show up without their stuff."
- "It may be time to address a cultural issue within your community."
- "This doesn't seem like it was well thought out."
- "I guess it's just a thought."

### Oneliner

Beau questions the wisdom of extreme measures in gun advocacy and suggests a more strategic, nuanced approach to address cultural and legislative issues in Virginia.

### Audience

Gun advocates

### On-the-ground actions from transcript

- Show up without weapons to demonstrate strength and unity (suggested)
- Advocate for a cultural shift within the community regarding firearms (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential pitfalls of resorting to extreme measures in gun advocacy and underscores the importance of strategic thinking and cultural change.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about exhausting all the options.
And we're going to talk about Virginia.
Before we get into it, understand you can save your breath.
I've heard it before, I don't own any more,
therefore I'm not a member of the club, therefore my voice doesn't matter.
Fine.
Fair enough.
Just want to ask some questions.
I just want to ask some questions.
Why is all this happening?
Well, because they want to take them.
Maybe on some level, that's true.
I'll give you that.
But why?
Because there is a growing trend in this country
for people to jump over all of the other available options
and go straight to one that makes people really uncomfortable,
go straight to the most extreme option.
And we have seen this play out in schools and in workplaces.
And that's unnerving to people, rightly so.
So what's your response to jump straight to the most extreme option?
It's kind of reinforcing the very idea
that brought about this legislation to begin with.
This may not play out the way you think.
It may actually still resolve
and make them more likely to just hammer it through.
Because the reality is, what happens?
This goes all the way through, gets signed into law.
And then there are years of legal challenges.
It's in court for years.
And then eventually, maybe it gets defeated.
Or maybe it gets enforced in earnest.
But there's a lot of other options along the way.
And you went straight to the most extreme.
Because the reality of this is that it could go loud.
I know most of the organizers that I've seen
have been adamant about the fact that they want
this to be completely peaceful.
But they can't control everybody.
They can't.
And if it goes loud, what happens?
You've completely reinforced the very idea
that brought about the legislation.
It's still a resolve.
Let's say it doesn't go loud.
What happens then?
It doesn't go loud.
Nothing bad happens.
Everybody just stands up there and looks sharp.
Why are you bringing them to begin with?
Well, so they know.
They already know.
How do you describe yourselves?
Your owners, right?
They know you've got one.
You don't need it.
All that does is increase the chances of something going wrong
and inflame the situation.
Straight to the most extreme option.
Probably not a good idea.
But let's say everybody brings them.
Everybody shows up, looks sharp, no problems whatsoever.
Then what happens?
It turns into an applause fest for the sheriffs who
said that they were going to turn their jurisdictions
and make them sanctuaries or deputize people or whatever.
Fair enough.
That's not damaging, is it?
Is it?
What's been the big fight for the last couple decades?
Getting ownership seen as an individual right
unconnected to government service.
And you're going to applaud a bunch of people
who are going to protect you by connecting it
to government service.
That probably isn't going to help either.
You may be undermining things because you didn't think this out.
You went straight to the most extreme option.
Maybe the best idea is to pull a fast one and everybody just show up without their stuff.
You don't need that to do a show of force.
They're politicians.
They care about getting re-elected.
They see a bunch of numbers and there would be more people coming if you weren't bringing
in your stuff, they see a bunch of numbers and they might change their tune.
They might realize that they suffered from negative voter turnout, people who
have turned against Trump because of his, well, jumping to the most extreme
option all the time.
See how this is working?
I don't think this was well thought out.
And I know, as far as those sheriffs, well, those are good guys.
They would never...
You're right.
I actually know a couple of them, and you're right.
They never would.
But as you just learned in the state House and Senate, other people eventually get elected.
Then what?
You've tied ownership to this, and they can just take it away.
The next person elected can just take it away.
It may be time to address a cultural issue
within your community.
Everybody always says, it's just a tool.
I'm the weapon.
That saying originated with people who thought,
who tried to think ahead, who tried to plan.
This doesn't seem like it was well thought out but again I don't know I'm
I'm not in the club anymore.
So I guess it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}