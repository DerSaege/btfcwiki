---
title: Let's talk about holding government accountable and Flint....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UCWhHjVxhVA) |
| Published | 2020/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court refused to hear cases related to the Flint water crisis, opening the door for officials involved to be held accountable.
- Government officials can be held accountable when the government stops protecting them.
- Beau questions if accountability will also happen in other high-profile cases.
- He suggests that the cover-up is often more significant than the crime itself.
- Beau hints at potential accountability actions even before the current administration leaves office.
- Government officials often live in an echo chamber, only listening to those who agree with them.
- Americans are unhappy with the current administration and its protectors.
- Beau mentions the possibility of another impeachment in the house.
- He points out that justice delayed is justice denied, drawing a parallel to the Flint water crisis.
- Beau concludes by reflecting on the potential consequences for senators during election time.

### Quotes

- "It's not the crime, it's the cover up."
- "Justice delayed is justice denied."
- "Americans are not happy with the current administration and those protecting it."

### Oneliner

The Supreme Court's refusal to hear Flint water crisis cases may signal accountability for officials, prompting reflections on cover-ups, potential future actions, and voter dissatisfaction with the current administration.

### Audience

Voters, Activists

### On-the-ground actions from transcript

- Contact elected officials to demand accountability for government actions (implied)
- Stay informed and engaged in political processes (implied)

### Whats missing in summary

The full transcript provides in-depth insights into government accountability, cover-ups, and the importance of timely justice in holding officials responsible.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about some news that may have been lost.
You know, the entire nation
is focused on
a politician being held accountable.
Hopefully being held accountable.
Well, in the background
there's another story
but it's actually the same story.
The Supreme Court
refused
to hear a couple of cases, and those cases stem way back to 2014 and the Flint water
crisis.
The state and local officials have argued this entire time that they're protected
by qualified immunity, that they can't be sued, they can't be held accountable.
Four courts along the way have, let's just say, not agreed and they took this case, this
argument all the way to the Supreme Court, who refused to hear it.
This opens the door for those officials who were involved to be held accountable.
This started six years ago.
It's one of those things when it comes to government accountability.
It's hard.
It is hard to hold your government accountable.
But what often happens is time goes by.
The influence of those who are being protected, well, it gives way.
And then all of a sudden, the government doesn't care to protect them anymore.
Those individuals that were within government that did things.
The government ceases to protect them and then they can be held accountable.
It happens a lot, probably more than most would imagine.
I'm just wondering if it's going to happen in the other case.
the case that's up front and center.
At some point this administration will end.
The next administration will take over.
And those who helped cover up any wrongdoing, well they're not going to have the protection
of the administration.
It won't exist anymore.
Now it will not be politically expedient to go after a president or a former president.
But a congressman, a senator, in the nation's eyes, a dime a dozen, they'll be the ones
that are held accountable.
It's not the crime, it's the cover up.
I'm sure they've heard that before.
And I have a feeling that that accountability may start even before this administration
leaves office.
People in DC, government officials in general, they live in an echo chamber.
They listen to the news they want, the news that agrees with them.
They listen to constituents that agree with them.
They listen to donors who agree with them.
And that's pretty much it.
It's not often that you get a politician that has their finger on the pulse of their
community.
You can look at things and tell that Americans are not happy with the current administration
and those protecting it.
Yeah, he may not be impeached this time.
Keep in mind, they can impeach him again in the house.
There's nothing stopping them from doing that.
We may not be impeached ever, but I think there's a lot of senators who are going to
take a hit come election time.
Because if there's one thing that Flint has taught us, it's that justice delayed is justice
denied, and I don't think the voters want to wait.
Anyway, it's just a thought.
I have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}