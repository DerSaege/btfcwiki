---
title: Let's talk about the climate-migrant paradox....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x868wZY3_U0) |
| Published | 2020/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a man from a country at risk of disappearing due to rising sea levels who fled to New Zealand and claimed he had to stay.
- The UN decided he could return because his country disappearing is 10 to 15 years away, not immediate danger.
- Mentions Americans who deny climate change and want to "own the libs" will have to face climate change refugees.
- States that Americans must choose between owning the libs by destroying the environment and hating brown people.
- Raises the question of whether people value owning the libs more than addressing climate change.
- Indicates the need for major changes in infrastructure, consumption, and addressing climate change.
- Mentions legal precedent protecting developing nations from actions of developed countries.
- Notes that those most impacted by climate change are often in locations with small carbon footprints.
- Suggests that people impacted by climate change may seek more habitable locations.
- Urges moving beyond bumper sticker mentalities and working on real solutions to address climate change.

### Quotes

- "Do they enjoy owning the libs more than they hate brown people?"
- "We have to make major changes. We have to address our infrastructure."
- "Those who are going to be most heavily impacted by climate change are in locations that aren't really responsible."
- "Might want to think about dropping the bumper sticker mentality."
- "A little bit of legal precedent protecting those in the developing world."

### Oneliner

Americans must choose between owning the libs by destroying the environment or addressing climate change to prevent a future influx of climate change refugees.

### Audience

Climate change activists

### On-the-ground actions from transcript

- Address infrastructure and consumption (implied)
- Work on real solutions to climate change (implied)

### Whats missing in summary

The emotional impact of individuals facing displacement due to climate change is best understood by watching the full transcript.

### Tags

#ClimateChange #Refugees #Immigration #Infrastructure #LegalPrecedent


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about some good news for most of the people of the world.
I think it's good news, however, there will be a subset of Americans that will see this
as just a horrible paradox.
An immovable force meets an immovable object, two closely held beliefs that are suddenly
thrown in direct contradiction to each other and they're going to have to make a decision.
It's kind of funny to me.
It started in New Zealand.
Near New Zealand there is a country and this country is at risk of disappearing
due to rising sea levels.
So, a man fled to New Zealand and claimed that he had to stay.
The case went all the way to the UN and what the UN decided was that they could return
him but only because it's 10 to 15 years away before his country disappears.
He's not in immediate danger.
If he was in immediate danger, well, they're going to have to let him stay.
So for many Americans who are just dead set on owning the libs by destroying the environment
and refusing to see horror on horror's face and accept that climate change is an issue,
We're now going to have to deal with the fact that if they proceed with that line of thinking,
we will inevitably receive climate change refugees.
So the question then becomes, do they enjoy owning the libs more than they hate brown
people? Or is it the other way around? Because they're going to have to make
that choice. They can either accept horror on horror's face and realize that
we have to do something. We have to make major changes. We have to address our
infrastructure. We have to address consumption. We have to do a whole lot of
of things, or they have to get real use to press in one for English.
This is an issue that won't be resolved anytime soon.
But now there is a little bit of legal precedent protecting those in the developing world from
the actions of those in the developed and that's the messed up part about it.
The reality is those who are going to be most heavily impacted by climate change are in
locations that don't have a big footprint that aren't really responsible for what's
going on, but they're going to suffer for it, or they can come here or to other locations
that are going to be a little more habitable, at least for a bit.
What was seen as something that is just really far off is already making its way through
court system. We might want to think about dropping the bumper sticker
mentality and working on some solutions. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}