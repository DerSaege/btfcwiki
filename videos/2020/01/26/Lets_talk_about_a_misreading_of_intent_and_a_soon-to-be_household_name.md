---
title: Let's talk about a misreading of intent and a soon-to-be household name....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xuufeeqsd9A) |
| Published | 2020/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing Muqtada al-Sadr, a powerful cleric in Iraq, making strategic moves to elevate his position.
- Muqtada al-Sadr has a significant following, with a powerful family background and a history of political and ideological shifts.
- He has been involved in various movements, currently positioning himself as a nationalist and a populist.
- During the US occupation, he led the Mahdi and engaged in activities supporting Sunnis in Fallujah.
- Recent protests in Iraq against corruption and US presence have involved his supporters, who later pulled out before Iraqi Security Services intervened.
- Muqtada al-Sadr's recent statement about peaceful protests is being misinterpreted in Western media, omitting the temporary nature of his stance.
- He demands the removal of US forces, closure of bases, and airspace to warplanes while allowing diplomatic missions.
- Despite calling for peaceful options, he has reactivated his armed organization, indicating potential regrouping rather than surrender.
- Beau believes al-Sadr is sincere in seeking peaceful resolutions but suggests that failure to respond positively could lead to resumed active resistance.
- Al-Sadr's ability to mobilize a large movement raises questions about US presence in Iraq, with Beau stating that the US never should have been there.

### Quotes

- "Muqtada al-Sadr will probably become a household name."
- "The U.S. never should have been there."
- "He's at the head of a very large movement."
- "There's no reason for us to be there now."
- "He is handing this administration in a gift-wrapped package, an easy out."

### Oneliner

Beau presents Muqtada al-Sadr, a powerful Iraqi cleric making strategic moves amidst protests, urging the US to reconsider its presence for a peaceful resolution.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Support organizations advocating for the withdrawal of US forces from Iraq (exemplified).
- Join local protests or movements against foreign presence and corruption in your community (suggested).

### Whats missing in summary

Insights on the potential implications of US foreign policy decisions on the situation in Iraq.

### Tags

#Iraq #MuqtadaAlSadr #USPresence #ForeignPolicy #Protests #PeacefulResolution


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to do kind of a biography, get to
know somebody a little bit.
We're going to do it because of recent events and a
statement that he made that I think the West is misreading.
I think they're misreading his true intentions.
And I believe that he may be poised to become a household
in the U.S. because of that. So this isn't great man theory at work. Understand we're
going to be talking about one person but he's representative of a whole lot of
people. He's somebody that's been around a very long time that is just making
moves that is going to elevate his position I think. Okay so who is he? Who
is he? He's a cleric, he's a preacher, he's a very very powerful cleric in Iraq
and his name is Muqtada al-Sadr. Some people right now are going why does that
how familiar. Others go, I remember him. Okay, so his family, very powerful, highly
regarded. Some of his older family members were taken out by Saddam. He has
a lot of support and a lot of people following him. He's, along the way, over
the years has flirted with secularism, communism, nationalism. Currently, he's
positioned himself as a nationalist. He's really a populist at heart. He has power.
He wants more. He ran something called the Mahdi during the US occupation. Now
most people that were like, oh yeah, they were active in a place called
Seder City. They also were active in other places. They ran supplies to the Sunnis in Fallujah. They
had a newspaper that got shut down for inciting violence. They were very powerful. They officially
disbanded in 2008. So that kind of gives you some background on who he is. Now from there he's
received since then received more religious training became a little more
political a little more shrewd a little smarter over the last month month and a
half or so there have been demonstrations in Iraq about corruption
about the US presence about a lot of things his guys have been there they
recently pulled out and almost immediately Iraqi Security Services went in and broke up the protests.
You can read that one of two ways.
His guys are tough enough to keep Iraqi Security Services in check or
they were coordinating and cooperating. The answer is yes, probably both.
So, he pulled his guys to go to his own protest.
where he said that he wanted to do things peacefully and it was an end to
resistance. That's how it's being framed in a lot of Western outlets. Some are
getting it right, most aren't. And sure enough, he said that. An end to
resistance. That's in the statement. However, omitting the word temporary
seems like it could be an error. His demands haven't changed. It's a removal
of US forces, closure of the bases, closure of the airspace to war planes
and intelligence gathering operations, but does allow for diplomatic
missions from the US. That didn't change and he said he wanted to exhaust
all peaceful options, not that he was throwing in the towel. I think the piece
of the puzzle that is being overlooked is that three weeks ago that disbanded
armed organization, he called for them to be reactivated. With that piece of the
puzzle in mind, his guys have been a protest where people are upset and
disaffected with the government, what have they been doing? Recruiting. They've
been recruiting and they're now at the point where the Mahdi are going to be
reactivated. This does not seem like a call for peace in the sense that he's
given up. This seems like he's regrouping. He's getting his guys up to speed. I have
no doubt that he's sincere, he would like to do this peacefully, and he's giving
the US a great out, he's handing the president a statement that says, we want
to do this peacefully, we want a timetable for US withdrawal, which could,
I mean, it makes it look as though the president's diplomacy, for lack of a
better word, over there, worked.
It didn't, but it could be read that way, if the president takes
advantage of it. If he doesn't, I have no doubt that his guys will resume active
resistance. I think he's setting the stage for it right now. So Muktada al-Sadr
will probably become a household name. We'll paint him as a bad guy and he is,
No doubt
He's has done a lot of bad stuff, but he is at the head of a very large movement
He can mobilize a lot of people he has had that support very very long time the only time you saw people
Defect from him even in their darkest times
was when he issued a statement saying that
Looters could keep what they looted as long as they gave his guys a cut that upset people and some of them left
left, but other than that, he has maintained support for decades, for decades, and he seems
to be positioning himself to resume operations.
Now the obvious question is, should the U.S. withdraw?
U.S. never should have been there.
Never should have been there.
And yes, they should have withdrawn.
They should have withdrawn.
We should have withdrawn in 2004.
If we were going to go there, we should have got out early.
There's no reason for us to be there now.
There's really not.
Our main reasons for being there as far as foreign policy, well, they changed when Trump
sold out the Kurds.
From that point forward, we've got nothing there.
We really don't.
And he is handing this administration in a gift-wrapped package, an easy out, that's
going to make it look like they won.
But we don't know if the president will take it.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}