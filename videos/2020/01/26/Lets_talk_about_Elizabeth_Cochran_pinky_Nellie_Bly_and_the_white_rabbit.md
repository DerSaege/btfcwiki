---
title: Let's talk about Elizabeth Cochran, pinky, Nellie Bly and the white rabbit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZvzTsLG8iOw) |
| Published | 2020/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the story of Elizabeth Mary Jane Cochran, also known as Pinky, who pursued a career in journalism.
- Cochran conducted investigative reporting on factory conditions, particularly focusing on women in factories.
- Facing pushback from factories, she was reassigned to cover gardening and fashion shows, which she found unsatisfactory.
- Cochran decided to become a foreign correspondent and traveled to Mexico, but had to flee after advocating for an imprisoned journalist.
- Upon returning, she was relegated to covering fluff pieces but eventually quit.
- She joined the New York World, Pulitzer's paper, and made a significant impact with her investigative work.
- Cochran orchestrated being committed to an asylum on Blackwells Island to expose the neglect and brutality faced by women there.
- Her experiences led to reforms being implemented.
- Despite her impactful work, Cochran continued to seek new adventures, traveling around the world in 72 days and becoming an industrialist with patents.
- Cochran's willingness to embrace opportunities for adventure and follow them wherever they led was a key factor in her extraordinary life.

### Quotes

- "Because when she saw the white rabbit and had a chance for an adventure, she took it."
- "She followed the rabbit. She went where it led her."

### Oneliner

Beau shares the remarkable life of Elizabeth Mary Jane Cochran, known as Pinky, who fearlessly pursued journalism, exposed injustices, and embraced adventures wholeheartedly.

### Audience

History enthusiasts, aspiring journalists

### On-the-ground actions from transcript

- Advocate for reforms in institutions or systems that neglect or mistreat marginalized groups (exemplified)
- Embrace new opportunities for adventure and growth (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of Elizabeth Mary Jane Cochran's life, inspiring viewers to seize opportunities for change and adventure.

### Tags

#Journalism #Adventure #Reforms #Injustice #Inspiration


## Transcript
Well howdy there internet people, it's Beau again.
So tonight's one of those nights where it's really just for one of you but it should be
interesting for everybody.
Tonight we're going to talk about a woman named Elizabeth Mary Jane Cochran.
She went by Pinky when she was little.
She wanted to be a journalist and eventually she got on at the Pittsburgh dispatch.
She did a lot of investigative reporting about factories.
Women in factories, the conditions they were in, uncovered some pretty disturbing stuff.
Factory started to complain.
She got moved from doing that kind of reporting to covering like gardening and fashion shows,
you know, good stuff for women to cover.
She couldn't deal with that.
So she decides she's going to be a foreign correspondent, and she takes off and heads
down to Mexico.
Not too long later, she had to flee Mexico because she spoke up for a journalist who
had been imprisoned, at this point she's like 21, 22.
She gets back and they put her back doing fashion articles and stuff like that, fluff
pieces.
So she quits.
Eventually she winds up walking into the offices of the New York World.
That's Pulitzer's paper.
And they hire her.
Not too long after that, she checks into a boarding house.
And something's wrong.
She's up all night.
Everybody around her knows something's wrong.
She starts acting a little weird.
And the next morning people are really concerned she hasn't slept and she's like, I'm not crazy,
you're crazy, you never know what crazy people are going to do.
Eventually the cops come, they take her, she goes and sees a judge.
Next thing she knows, she is on Blackwells Island, which is an asylum.
She's there for 10 days before the newspaper comes and gets her.
The book that came out of that was called Ten Days in the Madhouse, and the whole thing
was staged.
She faked being ill so she could get committed, so she could uncover the neglect and the brutality
that women faced on this island.
Reforms were made because of what she did.
Now at this point, this is a cool life, right?
You can stop now.
But no, not too long after that, she takes the clothes on her back, a couple extra pairs
of underwear, an overcoat, and a bag of totalitries, and sets out to do something that Jules Verne
said could be done in 80 days, travel all the way around the world.
She did it in 72.
did it in 72. She became a leading industrialist, got two patents, one
something to do with a garbage can and one with milk containers. During World War
One she was over there covering it and got arrested, falsely arrested as a
a British spy. At the end of the day, why did she get to live such a cool life, do so
many crazy things? Because when she saw the white rabbit and had a chance for an adventure,
she took it. She followed the rabbit. She went where it led her.
people that have lives like that. That's the thing they have in common. It's
normally the only thing they have in common. Is that when they had the chance
they said yes and they went anyway it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}