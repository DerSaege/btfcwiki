---
title: Let's talk about China, pandemics, and conveying calm....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hj39fDQava0) |
| Published | 2020/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- China has quarantined a large city, which can be unnerving.
- Attended a pandemic prevention conference where worst-case scenarios were discussed to instill caution.
- Presenter emphasized the importance of waiting for demographic information on patients.
- Majority of patients with the virus were older and had pre-existing conditions.
- Virus seems to affect those who are already compromised.
- Not everyone infected had a fever, which is concerning as fever is a common screening method.
- Prevention methods include washing hands frequently, staying away from sick individuals, and sneezing properly.
- Surveillance and early action are key in controlling new threats.
- Media sensationalism can lead to panic and desensitization to real pandemics.
- Beau believes it is not yet time to panic or wear gas masks.

### Quotes

- "There's no reason to panic."
- "So just stay calm, do what you do, and let them do what they do."
- "I don't see anything to really panic about."
- "I really don't see this as the giant pandemic everybody's worried about."
- "It's guaranteed we are going to have a bad pandemic at some point in the future."

### Oneliner

Beau conveys calm amidst pandemic concerns, stressing the importance of caution and trust in authorities while debunking media sensationalism.

### Audience
Community members

### On-the-ground actions from transcript
- Wash hands frequently, stay away from sick individuals, and sneeze properly (suggested)
- Trust in authorities and follow their guidelines (implied)

### Whats missing in summary
Tips on staying informed and critical of media sensationalism during health crises.

### Tags
#Pandemic #Prevention #Media #Trust #Community


## Transcript
Well howdy there internet people, it's Bo again.
So I guess it is time for me to convey calm because my inbox filled up.
I understand the news is a little unnerving, but there's no reason to panic.
There's no reason to panic.
And we're going to go through some stuff that hopefully will convey calm.
If you are not aware, China has quarantined a pretty large city, and that can be unnerving.
So years ago, I attended a pandemic prevention conference, and it was put on by FEMA and
the CDC for security professionals.
Most of it, they kind of got a bad guest list.
the people that I knew that were there,
they didn't have facilities that they were in charge of.
They were contract.
But that's what most of the conference was about,
was about how to deal with it in a facility
that you were tasked with handling.
The thing is, there was one presenter.
She was really smart and related things very well,
provided some information that I'm going
to provide to you right now.
She was like, you're security professionals.
When you're talking to your client,
you paint the best picture possible, right?
No, of course, because that's not what we do.
Not when I did.
Don't do that anymore.
But you tend to actually give worst-case scenarios,
because you want to produce an abundance of caution
in your client.
You want them to understand that this can happen
if you don't do X. The job is to stop bad things from happening.
And then she pointed out that they are kind of like security
health professionals, and their client is the American people.
So they present scenarios that are possible,
but are kind of worst case.
And that's good, because it produces an abundance of caution.
The problem is the media gets a hold of that.
They run with it.
They bring in their own talking heads.
The next thing you know, panic has ensued,
and everybody's picturing that little monkey from outbreak.
not the case. The other thing she said was that before we produced our response we
should wait to get the demographic information of the first cluster of
patients, those that didn't recover because that's going to tell us a whole
lot about anything new because in the beginning they don't really know how to
treat it they're kind of guessing. So those are going to be the ones that are
to be hit the hardest. We have that information. Okay, one was 40 to 50, one
was 50 to 60, five were 60 to 70, two were 70 to 80, and eight were 80 and older.
More than half had diagnosed pre-existing conditions and serious ones like
Parkinson's, coronary artery disease, stuff like that, stuff that could
complicate any efforts to treat. More than half had known pre-existing. Based
on that, this probably isn't the doomsday pandemic. It looks like it's affecting
those that are already in some way compromised. Now this is assuming that we
are getting all of the information that's available with that they are
telling us everything that they know. Normally they do because they want to
produce an abundance of caution. Out of everything that I've seen there's only
one thing that's a little a little worrisome and that's that not everybody
had a fever and that's worrisome because that's the main way that people are
screened at major hubs of transportation that they look for fever.
The fact that you could be infected and not have a fever that that's a little
worrisome. But again, not enough to panic. Okay, so how do you prevent it? Same way
you do almost anything else. Wash your hands frequently. You know, if you see
somebody who appears sick, stay away. Keep your distance. Sneeze into a tissue or
the crease of your elbow. It's normal stuff. There was also something about not
handling animals, but I think that was specific to China. I'm not sure, but I
don't think that had anything to do with the US. So that's pretty much it. I don't
see anything to really panic about. They're pretty good at what they do. They
really are. They get a handle on things pretty quickly. They've realized that
surveillance of a new threat as it moves and as it grows is almost as
effective as developing new treatments because as long as you know what's going
on you can counteract it and some of the steps may seem dramatic such as
quarantining a pretty big city but it helps keep it under control. I really
don't see this as the giant pandemic everybody's worried about the problem is
with the way the media handles these things eventually we will have that
pandemic it's gonna happen we are going to have a bad pandemic at some point in
the future it's guaranteed the worry is that because the media handles every
possible pandemic and treats every situation as if it could just spiral out
control it any second that when that pandemic comes people are going to be
burned out. Kind of a boy who cried wolf thing. Based on this, I'm not a doctor, but
based on this, I don't think it's time to break out the gas masks. I don't think
it's time to really freak out yet. So just stay calm, do what you do, and let
them do what they do. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}