---
title: Let's talk about Ex-Marines and the impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=q7exZB1GJh8) |
| Published | 2020/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of "Once a Marine, always a Marine" and why it holds true.
- Touches on the significance of oaths taken by individuals in government or non-government entities.
- Mentions Edward Snowden and John Karyaku as examples of individuals who upheld their oath by blowing the whistle.
- Talks about the serious nature of taking oaths among people with honor and integrity.
- Draws parallels between the Marine Corps' iconic knife, the K-Bar, and the symbolism behind it.
- Questions the actions of a large group of people in the U.S. Senate who seemingly violated their oath to remain impartial.
- Expresses concerns about politicians prioritizing political advantage over truth, justice, and the Constitution.
- Raises doubts about whether politicians truly represent the interests of the people they serve.
- Criticizes politicians for potentially lying to the public about various issues, including war, social security, and the economy.
- Challenges the American people to question the integrity of their representatives and their tolerance for political misconduct.

### Quotes

- "Once a Marine, always a Marine."
- "Nobody can look at this and say that it was impartial."
- "What makes you think they won't lie to you about the economy?"
- "Has the United States, the people of the United States, has their honor and integrity fallen this far that they tolerate it?"
- "They don't care about you; these people aren't your representatives."

### Oneliner

Beau examines the lasting impact of being a Marine, the significance of upholding oaths, and questions political integrity and representation in the U.S. Senate.

### Audience

Active Citizens

### On-the-ground actions from transcript

- Contact your representatives and demand accountability for upholding their oaths (implied).

### Whats missing in summary

The emotional depth and passion in Beau's delivery can only be fully experienced by watching the full transcript.

### Tags

#Marines #Oaths #PoliticalIntegrity #Representation #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about ex-Marines
and what ex-Marines can teach us.
And there's a whole bunch of people
who clicked on this video for the sole purpose
of telling me that my title is wrong
because there is no such thing as an ex-Marine.
Once a Marine, always a Marine, right guys?
Outside looking in,
That sounds like some silly gung-ho slogan.
And it is a slogan of sorts.
But the basis of it is the idea that going through that
training changes you fundamentally as a person.
And you will never change back.
Therefore, you will always be a Marine.
The other side to that is that that oath didn't have an
expiration date, did it?
So you will always be a Marine.
That's the idea.
That's where it comes from, and it's true.
It is true.
Most entities, government or non-government,
that defend the United States believe what they're doing
to defend the United States.
They swear a very similar oath.
Even intelligence officers, even if they burn out, leave.
I'm not doing this anymore.
They've seen something or done something that offends their morals, and they leave.
But they don't often talk about it.
They don't often come forward.
It's because of that oath, it's because of that oath.
People are certainly going to point to Edward Snowden and John Karyaku.
Well they blew the whistle, they did, they did.
And there is certainly a moral element to what they did.
But there's also a constitutional one.
And that is what they swore an oath to.
There's certainly a constitutional element to what they did.
So I wouldn't say that that was a violation of their oath.
More like there's an exemption to it.
Because the main oath they took was to protect the Constitution of the United States.
The other ones fall under that.
There's an argument, there's a debate to be had there.
I stand is pretty clear. John's a Facebook friend. I don't believe he violated his oath. I believe
that he did what his oath demanded, and he did what his personal morals demanded.
Oaths were a thing that were taken very seriously. Still are, among those people with honor and integrity.
What's a K-Bar for those who don't know it's a knife?
For the Marines, it's THE knife, right?
Why is that such an icon of the Marine Corps?
Because it is the Marines, it's a symbol of them, it's a utility knife that is lethal,
durable, everlasting, can be forced into any role.
It is the Marine Corps, that's why it became the icon.
Is the knife really that good?
It is actually a really good knife, but aside from that, the symbol and what it says, that's
why it's become synonymous with the Marines.
The U.S. Senate is a symbol of every American.
represents us all, in theory. What does it say that a group of people in the Senate,
a large group of people in the Senate, seemingly openly violated their oath to remain impartial?
What does that mean? I mean, really, start thinking about it, because there are a lot
of ramifications to it, especially when you factor in the fact that it appears they suppressed
evidence, to defend a man they don't even like, most of them don't even like Trump,
but they saw it as politically advantageous to do so, why?
From where I'm sitting, and I could be wrong, but there's only two reads, it's probably
a combination of both in my opinion, they believe that their voters are so partisan
They have ceased to care about truth, about justice, about the Constitution.
Or they believe their voters are so stupid and so naive that they're not going to catch
what it really means.
They swore an oath to be impartial, just like they swore an oath to represent you.
Nobody can look at this and say that it was impartial.
It was political maneuvering, openly.
What makes you think they represent you?
They're going to continue to, or that they ever did.
If they will do this and violate their responsibility to remain impartial, this openly, for somebody
they don't even like, what makes you think they won't lie to you about how just a war
is so they can send your kid off to die so their stock in Raytheon or whatever can go
up two or three dollars.
What makes you think they won't lie to you about your social security that you paid into?
And say well you really don't deserve this.
It's an entitlement.
What makes you think they won't lie to you about the economy?
you that you're in the middle of a blue-collar boom. Wow, there's been a 20% increase in
family farm bankruptcies. I wonder if they got any big donations from corporate agriculture.
I bet they did. Didn't look, but I bet they did.
Senate being a symbol of the US and this happening so openly, what does it say for us?
How does that represent us?
Has the United States, the people of the United States, has their honor and integrity fallen
this far that they would tolerate it?
That they would look at this and say, we don't care that you wiped your feet on the Constitution
And as you went to cast your vote, we don't care that deep down we all know that the Republican
Party suppressing this evidence means that Trump is guilty.
Let's think about it.
This whole time they said that it was a sham, it was a political show, it was partisanship
in the House.
We need evidence.
And then when it's available to them, after they know what it is, they're like, oh no,
no, no.
see this. Don't want to enter this into the record. If that evidence confirmed Trump's
story, believe me, they'd allow it. They put it on Fox News on loop, but they're not doing
that. They're doing everything they can to hide it, to suppress it, because they don't
want you, the American people, to know what it is. Because that would hurt them politically.
that's what they care about power not you these people aren't your
representatives much like Trump they probably don't even like you and when
the time comes they will sell you out the same way they just sold out the
Constitution and their obligation to be impartial anyway it's just a thought
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}