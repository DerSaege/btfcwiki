---
title: Let's talk about the will of the people and a united America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kfYtJQpDPWI) |
| Published | 2020/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the will of the people and unity in America, addressing blame and personal responsibility.
- Analyzing the divisive portrayals by media despite widespread agreement on issues like Roe versus Wade.
- Only 13% of Americans want to overturn Roe versus Wade, while 77% agree with it.
- Republicans making a mistake by not truly representing the will of the people on this issue.
- Criticizing politicians for catering to a small base rather than following the teachings of Jesus.
- Pointing out the hypocrisy in anti-abortion stances and lack of support for policies aiding the less fortunate.
- Calling out the Republican Party for clinging to outdated ideas and policies.
- Emphasizing the exercise of power and government intrusion in personal matters.
- Advocating for following the teachings of Jesus, promoting love and empathy over blame and division.

### Quotes

- "77% of Americans agree on this topic."
- "They're doing it because it's a base, it's a base."
- "Love thy neighbor. Let's start there."
- "Atheists are inadvertently following the teachings of Jesus more closely than those people that scream that they're Christian."
- "The government has no business in this, none, none."

### Oneliner

Exploring unity, blame, and personal responsibility while critiquing political representation and advocating for empathy and love over division.

### Audience

Americans

### On-the-ground actions from transcript

- Follow the teachings of Jesus in promoting love and empathy in your community (implied).
- Advocate for policies that benefit the less fortunate and prioritize social welfare (implied).
- Challenge politicians who do not represent the true will of the people on critical issues (implied).

### Whats missing in summary

The full transcript dives deep into the political landscape, the hypocrisy within certain political stances, and the importance of empathy and love in societal interactions.

### Tags

#Unity #Blame #RepresentativeGovernment #PoliticalCritique #Empathy


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today, we're gonna talk about the will of the people.
We're gonna talk about a united America.
We're gonna talk about blame, personal responsibility.
We're gonna talk about following the teachings.
And we're gonna do that in the aims
of getting towards some root causes.
We'll do a little root cause analysis.
Because the die is cast, we know where we're headed in 2020.
We know what one of the major election issues is gonna be
because a whole bunch of Republicans, Senators, and Representatives, although I am loathe
to use that term as you are about to find out they're not representative of the people,
well they sent a letter.
And because of that, the media is going to do its job and they're going to paint this
issue as divisive.
The United States is divided, it's irreconcilable, we will never be able to work this out, we
need Big Daddy government to step in and save the day because we are at each other's throats
But we're not.
There's polling data on this.
A lot of it.
It's all pretty much the same.
Most recent one I found.
13% of Americans want Roe versus Wade overturned.
13%.
So for 13% of the population, the Republican Party got
together and ask the Supreme Court to revisit this.
Now, Roe versus Wade is symbolic.
There's actually a whole lot of other legal precedents involved.
But when people say Roe versus Wade, they're talking about abortion.
13% of Americans want it gone.
There were 10% that were unsure, undecided when that poll was taking.
So let's get them all to them, 23%.
23% of Americans want it gone.
We're just gonna say that every single person
that was undecided decided that they want it overturned.
They want it gone.
77% of Americans agree.
Can you name any other topic
that America is this united on?
This isn't a divisive issue.
I think the Republicans have made a grand mistake here.
There's a little bit of nuance to it.
26% want to keep it,
but add a few more restrictions.
16% want to keep it as is,
14% want to keep it with less restrictions,
and 21% want it like McDonald's.
Anywhere, anytime, for any reason.
They want it expanded.
So to be clear, there are more people
that want completely unfettered access
than want it overturned.
77% of Americans agree on this topic.
The government, the GOP, cannot pretend that they're being representative of the will
of the people on this one.
They have fallen into a trap that happens on social media.
Your most vocal supporters aren't actually representative of all of your constituents.
But the die is cast.
Here we are.
What's it about, really?
13%.
No politician does something for 13%, they're doing it because it's a base, it's a base.
They're going after that Christian theocracy, the good Christians.
But here's the funny thing, if they actually followed the teachings of Jesus, the demand
would be greatly lessened, maybe gone.
Jesus miracles, healing the sick, feeding people, helping the poor. Man, could you
imagine what would happen to the demand for those services if we had health care,
food security, and work to end poverty? If there was opportunity, some social
mobility, things might change. But see, actually following the teachings of
Jesus, well that's hard. That's hard. It's much easier to just find a scapegoat, to
find people to look down on, kick down instead of punching up, instead of
working. It's funny because most people that are adamantly anti-abortion are
older than I am. They're part of that generation that looks down at the
younger people and says that they're lazy and entitled. Meanwhile they want to
use the government's gun to do something that 77% of Americans don't want.
And at the end of the day, it's their fault.
It's their fault.
They turned a blind eye to the corporate takeover of the United States.
It's funny because they want to regulate everybody else's life in the bedroom, but they said
nothing when their representative or their senator hopped into bed with any company that
would cut them a check. And that's how the policies got made. And they just said nothing.
Played along because it was okay for them at the time. When you look at the Republican
party, and you look at their policies, you realize they're not very forward-thinking.
They're not looking to the future, they're trying to appease the past.
They're trying to cling to something that just doesn't exist anymore, and it doesn't
exist because it didn't work.
It didn't work.
And rather than admit that their ideas were wrong, well, they've got to find a scapegoat.
They've got to find somebody to blame it on.
The immigrants, a religious minority, a racial minority, wait, women want power?
Oh no, no, no, we've got to do something about that.
You need to stay in the back with your little baby lady brain.
Let us take care of it.
And that's what this is about.
It's about an exercise of power.
The government has no business in this, none, none.
We see that when the legislation gets proposed.
You have representatives at the state level proposing impossible medical procedures because
they don't understand it.
They have no idea what they're doing.
If you want a Christian society, fine.
But follow his teachings.
That'd be a pretty cool place, actually.
And the ultimate irony of this is, is that those that support the Christian right, or
not even the Christian right, those politicians that pretend they're part of the Christian
right to get their vote, those that support them, they're not following any of the teachings.
When you find out very quickly that most of those crazy liberal ideas that Jesus would
have supported, they're proposed by people who are a lot of times atheists, agnostic.
You've got to work pretty hard to hate so much that you want to scapegoat everybody,
anybody that's not like you. You want to deny power to anyone that isn't like you.
Atheists are inadvertently following the teachings of Jesus more closely than those people that
scream that they're Christian. You want that kind of society? It'd be a pretty cool place.
actual teachings of Jesus. Love thy neighbor. Let's start there. I know it's a lot harder
than just blaming people. I'm curious if the GOP has any policies that don't require a
scapegoat. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}