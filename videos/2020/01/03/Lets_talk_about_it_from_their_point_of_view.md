---
title: Let's talk about it from their point of view....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ux9GI5nxMVU) |
| Published | 2020/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Less than 24 hours ago, Beau warned of tensions escalating, but didn't expect the situation to reach this level so quickly.
- Soleimani, viewed as a hero in Iran, had a background that surprised many, including helping the U.S. in Afghanistan before stopping due to George Bush's actions.
- Soleimani, promoted to major general, worked alongside the U.S. in Iraq and Syria, credited with strategic successes in the region.
- Beau believes the U.S. and Iran are more similar than people realize, both idolizing military leaders and engaging in questionable tactics for their perceived ends.
- Soleimani's assassination by the U.S. is seen as a significant and unprecedented event that could lead to dangerous escalations and consequences.
- Beau expresses concern that Iraq may become the battleground for a conflict between Iran and the U.S., urging Congress to intervene and prevent further escalation.

### Quotes

- "This is going from 0 to 120 miles an hour."
- "Martyrdom is what he sought, and we gave it to him."
- "I don't foresee the IRGC, specifically, or Iran in general, taking this lying down."
- "Congress should not be cheerleading this on."
- "It doesn't matter that General Soleimani was a bad actor; the fallout from this is going to be pretty big."

### Oneliner

Less than 24 hours after warning of escalating tensions, Beau delves into Soleimani's surprising background, expressing concerns over the unprecedented fallout of his assassination and the potential for dangerous escalations.

### Audience

Congress, Activists, Policymakers

### On-the-ground actions from transcript

- Contact Congress to urge them to intervene and prevent further escalation (suggested)
- Stay informed about the situation in the Middle East and advocate for peaceful resolutions (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of Soleimani's background and the potential consequences of his assassination, offering a unique perspective on the unfolding events.

### Tags

#Iran #Soleimani #US #Conflict #Tensions #MiddleEast #Congress #Intervention #Peacekeeping


## Transcript
Well, howdy there, Internet people.
It's Bo again.
Wow.
Um, so less than 24 hours ago, I said, expect to see a
ratcheting up of tensions.
This isn't what I meant.
I expected us to get to the point we're at now, maybe 60
days.
I certainly did not expect this to happen 24 hours later.
This is going from 0 to 120 miles an hour.
This is a huge escalation.
The media keeps saying that, but they're doing a really bad job
of explaining why.
They're framing everything from the point
of view of the United States.
And it doesn't make any sense.
Why would this matter if all of this is true?
You have to understand it from their point of view, what
see when they look at him and what they see is he's a hero.
He's a hero.
So we're going to go through his background from their point of view.
Before I do this, I want to say he's a bad actor on the international stage.
He is a bad actor, he is responsible for a lot of morally questionable things, a lot.
He has certainly run afoul of U.S. interests a number of times.
But there's also going to be some stuff in here that will surprise you.
Okay, so Soleimani started off, he enlisted in 1979, right after the revolution.
So put the emotional tie with it.
This guy signed up as soon as the country existed.
He was like a plumber or something before then.
He was a tradesman of some kind.
He signed up at a normal tour, participated in putting down a rebellion in Kurdish territory.
Then 1980, the Iran-Iraq war pops off.
He forms a company himself from guys from his hometown, trains them, gets them ready,
and then leads him into battle.
He develops a reputation as being just absolutely fearless, but more importantly, he was successful.
Extremely successful.
He started as a company commander.
He ended as a division commander.
He goes from commanding 100, 200 troops to commanding thousands.
He's not even 30 years old.
their perspective he's amazing. He joins their elite forces and he rides out a few years with
nothing going on back in his hometown as kind of like the regional commander there. Then in 97 he
becomes commander of what amounts to like a blending of our CIA and Green Berets. It's kind
of both in one little force. He becomes commander of it and he excels at it, and I mean excels.
Much so that in 2001 when the United States needed help in Afghanistan, they went to him and he helped.
He helped target those people that were real bad actors in Afghanistan.
Now he did it because they also, not just did they go after the US, they also went after
some Shia sites.
That bothered him.
So he helped the US then.
And he was instrumental.
George Bush popped off with that axis of evil stuff and he stopped helping.
That should tell you something.
Get a little bit more insight into him.
had no place for this man. So, let's fast forward. 2011. He is described by the
supreme leader of Iran as a living martyr. He's promoted to major general.
He is instrumental in fighting alongside the United States in Iraq and Syria.
He's credited with being the architect behind the strategy Assad used to turn the tide.
In the Middle East, he's pick your famous general, Patton, whoever, whoever it is that
you idolize in the Middle East, it's him. It's him. And he worked with the U.S. a lot.
Now at the same time, he's not there for U.S. interests. He's there for Iranian interests,
which means he backed organizations the same way the CIA does because he was running their
version of it, kind of. He backed organizations that were questionable in a lot of ways and
used a lot of questionable tactics, but to them, the ends justified the means the
same way it does here.
One of the things that I really wish Americans understood was that the reason
Iran and the US can't get along is because we're a whole lot alike.
We idolize our military leaders so much so that this gentleman was encouraged to
run for president in Iran three, maybe four times, turned him down each time, and the
last time he said that he had no intention of doing it, that anybody saying that he even
considered it was spreading to divisive rumors and was out to undermine the revolution.
He was a simple soldier and would always be a simple soldier.
That's the kind of person that gains that respect, that gains that admiration.
He got wounded.
One of his famous quotes, martyrdom is what I seek, but the hills and valleys won't give
it to me.
This is a guy who exemplified the Iranian John Wayne.
He is in the hearts and minds of these people.
He's a hero to them.
He's a national legend.
You need to look at him the way we look at George Washington or any great general.
When you're talking about our standard history, the history that the government approves of,
when theirs is written, he's going to have a huge place in it.
We can't understate how, how this is going to impact them beyond that because of his
role as director of their foreign service, so to speak, he's respected and had connections
to literally every group that hates the United States.
We're not talking about whether they have a reason to right now.
We're talking about the fact that they do.
And he helped them in many ways.
Intelligence, training, money.
He's respected not just in Iran, but all over the Middle East.
Martyrdom is what he sought, and we gave it to him.
Now Trump's idea is that well, Iran's a bully and they just need to get punched in
the nose, and this is the punch in the nose.
Truthfully I don't know what's going to happen, neither does anybody else.
This has no precedent.
Nobody has ever done anything like this.
The reason nobody's ever done anything like this is that it's insane.
The response from this is probably going to be a wave of stuff that we don't want to deal
with because we didn't just upset Iran.
Now maybe, maybe he's right.
Trump is right and this is all going to blow over and Iran's going to be a lot
more cooperative now. History seems to indicate that it's not going to go that
way. My gut tells me this is a real bad move and this is going to escalate and I
think that this is going to take it further than they wanted. I've said
before, they wanted, this administration wanted this war for election purposes, I
think they may have just gotten a little bit more than they could chew.
I don't foresee the IRGC, specifically, or Iran in general, taking this line down.
down. I think Iraq probably just got turned into the battlefield for a war between Iran
and the U.S. and it may escalate from there. Congress should not be cheerleading this on.
They need to put a stop to it as soon as possible. It doesn't matter that General Soleimani was
bad actor the fallout from this is going to be pretty big and I don't think we're
for it. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}