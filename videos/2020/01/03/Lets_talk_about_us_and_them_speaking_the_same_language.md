---
title: Let's talk about us and them speaking the same language....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AtAyJt850Dk) |
| Published | 2020/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a storm approaching in the US and another place, with governments pumping people up for inevitable events.
- Points out similarities in how both governments are treating their people and using propaganda.
- Clarifies that he respects General Soleimani as a dangerous tool of Iranian foreign policy, even though he doesn't agree with him.
- Warns against underestimating one's opposition, stressing the fatal consequences of doing so.
- Criticizes the US administration for escalating tensions with Iran unnecessarily.
- Commends the security details at the embassy for handling the situation calmly and professionally.
- Expresses frustration at how the US administration's actions played into Iranian propaganda.
- Criticizes those who cheer for war without considering the real consequences or volunteering to fight themselves.
- Calls out the hypocrisy of politicians who support war but wouldn't personally participate in combat.
- Concludes by pointing out the similarities in behavior between the US and Iranian governments and the manipulation of people for political purposes.

### Quotes

- "You can't underestimate your opposition. That is a fatal error."
- "War isn't a spectator sport, gentlemen."
- "They're both doing what they can to energize their people. They need us. We don't need them."

### Oneliner

Both the US and Iranian governments manipulate their people for war, while armchair supporters fail to acknowledge the true costs of conflict.

### Audience

Citizens, policymakers, activists

### On-the-ground actions from transcript

- Challenge war narratives and propaganda by engaging in critical discourse with peers (implied)
- Advocate for peaceful resolutions and diplomacy in international conflicts (implied)
- Support organizations working to prevent war and violence through peaceful means (implied)

### Whats missing in summary

The emotional impact of war propaganda and the human cost of political manipulation.

### Tags

#WarPropaganda #PoliticalManipulation #InternationalConflict #Diplomacy #Peacebuilding


## Transcript
Well howdy there internet people, it's Beau again. I normally don't record when
the weather is like this. It gets noisy at times. We got a storm coming in but
it seems fitting today to just go ahead and do it because there is a storm
coming. Right now, in both places, in both places, in the US and over there, the
governments are doing the same thing. They're pumping their people up, getting
them ready for what seems to be inevitable. And both sides are being
treated to the same type of thing from their media, from their politicians. It's
It's happening because, as I've said before, we're a lot alike.
We speak the same language most of the time.
And that's a big source of a lot of our problems.
Now I want to address a question that I got about that last video before we get into it.
Somebody said it sounded like I admired General Soleimani.
No, no, I respect him, I respect him.
See he's a tool, he was a tool, and I don't mean that as an insult, he was a tool of Iranian
foreign policy, he was an implement.
His role was to be dangerous, that was his job, he was a dangerous tool, and like any
dangerous tool, be it a chainsaw or anything else, you'd better respect it.
Because if you don't, you're going to get bit.
Us not respecting the amount of influence that this man had is going to bite us.
Yeah, yeah, I do, I respect him, I don't agree with him.
We're not on the same team most of the time.
But you can't underestimate your opposition.
That is a fatal error.
It's always a fatal error.
And it's something that we've done.
So both sides right now getting the same kind of stuff.
We got to get ready for it.
It's going to happen.
The storm is coming.
sides of being fed that same propaganda. It's time for war. There's no other choice, there's
no other option. There is. There is. And see, what has happened is that this administration
in the United States has escalated at every provocation. And the Iranian government has
has escalated at every provocation and here we are and it's infuriating to me because
as mentioned in the video about the embassy, they did what they were supposed to do.
Those security details, they did what they were supposed to do.
They kept their cool.
They kept their calm.
They denied their opposition, those headlines, and that propaganda they need to feed the
people at home.
They denied them that because they did what they were supposed to do.
They stayed cool.
They stayed less than lethal.
And then along comes the administration and snatches defeat from the jaws of victory.
We won that engagement.
And because of Trump's need to feel like a tough guy, we lost it after the fact.
Because they got their headlines, even more so.
It wasn't U.S. Marines open fire on civilians.
We assassinated a national icon.
They got their propaganda.
They got what they needed to energize their side.
And over here, yeah, people are cheering it on.
People who have no idea who he was, just going off of what they heard, well, he killed thousands.
Yeah, you realize that a whole lot of those were actually the same people we were fighting.
Didn't know that though, did you?
We speak the same language.
We're going to behave in the same manner.
So understand that as American politicians cheer for blood, that they're not going to
have to spill and they're not going to have to shed the same things happening over there.
While we're arguing over who's going to win, and we say we, we're going to win.
And most of the people saying that aren't going.
It's a popular thing right now.
Well why don't you move to Iran if you don't support the war?
No, you support the war, you move to Iran.
For a year at a time.
I got a recruiter's number for you.
You go fight it.
Now you don't want to do that.
You want to send some 19 year old kid that doesn't know any better.
Send him off so you can claim the glory and wave the flag and put a yellow ribbon on your
sticker and act like you're part of the team. War isn't a spectator sport, gentlemen. You
want to do it, go do it. But one thing that I really hope people can take away from this,
from this whole incident, is that both governments are behaving the same way. And I don't mean
that in some moral equivalency. I mean it in a wider sense. They're both doing what
they can to energize their people. They need us. We don't need them. Anyway, it's just
a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}