---
title: Let's talk about what Parnas gave up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m-Pe7IljLXM) |
| Published | 2020/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the release from the House Intel Committee, Beau points out the significance of the information and urges everyone to read through it.
- Beau expresses the gravity of the allegations presented in the release and stresses the importance of not letting them be overlooked.
- The transcript delves into specific notes and messages found within the released documents, shedding light on potential misconduct and abuse of power.
- There's a focus on the communication between individuals regarding surveillance of a US ambassador, raising concerns about security breaches.
- The involvement of Giuliani as the president's personal attorney in matters beyond the usual scope draws attention to potential misuse of power.
- The transcript hints at possible foreign involvement and surveillance of a US embassy, sparking questions about the intentions and implications behind such actions.
- Amidst speculation and uncertainty, Beau underlines the need for further investigation and clarification to fully comprehend the situation at hand.
- The ongoing surveillance and monitoring of the ambassador's movements indicate a serious breach of security protocols.
- The release of the House Intel Committee prompts Beau to encourage thorough understanding and engagement with the information provided.
- Beau acknowledges the shady and concerning nature of the revelations, prompting a call for transparency and accountability in addressing the allegations.

### Quotes

- "There are some pretty serious allegations that could be made from this."
- "It certainly appears that somebody is conducting surveillance on a US ambassador. That's pretty wild."
- "It certainly appears shady. I believe it was. But we need more."
- "Interesting times, isn't it?"
- "Anyway, it's just a thought. Y'all have a good night."

### Oneliner

Beau dives into the House Intel Committee release, uncovering alarming allegations and stressing the need for transparency and accountability in the face of potential misconduct and security breaches.

### Audience

Political enthusiasts, concerned citizens

### On-the-ground actions from transcript

- Reach out to representatives or investigative bodies for further inquiries into the allegations (suggested)
- Stay informed and engaged with updates on the situation (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the House Intel Committee release, urging readers to seek more information and stay updated on unfolding events.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about
what the House Intel Committee released.
Because, wow, that's a wild ride, isn't it?
If you haven't gone through it yet, understand,
you need to, it's 38 pages, link is gonna be below.
If you've been following this channel for any length of time,
you know that the fact that I'm putting the link down there
should tell you, you need to read it.
We're gonna go through what I think's important
in it, and I'm going to kind of give you a hot take on it.
Understand, there are things that
could happen that could change my read on this.
This is my opinion.
There are a lot of facts that are unknown.
There are things that could add more context.
But regardless of the full story as it develops,
this is something we have to talk about,
and we have to make sure it doesn't get swept under the rug,
because there are some pretty serious allegations that
could be made from this.
I will also say that it certainly
does appear to draw a circle and then a line straight
to the Oval Office, as far as the people that are involved.
OK, so on page one, there's that note.
It's on Ritt's letterhead.
And basically, it's a to-do list.
And once again, it's announced, it's saying that they need to get the president of Ukraine
to announce the investigation into the Bidens.
Every time it shows up, that word's there.
We've talked about it on this channel before.
It's the one thing to me that shows that this was purely political.
They never really worry about the investigation actually being conducted.
They just want it announced.
looking for a scandal, not for justice. Okay, page three, the same handwriting,
notes, it says get rid of get rid of somebody, get rid of Davis and in
parentheses nicely. I don't really want to speculate too much on that, I just want
to mark that I think that's going to turn out to be really important later.
On to page seven, this whole time we've been told that Giuliani is the president's personal attorney.
On page seven there's a message that appears to come from him that in regards to a declined visa,
he says, I can revive it.
That's a whole lot of authority to give a personal attorney
the ability to sway whether or not somebody gets a visa
to enter the country.
That may be a little beyond the authority
that a personal attorney should have.
And that may become important when
it comes to, let's just say, piercing the veil later.
Now, here's where it gets wild.
Scroll down to 17.
And this is a conversation between Parnas and Robert F.
Hyde.
And it starts off with, wow, can't believe Trump.
There's a typo.
But it's Trump hasn't fired this B word.
I'll get right on that.
There are a whole lot of people that seem to have a lot of
influence over, you know, ambassadorships.
That seems odd, but whatever.
a couple of attachments, and then this is where it gets just,
this is where eyebrows start getting raised.
She's under heavy protection outside Kiev,
and that was sent to March 23rd.
Parnass responds, I know crazy.
Robert Hyde, my guy thinks maybe FSB,
let me stop right there, his guy's wrong.
The odds that the FSB, that is Russian, by the way,
the odds that they would be protecting a US ambassador,
and that is the widely accepted context for this,
is that this is a discussion about Yovanovitch.
The chain of events required for the FSB
to be protecting a US ambassador is even crazier than this.
I see that as highly unlikely.
There's a number of reasons this could have been said.
If his guy, whoever his guy is, is Russian and he tags people that he sees as,
um, let's just say federal law enforcement, that would be how he said it.
It would be comparable to an American saying, well, the feds are there.
He said the FSB.
Or he could mean SBU, which is the Ukrainian version.
Another very, very unlikely scenario is that they have contractors who were former FSB.
But again, that's, it's almost impossible.
Your most likely read on this is that his guy, whoever his guy is, is Russian.
and saw people in suits,
and that's how he relayed the information.
It's probably just a language thing.
There's a lot of people making a big deal out of that.
I would find it really unlikely
that the FSB was involved.
It doesn't seem to be grounded in reality.
Okay, so there's some more news clippings
and attachments back and forth,
And Parnas just responds with interesting.
My guess is that Parnas understood what I just said.
OK.
And then we get back to the conversation.
They are moving her tomorrow.
The guys over there, assuming that's a typo for there,
asked me what I would like to do and what is in it for them.
And then, wake up Yankees man.
She's talked to three people.
Her phone is off.
Computer is off.
She's next to the embassy, not in the embassy.
Private security, been there since Thursday.
It certainly does appear that somebody is conducting surveillance on a US ambassador.
That's pretty wild. Parnass responds again, interesting. Robert F. Hyde. They
know she's a political puppet. They will let me know when she's on the move. And
they'll let me know when she's on the move. Parnass, perfect. Back to Hyde.
I mean where if they can find out. The address I sent you checks out. It's next
All of that, yeah, that's really shady.
And the idea that somebody is conducting surveillance on a U.S. embassy is terrifying.
Because it means one of two things.
You're talking about either straight goons that just do not know any better, or real
pros.
Embassies are like a giant camera facing out.
I can assure you there is photographic evidence of whoever this was.
Back to Hyde, update.
She will not be moved.
Special security unit upgraded force on the compound.
People already aware of the situation, my contacts are asking what is the next step
because they cannot keep going to check. People will start to ask questions.
If there was ever a wonder about whether or not this was something secretive,
well there's your answer.
If you want her out, they need to make contact with security forces from Ukrainians.
What's the word, bro? Any good stuff?
you soon in studio. Let's go, Holmes. RG was good, but Ingram had some hard
questions. Nothing has changed. She is still not moving. They checked today
again. Hi, buddy. It's confirmed. We have a person inside. Parnass sends a news
clip or something. Back to hide. Nice. Hey, brother, do we stand down? Or you still
need intel be safe she had visitors it's confirmed we have a person inside hey
bro tell me what we're doing what's the next step there's a lot of speculation
about that everything from causing her harm to just intimidating her there's a
whole bunch of stuff and yeah there needs to be there needs to be answers to
the exact context of this.
It certainly appears shady.
I believe it was.
But we need more.
We need to find out exactly what this meant.
Because there are some huge allegations from this.
And I definitely suggest you read the rest of it.
It lays out a lot of other stuff.
And I'm sure we will get more into this again later this week.
Interesting times, isn't it?
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}