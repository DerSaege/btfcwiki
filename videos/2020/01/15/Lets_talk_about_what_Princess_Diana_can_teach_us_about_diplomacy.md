---
title: Let's talk about what Princess Diana can teach us about diplomacy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hnbJDxM3G_4) |
| Published | 2020/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring Princess Diana's impact on diplomacy and humanitarian efforts.
- Princess Diana's influence as a trendsetter beyond fashion.
- How Princess Diana's actions in Angola drew attention to landmines.
- The success of Princess Diana's call to end the use of landmines.
- The devastating impact and hidden danger of landmines.
- Comparing the Trump administration's foreign policy to "landmine diplomacy."
- The need for future administrations to address the impacts of current foreign policies.
- The importance of humanizing those impacted by foreign policies.
- Mentioning countries impacted by the current administration's foreign policy.
- Beau's call for future administrations to navigate the "landmines" left by the current administration.

### Quotes

- "She had walked through social minefields."
- "She humanized them, brought them into the discussion."
- "The decisions that this administration is making on the world stage, a lot of them, they won't be around to deal with the mess."

### Oneliner

Exploring Princess Diana's impact on diplomacy, contrasting it with the Trump administration's "landmine diplomacy," and calling for future administrations to humanize those impacted by current foreign policies.

### Audience

Diplomats, policymakers, activists.

### On-the-ground actions from transcript

- Reach out to countries impacted by current foreign policies, humanize the affected individuals (exemplified).
- Advocate for the removal of landmines and support initiatives to address their devastating impact (exemplified).

### Whats missing in summary

Further insights on the importance of empathy and human connection in diplomatic efforts.

### Tags

#PrincessDiana #LandmineDiplomacy #ForeignPolicy #Diplomacy #HumanitarianEfforts #Empathy


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about the Princess of Wales,
Princess Diana.
We're gonna talk about her
and what she can teach us about our current diplomacy
and hopefully our future diplomacy.
Now, for the younger people,
you may not understand, she was a phenomenon.
She was something else.
And we're not just talking about the normal poison grace that goes along with being a
royal and all that nonsense.
She was a trendsetter in a lot of ways, and I'm not talking about fashion.
I think that we owe the trend of celebrities showing up in the developing world trying
to help and it being publicized to her.
I think that every image of Angelina Jolie or Kristin Davis wearing Kevlar is because
of her.
This could just be my perception of it, but to me it seemed like it started with her.
She had walked through social minefields.
She had navigated the minefields of the British tabloids.
But in 1997, in January, in Angola, she walked through a literal minefield and drew attention
to it.
A lot of very powerful imagery came out of it.
And there she is, wearing body armor.
It was kind of shocking to the world.
And today, in 1997, on this date, she put out a call to end the use of landmines.
160 countries answered that call and eventually signed on to the treaty.
I think it was successful because she humanized those that were impacted by it.
And for those that don't understand why it's a big deal,
landmines are horrible.
The way it works, you put out a bunch of them.
Put out 20 in a field, they're denial of area.
You don't want your opposition coming that way.
So you put out a bunch, but only one or two goes off.
And once they do, the opposition turns around.
They find another way to go.
What's that mean?
18 or 19 of them are still there.
Eventually, whatever the dust up is, comes to a close.
Five years later, some kid is playing soccer, and he finds it.
And he never plays soccer again.
She talked to those people.
those that were directly impacted by it.
She humanized them, brought them into the discussion.
And that's why her call was successful.
That's why she succeeded.
Um...
And when I was thinking about this today,
it kind of...
stream of consciousness, I started thinking about something else I've been trying to think of,
a good term
for this administration's foreign policy.
You know, a lot of administrations have cool names
for their foreign policy.
You know, Teddy Roosevelt had gunboat diplomacy.
The Trump administration has landmine diplomacy
because it works the same way.
They're putting it out there, it's covered up,
and unless you're really paying attention, you don't see it.
You don't see what's going to happen
you're really looking for it. See, what's going to happen is in a few years, some
other administration is going to come along and they're going to step on it.
I think that's probably the most accurate term for this administration's
foreign policy, because it's, and it's global, they're putting out these land
minds everywhere. And the only solution is for the next administration to be like Princess
Diana and go to all of the people, by people I mean countries, that were impacted by this
administration's foreign policy and humanize them and bring them into discussion. And when
And when I'm talking about this, I'm not just talking about the ones that we think of immediately.
Yeah, sure. Iran, Iraq, Syria, Ukraine.
Those are the ones that we can name easily.
But remember the beginning of the administration.
Every country, pretty much, south of our border, impacted by landmine diplomacy.
any nation that hosts a military base, any nation where we are trying to develop indigenous partners.
They're all going to have to be reached. They're all going to have to be brought into the discussion
because they've all had landmines buried around them.
The decisions that this administration is making on the world stage, a lot of them,
they won't be around to deal with the mess.
It's going to be some other administration.
And I hope that they have the courage to walk through that land, that minefield.
But the sad thing about it, the sad thing about this analogy, 160 countries answered
that call, got rid of landmines, the U.S. wasn't one of them.
But anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}