---
title: Let's talk about updates, plans, and replacements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MbLB1opzHFQ) |
| Published | 2020/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Gyan says:

- Trump's unprecedented gamble of targeting Soleimani didn't pay off as expected.
- Ismail Khani, Soleimani's replacement, has a similar ideology and background.
- There has been a massive escalation in tension with no change in Iranian doctrine.
- Reports suggest another strike post-Soleimani, but details are unclear.
- Speculation surrounds the recent events, with nothing confirmed yet.
- Beau questions if the US actions imply a state of war with Iran.
- The approach to destabilize the opposition seems different from typical militant tactics.
- Beau compares US and Iranian actions in international relations.
- He anticipates no de-escalation despite calls from allies.
- Beau predicts a potential increase in operations targeting American civilians by Iran.

### Quotes

- "We have reports that there was yet another strike in addition to the one on Soleimani."
- "Morally, tactically, strategically, it's all the same thing."
- "Not a lot of, it's kind of dry, sorry, but anyway, it's just a thought."

### Oneliner

Trump's gamble targeting Soleimani didn't work, leading to increased tension with Iran and uncertain future actions, including potential targeting of American civilians.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor developments and stay informed on the situation (implied)
- Advocate for peaceful resolutions and diplomacy in international conflicts (implied)

### Whats missing in summary

Context on the potential consequences and escalation of tensions in the US-Iran relations.

### Tags

#US-Iran #ForeignPolicy #Tensions #ProxyWar #Analysis


## Transcript
Well, howdy there, internet people.
It's Bo Gyan, we've got a little update, a little situation report on what's
going on over there, so we're going to kind of go through that, so tonight we're
going to talk about defiance plans, plans changing, um, replacements and
equivalencies, so the big question, even among people who, this is their field of
study? Is Trump's gamble going to pay off? Is it going to work? He did something
unprecedented that many viewed as just absolutely crazy, but at the same time
there were a whole lot of people going, well it's never been done before, we
can't really make a prediction on what's going to happen. So most of us, most
people who are up to speed, at least somewhat on that topic, believed it was a
bad idea. We got that confirmation with the selection of the replacement for
Soleimani. They chose Ismail Khani. Ismail Khani is basically Soleimani. Same guy. I
mean it's a different person but Khani was trained by Soleimani. Khani was
trusted by him completely. He ran the eastern operations on his own, has the
same doctrine, very much the same ideology. Nothing changed. So there's been
a massive escalation in tension and there has been no change in Iranian
doctrine, which is kind of what a lot of people expected, but we didn't know.
I mean, maybe, you know, there's no way to tell when you do something that unpredictable.
So the plan was to do something unpredictable and shake up Iran.
It didn't work.
It did not work.
We have reports that there was yet another strike in addition to the one on Soleimani.
The reports are fuzzy, to say the least.
We've seen it said that there was actually Iraqis who were hit.
We've seen it said that it was a medical convoy.
My friends are telling me that it was a finance officer for the Iranians who kind of, he was
a bag man.
He handed out cash is what he did and that there were de facto Iraqi military with him
when he got hit.
Is that true?
We don't know.
There's a whole lot of speculation about what's going on.
To my knowledge, nothing's been confirmed yet.
But that's yet another, it's not really an escalation, it's a continuation of this pattern,
which is at this point, if this does turn out to be the US, that hasn't been confirmed
yet either, but we really, I mean, come on.
We would have to assume that we now exist in a state of war.
A state of war exists.
It may not be the kind of hot war that everybody imagines because we're not used to war starting
this way.
We normally have a massive propaganda run up rather than just a bunch of events.
Some people have questioned whether or not this is them using the destabilization technique
that is often used for militant organizations.
I'm going to say no, because a key piece of that is that the opposition can't know you're
doing it.
This just seems like them taking targets of opportunity after taking out somebody that
they had intended to take out.
So that's where we're at.
Now, a lot of people in these things, when I say terms like bad actor or something like
that, people are, well, the U.S. does the same thing.
I'm going to try to approach this topic from a very academic and clinical standpoint.
If you're watching this channel, I assume you know the U.S. does the same thing.
I don't think that I'm going to need to say that in every video.
Bloody Gina is our director of CIA.
I mean, come on.
If you don't know who she is, look up John Karyaku, he'll fill you in on it.
We do have the exact same type of tools, implements, like Soleimani, like Connie, out there.
It is a part of modern international relations.
We do the exact same type of thing.
Some people will take issue with that and say, well, we don't fund those types.
Yeah, we do.
It's the same thing.
The only difference in how you view it is determined entirely based on what flag was
flying outside the hospital you were born in.
Morally, tactically, strategically, it's all the same thing.
It's using proxies to destabilize your opposition, to hinder your opposition, and to further
your cause.
We do it all the time.
So that's where we're at, I don't foresee any de-escalation, even though our allies
are calling for it, saying we're messing up, I would agree with them, I don't think that's
going to happen.
I think the president is going to go down this course unless he gets a serious backlash
in the polls, because to me, that's what this is about.
That is what this is about.
There aren't many advisors that would have told them this was a good idea because of
the response it would generate from Iran, and here we are.
Nothing changed in their doctrine.
We've just become a target, and I do believe that Iran's response to this will be the green
lighting of a lot of operations targeting American civilians.
That's where I think we're at.
I don't think that they're going to want to provoke or initiate a conventional fight.
I think they're going to be okay with using Iraq and the rest of the Middle East and parts
of Europe as the battleground for a proxy war between Iran and the United States.
Again, all of this is real early.
The airstrike information is tentative at best, but this is where we're at.
So not a lot of, it's kind of dry, sorry, but anyway, it's just a thought, y'all have
a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}