---
title: Let's talk about what Sherlock Holmes, the impeachment, and dogs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JclKjpmhj2Q) |
| Published | 2020/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Explains the power division between the House and Senate in impeachment proceedings.
- Democrats bet the 2020 election on impeaching the president with their best evidence.
- Polls show a significant portion of Americans support impeaching and removing the president.
- Mitch McConnell and the Republicans in the Senate have the power over the trial.
- Beau questions why Republicans are not aggressively dismantling the impeachment narrative.
- Draws parallels to a Sherlock Holmes story about a silent dog revealing a negative fact.
- Suggests that the Senate's behavior indicates they may recognize the guilt but are not exposing it.
- Questions why the Republicans are not seizing the chance to destroy their political opponents with weak evidence.
- Speculates that the Senate is intentionally downplaying and hiding proceedings.
- Beau concludes with a thought on the Senate's behavior.

### Quotes
- "Sometimes a negative fact can lead you to the right question."
- "I'm fairly certain that our senatorial dog recognizes the guilty person."

### Oneliner
Beau questions why Republicans in the Senate are not aggressively dismantling the impeachment narrative despite the weak evidence, drawing parallels to Sherlock Holmes' silent dog. He speculates that they may recognize the guilt but choose not to expose it.

### Audience
Political observers

### On-the-ground actions from transcript
- Watch and stay informed about the impeachment trial proceedings (implied)
- Engage in political discourse and analysis with others to understand different perspectives (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of the impeachment trial dynamics and questions the Senate's handling of the situation, urging viewers to critically think about the political strategies at play.

### Tags
#Impeachment #Senate #Republicans #MitchMcConnell #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about what Sherlock Holmes can teach us about the impeachment
trial because I don't hear any dogs barking.
Before we get into that, let's get some facts on record.
The House has sole power over impeachment.
The Senate has sole power over the trial.
When it was in the impeachment phase, the Democrats produced their best evidence.
They went all in on this, and in a lot of ways, they bet the 2020 election on it.
And they did it well.
Current polling shows about half, in some cases more than half, of Americans want the
president impeached and removed.
That's substantial.
And now it's in the Senate's hands.
The Senate is run by Republicans, Mitch McConnell, and he's not barking, he's not barking.
At this point in time, when the polls are showing what they show, there's nothing to
lose by doing what they said they were going to do during the impeachment phase.
Destroy this narrative, this flimsy evidence, this horrible testimony.
Destroy it.
And make everybody see you destroy it.
But that's not what's happening.
They're crafting rules that make it almost impossible for the average American to see
what's going on.
It's crazy, it's very odd, because at this point, if what the Republicans have been saying
is true is true, they can completely cement a 2020 victory. They can destroy this narrative,
show that the Democrats just politicized this whole thing, and win in a landslide.
Instead, that's not what's happening. That's not what's happening at all. The dog isn't barking.
In one of Sherlock Holmes' stories revolves around a horse getting stolen and there was
a dog in the area, a guard dog.
And when Holmes goes through and he does all of his interviews, nobody mentions the dog.
When they're recounting the events of the night, nobody mentions the dog.
It's the curious incident of the dog in the nighttime.
Dog didn't do anything curious in the nighttime.
That's what's curious.
He should have barked.
So it's called a negative fact.
It's something that you expect to be there that isn't.
Sometimes a negative fact can lead you to the right question.
that can lead you to the answer. In our story, the dog didn't bark because he knew the guilty
person. Guilty person came in, recognized him, didn't bark. Why would he? I'm starting
to think that our senatorial dog recognizes the guilty person, doesn't want to draw attention
to it.
Why would he?
It's the only explanation for what they're doing.
Have you ever known any politician of any flavor to pass up an opportunity to completely
destroy their political opponent?
If the evidence is as flimsy as the Republicans keep saying, and the narrative is
that easily shattered why would they not do that prime time instead they're
trying to downplay it trying to find a way to hide the proceedings what it
seems like
I'm fairly certain that our senatorial dog recognizes the guilty person.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}