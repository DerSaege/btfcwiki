---
title: Let's talk about the end of capitalism and Davos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NMdZSbfjLNI) |
| Published | 2020/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the topic of the World Economic Forum at Davos and its focus on the "peak decade."
- Mentions the negative impact of tariffs, with studies showing that American citizens, importers, and consumers bear the cost.
- Predicts the decline of central banks' effectiveness in handling financial crises.
- Foresees peak oil in 2035 leading to industry failures and job losses.
- Talks about the expected decline in car production due to urban infrastructure and a shift to electric and renewable energy.
- Addresses climate change and its economic implications, including strains on infrastructure.
- Points out the demographic shift towards more older people than younger, impacting social safety nets like social security.
- Mentions a Stanford professor predicting a population decline due to economic factors affecting birth rates.
- Raises concerns about income inequality and the potential end of capitalism, with CEOs focusing on stakeholders over shareholders.
- Suggests a shift towards socialism in business practices as a response to income inequality.
- Predicts the end of globalization with countries turning more protectionist.
- Forecasts a choice between state capitalist systems or open society systems with free markets for countries like the United States.

### Quotes

- "This really is a let's talk about it video."
- "The people who attend these things are pretty smart, and they find a way to cushion the negative effects of all of this."
- "It's just going to be bad."
- "Some of the brightest economic minds in the world are pretty much at this point saying that socialism is inevitable for a whole lot of countries."
- "I cannot wait to see the comments section, especially the economists that follow this."

### Oneliner

Beau introduces the World Economic Forum's focus on the "peak decade" with insights on tariffs, central banks, peak oil, climate change, income inequality, and the potential shift towards socialism and protectionism.

### Audience

Economists, policymakers, activists

### On-the-ground actions from transcript

- Attend or follow updates from economic forums for insights and potential solutions (implied)

### Whats missing in summary

Insights on how global economic trends are shaping the future and the potential need for shifts in economic systems.

### Tags

#WorldEconomicForum #Davos #EconomicTrends #Tariffs #ClimateChange #IncomeInequality #Socialism #Globalization


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we're going to talk about the World Economic Forum
at Davos and what they're talking about.
If you don't know what this is, it's basically
where the world's brightest economic minds get together,
and they talk about the issues they see coming.
They have described this year's forum as talking
about the peak decade, because a lot of things
that we are used to are going to end.
That's their prediction.
This really is a let's talk about it video,
because I will say I'm not an economist.
This is not my area of expertise.
This is something I follow and I enjoy following.
But normally when I make these videos,
actual economists come along in the comment section
and provide clarification and nuance to what I'm saying,
because I can give you broad strokes and not details.
So what are they going to talk about at the World Economic
Forum?
First, tariffs.
They're bad.
They're taking a toll.
We all knew this.
We talked about this on this channel repeatedly.
The Federal Reserve, Princeton, and Columbia
recently released a study that found
About 100% of the cost of the tariffs
were paid by American citizens, importers, and consumers.
There have been so many studies on this
that the Wall Street Journal recently
ran an article that said, how many of these studies
do we need?
That was pretty much, I think that was actually the headline.
How many tariff studies do we need?
Something along those lines.
This is creating worry among them because it slows economic growth and it's bad for all countries,
not just the country imposing the tariffs. Central banks are going to kind of be done.
They were successful in stopping the Great Recession of 2008 from turning into a Great
depression but now they're kind of maxed out. If there is another financial crisis, which there
will be eventually and we're already seeing signs that they kind of indicate it could happen at any
moment, states will have to loosen monetary policies and change their policies about this
and that basically central banks are kind of, are kind of on the out.
Peak oil is expected in 2035.
That means there are a whole lot of industries that are based around that, that are going to fail
because the demand is going to decrease, therefore production will decrease,
Therefore, businesses will go out of business, and people will lose jobs.
Add into that, car production is expected to decline, partly because of the emergence
of megacities that have the infrastructure to allow people that live in them not to have cars.
And then on top of that we will see the switch to electric and renewables. So all
of those companies that were too big to fail will start failing. Climate change.
We are passing bad waypoints left and right and we do not expect to see net
zero for another 30 years. And this is obviously going to cause economic strain on infrastructure
because of natural disasters and so on and so forth.
There are going to be more older people than younger people. What this means is there will
be less people working to support these safety nets for those that are older. Social security,
things like that may become strained. There is a Stanford professor who I guess
is going to be there who is predicting a population decline because people have
less money they are having less children therefore there will be less people to
sustain capitalism. Income inequality is a big one that they're going to talk
about the gap between those at the top and those at the bottom and because of
that they are expecting higher corporate taxes and they've kind of
expect this because currently 62 people will have the same amount of wealth as
the bottom 3.6 billion people. All of this leads to the end of capitalism.
talking about peak capitalism. You have CEOs discussing, paying attention to
stakeholders instead of shareholders. Stakeholders are me and you, customers
and employees. This opens the door for what many people are terrified of, the
dreaded S word, socialism. In order for these companies to still exist they're
to have to start paying closer attention to those people who allow the companies to exist, the
consumer. In order to do that, well, socialism comes into play. That leads to the end of
globalization. Now they're saying that we're going to start seeing the signs. I'm in the camp,
a small camp of people who think that this really started ending around 2001, when the military
might of the polar nations became questioned and used for other ends that led to kind of the end
of globalization. That's where it started. These guys are saying it's starting now and that we will
will see a decline in free trade because of that.
Countries will become more protectionist.
And that eventually, countries will
have to choose a state capitalist system
or an open society system with free markets.
So basically, the United States can
choose between China or the more socialistic countries
of northern Europe.
They're predicting that three major powers, which are kind of more like alliances, emerge from this.
China and its buddies, the United States and its buddies, meaning Canada and Mexico, and the European Union.
These are their topics. This is what they're going to be talking about. This is what they're going to be trying to
figure out.  It seems like a lot of doom and gloom.
However, if you look at past notes, you'll realize it's pretty much always doom and gloom.
And normally, these guys do
figure a way out. The people who attend these things
are pretty smart, and they find a way to
cushion the negative effects of
all of this. Some of it, as far as the environmental stuff,
They're kind of saying that, yeah, it's just going to be bad.
And because it's going to be bad, it will affect the economy in a large way.
But I do find it incredibly interesting that some of the brightest economic minds in the
world are pretty much at this point saying that socialism is inevitable for a whole lot
of countries, the United States included.
I cannot wait to see the comments section, especially the economists that follow this.
Clue us in.
Give us some more insight.
I basically have notes that they're going to talk about.
I want to know more.
Anyway, it's just a thought.
y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}