---
title: Let's talk about the load carriers of history and Doris Miller....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MUu-JitGePU) |
| Published | 2020/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the unsung heroes of history who carried the load and shaped events.
- Common people often go unrecognized for their contributions, overshadowed by leaders like presidents.
- Doris Miller, a cook during Pearl Harbor, stepped up to carry the wounded captain and assist in the defense.
- Despite lacking training, Miller operated an anti-aircraft gun effectively, engaging the enemy.
- After running out of ammo, Miller turned to aiding the wounded, showcasing bravery and selflessness.
- Miller's heroic actions saved lives on a day filled with chaos and danger.
- He was recognized for his bravery but later lost his life in action during World War II.
- Miller's legacy lives on as an aircraft carrier is named after him, a rare honor for an enlisted person.
- The decision to name a carrier after Miller signifies progress and change in institutions like the Navy.
- This historic moment marks the first time a black person has had an aircraft carrier named in their honor.

### Quotes

- "Naming an aircraft carrier after an enlisted person, that's a huge deal in and of itself."
- "It's a big deal. This is a big deal on a whole bunch of different levels."
- "There are still signs of progress, signs of hope."
- "Today they're naming one after an enlisted black guy. It's a cool development."
- "Y'all have a good night."

### Oneliner

Exploring the unsung heroes of history, Beau recounts the bravery of Doris Miller and the historic naming of an aircraft carrier after him, symbolizing progress and hope.

### Audience

History enthusiasts, advocates for recognition of unsung heroes.

### On-the-ground actions from transcript

- Research and share stories of lesser-known historical figures who made significant contributions (suggested)
- Advocate for more diverse representation and recognition in historical commemorations (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of Doris Miller's courageous actions during Pearl Harbor, shedding light on the importance of recognizing unsung heroes in history.

### Tags

#History #UnsungHeroes #Recognition #Progress #Hope


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about the load carriers.
Those people in history who literally or figuratively
carried the load and brought history to its conclusion
gave us the stories we know.
Those people, more often than not, are common people
and they're not recognized, not at the time
unless it benefits the powers that be, and normally not later, instead we recognize leaders.
Big names.
Harry Truman, Ronald Reagan, George Bush, Gerald Ford, John F. Kennedy, all these people
have something in common, and it's not that they were presidents.
We can add another name to this list now.
Doris Miller, a guy named Doris Miller.
he gets to join those names, and something that was pretty shocking to pretty much everybody.
So a little bit about Dory, which is what he went by, what his friends knew him as.
On December 7th, a date which will live in infamy, the Japanese attacked Pearl Harbor.
He was a cook.
He just got finished serving breakfast, and it all broke loose.
So he went to the area of the ship that is where all the corridors meet up, commonly
referred to as Times Square, and let it be known that he was available for duty.
He was asked to carry the captain who had been wounded.
When he was done with that, asked him to carry ammunition up to the anti-aircraft guns.
gets up there there's somebody who knows what they're doing and he's explaining to
Dory how to load it just wanting this cook who knows nothing to load the
weapon for him so he can continue fighting. Apparently he tells him how to
do it and turns away and when he turns back Dory has a nearby anti-aircraft gun
up and running, engaging the enemy. When he runs out of ammo, he decides, you know,
he's already been a cook that day in anti-aircraft, he's gonna be a nurse now,
goes down and starts moving wounded, pulling him out of rooms that are
filling with water, oil, carrying him through smoke-filled rooms as metal just
falls down around him. Saves lives, undoubtably. The ship went down, he survived, and he was
recognized at the time. He was transferred to the Indianapolis, but he was pulled from
that to go on a war-bond tour. Recognized, but serving those in power. He went around
recruiting. Now, he serves out a few years on the Indianapolis and then he gets transferred
to another ship and I can't remember the name of it, but it goes down. Japanese torpedo
got it. He did not survive. His parents got word of his death two years later on December
7th, 1943, two years to the day of Pearl Harbor.
So he is now part of that list, because that list is people who've had aircraft carriers
named after them, it's a huge deal, huge deal.
Generally aircraft carriers are named after presidents or federal lawmakers who were in
office an immeasurable amount of time.
The last time it wasn't a president was John C. Stennis, who sat on the Appropriations
Committee and determined how much money the Navy got.
That might have had something to do with him getting his name on a boat, but the funny
part about this is that Stennis was a huge segregationist.
They put his name on a carrier.
Doris Miller's black, I like that.
It shows that even in institutions as reluctant to change as the Navy, things are changing.
Now to those outside the military community who may not understand, set aside the fact
that he's black for a second.
Naming an aircraft carrier after an enlisted person, that's a huge deal in and of itself.
It just doesn't happen.
Addressing the race issue, this is the first time a black person has ever had a carrier
named after them.
It's a big deal.
This is a big deal on a whole bunch of different levels.
And understand that when the Navy deploys, they deploy typically in things called carrier
groups.
So there will be other ships assigned to the Doris Miller.
I mean, it's huge.
This is a huge deal, and I just think it's worth noting that even with everything going
on, there are still signs of progress, signs of hope.
This is something that even 25 years ago, 25 years ago they were naming a carrier after
segregationist and today they're naming one after an enlisted black guy. It's cool
development. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}