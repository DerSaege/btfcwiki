---
title: Let's talk about white hats in Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0dNu1qLhaKk) |
| Published | 2020/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how an event in Texas is being used to propagate a false idea about gun ownership and heroism.
- Points out the misconception that training is not necessary to use a gun effectively and safely.
- Contrasts the dangerous myth with the reality of the trained individual who stopped the incident in Texas.
- Emphasizes the importance of training, planning, and preparedness in ensuring safety and saving lives.
- Acknowledges the hero's responsibility and preparedness in handling the situation effectively.
- Argues against the belief that simply owning a gun without training or a plan is beneficial.
- Stresses the necessity of training for those who choose to be armed and the importance of having a plan in place.
- Disputes the idea that the object itself (the gun) is the solution, underlining the critical role of training.
- Warns about the risks of owning a firearm without proper training and preparedness.
- Encourages individuals to invest in training and preparation rather than solely relying on owning a gun.

### Quotes

- "It's a myth that training is required to use a gun effectively."
- "This idea is incredibly dangerous."
- "The debate in this country about whether or not people should be armed, about whether or not civilians should have access."
- "If you're going to purchase a firearm, you must train."
- "People with training hesitate."

### Oneliner

Beau explains how a Texas event is being exploited to push a false narrative on gun ownership and heroism, stressing the critical role of training for safety and effectiveness in handling such situations.

### Audience

Gun owners, activists

### On-the-ground actions from transcript

- Train with firearms for safety and effectiveness (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the dangers of propagating myths around gun ownership and heroism, stressing the critical need for training and preparedness in handling such situations effectively.

### Tags

#GunOwnership #Training #Safety #Heroism #Preparedness


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about how an event can be used to further a bad idea, a
false idea, even when all of the facts surrounding that event suggest that idea is false.
How an event can be used to propagandize something and further an idea that is
plainly false and the event itself shows it. We're going to talk about the desire to wear
the white hat. Because of that event out in Texas, more fuel has been added to the fire
and it's furthering the idea that all you need to do is just go buy one, strap it on
your hip and you'll be there. You'll be ready to save the day. You will be John Wayne, Gary
Cooper you'll be the good guy and you'll get to be the hero.
You will be the good guy with a gun.
I had already noticed that this was going on that people were using this incident out
there to further this idea when one of y'all tagged me in a thread on Twitter and this
This goes to prove that I do actually read it when y'all do that.
There's a quote in it, it almost gave me an aneurysm.
It's a myth that training is required to use a gun effectively.
Little old grandmas can pick up a gun, point and pull the trigger effectively and safely.
More training can and does help, sure, but not necessary for safe and effective usage.
I humbly beg to disagree.
This idea is incredibly dangerous.
Incredibly dangerous.
More importantly, it's the complete opposite of what happened.
of what happened. I would like to point to the Attorney General of Texas, his
description of the hero, the guy that stopped it. He was a reserve deputy and
had significant training, had his own shooting range, had taught other people
how to shoot, had taught many people in this church how to be prepared.
He wasn't just some old dude that happened to be on scene.
He was part of their security team.
This church had a security team.
They planned, they trained, they rehearsed, they knew what they were going to do and it
paid off and it saved lives.
There's no doubt of that.
But even with that, I'd like to point out that only one person actually engaged.
We've all seen the photos.
A whole bunch of people that were armed, one person engaged.
This has been proven time and time again that's what's going to happen.
Because most people, no matter how good the training is, most people aren't killers.
They're going to wait.
They're going to hesitate.
One person didn't, and he fired one shot, and yeah, he wore the white hat.
He saved the day with one shot.
Without training, do you think he could have done it with one shot?
he could have used more. Sometimes one shot is all you got. The idea that you don't need
to train is dangerous, incredibly dangerous. More importantly, to the memes, I want to
point to what the hero himself had to say about it. He said, the events put me in a
a position that I would hope no one would have to be in, but evil exists.
And I had to take out.
He had to.
Doesn't want anybody to be in that situation.
This isn't somebody who sat around fantasizing about wearing the white hat, being the good
guy with a gun.
This is somebody who took responsibility for their own safety and the safety of their religious
institution.
Trained, developed a plan, and when the time came was able to put it into action and it
saved lives.
Yeah that'll work a lot.
A lot of the time that's going to pay off, it won't always, but a lot of the time it
will.
Enough to invest in doing it.
You know what doesn't pay off?
Having a bunch of people who bought one and carry it, and don't train, and don't have
a plan.
That's not going to help anybody.
The debate in this country about whether or not people should be armed, about whether
or not civilians should have access.
When the idea is that the object itself is the solution, it's wrong.
Just like every person who supports the Second Amendment will tell you it's an object, it
doesn't do anything by itself.
That's true on both sides.
If you're going to purchase a firearm, you must train.
There's no other way to do it, none.
Just purchasing it and having it around, it's going to be used to hurt you, you're going
to get yourself hurt, or you're going to do nothing and feel bad because you had the means
at hand and didn't act, because you didn't train.
People with training hesitate.
If you don't have training, the odds are you are not part of that 4%.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}