---
title: Let's talk about over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OdVzibg-izg) |
| Published | 2020/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about a recent event involving strikes and protests in a particular area.
- Emphasizing the lack of political gain for any side in the event.
- Exploring the security concerns and the concept of the Green Zone.
- Addressing the response from Trump as relatively average in the context of his foreign policy decisions.
- Speculating on Iranian influence based on attempts to contact experts on Iran.
- Describing the chain of events leading to the protest after US strikes.
- Comparing the strikes to current counter-terrorism strategy and questioning its effectiveness.
- Rejecting the idea of this event being Trump's Benghazi and explaining why.
- Praising the security detail at the embassy for handling the situation effectively and avoiding escalation.
- Predicting more tensions between the US and Iran, potentially leading to heightened conflict before the election.

### Quotes

- "The embassy didn't fall because Iran didn't want it to."
- "The security detail at that embassy, they're the winners, they're the heroes here."
- "We need to be very cognizant of it."

### Oneliner

Beau dives into recent events, analyzes Trump's response, speculates on Iranian influence, praises embassy security, and warns of escalating tensions with Iran. 

### Audience

Foreign policy observers

### On-the-ground actions from transcript

- Stay informed on the situation and developments (suggested)
- Monitor geopolitical tensions and escalations (suggested)
- Advocate for peaceful resolutions and diplomacy (implied)

### Whats missing in summary

Detailed analysis of Trump's potential motivations and strategies for escalating tensions with Iran.

### Tags

#ForeignPolicy #Geopolitics #Iran #Security #Tensions #Embassy #Trump #Analysis


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about over there.
What's happening over there?
Here are the drums.
There's a lot of questions about it
and pundits are trying to spin the event
for political gain.
There's no political gain to be had in this.
It's an event.
There will be consequences from it,
But there's no real political capital in it for anybody on any side.
Those consequences could be pretty severe, but the talking points that are being used
are pretty much garbage.
So we're going to talk our way through them and see where it takes us.
To start off, I've got to tell you one of Murphy's laws.
Once you have secured an area, don't forget to tell the opposition.
If an area is secure, it is only secure until you stop securing it or become lax.
It doesn't maintain a level of security just because you give it a cute name like Green
Zone.
The Green Zone has been a green zone in name only for a while.
It's one of the questions that keeps popping up.
How could this happen there?
The securities lags.
The next obvious question is how's Trump doing?
Not bad.
Not bad actually.
If this is one of the first foreign policy videos you've seen from me, I think that's
the first time I've ever said that.
So if this was a normal president, this would be an average response.
Most foreign policy decisions from Trump are utterly disastrous.
So when we get an average response, one that doesn't cause a whole bunch of blowback, it
goes in the win column for him.
The bar has been lowered that much.
He's made some minor mistakes, but nothing too big.
that I think will really cause issues in the future, yet.
We'll see how the week progresses.
Is there Iranian influence?
I believe there is.
I believe there is, not because the administration says so,
not because the talking heads say so,
but because I tried to call every expert on Iran that I know.
All of their phones went straight to voicemail,
which means they're somewhere that they can't have a phone.
What that tells me is that the government,
at least they truly believe Iran was involved.
It's not just a narrative.
You don't bring in experts if you're just pushing a narrative
because those experts are going to poke holes in your narrative
and you don't want that.
Iran has a whole bunch of influence in Iraq anyway.
So this is very likely that they push some influence into it.
So what happened?
What was the chain of events?
The US conducted some strikes.
There was a protest in response.
The protest got pretty wild.
That's what happened.
That's what went down.
Questions about that.
Were the strikes legitimate?
That's a tough one.
Under the strategy we are currently using.
Yeah, yeah, they were legitimate targets.
They were justified, all of that.
Under the current strategy.
The problem is most counter-terrorism experts
will tell you our current strategy is wrong.
So you can take that however you want.
Yeah, by current doctrine, they were legit.
But the doctrine's wrong.
We can't really expect Trump, who can't handle, normally can't handle basic
foreign policy incidents, to completely revamp a strategy.
I think that's expecting too much of it.
Is this Trump's Benghazi?
No, no.
The two things are not even remotely comparable.
Other than involving State Department employees, they're nothing alike.
Trump's Benghazi was the tango-tango ambush and nobody cared.
The other thing we have to keep in mind is that if we are operating under the
assumption that this was orchestrated by Iran, the embassy didn't fall because
Iran didn't want it to.
When, when their people were that close to it, they could have taken the embassy.
They could have taken down the building.
The embassy didn't fall because Iran didn't want it to.
When they do this, when, when, when organizations of any kind conduct something like this, they
have a goal.
And that goal is to provoke an overreaction, to get nasty headlines out there about their
opposition, to get horrible imagery that they can use as propaganda to create organic protests.
That brings us to the real stars of the show, in my opinion.
And I don't think they're getting the credit they deserve.
The security detail at that embassy, they're the winners, they're the heroes here.
They're not going to be honored in the way they should because America tends to only
honor heroes who go loud, who create casualties.
They were facing a pretty rough situation and they stuck to the rules of engagement.
When things get that bad, rules of engagement tend to become flexible, for lack of a better
word.
They didn't.
They stuck to less than lethal, and they denied the opposition that propaganda they were seeking.
They won.
It may not look like it from the footage, but they won.
They did it right.
They kept their cool.
They kept their weapons on safe.
They did it right.
And I don't think that they're going to be recognized for that in the way they should.
Because had they lost their cool, the headlines would be very, very different.
And those headlines would have a huge impact all over the Middle East.
So I do think that the security details there should definitely get credit for that.
Okay, so where do we go from here?
What's next?
More of the same.
We're gonna see more ratcheting up of tensions
between the United States and Iran
because Iran will not back down.
And this administration needs a war.
They need a war shortly before the election.
So we're going to see heightened tensions.
You're gonna see more strikes
in the hopes of provoking them so they can get that war, so they can get us on a wartime
footing right before the election.
Because that's probably going to be the only thing that can save this administration.
So it's something we need to be aware of.
We need to be very cognizant of it.
We need to understand that more than likely the man who tweeted, you know, Obama's numbers
are bad and he's going to try to start a war with Iran to win the election is probably
going to try to do that himself.
And we have to decide, are we going to allow him to swap out American lives for votes?
That's what it's going to come down to.
There's a chance that maybe he doesn't.
I think it's pretty slim.
I think that that is part of his goal, that is part of his strategy, especially now with
the impeachment.
He's going to have to do something to energize his base in those moderates that are fleeing
him.
And there's nothing like a yellow ribbon that will do that.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}