---
title: Let's talk about locks, lessons, and Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iJoWeXSCpo4) |
| Published | 2020/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questioning the enforcement of a dress code that targets students with locks, particularly Black students.
- Examining the underlying issues and lessons being taught through these policies.
- Pointing out the inconsistency in enforcing the dress code based on race.
- Challenging the idea that certain hairstyles are distracting or unprofessional.
- Criticizing the expectation for Black students to conform to white standards.
- Expressing disbelief in the validity of the lessons being taught through this enforcement.
- Describing discipline for the sake of discipline as authoritarian and nonsensical.
- Emphasizing the message being sent to these young men about discrimination based on appearance.
- Inviting others to share their thoughts on the lessons being taught in this situation.
- Concluding with a powerful message about the impact of such policies on the students.

### Quotes

- "It doesn't matter how hard you work, what you do, what you achieve. Somebody can take it away from you because of the way you look."
- "I hate to break it to the school. I don't think there's a black kid in this country that needs that lesson taught to them because they see it every day from people like you."
- "Discipline for discipline's sake, that's just authoritarian nonsense."
- "The expectation is for a black student to try to be white and that's a really really bad expectation on a whole lot of levels."
- "I can't think of any reason for it to exist."

### Oneliner

Beau questions the enforcement of a dress code targeting Black students with locks, challenging the validity and lessons taught through such policies.

### Audience

Students, educators, activists

### On-the-ground actions from transcript

- Challenge and push back against discriminatory dress codes in schools (suggested)
- Advocate for inclusive and non-discriminatory policies regarding hairstyles and clothing (suggested)

### Whats missing in summary

Beau's passionate delivery and detailed analysis of the impact of discriminatory dress code policies can best be experienced by watching the full transcript.

### Tags

#Discrimination #DressCode #Inequality #Education #Community #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about locks and lessons.
Locks, like hair.
We'll just go through the normal stuff real quick.
Yes, hair does take a while to grow.
This shouldn't have been a surprise to anybody at the school.
But all of a sudden, they want to enforce it.
That seems a little odd.
A lot of people want to go straight to race.
And maybe.
Maybe.
I went to the website.
There are a whole lot of students, male students,
with hair that would be out of dress code
by the standards they're holding these young men to.
They don't seem to be having any problems.
But they don't look like them either.
So maybe that is it.
But let's pretend it's not for a second.
Let's give them the benefit of the doubt and say,
that's not it, for whatever reason.
Despite evidence to the contrary, that's not it.
See, when things like this happen,
people refer to it as the outrage of the week,
some simple thing that's just blown out of proportion.
So two teens have to cut their hair, big deal.
Why is this national news, right?
The thing is, when these things happen,
the reason they resonate is because there's
an underlying issue that needs to be addressed.
That's why they get picked up.
That's why it enrages people.
That's why the outrage exists.
So let's take a step back.
Why do we have a dress code at a school?
What's it there to do? Why do we have them?
The idea is that it's going to teach a lesson.
It's going to teach the students something.
Prepare them for the real world.
Depending on where you work, you may have to wear safety equipment
at school, or at work.
Still-toed boots, reflective clothing, something, okay?
That's why schools don't allow students
to wear the sandals that slip off really easily.
Fair enough, makes sense, I guess.
I would suggest that by high school,
students could make that determination on their own,
but whatever, fair enough.
That doesn't apply here.
Doesn't apply here, it's not like these kids
have hair down to their knees in fact it is just ridiculously normal like when
you actually see the photos of their hair it doesn't even make any sense and
it's kept up on top of that so that's one but it doesn't apply another one is
distraction it's gonna be a distraction to the other students doesn't apply
here. It does not apply here. This is very normal hair. Very normal. But even that idea kind of falls
flat when you think about it. Because if the policy wasn't in place, you would have more students
exercising their right to individual expression, showing themselves as people, as individuals.
Therefore, hairstyles and clothing styles would be varied to a greater degree. It wouldn't be a
distraction because it would be the norm. So that doesn't make any sense and that
really doesn't seem like it's preparing them for adulthood either. Sometimes it's
a puritanical holdover, you know. You ladies better cover yourselves. Those
boys, they just can't control themselves because that sends the right message, you
But those are the main reasons, and then you have professionalism is one.
This hairstyle would be accepted at almost every job.
It's not out of the norm in any way, shape, or form.
Not only that, those things change.
They really do.
When I was growing up, I was told if you had a visible tattoo, you wouldn't be able to
get a job.
to this day I don't have visible tattoos. Meanwhile my doctor has sleeves and a
tattoo on her neck. Those standards have obviously changed. The school said that
they have high expectations. I don't know what that means. There's nothing low
about having locks at all. It seems like the expectation is for a black student
to try to be white and that's a really really bad expectation on a whole lot of
levels. So for the life of me I can't think of one valid lesson being taught
by this. Can't think of any reason for it to exist. And you know, discipline for
discipline's sake, that's just authoritarian nonsense. It's torture.
You're just forcing your will on somebody because you can. It's not a valid reason
to do something. That is as invalid as saying we've always done it this way.
I would love to hear what y'all think. We can all come together and try to come up with a
valid lesson being taught here. Because as far as I can tell, these two young men are learning one
lesson and one lesson only. And that's that it doesn't matter how hard you work, what you do,
what you achieve. Somebody can take it away from you because of the way you
look. And I hate to break it to the school. I don't think there's a black kid
in this country that needs that lesson taught to them because they see it
every day from people like you. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}