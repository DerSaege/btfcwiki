---
title: Let's talk about spheres of influence and international affairs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ItRfOP0vaB4) |
| Published | 2020/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the current state of international affairs focusing on spheres of influence.
- Shares his perspective on how the world operates in terms of maintaining influence over smaller countries.
- Compares his beliefs with the actions taken by larger countries to maintain influence.
- Details the influence of larger countries in Syria, Iraq, Iran, and Afghanistan before and after Trump took office.
- Describes the repercussions of current foreign policy decisions on Russian influence stretching from Syria to Afghanistan.
- Criticizes President Trump's foreign policy legacy and its impact on American influence globally.
- Emphasizes the need for experienced diplomats to undo the damage caused by the current administration.

### Quotes

- "This doesn't really sound like what you believe."
- "A never-ending chain of Russian influence stretching from Syria to Afghanistan."
- "President Trump has one of the worst foreign policies in American history."
- "The Democrats need to understand that President Trump destroyed everything that this country fought and bled for."
- "This isn't anti-imperialism. It's assisting somebody else's imperialist ambitions."

### Oneliner

Beau provides a critical analysis of international affairs, discussing the shift in influence from the US to Russia under President Trump's foreign policy, urging the need for experienced diplomats to mitigate the damage done.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Seek out and support experienced diplomats for key positions in foreign policy (implied)
- Advocate for a comprehensive understanding of international relations and the Middle East and South America (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the impact of current foreign policy decisions on spheres of influence and the need for experienced diplomats to address the damage caused by the administration.

### Tags

#InternationalAffairs #SpheresOfInfluence #ForeignPolicy #Diplomacy #Critique


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about spheres of influence,
international affairs.
If you're not familiar with the geography of the region
we've been discussing this week,
go ahead and pull up a map.
It might be helpful to have a handy little visual reference
to some of this.
While you're doing that, I'm gonna go ahead
and address something I'm sure is going to pop up
in the comments section.
This doesn't really sound like what you believe.
not. Tonight we're not going to talk about how the world should work, we're going to talk about how
the world does work. Modern international affairs is about maintaining influence. Larger countries
maintaining influence over smaller countries, getting them to do their bidding for economic
advantage or political advantage. That's the way the world works today. It doesn't mean it's right,
but it's the way it is. If I was the President of the United States, this isn't
what we would do. We would promote self-determination at every turn for
various peoples and we would use US military might to do that. I am NOT the
President of the United States and there is no country on the planet that operates
in that fashion. The way they do operate is by collecting other countries, putting
in their pocket.
So what did the map look like as far as influence prior to President
Trump taking office?
Start in Syria.
Syria was mixed Russian and American influence.
America held their influence via the Kurds.
Move one country to the East.
Iraq, American influence, one country to the East, Iran, Russian influence,
but warming to the United States, the United Kingdom, and France, coming out of isolation.
One country to the east, Afghanistan.
American influence but shaky, to be honest, shaky.
Okay, so that was the scene when he took office.
What's it look like now?
Syria, Russian influence, completely, because we sold out the Kurds, we have no influence
there.
There's some little groups that are still with us, kind of, but no, we exert no influence in that country.
One country to the east, Iraq.
The parliament is asking us to leave.
The president of the United States is threatening a host nation with sanctions via Twitter.
better. If that course is maintained, they become a Russian sphere of influence. Russia
gets that. And we're going to talk more about that in a second, the repercussions from that.
One country to the east, Iran, being pushed firmly back into the arms of the Kremlin,
No longer warming, no longer coming out of isolation.
One country to the east.
We are in the process of just kind of throwing up our hands and letting the national government
fall, and allowing the guys who were running the show before we got there to take back
power.
When they do, they will loosely be in Russia's camp.
It will be shaky, like our influence over the national government.
There's not a strong central government in that country, so there's nothing to really
influence in that manner. To be fair, you can't really blame this one on Trump.
You would have needed somebody truly adept at international relations and
diplomacy, and somebody who truly understood the region. We didn't get that.
And to be honest, we didn't have anybody running that could have pulled that off.
That one's going to change.
Okay, so let's go back to Iraq.
What happens if the president maintains course and drives Baghdad into the arms of Moscow?
What happens?
The first thing that happens is any advanced weaponry, any advanced technology that we
gave them gets handed over to Russian intelligence who reverse engineers it and finds any weakness
they can exploit.
Then all of these Russian intelligence officers show up and they debrief anybody who worked
with American forces to learn our strategic, operational, and tactical practices.
We've talked about it in other videos.
Means and methods, that's what matters.
Those are the methods.
Russia gets an intelligence coup.
They get to understand everything about how we work.
So let's go to Iran.
Iran has announced that they're going to begin enriching without restriction.
And the administration is really, really mad.
Which is funny because according to the administration up until today, they'd been doing this anyway.
What does this tell you about what Iran was doing if this is their big statement?
Means they weren't doing it before and the deal was working.
a deal the president of the United States just withdrew from.
That's where we're at.
That's where we're at.
This is President Trump's foreign policy legacy.
A never-ending chain of Russian influence stretching from Syria to Afghanistan.
You can scream MAGA all you want, you can say Make America Great, and you can chant
whatever you want to.
These are the facts.
can't argue this. This is what's going on. It's almost like a less than successful
real estate developer was not up to the task of playing a real-world version of
risk with one of the 20th century's most prominent intelligence officers. Now to
be fair, a community organizer from Chicago was not up to the task either.
the differences he knew it. And he had advisors who he trusted and he had
people who could hold their own against him. He didn't try to meet him man-on-man
to show he was a tough guy. The reality is President Trump can barely handle
criticism on Twitter and Putin was a guy who stayed in an embassy by himself to
burn paperwork in the middle of a riot and then walked out and pulled a Wyatt
It was like, yeah, y'all may get me, but I'm going to take some of you with me.
President Trump cannot handle Putin and Putin does not respect President Trump.
The reality of this is that President Trump has one of the worst form policies in American history.
That's the reality of it.
It's so bad, it almost seems intentional.
And you can go back to the theories about that if you want to, that's fine, I don't
think it matters anymore, the damage is done, there's not much more he can do to disrupt
American foreign policy, there's really not.
The Democrats need to understand that President Trump destroyed everything that this country
fought and bled for for 20 years their current presidential candidates are all kitchen table
candidates they're about domestic issues and that's fine that's fine but they need to
understand that if they win in 2020 the the single most important thing that they're going
to have to do is decide on a secretary of state that knows what they're doing no political
favors, know this guy's my buddy, nothing like that.
You need an experienced diplomat.
You need somebody who truly understands the Middle East and South America and somebody
who is up to the task of trying to undo some of the damage that this administration has
done and it's immense.
Some of it you're not going to be able to undo, you just mitigate it.
that I hope that the Democratic Party understands that because this is one of
those things that's going to have to be dealt with when it's fresh, otherwise
this is going to stay. It's going to be like this. Now if this was a case of the
President of the United States just reducing American influence abroad, I
would be all for it. I think that would be a good move, but that's not what he's
doing. These actions are pushing these countries into the arms of the Kremlin.
This isn't anti-imperialism. It's assisting somebody else's imperialist
ambitions. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}