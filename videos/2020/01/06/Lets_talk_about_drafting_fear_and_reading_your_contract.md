---
title: Let's talk about drafting fear and reading your contract....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DCE6368MAB8) |
| Published | 2020/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the process leading up to the reintroduction of selective service and military recruitment.
- Focuses on the steps taken before selective service is brought back, indicating the warning signs to look out for.
- Mentions that the Department of Defense (DOD) is not keen on selective service returning, as they prefer volunteer service over conscription.
- Talks about tactics used by the military to avoid a draft, such as re-enlistment bonuses and retention efforts.
- Describes scenarios like the back-door draft and inter-service draft, which are not official drafts but serve a similar purpose.
- Mentions reserve units and the ability to involuntarily recall individuals back to service.
- Explains how enlistment bonuses are used to incentivize new recruits, especially targeting low-income areas.
- Talks about lowering standards for recruitment, both secretly and openly, when there is a shortage of personnel.
- Indicates that selective service is unlikely in the current times, and even if it were to return, efforts will be made to make it more acceptable to the public.
- Concludes by stating that the avoidance of selective service is preferred, especially under a commander-in-chief with draft-dodging allegations.

### Quotes

- "Your strongest ally in this may be surprising. It's DOD."
- "This is the point where you need to start paying attention."
- "So it's not a fear that at this moment you should really have."
- "I think they want to avoid it at all costs."
- "Have a good night."

### Oneliner

Beau explains the process leading up to selective service reintroduction, detailing warning signs and military tactics to avoid a draft, ultimately suggesting its unlikelihood and efforts to make it palatable to the public.

### Audience

Military-age individuals

### On-the-ground actions from transcript

- Stay informed on military recruitment processes (implied)
- Monitor warning signs for selective service reinstatement (implied)
- Advocate for transparent recruitment standards (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of military recruitment processes and the steps leading up to selective service reintroduction, offering insights into warning signs and efforts to avoid conscription.

### Tags

#SelectiveService #MilitaryRecruitment #AvoidingDraft #DOD #Standards


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So tonight we're going to talk about selective service
and military recruitment in general.
I've gotten a couple of questions about it, some
from people who are very concerned.
And then there's always a bunch of comments
about it coming up now.
Some of those are people who are concerned.
Some are obviously joking.
I'll start off by saying there's a whole bunch of things
to be concerned about right now.
This really isn't one of them.
So tonight, we're going to go through all of the things that
would happen before selective service is brought back.
And they're not really in any particular order
except for the last couple.
And those are your warning signs.
Once those things start happening,
then you need to start kind of paying attention to it.
Before then, it's just not likely.
Your strongest ally in this may be surprising.
It's DOD.
DOD does not want selective service to come back.
They've learned from history.
Generally speaking, this isn't always true,
but generally, your worst volunteer
is better than your best conscript.
They understand it creates a lot of morale issues,
and it affects the public perception of the war effort.
They don't like it.
So they have come up with tons of ways to avoid having to have a draft.
So the first thing you see normally is massive re-enlistment bonuses.
They focus on retention.
Those re-enlistment bonuses get really high.
They understand that a combat vet is better than a new guy.
So they focus on keeping them in.
If the re-enlistment bonuses don't work, well, then there's something called a back-door
draft.
It's not really a draft, but that's what it's called.
This is where you're in, and corporal whoever is like, I'm getting out in 60 days, and
the first sergeant walks in and he's like, hey, who's getting out in 60 days?
Not so fast, come with me.
The needs of the army come first, you're staying.
And they're stuck.
people find out they really should have read that contract a little closer. Then
there's something called the inter-service draft, which is not really
a draft either. This is where a lot of people sign up for like the Air
Force or the Navy because they don't see a lot of direct action. So you sign up
for the Air Force and you decide you're gonna be a cop, okay? And then all of a
sudden they're like, oh yeah, well you're going with the 101st, and they're like, whoa,
that's the Army, I joined the Air Force, and they're like, well they're airborne, that's
close enough to get on the plane, and you go over there and you're an MP with an Army
unit, and this happened, they did this during Iraq and Afghanistan, at the height.
Then you have all of the reserve units, the ones that you know about, you know, one weekend
a month, two weeks a year, those people.
And then you have the reservists you don't know about.
And basically you can go and do four years and get out and then you're working your job,
your civilian job, whatever, and a year later you get a letter saying, hey, come back.
And that's when you realize you should have read that contract a little closer.
They have the ability to involuntarily recall people and bring them back.
Now normally before they do that, unless it's like an in-demand skill, like a language,
if you speak a needed language, that happens to you pretty often.
But they also jack up enlistment bonuses, and this is the first part where we're getting
to new people.
Everything before this, this is all in-house.
So they jack up enlistment bonuses, and they can get really substantial.
During Iraq, I know a guy who was looking at an $80,000 enlistment bonus because he
spoke Arabic fluently.
So this bonus is money on top of your normal pay, and it's a way to target and incentivize
people from low-income areas.
especially during wartime this is true in general but especially during wartime
they target low-income communities and it is what it is I mean you you can look
at the you can look at the systemic stuff with that if you want to but yeah
this is another reason why you're always going to have resistance to free
education because it's a recruitment tool for the military. The GI Bill. Okay so
once now you've got this done. So the next part of this is where they start
lowering standards secretly. And that's when you go in and you want to join.
And you find out well you you know you actually we don't let you join with a
GED under normal circumstances. But right now we need people so bad we've got a
waver for that. What's that? When you're 18 you got busted moving some illicit substance?
We've got a waiver for that. You're overweight? We've got a waiver for that. And this is all
happening inside the recruiting station. Once they've exhausted that option, then they move
to openly lowering standards. This is the point where you need to start paying attention.
And the first ones don't matter so much, like they'll let people join at an older age.
There's a cap.
There's a cutoff.
You can only join up to a certain age.
They'll raise that.
Or they'll openly advertise and say, we'll take people with some felonies, as long as
it's not like domestic violence or a violent charge, something along those lines.
So you have all of this stuff, then they would start thinking about selective service.
But until all that other stuff is done, they're not even going to consider it.
And even here, now in the modern age, they're going to try to fill as many of those positions
as they can with contractors.
So it's not a fear that at this moment you should really have.
It's pretty unlikely and I also don't believe that even if it comes back, I don't believe
they'll be punishing people the way they have in the past.
I think they would be doing everything they could to make it more palatable to the populace,
especially under a commander-in-chief who is alleged to have dodged the draft.
That would severely undermine the moral authority of it.
So I think they want to avoid it at all costs.
Anyway, so it was just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}