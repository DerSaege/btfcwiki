---
title: Let's talk about societal growth, the world we want, and how to get it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zXe9PxMxIFI) |
| Published | 2020/03/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Situations like the current one prime everyone for societal growth, fostering a desire for improvement and forward-thinking.
- The establishment takes advantage of such situations to make power grabs, seeking more control and money.
- It's time for ordinary people worldwide to embrace change and play the same game as those in power.
- We need a clear vision of the society we want and tangible steps to achieve it, rather than just a wish list.
- While many agree on universal needs like food, healthcare, housing, and education, the path to achieving these essentials requires specific strategies.
- It's not enough to have utopian ideals; we must identify achievable stepping stones to progress.
- When the dust settles, we'll have the chance to shape the world we live in, but only if we know what we want and have a plan.
- Without a clear vision, we risk being at the mercy of those who benefit from the current system.
- Beau encourages viewers to share their ideas for an ideal world or steps toward positive change, promoting civil debate and forward-thinking.
- It's vital to have a roadmap and a clear picture of the future to guide our actions and avoid being passive recipients of the status quo.

### Quotes

- "We need a clear picture. We need that picture. We need that roadmap."
- "We're the ones that actually want the change."
- "We're going to have the opportunity to demand it."
- "We've got to shatter the mystery about what we want and how we want to get there."
- "It might be a nice reprieve to think about the positive aspects, to think about a positive future."

### Oneliner

In times of societal unrest, it's imperative for ordinary people worldwide to define their vision for change and take tangible steps towards a better future, lest they remain at the mercy of those in power.

### Audience

Global citizens

### On-the-ground actions from transcript

- Share your ideas for an ideal world and tangible steps towards positive change through civil debate (suggested)
- Engage in constructive dialogues with others to envision a better future and outline achievable steps (suggested)

### Whats missing in summary

The full transcript provides a thought-provoking call to action for viewers to actively participate in shaping a better future by defining their vision and taking concrete steps towards positive societal change.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about societal growth because we're ready for it.
Situations like the one we're in, they prime everybody around you for societal growth.
When something like this happens, we all look at it and we want things to be better.
We want to make sure it never happens again.
We want to make sure that we're looking forward.
The establishment, the powers that be, they're very well aware of this tendency and this
is worldwide.
They're aware of this.
That's why they never let a situation like this go to waste.
That's why you see agencies conducting little attempted power grabs right now because they
know what they want.
They know what they want.
They have a clear picture of it.
They want more power.
They want more money.
They want more control.
So it's easy for them to move in that direction when the opportunity presents itself.
They never let a situation like this go to waste.
We, the average person worldwide, we can play that same game.
We can do that same thing.
We're the ones that actually want the change.
We're the ones that are most drastically impacted by situations like this.
We're the ones that have to bear the cost.
So why don't we take advantage of it?
Why do we allow situations like this to go to waste?
Because we don't really know what we want.
Not really.
We have a rough sketch.
We want things to be better.
That's not a plan.
That's a wish list.
So the question, and what I want everybody to think about and comment on, tell me, because
I'm very curious.
What do we want?
What is the society we want?
What do we think everybody should have?
Most people watching this channel would say, yeah, everybody should have food, health care,
housing and an education.
I think that's going to be pretty universal.
That's cool, but that's also just a wish list.
How do we get there?
How do we incentivize labor?
How do we motivate people to innovate, to take more responsibility, to work harder?
So not just does everybody have what they need by the standards of today, but our society
advances and we get closer to that utopian world that deep down we all want.
So how do we get there?
And I know that some people will want to say, whatever ism is your favorite.
That's cool.
That's cool.
But let's be more specific.
Let's talk about tangible steps.
Let's talk about things that can be done.
Because the idea is, yeah, we all want these utopian isms, whatever your ism is.
But is the world ready for it?
Right now, is the world ready for it?
I know my ism.
And no, society's not quite there yet.
We're not.
I wish we were, but we're not.
So what kind of stepping stones could we get?
What can we make?
What little pathway can we lead that would get us there eventually?
What steps can we take right now?
Because when the dust settles from this, we're going to have the opportunity to demand it.
We're going to have the opportunity to reshape the society, the world that we live in.
But we have to know what we want.
Because those who are already benefiting from the system we have today, they know what they
want and they have a plan.
If we're not clear on what we want, then we're really just wishing and hoping that those
at the top will see fit to give us the scraps from the table.
That's really what we're about if we don't have a clear plan.
So I want to know what you want.
I want to know what ideas you have to get us there.
And I know there'll be debate down there, just trying to keep it civil, at least a little
bit.
This is one of those things.
We're all sitting around right now.
We don't have much going on.
And it might be a nice reprieve to think about the positive aspects, to think about a positive
future, rather than what's being broadcast to us constantly.
Tell me your ideal world, or at least a stepping stone that could get us there.
We need a clear picture.
We need that picture.
We need that roadmap.
If we can come up with a general idea, then we know what steps to take.
We know how to move in that direction.
But without it, we're literally just throwing ourselves at the mercy of our betters, those
who are already in control of the system that exists, those that have led us into this situation
and coming situations.
We've got to shatter the mystery about what we want and how we want to get there.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}