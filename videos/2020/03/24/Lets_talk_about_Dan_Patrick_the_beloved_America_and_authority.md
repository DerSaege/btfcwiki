---
title: Let's talk about Dan Patrick, the beloved America, and authority...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JCLEDQvmbgc) |
| Published | 2020/03/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Lieutenant Governor Dan Patrick of Texas made a statement regarding older Americans and passing down the America they love to their grandkids, but then he tied it to the economy and suggested business as usual, even if only one in five older Americans might die.
- In California, 80% of COVID patients are aged 18 to 65, showing that it's not just the elderly who are affected.
- The American ruling class is prioritizing profit over lives, refusing to take necessary actions like a 21-30 day shutdown that experts recommend.
- Beau questions whether an America willing to sacrifice lives for profit is worth saving and suggests that those in power making such decisions need to be replaced.
- He stresses the importance of preparing for significant societal changes post-pandemic and calls for genuine representation in leadership that prioritizes lives over money.

### Quotes

- "Maybe that isn't the America everybody loves."
- "How can you believe somebody this callous and this incompetent has authority over you?"
- "The decisions that are being made are going to cost thousands of lives, and it's all about money."
- "If we are going to have a representative democracy, it needs to be representative."
- "We just have to do what we need to do. We've got to shut it down."

### Oneliner

Lieutenant Governor's economic focus over lives, American ruling class prioritizing profit, and the need for genuine representation in leadership during the pandemic.

### Audience

Politically active citizens

### On-the-ground actions from transcript

- Replace leaders prioritizing profit over lives and genuine representation (implied)
- Advocate for necessary actions to protect lives over economic interests (implied)
- Prepare for significant societal changes post-pandemic (implied)

### Whats missing in summary

The urgency of replacing profit-focused leaders with genuine representatives for a post-pandemic society.

### Tags

#COVID19 #AmericanRulingClass #GovernmentPriorities #Leadership #Representation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So tonight, we're gonna talk about a statement
made by a Lieutenant Governor,
how it reflects on the character of the United States,
specifically the character of the current ruling class.
And we're gonna talk about how that may affect
our perception of the validity of this government.
So, Lieutenant Governor Dan Patrick of Texas,
he was talking to, I believe, Tucker Carlson.
And he said that, you know, those Americans
that are 70 or older, what they're really concerned about
is passing along the America that everyone loves
and passing that down to their grandkids.
Man, that's noble.
Man, that sounds cool.
And then he goes on to conflate that with the economy,
with money.
The general gist and what he was kind of floating
as a possible policy, I guess,
was that we should just go about business as usual.
And those who are 70 and older, well, it is what it is.
Odds aren't too bad, only one in five's gotta go.
Be all right.
Now, while that's fun to say, and it's fun to call it,
some of the terms that it's being called,
and all of that's all in good fun and dark humor
and all of that, the reality is that those trends
aren't necessarily going to hold in the United States.
In California, 80% of the patients are aged 18.
In California, 80% of the patients are aged 18 to 65.
Now, we don't know the severity of those cases, though.
It could just be that we're more prone
to go to the hospital in the US
because our healthcare system is so good.
It could be that people are a little scared,
so they're going to get checked out.
Or it could be environmental factors.
It could be the fact that the United States has,
well, most of us have a little bit of weight on us.
And that may be complicating things.
And the reality is that even in best case scenarios,
it isn't just the old.
It's those who are compromised.
And then, yeah, there's a percentage
of healthy adults that go out, too.
So it's not just those 70 and up by any means.
That's just a misrepresentation of reality.
But beyond that, what this is showing is that right now,
the American ruling class has decided this is the moment
they're going to try to prove that the mayor
in the movie Jaws was the hero.
The reality is we do not need to keep the beaches open.
The experts told us what we needed to do.
They told us a couple weeks ago.
And had we done it, we'd almost be done with this now.
We'd be on the downhill.
We'd almost be finished.
We have to shut it down for 21 to 30 days, I guess.
That's what we have to do.
The American ruling class seems to have decided
to try everything except that,
so they can scrape out a few more pennies,
make a few more stock deals.
Stock deals that, based on information
that the rest of us don't have.
I would suggest that maybe an America
that has lowered its character to the point
where a few pennies is worth the lives of its grandparents
and the vulnerable, the compromised.
Maybe that isn't the America everybody loves.
Maybe that's not even an America worth saving.
The reality is that a lot of these politicians
who are so concerned with their pocketbook right now
are undermining their very positions
because the average American,
the average, especially those who are, I don't know,
18 to 40, who are politically active, they see this.
The authority of the government is all based on perception.
It's a magic trick.
The United States government only has authority
because people believe that it does.
Statements like this undermine it.
They drive it out of existence.
How can you believe somebody this callous
and this incompetent has authority over you?
How can you believe in a system
that put these people in power,
those who are making life-and-death decisions?
The reality is we know what we have to do,
and eventually we're going to have to do it.
Every attempt that we make to try to stall that
just prolongs the downturn and worsens the economy.
So not only are they focusing on the wrong thing,
they're bad at it.
The decisions that are being made
are going to cost thousands of lives,
and it's all about money.
Is that the America that the older generations
really want to pass down?
Is that the America that everybody loves?
This is the time when we have to decide
what kind of society we want after this
because there's going to be huge changes.
This is a horrible thing that's happening right now,
and if it's going to mean anything,
we have to be prepared when we come out the other side
to know exactly what we want,
and I would suggest that we do not want a country
where the leaders are willing to write off
1% of the population
simply because of other dividends.
We're going to have to make huge changes.
The leaders who are showing their true colors right now,
they need to be remembered,
and when this is over, they need to be replaced.
If we are going to have a representative democracy,
it needs to be representative,
and there isn't a genuine representative out there
that is going to say, yeah, let my constituents go
because I lost a little bit of money.
The economy will bounce back.
Demand hasn't changed.
It's still there.
We just have to do what we need to do.
We've got to shut it down.
Anyway, it's just a thought, and I would point out
there's a lot of people concerned right now
suggesting that this is some nefarious plot
by the government to keep everybody locked down.
I would just remind everybody
to look at how hard they're fighting against it.
Never, never mistake incompetence for malicious intent.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}