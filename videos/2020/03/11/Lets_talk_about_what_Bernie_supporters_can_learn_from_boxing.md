---
title: Let's talk about what Bernie supporters can learn from boxing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=USYADtr838A) |
| Published | 2020/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocates for the concept of never kicking down and always punching up.
- Points out the danger of personifying a movement and the discouragement that comes when the personified figure loses.
- Talks about Bernie's diminishing chances of being the nominee but stresses that the battle is not just about one person.
- Shares that he has been interviewing candidates challenging big political names like Nancy Pelosi and Mitch McConnell.
- Mentions that these interviews will be released over the next few days and are worth watching for insights into the future of politics in the United States.
- Acknowledges technical issues in the interviews but encourages watching them for the content.
- Emphasizes that the fight for systemic change is about the ideas, not just one person.
- Expresses his interest in watching a good political fight without endorsing any specific candidates.

### Quotes

- "It's never the people below you who are the source of your problems."
- "If you become discouraged by that, you're personifying the movement."
- "This fight is not about one person. If you want systemic change in the United States, it's not about Bernie."
- "I don't endorse candidates. But I do love to watch a good fight."
- "Y'all have a good day."

### Oneliner

Beau advocates for punching up, warns against personifying movements, and interviews candidates challenging big political names, stressing that systemic change is about ideas, not individuals.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Watch the upcoming interviews with candidates challenging big political names (suggested)
- Stay informed about progressive ideas and candidates in politics (implied)

### Whats missing in summary

Insights on specific progressive ideas discussed during the interviews. 

### Tags

#ProgressivePolitics #PunchingUp #SystemicChange #PoliticalCandidates #Interviews


## Transcript
Well howdy there internet people, it's Bo again.
So we talk a lot on this channel about the concept
of never kicking down, always punch up, you know.
It's never the people below you who are the source
of your problems.
It's not the way it works.
On a recent live stream I had somebody ask me
if I watch sports, and I said no.
It's not entirely true.
I will watch a match if it's mismatched,
if somebody's going way outside their weight class,
like Root for the Underdog, somebody who is punching up.
At the same time, something else we talk about
on this channel is personifying a movement
and how it becomes very dangerous and very self-defeating.
People get discouraged if whoever is personified loses,
if they aren't the presumptive nominee.
It causes a breakdown of forward momentum when that happens.
Right now, Bernie is in that situation.
He is looking less and less like the nominee.
Sure, Bernie can still win, all that,
but it's looking less likely.
But that's only part of the battle.
If you become discouraged by that,
you're personifying the movement.
You're personifying that idea of progress.
There are a whole bunch of fights
going on in the House and in the Senate
that are just as significant when it comes to going
after the establishment.
In that vein, I've spent the last 16 hours or so
interviewing the person running against Nancy Pelosi,
another Democrat running against Nancy Pelosi,
the Democrat running against Mitch McConnell,
and the person priming Debbie Wasserman Schultz
here in Florida.
These people are all just coming into politics,
and they are taking on some of the biggest names in politics.
We're going to release those interviews
over the next few days.
These are all people who are punching up.
These are all people who have progressive ideas
of varying degrees.
Some of them are, well, let's just
say there were a few surprising moments for me in it.
They're definitely worth watching
if you want to get a good read on where
the future of politics in the United States is headed,
especially if you live in Broward County, San Francisco,
or Kentucky.
They'll be incredibly informing.
I will tell you that because of my internet connection,
each one of the interviews, when it starts, I sound like a robot.
But you're not really listening to these for me.
And it does get better as the show goes on.
But those will be coming up.
I think the first one goes online here in a few hours.
And that will be, never ever thought
I would see a candidate for the House seeing
the praises of a general strike.
Did not have that on my political bingo card.
This fight is not about one person.
If you want systemic change in the United States,
it's not about Bernie.
It's about those ideas, right?
There are other people representing those ideas.
There are other people seeking to implement them.
I don't endorse candidates.
But I do love to watch a good fight.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}