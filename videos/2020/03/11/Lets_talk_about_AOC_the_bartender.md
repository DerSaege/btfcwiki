---
title: Let's talk about AOC the bartender...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4qI1OpY0zDo) |
| Published | 2020/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the backlash against AOC for either misspeaking or using internet speak.
- Questions the emphasis on AOC's past as a bartender, suggesting she has more qualifications.
- Points out that elected officials often distance themselves from working-class backgrounds.
- Argues that individuals like bartenders have a better understanding of community issues.
- Advocates for more diverse representation in government, including bartenders, fast food workers, nurses, teachers, welders, and ranchers.

### Quotes

- "Why should she clarify? Everybody up there likes to pretend that they're a man of the working people."
- "We need more bartenders. We need more fast food workers. We need more nurses. We need more teachers. We need more welders. We need more ranchers and less professional politicians."
- "The House of Representatives might, should be, representative of us."

### Oneliner

Beau criticizes the backlash against AOC, advocates for diverse representation, and questions the focus on professional politicians over working-class backgrounds.

### Audience

Voters, Political Activists

### On-the-ground actions from transcript

- Elect diverse representatives who understand community interests (implied)
- Advocate for more representation from working-class backgrounds (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the importance of varied representation in government and the need for elected officials who truly understand community interests.

### Tags

#Representation #AOC #WorkingClass #CommunityInterests #GovernmentRepresentation


## Transcript
Well howdy there internet people, it's Bo again.
So I guess we're gonna talk about AOC again.
As you know, she had another thing today.
She either misspoke or used internet speak in real life.
I don't know which.
But it doesn't matter.
That's a major infraction right there.
Nobody should ever speak miss at any point in time
and if they do, that should just be
the end of their career.
Because that happened, she came up a number of times today.
And I heard the same thing over and over again.
Well you know, she's just a bartender.
True enough, statement of fact, her credentials,
that is one way to say it.
She is a bartender.
Another way to say it would be that she's a bilingual,
dual majored cum laude graduate with experience
in foreign affairs and immigration who answered
crisis calls when she was in Senator Kennedy's office
and has an asteroid named after her.
That would be another way to say it.
But bartender is cool too.
When I brought this up today, a number of people asked,
why doesn't she clarify that?
I didn't know that.
She should tell people that.
I don't know.
I don't know why she doesn't clarify.
I don't know.
I would imagine that it's the same reason I don't clarify
when somebody mistakes my orientation.
There's nothing wrong with being just a bartender.
You know, everybody in this country is like,
you know, our representatives, our government
doesn't represent our interests.
Oh yeah, we elect our betters to be rulers.
We don't elect people like us to represent us.
So of course they don't represent our interests.
And why should she clarify that?
Everybody up there, everybody on Capitol Hill
likes to pretend that they're a man of the working people.
They're self-made.
They made their millions and billions themselves.
But when you get one who has a working class background
of some kind, at least held a job,
which a lot of them didn't,
she's supposed to disavow it.
Doesn't make a whole lot of sense to me.
They don't all have to be lawyers.
Now not to be too stereotypical about this,
but most people who say she's just a bartender,
well, you look like me.
Let's be honest.
You look like me.
Got a question for you.
In your hometown, in your neighborhood,
in your community, who knows everybody's problems?
Two people, right?
The preacher and the bartender.
Who's going to have a better gauge
on the interests of the community?
The real estate broker who only talks to the wealthy
or the bartender who talks to everybody?
I'm going to go out on a limb here
and say that we need more bartenders.
We need more fast food workers.
We need more nurses.
We need more teachers.
We need more welders.
We need more ranchers and less professional politicians.
People talk about the political elite
and talk about the fact that we have an oligarchy
and we have a rolling class,
but we just keep electing them.
So we really are kind of getting the representation
that we deserve.
None.
The House of Representatives might, should be,
I don't know, representative of us.
We might have better luck that way.
We might have people that are actually concerned
with the interests of their community.
I would like to see a few more just bartenders.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}