---
title: Let's talk about Reagan, a cliche, fear, and love....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ea9sYVGJy3s) |
| Published | 2020/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the idea of a science fiction cliche where the world bands together to defeat an alien threat, as mentioned by President Reagan.
- The concept that this unity is not based on love or interconnectedness, but rather on fear.
- Governments use fear to motivate people and maintain control.
- Despite the narrative of unity in the face of a universal threat, reality shows that fear often leads to fragmentation.
- Media and government tend to amplify fear to control the population.
- Criticizes the fear-based leadership response to crises, citing examples from current events.
- Calls for leadership that does not rely on instilling fear and cowardice in people.
- Encourages looking for interconnectedness and love amidst fear to break the cycle of control.
- Urges people to follow their hearts, not just their leaders, and to resist falling in line out of fear.

### Quotes

- "Governments use fear to motivate their populaces, they always have, hopefully that will eventually end."
- "As we go through bumpy times, this situation, the economic situation, whatever, look for the interconnectedness. Look for the love."
- "Fall in love. Don't fall in line."

### Oneliner

President Reagan's sci-fi unity cliche reveals how fear, not love, drives governance, urging us to seek interconnectedness amidst control tactics.

### Audience

Global citizens

### On-the-ground actions from transcript

- Seek out opportunities for interconnectedness and love in your community (suggested)
- Challenge fear-based narratives and leadership by promoting unity and courage (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the manipulation of fear in governance and the importance of resisting control tactics through love and interconnectedness.

### Tags

#Fear #Unity #Interconnectedness #Leadership #Community


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight, we're going to talk about President Reagan, fear,
love, and a science fiction cliche.
There's a cliche in science fiction books and movies.
It gets played out so often that people probably
it's true, that it's a reality. You've seen it in Independence Day. It's a good
example of it. When the people from outer space arrive, what happens? The entire
world bands together, holds hands, and defeats whatever it is. Reagan talked
about this once. Probably one of the weirdest things he's ever said in public.
He said, perhaps we need some outside universal threat to make us recognize
this common bond. I occasionally think how quickly our differences worldwide
would vanish if we were facing an alien threat from outside this world. Sounds
Sounds good, sounds good.
The aliens show up and we all hold hands and sing, we are the world.
And at the end of the movie, everything's better.
But see, it sounds like a message of love.
It sounds like a message of interconnectedness, that common bond, right?
But it's not, it's not, it's a message of fear.
And it's telling us exactly how government governs.
We can't try to achieve this recognition of our common bond, this love, this interconnectedness,
unless we have something to be afraid of.
Because that makes it really easy to motivate people.
If you didn't have fear to unite people, well then you would have to actually work, lead.
It was true then, and it's true now.
The thing is, that's not how it actually plays out though.
When we have that universal threat, it's not what happens, it never is.
We become more fragmented.
And the powers that be, whether it be the government or the mass media, they go out
of their way to just instill more fear. So you follow your leader. Think about our recent
situation, our current one. First couple weeks of coverage, what was it? Look at what's happening,
they can't move. This could happen to you at any moment. Be ready, be afraid, be very
afraid. Yeah, I mean, I guess that's good for clicks. Gives a real visceral
reaction. But at the same time, it should be noted that anybody who's at risk owes
the people who endured that a pretty big debt because they slowed the
advance. They bought time because we are all interconnected. Instead it morphs into xenophobia
or Fredo. That's how the media spun it. That's how they played it. Focused on the fear. Follow
your leader. And then we have our bumbling leadership. If this was an
alien movie, the person appointed to spearhead the response is somebody who
really doesn't even seem to believe in aliens. And then you have President Trump
who is saying that he might decide to close the Mexican border because that
would help somehow. You know, that is exactly what a stable genius would do though. Make
sure that anybody entering the country from there is unchecked and unscreened, because
that will help with our current situation. But, it appeals to his rhetoric. Fear. Fear.
afraid. It appeals to his base. I cannot wait for the day when we have leadership
that does not focus on telling everybody to be a coward. Be a great day. Follow
your heart. Don't follow your leader. You know, that's how it plays out though.
though. Governments use fear to motivate their populaces, they always have, hopefully that will
eventually end. It doesn't look like it's going to happen anytime soon though.
As we go through bumpy times, this situation, the economic situation, whatever, look for the
the interconnectedness. Look for the love. It's there and it's easy to see if you
rise above the fear. That fear is how governments keep you in line. It's how
they keep you in check. Fall in love. Don't fall in line. Anyway, it's just a
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}