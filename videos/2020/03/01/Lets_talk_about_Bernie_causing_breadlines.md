---
title: Let's talk about Bernie causing breadlines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M9_qFrcWeUU) |
| Published | 2020/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the misconception that electing Bernie Sanders will lead to breadlines due to socialism.
- Clarifying that Bernie is a social democrat, not a socialist, despite being labeled as a democratic socialist.
- Debunking the idea that socialism brings poverty by examining countries that identify as Marxist-Leninist.
- Providing examples of countries like Cuba, China, Laos, and Vietnam that do not fit the stereotype of extreme poverty under socialism.
- Noting that many of the poorest countries operate under capitalist systems, not socialist ones.
- Mentioning that economic sanctions have significantly impacted countries like Cuba's economy.
- Pointing out that powerful economies like China and India refute the breadline narrative associated with socialism.
- Contrasting socialism's aim to eliminate poverty with capitalism's reliance on it as a planned feature.
- Emphasizing that poverty serves as a tool within the capitalist system to incentivize compliance.
- Advocating for dispelling the Cold War propaganda linking socialism to breadlines.

### Quotes

- "Socialism brings poverty. That's the idea. Is it true?"
- "Capitalism requires poverty. It's not a defect, it's a planned feature of it."
- "The breadline thing just needs to go away. It's not true."

### Oneliner

Examining the misconception that socialism leads to poverty by debunking the breadline narrative and contrasting socialism's poverty elimination aim with capitalism's dependence on poverty as a planned feature.

### Audience

Social Justice Advocates

### On-the-ground actions from transcript

- Challenge misconceptions about socialism by sharing factual information and engaging in constructive dialogues (suggested).
- Support policies and initiatives aimed at poverty alleviation and social welfare programs in your community (implied).

### Whats missing in summary

The full transcript provides a detailed analysis debunking the misconception that socialism leads to poverty by examining real-world examples and contrasting socialism's goals with capitalism's inherent features.

### Tags

#Socialism #Capitalism #Poverty #Misconceptions #Economy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we're going to talk about a meme.
And it's not just a meme in the sense of an image, it's an
idea that people have.
And we're going to look into it, understand that nothing I
say tonight should be taken as an endorsement of any kind of
state economy.
This is just me trying to get to the bottom of something.
because the idea is showing up because of Bernie.
Because if we elect Bernie, we're
going to get bread lines because he's a socialist.
OK, first, Bernie's not a socialist.
He's a social democrat.
This is made even more confusing by the fact
that he's described often as a democratic socialist.
None of his policies are socialist.
They are the policies of the social Democrat.
Politicians often get described as something they are not.
Take a look at Trump.
He's often described as a Republican when he's really a fascist.
That's how that works.
He's not a socialist.
But the idea, the meme, is the breadline thing.
Socialism brings poverty.
That's the idea.
Is it true?
There are four countries today that self-identify as Marxist-Leninist.
To keep it simple, with absolutely no nuance, we're just going to say, hardcore socialists,
alright?
Certainly, if this idea was true, those would be the four poorest countries on the planet.
Are they?
No.
Not even close.
Certainly one of them is at least in like the 10 poorest.
No, 20 poorest, no.
30, 40, 50, 60, 70.
No, no, no, no, no, you have to get up to 95, it's Laos.
Those countries incidentally are Cuba, China,
Laos and Vietnam.
There are nine more countries that reference socialism in their constitution, but they
aren't full blown socialists.
Certainly those are the poorest countries.
Now, if you were to take the bottom 50 poorest countries, two of them would be on that list.
Incidentally, both of those countries, Tanzania and Ghana, both of them are
countries that were pillaged via colonialism by capitalist society until
the 1960s. If you were to exclude those, you would have to get up to 87 before
you find a country that mentioned socialism and its constitution. Those
other countries by the way are Algeria, Bangladesh, Guyana, India, North Korea,
Nicaragua, Portugal, Sri Lanka, and Tanzania. So out of the 50 poorest
countries, 96% are operating under some form of a capitalist society, some form
of a capitalist economy.
Some cases, they're mixed.
It certainly doesn't seem like the breadline thing is true.
When you look at countries like Cuba, who is 147th,
then you have to factor in the fact
that they've been sanctioned for decades.
They've had decades worth of sanctions hitting their economy
And they're still 147th.
Another way to look at this would be to go from the other side.
What are the five most powerful economies in the world?
Two of them are on this list.
It's just not true.
It's Cold War propaganda that needs to go away.
way. China and India. China is always in the top 5. India falls between 3 and 6, depending
on the year. It's just a falsehood. We need to keep in mind that shortly before the breadlines,
there were people in the United States selling their children because they couldn't afford
feed them. It's just not true. It's almost like the economic system and the
governmental system aren't always connected. One of the other things that
we need to keep in mind is that socialism attempts in theory to eliminate
poverty. Capitalism requires poverty. It's not a defect, it's a planned feature of
it. It's the stick to go along with the carrot. If you are a good little boy or
girl and you're a good consumer and you play by all the rules and you do what
you're supposed to, you get the carrot of the middle class. If you don't, you get
stick to knock you out into the street into poverty. Poverty is the silent
ever-present policeman that keeps the capitalist system moving. The breadline
thing just needs to go away. It's not true. Anyway, it's just a thought. Y'all have a
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}