---
title: Let's talk about getting invited to the cookout....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LPc7OLVZ4aM) |
| Published | 2020/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Cookouts and barbecues offer lessons in community building.
- Despite generalizations, cookouts tend to have similar elements of social interaction and fun.
- Differences across demographic lines are unique and significant.
- Support for the black struggle often leads to invites to black cookouts.
- Beau shares his experience of attending a black cookout and feeling welcomed.
- Cookouts organized by black communities are diverse in terms of professions and attendees.
- There is a contrast in how white and black communities organize their gatherings.
- White cookouts are often class-based, while black cookouts are neighborhood-based.
- Self-segregation weakens neighborhoods and communities.
- Strong networks and neighborhoods are vital, especially during challenging times.
- Beau suggests breaking down walls and organizing block parties or open cookouts to strengthen communities.

### Quotes

- "Cookouts and barbecues offer lessons in community building."
- "Support for the black struggle often leads to invites to black cookouts."
- "Strong networks and neighborhoods are vital, especially during challenging times."

### Oneliner

Cookouts teach community building; support for black struggle leads to invites, urging us to strengthen neighborhoods.

### Audience

Community members

### On-the-ground actions from transcript
- Organize a block party or open cookout in your neighborhood (suggested)
- Break down walls by getting to know your neighbors (suggested)

### Whats missing in summary

The full transcript provides a deeper understanding of the significance of community gatherings and the need to overcome segregation for stronger neighborhoods.

### Tags

#CommunityBuilding #Cookouts #Neighborhoods #Inclusion #Support


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about cookouts, barbecues,
and what we can learn from them, because there is something
we can learn from them.
It's kind of cool how they differ
across demographic lines.
And as I say that, I understand, yeah, I'm
sure that what I'm going to say is a bit of a generalization.
It doesn't apply to all the people all of the time.
But it's something I've noticed over the years,
and it has something that has influenced me in a way.
So all cookouts are pretty much the same in some ways.
Last one I went to was just like all the other ones.
Everybody brought what they could bring.
There's good social interaction.
Everybody had a few drinks, relaxed.
Nobody went without.
Everybody had fun.
That's the same.
Where they differ when you get across demographic lines
is something very unique.
If you ever say anything supportive, empathetic,
or even remotely understanding of the black struggle
in the United States on social media,
somebody will pop up and say, you can come to Cookout.
I have always found that really, really funny.
Because I have been that one lone white guy at a black
Cookout a few times in my life.
Never really felt like I needed an invite.
Never felt like anybody did.
Very welcoming, very open, very friendly.
Not exclusive events.
And I picked this up the first time I went to one.
I was out of town.
My buddy found out that I was in his area.
And he's like, hey, we're having a cookout.
Why don't you come over?
Sure.
It's important to know that my buddy was a gangster.
That's what he did.
That was his job.
That's how he made a living.
nice guy but that was his profession. So I pull up at his house. I think I've got
the wrong time or maybe wrong location because there's only two cars in the
driveway, his and his girlfriend. Man, I thought I knew where I was going. Opened
the door and I could hear everybody in the backyard. So I walked around, walked
through the back gate and there was that that brief moment where half of the 80
people that were there stopped and looked at me. I was like, yeah that guy's on the
wrong spot but you know that was it very friendly afterward 80 people no cars no
cars started talking to people there was the guy that owned the corner store
down the road his employees there were a couple of nurses there was a real estate
agent. People from all walks of life were at my buddy The Gangster's cookout.
It was weird. It was weird. And that has repeated at every black cookout I've been to.
Same thing. No cars, very diverse professions. Every single one of them.
Now, if I was gonna have a cookout, the guest list, people that would be invited, well they'd
be journalists, ex-contractors.
If my wife was making it, it'd be medical professionals.
And if, when you pulled up, there'd be cars all over the road, parking out front.
we do ours by class. White folk do our barbecues. Company picnics by class, by
profession. It's been my experience. Black folk do theirs by geography, their
neighborhood. Might be a better way to do it. You know all over the country you
have people complaining saying you know we don't even know our neighbors anymore
more. This modern society. No, it's not the modern society. Neighborhoods, strong
neighborhoods still exist. They just normally don't exist with us. It's our
fault. We have to take responsibility for that. And it's because we choose to
self-segregate like that. We do it and our neighborhoods get weakened because
of it. With everything that's going on in the world today, it might be time to
tear the walls around us down. I got a feeling strong community, strong networks,
strong neighborhoods you know they're always important but with the possible
economic downturn and everything else that's going on they might become
really really important it might be time to have a block party to have an open
cookout in your neighborhood get to know people anyway it's just a thought y'all
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}