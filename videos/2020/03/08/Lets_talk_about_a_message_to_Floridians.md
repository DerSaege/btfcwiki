---
title: Let's talk about a message to Floridians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vJskimaw4TY) |
| Published | 2020/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses gratitude for the concern and well wishes he has received.
- He mentions that his opinion on the issue has not changed, but he is taking some new precautions.
- Despite being in a crowded place full of travelers the next day, Beau states he will not be altering his daily routine drastically.
- He criticizes the response of the state leadership in Florida, mentioning that the lack of information is causing fear.
- Beau points out the disappointing behavior of some Floridians, describing witnessing panic during his outing.
- Floridians are reminded by Beau about the various natural dangers they face in the state and the need to approach things with dark humor and community spirit.
- He encourages people to inform themselves, look at the numbers objectively, and follow the guidelines and precautions.
- Beau suggests that those at higher risk should take extra precautions and that the community should support vulnerable individuals by helping them with tasks like shopping and errands.
- In the absence of effective government leadership, Beau stresses the importance of individuals taking charge and not succumbing to panic.
- He criticizes panic buying behavior, especially pointing out the irrationality of hoarding gas as if it were a hurricane situation.
- Beau calls for Floridians to stay calm, maintain their sense of community, and use humor to navigate through challenging times.

### Quotes

- "We cannot let fear run our lives."
- "If we are not going to get leadership from the government, we have to lead ourselves."
- "Panic is not going to help."
- "Pull your stuff together. You're a Floridian."
- "Stay calm. Use the same humor and sense of community that gets us through everything else that gets thrown at us."

### Oneliner

Beau urges Floridians to stay calm, support each other, and lead themselves in the face of COVID-19 uncertainty, rejecting panic buying and advocating for a sense of community resilience.

### Audience

Floridian residents

### On-the-ground actions from transcript

- Support older or compromised individuals by doing their shopping and errands for them (suggested)
- Maintain a sense of community and humor to navigate through challenges (exemplified)

### Whats missing in summary

The full transcript provides a detailed perspective on how Floridians can handle the COVID-19 situation with resilience and community support. Watching the full text can offer a deeper understanding of maintaining calm and unity during times of crisis.

### Tags

#COVID-19 #CommunitySupport #Florida #PanicBuying #Resilience


## Transcript
Well howdy there internet people, it's Bo again.
First I want to thank everybody for their concern
and well wishes.
I'm not incredibly worried about it.
My opinion on this issue has not really changed.
Taking a few new precautions, but I'm
not going to be altering my daily routine
in any major manner.
Tomorrow I will be in an incredibly crowded place
full of travelers.
It is what it is.
We cannot let fear run our lives.
To my fellow Floridians, yeah, the response from our state leadership has been less than
ideal.
Information is not flowing as it should.
That lack of information is creating fear.
Yeah, by the time they tell us it's in one place, it's really in five others.
We got it.
We got it.
However, what is more disappointing to me is the behavior of some of my fellow Floridians.
I was out and about today and it was panic.
Remember where you choose to live.
We all live in a state where the heat, the snakes, the insects, the gators, the rats,
the bats, cats, and other miscellaneous animals, the tides and the hurricanes can kill us.
Because of that, we approach things with a rather dark humor.
We also approach things as a community, especially when there is a lack of state leadership.
This is not to make light of the situation, but it's the situation we're in.
And we need to deal with this just like we do anything else.
Inform yourself.
Take a good, hard, objective, non-panic-fueled look at those numbers.
Follow the guidance.
Take the precautions.
For those people who are at risk, yeah, you're going to need to take more than everybody
else.
But we're a community.
So if you have friends or family who are older, who are compromised, why don't you treat it
like it's after a hurricane?
Do their shopping for them.
Run some errands for them so they can stay home, so they can limit their exposure.
If we are not going to get leadership from the government, we have to lead ourselves.
Panic is not going to help.
And the panic buying I'm seeing doesn't even make sense.
It's not hurricane guys.
What's up with the gas?
Where are you going to go?
Look, we have hurricane parties.
That's how we are down here.
We do not panic.
We do not act like this.
It is unacceptable.
Pull your stuff together.
You're a Floridian.
And again, I'm not making light of this, but if it does come to the Q word, we're not having
those in this state.
We're having staycations.
And it is what it is.
Stay calm.
Use the same humor and sense of community that gets us through everything else that
gets thrown at us.
We have to calm down.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}