---
title: Let's talk about how Castro came to power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gkQSQLgZGhQ) |
| Published | 2020/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how once international events hit the headlines, the rising action is missed, leading to a lack of understanding of the origin story.
- Describes Castro's rise to power in the 1950s in Cuba, portraying himself as a populist with military background.
- Points out that Castro couldn't win an election, so he staged a coup to seize power.
- After gaining power, Castro curtailed civil liberties, stifled dissent, and created a kleptocracy with his allies.
- Economic stagnation, income inequality, and deals with criminals further fueled revolutionary sentiment.
- Estimates the number of executions under Castro's regime to be between 10 to 15, disputing higher figures like 20,000.
- Mentions that Castro overthrew Batista, who failed to respond to the growing revolutionary spirit.
- Outlines the necessary conditions for revolution: unresponsive government, corruption, income inequality, and discomfort among the people.
- Notes that the US has most conditions for revolution but has the potential for a peaceful transformation due to comfort and infrastructure.
- Warns about the deceptive nature of anti-establishment rhetoric and the need to ensure genuine change rather than a repeat of corrupt practices.

### Quotes

- "History doesn't repeat, but it rhymes."
- "You need all those conditions. Sometimes other things can substitute for them."
- "People only care about the pebble in their shoe."
- "Anti-establishment rhetoric, it's not really anti-establishment."
- "We just need to make sure that those who are pushing that, that are saying that they are going to make America great again, that they're really going to."

### Oneliner

Beau explains Castro's rise to power in Cuba, warns about deceptive anti-establishment rhetoric, and urges genuine transformation for America's future.

### Audience

History Buffs

### On-the-ground actions from transcript

- Advocate for genuine change and reforms within the government (exemplified)
- Educate others on the importance of understanding history and origin stories of events (suggested)

### Whats missing in summary

The emotional impact of historical events and the significance of learning from them. 

### Tags

#History #Revolution #Castro #AntiEstablishment #Reforms


## Transcript
Well howdy there internet people, it's Beau again.
We've talked about it on this channel before.
International events and international affairs, once the headlines start,
we don't get the back story. Because once it's news and once there's headlines,
we've already missed all the rising action. We don't get the origin story behind a lot of events
and how they came to be. So today we're going to talk about how Castro came to power.
Because I'm not sure many people know. All right, it's the 1950s, it's Cuba,
and there's this guy. He's got a military background and he wants a little more power.
So he paints himself as a man of the people, a populist, and he kind of reaches out
and gets some backing from a superpower, kind of becomes their puppet. Realizes he can't win
an election, but if you can't win at the ballot box, there's another way to do it. So he stages a coup.
And like many of his ilk, when he comes to power, well he starts curtailing civil liberties.
Good man of the people that he is. Gets rid of the ability to strike.
Staffs the government with his buddies. Turns into a kleptocracy.
They just start using their power to feather their own nests at the expense of the people.
That starts a revolutionary spirit of the country. Can't have that, so he starts curtailing dissidents.
The economy goes stagnant. The gap between the haves and the have-nots, it increases.
Income inequality grows. He cuts deals with international criminals.
And that revolutionary spirit keeps growing. So the executions come.
Now the high number is 20,000. My reading, my best guess, I'm not an authority, 10 to 15.
I've seen numbers as low as a few hundred. I'm pretty sure those aren't right.
And that's it. That's how Castro came to power. He overthrew that dude. That guy's name was Batista.
See, the conditions have to be right for revolution. You have to have certain things.
You have to have a government that isn't responsive, won't institute reforms.
So therefore people feel like revolution is the only option. They've got to be corrupt.
Be more interested in feathering their own nests than helping the people.
Be comfortable. People only care about the pebble in their shoe.
If everybody's comfortable, even if the government is corrupt, well, that kind of turns a blind eye to it.
But when that income inequality grows, people feel like they can't get a fair shake in the game.
They have to reset the game. You need all those conditions. Sometimes other things can substitute for them.
But you really need that. We've got most of them in the US.
Now, luckily in the US, because of our infrastructure and because we are incredibly comfortable,
we have a chance for that peaceful revolution.
The thing is, we just need to make sure that those who are pushing that,
that are saying that they are going to make America great again, that they're really going to.
Because a lot of times, anti-establishment rhetoric, it's not really anti-establishment.
It's not for the people. It's just designed to put a new set of people in charge so they can do the exact same thing.
We've seen this recently.
History doesn't repeat, but it rhymes. And a lot of things are rhyming right now.
Anyway, it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}