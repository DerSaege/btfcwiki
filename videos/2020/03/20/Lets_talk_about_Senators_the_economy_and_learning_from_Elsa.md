---
title: Let's talk about Senators, the economy, and learning from Elsa....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wJxM2XtGx9E) |
| Published | 2020/03/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Compares the current economic situation to an ice age in the movie Frozen, where essentials continue while everything else freezes.
- Mentions that the stock market is falling rapidly, but it doesn't represent the real economy at the ground level.
- Urges senators to provide cash infusions like blankets to help people who live paycheck to paycheck.
- Emphasizes that cash infusions are necessary to protect sectors and ensure people can pay their bills.
- Points out that without financial support, the economy will suffer, as people won't have money to spend post-crisis.
- Stresses the importance of giving people what they need and not nickel and diming in times of crisis.
- Calls for senators to understand that in survival situations, everyone becomes a socialist and cooperation is key.

### Quotes

- "You need to give them blankets. Those cash infusions that you are nickel and diming are those blankets."
- "In survival situations, in crisis situations, everybody becomes a socialist."
- "Give people what they need. Give them those blankets."

### Oneliner

Beau stresses the importance of providing necessary cash infusions like blankets to protect the economy and help those living paycheck to paycheck, urging senators to embrace cooperation in crisis situations.

### Audience

Senators, policymakers

### On-the-ground actions from transcript

- Provide immediate cash infusions to those living paycheck to paycheck (exemplified)
- Ensure sectors are protected by financial support (exemplified)
- Prioritize cooperation over competition in crisis situations (exemplified)

### Whats missing in summary

The full transcript provides a detailed analogy using Frozen's ice age to explain the current economic situation and stresses the importance of socialist principles in times of crisis.

### Tags

#Economy #CashInfusions #Cooperation #Socialism #CrisisResponse


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about what senators can learn about the economy from ELSA.
And if you are a senator, pay attention.
Read an article in the Atlantic today and it said it's not a recession, it's an ice
age.
Dun dun dun.
Yeah, yeah.
I think that's a good analogy right now.
It makes sense.
Specifically, it's the freeze that occurred in the movie Frozen.
What happens in that movie, there's somebody has something inside of them.
They can't control it.
Poses a risk to other people because it can't be controlled.
That person goes off to be by themselves.
The town breezes.
Everything stops except for essentials, right?
And meanwhile the government is out handing out blankets.
Trying to help people weather the storm.
Yeah.
Makes sense.
I like it.
It's a good analogy.
It makes sense.
And people can talk about the stock market.
It's falling at a faster rate than it did before the depression.
True.
True.
That's true.
That is a fact.
But that's not the real economy on the ground level, is it?
And let's be honest, I don't think anybody would look at what Disney, for example, is
trading at right now and say, hey, that's overvalued.
No, it's undervalued.
We all know that.
We all know that.
It will rebound once they start getting money again.
How are they going to get it?
Blankets.
The government's blankets in this case are the cash infusions.
I know a lot of you senators don't get this, but the average person in the U.S., they live
paycheck to paycheck.
They need help right now.
You need to give them blankets.
Those cash infusions that you are nickel and diming are those blankets.
And half measures are going to be half effective.
You guys are going to have to come off this.
You are going to have to come off this.
You got to get real comfortable with doing this because you're going to need to do it
as long as it's going on.
These infusions, they're not going to stimulate the economy immediately, but they're going
to protect those sectors that are still alive.
Right now, people will not be able to pay their bills, their basic bills.
Most people won't.
And you don't have the stick anymore.
There's not a lot of, I don't know any sheriff that's going to send his deputies out right
now to go evict somebody.
Yeah, give me that pen.
No, they're not going to do it.
You don't have the stick anymore.
You only have the carrot.
You need to start handing those carrots out real fast because that money will be used
to pay their rent, which will eventually be used to pay somebody's mortgage.
That mortgage goes to the banks.
Those banks are going to be necessary for the rebound.
This is simple.
Once this is over and people are given the all clear and get to go out again, they will.
Demand hasn't decreased.
I don't think anybody stuck at home watching this right now would choose to be here.
I mean, I know y'all like me and everything, but not that much.
You would like to be somewhere else, I imagine.
Demand hasn't decreased.
But because the average American lives paycheck to paycheck, because you senators will not
do anything about that, when this is over, they're not going to have any money.
You want the economy to rebound, you need to give them the cash to spend.
Wealth flows up.
And then you can accomplish your real goal.
Because there's no sense in lying about this anymore.
Everybody up there in those hallowed halls, what are y'all really about?
Protecting your donors and their wealth.
That's what you're concerned about.
You want to do that, you have to protect the little guy.
You don't have a choice.
And personally, I love it.
I think it's great.
You need to get real comfortable with those cash infusions.
Because as long as this goes on, you're going to have to make them.
Or it's all going to fall apart.
The system that you benefit from so much at the expense of others will come crashing down
around your ears.
So I would get real used to it and stop with the nickel and diming.
Give people what they need.
Give them those blankets.
And I know there's resistance because it's the dreaded S word.
It's socialism.
Oh no.
Here's the reality.
In survival situations, in crisis situations, everybody becomes a socialist.
Every sane person becomes a socialist.
We will get through this with cooperation, not competition.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}