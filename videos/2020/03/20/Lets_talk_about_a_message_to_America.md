---
title: Let's talk about a message to America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lu_58ZMe364) |
| Published | 2020/03/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump insulted a reporter who asked what he had to say to Americans who were afraid.
- Instead of inspiring the American people, Trump insulted the reporter, calling the question sensationalist.
- Fear is normal and can be a good motivator to take necessary precautions, but panic is the enemy.
- American exceptionalism could be used positively during this crisis, given the country's resources and diversity.
- The United States is being looked upon for leadership during this time, but it seems to be lacking.
- Many tough media personalities are having breakdowns on Twitter, but they do not represent what America truly stands for.
- People celebrate America for its working class and the ability to overcome hard times.
- The current situation does not require drastic action like storming beaches but rather staying calm and doing our part.
- Those providing essential services are conveying calm and helping everyone get through this crisis.
- The United States historically thrives on chaos and can lead itself through these challenging times.

### Quotes

- "Fear in some ways is good. Panic is the enemy."
- "We're not being asked to storm the beaches. We're being sent to go watch Netflix."
- "We are the United States. We thrive on chaos."

### Oneliner

President Trump missed an inspiring moment, but Americans can find strength in fear, avoid panic, and rely on their resilience during this crisis.

### Audience

Americans

### On-the-ground actions from transcript

- Remain calm and convey calm to others (exemplified)
- Find strength in fear to take necessary precautions (exemplified)
- Support those providing essential services (exemplified)

### Whats missing in summary

The full transcript provides additional context on American exceptionalism and the historical perspective of the United States in overcoming challenges.

### Tags

#Leadership #AmericanResilience #CalmAmidCrisis #CommunitySupport #USHistory


## Transcript
Well howdy there internet people, it's Beau again.
So the President of the United States was asked a question at a press conference.
The reporter asked him what he had to say to Americans who were afraid.
President Trump responded in true President Trump fashion.
He insulted the reporter.
Said he was being sensationalist.
That was not a sensationalist question.
That was a softball question designed to get a soundbite answer.
It was the President's opportunity to give one of those movie style speeches and inspire
the American people.
The American people are afraid.
And that's okay.
You're allowed to be afraid right now.
You're allowed to be scared.
It's perfectly normal.
Fear in some ways is good.
It gives you the motivation to take the precautions you need to stay alive.
Fear can be good.
Panic is the enemy.
Panic is something you're not allowed to do.
You're not allowed to do it, one, because it's self-defeating, and two, because you're
an American.
It boggles the mind.
This is the one time when American exceptionalism can be used for something good and nobody's
bringing it up.
You live in one of the wealthiest, most powerful countries on the planet.
We are incredibly resource rich.
We are incredibly diverse.
We will get through this.
We live in the greatest country on the earth.
You've heard that your whole life.
Why?
Why do we say that?
It's certainly not based on any statistics.
It's based on an attitude.
It's based on the achievements of those who came before us.
We won World War II.
It's what's in our history books.
We did that.
We, you and I, because we were there, right?
No.
It's us taking a collective pride in things that we had nothing to do with.
Taking pride in that reputation.
Now we're being asked to earn that reputation.
Not by our country, but by the world.
There are a lot of countries out there that are looking to the United States for leadership
and it is sorely lacking.
And yeah, a lot of the tough guy media personalities are on Twitter right now having mental breakdowns.
But that's fine, because let's be honest, they weren't really part of us anyway, were
they?
When people celebrate America, that's not what they're celebrating.
When people talk about the United States, they're talking about the working class.
They're panicking because they've never dealt with anything like this.
They've never been in a situation like that.
They haven't faced hard times.
You have, your neighbor has.
We'll get through this.
We are not being asked to storm the beaches.
We're not being sent to Iwo Jima.
We're being sent to go watch Netflix.
And then we have those people who are out there providing essential services and they're
doing their jobs and they're remaining calm and they're conveying calm to everybody else.
We're going to get through this.
We are the United States.
We thrive on chaos.
No, we don't know how long it's going to last.
The troops may not be home by Christmas, but that's okay because we're going to remain
calm even in the absence of real leadership.
We can lead ourselves.
Anyway, it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}