---
title: Let's talk about the numbers and mixed messaging....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fEmylMus014) |
| Published | 2020/03/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Encountered people ignoring medical advice due to low perceived risk.
- Explains the concept of a 0.2% chance to convey the real danger.
- Compares the risk to a Walmart analogy with a 500th person scenario.
- Urges following medical professionals' advice over politicians' on social media.
- Acknowledges differing opinions among medical professionals but advises following the strictest guidelines.
- Addresses accusations of mixed messaging regarding staying home.
- Reveals personal exposure risk due to wife being a medical professional.
- Describes going out to serve a greater cause while practicing strict self-isolation.
- Emphasizes the importance of staying home if there's no urgent need to go out.
- Urges not to risk life over economic concerns and not to use his actions as an excuse to violate self-isolation.
- Encourages listeners to determine their own level of involvement and prioritize safety.
- Reminds that the numbers represent lives, not just statistics on an exam.
- Urges trusting healthcare professionals over politicians during this crisis.
- Calls for heroism in staying home and following health guidelines.

### Quotes

- "Follow the advice of the medical professionals, not the advice of your favorite politician on Twitter."
- "You can be a hero right now by staying home."
- "It's your life."

### Oneliner

Beau stresses the importance of following medical advice during the pandemic, urging people to prioritize their lives over economic concerns and political influence, and be heroes by staying home.

### Audience

General public

### On-the-ground actions from transcript

- Stay home, wash hands, practice self-distancing, and follow medical professionals' advice (exemplified).
- Determine your level of involvement and prioritize safety (exemplified).

### Whats missing in summary

The emotional impact of choosing to stay home and prioritize safety during the pandemic.

### Tags

#Pandemic #HealthGuidelines #StayHome #MedicalAdvice #CommunitySafety


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about numbers and mixed messaging.
I was out and about today.
I was out and about.
I ran into people who should not be out and about.
And I asked them, I'm like, what are you doing out right now?
Well it's only a.2% chance a person's in my demographic, healthy my age.
Realize that's the problem.
That's why people are ignoring the advice of medical professionals.
.2% that means you got a 99.8% chance of being cool.
When you say it like that, I get it.
If you got a 99.8 on an exam, you'd be pretty happy.
Let's talk about what that actually means.
I want you to picture a Walmart.
Everybody knows what a Walmart looks like.
Two sets of doors, right?
Everybody that enters has to enter through the right door.
There's somebody standing there and they're counting out every 500th person.
If you're that 500th person, nothing happens to you right then.
You don't know.
Go on about your day.
But when you leave through the left side, bang, you're done.
It's over.
Would you still go to Walmart to get your Twinkies or Coke or whatever it is that's
so important?
Probably not.
Because it's immediate.
Now the reality is most projections are only saying 40, 50% are going to catch it.
So the number is closer to 1 in a thousand.
But I'm willing to bet you still wouldn't go.
Not if it was immediate.
Follow the advice of the medical professionals, not the advice of your favorite politician
on Twitter.
It's not a hoax.
It's not mass hysteria.
It's not a plot to undermine capitalism.
It's reality.
You can follow the advice of people who have absolutely no training in this, or the advice
of the people who have trained for this their entire lives.
Some of the medical professionals are differing on what you should do.
Go by the most strict.
You're talking about your life.
If you don't have a reason to go out, don't go out.
Now because of what I say on this channel versus what shows up on Twitter or Instagram
or wherever, I have been accused of mixed messaging.
You're telling people to stay home, but you're not staying home, are you Bo, you big hypocrite?
You're right, I'm not.
My wife is a medical professional.
She's going to be exposed to this.
Probably going to contract it.
Which means I'm going to be exposed to it.
I am in that 1 in 500 no matter what.
If I'm going to go out, I would rather go out serving something greater than myself.
So yes, today I was at Walmart.
Doing handheld radios and lights for a rural medical facility.
Went and did grocery deliveries for seniors so they could stay home.
Yeah, that's me.
I'm doing that.
But I'm self-isolating the rest of the time.
Get home, take a shower.
Every time I get out, in and out of the car, wash my hands.
I'm doing everything I can.
But I have a need to go out.
If you don't have a need, stay home.
There's no reason to risk your life over this.
None.
The economy isn't that important.
Don't use me as a reason to violate the self-isolation idea.
Don't do that.
I'm kind of dumb.
You have to determine your own level of involvement.
If you are not truly comfortable with 1 in 500 or whatever your demographic is, and in
some cases it is 1 in 10, stay home.
This is going to be going on for a while.
So get used to it now.
Understand that those numbers, it's not an exam.
It's not a grade on an exam.
It's your life.
It is your life.
Do you trust your life to politicians who have no training and are only concerned about
their portfolios?
Or do you trust your life to people who have dedicated their entire lives to the study
of this exact subject?
Health care professionals are not your enemy.
They have portfolios too.
This is the moment where you get to decide.
You know, there's a whole bunch of people that run around carrying a gun wanting to
be a hero.
You can be a hero right now by staying home.
Not exposing people.
Stay home.
Wash your hands, self-distancing, follow the advice of the medical professionals.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}