# All videos from March, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-03-31: Let's talk about unlocking the secrets of the universe from home.... (<a href="https://youtube.com/watch?v=M8JR9AqyJ6g">watch</a> || <a href="/videos/2020/03/31/Lets_talk_about_unlocking_the_secrets_of_the_universe_from_home">transcript &amp; editable summary</a>)

Beau introduces Galaxy Zoo, a project enabling people to unlock universe secrets from home during difficult times, offering a space-based escape and a positive distraction while aiding researchers in understanding galaxy evolution and interactions.

</summary>

"What better place to escape to than space?"
"Maybe there are some people out there that need something to look forward to, need something a little bit less real to look at."
"We are a very resilient species."

### AI summary (High error rate! Edit errors on video page)

Introduces a science project where people can help unlock universe secrets from home during this time of being stuck indoors.
Mentions the project, Galaxy Zoo, which involves looking at pictures of galaxies and answering questions to aid researchers.
Describes the multiple choice format and provides a field guide for understanding the task.
Talks about the excess of information captured compared to our ability to process it, making contributions valuable.
Expresses how engaging in this project can provide an escape to space for individuals feeling stressed or bored.
Emphasizes that participating helps researchers understand galaxy evolution, internal workings, interactions, and merges.
Acknowledges the present anxiety and suggests this project could offer a positive distraction and something to look forward to.
Notes the lack of immediate practical application but anticipates its future relevance.
Encourages staying positive and looking towards the future, as humanity has overcome challenges before.
Concludes with well wishes for the viewers.

Actions:

for science enthusiasts, space lovers,
Join Galaxy Zoo project to assist researchers in understanding galaxies (suggested)
Utilize spare time at home to contribute to the Galaxy Zoo project (implied)
</details>
<details>
<summary>
2020-03-31: Let's talk about the models being wrong.... (<a href="https://youtube.com/watch?v=CkU9IDBgl2w">watch</a> || <a href="/videos/2020/03/31/Lets_talk_about_the_models_being_wrong">transcript &amp; editable summary</a>)

Beau talks about the importance of acknowledging that social distancing works, despite political biases, and warns against disregarding evidence for personal beliefs.

</summary>

"The desire to set aside facts because you want to support a specific person is indicative of cult behavior."
"You made a bad choice. Everybody makes mistakes."
"If you disregard these precautions, you're only going to hurt yourself and those people you know."
"Is that level of selfishness and willful ignorance making America great?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talking about mistakes and misinterpreting data, leading to a lack of understanding of ongoing events.
People comparing initial predictions with current ones, dismissing the effectiveness of social distancing.
The importance of acknowledging that social distancing is working in lowering the number of cases.
Criticizing the president for extending social distancing but not admitting his initial models were incorrect.
Emphasizing the necessity to continue social distancing to prevent overwhelming medical infrastructure.
Warning against disregarding evidence and blindly supporting a specific person, likening it to cult behavior.
Encouraging people to prioritize public health over personal beliefs and political allegiances.
Reminding everyone to follow safety precautions like washing hands, not touching faces, and wearing masks.

Actions:

for general public,
Stay at home, follow social distancing guidelines, and wear a mask when going out (implied).
Wash hands frequently and avoid touching your face (implied).
</details>
<details>
<summary>
2020-03-31: Let's talk about The Time Machine, class, and a different reading.... (<a href="https://youtube.com/watch?v=Qb1ClxPq2A8">watch</a> || <a href="/videos/2020/03/31/Lets_talk_about_The_Time_Machine_class_and_a_different_reading">transcript &amp; editable summary</a>)

Beau dives into H.G. Wells' "The Time Machine" to draw parallels between class divisions in the book and global economic disparities, advocating for solidarity and responsible decision-making to prevent environmental destruction.

</summary>

"We are Eloi."
"We talk about working class solidarity a lot."
"We shouldn't have Morlocks."
"It is a global game."
"We have the means to make sure that everybody has what they need without destroying the planet."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of H.G. Wells' "The Time Machine" and delves into the theme of class.
Describes the two distinct groups in the book: the Eloi, portrayed as innocent and leisurely, and the Morlocks, depicted as strong and skilled.
Examines how the Morlocks essentially serve the Eloi and are implied to consume them.
Draws parallels between the book's class divide and real-world global economic disparities.
Points out that many viewers are likely part of the global elite, akin to the Eloi, and contrasts their challenges with those faced by the working class in less economically advantaged regions.
Raises concerns about the impact of production and consumption patterns on climate change, particularly affecting countries in the global south.
Advocates for global working class solidarity and responsible decision-making to prevent environmental destruction.
Concludes with a call to action for a more equitable world where everyone's needs are met without harming the planet.

Actions:

for global citizens,
Advocate for fair labor practices and support initiatives that empower workers globally (suggested)
Educate oneself and others on the impacts of global economic disparities on climate change (implied)
</details>
<details>
<summary>
2020-03-30: Let's talk about why the hospitals are empty.... (<a href="https://youtube.com/watch?v=o9VlKiOLLW0">watch</a> || <a href="/videos/2020/03/30/Lets_talk_about_why_the_hospitals_are_empty">transcript &amp; editable summary</a>)

People filming empty hospitals for conspiracy theories ignore the reality of medical procedures during a crisis and endanger public health by spreading misinformation.

</summary>

"You want to stand outside a hospital and film it, whatever. Enjoy your hobby."
"There is no curtain. There's certainly not a man behind the curtain."
"People like you, who are running into these places and going around town, you're the reason it's going to get real bad."

### AI summary (High error rate! Edit errors on video page)

People are filming outside empty hospitals, claiming it's proof that the situation is overblown and sensationalized.
Deanna Lorraine, a Republican candidate, and Sarah A. Carter from Fox News contributed to spreading this theory.
Hospitals are empty because non-urgent operations were shut down per Surgeon General's advice.
Most people going to emergency rooms are not experiencing actual emergencies.
Filming inside hospitals and questioning why patients aren't visible shows ignorance about isolation procedures.
Many hospitals have restricted visitors and are keeping patients separated.
People seek out these theories to find comfort in the chaos and believe in a sense of control.
The President's statement on potential loss due to the pandemic underscores the seriousness of the situation.
Taking precautions, staying at home, and following hygiene practices are critical.
Actions like filming inside hospitals can increase risks and contribute to the severity of the situation.

Actions:

for social media users,
Stay at home, wash your hands, and avoid touching your face to prevent the spread of the virus (exemplified)
Refrain from entering hospitals unnecessarily and filming inside medical facilities (exemplified)
</details>
<details>
<summary>
2020-03-30: Let's talk about healthcare workers, duty, accusations, and obligations.... (<a href="https://youtube.com/watch?v=b-oOpAX3ZUw">watch</a> || <a href="/videos/2020/03/30/Lets_talk_about_healthcare_workers_duty_accusations_and_obligations">transcript &amp; editable summary</a>)

Beau stresses the duty and support owed to healthcare workers on the front lines, likening them to troops without proper armor.

</summary>

"It should be a national scandal that we are sending our troops to the front lines without their armor."
"When it's over, they're not quitting because of the risk. They're quitting because we're not giving them the support they deserve and they need."
"Lead from the front, sir. Set the example."
"When this is over, I am so done. I am out of here after this."
"But see, the thing is, when it's over, they probably won't quit because they actually care about their job."

### AI summary (High error rate! Edit errors on video page)

Addressing duty, obligations, and accusations in relation to supporting healthcare workers.
Comparing healthcare workers to troops on the front lines.
Criticizing the lack of support healthcare workers receive.
Drawing parallels to past scandals where troops lacked necessary equipment.
Calling out government leadership for blaming frontline workers instead of taking responsibility.
Challenging accusations without evidence.
Questioning the hoarding of ventilators by hospitals.
Encouraging the release of unused ventilators for those in need.
Urging leadership to lead by example and prioritize the needs of healthcare workers.
Expressing concern over healthcare professionals contemplating quitting due to lack of support.
Emphasizing that healthcare workers care about their job and the people they serve.
Suggesting that those dedicated to their duty are the ones needed most during challenging times.

Actions:

for healthcare advocates,
Release unused ventilators for those in need (suggested)
Advocate for better support and resources for healthcare workers (implied)
</details>
<details>
<summary>
2020-03-29: Let's talk about what $1200 checks can teach us.... (<a href="https://youtube.com/watch?v=NeE6LslIAoQ">watch</a> || <a href="/videos/2020/03/29/Lets_talk_about_what_1200_checks_can_teach_us">transcript &amp; editable summary</a>)

Analyzing the $1200 stimulus checks, the challenges of minimum wage workers, and the need for decent wages for all Americans, especially during crises.

</summary>

"You're paying for it. You are providing a subsidy to big business out of your pocket."
"Minimum wage workers in this country are being exploited."
"Everybody who's working should have a decent wage."
"Essential employees [...] I think they deserve decent wages."
"This is something we might want to look at."

### AI summary (High error rate! Edit errors on video page)

Analyzing the $1200 stimulus checks and the reasoning behind the amount.
Exploring the challenges faced by minimum wage workers in the United States.
Addressing the difficulty in garnering empathy for minimum wage workers.
Pointing out the propaganda surrounding minimum wage workers and the need for self-interest to drive change.
Revealing how taxpayers end up subsidizing big businesses that pay low wages.
Providing historical context on the minimum wage and Franklin Roosevelt's perspective.
Arguing for a living wage for all Americans, not just a subsistence level.
Emphasizing the importance of decent wages for all workers, especially those deemed "essential" during the current crisis.

Actions:

for taxpayers, activists, workers,
Advocate for fair wages for all workers (implied)
Support policies that ensure a living wage for all Americans (implied)
</details>
<details>
<summary>
2020-03-29: Let's talk about Trump shipping PPE overseas.... (<a href="https://youtube.com/watch?v=hfyVDydFETo">watch</a> || <a href="/videos/2020/03/29/Lets_talk_about_Trump_shipping_PPE_overseas">transcript &amp; editable summary</a>)

Beau questions Trump's motives in sending protective equipment abroad, criticizes his lack of leadership, warns about the danger of false information during a crisis, and urges people to trust knowledgeable sources.

</summary>

"The president of the United States is a danger to the people of the United States."
"False hope is extremely dangerous in a situation like this."
"His desire to paint this rosy picture is harmful."
"We have to wonder why."
"Listen to the people that actually know what they're talking about."

### AI summary (High error rate! Edit errors on video page)

Beau addresses his recent decision to not talk much about Trump, focusing instead on the immediate problems at hand during the current crisis.
He questions the administration's decision to provide protective equipment to foreign countries while medical professionals in the US are struggling to find the same equipment.
Beau expresses curiosity about what Trump gained in return for sending protective equipment abroad, suspecting personal benefits rather than altruistic motives.
He criticizes Trump for not following established crisis management plans and for spreading false information that endangers public health.
Beau questions the media's continued coverage of Trump despite his misleading statements and lack of leadership.
He warns about the danger of false hope and misinformation during a crisis and urges people to listen to experts rather than Trump.
Beau concludes by urging listeners to stay informed and trust knowledgeable sources during these challenging times.

Actions:

for community members,
Listen to experts and trusted sources for accurate information (implied)
Stay informed about the situation from reliable sources (implied)
</details>
<details>
<summary>
2020-03-29: Let's talk about Joseph Lowery's lesson to us all.... (<a href="https://youtube.com/watch?v=t3U34uh2Yxk">watch</a> || <a href="/videos/2020/03/29/Lets_talk_about_Joseph_Lowery_s_lesson_to_us_all">transcript &amp; editable summary</a>)

Beau sheds light on Joseph Lowery, a pivotal figure in American civil rights history, showcasing the essence of being a supportive ally and amplifying marginalized voices.

</summary>

"He provided the nation with a shining example of what it means to be a good ally."
"He understood what it meant to be a good ally, to help elevate others."
"That's why he's the dean of American civil rights rather than just the dean of black civil rights."

### AI summary (High error rate! Edit errors on video page)

Introducing the topic of Joseph Lowery, a significant figure in civil rights history who deserves more attention from the media.
Lowery played a key role in the Civil Rights Movement, including during the Montgomery bus boycott and the Selma to Montgomery march.
Despite his major contributions, Lowery's legacy may not receive the comprehensive recognition it deserves.
Lowery's impact extended beyond racial equality as he advocated for other marginalized groups, like the LGBTQ+ community.
He used his influence and privilege to amplify the voices of others, showcasing what it truly means to be a good ally.
Lowery's approach involved supporting and empowering others rather than seeking the spotlight for himself.
His actions exemplify the essence of being a supportive ally and helping others gain power and agency.
Beau underscores the importance of understanding Lowery's broader impact on American civil rights, not just within the black community.
Lowery's legacy is a reminder of the power of quiet, behind-the-scenes advocacy and support for marginalized communities.
Beau encourages the media to ensure that Lowery receives the recognition and memorial he deserves, portraying him as the "Where's Waldo of American civil rights."

Actions:

for history enthusiasts, activists,
Honor Joseph Lowery's legacy by amplifying the voices of marginalized communities (implied)
Advocate for civil rights and equality for all, following Lowery's example of being a supportive ally (implied)
</details>
<details>
<summary>
2020-03-28: Let's talk about what the rich are teaching us about the future.... (<a href="https://youtube.com/watch?v=bMLQUArdeAw">watch</a> || <a href="/videos/2020/03/28/Lets_talk_about_what_the_rich_are_teaching_us_about_the_future">transcript &amp; editable summary</a>)

Beau explains how the wealthy have advantages in crises, urges stocking up on essentials post-crisis to aid community preparedness, and stresses the importance of being ready for future emergencies.

</summary>

"The wealthy. The wealthy."
"Get more than you need. Get more than your family needs. Because your community is going to depend on you."
"It's not delicious, but it will keep you alive and it will give you a sense of comfort anytime the shelf is empty."

### AI summary (High error rate! Edit errors on video page)

Talks about the lessons to be learned from the current situation, focusing on access to resources.
Points out how the wealthy have advantages in times of crisis like being able to access tests, fly out of affected areas, and isolate easily.
Mentions that major events will continue to favor the wealthy.
Predicts more incidents like the current crisis as climate change accelerates, leading to expensive necessities.
Explains how production of necessary items is currently high due to demand and cost, but prices will drop significantly after the crisis.
Advises stocking up on needed items once the crisis is over, especially for those who can make large purchases.
Emphasizes the importance of being prepared with supplies that have long shelf lives and can benefit the community.
Recommends having food reserves and mentions affordable food buckets with a month's supply.
Encourages readiness for future crises by taking advantage of surplus supplies post-crisis.

Actions:

for prepared individuals and community members.,
Stock up on necessary items post-crisis (suggested).
Ensure proper storage of essentials (implied).
Purchase affordable food buckets for a month's supply (implied).
</details>
<details>
<summary>
2020-03-28: Let's talk about the prepping community and a heart to heart.... (<a href="https://youtube.com/watch?v=PJGF2TnhYyI">watch</a> || <a href="/videos/2020/03/28/Lets_talk_about_the_prepping_community_and_a_heart_to_heart">transcript &amp; editable summary</a>)

Addressing the prepping community, Beau stresses sharing resources, understanding tactics beyond firearms, and advocating for proactive assistance in times of crisis to save lives.

</summary>

"Your security detail is out of ammo."
"Do what the whole theory is about. You've prepared for this moment. You can save lives."
"Get them the supplies they need."
"A gun is the last line."
"Be the hero."

### AI summary (High error rate! Edit errors on video page)

Addressing the prepping community, Beau stresses the importance of sharing resources and preparedness.
He questions the reasoning behind stockpiling specific calibers of ammunition and firearm choices within the community.
Beau explains the concept of pooling resources and the necessity of sharing within a network for security.
He urges preppers to understand the tactics associated with medical supplies like masks, especially during times of crisis.
Beau calls on individuals with excess resources to assist healthcare workers by providing necessary supplies.
He warns against solely focusing on gathering equipment without learning the necessary strategies and tactics to survive.
Beau advocates for being proactive in helping others and saving lives during emergencies.
He encourages preppers to think beyond just having equipment and to prioritize learning how to effectively utilize resources.
Beau acknowledges the surplus of supplies within the prepping community and the potential to make a significant impact by donating to local hospitals.
He underscores the importance of readiness and being able to adapt and assist during challenging situations.

Actions:

for prepping community members,
Share excess medical supplies with healthcare workers (exemplified)
Assist local hospitals by donating surplus resources (implied)
</details>
<details>
<summary>
2020-03-28: Let's talk about government assistance and advice.... (<a href="https://youtube.com/watch?v=dLqCjvGtCOs">watch</a> || <a href="/videos/2020/03/28/Lets_talk_about_government_assistance_and_advice">transcript &amp; editable summary</a>)

Beau addresses government assistance stigma, challenges privileged advice, and urges against blaming those in worse positions, advocating for destigmatization as more people may need assistance soon.

</summary>

"Somebody with less money, less power, and less influence is not the source of anything going wrong in your life."
"Those in a worse position than you are never the source of your problems."
"The source of your problems do not emanate from Section 8 housing."
"We need somebody to look down on and blame."
"We need to destigmatize government assistance because we're all about to be on it."

### AI summary (High error rate! Edit errors on video page)

Addresses government assistance stigma and advice from a privileged perspective.
Criticizes common advice about planning better, getting a valuable education, finding work, and joining the army.
Points out the hypocrisy of those who give advice but still accept government assistance.
Questions whether any advice given doesn't apply to everyone.
Acknowledges the importance of jobs that are now deemed non-essential during the crisis.
Compares government assistance to a stimulus to get through tough times.
Talks about how crises like poverty and lack of opportunities disproportionately affect those at the bottom.
Encourages cashing the government assistance checks but warns against looking down on others in worse positions.
Challenges the notion that people in Section 8 housing are the source of problems, instead pointing to those in power.
Calls for destigmatizing government assistance as more people may need it soon.

Actions:

for advocates for social justice,
Destigmatize government assistance (advocated)
Avoid blaming those in worse positions (advocated)
Challenge privilege and hypocrisy (advocated)
</details>
<details>
<summary>
2020-03-27: Let's talk about what makes a government.... (<a href="https://youtube.com/watch?v=SFbnUqZfqwk">watch</a> || <a href="/videos/2020/03/27/Lets_talk_about_what_makes_a_government">transcript &amp; editable summary</a>)

Governments rely on the perception of authority and a monopoly on violence to control, not protect, citizens, with loyalty and patriotism being optional.

</summary>

"A government does not have to do anything for the people that it controls."
"Patriotism, loyalty, all of these things. It's not a requirement. It isn't."
"Governments don't need flags. They don't need songs. They don't need anything."

### AI summary (High error rate! Edit errors on video page)

Governments require the perception of authority and a monopoly on violence to exist.
A government's obligation is to control, not protect, its citizens through force.
Patriotism, loyalty, and obligation to protect citizens are not necessary for a government to exist.
Governments can delegate parts of their monopoly on violence to the people.
The key function of a government is to preserve itself, not necessarily to look out for its citizens.
Governments can be small or large, with or without territory, as long as they have authority and a monopoly on violence.
The governments' job is to control people, not to serve them.
The perception of authority is vital for a government to maintain power.
Governments can weaken and fail if the populace loses faith in their authority.
Refusing to accept the authority of a government can be a form of resistance.

Actions:

for citizens, activists, community members,
Challenge authority through peaceful resistance (implied)
Support movements that question governmental control (implied)
Educate others on the nature of government authority (implied)
</details>
<details>
<summary>
2020-03-27: Let's talk about the troops you'll see.... (<a href="https://youtube.com/watch?v=fYVRXeVWM3k">watch</a> || <a href="/videos/2020/03/27/Lets_talk_about_the_troops_you_ll_see">transcript &amp; editable summary</a>)

Beau assures that military personnel activated in response to recent events are there for support, not to impose martial law, urging calm and trust in their expertise.

</summary>

"These guys are not there to impose, they are not there to impose martial law."
"Everybody needs to calm down."
"Do not panic."

### AI summary (High error rate! Edit errors on video page)

Beau starts off by providing a quick update to alleviate concerns surrounding the activation of plans due to recent events.
Initially, Beau was not worried as experts and plans were in place, but he later realized that the plans had not been activated.
Some plans are now being activated in response to the situation, prompting Beau to address concerns arising from social media.
Military personnel in uniform, such as the third and fourth sustainment units, are not there to impose martial law but are logistics experts providing support like setting up mobile hospitals and transportation.
National Guard troops are also present, with some individuals within the Department of Defense's command structure acting as liaisons in local police departments.
Beau reassures that the presence of CBRN teams with specialized equipment should not cause panic as these plans have existed since the 1960s and have never been activated before.
The activated personnel are not intended for civil insurrection suppression and are unlikely to be armed, similar to National Guard responses post-natural disasters.
Beau urges people to remain calm and let the activated personnel do their jobs, likening the situation to post-disaster recovery efforts and not martial law.

Actions:

for community members,
Trust the expertise of activated military personnel in providing support (implied).
</details>
<details>
<summary>
2020-03-27: Let's talk about a ray of hope and Mary.... (<a href="https://youtube.com/watch?v=p2Jrdzt2JpU">watch</a> || <a href="/videos/2020/03/27/Lets_talk_about_a_ray_of_hope_and_Mary">transcript &amp; editable summary</a>)

Beau shares a cautionary tale of Typhoid Mary to illustrate the importance of not blindly believing what we hope for, amidst discussing a UK study suggesting potential immunity to the virus.

</summary>

"We're going to have to pretend like it's not until we know for sure."
"We don't want to get out, cause a bunch of problems, and then have to go back."

### AI summary (High error rate! Edit errors on video page)

Introduces a story about a woman in the early 1900s known as Typhoid Mary, an asymptomatic carrier of typhoid who continued to spread the disease despite warnings.
Shares a cautionary tale of how believing something contrary to evidence can be dangerous, using Typhoid Mary as an example.
Mentions a study from the UK suggesting that many people may have already been exposed to and contracted the current virus, providing potential immunity.
Expresses his desire to believe the study's findings as it could mean being out of the woods from his illness.
Cautions against blindly accepting information just because we want it to be true, urging people to continue following safety measures until the study's results are confirmed.

Actions:

for health-conscious individuals,
Stay at home, wash hands, avoid touching face, practice social distancing (implied)
</details>
<details>
<summary>
2020-03-26: Let's talk about what's going to happen.... (<a href="https://youtube.com/watch?v=_BHlS64lyvU">watch</a> || <a href="/videos/2020/03/26/Lets_talk_about_what_s_going_to_happen">transcript &amp; editable summary</a>)

Today is expected to be a challenging day with signs pointing to it being bad, but despite the rough times ahead, we will get through it together.

</summary>

"Today's probably going to be a bad day."
"We are all in this together."
"Even if you are isolated and you are by yourself, you're not alone."
"The entire world is rooting for you."
"We will get through this together."

### AI summary (High error rate! Edit errors on video page)

Today is expected to be a challenging day with signs pointing to it being bad.
The day that many have feared and hoped wouldn't come is here.
Denial is about to be shattered, leading to anxiety, fear, anger, and tragedy.
Despite being isolated, we are not alone in facing these challenges.
This global crisis is a reminder that we are all in this together.
The world is united in fighting against this crisis.
Even though historically oceans have protected us, they cannot shield us from this crisis.
It is a time to come together while staying apart.
Limiting media consumption, especially if not in an affected area, is recommended.
Despite the rough times ahead, we will get through it together.

Actions:

for global citizens,
Limit media consumption, especially if not in an affected area (suggested)
Share hope with those feeling anxiety and fear (implied)
Support others and stay connected while staying apart (implied)
</details>
<details>
<summary>
2020-03-25: Let's talk about Rule 303 and the Red Cross.... (<a href="https://youtube.com/watch?v=_QVMtNVloRM">watch</a> || <a href="/videos/2020/03/25/Lets_talk_about_Rule_303_and_the_Red_Cross">transcript &amp; editable summary</a>)

Beau stresses taking responsibility and contributing, especially through blood donations, during the global battle against shortages.

</summary>

"If you have the means, you have the responsibility."
"We're in a global battle, and we all have to pitch in."
"Desperately need the products."
"Get out there, go do it."
"We all want to help."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of taking action and responsibility, especially for those with the means.
Mentions the impact of viewer participation through watching videos and live streams.
Talks about the critical shortage of medical supplies, including blood products.
Mentions the cancellation of blood drives due to restrictions on large gatherings.
Encourages those who are eligible and able to donate blood through organizations like the Red Cross.
Emphasizes the importance of following eligibility criteria and precautions before donating.
Acknowledges that not everyone may be in a position to leave their house to donate.
Urges individuals to help if they can, especially if they are universal donors.
Stresses the global need for everyone to contribute during this pandemic.
Concludes by encouraging people to take action and help in any way they can.

Actions:

for donors,
Donate blood through organizations like the Red Cross (suggested).
Follow eligibility criteria and precautions before donating (implied).
Help by being a universal donor (implied).
</details>
<details>
<summary>
2020-03-25: Let's talk about Britney Spears, Fran Drescher, Starlet, and systemic change.... (<a href="https://youtube.com/watch?v=0UQg-8kLetM">watch</a> || <a href="/videos/2020/03/25/Lets_talk_about_Britney_Spears_Fran_Drescher_Starlet_and_systemic_change">transcript &amp; editable summary</a>)

Beau talks about the importance of mainstream figures like Britney Spears and Fran Drescher endorsing radical ideas for systemic change and the need to support and encourage such voices.

</summary>

"You go to war with the army you have. They enlisted. Give them the support they need."
"We need to encourage more celebrities, more people with influence outside of the system to introduce those radical ideas to the general populace, to the mainstream."
"If we want systemic change, we have to act like it."

### AI summary (High error rate! Edit errors on video page)

Soviets sent up Sputnik 10 in 1961 with a mannequin and a dog named Starlet, the first dog in orbit, to capture the imagination and bring attention to space travel.
Britney Spears and Fran Drescher have endorsed radical ideas for systemic change on social media, sparking mixed responses.
Radical ideas related to systemic change are scary as they challenge the current system and require embracing the unknown.
Mainstream figures like Britney Spears carrying radical ideas make them less threatening and help in spreading the message.
Systemic change won't come from political leaders or the system itself, as evidenced by the stimulus package's allocation of funds.
It becomes easier to question and advocate for radical ideas when influential figures like Britney Spears and Fran Drescher openly support them.
Advocating for systemic change during challenging times is vital to push for change when the situation improves.
Encouraging more celebrities and influential figures to introduce radical ideas to the mainstream is necessary for systemic change.

Actions:

for advocates for systemic change,
Support and encourage celebrities and influential figures endorsing radical ideas (suggested)
Advocate for systemic change in your community and beyond (implied)
</details>
<details>
<summary>
2020-03-24: Let's talk about societal growth, the world we want, and how to get it.... (<a href="https://youtube.com/watch?v=zXe9PxMxIFI">watch</a> || <a href="/videos/2020/03/24/Lets_talk_about_societal_growth_the_world_we_want_and_how_to_get_it">transcript &amp; editable summary</a>)

In times of societal unrest, it's imperative for ordinary people worldwide to define their vision for change and take tangible steps towards a better future, lest they remain at the mercy of those in power.

</summary>

"We need a clear picture. We need that picture. We need that roadmap."
"We're the ones that actually want the change."
"We're going to have the opportunity to demand it."
"We've got to shatter the mystery about what we want and how we want to get there."
"It might be a nice reprieve to think about the positive aspects, to think about a positive future."

### AI summary (High error rate! Edit errors on video page)

Situations like the current one prime everyone for societal growth, fostering a desire for improvement and forward-thinking.
The establishment takes advantage of such situations to make power grabs, seeking more control and money.
It's time for ordinary people worldwide to embrace change and play the same game as those in power.
We need a clear vision of the society we want and tangible steps to achieve it, rather than just a wish list.
While many agree on universal needs like food, healthcare, housing, and education, the path to achieving these essentials requires specific strategies.
It's not enough to have utopian ideals; we must identify achievable stepping stones to progress.
When the dust settles, we'll have the chance to shape the world we live in, but only if we know what we want and have a plan.
Without a clear vision, we risk being at the mercy of those who benefit from the current system.
Beau encourages viewers to share their ideas for an ideal world or steps toward positive change, promoting civil debate and forward-thinking.
It's vital to have a roadmap and a clear picture of the future to guide our actions and avoid being passive recipients of the status quo.

Actions:

for global citizens,
Share your ideas for an ideal world and tangible steps towards positive change through civil debate (suggested)
Engage in constructive dialogues with others to envision a better future and outline achievable steps (suggested)
</details>
<details>
<summary>
2020-03-24: Let's talk about Dan Patrick, the beloved America, and authority... (<a href="https://youtube.com/watch?v=JCLEDQvmbgc">watch</a> || <a href="/videos/2020/03/24/Lets_talk_about_Dan_Patrick_the_beloved_America_and_authority">transcript &amp; editable summary</a>)

Lieutenant Governor's economic focus over lives, American ruling class prioritizing profit, and the need for genuine representation in leadership during the pandemic.

</summary>

"Maybe that isn't the America everybody loves."
"How can you believe somebody this callous and this incompetent has authority over you?"
"The decisions that are being made are going to cost thousands of lives, and it's all about money."
"If we are going to have a representative democracy, it needs to be representative."
"We just have to do what we need to do. We've got to shut it down."

### AI summary (High error rate! Edit errors on video page)

Lieutenant Governor Dan Patrick of Texas made a statement regarding older Americans and passing down the America they love to their grandkids, but then he tied it to the economy and suggested business as usual, even if only one in five older Americans might die.
In California, 80% of COVID patients are aged 18 to 65, showing that it's not just the elderly who are affected.
The American ruling class is prioritizing profit over lives, refusing to take necessary actions like a 21-30 day shutdown that experts recommend.
Beau questions whether an America willing to sacrifice lives for profit is worth saving and suggests that those in power making such decisions need to be replaced.
He stresses the importance of preparing for significant societal changes post-pandemic and calls for genuine representation in leadership that prioritizes lives over money.

Actions:

for politically active citizens,
Replace leaders prioritizing profit over lives and genuine representation (implied)
Advocate for necessary actions to protect lives over economic interests (implied)
Prepare for significant societal changes post-pandemic (implied)
</details>
<details>
<summary>
2020-03-23: Let's talk about what to do with your kids.... (<a href="https://youtube.com/watch?v=n5c522V1MIs">watch</a> || <a href="/videos/2020/03/23/Lets_talk_about_what_to_do_with_your_kids">transcript &amp; editable summary</a>)

Beau shares practical advice on creating a routine, engaging kids in activities, and cherishing the time spent with them during challenging times.

</summary>

"Have a schedule and keep their activities limited to an hour or so and make sure that they get some exercise."
"You got a chance to spend a lot of time with your kids and there will be a time later when you wish you could have spent more."
"They're a little out of sorts too. So give them a routine."
"But just keep them engaged and break it up by time periods."
"This is the opportunity to show them."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the challenges parents are facing with kids at home due to the current situation, with phones ringing off the hook.
He stresses the importance of thinking like a teacher, having a schedule, planning activities, and being mindful of kids' short attention spans.
Beau advises on watching children's diet and ensuring they get physical activity, even if they can't go outside.
He suggests using YouTube for exercise programs and educational videos for crafts and science projects.
Beau encourages involving kids in household tasks and repairs, viewing it as an opportunity to teach them valuable skills.
He reminds parents to cherish the time spent with their children during this period and to be patient and understanding.
Beau recommends being empathetic towards teachers who will have to handle many students once schools reopen.
For older kids, he suggests having meaningful and nostalgic conversations, seeing the positive side of the situation.
Beau underlines the importance of maintaining a schedule, limiting activities, ensuring exercise, and providing proper meals for children.
He acknowledges the challenges of the current situation but motivates to make the best of it and cherish the time spent with kids.

Actions:

for parents, caregivers,
Involve your kids in household tasks and repairs, teaching them valuable skills (implied)
Have meaningful and nostalgic conversations with older kids (implied)
Maintain a schedule for children's activities and ensure they get exercise (implied)
</details>
<details>
<summary>
2020-03-23: Let's talk about saving the economy by staycationing.... (<a href="https://youtube.com/watch?v=3yrmLQb0chQ">watch</a> || <a href="/videos/2020/03/23/Lets_talk_about_saving_the_economy_by_staycationing">transcript &amp; editable summary</a>)

Act decisively now to protect lives and the economy by supporting strict stay-at-home measures for 21 or 30 days.

</summary>

"Flatten the curve, not the economy."
"Stay home. It doesn't matter what the president says."
"If you want to protect the economy, support a 21 or 30 day, everybody stay at home."
"Your portfolio during that period, yeah, it's going to take a hit."
"This is how we buy enough time for treatments and preventative measures to be developed."

### AI summary (High error rate! Edit errors on video page)

Economic pundits push the idea that measures taken are detrimental to the economy.
Half-hearted measures will be ineffective and prolong economic slowdown.
Acting decisively now will benefit the economy in the long run.
Indecision and contradictory advice will destroy the U.S. economy.
Flattening the curve is vital to prevent a prolonged economic downturn.
Failure to support measures will prolong economic hardship and endanger lives.
Uncertainty and flare-ups due to lack of action will hinder economic recovery.
Action is needed, not just talk or tweets.
Support staying at home measures for 21 or 30 days to curb the spread.
Experts agree on the necessity of strict measures to protect society.
Disregarding advice will harm the economy and people's lives in the long term.
Temporary portfolio hits are a necessary sacrifice for long-term economic stability.
Time is needed for treatments and preventative measures to be developed and tested.
Collaboration and unity are key to overcoming the crisis.
Failure to act now will lead to catastrophic consequences for the economy and lives.

Actions:

for policy makers, citizens, leaders,
Support a 21 or 30-day stay-at-home period to curb the spread (implied).
Stay home to protect lives and the economy (implied).
Act together with your community to prevent economic devastation (implied).
</details>
<details>
<summary>
2020-03-23: Let's talk about a staycation scavenger hunt.... (<a href="https://youtube.com/watch?v=o8LKPydwS0k">watch</a> || <a href="/videos/2020/03/23/Lets_talk_about_a_staycation_scavenger_hunt">transcript &amp; editable summary</a>)

Beau encourages building bug out bags at home using only household items, fostering emergency preparedness for various scenarios and sharing for learning.

</summary>

"We are all going to build bug out bags out of the stuff that we have at home."
"We are going to exponentially increase the number of people who are prepared for an emergency."
"Improving on it, in this scenario, yeah, everybody's staying at home. In the next scenario, you may not be able to."

### AI summary (High error rate! Edit errors on video page)

Encourages a scavenger hunt to build bug out bags at home for emergency preparedness.
Rules: only use items at home, nothing from existing kits, and items once in the bag stay in the bag.
Goal is to increase the number of people prepared for emergencies.
Participants to post photos of their bug out bags on social media under the hashtag BowBag for learning and sharing.
Items in the bag should include food, water, fire, shelter, medicine, and a knife, with flexibility in interpretation.
Beau will choose the best bag and send a reward.
Reminder to continuously improve the bag for different emergency scenarios.
Emphasizes the importance of being prepared for various emergencies.

Actions:

for individuals at home,
Build a bug out bag using items at home and follow the guidelines provided (suggested)
Share a photo of your bug out bag on social media under the hashtag BowBag (suggested)
Continuously improve and update your bug out bag for different emergency scenarios (suggested)
</details>
<details>
<summary>
2020-03-22: Let's talk about what happens when the trucks stop.... (<a href="https://youtube.com/watch?v=1KCXmZ6l1j8">watch</a> || <a href="/videos/2020/03/22/Lets_talk_about_what_happens_when_the_trucks_stop">transcript &amp; editable summary</a>)

Beau outlines the critical role of truckers, the impact of a trucking halt, and the need to address infrastructure vulnerabilities post-crisis.

</summary>

"Everything I'm about to say, every three letter agency in the world is fully aware of."
"Your big box stores and your grocery stores most operate on just-in-time delivery."
"That's how critical trucking is to the United States."
"This is something we might want to address."
"When the trucker cuts you off by accident, let it slide, especially right now."

### AI summary (High error rate! Edit errors on video page)

Addresses truckers' concerns from previous comments.
Describes the critical role of truckers in the functioning of the country.
Clarifies that the current shortages are due to increased demand, not supply issues.
Assures that there are contingency plans in place to prevent a complete trucking shutdown.
Outlines a hypothetical scenario if trucks were to stop rolling.
Details the cascading effects of a trucking halt on fuel, medical supplies, food, and more.
Points out the impact on everyday life, like garbage piling up and fast food shortages.
Explains how trucking is vital for keeping stores stocked due to just-in-time delivery systems.
Stresses the importance of tap water supply and the potential risks if trucking were disrupted.
Advocates for revamping infrastructure post-crisis to address vulnerabilities.

Actions:

for transportation industry stakeholders,
Support local truckers by offering assistance or understanding their challenges (suggested)
Advocate for infrastructure improvements to ensure smoother operations in emergencies (implied)
</details>
<details>
<summary>
2020-03-22: Let's talk about ethics, the voluntary, and the mandatory.... (<a href="https://youtube.com/watch?v=JcvVRWyNRoo">watch</a> || <a href="/videos/2020/03/22/Lets_talk_about_ethics_the_voluntary_and_the_mandatory">transcript &amp; editable summary</a>)

Beau talks about acting voluntarily now to prevent mandatory actions later, stressing global cooperation in facing crises.

</summary>

"This is a responsibility to humanity that we all share."
"Those lines on a map are worthless."
"The cooperation and mobilization that is taking place right now is what will be needed to combat climate change."

### AI summary (High error rate! Edit errors on video page)

Talking about morals and ethics to do things voluntarily before they become mandatory.
Urges people to think about society as a whole, not just the current situation.
Mentions pushback on his statement that every sane person becomes a socialist during an emergency.
Gives examples from movies where people cooperate in survival situations.
References historical instances of rationing during the 1940s.
Questions why some people are not following the advice to stay at home during the current crisis.
Emphasizes the global nature of the emergency and the responsibility to humanity.
Talks about the need for large changes in philosophical outlook post-crisis.
Stresses the importance of global cooperation in facing future threats like climate change.
Raises the choice between voluntary action now or mandatory action later due to survival necessity or government force.

Actions:

for global citizens,
Stay at home voluntarily to prevent the need for mandatory lockdowns (exemplified)
Cooperate with global efforts to combat climate change through voluntary actions (suggested)
</details>
<details>
<summary>
2020-03-21: Let's talk about those keeping the country running.... (<a href="https://youtube.com/watch?v=2FUw55e59YM">watch</a> || <a href="/videos/2020/03/21/Lets_talk_about_those_keeping_the_country_running">transcript &amp; editable summary</a>)

Recognize the vital role of low-wage workers, ensure decent living standards, and support them politically for a fair society.

</summary>

"If a job is being performed and it is essential, it must occur for society to function."
"I think that's the bare minimum that we can do as a society."

### AI summary (High error rate! Edit errors on video page)

Acknowledges the importance of workers in grocery stores, big box stores, gas stations, and pharmacies during the current crisis.
Points out the societal misconception that jobs with plastic name tags are perceived as inferior due to poor life decisions.
Stresses that these jobs are actually essential and integral to society regardless of character or decisions made.
Calls for maintaining class solidarity and recognizing the necessity of these roles even after the crisis ends.
Argues that individuals in these vital positions should have a decent standard of living, including financial security and health insurance.
Challenges the notion that these jobs can be done by anyone, citing the skill and importance they hold.
Urges society to use the current situation as a learning experience to bring about necessary changes.
Encourages supporting these workers by backing politicians who advocate for their well-being come election time.
Emphasizes that ensuring a decent standard of living for these workers should be the minimum effort society puts forth.

Actions:

for workforce advocates,
Support politicians advocating for decent living standards for low-wage workers (implied).
Recognize and appreciate the importance of workers in grocery stores, big box stores, gas stations, and pharmacies (implied).
</details>
<details>
<summary>
2020-03-20: Let's talk about a message to America.... (<a href="https://youtube.com/watch?v=lu_58ZMe364">watch</a> || <a href="/videos/2020/03/20/Lets_talk_about_a_message_to_America">transcript &amp; editable summary</a>)

President Trump missed an inspiring moment, but Americans can find strength in fear, avoid panic, and rely on their resilience during this crisis.

</summary>

"Fear in some ways is good. Panic is the enemy."
"We're not being asked to storm the beaches. We're being sent to go watch Netflix."
"We are the United States. We thrive on chaos."

### AI summary (High error rate! Edit errors on video page)

President Trump insulted a reporter who asked what he had to say to Americans who were afraid.
Instead of inspiring the American people, Trump insulted the reporter, calling the question sensationalist.
Fear is normal and can be a good motivator to take necessary precautions, but panic is the enemy.
American exceptionalism could be used positively during this crisis, given the country's resources and diversity.
The United States is being looked upon for leadership during this time, but it seems to be lacking.
Many tough media personalities are having breakdowns on Twitter, but they do not represent what America truly stands for.
People celebrate America for its working class and the ability to overcome hard times.
The current situation does not require drastic action like storming beaches but rather staying calm and doing our part.
Those providing essential services are conveying calm and helping everyone get through this crisis.
The United States historically thrives on chaos and can lead itself through these challenging times.

Actions:

for americans,
Remain calm and convey calm to others (exemplified)
Find strength in fear to take necessary precautions (exemplified)
Support those providing essential services (exemplified)
</details>
<details>
<summary>
2020-03-20: Let's talk about Senators, the economy, and learning from Elsa.... (<a href="https://youtube.com/watch?v=wJxM2XtGx9E">watch</a> || <a href="/videos/2020/03/20/Lets_talk_about_Senators_the_economy_and_learning_from_Elsa">transcript &amp; editable summary</a>)

Beau stresses the importance of providing necessary cash infusions like blankets to protect the economy and help those living paycheck to paycheck, urging senators to embrace cooperation in crisis situations.

</summary>

"You need to give them blankets. Those cash infusions that you are nickel and diming are those blankets."
"In survival situations, in crisis situations, everybody becomes a socialist."
"Give people what they need. Give them those blankets."

### AI summary (High error rate! Edit errors on video page)

Compares the current economic situation to an ice age in the movie Frozen, where essentials continue while everything else freezes.
Mentions that the stock market is falling rapidly, but it doesn't represent the real economy at the ground level.
Urges senators to provide cash infusions like blankets to help people who live paycheck to paycheck.
Emphasizes that cash infusions are necessary to protect sectors and ensure people can pay their bills.
Points out that without financial support, the economy will suffer, as people won't have money to spend post-crisis.
Stresses the importance of giving people what they need and not nickel and diming in times of crisis.
Calls for senators to understand that in survival situations, everyone becomes a socialist and cooperation is key.

Actions:

for senators, policymakers,
Provide immediate cash infusions to those living paycheck to paycheck (exemplified)
Ensure sectors are protected by financial support (exemplified)
Prioritize cooperation over competition in crisis situations (exemplified)
</details>
<details>
<summary>
2020-03-18: Let's talk about the numbers and feasibility of a rumor.... (<a href="https://youtube.com/watch?v=N6WH7Tt-1Aw">watch</a> || <a href="/videos/2020/03/18/Lets_talk_about_the_numbers_and_feasibility_of_a_rumor">transcript &amp; editable summary</a>)

Beau debunks rumors of imminent martial law in the U.S., explaining why it's practically impossible and stressing the importance of following guidelines to combat the current situation.

</summary>

"Wash your hands. Don't touch your face. Stay at home. Practice social distancing."
"Please stop spreading that. You can get somebody hurt."
"I cannot envision a scenario in which true martial law is declared in the United States."

### AI summary (High error rate! Edit errors on video page)

Squashing a rumor gaining ground by acting and discussing feasibility.
Addressing the idea of martial law being declared coast to coast in the U.S.
Explaining the specifics of martial law versus states of emergency.
Breaking down the numbers needed for martial law and why it's practically impossible.
Mentioning the size difference between Iraq and the U.S. in terms of troops required.
Detailing the mobilization process for troops in the U.S. to attempt martial law.
Clarifying that even in extreme scenarios, true martial law is unlikely in the U.S.
Emphasizing the importance of following guidelines and not resisting authorities.
Urging people to practice basic hygiene and social distancing measures.
Encouraging trust in medical professionals' advice to combat the current situation.

Actions:

for general public,
Wash your hands. Don't touch your face. Stay at home. Practice social distancing. (implied)
Follow the advice of medical professionals. (implied)
</details>
<details>
<summary>
2020-03-18: Let's talk about the Earn It Act.... (<a href="https://youtube.com/watch?v=7nYo7tdRAJ4">watch</a> || <a href="/videos/2020/03/18/Lets_talk_about_the_Earn_It_Act">transcript &amp; editable summary</a>)

Beau warns about a bill allowing government surveillance through private companies, urging opposition and questioning societal readiness for extensive monitoring.

</summary>

"How far are you willing to take it?"
"Do you want the agencies that have bungled this current crisis so much to have access to every single message that you send?"
"Are you ready for Big Brother?"

### AI summary (High error rate! Edit errors on video page)

Draws a parallel between the lack of privacy in the book/movie 1984 and current privacy concerns.
Mentions a bill that allows the government to scan every message on the internet through private companies.
Explains that the bill doesn't mention encryption but mandates scanning every message, leading to the death of encryption.
Describes a committee headed by Attorney General Barr that will set best practices for online platforms, including scanning every message.
Points out the bill's packaging as a measure to protect exploited kids, similar to justifying surveillance in every building.
Raises questions about the extent of government surveillance and the justification for scanning every message.
Warns that the bill, known as the EARN IT Act, has bipartisan support and could gain traction if not opposed.
Encourages opposition to the bill and shares a resource from the Electronic Frontier Foundation.
Reminds of the government's tendency to exploit crises for their benefit.
Considers the implications of granting agencies access to every message sent, questioning if society is ready for such intrusion.

Actions:

for internet users,
Oppose the EARN IT Act to prevent extensive government surveillance (implied).
</details>
<details>
<summary>
2020-03-18: Let's talk about comparisons, pep talks, and disseminating information.... (<a href="https://youtube.com/watch?v=6BQzqkYsJDc">watch</a> || <a href="/videos/2020/03/18/Lets_talk_about_comparisons_pep_talks_and_disseminating_information">transcript &amp; editable summary</a>)

Beau shares a story from his protective detail work, stressing the importance of timely information dissemination and criticizing comparisons that downplay threats during challenging times, urging meme creators to focus on spreading positive messages instead.

</summary>

"Almost positive, bad guys, right here."
"Meme lords, please use your powers for good instead of evil."
"Please be part of the solution here."

### AI summary (High error rate! Edit errors on video page)

Shares a story from the early 2000s when running a protective detail for a high-risk client.
Describes how a counter surveillance team alerted him about suspicious individuals conducting surveillance.
Gives his team a pep talk after receiving photos of the suspicious individuals.
Emphasizes the importance of disseminating information at the appropriate time.
Criticizes the use of comparisons, like the likelihood of dying in a car accident versus gunfire, to convey calm during tense situations.
Stresses that downplaying threats through comparisons fosters ignorance and does not help.
Encourages meme creators to focus on spreading messages about handwashing and social distancing.
Urges people to listen to medical professionals for guidance on staying safe during challenging times.
Advises on practical measures like washing hands, avoiding face-touching, practicing social distancing, and staying home when possible.
Expresses confidence in the resilience of the American people to overcome challenges efficiently.

Actions:

for meme creators, general public,
Wash hands for 20 seconds with soap and hot water (suggested)
Avoid touching your face (suggested)
Practice social distancing (suggested)
Stay home if possible (suggested)
</details>
<details>
<summary>
2020-03-18: Let's talk about an announcement and staycationing in style.... (<a href="https://youtube.com/watch?v=3dbClP5awnI">watch</a> || <a href="/videos/2020/03/18/Lets_talk_about_an_announcement_and_staycationing_in_style">transcript &amp; editable summary</a>)

Beau plans to live stream answering questions on survival, staycationing, and community networking, advising on food planning, hygiene, activities, and staying calm during the 30-day staycation.

</summary>

"Hide your junk food."
"Stay calm. It will be alright."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau plans to do a live stream to answer questions about survival, staycationing, and community networking.
Suggestions on planning 30 days worth of food include physically laying it out and eating perishables first.
Beau advises to hide comfort food and not consume it all in the first week.
In case of a toilet paper shortage, alternatives like napkins can be used, but they should not be flushed.
Setting up a solar shower for warm water outside can be helpful for hygiene during staycation.
Beau recommends staying physically active during the 30 days to keep immunities up.
Alcohol consumption is discouraged as it lowers the immune system.
Suggestions for activities during the staycation include working on pending tasks, resume, business plan, and community network development.
Beau encourages staying calm and assures that everything will be alright during the staycation period.

Actions:

for staycationers,
Subscribe, ring the bell, and get notifications for Beau's live stream (suggested).
Physically plan out 30 days worth of food and stick to the plan (implied).
Stay physically active during the staycation period (implied).
</details>
<details>
<summary>
2020-03-17: Let's talk about the numbers and mixed messaging.... (<a href="https://youtube.com/watch?v=fEmylMus014">watch</a> || <a href="/videos/2020/03/17/Lets_talk_about_the_numbers_and_mixed_messaging">transcript &amp; editable summary</a>)

Beau stresses the importance of following medical advice during the pandemic, urging people to prioritize their lives over economic concerns and political influence, and be heroes by staying home.

</summary>

"Follow the advice of the medical professionals, not the advice of your favorite politician on Twitter."
"You can be a hero right now by staying home."
"It's your life."

### AI summary (High error rate! Edit errors on video page)

Encountered people ignoring medical advice due to low perceived risk.
Explains the concept of a 0.2% chance to convey the real danger.
Compares the risk to a Walmart analogy with a 500th person scenario.
Urges following medical professionals' advice over politicians' on social media.
Acknowledges differing opinions among medical professionals but advises following the strictest guidelines.
Addresses accusations of mixed messaging regarding staying home.
Reveals personal exposure risk due to wife being a medical professional.
Describes going out to serve a greater cause while practicing strict self-isolation.
Emphasizes the importance of staying home if there's no urgent need to go out.
Urges not to risk life over economic concerns and not to use his actions as an excuse to violate self-isolation.
Encourages listeners to determine their own level of involvement and prioritize safety.
Reminds that the numbers represent lives, not just statistics on an exam.
Urges trusting healthcare professionals over politicians during this crisis.
Calls for heroism in staying home and following health guidelines.

Actions:

for general public,
Stay home, wash hands, practice self-distancing, and follow medical professionals' advice (exemplified).
Determine your level of involvement and prioritize safety (exemplified).
</details>
<details>
<summary>
2020-03-17: Let's talk about helicopter money, Yang, and a mistake.... (<a href="https://youtube.com/watch?v=3B0sZUfeaZs">watch</a> || <a href="/videos/2020/03/17/Lets_talk_about_helicopter_money_Yang_and_a_mistake">transcript &amp; editable summary</a>)

Beau believes giving people money to stay home and not lose their homes is more about people than the economy, criticizing the opposition for not focusing on controlling what's controllable.

</summary>

"Money in the hands of the average person will not only help stimulate the economy, which I literally do not care about."
"If we're going to have an overreaching government that is capable of doing anything and has this power, this seems like a pretty good moment to use it."
"Keeping people in their homes is the best way to do that."

### AI summary (High error rate! Edit errors on video page)

The administration is considering the idea of giving everyone a couple thousand dollars to stimulate the economy, similar to Andrew Yang's plan.
Republicans support this to boost the economy, while some Democrats oppose it, suspecting a hidden agenda to boost polls.
Beau believes that money in people's hands will help them stay home, not lose their homes, and focus on flattening the curve.
He criticizes the Democratic Party for opposing this move, stating that it's more about people than the economy.
Beau prefers money to go to working individuals rather than Wall Street to keep people in their homes and stimulate the economy simultaneously.
He acknowledges the danger outside and sees this as a controllable situation where the government can step in to help.
Beau expresses concern that making the average American suffer just to show Trump's shortcomings is not necessary and suggests a different campaign strategy.
Keeping people in their homes is emphasized as the best way to control the situation and focus on what's controllable.

Actions:

for policy makers, political activists,
Distribute aid to keep people in their homes (exemplified)
Focus on controlling controllable elements (suggested)
</details>
<details>
<summary>
2020-03-16: Let's talk with Jen Perelman about Florida and 2020.... (<a href="https://youtube.com/watch?v=Dh_kMhDhls8">watch</a> || <a href="/videos/2020/03/16/Lets_talk_with_Jen_Perelman_about_Florida_and_2020">transcript &amp; editable summary</a>)

Jen Perlman, priming Debbie Wasserman Schultz in Florida, advocates for justice, single-payer healthcare, community service, and grassroots change against corporate influence.

</summary>

"It's about justice. It's about social justice, economic justice, and environmental justice."
"I have no career motives. I have no career ambition in this whatsoever. This is something that I want to do as a service."
"I don't see them as protecting and serving. I don't see the people that they're going after as being necessarily dangerous to society."
"I just have this crazy idea that people shouldn't have to work three jobs and drive an Uber to be able to live."
"Find the need, there's no shortage. Even if it's going to the library and just doing a little bit of research and saying like, what's going on in this area and how can I help?"

### AI summary (High error rate! Edit errors on video page)

Jen Perlman is priming Debbie Wasserman Schultz in Florida's 23rd congressional district, aiming to remove a long-standing incumbent.
Perlman criticizes Wasserman Schultz for prioritizing corporate interests over community concerns like environmental protection.
Perlman, a second-generation Floridian with a legal background, is focused on justice and serving the people, not a political career.
She advocates for removing profit motives from industries like healthcare and education, supporting single-payer healthcare and affordable education for all.
Perlman opposes bans on assault weapons, citing their inefficacy and the need to address root causes of gun violence.
She argues against the privatization of public services like education through vouchers, advocating for quality options for all.
Perlman denounces ICE as unnecessary and criticizes its treatment of immigrants, advocating for a humane approach to immigration.
She addresses wealth inequality and lack of affordable housing in Broward County, calling for a living wage and healthcare as human rights.
Perlman encourages community involvement and service as a way to enact change, urging people to connect with local organizations and address pressing needs.
She stresses the importance of grassroots support, small donations, and community engagement in her campaign against corporate influence.

Actions:

for community members, activists,
Reach out to local organizations, attend meetings, and offer help in areas of need (exemplified)
Organize clothing drives, toiletry collections, or beach cleanups to support local communities (exemplified)
Support Jen Perlman's campaign by donating to gen2020.com and spreading the word (exemplified)
</details>
<details>
<summary>
2020-03-16: Let's talk about situational awareness and a whole white baby.... (<a href="https://youtube.com/watch?v=KmDbvesjrks">watch</a> || <a href="/videos/2020/03/16/Lets_talk_about_situational_awareness_and_a_whole_white_baby">transcript &amp; editable summary</a>)

Beau explains the importance of developing situational awareness through real-life examples and encourages practicing deductive reasoning for better decision-making in various situations.

</summary>

"Situational awareness is the constant and immediate application of deductive reasoning."
"It's a skill that if you plan to be out and about or you plan to be, you don't plan to be in a crisis situation."
"You do it all the time whether or not you call it that or not."

### AI summary (High error rate! Edit errors on video page)

Defines situational awareness as the perception of environmental elements and events with respect to time or space, comprehension of their meaning, and projection of their future status.
Describes situational awareness as the constant and immediate application of deductive reasoning, allowing one to predict outcomes.
Shares a personal story where he applied situational awareness at a gas station late at night, evaluating a stranger's request for help.
Illustrates the process of deductive reasoning in real-time, showing how he assessed the situation and made decisions based on environmental cues.
Encourages conscious practice of situational awareness in daily life to become better at it and apply it effectively in crisis situations.
Mentions a video where individuals demonstrate situational awareness by rescuing a child found walking on a highway.
Emphasizes the importance of developing situational awareness as a valuable skill for navigating daily life and stressful situations effectively.

Actions:

for individuals, community members, safety advocates,
Practice conscious situational awareness in daily activities to hone the skill and improve decision-making (suggested).
Encourage others to develop situational awareness through real-life scenarios and practice deductive reasoning for better crisis management (implied).
</details>
<details>
<summary>
2020-03-16: Let's talk about a new library of censored material.... (<a href="https://youtube.com/watch?v=tumjxowQ_qM">watch</a> || <a href="/videos/2020/03/16/Lets_talk_about_a_new_library_of_censored_material">transcript &amp; editable summary</a>)

A Minecraft library defies censorship, ensuring information reaches those who need it most, showing that information always finds a way.

</summary>

"Information will always find a way to get to where it needs to be."
"Who's going to see this the most? People."
"There are countries that have much more severe problems in that regard."
"It's not going to work. It has never worked."
"There will always be somebody willing to get that information out in some fashion."

### AI summary (High error rate! Edit errors on video page)

Introduces a new library in Minecraft that holds information not meant to be shared, created by Reporters Without Borders and BlockWorks.
Describes Minecraft as a video game similar to digital Legos.
Mentions that the library contains articles and targets individuals in countries with restricted access to independent journalism.
Talks about the striking decor inside the libraries, like a cemetery in the Mexico section with tombstones of killed indie journalists.
Provides the server IP visit.uncensoredlibrary.com for multiplayer access and encourages downloading the archive for hosting elsewhere.
Emphasizes the significance of the project in ensuring information reaches its intended audience.
Stresses the importance of access to accurate information during the formation of ideas and values.
Reminds viewers of the various ongoing fights for information freedom worldwide.
Asserts that censorship is ultimately futile, as information always finds a way to be shared.
Encourages reflection on the broader implications beyond current headline news.

Actions:

for gamers, activists, information seekers.,
Visit visit.uncensoredlibrary.com to access the Minecraft library (suggested).
Download the entire archive to host it in another location (suggested).
</details>
<details>
<summary>
2020-03-15: Let's talk about a message to nurses and one from them.... (<a href="https://youtube.com/watch?v=oJCC58GYgdk">watch</a> || <a href="/videos/2020/03/15/Lets_talk_about_a_message_to_nurses_and_one_from_them">transcript &amp; editable summary</a>)

Beau addresses the stress and challenges faced by medical professionals on the front lines, urging understanding, empathy, and adherence to guidelines while reminding all to practice hygiene and social distancing.

</summary>

"Emergency being the key word."
"Cut them some slack."
"Their main patient is all of society."

### AI summary (High error rate! Edit errors on video page)

Addresses a message to medical professionals and from them, focusing on the stress and challenges faced by those on the front lines.
Shares a story about prior service military members in a medical facility and how their training intensity and expertise are being called upon now.
Describes the military's intense training scenarios and how it differs from civilian training due to profit motivations.
Talks about the high level of confidence and posture that military training instills in individuals.
Explains how prior service military members are falling back into their training mannerisms and attitudes during the current crisis.
Emphasizes the importance of recognizing genuine emergencies in the ER and understanding the triage process.
Advises against overwhelming medical infrastructure by visiting the ER unnecessarily or bringing multiple people.
Urges visitors to follow guidelines, limit movements within the facility, and not challenge medical professionals' expertise.
Acknowledges the stress and uncertainty faced by medical professionals on the front line and urges understanding and empathy.
Concludes with a reminder to practice hygiene, social distancing, and follow medical advice.

Actions:

for medical community, general public,
Follow guidelines, practice social distancing, and adhere to medical advice (suggested)
Limit visits to the ER to genuine emergencies and avoid overwhelming medical infrastructure (suggested)
Show understanding and empathy towards stressed medical professionals (implied)
</details>
<details>
<summary>
2020-03-14: Let's talk with Mike Broihier about taking on Mitch McConnell.... (<a href="https://youtube.com/watch?v=oxEihoNBipA">watch</a> || <a href="/videos/2020/03/14/Lets_talk_with_Mike_Broihier_about_taking_on_Mitch_McConnell">transcript &amp; editable summary</a>)

Beau introduces Mike Breuer, a candidate running against Mitch McConnell in Kentucky, who advocates for economic and social justice based on equality under the law and common ownership of resources while critiquing McConnell's focus on power and wealth accumulation.

</summary>

"Beat him with the unrelenting truth."
"Don't be a dick."
"He is not good for the Republic. And he needs to go."

### AI summary (High error rate! Edit errors on video page)

Introduction of special guest Mike Breuer, who is running against Mitch McConnell in Kentucky.
Mike Breuer's background as a retired Marine Lieutenant Colonel, educator, substitute teacher, and farmer.
Mike Breuer's platform focusing on economic and social justice for all based on equality under the law and common ownership of resources.
Critique of McConnell as someone focused on accumulating power and wealth rather than the well-being of Kentuckians.
Mike Breuer's emphasis on economic justice for Appalachian coal-producing states like Kentucky.
The interconnectedness of economic and social justice and the impact on Kentucky residents.
Mike Breuer's approach to communicating his message honestly and respectfully, drawing from his experience as a reporter and editor.
Criticism of the prison system in Kentucky and the financial burden it places on taxpayers.
Impact of budget cuts on public libraries and schools in Kentucky, especially during times like the coronavirus outbreak.
Strategy to beat McConnell with truth and by pointing out his lack of significant contributions to Kentucky.
Mike Breuer's stance on controversial issues like women's reproductive rights and universal background checks.
Critique of McConnell's use of public funds for projects that benefit him politically rather than the people of Kentucky.
Challenges faced by Kentucky farmers due to misguided policies like hemp legalization.
Mike Breuer's progressive stance on gay rights and the importance of honesty and fairness in politics.
Call for individuals to listen, understand, and respect others in order to make the world a better place.

Actions:

for voters,
Support Mike Breuer's campaign by donating at mikeforky.com (implied)
Spread the message on social media to increase visibility and support for Mike Breuer (implied)
Volunteer, donate, or come forward to help the campaign if in Kentucky (implied)
</details>
<details>
<summary>
2020-03-13: Let's talk about what you can do from home to help.... (<a href="https://youtube.com/watch?v=CcJlvW2o3ms">watch</a> || <a href="/videos/2020/03/13/Lets_talk_about_what_you_can_do_from_home_to_help">transcript &amp; editable summary</a>)

In times of crisis, everyone can contribute, even from home, through projects like "folding at home" to aid critical research efforts.

</summary>

"Everybody can do something in any situation."
"There will always be some way for everybody to contribute."
"The power of the internet is disseminating information."

### AI summary (High error rate! Edit errors on video page)

Sharing a message from someone feeling helpless about not being able to volunteer due to being at risk and staying at home.
Mention of a project called "folding at home" run by Stanford University that utilizes computing power to research folding proteins.
Describing how the distributed network of computers contributes processing power to aid in research.
Emphasizing the importance of everyone finding a way to contribute, even from home.
Encouraging individuals to seek out ways to help in any situation.
Hinting at future short videos to disseminate vital information during the current circumstances.

Actions:

for online community members,
Set up "folding at home" project on your computer (exemplified)
Use your computing power to aid research efforts (exemplified)
</details>
<details>
<summary>
2020-03-13: Let's talk about plans, experts, uncertainty, and the economy.... (<a href="https://youtube.com/watch?v=--WNarhBjGg">watch</a> || <a href="/videos/2020/03/13/Lets_talk_about_plans_experts_uncertainty_and_the_economy">transcript &amp; editable summary</a>)

Beau addresses the lack of a comprehensive plan to deal with the crisis, criticizes the focus on the economy over people's well-being, and advocates for utilizing existing resources effectively.

</summary>

"The longer this goes on, the worse the economy is going to get."
"We have the plans. We don't need the president."
"Wash your hands, social distancing, if you're at risk, stay home."
"The strategies and tactics used to contain it, they exist, and they're going to be the same."
"He's failing at protecting that."

### AI summary (High error rate! Edit errors on video page)

Addressing the current state of news footage resembling a post-apocalyptic film montage.
Expressing disappointment in the lack of a plan to deal with the situation.
Mentioning the concept of the "deep state" and its role in providing continuity of government.
Criticizing the focus on the economy over the well-being of those at risk.
Advocating for utilizing existing plans and experts instead of creating new ones.
Suggesting that governors or National Guard could step in to help citizens if the administration fails to act.
Emphasizing the importance of handwashing, social distancing, and staying at home for those at risk.
Noting the reluctance of the federal government to share information to avoid causing worry about the economy.
Pointing out the economic impact of uncertainty and the failure to protect what should be prioritized during the crisis.
Encouraging proactive measures to address the situation effectively.

Actions:

for citizens, governors,
Reach out to governors, National Guard, and Department of Defense to ensure preparedness (suggested)
Practice handwashing, social distancing, and stay at home if at risk (implied)
</details>
<details>
<summary>
2020-03-13: Let's talk about infrastructure and being proactive.... (<a href="https://youtube.com/watch?v=hxOUDi8CKwU">watch</a> || <a href="/videos/2020/03/13/Lets_talk_about_infrastructure_and_being_proactive">transcript &amp; editable summary</a>)

Beau stresses the need to learn from past mistakes, update infrastructure, and prepare for future crises to avoid catastrophic scenarios.

</summary>

"We have to update it. We have to upgrade it."
"But there are much, much worse scenarios out there."
"This needs to be done."
"We need to update our infrastructure."
"Wash your hands."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of learning from past mistakes and making suggestions for the future based on those lessons.
Notes the common scenario where security consultants are only called in after a problem occurs, stressing the need for post-event analysis and prevention strategies.
Points out the deficiencies in the current infrastructure, particularly in medical and internet sectors.
Emphasizes the urgent need for more ventilators and convertible ICU beds in the healthcare system.
Raises concerns about the lack of access to healthcare, especially for the millions of uninsured people in the country.
Identifies childcare, food security, and access to healthcare as critical issues that need immediate attention.
Urges for updating infrastructure to address current challenges and future crises.
Suggests that updating infrastructure could also aid in economic recovery and help combat climate change.
Calls for a proactive approach in tackling infrastructure issues and stresses the importance of taking action.
Raises awareness about the potential consequences of not addressing infrastructure weaknesses, especially in the face of more severe scenarios.

Actions:

for community members, policymakers, activists,
Update infrastructure to address current deficiencies and prepare for future crises (suggested)
Advocate for increased healthcare access for all individuals, including the uninsured (suggested)
Support initiatives for better childcare services and food security in communities (suggested)
</details>
<details>
<summary>
2020-03-13: Let's talk about cleaning up the mess together.... (<a href="https://youtube.com/watch?v=uLXOWT5t3Y0">watch</a> || <a href="/videos/2020/03/13/Lets_talk_about_cleaning_up_the_mess_together">transcript &amp; editable summary</a>)

Beau urges action over waiting for federal leadership, prioritizing people, unity, and cooperation to combat the global crisis effectively.

</summary>

"Everybody has a part to play."
"Put people over the economy."
"It's humanity against this thing."
"The sooner we start to act, the more manageable this becomes."
"It's just us. And we're going to have to work together."

### AI summary (High error rate! Edit errors on video page)

Urges taking action instead of waiting for federal leadership.
Encourages state and local governments to protect their people.
Emphasizes the importance of individuals educating themselves and taking responsibility.
Calls for putting people's well-being over the economy.
Criticizes scapegoating and division, advocating for cooperation and unity.
Reminds everyone to pitch in and do what they can.
Stresses the importance of demographic information in determining involvement levels.
Mentions the significance of health care and leave as national security issues.
Warns against panic and the need to focus on protecting vulnerable demographics.
Calls for collective mobilization in the absence of federal leadership.

Actions:

for state and local governments,
Protect the people within your jurisdiction (exemplified)
Educate yourself and learn from successful approaches (exemplified)
Put people's well-being over the economy (exemplified)
Avoid scapegoating and focus on cooperation and unity (exemplified)
Volunteer or help those who need support (exemplified)
Stay home if you're at high risk (exemplified)
Focus on protecting vulnerable demographics (exemplified)
Mobilize at a community level in the absence of federal leadership (exemplified)
</details>
<details>
<summary>
2020-03-12: Let's talk with Shahid Buttar about 2020.... (<a href="https://youtube.com/watch?v=RjTlMidhZrI">watch</a> || <a href="/videos/2020/03/12/Lets_talk_with_Shahid_Buttar_about_2020">transcript &amp; editable summary</a>)

Beau interviews Shahid Buttar, a leftist challenger taking on Nancy Pelosi in the primary, advocating for progressive policies and grassroots activism to reclaim political power.

</summary>

"I'm just a schmo with a pen, and I don't have a family legacy. I'm an immigrant." - Shahid Buttar
"I wanna see us commit to rights to housing and healthcare and food and education." - Shahid Buttar
"I fear that we could continue to be preyed upon by institutions that have the opportunity to turn a deaf ear to us." - Shahid Buttar
"If all else fails and tyranny emerges, we have the right to resist it." - Shahid Buttar
"Step one is proactively curate your news sources. Step two is meet your neighbors. Step three is do the thing, agitate." - Shahid Buttar

### AI summary (High error rate! Edit errors on video page)

Introduces Shahid Buttar as he takes on Nancy Pelosi in the primary, aiming to provide a leftist perspective challenging the status quo.
Shahid Buttar explains his motivation for entering politics, driven by a desire to address political frustrations and misrepresentations in Washington.
He outlines his leftist views, advocating for radical wealth redistribution, nationalizing certain industries, and focusing on civil liberties and voting rights.
As an immigrant, Buttar shares his commitment to civil rights advocacy and opposition to bipartisan fascism, leading to his decision to challenge Pelosi.
Buttar details his strategy to defeat Pelosi in the upcoming election, focusing on informing San Francisco residents about Pelosi's policies and running a volunteer-driven campaign.
The interview delves into Buttar's support for progressive policies like Medicare for All and the Green New Deal, linking these initiatives to social justice and climate resilience.
He also addresses immigration issues, advocating for immigrant rights and criticizing the current administration's policies.
Buttar offers a unique perspective on the Second Amendment, viewing it as a right to resist tyranny and suggesting that in the modern context, resistance may manifest through information access.
Funding his campaign through grassroots support from thousands of Americans, Buttar expresses gratitude for the backing he has received and his commitment to challenging corporate influence in politics.
He concludes by urging individuals to curate their news sources, connect with their communities, and work towards collective action, including the possibility of a general strike as a means of reclaiming sovereignty.

Actions:

for progressive activists, community organizers,
Connect with local organizers and neighbors to build community support for progressive causes (exemplified)
Curate news sources for accurate information and share reliable news with others (exemplified)
Organize events like letter-writing campaigns, protests, or community meetings to address political issues (implied)
</details>
<details>
<summary>
2020-03-11: Let's talk about what Bernie supporters can learn from boxing.... (<a href="https://youtube.com/watch?v=USYADtr838A">watch</a> || <a href="/videos/2020/03/11/Lets_talk_about_what_Bernie_supporters_can_learn_from_boxing">transcript &amp; editable summary</a>)

Beau advocates for punching up, warns against personifying movements, and interviews candidates challenging big political names, stressing that systemic change is about ideas, not individuals.

</summary>

"It's never the people below you who are the source of your problems."
"If you become discouraged by that, you're personifying the movement."
"This fight is not about one person. If you want systemic change in the United States, it's not about Bernie."
"I don't endorse candidates. But I do love to watch a good fight."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Advocates for the concept of never kicking down and always punching up.
Points out the danger of personifying a movement and the discouragement that comes when the personified figure loses.
Talks about Bernie's diminishing chances of being the nominee but stresses that the battle is not just about one person.
Shares that he has been interviewing candidates challenging big political names like Nancy Pelosi and Mitch McConnell.
Mentions that these interviews will be released over the next few days and are worth watching for insights into the future of politics in the United States.
Acknowledges technical issues in the interviews but encourages watching them for the content.
Emphasizes that the fight for systemic change is about the ideas, not just one person.
Expresses his interest in watching a good political fight without endorsing any specific candidates.

Actions:

for political enthusiasts,
Watch the upcoming interviews with candidates challenging big political names (suggested)
Stay informed about progressive ideas and candidates in politics (implied)
</details>
<details>
<summary>
2020-03-11: Let's talk about AOC the bartender... (<a href="https://youtube.com/watch?v=4qI1OpY0zDo">watch</a> || <a href="/videos/2020/03/11/Lets_talk_about_AOC_the_bartender">transcript &amp; editable summary</a>)

Beau criticizes the backlash against AOC, advocates for diverse representation, and questions the focus on professional politicians over working-class backgrounds.

</summary>

"Why should she clarify? Everybody up there likes to pretend that they're a man of the working people."
"We need more bartenders. We need more fast food workers. We need more nurses. We need more teachers. We need more welders. We need more ranchers and less professional politicians."
"The House of Representatives might, should be, representative of us."

### AI summary (High error rate! Edit errors on video page)

Criticizes the backlash against AOC for either misspeaking or using internet speak.
Questions the emphasis on AOC's past as a bartender, suggesting she has more qualifications.
Points out that elected officials often distance themselves from working-class backgrounds.
Argues that individuals like bartenders have a better understanding of community issues.
Advocates for more diverse representation in government, including bartenders, fast food workers, nurses, teachers, welders, and ranchers.

Actions:

for voters, political activists,
Elect diverse representatives who understand community interests (implied)
Advocate for more representation from working-class backgrounds (implied)
</details>
<details>
<summary>
2020-03-10: Let's talk about the criticism of Trump's response and what we can learn.... (<a href="https://youtube.com/watch?v=v5P3hpRB40o">watch</a> || <a href="/videos/2020/03/10/Lets_talk_about_the_criticism_of_Trump_s_response_and_what_we_can_learn">transcript &amp; editable summary</a>)

Beau criticizes Trump's response, compares it to "Jaws," and stresses the urgency of taking action to mitigate climate change before it escalates.

</summary>

"What it's not is surprising."
"We have to take action now to mitigate."
"Climate change is [a threat to the species]."
"It's going to get bad, a lot worse than this."
"We're running out of time."

### AI summary (High error rate! Edit errors on video page)

Criticizes Trump's response to a situation, calling it less than ideal and worthy of criticism.
Compares the response to the movie "Jaws," with scientists urging action while the establishment prioritizes immediate concerns like tourism and economy.
Points out that the government often ignores warnings from experts and focuses on short-term gains rather than mitigating harm.
Emphasizes the importance of taking action now to mitigate upcoming challenges rather than waiting until it's too late.
Notes that the current situation is unnerving and can be disruptive and tragic, but shouldn't be surprising given past patterns.
Criticizes elected officials, including Trump, for not taking necessary actions to address crises and for playing down serious issues like climate change.
Warns about the severity of climate change, stating it poses a greater threat than the current situation and will lead to significant disruptions and climate refugees if not addressed promptly.
Stresses the urgency of mitigating climate change before it escalates further.

Actions:

for aware citizens,
Take immediate action to mitigate climate change (implied)
Advocate for strong measures to address climate change (implied)
</details>
<details>
<summary>
2020-03-10: Let's talk about Harvard and an old guy.... (<a href="https://youtube.com/watch?v=t4aptbMYlbc">watch</a> || <a href="/videos/2020/03/10/Lets_talk_about_Harvard_and_an_old_guy">transcript &amp; editable summary</a>)

Beau warns against trusting deceptive administrations like Trump University over credible institutions like Harvard during uncertain times.

</summary>

"Imagine spending decades of your life being prepared for situations like this and then dying because you listened to a con man."
"It's not about keeping calm for them. It's not about conveying calm. It's about conning people."
"So please for a moment, most students at Harvard are not in an at-risk demographic. They're in that 0.2% range."
"You can trust Harvard or you can trust Trump University."
"Leadership from a respectable institution."

### AI summary (High error rate! Edit errors on video page)

Introduces the topics of probability, pedigree, universities, and trust.
Shares a personal story about a 69-year-old friend who trusted misleading information from the administration.
Expresses frustration at his friend's trust in the administration's denial of the seriousness of current events.
Questions the wisdom of trusting an administration known for dishonesty with one's life.
Contrasts the credibility of institutions like Harvard versus the administration in handling the situation.
Emphasizes the importance of considering the pedigree and honesty of the sources of information.
Advocates for listening to institutions like Harvard that prioritize caution and transparency.
Encourages viewers to trust reputable institutions over those with a history of deception.

Actions:

for public, community members,
Trust reputable institutions like Harvard for accurate information (implied).
Prioritize caution and transparency in handling situations (implied).
</details>
<details>
<summary>
2020-03-09: Let's talk about what Humpty-Dumpty and the economy.... (<a href="https://youtube.com/watch?v=x3ES6ZZftb8">watch</a> || <a href="/videos/2020/03/09/Lets_talk_about_what_Humpty-Dumpty_and_the_economy">transcript &amp; editable summary</a>)

Beau breaks down the economy using Humpty Dumpty as a metaphor, explaining how historical trends and current policies are shaping a potentially tumultuous future.

</summary>

"Trump didn't cause this downturn. He didn't pop the bubble, but he sharpened the needle."
"Obama really didn't fix the economy, but he helped it. Trump really didn't destroy it, but he helped destroy it."
"Trump's great economy was just a continuation of the trend."

### AI summary (High error rate! Edit errors on video page)

Explains the historical perspective he uses to analyze the economy, discussing the significance of the inverted yield curve signaling trouble.
Notes that the entire U.S. treasury curve being under 1% is unprecedented and seeks out experts to understand its implications.
Compares the economy to Humpty Dumpty, referencing the Great Recession and Obama's role in the recovery process.
Attributes the economic growth during Trump's administration as a continuation of trends from before, not solely due to his policies.
Describes Trump's policies like tariffs and attacks on social safety nets as factors contributing to a potential downturn.
Clarifies that while a correction was inevitable, Trump exacerbated the situation, likening him to sharpening a needle that was already present.
Mentions the trade war and OPEC Plus's recent disagreements as events affecting the economy and potentially leading to a downturn.
Suggests that the future of the economy is uncertain, with outcomes ranging from a mild recession to a depression, depending on upcoming events.
Acknowledges his lack of expertise in economics but provides insights based on the information gathered.
Concludes by summarizing Obama's and Trump's impacts on the economy and anticipates a turbulent period ahead.

Actions:

for economic analysts, concerned citizens,
Monitor economic developments closely and stay informed (implied)
Prepare for potential economic challenges by managing finances prudently (implied)
</details>
<details>
<summary>
2020-03-09: Let's talk about healthcare and tone deaf comments.... (<a href="https://youtube.com/watch?v=m8LDRdUTYaI">watch</a> || <a href="/videos/2020/03/09/Lets_talk_about_healthcare_and_tone_deaf_comments">transcript &amp; editable summary</a>)

Government officials' tone-deaf comments reveal their disconnect from Americans' struggles with health insurance, showcasing their fear and panic amidst a healthcare crisis.

</summary>

"It might be something worthy of putting on the platform in some way."
"A healthy community is an economically viable community."
"It's given him time to think about man's mortality."
"It displays how far away our representatives are from us."
"Thoughts and prayers and all that. But we don't want to hear about it."

### AI summary (High error rate! Edit errors on video page)

Millions of Americans lack health insurance or are underinsured, with many unable to handle a $400 emergency.
Tone-deaf comments from government officials showcase their disconnect from the fears and struggles of average Americans.
President Trump's germaphobia leads to fears of journalists giving him something on Air Force One.
Representative Paul Gosser from Arizona expresses fear of mortality and a desire to go out gloriously in battle.
Despite having top-notch healthcare funded by taxpayers, government officials exhibit fear and panic.
Officials should convey calm but instead spread worry and unease.
The lack of empathy and action on healthcare crisis is evident, dismissing it as socialism.
Suggestions to incentivize raising wages or addressing healthcare within the market are also rejected.
The disregard for healthcare and economic concerns will have severe impacts on the working class and the economy.
A healthy community is vital for economic stability, something that should be a priority for policymakers.
Access to healthcare should be a basic right in a powerful country like the United States.

Actions:

for policymakers, advocates,
Advocate for policies that prioritize healthcare for all (implied).
Support initiatives to incentivize raising wages for workers (implied).
Raise awareness about the economic significance of a healthy community (implied).
</details>
<details>
<summary>
2020-03-09: Let's talk about daddies, dollars, and letting them eat cake.... (<a href="https://youtube.com/watch?v=8zLedM3kXvU">watch</a> || <a href="/videos/2020/03/09/Lets_talk_about_daddies_dollars_and_letting_them_eat_cake">transcript &amp; editable summary</a>)

Comparing government disconnect to Marie Antoinette, contrasting aristocracies, noting rising awareness of wealth behavior, and questioning corruption in political elites.

</summary>

"My daddy can beat up your daddy."
"Everybody wants a better life for their kids than they had."
"Is the subconscious realization that corruption is happening one of the reasons those terms are coming into the mainstream?"

### AI summary (High error rate! Edit errors on video page)

Comparing the United States to Marie Antoinette's "let them eat cake" moment, showcasing government disconnect.
Contrasting European aristocracy with American aristocracy and their perceptions of wealth and privilege.
Noting the awakening of the working class in the U.S. to the behavior of wealthy families and the rise of terms like oligarchy and plutocracy.
Donald Trump Jr. challenging Hunter Biden to a debate on whose father helped them the most, reflecting tone-deafness amidst societal issues.
Expressing the historical trend of ruling classes becoming more flagrant once their privilege is exposed.
Acknowledging that while parents helping their children succeed isn't inherently wrong, corruption and taxpayer exploitation are problematic.
Questioning whether political elites are engaging in corruption as they accumulate vast wealth post-office, prompting mainstream recognition of terms like kleptocracy.

Actions:

for working-class americans,
Question the actions and wealth accumulation of political elites (implied)
Stay informed and aware of corruption within political circles (implied)
</details>
<details>
<summary>
2020-03-08: Let's talk about how Castro came to power.... (<a href="https://youtube.com/watch?v=gkQSQLgZGhQ">watch</a> || <a href="/videos/2020/03/08/Lets_talk_about_how_Castro_came_to_power">transcript &amp; editable summary</a>)

Beau explains Castro's rise to power in Cuba, warns about deceptive anti-establishment rhetoric, and urges genuine transformation for America's future.

</summary>

"History doesn't repeat, but it rhymes."
"You need all those conditions. Sometimes other things can substitute for them."
"People only care about the pebble in their shoe."
"Anti-establishment rhetoric, it's not really anti-establishment."
"We just need to make sure that those who are pushing that, that are saying that they are going to make America great again, that they're really going to."

### AI summary (High error rate! Edit errors on video page)

Explains how once international events hit the headlines, the rising action is missed, leading to a lack of understanding of the origin story.
Describes Castro's rise to power in the 1950s in Cuba, portraying himself as a populist with military background.
Points out that Castro couldn't win an election, so he staged a coup to seize power.
After gaining power, Castro curtailed civil liberties, stifled dissent, and created a kleptocracy with his allies.
Economic stagnation, income inequality, and deals with criminals further fueled revolutionary sentiment.
Estimates the number of executions under Castro's regime to be between 10 to 15, disputing higher figures like 20,000.
Mentions that Castro overthrew Batista, who failed to respond to the growing revolutionary spirit.
Outlines the necessary conditions for revolution: unresponsive government, corruption, income inequality, and discomfort among the people.
Notes that the US has most conditions for revolution but has the potential for a peaceful transformation due to comfort and infrastructure.
Warns about the deceptive nature of anti-establishment rhetoric and the need to ensure genuine change rather than a repeat of corrupt practices.

Actions:

for history buffs,
Advocate for genuine change and reforms within the government (exemplified)
Educate others on the importance of understanding history and origin stories of events (suggested)
</details>
<details>
<summary>
2020-03-08: Let's talk about a message to Floridians.... (<a href="https://youtube.com/watch?v=vJskimaw4TY">watch</a> || <a href="/videos/2020/03/08/Lets_talk_about_a_message_to_Floridians">transcript &amp; editable summary</a>)

Beau urges Floridians to stay calm, support each other, and lead themselves in the face of COVID-19 uncertainty, rejecting panic buying and advocating for a sense of community resilience.

</summary>

"We cannot let fear run our lives."
"If we are not going to get leadership from the government, we have to lead ourselves."
"Panic is not going to help."
"Pull your stuff together. You're a Floridian."
"Stay calm. Use the same humor and sense of community that gets us through everything else that gets thrown at us."

### AI summary (High error rate! Edit errors on video page)

Beau expresses gratitude for the concern and well wishes he has received.
He mentions that his opinion on the issue has not changed, but he is taking some new precautions.
Despite being in a crowded place full of travelers the next day, Beau states he will not be altering his daily routine drastically.
He criticizes the response of the state leadership in Florida, mentioning that the lack of information is causing fear.
Beau points out the disappointing behavior of some Floridians, describing witnessing panic during his outing.
Floridians are reminded by Beau about the various natural dangers they face in the state and the need to approach things with dark humor and community spirit.
He encourages people to inform themselves, look at the numbers objectively, and follow the guidelines and precautions.
Beau suggests that those at higher risk should take extra precautions and that the community should support vulnerable individuals by helping them with tasks like shopping and errands.
In the absence of effective government leadership, Beau stresses the importance of individuals taking charge and not succumbing to panic.
He criticizes panic buying behavior, especially pointing out the irrationality of hoarding gas as if it were a hurricane situation.
Beau calls for Floridians to stay calm, maintain their sense of community, and use humor to navigate through challenging times.

Actions:

for floridian residents,
Support older or compromised individuals by doing their shopping and errands for them (suggested)
Maintain a sense of community and humor to navigate through challenges (exemplified)
</details>
<details>
<summary>
2020-03-07: Let's talk about what Bernie Sanders is and isn't.... (<a href="https://youtube.com/watch?v=zn_4-7CpgU0">watch</a> || <a href="/videos/2020/03/07/Lets_talk_about_what_Bernie_Sanders_is_and_isn_t">transcript &amp; editable summary</a>)

Bernie Sanders is a social democrat, not a communist, urging for a break from the two-party system to embrace diverse ideologies for systemic change.

</summary>

"We don't have a leftist anything in the United States. Doesn't exist."
"If we're going to get anywhere, we've got to break free of Republicans and Democrats and the policies that they espouse."
"There's no reason to be afraid of ideas."
"Y'all are pretty much commies, to be honest. Y'all just don't know it."
"Republicans and Democrats, they have had a very, very long run, and those policies have had a very long run in the United States."

### AI summary (High error rate! Edit errors on video page)

Criticizes the lack of nuance in discussing different ideologies and philosophies in the United States.
Explains that in the global perspective, ideologies like the Green Party and Libertarians are not extreme, but center-right.
Clarifies the misconception of labeling Bernie Sanders as a communist, pointing out that communism advocates for communal ownership of production means and abolition of class and state.
Differentiates democratic socialism from communism, stating that Sanders may not fully embrace democratic socialist policies based on his advocacy for taxing profits.
Describes social democracy as advocating economic interventions for social justice within a capitalist system, which is where Bernie Sanders lies.
Urges for a broader exploration of political ideologies beyond the traditional Democratic and Republican spectrum in the U.S.
Encourages breaking free from the constraints of the two-party system to foster deeper systemic change.
Advises exploring various ideologies to understand different perspectives, even if they may seem unfamiliar or challenging.
Challenges viewers, including right-wingers, to look into different ideologies to gain a better understanding.
Suggests that many people might identify with certain ideologies without fully understanding or acknowledging them.

Actions:

for politically aware individuals,
Research various political ideologies beyond the traditional spectrum (suggested)
Engage in open-minded exploration of different ideologies (implied)
Encourage others to break free from the constraints of the two-party system (exemplified)
</details>
<details>
<summary>
2020-03-06: Let's talk about rhetoric and Ken Buck the super warrior.... (<a href="https://youtube.com/watch?v=9LqgSGTlOWg">watch</a> || <a href="/videos/2020/03/06/Lets_talk_about_rhetoric_and_Ken_Buck_the_super_warrior">transcript &amp; editable summary</a>)

Beau criticizes Congressperson Ken Buck for resorting to threatening rhetoric and irresponsible weapon display instead of engaging in constructive debate, urging him to reconsider his campaign tactics.

</summary>

"Come and take it."
"Guns are not toys, sir."
"You're supposed to be respectable."
"You're supposed to be somebody who uses logic and persuasion, not intimidation."
"You need to rethink your campaign."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of rhetoric and its purpose, using Congressperson Ken Buck as an example.
Describes a legislator's role in debating, discussing, and convincing opposing parties.
Criticizes Ken Buck for releasing a video filled with rhetoric instead of engaging in constructive debate.
Points out the use of an AR rifle as a prop in the video and questions Buck's familiarity with handling it.
Raises concerns about the irresponsible display of weapons and the potential dangers it poses.
Calls out Buck for using threatening rhetoric that appeals only to the uninformed.
Emphasizes the importance of setting a positive example as a congressperson.
Condemns the use of guns as props and the dangerous implications it can have.
Challenges Buck's image as a tough guy and questions his commitment to responsible gun ownership.
Urges Buck to reconsider his campaign tactics and focus on logic and persuasion rather than intimidation.

Actions:

for politically engaged citizens,
Educate others on responsible gun ownership and the dangers of using weapons as props (implied)
Advocate for constructive debate and logical persuasion in political discourse (implied)
Support candidates who prioritize respectful and informed communication in politics (implied)
</details>
<details>
<summary>
2020-03-06: Let's talk about elephants painting.... (<a href="https://youtube.com/watch?v=z1FN1SkOEpw">watch</a> || <a href="/videos/2020/03/06/Lets_talk_about_elephants_painting">transcript &amp; editable summary</a>)

Beau addresses the dark reality behind elephants painting and advocates for awareness of the cruelty involved in their training, urging a shift towards more ethical interactions with these intelligent creatures.

</summary>

"Elephants are incredibly intelligent. They are very human-like."
"An elephant painting an elephant holding a flower or something like that is manufactured."
"Those cute paintings become a lot less cute after you see what the pajamas is and after you see what the animal probably endured to get there."

### AI summary (High error rate! Edit errors on video page)

Addressing the topic of animals and art in response to comments about elephants painting after discussing artificial intelligence creating art.
Describing elephants as incredibly intelligent creatures full of human-like emotion, making it easy to believe they can paint.
Explaining the common scenario in videos of elephants painting in Southeast Asia with human assistants helping them.
Revealing the traditional training method for elephants in Southeast Asia, known as Pajan, involving cruel practices like beating and using bull hooks.
Pointing out that the elephant's painting is a trained behavior, not a display of creativity.
Not delving into all the disturbing details of the training process involving cruelty and pain inflicted on the elephants.
Expressing the emotional impact on viewers, often leading to tears due to elephants' intelligence and human-like qualities.
Exploring reasons why this practice is not widely known, including tourism dynamics, ancient traditions, conservation funding, and secrecy.
Mentioning the debate around whether elephants could be trained to paint without using cruel methods.
Encouraging awareness of the dark reality behind the seemingly cute paintings of elephants, shedding light on their suffering.
Mentioning the emergence of more ethical tourism practices with camps that allow genuine interaction with elephants rescued from harmful environments.

Actions:

for animal lovers, tourists, conservationists,
Visit and support ethical elephant camps to interact with and learn about rescued elephants (suggested)
Raise awareness about the cruel training methods used on elephants for painting (exemplified)
</details>
<details>
<summary>
2020-03-06: Let's talk about a gift to teachers and this channel.... (<a href="https://youtube.com/watch?v=3DCwe4qih80">watch</a> || <a href="/videos/2020/03/06/Lets_talk_about_a_gift_to_teachers_and_this_channel">transcript &amp; editable summary</a>)

Beau recites historical events, urging viewers to understand the past to influence the future effectively.

</summary>

"While we did not start the fire, it will keep burning."
"If you want to be relevant 70 years later, you're going to have to understand what happened 70 years ago."

### AI summary (High error rate! Edit errors on video page)

Reciting a list of historical events and figures from Billy Joel's song "We Didn't Start the Fire."
Channel's purpose: to help students better understand history and current events through engaging content.
Collaboration with public school teachers to incorporate elements from the song into videos.
Emphasizing the importance of history in understanding current events, philosophy, and community activism.
Stating the necessity of comprehending past events to remain relevant and effective in the present.
Encouraging viewers to view various aspects of life through a historical lens.
Conveying that the fire of history, once ignited, will continue burning.
Stressing the significance of history in shaping our understanding of the world.
Linking past events to present-day relevance and participation in shaping the future.
Signifying that the channel serves as a platform for exploring history and its impact on contemporary issues.

Actions:

for history enthusiasts, educators, activists.,
Collaborate with educators to create engaging historical content (implied).
Engage in community activism informed by historical perspectives (implied).
</details>
<details>
<summary>
2020-03-05: Let's talk about what two movies can teach us about foreign policy.... (<a href="https://youtube.com/watch?v=5YD3Twx6PlU">watch</a> || <a href="/videos/2020/03/05/Lets_talk_about_what_two_movies_can_teach_us_about_foreign_policy">transcript &amp; editable summary</a>)

Foreign policy mirrors interpersonal relationships, with irreversible consequences necessitating careful withdrawals to prevent unnecessary bloodshed and immorality.

</summary>

"It's not good versus evil. That's not how it works."
"Hasty withdrawals cost lives and it's not combatants, it's innocents, it's civilians."
"It is immoral to leave these people twisting in the wind after we plunged them into it."

### AI summary (High error rate! Edit errors on video page)

Foreign policy is more like interpersonal relationships, impacting everyone involved in the room.
In Jurassic Park, Jeff Goldblum's character's actions led to unnecessary consequences.
Once a decision is made in foreign policy, there will be consequences that cannot be easily undone.
In the movie Signs, sometimes there is a way to slowly pull out without causing destruction.
Hasty withdrawals in foreign policy can cost innocent lives and have dire consequences.
There are ways to withdraw from situations like Afghanistan and Syria without abandoning those affected.
Training local groups and providing resources can help stabilize regions like Afghanistan and Syria.
Leaving behind resources like helicopters and surveillance drones can give indigenous forces a fighting chance.
It is immoral to leave people in conflict zones without support after involving them in the first place.
Moral decisions in foreign policy should not be solely based on governmental actions but on what is right.

Actions:

for foreign policy analysts,
Provide resources and training to local groups in conflict zones (suggested).
Leave behind resources like helicopters and surveillance drones for indigenous forces (implied).
Avoid hasty withdrawals that can cost innocent lives (implied).
</details>
<details>
<summary>
2020-03-05: Let's talk about how this artwork does not exist.... (<a href="https://youtube.com/watch?v=OW_A7yd3Kxc">watch</a> || <a href="/videos/2020/03/05/Lets_talk_about_how_this_artwork_does_not_exist">transcript &amp; editable summary</a>)

Beau contemplates the blurring boundaries between human and algorithmic creativity, posing questions on the future where AI art may challenge human uniqueness.

</summary>

"The application of human creativity to evoke a response."
"What happens when one of our creations out creates us?"
"A machine will be evoking, manipulating human emotion."
"It is simultaneously amazing and disturbing."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of art and humanity, pondering on what it means to be human and the role of art in defining our uniqueness.
He questions the definition of art as the application of human creativity to evoke a response, discussing how this definition intersects with the rise of algorithm-generated art.
Beau mentions a website, thisartworkdoesnotexist.com, which showcases artwork created by an algorithm using deep learning, blurring the lines between human and AI creativity.
He contemplates the implications of algorithm-generated art evoking responses and being interpreted like human-created art, challenging the notion of what separates us from machines.
Beau raises existential questions about the future where AI creations may outdo human creations, prompting reflections on the control systems and societal constructs we create.
He invites viewers to visit the website to experience the artwork firsthand and encourages contemplation on the intersection of human creativity and artificial intelligence.
Beau leaves viewers with a thought-provoking notion about the evolving landscape of art, creativity, and the potential future where our own creations may surpass us.

Actions:

for art enthusiasts and thinkers,
Visit thisartworkdoesnotexist.com to experience algorithm-generated art and contemplate the implications of AI creativity (suggested)
</details>
<details>
<summary>
2020-03-05: Let's talk about how the 1.5 trillion-dollar F-35 is already obsolete.... (<a href="https://youtube.com/watch?v=UrWv7xDl_SY">watch</a> || <a href="/videos/2020/03/05/Lets_talk_about_how_the_1_5_trillion-dollar_F-35_is_already_obsolete">transcript &amp; editable summary</a>)

Beau delves into the resistance of institutions to change, critiquing the military's reluctance to embrace drone technology despite its potential benefits and cost-effectiveness.

</summary>

"The age of the fighter pilot, much like the age of the airship, is over."
"If we're not risking any human assets to accomplish something, we will probably do it more often because there's no risk."
"If it's just machines, nobody's going to care."
"We are wasting $1.5 trillion because it wasn't thought through."
"We're worried about creating the next generation fighter. We shouldn't."

### AI summary (High error rate! Edit errors on video page)

Explains how institutions resist change, even when it's necessary, due to institutional traditions.
Mentions how the military brings in outside experts for fresh perspectives, though sometimes these insights are not readily accepted.
Recalls the first use of airships by the military in 1912, leading to an obsession with air superiority around the world.
Criticizes the spending of $1.5 trillion on the F-35 fighter jets, which are considered obsolete even before being fully implemented.
Elon Musk's suggestion to shift towards drone technology for military aircraft is supported, as it eliminates the need for human pilots and reduces risks.
Raises concerns about the consequences of using drones in warfare, particularly the potential for increased civilian casualties due to reduced aversion to risk.
Points out Turkey's use of drones in a recent campaign as evidence supporting Musk's stance on transitioning to drone technology.
Emphasizes the wastefulness of funding obsolete military projects while claiming a lack of resources for other critical needs.

Actions:

for military decision-makers,
Advocate for the adoption of drone technology in military operations (exemplified)
Question and scrutinize military spending on potentially obsolete projects (exemplified)
</details>
<details>
<summary>
2020-03-03: Let's talk about what lifts people out of poverty.... (<a href="https://youtube.com/watch?v=zuY2Xr_Xmow">watch</a> || <a href="/videos/2020/03/03/Lets_talk_about_what_lifts_people_out_of_poverty">transcript &amp; editable summary</a>)

Beau questions the narrative of capitalism lifting people out of poverty, urging individuals to critically analyze its impact and take action for systemic change.

</summary>

"It's a moral judgment. It's very subjective. Do you believe that a system that encourages the very worst inhuman behavior is a force for good in the world?"
"You're a part of this system. Whether you approve of it or you don't, you're a part of it and because you're a part of it, you have the ability to influence it."
"You can make small changes. You can vote with your dollar."
"If we are looking for systemic change, we may need to adjust a whole lot."
"The economic system that has led to the world we have today, it has impacts far beyond poverty."

### AI summary (High error rate! Edit errors on video page)

Questions the widely accepted idea that capitalism has raised many people out of poverty.
Challenges the evidence behind the claim that capitalism is a force for good in the world.
Criticizes the use of World Bank numbers to support the argument about poverty reduction.
Contrasts the perspective on poverty as a feeling rather than a fact.
Notes that poverty is relative and comparative to one's surroundings.
Points out that the countries benefiting from poverty reduction are not necessarily free-market economies.
Emphasizes the lack of a causal relationship in the data supporting the argument for capitalism.
Provides statistics on poverty levels in the US over time to illustrate the correlation between consumerism, capitalism, and poverty reduction.
Raises questions about public social spending and its impact on poverty reduction.
Suggests that the debate around capitalism's benefits is centered on correlation rather than causation.
Encourages individuals to critically analyze whether capitalism is truly a force for good in the world.
Mentions examples of exploitative practices in major corporations as a reflection of capitalism's darker side.
Urges viewers to take action within their own spheres of influence to shape the economic system.
Stresses the need for systemic change and hard data to address issues beyond poverty, such as climate change.
Leaves the audience with a call to action to initiate changes starting from their own communities.

Actions:

for critical thinkers, activists,
Analyze the impact of capitalism in your community (suggested)
Support ethical businesses and practices (suggested)
Advocate for fair labor practices (suggested)
Engage in local initiatives for economic justice (suggested)
</details>
<details>
<summary>
2020-03-03: Let's talk about Trump's historic achievement.... (<a href="https://youtube.com/watch?v=pW8pZnLhJzk">watch</a> || <a href="/videos/2020/03/03/Lets_talk_about_Trump_s_historic_achievement">transcript &amp; editable summary</a>)

President Trump's foreign policy decisions have led to a historic number of military campaign losses, damaging U.S. credibility and relationships in the Middle East.

</summary>

"Trump's historic achievement is being the first U.S. president to lose three major military campaigns in less than one full term in office."
"This is not a peace deal. It is not a peace process. It is a U.S. surrender."
"The consequences of Trump's foreign policy decisions are significant and damaging."
"The administration's actions have undermined U.S. interests and relationships in the Middle East."
"The impact of these failures extends beyond the administration's term in office."

### AI summary (High error rate! Edit errors on video page)

President Trump's administration is on track to become the first in American history to lose three wars in less than one full term in office.
Trump's decisions in Syria led to a situation spiraling out of control, tarnishing the U.S.'s reputation.
In Iraq, Trump's actions turned the nation against the U.S., losing a potential ally.
The peace deal in Afghanistan is viewed as a U.S. surrender to the Taliban.
Trump's historic achievement is being the first president to lose three major military campaigns in a single term.
The consequences of Trump's foreign policy decisions are significant and damaging.
The administration's actions have undermined U.S. interests and relationships in the Middle East.
Trump's eagerness to outdo his predecessor has led to disastrous outcomes in foreign policy.
The loss of credibility and allies due to these decisions is a significant concern.
The impact of these failures extends beyond the administration's term in office.

Actions:

for policymakers, activists, voters,
Contact policymakers to advocate for a reevaluation of U.S. foreign policy decisions (implied).
Join advocacy groups working on international relations to raise awareness and push for accountability (implied).
Organize events or protests to bring attention to the consequences of failed foreign policy decisions (implied).
</details>
<details>
<summary>
2020-03-02: Let's talk about Reagan, a cliche, fear, and love.... (<a href="https://youtube.com/watch?v=Ea9sYVGJy3s">watch</a> || <a href="/videos/2020/03/02/Lets_talk_about_Reagan_a_cliche_fear_and_love">transcript &amp; editable summary</a>)

President Reagan's sci-fi unity cliche reveals how fear, not love, drives governance, urging us to seek interconnectedness amidst control tactics.

</summary>

"Governments use fear to motivate their populaces, they always have, hopefully that will eventually end."
"As we go through bumpy times, this situation, the economic situation, whatever, look for the interconnectedness. Look for the love."
"Fall in love. Don't fall in line."

### AI summary (High error rate! Edit errors on video page)

Exploring the idea of a science fiction cliche where the world bands together to defeat an alien threat, as mentioned by President Reagan.
The concept that this unity is not based on love or interconnectedness, but rather on fear.
Governments use fear to motivate people and maintain control.
Despite the narrative of unity in the face of a universal threat, reality shows that fear often leads to fragmentation.
Media and government tend to amplify fear to control the population.
Criticizes the fear-based leadership response to crises, citing examples from current events.
Calls for leadership that does not rely on instilling fear and cowardice in people.
Encourages looking for interconnectedness and love amidst fear to break the cycle of control.
Urges people to follow their hearts, not just their leaders, and to resist falling in line out of fear.

Actions:

for global citizens,
Seek out opportunities for interconnectedness and love in your community (suggested)
Challenge fear-based narratives and leadership by promoting unity and courage (implied)
</details>
<details>
<summary>
2020-03-01: Let's talk about getting invited to the cookout.... (<a href="https://youtube.com/watch?v=LPc7OLVZ4aM">watch</a> || <a href="/videos/2020/03/01/Lets_talk_about_getting_invited_to_the_cookout">transcript &amp; editable summary</a>)

Cookouts teach community building; support for black struggle leads to invites, urging us to strengthen neighborhoods.

</summary>

"Cookouts and barbecues offer lessons in community building."
"Support for the black struggle often leads to invites to black cookouts."
"Strong networks and neighborhoods are vital, especially during challenging times."

### AI summary (High error rate! Edit errors on video page)

Cookouts and barbecues offer lessons in community building.
Despite generalizations, cookouts tend to have similar elements of social interaction and fun.
Differences across demographic lines are unique and significant.
Support for the black struggle often leads to invites to black cookouts.
Beau shares his experience of attending a black cookout and feeling welcomed.
Cookouts organized by black communities are diverse in terms of professions and attendees.
There is a contrast in how white and black communities organize their gatherings.
White cookouts are often class-based, while black cookouts are neighborhood-based.
Self-segregation weakens neighborhoods and communities.
Strong networks and neighborhoods are vital, especially during challenging times.
Beau suggests breaking down walls and organizing block parties or open cookouts to strengthen communities.

Actions:

for community members,
Organize a block party or open cookout in your neighborhood (suggested)
Break down walls by getting to know your neighbors (suggested)
</details>
<details>
<summary>
2020-03-01: Let's talk about Bernie causing breadlines.... (<a href="https://youtube.com/watch?v=M9_qFrcWeUU">watch</a> || <a href="/videos/2020/03/01/Lets_talk_about_Bernie_causing_breadlines">transcript &amp; editable summary</a>)

Examining the misconception that socialism leads to poverty by debunking the breadline narrative and contrasting socialism's poverty elimination aim with capitalism's dependence on poverty as a planned feature.

</summary>

"Socialism brings poverty. That's the idea. Is it true?"
"Capitalism requires poverty. It's not a defect, it's a planned feature of it."
"The breadline thing just needs to go away. It's not true."

### AI summary (High error rate! Edit errors on video page)

Exploring the misconception that electing Bernie Sanders will lead to breadlines due to socialism.
Clarifying that Bernie is a social democrat, not a socialist, despite being labeled as a democratic socialist.
Debunking the idea that socialism brings poverty by examining countries that identify as Marxist-Leninist.
Providing examples of countries like Cuba, China, Laos, and Vietnam that do not fit the stereotype of extreme poverty under socialism.
Noting that many of the poorest countries operate under capitalist systems, not socialist ones.
Mentioning that economic sanctions have significantly impacted countries like Cuba's economy.
Pointing out that powerful economies like China and India refute the breadline narrative associated with socialism.
Contrasting socialism's aim to eliminate poverty with capitalism's reliance on it as a planned feature.
Emphasizing that poverty serves as a tool within the capitalist system to incentivize compliance.
Advocating for dispelling the Cold War propaganda linking socialism to breadlines.

Actions:

for social justice advocates,
Challenge misconceptions about socialism by sharing factual information and engaging in constructive dialogues (suggested).
Support policies and initiatives aimed at poverty alleviation and social welfare programs in your community (implied).
</details>
