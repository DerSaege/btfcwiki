---
title: Let's talk about what happens when the trucks stop....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1KCXmZ6l1j8) |
| Published | 2020/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses truckers' concerns from previous comments.
- Describes the critical role of truckers in the functioning of the country.
- Clarifies that the current shortages are due to increased demand, not supply issues.
- Assures that there are contingency plans in place to prevent a complete trucking shutdown.
- Outlines a hypothetical scenario if trucks were to stop rolling.
- Details the cascading effects of a trucking halt on fuel, medical supplies, food, and more.
- Points out the impact on everyday life, like garbage piling up and fast food shortages.
- Explains how trucking is vital for keeping stores stocked due to just-in-time delivery systems.
- Stresses the importance of tap water supply and the potential risks if trucking were disrupted.
- Advocates for revamping infrastructure post-crisis to address vulnerabilities.

### Quotes

- "Everything I'm about to say, every three letter agency in the world is fully aware of."
- "Your big box stores and your grocery stores most operate on just-in-time delivery."
- "That's how critical trucking is to the United States."
- "This is something we might want to address."
- "When the trucker cuts you off by accident, let it slide, especially right now."

### Oneliner

Beau outlines the critical role of truckers, the impact of a trucking halt, and the need to address infrastructure vulnerabilities post-crisis.

### Audience

Transportation industry stakeholders

### On-the-ground actions from transcript

- Support local truckers by offering assistance or understanding their challenges (suggested)
- Advocate for infrastructure improvements to ensure smoother operations in emergencies (implied)

### Whats missing in summary

Importance of community support for truckers during crises.

### Tags

#Truckers #SupplyChain #Infrastructure #EmergencyPreparedness #Logistics


## Transcript
Well howdy there internet people, it's Bo again.
So in the comments of that last video, there were a whole bunch of truckers going, you
didn't even mention us?
What?
You know?
Cause y'all are getting your own video.
I wanted to wait until certain messaging went out, because I'm going to describe what happens
if y'all take two weeks off.
Y'all go on a two week vacation.
And y'all know what that means.
Y'all are fully aware of everything I'm about to say.
You know how important you are to the functioning of this country.
But I'm not sure that most Americans are.
And with everything going on, I wanted to make sure everybody was aware that there is
no supply issue.
There's plenty of supplies.
What we're seeing right now is due to a spike in demand.
We have increased demand.
There's no supply issue.
Increased demand is why the shelves are empty.
There's plenty of supply.
It's coming.
I wanted to make sure that that was well known before I described this.
The other thing I want to point out is that this is not going to happen.
In the current mess that we're in, this is not a realistic scenario.
Everything I'm about to say, you can find more in depth.
I'm just going to hit the highlights.
Everything I'm about to say, every three letter agency in the world is fully aware of.
There are contingency plans and continuity of society plans, continuity of government
plans that are all focused on making sure this doesn't happen.
There are plans to have soldiers pressed into service to run the trucks if need be.
This isn't going to happen in this mess that we're in right now.
However, when we're done, after this is over, when everything settles down, we're going
to be revamping our infrastructure.
This is something we might want to look at because while it isn't a realistic possibility
in this current mess, there are scenarios in which this could happen.
Okay, so welcome to the stuff of nightmares.
Right now, trucks stop rolling.
Twenty four hours from now, there are fuel shortages.
That fast.
There are fuel shortages.
There are medical supply shortages.
That fast.
Your mail stops running.
So no Amazon.
There's no getting stuff from out of town delivered.
Okay, 48 to 72 hours, so two to three days after the trucks stop, you start seeing food
shortages.
Cash is not available at the ATM.
There is no gas.
You don't have shortages.
There's no gas.
It just, it's not there.
Garbage begins to pile up.
And this is where we start to see the breakdown of the supply chain.
So yeah, we have railroads and shipping and all of this stuff.
It all requires trucks to get it from the rail yard or wherever to its final destination.
At this point, ports grind to a halt.
Rail yards grind to a halt.
It all starts within three days.
Without trucks, everything stops.
Within a week, you don't see cars on the road anymore.
Because those who were lucky enough to have, to get gas that last day, well, they've used
it.
At this point is when your favorite fast food joint starts running out.
They last a little longer because most have walk-in freezers.
Your big box stores and your grocery stores most operate on just-in-time delivery or zero
inventory, whatever the buzzword is that they're using right now.
What that means is the truck shows up and it goes out on the floor for you to buy.
They don't have a back room like they used to.
This is to reduce overhead.
However, you see the issue with it.
Two weeks after the truck stop, you got to be careful with your tap water.
The supplies and the equipment used to clean it, it's not coming.
The estimate is two weeks after that, you better not be drinking anything without boiling
it.
That's how critical trucking is to the United States.
Now, this is our infrastructure.
This is it as it stands right now.
When this is over, we're going to be revamping a lot of our infrastructure.
We're going to be rebuilding it.
We have to decide how to build it.
While this isn't a scenario that's going to play out during this mess, it could play out
in others.
This is something we might want to address.
Zero inventory is great for corporate profits.
It reduces their overhead.
However, in an emergency, well you've seen the results.
To tie it to the other video, when the trucker cuts you off by accident, let it slide, especially
right now.
They've got to keep rolling.
A lot of them are not, let's just say their log books may be a little askew at the moment
because they're trying to get everything everybody needs.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}