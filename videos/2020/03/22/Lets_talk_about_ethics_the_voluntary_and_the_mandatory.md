---
title: Let's talk about ethics, the voluntary, and the mandatory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JcvVRWyNRoo) |
| Published | 2020/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about morals and ethics to do things voluntarily before they become mandatory.
- Urges people to think about society as a whole, not just the current situation.
- Mentions pushback on his statement that every sane person becomes a socialist during an emergency.
- Gives examples from movies where people cooperate in survival situations.
- References historical instances of rationing during the 1940s.
- Questions why some people are not following the advice to stay at home during the current crisis.
- Emphasizes the global nature of the emergency and the responsibility to humanity.
- Talks about the need for large changes in philosophical outlook post-crisis.
- Stresses the importance of global cooperation in facing future threats like climate change.
- Raises the choice between voluntary action now or mandatory action later due to survival necessity or government force.

### Quotes

- "This is a responsibility to humanity that we all share."
- "Those lines on a map are worthless."
- "The cooperation and mobilization that is taking place right now is what will be needed to combat climate change."

### Oneliner

Beau talks about acting voluntarily now to prevent mandatory actions later, stressing global cooperation in facing crises.

### Audience

Global citizens

### On-the-ground actions from transcript

- Stay at home voluntarily to prevent the need for mandatory lockdowns (exemplified)
- Cooperate with global efforts to combat climate change through voluntary actions (suggested)

### Whats missing in summary

Importance of global solidarity and proactive voluntary action in facing current and future crises.

### Tags

#Morals #Ethics #GlobalCooperation #Responsibility #ClimateChange


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about morals and ethics and doing things voluntarily so
they won't become mandatory.
You know there are a lot of situations where you get asked to do something voluntarily
but the reality is if you don't do it voluntarily it's going to become mandatory.
We're at a point right now where everybody needs to start thinking about society as a
whole and I'm not just talking about the current mess.
We can learn a lot from this current mess and apply it to things that are coming down
the road.
I got a lot of pushback on a statement that I said in a recent video.
I said every sane person becomes a socialist during an emergency.
No that's not true.
I want you to think that every movie you have ever watched that has that theme.
Whether it's people on a lifeboat rationing out that water or a zombie apocalypse and
people pooling their resources to make sure everybody had what they needed.
That is what happens in survival situations for those people who make it.
It doesn't happen in every scenario and typically those people don't make it.
And I know somebody said well yeah well those are movies and those are isolated incidents.
It's not like we ever did that on a national level.
You're right.
We have absolutely never rationed rubber, sugar, tires, canned dog food, toothpaste,
bicycles, gasoline, typewriters, dried fruit, meat, butter, silk, nylon, and medicine.
That never happened.
It especially didn't happen during the 1940s, the period in which everybody looks back and
says, hey they're the greatest generation.
It didn't happen then.
There weren't little books, little rationing books.
That didn't occur.
We had to mobilize because there was a large looming threat.
Right now people are being asked to stay at home.
200 million people are having to stay home now.
The real question is why aren't the other 200 million doing it?
We know that it needs to be done.
I'm not much for blind patriotism.
I am not much for anything like that.
That's not what this is.
This is a responsibility to humanity that we all share.
This doesn't stop at borders.
This isn't a national thing.
This is a global emergency and you're going to have to act or not.
Or not.
But what you do determines whether or not voluntary things become mandatory.
We're going to have to make changes when the dust settles.
Lots.
And I'm not just talking about to infrastructure or to healthcare or to all of the things that
are the hot topics right now.
We're going to have to make large changes.
We're going to have to change our philosophical outlook on how we view the world.
Because this has taught us borders mean nothing.
Those lines on a map are worthless.
They don't actually stop large global threats in any way, shape, or form.
There are other large global threats on the horizon and just like this, people are ignoring
it.
People aren't doing what they can voluntarily.
And if they don't, a lot of those suggestions will become mandatory.
Either out of necessity for survival or through government force.
We have other threats on the horizon.
We can use this as a training exercise on how to deal with them.
Live fire training going on right now.
The cooperation and mobilization that is taking place right now is what will be needed to
combat climate change.
We can do it voluntarily or we can wait until it gets so bad it's going to be done with
force.
It all depends on our philosophical outlook.
Anyway it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}