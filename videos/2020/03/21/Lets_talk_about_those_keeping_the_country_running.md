---
title: Let's talk about those keeping the country running....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2FUw55e59YM) |
| Published | 2020/03/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges the importance of workers in grocery stores, big box stores, gas stations, and pharmacies during the current crisis.
- Points out the societal misconception that jobs with plastic name tags are perceived as inferior due to poor life decisions.
- Stresses that these jobs are actually essential and integral to society regardless of character or decisions made.
- Calls for maintaining class solidarity and recognizing the necessity of these roles even after the crisis ends.
- Argues that individuals in these vital positions should have a decent standard of living, including financial security and health insurance.
- Challenges the notion that these jobs can be done by anyone, citing the skill and importance they hold.
- Urges society to use the current situation as a learning experience to bring about necessary changes.
- Encourages supporting these workers by backing politicians who advocate for their well-being come election time.
- Emphasizes that ensuring a decent standard of living for these workers should be the minimum effort society puts forth.

### Quotes

- "If a job is being performed and it is essential, it must occur for society to function."
- "I think that's the bare minimum that we can do as a society."

### Oneliner

Recognize the vital role of low-wage workers, ensure decent living standards, and support them politically for a fair society.

### Audience

Workforce advocates

### On-the-ground actions from transcript

- Support politicians advocating for decent living standards for low-wage workers (implied).
- Recognize and appreciate the importance of workers in grocery stores, big box stores, gas stations, and pharmacies (implied).

### Whats missing in summary

The importance of valuing and respecting all types of work, especially during crises, and advocating for lasting changes to uplift low-wage workers. 

### Tags

#LowWageWorkers #EssentialWorkers #Society #DecentLivingStandards #Support


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about something that we've all recognized, we may have known
before, but now it's something that nobody in the United States can deny.
It's one of those things that's just completely been exposed and it should change the way
our country looks at something.
But before we get into that I want to kind of give thanks to all the people working in
grocery stores and big box stores and gas stations and pharmacies, all of those people
who are right now doing what has to be done, literally has to be done.
That's what we have to recognize and we have to really bring that into the national consciousness
because for a very long time in this country there's been the idea that if you're working
in a job with a plastic name tag, well you've done something wrong.
You didn't play the game right.
Somehow you have made poor life decisions that put you in that position.
The reality is these jobs are essential.
They're essential.
They have to happen.
Society is structured in a way where there are going to be people doing these jobs.
Has nothing to do with their character.
It has nothing to do with decisions that they made.
Somebody is going to do them.
We need them to.
While many of the higher paying jobs are hanging out at home right now, those professions,
they're getting up every day going into work and risking their lives.
And yet we're appreciative now.
I really hope that we maintain that class solidarity with them when all the dust settles.
We've been forced to recognize openly, publicly, that these jobs are essential to our society.
They have to occur.
If a job is being performed and it is essential, it must occur for society to function, I would
suggest that anybody performing that job should have a decent standard of living.
I don't think that anybody who is fulfilling a job, fulfilling a role in society that is
so important, they don't get to stay home right now.
They shouldn't have to worry about their bills or health insurance.
They should have a decent standard of living.
Yet we have been conditioned to pretend like those positions can be done by anybody and
maybe sure their unskilled labor.
Quote.
Yeah.
You say that until you're at a fast food joint and it takes 20 minutes.
Yeah, it's a skill.
I would really hope that when all of the dust settles from this, we use it as a teachable
moment for ourselves and we make the changes necessary.
There's a lot of good that can come out of this, but we have to be willing to remember
what we learned during it.
Saying thank you right now, yeah, it may make you feel good, but you know what will make
them feel good?
If come election time, we support those people who support them.
We push to make sure that they have a decent standard of living.
I think that's the bare minimum that we can do as a society.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}