---
title: Let's talk about cleaning up the mess together....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uLXOWT5t3Y0) |
| Published | 2020/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges taking action instead of waiting for federal leadership.
- Encourages state and local governments to protect their people.
- Emphasizes the importance of individuals educating themselves and taking responsibility.
- Calls for putting people's well-being over the economy.
- Criticizes scapegoating and division, advocating for cooperation and unity.
- Reminds everyone to pitch in and do what they can.
- Stresses the importance of demographic information in determining involvement levels.
- Mentions the significance of health care and leave as national security issues.
- Warns against panic and the need to focus on protecting vulnerable demographics.
- Calls for collective mobilization in the absence of federal leadership.

### Quotes

- "Everybody has a part to play."
- "Put people over the economy."
- "It's humanity against this thing."
- "The sooner we start to act, the more manageable this becomes."
- "It's just us. And we're going to have to work together."

### Oneliner

Beau urges action over waiting for federal leadership, prioritizing people, unity, and cooperation to combat the global crisis effectively.

### Audience

State and Local Governments

### On-the-ground actions from transcript

- Protect the people within your jurisdiction (exemplified)
- Educate yourself and learn from successful approaches (exemplified)
- Put people's well-being over the economy (exemplified)
- Avoid scapegoating and focus on cooperation and unity (exemplified)
- Volunteer or help those who need support (exemplified)
- Stay home if you're at high risk (exemplified)
- Focus on protecting vulnerable demographics (exemplified)
- Mobilize at a community level in the absence of federal leadership (exemplified)

### Whats missing in summary

The emotional urgency and call for collective action against a global crisis.

### Tags

#CommunityAction #Unity #Responsibility #Cooperation #GlobalCrisis


## Transcript
Well howdy there internet people, it's Beau again.
So we're in a mess.
So what are we going to do?
Throw our arms up?
Wait for federal leadership that does not appear to be coming?
Or are we going to start cleaning up the mess ourselves?
Seems like the smarter route.
Everybody has a part to play.
Doesn't matter where you're at.
We're going to go through that right now.
We're going to go through some do's and don'ts.
If you are on the state or local government, do move to protect the people within your
jurisdiction.
You have authority vested in you.
And everybody in state or local government has the ability to help.
And this goes from school board members to the governor.
You can't wait for federal leadership.
Take your cue from nations that are already dealing with this.
Educate yourself, learn what worked and what didn't.
Try to apply it, do the best you can for those within your jurisdiction.
This is your moment.
If you have political aspirations of any kind, this is your moment to shine.
You want to get on the national stage?
Here you go.
Show that you have the character to lead at the national level.
This is going to propel a lot of people to the national scene.
Do put people over the economy.
We can bring the economy back.
Can't bring people back.
Focus on what matters.
Don't scapegoat.
It's foreign.
Dun dun dun.
We have senators floating baseless theories because that's what they're used to.
Scapegoat, blame, blame the other, those people.
It's not going to work this time because this thing, it does not care about the other.
It doesn't care about your borders at all.
I would also like to point out that those who they are trying to scapegoat sacrificed
their freedom of movement for a pretty long time to buy us time.
And that time was squandered, used ineffectively by the people attempting to scapegoat them.
That's not going to be a winning strategy.
Division isn't going to work here.
Cooperation, together, unity, that's going to work.
For the average citizen, rule 303 is in effect worldwide, do what you can, when you can,
where you can for as long as you can.
If you have the means, you have the responsibility.
Everybody's going to have to pitch in and for different people that is going to mean
different things.
You have to take a good, long, hard look at that demographic information.
Determine your risk level and that should determine your level of involvement.
If you're high risk, stay home.
And it's not just about you.
You don't want to overwhelm our infrastructure.
So stay home.
If you're not at risk, it could be volunteering at places that are probably going to need
volunteers.
It could be just getting supplies for those people who have to self-isolate.
Everybody has a part to play.
Even self-isolating, that's playing a part too.
And I know there's a lot of people right now saying, I don't have the means to self-isolate.
Yeah, I know.
Probably a lot of people like that.
But even that, you're doing something.
And most importantly, it can be a learning experience for everybody.
Come November, we're going to have a chance to alter some things.
Understand that health care, leave, these types of things, they're not just good.
They're not just fringe benefits.
It's a national security thing.
It needs to happen.
And I don't want to hear how we're going to pay for it.
Not after $1.5 trillion was just flushed down the toilet to keep numbers up.
Because that's what it's about.
It's about numbers.
And the numbers are getting bigger.
The ones that matter, not the economy, the people, those numbers are getting bigger.
This is a worldwide thing.
And we're looking at our numbers, and they're, yeah, they're getting up there.
But we cannot give way to panic.
Panic, the inability to affect self-help.
We can't give in to that.
Remember the percentages.
Remember the demographics.
Focus on protecting those.
We're going to have to get through this together.
And it's going to take a mobilization.
And since we're not getting a leadership from the federal, getting any leadership from the
federal government, we've got to do it ourselves.
Two kind of off-topic things.
One, if you're on Twitter, Instagram, you see somebody doing what they can, hashtag
Rule 303, please.
I think people are going to need some positive news.
Maybe those who are setting the example.
Another thing that I got tonight from a number of people was, I'm calming.
And if they self-isolate, they may need that.
I don't know.
I will try to come up with something.
Maybe we'll pick a zombie movie on Netflix and everybody watch it together on live stream.
I don't know.
I will try to come up with something to occupy your time as you go on a staycation.
But at the end of the day, we have to do this.
It's just us.
And we're going to have to work together.
There's no scapegoating.
None of that's going to be any use.
It's humanity against this thing.
It's not Americans.
It's all of us.
And the sooner we get that through our heads, the better.
The sooner we start to act, the more manageable this becomes.
The sooner we get over the American exceptionalism of, oh, it won't happen here, the easier this
is going to be.
The longer we delay, the bigger of a mess we end up with.
It's going to be harder to clean up.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}