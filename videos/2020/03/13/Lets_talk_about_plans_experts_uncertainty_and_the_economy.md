---
title: Let's talk about plans, experts, uncertainty, and the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=--WNarhBjGg) |
| Published | 2020/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the current state of news footage resembling a post-apocalyptic film montage.
- Expressing disappointment in the lack of a plan to deal with the situation.
- Mentioning the concept of the "deep state" and its role in providing continuity of government.
- Criticizing the focus on the economy over the well-being of those at risk.
- Advocating for utilizing existing plans and experts instead of creating new ones.
- Suggesting that governors or National Guard could step in to help citizens if the administration fails to act.
- Emphasizing the importance of handwashing, social distancing, and staying at home for those at risk.
- Noting the reluctance of the federal government to share information to avoid causing worry about the economy.
- Pointing out the economic impact of uncertainty and the failure to protect what should be prioritized during the crisis.
- Encouraging proactive measures to address the situation effectively.

### Quotes

- "The longer this goes on, the worse the economy is going to get."
- "We have the plans. We don't need the president."
- "Wash your hands, social distancing, if you're at risk, stay home."
- "The strategies and tactics used to contain it, they exist, and they're going to be the same."
- "He's failing at protecting that."

### Oneliner

Beau addresses the lack of a comprehensive plan to deal with the crisis, criticizes the focus on the economy over people's well-being, and advocates for utilizing existing resources effectively.

### Audience

Citizens, Governors

### On-the-ground actions from transcript

- Reach out to governors, National Guard, and Department of Defense to ensure preparedness (suggested)
- Practice handwashing, social distancing, and stay at home if at risk (implied)

### Whats missing in summary

The full transcript provides detailed insights on the importance of utilizing existing resources and experts effectively during a crisis, rather than solely focusing on economic concerns. It also underlines the significance of proactive measures and the need for proper communication from the government.

### Tags

#CrisisManagement #GovernmentResponse #Economy #PublicHealth #Preparedness


## Transcript
Well howdy there internet people, it's Bo again.
So I guess we're going to talk about the big news, you know.
Given the fact that news footage right now looks like a montage that occurs before a
post-apocalyptic science fiction film, I guess it's probably time to talk about it again.
You go back to the first video I did on this subject, general tone is, here's factual information,
stay calm, let the experts do their job.
Because I wrongly assumed that the experts would be allowed to do their jobs.
Right now, the tone in the media is that the president can't come up with a plan to deal
with this.
Can't come up with a plan to battle this.
Well maybe he's just been too busy battling the deep state, you know.
So because of that, his resources are limited.
Might be a good time to disclose and discuss what that really is.
The deep state are those people who are in non-political positions that survive transfers
of power that provide a continuity of government.
That's what they are.
Those would be the people that know the president doesn't have to come up with a plan.
Because we've had them on the books since the 1960s.
I don't think the fact that this wasn't intentionally done by the Soviets really changes much on
the ground as far as what needs to be done.
These plans exist, we've spent billions developing them over the decades, and they're sitting
idle.
It's almost as if the president is more concerned about the economy than he is the 3-6% of people
who are truly at risk.
Since the president seems to only be motivated by self-interest, I would like to remind the
administration that those at risk are age 50 and up, really.
That's where it really hits hard.
That's your base.
The president did not get a majority of any age demographic until you hit that range.
There's your self-interest.
We have experts, we have plans.
All it takes is for the administration to get out of the way.
The president does not need to come up with a plan.
They already exist.
Those evil deep state people, they have them.
They know what they are.
C-burn units are not paid for what they do on a daily basis.
They don't do much.
They're paid for what they can do when they need to.
This seems like a situation where they could be utilized.
Call me crazy.
And if the administration is too distracted, unwilling, or just doesn't care, doesn't have
the desire to enact these plans, or at least allow these people to serve as advisors, maybe
these plans should be given to the governors of the various states so they can make some
attempt to help their citizens.
This president is more concerned about the economy, and that was plain in his address,
than he is his voters.
Literally, his voters.
Not just American citizens.
Those people who support him.
We have the plans.
We don't need the president.
We certainly don't need Jared Kushner deciding what to do.
The plans exist.
They were developed by professionals, experts, over decades.
And they're sitting idle.
The trigger for the event doesn't matter.
The strategies and tactics used to contain it, they exist, and they're going to be the
same.
Billions.
And when the time comes to use it, we have a president that is too distracted by Fox
News and Twitter to enact them.
It might be time for the governors to actually reach out, talk to DOD, talk to their National
Guard.
Because I'm fairly certain that the National Guard has them as well.
Has access.
Anyway, wash your hands, social distancing, if you're at risk, stay home.
This is all information that should be constantly coming from the federal government, should
be on social media, everywhere being blasted out from every possible channel.
But they don't want to do that because they don't want to worry anybody because they're
concerned about the economy.
The longer this goes on, the worse the economy is going to get.
The stock market does not like uncertainty.
This is a very uncertain situation.
So not just is he concerned with protecting the wrong thing, he's failing at protecting
that.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}