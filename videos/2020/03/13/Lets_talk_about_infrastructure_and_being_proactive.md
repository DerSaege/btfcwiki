---
title: Let's talk about infrastructure and being proactive....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hxOUDi8CKwU) |
| Published | 2020/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of learning from past mistakes and making suggestions for the future based on those lessons.
- Notes the common scenario where security consultants are only called in after a problem occurs, stressing the need for post-event analysis and prevention strategies.
- Points out the deficiencies in the current infrastructure, particularly in medical and internet sectors.
- Emphasizes the urgent need for more ventilators and convertible ICU beds in the healthcare system.
- Raises concerns about the lack of access to healthcare, especially for the millions of uninsured people in the country.
- Identifies childcare, food security, and access to healthcare as critical issues that need immediate attention.
- Urges for updating infrastructure to address current challenges and future crises.
- Suggests that updating infrastructure could also aid in economic recovery and help combat climate change.
- Calls for a proactive approach in tackling infrastructure issues and stresses the importance of taking action.
- Raises awareness about the potential consequences of not addressing infrastructure weaknesses, especially in the face of more severe scenarios.

### Quotes

- "We have to update it. We have to upgrade it."
- "But there are much, much worse scenarios out there."
- "This needs to be done."
- "We need to update our infrastructure."
- "Wash your hands."

### Oneliner

Beau stresses the need to learn from past mistakes, update infrastructure, and prepare for future crises to avoid catastrophic scenarios.

### Audience

Community members, policymakers, activists

### On-the-ground actions from transcript

- Update infrastructure to address current deficiencies and prepare for future crises (suggested)
- Advocate for increased healthcare access for all individuals, including the uninsured (suggested)
- Support initiatives for better childcare services and food security in communities (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of infrastructure deficiencies and the urgency of addressing them to prevent future crises effectively.

### Tags

#Infrastructure #Healthcare #ClimateChange #CommunityAction #Prevention


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about
what we've learned so far.
And based on that, we're gonna kinda come up
with some suggestions for the future.
One of the things that always happens,
you know, when I was a security consultant,
nobody calls you beforehand.
Nobody calls you before there's a problem.
They call you once the problem exists.
And the first thing you have to do, 90% of the time,
is after action, hot wash and explain,
this is what went wrong and this is how you can stop it
from going wrong again in the future.
It's almost every first meeting.
Okay, so what have we learned thus far?
We have an issue with our infrastructure.
It is lacking in a bunch of different ways.
It's not just the obvious ones, okay?
So what do we have?
We know that our medical infrastructure
is lacking in a big way.
We need more ventilators.
We need more beds that can quickly be converted to ICU beds.
They don't have to be ICU beds all the time,
but they need to be able to be quickly flipped over to that.
Those are two things that are real obvious.
The thing is, this was being discussed 10 years ago,
more than 10 years ago.
Professionals have been warning about this for a long time.
The infrastructure is not there.
Okay, what else have we learned?
Our internet infrastructure is apparently not up to task.
If everybody stays home to work,
then, well, the internet will go down.
Well, we should probably fix that
because in times of emergency,
everybody's gonna be trying
to get their information that way.
So even if it's not just normal work,
it's going to impact it in critical times.
We have realized that not just do we
have limited healthcare infrastructure,
we have limited access to healthcare,
and that's gonna be an issue.
Right now, people are still debating this.
I don't know why we have to do this.
Understand, solve all the problems you want,
but if the tens of millions of uninsured people
in the country can't get healthcare,
you're not gonna solve anything
because it's gonna keep coming back around.
So it's not just a moral thing.
It's not just a, you know,
we're kind of like the leader of the free world type thing
and people should have access to it.
It's a national security issue.
So it's something we have to solve.
Childcare.
We need more childcare.
We need better access to food.
Food security, a big one.
These are all things that are becoming very, very apparent.
And it's all been warned about.
It's all been talked about for years.
We knew it was coming.
And all of it can be solved by updating our infrastructure.
It's all been talked about for years.
There's another group of scientists out here
that have been warning us about something for years,
for decades, and we have to alter the infrastructure
to deal with it.
Maybe we should do it at the same time.
It would make more sense to do it at the same time.
People are wondering how we're gonna get the economy
back on track.
Well, I would suggest that updating the infrastructure
would probably help do that.
So we need to take the blinders off.
We need to address this as if it's a problem
that we have some kind of input on.
And we just need to adopt the stance.
This needs to be done.
We need to update our infrastructure.
While we're updating it to deal with crises like this,
we need to update it to deal with climate change.
We can do it at the same time.
It would be a whole lot more effective.
Or we can ignore climate change the way we ignored
this possibility that we have been warned about for decades
and understand this isn't the bad one.
When I went to those conferences,
this isn't what they were talking about.
This was bad.
But there are much, much worse scenarios out there.
This is overwhelming our infrastructure.
Imagine what the really bad one will do.
We've got to update it.
We have to upgrade it.
And we can do climate change at the same time.
Anyway, it's just a thought.
Y'all have a good day.
Wash your hands.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}