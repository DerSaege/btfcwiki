---
title: Let's talk about the prepping community and a heart to heart....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PJGF2TnhYyI) |
| Published | 2020/03/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the prepping community, Beau stresses the importance of sharing resources and preparedness.
- He questions the reasoning behind stockpiling specific calibers of ammunition and firearm choices within the community.
- Beau explains the concept of pooling resources and the necessity of sharing within a network for security.
- He urges preppers to understand the tactics associated with medical supplies like masks, especially during times of crisis.
- Beau calls on individuals with excess resources to assist healthcare workers by providing necessary supplies.
- He warns against solely focusing on gathering equipment without learning the necessary strategies and tactics to survive.
- Beau advocates for being proactive in helping others and saving lives during emergencies.
- He encourages preppers to think beyond just having equipment and to prioritize learning how to effectively utilize resources.
- Beau acknowledges the surplus of supplies within the prepping community and the potential to make a significant impact by donating to local hospitals.
- He underscores the importance of readiness and being able to adapt and assist during challenging situations.

### Quotes

- "Your security detail is out of ammo."
- "Do what the whole theory is about. You've prepared for this moment. You can save lives."
- "Get them the supplies they need."
- "A gun is the last line."
- "Be the hero."

### Oneliner

Addressing the prepping community, Beau stresses sharing resources, understanding tactics beyond firearms, and advocating for proactive assistance in times of crisis to save lives.

### Audience

Prepping community members

### On-the-ground actions from transcript

- Share excess medical supplies with healthcare workers (exemplified)
- Assist local hospitals by donating surplus resources (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the mindset of the prepping community, the importance of resource sharing, and proactive assistance during crises.

### Tags

#Prepping #ResourceSharing #CommunityAssistance #EmergencyPreparedness #SavingLives


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna have a little heart to heart
with the prepping community.
If you're not within that community,
you may not understand everything I'm about to say
because I'm going to speak their language.
Still worth watching though,
because you're gonna learn a little bit about it
and how to prepare yourself if you wanted to.
Okay, so to the people in that community,
I don't know you, I don't know who's watching this,
but I'm willing to bet, sitting in a locker
or on a shelf or buried underground or whatever,
five, five, six, seven, six, two,
45, nine, 22, 12, right?
That's what you've got.
Those are probably, you may have some extra random stuff
thrown in there, but those are probably the rounds you have.
That's what you have in quantity.
Why?
Why do you have those particular calibers?
Why does your carbine match the ammo in your pistol?
Interchangeability, right?
Makes sense.
Why are you using an AR?
Why do you have one?
You telling me that is the best out there?
That is a crowning achievement of firearms design?
No.
No.
Doing it because everybody else in your network has one.
Interchangeability, if stuff goes loud,
you can slap a magazine in their hand if they run out of ammo.
Why do you have 10,000, 50,000, 100,000, millions of rounds?
Are you telling me you foresee a combat scenario
in which you expend more ammo than a soldier in Iraq?
Probably not.
Probably not.
Interchangeability.
If you're advanced, you probably have a plan,
location you're all going to meet up at.
Maybe you're going to double up.
You're going to secure some one person's location
because they have all the resources, whatever.
Everybody's going to bring their stuff there,
and you're going to run security details,
have patrols out, right?
So first, inherent in the idea of doubling up,
of securing a location and everybody bringing what they have,
inherent in that idea is the idea of sharing.
That's why it's there.
If something happened to somebody in your network
and they don't have all of their ammo
or they don't have whatever, you've got them covered.
That's what it's for.
That's why you do it, pooling the resources.
Your security detail,
you wouldn't send them out there with no ammo.
That would be dumb.
You wouldn't do that because if they have no ammo,
the opposition, the threat, whatever it is,
can get through them into you.
They're at your door, right?
Nothing I'm saying right now is controversial.
Nobody's going to argue with what I'm saying at this moment.
The problem is within this community,
people focus on that gun.
That's the equalizer, right?
But that's not all that you stockpile.
That's not all you have sitting on shelves, is it?
You've probably got a lot of other stuff,
half-face respirators, P100s, N95, surgical masks.
You've got it because you knew this was a threat.
You knew this was coming.
This could happen, and you were prepared for it.
But unlike the firearm,
you may not understand the tactics
associated with those things
because right now, your security detail, they got no ammo.
They are out there with no ammo.
They don't have the tools they need.
If healthcare workers get sick because they don't have
the tools they need, they don't have the ammo, what happens?
Nobody gets treated.
If nobody gets treated,
they get through your security detail,
and they're at your door, the threat's at your door.
I'm not saying that you need to go and drop off
your entire stash at the local hospital.
That would be counter to pretty much everything
within the prepping community.
That's not what you're supposed to do.
But if you have a family of four,
and you're sitting on hundreds or thousands
or tens of thousands of N95s or P100s,
you have way more than you could ever use.
You need, like, three for each person in your family.
Everything else, it's just going to sit on a shelf.
This is the moment you prepared for.
People want to be a hero with that gun.
You can be a hero right now by dropping off masks.
And if you're in a small, most people who prepare,
you live in a small community.
Take it, put it in a bag, write your name
and phone number on it.
If they don't need it, they'll give it back.
But right now, they do.
Right now, they do.
Your security detail is out of ammo.
Are you going to run them their supplies?
Or are you going to stay in the rear?
What are you going to do?
And let's be honest, people in the prepping community,
you don't need the masks because you're prepared.
You've got food.
You've got medical supplies.
You've got cleaning supplies.
You're not going anywhere.
You're sitting at home, like, sitting at home,
like, yep, saw this coming.
You have nothing to worry about
because you're not going anywhere.
You don't have a need for the masks.
They do.
They're on the front line.
And let's also be honest.
You may not understand how to combat the threat,
but you know about the gear
because that's what the prepping community focuses on,
the equipment.
And because the community tends to focus on things
that go wrong and then go really, really wrong,
you know how to reuse those masks,
and you have the supplies.
Tell me you don't have a bunch of hydrogen peroxide,
bleach, alcohol.
You do, and you know how to reuse them.
The healthcare community, your healthcare providers,
they've always had supplies.
They've never had to think like that.
They don't know, a lot of them.
They're learning now.
You can help ease that learning curve for them.
Now, the reality is, if what I'm saying right now
is a shock to you, it's a surprise to you,
or you want to reject it out of hand,
you're not going to make it through any real bad scenario anyway
because you focus solely on gathering equipment
and not learning any of the strategies or tactics
that go along with it,
which means, in reality, some 16-year-old with a 22
is going to pop you a new hole right in the middle of your forehead
when things get bad.
Going to get lucky, and the $500,000 you spent preparing
just turned your house into a loot box.
A gun is the last line.
You want to make sure it never gets to that
if you've got any brains in your head,
and if you don't, if things get bad,
they won't be there long anyway.
If you have extras,
you need to drop them off to your local hospital.
Be the hero.
Because let's be honest, within the prepping community,
how many of these masks are out there?
Two million, five million, ten million?
We don't know because nobody says what they have.
But you've got them, and you've got the P100s,
and those are better than the N95s.
You've got them.
You probably also have full CBRN kits,
which are better.
And if things get bad, what are you going to use?
The CBRN stuff.
You have multiple layers of defenses,
and you've got a whole bunch right now
that could stop it from getting bad.
Right now, your health care provider,
the people protecting your community,
your security detail, your front line,
they don't have ammo.
They're running into these rooms Donald Duck, no pants.
I'm not saying give up everything you have.
I know that doesn't even make sense.
But you got way more than you need,
than you could ever need.
Do what the whole theory is about.
You've prepared for this moment.
You've prepared for an event like this.
You can save lives.
And that's what it's about, right?
I know some of you are out there and you're just nuts,
and you're looking forward to this
because you think you're going to be the one
that makes it through and gets to set up
a little kingdom or whatever.
Real life, that's not the way it's going to go down
because somebody's going to get lucky.
And if you haven't thought about this part,
you haven't thought far enough ahead.
Your security detail is out of ammo.
Get them the supplies they need.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}