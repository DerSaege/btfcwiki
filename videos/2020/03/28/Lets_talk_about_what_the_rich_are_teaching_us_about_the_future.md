---
title: Let's talk about what the rich are teaching us about the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bMLQUArdeAw) |
| Published | 2020/03/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the lessons to be learned from the current situation, focusing on access to resources.
- Points out how the wealthy have advantages in times of crisis like being able to access tests, fly out of affected areas, and isolate easily.
- Mentions that major events will continue to favor the wealthy.
- Predicts more incidents like the current crisis as climate change accelerates, leading to expensive necessities.
- Explains how production of necessary items is currently high due to demand and cost, but prices will drop significantly after the crisis.
- Advises stocking up on needed items once the crisis is over, especially for those who can make large purchases.
- Emphasizes the importance of being prepared with supplies that have long shelf lives and can benefit the community.
- Recommends having food reserves and mentions affordable food buckets with a month's supply.
- Encourages readiness for future crises by taking advantage of surplus supplies post-crisis.

### Quotes

- "The wealthy. The wealthy."
- "Get more than you need. Get more than your family needs. Because your community is going to depend on you."
- "It's not delicious, but it will keep you alive and it will give you a sense of comfort anytime the shelf is empty."

### Oneliner

Beau explains how the wealthy have advantages in crises, urges stocking up on essentials post-crisis to aid community preparedness, and stresses the importance of being ready for future emergencies.

### Audience

Prepared individuals and community members.

### On-the-ground actions from transcript

- Stock up on necessary items post-crisis (suggested).
- Ensure proper storage of essentials (implied).
- Purchase affordable food buckets for a month's supply (implied).

### Whats missing in summary

The importance of community cooperation and support during and after a crisis.

### Tags

#Preparedness #CommunitySupport #WealthDisparity #EmergencyPlanning #ClimateChange


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about what we can learn from all of this, because there
is something we can learn and it's important and it can make our lives better in the future.
Who isn't having a problem right now?
Who is able to get access to tests?
Who is able to fly out of anywhere there was an issue?
Who is able to isolate easily?
The wealthy.
The wealthy.
That is going to be true for every major coming event.
They will find a way out.
That is why a lot of the things that people are like, well, you know, they wouldn't do
that because it's going to impact them too.
No, it won't.
It won't.
There are going to be more incidents like this.
It's going to happen.
As climate change accelerates, you will see larger ranges for insects.
You will see more and more stuff like this occur.
It's been predicted for 20 years and it's happening.
Now when something like this is going on, as you certainly know if you're on my Twitter
feed, stuff that is necessary to deal with it becomes expensive.
Prohibitively expensive.
But let me tell you what happens immediately after it.
All of this stuff is being produced en masse right now.
Just tons and tons and tons.
They're pumping out as much as they can because one, it's really expensive right now and they
get a premium on it and two, there's a high demand and they don't know how long that demand
is going to last.
So it's just, they're working overtime producing as much stuff as they can to deal with this
scenario.
As soon as this scenario is over, all of that stuff is available at rock bottom prices.
If there is one thing I have learned, it's that when this is over, I'm going to stock
up on the stuff that's needed.
And if you have the means to make large purchases, you really should.
Because they're going to have access to private hospitals and doctors.
We won't.
And our hospitals will be in need again.
It will happen again.
This stuff, most of the stuff that is necessary for this scenario, they have expiration dates
on them, but not really.
As long as they're stored properly, they don't really go bad.
As far as the masks, what you really have to worry about is the elastic.
That's it.
You can fix those.
You can repair those.
There's going to be a whole lot of stuff like that that's available for cheap.
That's the time to get it.
And get more than you need.
Get more than your family needs.
Because your community is going to depend on you.
And it is frustrating to have to try to track this stuff down and pay an arm and a leg for
it.
So if you are somebody that has the means, take it upon yourself.
Rule 303.
Get some of it and store it properly.
It will help.
It will save lives.
And if there's one thing I hope everybody learned, it's make sure you have food in your
house.
I will tell you, I'm not going to say a brand, but there are a number of companies out there
that produce buckets that have 30 days worth of food in them.
Under normal circumstances, they're like $70.
Right now, if you can get one, they're probably $300.
It's not delicious, but it will keep you alive and it will give you a sense of comfort anytime
the shelf is empty.
Those will be cheap too, because they are pumping them out right now.
So there will be a surplus when this is over.
Take advantage of it and get prepared.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}