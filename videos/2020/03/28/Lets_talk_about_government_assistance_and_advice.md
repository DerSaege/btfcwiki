---
title: Let's talk about government assistance and advice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dLqCjvGtCOs) |
| Published | 2020/03/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses government assistance stigma and advice from a privileged perspective.
- Criticizes common advice about planning better, getting a valuable education, finding work, and joining the army.
- Points out the hypocrisy of those who give advice but still accept government assistance.
- Questions whether any advice given doesn't apply to everyone.
- Acknowledges the importance of jobs that are now deemed non-essential during the crisis.
- Compares government assistance to a stimulus to get through tough times.
- Talks about how crises like poverty and lack of opportunities disproportionately affect those at the bottom.
- Encourages cashing the government assistance checks but warns against looking down on others in worse positions.
- Challenges the notion that people in Section 8 housing are the source of problems, instead pointing to those in power.
- Calls for destigmatizing government assistance as more people may need it soon.

### Quotes

- "Somebody with less money, less power, and less influence is not the source of anything going wrong in your life."
- "Those in a worse position than you are never the source of your problems."
- "The source of your problems do not emanate from Section 8 housing."
- "We need somebody to look down on and blame."
- "We need to destigmatize government assistance because we're all about to be on it."

### Oneliner

Beau addresses government assistance stigma, challenges privileged advice, and urges against blaming those in worse positions, advocating for destigmatization as more people may need assistance soon.

### Audience

Advocates for social justice

### On-the-ground actions from transcript

- Destigmatize government assistance (advocated)
- Avoid blaming those in worse positions (advocated)
- Challenge privilege and hypocrisy (advocated)

### Whats missing in summary

The full transcript provides a deep dive into societal attitudes towards government assistance and challenges individuals to rethink their perspectives on blaming those in need.

### Tags

#GovernmentAssistance #Stigma #Privilege #Destigmatization #SocialJustice


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about government assistance and the stigma and advice that
often goes along with it.
That advice is coming from people who are in a better position and of course it's well
meaning.
Maybe sometimes.
Maybe not.
Maybe it's just a way to feel better about ourselves.
Because it's always coming from somebody just a little better off.
We've all heard that advice, maybe given it.
You know you should have just planned better.
Should have got a more valuable education.
An in demand field.
That's what you should have done.
You know there's always some kind of work out there to be done.
Just go find it.
Turn in an application.
You know you should have got a more valuable job, this wouldn't have happened.
You know if you'd been more productive, you wouldn't have got laid off.
Your able bodied, why don't you join the army?
Just made bad life decisions.
Think we've all heard it, many have said it, and I'm willing to bet that everybody who
said it is going to cash their $1200 government assistance check when it comes.
Is there any of this advice that doesn't apply to us?
Couldn't we have all planned a little bit better?
Maybe we should have got a more valuable education.
Been nurses, they're needed.
A more essential job, the funny part being, jobs everybody was talking about being valuable,
they're all at home right now.
Yeah it could all be said to us.
This isn't government assistance, this is a stimulus check.
It's just there to get me through and stimulate the economy.
Yeah, like SNAP, food stamps, that's what that does.
Same thing.
Same thing.
No no no, see this is different, this is no fault of my own.
You know this is because there's a crisis.
Yeah there's a lot of crises in this country.
You know you've got poverty, education, lack of opportunities, there's a whole bunch of
crises, tons, giant list.
The thing is those crises, they tend to just affect those people down there on the bottom,
and since they're not big enough to affect the bottom line of those in power, well they
don't get as much air time.
And since they don't get as much air time and they aren't explained, well we don't have
to be understanding about those people, those others, right?
I'm not saying don't cash the check.
Definitely go cash the check.
It's your money, go cash it.
What I'm saying is don't kick down.
Those in a worse position than you are never the source of your problems.
Somebody with less money, less power, and less influence is not the source of anything
going wrong in your life.
That's just what those up top have trained us to believe.
We need somebody to look down on and blame.
Definitely go cash the check.
What I'm saying is don't kick down.
The source of your problems do not emanate from Section 8 housing.
They emanate from those people in DC.
We need to destigmatize government assistance because we're all about to be on it.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}