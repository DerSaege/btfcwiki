---
title: Let's talk about situational awareness and a whole white baby....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KmDbvesjrks) |
| Published | 2020/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines situational awareness as the perception of environmental elements and events with respect to time or space, comprehension of their meaning, and projection of their future status.
- Describes situational awareness as the constant and immediate application of deductive reasoning, allowing one to predict outcomes.
- Shares a personal story where he applied situational awareness at a gas station late at night, evaluating a stranger's request for help.
- Illustrates the process of deductive reasoning in real-time, showing how he assessed the situation and made decisions based on environmental cues.
- Encourages conscious practice of situational awareness in daily life to become better at it and apply it effectively in crisis situations.
- Mentions a video where individuals demonstrate situational awareness by rescuing a child found walking on a highway.
- Emphasizes the importance of developing situational awareness as a valuable skill for navigating daily life and stressful situations effectively.

### Quotes

- "Situational awareness is the constant and immediate application of deductive reasoning."
- "It's a skill that if you plan to be out and about or you plan to be, you don't plan to be in a crisis situation."
- "You do it all the time whether or not you call it that or not."

### Oneliner

Beau explains the importance of developing situational awareness through real-life examples and encourages practicing deductive reasoning for better decision-making in various situations.

### Audience

Individuals, Community Members, Safety Advocates

### On-the-ground actions from transcript

- Practice conscious situational awareness in daily activities to hone the skill and improve decision-making (suggested).
- Encourage others to develop situational awareness through real-life scenarios and practice deductive reasoning for better crisis management (implied).

### Whats missing in summary

The full transcript provides detailed examples and explanations on the concept of situational awareness, offering practical insights on applying deductive reasoning in real-time situations for improved decision-making.

### Tags

#SituationalAwareness #DeductiveReasoning #SafetySkills #CrisisManagement #CommunitySafety


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about situational awareness.
Why?
Because on a recent live stream somebody asked me to talk about it and it's a cool topic.
But first, we have to decide what it is.
What is situational awareness?
Situational awareness is the perception of environmental elements and events with respect
to time or space.
The comprehension of their meaning and the projection of their future status.
Right.
That's Inslee's model.
That's a good definition if you're going to get really into this topic and you want to
understand the psychology of it.
It might be a little too in depth for our purposes though.
A perhaps simpler definition would be the constant and immediate application of deductive
reasoning.
It allows you to predict outcomes.
It's the same thing.
It's just a little less wordy.
The easiest way to talk about it and describe it is to talk about it in context.
So I'm going to tell you a story and then I'll tell you about a video that you can watch
and you can actually see people apply situational awareness in real time.
And they were not people who were trying to illustrate it.
They just did it.
Okay.
Few nights ago, it's like 2.30 in the morning.
I stop at a Stucky's.
That's a gas station for those people outside the South.
One of those that's always lit up like a Christmas tree.
I'm sitting there pumping gas and this guy comes up and he's like, hey, my battery's
dead.
Can you help me out?
Now in my mind, I'm looking at him and I'm like, never seen this dude before in my life.
Now I live outside of a town of 4,000 people.
Even if I don't know you, I've seen you.
I'll recognize you.
However, we are close to a highway.
Might have came from there.
Immediate application of deductive reasoning.
All of that took place in my head in a split second.
What did I say?
Of course, absolutely.
To which he replies, I'm parked around back.
In my mind, right.
Let me tell you a story about picking the wrong one.
But what do I say?
Okay.
Because through situational awareness, I happen to know that that parking lot is a loop.
What runs behind the gas station comes out the other side.
If it is something untoward, I can drive through it.
When I get in the truck to pull around, I have two things going back and forth in my
mind.
The first is the reality.
Realistically, if you are somebody looking to rob someone, you're looking for easy prey.
You're probably not going to pick a guy that looks like me.
Number one, I don't look like I have any money.
Number two, I look like I'm dumb enough to fight back.
Number three, I'm a redneck.
I mean, it's just not, probably not the ideal target.
The other thing going through my mind is right, around back, in the dark.
Cool, I'm going to get to kick this dude's teeth out and stuff him in a dumpster.
When I make it around the building, I see the car.
The car is parked with its front bumper up against something.
Not looking to make a getaway.
Pull up next to it, and before I get out, I look over.
The driver's side seat is leaned all the way back.
So apply deductive reasoning to that.
The constant application of deductive reasoning.
Guy I don't know, near a highway, parked in the dark, behind the gas station lit up like
a Christmas tree, in the middle of the night, and his seat is leaned all the way back.
Pulled off the road, pulled off the highway to take a nap, left his lights on.
Parked out back because he didn't want to be kept awake by the lights.
Makes sense.
Get out of the car, as I'm talking to him, very nice guy.
That's exactly what happened.
That's a clear demonstration of situational awareness.
It's the constant processing of the information.
You do it all the time whether or not you call it that or not.
Your conclusion changes as you get more information.
It's a dynamic process.
It's a cycle.
And if you go to the other definition, you'll see that it's a loop.
Now if you want to watch it in real time, there's a video.
There's two guys, they're driving down the road, and they're driving down the highway,
and they see a kid, like two.
It's cold outside.
Kid's wearing a diaper.
And he's in the highway with 18 wheelers.
Them being good people and not wanting to see pet cemetery in real life, they stop.
They start live streaming.
Why?
I would guess they did it because sometimes people stereotype.
And you have a kid who is clearly not where he's supposed to be, who might have been reported
missing.
It would be a good idea to have clear evidence that you, two black guys, did not kidnap this
whole white baby.
If you're going to look for it, that's the term you need to look for for the video.
Whole white baby, because they found a whole white baby in the middle of the road.
So the kid was walking down the hill.
So what do they do?
They go up the hill.
He's probably walking away from where he's supposed to be.
Immediate application of deductive reasoning.
They get up there, they start knocking on doors, those that they can get to.
Some of the houses are fenced off.
They can't find where the kid's supposed to be.
They call the cops.
Cops show up and they tell them what's going on.
Cops are like, you know, there's a mobile home parked down the road.
He's probably from there.
So for all those people who are like, cops won't stereotype like that.
I don't know about that.
Anyway, so one of the guys, when the cop says that, he's like, no, he's barefoot and his
feet are clean.
He hasn't walked far.
The immediate application of deductive reasoning.
And it's constant.
And you can watch the whole video.
The kid is returned safely by the end of it.
And he was not from the mobile home park.
It's worth watching because these guys, they didn't set out to demonstrate situational
awareness in real time, but they did.
Everybody does it in the course of their daily lives.
I would suggest that a lot of times when you get that feeling that something's not right,
subconsciously you have processed something that has led you to conclude there's a threat
of some kind.
So you act on it.
People want to know how to develop it because it's used in crisis situations.
It becomes really important.
The same way you develop any skill, you practice it.
When you're going about your daily life, be conscious of it.
The more conscious of it you are, the better you get at it.
And then eventually it just becomes the constant and immediate application of deductive reasoning.
You're doing it all the time.
And when you're conscious of it, you're better at it.
You can practice it just walking around the store, looking at people, trying to deduce
things.
You know, based on shoes, what somebody has clipped to their belt.
We do it all the time.
That expression about first impressions, that's all based on deductive reasoning.
Situational awareness is just making that dynamic, making it a continual thing.
It's a skill that if you plan to be out and about or you plan to be, you don't plan to
be in a crisis situation.
If you want to be more effective in stressful situations, it's a skill you should hone.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}