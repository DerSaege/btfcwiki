---
title: Let's talk with Jen Perelman about Florida and 2020....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Dh_kMhDhls8) |
| Published | 2020/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Jen Perlman is priming Debbie Wasserman Schultz in Florida's 23rd congressional district, aiming to remove a long-standing incumbent.
- Perlman criticizes Wasserman Schultz for prioritizing corporate interests over community concerns like environmental protection.
- Perlman, a second-generation Floridian with a legal background, is focused on justice and serving the people, not a political career.
- She advocates for removing profit motives from industries like healthcare and education, supporting single-payer healthcare and affordable education for all.
- Perlman opposes bans on assault weapons, citing their inefficacy and the need to address root causes of gun violence.
- She argues against the privatization of public services like education through vouchers, advocating for quality options for all.
- Perlman denounces ICE as unnecessary and criticizes its treatment of immigrants, advocating for a humane approach to immigration.
- She addresses wealth inequality and lack of affordable housing in Broward County, calling for a living wage and healthcare as human rights.
- Perlman encourages community involvement and service as a way to enact change, urging people to connect with local organizations and address pressing needs.
- She stresses the importance of grassroots support, small donations, and community engagement in her campaign against corporate influence.

### Quotes

- "It's about justice. It's about social justice, economic justice, and environmental justice."
- "I have no career motives. I have no career ambition in this whatsoever. This is something that I want to do as a service."
- "I don't see them as protecting and serving. I don't see the people that they're going after as being necessarily dangerous to society."
- "I just have this crazy idea that people shouldn't have to work three jobs and drive an Uber to be able to live."
- "Find the need, there's no shortage. Even if it's going to the library and just doing a little bit of research and saying like, what's going on in this area and how can I help?"

### Oneliner

Jen Perlman, priming Debbie Wasserman Schultz in Florida, advocates for justice, single-payer healthcare, community service, and grassroots change against corporate influence.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Reach out to local organizations, attend meetings, and offer help in areas of need (exemplified)
- Organize clothing drives, toiletry collections, or beach cleanups to support local communities (exemplified)
- Support Jen Perlman's campaign by donating to gen2020.com and spreading the word (exemplified)

### What's missing in summary

The full transcript provides a comprehensive look at Jen Perlman's background, values, and platform, offering insight into her grassroots campaign and commitment to community service. Watching the full transcript can provide a deeper understanding of her approach to politics and activism.

### Tags

#Florida #Politics #CommunityService #SocialJustice #GrassrootsCampaign #CorporateInfluence #SinglePayerHealthcare #AffordableHousing


## Transcript
There we go.
Well, howdy there internet people, it's Bill again.
And so today we are going to be talking to
the person priming Debbie Wasserman.
So take it away.
My name is Jen Perlman
and I am the person priming Debbie Wasserman Schultz
in Florida's 23rd congressional district.
We are down here in, mostly we're Broward.
We have a little bit of Dade County in our district.
So just so people have an idea of where we are.
We are an exceptionally gerrymandered blue district
and we also have closed primaries.
And that is one of the ways in which that our incumbent
has been sitting safely for so many years.
So as far as me, I am a second generation native Floridian.
I was born and raised in North Miami Beach.
I have degrees in journalism, marketing and law.
And I was a practicing attorney doing criminal defense.
And then after that, doing some corporate litigation
and some other things when I moved down here to Florida,
I was gone for a few years getting my degrees,
came back home where I've now been since 2003.
So I have been watching her as my congressperson
for 15 years, 16 years.
16 years she's been our congressperson.
And over that time, she has taken inordinate amounts
of money from special interests and corporate interests
that are very contrary to our interests.
And as a Floridian, and one of the main things
that people have noticed down here
is the hurting of our environment.
And one of the causes of our problems,
whether it's blue green algae or red tide being exacerbated
is the industrial agricultural runoff.
And one of the biggest sources of that
is the big sugar industry.
And that is one of Debbie's biggest corporate donors
is big sugar.
So I've just been watching for years as these corporations
and their interests have basically
been dismantling our interests.
And so when I saw in 18 that several non-corporate candidates
were able to break through the establishment,
I felt that maybe the timing was somewhat, maybe we're there.
Maybe we're at the breaking point
where people are starting to recognize
that the corporate takeover has not served us.
And maybe there is an opportunity now
to come in and try to get that money out.
So my involvement now is more of a timing thing,
even though I've been involved in policy and campaigns
since before I was old enough to vote.
So for those people outside of Florida,
explain to them what the runoff does, especially to those of us
up here in the panhandle when our beaches go.
Anyway, go ahead.
OK, so it's complicated.
And I know I have spoken with several people who
are experts on this who really understand it.
Sometimes their voices start to sound
like the teacher from Charlie Brown with some of this stuff.
But essentially, what has happened
is we have industrial agricultural industries.
Now, Florida happens to be very big with sugar,
but there's other ones.
It's not only big sugar.
And essentially, what has happened
is south of Lake Okeechobee, where
a lot of these agricultural plants are,
is their industrial runoff is causing
this exacerbated bloom of this toxic blue-green algae
in our water.
Now, the blue-green algae is more of an inland water
problem, whereas red tide is much more of a coastal problem.
And both of those things would be naturally occurring,
but they would not be exacerbated
at the rate they are.
So if you were to look at a map of Florida,
where Lake Okeechobee goes, and you
were to look south of Lake Okeechobee,
that used to all be Everglades.
And what would naturally happen with any sort of runoff
is the Everglades would actually clean it.
The Everglades act as a filter.
It's a natural filter for our water
that we do use as our drinking water.
But what happens now is companies
that are big sugar companies, they
own that land that's south of Lake Okeechobee,
and they do not let it do what it's meant to naturally do.
And instead, it's being blocked, and our water is not
getting naturally filtered.
And then I know that they're getting subsidies.
So in other words, in order, I try to explain,
we need to deal with this like they're children.
So we need to incentivize good behavior,
and we need to disincentivize the bad behavior.
And instead, what we're doing is by giving sugar subsidies,
and I'm sure many other of our industrial polluters
are given subsidies.
It's not just sugar.
That's just what I'm familiar with.
So we're giving them subsidies.
So we're essentially encouraging them
to not allow us to use the Everglades
for natural filtration, and to not
reduce their level of contamination
and runoff into our inland waters.
And instead, we're promoting it.
So wouldn't we want to be maybe taxing the companies
at a higher rate for the amount of pollution,
as opposed to encouraging it?
I would think so, but I'm not somebody
taking money from Big Sugar.
So I think that the appearance of impropriety
with my particular congressperson
and this particular issue is very, very huge.
Up here, when I was younger, we'd
get red-tied like maybe once every three or four years.
And it wouldn't last long, and it wouldn't be a big deal.
It's every year now.
And they end up, a lot of times, end up closing the beaches
or throwing the biological warning sign flags up
and stuff like that.
It's a pain.
It really is.
OK, so what's your ideology?
Are you a social Democrat?
Are you a Democratic Socialist?
Are you, where are you at on that spectrum?
Because I see a lot of parallels with some other upstart people
in the House of Representatives.
Yeah, the label thing is very interesting to me
because I never really understood
why Bernie calls himself a Democratic Socialist,
because that doesn't seem to me to be.
He is a social Democrat, and I actually
wish he had said that from the beginning
and instead giving all this sort of ammunition
to the right to use the term socialism,
like it's some horrible, scary thing.
You know what, I don't necessarily have one particular ism.
And what I would like to, as far as I'm concerned,
it's about justice.
It's about social justice, economic justice,
and environmental justice.
And more than anything, it's about punching up.
So I would say that the key point of my campaign
and what distinguishes us is that we see it as actually
as a term of service and not a career.
So I have no career motives.
I have no career ambition in this whatsoever.
This is something that I want to do as a service.
I want to get the money out.
I want to represent regular people.
I believe that certain industries, such as,
for example, health care, public education, and corrections,
should never be for profit.
There should never be a profit motive
in those particular industries.
However, with regards to, let's say you invent a widget
and you want to rip people off and make millions of dollars
by selling your widget, I'm all about capitalism.
If you want to do that and you can get very wealthy
properly treating your employees
and not polluting the environment and paying your taxes,
I'm all for it.
So I wouldn't say that I'm not in favor of capitalism,
but I just don't think that the people
that are getting that wealthy could get that wealthy
without stealing that money.
So I think that if I had met a billionaire
that I truly believe earned that fairly,
I wouldn't have a problem with that.
I just don't think it's possible.
Yeah, I'll be honest.
I know one very, very extremely wealthy person
that got it honestly,
and he will tell you more than anything, it was luck.
He bought a bunch of land that wound up
a resort up here, wanted to buy it.
And he went from nothing to having hundreds of millions
like overnight, but he'll be the first one to tell you,
it was luck.
Yeah, it wasn't hard work.
I will never accept that people like Jeff Bezos
work harder than my husband or my dad did
or anybody else who just is a working person.
So there is no, and in fact,
some of the hardest working people
are some of the poorest people in this country.
So to say that there's somehow a meritocracy
or somehow they've earned that money,
nobody earns a billion dollars like that.
Nobody works that hard.
Okay, so let's get to breaking down on specific issues.
Here in Florida, one that's definitely
gonna be coming up, guns.
Where are you at?
That's funny because the past few days,
I've actually had a couple people,
and of course, these are people in the community
where I'm in that have been very affected
by not just Stoneman Douglas,
but we are talking about the wealthy, educated,
suburban sort of gun fear factor
is what it really seems like to me.
And I have a couple of thoughts on it.
I mean, I support any and all federal regulations,
background checks, registries.
I think people should have to take courses
to be able to own firearms.
I have no problem with any of that.
But interestingly, thanks to you,
I am no longer a supporter of banning assault weapons,
even though I will say in my state,
I had prior to watching your series,
I had signed the petition for the amendment
to ban assault weapons.
I don't know that it'll necessarily go anywhere.
And my thought on that is,
it's a bandaid that is gonna make a whole group
of people feel better,
but not actually accomplish anything.
And I think at the same time,
well, banning things in general
has just never really been the way to go.
But I do a lot of work in the black community
and in more disenfranchised communities,
and banning assault weapons
will really not do anything at all to help them
with the issues that they have with gun violence.
So when people in the disenfranchised communities
see an event like Parkland,
and it gets everybody all up in arms and freaking out,
and we gotta do this, and we gotta do that,
and ban this and do that,
a lot of the people in the communities that I work with
feel that that is a very white privileged somewhat mentality
because we've never cared so much
about the gun violence in their neighborhoods.
And we don't really care about really handling
the basis of what our problem is.
So my thoughts on gun,
and I am very familiar with a well-regulated militia,
and it isn't a second amendment issue,
it's a sick society issue.
But in general, I believe in any regulations
to regulate who can have weapons as best as we can,
but banning, I just don't think it'll work.
So that's my opinion on the second amendment.
All right, Medicare for all.
I am a strong proponent of single payer.
I do not think we should have any profit motive
in healthcare for any essential services.
I don't think we should have insurance companies
providing duplicative services.
I think as soon as we allow that, we have a two-tiered system
which will inevitably go to hurt
the most disenfranchised people.
So to me, the only way to safeguard healthcare
as a human right is for me to have the same insurance
as my congressperson, who is my employee,
who has amazingly good health coverage, interestingly.
And if we all have the same coverage,
we will all have the same vested interest
in protecting the quality of our care.
So I don't like the idea of a two-tiered system.
I don't like the idea of a public option.
I think those will not give a real,
we won't get an idea as to how well single payer can work
so long as we're allowing any profit motive in our system.
Okay, all right.
What's another big one here in Florida?
The school voucher thing.
Imagine you have some opinions on that.
I do, I do have opinions on that.
Like I was saying, I mean, public education
is like healthcare in that it should not
have a profit motive.
And I don't necessarily have a problem
with the concept of some charter schools.
We have a couple down here that are non-profit,
that they actually go through the municipalities
and they function very much just like any public school.
And as a result, they are very good charter schools.
So I don't necessarily have a problem
with more of a privatization
if there's not a profit motive in it
and it is still a public service.
So it really depends on the nature.
But as far as vouchers, that is just another way
for people to siphon the money out of our public services
and give it to private industry.
So that is nothing that I would support.
And all it's doing is lowering the quality
of our public education.
Because even with a voucher,
most of the people couldn't afford to go
to a private school with that amount of money anyway.
So you're just, it's another band-aid.
It's another thing that makes people
like they have a choice.
They like this idea of having a choice.
Well, I could choose to spend my money here or there.
Yeah, you have a choice, but you're lowering
the quality of the choice for everybody else.
So I don't think that choice is good
if the choices are not quality choices.
But ultimately, healthcare and education specifically
are to me, they're not costs.
Those are investments.
And for us to compare that to something like,
let's say war cost, makes no sense.
Healthcare and education are in the best interest
of everybody in this country.
Nobody is better off with uneducated,
sick people amongst us.
So it doesn't even make sense to call it a cost.
It's an investment.
And I don't think we have even begun
to realize the payoff of those investments.
Certainly not with healthcare,
because we don't have universal healthcare.
But even with public education in this country,
it's always been woefully underfunded.
It's never been, teachers are not properly compensated.
We devalue the quality of education,
and we devalue the nature of the people
that provide that education.
So that only hurts us all.
I, you know, it's amazing to me.
I don't see how anybody would begrudge investing
in educating our citizenry.
I just don't understand that at all.
No, I definitely feel you.
Let's see.
I mean, it's Florida, immigration's not really,
now let's ask about immigration.
Where are you at on that?
Personally, I think ICE is an unnecessary organization.
I think it's superfluous.
I think we don't need it.
I think we did fine patrolling our borders
and keeping ourselves safe from an immigration standpoint.
We did fine prior to ICE.
And to me, the way that it is being used,
now that could be just how the people at the top
are training and implementing,
and it might not be just the nature of ICE,
but to me, it seems like almost a fascist Gestapo
just basically with the sole purpose of hurting people.
I don't see them as protecting and serving.
I don't see the people that they're going after
as being necessarily dangerous to society.
And the mannerism in which they are going about
doing their job is a level of brutality
that should just not exist in this country.
There's just no place for treating people
that way in this country to me.
Yeah, okay.
That's kind of what I expected.
It's Florida, we're like, yeah, we really don't care.
I mean, like-
No, we on the left certainly do not care.
A lot of the right and a lot of the Trump supporters,
I actually do believe that that is one of their issues
that they feel strong about in terms of
keeping America first,
making sure that we can take care of our own.
Ironically, they don't really wanna do that either,
but there is this very strong sentiment
that we've all been feeling in the past few years
that's very anti-other,
whether it's Muslims, immigrants,
poor brown people from Mexico
or wherever we think they're coming from.
And this is the key thing that I implore to everybody.
The source of your issues, the source of your problems
is never from the poor disenfranchised people
that are below you socioeconomically.
That is never the source of the issue.
The source of the issue is up and never down.
So when we're talking about any sort of maltreatment
or not even welcoming immigrants to this country,
it's abhorrent to me.
I find it sickening and it makes my skin crawl.
And not only that, but factually speaking,
and from a criminal justice perspective,
the immigrants are infinitely less likely
to be perpetrators of crime in this country
than the people that are from here.
So it's not even based on any sort of reasonable basis.
There's no basis other than fear factor.
And so I don't usually ever get behind anything
that isn't fact and reason based
and having any sort of dislike or mistrust of immigrants
is completely irrational.
Well, I'm up here in the very, very red part of Florida.
And as far as the rhetoric, they're like,
yeah, build the wall or whatever.
But when you actually talk to them
about anybody they've ever had contact with,
they're like, oh, no, no, no, not that.
That doesn't count, not for them.
One of those things they bought into the rhetoric
because the person saying it has an R behind their name,
but they don't believe it themselves.
And they know they don't believe it
when it's placed in front of them.
But anyway.
This is where the masses are so much more infinitely dangerous
when they just follow some sort of directive
that makes absolute no logical sense whatsoever.
But essentially, we know this.
We've seen this forever.
You take a bunch of people that feel like their luck is down
and they don't have what they need and you blame them
or you blame others for their lot in life
and you blame the poor, you blame the disenfranchised,
you blame minorities.
And it alleviates the top from anybody looking at them
as to the source of the problem.
And it's just, it will never,
barring immigrants and disenfranchising poor people
and people of different ethnicities
and people that are disenfranchised,
hurting those people is never going to help us.
It just isn't.
There is no logical connection
between keeping poor people disenfranchised
and keeping us healthy and safe.
There's no connection between those things.
And speaking of those people at the top,
for those outside of Florida,
her opponent here is extremely connected.
So I'm very curious, what's the plan?
How are you gonna go after her?
Well, in some respects,
I do have a certain amount of concern,
especially watching the past few days
that where no matter what you do,
the machine and the establishment
is just churning twice as hard against you.
And I get that.
And to some extent,
that is what has kept things the way they are
for so many years.
But like I said, I feel somewhat hopeful
that we have seen some non-corporatists
be able to slide through.
Now it's more of a slow trickle,
but, and it won't happen quickly.
It took us, you know, how many years to get into this mess.
It's not gonna happen in two election cycles
to get out of it.
But the reality is when I talk to people,
and that's the key thing is talking to people.
I'm on the ground canvassing every single day.
I'm knocking on doors that probably my opponent
has never even seen.
I'm going into neighborhoods that she would never go in
and doesn't know anything about.
And when you go and talk to actual people,
for the most part,
they are just so appreciative and thankful
that someone's even coming to talk to them,
that it doesn't even matter
if I could promise the world to these people.
The fact that I care enough to just come in and talk
and find out what's going on here,
what can I do to help you?
So the first thing is just talking to people.
But the cool thing that we've done
is I've created an organization called GenCorp.
And GenCorp, like Marine Corps, but GenCorp.
And GenCorp is what I call our volunteers.
And we are out doing community service
every day throughout the district.
I am getting connected with nonprofits, municipalities.
I have people going to meetings saying,
how can we help you?
And the idea, like I said,
is if congressional representation is a term of service,
then we need to just be doing the service.
So basically, win or not, we're just doing the job.
We're out there showing this congressional district
what it's supposed to look like.
Having a representative
that actually is present in your district,
knows what's going on, cares about you,
and is working to find solutions.
And not everything has a solution.
Not everybody can be helped.
But it is amazing how people respond
when you tell them, yeah, we're here just to serve you.
I'm actually here to find out what can we do to help you.
So whether it's cleaning beaches, we do parks,
we do the mobile school pantry,
we work at the veterans nursing home.
Basically, everywhere I can find a link
to where we can be sending somebody to serve,
that's what I've been doing.
And I've been doing it now for a little less than a year.
And it's amazing.
So I feel, to me, honestly,
regardless of what happens with this election,
I will keep this going.
I honestly don't know why she doesn't have
her own little group out there serving the community.
It's really not hard to do.
It's actually quite easy.
It's not.
It's actually quite easy.
So I think the combination of just being present
in these communities, reaching neighbors and community people
that have never really come out to vote,
that's a big issue, is we have,
mostly everybody in Broward that could be registered
is registered.
We have less than 1% of our people that could be registered
but choose to not be registered.
So our issue isn't registering voters.
It's getting them to come vote
and giving them a reason to vote.
And of course, then educating them on closed primaries
and being registered a certain way
in order to participate and all that.
But really, I've been reaching into communities
that have been otherwise unengaged.
And ironically, those are the people
that need the most help.
So, yeah, that's how that normally works out.
Those that are forgotten, as far as representation,
they end up in the worst.
Or voting against their own self-interest.
And a lot of the ways that that happens,
specifically down here in the Black community down here,
and it's worked for Debbie for very many years,
is essentially you pay pastors and church leaders
and you make a donation to their congregation
and essentially they get their flock to support you.
And instead of going about it that way,
I'm going into these neighborhoods
and I'm meeting the flock
and I'm finding out how I can help them.
So I just have a completely different paradigm
as to how to do this.
And it may work and it may not.
But I do find that it resonates
and I do feel better about what I'm...
If I spent this whole year just talking about myself
and then I lost, then I wasted a year of my life.
If I spend this whole year doing community service
and I lose, then I've spent this whole year
doing community service.
So that's more of just sort of helping me get through this
than anything.
It's somewhat self-serving in that way,
but it's been working, I think.
Well, the other thing is,
you build an organization like that, it will carry on.
Even if it's not under you, it will carry on.
So even if you lose, you win.
Exactly.
You build a community strong enough
and it doesn't matter what the representatives do.
All right, so is there anything else you wanna touch on?
Well, I mean, we can always make our pitch
that we need financial help as much as we don't take...
We won't take corporate money
and I don't take corporate PAC money and I can't be bought.
So when you're like that, it's not so easy to raise money
because the people that are supporting us
are the people that don't have a lot of money.
So we really are very much funded
on small monthly donations.
I think my average donation is somewhere like $23,
actually, which is fine,
because we ask for 23 for District 23
and my number keeps coming down
as I get more and more contributors.
But yeah, I mean, we need help.
We're fighting a corporate monolith
and we need people to go to gen2020.com
and donate as much as they can
or even as little as they can possibly.
Every little bit is a message to the machine
that we do not answer to them.
And I also wanna make it very clear
that even though I'm running as a Democrat,
because it really is the only way to do that
in Florida in a gerrymandered district,
I have no love for that organization or that party.
I do not give them money.
And in fact, when events come up down here
and people ask my campaign, well, do you wanna do this?
I'm like, no, I'm not gonna give them money.
I've got people that are hardworking people
giving me money from all over the country
to stand up to this establishment.
Why would I give money to the establishment?
So I do the best I can to sort of toe the line
within the party to be able to do this in a primary.
But I get a lot of support
from no party affiliates and Republicans
because they know they're stuck
with a Democrat in this district.
They know that.
They're not getting a Republican in this district.
So they then have an option
to have someone that can't be bought.
And I think that appeals across the political spectrum.
Right, well, I think you'd probably find a lot of people
who would be more at ease with somebody
who's running from their ideological standpoint
what they believe rather than who can donate the most.
So yeah, definitely make sure you,
before we wrap out, you give up all the information
for the websites again.
Is there, are there any issues specific to Broward
that you wanna talk about?
Yeah, I could do that.
I mean, I don't know that it's so specific to Broward
but it's definitely huge here
is the obvious wealth inequality
and lack of affordable housing.
Broward has one of the worst,
I mean, statistically, as far as affordable housing,
it's really, really bad here.
And I don't know exactly what the numbers are
but I know that we're in like the top handful
of urban areas where people just cannot afford to live.
And yet there are so many wealthy people here.
So it's an inequality.
But interestingly, I will be talking at a rally tomorrow
for the Fight for 15 folks at a McDonald's
that are still making, I think eight and a quarter,
I think they're at eight and a quarter an hour.
And that's just obscene.
It is just, so the concept of anybody working
a full-time job should be able to live with dignity
isn't radical.
It's just not radical.
And I think the more that I can communicate this
to people in terms of, you know,
when someone like Joe Biden says,
oh, give me a break.
When I was young, I had, you just go get a job.
Just stop complaining.
Well, I wanna say, well, yeah, Joe,
but when you were that age,
somebody could work at minimum wage and still live.
So yeah, I get that.
And so to me, if we would raise the minimum wage
to a living wage and provide healthcare
to people simultaneously,
we wouldn't have to be obsessed with this concept
of affordable housing,
because people would just be able to afford to live.
So, you know, it all needs to happen simultaneously,
but down here, we definitely have
an affordable housing crisis.
So yeah, the municipalities need to step in
and we need to build affordable housing,
but I still think overall the best approach
would be to just pay people a living wage.
So it would, then their housing would be affordable.
So that's definitely a big issue down here.
I don't know what you see up where you are.
Oh, we have, like along the beach here,
you have like just an insane amount of tourism
and it's year round, it never stops.
It slows a little in the winter, but not much.
So even like the McDonald's and stuff like that here,
they're paying 10, 11 bucks an hour.
And if they, people live North of the Bay,
they have affordable housing.
Like here, because of the way it's kind of self segregated
between the wealthy and the not wealthy,
it's basically driving over the bridge
and then everything becomes affordable
because you're making three or $4 more an hour.
We don't have that here.
Yeah, I mean, that's like when you said 825 or 850,
I was like, wow, like that was what they were,
when I was in high school, that's what was being paid.
And these are, and the thing is,
is they, some people in that sort of mentality dismiss it
like, oh, it's teenagers.
No, most of the people working in McDonald's are adults
and trying to have a normal income and feed their families.
So, you know, I just have this crazy idea
that people shouldn't have to work three jobs
and drive an Uber to be able to live.
So, you know, if you're working a solid work week,
you should be able to afford to live.
This shouldn't be a thing.
And 825 is not cutting it, not in the Fort Lauderdale area.
And so it's really, I've met people in the past year
that they barely know how they're living themselves.
People living in such squalor,
people living in conditions that are just not,
it's just not acceptable.
It's just not acceptable.
So I'm gonna do everything I can
to make that better for those people.
And yeah, a living wage would be a good start.
That would be a good start.
And honestly, $15 an hour
still really isn't a living wage down here.
I believe it would be about 23.
From the last numbers I heard,
if it had kept rising with cost of living,
I believe we'd be at about 23 in my metro area.
So when they're sitting there fighting for 15
and nobody's even willing to get,
it's talk about just begging for scraps.
And yeah.
Well, yeah, that was a surprising number
when you said that,
because it's, and I forget the way we're set up here.
And it's just the way it was developed.
But-
Well, supply and demand.
Yeah, I mean, we don't,
if we're one of the few areas,
where I live is one of the few areas
where if you're making 24 a year,
you're completely, you can be comfortable.
If you live here.
If you live south of the bay,
you're not getting anywhere.
Right.
And if you live in Broward,
you're probably not even in housing.
Right.
Okay, so one question I ask everybody
is what can the people watching this,
people at home,
what's one thing they can do in their own communities
to change the world, save everything?
Okay, well, I would recommend doing
what we're doing with my organization.
Go to community association meetings,
go to homeowner association meetings,
go to civic organizations,
go to municipalities and listen
and find out where there's need to help.
There is no shortage of need.
And a lot of people are very familiar with organizations.
You just need to reach out and say,
how can I help you?
It's amazing what we can do.
And the more people that you reach out,
the more connections that you make,
the more they connect to each other.
I have bridged so many organizations together
in the past year that it's almost like
I feel like I'm putting a puzzle together
and it's just really cool to see.
And it feels good.
And so I would just encourage people,
find the need, there's no shortage.
Even if it's going to the library
and just doing a little bit of research
and saying like, what's going on in this area
and how can I help?
I mean, we have a lot of environmental issues here,
but some places it's,
maybe it is a huge homeless problem
specifically in your area.
And the best way to do is maybe have a fundraiser
or a clothing drive.
A lot of people are in need of toiletries.
That's a huge thing among a lot of the needy people
is things like toiletries, feminine hygiene products,
things that we take for granted that are actually costly.
And a lot of people just cannot afford.
So something like high school students can easily do
is have a drive for used clothes
or bring in the little samples of stuff
that you steal from hotels
and do a mass collection and distribution.
There's so many people that need.
So, or go to a beach cleanup.
There's no shortage of beaches that need to be cleaned.
All right.
Okay, give everybody your information again,
your Twitter, Facebook, and your website.
Sure, so our website is Jen, J-E-N, 2020.com.
And Facebook is Jen Perlman.
Wait a minute, what is that?
No, no, no, that's not even my Facebook.
Hold on, Jen Perlman for Congress,
or is it just Jen 2020?
Jen 2020 is Facebook.
And then Instagram and Twitter are JenFL23.
So that's the two that are different.
And this is where my son tells me I'm old
because I don't understand how the Twitter
and the Instagram work, but it's JenFL23.
And yeah, we just need the love.
We need the support.
We need the word spread and we need money.
All right, all right, everybody.
So that's gonna wrap up the show for today.
And I guess it's just a thought and we will talk to you later.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}