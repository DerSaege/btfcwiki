---
title: Let's talk about a new library of censored material....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tumjxowQ_qM) |
| Published | 2020/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a new library in Minecraft that holds information not meant to be shared, created by Reporters Without Borders and BlockWorks.
- Describes Minecraft as a video game similar to digital Legos.
- Mentions that the library contains articles and targets individuals in countries with restricted access to independent journalism.
- Talks about the striking decor inside the libraries, like a cemetery in the Mexico section with tombstones of killed indie journalists.
- Provides the server IP visit.uncensoredlibrary.com for multiplayer access and encourages downloading the archive for hosting elsewhere.
- Emphasizes the significance of the project in ensuring information reaches its intended audience.
- Stresses the importance of access to accurate information during the formation of ideas and values.
- Reminds viewers of the various ongoing fights for information freedom worldwide.
- Asserts that censorship is ultimately futile, as information always finds a way to be shared.
- Encourages reflection on the broader implications beyond current headline news.

### Quotes

- "Information will always find a way to get to where it needs to be."
- "Who's going to see this the most? People."
- "There are countries that have much more severe problems in that regard."
- "It's not going to work. It has never worked."
- "There will always be somebody willing to get that information out in some fashion."

### Oneliner

A Minecraft library defies censorship, ensuring information reaches those who need it most, showing that information always finds a way.

### Audience

Gamers, activists, information seekers.

### On-the-ground actions from transcript

- Visit visit.uncensoredlibrary.com to access the Minecraft library (suggested).
- Download the entire archive to host it in another location (suggested).

### Whats missing in summary

The full transcript provides a deeper insight into the innovative ways information can be shared and the importance of combating censorship on a global scale.

### Tags

#Minecraft #Censorship #InformationFreedom #ReportersWithoutBorders #BlockWorks


## Transcript
Well, howdy there, internet boobalidsbo again.
So tonight we're going to talk about a unique way information
is getting out and a brand new library.
Only been open a few days, and it
is unique in the sense that it only
holds information that's not supposed to get out.
It's cool.
It's in Minecraft.
Minecraft is a video game, by the way, if you don't know.
Picture it like digital Legos, kind of.
Not really, but kind of.
And Reporters Without Borders teamed up
with a group called BlockWorks, which apparently
makes very striking architecture inside this game.
And they created a library.
And it's not just a visual representation of a library.
There are articles in it.
And the target audience are people in countries that censor access to independent journalism.
The decor inside the libraries are striking.
The one for Mexico is the center of it is a cemetery.
And the tombstones have the names of indie journalists who were killed down there.
Saudi Arabia is another country, there's a few of them.
If you want to get to it and access it in the multiplayer mode, you go to visit.uncensoredlibrary.com.
That's the server IP.
And if you want, because we don't know how long this little loophole will last, you can
download the entire archive to host it in some other location.
The cool thing about this is not just demonstrating that information will always find a way to
get to where it needs to be.
The cool thing to me is that it's target audience.
Who's going to see this the most?
people. So much of how we think and how we act is based on the information we
process when we're younger, when we're forming our ideas. I think it's an
incredibly worthy project. I know right now we're all focused on, you know, the
main headline. But it's important to remember that even as that fight goes on, there are
a lot of other fights that are going on all around the world. And understand as we struggle
with getting accurate information from our government, from our administration, there
There are countries that have much more severe problems in that regard.
So if there's one thing that Reporters Without Borders just taught countries that engage
in censorship is that it's not going to work.
It is not going to work.
It has never worked.
There will always be somebody willing to get that information out in some fashion.
Anyway, it's just a thought. y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}