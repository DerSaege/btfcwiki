---
title: Let's talk about what two movies can teach us about foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5YD3Twx6PlU) |
| Published | 2020/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Foreign policy is more like interpersonal relationships, impacting everyone involved in the room.
- In Jurassic Park, Jeff Goldblum's character's actions led to unnecessary consequences.
- Once a decision is made in foreign policy, there will be consequences that cannot be easily undone.
- In the movie Signs, sometimes there is a way to slowly pull out without causing destruction.
- Hasty withdrawals in foreign policy can cost innocent lives and have dire consequences.
- There are ways to withdraw from situations like Afghanistan and Syria without abandoning those affected.
- Training local groups and providing resources can help stabilize regions like Afghanistan and Syria.
- Leaving behind resources like helicopters and surveillance drones can give indigenous forces a fighting chance.
- It is immoral to leave people in conflict zones without support after involving them in the first place.
- Moral decisions in foreign policy should not be solely based on governmental actions but on what is right.

### Quotes

- "It's not good versus evil. That's not how it works."
- "Hasty withdrawals cost lives and it's not combatants, it's innocents, it's civilians."
- "It is immoral to leave these people twisting in the wind after we plunged them into it."

### Oneliner

Foreign policy mirrors interpersonal relationships, with irreversible consequences necessitating careful withdrawals to prevent unnecessary bloodshed and immorality.

### Audience

Foreign Policy Analysts

### On-the-ground actions from transcript

- Provide resources and training to local groups in conflict zones (suggested).
- Leave behind resources like helicopters and surveillance drones for indigenous forces (implied).
- Avoid hasty withdrawals that can cost innocent lives (implied).

### Whats missing in summary

The full transcript provides a detailed analysis on the intricacies of foreign policy decisions, the importance of careful withdrawals, and the moral obligations towards those affected by conflicts.

### Tags

#ForeignPolicy #WithdrawalStrategies #MoralObligations #ConflictZones #ResourceAllocation


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna talk about what two movies
can teach us about unringing bells and foreign policy.
There's a very common desire to apply morals and ideology
to foreign policy decisions.
And it sounds great,
But that's not what foreign policy is.
Foreign policy is more like interpersonal relationships.
There's a couple hundred people in a room.
They all know each other, and they're all talking.
Every conversation impacts the way all of the other people
in the room look at that person.
That's what foreign policy is more like.
It's not good versus evil.
That's not how it works.
But in Jurassic Park, the very first one,
there's that scene, the famous scene with the two cars,
the kids are in the first one, and the T-Rex,
where the eye dilates.
It was a big thing back in the day.
Jeff Goldblum's character gets out
of that second vehicle with that flare.
He never should have done that.
That was stupid.
He should not have done that.
Had he not done that, odds are things would be better.
He didn't need to get involved, but he did.
Now once he did that and he threw the flare and kept running and the T-Rex was chasing
him, what would have happened if he had just ran back to the car?
Unnecessary bloodshed, right?
Yeah.
It's not a good idea.
Foreign policy works the same way.
Once a decision is made and it's been acted on, you can't just go back.
will be consequences.
Then there's the movie signs.
At the beginning of the movie, Mel Gibson's wife, I guess she's walking down the road
and boom, the car, right?
That horrible scene.
She's pinned against the fence.
The car never should have been there.
Never should have been there.
But it was all that was holding her together.
Unlike those movies, unlike that car, sometimes there is a way to slowly pull out without
destroying everything.
A lot of people questioned my stance on the peace deal, luckily we don't have to worry
about that because that was the shortest lived surrender I've ever seen in my life.
But it's still a good jumping off point.
Withdrawing from Afghanistan, just cutting and running, and not helping the national
government guarantees that the opposition takes back over.
You can't say, well, we just need to leave, because what you're really saying is we just
need to allow all of this horrible stuff to happen.
That's what you're saying.
We can't just cut and run unless you are comfortable with what that opposition group is going to
do to the people that live in that country, especially those that helped the United States.
Same thing applies in Syria.
We went in there, never should have been there, never should have been there.
But once we were there, we were stuck.
And people called it at the time, you know, like we can't really leave now.
Yeah, that's true.
Because you create that power vacuum.
And that space is going to get filled by somebody.
By one of the other 200 people in the room.
Somebody's going to walk over and fill that space.
And odds are, they won't be a good country, a good person.
And other people get to see you pick a fight for your buddy and then leave.
Hasty withdrawals cost lives and it's not combatants, it's innocents, it's civilians.
There's people that had nothing to do with it.
There are numerous ways to get out of these situations that don't amount to just condoning,
silently saying, yeah, fine, forget all these people.
We know what's going to happen, but we're going to look the other way.
There's a bunch of ways that we could leave Afghanistan and not do that.
bunch of ways we could leave Syria and not do that. It's completely possible. We
have entire units within the military whose their sole mission in life is to
train local groups. That's what they do. With Afghanistan, yeah we're gonna need
to give them helicopters and our old surveillance drones. They're not any good
anymore now anyway. Yeah, but it can be done. It would probably take two years. We
could pull out our combat forces, leave a carrier group offshore for air support,
and allow the special forces to do their job, give them the resources they need,
and allow them to train the the indigenous forces. That's all that needs
to happen. We give them the helicopters, we send maybe pilots from the 160th to
help train local Afghan pilots, get them up to speed so they can have the
mobility they need. We can't just leave them to be done away with. I think that
we owe them, after plunging them into this, we owe it to them to leave them with at least
a fighting chance.
And Syria's very, very similar.
Syria's harder because there are more people in that conversation, and there are more people
trying to get something out of that country, but again that was a situation
that had the administration just stayed the course, it would have stabilized on
its own we can't allow ideology to make these decisions and we can't allow the
morality that should have been in place 10 years ago to affect the decision today
because what was moral for us to do in 2006 that's not the moral thing to do
anymore and the other thing that I've seen a lot of people say is that well
you know even if we did do that the government wouldn't be doing that to
help them they would have their own reasons yeah that's probably true that's
That's probably true, but I would suggest that it is a really, really bad idea to base
your morality and what you support off the government's morality and allow them to make
decisions and those decisions influence what you think is right and wrong.
It is immoral to leave these people twisting in the wind after we plunged them into it.
We can do it without staying there forever.
We can do it without selling people out.
We can do it without costing tens of thousands, if not hundreds of thousands of innocent lives.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}