---
title: Let's talk about how the 1.5 trillion-dollar F-35 is already obsolete....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UrWv7xDl_SY) |
| Published | 2020/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how institutions resist change, even when it's necessary, due to institutional traditions.
- Mentions how the military brings in outside experts for fresh perspectives, though sometimes these insights are not readily accepted.
- Recalls the first use of airships by the military in 1912, leading to an obsession with air superiority around the world.
- Criticizes the spending of $1.5 trillion on the F-35 fighter jets, which are considered obsolete even before being fully implemented.
- Elon Musk's suggestion to shift towards drone technology for military aircraft is supported, as it eliminates the need for human pilots and reduces risks.
- Raises concerns about the consequences of using drones in warfare, particularly the potential for increased civilian casualties due to reduced aversion to risk.
- Points out Turkey's use of drones in a recent campaign as evidence supporting Musk's stance on transitioning to drone technology.
- Emphasizes the wastefulness of funding obsolete military projects while claiming a lack of resources for other critical needs.

### Quotes

- "The age of the fighter pilot, much like the age of the airship, is over."
- "If we're not risking any human assets to accomplish something, we will probably do it more often because there's no risk."
- "If it's just machines, nobody's going to care."
- "We are wasting $1.5 trillion because it wasn't thought through."
- "We're worried about creating the next generation fighter. We shouldn't."

### Oneliner

Beau delves into the resistance of institutions to change, critiquing the military's reluctance to embrace drone technology despite its potential benefits and cost-effectiveness.

### Audience

Military decision-makers

### On-the-ground actions from transcript

- Advocate for the adoption of drone technology in military operations (exemplified)
- Question and scrutinize military spending on potentially obsolete projects (exemplified)

### Whats missing in summary

The full transcript delves deeper into the implications of technological advancements in military operations and the ethical considerations surrounding warfare conducted through unmanned drones.

### Tags

#Military #InstitutionalTraditions #DroneTechnology #MilitarySpending #EthicalConcerns


## Transcript
Well howdy there internet people, it's Bo again.
Got a break in the rain, a little bit anyway.
So we're going to talk about tradition, institutional traditions, and how sometimes things can change
and institutions don't want them to.
So they just don't accept that things have changed.
The military will often bring in outside experts, civilians, people that have nothing to do
with the military, to give them a fresh perspective on things, to give them insight that they
don't have because they're in a bubble.
They're in an institutional bubble.
Sometimes these conversations aren't even asked for, they just happen kind of offhand.
That recently occurred with Elon Musk.
He said something and he's right.
He's right.
But the military hasn't accepted that he's correct yet.
It's interesting that we're talking about this today because today in 1912 was the first
time that airships were used, by the military anyway.
They were used for observation by the Italian, surveillance, reconnaissance.
Ever since then, militaries all over the world have been just obsessed with maintaining air
superiority.
The US, so much so, that right now we're spending $1.5 trillion more or less for the F-35 at
the cost of roughly $100 million a unit, $100 million an aircraft.
Some are a little more, some are a little less depending on what version.
They cost about $40,000 to keep in the air, $40,000 an hour to keep in the air.
And they're obsolete.
They're not even in the field yet.
They're not even fully out yet and they're obsolete.
They're done.
Because they were reluctant to change.
Elon Musk pointed out, we are pushing the boundaries of human endurance when it comes
to what aircraft are capable of.
What's the solution?
Take the human out of the aircraft, run it from the ground.
He said it, and he's right, the competitor for the F-35 should be a drone.
The age of the fighter pilot, much like the age of the airship, is over.
Even though airships are kind of making a comeback in a weird way, but aside from that,
it's over.
When you think about it, the aircraft, if you're talking about a drone, the pilot has
no fear.
In a dogfight, the pilot has no fear, less prone to error.
What happens if they're shot down?
Nothing.
They're safe in a trailer at Fort Hood.
Nothing.
They get a new plane.
It's like a video game with an extra life.
Having no fear, while that's good for the pilot, that's bad in other ways.
Why do we want air superiority?
Because eventually we want to do air to ground.
That's when it becomes worrisome.
Because if there is no fear for the pilot, there is no fear politically.
There's no potential blowback.
If we're not risking any human assets to accomplish something, we will probably do it more often
because there's no risk.
There's no political risk.
If it's just machines, nobody's going to care.
It can be done without risk of all of the things that come along with it for us.
However, when we get to the air to ground aspects of it, what happens?
It's not combatants anymore, it's non-combatants.
It's always non-combatants that pay the highest price.
If we have no fear of using it, and we are using them more often, there will be more.
And that is horrible.
And then this week, Turkey proved it was true.
They used drones in a campaign.
And it was unprecedented, was the word that I saw tossed around a lot.
It was, but it proved that Musk was right.
It proved that Musk was right.
We are wasting $1.5 trillion because it wasn't thought through.
When the government tells you that we don't have the money for something, remember this.
Many times, we're funding stuff that we don't need to be.
These aircraft are already obsolete because other nations weren't reluctant to change,
and they caught up.
We're worried about creating the next generation fighter.
We shouldn't, not in the way we're thinking of it, because it's not going to have a pilot.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}