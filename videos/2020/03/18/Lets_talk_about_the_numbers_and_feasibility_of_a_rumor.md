---
title: Let's talk about the numbers and feasibility of a rumor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N6WH7Tt-1Aw) |
| Published | 2020/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Squashing a rumor gaining ground by acting and discussing feasibility.
- Addressing the idea of martial law being declared coast to coast in the U.S.
- Explaining the specifics of martial law versus states of emergency.
- Breaking down the numbers needed for martial law and why it's practically impossible.
- Mentioning the size difference between Iraq and the U.S. in terms of troops required.
- Detailing the mobilization process for troops in the U.S. to attempt martial law.
- Clarifying that even in extreme scenarios, true martial law is unlikely in the U.S.
- Emphasizing the importance of following guidelines and not resisting authorities.
- Urging people to practice basic hygiene and social distancing measures.
- Encouraging trust in medical professionals' advice to combat the current situation.

### Quotes
- "Wash your hands. Don't touch your face. Stay at home. Practice social distancing."
- "Please stop spreading that. You can get somebody hurt."
- "I cannot envision a scenario in which true martial law is declared in the United States."

### Oneliner
Beau debunks rumors of imminent martial law in the U.S., explaining why it's practically impossible and stressing the importance of following guidelines to combat the current situation.

### Audience
General public

### On-the-ground actions from transcript
- Wash your hands. Don't touch your face. Stay at home. Practice social distancing. (implied)
- Follow the advice of medical professionals. (implied)

### Whats missing in summary
The detailed breakdown and analysis of the numbers required for martial law can be best understood by watching the full transcript.

### Tags
#MartialLaw #Rumors #Debunking #Hygiene #SocialDistancing


## Transcript
Well howdy there internet people, it's Bo again.
So today, I'm gonna squash a rumor.
Um, and apparently it's gaining ground.
So we're gonna get rid of that.
We're gonna do that by acting as if we wanted to do it.
We're gonna talk about the numbers we would need to do it
and conduct a little feasibility study to see if we could.
Um, basically what's going on is there are people who are in search of clicks and revenue
who are using the current situation to enrich themselves is what it boils down to.
To scare people.
Yeah, there's gonna be some drastic actions taken over the next few weeks in the United
States.
No doubt, it's probably gonna happen.
However, the idea that sometime in the next few days, martial law is going to be declared
coast to coast, that's not gonna happen.
That's not a thing.
If you don't want to watch the rest of the video, I will just tell you now, that's not
a thing.
It's not gonna happen.
We're gonna break down exactly why it literally can't happen.
First, martial law is a specific thing.
Martial law means direct control of civilian offices by the military.
That's what martial law is.
The National Guard showing up to help or secure something after a hurricane or in any kind
of event like that, that's not martial law.
That's a state of emergency.
It's different.
They are under civilian control, typically the governor.
Okay, but let's say we're in power and we want to enact martial law across the United
States.
Given the prevalence of firearms in the United States and veterans that are combat trained,
we have to approach this as if we are dealing with a nation we have occupied.
The most ready example, where the numbers are still fresh in everybody's mind, is Iraq.
In 2008, we had roughly 180,000 troops there.
In addition to that, there were 30,000 to 50,000 host nation troops, Iraqi national
troops.
We're gonna use the low end just because.
So we're up to 210,000 troops.
You also have the Iraqi police and the non-state actors who were very, very good at keeping
order, the allied non-state actors.
We're not gonna count them at all.
Not gonna count the cops or the non-state actors.
Those are just gonna parallel and knock out the US police, so we don't have to worry about
adding that up.
We're going to focus on the military.
210,000.
Now can the Army and the Marines fill 210,000 troops?
Absolutely.
Absolutely.
They can do that in pretty short order.
The problem is the population of Iraq at the time in 2008 was 29 million.
The United States has 320 million.
So 10 times.
So you're gonna need 10 times as many troops.
You're gonna need 2.1 million troops.
There's other factors that we'll briefly go over here later that make it even more difficult,
but right now we're just gonna stick with that.
So we want to do it, we need to come up with 2.1 million troops.
All the Army and the Marines.
Go to them.
Can they fill that?
No.
If we used everybody in the Army and everybody in the Marines, we would get to almost a third
of what we would need.
So we have to tap into the National Guard, the Army Reserves, and Marine Reserves.
Gotta call all of them up.
If we call all of them up, we're about halfway to where we need to get to.
So in comes the Navy and the Air Force.
We're mobilizing everybody.
Needs of the Air Force, needs of the Navy.
Y'all are off the boats and on the land.
We're close enough now where we could try.
And at this point, we have called up literally everyone.
Everyone that we have the ability to call up.
Brought everyone home from overseas.
Pulled sailors off of boats.
Done everything we can.
That's what it would take.
That's what it would take to get close.
Then we also have to factor in the fact that the United States is 20 times the size of
Iraq with a whole bunch of different terrain.
Makes logistics even harder.
Requires more troops.
The idea that sometime soon they are going to declare martial law coast to coast, it's
not a thing.
It is not going to happen.
Now could they do it in certain areas?
Yeah, they could.
Would they?
There's no reason to declare martial law.
They can use the National Guard if they have to enforce shelter in place or something like
that.
And it wouldn't be martial law.
It would still be under civilian authority.
So even in the scenarios where it might occur, where something that appears to be martial
law, you know, troops on the street, it's still not actually martial law.
Now the reason I want to talk about this is because the underlying theme is that people
need to get ready to fight back against this.
That's dangerous.
Because it is extremely likely that National Guard medical units show up.
The idea that people should be ready to respond to them, that's dangerous.
They're there to help.
If you want to make sure that you don't have the National Guard patrolling your streets
telling you to stay in your home, here's a crazy idea.
Wash your hands.
Don't touch your face.
Stay at home.
Practice social distancing.
Follow the suggestions.
All of these things that are becoming like orders, they were suggestions first.
I don't think it's a good idea to try to rally people to resist those who are trying to save
their lives.
Just stay home.
You're not going to have to worry about this.
But I cannot envision a scenario in which martial law, true martial law, is declared
in the United States.
I don't see it happening.
We have so many civilian mechanisms that it's unnecessary.
It's not going to happen.
Please stop spreading that.
You can get somebody hurt.
Anyway, I'm going to say it again.
Wash your hands.
Don't touch your face.
Stay at home.
Practice social distancing.
Follow the advice of the medical professionals who are trying to save your life.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}