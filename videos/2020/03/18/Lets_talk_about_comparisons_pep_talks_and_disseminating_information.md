---
title: Let's talk about comparisons, pep talks, and disseminating information....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6BQzqkYsJDc) |
| Published | 2020/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares a story from the early 2000s when running a protective detail for a high-risk client.
- Describes how a counter surveillance team alerted him about suspicious individuals conducting surveillance.
- Gives his team a pep talk after receiving photos of the suspicious individuals.
- Emphasizes the importance of disseminating information at the appropriate time.
- Criticizes the use of comparisons, like the likelihood of dying in a car accident versus gunfire, to convey calm during tense situations.
- Stresses that downplaying threats through comparisons fosters ignorance and does not help.
- Encourages meme creators to focus on spreading messages about handwashing and social distancing.
- Urges people to listen to medical professionals for guidance on staying safe during challenging times.
- Advises on practical measures like washing hands, avoiding face-touching, practicing social distancing, and staying home when possible.
- Expresses confidence in the resilience of the American people to overcome challenges efficiently.

### Quotes

- "Almost positive, bad guys, right here."
- "Meme lords, please use your powers for good instead of evil."
- "Please be part of the solution here."

### Oneliner

Beau shares a story from his protective detail work, stressing the importance of timely information dissemination and criticizing comparisons that downplay threats during challenging times, urging meme creators to focus on spreading positive messages instead.

### Audience

Meme creators, general public

### On-the-ground actions from transcript

- Wash hands for 20 seconds with soap and hot water (suggested)
- Avoid touching your face (suggested)
- Practice social distancing (suggested)
- Stay home if possible (suggested)

### Whats missing in summary

The emotional impact of receiving potentially alarming information and the necessity of heeding advice from experts during crises.

### Tags

#PepTalks #DisseminatingInformation #Comparison #TimelyAdvice #CommunityResilience


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about pep talks, comparisons, and disseminating information
at the appropriate time.
To further this discussion I'm going to tell you a story.
In the early 2000's I was running a protective detail, high risk client, somebody who was
likely to have something bad go down.
So we have a counter surveillance team.
Counter surveillance team, what they do is they're out there looking for people who are
conducting surveillance because surveillance normally foreshadows something bad going down.
One day they come to me and they got photos.
These guys, these guys right here, them, been around way too much, they're out of place,
they don't have a purpose, they're photographing, you're going to get hit.
Almost positive, bad guys, right here.
Great, just what I wanted to hear, you know.
Fantastic, because we're going out that day.
So I take the photos and I get my guys together because I got to give them that pep talk.
Got to make sure they know what's going down.
Pass the photos around, explain.
Counter surveillance guys say we're in trouble, we're going to get hit.
These are the people that are conducting the surveillance.
Pick up that MP5.
What I want you to think about when we go out there today and it goes loud, I want you
thinking about the fact that you are more likely to be killed in a car accident than
by gunfire.
No of course not, that didn't happen.
These comparisons, I understand that for some people they're coming from a place of conveying
calm, not wanting people to panic.
I get that.
However, the result of it is that people are downplaying it.
It's just fostering ignorance.
It's not helping.
Those comparisons do no good.
None.
Yeah, a lot of people die from car accidents.
Fact.
That's why we have seatbelts and airbags and traffic signals and constantly redo the way
we do our roads to cut down on accidents.
We take precautions.
We try to mitigate.
Meme lords, please use your powers for good instead of evil.
If you want to sit around and make memes, please make them about washing your hands,
the effects of social distancing, so on and so forth.
I'm sure you can make some spicy memes about that.
Please be part of the solution here.
If you want to use the car accident comparison, what I would suggest is that you point out
medical professionals giving you advice right now.
Those are the traffic signals.
Red light, yellow light, green light.
That's what they are.
They are giving you the best advice they can on how to stay safe.
They're doing their part.
You have to listen to them.
Anyway, wash your hands.
Don't touch your face.
When you wash your hands, do it for 20 seconds, soap and hot water, sing the ABC song.
Don't touch your face, practice social distancing, stay home if at all possible.
We are the United States.
The American people are very resilient.
We will get through this.
There's no doubt about that.
But let's get through it as efficiently as possible.
It's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}