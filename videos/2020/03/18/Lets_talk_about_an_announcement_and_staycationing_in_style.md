---
title: Let's talk about an announcement and staycationing in style....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3dbClP5awnI) |
| Published | 2020/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau plans to do a live stream to answer questions about survival, staycationing, and community networking.
- Suggestions on planning 30 days worth of food include physically laying it out and eating perishables first.
- Beau advises to hide comfort food and not consume it all in the first week.
- In case of a toilet paper shortage, alternatives like napkins can be used, but they should not be flushed.
- Setting up a solar shower for warm water outside can be helpful for hygiene during staycation.
- Beau recommends staying physically active during the 30 days to keep immunities up.
- Alcohol consumption is discouraged as it lowers the immune system.
- Suggestions for activities during the staycation include working on pending tasks, resume, business plan, and community network development.
- Beau encourages staying calm and assures that everything will be alright during the staycation period.

### Quotes

- "Hide your junk food."
- "Stay calm. It will be alright."
- "It's just a thought."

### Oneliner

Beau plans to live stream answering questions on survival, staycationing, and community networking, advising on food planning, hygiene, activities, and staying calm during the 30-day staycation.

### Audience

Staycationers

### On-the-ground actions from transcript

- Subscribe, ring the bell, and get notifications for Beau's live stream (suggested).
- Physically plan out 30 days worth of food and stick to the plan (implied).
- Stay physically active during the staycation period (implied).

### Whats missing in summary

Tips on mental well-being during a staycation period

### Tags

#Staycation #Survival #CommunityNetworking #FoodPlanning #Hygiene


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk a little about staycationing in style.
It appears the powers that be have decided or are leaning towards just sending everybody
on a 15, 21 or 30 day staycation to try to ride this out.
Getting a lot of questions about it.
I will tell you that tomorrow we're going to do a live stream, hopefully two, one earlier
in the day and one later at night.
And I will answer all your questions about survival, staycationing in general and community
networking.
I don't know what time yet, so I'm going to say something I've never said on this channel.
Subscribe, ring the bell, get the notifications.
I will try to give you a heads up via social media as soon as I know what time it's going
to be.
Okay so to the questions that I've already gotten.
How do you plan 30 days worth of food?
Physically plan it out.
Lay it out.
Know what you're going to eat each day and then stick to that plan.
When you start eating, eat the perishable stuff first.
Seems like it should go without saying.
Hide your junk food.
You know, that's going to be comfort food.
So don't tear through it all the first week.
Alright toilet paper.
Those people who are suffering from the great toilet paper shortage of 2020.
Yeah you can use napkins or baby wipes or paper towels or whatever, but you can't flush
it.
It will tear up your plumbing.
So seal it up in a grocery bag or have a bucket with a liner in it that closes because it's
going to smell.
Had a question from somebody who was talking about having people shower before they come
into their home.
You can set up a solar shower so you have hot-ish water, warm water outside.
If you don't have one, you can make one out of a black trash bag.
The black will absorb the heat.
This is obviously only going to work down here in the south where it's warm.
Those of you still where it's freezing, not going to be able to pull that off.
As far as meds that you need, Tylenol, acetaminophen to manage the fever and then your favorite
cold medicine.
Activities during those 30 days.
One, stay physically active.
It's a good time to do that honey-do list.
That list of stuff you have around the house that you need to get done, perfect opportunity
to do it.
You need to stay physically active because it will keep your immunities up.
On that note, unless you are completely isolated and there is nothing new coming into the environment
that you are staying in, don't drink.
It lowers your immune system.
It suppresses your immune system.
So no alcohol.
You could also work on your resume, business plan, whatever because after crisis there
is opportunity.
Aside from that you could also work on developing a plan for your community network so next
time something like this happens it won't be quite as scary.
Anyway I won't answer any other questions that you guys have.
I'm going to try to do it a couple hours in the morning and a couple hours at night.
Well not really morning, midday.
I will let you know as soon as I have the times.
Anyway we will get through this.
Stay calm.
It will be alright.
It's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}