---
title: Let's talk about the Earn It Act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7nYo7tdRAJ4) |
| Published | 2020/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Draws a parallel between the lack of privacy in the book/movie 1984 and current privacy concerns.
- Mentions a bill that allows the government to scan every message on the internet through private companies.
- Explains that the bill doesn't mention encryption but mandates scanning every message, leading to the death of encryption.
- Describes a committee headed by Attorney General Barr that will set best practices for online platforms, including scanning every message.
- Points out the bill's packaging as a measure to protect exploited kids, similar to justifying surveillance in every building.
- Raises questions about the extent of government surveillance and the justification for scanning every message.
- Warns that the bill, known as the EARN IT Act, has bipartisan support and could gain traction if not opposed.
- Encourages opposition to the bill and shares a resource from the Electronic Frontier Foundation.
- Reminds of the government's tendency to exploit crises for their benefit.
- Considers the implications of granting agencies access to every message sent, questioning if society is ready for such intrusion.

### Quotes

- "How far are you willing to take it?"
- "Do you want the agencies that have bungled this current crisis so much to have access to every single message that you send?"
- "Are you ready for Big Brother?"

### Oneliner

Beau warns about a bill allowing government surveillance through private companies, urging opposition and questioning societal readiness for extensive monitoring.

### Audience

Internet users

### On-the-ground actions from transcript

- Oppose the EARN IT Act to prevent extensive government surveillance (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of the implications of a bill allowing government surveillance through private companies, urging viewers to take action and oppose the EARN IT Act.

### Tags

#Privacy #GovernmentSurveillance #EARNITAct #OnlinePrivacy #Opposition


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about your privacy.
What was the creepiest part of the book or the movie 1984?
What was the most disturbing part?
The lack of privacy, right?
Everything was under surveillance.
Everything.
The state knew everything that you were doing.
Big Brother was watching.
It's one of those things that we act like it just can't happen.
Right now there is a bill designed to allow the government through proxies to scan every
single message on the internet.
Every single one of them.
Now the way they're trying to avoid sounding the alarm bells is the bill doesn't mention
the word encryption.
And it's not really the government doing it.
It's private companies doing it.
At the behest of the government.
The way it's set up is there will be a committee of unelected, unaccountable people headed
by Attorney General Barr who will determine best practices for online platforms.
And if they don't follow those best practices, well they lose all their legal protections.
One of the practices is scanning every message.
Encryption dies.
It's gone.
It can't exist.
There's no way to have client-side scanning or a backdoor that can't be exploited by someone
else.
It's not just a tool for good guys.
They're packaging it the same way they package any large infringement on your rights.
Well it's for the children.
Won't somebody think of the children?
It's being packaged as something to help exploited kids.
And I mean, yeah, I guess it would.
I mean that's true.
It surely would.
But it would also help them if they had a camera in every building in the United States
and the government could watch it.
That would help them too.
How far are you willing to take it?
And does that justify every message you send being scanned by the government or its proxies?
This is an incredibly intrusive step.
And it is an incredibly large step to all of those science fiction dystopias that we
read about.
The Attorney General and the United States government in general have made it very clear
they want total information awareness.
They want to know everything.
They want that surveillance state that is the plot of so many science fiction novels.
And they're taking a huge step to get it right now.
It's called the EARN IT Act.
It has bipartisan support.
Lindsey Graham is the Republican and Blumenthal.
Don't send hate mail to him, you know, based on that.
You might want to check that first.
I think it's Blumenthal on the Democratic side.
Those are the people seeking it.
With that kind of support, it's likely that this is going to get traction if people don't
start opposing it now.
The Electronic Frontier Foundation has a good article on it.
I will put that in the comments section.
With everything going on, we need to remember that the government rarely lets a crisis go
without exploiting it.
And it looks like they're going to try to do it in a number of ways.
I'll keep you up to date as much as I can.
There's been a couple comments about this.
With all the kids at home because of schools closing, it's incredibly loud outside because
there are open fields and everything near my house where they are all congregating and
playing.
So that's why I'm recording inside a lot right now.
We've got to really determine what kind of society we want.
Do you want the agencies that have bungled this current crisis so much to have access
to every single message that you send?
Are you ready for Big Brother?
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}