---
title: Let's talk about what Humpty-Dumpty and the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x3ES6ZZftb8) |
| Published | 2020/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the historical perspective he uses to analyze the economy, discussing the significance of the inverted yield curve signaling trouble.
- Notes that the entire U.S. treasury curve being under 1% is unprecedented and seeks out experts to understand its implications.
- Compares the economy to Humpty Dumpty, referencing the Great Recession and Obama's role in the recovery process.
- Attributes the economic growth during Trump's administration as a continuation of trends from before, not solely due to his policies.
- Describes Trump's policies like tariffs and attacks on social safety nets as factors contributing to a potential downturn.
- Clarifies that while a correction was inevitable, Trump exacerbated the situation, likening him to sharpening a needle that was already present.
- Mentions the trade war and OPEC Plus's recent disagreements as events affecting the economy and potentially leading to a downturn.
- Suggests that the future of the economy is uncertain, with outcomes ranging from a mild recession to a depression, depending on upcoming events.
- Acknowledges his lack of expertise in economics but provides insights based on the information gathered.
- Concludes by summarizing Obama's and Trump's impacts on the economy and anticipates a turbulent period ahead.

### Quotes

- "Trump didn't cause this downturn. He didn't pop the bubble, but he sharpened the needle."
- "Obama really didn't fix the economy, but he helped it. Trump really didn't destroy it, but he helped destroy it."
- "Trump's great economy was just a continuation of the trend."

### Oneliner

Beau breaks down the economy using Humpty Dumpty as a metaphor, explaining how historical trends and current policies are shaping a potentially tumultuous future.

### Audience

Economic analysts, concerned citizens

### On-the-ground actions from transcript

- Monitor economic developments closely and stay informed (implied)
- Prepare for potential economic challenges by managing finances prudently (implied)

### Whats missing in summary

Beau's breakdown provides historical context and policy analysis to anticipate economic turbulence, urging vigilance and preparedness for potential downturns.

### Tags

#Economy #HistoricalPerspective #Trump #Obama #Trends #PolicyImpact #Uncertainty


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about the economy.
Is it time to party like it's 1929?
Um, okay, so when we've talked about the economy before I've said this, but I'll say it again,
I'm not an economist.
That is not my area of expertise.
What I know about the economy comes from a historical perspective.
I can look at certain events and say, this happened in the past and this is what it meant
then.
That's why we talked about the inverted yield curve.
Because when that happens it normally means something bad is about to happen.
Okay, so today, somebody sent me something, the entire U.S. treasury curve is under 1%.
You know what that means from a historic perspective?
You don't and nobody else does because it's never happened.
That has not occurred before.
So I had to call people that actually understand the inner workings rather than just being
able to know that history rhymes.
So after talking to a couple of different people and digesting what they told me, I'm
going to tell you what I kind of took away from it.
Our economy is humpty dumpty.
To understand what's happening now, we have to go back to the great recession.
Humpty dumpty fell and he was put back together.
And in walks President Obama and picks him up and puts him back on the wall.
No.
No.
He climbed up a ladder and Obama installed two of the steps.
Let's say there's ten.
Obama helped with the recovery.
He didn't really cause it.
He assisted.
So when Trump takes over, Humpty is still climbing up that ladder.
And this is cool, you should do this anyway.
If you look at economic charts that go back to 2008, you'll see where the economy started
to rebound, it started to go up.
Trump's great economy, everything that he's been taking credit for, is just a continuation
of the trend that it was on.
Had nothing to do with him.
Okay, so he starts enacting his own policies.
Humpty is now on the wall.
When you enact an economic policy, it doesn't have impacts right away.
It takes a while.
So the tariffs, the trade war, the attacks on social safety nets, all that stuff, it
took a while to take effect.
So the trade war, that's like him smearing butter all over the top of the wall and Humpty
Dumpty is up there sliding around on it.
The attacks on social safety nets, that's like him putting a frying pan down at the
bottom of the wall for when Humpty falls.
Everybody I talked to did want me to make one thing clear.
A correction would have occurred no matter what.
Trump did not cause this downturn.
It was going to happen.
But just like with Obama helping it go up, Trump made it worse and is going to help it
go down.
So he didn't cause this.
He didn't pop the bubble, but he sharpened the needle.
The needle was already there, it was going to eventually happen.
He just made it happen more.
Okay so when Humpty starts slipping around because of the trade war, that's the inverted
yield curve.
That was our warning.
He's up there but he manages to stay on his feet and then he starts sneezing.
And every time he hears about one of his friends sneezing, he gets a little bit shakier.
That's what we've been watching the last couple weeks.
And then the thing nobody saw coming, at least I didn't, OPEC Plus, they had an argument.
The Saudis wanted to stabilize the oil markets, which are shaky because everybody's sneezing.
And the Russians didn't want to play ball.
They disagreed.
So in response, to punish the Russians, the Saudis are kind of like, well we're just going
to flood the market with oil.
And apparently that was like walking up behind Humpty and pushing him.
That's where we're at right now.
Now tomorrow, today, by the time you watch this, we will find out how far Humpty falls
and whether or not Trump lights a fire underneath the frying pan.
So that's where we're at.
That's what, in simple terms, simple enough for me to understand it, that's what went
down.
Nobody I talked to could really give me a guess.
Nobody was like, well, it could be a mild recession, not really that big of a deal,
last till the, you know, just the first half of this year, or it could be a depression.
That's a pretty big range of outcomes.
And I think it's going to widely be determined by what happens over the next couple of days.
But again, I'm not an economist, I'm sure there are going to be people in the comments
section filling in anything that I left out, and I'm sure I left something out.
This is outside my comfort zone, but a whole bunch of people asked questions about it.
And I'm giving you the best information I could get.
But the cool takeaways are that Obama really didn't fix the economy, but he helped it.
Trump really didn't destroy it, but he helped destroy it.
And that's where we're at.
And Trump's great economy was just a continuation of the trend.
Really look at the charts.
That's really clear.
We should be able to reach just about anybody by showing them it in visual form.
So that's where we're at.
We're probably in for a bumpy few weeks, at least.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}