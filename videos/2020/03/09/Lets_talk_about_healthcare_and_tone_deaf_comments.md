---
title: Let's talk about healthcare and tone deaf comments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m8LDRdUTYaI) |
| Published | 2020/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Millions of Americans lack health insurance or are underinsured, with many unable to handle a $400 emergency.
- Tone-deaf comments from government officials showcase their disconnect from the fears and struggles of average Americans.
- President Trump's germaphobia leads to fears of journalists giving him something on Air Force One.
- Representative Paul Gosser from Arizona expresses fear of mortality and a desire to go out gloriously in battle.
- Despite having top-notch healthcare funded by taxpayers, government officials exhibit fear and panic.
- Officials should convey calm but instead spread worry and unease.
- The lack of empathy and action on healthcare crisis is evident, dismissing it as socialism.
- Suggestions to incentivize raising wages or addressing healthcare within the market are also rejected.
- The disregard for healthcare and economic concerns will have severe impacts on the working class and the economy.
- A healthy community is vital for economic stability, something that should be a priority for policymakers.
- Access to healthcare should be a basic right in a powerful country like the United States.

### Quotes

- "It might be something worthy of putting on the platform in some way."
- "A healthy community is an economically viable community."
- "It's given him time to think about man's mortality."
- "It displays how far away our representatives are from us."
- "Thoughts and prayers and all that. But we don't want to hear about it."

### Oneliner

Government officials' tone-deaf comments reveal their disconnect from Americans' struggles with health insurance, showcasing their fear and panic amidst a healthcare crisis.

### Audience

Policymakers, Advocates

### On-the-ground actions from transcript

- Advocate for policies that prioritize healthcare for all (implied).
- Support initiatives to incentivize raising wages for workers (implied).
- Raise awareness about the economic significance of a healthy community (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the disconnect between government officials and the healthcare struggles of everyday Americans, urging action to prioritize healthcare and economic stability for all citizens.

### Tags

#Healthcare #GovernmentOfficials #EconomicStability #CommunityHealth #Advocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So we're gonna talk about some more tone-deaf comments
coming from our government today.
But before we get into that, I want to remind everybody
that there are tens of millions of Americans
that have no health insurance.
There are tens of millions more who are underinsured.
That one out of 10 Americans cannot handle
a $400 emergency, and 40% would have difficulty doing so.
There's our baseline before we start this conversation.
Because when we talk about tone-deaf comments,
what it displays in glorious fashion
is how far are our representatives,
those people who are supposed to represent us up in D.C.,
how far away from us they are.
They don't even understand the situation, the fears.
So how on earth can they represent us?
So what are the comments?
We all know President Trump is a bit of a germaphobe.
Well, despite what he's telling everybody publicly,
I guess that he's kind of worried
that a journalist may give him something on Air Force One.
And he's worried about that, terrified of it, panicking,
meltdown.
These are the words that were used.
Representative Paul Gosser out of Arizona,
he's on a staycation.
And he said that it's given him time to think about man's mortality.
And he's decided he would rather go out gloriously in battle.
Fear.
They're afraid.
And they have access to the finest health care
that money can buy, with your money, by the way.
And they're afraid.
They can take time off.
They can stay home.
And they're letting these comments get out.
Their job is to convey calm.
If it is scary or unnerving or worrisome
for people who have the ability to stay home and rest,
to go on a staycation, who have health insurance,
who have private physicians available to them,
if it's worrisome for them,
what does that say for the rest of the country?
What does that say for the tens of millions of uninsured?
Those who don't have access to health care?
Those who can't take a week off, much less, too?
Or when anybody brings up addressing the health care
crisis in this country, oh, no, no, no, we can't do that.
That's socialism.
Can't do that.
Can't let people have health care.
That's just wrong.
Well then, let's do it within the market.
Let's find some way to incentivize raising wages.
No, no, can't do that.
Can't do that either.
It would just be great if you guys would stay poor and die quietly.
Thoughts and prayers and all that.
But we don't want to hear about it.
This should be a wake-up call for the GOP.
It's not going to be, but it should be.
The economic concerns that people are worried about,
they're going to be drastically greater here
because the working class don't have health care.
Those people who you count on to run those factories, well,
our supply chain is going to be interrupted.
It's going to be a whole lot worse when workers can't come in.
A healthy community is an economically viable community.
It might be something worthy of putting on the platform in some way.
And truthfully, nobody cares how it's done.
But if you're in one of the most powerful countries on the planet,
you should probably be able to see a doctor.
Anyway, it's just a thought. And I have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}