---
title: Let's talk about daddies, dollars, and letting them eat cake....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8zLedM3kXvU) |
| Published | 2020/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparing the United States to Marie Antoinette's "let them eat cake" moment, showcasing government disconnect.
- Contrasting European aristocracy with American aristocracy and their perceptions of wealth and privilege.
- Noting the awakening of the working class in the U.S. to the behavior of wealthy families and the rise of terms like oligarchy and plutocracy.
- Donald Trump Jr. challenging Hunter Biden to a debate on whose father helped them the most, reflecting tone-deafness amidst societal issues.
- Expressing the historical trend of ruling classes becoming more flagrant once their privilege is exposed.
- Acknowledging that while parents helping their children succeed isn't inherently wrong, corruption and taxpayer exploitation are problematic.
- Questioning whether political elites are engaging in corruption as they accumulate vast wealth post-office, prompting mainstream recognition of terms like kleptocracy.

### Quotes

- "My daddy can beat up your daddy."
- "Everybody wants a better life for their kids than they had."
- "Is the subconscious realization that corruption is happening one of the reasons those terms are coming into the mainstream?"

### Oneliner

Comparing government disconnect to Marie Antoinette, contrasting aristocracies, noting rising awareness of wealth behavior, and questioning corruption in political elites.

### Audience

Working-class Americans

### On-the-ground actions from transcript

- Question the actions and wealth accumulation of political elites (implied)
- Stay informed and aware of corruption within political circles (implied)

### Whats missing in summary

Insights on the societal impact of political corruption and wealth accumulation.

### Tags

#GovernmentDisconnect #WealthDisparity #PoliticalCorruption #SocialAwareness #EconomicInequality


## Transcript
Well howdy there internet people, it's Bo again.
We've got something a little light-hearted tonight
because the United States may have just had its own let them eat cake moment.
Now,
before the history buffs tear me apart in the comment section, I will go ahead and say
Marie Antoinette probably never said that, and if she did it was incredibly
sarcastic.
That term had been around for a really long time.
It wasn't even let them eat cake, it was let them eat...
I can't remember... something else.
Some other dessert that was typically reserved for the wealthy.
But that story, it sticks with us, right?
Because it's a universal story.
It illustrates how our
governments are just so out of touch with the plight of the average person.
You know, the story that the advisor comes in,
the peasants are revolting, they don't have any bread.
Well, let them eat cake.
Because they just don't get it.
Because they're out of touch.
We may have had our own little moment like that.
I haven't seen anybody really latch onto it yet, but it's been cracking me up
ever since I heard about it.
You know, the difference between
the European aristocracy
and the American aristocracy is that at least the European aristocracy knew
they were born well.
They got lucky.
Maybe it's a holdover from divine right and nobility and all of that.
But they understand
that their position is due to their family.
In the United States,
every millionaire and billionaire out there wants us to believe that they're self-made.
Most of them aren't.
They all want us to believe that they hit that home run themselves,
even though they were born on third base.
And
us mere peasants
in the United States, we're noticing this.
And we're noticing how these
wealthy families act.
And that's why words like oligarchy, kleptocracy, plutocracy, these things,
they're being used on cable news now.
They're entering the mainstream.
Words have been around a long time,
but now they're being talked about openly.
Because we're waking up to how
our betters behave.
While this is going on,
while
the working class in the United States is kind of waking up to this fact,
Donald Trump Jr.
has challenged Hunter Biden
to a debate
to see whose daddy has helped them the most.
I'm not joking. That happened.
I...
I guess uh...
Jr. was a little mad
about uh...
the idea that he only
has what he has because of his dad.
And so he...
yeah, he even
offered the person interviewing
to be the moderator for it.
It's incredibly tone deaf.
With all of...
all of this going on in the U.S.
That's the challenge.
I really think it should happen personally.
I would love to see
members of the American political elite
get on national television
and have an argument that amounts to, well,
my daddy can beat up your daddy.
Uh...
The sad part about it
is historically
once the ruling classes
have realized that they can't hide it anymore,
they just become more flagrant with it.
Now I think this debate should happen. It'll be hilarious. I can't wait for it.
It's never gonna happen, but it would be cool if it did.
That challenge
pretty much confirmed everything
that
drives those terms into the mainstream.
You know, and there's no reason for a debate about it.
I mean, I could settle that right now, and neither one of them would be where they are
if it was not for their parents or grandparents.
And there's nothing inherently wrong with that.
Everybody wants
a better life for their kids than they had.
There's nothing inherently wrong with that.
It only becomes wrong if they're
ripping off the taxpayers to make it happen.
If they're engaging in corruption.
That's when it would be wrong.
And we all have to ask ourselves
as we watch
the political elites
who have
relatively
modest salaries
for
the amount of wealth they have
when they leave office.
If we believe
that they are
not ripping us off.
If we believe they aren't
engaging in corruption.
Or
is the subconscious realization that it is happening
one of the reasons those terms
are coming into the mainstream?
Anyway,
it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}