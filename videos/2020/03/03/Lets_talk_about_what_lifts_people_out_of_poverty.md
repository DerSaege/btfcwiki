---
title: Let's talk about what lifts people out of poverty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zuY2Xr_Xmow) |
| Published | 2020/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the widely accepted idea that capitalism has raised many people out of poverty.
- Challenges the evidence behind the claim that capitalism is a force for good in the world.
- Criticizes the use of World Bank numbers to support the argument about poverty reduction.
- Contrasts the perspective on poverty as a feeling rather than a fact.
- Notes that poverty is relative and comparative to one's surroundings.
- Points out that the countries benefiting from poverty reduction are not necessarily free-market economies.
- Emphasizes the lack of a causal relationship in the data supporting the argument for capitalism.
- Provides statistics on poverty levels in the US over time to illustrate the correlation between consumerism, capitalism, and poverty reduction.
- Raises questions about public social spending and its impact on poverty reduction.
- Suggests that the debate around capitalism's benefits is centered on correlation rather than causation.
- Encourages individuals to critically analyze whether capitalism is truly a force for good in the world.
- Mentions examples of exploitative practices in major corporations as a reflection of capitalism's darker side.
- Urges viewers to take action within their own spheres of influence to shape the economic system.
- Stresses the need for systemic change and hard data to address issues beyond poverty, such as climate change.
- Leaves the audience with a call to action to initiate changes starting from their own communities.

### Quotes

- "It's a moral judgment. It's very subjective. Do you believe that a system that encourages the very worst inhuman behavior is a force for good in the world?"
- "You're a part of this system. Whether you approve of it or you don't, you're a part of it and because you're a part of it, you have the ability to influence it."
- "You can make small changes. You can vote with your dollar."
- "If we are looking for systemic change, we may need to adjust a whole lot."
- "The economic system that has led to the world we have today, it has impacts far beyond poverty."

### Oneliner

Beau questions the narrative of capitalism lifting people out of poverty, urging individuals to critically analyze its impact and take action for systemic change.

### Audience

Critical Thinkers, Activists

### On-the-ground actions from transcript

- Analyze the impact of capitalism in your community (suggested)
- Support ethical businesses and practices (suggested)
- Advocate for fair labor practices (suggested)
- Engage in local initiatives for economic justice (suggested)

### Whats missing in summary

In-depth exploration of the systemic implications of capitalism and the need for collective action and reflection. 

### Tags

#Capitalism #Poverty #SystemicChange #EconomicJustice #CommunityEngagement


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we are going to talk about a closely held idea,
an idea that many, many people believe
and accept at face value.
They believe that it's true.
And we're gonna explore whether or not
there's any evidence to back it up.
Like a lot of ideas that people just accept,
that evidence may not be there.
Tonight will be a little different in the sense that I may not have an answer at the
end.
Normally when I make a video, I have an answer of some kind to the topic being discussed.
It may not be an answer you like, it is certainly not an answer that I demand you accept, but
I have some form of an answer.
not have one this time because this topic is pretty cloudy.
So what's the idea in question?
Any time somebody presents any form of critique about capitalism, about free trade, somebody
shows up to say, you know capitalism has raised a bazillion, jillion people out of poverty.
And we're going to talk about whether or not that's true.
When somebody says this and you ask them for a source,
if they are willing to give you one nine times out of 10,
they're going to point you to World Bank numbers.
There's a lot of problems with that.
If you don't know how it works, basically $2 a day
is the cutoff.
If you are getting by on less than $2 a day,
well, you're in poverty.
over $2 a day, you're not.
Magic.
You can already see some issues with that.
Going from $1.99 a day to $2.01 a day
probably does not meaningfully change
your view of whether or not you're in poverty.
And the reality is, poverty is more of a feeling than a fact
in a lot of ways.
Now, sometimes, yeah, if you can't eat,
you're obviously in poverty.
But aside from that, it's comparative.
It's relative to what's around you.
Now, when you look at those numbers, what they show
is that, yeah, a whole lot of people
since the end of the Cold War or whatever
have been raised out of poverty.
It shows that.
It does not draw attention to the fact
that those countries that benefited the most from that jump are not free market economies.
They're like China, a state-directed economy.
Or they are economies that engage in protectionist behavior.
It's just not there.
Aside from that, it doesn't show a causal relationship.
It shows correlation.
These two things happen at the same time.
The numbers aren't really there for that.
So let's try to look at some other numbers.
The US, we obviously have good numbers on poverty in the US.
From 1959 on, we do.
Prior to that, it's a lot of estimates.
Now, I will say the estimates that I've seen,
they make sense.
The 1950s was seen as an era of prosperity.
It would make sense that in the Depression,
there was a higher rate of poverty.
So they work.
But at the same time, our capitalist economy of today
is based in consumerism.
that came about in the 50s. So we really don't even need to get into the guesswork. We can
just stick with the numbers that we have. What are those numbers? In 1959, somewhere
between one and four and one and five, rough estimate, people was in poverty in the US.
pretty high. I mean that's a big chunk. When you get to 2016-2017 it's closer to
one in ten. Well here's your proof. Consumerism, capitalism, free markets, all
of that, it worked. Almost cut it in half. Again, that is a correlation. It's not
causation. Throw out another set of numbers for you. In 1930, the public social spending as a share
of the gross domestic product was 0.56. Very low, less than a full percent. In 1960, it was 6.2%.
percent and in 2016 it was 19.32 percent this is money that is spent to get
people out of poverty who would have thought it got people out of poverty but
that's not capitalism but that's also not real evidence it's correlation it's
not causation we don't have it the data I don't have the data to show how
effective those programs were. The reality is this entire debate seems to
be very centered on correlation and when you're doing that you can find evidence
to tell you anything. But what's the main argument? What's the main question? What
why is this said? The idea is that capitalism is a force for good in the
world. If that's what you're trying to find out, just evaluate whether or not
capitalism is a force for good in the world. That's all you have to do. You don't
really need the numbers. It is a moral judgment. It's very subjective. Do you
believe that a system that encourages the very worst inhuman behavior is a
force for good in the world? It's one way it could be phrased. Somebody who
supported capitalism earnestly could phrase it a different way. I would suggest given the fact
that this week two major coffee companies have been accused of having eight-year-olds in their
supply chains gathering their coffee and that Nike and Adidas and Apple were all accused of having
having forced labor in their supply chains that maybe the free market isn't so free.
And it's not a rarity that happens all the time.
It's a very common occurrence for large corporations to be exploitive.
Is it a facet of capitalism?
So while I don't have an answer to that question, and I don't think anybody does to be honest,
I think anybody who's telling you for certain one way or the other either isn't thinking
about it clearly, isn't factoring in the fact that innovation and improvements in distribution
have something to do with this as well.
We didn't even touch on that.
They're either not thinking about it in depth, they're being disingenuous, or they've got
access to numbers I don't have.
They have data that I do not have.
While I don't have an answer to that, I do have a course of action that you can take
no matter what your answer is.
What are you doing standing around?
You're a part of this system.
Whether you approve of it or you don't, you're a part of it and because you're a part of
it, you have the ability to influence it.
Where you work, where you play, what you do, where you lay your money down, what you say,
all feeds into that system.
If you want to change it, you can.
It's up to you.
It starts right now in your own backyard, in your own hometown.
You can make small changes.
You can vote with your dollar.
This is a question that we're going to have to look into and we're going to need hard
data because if we are looking for systemic change, we may need to adjust a whole lot.
And the economic system that has led to the world we have today, it has impacts far beyond
poverty.
A lot of issues that deal with climate change may have something to do with it.
system that rewards exploiting and packaging and turning everything into a
product. Rewards products being thrown out, planned obsolescence.
is these are all behaviors that are encouraged by capitalism maybe it can be
reformed maybe it has to be changed it's a question for you guys that not me
anyway it's just a thought y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}