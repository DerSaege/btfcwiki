---
title: Let's talk about elephants painting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z1FN1SkOEpw) |
| Published | 2020/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the topic of animals and art in response to comments about elephants painting after discussing artificial intelligence creating art.
- Describing elephants as incredibly intelligent creatures full of human-like emotion, making it easy to believe they can paint.
- Explaining the common scenario in videos of elephants painting in Southeast Asia with human assistants helping them.
- Revealing the traditional training method for elephants in Southeast Asia, known as Pajan, involving cruel practices like beating and using bull hooks.
- Pointing out that the elephant's painting is a trained behavior, not a display of creativity.
- Not delving into all the disturbing details of the training process involving cruelty and pain inflicted on the elephants.
- Expressing the emotional impact on viewers, often leading to tears due to elephants' intelligence and human-like qualities.
- Exploring reasons why this practice is not widely known, including tourism dynamics, ancient traditions, conservation funding, and secrecy.
- Mentioning the debate around whether elephants could be trained to paint without using cruel methods.
- Encouraging awareness of the dark reality behind the seemingly cute paintings of elephants, shedding light on their suffering.
- Mentioning the emergence of more ethical tourism practices with camps that allow genuine interaction with elephants rescued from harmful environments.

### Quotes

- "Elephants are incredibly intelligent. They are very human-like."
- "An elephant painting an elephant holding a flower or something like that is manufactured."
- "Those cute paintings become a lot less cute after you see what the pajamas is and after you see what the animal probably endured to get there."

### Oneliner

Beau addresses the dark reality behind elephants painting and advocates for awareness of the cruelty involved in their training, urging a shift towards more ethical interactions with these intelligent creatures.

### Audience

Animal lovers, tourists, conservationists

### On-the-ground actions from transcript

- Visit and support ethical elephant camps to interact with and learn about rescued elephants (suggested)
- Raise awareness about the cruel training methods used on elephants for painting (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of the dark truth behind elephants painting and the importance of advocating for ethical treatment of these intelligent animals.

### Tags

#Animals #Art #Elephants #EthicalTourism #AnimalCruelty


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about animals and art.
We're going to talk about it because when we did the video about the artificial intelligence
that was creating art, a lot of people brought up elephants painting in the comment section.
So we're going to talk about that.
It is easy to believe that elephants can paint.
If you've ever been around them you know they are incredibly intelligent creatures and they
are full of emotion.
Human like emotion.
Real emotion.
You can definitely believe that they have the creative abilities to make art, to paint.
Add into that, you very well may have seen a video of an elephant painting.
I mean that definitely could lead you to believe that they paint.
And the videos are pretty much always the same.
They're made in Southeast Asia and there's an elephant there, an easel in front of them.
And they have their human assistant.
And the human assistant helps them with the paintbrush, gives them the paintbrush, maybe
holds a palette for them.
And the rest of the time is just standing there reassuring them.
Kind of petting them.
Okay, so those are at locations for tourists and those are trained elephants.
Traditionally in Southeast Asia elephants are trained through something called the Pajan.
Literally translated that means the crush or to crush.
It's not just talking about the animal spirit.
The elephant is placed in what amounts to a vice and held there for days.
It is beaten.
There are all kinds of horrible things that happen to it.
There are videos of it.
I'm not going to detail them all.
They are trained, to use the word loosely, with something called a bull hook.
Which is basically a stick with a nail in it.
And that nail is inserted to the soft tissue right behind the ear.
Some of them also have a hook on the end of it, a sharp hook, that is used to bash the
animal.
Knowing that, think about what you're actually seeing when the human assistant is standing
beside the elephant, going like this.
The nail.
And the reason the assistant hands the elephant the paintbrush is because it's actually dropped
down their trunk.
The paintbrush has a cross member that stops it from going all the way in.
It is, the elephant is doing what it has been trained to do.
It's not really creativity on the elephant's part.
Okay.
There are tons of videos about this.
I don't want to go into all of the details on it because it is disturbing.
In fact, if you watch these videos, you will see the people describing it, a lot of them
start crying.
Why?
Because elephants are incredibly intelligent.
They are very human-like.
And when you see them going through this, it does elicit a response.
So why isn't this more well-known?
A whole bunch of reasons.
A whole bunch.
First is, let's think about the people that go to Southeast Asia for tourism.
They are normally people who are going for an experience in another culture.
They are the, I'm going to Burma for the summer, have my granola type of people.
And they often don't want to interfere because they've been trained.
They know you don't want to be a colonizer, you don't want to be a settler, you don't
want to be the ugly American, so they don't talk about it if they see it.
Now add into that that this is an ancient practice.
Ancient, literally ancient.
Not just old, ancient.
So there is a lot of leeway given in that regard.
Then you have to add into it that the money that is made through these endeavors really
does go to conservation a lot of times.
Then you have to add into it that if the animal wasn't in these camps, it may not make it.
They're targeted.
And then there's the big one.
This is done in secret.
There's not a lot of documentation on it.
And there are a lot of people, myself included, who completely believe that an elephant could
be trained to do this without using these methods.
And there are some that claim they don't.
So they, it's hard to single out who is and who isn't.
This practice is very widespread.
It's pretty horrible.
But a lot of the handlers there don't actually use the horrible methods.
You don't know who it is.
But at the end of the day, understand that an elephant painting an elephant holding a
flower or something like that is manufactured.
It's not the creativity of the animal.
It's something that the animal has been trained to do most often under conditions of torture.
That's not to say all, but it's definitely something that people need to be aware of.
Those cute paintings become a lot less cute after you see what the pajamas is and after
you see what the animal probably endured to get there.
Now on the upside, there is a new kind of tourism going on over there.
And especially in relation to elephants.
There are camps that are set up where people can actually interact and get to kind of get
to know the elephant.
Those camps are most often foster camps.
They're bringing in elephants that were injured during the pajama or at one of these less
reputable camps.
And again, there's a bunch of information out there about it.
I just think it's one of those things that because it doesn't get talked about often,
should probably be brought up every once in a while.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}