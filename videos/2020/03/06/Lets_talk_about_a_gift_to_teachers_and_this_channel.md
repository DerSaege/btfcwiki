---
title: Let's talk about a gift to teachers and this channel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3DCwe4qih80) |
| Published | 2020/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reciting a list of historical events and figures from Billy Joel's song "We Didn't Start the Fire."
- Channel's purpose: to help students better understand history and current events through engaging content.
- Collaboration with public school teachers to incorporate elements from the song into videos.
- Emphasizing the importance of history in understanding current events, philosophy, and community activism.
- Stating the necessity of comprehending past events to remain relevant and effective in the present.
- Encouraging viewers to view various aspects of life through a historical lens.
- Conveying that the fire of history, once ignited, will continue burning.
- Stressing the significance of history in shaping our understanding of the world.
- Linking past events to present-day relevance and participation in shaping the future.
- Signifying that the channel serves as a platform for exploring history and its impact on contemporary issues.

### Quotes

- "While we did not start the fire, it will keep burning."
- "If you want to be relevant 70 years later, you're going to have to understand what happened 70 years ago."

### Oneliner

Beau recites historical events, urging viewers to understand the past to influence the future effectively.

### Audience

History enthusiasts, educators, activists.

### On-the-ground actions from transcript

- Collaborate with educators to create engaging historical content (implied).
- Engage in community activism informed by historical perspectives (implied).

### Whats missing in summary

The full transcript provides a comprehensive historical overview through a modern lens, encouraging viewers to connect past events with present actions for a better future.

### Tags

#History #Education #Community #Activism #Understanding


## Transcript
Well howdy there internet people, it's Bo again.
And this is what the channel's about.
If you stick around to the end, you'll find out why this exists.
Harry Truman
Doris Day
Red China
Johnny Ray
South Pacific
Walter Winchell
Joe DiMaggio
Joe McCarthy
Richard Nixon
Studebaker
Television
North Korea, South Korea
Marilyn Monroe
Rosenberg
H-Bomb
Sugar Ray
Pamu John
Brando
The King and I
and the Catcher in the Rye
Eisenhower
Vaccine
England Has a New Queen
Marciano
Liberace
Santa Ana
Goodbye
We Didn't Start the Fire
It Was Always Burning Since the World's Been Turning
Joseph Stalin
Melnikov
Nasser
Prokofiev
Rockefeller
Campanella
Communist Bloc
Roy Khan
Juan Per??n
Tuscany
Dacron
Gin Binfoud
Falls
Rock Around the Clock
Einstein
James Dean
Brooklyn's Got a Winning Team
Davy Crockett
Peter Pan
Elvis Presley
Disneyland
Bardot
Budapest
Alabama
Khrushchev
Francis Grace
Satan Plays Trouble in the Suez
We Didn't Start the Fire
It Was Always Burning Since the World's Been Turning
Little Rock
Pastranac
Mickey Mantle
Kerouac
Putnik
Shoe and Lie
Ridge on the River Kwai
Lebanon
Charles de Gaulle
California Baseball
Starkweather Homicide
Children of Thalidomide
Buddy Holly
Ben Hur
Space Monkey
Mafia
Hula Hoop
Castro
Edsels
Isonogo
U2
Sigmund Rhee
Paola
Ann Kennedy
Chubby Checker
Psycho
Belgians in the Congo
We Didn't Start the Fire
It Was Always Burning Since the World's Been Turning
Hemingway
Eichmann
Stranger in a Strange Land
Dillon
Berlin
Bay of Pigs Invasion
Lawrence of Arabia
British Beatlemania
Old Miss
John Glimm
Liston beats Patterson
Pope Paul
Malcolm X
British Politician Sex
JFK
Blowed Away
What else do I have to say?
We Didn't Start the Fire
It Was Always Burning Since the World's Been Turning
Birth Control
Ho Chi Minh
Richard Nixon's back again
Moonshot
Woodstock
Watergate
Punk Rock
Biggin
Reagan
Palestine
Terror on the Airlines
Ayatollah in Iran
Russians in Afghanistan
Will of Fortune
Sally Ride
Heavy Metal Suicide
Foreign Debts
Homeless Vets
AIDS
Crack
Bernie Getz
Hypodermics on the Shore
China's Under Martial Law
Rock or Roll or Kola Wars
I can't take it anymore.
We Didn't Start the Fire
It Was Always Burning Since the World's Been Turning
So this exists because some public school teachers
reached out and asked for a favor.
They use a Billy Joel song
to try to create a spark in their students
to get them interested in history
so they can better understand today.
They asked me if I would be willing to work
some of the elements from that song
into some of my videos
to help generate that spark.
And I was more than happy to do that.
This channel isn't really just about history
especially not just post-World War II history.
It's about current events.
It's about philosophy.
It's about being active in your community.
The thing is if you want to be effective
at any of those things
they're best viewed through the scope of history.
And while we did not start the fire
it will keep burning.
And if you want to play a part in it
and you want to be relevant
70 years later
you're going to have to understand what happened 70 years ago.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}