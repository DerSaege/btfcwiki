---
title: Let's talk about rhetoric and Ken Buck the super warrior....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9LqgSGTlOWg) |
| Published | 2020/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of rhetoric and its purpose, using Congressperson Ken Buck as an example.
- Describes a legislator's role in debating, discussing, and convincing opposing parties.
- Criticizes Ken Buck for releasing a video filled with rhetoric instead of engaging in constructive debate.
- Points out the use of an AR rifle as a prop in the video and questions Buck's familiarity with handling it.
- Raises concerns about the irresponsible display of weapons and the potential dangers it poses.
- Calls out Buck for using threatening rhetoric that appeals only to the uninformed.
- Emphasizes the importance of setting a positive example as a congressperson.
- Condemns the use of guns as props and the dangerous implications it can have.
- Challenges Buck's image as a tough guy and questions his commitment to responsible gun ownership.
- Urges Buck to reconsider his campaign tactics and focus on logic and persuasion rather than intimidation.

### Quotes

- "Come and take it."
- "Guns are not toys, sir."
- "You're supposed to be respectable."
- "You're supposed to be somebody who uses logic and persuasion, not intimidation."
- "You need to rethink your campaign."

### Oneliner

Beau criticizes Congressperson Ken Buck for resorting to threatening rhetoric and irresponsible weapon display instead of engaging in constructive debate, urging him to reconsider his campaign tactics.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Educate others on responsible gun ownership and the dangers of using weapons as props (implied)
- Advocate for constructive debate and logical persuasion in political discourse (implied)
- Support candidates who prioritize respectful and informed communication in politics (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the problematic rhetoric used by a congressperson, urging for a shift towards responsible communication and debate in politics.

### Tags

#Rhetoric #GunControl #PoliticalDiscourse #ResponsibleCommunication #CampaignTactics


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about rhetoric.
What it is, what it's designed to do, and what it does.
And in order to do that, we're going to talk about
a congressperson named Ken Buck.
Ken Buck, out of Colorado,
I believe he is from the 4th District.
We're gonna talk about him, cause he released a video.
Now a legislator's job, what they're supposed to do,
is debate, discuss things.
Convince the opposing party to change their mind
and embrace their ideas.
They're supposed to debate legislation,
introduce legislation, sponsor bills, that sort of stuff.
Vote.
That's the job of a legislator.
But not for Ken Buck.
Not Ken Buck, cause he's gonna go that extra mile
cause he is a patriot.
And he released a video using a lot of rhetoric.
And what is rhetoric designed to do?
Inspire.
Very much a call to action.
Cause that's what real patriots do.
If you have not seen the video,
allow me to describe it for you.
There is our super patriot, Ken Buck, standing in his office.
Because he is a just good red-blooded American,
on the wall behind him is an AR.
And of course that AR is covered in American flags.
Isn't that adorable?
He goes through his little speech
in which he names opposing politicians.
And then grabs that rifle.
Come and take it.
So rather than do your job
and convince the opposing party of your position
using logic and trying to debate it,
you go straight for that weapon.
You know that's one of the worries
of those in favor of gun control, right?
That people go to it too quickly.
That they don't exhaust all other options first.
It's one of their worries.
Another one is untrained people playing with weapons.
Fits you, doesn't it, sir?
I am fairly certain
that you're not really comfortable with that thing.
Number one, you're charging handles back.
Number two, by the way you're holding it.
I can tell by the placement of one finger,
you're not actually comfortable with that thing.
You've never trained with that, have you?
Not for its intended purpose.
You're not really the super soldier warrior, are you?
That brings me to my final point.
And while I'm fairly certain
you've never trained with that particular rifle,
if you are going to level threats at people
and you want to portray yourself
as the super tough guy warrior,
I strongly suggest you have sights on your rifle, sir.
That particular rifle is flat on the top.
It's not designed like that to look cool.
That's so you can mount optics on it.
And you never did that.
It's never been fired, has it?
Tough guy.
When you use that kind of rhetoric,
it doesn't appeal to anybody
who actually knows anything about violence.
You don't look tough.
It only appeals to people who aren't trained.
It only appeals to people who really don't understand.
You're a congressperson.
You're supposed to be respectable.
You're supposed to set the example and be a leader.
People like you are why there was a Twitch streamer,
a live streamer, the other day, who pulled out his pistol,
doing the same thing, using it as a prop.
Guns are not toys, sir.
Using it as a prop, he chambered a round.
And then when he went to clear it,
he forgot to drop the magazine.
So when he racked that slide, it ejected the first round,
put the second round in it.
Then he pulled the trigger.
Somebody could have got killed
because he was using it as a prop, like you,
a congressperson.
You're supposed to set the example.
And the fact is you are.
You're not setting a good one, though.
What do people hear, those untrained people?
Come and take it.
Well, my congressperson said not to give up my gun.
So when somebody who may be a little off
gets a red flag notice and the cops show up to take it,
what's he gonna do?
Flag-covered coffin, sir, and they're your fault
because you can't debate,
don't know how to actually run a campaign,
want to appeal to people who think that looks tough.
You do not impress me, sir.
You're a disappointment.
And for the record,
I am a stronger Second Amendment supporter
than you will ever be.
You need to rethink your campaign.
You are doing a disservice to your cause
and to your community.
You aren't a tough guy.
And you're not a killer.
And that's a good thing.
You're not supposed to be.
You're supposed to be somebody who uses logic and persuasion,
not intimidation.
Anyway, it's just a fault.
You have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}