---
title: Let's talk with Mike Broihier about taking on Mitch McConnell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oxEihoNBipA) |
| Published | 2020/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction of special guest Mike Breuer, who is running against Mitch McConnell in Kentucky.
- Mike Breuer's background as a retired Marine Lieutenant Colonel, educator, substitute teacher, and farmer.
- Mike Breuer's platform focusing on economic and social justice for all based on equality under the law and common ownership of resources.
- Critique of McConnell as someone focused on accumulating power and wealth rather than the well-being of Kentuckians.
- Mike Breuer's emphasis on economic justice for Appalachian coal-producing states like Kentucky.
- The interconnectedness of economic and social justice and the impact on Kentucky residents.
- Mike Breuer's approach to communicating his message honestly and respectfully, drawing from his experience as a reporter and editor.
- Criticism of the prison system in Kentucky and the financial burden it places on taxpayers.
- Impact of budget cuts on public libraries and schools in Kentucky, especially during times like the coronavirus outbreak.
- Strategy to beat McConnell with truth and by pointing out his lack of significant contributions to Kentucky.
- Mike Breuer's stance on controversial issues like women's reproductive rights and universal background checks.
- Critique of McConnell's use of public funds for projects that benefit him politically rather than the people of Kentucky.
- Challenges faced by Kentucky farmers due to misguided policies like hemp legalization.
- Mike Breuer's progressive stance on gay rights and the importance of honesty and fairness in politics.
- Call for individuals to listen, understand, and respect others in order to make the world a better place.

### Quotes

- "Beat him with the unrelenting truth."
- "Don't be a dick."
- "He is not good for the Republic. And he needs to go."

### Oneliner

Beau introduces Mike Breuer, a candidate running against Mitch McConnell in Kentucky, who advocates for economic and social justice based on equality under the law and common ownership of resources while critiquing McConnell's focus on power and wealth accumulation.

### Audience

Voters

### On-the-ground actions from transcript

- Support Mike Breuer's campaign by donating at mikeforky.com (implied)
- Spread the message on social media to increase visibility and support for Mike Breuer (implied)
- Volunteer, donate, or come forward to help the campaign if in Kentucky (implied)

### Whats missing in summary

Mike Breuer's detailed plans on how to bring about economic and social justice in Kentucky.

### Tags

#Kentucky #MitchMcConnell #EconomicJustice #SocialJustice #Campaign


## Transcript
Well, howdy there internet people.
It's Bill again.
So tonight we've got a special guest.
He's the guy running against Mitch McConnell.
You wanna introduce yourself?
I lost the feed here.
All right.
My name's Mike Breuer and I'm a obviously Democrat
running against Mitch McConnell here in Kentucky.
And I'm a retired Marine Lieutenant Colonel
and I'm married to a retired Marine.
And right now I'm a, let's see,
I done a lot of things.
Started off as an artilleryman in the Marine Corps,
served as a provisional rifle commander
and rifle company commander in Mogadishu,
92, 93 during the Somali Civil War
and served as the plans in Korea
for Marine Forces Korea, 2002.
I'm also an educator,
taught for three years at the University of California
and also I'm a substitute teacher here
in Lincoln County, Kentucky.
And I'm a farmer.
My wife Lynn and I have been farming here in Kentucky
for the last 15 years.
So that's my story and I'm sticking to it.
All right.
Okay, so the one thing you're not gonna have to do
with this audience is convince anybody
that McConnell needs to go.
So what, tell us about your platform.
Where do you differ?
From whom?
From McConnell.
Let's start there.
Okay.
Well, how about on everything?
Other than oxygen is good,
but that's probably even debatable,
but the basis of my platform, man, is this,
is that I believe in economic and social justice for all.
And really the founding fathers nailed it
when they talked about the fact
that we are all equal in the eyes of the law.
We all deserve equal protection under the law
and that the bounty of our land, our resources,
our wealth, our energy is something
that we all hold in common.
And that's the basis of my platform
from the very, very beginning.
I don't know if the Senate majority leader
actually believes in that.
I think he's a guy who believes
in accumulating power and wealth,
and he doesn't give a crap about the constitution,
about the Republic or about the people of Kentucky.
I can definitely see where you're coming from on that one.
When you say economic justice,
what does that mean for the people of Kentucky?
Well, you know, man, when I started out,
I viewed economic and social justice
as like two discrete items.
And, but the more I thought about it and talked about it,
I guess I kind of convinced myself
that they're so tightly linked
that they're kind of the same thing.
But let's talk about economic justice.
So Kentucky and probably the eight Appalachian
coal producing states.
I mean, we built the great cities of America.
We built the railroads, we built the cities,
we built the bridges,
and we've been left holding the bag
economically and environmentally too.
And I don't know if your fans
are interested in environmental issues,
but I mean, literally we are holding the bag
on 200 years of, first it was timber and then it was coal.
And now the coal's running out, it is sayonara.
And I would not want to be a Republican politician
going out to coal country, going to Arlington County
and saying, now we're bringing coal back boys,
because that's not go over very well
with people out in Eastern Kentucky.
Or, and you know, we've got coal fields
out in Western Kentucky too.
Hmm.
So the idea of, from those of us outside
or those of us who used to live in Kentucky,
the idea of bringing social justice to Kentucky
seems like an uphill battle.
What is your, what's your plan?
How are we gonna do this?
I definitely think you've got the blue collar workers.
You have their attention, but that phrase,
social justice carries a lot of baggage there.
So what's the plan?
How are you gonna market it?
Well, you know, I don't know if it's marketing,
you know, I'd spent, you know,
we've been farming here for 15 years,
but it takes a little bit of capital to farm.
And so we've done work off the farm, obviously.
And for two years I was a reporter
and three years I was an editor of a rural newspaper,
third oldest here in Kentucky.
And you learn how to communicate your message to people
and I'm who I am, who I am.
And so kinda, you know, as a little scary on the outside,
but kinda squishy on the inside,
I had to, you know, learn how to communicate to folks
who would read my weekly editorials.
And I learned that you gotta be right,
but not like self-righteous.
And you gotta be patient
and you can't make people feel dumb
for believing what they believe.
And so when I talk about social justice,
I mean, it's as simple as things as looking at like, okay,
we've turned prisons into an industry in Kentucky, right?
And in the last seven years,
we've increased the number of women in prison by 300%.
You know, those are families without moms
or without sisters or daughters
and primarily African-American and for nonviolent crimes.
And it's because, you know,
we have got the term prison and industry
in the same sentence, you know,
just like education and industry
shouldn't be in the same sentence.
You know, prisons are for prisons,
not for making profits for people,
but we've created a system here in Kentucky
where a lot of people are making a lot of money
off of our prisons.
And we're more than happy to provide them clients
to fill those prisons up to the max capacity.
You know, a lot of folks I run into say,
well, you know, I'm fiscally conservative, right?
And it's like, all right, you're fiscally conservative.
Explain to me why someone who's committed
a nonviolent crime, maybe drug possession
or possession of marijuana,
why it's worth the taxpayers $124 a day
to take that person away from their family
and their home, we're talking $25,000 a year
to keep them locked up and give them no re-entry
into what, you know, whether it's in rural Kentucky
or urban Kentucky, give them no smooth entry
back into society where they're just gonna re-offend
and send them right back to prison.
So we've created this self-licking ice cream cone.
That's just one example, you know, of the prison system.
And, you know, just ridiculous things like
just today in here in Kentucky,
the state Senate passed a bill
cutting all the money to public libraries, right?
All right, it may not be a big deal for people,
but if you live, right?
If you live in the country, I mean,
the public library is your internet service provider
or McDonald's is your internet service provider.
And so here we are looking at the coronavirus
and they're talking about shutting down schools
and the kids needing to get, you know,
do distance learning to continue on their school year.
Perfect time to cut the school budget.
You know, those are social justice issues.
And whether you live in Louisville or Lexington
or you live out here in Chicken Bristle, it hurts.
And that's what I talk about
when I'm talking about social justice.
Makes sense, makes sense.
So McConnell has money.
He's got backers.
He's got a lot of people.
Well, not people.
He's got a lot of companies throwing cash his way.
How are you gonna offset that?
Well, it's by talking.
I mean, you know what I tell people is that, listen,
you know, they say, how are you gonna beat McConnell?
I say, you beat him with the unrelenting truth, right?
You know, the fact that we're,
so he's been Senate majority leader for a long time.
He's been in Congress.
He and I entered federal service the same year, right?
One of us is a multi, right, in 1984,
one of us is a multi, multi, multi-millionaire
and the other is a freaking asparagus farmer
in Lincoln County, Kentucky.
But you know what, man, I made the right choice
because I got to, I could sleep at night.
But how do you beat him?
So you beat him with unrelenting truth.
And, you know, I was talking to a group of women Democrats
out in Estill County, which is in Eastern Kentucky
and they asked the same question.
And so, you know, okay, so we're seventh in diabetes.
We have the fifth worst economy in the United States.
We're third worst in cancer deaths.
And, you know, we are like number one in child abuse
and number one in child homelessness, right?
Here in Kentucky.
And so when people say, oh, how do you give him the prestige
of having the Senate majority leader?
He hasn't done nothing for Kentucky.
You know, go out and ask that question in the coal fields
and see what people say.
And that's how you beat him.
Now, sadly, 70 days to the primary.
And I got a lot of ground to cover,
but we've been hitting it hard since July.
And we can close the deal for the May 19th primary.
Okay, so I'm going to address anything
that's going to separate you from McConnell
that may surprise people, separate you from anybody else.
Are you breaking up a little bit, dude?
But I think what you said was,
is there things that are gonna separate me from McConnell?
Yeah, any hot button issues
on the more controversial stuff.
Oh, right, the controversial stuff.
Yeah, obviously.
I mean, there are some hot button issues.
Women's reproductive rights, big issue in Kentucky.
And also, and I gotta tell you,
judging by that 303 shirt,
you're not gonna like this one.
But I have a firm backer in universal background checks
and some of the red flag laws
that are included in the Violence Against Women Act.
And those are kind of considered to be hot button issues
here in Kentucky.
But I gotta tell you what I found, man.
When you tell people what you believe upfront
and you don't try to triangulate or spurs
or give them some kind of long shaggy dog story
about your upbringing, they say,
well, I disagree with you,
but at least you didn't lie to me.
Now, I know that's a low bar, politician,
but I gotta tell you,
like I was talking to some farmers up in Northern Kentucky
and one of the guys asked me whether I was pro-choice
or pro-life and I told him pro-choice
and he kind of slapped his hand down.
He said, well, I disagree with you,
but at least you didn't lie to me, so you got my vote.
I'm like, all right, there's one.
But I think that being honest with people
and even if they disagree with you,
they're like, at least this guy's not bullying me.
I don't know how you feel about that.
No, as far as the Violence Against Women Act,
I think that we've got,
there's not just do we need to have some motion there,
we've got to close the hole.
Well, you were charged with domestic violence,
but it got knocked back to assault,
so you don't lose your rights.
I think we need to address that too.
That's a huge, huge loophole that always gets ignored
and domestic violence is a really, really good indicator
of future violence.
So no, I'm definitely pro-Second Amendment,
but I'm also not insane.
I would like people not to get killed.
But yeah, I could see being pro-choice
being a tough sell in Kentucky too,
but again, I don't know that
with the climate there right now,
I don't know that that's gonna be a make or break issue.
Well, the other thing that McConnell's good at
is parachuting in some pork.
I mean, his wife is the Secretary of Transportation,
so dropping off $60 million for a bypass is no problem.
And it's happening all over Kentucky.
You want that bridge built now?
You need that bypass, here you go.
Here's $15 million, $20 million, up to 60 million bucks.
But what people, and you gotta keep communicating
this carefully is that it's not his money.
It's our money.
He's buying your votes back.
He's buying your votes with your money.
So I think people are onto his game.
There's been a couple busts.
For example, Hamp, he got a great deal of credit
for ramming through the legalization of hemp,
but it's completely backfired here.
I mean, I can line up farmers for you
who've got a barn full of hemp they can't sell.
And the only people, you know,
I said holding the bag before,
the only on the hook for this are the farmers.
The processing companies and their investors,
they all got their money.
And you've got farmers literally
with barns full of unsold hemp,
and they're gonna lose their homes
because hemp's a non-traditional crop.
You can't get a typical farm loan for it
or farm insurance for it.
And if no one buys your hemp, you eat it.
And so people mortgage their homes.
And we're talking about like salt of the earth Kentuckians.
Farmers have mortgaged their homes
to go into this new venture, and they're out.
I mean, there's no next year for them.
It's not like soybean or corn or cows or something.
They're done.
And so as a farmer myself, I could talk to them.
I understand I've been through the process.
And I think that I can hit home with them
and point out the fact that they're just being used.
So it's a long road, but I'm a patient guy.
All right.
In Kentucky, I guess another hot button issue would be,
I mean, the thing is it's a decided issue really,
but it's still something that gets talked about
is gay marriage.
Is that something you wanna wait in on
or are you just going to stand with what the law has decided?
Well, I was way ahead of the law.
I was a newspaper editor back in 2010.
If this is a long, boring story, man,
just give me the high sign and I'll shut up.
But all right.
So it was back when they're reauthorizing
Don't Ask, Don't Tell.
And so I'm editor of a rural weekly newspaper
and I wrote an editorial called Don't Ask, Don't Tell,
Don't Care.
And basically based on my 40 plus years of my wife
and my experience, I know plenty of gay people in the military.
And I said, listen, no one in the military
gives a damn about this.
The only people who really care about who you, yeah, OK.
The only people who care who you sleep with
are preachers and politicians.
And they only care because they think you care.
So anyways, I published this editorial.
And this is a very, very small, very baptist-y kind of community.
And I get a call the next morning.
Someone wants to talk about my editorial.
And the lady says, she's a self-identifies.
She's a little old church lady from Crab Orchard, Kentucky
wants to talk about my editorial.
But here's what she said, man.
She said, I read your editorial.
I thought about it.
And I prayed about it.
And I realized you're right.
And if you're OK, I'm going to copy it and take it
to Bible study on Monday and tell everybody
that you're right.
And I counted that as a big victory.
And it kind of reinforced the fact that, listen,
you just got to talk honestly and with a sense of fairness
to people of Kentucky.
And they'll come on board.
They have keen sense of fairness here, common sense.
May not have degrees and things like that.
But they pride themselves on being fair and understanding
other people.
And so, yeah, I got to tell you, man,
I don't care who someone sleeps with.
I don't care who they love.
And you know what?
Everybody, I think, these days knows somebody who is probably
gay, lesbian, transgender, or whatever.
And I got to tell you, and I'm not
going to talk too in detail specifics,
but as substitute teacher, this past year,
I had my first two trans students.
Went to kindergarten.
And so here I am leading these kids off to the bathroom
because kindergarten, you go about 20 times a day.
And the only person worried about what
was going to happen next was me because the kids just
sorted themselves out, went in the bathroom,
and no one said nothing.
And they just went back and went back to being themselves.
And my fear of telling that story
is that some self-righteous prick
is going to hunt down that poor kid
and run them out of the school.
And so what?
I don't care.
And I think that overwhelmingly, ultimately, everyone
knows somebody who has a different orientation than them.
And other than some few hateful people out there,
this is something that's gone past the point
of being important.
Good.
All right.
So here's a question I always ask everybody
that comes on the show.
If there's all these people watching,
tell them how to change the world.
One thing that an individual can do in their community
to make the world a better place.
What do you got?
Don't be a dick.
Don't be a dick.
Fair enough.
No, no.
Seriously.
I'll tell you.
One thing about farming, like the way we farm,
you get a lot of time to reflect.
There's an awful lot of time walking around picking crops
by hand, like three, four, five hours a day.
And the worst thing you can do is
have a real destructive internal dialogue going on.
It's the, when you're in the shower,
you're viewing the argument, you're going,
yeah, I should have said this.
You know?
But just turning off that internal dialogue for a while
and opening your ears and opening your minds
and saying, all right, where's this person coming from?
And I mean, I guess you're asking me to reflect back
on 58 years of what I've learned in my life about getting along.
But here's one thing is that at my age,
people take a look at you and say,
well, you know, plus years in the military, combat veteran,
blah, blah, blah.
They expect me to be like real hard and brutal.
But I got to tell you, man, as time has gone on,
my mind's opened up.
And I've gotten able to listen to people
and really appreciate them.
Just shut that noise off my own head
and listen to what people are saying.
And yeah, you don't have to agree with them, but you don't
have to hate them either.
They're not the enemy.
And so yeah, I guess if you were to ask me how to change the
world, don't be a dick.
I like it.
That should actually be a campaign slogan, to be honest.
So I'm assuming, I don't know, I'm
assuming your campaign is self-funded and through
the community.
Do you have any big donors lined up or anything like that?
Not a one.
Not a one.
All of our donations so far have been from individuals.
But I'll tell you what, lately, things have actually
improved quite a bit.
I mean, to get started, we sold off all our excess farm
equipment.
We sold all the animals, emptied out the bank accounts.
What little there was in there.
But I've had a whole bunch of people come off Andrew Yang's
campaign and literally flooding here to Kentucky.
And I told someone earlier today, last week, I had five
grown men sleeping in my two-bedroom farmhouse.
And I had a neighbor drive a leaky old mobile home over
here.
I had two poor kids staying out there.
And so but it's been a real shot in the arm.
And they're better at fundraising.
And they've got a much bigger reach than I ever did.
So yeah, the last couple, last two weeks particularly, have
been very, very good.
But no PACs, no endorsements.
We've gotten a handful of checks from companies.
And we mailed them right back.
So all of our donations are coming right from individuals.
All right.
OK.
So on that note, give everybody your information.
Give everybody where they can donate, where they can find out
more about your platform, where they can get one of those cool
shirts with the guy throwing the flowers.
I'm not supposed to show that.
Oh.
Is that it with the Banksy?
Yeah.
Yeah.
You know, when we initially had that show and people go, oh, my
God, is he Antifa?
It was like, oh, you've missed the point.
But that's all right.
So if anyone's interested, they can go to mikeforky.com,
mikeforky.com.
And there's obvious, the usual suspects donate.
But my issues page, I kind of spilled my guts back when I
launched back in July and just kind of let it all hang out
there.
This is a one-shot deal.
Like I said, no triangulation, no folks grouping.
I just kind of spilled it all back then.
And I'm sticking to it on the hope that at least people
appreciate honesty.
So mikeforky.com.
And if they care to donate, which would be awesome,
act blue.
And they can also reach us at the site if they're in Kentucky
or they want to come volunteer, donate.
And the last thing I'll say is, if you like the message,
spread it Facebook, Twitter, retweet this or repost this
podcast if people like it or hate it.
I don't know.
I don't care.
Go ahead and post the whole thing.
And that really helps because there's this exponential force
of the internet where all you got to do is tell three
friends and it just blows right up, which I'm sure you know.
Is there anything else you want to, any other information
you want to get out, any other ideas you want to get across
before we wrap this up?
No, bro, but I just want to say thank you for having me.
And thanks for being a great host.
And this is very kind.
People have got no reason.
They don't owe me nothing.
But I think that even the most conservative person in America
can look at Mitch McConnell and say,
he is not good for the Republic.
And he needs to go.
He's had 35 years.
He's had his turn.
Time to go.
And I'm the guy to beat him.
So thank you so much for having me.
And you're a good host, good questions.
And I'll come back any time you want.
All right.
All right.
Well, that's it for this one.
And we will see all y'all later.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}