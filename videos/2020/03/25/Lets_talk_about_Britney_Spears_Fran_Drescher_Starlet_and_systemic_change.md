---
title: Let's talk about Britney Spears, Fran Drescher, Starlet, and systemic change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0UQg-8kLetM) |
| Published | 2020/03/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Soviets sent up Sputnik 10 in 1961 with a mannequin and a dog named Starlet, the first dog in orbit, to capture the imagination and bring attention to space travel.
- Britney Spears and Fran Drescher have endorsed radical ideas for systemic change on social media, sparking mixed responses.
- Radical ideas related to systemic change are scary as they challenge the current system and require embracing the unknown.
- Mainstream figures like Britney Spears carrying radical ideas make them less threatening and help in spreading the message.
- Systemic change won't come from political leaders or the system itself, as evidenced by the stimulus package's allocation of funds.
- It becomes easier to question and advocate for radical ideas when influential figures like Britney Spears and Fran Drescher openly support them.
- Advocating for systemic change during challenging times is vital to push for change when the situation improves.
- Encouraging more celebrities and influential figures to introduce radical ideas to the mainstream is necessary for systemic change.

### Quotes

- "You go to war with the army you have. They enlisted. Give them the support they need."
- "We need to encourage more celebrities, more people with influence outside of the system to introduce those radical ideas to the general populace, to the mainstream."
- "If we want systemic change, we have to act like it."

### Oneliner

Beau talks about the importance of mainstream figures like Britney Spears and Fran Drescher endorsing radical ideas for systemic change and the need to support and encourage such voices.

### Audience

Advocates for systemic change

### On-the-ground actions from transcript

- Support and encourage celebrities and influential figures endorsing radical ideas (suggested)
- Advocate for systemic change in your community and beyond (implied)

### Whats missing in summary

The full transcript provides a deep dive into how influential figures can drive systemic change by endorsing radical ideas and the importance of supporting such voices in advocacy efforts.

### Tags

#SystemicChange #Advocacy #Influence #Support #Mainstream #Celebrities


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about Starlet, Britney Spears, Fran Drescher, and how they
relate to systemic change.
Don't laugh.
Don't laugh.
Today in 1961, the Soviets sent up Sputnik 10, what we call Sputnik 10 here in the US,
they call it something else over there.
On board Sputnik 10 was a mannequin and a dog, a puppy.
Dog's name was Starlet.
Starlet was given that name by Yuri Gagarin.
Yuri Gagarin was the first person in space, Starlet became the first dog in orbit.
Why were the Soviets sending up dogs?
At this point in time they had done it a number of times.
Why were they sending up dogs?
Scientists had already said they kind of felt a little bad because they weren't really learning
enough to justify doing it because not all the dogs made it.
So why were they doing it?
Because nobody cares about a mannequin.
Nobody cares about an inanimate object.
As interesting as space travel is, as interesting as that idea is, it's just a mannequin.
It's just something thrown out there.
Who cares?
What about with Starlet on board?
Well then it's interesting.
Then it's interesting.
It captures the imagination.
It helps get that message out to the general populace.
Starlets capture the imagination.
If you don't know, over the last day or two, Britney Spears and Fran Drescher have taken
to social media and seem to have endorsed some pretty radical ideas.
Ideas that would lead to systemic change.
Don't laugh because that is one of the responses.
You've seen two main responses to this.
One is a supportive yet joking, yes, queen of the proletariat.
I've got to admit I've made some jokes too.
The other is, man, I wish it wasn't them.
Why?
They're ideal.
They are ideal.
These are notable figures who are easily recognized within the mainstream and they are carrying
these ideas with them.
Radical ideas related to systemic change are scary by their very nature.
To have systemic change you have to give up the system.
You have to give up what you know in favor of the unknown.
The unknown is scary.
It's that simple.
It's less scary coming from Britney Spears, somebody who was America's sweetheart, than
it is coming from somebody else.
It's much less threatening.
Britney Spears isn't afraid of it.
Why should I be?
And it gets the idea out there.
Yeah, I mean, there's plenty of jokes to be made.
There is.
That's fine.
But make sure you are supportive of the idea and you're drawing attention to the idea.
I know there is the idea that we should be looking to our political leaders for this.
The reality is that's not where it's going to come from.
If you want systemic change, you're not going to get it from the system.
You can't expect that, even if they're blue.
If you want proof of that, look at that wonderful stimulus package.
Take a look at how much was designated to hospitals.
$130 billion during this mess.
Then take a look at how much was designated for major corporations.
The system told you how much it values you.
They put a dollar figure on it.
You're not important.
You are not important.
There are very few people who have the level of integrity necessary to get inside the system
and maintain their ideals.
The pressures of the system are immense.
Most people can't do it.
You cannot look to them for leadership, even if they're on your team.
It's not going to come from there.
It is not going to come from there.
However, it becomes easier to question, and it becomes easier to point out to those within
the mainstream, those who don't hold these radical ideas, who may never have been exposed
to them.
Britney Spears isn't afraid of the phrases, general strike or redistribution of wealth.
Why is Mitch McConnell?
If Fran Drescher can offer an open critique of capitalism, why can't Nancy Pelosi?
It helps carry the message.
You go to war with the army you have.
They enlisted.
Give them the support they need.
If you had told me 20 years ago that during the nation facing down the greatest threat
it has seen in a very long time, that I would be sitting at home watching Netflix and recording
a video to defend Britney Spears' political positions, I probably would have laughed at
you.
Because that's not how it should have worked.
I should have been in fatigues out there doing something, right?
We all have this image of how it's going to play out.
No plan survives first contact.
We've had first contact.
We've got to advocate for systemic change during this.
Because when we come out of this, it's the time to make it.
It's the time to push hard for it.
So we have to get these ideas into the mainstream.
And they're doing the job for us.
We need to offer them the support they need.
We've got to work.
This isn't a situation that I expected.
But if push comes to shove, yeah, I would be happy to be on the barricades with Britney
and Fran.
Absolutely.
We need to encourage more celebrities.
More people with influence that are outside of the system to introduce those radical ideas
to the general populace, to the mainstream.
We've got to.
It's going to be really important.
These ideas can't be new.
They have to have already heard of them.
Some of them are going to be necessary.
If we want the systemic change, we have to act like it.
We have to act like it.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}