---
title: Let's talk about Rule 303 and the Red Cross....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_QVMtNVloRM) |
| Published | 2020/03/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of taking action and responsibility, especially for those with the means.
- Mentions the impact of viewer participation through watching videos and live streams.
- Talks about the critical shortage of medical supplies, including blood products.
- Mentions the cancellation of blood drives due to restrictions on large gatherings.
- Encourages those who are eligible and able to donate blood through organizations like the Red Cross.
- Emphasizes the importance of following eligibility criteria and precautions before donating.
- Acknowledges that not everyone may be in a position to leave their house to donate.
- Urges individuals to help if they can, especially if they are universal donors.
- Stresses the global need for everyone to contribute during this pandemic.
- Concludes by encouraging people to take action and help in any way they can.

### Quotes

- "If you have the means, you have the responsibility."
- "We're in a global battle, and we all have to pitch in."
- "Desperately need the products."
- "Get out there, go do it."
- "We all want to help."

### Oneliner

Beau stresses taking responsibility and contributing, especially through blood donations, during the global battle against shortages.

### Audience

Donors

### On-the-ground actions from transcript

- Donate blood through organizations like the Red Cross (suggested).
- Follow eligibility criteria and precautions before donating (implied).
- Help by being a universal donor (implied).

### Whats missing in summary

The importance of community support and collective effort during times of crisis.

### Tags

#Responsibility #BloodDonation #GlobalBattle #CommunitySupport #CrisisResponse


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what you can do.
We've got a little rule 303 going on.
If you have the means, you have the responsibility.
This is not something that everybody's going to be able to do because not everybody has
the means.
It wouldn't be advisable for everybody to do this, but some people can.
Everybody wants to contribute right now and we're going to talk about a way that you can,
something that you can do.
A lot of you have contributed, even if you don't know it, just through participating
in this channel, watching the videos and participating in the live stream.
You've helped buy dial-a-flows and pressure infusers and all kinds of stuff.
I don't even know what it's for.
For medical professionals, that's happened.
We can't always track down all of the manufactured stuff that they need.
That's not something that we're able to do.
There's a shortage.
There's an old army saying, old military saying, if you're short of everything except for the
opposition, you're in combat.
I guess the new healthcare saying is that if you're short of everything but patients,
you're in a pandemic.
That's where they're at.
They have shortages.
One of the things that they need, that there's a critical need of, is blood products.
Because of the prohibitions and the advice against having large groups of people together,
a lot of blood drives got canceled.
Now, the Red Cross is still taking donations.
They're still doing it.
According to their website, they've enacted some more precautions, which would seem advisable.
You can go to their website, look at the eligibility, make sure that you meet it.
If you've had overseas travel, there's a lot of things that can prohibit you from helping
in this manner.
Some of you may not be in a situation where it would be advisable for you to leave your
house, so don't.
This is for those who can.
This is for those that have the means to participate and to help in this manner.
They're still taking donations.
They've added some precautions as far as disinfecting and stuff like that.
They desperately need the products.
We all have it, so if it's something you can do, get out there, go do it.
Get your selfie, post it on Instagram, and help in that way, especially if you are a
universal donor.
They desperately need you guys a lot.
We all want to help.
We're in a global battle, and we all have to pitch in.
This is one way that some people can help.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}