---
title: Let's talk about saving the economy by staycationing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3yrmLQb0chQ) |
| Published | 2020/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Economic pundits push the idea that measures taken are detrimental to the economy.
- Half-hearted measures will be ineffective and prolong economic slowdown.
- Acting decisively now will benefit the economy in the long run.
- Indecision and contradictory advice will destroy the U.S. economy.
- Flattening the curve is vital to prevent a prolonged economic downturn.
- Failure to support measures will prolong economic hardship and endanger lives.
- Uncertainty and flare-ups due to lack of action will hinder economic recovery.
- Action is needed, not just talk or tweets.
- Support staying at home measures for 21 or 30 days to curb the spread.
- Experts agree on the necessity of strict measures to protect society.
- Disregarding advice will harm the economy and people's lives in the long term.
- Temporary portfolio hits are a necessary sacrifice for long-term economic stability.
- Time is needed for treatments and preventative measures to be developed and tested.
- Collaboration and unity are key to overcoming the crisis.
- Failure to act now will lead to catastrophic consequences for the economy and lives.

### Quotes

- "Flatten the curve, not the economy."
- "Stay home. It doesn't matter what the president says."
- "If you want to protect the economy, support a 21 or 30 day, everybody stay at home."
- "Your portfolio during that period, yeah, it's going to take a hit."
- "This is how we buy enough time for treatments and preventative measures to be developed."

### Oneliner

Act decisively now to protect lives and the economy by supporting strict stay-at-home measures for 21 or 30 days.

### Audience

Policy makers, citizens, leaders

### On-the-ground actions from transcript

- Support a 21 or 30-day stay-at-home period to curb the spread (implied).
- Stay home to protect lives and the economy (implied).
- Act together with your community to prevent economic devastation (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of how decisive action and unity in supporting stay-at-home measures are necessary to prevent a prolonged economic slowdown and protect lives. 

### Tags

#Economy #StayAtHome #COVID19 #Unity #Action


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the economy because the pundits have seen their stock
market portfolios and they are just out in full force pushing the idea that none of this
is necessary.
That it's more detrimental to the economy than it is good for people.
That's not true.
I've said a couple of times that I don't care about the economy in regards to some of the
measures that are being taken.
The reason is taking these measures is good for the economy over the long haul.
The reality is this, if we only enact a half measures, if we only do this in a half-hearted
manner, it's going to be half effective, which means not effective at all.
It's not going to do any good, which is going to prolong the economic slowdown.
So let's run through a scenario.
Right now, everybody's aware of the problem.
And people are being told to kind of hang out at home, staycation.
If tomorrow the president comes out and says, hey, never mind, go on about your business.
And I head out to Disney World and there is a whole bunch of people that catch it there.
What happens?
Everybody goes back to staying at home.
And then the next time, those in authority say that everything's clear, it's all good,
go on out and resume your lives.
Nobody believes it.
So they stay home again.
So what you will end up doing is lengthening the period of economic slowdown.
We either have to act or deal with the fact that this indecision by the president, this
advice he's getting from people that are in no position to give advice, is going to completely
destroy the U.S. economy.
The rallying cry of flatten the curve, not the economy.
If we do not flatten the curve, if we do not act in a decisive manner to keep everybody
at home and isolated and not out there spreading this, it will flatten the economy for a longer
period than 21 or 30 days.
It will go on all year and into next.
This is what's going to have to happen.
It doesn't matter if you like it.
It doesn't matter what it does to your portfolio.
It doesn't matter.
If you do not support these measures, if you do not act, if you do not participate yourself
and you get out there and you just start spreading it around, then you're going to destroy your
portfolio, that which you really care about, since apparently human life is not a motivator
for you.
We all have to get behind this.
This is what it's going to take.
The idea that we can do it half-heartedly or the idea that any time the stock market falls, we can just call it off.
Number one, you're endangering people.
Number two, it's self-defeating because it will prolong the period of economic slowdown
because there will be that uncertainty.
There will be those flare-ups randomly.
That's not what we need.
We need action, not talk on Twitter.
You can't even commit to an idea on Twitter, maybe you're not really fit to lead a nation.
If you can't just stick to a concept, it's not like anybody's expecting the president
to go do anything anymore.
We're just, at this point, most of us are just hoping for him not to be an active harm.
This is what it's going to take.
This is what every expert in the field says it's going to take.
You can listen to them or yeah, you can listen to Fox News or Breitbart.
Who do you think has your best interest at heart?
Those who have shown that their interest is money or those who have spent their life trying
to figure out a way to protect society from this very threat.
Stay home.
It doesn't matter what the president says.
It doesn't matter what Fox News says.
It doesn't matter what Donald Jr. retweets.
If you want to protect the economy, support a 21 or 30 day, everybody stay at home because
that's what it's going to take.
Your portfolio during that period, yeah, it's going to take a hit.
It most certainly will.
It'll be a bad one.
But if you don't support it, you don't act on it, it's going to take that hit now and
then two months from now and then three months from now and it'll continue.
This is how we stop it.
This is how we buy enough time for treatments and preventative measures to be developed,
to be tested, to be put out there.
Because contrary to some other things on the president's Twitter profile, medicines have
to be treated or medicines have to be tested.
You just can't throw them out there.
We're going to have to act together and this is what it's going to take.
You can either get on board with it or you can be responsible for completely destroying
the US economy and people's lives.
Anyway it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}