---
title: Let's talk about what to do with your kids....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=n5c522V1MIs) |
| Published | 2020/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the challenges parents are facing with kids at home due to the current situation, with phones ringing off the hook.
- He stresses the importance of thinking like a teacher, having a schedule, planning activities, and being mindful of kids' short attention spans.
- Beau advises on watching children's diet and ensuring they get physical activity, even if they can't go outside.
- He suggests using YouTube for exercise programs and educational videos for crafts and science projects.
- Beau encourages involving kids in household tasks and repairs, viewing it as an opportunity to teach them valuable skills.
- He reminds parents to cherish the time spent with their children during this period and to be patient and understanding.
- Beau recommends being empathetic towards teachers who will have to handle many students once schools reopen.
- For older kids, he suggests having meaningful and nostalgic conversations, seeing the positive side of the situation.
- Beau underlines the importance of maintaining a schedule, limiting activities, ensuring exercise, and providing proper meals for children.
- He acknowledges the challenges of the current situation but motivates to make the best of it and cherish the time spent with kids.

### Quotes

- "Have a schedule and keep their activities limited to an hour or so and make sure that they get some exercise."
- "You got a chance to spend a lot of time with your kids and there will be a time later when you wish you could have spent more."
- "They're a little out of sorts too. So give them a routine."
- "But just keep them engaged and break it up by time periods."
- "This is the opportunity to show them."

### Oneliner

Beau shares practical advice on creating a routine, engaging kids in activities, and cherishing the time spent with them during challenging times.

### Audience

Parents, caregivers

### On-the-ground actions from transcript

- Involve your kids in household tasks and repairs, teaching them valuable skills (implied)
- Have meaningful and nostalgic conversations with older kids (implied)
- Maintain a schedule for children's activities and ensure they get exercise (implied)

### Whats missing in summary

Beau's warm and encouraging tone and the emphasis on finding positives in spending time with children during challenging circumstances.

### Tags

#Parenting #Kids #Routine #FamilyTime #Cherish


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about what to do with your kids.
Because I was talking to a friend of mine today who also works from home and who also
has a bunch of kids and his phone has been just like mine.
Ringing off the hook from people who, well, just haven't ever spent this much time with
their kids in a row like this.
There's a whole lot of people who have become extremely appreciative of teachers right now.
Okay, so I'm going to run through some things that I've learned over the years that might
help.
First think like a teacher.
Think like a teacher.
Have a schedule.
You're going to be doing this a while.
Have a schedule.
Plan out activities.
And remember that kids have short attention spans.
So try to limit it an hour or so and then change.
It helps keep them interested in what's going on.
The other thing to get out of the way right up front is watch their diet.
Don't let them eat all the junk food the first week.
Number one, they're going to drive you crazy the second week.
Number two, all that sugar is all bad.
That's not going to go well for you.
They also need physical activity.
Now you may not be able to run around outside depending on where you live, but YouTube is
your friend in a whole lot of ways during this conversation.
Yoga or any normal exercise.
They have programs on YouTube, clips for kids.
And let them do it.
Let them burn off some of that energy.
Aside from that, as you're breaking up your day, let them know what the schedule for the
week is.
So they're looking forward to certain events.
Their routine has been interrupted as well.
It's stressful for everybody.
And kids are extremely resilient, so it'll be okay.
But just remember, they're a little out of sorts too.
So give them a routine.
Tell them what you're going to do each day.
How it's going to work.
A lot of this is age dependent, but I'm trying to be as general as possible.
YouTube is also your friend if you don't know how to do any crafts or science projects,
stuff like that.
Kids enjoy that.
Something as simple as the little volcano, the baking soda thing.
They love it.
Just keep them engaged and break it up by time periods.
You probably want to get a lot of repairs and stuff done around the house.
Your honeydew list, that list of stuff that's just been sitting there the whole time.
There's no reason they can't help you with it.
You're right, it won't go as fast as it would if you did it on your own.
But number one, it's not like you're going anywhere.
You got time to kill.
Number two, one of the big complaints today is that our youth don't know how to do anything.
This is the opportunity to show them.
You can teach them how to make minor repairs around the house.
You can teach them to cook, depending on their age.
Rather than looking at this as some horrible event, remember, one of the few good things
that could come out of this, you're getting to spend time with your kids.
Something that most of us, during a normal work week, you probably feel like you don't
get to spend enough time with them.
Talk to them, spend time with them.
That's all they want.
Kids generally like their parents, most times.
Try to play into that.
Just incorporate them into what you're doing and be patient because they are children.
They are stressed too.
They're relaxed.
Also remember this, as time goes on and they go back to school, take it easy on the teachers
because not just are they dealing with yours, they got like 25 other ones.
For older kids, this may be the time to have a lot of those conversations, the Andy Griffith,
leave it to Beaver conversations that you never get to have anymore.
Not all of this has to be doom and gloom.
There's a lot of positive things that can come out of this and this is one of them.
But the key thing, have a schedule.
Have a schedule and keep their activities limited to an hour or so and make sure that
they get some exercise and make sure they're eating a decent meal.
I know given what was on the shelves, that may not be entirely possible.
It may not be great, but do as best you can.
Y'all have to make do with what we have right now and we will get through this.
But this, I don't think that this should be something that you're calling your friends
over.
You got a chance to spend a lot of time with your kids and there will be a time later when
you wish you could have spent more.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}