---
title: Let's talk about a staycation scavenger hunt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=o8LKPydwS0k) |
| Published | 2020/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Encourages a scavenger hunt to build bug out bags at home for emergency preparedness.
- Rules: only use items at home, nothing from existing kits, and items once in the bag stay in the bag.
- Goal is to increase the number of people prepared for emergencies.
- Participants to post photos of their bug out bags on social media under the hashtag BowBag for learning and sharing.
- Items in the bag should include food, water, fire, shelter, medicine, and a knife, with flexibility in interpretation.
- Beau will choose the best bag and send a reward.
- Reminder to continuously improve the bag for different emergency scenarios.
- Emphasizes the importance of being prepared for various emergencies.


### Quotes

- "We are all going to build bug out bags out of the stuff that we have at home."
- "We are going to exponentially increase the number of people who are prepared for an emergency."
- "Improving on it, in this scenario, yeah, everybody's staying at home. In the next scenario, you may not be able to."

### Oneliner

Beau encourages building bug out bags at home using only household items, fostering emergency preparedness for various scenarios and sharing for learning.

### Audience

Individuals at home

### On-the-ground actions from transcript

- Build a bug out bag using items at home and follow the guidelines provided (suggested)
- Share a photo of your bug out bag on social media under the hashtag BowBag (suggested)
- Continuously improve and update your bug out bag for different emergency scenarios (suggested)

### Whats missing in summary

Engaging in the scavenger hunt and building a bug out bag can enhance emergency preparedness and readiness for unforeseen circumstances.

### Tags

#EmergencyPreparedness #BugOutBag #HomePreparedness #CommunityAction #SocialMedia


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we are going to play a game.
We got like 130 million Americans stuck at home right now and untold millions more overseas.
We could probably all use a distraction.
And if we're going to play a game, it might as well be a learning experience and be productive.
If there is one thing I'm pretty sure that everybody in the world has learned over the
last month, it's that the government response may not always be what we would like it to
be.
And that we all need to be prepared to kind of hang in there for as long as we can by
ourselves.
So tonight and over the next few days, we're going to have a scavenger hunt.
And we are all going to make ourselves a little bit more prepared for the next scenario.
We're all going to build bug out bags out of the stuff that we have at home.
Now the rules to this are pretty simple.
You can only use stuff in your home.
Anything you put in the bag has to stay in the bag, so if you put a can opener in it,
it can't be the one from your kitchen that you always use.
The reason for this is, one, we want these bags to stay together.
So we are going to exponentially increase the number of people who are prepared for
an emergency.
The other part of this is that you often, when you talk to people about this, what they
will tell you is, well I have all of this stuff, but do you have it together?
Because in an emergency, it's got to be together.
You don't have time to do what we're going to do now.
So that's the first rule.
Second rule is, you can't use your kit.
If you already have one, you can't use that one.
You got to build another one.
I'm not going to, and I'm not going to use any of the stuff that I have.
You got to build one from scratch.
Now if somebody wants to be generous and wants to post a photo of the kit that they already
have put together so people can get a better idea of what should be in one, that's awesome.
That's awesome.
We are all going to take photos of our scavenger hunt results and post them on Twitter or Instagram
or wherever under the hashtag BowBag.
So B-E-A-U B-A-G.
That way everybody can scroll through and it's a learning experience.
We all get to see what other people put in theirs.
That way we can all learn from it.
So be prepared to explain anything weird that you stick in yours.
What's our list?
What should be in it?
The short answer, the simple answer is food, water, fire, shelter, medicine, and a knife.
But food doesn't really just mean food.
Food could be a fishing kit.
It could be rope for snares.
It could be a can opener.
Water could be water purification tablets.
Fire could be matches.
A lighter, also a flashlight falls under that category.
Shelter, yeah it could be a tent or it could be a poncho.
Medicine includes a first aid kit if you have one.
Knife could be a multi-tool or anything else.
And yes, you can go above and beyond and throw in a radio or anything else you think you
might need.
So it should be fun and it should be a learning experience for everybody.
Here in a few days I will scroll through the hashtag and whoever I think put together the
best one I'm going to send you something.
And then the final rule is, you know, it's all going to stay together, but improve on
it.
Improve on it.
In this scenario, yeah, everybody's staying at home.
In the next scenario, you may not be able to.
Emergencies happen.
It could be a hurricane.
It could be a wildfire.
It could be an earthquake.
It could be a flood.
You may have to evacuate.
So this is going to give you a leg up.
This is going to help, hopefully, a whole bunch of people become a whole lot more prepared.
And it'll be fun.
And we'll all learn something.
I don't really have a closing thought tonight, so ready, set, go.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}