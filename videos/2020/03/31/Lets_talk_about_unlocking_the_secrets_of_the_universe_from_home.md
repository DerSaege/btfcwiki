---
title: Let's talk about unlocking the secrets of the universe from home....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M8JR9AqyJ6g) |
| Published | 2020/03/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a science project where people can help unlock universe secrets from home during this time of being stuck indoors.
- Mentions the project, Galaxy Zoo, which involves looking at pictures of galaxies and answering questions to aid researchers.
- Describes the multiple choice format and provides a field guide for understanding the task.
- Talks about the excess of information captured compared to our ability to process it, making contributions valuable.
- Expresses how engaging in this project can provide an escape to space for individuals feeling stressed or bored.
- Emphasizes that participating helps researchers understand galaxy evolution, internal workings, interactions, and merges.
- Acknowledges the present anxiety and suggests this project could offer a positive distraction and something to look forward to.
- Notes the lack of immediate practical application but anticipates its future relevance.
- Encourages staying positive and looking towards the future, as humanity has overcome challenges before.
- Concludes with well wishes for the viewers.

### Quotes

- "What better place to escape to than space?"
- "Maybe there are some people out there that need something to look forward to, need something a little bit less real to look at."
- "We are a very resilient species."

### Oneliner

Beau introduces Galaxy Zoo, a project enabling people to unlock universe secrets from home during difficult times, offering a space-based escape and a positive distraction while aiding researchers in understanding galaxy evolution and interactions.

### Audience

Science enthusiasts, Space lovers

### On-the-ground actions from transcript

- Join Galaxy Zoo project to assist researchers in understanding galaxies (suggested)
- Utilize spare time at home to contribute to the Galaxy Zoo project (implied)

### Whats missing in summary

The full transcript provides additional details and Beau's unique perspective, encouraging viewers to participate in an engaging and educational space project from the comfort of their homes.

### Tags

#Science #Space #GalaxyZoo #Research #Volunteer


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a science project and how you can help unlock the secrets
of the universe from home, right now.
I would imagine that there's a whole lot of people that are pretty bored right now, stuck
at home, can't go anywhere, can't do anything, probably a little stressed, the numbers are
getting higher, so the anxiety level is probably getting higher, might have kids you want to
entertain, this will help with that and be educational.
And most people may just need an escape.
What better place to escape to than space?
And that is where Galaxy Zoo comes in.
I'll put the link down below because that's not the link, but the project, it's been around
for 10 years, and your part in it is to look at pictures of galaxies and answer questions
about them.
It's multiple choice, and there's a little field guide that helps you learn a little
bit about what you're doing.
Your information gets compiled with a computer's analysis, and it helps the researchers understand
it better.
Basically, our ability to capture information and get these photos far exceeds our ability
to process it.
So you would be helping out with that.
Now, other than killing time with something that, once you get the hang of it, is actually
kind of like a little monotonous.
Other than that, what good is it?
Well you're helping these researchers, and what they're trying to do is literally understand
how the universe works.
No small feat, but maybe right now people need something a little fantastic to get involved
in.
The information helps them understand how galaxies evolve, how they change, how they
work internally, how they interact with each other, how they merge, and you'll get to see
pictures of all of this.
It's cool.
I mean, I did it for probably half an hour earlier tonight, but you know, everybody right
now focused on the present, and the present is full of a lot of anxiety.
Maybe there are some people out there that need something to look forward to, need something
a little bit less real to look at.
I don't know that there's any immediate practical application to this, but I bet there will
be in the future.
And there may be some people out there that need to be thinking about the future, need
to stay positive, and understand that we'll get through this like we have everything else.
We are a very resilient species.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}