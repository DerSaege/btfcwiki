---
title: Let's talk about The Time Machine, class, and a different reading....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Qb1ClxPq2A8) |
| Published | 2020/03/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the topic of H.G. Wells' "The Time Machine" and delves into the theme of class.
- Describes the two distinct groups in the book: the Eloi, portrayed as innocent and leisurely, and the Morlocks, depicted as strong and skilled.
- Examines how the Morlocks essentially serve the Eloi and are implied to consume them.
- Draws parallels between the book's class divide and real-world global economic disparities.
- Points out that many viewers are likely part of the global elite, akin to the Eloi, and contrasts their challenges with those faced by the working class in less economically advantaged regions.
- Raises concerns about the impact of production and consumption patterns on climate change, particularly affecting countries in the global south.
- Advocates for global working class solidarity and responsible decision-making to prevent environmental destruction.
- Concludes with a call to action for a more equitable world where everyone's needs are met without harming the planet.

### Quotes

- "We are Eloi."
- "We talk about working class solidarity a lot."
- "We shouldn't have Morlocks."
- "It is a global game."
- "We have the means to make sure that everybody has what they need without destroying the planet."

### Oneliner

Beau dives into H.G. Wells' "The Time Machine" to draw parallels between class divisions in the book and global economic disparities, advocating for solidarity and responsible decision-making to prevent environmental destruction.

### Audience

Global citizens

### On-the-ground actions from transcript

- Advocate for fair labor practices and support initiatives that empower workers globally (suggested)
- Educate oneself and others on the impacts of global economic disparities on climate change (implied)

### Whats missing in summary

Insights on how individuals can actively contribute to global solidarity and sustainability efforts.

### Tags

#ClassDivide #GlobalSolidarity #ClimateChange #EconomicDisparities #SocialResponsibility


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about The Time Machine, H.G. Wells, the book.
And because we're going to talk about The Time Machine, we've got to talk about class.
But this isn't going where you think it is.
So even if you've read the book, stick with me.
If you haven't read it, I'm going to give you a general overview of the plot.
It's not going to be enough to ruin the read though.
You can still read it after this and enjoy it.
Okay so there's a time traveler and he decides he's going to go into the future.
And by into the future, I mean way into the future.
Like 800,000 years into the future.
When he gets there, he meets this group of people called the Eloi.
And they're short and they're childlike, they're innocent, they lead a life of leisure.
They are also pretty much incapable of doing anything.
They're ineffective, they have short attention spans.
They're just not very bright.
And he starts to think, man, this is how humanity evolved.
Because they didn't have any challenges after a while.
It's interesting.
As he continues to explore this world, he begins to wonder, who built these buildings?
Who makes their clothes?
Because it's not these people.
They're incapable of that.
They don't have it in them.
And he continues to explore and he finds this ventilation shaft.
He goes down it and he meets another group of people called the Morlocks.
They're different.
They're big, they're strong.
They have skills, they're very adept.
Doesn't take them long to realize that they're producing everything that the Elois need.
And that occasionally they are quite literally eating the rich.
Because he's theorizing that the Morlocks, those living underground, literally under
servitude, they're the working class.
And the Eloi, well, they're the wealthy.
They're the global elites.
And this is how they evolve over time, eventually split into two different species.
It's a cool story.
Which one are we?
Most people watching this right now, I'm a Morlock, I'm working class.
Yeah, but only if we limit it to our own home country.
Globally, what are you?
We're Eloi.
At $32,000 a year, you become part of the global 1%.
We're Eloi.
Most of the people watching this are Eloi.
Now that's not to say that the working class in the United States don't face challenges,
don't have problems.
Of course we do, we all do.
And that's not to say that everybody watching this is an Eloi, is part of that super global
elite making $32,000 a year or more.
But even if you're not, if you're watching this, you're probably still an Eloi.
Even if you're not a 1%.
You've got problems, you have challenges.
But your problems and challenges are very different than the real Morlocks down there
in the global south in those countries we never think about.
Those countries that are a little less well-off, those countries that have been exploited a
little bit more.
That's where the real Morlocks are.
Most of us in countries that are economically powerful, we're Eloi.
We are Eloi.
And the thing about it is, already they produce everything for us.
And there's nothing inherently wrong with this, with labor transferring its goods.
There's nothing wrong with that in and of itself.
Distribution is what it is.
But the way that production, the way that production occurs and the distribution occurs,
well it's accelerating problems, right?
And we talk about it all the time.
We talk about climate change.
And climate change is going to hit the global south real hard.
I read tonight that experts are advising those in the global south to start building their
cities underground.
Because it'll be cooler.
They can still be down there and produce for us Eloi.
And we won't have to deal with those refugees.
Man, that's a lot to swallow, isn't it?
The decisions that we make here in Eloi territory are also the decisions that determine whether
or not their home gets destroyed, because they are producing for us.
The consumption is ours.
The production occurs over there.
But it's to feed our demands, the Eloi's demands.
And that's what's going to drive them underground.
It's what's going to destroy their home.
We talk about working class solidarity a lot.
I don't know if we've ever used those terms on this channel, but the themes are definitely
there.
We need to remember that this is a global game.
It is a global game.
We shouldn't have Morlocks.
They shouldn't exist.
We are a very resilient species.
We have the means to make sure that everybody has what they need without destroying the
planet.
Now I don't think, I think it would be a pretty big stretch to imagine a scenario in which
the global south and us Eloi's diverge into different species over time.
And I don't think that they, you know, literally want to eat the rich.
But if this goes on, their homes get destroyed, I imagine they wouldn't mind seeing us stuffed.
And I can't say that I blame them.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}