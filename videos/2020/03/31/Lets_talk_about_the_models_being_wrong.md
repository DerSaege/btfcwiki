---
title: Let's talk about the models being wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CkU9IDBgl2w) |
| Published | 2020/03/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about mistakes and misinterpreting data, leading to a lack of understanding of ongoing events.
- People comparing initial predictions with current ones, dismissing the effectiveness of social distancing.
- The importance of acknowledging that social distancing is working in lowering the number of cases.
- Criticizing the president for extending social distancing but not admitting his initial models were incorrect.
- Emphasizing the necessity to continue social distancing to prevent overwhelming medical infrastructure.
- Warning against disregarding evidence and blindly supporting a specific person, likening it to cult behavior.
- Encouraging people to prioritize public health over personal beliefs and political allegiances.
- Reminding everyone to follow safety precautions like washing hands, not touching faces, and wearing masks.

### Quotes

- "The desire to set aside facts because you want to support a specific person is indicative of cult behavior."
- "You made a bad choice. Everybody makes mistakes."
- "If you disregard these precautions, you're only going to hurt yourself and those people you know."
- "Is that level of selfishness and willful ignorance making America great?"
- "Y'all have a good day."

### Oneliner

Beau talks about the importance of acknowledging that social distancing works, despite political biases, and warns against disregarding evidence for personal beliefs.

### Audience

General public

### On-the-ground actions from transcript

- Stay at home, follow social distancing guidelines, and wear a mask when going out (implied).
- Wash hands frequently and avoid touching your face (implied).

### Whats missing in summary

The emotional impact of realizing the necessity to prioritize public health over personal beliefs and political allegiances.

### Tags

#SocialDistancing #PublicHealth #CultBehavior #SafetyPrecautions #CommunityLeadership


## Transcript
Well, howdy there internet people, it's Beau again.
So it is quite literally the calm before the storm here, so you may hear some rain on the
roof, but that's kind of fitting, isn't it?
So today we're going to talk about mistakes, misinterpreting data.
And because you've misinterpreted the data, well, then you don't understand other things
that are happening.
So we're going to talk about today, because there are a lot of people doing this right
now.
There is a widespread inclination among some people to look at the current predictions
and current models and compare them to the initial ones and say that those initial ones
were way off base, they were way wrong.
See, it was overhyped, it was sensationalized, it was a media hoax.
Because they want to believe the president.
That's what they want.
So they're going to disregard information that is plain as day.
The models are getting better, the predictions are getting better.
Right now I think the current one is we're looking at 100,000 gone.
It's hard to think of 100,000 gone as good.
But in comparison to a month ago, that's really good.
We're doing a great job, right?
But then there's that other thing, and that's that the president is extending social distancing.
A man who never admits that he's wrong, accepts responsibility for absolutely nothing, has
pushed back his date.
Why?
Because the initial models weren't that wrong.
They weren't off base.
They were good estimates.
And social distancing is working.
That's the point.
That's why we're staying home.
That's why we're self-isolating, is to lower the number of people that get it.
And it's working.
Therefore, the president extends how long we're doing it, because it's working.
The desire to set aside facts and to set aside things that's plain as day, because you want
to support a specific person, and you want to believe that they're never wrong, that's
indicative of cult behavior.
You really need to check yourself.
It's probably going to extend maybe a little bit longer than the current date, but it's
because it's working.
It's saving lives.
That's why we're staying home.
The whole idea was to flatten the curve.
And we have, in a way, it has dropped significantly.
But we're still at the point where at peak, it may overwhelm medical infrastructure.
And that's what we're trying to avoid.
So we've got to get it down again.
We've got to keep pushing it down.
If we let up now, there's still enough cases out there to reinitiate that climb, and therefore
get over the point that hospitals can handle.
That's why it's being extended.
The president isn't going to look at these models that keep dropping and say, I can show
that I was right right now, but instead I'm going to postpone the reopen.
That doesn't even make sense.
The only reason that you can look at this and think it was overblown is if you're disregarding
evidence and you're disregarding evidence because you have fallen for a cult of personality.
He's not a great leader.
He's not.
He's not going to make America great.
His accomplishments are nil.
You got to let it go.
You made a bad choice.
Everybody makes mistakes.
Get back to, well, I don't know.
I'm not a particular fan of Hillary either.
But the faith in the red hat and that slogan, it's misplaced.
If you disregard these precautions, if you don't follow them, you're only going to hurt
yourself and those people you know, those people in your community.
Is that level of selfishness and willful ignorance making America great?
Anyway, wash your hands.
Don't touch your face.
Stay at home.
If you do go out, wear a mask.
If you have a beard, wearing an N95 with a beard is basically like putting fishnets over
your head.
It doesn't do any good.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}