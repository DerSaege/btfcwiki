---
title: Let's talk about what $1200 checks can teach us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NeE6LslIAoQ) |
| Published | 2020/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the $1200 stimulus checks and the reasoning behind the amount.
- Exploring the challenges faced by minimum wage workers in the United States.
- Addressing the difficulty in garnering empathy for minimum wage workers.
- Pointing out the propaganda surrounding minimum wage workers and the need for self-interest to drive change.
- Revealing how taxpayers end up subsidizing big businesses that pay low wages.
- Providing historical context on the minimum wage and Franklin Roosevelt's perspective.
- Arguing for a living wage for all Americans, not just a subsistence level.
- Emphasizing the importance of decent wages for all workers, especially those deemed "essential" during the current crisis.

### Quotes

- "You're paying for it. You are providing a subsidy to big business out of your pocket."
- "Minimum wage workers in this country are being exploited."
- "Everybody who's working should have a decent wage."
- "Essential employees [...] I think they deserve decent wages."
- "This is something we might want to look at."

### Oneliner

Analyzing the $1200 stimulus checks, the challenges of minimum wage workers, and the need for decent wages for all Americans, especially during crises.

### Audience

Taxpayers, Activists, Workers

### On-the-ground actions from transcript

- Advocate for fair wages for all workers (implied)
- Support policies that ensure a living wage for all Americans (implied)

### Whats missing in summary

Historical context and specific examples

### Tags

#StimulusChecks #MinimumWage #LivingWage #DecentWages #Workers'Rights


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about that stimulus check.
Twelve hundred bucks.
See I think I know where they came up with that number.
They were worried about giving out too much money.
Don't want to make the commoners lazy and all.
So they were worried about that.
So twelve hundred dollars.
When you talk to people about that twelve hundred dollars you mainly get two reactions.
You get one that is twelve hundred dollars, alright.
And you get one that's twelve hundred dollars.
You can't survive on that.
You can't feed your family on that.
I feel you.
Minimum wage in the United States is seven dollars and twenty-five cents an hour.
That's the federal minimum wage.
At that level, working forty hours a week, what do you make a month?
One thousand one hundred sixty bucks.
Before taxes.
Can't survive on that.
Can't feed your family on that.
I feel you.
It is hard to get people to care about minimum wage workers for two reasons.
Three really.
The first is that it's a small percentage of people who make exactly minimum wage.
That's a small number of people.
However, if you were to say bump it up to seven fifty or seven seventy-five, well that
number grows a whole lot.
Another reason is that there's a whole lot of propaganda about minimum wage workers.
I don't know why you would need to produce propaganda against people that have no power.
Doesn't make a whole lot of sense.
Unless of course, what you're doing is so immoral and so against the character and tradition
of this country that normal people would speak out against it if they really got what was
going on.
That could be a reason.
And then the other thing is the standard thing.
It's hard to get people to care about something if they don't have any self-interest in it.
So let's just go ahead and give everybody that self-interest.
If you're one of those people who looks at a twelve hundred dollar check and is like,
man, that's not going to help much.
That's not going to put a dent in it.
If you're one of those people, you're definitely the person I'm talking to right now.
Because you're paying for a whole lot of minimum wage workers.
See at that level, somebody working forty hours a week, and understand this is even
higher than $7.25 if you go up, this still applies because our wages are that low in
this country.
Somebody who's working forty hours a week at that level, they still qualify for government
assistance.
You're paying for that through your tax dollars.
You're paying for it.
So basically what's happening is you cover part of their housing.
You cover part of their food bill.
So the company who is exploiting their labor, well they don't have to.
They can pay them so little that they qualify for government assistance even if they're
working full time.
Don't worry, you'll pick up the tab.
There is your self-interest.
If the moral reasons don't do it for you, there's your self-interest.
You are paying for it.
You are providing a subsidy to big business out of your pocket.
Now anytime the subject of minimum wage comes up, there's always an argument that's thrown
out and I'm going to go ahead and just get rid of that now.
Minimum wage came around in 1938 under Franklin Roosevelt.
What did he have to say about wages?
No business, which depends for existence on paying less than living wages to its workers,
has any right to continue in this country.
He goes on.
By workers I mean all workers, the white collar class as well as the men in overalls.
So it wasn't just for the upper middle class, it was for everybody.
He goes on.
And by living wage I mean more than a bare subsistence level, I mean the wages of a decent
living.
The idea was to make sure that everybody who was working had a decent life.
I am 100% certain that the intention was not to have somebody working 40 hours a week and
then have to go apply for government assistance with the stigma that that carries today when
they're not doing anything wrong.
They're putting in labor that has to be done.
We have all learned that many things that are essential are pretty low paying jobs.
So the idea that minimum wage is just for teenagers or whatever, it's not true.
It's not grounded in fact, it's not grounded in reality, that is not what it was for.
It was to provide a living wage for all Americans.
Everybody who's working should have a decent wage.
I think I made an error.
Because what's happened is it became very easy to illustrate what minimum wage workers
make because they kind of chose a minimum wage number.
Using $1200 made it very easy to highlight how much the minimum wage workers in this
country are being exploited and the fact that those who look at a $1200 check and are kind
of like, man, that's not going to help, you're subsidizing it.
When we come out of this we're going to have the opportunity to change a lot of infrastructure.
This is something we might want to look at.
These essential employees, these essential workers who are out there literally risking
their lives right now, I think they deserve decent wages.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}