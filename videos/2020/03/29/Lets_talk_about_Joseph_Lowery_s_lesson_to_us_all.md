---
title: Let's talk about Joseph Lowery's lesson to us all....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t3U34uh2Yxk) |
| Published | 2020/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing the topic of Joseph Lowery, a significant figure in civil rights history who deserves more attention from the media.
- Lowery played a key role in the Civil Rights Movement, including during the Montgomery bus boycott and the Selma to Montgomery march.
- Despite his major contributions, Lowery's legacy may not receive the comprehensive recognition it deserves.
- Lowery's impact extended beyond racial equality as he advocated for other marginalized groups, like the LGBTQ+ community.
- He used his influence and privilege to amplify the voices of others, showcasing what it truly means to be a good ally.
- Lowery's approach involved supporting and empowering others rather than seeking the spotlight for himself.
- His actions exemplify the essence of being a supportive ally and helping others gain power and agency.
- Beau underscores the importance of understanding Lowery's broader impact on American civil rights, not just within the black community.
- Lowery's legacy is a reminder of the power of quiet, behind-the-scenes advocacy and support for marginalized communities.
- Beau encourages the media to ensure that Lowery receives the recognition and memorial he deserves, portraying him as the "Where's Waldo of American civil rights."

### Quotes

- "He provided the nation with a shining example of what it means to be a good ally."
- "He understood what it meant to be a good ally, to help elevate others."
- "That's why he's the dean of American civil rights rather than just the dean of black civil rights."

### Oneliner

Beau sheds light on Joseph Lowery, a pivotal figure in American civil rights history, showcasing the essence of being a supportive ally and amplifying marginalized voices.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Honor Joseph Lowery's legacy by amplifying the voices of marginalized communities (implied)
- Advocate for civil rights and equality for all, following Lowery's example of being a supportive ally (implied)

### Whats missing in summary

The emotional depth and nuances of Lowery's impactful legacy beyond mainstream recognition.

### Tags

#JosephLowery #AmericanCivilRights #Allyship #MarginalizedCommunities #Recognition


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about Joseph Lowery.
Now, if there's any justice in the world,
the media's gonna be talking about this too.
There was a little coverage last night,
but not enough in my opinion.
Not for somebody whose legacy is as big as his.
So we're gonna talk about him,
we're gonna talk about what he did,
we're gonna talk about what the media's gonna talk about,
and then we're gonna talk about
what they're probably not gonna talk about,
because what they're not going to talk about
probably contains the biggest lesson,
the most valuable lesson for the most people.
So he was a pastor, and he burst onto the scene
during the Montgomery bus boycott, Rosa Parks.
That's when he really came into his own.
He was part of the Selma to Montgomery march,
and if you're not familiar with that,
it was a march from Selma to Montgomery
to speak with Governor George Wallace of Alabama.
This march was, to put it nicely,
interrupted by Alabama State Troopers.
And by interrupted, I mean it became known as Bloody Sunday.
He went on, and this is a man
who hung out with Martin Luther King.
He helped build the coalition of churches
that became the infrastructure, the backbone,
of the Civil Rights Movement, especially in the South.
He spoke at President Obama's inauguration.
Kind of a cool bookend there, right?
It was part of a march that was attacked
and he was going to speak to a man who said,
segregation now, segregation tomorrow,
and segregation forever, and spoke at Obama's inauguration.
It's pretty clear who won that fight.
This is what the media's gonna talk about.
This is what they're gonna mention.
This is what they're gonna say.
Hey, this is why this guy's great.
This is why we should remember him.
And all that's true, yeah, absolutely,
but he wasn't the dean of black civil rights.
He was the dean of American civil rights.
Because as if helping to end segregation wasn't enough,
he didn't stop there.
He kept going.
Because of his actions in the 50s and 60s,
he had a lot of power.
People knew him.
He had a lot of privilege,
especially when it came to churches in the South.
And he used that privilege to help elevate other voices.
He spoke in favor of civil unions and marriage
for people who couldn't get married.
But he never tried to take the lead.
He never tried to take over their movement.
He knew that they would be able to speak best for themselves.
So he drew attention to it,
but for the most part, he just handed them a megaphone,
helped them get to where they could speak about it.
He didn't want to be the star in the room,
even though he was a historical figure.
Even when he was alive, he was a historical figure already.
But he knew when it was time to take a back seat
and let others drive, and he did it.
He provided the nation with a shining example
of what it means to be a good ally.
Now, for all I know,
he did this for other marginalized groups.
I just happen to know about his work for gay rights
because I live in the South.
But for the most part, we shouldn't know about it
because he did it quietly.
He did it behind the scenes.
He helped where he could.
He helped elevate their voices.
So he didn't become the focal point.
He made sure that he was able to help them
gain the power themselves,
not get rights under his auspices.
He understood what it meant to be a good ally,
to help elevate others.
That's why he's the dean of American civil rights
rather than just the dean of black civil rights.
Now, even with all the coverage of the major topic today,
I would hope that major media outlets
provided a little bit of a memorial for him.
But just in case they don't, there you go.
That's who he was,
the Where's Waldo of American civil rights.
He's there pretty much everywhere.
You just got to look for him.
Anyway, it's just a thought. Y'all try to have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}