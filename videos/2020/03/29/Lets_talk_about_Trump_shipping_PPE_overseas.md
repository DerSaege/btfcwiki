---
title: Let's talk about Trump shipping PPE overseas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hfyVDydFETo) |
| Published | 2020/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses his recent decision to not talk much about Trump, focusing instead on the immediate problems at hand during the current crisis.
- He questions the administration's decision to provide protective equipment to foreign countries while medical professionals in the US are struggling to find the same equipment.
- Beau expresses curiosity about what Trump gained in return for sending protective equipment abroad, suspecting personal benefits rather than altruistic motives.
- He criticizes Trump for not following established crisis management plans and for spreading false information that endangers public health.
- Beau questions the media's continued coverage of Trump despite his misleading statements and lack of leadership.
- He warns about the danger of false hope and misinformation during a crisis and urges people to listen to experts rather than Trump.
- Beau concludes by urging listeners to stay informed and trust knowledgeable sources during these challenging times.

### Quotes

- "The president of the United States is a danger to the people of the United States."
- "False hope is extremely dangerous in a situation like this."
- "His desire to paint this rosy picture is harmful."
- "We have to wonder why."
- "Listen to the people that actually know what they're talking about."

### Oneliner

Beau questions Trump's motives in sending protective equipment abroad, criticizes his lack of leadership, warns about the danger of false information during a crisis, and urges people to trust knowledgeable sources.

### Audience
Community members

### On-the-ground actions from transcript

- Listen to experts and trusted sources for accurate information (implied)
- Stay informed about the situation from reliable sources (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's actions during the crisis and urges people to prioritize accurate information from experts.

### Tags

#Trump #COVID19 #Leadership #Media #PublicHealth #CommunitySafety


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump, protective equipment, aid, and the media.
Okay so you may have noticed I've kind of laid off Trump recently.
Haven't been talking about him much.
It's not that I think he has enough on his plate, it's not that I think that in a time
like this you can't criticize the president, it's not that I think he's doing a good job,
it's that it doesn't matter right now, we have to focus on the problem at hand.
We can talk about mistakes and what could have been done afterward.
Right now, most immediate concern first.
That being said, it has become a news story that the administration has provided the very
protective equipment our docs and nurses, medical professionals, are struggling to find
to more than 25 foreign countries.
The very protective equipment I have spent weeks scrounging, bagging, borrowing, stealing,
trying to get for local hospitals.
Okay, I'm actually not mad about that.
I'm not.
In hindsight, yeah, probably wasn't the wisest decision.
At the same time, when the decision was made, it was probably in an attempt to do exactly
what I said.
Focus on the most immediate problem first.
I get that.
I'm not going to trash him for that.
But I've got questions, a bunch.
First I want to know what he got for them.
The president is not known for doing things out of the goodness of his heart.
I want to know what he got for them.
I don't think the man that is telling US governors that they need to be nice to him, that they
don't really need the ventilators, that they don't need the equipment, that I don't think
that he's holding them to a higher standard than he is for heads of state.
So I want to know what he got because it does not appear that the US got anything for him.
So I have to assume he got something personally.
And I want to know what it is.
Whether it's just his ego being stroked, whether it's promises of future hotel deals, whatever
it is, I want to know.
I don't think he just did this out of the goodness of his heart.
We do have to entertain the possibility that it was just deep state functionaries doing
their jobs.
We do have to entertain that possibility.
But I'm willing to bet approval was given.
I want to know what he got.
I don't think that the man who's saying, don't even call that woman, is shipping goods, protective
equipment, to countries that he has not referred to in polite terms.
I don't think he's shipping that stuff just to be a good person.
So we also have to talk about something else.
This isn't leadership.
The president is not showing leadership.
I've had a bunch of people ask me now that it's come out that yes, there was this playbook,
how I knew about this top secret plan.
It's not top secret.
I mean, that one is.
It's marked top secret.
These plans have existed since the 1960s.
The newest plan is top secret.
But they've existed.
These playbooks have existed this entire time.
He's not following them.
Makes it very hard to predict what he's going to do because he's not following the best
practices as developed over 50 years because he thinks he knows better.
Because of his infinite wisdom and his obvious mass of knowledge on this particular subject.
At this point, I have to look at the media.
I gotta ask, why are they still airing him?
I understand that the standard protocol is to give air time to the president.
He is the president after all.
But at this point, he's a danger to the United States.
Supplies are on their way.
No they're not.
They're not.
I mean, maybe to other countries.
But they're not going where we need them here in the US.
He's haggling over them.
There will be tests in every big box store.
But there's not.
A vaccine is right around the corner.
Be here soon.
No, it's not.
It is not.
We have great cures.
Treatments.
No, we don't.
We don't.
We have symptom management.
That's what we have.
This is dangerous because people who believe this stuff, they're not taking the precautions
they should.
The president of the United States is a danger to the people of the United States.
His desire to paint this rosy picture is harmful.
False hope is extremely dangerous in a situation like this.
Saying stuff not based in science, stuff that isn't evidence based, is very dangerous.
But the media continues to give him a platform to lie to the American people over and over
and over again.
Focus on the people that know what they're doing.
Because he doesn't.
He's going to, he already has, gotten people killed.
I want to know what he's got, what he got for it.
That's the one thing that we should all be asking at this point.
Because there's no way he's holding US governors to a higher standard than foreign heads of
state.
And it's unlikely that he cares about foreign lives more than he does American lives.
Because honestly, I don't think he cares about anybody's life.
He cares about his ego.
He cares about his image, his legacy.
And he's tied that legacy to the economy.
So that's what he cares about.
If he comes out and says, everybody go back to what you were doing on Easter, you need
to stay at home until you hear from the docs.
Because he doesn't know what he's doing.
And he's not following the plans.
We have stockpiles.
We have strategic reserves of all of this stuff.
Why isn't it being released?
It's in the playbooks.
And if it is being released piecemeal, we have to wonder why.
Is he not getting enough?
What exactly are his demands as he holds the nation hostage?
Anyway, it's just a thought.
Y'all try to have a good day.
We'll get through this.
Listen to the people that actually know what they're talking about. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}