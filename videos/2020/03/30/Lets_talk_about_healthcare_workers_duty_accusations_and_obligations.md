---
title: Let's talk about healthcare workers, duty, accusations, and obligations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b-oOpAX3ZUw) |
| Published | 2020/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing duty, obligations, and accusations in relation to supporting healthcare workers.
- Comparing healthcare workers to troops on the front lines.
- Criticizing the lack of support healthcare workers receive.
- Drawing parallels to past scandals where troops lacked necessary equipment.
- Calling out government leadership for blaming frontline workers instead of taking responsibility.
- Challenging accusations without evidence.
- Questioning the hoarding of ventilators by hospitals.
- Encouraging the release of unused ventilators for those in need.
- Urging leadership to lead by example and prioritize the needs of healthcare workers.
- Expressing concern over healthcare professionals contemplating quitting due to lack of support.
- Emphasizing that healthcare workers care about their job and the people they serve.
- Suggesting that those dedicated to their duty are the ones needed most during challenging times.

### Quotes

- "It should be a national scandal that we are sending our troops to the front lines without their armor."
- "When it's over, they're not quitting because of the risk. They're quitting because we're not giving them the support they deserve and they need."
- "Lead from the front, sir. Set the example."
- "When this is over, I am so done. I am out of here after this."
- "But see, the thing is, when it's over, they probably won't quit because they actually care about their job."

### Oneliner

Beau stresses the duty and support owed to healthcare workers on the front lines, likening them to troops without proper armor.

### Audience

Healthcare advocates

### On-the-ground actions from transcript

- Release unused ventilators for those in need (suggested)
- Advocate for better support and resources for healthcare workers (implied)

### Whats missing in summary

The emotional impact conveyed by Beau's plea for proper support and recognition of healthcare workers dedicated to their duty.

### Tags

#HealthcareWorkers #Support #Duty #Obligations #Frontline #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about duty, obligations, and accusations.
And supporting our troops, and by our troops I mean our healthcare workers.
And that's not really a stretch of the imagination to call them that.
It's not an exaggeration.
Right now they're in the fight.
They're on the front lines.
They're the ones taking the risk.
And they're not getting the support they need, they deserve, from areas where they should
always be able to count on the support.
Instead they're catching the blame from them.
Y'all remember 2003, 2004?
Soldiers, our troops, literal troops, buying their own armor before they went over, their
own optics, stuff like that, was a national scandal.
Was a national scandal.
Because it's appalling to send our troops to the front line without the armor they need,
without the protective equipment they need.
And instead of it being a scandal, painting the government in an incompetent light right
now, the government, the leadership, is trying to pass the blame down to those on the front
line while they're headed out the back door, accusing them.
Headed out the back door so they could be sold, huh?
The statement said with no evidence whatsoever, nothing presented.
I'm telling you, if they are going out the back door and being sold, tell me where.
I got some hospitals around me that would love to buy them.
Only to bet you won't be able to do that.
Because it's not true.
It's just something that was made up to cover up his shortcomings.
And then, hospitals are hoarding vents.
Hospitals are the people who are supposed to have the vents.
If they're not in use, they should be released.
You first, Mr. President.
How many vents does the Secret Service have for you?
Portable vents and ventilator and vehicles?
Vents at secure locations to have you treated?
If they're not in use, they should be released.
They're sitting idle right now, aren't they?
Lead from the front, sir.
Set the example.
I've been talking to a lot of docs and nurses over the last couple weeks.
Whole lot of them, a whole lot of them are talking about quitting.
But there's one thing that always catches my ear.
When this is over, I am so done.
I am out of here after this.
When it's over, after this, they're not quitting because of the risk.
They're quitting because we're not giving them the support they deserve and they need.
They're on the front lines and they're the subject of personal attacks, attacks on their
character, physical attacks, and conspiracy theories.
Wonderful.
I would suggest those medical professionals who are saying stuff like that, when this
is over, after this, I'm out.
I would suggest those are the ones that we really want because they understand duty and
obligation better than our leadership when it's over.
But see, the thing is, when it's over, they probably won't quit because they actually
care about their job.
They care about the people they serve.
They're not really in it for themselves, not as much as I think people believe.
It should be a national scandal that we are sending our troops to the front lines without
their armor.
But it's not because people have abandoned their sense of duty, their obligation.
Anyway, it's just a thought. Y'all try to have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}