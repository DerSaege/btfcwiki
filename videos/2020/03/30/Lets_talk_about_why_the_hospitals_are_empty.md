---
title: Let's talk about why the hospitals are empty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=o9VlKiOLLW0) |
| Published | 2020/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People are filming outside empty hospitals, claiming it's proof that the situation is overblown and sensationalized.
- Deanna Lorraine, a Republican candidate, and Sarah A. Carter from Fox News contributed to spreading this theory.
- Hospitals are empty because non-urgent operations were shut down per Surgeon General's advice.
- Most people going to emergency rooms are not experiencing actual emergencies.
- Filming inside hospitals and questioning why patients aren't visible shows ignorance about isolation procedures.
- Many hospitals have restricted visitors and are keeping patients separated.
- People seek out these theories to find comfort in the chaos and believe in a sense of control.
- The President's statement on potential loss due to the pandemic underscores the seriousness of the situation.
- Taking precautions, staying at home, and following hygiene practices are critical.
- Actions like filming inside hospitals can increase risks and contribute to the severity of the situation.

### Quotes

- "You want to stand outside a hospital and film it, whatever. Enjoy your hobby."
- "There is no curtain. There's certainly not a man behind the curtain."
- "People like you, who are running into these places and going around town, you're the reason it's going to get real bad."

### Oneliner

People filming empty hospitals for conspiracy theories ignore the reality of medical procedures during a crisis and endanger public health by spreading misinformation.

### Audience

Social media users

### On-the-ground actions from transcript

- Stay at home, wash your hands, and avoid touching your face to prevent the spread of the virus (exemplified)
- Refrain from entering hospitals unnecessarily and filming inside medical facilities (exemplified)

### Whats missing in summary

The full transcript provides a detailed explanation of why hospitals appear empty, debunking conspiracy theories with factual information and urging responsible behavior during the pandemic. Watching the full video offers a comprehensive understanding of the importance of following guidelines and avoiding unnecessary risks.

### Tags

#Hospitals #ConspiracyTheories #Pandemic #PublicHealth #Misinformation


## Transcript
Well howdy there internet people, it's Beau again.
So tonight I'm going to answer the burning question on Twitter and then we're going to
talk about why people look for stuff like that during times like these.
Now for those who are not on the platform, let me catch you up.
There are a bunch of people running around right now filming the outside of their local
hospital and because the parking lots are empty, well that's obviously proof that, you
know, it's all hyped, it's overblown, it's sensationalized, it's a hoax.
It's not really that bad because the parking lots are empty.
Some actually went inside the hospital.
This is all, you can find all of this stuff under hashtag film your hospital.
I'm going to explain why that's a bad idea here in a second.
But before I get to answering the important question of why the hospitals are empty, I
want to draw attention to two people that helped get this theory out there.
The first is Deanna Lorraine, a Republican congressional candidate from California.
And her tweet was retweeted by Sarah A. Carter, a Fox News correspondent.
Miss Carter, I understand that you didn't co-sign this when you retweeted it.
You just said, it's interesting for sure, I'll be watching.
But see, you're a journalist.
You know everything that I'm about to say.
You should.
It's not interesting.
You retweeting this and giving this little theory life, I don't think it was a wise decision.
You should know everything I'm about to say, at least most of it.
And you should also know that your viewers are pretty easily influenced.
They saw you express interest in this.
If people believe that it's overhyped, do you think they're going to take the precautions
they need to?
Journalism is supposed to be a public service.
Okay, so why are the hospitals empty?
Because most of the hospital's operations are shut down.
Go figure.
That means the parking lot's empty.
Crazy, right?
The Surgeon General advised that hospitals get rid of elective procedures right now.
Don't do them.
Most did.
I think all did, actually.
The emergency room is just a tiny bit of what a big hospital does.
All of that other stuff, unless it's emergent or urgent, related to that, it's not going
on right now.
Therefore, the parking lots are empty.
There's no big mystery there.
No.
But you can play with that information for a second.
That may help you determine whether or not it's serious.
Hospitals have to watch their bottom line, right?
And they're choosing to forgo all of the revenue from all of this other stuff and keep the
beds empty because it's overhyped, because it's sensationalized, or is it because they're
worried about a surge and they don't want little Timmy in there who just had his tonsils
out catching something.
They don't want to waste the PPE, the protective equipment, doing stuff that doesn't absolutely
have to be done right now.
Maybe they need the beds.
Hospitals, even though they have to watch the bottom line, they also provide a public
service.
They're being responsible.
Now, to those who went inside a hospital, the ERs, yeah, they're empty.
They are.
Most people who go to an ER, they're not actually having an emergency.
Right now, I've talked about this on this channel before, an ER is for, oh, I cut my
finger off.
Not, I've had the sniffles for three days and now I feel like I want some meds.
That's not actually an emergency.
But right now, because there's a risk associated with going to the hospital and you've got
to be a certain kind of stupid to go without reason, people are kind of exercising a little
more discretion.
Is it really an emergency?
No.
Nobody's showing up with a tummy ache right now.
And for the record, that's the standard, what you're using right now, that's the standard
you should always use.
An emergency room is for serious issues.
Now, to those who went inside the hospital and wandered around filming, let me show you
what you did.
Because the big question is, why don't I see any of them?
Don't worry.
Because see, you took your phone and you hit record, right?
And then you held it down low and walked around because you're a super secret spy and everything.
And you're opening up doors and touching elevator buttons and all that stuff.
Then you walked outside and grabbed your phone with the other hand and uploaded it.
And then you put that phone up to your ear.
Don't worry.
In about two weeks, you should probably see where the patients are.
That PPE, one of the things they wear is called an isolation gown.
From that, what can you infer?
That people are in isolation.
No, they're not going to be hanging out in the hallways.
They're going to be in different rooms, hopefully, because maybe this person has something bad
and this person just has the sniffles.
They're going to try to keep them separated.
Most hospitals aren't even allowing visitors anymore.
So no, you are not going to just walk in and find them and film them.
And please, don't film patients.
Don't do that.
So that should answer the questions, at least I hope.
But now we have another question.
Why do people look for stuff like this?
Why do they want to believe this?
We've talked about it before in other videos, when we talk about theories like this.
The world's scary.
Seems like it's out of control.
And sometimes it's comforting to think that there's a group of people in control.
Because then it's not just chaos.
There's a plan.
There's somebody out there making things happen.
And it's comforting in a way to think of it like that.
Yeah, I wish that it was sensationalized.
I wish it was overblown.
I really do.
But understand the President of the United States said that he'd be doing a good job
if only 100,000 people were lost.
And when you compare that to those numbers at the beginning, if we did do social distancing
and stuff like that, 100,000 seems alright.
100,000 in a couple months.
That's more than US soldiers were lost during 10 years of the Vietnam War.
And 10 years of Iraq.
And 10 years of Afghanistan.
All added together.
It's not overblown.
You need to take the precautions.
And look, you want to stand outside a hospital and film it, whatever.
Enjoy your hobby.
Walking around inside one, it's a bad idea on a whole bunch of levels.
And since you probably don't care about other people, we'll do it this way.
You'll probably pick something up that you're going to take home to people you do care about.
Right now, stay at home.
Wash your hands.
Don't touch your face.
It's scary.
And it's out of control.
There is no curtain.
There's certainly not a man behind the curtain.
We're going to have to ride this out.
And people like you, who are running into these places and going around town, you're
the reason it's going to get real bad.
You're putting a lot of people at risk with this.
Anyway, it's just a thought. So have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}