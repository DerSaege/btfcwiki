---
title: Let's talk about what's going to happen....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_BHlS64lyvU) |
| Published | 2020/03/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Today is expected to be a challenging day with signs pointing to it being bad.
- The day that many have feared and hoped wouldn't come is here.
- Denial is about to be shattered, leading to anxiety, fear, anger, and tragedy.
- Despite being isolated, we are not alone in facing these challenges.
- This global crisis is a reminder that we are all in this together.
- The world is united in fighting against this crisis.
- Even though historically oceans have protected us, they cannot shield us from this crisis.
- It is a time to come together while staying apart.
- Limiting media consumption, especially if not in an affected area, is recommended.
- Despite the rough times ahead, we will get through it together.

### Quotes

- "Today's probably going to be a bad day."
- "We are all in this together."
- "Even if you are isolated and you are by yourself, you're not alone."
- "The entire world is rooting for you."
- "We will get through this together."

### Oneliner

Today is expected to be a challenging day with signs pointing to it being bad, but despite the rough times ahead, we will get through it together. 

### Audience

Global citizens

### On-the-ground actions from transcript

- Limit media consumption, especially if not in an affected area (suggested)
- Share hope with those feeling anxiety and fear (implied)
- Support others and stay connected while staying apart (implied)

### Whats missing in summary

The full transcript provides a message of unity, shared emotions, and global support during challenging times, encouraging solidarity and hope. 

### Tags

#Unity #GlobalCrisis #Support #Community #Anxiety


## Transcript
Well howdy there internet people, it's Bo again.
So uh, today's probably going to be a bad day.
Today's probably going to be a bad day.
All signs point to today being a pretty bad day.
We all knew this day was coming.
We all hoped it wouldn't.
But we all knew deep down this day was coming.
It's here, now we have to go through it.
There's no going around it.
We're going to have to go through it.
It does appear that the denial that a lot of us were allowing ourselves to live in is
about to be shattered over the next few days.
And it's hard to find a silver lining.
It's hard to find anything positive in this.
Because there's going to be a lot of anxiety, there's going to be a lot of fear, there's
going to be a lot of anger, and there's going to be a lot of tragedy.
Got to get our mind around that now.
It's going to happen.
But even as we are isolated, we are not alone.
Think about when it was happening in China.
Think about watching Italy.
You were rooting for them.
We all were.
They're going to be rooting for us.
This isn't something that's happening to the United States.
This isn't something that's happening to New York.
This isn't something that's happening to Miami or New Orleans or LA.
This is something that's happening to the world.
We are all in this together.
And we need to remember that.
It is the time to come together while staying apart.
The doctors and researchers in China, they're not just fighting for Chinese lives.
Those in Europe aren't just trying to save Europeans, and those here aren't just trying
to save Americans.
We are all in this together.
In the United States, we're isolated.
We don't experience a lot of things that other countries do.
Our oceans protect us.
They protect us from that.
They have historically.
They couldn't protect us from this.
It's here, and we're going to have to deal with it.
But we are not alone.
Even if you are isolated and you are by yourself, you're not alone.
The emotions that you're feeling, the anxiety you have, the fear you have, it is shared
by millions.
And as you share that fear and that anxiety with them, allow those who still have hope
to share that with you.
We're going to get through this.
We don't have a choice.
It's going to be rough.
There's going to be a lot of imagery that we probably don't want to see.
I would honestly recommend limiting your media consumption.
If you're not in one of these affected areas that's surging right now, I wouldn't watch
it.
Not too much.
There's no reason to put yourself through that.
We're in for a rough time, but we will get through it, and we will get through it together.
The entire world is rooting for you.
Anyway, it's just a thought. Y'all try to have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}