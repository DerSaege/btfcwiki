---
title: Let's talk about what makes a government....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SFbnUqZfqwk) |
| Published | 2020/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governments require the perception of authority and a monopoly on violence to exist.
- A government's obligation is to control, not protect, its citizens through force.
- Patriotism, loyalty, and obligation to protect citizens are not necessary for a government to exist.
- Governments can delegate parts of their monopoly on violence to the people.
- The key function of a government is to preserve itself, not necessarily to look out for its citizens.
- Governments can be small or large, with or without territory, as long as they have authority and a monopoly on violence.
- The governments' job is to control people, not to serve them.
- The perception of authority is vital for a government to maintain power.
- Governments can weaken and fail if the populace loses faith in their authority.
- Refusing to accept the authority of a government can be a form of resistance.

### Quotes

- "A government does not have to do anything for the people that it controls."
- "Patriotism, loyalty, all of these things. It's not a requirement. It isn't."
- "Governments don't need flags. They don't need songs. They don't need anything."

### Oneliner

Governments rely on the perception of authority and a monopoly on violence to control, not protect, citizens, with loyalty and patriotism being optional.

### Audience

Citizens, Activists, Community Members

### On-the-ground actions from transcript

- Challenge authority through peaceful resistance (implied)
- Support movements that question governmental control (implied)
- Educate others on the nature of government authority (implied)

### What's missing in summary

The full transcript delves into the essence of government authority, challenging commonly held beliefs about patriotism and loyalty while underscoring the fundamental role of control and force in governance. Watching the entire speech can provide a deeper understanding of these complex dynamics.

### Tags

#Government #Authority #MonopolyOnViolence #PerceptionOfAuthority #Control


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about what a government needs to exist.
I'll go ahead and tell you now, get ready to go down a rabbit hole.
In a recent video I mentioned that government was a magic show.
It's all smoke and mirrors.
The government has authority because people believe that it has authority.
Got a lot of questions about that and a whole bunch of suggestions about things that a government
must have in order to exist.
Things like the loyalty of the populace, patriotism, the obligation to look out for its citizens.
I mean those things might be nice if a government did that, but it's not required.
But at the same time it's not really all smoke and mirrors.
There is one other thing that a government must have in order to exist.
Really two, but one is less important.
So it has to have the perception that it has authority and then it has to have a monopoly
on violence.
That's really it.
It would be nice if it had a claim to a territory as well.
But even that isn't a requirement, it's just something that most have.
That monopoly is how governments function and that's every government in the world.
And I know if you're living overseas in a more socially liberal country you're thinking,
not really, probably to a much lesser degree than the United States.
The officers of government in your country may be a whole lot slower to go to their belt,
but I'm willing to bet that your country has the concept of incarceration.
What happens if under the authority they are perceived to have the government tells you
that you have to go and you don't?
I'm willing to bet they will exercise that monopoly.
Probably to a lesser degree than the United States, but it's still there.
They still have it.
It's implied and at any point in time, even in socially liberal countries, the power structure
that is government can change and it can choose to exercise that monopoly to a greater degree.
Now the government may decide to delegate parts of that monopoly to the people.
Well you can protect yourself, you can protect your property, but they'll probably tell you
how and how far.
That's it.
Patriotism, loyalty, all of these things.
It's not a requirement.
It isn't.
The government has no obligation to protect you.
It's not a protector, it's a government.
It's there to control you.
That's what a government is.
It is a method of control over a group of people through force.
It's every government in the world.
Right now in Rio, they're dealing with the same situation that pretty much every other
location on the planet is dealing with, and three entities down there, they decided that
they were going to take action.
And the first group, they sent their officers of government out with bullhorns, loudspeakers,
and they basically said, we're only doing this because you guys didn't take it seriously.
There's a curfew in effect.
It's best to just stay home and chill.
The message has been sent.
The second group, well, they just put the word out there was a curfew.
The third group said, well, you can go out, but only in pairs, no large groups.
We can't let this spread.
In many ways, the governments down there, they responded better than a lot of the state
governments, and certainly the federal government here in the United States.
The difference is those in Rio were street gang.
They were street gangs.
But they had the perception of authority and that monopoly on violence.
Their de facto government, that's all it takes.
It doesn't take anything else.
All of those wonderful ideas are just that, they're ideas.
It's window dressing.
It's more smoke and mirrors.
At the end of the day, a government's job is to preserve itself.
Nothing else.
Does not have to look out for you.
Think we're all learning that right now.
In the United States especially, we like to tie government to patriotism.
And it's because of the way our country was founded.
I would like to point out that no person that we view as a patriot, no person that we view
as a founding father, held any loyalty to the United States government in 1776, because
it didn't exist.
When they spoke of country, they were talking about their neighbors, they were more akin
to the guys in Rio than the people in DC.
And how did they become the founders?
They rejected that authority.
That perception of authority was lost.
And then they were willing to end that monopoly.
That's like 90% of all revolutions throughout history.
A government does not have to do anything for the people that it controls.
It doesn't.
There have been a lot of governments throughout history that did not look out for their citizens.
As we move through this, and as governments in general show that they are incapable of
managing the authority that has been vested in them, that perception is being weakened.
And that's a little unnerving at times.
It should be remembered that, yeah, 90% of revolutions ended that monopoly, and they
went that route.
That doesn't have to be the route that people go, and it's probably not advisable.
Just the lack of believing in the authority is enough.
That's enough.
When that happens, the government becomes a failed state.
There are many times when governments simply wither away because the populace lost faith
in their authority and ignored them.
You don't have to end the monopoly directly.
And it's just something I think people should keep in mind over the coming weeks, because
I have a feeling there's going to be a lot of anger.
Just remember that it's not necessary.
It's not a requirement that that element be confronted directly.
Just refusing to accept that authority is enough.
And maybe I'm wrong.
Maybe the governments know what they're doing, and everything's going to be fine.
It's possible.
There are a few out there that seem to be doing pretty well.
Unfortunately, it's probably not the government of anybody who's watching this.
Governments don't need flags.
They don't need songs.
They don't need anything.
Governments can be small, over a very tiny area, or no area, or they can be an empire.
All they have to have is the perception of authority and that monopoly.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}