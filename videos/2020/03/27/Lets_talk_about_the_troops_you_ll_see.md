---
title: Let's talk about the troops you'll see....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fYVRXeVWM3k) |
| Published | 2020/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts off by providing a quick update to alleviate concerns surrounding the activation of plans due to recent events.
- Initially, Beau was not worried as experts and plans were in place, but he later realized that the plans had not been activated.
- Some plans are now being activated in response to the situation, prompting Beau to address concerns arising from social media.
- Military personnel in uniform, such as the third and fourth sustainment units, are not there to impose martial law but are logistics experts providing support like setting up mobile hospitals and transportation.
- National Guard troops are also present, with some individuals within the Department of Defense's command structure acting as liaisons in local police departments.
- Beau reassures that the presence of CBRN teams with specialized equipment should not cause panic as these plans have existed since the 1960s and have never been activated before.
- The activated personnel are not intended for civil insurrection suppression and are unlikely to be armed, similar to National Guard responses post-natural disasters.
- Beau urges people to remain calm and let the activated personnel do their jobs, likening the situation to post-disaster recovery efforts and not martial law.

### Quotes

- "These guys are not there to impose, they are not there to impose martial law."
- "Everybody needs to calm down."
- "Do not panic."

### Oneliner

Beau assures that military personnel activated in response to recent events are there for support, not to impose martial law, urging calm and trust in their expertise.

### Audience

Community members

### On-the-ground actions from transcript

- Trust the expertise of activated military personnel in providing support (implied).

### Whats missing in summary

Beau's calming reassurance and explanation on the presence of military personnel during recent events.

### Tags

#MilitarySupport #CommunityReassurance #EmergencyResponse #NationalGuard #LogisticsExperts


## Transcript
Well, howdy there, internet people, it's Bo again.
So real quick update,
try to alleviate some concerns that may exist.
When all of this started, when it was still over there,
I was like, you know, I'm not incredibly worried about this.
We have plans, we have experts,
they're pretty good at what they do.
And then later on, I was like, yeah, we have these plans,
they haven't been activated.
I kind of assumed that experts would be allowed
to do their job.
Okay, so some of those plans are being activated.
Now, because of some of the stuff
that I'm seeing on social media,
I wanna get something out real quick.
You're gonna see people in uniform, in military uniforms.
Now, what does that mean and what do they do?
I don't have a complete rundown
of every unit that was activated.
I do know that the third and fourth sustainment,
as they've been called out.
These guys are not there to impose,
they are not there to impose martial law.
That's not what they do.
These guys, they're not even trained to do that.
These are logistics experts.
They bring stuff in, they set stuff up,
they get stuff moving, they can provide food,
transportation, stuff like that.
That's what they do.
They're going to be, as far as I know,
in New York and Washington, Washington State,
to help manage what's going down right now.
So they may be there setting up mobile hospitals.
They may be there running reefer trucks, if necessary.
They could be doing a lot of things.
You're going to see them.
You're also gonna see normal National Guard troops out.
You will also, if you are in a local PD,
because I saw one post on social media expressing concern
from somebody who was in law enforcement,
who had somebody come in and they were from DOD.
That person is National Guard.
However, they are within DOD's command structure
because there's a lot of weird laws
about using US military within the United States.
That person is really National Guard,
but they also exist inside DOD's chain of command.
So they're like a liaison.
They'll probably put one or two of those
in every department in the areas
that they're gonna be operating in.
They are not there to impose martial law.
That's not what this is.
Everybody needs to calm down.
The next phase of this, you will see those CBRN teams,
those people that I've talked about before, show up.
Understand that may be scary because when they show up,
they're gonna be masked up in a way
that most people probably haven't ever seen
outside of a movie.
But that's the equipment they have.
They may just be running around within 95s, but I doubt it.
They'll probably be fully masked up.
Just don't panic.
Don't panic over this.
It's not the beginning of Trump
taking over the United States.
These plants have been around for a really, really long time.
They've existed since the 1960s.
They've been revamped.
I think they were revamped in 99.
And they've never been activated before, only in training.
So we're gonna see how well they work.
These plants, they've put a whole lot of money
and time into them.
None of the people that are being activated
and being sent out from DOD are part of any unit
designed for suppressing civil insurrection
or anything like that.
That's not what they are.
I seriously doubt they're even gonna be armed.
So chill out, let them do their job.
Look at it the same way you would
as if it was the National Guard coming in
after a hurricane or an earthquake or something like that.
This is not martial law.
Do not panic.
All right, I'm gonna get back to playing with the kids.
Y'all, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}