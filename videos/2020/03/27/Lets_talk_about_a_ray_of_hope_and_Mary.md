---
title: Let's talk about a ray of hope and Mary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p2Jrdzt2JpU) |
| Published | 2020/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a story about a woman in the early 1900s known as Typhoid Mary, an asymptomatic carrier of typhoid who continued to spread the disease despite warnings.
- Shares a cautionary tale of how believing something contrary to evidence can be dangerous, using Typhoid Mary as an example.
- Mentions a study from the UK suggesting that many people may have already been exposed to and contracted the current virus, providing potential immunity.
- Expresses his desire to believe the study's findings as it could mean being out of the woods from his illness.
- Cautions against blindly accepting information just because we want it to be true, urging people to continue following safety measures until the study's results are confirmed.

### Quotes

- "We're going to have to pretend like it's not until we know for sure."
- "We don't want to get out, cause a bunch of problems, and then have to go back."

### Oneliner

Beau shares a cautionary tale of Typhoid Mary to illustrate the importance of not blindly believing what we hope for, amidst discussing a UK study suggesting potential immunity to the virus.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Stay at home, wash hands, avoid touching face, practice social distancing (implied)

### Whats missing in summary

Further insights on the importance of critical thinking and patience in waiting for scientific confirmation before altering safety measures.

### Tags

#CautionaryTale #ImmunityStudy #PublicHealth #CriticalThinking #SafetyMeasures


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a ray of hope.
But before we do that I'm going to tell you a story.
Because if you ever wanted to believe something was true so bad that it kind of overrode your
common sense, I've been there.
I don't want to give anybody false hope, but we do have a ray of hope.
We got some pretty cool news out of the UK and I want to talk about it, but before we
do I'm going to tell you a little cautionary tale.
So it's the early 1900s and there's this woman, she's a cook, cooks for wealthy families,
she is famous for her peach ice cream, and she's just incredibly lucky.
I mean incredibly lucky because everywhere that she goes people get sick, they get typhoid.
And you know she just moves on to the next job.
And sure enough, well, that happens there too.
She's not incredibly lucky, she's an asymptomatic carrier for typhoid.
Everybody knows her today as Typhoid Mary.
Now eventually this is found out and she is placed into isolation.
And she doesn't believe that she is a carrier.
She doesn't believe that's true.
She wants to believe something else.
Now eventually they let her out and they say, look, we're going to let you out, but you
have to take these precautions.
You can't be a cook, you know, and laid it out for her.
She wanted to believe something else so bad that she abided by the precautions for a little
while.
And then she went back to being a cook.
And it happened again and again.
Then on March 27th, so today in 1915, so 105 years ago today, she went back into isolation
where she stayed.
Where she stayed.
Okay, so now that we all have it in our minds that we can't assume something is true simply
because we want to believe it, let's talk about a study out of the UK.
There's a study out of Oxford that is suggesting a whole lot of people have already been exposed
and contracted what's going around.
And that should provide some immunity.
Now I definitely want to believe this because if you take their guesstimate and move it
over to the US, if y'all remember when I was sick for like a week and my voice sounded
all weird and there were three days when I didn't make videos and you guys were literally
sending me messages asking if I was dead.
That's the time period in question.
So I definitely want to believe this because it means I'm kind of out of the woods.
But a respected epidemiologist, Adam Kucharski, I will put the link below, has kind of explained
what's going on.
The Oxford study, it has a couple of different scenarios.
Now the media is focusing on the most extreme that is suggesting tens of millions have already
been exposed.
Kind of lays out why that's probably not true, but it is possible that a whole bunch of people
were exposed.
And this really should help.
And we'll know whether or not it's true pretty soon.
The peaks that we're expecting, they'll be lower than anticipated.
And things will get better quickly.
But the thing is, we don't know that it's true.
And just because we want to believe it doesn't mean that it is true.
So we're going to have to pretend like it's not until we know for sure.
We don't want to get out, cause a bunch of problems, and then have to go back.
So for the time being, stay at home.
Wash your hands.
Don't touch your face.
Practice social distancing.
But at least we have something to hope for.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}