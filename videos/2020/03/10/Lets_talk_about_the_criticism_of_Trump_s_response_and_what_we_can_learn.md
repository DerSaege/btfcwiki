---
title: Let's talk about the criticism of Trump's response and what we can learn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v5P3hpRB40o) |
| Published | 2020/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes Trump's response to a situation, calling it less than ideal and worthy of criticism.
- Compares the response to the movie "Jaws," with scientists urging action while the establishment prioritizes immediate concerns like tourism and economy.
- Points out that the government often ignores warnings from experts and focuses on short-term gains rather than mitigating harm.
- Emphasizes the importance of taking action now to mitigate upcoming challenges rather than waiting until it's too late.
- Notes that the current situation is unnerving and can be disruptive and tragic, but shouldn't be surprising given past patterns.
- Criticizes elected officials, including Trump, for not taking necessary actions to address crises and for playing down serious issues like climate change.
- Warns about the severity of climate change, stating it poses a greater threat than the current situation and will lead to significant disruptions and climate refugees if not addressed promptly.
- Stresses the urgency of mitigating climate change before it escalates further.

### Quotes

- "What it's not is surprising."
- "We have to take action now to mitigate."
- "Climate change is [a threat to the species]."
- "It's going to get bad, a lot worse than this."
- "We're running out of time."

### Oneliner

Beau criticizes Trump's response, compares it to "Jaws," and stresses the urgency of taking action to mitigate climate change before it escalates.

### Audience

Aware citizens

### On-the-ground actions from transcript

- Take immediate action to mitigate climate change (implied)
- Advocate for strong measures to address climate change (implied)

### Whats missing in summary

The full transcript provides more context on the analogy between Trump's response and the movie "Jaws," as well as a deeper exploration of the consequences of failing to address climate change promptly.

### Tags

#ClimateChange #TrumpResponse #Mitigation #Urgency #Analogies


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about responses
and an analogy that keeps being brought up.
And it's a fair analogy, I'll go ahead and say that now.
It is.
Right now everybody is criticizing Trump's response.
And it's fair.
It's very worthy of criticism.
It was less than ideal, and we'll just leave it at that.
The analogy that keeps being brought up is the movie Jaws.
Makes sense.
Makes sense.
You've got the scientists, those with practical experience
and those concerned, saying, hey, we've got to take action.
We've got to do something to mitigate.
There's the shark.
And then you have the establishment personified
in the mayor.
See the establishment, they've got other things on their mind,
you know, preoccupied with the here and now.
Got to get those tourists in, got to make that money,
got to keep the economy rolling.
It is an incredibly fair analogy.
It's accurate.
What it's not is surprising.
You know, people are acting like this is kind of new.
This is what the government does a lot.
The scientists and those with practical experience,
they point something out, and they're like, hey,
this is coming.
And rather than act on it, well, they worry about the here
and now.
They don't focus on anything that could mitigate,
that could reduce the harm.
They'll worry about it when it gets here.
Sometimes when it gets here, it's too late.
And along the way, they'll sit there and say, no, it's not
even happening.
The establishment will lobby and get it called a hoax.
It's not real.
It's not a real thing.
But it is.
But it is a real thing.
And right now, what we're facing, it's unnerving.
Can cause some minor disruptions.
It's not going to end the species.
It won't.
It's going to be annoying.
And in some cases, tragic.
But it shouldn't be surprising.
We've had this scenario playing out for decades.
The scientists, those with practical experience,
and those concerned, they've been sounding the alarm.
And the establishment has been preoccupied with the here
and now, calling it a hoax.
It's just media hysteria.
We'll wait for it to get here.
Just like the current situation, we
can't wait for it to get here.
We have to take action now to mitigate.
It's coming.
It's going to happen.
There are some people in our elected representatives
who've tried.
But the criticism that's being leveled at Trump
can be leveled against 90% of our elected officials.
Because they've all played that game.
They haven't taken the hard line they need to.
Yeah, maybe they didn't.
Yeah, maybe they didn't defund the organization that's
supposed to stop it.
But they couldn't, because we don't have one.
And the difference between the crises
is that while this, what's happening now,
it's not a threat to the species.
Climate change is.
And it's going to cause a whole lot more than disruptions.
It's going to be a lot more drastic than having
a staycation.
People are going to have to move.
They're going to have to relocate.
You're going to have climate refugees.
It's going to get bad, a lot worse than this,
if we don't move to mitigate soon.
We're running out of time.
Anyway, it's just a thought.
Y'all have a good night, after me saying all that.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}