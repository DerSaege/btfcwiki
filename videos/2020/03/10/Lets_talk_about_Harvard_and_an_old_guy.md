---
title: Let's talk about Harvard and an old guy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t4aptbMYlbc) |
| Published | 2020/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topics of probability, pedigree, universities, and trust.
- Shares a personal story about a 69-year-old friend who trusted misleading information from the administration.
- Expresses frustration at his friend's trust in the administration's denial of the seriousness of current events.
- Questions the wisdom of trusting an administration known for dishonesty with one's life.
- Contrasts the credibility of institutions like Harvard versus the administration in handling the situation.
- Emphasizes the importance of considering the pedigree and honesty of the sources of information.
- Advocates for listening to institutions like Harvard that prioritize caution and transparency.
- Encourages viewers to trust reputable institutions over those with a history of deception.

### Quotes

- "Imagine spending decades of your life being prepared for situations like this and then dying because you listened to a con man."
- "It's not about keeping calm for them. It's not about conveying calm. It's about conning people."
- "So please for a moment, most students at Harvard are not in an at-risk demographic. They're in that 0.2% range."
- "You can trust Harvard or you can trust Trump University."
- "Leadership from a respectable institution."

### Oneliner

Beau warns against trusting deceptive administrations like Trump University over credible institutions like Harvard during uncertain times.

### Audience

Public, Community Members

### On-the-ground actions from transcript

- Trust reputable institutions like Harvard for accurate information (implied).
- Prioritize caution and transparency in handling situations (implied).

### Whats missing in summary

The emotional impact of witnessing a friend's misplaced trust and the urgency of relying on trustworthy sources during crises.

### Tags

#TrustworthySources #CredibleInstitutions #Deception #Caution #CommunityLeadership


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about probability, pedigree, universities, and trust.
That's what we're going to talk about today.
Talk about that because I've got a friend.
He is 69 years old.
Nice I know, right?
Okay, he is 69 years old.
He's overweight, he has diabetes and coronary artery disease.
He has been one of those people who's been prepared for anything for as long as I've
known him.
Decades.
Decades.
One of those guys that was just always prepared for anything.
Spent decades of his life staying prepared.
However, he has fallen to the red hat syndrome.
Somebody in the administration said that it's media hysteria.
That it's a hoax.
It's contained.
And he has taken them at their word despite my best efforts.
Imagine spending decades of your life being prepared for situations like this and then
dying because you listened to a con man.
Because you listened to an idiot.
You're talking about an administration that can't even tell the truth about the crowd
size at a rally.
But you're going to trust them with your life.
Sometimes you need to take into account the pedigrees of the institution you're listening
to.
The administration is not known for truthfulness.
They aren't.
It's not about keeping calm for them.
It's not about conveying calm.
It's about conning people.
You have an administration that lies about literally everything.
Even stuff it doesn't need to.
And the founders of Trump University on one hand.
And they're telling you it's a hoax.
That it's a liberal plot.
On the other hand, you could look to institutions like Harvard.
Harvard has told its students not to come back after spring break.
Everything's going online.
So please for a moment consider that most students at Harvard are not in an at-risk
demographic.
They're in that 0.2% range.
Not in the 3.6 or 8% range.
They're doing that out of an abundance of caution.
They don't want to become a hotbed of transmission.
So they're doing their part.
And they're conveying calm when doing it.
You guys just, we're not going to do it now, but don't come back after spring break.
Y'all stay home.
Leadership from a respectable institution.
The end of the day, you can trust Harvard or you can trust Trump University.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}