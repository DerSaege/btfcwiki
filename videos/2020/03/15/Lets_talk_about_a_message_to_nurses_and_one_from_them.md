---
title: Let's talk about a message to nurses and one from them....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oJCC58GYgdk) |
| Published | 2020/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a message to medical professionals and from them, focusing on the stress and challenges faced by those on the front lines.
- Shares a story about prior service military members in a medical facility and how their training intensity and expertise are being called upon now.
- Describes the military's intense training scenarios and how it differs from civilian training due to profit motivations.
- Talks about the high level of confidence and posture that military training instills in individuals.
- Explains how prior service military members are falling back into their training mannerisms and attitudes during the current crisis.
- Emphasizes the importance of recognizing genuine emergencies in the ER and understanding the triage process.
- Advises against overwhelming medical infrastructure by visiting the ER unnecessarily or bringing multiple people.
- Urges visitors to follow guidelines, limit movements within the facility, and not challenge medical professionals' expertise.
- Acknowledges the stress and uncertainty faced by medical professionals on the front line and urges understanding and empathy.
- Concludes with a reminder to practice hygiene, social distancing, and follow medical advice.

### Quotes

- "Emergency being the key word."
- "Cut them some slack."
- "Their main patient is all of society."

### Oneliner

Beau addresses the stress and challenges faced by medical professionals on the front lines, urging understanding, empathy, and adherence to guidelines while reminding all to practice hygiene and social distancing.

### Audience

Medical community, general public

### On-the-ground actions from transcript

- Follow guidelines, practice social distancing, and adhere to medical advice (suggested)
- Limit visits to the ER to genuine emergencies and avoid overwhelming medical infrastructure (suggested)
- Show understanding and empathy towards stressed medical professionals (implied)

### Whats missing in summary

The full transcript provides detailed insights into the experiences and challenges faced by medical professionals and offers valuable guidance on how individuals can support and cooperate with healthcare workers during these trying times.

### Tags

#MedicalProfessionals #EmergencyRoom #Triage #Stress #Empathy #Hygiene #SocialDistancing #Support


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about a message to medical professionals and one from them.
We're going to start with the message to them because you guys are stressed, you don't have
a lot of time, and y'all are on the front lines right now.
Thank you, by the way.
Okay so you may find yourself counting yourself as lucky because you have found out you have
prior service military in your hospital or clinic or facility and they just have a wealth
of knowledge about the most pressing issue of the day.
Alternatively, you may be counting yourself less than lucky because all of a sudden your
prior service military, well, their attitudes have changed.
Their language has become more coarse, their posture has changed, they have become, to
put it nicely, a little more direct.
I try to shed some light on what's going on.
True story, my wife gets off the plane, there is no hospital, there's no medical facility,
just flat land, they have to build it.
So they put up the tents, they bring in the beds, they set up a mobile medical facility.
The second day, I think, that she's there, her and some other nurses, they're sitting
around this table outside in the baking sun, eating lunch out of a bag, MREs.
Bad guy in the facility has somebody captured.
Security forces runs up, screaming at them, put it down, put it down.
All of a sudden, an RT, a respiratory therapist, she rounds the back corner of this tent.
Bang!
She actually says bang, because it's a training exercise.
In the civilian world, because it's profit motivated, you don't have intensive training
like that, because it's expensive.
It is expensive.
You have tabletop, or you have role playing in a room, and the scenario presented is probably
a 50 car pile up, or a plane crash, or something like that.
This scenario, what's happening right now, is one of the scenarios used to train in the
military.
When they show up, they have to build the facility first.
The whole time, vest, weapon, mask, they have to build it.
Then beds come in, mannequins, sometimes live tissue models.
The entire time, the instructors are throwing stuff at them, trying to get them to mess
up.
Parts of the facility become destroyed.
Even during patient care, crazy things happen.
It's incredibly stressful.
It is incredibly intense.
That level of intensity breeds a lot of confidence in the subject matter.
At the same time, it does create a certain posture.
What's happening is now that they're being called upon to access that training, and access
that expertise, they're falling back into those mannerisms, into that attitude.
You're going to get a lot of language, coarse language.
I know ER nurses are rolling their eyes right now.
Probably a lot of gallows humor.
They're going to be more direct.
Their posture is going to change.
I saw a doctor leaned into somebody, going like this.
The prior service watching this, they have no idea what was said, but they know the person
she was talking to was in trouble.
It's the cost.
It's the cost of having somebody on hand that might be able to MacGyver a negative pressure
room.
If you're in hospital administration and you are unaware of this, you should probably be
going through your HR files right now, because you may have people in your hospital, or in
your facility, that are being underutilized.
They train for this.
Okay.
So now the message from the medical professionals.
First and foremost, it's an ER.
ER stands for emergency room.
Emergency being the key word.
It's not advising people not to seek medical treatment.
It's advising to be aware of what an emergency is.
Understand that when you go there, you get triaged.
It's not first come, first serve.
It's whose life is most at risk.
That's who goes back first.
If you go there and you're not having a genuine emergency, number one, you're going to wait
a long time.
Number two, you're wasting resources.
One of the big concerns right now is overwhelming the medical infrastructure.
So take careful stock of whether or not it's an emergency.
The other side to this is when you're there, understand the door that you came in, that's
only one of the doors that leads back there.
There's the other door with the ambulances.
Those people are probably going to be more emergent.
They're going to be seen before you.
It's not first come, first serve, so stop yelling at the lady at the desk.
If you go to visit somebody who is down with what's going around right now, it would probably
be advisable to limit it to one person.
That person should be the person who is designated to have medical information released to them
or to make end of life decisions, that sort of thing.
You don't want to overwhelm the medical infrastructure.
When you go into your visit, you're going to be gowned up.
They're going to give you all kinds of stuff to protect you.
You can't leave the room with it.
This is something that needs to be discussed.
You can't leave the room with it.
It has to stay away.
You can't walk around the hospital with this.
It has to come off.
What that also means is when you go in for your visit, go in, have your visit, leave.
Don't come in and out because every time you come in and out, that stuff has to come off
and you have to get new stuff.
It's wasting resources.
Don't overwhelm the medical infrastructure.
Follow the guidance of the medical professionals that you see.
You came to them.
Saying that it's a hoax or that it's really just the flu is probably not going to elicit
a good response.
They are going to assume that you're going to be noncompliant with your treatment.
They are operating under the best practices as handed to them by the CDC and the World
Health Organization.
They're not your enemy.
You have that.
The next thing to understand is that they are incredibly stressed.
There's a lot of uncertainty.
They are on the front line and they're going to be fighting an uphill battle.
It hasn't even hit yet and the stress is already there.
So take that into account.
They may have a less pleasant bedside manner.
They may be a little slower getting a turkey sandwich or ice or whatever.
Their main job right now, their main patient is all of society.
They've got a lot on their plate.
So cut them some slack.
Okay, so aside from that, wash your hands, practice social distancing, do what the medical
professionals tell you to do, and I guess stay out of red robin.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}