---
title: Let's talk with Shahid Buttar about 2020....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RjTlMidhZrI) |
| Published | 2020/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Shahid Buttar as he takes on Nancy Pelosi in the primary, aiming to provide a leftist perspective challenging the status quo.
- Shahid Buttar explains his motivation for entering politics, driven by a desire to address political frustrations and misrepresentations in Washington.
- He outlines his leftist views, advocating for radical wealth redistribution, nationalizing certain industries, and focusing on civil liberties and voting rights.
- As an immigrant, Buttar shares his commitment to civil rights advocacy and opposition to bipartisan fascism, leading to his decision to challenge Pelosi.
- Buttar details his strategy to defeat Pelosi in the upcoming election, focusing on informing San Francisco residents about Pelosi's policies and running a volunteer-driven campaign.
- The interview delves into Buttar's support for progressive policies like Medicare for All and the Green New Deal, linking these initiatives to social justice and climate resilience.
- He also addresses immigration issues, advocating for immigrant rights and criticizing the current administration's policies.
- Buttar offers a unique perspective on the Second Amendment, viewing it as a right to resist tyranny and suggesting that in the modern context, resistance may manifest through information access.
- Funding his campaign through grassroots support from thousands of Americans, Buttar expresses gratitude for the backing he has received and his commitment to challenging corporate influence in politics.
- He concludes by urging individuals to curate their news sources, connect with their communities, and work towards collective action, including the possibility of a general strike as a means of reclaiming sovereignty.

### Quotes

- "I'm just a schmo with a pen, and I don't have a family legacy. I'm an immigrant." - Shahid Buttar
- "I wanna see us commit to rights to housing and healthcare and food and education." - Shahid Buttar
- "I fear that we could continue to be preyed upon by institutions that have the opportunity to turn a deaf ear to us." - Shahid Buttar
- "If all else fails and tyranny emerges, we have the right to resist it." - Shahid Buttar
- "Step one is proactively curate your news sources. Step two is meet your neighbors. Step three is do the thing, agitate." - Shahid Buttar

### Oneliner

Beau interviews Shahid Buttar, a leftist challenger taking on Nancy Pelosi in the primary, advocating for progressive policies and grassroots activism to reclaim political power.

### Audience

Progressive activists, community organizers

### On-the-ground actions from transcript
- Connect with local organizers and neighbors to build community support for progressive causes (exemplified)
- Curate news sources for accurate information and share reliable news with others (exemplified)
- Organize events like letter-writing campaigns, protests, or community meetings to address political issues (implied)

### Whats missing in summary

The full transcript provides a comprehensive insight into Shahid Buttar's background, motivations, policy stances, and grassroots campaign strategies, offering a nuanced look at his challenge against Nancy Pelosi and his vision for progressive change in politics.

### Tags

#ShahidButtar #NancyPelosi #PrimaryElection #ProgressivePolitics #GrassrootsActivism


## Transcript
Well, howdy there, Internet people.
It's Bo again.
And tonight we've got an interesting conversation.
We've got Shahid Buttar here.
And if you're not familiar, this is
the guy who's taking on Nancy Pelosi in the primary.
So we're going to get to hear about Pelosi getting primaried
by someone who is kind of actually on the left for once.
So why did you start to do this?
I mean, when you're jumping into politics
and you're going after Pelosi to start off, that's, I mean,
you picked a big mountain to start with there.
I did.
I say to people that I don't have political aspirations.
I just have very deep political frustrations.
And I'm tired of being misrepresented in Washington.
Basically, it's that simple.
I live in a city where the United Nations was founded.
And our voice in Washington facilitates
human rights abuses.
I live in a city that's dedicated to global peace
and justice.
And our voice in Washington funds
every war that's ever been proposed during the time
that she's represented our city in DC.
And we're a city that's committed, for instance,
our board of supervisors unanimously supports
universal health care.
But our voice in Washington refuses.
And so I'm running to liberate our city's voice in Congress
to make sure that San Francisco can finally
get a voice aligned with our city
to inform the federal policy process.
All right.
OK, so we've been talking a lot on my channel.
Are you a social Democrat, a Democratic Socialist?
What are you?
I'm way left of where we're at now.
And I'm eager to push it as far as it can go.
My first act of academic scholarship 20 years ago,
I wrote a thesis that got me into Stanford Law School
about how to perpetuate, through trust in a state's reform,
radical wealth redistribution through a market economy, which
is to say carving new ground in this construction of what
does the future look like.
I'm also, separate from that agenda, very eager to,
for instance, nationalize weapons manufacturers,
fossil fuel extraction companies, resource extraction
companies generally, to make sure that profit does not
inhere in plunder.
So that's well to the left of what the established system is.
I'm also, though, I make a strong point about the D,
back in Democratic Socialist.
I'm very grateful for the endorsement
of Democratic Socialists of America in San Francisco.
And what that means is that I'm particularly
concerned about the intersection of surveillance,
keeping our neighbors silent and preventing them
from being able to raise their voices about whatever
might matter to them, executive secrecy,
keeping Congress and the courts in the dark,
threats to voting rights that prevent us from even having
control over our process, and then finally,
the loss of the independence of the courts.
And I have ideas and solutions in each of those four vectors.
So as an immigrant to the United States,
I was brought to this country to be free.
I was two years old when my family came here.
I grew up in rural Missouri.
And I got out of the South Side of Chicago to study and then
teach law at Stanford.
And during the 20 years that I've
been fighting for civil rights in the United States,
I have witnessed the rise of bipartisan fascism.
And it is the unfortunate, sad, and unacceptable
complicity of the corporate Democratic Party
in all of these different dimensions of authoritarianism
that ultimately forced my hand into the race.
I can't watch this go down from the sidelines
knowing too well how Pelosi actually
operates in Washington.
I may have to eat my words here, because I recently just said,
we don't have any actual leftists running.
OK, so you talked about you fought for civil rights.
Now, that was with Electronic Frontier.
Is that right?
Yeah, and before that, at EFF, my work
was particularly around surveillance and net neutrality,
the rights to encryption, the right to repair.
15 years before that, I was an early advocate
for marriage equality.
And that was when I was in private practice working
at a law firm.
And I was representing the second mayor in the country
to recognize the rights of my neighbors
to marry a partner of their choice.
I've fought for civil rights across a few different contexts,
also immigrant rights, civil rights
in the face of police persecution and predation.
That's a spectrum a little bit.
So we got to ask the one question.
How are you going to beat Pelosi?
That's what I want to know.
It's the million dollar question.
So to this point, we have our foot in the door.
I'm in the general election.
So we have a top two primary system in California.
And I won second place last week.
So we now have the chance to face off one on one
against her in November.
And I'll be the first challenger that she's
faced in a general election from within the Democratic Party
in the 30 years that she's been in Congress.
And our campaign is very much built on the premise
that San Francisco stands very well to her left.
At the moment, the city remains relatively uninformed
about how Pelosi shows up in Congress.
She still has a reputation of being a lion of the left,
even though she supports Trump's trade policy,
and even though she supports Trump's foreign policy,
and even though she supports Trump's fiscal austerity rules,
and slow walked and then limited an impeachment process that
failed, even though we could have won it.
A lot of people still think of Pelosi
because they see her on MSNBC tearing up speeches,
and shady clapping, and pointing across a table,
and putting on her sunglasses as she's sternly
walking out of a meeting.
We're basically running to deconstruct
the myth of the photo ops.
And this really comes down to just knocking on doors.
We've got an army of volunteers, literally hundreds,
just in the Bay Area, thousands across the country,
and between my neighbors and allies
who are pounding pavement and knocking on the doors with me,
and our neighbors who are organizing phone banks
in their various cities around the country.
We've had about a half dozen happen so far from Los Angeles
to New York.
And anybody who wants to get Pelosi out of office
and liberate San Francisco's voice in Congress,
or alternatively end the corporate control
of the Democratic Party, is welcome to jump in.
The volunteer invitation is open to anybody.
And folks can go on our website, shahidforchange.us,
to learn more and to volunteer.
That's fantastic.
Yeah, she does.
She has a reputation.
And as, you know, it's funny,
because we talk about it on our channel,
you know, it's the lion of the left.
Yeah, but she's not left.
She's center right, you know?
And there's a large part of the Democratic Party
that's cool with that.
I don't know that there's a large part of San Francisco
that's cool with that.
So, I mean, if you're gonna do it,
you pick the right place to do it.
And, you know, there's a whole lot of people
going after the establishment Dems right now.
And I just, are y'all talking to each other at all, or?
Yeah. Yeah, there's been a lot of endorsements.
You know, I've endorsed, for instance,
Lauren Ashcraft and Isaiah James in New York.
I've endorsed Heidi Sloan in Texas,
and Jessica Cisneros, who unfortunately lost her challenge
to Henry Cuellar, largely because Nancy Pelosi
insisted on supporting the most conservative Democrat
in the House, an anti-choice, anti-Frankly Walker Democrat,
and helped him defeat a progressive woman lawyer of color
who was running on the civil rights platform,
Vision of the Future, to expand the squad,
who Pelosi continues to kick down the stairs.
And so, you know, the alliance among us that's emerging
is part of the reason why, frankly,
I'm running for this seat.
I see the person that I'm confronting
as playing a particularly crucial role
in consolidating not just corporate rule,
and we can see how that inflects everything
from predatory health care at a time
when we needed to be free in order to deal sensibly
with a global pandemic,
to wars for profit and plunder abroad
that she funds and facilitates at every opportunity,
that she consistently puts corporations
before her constituents.
And at the end of the day,
I think that the voice, not just of San Francisco,
but I think the rest of the country is aligned
in wanting the United States
to join the rest of the industrialized world
and making sure that we don't kick our sick
and ill neighbors into the street
just because they can't pay for the doctor,
making sure that your grandkids have a shot
at a livable, viable future,
a chance not just to survive, but hopefully to thrive.
And that's the kind of opportunity
that not just Republicans, but corporate Democrats too
have been frankly assaulting for decades.
And I thankfully don't have children,
but I have nephews and nieces,
and I have friends with kids and supporters with kids,
and I'm here to defend them and the future
from the failures of our predatory corporate past
represented principally by Nancy Pelosi.
Wow.
Okay, so when it comes to,
we've definitely got a good overview of your platform here.
When it comes to specifics, Medicare for all,
just blanket support for it, where are we at on that?
Absolutely blanket support.
The way I describe it and the way I present it
in San Francisco is the intersectional benefits
that it will offer.
People think of it through the lens of,
how much do I wanna pay for my corporate
health insurance plan with the high deductible
and all these exclusions,
and there's limitations of what doctors I can see
versus a Medicare for all framework
where you can choose your own doctor,
you can get the preventive care that you need.
The medicines are very low cost,
under Bernie's plan, they're capped at under $250
a year per person.
That's the plan I endorse.
I particularly though present it to San Franciscans
as a solution or at least one among many
to our homelessness crisis.
People forget that the leading cause of homelessness,
and this is preposterous, is medical bankruptcy.
And it is morally bankrupt that medical bankruptcy
plays such a prominent role in driving homelessness.
And I think there's a very visible crisis
in San Francisco with respect to our failure
to provide housing to all of our residents.
And I think Medicare for all would not only
improve our public health outcomes,
dramatically drive down the costs,
relieve the burden facing sick and ill patients
and their families,
and it would keep people in their homes.
And I think that's really, really important
at a time like this.
I especially think it's crucial in the context
of the coronavirus,
because the only way to prevent contagion
is to make sure that everybody can get tested.
And the fact that there are people
who are not being tested
because they can't afford to get the test,
it's not just foolish,
it is literally self spiteful and defeating.
We are inviting a pandemic because we're too stupid
to allow people to get access to health care
and to have access to the care and the resources,
not just that they need,
but that we need each other to have.
We all share a need for each other to have access to care.
And the fact that we resign care
to predatory health insurance companies
and pharmaceutical companies
that practice arbitrary pricing,
is just a signal that we're too willing
to put profit before people
and corporations before communities.
I think this is a signature issue
for the left and the future.
And frankly, we need it in order to allow
Americans the opportunity to simply survive
in the current economy.
All right.
And Green New Deal.
I'm super into the Green New Deal,
the way I present it in San Francisco
around climate resiliency projects
enabled by the federal jobs guarantee.
So one of our principal challenges
in California writ large,
not just San Francisco,
but particularly across Northern California
are the wildfires that have,
in high frequency devastated our state
each of the last several years,
particularly in the fall,
when the forests are drying out,
we have high wind season,
and it's very prone to devastating wildfires,
which have cost billions of dollars
and dozens of lives over the last few years alone.
Largely due to the malfeasance and negligence
of a private utility company.
I'd also like to nationalize utility companies
to make sure that they're doing what they need to do
to put safety before profits.
But beyond that opportunity for prevention,
one of the things that Green New Deal
could mean in California
is the employment of a generation of young people
to basically replicate
the kind of traditional forest management techniques
that were practiced by indigenous Americans
before the genocide.
And that's a high labor, low,
it's the Green New Jobs federal jobs guarantee
will have a minimum wage.
And I'm hoping to peg that at least at $15 an hour,
but without that jobs guarantee,
this is low wage work.
That's why the market hasn't been able to do it.
And because we've resigned
so much of what need to be
collectively shared responsibilities to the market,
you know, the construction of affordable housing,
for instance, it just doesn't happen.
And that's why I want a Green New Deal for housing
to ensure that we have publicly owned social housing
owned by the people, managed by communities,
accountable to the residents.
These are opportunities for us to pool our resources,
to defray the challenges that individuals
and our families confront alone.
We, in this day and age,
I think so many Americans are staggering
under the brunt of predatory healthcare,
if not for themselves and for their kids
or their ailing parents,
everybody gets sick eventually.
And it's senseless that we use it as an opportunity
for corporate extraction.
I'd much rather see us as a civilization
and a society shoots to put ourselves
in each other first.
And that's the Green New Deal represents that
not just with respect to each other,
but also the future generations that don't have a chance
to even raise their voices at the moment.
I'm, if I may, this is a bit of a,
it's not exactly a digression, but just a tangent.
When people talk about the Green New Deal,
obviously one of the things we're concerned about
is climate justice.
And for me, just to demonstrate another intersection
that gets often overlooked,
one of the reasons I care so deeply about civil liberties
and the right to dissent is particularly
because of how it's been impacted
or how it has impacted climate justice and energy policy.
A lot of Americans don't realize
that the constitutional crisis
when we started jailing dissidents as terrorists,
that didn't happen after the 9-11 attacks.
That started in the 90s under Clinton.
Frankly, it started back in the 50s
during the McCarthy era, but even under Clinton,
Americans were being jailed,
convicted of terror offenses
for environmentally motivated vandalism.
So we had a radical Earth rights movement in the 90s
in the United States that was criminalized,
driven abroad and driven underground.
And I wonder how much different might our energy policy be
and our climate policy be,
and how much less severe might the climate crisis be
if we listened to the generation of environmental activists
who were warning us 30 years ago
that we were racing humanity off a cliff
instead of vilifying them and criminalizing them.
I see that dynamic replicated in lots of different ways.
We need to protect dissent, not just to guard our rights,
but also to enable sensible policy decisions
that never happened otherwise,
because in this scenario, we've locked up the dissidents
and we've given corporations the keys to the castle.
All right, okay.
What about immigration?
I am an immigrant myself,
and so I'm very committed to immigrant rights.
I see the Trump administration
readying an attack on naturalized citizens,
which hits very close to home
because I am one.
The president wants to strip me of my citizenship,
and I want to make this clear.
I am a more loyal and faithful American
than our criminal president has ever been and ever will be.
And that's true for millions of us immigrants
who are dedicated to the principles of our constitution
and our country.
And we don't need a racist billionaire
to make it great again.
What we need in order to make it great again
are opportunities for working families to lead viable lives
without being preyed upon by predatory police
or private utility companies
that put our communities at risk.
With respect to immigration,
the need to allow labor to migrate,
we could justify it even in strictly neoliberal terms
if we wanted to, right?
Capital flows liberally and that helps markets.
Yes, this week is an interesting study
in what helps markets.
You see that can drive them to the floor.
But liberalizing labor migration flows
also serves the economy.
Even if we don't view it
through the liberatory socialist lens
that says people have a human right
to for instance, be with their family
or alternatively escape violence
or alternatively escape climate chaos.
A lot of folks don't realize
that the refugees on our Southern border,
they're being driven by a drought,
which is itself inextricably related to the climate crisis.
It's partly, it's in a big way our fault.
And it's not like we're passive observers
as these people are washing up on our shores.
We've destabilized the places that they're coming from,
often with violence and encouraging paramilitary assaults
on the rights of communities.
And we have an important thing to remember
with respect to immigration.
There is, we fought a world war
to establish an international right to asylum.
If you fear where you live, the international community,
our planet, our species has decided
you have a right to flee
and anywhere has an obligation to accept you
to at least give you a chance
to say why you have a right to be there.
And we didn't lose that right to the Nazis.
We didn't lose that right in a war.
We lost that right to Donald Trump
and the deference of corporate Democrats
who have endorsed his border militarization,
more recently shutting it down entirely,
the detention camps that Trump proposed
that Pelosi funded in which thousands of people
are being illegally held when they have a right
to be in the United States,
seeking asylum through a legal process.
And maybe to close the loop there,
I see the attacks on immigrant rights
as precursors to attacks on the rights of all Americans.
We are canaries in a constitutional coal mine.
And a lot of Americans have come to fear fascism
in the last four years.
I've been fighting fascism in the United States since 2001.
And I've recognized it in those terms.
And as an immigrant Muslim,
I have a lot of skin in the game.
And in the very same fights
that Pelosi has been unwilling to have,
the places where she's been all too willing
to go along to get along,
I wanna put someone like Barbara Lee,
who has voted against the Patriot Act,
who did vote against the war in Iraq,
votes where Pelosi failed,
where she went along with the executive branch.
I wanna put a real congressional leader
like Barbara Lee in the speaker's seat
and get her the gavel
and that's gonna require removing Nancy Pelosi.
All right.
So let's see.
What's another hot button?
The second amendment, guns.
Okay, that's a-
How does San Francisco feel about that?
So the city is very committed to sensible restrictions
to ensure the safety of our communities.
Background checks are a bare bones example.
I would like to see limits on assault style weapons.
And I have an analysis here.
I wrote an article some time ago,
Ferguson and the second amendment,
that attempts to some extent bridge the gap
between traditional left and right analyses
of the second amendment.
So you have conservatives who will often say,
the second amendment is our right to bear weapons
and without having weapons, we're not free.
And you have liberals who say, that's insane.
We live in communities and people die every day
at the hands of each other.
It's the guns that are killing us.
So can we just get these off the streets
and we can all live, please?
And one of the historical evolutions that people overlook
in this debate is the rise of paramilitary police.
So I would suggest that the second amendment
is not about the right to bear arms at all.
That's what the text says.
But when the constitution was being written,
the right to bear arms, the reason it's there,
what is the purpose of the right to bear arms?
The purpose of the constitutional design
was an escape hatch.
The first amendment are a right to escalating series
of opportunities to participate in a political process.
The right to your own beliefs,
the right to write about them,
the right to publish, to gather people,
to petition the government.
The second amendment is the escape hatch.
If all else fails and tyranny emerges,
we have the right to resist it.
That's what the second amendment is,
is a right to resist tyranny.
What foreclosed that?
The emergence of paramilitary police.
The militarization of police in the United States
renders the right to bear arms
disconnected from the right to resist
because you can't resist a country
that is militarily occupied in every community.
That's what we are living under today.
Militarized police basically occupy every urban center
across the United States.
And resistance through arms is entirely untenable.
And so when second amendment enthusiasts
and conservatives say our right to bear arms keeps us safe,
they forget that your right to bear arms
doesn't do anything of the sort anymore.
And as an example, I just cite,
there are all these vets who come home
who many of them can't get access to services.
A lot of them end up struggling with housing.
There have been any number of cases of vets
who've been targeted for assault, like SWAT raids,
particularly because they might be involved
in something illicit.
And they're highly trained people.
And usually a couple of members of the SWAT teams
end up injured or dead.
And so does the person that they go after
because no single person can resist a SWAT team.
It's just, it's impossible, right?
When you have a lot of highly trained people,
only organized resistance can combat it.
And unless, and I'll be honest with you here,
I do think occasionally about the, and I do fear,
and the FBI has investigated and frankly gone very soft
on the armed and organized right-wing militias
that frankly are the only locus of possible resistance
to militarized police.
But the right to bear arms,
I think people misconstrue what it means.
I like to take it back to the right to resistance.
In my mind, what the second amendment means today
is a right to hack the government,
to exfiltrate information that is kept from us
illegally and unconstitutionally, illegitimately.
We, the people are sovereign.
The second amendment right to resistance
in the information age is a right to hacking.
And I'm very eager to defend that
and vindicate that principle going forward.
Wow, all right.
Wasn't the answer I was expecting.
All right, okay, let's see.
So at this point, I'm actually just trying
to throw you a curve ball.
I appreciate that, that's fun.
Okay, so you've got, you've taken a very little
L libertarian socialist stance on, I mean,
you seem to be very ideologically consistent here.
Who is paying your bills?
13,000 Americans from every one of the 50 States.
We have US expat citizen supporters who live abroad now.
So literally the sun never sets on our support base.
And unlike the British empire,
which colonized the land of where my forebears came,
I didn't have to fire a shot to do it or enslave anyone
or move anybody around the world.
I'm very grateful for the support
of all the people that have stepped forward.
I often say to people, I'm just a schmo with a pen,
and I don't have a family legacy.
I'm an immigrant.
My family lost our house when I was 16.
I got my undergrad degree going to night school for 10 years.
I'm just a schmo, but what's put me in a position
to challenge the most powerful corporate politician
on the planet is precisely the backing
of thousands of my neighbors,
not just in San Francisco, but far beyond.
And I'm so grateful for that help.
We saw the impact of it last week in particular.
So we ended up raising for the primary cycle,
which we just took second place in half a million dollars.
And that's the strongest challenge that Pelosi's ever faced.
And we're just getting started.
We have another eight months to go.
It's the beginning of a new cycle.
We're starting with some resources on hand
and a fully professionalized developed campaign team,
hundreds of volunteers.
We're gonna be hitting the ground running
in the next several months and making the case
for why we need these paradigm shifting policies
to meet the needs of the future,
like Medicare for all and paid sick leave to keep,
and stopping ICE from seizing immigrants
when they go to the hospital or the courthouse
to make sure that we can deal with this pandemic threat
sensibly instead of irrationally and maniacally
in the way that our criminal president would prefer.
ICE has declared basically a crackdown
on sanctuary cities across the country,
including San Francisco.
I was at a press conference
at which members of our public defender's office
were speaking just yesterday at the hall of justice,
defending civil rights, defending immigrant rights
and defending our city's sovereignty
and commitments in the face of an authoritarian
federal crackdown.
That synthesis I would describe of libertarian principles
as it relates to the state and socialist policies
as it relates to the expansion of human rights
to encompass our human needs.
We don't have a lot of affirmative rights.
We have negative rights,
rights to theoretically be free from,
for instance, government surveillance, which is ubiquitous,
rights to be free from unreasonable searches and seizures,
which happen all the time,
rights to due process is a thin affirmative commitment
because we do have minimal due process protections
that we do commit to.
But I wanna see us commit to rights to housing
and healthcare and food and education.
Those are rights that not only benefit the individual
who receives them, but they benefit our civilization.
We have an opportunity at the moment.
I think a lot of Americans are waking up
to how bad life in the United States can be
if you're not a billionaire,
and that we're not alone
in the struggles that we share.
And if we're able to get on the other side of this,
if we can throw off the yoke
of the bipartisan corporate rule
that has constrained our responses to our social crises,
if we can ensure that everybody has an opportunity
in the new economy,
if we can ensure that everybody can get to a doctor,
if we can ensure that your kids
and your grandkids can survive,
we have the opportunity, I think,
not only to meet the crises of this moment,
but to create a whole new spectrum of opportunities
for the future.
And I see that promise.
I'm really excited by it.
That's the socialist thing that inspires me
at the same time as the dystopia
and the authoritarianism that looms also terrifies me.
Really both of those strands
that are fueling our campaign and my run to replace Nancy.
All right.
So here's a question I ask pretty much everybody
that comes on the show.
What is something that, anybody who watches this,
what is something that they can do
to better their community,
they can act on their own?
What is one way right now, tell us how to change the world?
Thank you so much for asking that,
because that's where the action is actually at.
Step one is proactively curate your news sources.
Your listeners are probably doing that already
because they're listening to you
and not the lies that they're gonna hear
on Fox News or MSNBC.
Step two is meet your neighbors.
We can develop concerns in isolation,
but it's only when we organize and we connect
that we can do anything about it.
Individual solutions to grand problems
are ultimately and necessarily infinitesimal.
But when you meet your neighbors
and you talk about your shared concerns,
and this is the key,
when you come up with plans to do something together,
whatever it is, I don't even care what it is.
You can write letters to some official.
You can stand on a street corner with some signs.
You can host a teach-in at the community center
or the library.
You can go to the nearest college
and try to find some students who are organizing
around whatever you care about and connect with them.
You can take a trip to your state capital
and lobby your state officials.
There's so many ways to get involved.
And again, I don't even think it matters how.
I mean, I just think what matters most
is that people do the thing,
whatever the thing looks like to you.
I've seen people use all kinds of different shared things
as platforms for organizing.
Some of my earliest political acts
were organizing collectives of politicized artists,
hip hop artists, MCs, spoken word artists, comedians.
So there's three different groups,
Stanford Spoken Word Collective in Palo Alto,
the DC Guerrilla Poetry Insurgency,
and an outdoor poetry convergence that happened
every week in San Francisco for the last 17 years.
And I've had a chance in pulling each of them together.
They're all still active.
And together they've churned out,
probably at this point, thousands of artists,
visionaries who are in their own ways,
in small rooms and big rooms,
on street corners, on train platforms.
They're polemically making the case in public.
And for me, that was about poetry and creative writing
as a vehicle to express a political sensibility.
And that started, each of those collectives
just started with groups of poets,
sharing their frustrations, sharing their poetry,
and coming up with a plan for how to share and promote it.
So we've got curate the news.
So educate, get together with your neighbors,
organize, and then do the thing, agitate.
I like it.
I like it.
The thing to agitate towards,
a strategic goal, general strike.
People ask me what happens if Trump steals the election?
And that's a real question.
We can't take for granted that he will leave
the White House if he's beaten in the election.
And at that point, all bets are off.
If we, the people, are not ready to respond
by reclaiming our sovereignty,
the only way we can do that is mass work stoppage.
And that takes organizing.
You have to know your neighbors in order to pull that off.
The Montgomery bus boycott wasn't just a boycott.
People self-organized rides.
They still got to work.
They just found other ways to get there.
Communities came together to provide alternatives
to the institutional services.
And a mass workshopage, you know, communities and families,
you're still gonna need to eat.
So like figuring out how to do that,
even if people aren't working for a while,
that's almost like the million dollar question.
And until we gain the capacity
to assert ourselves collectively,
I fear that we could continue to be preyed upon
by institutions that have the opportunity
to turn a deaf ear to us and a blind eye to our concerns.
And I'm fighting in the electoral arena
because I still have faith in the democratic process.
And I think if enough of us engage,
we can flip some of these seats and change the policies.
At the end of the day, as I mentioned before,
there are plenty of holes in the bucket of our democracy.
And it might take steps beyond the democratic levers
to defend it.
And I think a general strike is the strongest tool
in the box that we, the people, have.
Okay.
All right.
Plug your site again.
You gave everybody the information they need
because I got a feeling you won some hearts and minds
tonight.
Thank you, Beau.
Folks can find us online at shahid4change.us.
That's S-H-A-H-I-D4change.us.
We're also on all of the social media platforms,
Instagram, Twitter, and Facebook at Shahid4change.
All right.
Okay, guys.
So that's gonna wrap up the interview for tonight.
It's just a thought.
And we'll see you probably tomorrow.
Thanks a lot.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}