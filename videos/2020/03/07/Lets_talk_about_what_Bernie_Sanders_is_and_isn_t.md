---
title: Let's talk about what Bernie Sanders is and isn't....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zn_4-7CpgU0) |
| Published | 2020/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the lack of nuance in discussing different ideologies and philosophies in the United States.
- Explains that in the global perspective, ideologies like the Green Party and Libertarians are not extreme, but center-right.
- Clarifies the misconception of labeling Bernie Sanders as a communist, pointing out that communism advocates for communal ownership of production means and abolition of class and state.
- Differentiates democratic socialism from communism, stating that Sanders may not fully embrace democratic socialist policies based on his advocacy for taxing profits.
- Describes social democracy as advocating economic interventions for social justice within a capitalist system, which is where Bernie Sanders lies.
- Urges for a broader exploration of political ideologies beyond the traditional Democratic and Republican spectrum in the U.S.
- Encourages breaking free from the constraints of the two-party system to foster deeper systemic change.
- Advises exploring various ideologies to understand different perspectives, even if they may seem unfamiliar or challenging.
- Challenges viewers, including right-wingers, to look into different ideologies to gain a better understanding.
- Suggests that many people might identify with certain ideologies without fully understanding or acknowledging them.

### Quotes

- "We don't have a leftist anything in the United States. Doesn't exist."
- "If we're going to get anywhere, we've got to break free of Republicans and Democrats and the policies that they espouse."
- "There's no reason to be afraid of ideas."
- "Y'all are pretty much commies, to be honest. Y'all just don't know it."
- "Republicans and Democrats, they have had a very, very long run, and those policies have had a very long run in the United States."

### Oneliner

Bernie Sanders is a social democrat, not a communist, urging for a break from the two-party system to embrace diverse ideologies for systemic change.

### Audience

Politically Aware Individuals

### On-the-ground actions from transcript

- Research various political ideologies beyond the traditional spectrum (suggested)
- Engage in open-minded exploration of different ideologies (implied)
- Encourage others to break free from the constraints of the two-party system (exemplified)

### Whats missing in summary

The full transcript provides a comprehensive breakdown of political ideologies in the U.S. and advocates for exploring diverse political perspectives beyond the traditional spectrum to foster systemic change.

### Tags

#PoliticalIdeologies #BernieSanders #SocialDemocracy #TwoPartySystem #SystemicChange


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Bernie Sanders.
We're going to talk about what he is and what he isn't.
In the United States, we have an amazing lack of nuance
when it comes to the discussion of different ideologies
and philosophies because we're very, very well trained.
We have an acceptable discourse.
We have this range that we're allowed to debate.
Democrats, Republicans, left and right.
Those are the extremes.
If you go a little bit outside that,
you get the Green Party and Libertarians.
But it's all right in that range.
Now, on a global level, when you're looking at ideological
perspectives and you're looking at it on a spectrum,
those are right next to each other.
They're not extremes.
They're very, very close together.
They're both center-right.
We don't have a leftist anything in the United States.
Doesn't exist.
We have been conditioned to only talk about this narrow,
narrow frame, this little window.
And those are the philosophies that people are allowed to have.
When you get outside that, there's a just amazing lack
of understanding.
Bernie Sanders often gets called a communist.
And when people say that, you know, Bernie Sanders is a communist.
What I hear is, I don't know anything about communism.
Or I believe Bernie Sanders is a small government dude.
I'm willing to bet that people saying this do not believe that Bernie Sanders
is a small government guy.
Communism is a political and economic ideology, political and
economic system that focuses on communal ownership of the means of production,
communal ownership of property.
There's an exemption for your toothbrush, don't worry.
And well, the abolishment of class, money, and the state.
Do you believe that Bernie Sanders wants to get rid of the government?
Probably not.
Probably not something you believe.
He's not a communist.
And I know people are going to say, well, what about Russia?
The Soviet Union, Union of Soviet Socialist Republics, was what's in the theory
referred to as a transitionary state.
It was supposed to eventually become stateless.
That's the idea.
That's the idea behind communism.
It's reflected in their slogans.
You don't have a lot of nationalism, really.
It's not workers of Russia unite.
It's workers of the world unite.
It's stateless.
He is not a communist.
So what could he be?
He's often called himself a democratic socialist.
Maybe he is.
Maybe he is.
But even if that's what he really believes deep down, maybe he understands that there's
the way you want things to be and the way things are.
Are those the policies that he advocates for?
Democratic socialism requires democracy to regulate the socially owned means of production.
That means society as a whole owns all the companies.
They own the means of production.
The profits from those companies, society owns those too.
There's no shareholders.
There's stakeholders.
Everybody owns a piece of every company.
Coca-Cola, Coca-Cola, their profits would be used for society as a whole, not the shareholders.
Given the fact that Bernie Sanders often talks about taxing profits, he's not really a democratic
socialist, is he?
Because if he was, well, he'd want all of it.
Not a democratic socialist.
At least not in the policies that he espouses.
There are a whole lot of people out there who understand that the way things are are
too far from the way they want them to be, so they don't advocate for the entirety of
what they truly believe.
At least not openly, or at least not... they may advocate for baby steps to get there.
More reformists than revolutionary.
Then you have social democracy.
Now what is that?
That is the belief that there should be economic interventions to promote social justice and
liberal policies, and it operates within a capitalist economy.
That's Bernie Sanders.
That's what he is.
He's a social democrat.
Social democrats and democratic socialists, it sounds the same.
It's not.
There's that whole market socialist system that occurs in the democratic socialist ideology
that does not exist in the social democrat one.
A lot of pundits use the terms interchangeably.
They're not.
A lot of people will argue that social democracy is a step towards democratic socialism.
Yeah, maybe, maybe.
I can go with that.
That sounds fair.
And then some people would argue that democratic socialism is a step towards communism.
So then it's fair to call them a communist, right?
No, not really.
Because you can do that with anything.
You can take it... everything is connected to everything else.
You can go the other way with it.
You can take social democracy and move to the right instead of the left.
The problem is these terms, and there's a whole bunch of other ones, they have no meaning
in the United States.
They have no meaning because our schools and ourselves, we don't look into anything outside
of that acceptable norm, that narrative in the middle.
Republicans and Democrats, this is all that's allowed.
It's almost like it's a one-party system.
If we want deep systemic change in the United States, we might want to look into other philosophies.
We might want to look into other ideologies.
Because right now, we're leaving a whole bunch of options on the table just because we're
afraid of the terms.
We don't want to learn what they mean.
We don't want to take that next step and try to evolve.
I think that if we're going to get anywhere, we've got to break free of Republicans and
Democrats and the policies that they espouse.
Well, those are the extremes.
Because they're not.
They're both center-right.
And there's really...
I know being in the United States and being in this system, when we look at it, we act
like there's this massive gulf between the two.
If you're caught in that gulf, and you're one of the people who is being directly impacted
by that divide, yeah, yeah, it definitely seems like one is definitely better than the
other.
But there are a whole bunch of other options on the table.
And I really think we need to start looking into some of them to get some better, at least
understanding.
And if you are a right-winger, if you're one of the right-wingers that watch this channel,
if you look into this stuff, you'll at least understand where they're coming from.
And I will tell you right now, most of the hillbillies and rednecks that watch this,
if you actually looked into communism, you're not going to agree with everything.
You won't.
But you're going to agree with a whole lot more of it than you think you will.
Especially you guys that are all about power to the county.
Y'all are pretty much commies, to be honest.
Y'all just don't know it.
There's a lot of people who describe themselves as something that really believe something
else, but they want to stay within that narrative.
They want to stay within that allowed spectrum of discussion.
Get out of it.
Don't be afraid of it.
Get out of it.
Republicans and Democrats, they have had a very, very long run, and those policies have
had a very long run in the United States.
And here we are.
We might need some fresh ideas.
There's no reason to be afraid of ideas.
Anyway, it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}