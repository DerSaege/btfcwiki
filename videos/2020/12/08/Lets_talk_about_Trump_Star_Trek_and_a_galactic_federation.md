---
title: Let's talk about Trump, Star Trek, and a galactic federation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IBi0jz7Zqas) |
| Published | 2020/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A story about the Federation prompts an entertaining reaction on social media, involving aliens in contact with governments.
- Humans are not considered ready to join an exclusive club of intelligent beings in the galactic federation.
- The response to this story suggests that humanity is seen as the "bad neighborhood" in the galaxy.
- Lack of a warp drive is cited as a hindrance, but most people point to the state of the world as the reason for not being part of the Federation.
- Issues like food distribution, wars for resources, homelessness, and scientific focus on weapons are cited as reasons why Earth is seen as a problematic planet.
- Science fiction allows for political discourse in a different context, exploring the idea of advanced species wanting to help lesser planets.
- There is a belief that certain technological and philosophical milestones need to be met to join the Federation, influenced by science fiction.
- Beau questions whether the rejection of Earth's application to the Federation is due to humanity's acknowledgment of its failed state and lack of significant change efforts.
- He suggests that advanced beings might see Earth as a species that needs to be civilized and brought to a better future.
- The story includes an unbelievable aspect of Donald Trump being aware of the galactic federation but choosing not to talk about it.

### Quotes

- "We're very well aware of the fact we are behaving as a failed species, but we're not doing much to change it."
- "We know the world is messed up, and we're not doing much to change it."
- "Not technology."
- "He believed there were alien beings in space, he would, I don't know, build a wall in space or something."
- "We're going to have to face the fact that we have to change."

### Oneliner

Beau delves into a story about the Federation, reflecting on humanity's shortcomings and the potential for change to avoid a Mad Max future.

### Audience

Sci-fi enthusiasts, advocates for societal change.

### On-the-ground actions from transcript

- Start actively participating in initiatives that address food distribution and homelessness (implied).
- Advocate for peaceful resolutions to conflicts and work towards resource sharing (implied).
- Support scientific advancements focused on improving life for all beings (implied).

### Whats missing in summary

Exploration of how societal changes and global cooperation are vital for humanity to progress towards a more inclusive and advanced future.

### Tags

#GalacticFederation #ScienceFiction #Humanity #Change #Potential


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Federation and what we can learn from a story, not necessarily
a very credible story, but a story that broke last night and it prompted an entertaining
reaction on social media.
As the story goes, they're here.
Today those people from up there, aliens, they're here and they have been in contact
with a couple of governments.
And there are some less believable pieces to this story as well.
But a key part of it is that there is a galactic federation, but we're just not ready for
it yet.
Humans, humanity, we're not ready to join this exclusive club of intelligent beings.
And the thing is, when that piece of information circulated, the response was, well yeah, I
mean we get it, we're the neighborhood in the galaxy that everybody drives by with their
doors locked.
Got it.
And yeah, it makes sense.
And there are a whole bunch of reasons that people think that's the case.
So while this story doesn't have a whole lot of credibility, it's worth entertaining as
a thought exercise because I think there's something we can learn from it.
You know, obviously the number one hindrance to us joining a federation is the fact that
we don't have a warp drive.
I mean, everybody knows you have to have a warp drive to join the Federation.
But that's not what most people pointed to.
Most people just pointed around them, to the state of the world.
And that's the reason.
Because we are the bad neighborhood in the galaxy.
You know, we have enough food to feed everybody, but not everybody's getting fed because we
can't handle distribution on our own planet.
We have wars for resources because we still have a profit motivator for everything.
We have people living in the streets.
Our main scientific drive is to find ever more efficient ways to hurt each other.
Meanwhile not everybody on the planet has clean water.
These are the reasons that people pointed to.
The state of the world.
And on some level that makes sense.
And that has been heavily influenced by science fiction.
Science fiction is a unique tool that allows people to remove current political discussions
from this context and put it into another so it can be discussed more freely.
Barring some overriding prime guidance, some kind of directive that would bar intelligent
beings from interfering in the affairs of lesser planets, I would imagine that some
scientifically advanced species might want to help.
They'd probably be pretty compassionate.
You know, this idea that there are certain technological and philosophical milestones
that have to be met, that came from science fiction and it came from one of the largest
franchises.
The thing is that was really political.
That was an incredibly political series and I don't know that he actually believed that
would be the way intelligent beings from other worlds behaved.
I think that was his guidance.
This is how we get from a world that at the time was just bent on destroying itself, even
more so than now, to a world where we can boldly go where no man has gone before.
The thing is, I don't think that's right.
I don't think that's why, if such a federation existed somewhere, why they wouldn't approach
us.
I think the reason they wouldn't approach us is because when the concept, the idea of
our application being rejected surfaced here on this planet, we all laughed and acknowledged
it.
Like, of course they wouldn't.
I think that's the reason.
Because we're very well aware of the fact we are behaving as a failed species, but we're
not doing much to change it.
I think that would be the reason.
I don't think it would have to do with technology or philosophy, because it's not like we're
unteachable.
I would imagine that they would see us and want to civilize us, so to speak, and bring
us to the next generation.
We know the world is messed up, and we're not doing much to change it.
We're not striving to get ahead.
We're very complacent.
This is the way it is.
We are a backward species, and we're okay with it.
That's the reason we're the neighborhood that everybody drives past with their doors locked.
Not technology.
Now all of that is, the story is less than believable.
The most unbelievable part is the idea that Donald Trump is aware of said galactic federation,
but was convinced not to talk about it.
Let's just be honest.
If he knew about this, we would know about this.
He would have either said something while standing next to the MyPillow guy, or tweeted
it out in the middle of the night.
We'd also be able to tell by his reactions.
If he believed there were alien beings in space, he would, I don't know, build a wall
in space or something.
I'm sure some are good people, though.
At the end of the day, when we look at this, and we think about these possibilities, because
while this story is very less than credible, the idea of other beings on other planets
isn't.
It's kind of a mathematical certainty that there are.
We're going to have to face the fact that we have to change.
We have to pursue Star Trek, or we will end up with Mad Max.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}