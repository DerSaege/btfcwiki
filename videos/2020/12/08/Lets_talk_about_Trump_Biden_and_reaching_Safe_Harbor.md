---
title: Let's talk about Trump, Biden, and reaching Safe Harbor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UKysWnZ-NXU) |
| Published | 2020/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of safe harbor in relation to the electoral college.
- Mentions that if a state certifies their election results and resolves legal challenges by a certain date, those results enter safe harbor.
- Notes that almost every state, except Wisconsin, has completed this process.
- States that the overwhelming majority of states' results won't be challenged in Congress.
- Mentions that even if Wisconsin's electoral votes were not counted for Biden, he still has enough to win.
- Points out that the electoral college doesn't meet until the 14th.
- Expresses skepticism about any potential challenges having an impact at this stage.
- Talks about the possibility of Congress objecting to electors, with a Republican member planning to object in the House.
- Explains the process for objections to have an impact on the election outcome.
- States that the Trump administration's legal options are running out.
- Concludes that there is little realistic chance of any significant change in the election outcome.

### Quotes

- "Anything now that's going to change the outcome [of the election] would have to be just ridiculously bizarre."
- "There's nothing even remotely normal left that could alter the outcome of the election."

### Oneliner

The safe harbor concept ensures electoral results can't be challenged in Congress, making significant changes unlikely.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor updates on the electoral process (implied)
- Stay informed about legal challenges and election results (implied)

### Whats missing in summary

Details on the specific legal challenges or objections that may arise during the electoral process. 

### Tags

#SafeHarbor #ElectoralCollege #ElectionResults #Congress #LegalChallenges


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the safe harbor concept.
Everybody knows about the electoral college and how bizarre it is.
There are some procedural things that go along with it and one of them is a concept called
safe harbor.
What it boils down to is that if a state has certified their results, the election results,
and any state-level legal challenges have been resolved by a certain date, those results
enter safe harbor, which means they can't really be challenged in Congress.
Congress has to accept them.
The date for that to occur is right now.
It appears that every state, with the exception of Wisconsin, got it done.
So the end result here is that the overwhelming majority of states, their results aren't going
to be challenged in Congress.
Wisconsin has 10 electoral votes.
Even if there was some challenge and Biden didn't get those votes, he still has more
than 270.
Now the electoral college doesn't actually meet until the 14th, so a few more days.
That's when we will find out about any shenanigans that the Trump administration has planned
for that day.
It is unlikely that any of it's going to matter, to be completely honest.
Okay, now the Congress can object, members of Congress can object to various things as
far as the electors go.
There is a member in the House, a Republican in the House, I want to say from Alabama,
but it might have been Mississippi, that is going to object.
For that to matter, they also have to have somebody who will object on the same grounds
in the Senate.
Even if that happens, then both houses, so the House of Representatives and the Senate,
both have to sustain the objection.
The likelihood of the House of Representatives sustaining any objection that is going to
impact Biden being elected is a statistical zero.
It's not really going to happen.
This Congress person would have to bring some significant evidence of something that would
cause them to vote against Biden.
It's just not a realistic possibility.
So that's occurred.
The Trump administration and their legal strategy is running out of options.
Again, I don't think that this is a realistic possibility that anything is going to come
of this, but it is kind of reassuring to mark all of these little moments as they pass,
and we hit one today.
So there are enough electoral votes in safe harbor for Biden to win at this point.
Anything now that's going to change the outcome would have to be just ridiculously bizarre.
There's nothing even remotely normal left that could alter the outcome of the election.
So there we go.
Yeah, that's pretty much it.
Good morning.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}