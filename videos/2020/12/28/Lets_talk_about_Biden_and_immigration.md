---
title: Let's talk about Biden and immigration....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L-FDEGCSiAc) |
| Published | 2020/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's campaign aimed to solve immigration issues on day one, but a realistic timeline is around six months.
- Setting up fair asylum judges is understandable due to political constraints, but other aspects remain unclear.
- Most immigration changes can be achieved with a pen stroke, allowing the Biden administration early wins to fulfill promises.
- Anticipating a surge of migrants due to Trump administration policies negatively impacting many lives.
- Urgency is vital as people's lives are at risk, and delays could be detrimental.
- American people demand deep systemic change, and immigration policy is a critical issue.
- Failure to address key issues like climate change and healthcare overhaul may prompt the formation of a left party.
- Immigration judges are acknowledged as a challenging but necessary step, while other changes seem easily achievable.
- Immediate action is imperative as people's lives hang in the balance amid immigration policy shifts.
- Addressing treatment of individuals requires swift action, especially in undoing Trump-era policies.

### Quotes

- "The reason there's going to be a surge is because their lives are at risk. They can't wait six months."
- "People's lives are at stake."
- "Most of this is pen stroke stuff."
- "American people demand deep systemic change."
- "This is something that needs to be addressed immediately."

### Oneliner

Biden's immigration timeline faces scrutiny as urgency for immediate action to save lives becomes paramount amid policy shifts and demands for systemic change.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Advocate for immediate action on immigration policy changes to save lives (implied).
- Support movements demanding deep systemic change in immigration and other critical issues (implied).

### Whats missing in summary

The full transcript provides deeper insights into the urgency of addressing immigration policy changes immediately to save lives and fulfill campaign promises effectively.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Biden and immigration.
One of the Biden campaign's early statements is that they were going to solve a lot of
problems on day one.
I said at the time that wasn't realistic, it was going to take a little while.
I was thinking like 90 days.
He has recently set out a timeline of six months.
It's not entirely clear what he's talking about.
One of the things that was mentioned was getting fair asylum judges.
Yeah I can see that.
I don't like it, but the political realities of getting them in place, yeah I get it.
The rest of it, no.
I don't get it, I don't understand it at all.
Most of this is pen stroke stuff.
This is a bunch of early wins that the Biden campaign can bank, can show that they're actually
living up to their promises of trying to build back better.
The whole campaign was I'm not Trump.
You have to show the American people that.
The concern is that there's going to be a surge of people trying to come to the US.
Yeah of course there will, absolutely.
There will be.
And yeah, it's probably going to be difficult.
We are the United States, we can handle it.
The reason there's going to be a surge is because there are a whole bunch of people
who are negatively impacted by the Trump administration's rules.
Rules that can be undone with a pen stroke.
The reason there's going to be a surge is because, well, their lives are at risk.
They can't wait six months.
And the Biden administration shouldn't want them to.
The Biden administration is coming in at a time when there are a whole lot of people
who are politically engaged that haven't been in the past.
And they want real change, deep systemic change.
This is something that can be done early on, up front, very easily, to show that at least
something is going to happen.
There are certain topics that the American people are going to be intractable on.
This is one of them.
This real action on climate change, a major overhaul of the health care system, these
things are going to be make or break issues for the Democratic Party.
There are already movements that are starting to suggest a split in the creation of an actual
left party that have some pretty big names behind them.
If the Democratic Party does not want a spoiler third party running in two years or four years,
they're going to have to act.
And this is one of the topics that is going to be at the forefront.
I can understand the immigration judges.
I get that.
I understand getting them in place is going to be difficult.
That part I can swallow.
Don't like it, but I get it.
As far as a lot of the stuff regarding treatment of people, it's a pinstripe.
It is a pinstripe.
Undoing a lot of the issues that Trump caused is a pinstripe.
Yes, there's some logistical stuff that has to be worked out, but you've got a really
good team and you've had them for months.
This is something that needs to be addressed immediately.
People's lives are at stake.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}