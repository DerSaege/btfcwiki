---
title: Let's talk about Trump losing influence and the relief bill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iZm-5ZKcaZc) |
| Published | 2020/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump signed the relief bill earlier than expected.
- The bill being signed means that payments will restart.
- Due to the delay in signing, there may be a gap in unemployment payments.
- Americans relying on these payments should prepare for a possible missed payment.
- Senator McConnell seems to be winning the internal power struggle within the Republican Party.
- Trump's involvement in the Georgia elections will tie him to the outcome.
- The electoral votes will reveal who has won the power struggle.
- McConnell is predicted to emerge victorious in this struggle.
- The political calculations are focused on power and influence rather than the American people.
- Trump's actions were more about political power than genuine concern for the people.
- The delay in payments may impact economic recovery as less money will be spent.

### Quotes

- "This is all about political power and seeing who can get the most after the transition."
- "Just be aware that it's coming and anything you can do to lessen its impact on you."
- "Trump's actions were more about political power than genuine concern for the people."

### Oneliner

President Trump signed the relief bill earlier than expected, but the delay may cause a gap in payments and hinder economic recovery, reflecting political power struggles over genuine concern for the people.

### Audience

American citizens

### On-the-ground actions from transcript

- Prepare for a possible delay in unemployment payments (implied)
- Be aware of the impact and find ways to lessen it (implied)

### Whats missing in summary

Analysis of the potential long-term effects on economic recovery from the delay in payments. 

### Tags

#ReliefBill #UnemploymentPayments #PoliticalPowerStruggle #EconomicRecovery


## Transcript
Well howdy there internet people, it's Beau again.
So President Trump caved.
He blinked.
He signed the relief bill.
Good.
So we're going to go over some takeaways from this because there are a few to go through.
The most important is that the bill was signed.
And signed earlier than most people projected.
I didn't have him signing it until Tuesday.
Some were saying that he was going to hold off until after the electoral vote.
But he signed this weekend.
And that's good.
That is good.
That means payments will restart.
The bad news is that there was already a break.
The deadline had already lapsed.
If you are dependent on unemployment payments through these programs, make preparations
for you to miss a payment.
Because it's likely.
The government is an unwieldy machine.
It takes time to start and stop.
So it is pretty probable that a lot of Americans are going to miss a payment.
If you can, I don't know how you can, but if you can, try to plan for it.
Try to mitigate it somehow.
I don't know how you'd really be able to do that with as little assistance as being given out.
So that's one takeaway.
Another is that it does appear that Senator McConnell is winning the internal power struggle
for control of the Republican Party.
There are still some other things that we'll be able to gauge by the electoral vote and
the elections.
In Georgia, Trump has decided to campaign and throw his weight behind the candidates
in Georgia.
That is going to tie him to the outcome of those races.
Whether they win or lose and how much the Senators and Representatives believe Trump
influenced that win or loss, that's all going to figure into the political calculations
that are going on right now.
We will get to know for sure who has won the power struggle when they do the electoral
votes.
My guess is McConnell is going to win.
The smart money is on McConnell.
When this occurs, and undoubtedly some will launch objections, keep in mind scale.
If you have just a handful supporting them, McConnell won.
If there's a lot of them, Trump won, which means he is going to retain political power.
We can safely assume that he does not have as much influence within the Republican Party
as a lot of his supporters believe.
If he did, he wouldn't have had to blink on this.
He wouldn't have had to cave.
The other thing to keep in mind is that this wasn't about you.
This was not about the American people.
Nobody cares.
The people involved in this decision, the people playing these political games, do not
care about you.
This is all about political power and seeing who can get the most after the transition.
In true Trump fashion, he created a situation that put people at risk and then tried to
act like the savior.
It's classic Trump.
So those are the key takeaways.
But please, just because this bill was signed, please understand that there is likely to
be a delay in payments.
There could be a break.
I know there's no real way to schedule for it at this point, but just be aware that it's
coming and anything you can do to lessen its impact on you would be good.
This is also going to hinder economic recovery because even though it may only be one missed
payment, that's a lot of money that isn't going to be spent.
So it may slow that down.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}