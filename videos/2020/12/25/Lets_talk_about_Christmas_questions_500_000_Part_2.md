---
title: Let's talk about Christmas questions/500,000 (Part 2).....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dLYYawe5Ti0) |
| Published | 2020/12/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares basic dishes from around the world, nothing too special.
- Unsure about moving to Georgia or voting.
- Plans for window panes in greenhouse, but preoccupied.
- Met lovely wife in a simple way.
- Suggests traditional ways for workers to unionize.
- Favorite holiday food: sugary Christmas trees from Little Debbie.
- Trims own beard with wireless trimmer, knows it grows back fast.
- Won YouTube plaque at 100k subscribers.
- Birds, squirrels, snakes enter shop due to design.
- Enjoys animated movies, Frozen 2 a current favorite.
- Prefers cordless power tools.
- Missed chance to wrap video in Christmas lights.
- Possibly bought a Nirvana t-shirt as a teen.
- Kids sometimes reply with "it's just a thought."
- Handles hundreds of messages daily via social media.
- Longs to travel all over the United States.
- Struggles to show t-shirts clearly in videos.
- Likes chocolate Santas or Nestle Crunch bars for stocking stuffers.
- Laughs most with kids, never heard a belly laugh.
- Kids unaware that Santa prefers tacos.
- Favorite milkshake is Oreo from Sonic.
- Offers advice on explaining libertarian positions.
- Concerns about benefits of artificial intelligence in war.
- Not a fan of hunting, prefers cordless power tools.
- Metal roof withstood a category 5 hurricane.
- Dressing like Santa for Christmas requested.
- Supports abolishing political parties with more vigilant voters.
- Prefers Denzel Washington as a better actor over Tom Hanks.
- Loyalty to the Gators but not a sports follower.
- Dogs and pets featured more in future content.
- Ideal Christmas dinner includes Green Bean Casserole and ham.
- Dream to run a facility akin to Highlander Folk School.
- Acknowledges not all military members are heroes.
- Describes healthy masculinity as understated and subtle.
- Interested in working with Bailey Saron on true crime content.

### Quotes

- "Birds, squirrels, snakes enter shop due to design."
- "Longs to travel all over the United States."
- "Dream to run a facility akin to Highlander Folk School."
- "Not all military members are heroes."
- "Describes healthy masculinity as understated and subtle."

### Oneliner

Beau shares about basic world dishes, future plans, favorite holiday treats, beard care, social media engagement, and dream travel goals.

### Audience

Content creators and social media users.

### On-the-ground actions from transcript

- Watch Working Stiff USA on YouTube (suggested).
- Invest in a good wireless trimmer for beard maintenance (implied).
- Show solidarity towards countries impacted by US foreign policy (generated).

### Whats missing in summary

Insights into Beau's personal experiences and anecdotes related to charity work, pets, travel dreams, and content creation. 

### Tags

#Community #Food #Travel #SocialMedia #ContentCreation


## Transcript
how to cook different dishes from all over the world but they're just basic
dishes from those countries so I don't know that it's really like anything
special. When are you moving to Georgia and when will you vote? I don't know
about moving to Georgia. I don't know about that. Can you explain your plans
for the stack of window panes? I could use them. So those were in the shop when
we got the property. Now when I the original plan was I was going to use
them in a greenhouse but it's it's been a while and I have been pretty
preoccupied. How did you and your lovely wife meet? I just said. How can workers
unionize? The same way they have throughout history. I mean there's
there are tried-and-true methods for this. I would suggest checking out
Working Stiff USA on YouTube.
What's your favorite holiday food? Those ridiculously bad for you
over-processed sugary Christmas trees from Little Debbie. Do you trim your own
beard and if you do do you have any tips for someone who hasn't and is tired of
waiting to go to the barber? I do get a good wireless trimmer and just do it and
understand it's gonna grow back right away. I mean I my beard grows very
quickly so if I mess up if something happens it's fixed in like two days
because it grows back. More questions about the windows. Did you win a YouTube
plaque? I got one at a hundred thousand subscribers. I think the next one isn't
until there's a million. I don't I don't know maybe there's one for five hundred
thousand. My husband wants to know how the birds get into your garage. So the
the where I normally film is a shop. It's freestanding. It's a freestanding
building and it is designed to let heat out so there's a gap that birds or
squirrels or snakes or whatever often come in. It's like Noah's Ark out there
at times. That's why we don't live stream out there. It's so far away
from the house the Wi-Fi doesn't reach.
What is your favorite animated feel-good movie? Right now Frozen 2 is one that I'm
watching a lot because of my daughter but in in general I like almost all of
them. They can help you get in a mood that is better than the one you
were in. Corded or cordless power tools? Cordless.
Do the video wrapped in Christmas tree lights and they have to be on? I wish I
had seen this earlier I would have done that. Growing up what was the first
t-shirt you went and bought for yourself? Not bought by a parent. I don't
know but probably like a Nirvana t-shirt or something like that. How often do your
kids respond with it's just a thought more than I would like them to? And
generally it's when they've got me and they know they've got me. Do you read all
of your patreon and Twitter messages? What is the best way to message you? The
fastest way to message me is via Twitter however it may get missed. Between
Instagram and Twitter and Facebook and patreon no joke I get 400 messages a day.
I try to get through as many of them as I can. I can't get through all of them.
Patreon I sit down about once a week and try to answer as many of them as I can.
So there's dedicated time for that. Everything else is hit and miss. Twitter
is something that I use in my downtime so it may be that it's easier to reach
me through that. Once it's safe to travel again is there any place special you
want to go? All over the United States. Everywhere. I do not like not traveling.
That is the one thing that is truly truly starting to grate on my nerves
about all of this. So can you please show us a better view of your t-shirts in
your videos? I've tried to figure out a way to do it without making it weird.
Standing further back it messes up the entire framing and it just it doesn't
look right. Stepping back in the middle of it if I remember to do it sometimes I
do or I will raise up so you can see the bottom text. But on the second channel
the framing will be different and it'll be shot from different angles inside the
shop when we're there so you'll get to see more of it and some of it is full
length so you'll be able to see some of the shirts. Let's see. Do you have a favorite
sweet for your stocking stuffer? You know those like horrible trash chocolate
Santas? I actually really like those or the ones that taste like Nestle Crunch
bars. How much do you love your beard and why is it so magnificent? What really makes you
laugh? I've heard a snicker but never a belly laugh. My kids make me laugh a lot
all the time.
Why don't kids understand that Santa prefers tacos? We haven't educated them
properly. What's your favorite kind of milkshake? The Oreo one at Sonic. How do I
explain to a right libertarian, yellow and black, that his position is self-defeating?
So when you get somebody on the right who is willing to embrace anti-
authoritarianism, asking them to give up their economic system at the exact same
time, maybe a little much. It might be better to try to move them to a
centrist market type but not outright capitalism and go from there. You are
asking a lot for people to reject the idea of the state and the economic
system they're familiar with at the same time. Some people need more stepping
stones than that. As far as explaining that it's self-defeating, just carry it
to its logical conclusion. Eventually power wealth will consolidate and you
can try to provide examples of that happening but it doesn't matter what you
say. This person will probably say that's not capitalism, that's corporatism.
Technically that's actually a true answer in most cases. At the same time I
don't know that it would vary the outcome. Do this video in Santa cosplay.
I don't want to be ignorant so I put something together on Amazon and then
some news broke and I didn't want to use Amazon. I'll find another way to put
that together. What do you see being the biggest benefits of artificial
intelligence and machine learning in war? Okay so I've heard a whole lot of
benefits. I think it's people looking for benefits because it is terribly
efficient which is generally a bad thing. I think the main benefit would be that
theoretically there would be less civilians injured but I would say that
over time the true benefit would be that it is going to become so costly that
nation-states won't want to engage in it. It's kind of that mutually assured
destruction thing from the Cold War. Do you enjoy hunting? Not really. I'm like
that guy from the Redneck Comedy Tour. It is really early, really cold and I do
not want to go. When I was younger I did but it definitely lost its appeal to me.
Is that a metal roof? Yes. Can we see it from the outside? How do you like it?
It withstood Michael. There's not much of a stronger endorsement than that. It
withstood a category 5 hurricane.
Dress like Santa. All I want for Christmas is a Bo video about how Moana is the
greatest Disney princess. She's not a princess, she's a daughter of the chief.
Breaking the only law imposed by the state to solve a food insecurity
disaster. Kind of seems like your bag. Yeah, that is totally on brand for me.
That's a video that's gonna get made. That'll happen. Are you in favor of
abolishing political parties? Yes, once the average voter is more willing to
keep an eye on their rep. Tom Hanks versus Denzel Washington. Who's the
better actor? Depends on whether you want a good good guy or an antihero. Good good
guy is obviously Tom Hanks. The antihero is Denzel Washington. As far as better
actor, I would suggest it is harder to pull off a role like Man on Fire than a
lot of Tom Hanks's roles. And to see you know Man on Fire is a true story.
Happened in Italy though. Anyway, so I would say I guess Denzel Washington,
better actor. Let's see, lots of questions about my t-shirts. Do you watch sports? If so,
what are your teams? I will watch a game socially. I don't really follow sports. I
owe some loyalty to the Gators. Do you condition your beard? No. Can we see your
dogs more? Yes. Try to figure out a way to do that.
Introduce us to your pets. Which include the kinds that live in bars. That's gonna
happen eventually. We'll do maybe do something on horseback. Tell us your
celebrity crush. That just seems like a bad idea. Let's see. A time I got in
trouble that wasn't... okay. A time I got in trouble. I was sitting... okay, so my wife's
pregnant and I don't remember what it was that she wanted, but it was at
Walmart and I had to go get it and for some reason the store was closed. I think
there was actually a protest earlier there, earlier in the day there. And I'm
sitting outside waiting for them to open because I know it's going to be
relatively soon and a cop walks up and demands my ID. I'm sitting in my car and
he's like, I need to see your ID. And I'm like, no you don't. And he's like, yeah I
do. Like, no you really don't. And we have a back-and-forth for a few
moments and he's like, you don't want to go to jail over this. And I'm like, you
don't know that. I really might. You know, I've got a whole bunch of kids and a
pregnant wife. I can totally use a good night's sleep right now. And at this
point is when the sergeant walks up who had met me working covering the protest
over John Crawford. And he kind of pulled the cop away. But that
turned... that was probably something that would have turned ugly at some point.
Give me your hope and dreams for 20 years from now. I want to run a facility
like Highlander Folk Academy, like the Highlander Folk School in Tennessee. And
that's what I would like to do. That wasn't personal. That was about the
nation. Who is president? Don't know. How's the racial divide? In 20 years I would
hope a whole lot better. Health care, it's the United States so we probably still
won't have it. Immigration, I don't think we have a choice on this one. I think
climate migration is going to force us to change our views on it. And we will
become more accepting. Or we will end up being a country that is sanctioned.
Boxers or brief boxers. Basic IFAC suggestions. That's individual first aid
kit. Things that may or may not be in yours. Just always make sure you have a
tourniquet. A good one. You know, one of the little the black nylon ones with the
spinny handle. And then I would... maybe chest... chest... chest seals. And I don't know
if it's a medical device or not so I don't know if you can get them. But chest
decompression needles. And if you have one make sure you know how to use it.
Don't just get it and stick it in your bag and not know what you're doing.
Definitely go through whatever certification process it takes for your
use of that to be legal. But that is something that it's one of those things
that if you don't have it there's a negative outcome. So that those would be
things that I would look into. But I'm I'm not sure if the decompression
things are medical devices that are regulated or not. You'd have to look into
that.
Which of the 50 United States do you think is in general most in line with
our, the community's, values? The most actually progressive leftist state as
much as we might even have something by that description. Would depend on what
you mean by leftist. Depending on whether you mean the government or the people.
Here in in this part of Florida as red as it is the individual person behaves
in a very leftist manner. Although they probably all have bumper stickers about
how much they dislike socialism. The way they actually behave is something else.
As far as the government, maybe Massachusetts. I don't know. I don't know.
Who, what inspired you to be the powerhouse journalist you are today?
Yeah I don't think that's an accurate description at all. I think that I take a
lot of my influence from Orwell, Thompson, Hunter S Thompson. People like that.
People who used fiction to get to truth in their journalism.
Do your kids help out with some of your mutual aid? How much do you recommend
kids be involved in direct action? Depends on the type. It depends on the
type. If it is something where you are out doing something that is objectively
good and will not run afoul of law enforcement, sure bring your kids. If not
then don't. My kids do go with me at times. In fact there's a video called a
tale of two neighborhoods. My kids were with me through that entire story.
You can say you you say you can go without sleeping. Is this a technique
learned to best utilize time or are you one of those people that can live off of
a couple of hours? I've always been able to not sleep a lot. I've never actually
required a lot of sleep but as I get older it's even less. I honestly don't
think it's a technique. I think it's like PTSD. I don't think it's a good thing.
I'm sure it will catch up with me at some point.
I hope this isn't too serious. Favorite gun? The whole community has
changed a lot since I carried or owned. I'm kind of stereotypical. An older
white guy from the South. Yeah I carried a 1911. I was a big fan of the USP and
its big brother. As far as larger stuff I was a fan of the mp5. A real big fan of
the K version. I think as far as fun I really liked the L1A1. I wouldn't want
to carry it. I wouldn't want to actually use it in any kind of situation where
it's designed to be used but it's a very unique fun weapon to shoot.
When the Rona era is over where would you like to go on vacation and why?
Puerto Rico. Puerto Rico my wife hasn't been and and yeah.
Since you mentioned jaywalking in a recent post perhaps an explainer? That's a good
video that I'm not getting into now but that's a good video idea. Do you have or
have you had a motorcycle? I do not because I'm one of those people that
tends to get lost in my own thoughts at times and that doesn't lend itself well
to riding a bike. I can ride but I'm a menace. I shouldn't. How would you define
healthy masculinity? Years ago I had a Japanese, an older Japanese man explain
an art concept to me. I have a video on it I'll try to find it below and I think
that's a good way to look at it. It's understated, subtle, imperfect. I'll find
the video. Do you have a blooper video? I've put a couple on Twitter over you
know over the months or years or whatever it is now. Normally it's when I
get fired up and some language slips out. At some point maybe we'll start saving
some of those to put them together on a reel in one video but as it is once I'm
done recording I don't save them so the outtakes, they're gone forever.
With the clothing industry typically having horrible conditions for workers
why don't you get your merch from a place which is open about where they get
their clothing? My understanding is that they get it from Tennessee. My
understanding is that they're a very ethical company. If you know something I
don't please let me know. That's surprising. So yeah definitely if you
know something about them that I don't let me know. If you were president what
would your main priority or priorities be? Foreign policy, immigration, things
like that. Shifting our role in the world from being the world's policeman to
being the world's EMT. Downsizing the military, stuff like this. I know most
people would want to focus on domestic issues first. I would point out that if
we focus on changing our role overseas there's gonna be boatloads more money
and we can reprioritize a lot of resources. So I think that that would be
need to be curtailed first. So that's where I would probably start. I'd like to
hear about the connections between video content and you're wearing a Beatles
t-shirts. Normally when I'm wearing a shirt from a band it is there is a
connection to a specific song that they have done in the video. Not necessarily
the shirt or the band name. The Beatles are a little bit different because a lot
of times I will be talking about something from the United Kingdom and I
have a Beatles shirt that has a Union Jack on it.
Request for channel recommendations. I will set up the the channel
recommendations for on YouTube so you guys can click the tab and see the ones
that I listen to. Because there's a whole bunch out there that don't get
the attention they should. Like you know people are mentioning Ed Trevers
because yeah. But there's a whole bunch of people like that that I think
y'all would like but because of their appearance or the graphics that they
use you probably skip right over them. Kind of like with me initially. You know
people look at me and they're like yeah this guy is about to say some racist
stuff. And there are people who have that visual appearance or not
necessarily that one but have some other stereotypical visual appearance that
people may just skip right over them because of that. And they have a lot of
good information. How long have you had the beard? Off and on like forever. I
think the last time I had a total shave was in like 2008.
Can we acknowledge that not all military members are heroes? Not all need to be
thanked for their service and not all are good people. This is coming from
someone who is who served 12 years and is a disabled combat vet. On
this channel I don't think that's a point that needs to be made. I think most
people who watch who are watching this completely understand the military is a
massive organization and there are good and bad people in it. There are outright
horrible people in the military and that's before you even get into politics
and the actual outcomes of military service and its use and all of
that stuff. Just on a personal level there's going to be good and bad people
everywhere. With whom would you most want to have dinner and a conversation with?
Living or dead? Marilyn Monroe. How does one build international solidarity? By
showing it first. When it comes to that the ball is in the court of the
people in the United States. I don't know that we do enough to speak out for
countries that are victimized by our foreign policy and I think that would be
a good place to start. Is Santa Claus real? Yes. What is Miss Bo's idea of an
ideal Christmas dinner spread? Green Bean Casserole, ham, stuffing, mashed potatoes,
pumpkin pie. I think that's it. I don't know. I'm sure there's other stuff that I'm
missing.
What YouTube creator whom you haven't already collaborated with would you like
to work with in the future? Bailey Saron. In fact, I reached out to her in the
middle of October and if you don't know she does like a true crime series. That's
like around Halloween is like not a good time for her and then we've had the
election since then so I'm gonna have to reach back out. She's somebody I'm dying
to know how she does her research and I think that most people on this channel
would enjoy her content.
With all the charity work you do, was there ever a time that stands out as a
particularly happy moment? Like a moment that reminds you of why you do it? I don't
know if this would qualify as charity work in most people's minds. There was a
group of people that had recently come to the United States and I went with
these older men and they were coming from a pretty country that had
seen a lot of trouble and we went to Walmart and seeing grown men cry over an
abundance of produce was definitely something that will probably always
stick with me. What local dish restaurant would you miss the most if you ever
moved away from the area? The Donut Hole. They have a plate there called the diet
plate which is not for anybody on a diet. It's a giant pile of breakfast food.
And McGuire's as well would be another good one.
If you can make a Curious George episode, what would be the plot, the message, the
actual story of Curious George? Because it's awesome. I don't know how you would
actually make that into a Curious George episode but somehow bring the history of
that character to life because when you first see it you might have a very wrong
impression of where it came from. I did until I found out the story. I did a
video on this. I'll try to drop that below too.
If you and Beau's wife are in North Carolina, would you ever consider stopping by for a
Caribbean meal? Yeah, always.
What's your favorite dinosaur? The Scrapadactyl. Yeah, I'm going to say the
Raptor to be honest and mainly based off of the ideas the image put forth in the
Jurassic Park where they're really smart. I don't know.
Do you put any faith in the character assessments made by your dogs? More than
you can possibly imagine. Yeah, especially Destro. If Destro doesn't like you, I
just assume you are a horrible human being.
Do you have any animals? I have two dogs and a couple of horses. I'm actually sure
that there's probably other animals that my wife has picked up along the way that
I don't even know about. Not safe for work blooper reel. What is the
most funniest or most embarrassing thing that has happened to you while filming?
Recently and I actually caught it before it went out, recently my stomach was
growling. I filmed something and my stomach was growling and I didn't think
the mic would pick it up but it was so loud. I actually we I reshot the
video after I...

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}