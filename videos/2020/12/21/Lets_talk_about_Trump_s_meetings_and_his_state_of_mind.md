---
title: Let's talk about Trump's meetings and his state of mind....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v9Jvf1vLZzE) |
| Published | 2020/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump's reported meetings raise questions about his control and judgment.
- Psychologists caution against diagnosing based on headlines.
- Those in Trump's orbit may be obligated to act if he has indeed "lost it."
- Public disclosure and debunking of extreme claims are necessary.
- Concerns about invoking the 25th Amendment if Trump's judgment is impaired.
- Rudy Giuliani's role as a voice of reason is concerning.
- Staying silent may suggest complicity in dangerous actions.
- Urges GOP members to speak out for the truth.
- Silence is seen as complicity in enabling potentially harmful behavior.
- Encourages transparency and disclosure from those close to Trump.

### Quotes

- "Silence here is complicity."
- "Those in his orbit are now obligated to come out and actively, openly, and publicly explain and debunk..."
- "We need a lot of disclosure here."
- "Your voice is becoming very important."
- "If they want to salvage any, any shred of public image, they have to come forward."

### Oneliner

President Trump's reported meetings prompt concerns about control, judgment, and the need for transparency from those in his orbit to debunk extreme claims and uphold accountability.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact politicians to demand transparency and accountability from those in power (implied)
- Speak out against harmful actions and enable public discourse (implied)

### Whats missing in summary

Analysis of the potential consequences of enabling dangerous behavior and the importance of holding leaders accountable.

### Tags

#PresidentTrump #Transparency #Accountability #Politics #Leadership


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about President Trump and the meetings that reportedly took
place over the weekend, the content of those meetings, and what those in his orbit may
now be obligated to do.
We start off by saying we are going to operate under the assumption that what is being reported
actually happened.
Those meetings did in fact take place and the content of those meetings is what's being
reported.
At that point, there's really only two things going on.
Either the President is in control or he is not.
It's that simple.
Some are suggesting that he is no longer in control, that he has suffered a break, that
Biden defeating him was so crushing that the walls are closing in and he is now impaired.
Now I would like to point out that I've talked to psychologists about this topic in a way.
The one thing that all of them said is that they would never attempt to diagnose somebody
based on headlines.
So that's something to keep in mind as there are a bunch of claims in headlines that he
has just lost it.
Those who I know who are in that field say they would never attempt to make a diagnosis
like that without actually talking to the person.
Be that as it may, those in his orbit, they know.
They know.
And that's really what all of this hinges on.
If he has lost it, there's one set of options.
If he hasn't, there's another.
If he hasn't understand that them going to the press and feeding them tidbits, that's
not enough.
That is not enough.
They're obligated to do more than that.
The topics being discussed are far too extreme to just be left with a leak.
That's not enough.
If those conversations occurred, those in his orbit are now obligated to come out and
actively, openly, and publicly explain and debunk the president's wilder claims.
Not even his claims because he's not really making most of them.
He's just leaning into them, giving them air.
They have to come out and explain to the American people what's going on and whether or not
those claims are accurate.
And more importantly, whether or not the president believes they're accurate because he has a
long history of saying things that we know he doesn't actually believe.
It can't just be a warning through the press like that.
That's not enough.
If he has suffered some form of break, it may be time to take a long, hard look at the
25th Amendment.
If his judgment is impaired to the point where he would entertain some of the ideas that
are being reported, his judgment is not up to the task of being the president of the
United States.
That lack of judgment is not going to be limited to his efforts to overturn the election.
It will extend into other things.
And we kind of have some other stuff going on right now that requires his attention.
So it may be time to look at that provision in the Constitution.
I know that there is some idea that there are those in his orbit who are staying there
to try to be the adult in the room.
By what is being reported, Rudy Giuliani was a voice of reason during these conversations.
That should kind of indicate how far it's gone.
With these topics, there is no harm reduction.
There is no being the adult in the room.
If they stay on and do not come out and publicly debunk these claims, it doesn't read to
me that they're attempting to guide the president, that they are attempting to keep
it from going off the rails.
It reads more like them sending up a balloon to see which way the wind's blowing.
And make no mistake about it, that is how history will record it.
If they want to salvage any, any shred of public image, they have to come forward.
They don't have a choice.
We're at that point.
What's the worst that's going to happen?
He's going to fire them?
They're out of a job in a month anyway.
Now to set people's minds at ease, realistically, the wildest claim that got floated, that's
not really a thing.
The most likely outcome, if that was ordered, would be for the brass to drag their feet
until the courts stepped in.
That's the most likely outcome.
They could just tell him no.
I mean, that's also a possibility.
But I think the most likely outcome would be for them to say, okay, we'll do it and
do it real slow and wait for the judges to step in.
Because even given, even if you were to give the administration the widest definitions
possible, it doesn't warrant it.
I wouldn't worry too much about that.
That doesn't seem likely.
But the idea that it is being floated in the Oval Office, that's something that the
American people need to understand.
We need to know where Trump is at.
And the only people that can actually tell us that are those in his orbit, those who
went to the press to try to figure out how the American people felt about that or actually
intended on issuing a warning.
Either way, that's not enough.
We need a lot of disclosure here.
We need a lot of information.
And for those in the GOP, understand that if a situation arises in a country where the
head of state begins doing what was reportedly suggested, it's never limited to the opposing
political party.
Out of self-interest, it's probably time to begin telling the American people the truth.
I know that's really hard to do up there on Capitol Hill and all.
But your supporters, they listen to you.
They may not like what you have to say when you say this, but your voice is becoming very
important.
Silence here is complicity.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}