---
title: Let's talk about the good you do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_D9qaJs_Ch4) |
| Published | 2020/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Organized a fundraiser through a live stream to put together gift bags for teens in need, specifically those in a domestic violence shelter during the holidays.
- The goal was to provide tablets, cases, battery backups, Bluetooth speakers, earbuds, gift cards, and other essentials for the teens.
- Despite initially planning for five bags, they ended up successfully putting together 12 gift bags.
- The tablets included in the bags provide the teens with a personal space to retreat and process what's going on around them.
- The fundraiser exceeded expectations, raising around $10,000 in just an hour and a half.
- Beau expresses deep gratitude towards the community for their overwhelming support and generosity.
- Along with gift cards and essentials, additional funds were donated directly to the shelter for their discretion.
- Beau acknowledges the collective effort of individuals coming together to achieve a common goal.
- Plans to showcase the behind-the-scenes process of fundraising and mobilization through a video on the second channel.
- Emphasizes the impact of collective action in bringing light to dark spaces and making a difference in children's lives.

### Quotes

- "Y'all made 12 kids really happy."
- "Everybody contributes in their own way."
- "When it comes to stuff like this, everybody contributes in their own way."
- "We have to set our sights higher when we do stuff like this."
- "It's heartwarming, even to me."

### Oneliner

Beau organized a successful fundraiser through a livestream, exceeding the goal and providing gift bags for teens in need, showcasing the power of collective action and generosity.

### Audience

Community members, supporters

### On-the-ground actions from transcript

- Donate directly to shelters for immediate assistance (suggested)
- Support fundraisers and live streams for charitable causes (suggested)
- Participate in engagement activities like sharing content to increase reach and impact (suggested)

### Whats missing in summary

The full transcript provides a detailed account of Beau's successful fundraiser, showcasing the impact of collective action and generosity in supporting teens in need during the holidays.

### Tags

#Fundraiser #CommunitySupport #TeenEmpowerment #CollectiveAction #Gratitude


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the fundraiser
and how it went, and we're gonna show you how it went.
If you don't know, last week, maybe the week before,
not sure, we did a live stream to raise money.
Specifically, main goal was to put together gift bags
for teens who are in a bad way right now.
It's the second year we've done it.
Last year we had to put together five bags,
this year we had to put together 12, and succeeded.
So in those bags, there's a tablet, a case,
a battery backup, Bluetooth speaker, earbuds,
and then Netflix card, prepaid Visa card,
prepaid Visa card, Domino's card, and then
they're also going to get a $15 Google Play card,
but they're in multi-packs and I haven't broken them up
and put them in the bags yet.
The gift bags are for teens who are in a domestic
violence shelter over the holidays.
So the reason we do tablets, even though this year
I considered doing Chromebooks,
but I got overruled by people on Twitter.
The reason we do the tablets is because the younger kids,
if they're around, they may be monopolizing the TV.
This gives them a space to kind of retreat to,
be alone with their own thoughts.
It may just be an eight inch screen,
but it's their eight inch screen.
They can process what's going on.
That's the reason we do these.
That was the goal.
Realistically, we fulfilled that goal in probably
the first 15, 20 minutes of the live stream
because y'all are amazing.
I had no doubt that we'd be able to raise the money for it.
We grossly, ridiculously overshot our goal.
I can't thank you enough for it.
So that explains why my shop looks like a lingerie store.
Shelter also said they needed pajamas and bras
down on the floor below.
And you can't see a bunch of paper products too,
which are like trash bags, paper towels, wrapping paper,
because other people are taking care of gifts
for younger kids, but they're coming unwrapped.
So they have that.
In front of me, we have additional gift cards
for like dominoes or whatever
that the shelter can distribute.
As they see fit.
We have underwear, socks, leggings, more pajamas,
a whole bunch of thermal stuff like that.
Somebody during the live stream specifically said
to get Batman pajamas.
Done.
So we also have all of this.
On top of that, we know a bunch of you
donated directly to the shelter.
Amazing.
On top of that, we still have about $3,500
that we will just be giving to them to use as they see fit.
Really successful.
Cannot thank you enough.
If you don't know, this is a personal cause for me.
So I truly appreciate it.
I like showing people what's done,
not just to show that it gets done,
but to show that a whole bunch of people coming together,
acting individually in pursuit of a collective goal,
can accomplish a whole lot in a very short period of time.
If you add up what we raised
and the money that was donated privately,
we're talking about raising right around 10 grand
in like an hour and a half.
And that far, far exceeded what I expected.
I cannot thank y'all enough
for making this channel a force for good,
because it really is y'all that accomplish that.
When it comes to the stuff like this
or any of the other stuff that we do,
I get the credit, but y'all do the work.
I'm a shipping clerk.
I'm a logistics clerk for all of it.
I have been asked to do an explainer
on how we raise funds
and how we mobilize for this kind of stuff.
This whole process, start to finish,
was filmed behind the scenes,
and it will be put together and be on the second channel.
So it'll explain why I don't name the shelter
until the actual live stream, until we're actually doing it.
It'll explain why I'm making this video right now,
because this is part of the fundraising mobilization process
even though the goal is already completed.
At the end of the day, y'all did really good.
Y'all made 12 kids really happy.
They are in a pretty dark space,
and you made it a little bit brighter through your efforts.
And it's not just donating money.
If you were in that chat and you were participating,
if you shared it, even if you just sat in there
and let it play, all of that increased engagement,
which made YouTube show it to more people,
which brought more people in, which brought more money in.
When it comes to stuff like this,
everybody contributes in their own way.
And the one thing I learned from this
is that we have to set our sights higher
when we do stuff like this.
Had no doubt we would be able to get the gifts
and overshoot.
Did not know it was going to be this much.
And it is.
It's heartwarming, even to me.
I can't thank y'all enough.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}