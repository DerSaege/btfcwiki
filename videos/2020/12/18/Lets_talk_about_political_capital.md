---
title: Let's talk about political capital....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SvSkBsiEjtY) |
| Published | 2020/12/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains political capital as goodwill in politics, with two types: reputational and representative.
- Compares political capital in politics to social media, particularly YouTube.
- Illustrates reputational political capital by discussing consistency in policies and ideological stances.
- Differentiates between reputational political capital (support from base) and representative political capital (related to getting things done).
- Uses examples from YouTube to explain the concept of political capital.
- Mentions how failing to deliver on promises or facing scandals can impact reputational political capital.
- Relates political capital to current political situations involving Pelosi, AOC, Trump, and McConnell.
- Analyzes the battle for control within the party based on reputational and representative political capital.
- Cites an example of a decline in reputational political capital for Crenshaw due to issues with the VA and social media.
- Emphasizes the importance of political capital in politicians' decision-making and potential impact on re-election.

### Quotes

- "Political capital is basically goodwill."
- "It's worth noting that this isn't the way it should be, but it's the way it is right now."
- "That's how this works."
- "It's just a thought."
- "Y'all have a good day."

### Oneliner

Beau explains political capital as goodwill in politics, comparing it to social media, and how it impacts decision-making and re-election potential.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Analyze and understand how political capital influences decision-making (implied).
- Stay informed about politicians' actions and their impact on political capital (implied).
- Engage in community organizing to support or hold politicians accountable based on their political capital (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of political capital, including its types, impact, and relevance in political decision-making. Viewing the entire transcript offers a comprehensive understanding of this abstract concept in politics.

### Tags

#PoliticalCapital #Politics #SocialMedia #DecisionMaking #Reputation #Representation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about an abstract concept
called political capital.
uh...
It's something that is very hard to quantify. In fact, anytime I've ever seen
anybody try, I didn't like the way they did it.
But it is very real
and it's at play all the time.
It's also kind of hard to explain
so we're going to take it out
of politics
and bring it into a world
that I know everybody watching this is going to be familiar with.
Social media, YouTube.
So political capital is basically goodwill.
It can be goodwill from your base,
so your voters or your subscribers,
uh...
or
goodwill from other politicians or other creators.
And then you have two kinds of political capital. You have reputational
and representative.
Reputational is
goodwill from your base.
Okay, so that is...
If you read about this, it'll say something along the lines of being
consistent in
policy or ideological stances, something along those lines.
The reason it says that
is because if you are consistent and you continue to get re-elected, your base
supports you.
Okay, so
reputational is about support from your base.
Representative
is political capital related to getting stuff done.
Two different kinds.
So, on YouTube
you have...
let's take this channel
as an example.
Five hundred thousand,
little less than,
subscribers.
To me, that's massive. It's huge.
Um...
In the grand scheme of things, on YouTube,
it's not.
We don't have a lot of reputational political capital.
But when it comes to representative, we have tons.
Because when we set out to do something real world, whether it be a fundraiser
or
getting supplies to where they need to be, or hurricane relief, or whatever,
when we set out to do it,
we do it. Therefore, people are more likely to support
our activities in the future.
So, if somebody was to say,
hey, we're gonna
raise money to hold a barbecue, and then the barbecue never gets held,
they failed. They couldn't get stuff done.
So, their representative political capital declines.
If somebody makes a bunch of bad takes, and has a whole bunch of people
unsubscribe,
a bunch of bad videos,
their reputational political capital
is declining.
This can also happen if there's
a scandal of some kind.
So, that's how it works.
All of this directly transfers to politics.
And it is entering into a whole bunch
of political discussions right now.
As an example, the Pelosi AOC, Medicare for All floor vote thing.
Political capital is definitely entering into the decisions
of people involved in that.
The Trump-McConnell thing,
that's entirely about political capital.
Trump has a bunch of reputational
political capital.
His base
still supports him.
He doesn't have much representative because he's outgoing. He can't get
anything done once he's out of office.
McConnell
has a whole lot
of representative political capital because he's been up there for so long.
And he's got stuff done.
And stops stuff from getting done, which I guess can also generate political capital.
And those two are battling for control
of the party.
And to show you that it is always in play,
Crenshaw
is taking a huge hit right now in reputational political capital
because of the thing with the VA.
Veterans
are upset.
That's a big part of his base.
He's also taking a hit because he
followed somebody on Twitter that I guess
people don't approve of him following.
And that's causing
a decline in reputational political capital.
So that shows that it can be something real,
the thing with the VA,
or something that realistically doesn't matter,
who he follows on Twitter,
that can impact
that kind of political capital.
Now when you're talking about it going into decline,
it also impacts his representative
political capital.
People are going to be less likely to do him favors
because if his reputational political capital drops too far,
well he may not get re-elected,
which means he may not be there to return the favor.
So that's how this works.
That's
a brief overview
of an abstract concept
that is incredibly hard to quantify
but is very real
and is at play with every politician you know.
It's worth noting
that this isn't the way it should be,
but it's the way it is right now.
And it is definitely something
that impacts people's decisions.
And it's just something that we might should keep in mind.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}