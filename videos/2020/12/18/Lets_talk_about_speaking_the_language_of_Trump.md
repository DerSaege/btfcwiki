---
title: Let's talk about speaking the language of Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gijxSuWRmBw) |
| Published | 2020/12/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of understanding the rhetoric that resonates with Trump supporters.
- Points out that fear is a key motivator for Trump's base, used in his rhetoric to captivate them.
- Emphasizes that addressing fears, not creating progressives, can help sway Trump supporters.
- Suggests that making Trump supporters anti-authoritarian is more feasible than making them progressive.
- Stresses the need to debunk false claims persistently to sway a significant portion of Trump's base.
- Outlines a strategy to exhaust Trump's reputation and political capital over time.
- Urges framing arguments around Trump supporters' fears to effectively communicate with them.

### Quotes

- "It's all about fear."
- "If you want to convince somebody that Trump really resonated with, if you want to convince them of anything, you have to frame it around their fears."
- "The only thing that creates progressives is education and context."
- "The President's own rhetoric, his own wild claims, can be used to show his base exactly who he is."
- "When you are talking to people who are under Trump's sway, you have to frame it around their fears."

### Oneliner

Understanding and addressing the fears of Trump supporters is key to swaying his base and exhausting his political capital.

### Audience

Activists, Educators, Progressives

### On-the-ground actions from transcript

- Debunk false claims persistently until a significant portion of Trump's base understands (implied)
- Work towards defeating politicians who supported baseless claims in the midterms (implied)

### Whats missing in summary

In-depth analysis on the nuances of addressing fear-based rhetoric and strategies for long-term political change.

### Tags

#TrumpSupporters #PoliticalStrategy #Debunking #AntiAuthoritarianism #FearBasedRhetoric


## Transcript
Well howdy there internet people. It's Beau again. So today we are going to talk about speaking
the language of Trump. We're going to talk about how to reach across and hopefully
get people to fall out from underneath his spell.
Today I want to point out that we are talking about the way things are, not the way they should be.
Those things are rarely the same and it's an important distinction to make today.
Okay, so how do you reach them? You have to figure out what kind of rhetoric is going to work on them,
right? What will sway them? To get a good read on that, what kind of rhetoric do they use on you?
When they're trying to convince you of something, what do they say?
Generally, they're trying to scare you, right? They're trying to scare you.
If you don't do this, this bad thing will happen.
When you think about their insults, it's all about making somebody seem weak.
Because again, if somebody's weak, well they need to be scared. It's all about fear.
It's funny because when I asked that in yesterday's video, you know, what motivates
people like racism? I mean, yeah, that's part of it. Because where's racism come from? Fear.
Fear. Same thing, fear of the unknown. Where's fear come from? Ignorance.
And solving the problem of ignorance, yeah, that's a long-term goal. But for our purposes
right now, fear will work. Why was Trump successful? Because he spoke to their fears.
It's that simple. He spoke about the things they cared about, what they were afraid of.
He spoke to their fears and gave them new ones. He used a lot of hyperbole. I mean,
look at Trump. He wasn't a great orator. He could barely string a sentence together.
But man, he had those crowds just captivated because he spoke to their fears.
And that's what motivates them. If you want to convince somebody that Trump really resonated with,
if you want to convince them of anything, you have to frame it around their fears,
what their fears are, and how this policy is going to alleviate that fear.
That's really it, because their motivations are pretty much all fear-based. And then they have,
well, it's nationalism masquerading as patriotism that they use for rhetoric.
But that's it. They're simple. They don't want change. They're afraid of change.
That's why they're conservative. Okay, so doing it this way and appealing to their fears,
that doesn't create progressives. That doesn't switch them. It doesn't create liberals.
The only thing that creates progressives is education and context. That's how people become progressive.
Okay, this isn't going to do that. Framing things around their fears will not do that.
But you don't have to create progressives to whittle away at Trump's base that still exists.
It's still out there. You don't have to create progressives. You don't have to convince them of
anything like that. You don't have to convince them of anything like that.
You just have to make them anti-authoritarian. It's all you have to do.
And that's relatively easy because their rhetoric is already anti-authoritarian.
And that anti-authoritarian rhetoric can sometimes override the fear.
That's why you had the whole mask thing.
So this all sounds great, sounds real simple. How do you execute it? Right?
How do you make it a thing? How do you make it happen?
There is a reason they are not letting go of the Kraken.
There's a reason they're going to continue to push those claims,
no matter how many times they're debunked. Because those in power are going to continue to push those claims.
Those in power understand. The second that they admit that that was all made up,
well, they're handing their political opposition all of the tools they need
to prove that they are in fact the authoritarians.
And then all of their rhetoric comes back to bite them.
They are the ones that were trying to subvert the Constitution.
They are the ones that were trying to seize control.
And I know a lot of people believe that everybody who followed Trump is innately authoritarian.
It's not true. Not really.
When you make a sweeping generalization like that, it's always wrong. Even that one.
They resonated with his speech because he spoke to their fears.
That's what it boils down to. There's not 70 million authoritarians in this country.
I mean, there kind of are, but not in the way we're talking about right now.
So the key element, the first step, is to, as much as it is going to be a pain,
because you know how long they hold on to stuff.
It's been however many years, and you know, but her emails.
The Kraken has to be debunked over and over and over again.
Until a significant portion of that 70 million gets it.
And then those politicians who signed on to that, those who backed it, who enabled it,
they have to lose in the midterms.
If that happens, it becomes politically untenable to appear to continue to be a Trump supporter.
Or to mimic Trump's behavior two years after that.
Which is when the presidential election is.
And that's the goal. Yesterday people pointed out, you know, no, we have two years to do it.
Kind of. We have two years to put a really big start on it.
But the real battle is in four years.
And we have to create a situation where authoritarian rhetoric is something that people steer clear of.
Because they don't want to be associated with Trump.
Because that becomes associated with a bunch of losses.
We want to exhaust his reputation, political capital.
We want to make sure that goes away.
And then from there, policies related to him, the representative political capital, will disappear.
So, as much as it is going to seem counterintuitive to you,
because if you're watching this channel, you are probably not fear-based.
You're probably somebody who is hope-based.
Looking to the future, wanting things to do better.
Wanting things to grow.
When you are talking to people who are under Trump's sway, you have to frame it around their fears.
And over the next few weeks, I will do a whole bunch of videos showcasing how to do this.
But just to give you a general overview of how you've got to reach them, that's it.
And I know, you're like, that's not right, we need to educate them.
And we do, yeah, absolutely. That needs to happen.
But that's probably not going to happen in two years.
In the meantime, they've handed us the tools to reach them.
The President's own rhetoric, his own wild claims, can be used to show his base exactly who he is.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}