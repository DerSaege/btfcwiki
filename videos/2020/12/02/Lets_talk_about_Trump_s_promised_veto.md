---
title: Let's talk about Trump's promised veto....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mEEJ6LdKbnQ) |
| Published | 2020/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump might veto the defense spending bill because he's upset with Twitter fact-checking him.
- Trump is demanding to insert something into the bill that repels Section 230 of the Communications Decency Act.
- Section 230 protects companies like Twitter from lawsuits related to user-generated content.
- There's uncertainty if Trump also plans to veto the bill over renaming bases named after Confederate generals.
- Politicians are unsure how to respond to Trump's declaration.
- Beau suggests calling Trump's bluff or letting him veto the bill, which includes a pay raise for troops before Christmas.
- The bill in question, the NDAA, has a long history of bipartisan support and is likely veto-proof.
- Overriding Trump's veto on the NDAA could be the perfect end to his administration.
- This is an opportune moment for the Republican Party to distance themselves from Trump.
- Democrats should not miss the chance to send the bill for Trump to potentially veto.
- Beau doubts Trump will actually veto the bill, as it may be a lasting source of mockery for him.
- The NDAA bill typically passes without issues, and there's no need to give in to Trump's demands.
- Beau encourages letting Trump upset the people of Georgia and believes the bill will eventually go through.

### Quotes

- "Call his bluff."
- "Let him do it."
- "It's the NDAA. It always gets through."
- "Stop playing his games."
- "He's on his way out."

### Oneliner

President Trump might veto the defense spending bill over Twitter backlash, but it's likely his bluff will be called as the NDAA is expected to pass, providing an opportune moment for both parties to take a stand.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Call out Trump's bluff and urge politicians to stand firm against his demands (implied)
- Support bipartisan efforts to ensure the NDAA bill passes without giving in to Trump's veto threats (implied)

### Whats missing in summary

Full context and nuances of Trump's potential veto over Twitter dispute and renaming bases, along with the political strategies surrounding the situation.

### Tags

#Trump #NDAA #DefenseSpendingBill #Section230 #Bipartisan


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about President Trump's declaration, his statement, that he
is going to veto the defense spending bill because he's mad at Twitter.
It's really what it boils down to.
He is demanding that something be inserted into the defense spending bill that repels
Section 230 of the Communications Decency Act.
Section 230 basically protects companies like Twitter from lawsuit because they use user
generated content.
He's mad because Twitter fact checked him.
It's really what it boils down to.
And because of this he is throwing a temper tantrum and is willing to veto a defense spending
bill.
I'm not sure if he still plans on vetoing it over renaming bases that are named after
Confederate generals.
I don't know if that's on the table or not.
He made this little declaration last night and politicians don't know what to do.
There's a lot of talk about it.
Oh no, call his bluff.
That's what you do.
Call his bluff.
Or let him go out with a bang.
Let Mr. I support the military veto a defense spending bill that I'm pretty sure includes
a pay raise for troops right before Christmas.
Yeah let him do it.
I'm sure that that's not going to influence the way anybody thinks in Georgia, particularly
around Columbus where Fort Benning is.
Let him do it.
Make him do it.
Even if he does do it, what happens next?
It's the NDAA.
This bill has been passed every year for like the last 58, 59 years with massive bipartisan
support.
It's veto proof.
His veto will get overridden.
That would be the perfect end to the Trump administration.
Call his bluff.
Stop playing his games.
This is the Republican Party's chance to distance themselves from President Trump.
This is their opportunity.
And as far as Democrats go, it would be a massive, massive mistake to not send this
up and have him veto it.
Let him do it.
Call his bluff.
I don't think that he will.
I don't think he'll veto it.
I don't think that he's going to want one of his last acts as president to be something
that is mocked forever.
The bill's going to get through.
It's the NDAA.
It always gets through.
There's no reason to cave to President-reject Trump.
He's on his way out.
He's done.
Let him upset the people of Georgia.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}