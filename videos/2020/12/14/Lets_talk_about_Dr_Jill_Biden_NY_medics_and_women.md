---
title: Let's talk about Dr. Jill Biden, NY medics, and women....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8SGDIVW06wk) |
| Published | 2020/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dr. Jill Biden was criticized for using the title "doctor" because she has a doctorate in education, not medicine.
- The New York Post reported on a medic who had a side gig on a certain website for her fans.
- The focus should have been on the financial struggles of frontline healthcare workers, not moral outrage over a medic's side job.
- There is a disparity between frontline healthcare workers and the multi-billion dollar industry profiting from them.
- The message conveyed was that women shouldn't be proud of their educational accomplishments, brains, or bodies.
- Society's approval seems necessary for women to be proud of themselves.
- Equality is still a work in progress in the country.
- Women should be free to be who and what they want to be without seeking society's approval.

### Quotes

- "You can do whatever you want in this world. As long as we approve."
- "In today's world, a woman has to be two things. Who and what she wants."

### Oneliner

Dr. Jill Biden faces criticism for being called "doctor," while a medic's side gig becomes a focus of moral outrage, showcasing the struggles of frontline healthcare workers amid societal judgment of women's accomplishments and bodies.

### Audience

Women, Healthcare Workers

### On-the-ground actions from transcript

- Support frontline healthcare workers financially and advocate for fair compensation (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the societal expectations placed on women and the ongoing struggle for gender equality.

### Tags

#GenderEquality #HealthcareWorkers #SocietalExpectations #Women'sRights #Equality


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about doctors and medics and photos, oh my.
We're going to talk about judgments, titles, and what women are allowed to do.
Because we found out some new limitations this weekend.
Found out some new things that women need to refrain from doing because it might upset
somebody, because those who are just truly intimidated by women were out in full force
this weekend.
It started with former second lady, soon to be first lady, Dr. Jill Biden being told that
she shouldn't call herself doctor because she's not a medical doctor, she's only a doctor
of education, which I personally find hilarious because I know where the word doctor comes
from and what a doctor qualifies you to do.
And I would suggest that a teacher calling herself doctor is just wildly appropriate.
But the takeaway was, well kiddo, you don't really need to be so proud of that, just sit
there and look pretty now.
On the other end of the spectrum, you had a just hard hitting piece of reporting coming
from the New York Post detailing how a medic in New York had a side gig.
And that side gig was a webpage that was only for her fans.
If you're familiar with the website, you can imagine the kind of images there.
This of course was used to attempt to provoke some kind of moral outrage.
Before we go too far, I would point out that I know a lot of people who have pages on that
particular site and I know people in that industry more professionally.
They are some of the nicest, most moral people I know.
You on the other hand, write for the New York Post.
You're telling me you do that because of the high ethical standards?
The hard hitting journalism you can find in the metro section?
I seriously doubt it.
This was an opportunity to showcase the disparity between frontline healthcare workers, paramedics,
truly frontline healthcare workers, and the multi-billion dollar industry that profits
from them.
Showing how, well, they can't all make ends meet.
That's what this could have been used to show.
But it wasn't.
Of course it was used to kick down because there's a certain segment of the American
population that just truly enjoys that and that gets a lot of good clicks.
At the end of the weekend, the message, the take away from this was that if you're a woman,
don't be proud of your educational accomplishments.
Don't be proud of your brains.
Don't be proud of your body.
Don't be proud if you have them both.
You can do whatever you want in this world.
As long as we approve.
There are a whole lot of people who do not understand that equality is slowly becoming
a thing in this country.
They need to catch up.
In today's world, a woman has to be two things.
Who and what she wants.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}