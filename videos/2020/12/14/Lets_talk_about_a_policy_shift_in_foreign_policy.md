---
title: Let's talk about a policy shift in foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3mILljoPj5Y) |
| Published | 2020/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Boba Gowd says:

- Department of Defense to limit personnel detailing and temporary reassignment to intelligence world.
- Since 2001, personnel were commonly detailed for intelligence work due to lack of opposition embassies and the need for deniability.
- Shift in U.S. foreign policy prioritizes near peers like China and Russia over previous groups.
- Deniability is now key in intelligence work, necessitating more expendable personnel for potential situations.
- Expected change in policy was long overdue and not a surprise.
- Likely some internal dispute or reason for the change at this specific moment.
- Not a cause for worry, rather a necessary adjustment reflecting a shift in priorities.
- Unlikely that the framing of the news will directly address the need for deniability in intelligence operations.
- Speculation on how the news will be presented to the public and potential reactions.
- A heads up about the upcoming news and its possible impact on public perception.

### Quotes

- "The Department of Defense is going to begin limiting the detailing, the temporary reassignment of their personnel to the intelligence world."
- "The new priority is going to be near peers. Near peers being countries like China or Russia."
- "There has to be a level of deniability."
- "This realistically, this should have happened like five years ago."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Boba Gowd warns of shifts in intelligence personnel assignments, prioritizing deniability in a changing foreign policy landscape, leading to potential changes in public perception.

### Audience

Policy analysts

### On-the-ground actions from transcript

- Stay informed about changes in national security policies (implied).

### Whats missing in summary

Context on the implications of deniability in intelligence operations.


## Transcript
Well howdy there internet people, it's Boba Gowd.
So today we're going to talk about something that's kind of occurring in the background.
It really hasn't made huge headlines yet, but I think eventually it might.
And depending on how it's framed, when it does,
it may raise eyebrows when it really doesn't need to.
Everybody knew that this little piece of news was coming.
In fact, it's long overdue.
But given the current political situation, any kind of shuffling in this community makes
people nervous right now.
So what's the news?
The Department of Defense is going to begin limiting the detailing, the temporary reassignment
of their personnel to the intelligence world.
Ever since 2001, it was really common.
And the reason it was really common was because they could get away with it.
The opposition at the time, well they didn't have embassies.
And nobody needed to maintain deniability.
We were advertising that we did this stuff, you know.
The foreign policy of the United States is shifting.
Those groups are no longer a priority.
The new priority is going to be near peers.
Near peers being countries like China or Russia.
Powerful countries.
So when that little world of people does something, they have to be able to say, oh that wasn't
us, if the people get caught.
You can't do that if it's Staff Sergeant whoever that gets caught.
There has to be a level of deniability.
And I think this is playing into this.
This is something everybody knew was coming.
But there probably is some kind of dispute, some kind of internal riff that caused it
to happen at this exact moment.
But it's not something to worry about.
It's not a weird Trump power grab or anything like that.
This realistically, this should have happened like five years ago.
And there are probably a bunch of other reasons that this is occurring.
I am curious to see how they frame it though.
Because I think it's highly unlikely that they're going to walk out and say, oh, well,
you know, we're going to be spying on other countries more, so we need to be able to say
it wasn't us, therefore we're going to use people who are more expendable by the way
we're hiring.
That seems unlikely.
But anyway, this is more of just a heads up because we don't know how this news is going
to be framed when it comes out and when it starts to get publicized.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}