---
title: Let's talk about how the stock market isn't the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=80DeGZ_M-yg) |
| Published | 2020/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an overview of the economy, stock market, and jobs report for November.
- Initially, the job report of almost a quarter million new jobs created seems positive.
- However, job growth is actually slowing down and trending downwards.
- Currently, there are still 10 million jobs less than before the economic downfall.
- It is projected to take about four years to recover the lost jobs at the current rate.
- The early job gains were mainly due to people returning to work after temporary layoffs.
- Long-term unemployment is rising, making it harder for people to re-enter the job market.
- Benefits for the unemployed are running out, indicated by a significant increase in long-term unemployed individuals.
- The total number of long-term unemployed individuals has reached 3.9 million.
- The jobless rate for the black community is significantly higher at 10.3% compared to 5.9% for white individuals.
- Despite the grim job market outlook, the stock market reacted positively to the news, expecting more stimulus.
- There is a significant disconnect between the stock market's performance and the real economy.
- The stock market is not an accurate reflection of how many people are struggling to make ends meet.
- Beau suggests that there needs to be a change in the way the economy operates to address these challenges effectively.

### Quotes

- "All of this is bad news for us."
- "The stock market is not the economy."
- "How the stock market is performing does not typically affect the way a whole lot of people are getting food on their table."

### Oneliner

Beau breaks down the disconnect between the stock market and the struggling economy, urging for sustainable economic changes.

### Audience

Economic policymakers, activists

### On-the-ground actions from transcript

- Advocate for sustainable economic policies to address job market challenges (suggested)
- Support initiatives that focus on helping long-term unemployed individuals re-enter the job market (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the disconnect between the stock market and the real economy, urging for fundamental changes to ensure economic sustainability.

### Tags

#Economy #StockMarket #JobsReport #Unemployment #PolicyChange


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're going to talk about the economy
and the stock market and the jobs report
that just got released for the month of November.
On the headline, that looks pretty good.
Almost a quarter million new jobs created,
that sounds awesome.
When you get past the headline, not so much.
What it shows is that job growth is slowing.
It is trending down.
We are creating less jobs per month than we should be.
We are currently still 10 million jobs down
from when the bottom fell out.
At this rate, with current trends,
it will take about four years to recoup the jobs that were lost.
All of those early gains that somebody took credit for,
That was just people going back to work
after being temporarily laid off.
So that's not great.
We also see rising long-term unemployment.
That is people who have been unemployed
for longer than six months.
The longer somebody is out of work,
the harder it is to get back into the job market.
Aside from that, benefits run out.
That's a bad sign.
That has gone up by 385,000 last month to a total of 3.9 million,
making long-term unemployed 37% of all of those out of the job right now.
That's pretty high, just historically speaking.
Aside from that, we also have a shrinking labor force.
People are choosing to exit the job market completely,
or at least for an extended period of time,
for a whole bunch of reasons.
Of course, all of this is hitting the black community
the hardest.
The current jobless rate is 10.3%.
For white folk, it is 5.9%.
OK, so here's the real lesson, though.
All of this is bad news for us.
But when the stock market heard the news, well, it went up.
The Dow, S&P 500, and NASDAQ all were like, hey, this is good,
because they believe that it kind of forecasted more
stimulus.
So what this does is it shows a disconnect.
The stock market is not the economy.
It never has been.
That's how a lot of people have been looking at it
over the last four years or so,
because that was the metric that was used.
But for the average person,
that's not a good metric.
How the stock market is performing
does not typically affect the way a whole lot of people
are getting food on their table.
All of this is going on, and, well, maybe
we'll get another stimulus check for those who need it.
Something probably has to change in the way our economy is run.
This is probably not a sustainable method
of doing things.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}