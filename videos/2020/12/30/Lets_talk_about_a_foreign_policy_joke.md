---
title: Let's talk about a foreign policy joke....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cmTSRl5N6Iw) |
| Published | 2020/12/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes foreign policy initiative on gender studies.
- Supports spending American tax dollars on gender studies in Pakistan, Sudan, Saudi Arabia.
- Believes US government wouldn't introduce gender equality ideas in Saudi Arabia to maintain status quo.
- Explains how patriarchal societies in certain countries maintain power and support.
- States introducing gender equality ideas could limit opposition forces on the battlefield.
- Compares gender studies in cultural wars vs. real wars, where education is weaponized.
- Urges to smash the patriarchy for the safety and effectiveness of these educational programs.
- Mentions these programs have been running effectively for 70 years, similar to building irrigation ditches for farmers.
- Emphasizes that undermining the opposition is the main goal, with women's empowerment being a bonus.
- Calls for supporting troops beyond symbolic gestures like yellow ribbon bumper stickers.

### Quotes

- "If you want to remain a red-blooded American who supports troops, it's time to smash the patriarchy."
- "These educational programs have been run by the US government for 70 years. They are incredibly effective."
- "You can have a cute joke for the internet and be all macho, or you can support the troops a little bit more."

### Oneliner

Beau criticizes foreign policy, supports gender studies funding in patriarchal societies, and urges smashing the patriarchy to undermine opposition forces and support troops effectively.

### Audience

Policymakers, activists, citizens

### On-the-ground actions from transcript

- Support educational programs in patriarchal societies (implied)
- Smash the patriarchy through advocacy and action (implied)
- Support initiatives that empower women globally (implied)

### Whats missing in summary

The nuances of how gender equality programs can strategically undermine opposition forces and the importance of supporting troops beyond symbolic gestures.

### Tags

#ForeignPolicy #GenderEquality #SupportTroops #Patriarchy #USGovernment


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a foreign policy initiative that has raised eyebrows.
It has drawn a lot of criticism from certain political circles and it has become a joke
now.
So we're going to talk about that.
I got a message, not a very polite message, but I got a message and when the person was
done questioning my plant-based diet and my orientation, a line in it was that I must
be one of those liberals who support spending American tax dollars on gender studies in
the Pakistan.
The Pakistan.
Yes, I do actually support spending American tax dollars on gender studies in the Pakistan.
Also think we should do it in Sudan and in Saudi Arabia.
However the US government would never do it in Saudi Arabia.
Wouldn't introduce those ideas there.
Wouldn't introduce the idea of gender equality because for whatever reason the US government
believes the Saudi government is their friend and they wouldn't want to undermine them.
If those ideas were to take root in Saudi Arabia it would disrupt the status quo, disrupt
the government's ability to do things the way it always has.
Pakistan is not an opposition nation.
However they share a very porous border with another country.
One that we've been in for 20 years.
And if you were to watch any movie on Netflix when an American soldier walks into a village
and needs to speak to the head man, who shows up?
Some old dude with a beard, right?
Why?
Because it is an incredibly patriarchal society.
That is how they maintain their power.
That is how they maintain their support.
That is how they function.
That is how they recruit.
Introducing the ideas of gender equality to the area and having them spread would limit
their ability to function.
It would keep opposition forces off of the battlefield.
It is a whole lot easier and less expensive to keep them off the battlefield than it is
to remove them once they are there.
I get it.
Here in the US gender studies has become a buzzword in the cultural wars.
In real wars though, that education is weaponized.
It helps undermine the opposition.
So I hate to break it to you guys, but if you want to remain 6'2", red, white, and blue,
red-blooded American who supports troops and all that, it's time to smash the patriarchy.
Because that power structure is dangerous.
These educational programs like this have been run by the US government for, I don't
know, 70 years.
They are incredibly effective.
This is no different than building irrigation ditches for farmers.
So they don't have to rely on the power structure of the opposition.
It's the same thing, only this is going on the offensive and undermining the very ideology
that fosters the opposition.
The fact that it will actually better the lives of millions of women there, that's
kind of just a happy bonus as far as the government is concerned.
That's not the reason they're doing it.
They're doing it because it undermines the opposition.
So you have a choice, gentlemen.
You can have a cute joke for the internet and be all macho and everything, or you can
support the troops a little bit more than that yellow ribbon bumper sticker on your
truck.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}