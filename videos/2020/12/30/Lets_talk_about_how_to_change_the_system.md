---
title: Let's talk about how to change the system....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aDXsa9h2cN8) |
| Published | 2020/12/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Social media post prompts reflection on the broken system when someone asks for help with groceries.
- People offering aid to strangers in need on social media is a common occurrence.
- The generosity displayed in these acts of kindness is not uplifting but rather a stark reminder of systemic failures.
- Acts of kindness expose the brokenness of society and the government's lack of basic care for its citizens.
- Individuals helping each other challenges the system's teachings of self-preservation over community support.
- Over time, consistent acts of aid can lead to the institutionalization of community support systems.
- Relying on community aid erodes the authority and power of the current government system.
- Encourages building a new system based on voluntary cooperation and support rather than reliance on existing institutions.
- Helping others in a grassroots, community-driven manner is a revolutionary act that can lead to a shift in societal values.
- A system built on cooperation and mutual support will be more effective and uplifting for all.

### Quotes

- "Every one of these stories is an indictment of the system."
- "Doing what you can, when you can, where you can, for as long as you can, is a revolutionary act."
- "People actively undermining the system that is holding so many down."

### Oneliner

Acts of kindness in challenging the system by fostering community support over reliance on failing institutions.

### Audience

Community members, activists, allies

### On-the-ground actions from transcript

- Support community aid initiatives by participating actively and consistently (exemplified)
- Build networks for mutual aid and support within your neighborhood (suggested)

### Whats missing in summary

The full transcript provides a deep dive into the impact of community support in challenging systemic failures and promoting a more caring society.

### Tags

#CommunitySupport #SystemicChange #GrassrootsActivism #MutualAid #SocialJustice


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the system.
The system as a whole and how to change the system over time.
And something that happened on my social media
because it prompted a question that leads us to this topic.
It started on Instagram where somebody asked, basically they were asking when the stimulus
checks were coming because they need groceries like now.
They're out.
The replies to that question was half a dozen people saying, you know, what's your PayPal?
I got you.
And just to be honest, I'm never prepared for when I see that in the comments and I
see it way more than you might expect.
That kind of aid occurring between y'all.
It happens a lot.
This one, for whatever reason, I took a screenshot of, blocked the names out so nobody could
see who was involved, and put it on Twitter.
Which prompted another question.
Am I cynical and burnt out?
This kind of thing doesn't get me at all.
The fact that folk are in that position when there's no need to be if we had a functioning,
caring, and socially intelligent government.
Literally, no need for anyone to be without basic needs, food, shelter, warmth.
This religious kind of Christmas miracle outpouring of generosity doesn't restore my faith in
humanity.
It confirms that it is so celebrated and needed that it shows just how much humanity and society
as a whole is broken, and maybe always has been.
Am I just blah?
Nah.
You're not just blah.
You're absolutely correct.
You are absolutely correct.
Inside every one of those heartwarming stories where a kid is selling lemonade so their neighbor
down the road can get a prosthetic, or there's people getting food for somebody, or people
acting to keep someone in their home, whatever it is.
Inside of that story is an indictment of the system.
Absolutely.
You are absolutely correct.
Then the question becomes, how do we change the system?
A system that is notoriously reluctant to change.
Get a piece of parchment paper and sign our names to it?
Doesn't work like that anymore.
Takes time, and it takes altering the system.
The easiest way to do that is to ignore the system as much as humanly possible, to not
play by the rules the system has taught you.
What are the rules the system has taught about this situation?
Well they probably deserved it.
Nobody should help because everybody should be out for themselves, right?
Every one of these stories is an indictment of the system, but it also shows that people
aren't playing by those rules anymore.
People are going to help each other.
They're going to engage in this kind of aid and assist one another when they're down.
If that goes on long enough, it becomes institutionalized.
It becomes the way people attempt to get help.
What does that mean for the government institutions?
The government only has power.
The system that we're in only has authority because people believe that it does.
If people stop turning to this system for help and they build their own system that
exists outside of the powers that be, they don't put much faith in the current system.
It erodes that faith.
It erodes its authority.
It erodes its hold on people.
Goes on long enough, and this could take a hundred years, goes on long enough and people
don't even look for help there.
That's not where they turn.
They don't turn to this system.
They turn to their neighbors.
They turn to a new system, one that was voluntarily created without coercion, without force.
At that point, the system that is so reluctant to change has two choices.
It can either catch up and reflect the will of the people, promote the general welfare,
or it can fall into disuse and become obsolete.
Cease to be.
As odd as it may seem, in the current system, helping each other like this, doing what you
can, when you can, where you can, for as long as you can, is a revolutionary act because
over time, people will begin to rely on each other more than their betters, those who are
in control of this system.
I would suggest that any system that is born out of people wanting to help each other,
wanting to cooperate, wanting everybody to have what they need, and it being done voluntarily,
without coercion, without force, any system that comes from those tenants is going to
be pretty cool.
It's going to be a system we want.
It's going to be a system we can be proud of, one that doesn't kick down, one that lifts
everybody up.
So yeah, nobody should be in that position, but in the system we have, there are people
like that.
There are people in that situation.
But when you see those heartwarming stories that are definitely an indictment of the system,
look at it more as people actively undermining the system that is holding so many down.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}