---
title: Let's talk about a gift to Democrats and messaging....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BG6Vkbl13VU) |
| Published | 2020/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People may act against their interests if the message doesn't resonate with them.
- The Democratic Party has an opening to become the party of national security.
- Recent events have shown the need for increased medical infrastructure and readiness.
- Foreign policy shifts suggest future opposition from powerful countries.
- To strengthen national security, the medical infrastructure must be made more resilient.
- Increasing usage of medical facilities is key to enhancing readiness.
- Access to healthcare for everyone can significantly boost national security.
- Renaming existing bills like M4A (Medicare for All) can help in this regard.
- Improving medical infrastructure is vital for facing challenges from near-peer nations.
- Reframing healthcare as a national security issue can shift perspectives effectively.

### Quotes

- "Our medical infrastructure being in the state that it is in is a national security issue."
- "The easiest way to secure this nation from this kind of issue is to up our medical infrastructure."
- "The easiest way to do that is to increase usage."
- "It's just messaging."
- "The exact same bill, retitled."

### Oneliner

People may act against their interests if the message doesn't resonate; Democrats can seize the national security narrative through improved medical infrastructure like Medicare for All.

### Audience

Policy advocates, Democrats

### On-the-ground actions from transcript

- Advocate for policies that improve medical infrastructure (suggested)
- Support bills like Medicare for All (suggested)

### Whats missing in summary

The full transcript expands on the critical link between national security and healthcare messaging.

### Tags

#Messaging #NationalSecurity #Healthcare #MedicareForAll #PolicyChange


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about messaging.
Because sometimes people will do things against their own interests simply because it wasn't
presented to them in the right way.
It didn't reach them on a level that resonated.
So we're going to talk about messaging.
We're also going to give a gift to the Democratic Party because something happened over the
last few months and even though it is normally something Republicans would be all over, they
failed.
They failed.
The Democrats have the opportunity to become the party of national security.
Even though that's something you would typically expect from Republicans.
Everybody saw this country grind to a screeching halt from the recent public health issue.
And by everybody, I mean everybody all over the world.
Due to foreign policy shifts, the expected opposition in the future is going to be near
peer nations, near peer, very powerful countries.
These are countries that might be able to use that newly exposed weakness.
Because they watched as this country stopped working.
They watched our readiness become degraded.
So what do we need?
We have to up our medical infrastructure, right?
We have to make it more resilient.
We have to make it to where it can handle a shock.
So we need more.
More of everything.
More docs, nurses, techs, supplies, beds, everything.
How do we get it?
We can't just hire them and not have them do anything.
It's not going to work.
It won't be effective.
Even if we did that, they wouldn't be ready because they wouldn't have the workload.
So what we have to do is we have to increase usage of our medical infrastructure.
We have to get more people to go to the hospital, to go to the doc.
That way, well, the increased volume will require more infrastructure.
It will require more beds.
It will require more supplies on hand.
That way if something happens, they're there.
Because just like now, if something happens, we'll all stay home.
Everybody will stay home except for those who need it.
This is how we can increase the national security of this country.
So we have to increase usage.
Easier said than done.
Well, I mean, I guess the easiest way to do it would just be to give everybody access
to health care.
Call it the Military Medical Preparedness Act.
Because with everybody having access, it's more volume.
All that other stuff we just talked about happens by default.
It's real simple.
I know people are going to want to know like the particulars.
There's already a bill out there.
It goes by the nickname M4A.
That would work.
We just retitle it and that would work.
Medicare for All.
I know that some of y'all were laughing because y'all knew where this was going.
And yeah, I mean, it is funny in a way.
But it's also true.
Our medical infrastructure being in the state that it is in is a national security issue.
If we are actually going to start facing near-peer nations, it is something we have to consider.
The easiest way to secure this nation from this kind of issue is to up our medical infrastructure.
The easiest way to do that is to increase usage.
The easiest way to do that is Medicare for All.
Just retitle it.
When you do that, you're going to have a lot less Republicans speaking out against a national
security issue.
You're going to have a lot of people who are opposed to handouts suddenly in favor of it
because it helps us militarily.
It helps us on a national security level.
It's just messaging.
The exact same bill, retitled.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}