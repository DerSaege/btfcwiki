---
title: Let's talk about Trump's base, messaging, and incrementalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Il3DgKLPVUM) |
| Published | 2020/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Clarifies the misinterpretation of Martin Luther King Jr.'s quote about immediate results in pursuing social change.
- Emphasizes the need for tireless, persistent efforts in the long-term pursuit of social progress.
- Points out the misconception of using MLK's quote to justify instant gratification in activism.
- Urges the importance of continuous work towards social change, noting that progress is incremental.
- Stresses the necessity of reaching out to the 70 million Americans who supported Trump and engaging in meaningful dialogues.
- Advocates for speaking the same "language" as those with differing ideologies to bridge the gap.
- Warns against complacency after Biden's win, citing the lack of a progressive mandate and the need for individual action.
- Acknowledges the challenge of convincing a significant portion of Trump's supporters to reconsider their beliefs.
- Calls for framing issues in a way that resonates with different audiences, including both big picture and kitchen table issues.
- Criticizes the Democratic Party's messaging and failure to connect with low-income rural voters who share similar values.

### Quotes

- "The time is always right to do what is right."
- "Social change doesn't just will in on its own. You have to make it happen."
- "We have to reach out to them and we have to reach them successfully."
- "Trump lost, but he didn't lose big."
- "There's a lot of work to be done."

### Oneliner

Beau clarifies MLK's quote on immediate action, stresses incremental progress, and urges reaching out to Trump supporters to prevent future authoritarian leadership, advocating for effective communication across ideological divides.

### Audience

Activists, Communicators, Progressives

### On-the-ground actions from transcript

- Reach out to Trump supporters in a way that resonates with them, adopting effective rhetoric to bridge ideological gaps (implied).
- Frame issues as both big picture issues and kitchen table issues to appeal to diverse audiences (implied).
- Work towards convincing a significant portion of Trump's 70 million supporters to reconsider their beliefs through meaningful engagement (implied).

### Whats missing in summary

The full transcript provides detailed insights on the necessity of persistent efforts in pursuing social change, the challenges of bridging ideological divides, and the imperative to reach out to Trump supporters to prevent future authoritarian leadership.

### Tags

#SocialChange #Activism #Communication #PoliticalEngagement #ProgressiveMovements


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about messaging and reaching people and incrementalism and
a quote.
We're going to start off talking about that quote because I've seen it used a lot lately
and I just want to clarify some things.
The quote is a Martin Luther King Jr. quote, the time is always right to do what is right.
And yes, he said that, absolutely.
And he definitely meant that you should take action today in pursuit of social change.
However, when that quote is being used today, it seems that people think not only should
you take action today, but that you should see results today.
And that is not what that quote meant.
If you go to the sentence before, he was talking about the tireless efforts and persistent
work of dedicated individuals.
Tireless and persistent does not seem to mean immediate.
The whole little spiel here that he was going through was explaining that social change
doesn't just will in on its own.
You have to make it happen.
You can't just wish for it.
You have to work for it.
You have to be persistent, tireless in the pursuit of social change.
I would suggest that it is incredibly odd to use a Martin Luther King quote to justify
some form of instant gratification because he was very well aware of the fact that people
in his struggle came before him and would come after him.
As big as his wins were, as much as they mattered, I would suggest they were incremental.
Turn on your TV.
I'm willing to bet it wouldn't take long before you were reminded of an issue related to his
struggle that is currently occurring in the United States.
It wasn't an immediate win.
It's a movement.
It continues to go.
Social progress does not stop.
So with that in mind, Trump lost, but he didn't lose big.
Seventy million Americans still voted for him, still bought into that authoritarian
rhetoric and who can blame them?
Nobody else is talking to them.
Really, when you think about it, nobody else is talking to them.
His voters, nobody's reaching out for them, at least not in a language they speak.
And we know how much that crew likes foreign languages.
When you hear progressives talk to authoritarians or conservatives even, they're often not speaking
the same language.
And that is something we have to change.
And I'm not just saying this in some philosophical thing.
I'm saying pragmatically, we have four years to reach a significant portion of that 70
million and convince them that they were wrong.
And that's a task.
We have to be tireless in this pursuit and we have to be persistent because the Biden
win was not decisive enough to send a message to the Republican Party that they should abandon
that style of authoritarian leadership.
If we don't reach them, we stand the chance of getting another Trump in four years.
And this one might be competent.
This one might not telegraph everything he's going to do over Twitter, which means we will
go through those 14 characteristics very quickly.
This isn't to tone police and say that you have to coddle conservatives.
If you want to be one of those charging ahead, more power to you.
If you want to be one of those leading the way, the pathfinders out front, that's good.
We need that.
But we also have to have people who are reaching back to drag a large portion of that 70 million
into the modern era.
And they are going to kick and scream the entire time.
We have to reach out to them and we have to reach them successfully, which means we have
to frame every issue as a big picture issue and a kitchen table issue.
When you talk about progressives innately, we're big picture people.
We look at the big picture.
Conservatives, those who fall for authoritarian rhetoric, they don't.
They care about the here and now.
They care about kitchen table issues.
Everything has to be framed both ways.
And we have to make an effort to speak their language and adopt rhetoric, not policy, that
is effective with them, that reaches them, that resonates with them.
We don't have a choice on this.
The reason that Trump's rhetoric was so successful is because he appealed to people who generally
aren't reached out to.
And he did it with comfortable rhetoric, with rhetoric that they had heard, with that patriotic
rhetoric that has just permeated this country since the end of World War II.
We have to find a way to convince, I don't know, 20 million people that policies from
almost 100 years ago may not really be suited for the modern era.
It's a task.
But we have four years to do it.
Biden does not have a mandate.
He didn't win big enough.
If you are hoping that Biden is going to take office and everything is going to be okay,
you're wrong.
There's a lot of work to be done.
There is no progressive mandate.
You cannot expect a bunch of progressive policies to be pushed through.
The most you can expect from Biden is damage control.
That's it.
If you want those progressive policies, you have to work for them.
And that's going to take individual action.
And it's going to take convincing a large portion of that 70 million.
And this is a group of people who are already going to be on the defensive.
That video from this morning, kind of a joke, but not really.
We have to take every issue and put it into a language they will understand.
And it's just going to move them a little bit.
They're always going to be behind because we always are going to have people who are
moving forward.
But we have to start making an effort to pull them along because they are stuck in the 1950s.
That mentality is still there.
You still have a large portion of those people who believe that Lincoln was a conservative.
These little things like this have to be addressed.
We have to convince them that history exists.
We have to convince them that it actually isn't still the Cold War.
We have to move forward.
And the Democratic Party is horrible when it comes to messaging.
And they generally do not make an effort to reach people who agree with them on most issues.
Generally speaking, low-income rural people hold the same values as the Democratic Party,
with the exception of just a couple of hot-button issues.
But because of those hot-button issues, Democrats have wrote them off for a long time.
The problem is it's no longer simple partisanship anymore because authoritarians have reached
out for them.
And they got them.
70 million of them.
Since June, 8 million Americans fell into poverty, but Trump still retained 70 million
supporters.
That shows you how significant his hold is on people and how hard this is going to be.
But we don't have a choice.
It's not something that we can forego doing.
It may be annoying.
I'm certain it's going to be annoying.
But we have to make a concerted effort to reach out to those who still aren't in the
modern era.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}