---
title: Let's talk about the future of water....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ROPvC2nlUUA) |
| Published | 2020/12/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Water dripping onto a tin roof during the video sets a dystopian tone, despite not being a satire.
- Water futures, a new concept, allow locking in prices for commodities like large consumers and sellers.
- Speculation is enabled through futures, with price fluctuations driven by supply and demand.
- The NASDAQ-Valez California Water Index, symbol NQH20, indicates significant price increases since its introduction in 2018.
- Two billion people currently face water scarcity, with projections of two-thirds facing shortages by 2025.
- Despite water covering the world, fresh water scarcity is a critical issue due to accessibility.
- The futures market for water is a tangible tool to showcase the reality of water scarcity to skeptics of environmental issues.
- The emergence of water futures signifies industry leaders recognizing the pressing need to address water scarcity.
- Utilizing the futures market can help demonstrate the concrete impacts of climate change and environmental degradation.
- Beau advocates for proactive changes to address water scarcity before it becomes an overwhelming issue for the general population.

### Quotes

- "There is a whole futures market dedicated to water now. That's a thing. It exists."
- "This is a good tool because there are a whole lot of people out there that do not believe this issue is real."
- "Titans of industry here. They're saying, we need to start paying attention to this."
- "It's also handing us a tool that we can use to show people the impacts of this are real."

### Oneliner

Beau introduces the concept of water futures, indicating the severity of global water scarcity and urging proactive measures through industry recognition and tangible tools.

### Audience

Climate advocates, environmentalists

### On-the-ground actions from transcript

- Educate communities on the significance of water futures and their implications (suggested)
- Advocate for proactive measures to address water scarcity in local communities (implied)

### Whats missing in summary

The full transcript provides detailed insights into the emergence of water futures as a tool to address water scarcity and compel action on climate change and environmental issues.

### Tags

#WaterFutures #ClimateChange #EnvironmentalDegradation #GlobalWaterScarcity #IndustryRecognition


## Transcript
Well howdy there internet people, it's Beau again.
So let me start off by saying that if you hear
pinging or a banging noise
during this video,
that's water
dripping out of the trees over the shop onto the tin roof.
I normally don't record when this is happening, but today it seems kind of
fitting.
I also want to point out that
despite the fact that what I'm about to say is going to sound like something out of a
dystopian novel, this is not a satire video.
Today we're going to talk about futures.
You know, like corn futures,
wheat futures,
oil futures,
a new kind of futures,
water futures,
because that's a thing now.
Water futures.
If you don't know what futures are,
super basic
way of looking at it,
it's a contract
that
allows a large consumer of something to lock in a price of a commodity,
and the seller to lock in a price of a commodity.
It also allows people to position themselves
based on whether they think that price is going to go up or down in the future.
It opens the door for speculation.
Now when people talk about this, they're going to point out the good things,
and there are some.
It will help large farmers
kind of
guess what their water costs are going to be in the future.
It'll be a good tool for
discovering how much you should pay,
or how much you should sell for.
There's going to be a lot of
talk about this, and trying to frame it in a way that is really positive,
but at the end of the day, one of the really important things to realize here
is that
price fluctuations
are what make a market like this,
and those fluctuations are caused by supply
and demand.
This is a really good indication that some pretty smart people
see
large fluctuations
in the supply and demand
of water.
It's probably worth noting.
This little instrument, this little tool,
is going to be based on the NASDAQ-Valez California Water Index.
The symbol is NQH20,
if you want to look into it.
It came on the scene around 2018 at about 370.
I want to say now it's hanging out around 500.
Pretty uh...
pretty significant increase.
Right now,
two billion people in the world
face water scarcity, water insecurity issues.
By a lot of estimates,
two-thirds of the world's population will face water shortages by 2025.
That is not some distant year.
That is right around the corner,
because while the world is covered in water, only three to four percent of it is fresh.
And a whole lot of that is really hard to get to.
So,
this is something that's on the horizon,
and there are a whole bunch of ways to look at this information.
You can look at it in the sense of,
well I mean, yeah,
something we need to get prepared for, we need to have a good gauge.
You can look at it in the sense of the fact that
two-thirds of the world's population
is going to face water shortages,
and
in a truly American
spirit,
somebody found a way to profit on it.
Um...
But I think most relevant
to the people watching this
is that this is a really good tool.
You want to find the silver lining
to the fact that we now live in the setting to some movie or book that we do not want
to be in.
This is a good tool,
because there are a whole lot of people out there
that do not believe this issue is real.
There is a whole futures market
dedicated to water now.
That's a thing.
It exists.
For those people
who talk to others about climate change,
environmental degradation,
this should probably come up,
because this is one of those things that you can point to. It is concrete.
These are
titans of industry here.
They're saying, we need to start paying attention to this.
If they're doing it,
that may sway
people who don't want to take this seriously.
So yeah,
there's some pretty unnerving elements to this,
but it's also handing us a tool
that we can use to show people
the impacts of this are real,
and they are going to affect us.
We should probably engage in some changes
before these numbers
get too high for the average person.
Anyway,
it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}