---
title: Let's talk about Trump's legal challenges and the high-water mark....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aPXpzpY9WZc) |
| Published | 2020/12/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing President Trump's post-election legal endeavors and their impact.
- Doubts Trump's ability to change election outcome through legal means.
- Warns of the risks and damage caused by Trump's activities.
- Shift in focus from policy to Trump himself due to legal battles.
- Emphasizes the importance of not letting Trump's removal be the peak of political engagement.
- Urges continued engagement post-Trump era to address existing problems and policies.
- Stresses the need to hold Biden accountable and push for positive change post-Trump.
- Concerns about people viewing Trump's exit as the end goal rather than a step in ongoing progress.
- Calls for sustained political activism to ensure government responsiveness to people's needs.
- Warns against prematurely celebrating victory when challenges still exist post-Trump.

### Quotes

- "We can't let the removal of President Trump become the high watermark."
- "It's his policies that were bad. It's his policies that we have to fight to undo."
- "We have to stay politically active and stay engaged so he responds, so the government responds and takes care of those of us down here on the bottom."
- "If we make it about a person, if we make it about Trump out, and that's the goal, we're going to lose a whole lot of people who think the battle's already over."
- "It's really unlikely that they're going to amount to anything, but it can create a situation in which people believe victory has been achieved when the fight hasn't even started yet."

### Oneliner

Beau warns against celebrating too soon post-Trump, stressing the need for sustained political engagement to address policy issues.

### Audience

Activists, Voters, Citizens

### On-the-ground actions from transcript

- Stay politically active and engaged to ensure government responsiveness to people's needs. (implied)
- Continue fighting against harmful policies enacted by President Trump even after his departure. (implied)
- Push for positive change post-Trump era by holding Biden accountable and advocating for progress. (implied)

### Whats missing in summary

The full transcript provides deeper insights into the risks of premature victory celebrations and the importance of ongoing political engagement beyond Trump's presidency.

### Tags

#Trump #PostElection #LegalChallenges #PoliticalEngagement #PolicyChange


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about President Trump's post-election activities,
the legal endeavors that he has been engaged in.
You know, I've made no secret of the fact I don't think that he is going to be able
to successfully alter the outcome.
I don't think he's going to be able to remain in office through these legal mechanisms.
I don't think it's going to happen.
I don't think he's going to be able to subvert the election through the courts.
That being said, his activities aren't without risk.
They are doing damage.
And I'm not talking about some vague notion of subverting faith in democracy or anything
like that.
I'm talking about him turning it into a battle about him.
One of the things that has been a positive from the Trump administration is that people
began to focus a lot on policy and look at all the bad things that he was doing.
They could identify him.
Due to the legal battles, it's shifting away from that and it's becoming more of
Trump out.
And yeah, there is some notion that everything's going to turn out all right and that those
who want him out are winning.
And yeah, that's probably true.
But that runs the risk of it being seen as not just a victory, but the victory once he's
out of office.
We can't let the removal of President Trump become the high watermark.
We can't lose the political engagement that has been built over the last four years in
opposition to his policies simply because the person is gone.
That's something that could cause long-lasting damage.
Because at the end of the day, when Joe Biden takes office, nothing has fundamentally changed.
All of the problems still exist.
All of the policies that President Trump enacted are still going to be in effect.
We can't lose the engagement because then those in the establishment may lose the will
to reverse course and take us back in a direction of forward movement.
They could theoretically just stay right where they were at and not achieve anything.
And it'd still be seen as a victory.
Them still have good approval ratings simply because they weren't the person.
Trump, yeah, with him it's really hard to separate the person from the policy because
he makes everything about him.
But being brash and narcissistic and all of the negative character traits that he has,
that in and of itself doesn't mean anything.
It's his policies that were bad.
It's his policies that we have to fight to undo.
And those don't change when Joe Biden takes office.
They're still there.
And all of the causes that brought us President Trump still exist.
Biden is in a situation where he doesn't have to do much to be seen as a good president
because the bar's been lowered.
Once he takes office, we have to fight to raise that bar.
We have to stay politically active and stay engaged so he responds, so the government
responds and takes care of those of us down here on the bottom, is responsive to the needs
and the will of the people.
If we make it about a person, if we make it about Trump out, and that's the goal, we're
going to lose a whole lot of people who think the battle's already over.
It's been won the second he leaves office.
And that's probably the most dangerous aspect of Trump's legal challenges.
It's really unlikely that they're going to amount to anything, but it can create a situation
in which people believe victory has been achieved when the fight hasn't even started yet.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}