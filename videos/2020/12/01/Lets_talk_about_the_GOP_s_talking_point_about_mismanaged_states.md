---
title: Let's talk about the GOP's talking point about mismanaged states....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lI_QZpT0KvU) |
| Published | 2020/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- GOP stalling stimulus, citing not wanting to bail out mismanaged states.
- Rick Scott from Florida supports GOP's stance, despite the state's own mismanagement issues.
- Florida's economy heavily relies on tourism, which is at risk if other states' economies tank.
- Unemployment funding comes from the people accessing it, not senators denying access.
- States being called mismanaged contribute the most to federal taxes.
- Mismanagement in states originated from the Republican Party at the federal level.
- GOP had no issue bailing out large companies but hesitates to support average people.
- Beau questions why average people should suffer the consequences of federal government mismanagement.

### Quotes

- "If New York's in debt, why should Florida bear it?"
- "They're denying you access to your money."
- "They can suffer the consequences of the federal government's mismanagement."

### Oneliner

GOP stalls stimulus, blaming mismanaged states, while denying people access to their own money during a crisis.

### Audience

Advocates for fair economic relief.

### On-the-ground actions from transcript

- Contact local representatives to advocate for fair distribution of stimulus funds (implied).
- Support community organizations working to provide relief to those impacted by the economic crisis (generated).

### Whats missing in summary

The full transcript provides a detailed analysis of the current stimulus situation and the impact on everyday people, urging for fair and equitable distribution of funds.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the stimulus and how the stimulus appears to be stalling
yet again because the GOP has found a new talking point.
Their current talking point is that they don't want to bail out mismanaged states.
Their term.
Florida's own Rick Scott has jumped on this bandwagon.
Florida being the state that isn't mismanaged, you know, that's the one that had the unemployment
website that was so mismanaged it couldn't process anybody's claims and kept crashing.
Governor DeSantis, the current governor of Florida, who is no stranger to the concept
of mismanagement, he wouldn't take that on the chin.
He wouldn't just accept that as being his fault.
So he made sure to point out where the blame really belonged for the needless roadblocks
and laid the blame squarely at the feet of none other than Rick Scott.
I would also like to point out, beside the absurdity of the talking point, that Florida's
economy is heavily reliant on this thing called tourism.
Tourism generally comes from other states.
If those states fail their economy tanks, if their economy tanks, people don't have
jobs to take vacations from, which means they don't come here and spend money, which means
our economy tanks.
Despite the current framing coming from President Reject Trump, we don't actually have two countries,
one red and one blue.
We have one country, the United States, and this country decided this particular issue
back in 1790.
If New York's in debt, why should Virginia bear it?
I mean Florida bear it?
Where's Alexander Hamilton when you need him?
I would point out that, number one, the unemployment funding that people are trying to access is
funded by the people trying to access it.
These senators, they don't have any money.
They're denying you access to your money.
I would also point out that many of the states that are being singled out as mismanaged contribute
the most to federal coffers.
This comes from federal taxes.
It's not like there's going to be a special tax imposed on the people of Florida to benefit
those in New York.
That's not a thing.
At the end of the day, these states, generally more populous, were trying to respond to mismanagement.
The mismanagement that upset the economy.
The mismanagement of the public health issue.
That mismanagement originated squarely with the Republican Party at the federal level.
That's where it came from.
Now those same people would like to deny funding that is necessary under this current system
for people to survive.
They had no issue bailing out large companies because it impacted them directly.
Because it hurt their stock portfolios.
But when it comes to the average person, once again, kick down.
Doesn't matter.
Those people, they can just stay poor.
They can suffer the consequences of the federal government's mismanagement.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}