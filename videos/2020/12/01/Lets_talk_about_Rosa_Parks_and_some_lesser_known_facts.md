---
title: Let's talk about Rosa Parks and some lesser known facts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f-BT6erndV0) |
| Published | 2020/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses Rosa Parks' story, pointing out some lesser-known facts about her.
- Rosa Parks is not accurately represented in the story of her refusing to give up her seat on December 1st, 1955.
- It is mentioned that Rosa Parks had been battling injustice for more than a decade before her famous act.
- She had been involved with the NAACP since 1943, indicating a long history of activism.
- Rosa Parks was carefully chosen by the NAACP to be a symbol and carry a message due to her potential to win in court.
- Parks attended the Highlander Folk School in Tennessee, where she was mentored by Septima Clark, known as the "mother of the movement."
- The courage to refuse to give up her seat cannot be taught, but the ability to carry a message can be learned.
- Beau stresses the importance of workshops, training, and organization in social change efforts.
- He mentions that Rosa Parks worked for a liberal couple, the Durrs, who were also active in the civil rights movement.
- The story of Rosa Parks goes beyond just being tired; it's about a long-standing commitment to fighting injustice.

### Quotes

- "The courage and bravery that it takes to say, no, I'm not moving. That can't be taught."
- "Boiling it down to her being a seamstress and just being tired, not wanting to move. I don't think that's a fair representation of the commitment it takes."

### Oneliner

Beau sheds light on the lesser-known facts about Rosa Parks, stressing the depth of her activism and commitment beyond the familiar narrative of tiredness.

### Audience

Activists, History Buffs

### On-the-ground actions from transcript

- Attend workshops or training sessions focused on activism and social change (suggested)
- Support organizations like the NAACP that work towards justice and equality (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of Rosa Parks' activism and the importance of continuous dedication to fighting injustice.

### Tags

#RosaParks #Activism #CivilRights #SocialChange #Education #History


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about Rosa Parks
and some lesser known facts about Rosa Parks. And we're going to do this because
obviously the anniversary and because I got a message.
Beau, I love you brother but I feel like your recent focus on training, organization, and advice
denies the historical reality that most times social change and revolution is caused by somebody
who reacts to an injustice and others follow their lead. Tomorrow is the 65th anniversary
of Rosa Parks. I'd love to hear a few words about her. Deal. Okay, so we all know the story.
December 1st, 1955, Rosa Parks becomes the first person to refuse to give up her seat.
She's a seamstress and she's tired. She is exhausted physically and the white section of the bus gets
full. The driver tells her to move and she says no I'm not doing it because she's also tired of giving in.
From there, there's the Montgomery bus boycott and it's a success.
That's the story. That's how it gets framed most times and there are a lot of facts in there.
There's not a whole lot of truth though because those things aren't always the same thing because
it leaves some stuff out and to be honest I don't think it does justice to her story.
First, she wasn't the first person to do this. She was somebody the NAACP looked at and was like,
you know what, she can win in court. She can carry the message. She can get it done.
I wonder how they knew that. She's a seamstress. Seems odd.
Until you find out that she had worked with the NAACP since 1943, investigated some high-profile stuff,
was a secretary. She was tired because she'd been battling this injustice for more than a decade.
In 1944, she had a job at Maxwell Air Force Base. She said, you might just say Maxwell opened up my eyes.
Maxwell Air Force Base is in Montgomery, Alabama. Why would it open her eyes?
Because it was the military, federal property. Trolleys were already desegregated.
Now, over time, she kind of loses a little bit of hope. She starts focusing more on youth outreach,
getting ready to pass the torch, hoping they'd succeed where she failed.
She worked for this liberal couple who was also involved in the civil rights movement, the Durrs.
In 1955, they arranged for her to get a scholarship to the Highlander Folk School in Tennessee.
A place for training activists. While there, she was mentored by none other than Septima Clark,
a woman that Martin Luther King referred to as the mother of the movement.
She was only there a couple weeks. Only there a couple weeks, and that was in August of 1955.
December 1st of 1955, when she changed the country.
The courage and bravery that it takes to say, no, I'm not moving. That can't be taught.
Doing it in a way that can carry the message, that's something that can be learned.
And organizing like that, the workshops that she and many others attended,
they can help provide those skills. They can also just put you around people who are committed to the same cause.
Even if it's just for a couple of weeks. And can help reignite that fire.
Get you back in the game. Highlander Folk School actually still exists.
It's got a new name, like Highlander Research and Education Center.
But it still exists, it's still up there in Tennessee. Because those kind of workshops,
that kind of training, that kind of organization, it's still necessary.
And it's going to stay necessary until we get a society where everybody gets a fair shake.
When you think of this story, when you think of Rosa Parks,
remember that the story that's often told, where she's just tired, doesn't want to move.
Yeah, I mean, okay. But she was tired of giving in.
Because she had been in this struggle for more than a decade.
That's a long fight and she had dealt with the injustice her entire life.
Boiling it down to her being a seamstress and just being tired, not wanting to move.
I don't think that's a fair representation of the commitment it takes to engage in what she engaged in,
in Montgomery, Alabama, back then.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}