---
title: The roads to the second channel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9copM6bMXZE) |
| Published | 2020/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing the second channel and its purpose.
- The second channel will feature longer, more in-depth, and edited content than the first one.
- Historical deep dives and practical demonstrations will be a part of the content.
- Filming on location and using entire cities to provide context for issues.
- Focus on community networking and actively demonstrating it.
- Plans to help set up community networks and assist existing ones in achieving their goals.
- Mention of in-person events once public health concerns clear up.
- Viewers will become part of the show, with plans for more produced and edited satire.
- Explanation for creating a second channel rather than combining all content on the first one.
- Acknowledgment that content on the second channel may not be for everyone due to longer duration and different type.
- Updates on the second channel will not be daily but aim for a weekly release once workflow is streamlined.

### Quotes

- "We'll be traveling and helping some of y'all set up your own community networks or meet up with those that are already running."
- "So we're definitely moving into a new chapter here, and we're taking the show on the road."
- "Don't want to force this on anybody."
- "Y'all are going to become part of the show here."
- "It may not be something that is for everybody."

### Oneliner

Beau introduces the longer, more in-depth second channel with historical deep dives, practical demonstrations, community networking focus, and plans for in-person events.

### Audience

Content Creators, Community Organizers

### On-the-ground actions from transcript

- Set up your own community networks or meet with existing ones (implied)
- Share feedback, suggestions, and topics in the comments (implied)

### Whats missing in summary

The full transcript provides a detailed overview of Beau's plans for the second channel, including historical deep dives, practical demonstrations, community networking, and viewer involvement.

### Tags

#ContentCreation #CommunityNetworking #InDepthContent #ViewerEngagement #SecondChannel


## Transcript
Well howdy there internet people, it's Bo again.
So today, we're going to talk about how you stumbled onto the second channel
because that's where you are.
This is the placeholder for the second channel.
Since I have you here, I'm going to go ahead and tell you a little bit about what's going to be here
as it goes live.
It's going to be a variety of stuff just like the first channel.
The main difference is this is going to be longer,
more in-depth, more produced. It's actually going to be edited.
It won't all be filmed here in the shop.
We'll be doing historical deep dives.
So we'll take a historical figure or an event
and explore it to the fullest.
We'll be doing how-to's.
On the other channel, we talk about a lot of stuff in theory.
Here, we're going to demonstrate it
and show the practical applications of some of it in the real world.
We're going to be filming on location a lot.
On the other channel, I'm very fond of taking
some event in history,
bringing it into a modern context, and using it to showcase
some issue that we're currently facing.
We're going to be doing the same thing,
only using entire cities
to provide the context.
We'll be doing a lot of community networking,
not just talking about it
in the theoretical sense as we do on the other channel,
but actively demonstrating it.
We'll be traveling and helping
some of y'all
set up your own community networks
or meet up with those that are already running
and hopefully help them achieve their goals.
Obviously, that's waiting for the public health stuff to clear up.
We'll be doing a lot of in-person events.
I don't just mean interviews
with people that are interesting in person.
I mean you.
Y'all are going to become part of the show here.
We will be doing some. We don't have any scripted yet,
but we plan on doing some more
produced and edited satire
on this channel as well.
The big question I keep getting is why are we doing a second channel? Why not
just put all this stuff on the first?
The people who signed up for the first channel,
most of them signed up for a three to twelve minute daily video,
sometimes twice a day.
I don't want to
force this on anybody.
I don't think that there's going to be anything on this channel less than twenty
minutes long.
It may be a different
type of
content. It may not be something that is for everybody.
So we're going to keep the channels divided.
It won't be updated every day like the other channel is.
This takes a lot more time to produce.
In fact, the first video that we did, it took me
like six days to film it, and it's been in editing a month.
There's a learning curve. The last one we did,
I think we filmed it in a day,
and it
should be out of editing
pretty quickly.
Once the workflow gets figured out, we are hoping to get one out a week.
It may be once every other week, depending on
how all of that works out.
Right now, the reason we're not publishing the videos that we
currently have, we're kind of holding them back,
is because we're building up
a library of content. So once we do start publishing them,
one will come out like every week, and it will give people a good feel for the
channel.
Now,
you're going to hear me say something that I never say.
Don't forget to subscribe. This is a new channel.
So if you want to stay updated on it,
go ahead and click the button, I guess.
And aside from that, I really would
like to hear from you all in the comments on this one.
And y'all,
kind of give me an idea
of anything you think that we're going to be...
that we may have missed in our planning here,
or
any topics that you definitely want addressed, or locations.
So we're definitely moving into a new chapter here, and we're taking the show
on the road.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}