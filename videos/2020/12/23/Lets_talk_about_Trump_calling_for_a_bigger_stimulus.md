---
title: Let's talk about Trump calling for a bigger stimulus....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aA2DXNtfT38) |
| Published | 2020/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump called for more direct payments to Americans after a deal was reached for $600 payments, which are significantly less than what other countries have offered.
- McConnell has been obstructing efforts to increase the payments.
- Beau questions Trump's motives, doubting his sudden concern for American workers.
- Beau suggests that Trump's actions may be driven by his true nature coming out.
- There is a power struggle between McConnell and Trump for control of the Republican Party post-Trump's presidency.
- Beau advises Democrats not to give up anything in return for the increased payments and let Trump and McConnell figure it out.
- He believes it might benefit working-class Republicans to see Trump advocating for them, even if it's not genuine.
- Beau encourages breaking down false beliefs, such as the notion that the Republican Party truly represents the rural working class.
- He concludes by expressing the importance of moving forward as a country and letting the dynamics between Trump and McConnell play out.

### Quotes

- "Let them fight and see what happens."
- "There's no real loss in it for Democrats either way."
- "Best case scenario, get the cash payments to the people who need them."
- "It might do a lot of working class Republicans some good to see Trump saying, hey, let's help out the little guy."
- "We're at the point where we have to move forward as a country."

### Oneliner

Trump pushes for more payments, Beau questions motives, advises Dems not to give in, suggests letting Trump and McConnell duke it out for the benefit of the nation.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Let Trump and McConnell's power struggle play out (suggested)
- Challenge false beliefs about political parties (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the power dynamics between Trump, McConnell, and the impacts on American workers and the Republican Party.

### Tags

#Trump #McConnell #DirectPayments #RepublicanParty #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump calling for more direct payments to Americans right
after the deal was reached.
And why?
People are questioning his motives.
So if you don't know, the deal was reached for cash payments to Americans of $600, which
pales in comparison to what most comparable nations have been offering, either through
just their normal unemployment benefits, through cash payments, or through paychecks delivered
through employers but backed by the government.
We're way behind on this.
As soon as the deal was reached, Trump said he wanted more.
The entire time McConnell has kind of been standing as a roadblock in the way of getting
more.
People are questioning Trump's motives for doing this.
I don't believe it's altruism.
I don't think he suddenly actually cares about the American worker.
He could have put this pressure on McConnell a long time ago.
He didn't.
I think that it has to do with Trump's true nature coming out.
Years ago, I got a call from a friend in the middle of the night, and he's like, I need
you to come over here.
It's an emergency.
I'm like, OK.
Get over there.
And he hands me a lot of cash, and he's like, my friend just got beat up by her boyfriend.
I'm going to need you to bail me out.
Sure enough, fine.
Sounds good, but hang on one second.
That's going to make you feel better, but what happens next?
After he takes a hit, loses that power, feels emasculated, what's he going to go home and do?
I think that that may have something to do with it.
McConnell has exercised power without limit, unchecked for years, and now all of a sudden
he's being checked.
I think he is taking it out on those inside his house, inside his party.
I think he's trying to exercise power for power's sake.
I don't believe he actually cares.
I also think this has to do with the feud between McConnell and Trump and the vying
for control of the Republican Party after Trump leaves office.
It's furthering that because McConnell put a lot of stock, put a lot of political capital
on the line for this, and Trump undermined it almost immediately.
That's what I think it has to do with.
That being said, okay, take it.
Take it.
If you're a Democrat, though, I wouldn't give anything up in return.
Sure, people need it, no doubt.
No doubt people need it.
But long term, what's better?
What is better for the average American, to give up and give in to more of Trump and McConnell's
demands that generally undermine the average American long term, or to give nothing in
return and let McConnell and Trump duke it out and see who wins?
Worst case scenario, the Republicans have a bunch of division within their party and
the payments get passed in January after Biden takes office.
Realistically, that's your worst case scenario.
Best case scenario is you give up nothing and the payments come through.
But I think it might do a lot of working class Republicans some good to see Trump saying,
hey, let's help out the little guy, whether he means it or not, and see McConnell stand
in the way of that.
I think that might do the nation a whole lot of good.
And I think it may be worth being uncomfortable for an extra month.
We're at the point where we have to move forward as a country.
And a lot of that is going to be breaking down things that people believe that aren't
true.
One of those is that the Republican Party is the party of the rural working class, because
they're not.
They never have been.
But they push that image.
So I would let them fight and see what happens.
There's no real loss in it for Democrats either way.
Best case scenario, get the cash payments to the people who need them.
Worst case scenario, they get them a little bit later, but it exposes this false belief
that a lot of people have.
And I think that may be worth it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}