---
title: Let's talk about the Trump vs McConnell contest....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qV-ZS3m3gD8) |
| Published | 2020/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into the internal power struggle between Trump and McConnell within the Republican Party.
- The Republican Party will conduct a poll on January 6th to decide whether they want to be the party of Trump or McConnell.
- Despite being termed as a poll, it's more of a theatrical voting on objections to the electoral votes.
- The votes are expected to go in favor of McConnell over Trump due to long-term political capital considerations.
- The breakdown of votes will reveal who supports Trump's control and who prefers McConnell's leadership.
- Newer representatives may lean towards supporting Trump, thinking he holds more power, while experienced politicians understand McConnell's influence.
- McConnell is likely to win the poll, leading the Republican Party to revert back to obstructionist tactics during the Biden administration.
- Beau predicts that if McConnell wins, the party will adopt a George Bush-era style, whereas a Trump victory may further radicalize the party.
- Despite the theatrics expected on January 6th, the essence of the poll is deciding between McConnell and Trump's control over the party.
- Beau advises staying calm amidst the wild statements from Trump loyalists and suggests that smart politicians will lean towards McConnell for their own interests.

### Quotes

- "It's really deciding whether McConnell or Trump gets to hold the reins of power."
- "My best guess is that McConnell will win this poll."
- "A smart politician, somebody who is concerned with self-interest and maintaining their own power, they're probably going to go with McConnell."
- "You will be rooting for McConnell, because he is definitely the lesser of two evils out of that."
- "Just stay calm, because my guess is that you're going to see a lot of Trump loyalists trying to prove how much they love Trump."

### Oneliner

Beau delves into the internal power struggle between Trump and McConnell within the Republican Party, predicting McConnell’s victory in the upcoming poll and the party reverting to obstructionist tactics during the Biden administration.

### Audience

Politically engaged individuals.

### On-the-ground actions from transcript

- Stay informed about the outcomes of the Republican Party's poll on January 6th (suggested).
- Monitor how your representatives and senators vote in the poll and understand their stance on Trump or McConnell's control (implied).
- Advocate for political strategies that prioritize governance over theatrics and political capital (implied).

### Whats missing in summary

Insights on the potential implications of McConnell or Trump's victory for the future direction of the Republican Party.

### Tags

#RepublicanParty #Trump #McConnell #PowerStruggle #PoliticalPrediction


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Trump, McConnell,
and a question that was messaged to me.
I went back and looked, and I have talked about this topic
quite a few times, and I have never answered this question.
We've discussed how there's an internal contest
between Trump and McConnell for control of the Republican Party
once Trump leaves.
Who is going to be the real power broker?
And the question is, when do we get to find out who wins?
On January 6th, the Republican Party will be conducting a poll,
and they will decide whether they want to be the party
of Trump or the party of McConnell.
Now, prior to this poll being taken,
there's going to be all kind of theatrics.
There's going to be a bunch of political theater
because it's not really a poll.
They're going to be voting on objections
to the electoral votes.
Now, these objections,
despite all of the political theater that's going to occur,
they're not going to go anywhere.
Prior to walking out here, I tried to come up with a scenario
in which they could go somewhere, and I couldn't.
You're talking about a percent of a percent of a percent
of a percent of a chance.
It's not going to happen,
but the votes are going to take place anyway.
A bunch of Trump loyalists will object to the Electoral College,
and Republicans will vote whether to sustain
those objections or, you know, shoot them down.
Those who vote in favor of the objections are saying,
I want Trump to control the party.
Those who vote against them are saying,
I want McConnell to control the party.
That's really what it's about.
It's not about the electoral votes at all.
It's a poll, and that's how you need to think of it.
When you see the breakdown of who voted which direction,
that's what you're going to see.
Trump loyalists are going to vote
in favor of these objections.
Anybody who votes against them is conceding
that they're going to roll with McConnell,
regardless of what they actually think of Trump,
because it's not about that.
It's about political capital at this point.
My best guess is that you're going to see a whole lot
of newer representatives and senators
vote in favor of these objections,
because to them, it may seem like Trump
is the real powerhouse.
To the more experienced politicians,
the people who have been up there a while,
they know McConnell is actually the one that holds the reins.
They understand that if they vote in favor of Trump,
McConnell can make life pretty hard for them.
We know how good McConnell is at obstructing things.
He can do the same thing to them.
Trump isn't going to be in power.
McConnell will be.
However, Trump has Twitter.
To newer reps, they may not want to face that backlash,
because they're new.
They don't understand the long-term game
that gets played up there.
So that's when we get to find out, January 6th.
And regardless of all of the theatrics
that I'm sure will be taking place,
it's really deciding whether McConnell or Trump
gets to hold the reins of power.
If I'm wrong and a whole bunch of people
vote to sustain these objections,
count on Trump still running the party from Twitter.
If he is decisively defeated in this poll, it's McConnell.
McConnell will go back to George Bush-era style
of Republican Party.
Trump will try to push his supporters
to be even more Trump-esque.
So as weird as it is for people who watch this channel,
on January 6th, you will be rooting for McConnell,
because he is definitely the lesser of two evils out
of that.
Weird to say, right?
My best guess is that McConnell will win this poll.
And then once he does that, the Republican Party
is going to revert back to being very obstructionist
during the Biden administration.
That's my best guess.
But we'll find out.
Just stay calm, because my guess is
that you're going to see a lot of Trump loyalists trying
to prove how much they love Trump.
And they are going to say all kinds of wild things.
I wouldn't worry about them too much.
A smart politician, somebody who is
concerned with self-interest and maintaining
their own power, which is, yeah, I mean, that's them,
they're probably going to go with McConnell,
if they understand how the game is played.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}