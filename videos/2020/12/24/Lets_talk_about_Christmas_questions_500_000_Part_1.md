---
title: Let's talk about Christmas questions/500,000 (Part 1)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LuuKDwsytrk) |
| Published | 2020/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Taking light-hearted questions from Twitter and answering them.
- Sharing favorite children's shows and movies for their empowering messages.
- Me-time activity is talking with internet people, now missing road trips.
- Favorite fiction book series by Bernard Cromwell.
- Cherished stuffed animal: a stuffed pumpkin.
- Addressing the impact of becoming a celebrity on family.
- Suggests Irish cuisine for saving while cooking.
- Believes in the power of community networks over government reliance.
- Cooking shepherd's pie and enjoying Oreos as a treat.
- Views on balancing mutual aid and state services.
- Sharing about life, celebrity status, and family dynamics.
- Reflections on Christmas stories, songs, and holiday traditions.
- Favorite American and non-American foods.
- Hopeful predictions for 2021.
- Recalling past Christmas gifts and New Year's resolutions.
- Sharing experiences with his wife and family traditions.
- Details about the filming setup in his shed and favorite tools.
- Thoughts on political narratives in stories like The Grinch.
- Thoughts on Santa Claus, cereal, and historical figures.
- Favorite travel destination in the US and Christmas traditions.
- Memorable encounter with his wife and favorite cultural tradition.
- Family holiday traditions and unique Christmas requests.

### Quotes

- "I think that's one of those moments where not only somebody, people corrected something that was wrong, but it shows that there are some things that can unite everybody."
- "I don't know if they want to buy me a drink or hit me with a bottle."
- "Anything belongs on pizza if you're brave enough."
- "Not particularly my choice, but yeah pineapple on pizza an abomination or an extremely wrong choice."
- "That may happen at some point in fact we thought about doing it for this one but you know it's it's the holidays so."

### Oneliner

Beau shares light-hearted moments, favorite shows, family traditions, and community reflections while answering Twitter questions. 

### Audience

Twitter users, families

### On-the-ground actions from transcript

- Send Beau a physical mail package to P.O. Box 490 Sneeds, Florida, 32460 (suggested).
- Try cooking Irish cuisine for saving while exploring new flavors (implied).
- Stock up on essentials for disaster relief packages following climate or situation-specific needs (suggested).

### Whats missing in summary

Insights on Beau's personal growth, values, and community building efforts.

### Tags

#Twitter #Family #Community #Cooking #Celebrity #Christmas #MutualAid #Books #Travel #Traditions


## Transcript
Well, howdy there internet people it's Bo again
so today we're going to take a bunch of light-hearted questions from Twitter and
Answer them. This is just in case you're bored tonight or tomorrow and
You need something to do this is also going to double as our
500,000 subscribers video because that just happened as well
Thank you for that, by the way
So, let's just dive in here and see what we have.
These are all supposed to be light-hearted.
I don't know if it turned out that way and we'll go from there.
If there's anything political or that's just a downer, we're going to skip it.
All right, so Disney Princesses, Curious George, Mr. Rogers, Sesame Street, Magic School Bus,
What other children's shows and movies are important to you and what lessons do you take
from them?
All of them.
All of them.
G.I. Joe, Kim Possible, I think anything that spurs imagination spurs critical thought.
And I think that the messages contained in most children's shows are some that maybe
adults need to be reminded of from time to time you know back when there was
still the belief that the world can be better because it can be and you can
help do it and that's the message in a whole lot of shows and I think that
that's important for kids too that aspect of empowerment what's your
favorite me time activity that you do for fun to cheer yourself up that isn't
talking with us internet people. That is my me time activity. Prior to this year I
like to go on road trips and travel and do a whole bunch of stuff I'm not doing
right now because yeah because it's a mess.
What's your favorite fiction book series? Probably anything by Bernard Cromwell.
The person who wrote What Became the Last Kingdom on Netflix and the BBC is a whole
bunch of different series and almost all of them are pretty good.
Favorite stuffed animal growing up.
This isn't a joke when I say this.
I had a stuffed pumpkin.
the one on the back shelf. But yeah, I had a stuffed pumpkin. In case you're wondering
why we're not doing this in the shop, it's raining. The weather outside is frightful.
Do your kids know what a big deal you're becoming? What do they think? They don't know that because
Because I don't know that.
I mean, I understand the numbers.
And I get that.
But there's still an element that just I don't know.
And I don't know how to take it, to be completely honest.
Recipes, cooking for those who are trying to save.
I would look into Irish cuisine.
that is, a lot of it is born out of poverty.
You end up using a lot of the same ingredients,
so it helps, but it tastes different
so you don't suffer palate burnout.
I mean, I think that would be a good place to start.
Who's your favorite artist?
That's a broad question.
I'm gonna say Ansel Adams, just off the top of my head.
photographer. I'm gonna skip that one because I would I would end up making
It's a Wonderful Life very political and probably a downer. This may
sound more serious than the tone in which I'm asking so try to take this
light-heartedly because that's how I mean it right now. Why are you doing this for
or exmus instead of solstice.
Cultural norms among the majority of the audience,
the majority of you guys.
I don't know if you're asking this
because you are familiar with some of my more
earthy beliefs or this is just random.
Generally, on other holidays, you don't have
negative impacts from people being alone.
It's really that simple.
Bo, I need your top five all-time favorite country albums and or artists.
I could go on about that all day, so we're going to rephrase that and keep it simple.
Who is your favorite country artist and why is it Steve Earle?
What is your favorite breed of horse?
I don't have one.
I like horse's personalities.
Pirate, the most recent addition is really
because she was just very playful and interested
in me particularly for some reason followed me around
and we became buddies.
Not really so much about the breed.
What are your thoughts on balancing providing mutual aid
and the state using that as an excuse to cut services
but not taxes for real people.
I don't know that there is a balancing act there.
See, to me, the theory is that the use of mutual aid
and community networks would reduce reliance on government,
therefore those services wouldn't be needed,
and eventually they would get cut.
And realistically, government is not going to cut taxes
when they do that.
The difference is effectiveness and bang for your buck.
That's one of those uncomfortable phases
that we will have to go through to get to a better society,
I believe.
Realistically, if the government was to be the sort
that would reduce taxes
when needs for services declined,
it would be responsive enough to the point
we wouldn't need to engage in mutual aid.
So yeah, I don't know that there is a balancing act.
I think that's something that is going to occur and when it does, I actually think it
will help speed things along because people will realize the inefficiency, but people
have known how inefficient the government is in a lot of ways for a long time.
Do you cook?
What's your best dish?
What is your favorite treat?
I do cook.
I would say shepherd's pie is what I do the best.
I don't know.
What is my favorite treat?
Oreos.
And those just come out of a bag.
I don't cook those.
If you were to frame the story of how the Grinch stole
Christmas as political, how would you do it?
What Christmas song do you hate the most?
Eggnog, yes or no?
yes or gross. I would say that the Grinch story is political. At its heart it's a
story about bullying and the haves and the have nots. To me it is a
political story. I mean realistically when the world is the way it is
everything is political in some way. I don't know that I have a Christmas song
that I truly despise? Maybe the one from that movie where they played it
constantly in the background. The British movie and I can't remember if it was Love
actually, maybe it was the name of it. I actually have the response that the
characters have in that movie. Eggnog, yes or gross, depends on how much coffee I
had before I had the eggnog and by coffee I mean coffee with the other
ingredients. Generally not something I love, but if I've already started it'll
be alright. What is your most cherished accomplishment? That's a tough one.
Probably something that I wouldn't talk about on YouTube, realistically. I don't
know exactly, but my guess would be that it falls into that category of stuff
that I did when I was younger.
The wife and I are curious to know if your collection of socks is anywhere as prolific
as your collection of shirts.
No, it is not.
Is Santa real?
Yes, of course.
Do you put the milk in the bowl before the cereal?
No.
No, of course not, that's, no, that's wrong.
What cereal is it?
Some kind of kid cereal, Lucky Charms,
I don't know, something like that.
Baroness stream when?
Baroness is in a couple videos.
There's a video where she is sitting by me
when I'm by a fire.
I'll try to find a link and put it down there.
As far as a strain, Baroness is, you know, smart,
as I've pointed out, but she's not exactly friendly.
I don't know that, yeah, she's not actually a nice puppy,
to be honest.
How can we send you physical mail, like a package?
Well, now I'm nervous.
I have cool stuff I know for a fact you will like,
and I guess you cannot find it in America.
Also, please release a clean plate of your workshop background going to use these in Zoom meetings.
Actually, I had a couple of people ask for the photo of the shop. I will try to do that soon.
As far as a mailing address, it's P.O. Box 490 Sneeds, Florida. 32460 Sneeds, which is S-N-E-A-D-S.
What's your favorite all-American food? New York Strip. What's your favorite non-American food?
What did you do to stay positive when you, shall we say, had a lot of time to yourself
to think?
There's a movie called A Bronx Tale, and in that movie somebody asks that question and
it's basically, you know, you can do three things.
You can lift weights, play cards, you're getting into trouble.
I read.
And I did.
I mean, I worked out a little bit, but mainly I spent my time reading.
Did you ever expect to be a celebrity?
No, given the fact that I tried to keep a low profile most of my life.
No, I absolutely did not.
And I'm still, I am not used to it.
When somebody recognizes me, I'm still at that point where I don't know if they want
to buy me a drink or hit me with a bottle.
There's still, I'm very awkward in that.
When can we see Baroness and Destro in a video?
Destro maybe, he would hang out, but let's see.
What are the most useful items to stock up on
for relief disaster packages
when you are out applying rule 303?
Depends on the situation, obviously,
like climate, stuff like that.
it's cold, people always need socks. Always. It doesn't matter what's going on, they
always need socks. Generally, you want food, water, fire, shelter, first aid, and a knife
in a kit. A lot of times hygiene stuff is left out. Typically, feminine hygiene is almost
always forgotten. And then there's also, if you're talking about larger stuff, it changes
each time. Like in the last hurricane, we had a bunch of people run out of toilet paper,
which now everybody in the country is familiar with how bad that is. But I would imagine
because of that, next time we won't and everybody will have tons. So there's a historical learning
curve that goes along with it. Gooseneck or bumper? I'm gonna assume this is about
trailer hitches and I'm gonna say Gooseneck. What's your morning routine?
Because I married an angel I normally get coffee like right as I wake up
sometimes even in bed and then I read the news and then I plan my day from
there. That's that's my normal routine. Q&A with you and Bo's wife together
please. That may happen at some point in fact we thought about doing it for this
one but you know it's it's the holidays so. Ribbon candy good or only for
grandparents. I'm gonna say only for grandparents. Can you give a little of
your background for newer subscribers? There's a video I just always like to
direct the people to that because it's it's more in-depth than anything I would
do when I'm briefly asked a question. Let's talk about 100,000 subscribers.
That actually has answers all the questions that people actually want
answered when they asked. I will say that most of the stories are not accurate. There may be some
truth to some of them, but they are not accurate in the way that they are out there, if that's
in relation to anything in particular. If you had to move to any other state, which would it be and
why? Tennessee, Eastern Tennessee. Mountains are the ocean. Very extreme when it comes to that.
I always love your history lessons. Hopefully if the rain stops, you're going to get one
tomorrow about the Christmas truce. If not, look it up. It's a cool story.
any good predictions for 2021? I'm gonna just hope for a return to normalcy of
some sort. That's the only prediction I have and that the second
channel will go live at some point. I was hoping for it to be right after the new
year, which with the way this has been going, I'm gonna say the end of January.
worry. What do you think about Mason Silva winning skater of the year 2020?
I've been I'm wondering if this is the same person. I've been asked about this
multiple times. I think maybe there's a photo of me with a skateboard at some
point but that just so you know that was like 30 years ago how many exactly t-shirts
do you have 1,312 I have no idea I have no clue a lot a lot and it's an ever
growing pile. What other work do you do in your workshop? Woodworking, mechanical, or
just fixing stuff? What's your most used tool? Right now, honestly, I spend all of
my time in there making videos. There was a time that I used it for the gardening
stuff. We grow a lot of stuff here and basic carpentry. I do do a little bit of
metalworking but not much. It is more as needed. I would say the most used tool is
probably the nail gun. Just a guess. Did the founding fathers consciously list
life before liberty and the pursuit of happiness? I've never thought about that
before but it would make sense that they would. I mean that that does seem to be a
descending list. Is cereal soup? Yes. Is Die Hard a Christmas movie? Yes. Are hot
dog sandwiches? Also yes. Does pineapple belong on pizza? Anything belongs on
pizza if you're brave enough. Not particularly my choice, but yeah pineapple
on pizza an abomination or an extremely wrong choice. I'm gonna say a wrong
choice but yeah that's personal taste I would guess. What is your least favorite
thing about this holiday season? Not sure if this is specific. Generally I do not
like the commercialization of the whole idea and I wish people understood how
the holidays evolved and all of that stuff. I think that would be a good
unifying thing to find common roots in a lot of them. If you're talking about this
year in particular, the whole public health thing just making it an absolute
pain. That would be high on my list. Where do you find all your
t-shirts and which are you most fond of? The ones that I'm most fond of are ones
you will probably never see in a video. A lot of my shirts have political
connotations to them, and some of them feature language that I would not put on the channel.
As far as where they come from, some are in our Teespring store, and most of the other
ones my wife brings home, and it's like, ah, look at this.
And I'll be honest, as much as she complains about all the shirts on Twitter, she's responsible
for like half of them. So I mean you can have sympathy but not too much. How do you have
your coffee tea? Coffee by the gallon. Like I make Lorelei and Rory look like Mormons.
Um and in any way I just yeah I drink a lot of coffee probably way too much. How much
How much do you water your beard, how much sunlight does it need?
I do absolutely nothing to my beard.
The reason I have a beard is not out of fashion statement, it's that I don't want to deal
with shaving.
So if I had an extensive grooming routine for it, it would defeat the purpose.
Best Xmas gift you've gotten.
When I was a kid, I got the USS Flag.
It's a GI Joe playset, and it's like the size of a dining room table.
It's huge.
It's an aircraft carrier.
It's big enough to like lay down on.
I thought that was the coolest thing in the world, and given the fact that my dad was
enlisted Army at the time, I know that he, I know that that was something that they put
lot of effort into getting because it wasn't cheap. So I think maybe that was
the best? I don't know because I think that that would change by age. Right now
the best gift I could get would be like a vacation to somewhere warm. If you could
debate any historical figure living or dead, real or fiction, who would you
choose and what topics would y'all debate? I'm gonna say Thomas Jefferson and it's really just
more of an interrogation. I have so many questions. Was it really about political expediency? Was it
about money? Why were some of these things done when he referred to them as a hideous blot?
One of the people, at least in American history, that is so contradictory, that could have done so
much good and did some but not as much as he could have and I just I have
questions. Does the shed ever get too hot cold to film in? What do you do if when
it does? Temperature normally isn't much of a problem. It's normally rain because
it has a tin roof. The shop is designed for Florida so the the light you see in
the back sometimes that those aren't like that's not like track lighting
that's actually sunlight there's there's gaps up near the roof to let hot air
out so it stays pretty cool even in there even when it's like a hundred
degrees that does present a little bit of a problem in the winter because it
It does not retain heat at all.
But I have a little heater out there that I will let it warm
the place up and then turn it off right before I film,
because it's loud.
And you guys generally, y'all have said y'all like the
ambient noise of the grasshoppers and crickets and
frogs and birds and whatever.
Cake or pie?
Pie.
Do you think the US and the world will eventually move
down and left?
Yes.
I honestly believe that that is the,
I think that is the inevitable future
unless things go horribly wrong.
I think that's where we will head if we are smart
and if we work to do that.
The most amount of freedom for the most people
with the most equality.
that seems like it should be the goal and I think
eventually that is where we will go. Hopefully we get there before we face
just irrevocable
negative outcomes
from past behavior. Of
all of the places you have visited, which was your favorite
and why?
That's a tough question.
I'm going to say stay in the US on this one.
I'm going to say I don't even know then.
New Orleans.
I went to New Orleans prior to Katrina.
And I really enjoyed the atmosphere and the way, just
the way people were.
It was unique in the sense that it was still a very small town feel in a large major city.
Now from my understanding is that after Katrina, things were changed and it doesn't have that
anymore.
I haven't really been back for any length of time since then though, so I don't know
for a fact.
Do you put potato chips inside your sandwiches?
generally. I'm going to rephrase that and I'm going to be kind of vague in this
answer. Santa's effect on kids positive or negative? I'm going to say positive. I
understand some of the concerns that people have about it but at the same
time anything that sparks imagination sparks critical thought later and I
think overall I do not see much of a difference between that and Halloween
and that or superheroes or anything like that so it is far if I'm if I'm catching
this question. New Year's resolutions, please. For me, I don't know. I would like to start
running again. I say that and I know full well that it's really unlikely that I start.
Get the second channel actually up and running. That would be my New Year's resolution. Tell
me a story about a group of people that saw something wrong, took action, and corrected
that wrong. I could tell hundreds of those. A recent one is after Michael here. You had
a whole bunch of people who were very, let's just call them distanced politically, come
together to help people in Panama City and the surrounding areas because they were in
of really bad way. That hurricane didn't get the coverage, I think it really should
have. There were parts of that city that were just flat when it was done. And people who
generally would not associate came together to make stuff happen and get things to people
The staging area that was used is a facility that helps vets with PTSD and they use horses to do it.
It's a really novel concept. It's really cool.
I would be willing to bet that many of the people involved in that project have Trump flags.
I don't know that because we didn't talk politics because that would have been bad.
you know, you don't want focused on the mission at hand. But given the nature of
the event, nobody cared about any of that. We had people that needed baby formula,
we had people that needed all kinds of stuff and we had to get it to them. And I
think that's one of those moments where not only somebody, people corrected
something that was wrong, but it shows that there are some things that can
unite everybody. How'd you meet your wife? She came by the studio and with her
mother and I flirted with her and she did not respond. I'm not a pushy person
so I stopped as they were leaving. Her mom was like you know she's really
preoccupied today and probably didn't even notice. So when she came back by I
I didn't have everything ready and I'm sitting there putting it together and of
course that day she happens to be in a hurry and she's like hey you know you
couldn't have had this done already and I'm like well I was hoping to have a
little bit of time to talk to you and she's like yeah how's that working out?
Fantastic okay and yeah so when she starts to leave she looks over her
shoulder and she's like yeah you have my number okay now she called me prior to
me calling her because she had been over changed there are some people who
believe that that may be a good way to determine somebody's integrity at some
point I'm not saying that happened but either way she called back and I was
like, hey, why don't you give me that back over dinner, or lunch, or breakfast, or coffee?
And she settled on coffee.
And then for like two weeks, I got text messages explaining why she couldn't make it.
And I guess a couple of times, I was unavailable.
And I was like, you know, this is either the most elaborate blow off in history or we're
really busy.
You just let me know when you're available.
And then the next day she stopped by and brought me coffee and that's when it started.
I think we had lunch finally like a week later.
So let's see, already answered that.
What's your favorite cultural tradition, either by what it means that it's fun to do or both?
I may have mentioned this, I like sweat lodges and EPs.
I don't know that I would describe it as fun,
but I would describe it as worthwhile, fulfilling.
If you ever have the opportunity to do it,
I would suggest it.
What's your favorite science fiction novel?
I can't remember the name of it.
box or if you press the box you get you get a reward but somebody else gets
punished and they made a movie about it that was not a fantastic version but
that I can't believe I can't remember the name of it it may actually be the
box see what are your family's favorite holiday traditions we open one gift
On Christmas Eve my wife puts peanuts in the stockings. I honestly don't know why
it's something her family's always done. I guess that's it. I think those are the
only ones that are unique. If you leave a plate for Santa, what's on it? Oreos, of
course, and this year hand sanitizer. What's the coolest thing someone in your
family requested for Christmas. My 6-year-old just told me that he wanted a werewolf robot.
And this isn't like some toy, he just like a legitimate werewolf robot. Yeah, I don't
know about that. I don't know how we're going to do that. If he had to choose a theme song,
what would it be? Prayer of a Refugee.
Are you a foodie and if so, what do you and family love to cook for dinner?"
I mean, I don't know.
I enjoy cooking, but I don't cook anything special.
I cook.
I have a wide variety of...
I've learned.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}