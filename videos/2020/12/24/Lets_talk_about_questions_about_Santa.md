---
title: Let's talk about questions about Santa....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tWTMerznrfU) |
| Published | 2020/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing concerns about Santa's ability to deliver presents due to international borders and health issues.
- Acknowledging a child's thoughtful questions and appreciating their level of concern.
- Santa has been vaccinated and ensured safety for his workers at the North Pole.
- Santa has plans in place for crossing international lines and has volunteers to assist in countries with travel bans.
- Production may be lower due to social distancing, but Christmas will still occur with the spirit of giving.
- People around the world come together during Christmas, transcending boundaries and differences.
- The voluntary efforts of individuals during the holiday season transform communities.
- Emphasizing the power of cultural norms in bringing people together without coercion.
- Encouraging the importance of changing thought to address issues that governments may not act on.
- Advocating for starting cultural norms based on care and compassion.

### Quotes

- "Christmas will occur."
- "The volunteers are Santa's community networks."
- "You don't have to change the law. You have to change thought."
- "It just became a cultural norm, because people got behind it."
- "You have to start doing it."

### Oneliner

Addressing concerns about Santa's ability to deliver presents, Beau assures that Christmas will occur with the spirit of giving, showcasing the power of cultural norms to unite people globally.

### Audience

Global community members

### On-the-ground actions from transcript

- Volunteer to assist in local community events or initiatives to spread the spirit of giving during the holiday season (suggested).
- Initiate or participate in activities that foster care and compassion towards the environment and fellow individuals (suggested).

### Whats missing in summary

The full transcript provides a heartwarming reminder of the power of unity during the holiday season and the impact of cultural norms in fostering care and compassion globally.

### Tags

#Christmas #Unity #Community #CulturalNorms #Volunteer


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Santa
and the issues that Santa has to overcome this year,
as well as what we can all learn from Santa.
So even if you're an adult, stick around.
We're going to focus on some questions that
came from a child and hopefully address
some of the concerns.
The main questions were whether or not
Santa was going to be able to do what he normally
does because of people not being able to cross
international borders, and the fact
that because the public health issue has reached Antarctica,
it is safe to assume it has also reached the North Pole.
First, I want to take a moment just
to appreciate the level of thought that this child has
because that's amazing.
And now let's actually talk about it.
Christmas will be fine, there's no doubt in my mind.
The North Pole has its own approval process for treatments
and it moves a lot faster than the United States.
As Dr. Fauci said, Santa has already been vaccinated.
He'll be fine.
And they didn't have issues up there
because Santa cared about his workers
and made sure that everybody was distanced.
So they didn't have any issues.
When it comes to crossing international lines,
it should be noted that this is Santa.
He has plans for everything.
He has dealt with this before, back in 1918, and he did fine.
He has a bunch of volunteers who are
willing to help him out in countries
that he may not be able to get access to because of travel
bans.
Now, it is worth noting that because the workers were
socially distanced and to protect them,
that production was lower.
So this Christmas may not be as big as Christmases of the past,
but rest assured, Christmas will occur.
It's important to note that all of these people
volunteered because the true spirit of the holiday
is one of giving.
And you have a whole bunch of people all over the world
willing to give their time and energy to make sure
that this comes off.
Now, I hope that alleviates this child's concerns.
For the adults, I would like to point out
that this year, like many other years,
there are people all over the world
that cross international boundaries,
that this idea permeates and resonates
and permeates and reaches people who
don't agree on anything else.
But they come together for this one thing.
There's no government force involved.
There's no coercion.
It just becomes a cultural norm for a subset of people.
It's not worldwide.
It's not everybody.
But for a large group of people, this is a cultural norm.
And it's something that transforms a whole month.
And there's nothing forcing them to do it.
Laws weren't changed.
Thought was.
And that's how this came about.
The volunteers that all act individually
in pursuit of this goal are Santa's community networks.
And they literally alter the entire month in the area
where this is a common practice.
And this is something that is generally
something that could be polarizing,
because many people do their best to divide people
among religions.
But at this time of year, people tend
to come together in the areas where this is celebrated.
It wasn't law.
It wasn't any form of coercion.
It just became a cultural norm, because people got behind it
and made it a thing.
For those things that government seems intractable on,
those things that government will not act on,
you don't have to change the law.
You have to change thought, make it a cultural norm
to care about the environment, to care about your fellow man,
whatever it is.
And you just have to start it.
You have to start doing it.
And don't hide the fact that it's important to you.
And it can spread, just the way the current version
of Christmas spread.
Anyway, it's just a thought.
Y'all have a good day.
Merry Christmas.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}