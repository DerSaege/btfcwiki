---
title: Let's talk about Trump, concession, and decisions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ltTT7ZEJFhk) |
| Published | 2020/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts the timeline of events surrounding Al Gore's concession speech in 2000.
- Compares the situation to a boxing match where the losing boxer refuses to accept the decision.
- Criticizes Trump for not accepting the Supreme Court's decision and draws parallels to unfit behavior in the presidency.
- Points out that Trump's actions indicate a lack of understanding of the Constitution, integrity, and self-centeredness.
- Mentions the surprise of Trump's supporters at judicial rulings and clarifies the independence of the judiciary.
- Stresses the urgency for Trump to concede to prevent further damage to the country's credibility.
- Expresses eagerness for a resolution to the situation and to stop discussing Trump.

### Quotes

- "The President needs to concede and needs to remove himself from public service."
- "He further damages the credibility of the United States."
- "He doesn't know the rules."
- "Never should have stepped into the ring, so to speak."
- "I cannot wait to get to the point where I never have to type the phrase let's talk about Trump again."

### Oneliner

Beau outlines the timeline of Al Gore's concession speech, criticizes Trump's refusal to accept the Supreme Court's decision, and stresses the urgency for him to concede and step away from public service.

### Audience

Political activists and concerned citizens.

### On-the-ground actions from transcript

- Contact your representatives to urge them to push for Trump's concession and removal from public service (implied).
- Educate others on the importance of accepting democratic processes and decisions (implied).

### Whats missing in summary

The emotional weight and detailed analysis present in Beau's full video.

### Tags

#Timeline #Politics #ConcessionSpeech #Trump #Constitution #Judiciary


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about timelines.
We're going to go through a timeline day by day.
We're going to talk about what it means.
We're going to talk about contests, decisions, stuff like that.
And we'll see where it goes.
On December 11th, the case was argued before the Supreme Court.
On December 12th, the case was decided.
On December 13th, the concession speech was given.
Yeah, I'm not talking about this year.
I'm talking about 2000.
Al Gore.
Twenty years ago today, Al Gore gave his concession speech.
One day after the Supreme Court decided not in his favor.
The President of the United States still has not done that.
A lot of the people who are kind of cheering him on, if you took this same situation and
put it in any other context, they would be mocking him for his behavior.
He took it out of politics and made it a boxing match, right?
Two boxers go in, decision gets made.
And then the losing boxer whines and cries about it.
What do you think they'd say?
Never should have stepped in the ring.
Didn't deserve to be there.
Never should have laced up his gloves, especially if they got to pick a large percentage of
the judges.
That's not what's happening right now.
That's not what's going on.
The reason is they don't know the rules.
If this was sports, something that they truly care about, they know the rules.
They know how it's supposed to work.
And they would mock him for this.
But it's very much the same thing.
The President of the United States not accepting the decision of the Supreme Court is like
not accepting the decision in a boxing match.
Shows he never should have been there.
Shows he was unfit.
Never should have been there to begin with.
And I think it's also going to foreshadow a whole lot of stuff that we're going to
find out over the next few years about the Trump administration.
If he is willing to disregard the rules when it comes to something this public, imagine
what he did in private.
I have a feeling that a lot of the things he takes credit for, his accomplishments,
we're going to find out that those numbers might have been massaged a little bit.
He just said it over and over and over again, like he's doing now, until people believed
it.
This is not some rich kid, preparatory, student body election.
This is an election for the Presidency of the United States, the highest office in the
land.
A number of things have been made very clear by President Trump's actions as of late.
First, he does not understand the Constitution at all.
Second, he doesn't have the integrity to be in that office.
Third, he is more concerned about himself than anything else.
He never should have been there.
Never should have stepped into the ring, so to speak.
He doesn't know the rules.
A lot of people supporting him don't know the rules.
The one thing President Trump succeeded at was creating a whole lot of new people who
are engaged in politics for the first time, who care about it for the first time, and
they don't understand it.
They were surprised by these rulings when nobody else was.
They were surprised by judges he picked going against him.
Nobody else was.
It's not how it works.
The judiciary is independent.
That's actually why the appointments for a lifetime, they're beholden to nobody once
they get that position.
In this case, it worked.
Got to be honest, I didn't expect all of them to do the right thing.
That was a surprise.
But the fact that it was a dismal failure, that wasn't surprising at all.
The President of the United States needs to concede.
He has waited far too long.
Every day it goes on.
He further damages the credibility of the United States.
He further damages the country he said he was going to make great.
Doesn't seem like he was very successful in that.
It seems like the voters know that.
We should see a resolution to all of this pretty soon.
And I can't wait.
I'll be honest, I cannot wait to get to the point where I never have to type the phrase
let's talk about Trump again.
The President needs to concede and needs to remove himself from public service.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}