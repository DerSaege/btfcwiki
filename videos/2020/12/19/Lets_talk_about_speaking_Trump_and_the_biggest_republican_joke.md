---
title: Let's talk about speaking Trump and the biggest republican joke....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iwzwRCrHBxU) |
| Published | 2020/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing a series on speaking the "second language" of Trump and his supporters based on fear and ignorance.
- Explaining how fear and ignorance are key components of Republican talking points.
- Describing the common response of labeling anything left-leaning as communist without understanding the term.
- Pointing out that the fear of communism is based on imagery and misinformation rather than actual knowledge.
- Listing the common fears associated with communism: bread lines, rationing, propaganda, police presence, controlled press, cronyism, etc.
- Challenging the audience to name something from the list that did not occur under Donald Trump's administration.
- Illustrating how many of the fears associated with communism actually manifested during Trump's presidency.
- Emphasizing the manipulation of individuals through fear and misinformation to support authoritarian practices.
- Encouraging listeners to confront supporters with the reality of what has already taken place rather than focusing solely on Trump.
- Urging for a shift towards anti-authoritarianism rather than making the discourse about party politics.

### Quotes

- "Fear and ignorance are key components of Republican talking points."
- "Everything they fear about communism occurred under Donald Trump."
- "The truth isn't told, it's realized."
- "Make them anti-authoritarian."
- "The goal is not to turn them into progressives, but to make them anti-authoritarian."

### Oneliner

Beau delves into the manipulation through fear and ignorance in Republican talking points, challenging supporters to confront the reality of authoritarian practices under Trump to foster anti-authoritarianism.

### Audience

Political observers

### On-the-ground actions from transcript

- Challenge misconceptions about communism with facts and examples (implied)
- Encourage reflection on authoritarian practices and their implications (implied)
- Foster anti-authoritarian perspectives through open dialogues (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how fear and ignorance are manipulated in political discourse, urging listeners to confront these tactics to foster anti-authoritarian sentiments.

### Tags

#Politics #Republican #Fear #Ignorance #AntiAuthoritarianism


## Transcript
Well, howdy there internet people. It's Beau again. So today is going to be the first video
in that series where we're teaching you how to speak a second language. How to speak the language
of Trump and his supporters. And we're going to start off by
talking about one of the biggest jokes, the biggest comebacks, the biggest talking points
of the Republican Party. What makes a good Republican talking point? What are the two
components? Fear and ignorance. When I say that, understand I'm saying ignorant to the subject
matter. Not that the person can't learn, just that they haven't learned it yet. Okay.
If you are on an online forum of any sort and say anything that is even remotely left-leaning,
it doesn't even have to be leftist, what do you get called? Almost immediately,
communist. Then what happens if you ask them to define it? When they say communist, do they mean
an adherent to a philosophy that wants to achieve a stateless, classless, moneyless society? Of
course not. I don't even know what that means because they're ignorant to the subject matter.
And that ignorance creates the fear which allows them to be manipulated.
Okay. When you ask them to define it, you don't get a coherent definition. You get imagery. Why?
Because that's where it came from. The imagery that was used to scare them.
You're going to get a list of stuff, and I have done this enough to know what's on the list.
Bread lines, rationing, empty store shelves, propaganda in the schools.
This probably sounds familiar if you've ever done that. Armored vehicles and troops on the street.
The police are everywhere. You could get locked up for anything.
Don't even have to really have done anything wrong. The press is controlled.
Cronyism. Those party members at the top. Well, they get everything right.
We become allied with tyrants, become a laughing stock, and all of the wealth gets transferred up.
That's the list. When you talk to somebody about this, this is the list you're going to get.
You probably won't get all of them with each person, but everything you're going to get
is going to be on this list. Why is it that you're going to get all of them?
It's going to be on this list. Why is this not just a talking point and a comeback, but why is it the biggest joke?
Name one of these things that did not occur under Donald Trump. One.
I think we all remember the patriotic education program or whatever it was, the propaganda he was
trying to put in schools. The bread lines weren't just about bread, but I'm pretty sure we all
remember the giant line of cars waiting to pick up their rations, right? Empty store shelves,
armored vehicles on the streets because we have to dominate the streets. Controlled press.
Only listen to the news outlets that the premier, I'm sorry, president,
says are okay. Everything else is fake news. It is western propaganda.
Show me the person and I'll show you the crime, right? Get locked up for anything. Didn't even
have to do anything wrong. Lock her up. Lock her up. When you ask them for a specific charge,
they can't give you one. So they'll just blanketly say treason. And if you've watched this channel
long enough, you know treason's a very specific charge and it doesn't fit. We become a laughing
stock allied with tyrants. There are photos of our allies laughing at Trump. There are photos
of him saluting people that are generally referred to and seen as our opposition
and the transfer of wealth up to the party members. During this public health issue,
did the billionaires hurt or did they actually make more money? Billions more.
Those in the party did all right. They even got special authorization to get medical treatment
that those of us down here at the bottom can't get as we wait for our $600 ration.
Everything they fear about communism occurred under Donald Trump because they're not actually
afraid of communism. They don't know what it is. What this is is a list of stuff that tends to
occur under authoritarianism. Now if you do this and you ask somebody this and you start getting
the list, if it's in person, write it down, type it in your phone, whatever. And when they're done,
ask them how much of it hasn't already occurred. Don't say under Trump. You can't question dear
leader yet. And realistically, the truth isn't told, it's realized. You can give them the pieces,
but they have to come to that final realization on their own. This will help them do it.
If you can show them that because they were manipulated by a word that they really can't
define into supporting the very things they think that word represents, that might be enough
to overcome the cognitive dissonance that they have. They hold conflicting views. Double think.
They know what their principles are, but the party tells them to think otherwise. So they do.
This is an easy one. We will go through a whole bunch of them before this is over.
I'm not going to do an everyday thing with it, but we're going to go through a lot of this.
But just remember, when you're doing it, you can't make it about Trump.
Don't even really use the word Republican unless they make it partisan. If they say,
well, yeah, all this stuff happened, but it was the Democrats' fault, then you kind of have to
point out who actually had power. But other than that, just leave it at government,
because the goal is not to turn them into progressives, but to make them anti-authoritarian.
So they reject the next Republican candidate that uses that style of leadership, that poses a
threat to the United States, to the Constitution, to all of these things that they say they support.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}