---
title: Let's talk about leadership, mandates, Trump, and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w1pGDnmYxx4) |
| Published | 2020/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparing leadership styles of the outgoing Trump administration and incoming Biden administration.
- Biden planning to ask people to wear masks for the first hundred days, facing pushback.
- Emphasizing the use of numbers for certainty and drawing people in.
- Duration of mask-wearing until a safe and available vaccine is widely distributed.
- Lack of a public distribution plan despite assurances from the Trump administration.
- Mentioning a website that may indicate where individuals fall in the vaccine distribution line.
- Biden's approach of setting an example rather than mandating masks or vaccines.
- Encouraging and incentivizing vaccine uptake without mandating it.
- Acknowledging resistance but focusing on leading by example.
- Urging people not to travel during the upcoming holidays for safety.
- Stressing the importance of following health guidelines even if choosing to travel.
- Reminding to take precautions to protect vulnerable loved ones during the pandemic.

### Quotes

- "Biden is going to ask people to wear masks. He's not going to mandate it either."
- "Encourage it, incentivize it, do everything you can to get people to get the vaccine, but you can't really mandate it."
- "Do everything that you can, even if you're going to disregard that."
- "Take every precaution that you can. Mitigate in every way that you can."
- "If you mess this up, Granny June probably won't see spring."

### Oneliner

Beau compares leadership styles, advocates for mask-wearing without mandating, and stresses following health guidelines to protect loved ones during the ongoing pandemic.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Follow health guidelines strictly: wear masks, maintain distance, wash hands (implied)
- Encourage and support vaccine uptake without mandating it (suggested)

### Whats missing in summary

Importance of consistent adherence to health guidelines for overall community safety.

### Tags

#LeadershipStyles #MaskWearing #VaccineUptake #HealthGuidelines #COVID19 #PandemicAwareness


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
leadership styles and we're going to compare the outgoing Trump administration and the
incoming Biden administration. We're going to compare their leadership styles. Even though
Biden has not taken office yet, he has made it pretty clear how he plans to handle certain
things and we're going to focus on one in particular. Okay, so Biden has said that he's
going to ask people to wear masks for the first hundred days. He's already getting pushback on
that. Why a hundred days? That's an arbitrary number. How does he know it's going to be over
by then? He's just making that period of time up. Why? Why? Why? Because people like numbers.
You're right, it's an arbitrary number. There's absolutely no guarantee that it's only going to
be a hundred days. In fact, I can pretty much guarantee that it won't be for a whole lot of
people. But people like the certainty of numbers. Eight life hacks that will change your life.
That's why headlines are structured that way. People are drawn into the certainty of numbers.
And that's why he's doing it. How long is it really going to last? Until there is a safe,
effective, reliable, and available vaccine. Nothing changes. It's what we've been saying
the whole time. For a whole lot of people, hopefully, that will be a hundred days or less.
As far as we know, there actually is no distribution plan, despite assurances from
the Trump administration. There's been none that's been made public, and the Biden administration
says they haven't been briefed on one. If it's done correctly, the distribution plan should
take care of those people who are most at risk first. There's actually a website,
and I'll see if I can find it, put it down below, that will tell you where you're at in line,
if it's done properly, which would cover like health care workers and the elderly and those
most at risk first. If you are youngish and in pretty good health, it's going to be a while.
Judging by that website, I'll probably be wearing a mask until Thanksgiving of 2021.
Oh well. I mean, I guess I'll get to try out some new styles of masks. It's not a big deal.
Then the whole time under the Trump administration, they said they didn't have the authority to
mandate masks, and Biden is going to ask people to wear masks. He's not going to mandate it either.
Mandating, it's that ruler mentality again, rather than the leader mentality.
Biden is going to have to overcome a lot of resistance because the Trump administration
politicized this, turned it into a political thing rather than a health thing, but his hope
is that by setting the example and doing everything that he's supposed to and encouraging his staff
to do so, that people will do what's right and follow the advice of the docs. It makes sense.
Go back and look at the videos. It's what I said Trump should have been doing the whole time.
That's how you lead. You don't have to mandate. And then when it comes to mandating, he also said
he wasn't going to mandate the vaccine. That's also the right move. He's not going to mandate
the vaccine. And I know there's going to be a lot of resistance to that, but encourage it,
incentivize it, do everything you can to get people to get the vaccine, but you can't really
mandate it. If you're going to stay ideologically consistent, there aren't a whole lot of situations
in which most people are okay with the government saying, this is what you're going to do with your
adults. I think that's the right call. Aside from that, aside from the moral and ethical aspects of
it, on the practical side, there's already going to be resistance to it. If it is mandated,
it's going to be like a security clampdown. It's going to increase the resistance.
I think that's the right move. I do think that they should encourage it. And realistically,
my best guess is that administration officials will get the vaccine before anybody else
and they'll still be out there wearing masks, because they're going to try to set the example.
They're going to try to lead rather than rule. It's what his statements indicate. And I think
that's the right move. I think that's probably what this country needs more than anything right
now. Now, aside from all of that, I do want to point something out. The docs have spoken up.
We are not even to the Thanksgiving peak yet. It is going to get worse before it gets better.
To make it even worse, we're going to hit the Thanksgiving peak right around the time people
start traveling for the December holidays, which means it's going to get a lot worse before it
gets better. The docs are saying don't travel. It's probably good advice. They know what they're
talking about. It's also the United States and we know that people don't always listen to them.
I just want to point out that if you are going to disregard that piece of advice, it doesn't mean
you should disregard all of their advice. Wear a mask, stay distanced, wash your hands, do everything
that you can, even if you're going to disregard that. I think that might have been part of the
problem over Thanksgiving. The best advice is to do what the docs say. Don't travel.
If you just feel obligated, because, you know, it could be Granny June's last Christmas,
do everything that you can. Take every precaution that you can. Mitigate in every way that you can
and understand that if you mess this up, Granny June probably won't see spring.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}