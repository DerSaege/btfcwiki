---
title: Let's talk about Republicans leaving Trump and moving forward....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ELAry9HNn7s) |
| Published | 2020/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans had a sudden change of heart and started distancing themselves from Trump after the electors cast their votes.
- A viewer suggested updating a video from July 2019, detailing 14 characteristics, 10 stages, and five components of a specific form of government.
- Viewers are encouraged to watch the video, add their own examples, and acknowledge how close the country came to a different path.
- The reason Republicans are now jumping ship is because the 14th characteristic failed, not due to a change of heart.
- It's vital for the nation to acknowledge how close it came to a different outcome and to have a national dialogue about what happened.
- Those calling to move forward and put things behind us enabled and supported the previous administration's policies.
- If Trump had succeeded, those currently distancing themselves from him would still be supporting him.
- Moving forward, healing, and reuniting as a nation is possible but requires acknowledging the recent past.
- It's suggested that the nation engages in a critical national discourse to prevent similar events from happening again.
- While forgiveness is possible, it's vital not to forget how close the country was to a different path.

### Quotes

- "It wasn't a change of heart. It was a change of the situation."
- "We may forgive. But don't forget."
- "I think it's incredibly important that that conversation take place."

### Oneliner

Republicans distancing from Trump wasn't a change of heart but a response to failure, prompting a call for national reflection to prevent similar events.

### Audience

Viewers, Citizens

### On-the-ground actions from transcript

- Watch the suggested video, add your own examples, and contribute to the national discourse (suggested)
- Engage in critical national dialogues about recent events (suggested)

### Whats missing in summary

The full transcript provides a detailed insight into the recent shifts in Republican attitudes towards Trump and calls for a national reflection on the recent past to prevent similar events in the future.

### Tags

#Republicans #Trump #NationalReflection #PoliticalChange #NationalDiscourse


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about all of these Republicans
having a change of heart all of a sudden,
jump and ship on Trump.
And a message from one of you, and a homework assignment.
So it seems like as soon as the electors cast their vote,
Republicans, well, they all saw the light,
all started talking about turning the page,
moving forward, putting all this unpleasantness behind us,
reuniting, and healing the nation.
Man, that sounds good.
Really does.
Sounds good.
But see, one of y'all sent me a message last night.
And it was a link to one of my videos made in July of 2019.
I'm going to put that link down below.
She was asking for it to be updated, new examples.
I think that's a great idea.
The video details 14 characteristics, 10 stages,
and five components.
The 14 characteristics are of a specific form of government.
I beg you to watch that video and do what she asks.
Add your own examples.
Year and a half later, add your own examples in the comments.
Kind of flesh the video out, update it.
And as you do it, acknowledge how close we came.
And then acknowledge the only reason they are jumping ship
is because the 14th characteristic failed.
They lost.
Had they succeeded?
Had Trump pulled it off?
They'd still be supporting him.
We'd still be on that path.
And then think about the next stages.
Yeah, we can move forward.
Absolutely.
We can heal.
We can reunite.
And we will.
But I would suggest that it is incredibly important
that we as a nation acknowledge how close we came,
how close we were.
And those who are calling to put all this behind us,
I understand it because in many cases, they enabled it.
They helped him.
They promoted the policies.
And they would have been there had that 14th characteristic
succeeded.
It wasn't a change of heart.
It was a change of the situation.
We have to acknowledge that.
And we have to have some national dialogue about what
happened and acknowledge that it can, in fact, happen here
because it almost did.
I think it's incredibly important
that that conversation take place
and that we don't forget.
Don't forget.
We may forgive.
But don't forget because I think we
were way closer than most people want to acknowledge.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}