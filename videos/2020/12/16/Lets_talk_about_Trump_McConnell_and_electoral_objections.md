---
title: Let's talk about Trump, McConnell, and electoral objections....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eJGwmqHWmiw) |
| Published | 2020/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator McConnell is reaching out to senators, advising them not to object to Trump’s actions, implying it will be voted down.
- McConnell is likely doing this to distance himself from the president and clear himself of enabling Trump for the past four years.
- McConnell is setting up a potential showdown between himself and Trump, trying to gain control of the Republican Party.
- Most Americans view objections to the election results as undermining the process and un-American.
- McConnell wants actual control and loyalty within the party, setting up a choice for Congress members between him and Trump.
- Even though Trump is leaving office, he still holds influence through social media and his base.
- Republicans in Congress are in a tough spot – objecting will likely lead to losing, making them look silly, while not objecting may anger Trump.
- The decision-making process for Republicans is more about power and control within the party than what's best for the country.
- If there are objections within the party, it will lead to a fractured Republican Party lacking unity.
- If everyone falls in line behind McConnell, Trump’s influence may diminish, potentially disillusioning his base from supporting the party.

### Quotes

- "This is about power, as is often the case up on Capitol Hill."
- "If there are objections, if there are people that side with those objections, the Republican Party is going to be fractured for a while."
- "They're going to weigh those two options. I note that nowhere in it does what's best for the country factor into it."
- "It's a bold strategy. Let's see if it pays off for him."
- "Anyway, it's just a thought. I have a good night."

### Oneliner

Senator McConnell seeks control of the Republican Party by setting up a choice between siding with him or Trump, potentially fracturing the party.

### Audience

Political observers, Republican Party members

### On-the-ground actions from transcript

- Support politicians who prioritize country over party loyalty (implied)
- Stay informed about the actions and decisions of politicians within your party (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of McConnell's strategy and the potential consequences for the Republican Party.


## Transcript
Well howdy there internet people it's Beau again.
So today we are going to talk about Trump McConnell objections, where the Republican
Party is headed, and loyalty.
It is being reported that Senator McConnell has reached out to other senators and told
them not to object and if they do it's going to be voted down.
Cool okay.
Why is he doing that?
Obviously there's self interest.
You know McConnell is wanting to distance himself from the president and the president's
actions.
That makes sense.
He wants to hopefully clear himself a little bit of enabling Trump for the last four years.
That makes sense.
But he's setting up a McConnell versus Trump showdown.
Now the conventional wisdom is that McConnell is doing this because he has to defend the
Senate and he knows that most Americans see these objections as jokes, as something that
is undermining the process, something that is kind of un-American.
I mean because they are.
So there's that.
But see I think there's something else.
McConnell is trying to get control of the Republican Party.
Now he's going to have official control with Trump leaving office.
He's going to be the party leader.
But he needs actual control.
He needs that loyalty.
So he's setting it up to where people in Congress, both House and Senate, have to make a choice.
Are they going to side with McConnell or are they going to side with Trump?
Is the Republican Party going to be the party of McConnell or the party of Trump?
Because even though Trump's leaving office and he's not going to have any power officially,
well he's still got social media.
He still has that fired up base.
And this puts Republicans in the House and the Senate in a pretty uncomfortable position.
Because if they object, McConnell's probably not going to be happy.
They also know the objection is going to lose because not just is it pretty much certain
to lose in the House, it's probably going to lose in the Senate too.
So they're going to look silly.
They're going to expend political capital and a lot of them don't really have much.
On the other hand, Trump values loyalty above all else.
And if they don't object or they don't side with it, well he might tweet about them.
I would point out that this is the realistic breakdown of the decision-making process.
That's probably how the decision is going to be made.
They're going to weigh those two options.
I would note that nowhere in it does what's best for the country factor into it.
This is about power, as is often the case up on Capitol Hill.
This is about determining who controls that party.
And we're going to get to find out if there are objections, if there are people that side
with those objections.
The Republican Party is going to be, well, they're going to be fractured for a while.
They're not going to have the unity they need to do anything.
And if there are no objections, if everybody falls in line behind McConnell, Trump becomes
a laughingstock and his base becomes, well, disillusioned.
And they may not continue to support the Republican Party.
It is an interesting turn of events.
It's a bold strategy.
Let's see if it pays off for him.
Anyway, it's just a thought.
I have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}