---
title: Let's talk about for whom the bell tolls and an update....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8yz0SuYWHek) |
| Published | 2020/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides historical context by comparing the number of combat losses during World War II to the tolling of a bell at the National Cathedral.
- Updates on the current public health struggles, mentioning the hope for wide vaccine availability by March/April.
- Acknowledges the impressive logistics required for mass vaccine distribution.
- Mentions personal skepticism about vaccine availability due to Governor DeSantis's involvement in the rollout.
- Emphasizes the importance of continuing to wear masks and practice social distancing even after vaccination.
- Explains that while the vaccine is effective at preventing serious illness, its impact on transmission to others is still unclear.
- States the need to continue preventive measures until more information is available or a large portion of Americans are vaccinated.
- Warns macho individuals about potential long-term health effects, urging them to take precautions seriously.
- Encourages basic protective measures like hand washing, avoiding face touching, and staying home when possible.
- Expresses optimism about seeing the light at the end of the tunnel due to the imminent availability of a vaccine.

### Quotes

- "Our current public health struggles now are larger in scope of loss than World War II."
- "You know you're big bad and brave and all I know you don't care about the worst possible outcome."
- "We are nearing the end of this. For the first time, I can actually see a light at the end of the tunnel."

### Oneliner

Beau provides updates on vaccine availability, urges continued precautions post-vaccination, and expresses optimism about the pandemic's end.

### Audience

Public Health Advocates

### On-the-ground actions from transcript

- Wash your hands, avoid touching your face, and wear a mask when going out (implied)
- Stay updated on vaccine availability and distribution in your area (implied)

### Whats missing in summary

Beau's tone and delivery, which can provide additional context and emotion to the message. 

### Tags

#COVID19 #VaccineAvailability #PublicHealth #Precautions #Optimism


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about for whom the bell tolls.
Before we do that, I'm going to give you some context, a
little bit of historical context.
Because sometimes numbers get so big, it's hard to really
get your mind around.
During World War II, the number of combat lost was
291,557.
The National Cathedral just told its bell 300 times.
One ring of the bell to represent 1,000 people lost.
Our current public health struggles now are larger in scope of loss than World War II.
So we're going to do a little update on it.
First, the good news. The good doc is saying that he hopes the vaccine is going to be widely
available by the end of March, beginning of April. That would be amazing. The logistics
involved with actually pulling that off are huge. It'd be really impressive. I recently
said I probably wouldn't be able to get one until Thanksgiving of next year, and that
That may still be true for me because Governor DeSantis is in charge of our rollout and yeah.
But y'all should be alright.
Y'all should be able to get one pretty soon.
So that's the good news.
That being said, you still have to wear a mask socially distanced for a little bit.
Why?
Because what the studies show, as far as the vaccine goes, is that it's very effective
stopping you from becoming seriously ill or even showing symptoms. There's not
really a whole lot of information about whether you can get it and give it to
somebody else even after the vaccine. So does this mean that we're gonna have to
wear masks and socially distance forever? No. We have to do it until either that
information becomes available or about 230 million Americans get it because by
that point it's gonna be really hard for it to transmit too much. So that's where
we're at there. Now for all you macho guys I have some news that may change
your opinion about wearing a mask. You know you're big bad and brave and all I
know you don't care about the worst possible outcome. However, if you get it
and make it, there's some long-term considerations you might want to
consider. There are now studies suggesting that, well, one piece of anatomy that you
truly do care about may not work so well. Believe you in bad standing. Wouldn't be
too hard to understand. Follow? So wash your hands. Don't touch your face. Stay at
home. If you have to go somewhere, wear a mask. We are nearing the end of this. For
the first time, I can actually see a light at the end of the tunnel. I said from
the beginning this is going to go on until we get a safe, reliable, effective,
available vaccine. And that appears to just be a few months away so hang in
there, don't let up now. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}