---
title: Let's talk about Trump's 2020 coming to a close and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GC20xZq2wR8) |
| Published | 2020/12/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 2020 is nearing its end, and many people are relieved, blaming the year for various issues.
- The current state of affairs is the result of not just this year, but a culmination of four years and more.
- The administration has brought existing issues to light through incompetence, leading to a desire for deep systemic change among viewers.
- 2021 is seen as the year to initiate real change, not simply because of a new year or administration, but due to a populace primed for transformation.
- Political engagement is at an all-time high, and individuals must capitalize on this for systemic change.
- Change will come from individual action, organizing at the local level, and demanding necessary changes.
- Those in the middle and upper classes may begin to understand systemic issues as economic challenges impact them.
- The delay in relief and stimulus efforts will affect the economy, potentially leading to a shift in perceptions among the more affluent.
- Biden’s administration is viewed as status quo, requiring individuals to drive real change towards a collective goal of systemic transformation.
- Organization at a grassroots level is emphasized, as top-down approaches are seen as likely influenced by existing systems.
- Real change requires individual involvement focused on specific issues, avoiding the tendency of political figures to play it safe.
- Society can progress by collectively working towards what is most vital to each individual, without impeding each other's efforts.
- The past years have hindered progress, and it is now up to individuals to guide society forward by recognizing and addressing existing problems.

### Quotes

- "2020 is coming to a close, but the real work doesn't start until next year."
- "The most effective people at social change had a very narrow focus and they changed it."
- "It's going to take you as an individual."
- "Real change requires individual action."
- "Society can progress by collectively working towards what is most vital to each individual."

### Oneliner

2020 is ending, but true change begins in 2021 through individual action towards systemic transformation.

### Audience

Activists, community organizers

### On-the-ground actions from transcript

- Organize your community at the local level towards systemic change (implied)
- Demand necessary changes in the system (implied)
- Get involved in social change efforts with a narrow focus (implied)
- Avoid stepping on each other's toes while working towards collective progress (implied)

### Whats missing in summary

The full transcript provides additional context on the impact of the administration, the need for grassroots organization, and the role of individuals in effecting systemic change.

### Tags

#SystemicChange #GrassrootsOrganization #PoliticalEngagement #CommunityAction #IndividualEmpowerment


## Transcript
Well howdy there internet people, it's Beau again.
So 2020 is almost over, just a few days left.
And that's how a lot of people feel about it.
They feel as though it is this year
that
led to
all of this.
That's not really the case. This is the culmination
of
four years specifically
but many more
in the run-up.
This administration
highlighted
a whole bunch of existing issues in this country
through its incompetence and ineptitude.
The one thing
that unites people
watching this channel
is that they want
deep systemic change of some sort.
A whole bunch of different causes, a whole bunch of different reasons,
but that's the one thing
that unites everybody
watching this.
2021
is going to be the year to start,
to really start in earnest.
Not because it's a new year,
not because
of the Biden administration,
but because the populace is primed. They're ready.
They want change too.
They look at the government and they see how the government failed them.
They look at the economic system and they see how the economic system failed them.
And everything that has happened
from
going back to the very beginning of the Trump administration
highlighted it,
brought it all to the surface. It wasn't that these things didn't exist before,
it's that
he was incapable of maintaining the facade
that kept most people complacent.
That has changed.
There are more people politically engaged right now
than
at any point in my lifetime.
We have to be ready to capitalize on that
because if you want deep systemic change
it is going to come
from you.
It is going to come
from individual action
on your part.
It's going to come from organizing your community at the local level.
It's going to come from
demanding
that changes be made.
And when
the government isn't responsive to some of those demands,
changing it yourself,
filling in the gap.
Right now
it may seem daunting because there's still a large portion of the population that doesn't get it.
The thing is
that portion of the population is typically
middle and upper middle class.
The reason they don't get it, they don't see it,
is because it hasn't reached them yet.
However,
I would point out that the economic downturn started before
the public health issue.
And Trump's latest temper tantrum
and the
slowing, halting, delaying, whatever you want to call it,
of relief and stimulus
is going to place
those in the middle and upper classes
on uneasy footing.
They're going to start to see it as well.
They're going to start to feel it.
Whereas right now
their main concern
has been
masks, lockdowns, not being able to go out and eat.
Basically their main concern has been
that they had to live like
everybody else.
Because a lot of this stuff
your average person never got to do much anyway.
But they did.
That has upset them.
When the economic system
slows
as it probably will,
they're going to feel it.
This delay by the Trump administration,
it's going to have a lag.
Even if he signs it today or tomorrow,
which I don't see as likely,
it's going to cause a lag.
And there's going to be a period
where there's less money moving around.
That's going to impact them even more.
We're at the point
where we can start moving forward,
where we can start making those changes.
We can't rely on Biden.
We can't rely on the incoming Democratic administration.
They're not going to do it.
They are status quo.
They want to go back to the period
where the facade was up.
That's really their goal
because they're not feeling it at all.
It is going to take individual action
towards a collective goal.
And that collective goal
is that deep systemic change.
The fundraiser,
a whole bunch of people coming together,
each doing whatever they could,
created a major change.
It's the same thing.
It's decentralized.
It's sporadic.
But it is organized.
And that's the part that we have been missing
for so long is organization.
It's not going to be a top-down organization.
Not to achieve this
because anything that is top-down
is probably going to be operating
at the behest of the system
because they're already at the top.
If you want real change,
you have to do it.
You have to get involved.
And now is the time
because there is so much discontent,
because there is such a spotlight
on all of the inequities in this country.
Now is the time to make the change.
And it's going to take you as an individual.
We can't rely on political figures.
Political figures tend to play it safe.
Biden is going to play it safe.
It's what he's going to do
because he wants broad appeal.
The most effective people at social change
didn't do that.
They had a very narrow focus
and they focused on that one issue
and they changed it.
There's a whole bunch of different priorities
out there with you all.
It's going to take you acting
on the grounds and in pursuit
of what is most important to you.
And if we all do it
and try not to step on each other's toes
as much as possible,
we can achieve something.
We can move society forward
because we have been drug back for years.
For the last four years specifically,
but even before then,
we were held back.
The progress wasn't being made.
There are enough people
who have recognized the inherent problems
in our current government,
economic system and societal structure
that are ready to move forward.
It's going to be up to us
to help guide that
and to help move it along.
So while you may be relieved
that 2020 is coming to a close,
the real work doesn't start until next year.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}