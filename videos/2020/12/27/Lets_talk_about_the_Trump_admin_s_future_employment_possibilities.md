---
title: Let's talk about the Trump admin's future employment possibilities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RozukxGTiuw) |
| Published | 2020/12/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the employment opportunities and roadblocks for former Trump administration officials.
- Points out the tone-deafness of generating sympathy for individuals who enabled Trump while others are in need of relief.
- Notes that some officials joined the administration late and still took the job despite knowing what Trump was doing.
- Questions the judgment of those who enabled Trump's actions undermining the country's fabric and principles.
- Suggests that public service might not be the true calling for these officials and that they were driven by self-service.
- Mentions a personal anecdote about consequences for poor judgment and actions.
- States a lack of sympathy towards anyone in the Trump administration facing future employment problems.
- Believes that standard image rehabilitation post-administration won't be possible due to wholesale rejection by the people.

### Quotes

- "I do not believe that any of them that came in late in the administration believed that President Trump was a net good for the country."
- "Those in the beginning I can give the benefit of the doubt to some and say hey maybe they really did believe they were going to be the adult in the room."
- "I have a friend when he was 18 he got caught with just enough of a substance to make it a felony."
- "As much as I might want to try, I do not know that I will be able to find any sympathy for anybody in the Trump administration and their future employment problems."
- "This is not a normal administration and I do not believe that the standard image rehabilitation that normally occurs after an administration is going to be possible because people are going to reject it wholesale as they should."

### Oneliner

Former Trump administration officials face employment roadblocks due to their actions undermining the country, leading to a lack of sympathy and rejection of standard image rehabilitation.

### Audience

Activists, Politically Engaged Individuals

### On-the-ground actions from transcript

- Reject any attempts at normalizing or rehabilitating the image of former Trump administration officials (implied).

### Whats missing in summary

The emotional weight and personal anecdotes shared by Beau in the transcript. 

### Tags

#TrumpAdministration #EmploymentOpportunities #Sympathy #ImageRehabilitation #Consequences


## Transcript
Well howdy there internet people it's Beau again. So today we are going to talk about a whole new
genre of article that has appeared and employment opportunities and roadblocks
to those employment opportunities for former Trump administration officials.
I have seen a sheaf of articles bemoaning the possible future roadblocks for those who enable Trump.
The thing about this is first I would suggest that it is incredibly tone deaf
to try to generate sympathy among the populace for a group of people who enable Trump while a
large segment of the population is waiting on him to get some form of relief. That seems off.
Aside from that I want to point out that many of those currently in the administration joined the
administration late. They weren't brought into their positions on day one. They saw
what he was doing for years and still took the job. They still took the job. I do not believe
that any of them that came in late in the administration believed that President Trump
was a net good for the country. They saw what he was doing. Those in the beginning I can give the
benefit of the doubt to some and say hey maybe they really did believe they were going to be
the adult in the room. I would say they did a poor job of it but I don't know how difficult that task
is. Those who came in late I don't have any sympathy for. They watched as he undermined the
moral and societal fabric of this country. They watched as he undermined our foreign policy. They
watched as he undermined public health officials. They watched as he undermined the election. They
watched as he undermined the founding principles of this country and they went to work for him.
I'm going to suggest that that's poor judgment and that maybe public service isn't really calling
them. Maybe self-service is and they believed he was going to get a second term and they thought
it would be good for them and now they're having second doubts. I have a friend when he was 18
he got caught with just enough of a substance to make it a felony. He is now 40 and that has
haunted him the rest of his life and if you were to ask those people who are being mentioned in
these articles, those who these articles are hoping to garner sympathy for, they would say
that it was poor judgment and that actions have consequences. As much as I might want to try,
I do not know that I will be able to find any sympathy for anybody in the Trump administration
and their future employment problems. That doesn't seem like something that's going to
be weighing on my mind regardless of how many articles are written about it.
This is not a normal administration and I do not believe that the standard image rehabilitation
that normally occurs after an administration is going to be possible because people are going
to reject it wholesale as they should. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}