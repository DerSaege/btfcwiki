# All videos from December, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-12-30: Let's talk about how to change the system.... (<a href="https://youtube.com/watch?v=aDXsa9h2cN8">watch</a> || <a href="/videos/2020/12/30/Lets_talk_about_how_to_change_the_system">transcript &amp; editable summary</a>)

Acts of kindness in challenging the system by fostering community support over reliance on failing institutions.

</summary>

"Every one of these stories is an indictment of the system."
"Doing what you can, when you can, where you can, for as long as you can, is a revolutionary act."
"People actively undermining the system that is holding so many down."

### AI summary (High error rate! Edit errors on video page)

Social media post prompts reflection on the broken system when someone asks for help with groceries.
People offering aid to strangers in need on social media is a common occurrence.
The generosity displayed in these acts of kindness is not uplifting but rather a stark reminder of systemic failures.
Acts of kindness expose the brokenness of society and the government's lack of basic care for its citizens.
Individuals helping each other challenges the system's teachings of self-preservation over community support.
Over time, consistent acts of aid can lead to the institutionalization of community support systems.
Relying on community aid erodes the authority and power of the current government system.
Encourages building a new system based on voluntary cooperation and support rather than reliance on existing institutions.
Helping others in a grassroots, community-driven manner is a revolutionary act that can lead to a shift in societal values.
A system built on cooperation and mutual support will be more effective and uplifting for all.

Actions:

for community members, activists, allies,
Support community aid initiatives by participating actively and consistently (exemplified)
Build networks for mutual aid and support within your neighborhood (suggested)
</details>
<details>
<summary>
2020-12-30: Let's talk about a foreign policy joke.... (<a href="https://youtube.com/watch?v=cmTSRl5N6Iw">watch</a> || <a href="/videos/2020/12/30/Lets_talk_about_a_foreign_policy_joke">transcript &amp; editable summary</a>)

Beau criticizes foreign policy, supports gender studies funding in patriarchal societies, and urges smashing the patriarchy to undermine opposition forces and support troops effectively.

</summary>

"If you want to remain a red-blooded American who supports troops, it's time to smash the patriarchy."
"These educational programs have been run by the US government for 70 years. They are incredibly effective."
"You can have a cute joke for the internet and be all macho, or you can support the troops a little bit more."

### AI summary (High error rate! Edit errors on video page)

Criticizes foreign policy initiative on gender studies.
Supports spending American tax dollars on gender studies in Pakistan, Sudan, Saudi Arabia.
Believes US government wouldn't introduce gender equality ideas in Saudi Arabia to maintain status quo.
Explains how patriarchal societies in certain countries maintain power and support.
States introducing gender equality ideas could limit opposition forces on the battlefield.
Compares gender studies in cultural wars vs. real wars, where education is weaponized.
Urges to smash the patriarchy for the safety and effectiveness of these educational programs.
Mentions these programs have been running effectively for 70 years, similar to building irrigation ditches for farmers.
Emphasizes that undermining the opposition is the main goal, with women's empowerment being a bonus.
Calls for supporting troops beyond symbolic gestures like yellow ribbon bumper stickers.

Actions:

for policymakers, activists, citizens,
Support educational programs in patriarchal societies (implied)
Smash the patriarchy through advocacy and action (implied)
Support initiatives that empower women globally (implied)
</details>
<details>
<summary>
2020-12-29: Let's talk about the media running dry and explain a term.... (<a href="https://youtube.com/watch?v=5Hw3SrCHxmw">watch</a> || <a href="/videos/2020/12/29/Lets_talk_about_the_media_running_dry_and_explain_a_term">transcript &amp; editable summary</a>)

Beau debunks media sensationalism by explaining the truth behind "dry runs," urging viewers to resist fear manipulation through knowledge.

</summary>

"If it's a dry run, it doesn't make the news."
"No dry run will ever be dramatic. It's designed to not be."
"This gets used to manipulate people through fear."
"Hopefully, now that you know what a dry run is, this won't work on you anymore."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Media uses a term to keep viewers glued to the screen, letting imagination run wild.
Talks about the bystander effect and how knowing about it can prevent succumbing to it.
Explains the concept of "dry runs" in gauging security posture and rehearsal.
Emphasizes that a dry run is not meant to be dramatic and won't make the news.
Describes a dry run scenario of parking a vehicle to test locals' reactions without causing harm.
States that a dry run will never end in a catastrophic event like a giant fireball.
Mentions that destroying a street during a dry run makes observations irrelevant.
Criticizes the manipulation of people through fear using the concept of dry runs inaccurately.
Encourages viewers to be informed about what a dry run truly is to avoid being manipulated by fear tactics.
Concludes by suggesting that understanding the concept can prevent anxiety and skepticism towards sensationalized media.

Actions:

for media consumers,
Educate others on the concept of "dry runs" to prevent fear manipulation (suggested).
</details>
<details>
<summary>
2020-12-29: Let's talk about the $2000 stimulus, the veto, McConnell, Bernie, and Trump.... (<a href="https://youtube.com/watch?v=wVzu2QtvxrQ">watch</a> || <a href="/videos/2020/12/29/Lets_talk_about_the_2000_stimulus_the_veto_McConnell_Bernie_and_Trump">transcript &amp; editable summary</a>)

Beau explains the Capitol Hill dynamics through the Trump-McConnell power struggle, where decisions are about consolidating power, not current issues.

</summary>

"It's about consolidating power under McConnell or allowing Trump to keep power after he leaves office."
"It really is about who gets to be president after he leaves office."
"Doesn't have to do with the defense budget."

### AI summary (High error rate! Edit errors on video page)

Explains the Capitol Hill situation through the lens of the Trump-McConnell power struggle.
House passed the veto override for the defense budget and a separate measure for $2,000 stimulus checks; now goes to the Senate.
McConnell's best outcome is for the veto override to pass and the stimulus checks to fail, breaking with Trump.
Bernie Sanders and Rand Paul are standing in McConnell's way, each for different reasons.
Bernie wants a vote on stimulus checks, while Rand wants to make override look challenging to Trump.
Political maneuvering is happening, making the outcome uncertain.
Decisions are more about consolidating power under McConnell or allowing Trump to maintain power post-office.
It's all about who gets to be president and party leader for the Republicans, not the current issues at hand.
The maneuvering doesn't revolve around the defense budget.
Playing out amidst Georgia's runoff elections and the electoral vote on the 6th.

Actions:

for political analysts, voters,
Stay informed about political power struggles and motives (suggested)
Follow updates on the Capitol Hill situation and Georgia runoff elections (suggested)
</details>
<details>
<summary>
2020-12-28: Let's talk about Trump losing influence and the relief bill.... (<a href="https://youtube.com/watch?v=iZm-5ZKcaZc">watch</a> || <a href="/videos/2020/12/28/Lets_talk_about_Trump_losing_influence_and_the_relief_bill">transcript &amp; editable summary</a>)

President Trump signed the relief bill earlier than expected, but the delay may cause a gap in payments and hinder economic recovery, reflecting political power struggles over genuine concern for the people.

</summary>

"This is all about political power and seeing who can get the most after the transition."
"Just be aware that it's coming and anything you can do to lessen its impact on you."
"Trump's actions were more about political power than genuine concern for the people."

### AI summary (High error rate! Edit errors on video page)

President Trump signed the relief bill earlier than expected.
The bill being signed means that payments will restart.
Due to the delay in signing, there may be a gap in unemployment payments.
Americans relying on these payments should prepare for a possible missed payment.
Senator McConnell seems to be winning the internal power struggle within the Republican Party.
Trump's involvement in the Georgia elections will tie him to the outcome.
The electoral votes will reveal who has won the power struggle.
McConnell is predicted to emerge victorious in this struggle.
The political calculations are focused on power and influence rather than the American people.
Trump's actions were more about political power than genuine concern for the people.
The delay in payments may impact economic recovery as less money will be spent.

Actions:

for american citizens,
Prepare for a possible delay in unemployment payments (implied)
Be aware of the impact and find ways to lessen it (implied)
</details>
<details>
<summary>
2020-12-28: Let's talk about Biden and immigration.... (<a href="https://youtube.com/watch?v=L-FDEGCSiAc">watch</a> || <a href="/videos/2020/12/28/Lets_talk_about_Biden_and_immigration">transcript &amp; editable summary</a>)

Biden's immigration timeline faces scrutiny as urgency for immediate action to save lives becomes paramount amid policy shifts and demands for systemic change.

</summary>

"The reason there's going to be a surge is because their lives are at risk. They can't wait six months."
"People's lives are at stake."
"Most of this is pen stroke stuff."
"American people demand deep systemic change."
"This is something that needs to be addressed immediately."

### AI summary (High error rate! Edit errors on video page)

Biden's campaign aimed to solve immigration issues on day one, but a realistic timeline is around six months.
Setting up fair asylum judges is understandable due to political constraints, but other aspects remain unclear.
Most immigration changes can be achieved with a pen stroke, allowing the Biden administration early wins to fulfill promises.
Anticipating a surge of migrants due to Trump administration policies negatively impacting many lives.
Urgency is vital as people's lives are at risk, and delays could be detrimental.
American people demand deep systemic change, and immigration policy is a critical issue.
Failure to address key issues like climate change and healthcare overhaul may prompt the formation of a left party.
Immigration judges are acknowledged as a challenging but necessary step, while other changes seem easily achievable.
Immediate action is imperative as people's lives hang in the balance amid immigration policy shifts.
Addressing treatment of individuals requires swift action, especially in undoing Trump-era policies.

Actions:

for policy advocates,
Advocate for immediate action on immigration policy changes to save lives (implied).
Support movements demanding deep systemic change in immigration and other critical issues (implied).
</details>
<details>
<summary>
2020-12-27: Let's talk about the Trump admin's future employment possibilities.... (<a href="https://youtube.com/watch?v=RozukxGTiuw">watch</a> || <a href="/videos/2020/12/27/Lets_talk_about_the_Trump_admin_s_future_employment_possibilities">transcript &amp; editable summary</a>)

Former Trump administration officials face employment roadblocks due to their actions undermining the country, leading to a lack of sympathy and rejection of standard image rehabilitation.

</summary>

"I do not believe that any of them that came in late in the administration believed that President Trump was a net good for the country."
"Those in the beginning I can give the benefit of the doubt to some and say hey maybe they really did believe they were going to be the adult in the room."
"I have a friend when he was 18 he got caught with just enough of a substance to make it a felony."
"As much as I might want to try, I do not know that I will be able to find any sympathy for anybody in the Trump administration and their future employment problems."
"This is not a normal administration and I do not believe that the standard image rehabilitation that normally occurs after an administration is going to be possible because people are going to reject it wholesale as they should."

### AI summary (High error rate! Edit errors on video page)

Addresses the employment opportunities and roadblocks for former Trump administration officials.
Points out the tone-deafness of generating sympathy for individuals who enabled Trump while others are in need of relief.
Notes that some officials joined the administration late and still took the job despite knowing what Trump was doing.
Questions the judgment of those who enabled Trump's actions undermining the country's fabric and principles.
Suggests that public service might not be the true calling for these officials and that they were driven by self-service.
Mentions a personal anecdote about consequences for poor judgment and actions.
States a lack of sympathy towards anyone in the Trump administration facing future employment problems.
Believes that standard image rehabilitation post-administration won't be possible due to wholesale rejection by the people.

Actions:

for activists, politically engaged individuals,
Reject any attempts at normalizing or rehabilitating the image of former Trump administration officials (implied).
</details>
<details>
<summary>
2020-12-27: Let's talk about Trump's 2020 coming to a close and what's next.... (<a href="https://youtube.com/watch?v=GC20xZq2wR8">watch</a> || <a href="/videos/2020/12/27/Lets_talk_about_Trump_s_2020_coming_to_a_close_and_what_s_next">transcript &amp; editable summary</a>)

2020 is ending, but true change begins in 2021 through individual action towards systemic transformation.

</summary>

"2020 is coming to a close, but the real work doesn't start until next year."
"The most effective people at social change had a very narrow focus and they changed it."
"It's going to take you as an individual."
"Real change requires individual action."
"Society can progress by collectively working towards what is most vital to each individual."

### AI summary (High error rate! Edit errors on video page)

2020 is nearing its end, and many people are relieved, blaming the year for various issues.
The current state of affairs is the result of not just this year, but a culmination of four years and more.
The administration has brought existing issues to light through incompetence, leading to a desire for deep systemic change among viewers.
2021 is seen as the year to initiate real change, not simply because of a new year or administration, but due to a populace primed for transformation.
Political engagement is at an all-time high, and individuals must capitalize on this for systemic change.
Change will come from individual action, organizing at the local level, and demanding necessary changes.
Those in the middle and upper classes may begin to understand systemic issues as economic challenges impact them.
The delay in relief and stimulus efforts will affect the economy, potentially leading to a shift in perceptions among the more affluent.
Biden’s administration is viewed as status quo, requiring individuals to drive real change towards a collective goal of systemic transformation.
Organization at a grassroots level is emphasized, as top-down approaches are seen as likely influenced by existing systems.
Real change requires individual involvement focused on specific issues, avoiding the tendency of political figures to play it safe.
Society can progress by collectively working towards what is most vital to each individual, without impeding each other's efforts.
The past years have hindered progress, and it is now up to individuals to guide society forward by recognizing and addressing existing problems.

Actions:

for activists, community organizers,
Organize your community at the local level towards systemic change (implied)
Demand necessary changes in the system (implied)
Get involved in social change efforts with a narrow focus (implied)
Avoid stepping on each other's toes while working towards collective progress (implied)
</details>
<details>
<summary>
2020-12-26: Let's talk about Tennessee, investigations, and intent.... (<a href="https://youtube.com/watch?v=qQfwskZ02Y4">watch</a> || <a href="/videos/2020/12/26/Lets_talk_about_Tennessee_investigations_and_intent">transcript &amp; editable summary</a>)

Beau addresses speculation about intent in the Tennessee incident, stressing the importance of following investigative steps to uncover the truth and avoid premature conclusions.

</summary>

"The intent is the holy grail. That's what you find at the end."
"It's really hard to do something like this in the United States and get away with it."
"Keep an open mind on this until more information becomes available."

### AI summary (High error rate! Edit errors on video page)

Addressing questions about Tennessee, Beau jumps right into discussing what is known and a mistake people are making.
Beau cautions against speculating about intent in situations like these, as there are often unintended consequences.
He mentions that disrupting communications may not have been the actual goal and could have been an unintended consequence.
Beau points out that without knowing the who, speculation on intent is premature.
The method of delivery, technical expertise, and other factors can help identify the culprits and lead to understanding the why.
Beau notes that despite the magnitude of the incident, technical expertise appears low based on available information.
He suggests that the lack of a claim of responsibility indicates a blend of personal grievance and politics rather than solely political motives.
The importance of following investigative steps to uncover intent is emphasized by Beau.
Beau mentions the next steps for investigators, including tracing the origin of the vehicle to potentially identify the perpetrators.
Keeping an open mind until more information is available is advised by Beau to prevent premature conclusions and wild claims.

Actions:

for investigators, analysts, concerned citizens,
Analyze available information to contribute to understanding the incident (implied).
Keep an open mind and await further information before forming conclusions (implied).
</details>
<details>
<summary>
2020-12-26: Let's talk about Pence, analogies, and the working class.... (<a href="https://youtube.com/watch?v=vxSoddoxw7M">watch</a> || <a href="/videos/2020/12/26/Lets_talk_about_Pence_analogies_and_the_working_class">transcript &amp; editable summary</a>)

Beau explains a flawed analogy of rising tides in economics, criticizing wealth inequality and advocating for uplifting the working class to stimulate the economy.

</summary>

"You want to raise the tide, you have to raise the working class."
"The stimulus, yeah, it's got to start at the bottom."
"This country has a widening gap between the haves and the have-nots."

### AI summary (High error rate! Edit errors on video page)

Explains the analogy of a rising tide lifting all ships and its connection to the economy and working class.
Criticizes the analogy for being inaccurate and misleading, particularly in relation to the stock market.
Notes that the rich are getting richer while the working class is suffering, contrary to the analogy.
Points out that most Americans are actually in the water, not on ships, hinting at the working class being the foundation that supports the wealthy.
Mentions how senators favor the rich by artificially boosting their wealth, leading to income inequality.
Emphasizes the importance of uplifting the working class for overall economic growth and prosperity.
Advocates for stimulus efforts starting from the bottom to benefit those who need it most and boost economic activity.
References the concept of velocity in economics, suggesting that money circulated among the working class is more impactful than stagnant wealth.
Warns about the widening wealth gap and the detrimental effects of government policies favoring the wealthy.
Concludes by encouraging reflection on these economic dynamics and their implications for society.

Actions:

for policy makers, economic activists,
Support grassroots movements advocating for economic equality (implied)
Advocate for policies that prioritize supporting the working class (implied)
</details>
<details>
<summary>
2020-12-25: Let's talk about Christmas questions/500,000 (Part 2)..... (<a href="https://youtube.com/watch?v=dLYYawe5Ti0">watch</a> || <a href="/videos/2020/12/25/Lets_talk_about_Christmas_questions_500_000_Part_2">transcript &amp; editable summary</a>)

Beau shares about basic world dishes, future plans, favorite holiday treats, beard care, social media engagement, and dream travel goals.

</summary>

"Birds, squirrels, snakes enter shop due to design."
"Longs to travel all over the United States."
"Dream to run a facility akin to Highlander Folk School."
"Not all military members are heroes."
"Describes healthy masculinity as understated and subtle."

### AI summary (High error rate! Edit errors on video page)

Shares basic dishes from around the world, nothing too special.
Unsure about moving to Georgia or voting.
Plans for window panes in greenhouse, but preoccupied.
Met lovely wife in a simple way.
Suggests traditional ways for workers to unionize.
Favorite holiday food: sugary Christmas trees from Little Debbie.
Trims own beard with wireless trimmer, knows it grows back fast.
Won YouTube plaque at 100k subscribers.
Birds, squirrels, snakes enter shop due to design.
Enjoys animated movies, Frozen 2 a current favorite.
Prefers cordless power tools.
Missed chance to wrap video in Christmas lights.
Possibly bought a Nirvana t-shirt as a teen.
Kids sometimes reply with "it's just a thought."
Handles hundreds of messages daily via social media.
Longs to travel all over the United States.
Struggles to show t-shirts clearly in videos.
Likes chocolate Santas or Nestle Crunch bars for stocking stuffers.
Laughs most with kids, never heard a belly laugh.
Kids unaware that Santa prefers tacos.
Favorite milkshake is Oreo from Sonic.
Offers advice on explaining libertarian positions.
Concerns about benefits of artificial intelligence in war.
Not a fan of hunting, prefers cordless power tools.
Metal roof withstood a category 5 hurricane.
Dressing like Santa for Christmas requested.
Supports abolishing political parties with more vigilant voters.
Prefers Denzel Washington as a better actor over Tom Hanks.
Loyalty to the Gators but not a sports follower.
Dogs and pets featured more in future content.
Ideal Christmas dinner includes Green Bean Casserole and ham.
Dream to run a facility akin to Highlander Folk School.
Acknowledges not all military members are heroes.
Describes healthy masculinity as understated and subtle.
Interested in working with Bailey Saron on true crime content.

Actions:

for content creators and social media users.,
Watch Working Stiff USA on YouTube (suggested).
Invest in a good wireless trimmer for beard maintenance (implied).
Show solidarity towards countries impacted by US foreign policy (generated).
</details>
<details>
<summary>
2020-12-24: Let's talk about the Trump vs McConnell contest.... (<a href="https://youtube.com/watch?v=qV-ZS3m3gD8">watch</a> || <a href="/videos/2020/12/24/Lets_talk_about_the_Trump_vs_McConnell_contest">transcript &amp; editable summary</a>)

Beau delves into the internal power struggle between Trump and McConnell within the Republican Party, predicting McConnell’s victory in the upcoming poll and the party reverting to obstructionist tactics during the Biden administration.

</summary>

"It's really deciding whether McConnell or Trump gets to hold the reins of power."
"My best guess is that McConnell will win this poll."
"A smart politician, somebody who is concerned with self-interest and maintaining their own power, they're probably going to go with McConnell."
"You will be rooting for McConnell, because he is definitely the lesser of two evils out of that."
"Just stay calm, because my guess is that you're going to see a lot of Trump loyalists trying to prove how much they love Trump."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the internal power struggle between Trump and McConnell within the Republican Party.
The Republican Party will conduct a poll on January 6th to decide whether they want to be the party of Trump or McConnell.
Despite being termed as a poll, it's more of a theatrical voting on objections to the electoral votes.
The votes are expected to go in favor of McConnell over Trump due to long-term political capital considerations.
The breakdown of votes will reveal who supports Trump's control and who prefers McConnell's leadership.
Newer representatives may lean towards supporting Trump, thinking he holds more power, while experienced politicians understand McConnell's influence.
McConnell is likely to win the poll, leading the Republican Party to revert back to obstructionist tactics during the Biden administration.
Beau predicts that if McConnell wins, the party will adopt a George Bush-era style, whereas a Trump victory may further radicalize the party.
Despite the theatrics expected on January 6th, the essence of the poll is deciding between McConnell and Trump's control over the party.
Beau advises staying calm amidst the wild statements from Trump loyalists and suggests that smart politicians will lean towards McConnell for their own interests.

Actions:

for politically engaged individuals.,
Stay informed about the outcomes of the Republican Party's poll on January 6th (suggested).
Monitor how your representatives and senators vote in the poll and understand their stance on Trump or McConnell's control (implied).
Advocate for political strategies that prioritize governance over theatrics and political capital (implied).
</details>
<details>
<summary>
2020-12-24: Let's talk about questions about Santa.... (<a href="https://youtube.com/watch?v=tWTMerznrfU">watch</a> || <a href="/videos/2020/12/24/Lets_talk_about_questions_about_Santa">transcript &amp; editable summary</a>)

Addressing concerns about Santa's ability to deliver presents, Beau assures that Christmas will occur with the spirit of giving, showcasing the power of cultural norms to unite people globally.

</summary>

"Christmas will occur."
"The volunteers are Santa's community networks."
"You don't have to change the law. You have to change thought."
"It just became a cultural norm, because people got behind it."
"You have to start doing it."

### AI summary (High error rate! Edit errors on video page)

Addressing concerns about Santa's ability to deliver presents due to international borders and health issues.
Acknowledging a child's thoughtful questions and appreciating their level of concern.
Santa has been vaccinated and ensured safety for his workers at the North Pole.
Santa has plans in place for crossing international lines and has volunteers to assist in countries with travel bans.
Production may be lower due to social distancing, but Christmas will still occur with the spirit of giving.
People around the world come together during Christmas, transcending boundaries and differences.
The voluntary efforts of individuals during the holiday season transform communities.
Emphasizing the power of cultural norms in bringing people together without coercion.
Encouraging the importance of changing thought to address issues that governments may not act on.
Advocating for starting cultural norms based on care and compassion.

Actions:

for global community members,
Volunteer to assist in local community events or initiatives to spread the spirit of giving during the holiday season (suggested).
Initiate or participate in activities that foster care and compassion towards the environment and fellow individuals (suggested).
</details>
<details>
<summary>
2020-12-24: Let's talk about Christmas questions/500,000 (Part 1).... (<a href="https://youtube.com/watch?v=LuuKDwsytrk">watch</a> || <a href="/videos/2020/12/24/Lets_talk_about_Christmas_questions_500_000_Part_1">transcript &amp; editable summary</a>)

Beau shares light-hearted moments, favorite shows, family traditions, and community reflections while answering Twitter questions.

</summary>

"I think that's one of those moments where not only somebody, people corrected something that was wrong, but it shows that there are some things that can unite everybody."
"I don't know if they want to buy me a drink or hit me with a bottle."
"Anything belongs on pizza if you're brave enough."
"Not particularly my choice, but yeah pineapple on pizza an abomination or an extremely wrong choice."
"That may happen at some point in fact we thought about doing it for this one but you know it's it's the holidays so."

### AI summary (High error rate! Edit errors on video page)

Taking light-hearted questions from Twitter and answering them.
Sharing favorite children's shows and movies for their empowering messages.
Me-time activity is talking with internet people, now missing road trips.
Favorite fiction book series by Bernard Cromwell.
Cherished stuffed animal: a stuffed pumpkin.
Addressing the impact of becoming a celebrity on family.
Suggests Irish cuisine for saving while cooking.
Believes in the power of community networks over government reliance.
Cooking shepherd's pie and enjoying Oreos as a treat.
Views on balancing mutual aid and state services.
Sharing about life, celebrity status, and family dynamics.
Reflections on Christmas stories, songs, and holiday traditions.
Favorite American and non-American foods.
Hopeful predictions for 2021.
Recalling past Christmas gifts and New Year's resolutions.
Sharing experiences with his wife and family traditions.
Details about the filming setup in his shed and favorite tools.
Thoughts on political narratives in stories like The Grinch.
Thoughts on Santa Claus, cereal, and historical figures.
Favorite travel destination in the US and Christmas traditions.
Memorable encounter with his wife and favorite cultural tradition.
Family holiday traditions and unique Christmas requests.

Actions:

for twitter users, families,
Send Beau a physical mail package to P.O. Box 490 Sneeds, Florida, 32460 (suggested).
Try cooking Irish cuisine for saving while exploring new flavors (implied).
Stock up on essentials for disaster relief packages following climate or situation-specific needs (suggested).
</details>
<details>
<summary>
2020-12-23: Let's talk about Trump calling for a bigger stimulus.... (<a href="https://youtube.com/watch?v=aA2DXNtfT38">watch</a> || <a href="/videos/2020/12/23/Lets_talk_about_Trump_calling_for_a_bigger_stimulus">transcript &amp; editable summary</a>)

Trump pushes for more payments, Beau questions motives, advises Dems not to give in, suggests letting Trump and McConnell duke it out for the benefit of the nation.

</summary>

"Let them fight and see what happens."
"There's no real loss in it for Democrats either way."
"Best case scenario, get the cash payments to the people who need them."
"It might do a lot of working class Republicans some good to see Trump saying, hey, let's help out the little guy."
"We're at the point where we have to move forward as a country."

### AI summary (High error rate! Edit errors on video page)

Trump called for more direct payments to Americans after a deal was reached for $600 payments, which are significantly less than what other countries have offered.
McConnell has been obstructing efforts to increase the payments.
Beau questions Trump's motives, doubting his sudden concern for American workers.
Beau suggests that Trump's actions may be driven by his true nature coming out.
There is a power struggle between McConnell and Trump for control of the Republican Party post-Trump's presidency.
Beau advises Democrats not to give up anything in return for the increased payments and let Trump and McConnell figure it out.
He believes it might benefit working-class Republicans to see Trump advocating for them, even if it's not genuine.
Beau encourages breaking down false beliefs, such as the notion that the Republican Party truly represents the rural working class.
He concludes by expressing the importance of moving forward as a country and letting the dynamics between Trump and McConnell play out.

Actions:

for politically engaged individuals,
Let Trump and McConnell's power struggle play out (suggested)
Challenge false beliefs about political parties (implied)
</details>
<details>
<summary>
2020-12-22: Let's talk about Trump, Biden, counties, and votes.... (<a href="https://youtube.com/watch?v=ckLt-QBL4EI">watch</a> || <a href="/videos/2020/12/22/Lets_talk_about_Trump_Biden_counties_and_votes">transcript &amp; editable summary</a>)

Counties vary in population size, debunking the misleading talking point about Trump winning more counties but Biden winning more votes.

</summary>

"Counties vary significantly in population, so comparing them directly is not accurate."
"They're made up and they use different metrics and compare them to confuse people."
"If a pundit, a commentator that you follow has pushed this idea, you might want to reconsider following them."

### AI summary (High error rate! Edit errors on video page)

Explains the talking point of Trump winning more counties but Biden winning more votes.
Counties vary significantly in population, so comparing them directly is not accurate.
Illustrates with the example of Biden winning LA county with over three million votes.
Contrasts Trump winning 142 counties with votes from Wyoming, North Dakota, and South Dakota.
Emphasizes that Trump's vote count from those states includes every person, not just eligible voters.
Points out that LA county has a larger population than most states that voted for Trump.
Concludes that the talking point is misleading, not grounded in reality, and aims to confuse people.
Encourages viewers to question sources spreading such misinformation.

Actions:

for voters, information seekers,
Fact-check information from pundits or outlets spreading misleading narratives (implied).
</details>
<details>
<summary>
2020-12-22: Let's talk about Pat Robertson turning on Trump.... (<a href="https://youtube.com/watch?v=sUwpAK8IvGE">watch</a> || <a href="/videos/2020/12/22/Lets_talk_about_Pat_Robertson_turning_on_Trump">transcript &amp; editable summary</a>)

People, including Pat Robertson, are publicly distancing themselves from Trump as his support dwindles, making it harder for him to continue his offensive against reality.

</summary>

"He should retire and not run again."
"The more of the real power brokers who cut Trump loose, the harder it is for him to carry on his offensive against reality."
"It will be easier to get the more committed of his loyalists gone."

### AI summary (High error rate! Edit errors on video page)

People, including big names like Pat Robertson, are distancing themselves from Trump, not wanting to be associated with him when his support dwindles.
Pat Robertson made a statement criticizing Trump, suggesting that he should retire and not run again, as his claims about Biden have not come to fruition.
Trump is likely to lose support from evangelicals, but conservative people are slow to change, so the shift may take time.
Pat Robertson is noted for his two different sides - the evangelical leader and the politically active figure who has supported progressive policies and candidates.
Robertson's successful political career stems from his ability to read the political climate and transition between being in power and out of power.
Establishment Republicans, who understand the political game, are expected to distance themselves from Trump publicly, making it harder for him to continue his tactics against reality.
As support for Trump dwindles, it will be easier to sway his committed loyalists, particularly those who only know government under Trump, out of public office.

Actions:

for political observers,
Publicly distance oneself from politicians or figures supporting harmful ideologies (suggested)
Support policies and candidates that prioritize treatment over incarceration (exemplified)
Be vocal about calling out political figures who enable harmful actions (implied)
</details>
<details>
<summary>
2020-12-21: Let's talk about the good you do.... (<a href="https://youtube.com/watch?v=_D9qaJs_Ch4">watch</a> || <a href="/videos/2020/12/21/Lets_talk_about_the_good_you_do">transcript &amp; editable summary</a>)

Beau organized a successful fundraiser through a livestream, exceeding the goal and providing gift bags for teens in need, showcasing the power of collective action and generosity.

</summary>

"Y'all made 12 kids really happy."
"Everybody contributes in their own way."
"When it comes to stuff like this, everybody contributes in their own way."
"We have to set our sights higher when we do stuff like this."
"It's heartwarming, even to me."

### AI summary (High error rate! Edit errors on video page)

Organized a fundraiser through a live stream to put together gift bags for teens in need, specifically those in a domestic violence shelter during the holidays.
The goal was to provide tablets, cases, battery backups, Bluetooth speakers, earbuds, gift cards, and other essentials for the teens.
Despite initially planning for five bags, they ended up successfully putting together 12 gift bags.
The tablets included in the bags provide the teens with a personal space to retreat and process what's going on around them.
The fundraiser exceeded expectations, raising around $10,000 in just an hour and a half.
Beau expresses deep gratitude towards the community for their overwhelming support and generosity.
Along with gift cards and essentials, additional funds were donated directly to the shelter for their discretion.
Beau acknowledges the collective effort of individuals coming together to achieve a common goal.
Plans to showcase the behind-the-scenes process of fundraising and mobilization through a video on the second channel.
Emphasizes the impact of collective action in bringing light to dark spaces and making a difference in children's lives.

Actions:

for community members, supporters,
Donate directly to shelters for immediate assistance (suggested)
Support fundraisers and live streams for charitable causes (suggested)
Participate in engagement activities like sharing content to increase reach and impact (suggested)
</details>
<details>
<summary>
2020-12-21: Let's talk about Trump's meetings and his state of mind.... (<a href="https://youtube.com/watch?v=v9Jvf1vLZzE">watch</a> || <a href="/videos/2020/12/21/Lets_talk_about_Trump_s_meetings_and_his_state_of_mind">transcript &amp; editable summary</a>)

President Trump's reported meetings prompt concerns about control, judgment, and the need for transparency from those in his orbit to debunk extreme claims and uphold accountability.

</summary>

"Silence here is complicity."
"Those in his orbit are now obligated to come out and actively, openly, and publicly explain and debunk..."
"We need a lot of disclosure here."
"Your voice is becoming very important."
"If they want to salvage any, any shred of public image, they have to come forward."

### AI summary (High error rate! Edit errors on video page)

President Trump's reported meetings raise questions about his control and judgment.
Psychologists caution against diagnosing based on headlines.
Those in Trump's orbit may be obligated to act if he has indeed "lost it."
Public disclosure and debunking of extreme claims are necessary.
Concerns about invoking the 25th Amendment if Trump's judgment is impaired.
Rudy Giuliani's role as a voice of reason is concerning.
Staying silent may suggest complicity in dangerous actions.
Urges GOP members to speak out for the truth.
Silence is seen as complicity in enabling potentially harmful behavior.
Encourages transparency and disclosure from those close to Trump.

Actions:

for politically engaged individuals,
Contact politicians to demand transparency and accountability from those in power (implied)
Speak out against harmful actions and enable public discourse (implied)
</details>
<details>
<summary>
2020-12-20: Let's talk about my first job and two new characters.... (<a href="https://youtube.com/watch?v=73Hf5SPOgbY">watch</a> || <a href="/videos/2020/12/20/Lets_talk_about_my_first_job_and_two_new_characters">transcript &amp; editable summary</a>)

Beau shares insights from his first job, including the impact of stereotypes, learning English from Sesame Street, and the importance of adapting to global migration trends.

</summary>

"I learned to speak English from Sesame Street."
"A lot of the stereotypes that we have about people from other places they gotta go."
"The world is changing and we're going to have to adjust."

### AI summary (High error rate! Edit errors on video page)

Recalls his first job experience at fourteen, washing dishes for an immigrant who was a happy and jovial guy but faced stereotyping due to his mannerisms.
Initially found the stereotype of his employer being connected to organized crime cool as a teenager, but realized its harmful nature as he got older.
Shares a memorable incident where the happy employer, who learned English from Sesame Street, admonished a young man for not taking his classes seriously.
Expresses how Sesame Street, a timeless institution, has been a part of many people's childhoods and stories by not chasing trends and thinking in decades rather than years.
Mentions the introduction of new characters Aziz and Nor who live in refugee camps on Sesame Street, indicating long-term educational support for children in such settings.
Emphasizes the need to address the increasing migration of people due to climate change, requiring the eradication of stereotypes and nationalism.
Acknowledges the changing world and the necessity for institutions to forecast and adapt to the movement of people globally.

Actions:

for educators, policymakers, community leaders,
Support educational initiatives for children in refugee camps (implied)
Challenge and dismantle stereotypes about people from different backgrounds (implied)
Advocate for policies that address climate-induced migration and support displaced populations (implied)
</details>
<details>
<summary>
2020-12-19: Let's talk about speaking Trump and the biggest republican joke.... (<a href="https://youtube.com/watch?v=iwzwRCrHBxU">watch</a> || <a href="/videos/2020/12/19/Lets_talk_about_speaking_Trump_and_the_biggest_republican_joke">transcript &amp; editable summary</a>)

Beau delves into the manipulation through fear and ignorance in Republican talking points, challenging supporters to confront the reality of authoritarian practices under Trump to foster anti-authoritarianism.

</summary>

"Fear and ignorance are key components of Republican talking points."
"Everything they fear about communism occurred under Donald Trump."
"The truth isn't told, it's realized."
"Make them anti-authoritarian."
"The goal is not to turn them into progressives, but to make them anti-authoritarian."

### AI summary (High error rate! Edit errors on video page)

Introducing a series on speaking the "second language" of Trump and his supporters based on fear and ignorance.
Explaining how fear and ignorance are key components of Republican talking points.
Describing the common response of labeling anything left-leaning as communist without understanding the term.
Pointing out that the fear of communism is based on imagery and misinformation rather than actual knowledge.
Listing the common fears associated with communism: bread lines, rationing, propaganda, police presence, controlled press, cronyism, etc.
Challenging the audience to name something from the list that did not occur under Donald Trump's administration.
Illustrating how many of the fears associated with communism actually manifested during Trump's presidency.
Emphasizing the manipulation of individuals through fear and misinformation to support authoritarian practices.
Encouraging listeners to confront supporters with the reality of what has already taken place rather than focusing solely on Trump.
Urging for a shift towards anti-authoritarianism rather than making the discourse about party politics.

Actions:

for political observers,
Challenge misconceptions about communism with facts and examples (implied)
Encourage reflection on authoritarian practices and their implications (implied)
Foster anti-authoritarian perspectives through open dialogues (implied)
</details>
<details>
<summary>
2020-12-18: Let's talk about speaking the language of Trump.... (<a href="https://youtube.com/watch?v=gijxSuWRmBw">watch</a> || <a href="/videos/2020/12/18/Lets_talk_about_speaking_the_language_of_Trump">transcript &amp; editable summary</a>)

Understanding and addressing the fears of Trump supporters is key to swaying his base and exhausting his political capital.

</summary>

"It's all about fear."
"If you want to convince somebody that Trump really resonated with, if you want to convince them of anything, you have to frame it around their fears."
"The only thing that creates progressives is education and context."
"The President's own rhetoric, his own wild claims, can be used to show his base exactly who he is."
"When you are talking to people who are under Trump's sway, you have to frame it around their fears."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of understanding the rhetoric that resonates with Trump supporters.
Points out that fear is a key motivator for Trump's base, used in his rhetoric to captivate them.
Emphasizes that addressing fears, not creating progressives, can help sway Trump supporters.
Suggests that making Trump supporters anti-authoritarian is more feasible than making them progressive.
Stresses the need to debunk false claims persistently to sway a significant portion of Trump's base.
Outlines a strategy to exhaust Trump's reputation and political capital over time.
Urges framing arguments around Trump supporters' fears to effectively communicate with them.

Actions:

for activists, educators, progressives,
Debunk false claims persistently until a significant portion of Trump's base understands (implied)
Work towards defeating politicians who supported baseless claims in the midterms (implied)
</details>
<details>
<summary>
2020-12-18: Let's talk about political capital.... (<a href="https://youtube.com/watch?v=SvSkBsiEjtY">watch</a> || <a href="/videos/2020/12/18/Lets_talk_about_political_capital">transcript &amp; editable summary</a>)

Beau explains political capital as goodwill in politics, comparing it to social media, and how it impacts decision-making and re-election potential.

</summary>

"Political capital is basically goodwill."
"It's worth noting that this isn't the way it should be, but it's the way it is right now."
"That's how this works."
"It's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains political capital as goodwill in politics, with two types: reputational and representative.
Compares political capital in politics to social media, particularly YouTube.
Illustrates reputational political capital by discussing consistency in policies and ideological stances.
Differentiates between reputational political capital (support from base) and representative political capital (related to getting things done).
Uses examples from YouTube to explain the concept of political capital.
Mentions how failing to deliver on promises or facing scandals can impact reputational political capital.
Relates political capital to current political situations involving Pelosi, AOC, Trump, and McConnell.
Analyzes the battle for control within the party based on reputational and representative political capital.
Cites an example of a decline in reputational political capital for Crenshaw due to issues with the VA and social media.
Emphasizes the importance of political capital in politicians' decision-making and potential impact on re-election.

Actions:

for politically engaged individuals,
Analyze and understand how political capital influences decision-making (implied).
Stay informed about politicians' actions and their impact on political capital (implied).
Engage in community organizing to support or hold politicians accountable based on their political capital (implied).
</details>
<details>
<summary>
2020-12-17: Let's talk about a gift to Democrats and messaging.... (<a href="https://youtube.com/watch?v=BG6Vkbl13VU">watch</a> || <a href="/videos/2020/12/17/Lets_talk_about_a_gift_to_Democrats_and_messaging">transcript &amp; editable summary</a>)

People may act against their interests if the message doesn't resonate; Democrats can seize the national security narrative through improved medical infrastructure like Medicare for All.

</summary>

"Our medical infrastructure being in the state that it is in is a national security issue."
"The easiest way to secure this nation from this kind of issue is to up our medical infrastructure."
"The easiest way to do that is to increase usage."
"It's just messaging."
"The exact same bill, retitled."

### AI summary (High error rate! Edit errors on video page)

People may act against their interests if the message doesn't resonate with them.
The Democratic Party has an opening to become the party of national security.
Recent events have shown the need for increased medical infrastructure and readiness.
Foreign policy shifts suggest future opposition from powerful countries.
To strengthen national security, the medical infrastructure must be made more resilient.
Increasing usage of medical facilities is key to enhancing readiness.
Access to healthcare for everyone can significantly boost national security.
Renaming existing bills like M4A (Medicare for All) can help in this regard.
Improving medical infrastructure is vital for facing challenges from near-peer nations.
Reframing healthcare as a national security issue can shift perspectives effectively.

Actions:

for policy advocates, democrats,
Advocate for policies that improve medical infrastructure (suggested)
Support bills like Medicare for All (suggested)
</details>
<details>
<summary>
2020-12-17: Let's talk about Trump's base, messaging, and incrementalism.... (<a href="https://youtube.com/watch?v=Il3DgKLPVUM">watch</a> || <a href="/videos/2020/12/17/Lets_talk_about_Trump_s_base_messaging_and_incrementalism">transcript &amp; editable summary</a>)

Beau clarifies MLK's quote on immediate action, stresses incremental progress, and urges reaching out to Trump supporters to prevent future authoritarian leadership, advocating for effective communication across ideological divides.

</summary>

"The time is always right to do what is right."
"Social change doesn't just will in on its own. You have to make it happen."
"We have to reach out to them and we have to reach them successfully."
"Trump lost, but he didn't lose big."
"There's a lot of work to be done."

### AI summary (High error rate! Edit errors on video page)

Clarifies the misinterpretation of Martin Luther King Jr.'s quote about immediate results in pursuing social change.
Emphasizes the need for tireless, persistent efforts in the long-term pursuit of social progress.
Points out the misconception of using MLK's quote to justify instant gratification in activism.
Urges the importance of continuous work towards social change, noting that progress is incremental.
Stresses the necessity of reaching out to the 70 million Americans who supported Trump and engaging in meaningful dialogues.
Advocates for speaking the same "language" as those with differing ideologies to bridge the gap.
Warns against complacency after Biden's win, citing the lack of a progressive mandate and the need for individual action.
Acknowledges the challenge of convincing a significant portion of Trump's supporters to reconsider their beliefs.
Calls for framing issues in a way that resonates with different audiences, including both big picture and kitchen table issues.
Criticizes the Democratic Party's messaging and failure to connect with low-income rural voters who share similar values.

Actions:

for activists, communicators, progressives,
Reach out to Trump supporters in a way that resonates with them, adopting effective rhetoric to bridge ideological gaps (implied).
Frame issues as both big picture issues and kitchen table issues to appeal to diverse audiences (implied).
Work towards convincing a significant portion of Trump's 70 million supporters to reconsider their beliefs through meaningful engagement (implied).
</details>
<details>
<summary>
2020-12-16: Let's talk about for whom the bell tolls and an update.... (<a href="https://youtube.com/watch?v=8yz0SuYWHek">watch</a> || <a href="/videos/2020/12/16/Lets_talk_about_for_whom_the_bell_tolls_and_an_update">transcript &amp; editable summary</a>)

Beau provides updates on vaccine availability, urges continued precautions post-vaccination, and expresses optimism about the pandemic's end.

</summary>

"Our current public health struggles now are larger in scope of loss than World War II."
"You know you're big bad and brave and all I know you don't care about the worst possible outcome."
"We are nearing the end of this. For the first time, I can actually see a light at the end of the tunnel."

### AI summary (High error rate! Edit errors on video page)

Provides historical context by comparing the number of combat losses during World War II to the tolling of a bell at the National Cathedral.
Updates on the current public health struggles, mentioning the hope for wide vaccine availability by March/April.
Acknowledges the impressive logistics required for mass vaccine distribution.
Mentions personal skepticism about vaccine availability due to Governor DeSantis's involvement in the rollout.
Emphasizes the importance of continuing to wear masks and practice social distancing even after vaccination.
Explains that while the vaccine is effective at preventing serious illness, its impact on transmission to others is still unclear.
States the need to continue preventive measures until more information is available or a large portion of Americans are vaccinated.
Warns macho individuals about potential long-term health effects, urging them to take precautions seriously.
Encourages basic protective measures like hand washing, avoiding face touching, and staying home when possible.
Expresses optimism about seeing the light at the end of the tunnel due to the imminent availability of a vaccine.

Actions:

for public health advocates,
Wash your hands, avoid touching your face, and wear a mask when going out (implied)
Stay updated on vaccine availability and distribution in your area (implied)
</details>
<details>
<summary>
2020-12-16: Let's talk about Trump, McConnell, and electoral objections.... (<a href="https://youtube.com/watch?v=eJGwmqHWmiw">watch</a> || <a href="/videos/2020/12/16/Lets_talk_about_Trump_McConnell_and_electoral_objections">transcript &amp; editable summary</a>)

Senator McConnell seeks control of the Republican Party by setting up a choice between siding with him or Trump, potentially fracturing the party.

</summary>

"This is about power, as is often the case up on Capitol Hill."
"If there are objections, if there are people that side with those objections, the Republican Party is going to be fractured for a while."
"They're going to weigh those two options. I note that nowhere in it does what's best for the country factor into it."
"It's a bold strategy. Let's see if it pays off for him."
"Anyway, it's just a thought. I have a good night."

### AI summary (High error rate! Edit errors on video page)

Senator McConnell is reaching out to senators, advising them not to object to Trump’s actions, implying it will be voted down.
McConnell is likely doing this to distance himself from the president and clear himself of enabling Trump for the past four years.
McConnell is setting up a potential showdown between himself and Trump, trying to gain control of the Republican Party.
Most Americans view objections to the election results as undermining the process and un-American.
McConnell wants actual control and loyalty within the party, setting up a choice for Congress members between him and Trump.
Even though Trump is leaving office, he still holds influence through social media and his base.
Republicans in Congress are in a tough spot – objecting will likely lead to losing, making them look silly, while not objecting may anger Trump.
The decision-making process for Republicans is more about power and control within the party than what's best for the country.
If there are objections within the party, it will lead to a fractured Republican Party lacking unity.
If everyone falls in line behind McConnell, Trump’s influence may diminish, potentially disillusioning his base from supporting the party.

Actions:

for political observers, republican party members,
Support politicians who prioritize country over party loyalty (implied)
Stay informed about the actions and decisions of politicians within your party (implied)
</details>
<details>
<summary>
2020-12-16: Let's talk about Georgia's election.... (<a href="https://youtube.com/watch?v=ShcaULvQIeE">watch</a> || <a href="/videos/2020/12/16/Lets_talk_about_Georgia_s_election">transcript &amp; editable summary</a>)

Georgia's high-stakes Senate runoff election on January 5th determines power dynamics in Congress, with Democrats aiming for control and Republicans facing strategic implications.

</summary>

"It's a runoff election on January 5th."
"Whoever has highest turnout."
"The entire country cares about the senators that Georgia elects."

### AI summary (High error rate! Edit errors on video page)

Georgia runoff election on January 5th with high stakes for Senate majority.
Democrats win Senate majority if they secure two seats, leading in polling by a slim margin.
If Republicans win, McConnell retains power with Trump's support, creating rift within the party.
Unique considerations in the race, including Republicans backing Trump's election claims.
Potential factors affecting turnout include Trump loyalists' response to McConnell and military voters.
Long-term implications: Democrats may undo Trump's policies, while Republicans may obstruct Biden's agenda.
Outcome depends on voter turnout, with Republicans historically showing higher engagement.
Uncertainty in predicting the election outcome due to unusual circumstances and candidate actions.
Importance of the Georgia Senate race lies in shifting power dynamics in Congress.

Actions:

for georgia voters,
Mobilize for early voting and turnout in the Georgia Senate runoff election (suggested).
Educate others on the candidates and the stakes involved in the election (suggested).
</details>
<details>
<summary>
2020-12-15: Let's talk about Republicans leaving Trump and moving forward.... (<a href="https://youtube.com/watch?v=ELAry9HNn7s">watch</a> || <a href="/videos/2020/12/15/Lets_talk_about_Republicans_leaving_Trump_and_moving_forward">transcript &amp; editable summary</a>)

Republicans distancing from Trump wasn't a change of heart but a response to failure, prompting a call for national reflection to prevent similar events.

</summary>

"It wasn't a change of heart. It was a change of the situation."
"We may forgive. But don't forget."
"I think it's incredibly important that that conversation take place."

### AI summary (High error rate! Edit errors on video page)

Republicans had a sudden change of heart and started distancing themselves from Trump after the electors cast their votes.
A viewer suggested updating a video from July 2019, detailing 14 characteristics, 10 stages, and five components of a specific form of government.
Viewers are encouraged to watch the video, add their own examples, and acknowledge how close the country came to a different path.
The reason Republicans are now jumping ship is because the 14th characteristic failed, not due to a change of heart.
It's vital for the nation to acknowledge how close it came to a different outcome and to have a national dialogue about what happened.
Those calling to move forward and put things behind us enabled and supported the previous administration's policies.
If Trump had succeeded, those currently distancing themselves from him would still be supporting him.
Moving forward, healing, and reuniting as a nation is possible but requires acknowledging the recent past.
It's suggested that the nation engages in a critical national discourse to prevent similar events from happening again.
While forgiveness is possible, it's vital not to forget how close the country was to a different path.

Actions:

for viewers, citizens,
Watch the suggested video, add your own examples, and contribute to the national discourse (suggested)
Engage in critical national dialogues about recent events (suggested)
</details>
<details>
<summary>
2020-12-14: Let's talk about a policy shift in foreign policy.... (<a href="https://youtube.com/watch?v=3mILljoPj5Y">watch</a> || <a href="/videos/2020/12/14/Lets_talk_about_a_policy_shift_in_foreign_policy">transcript &amp; editable summary</a>)

Boba Gowd warns of shifts in intelligence personnel assignments, prioritizing deniability in a changing foreign policy landscape, leading to potential changes in public perception.

</summary>

"The Department of Defense is going to begin limiting the detailing, the temporary reassignment of their personnel to the intelligence world."
"The new priority is going to be near peers. Near peers being countries like China or Russia."
"There has to be a level of deniability."
"This realistically, this should have happened like five years ago."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Department of Defense to limit personnel detailing and temporary reassignment to intelligence world.
Since 2001, personnel were commonly detailed for intelligence work due to lack of opposition embassies and the need for deniability.
Shift in U.S. foreign policy prioritizes near peers like China and Russia over previous groups.
Deniability is now key in intelligence work, necessitating more expendable personnel for potential situations.
Expected change in policy was long overdue and not a surprise.
Likely some internal dispute or reason for the change at this specific moment.
Not a cause for worry, rather a necessary adjustment reflecting a shift in priorities.
Unlikely that the framing of the news will directly address the need for deniability in intelligence operations.
Speculation on how the news will be presented to the public and potential reactions.
A heads up about the upcoming news and its possible impact on public perception.

Actions:

for policy analysts,
Stay informed about changes in national security policies (implied).
</details>
<details>
<summary>
2020-12-14: Let's talk about Dr. Jill Biden, NY medics, and women.... (<a href="https://youtube.com/watch?v=8SGDIVW06wk">watch</a> || <a href="/videos/2020/12/14/Lets_talk_about_Dr_Jill_Biden_NY_medics_and_women">transcript &amp; editable summary</a>)

Dr. Jill Biden faces criticism for being called "doctor," while a medic's side gig becomes a focus of moral outrage, showcasing the struggles of frontline healthcare workers amid societal judgment of women's accomplishments and bodies.

</summary>

"You can do whatever you want in this world. As long as we approve."
"In today's world, a woman has to be two things. Who and what she wants."

### AI summary (High error rate! Edit errors on video page)

Dr. Jill Biden was criticized for using the title "doctor" because she has a doctorate in education, not medicine.
The New York Post reported on a medic who had a side gig on a certain website for her fans.
The focus should have been on the financial struggles of frontline healthcare workers, not moral outrage over a medic's side job.
There is a disparity between frontline healthcare workers and the multi-billion dollar industry profiting from them.
The message conveyed was that women shouldn't be proud of their educational accomplishments, brains, or bodies.
Society's approval seems necessary for women to be proud of themselves.
Equality is still a work in progress in the country.
Women should be free to be who and what they want to be without seeking society's approval.

Actions:

for women, healthcare workers,
Support frontline healthcare workers financially and advocate for fair compensation (implied)
</details>
<details>
<summary>
2020-12-13: Let's talk about Trump, concession, and decisions.... (<a href="https://youtube.com/watch?v=ltTT7ZEJFhk">watch</a> || <a href="/videos/2020/12/13/Lets_talk_about_Trump_concession_and_decisions">transcript &amp; editable summary</a>)

Beau outlines the timeline of Al Gore's concession speech, criticizes Trump's refusal to accept the Supreme Court's decision, and stresses the urgency for him to concede and step away from public service.

</summary>

"The President needs to concede and needs to remove himself from public service."
"He further damages the credibility of the United States."
"He doesn't know the rules."
"Never should have stepped into the ring, so to speak."
"I cannot wait to get to the point where I never have to type the phrase let's talk about Trump again."

### AI summary (High error rate! Edit errors on video page)

Recounts the timeline of events surrounding Al Gore's concession speech in 2000.
Compares the situation to a boxing match where the losing boxer refuses to accept the decision.
Criticizes Trump for not accepting the Supreme Court's decision and draws parallels to unfit behavior in the presidency.
Points out that Trump's actions indicate a lack of understanding of the Constitution, integrity, and self-centeredness.
Mentions the surprise of Trump's supporters at judicial rulings and clarifies the independence of the judiciary.
Stresses the urgency for Trump to concede to prevent further damage to the country's credibility.
Expresses eagerness for a resolution to the situation and to stop discussing Trump.

Actions:

for political activists and concerned citizens.,
Contact your representatives to urge them to push for Trump's concession and removal from public service (implied).
Educate others on the importance of accepting democratic processes and decisions (implied).
</details>
<details>
<summary>
2020-12-12: Let's talk about Trump, the Supreme Court, and the Constitution.... (<a href="https://youtube.com/watch?v=yax4slhRYyo">watch</a> || <a href="/videos/2020/12/12/Lets_talk_about_Trump_the_Supreme_Court_and_the_Constitution">transcript &amp; editable summary</a>)

Beau predicts Supreme Court decision accurately, underscores conflict between experts and Trump supporters, and challenges individuals to choose between supporting the Constitution or the President.

</summary>

"Experts versus Trump."
"If you are willing to rip the country apart because you do not agree with the Supreme Court decision, you don't support the Constitution."
"Those media pundits who are still out there saying, oh, constitutionality. They don't know anything about the Constitution."
"You've given up on moving the country forward."
"To move forward. You've given up on having this country succeed."

### AI summary (High error rate! Edit errors on video page)

Predicted the Supreme Court's decision accurately, based on expert opinions.
Explained the real battle as between experts and those supporting Trump.
Emphasized the importance of respecting the Supreme Court's decision for the Constitution.
Criticized individuals undermining the Constitution while claiming to defend it.
Urged supporters of Trump to re-evaluate their beliefs and allegiance.
Encouraged people to choose between supporting the Constitution or the President.
Pointed out the lack of true constitutional scholars being brought onto certain media shows.
Anticipated internal turmoil for individuals as they grapple with their beliefs.
Stressed the importance of moving the country forward rather than dwelling in the past.
Reminded listeners of the goal of looking forward and progressing as a nation.

Actions:

for political observers, constitution defenders,
Re-evaluate your beliefs and allegiance (implied)
Support moving the country forward (implied)
</details>
<details>
<summary>
2020-12-11: Let's talk about Biden, cops, and tools.... (<a href="https://youtube.com/watch?v=Mn-ca6ZQYtw">watch</a> || <a href="/videos/2020/12/11/Lets_talk_about_Biden_cops_and_tools">transcript &amp; editable summary</a>)

Beau shares a story about having the right tools for the job, questioning the effectiveness of military equipment for law enforcement, and advocating for consent-based policing over militarization.

</summary>

"Tools for different jobs don't help."
"Law enforcement should be focusing more on moving towards consent-based policing than trying to turn itself into a military."
"In the risk versus reward, apparently there's only risk."

### AI summary (High error rate! Edit errors on video page)

Story about a friend needing help with a privacy fence but actually needing help moving agricultural fencing in the backyard.
Importance of having the right tools for the job, even if tasks seem similar.
Studies show that access to military equipment doesn't reduce crime or increase officer safety.
Lack of training for law enforcement agencies with access to military equipment.
Pushback against terminating programs that provide military equipment to law enforcement.
Emphasis on the difference between military and law enforcement roles and the tools they require.
Call for law enforcement to focus on consent-based policing rather than militarization.
Critique on the lack of evidence supporting the effectiveness of military equipment for law enforcement.
Hope for President Biden to end programs providing military equipment to law enforcement.
Emphasis on the risks associated with militarization without clear rewards.

Actions:

for community members, advocates.,
Advocate for consent-based policing to local law enforcement agencies (implied).
Support organizations pushing to terminate programs providing military equipment to law enforcement (implied).
</details>
<details>
<summary>
2020-12-10: Let's talk about Trump, Feinstein, and term limits.... (<a href="https://youtube.com/watch?v=ctSN4wXDwgM">watch</a> || <a href="/videos/2020/12/10/Lets_talk_about_Trump_Feinstein_and_term_limits">transcript &amp; editable summary</a>)

Beau questions the effectiveness of term limits, stressing that increased civic engagement is vital in holding politicians accountable and addressing corruption.

</summary>

"The voter is the term limit."
"People have to get involved. That's the actual solution."
"Increasing civic engagement is the key solution to addressing political issues."

### AI summary (High error rate! Edit errors on video page)

Reports suggest Senator Feinstein may not be as sharp due to age, sparking the term limits debate.
Beau questions the effectiveness of term limits in addressing the real issue.
The concern for a second Trump term is his potential to act without restraint.
The main reason people want term limits is due to corruption, ineffectiveness, and re-election of politicians.
Beau likens implementing term limits to buying a fire extinguisher after a fire; not addressing the root problem.
He explains how term limits could potentially lead to politicians rushing to exploit their positions.
Beau argues that voters should be responsible for removing corrupt or ineffective politicians.
Increasing civic engagement is presented as the key solution to addressing political issues.
Beau believes that limiting terms may not solve the problem but could serve as a temporary solution.
He stresses the importance of an informed and engaged populace in holding politicians accountable.

Actions:

for voters, citizens, activists,
Increase civic engagement by attending town hall meetings and engaging with local politics (implied).
Educate oneself on politicians' policies and actions rather than voting solely based on party affiliation (implied).
Actively participate in the political process by voting for candidates based on their policies and track record (implied).
</details>
<details>
<summary>
2020-12-10: Let's talk about Trump's latest move.... (<a href="https://youtube.com/watch?v=ts97r0zsCNo">watch</a> || <a href="/videos/2020/12/10/Lets_talk_about_Trump_s_latest_move">transcript &amp; editable summary</a>)

Texas lawsuit supported by Trump unlikely to succeed, revealing normal politics taken to an extreme.

</summary>

"It's the normal machinery of American politics."
"It's what we have come to tolerate and accept."
"Perhaps it is time to adjust some of the normal machinery of American politics."

### AI summary (High error rate! Edit errors on video page)

Texas filed a lawsuit supported by other states and Trump, but constitutional scholars doubt its success.
The Supreme Court is unlikely to invalidate votes, as it could be seen as deciding to end the United States.
Speculation exists that the lawsuit was filed for a possible pardon due to an indictment.
Some supporters may have been misled by Trump, while others seek political advantage.
Trump’s base may eventually realize they were deceived about the election outcome.
The potential misuse of funds collected from supporters could lead to dissatisfaction.
Money diverted to political action committees might explain some support for Trump.
The situation reveals the normal but problematic workings of American politics taken to an extreme.
Legal implications and precedents set by the lawsuit could lead to long-lasting legal issues.
Despite concerns, experts believe the lawsuit will likely fail to gain traction.

Actions:

for american citizens,
Question and stay informed about political actions and lawsuits (suggested)
Advocate for adjustments to improve the political system (suggested)
</details>
<details>
<summary>
2020-12-09: Let's talk about the Ft. Hood review, Vanessa Guillen,  and you.... (<a href="https://youtube.com/watch?v=RFFiv-to9jk">watch</a> || <a href="/videos/2020/12/09/Lets_talk_about_the_Ft_Hood_review_Vanessa_Guillen_and_you">transcript &amp; editable summary</a>)

Rare call to action led to investigation at Fort Hood revealing command failures; actions taken to prevent future incidents, though justice not served.

</summary>

"Command failed. They made 70 recommendations to make sure this sort of thing does not happen again."
"One of those who was relieved was a major general. One was a colonel. One was a command sergeant major."
"At the end of the day, did this bring justice for Vanessa Guillen or the other soldiers? No. Absolutely not."
"Your action combined with a whole bunch of other people's actions made this happen."
"Without enough pressure, this wouldn't have occurred."

### AI summary (High error rate! Edit errors on video page)

Rarely issues calls to action, but made one regarding an investigation at Fort Hood.
Investigation at Fort Hood happened due to public pressure after incidents like Vanessa Guillen's case.
Investigation findings revealed command failure and issued 70 recommendations for improvement.
Key finding: command knew of high risk to female soldiers and did nothing to mitigate.
Surveyed about 30,000 troops at Fort Hood, with 93 credible allegations from 500 women soldiers.
14 people in leadership positions relieved or suspended following the investigation.
Independent civilian review took action, but military's own investigation is ongoing.
Actions taken aim to change the climate at Fort Hood and prevent similar incidents in the future.
Investigation did not bring justice for Vanessa Guillen or other soldiers, but may lead to positive changes.
Acknowledges community's role in pressuring for the investigation and its outcomes.

Actions:

for community members, advocates,
Support live stream fundraiser on Friday (suggested)
Stay informed about ongoing actions and investigations at Fort Hood (implied)
</details>
<details>
<summary>
2020-12-09: Let's talk about cops refusing to enforce laws.... (<a href="https://youtube.com/watch?v=wl8BwT76frY">watch</a> || <a href="/videos/2020/12/09/Lets_talk_about_cops_refusing_to_enforce_laws">transcript &amp; editable summary</a>)

Law enforcement officers' selective enforcement raises questions about authority and accountability, undermining their role as public servants.

</summary>

"Sheriffs all over the country right now are showing that's not true. They are showing that when they enforce an unjust law, they are culpable."
"There are a lot of laws that significantly impact populations in a manner that is unfair. When officers choose to enforce that, they're making the choice."
"They're not supposed to be unaccountable people with guns. They're supposed to be public servants."
"If law enforcement does in fact have the ability to disregard the laws and regulations coming from elected officials, where does their authority come from?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Raises questions about law enforcement officers in California refusing to enforce mandates.
Notes that all law enforcement officers in California refusing to enforce mandates are sheriffs.
Explains that sheriffs have more power than chiefs of police and can prioritize law enforcement actions.
Mentions that sheriffs may delay enforcement if they doubt the constitutionality of a mandate.
Points out that sheriffs cannot simply refuse to enforce laws based on personal preference.
Suggests that sheriffs may deprioritize enforcement based on perceived unconstitutionality.
Argues that sheriffs choosing not to enforce certain laws show they have discretion and are not just robots.
Raises concerns about potential biases when law enforcement officers choose which laws to enforce.
Questions the source of authority for law enforcement officers if they can disregard elected officials' laws.
Criticizes the idea of law enforcement being autonomous and unaccountable, advocating for them to be public servants.

Actions:

for community members, activists,
Question law enforcement actions (suggested)
Advocate for accountability in law enforcement (implied)
</details>
<details>
<summary>
2020-12-08: Let's talk about Trump, Star Trek, and a galactic federation.... (<a href="https://youtube.com/watch?v=IBi0jz7Zqas">watch</a> || <a href="/videos/2020/12/08/Lets_talk_about_Trump_Star_Trek_and_a_galactic_federation">transcript &amp; editable summary</a>)

Beau delves into a story about the Federation, reflecting on humanity's shortcomings and the potential for change to avoid a Mad Max future.

</summary>

"We're very well aware of the fact we are behaving as a failed species, but we're not doing much to change it."
"We know the world is messed up, and we're not doing much to change it."
"Not technology."
"He believed there were alien beings in space, he would, I don't know, build a wall in space or something."
"We're going to have to face the fact that we have to change."

### AI summary (High error rate! Edit errors on video page)

A story about the Federation prompts an entertaining reaction on social media, involving aliens in contact with governments.
Humans are not considered ready to join an exclusive club of intelligent beings in the galactic federation.
The response to this story suggests that humanity is seen as the "bad neighborhood" in the galaxy.
Lack of a warp drive is cited as a hindrance, but most people point to the state of the world as the reason for not being part of the Federation.
Issues like food distribution, wars for resources, homelessness, and scientific focus on weapons are cited as reasons why Earth is seen as a problematic planet.
Science fiction allows for political discourse in a different context, exploring the idea of advanced species wanting to help lesser planets.
There is a belief that certain technological and philosophical milestones need to be met to join the Federation, influenced by science fiction.
Beau questions whether the rejection of Earth's application to the Federation is due to humanity's acknowledgment of its failed state and lack of significant change efforts.
He suggests that advanced beings might see Earth as a species that needs to be civilized and brought to a better future.
The story includes an unbelievable aspect of Donald Trump being aware of the galactic federation but choosing not to talk about it.

Actions:

for sci-fi enthusiasts, advocates for societal change.,
Start actively participating in initiatives that address food distribution and homelessness (implied).
Advocate for peaceful resolutions to conflicts and work towards resource sharing (implied).
Support scientific advancements focused on improving life for all beings (implied).
</details>
<details>
<summary>
2020-12-08: Let's talk about Trump, Biden, and reaching Safe Harbor.... (<a href="https://youtube.com/watch?v=UKysWnZ-NXU">watch</a> || <a href="/videos/2020/12/08/Lets_talk_about_Trump_Biden_and_reaching_Safe_Harbor">transcript &amp; editable summary</a>)

The safe harbor concept ensures electoral results can't be challenged in Congress, making significant changes unlikely.

</summary>

"Anything now that's going to change the outcome [of the election] would have to be just ridiculously bizarre."
"There's nothing even remotely normal left that could alter the outcome of the election."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of safe harbor in relation to the electoral college.
Mentions that if a state certifies their election results and resolves legal challenges by a certain date, those results enter safe harbor.
Notes that almost every state, except Wisconsin, has completed this process.
States that the overwhelming majority of states' results won't be challenged in Congress.
Mentions that even if Wisconsin's electoral votes were not counted for Biden, he still has enough to win.
Points out that the electoral college doesn't meet until the 14th.
Expresses skepticism about any potential challenges having an impact at this stage.
Talks about the possibility of Congress objecting to electors, with a Republican member planning to object in the House.
Explains the process for objections to have an impact on the election outcome.
States that the Trump administration's legal options are running out.
Concludes that there is little realistic chance of any significant change in the election outcome.

Actions:

for political observers,
Monitor updates on the electoral process (implied)
Stay informed about legal challenges and election results (implied)
</details>
<details>
<summary>
2020-12-07: Let's talk about the future of water.... (<a href="https://youtube.com/watch?v=ROPvC2nlUUA">watch</a> || <a href="/videos/2020/12/07/Lets_talk_about_the_future_of_water">transcript &amp; editable summary</a>)

Beau introduces the concept of water futures, indicating the severity of global water scarcity and urging proactive measures through industry recognition and tangible tools.

</summary>

"There is a whole futures market dedicated to water now. That's a thing. It exists."
"This is a good tool because there are a whole lot of people out there that do not believe this issue is real."
"Titans of industry here. They're saying, we need to start paying attention to this."
"It's also handing us a tool that we can use to show people the impacts of this are real."

### AI summary (High error rate! Edit errors on video page)

Water dripping onto a tin roof during the video sets a dystopian tone, despite not being a satire.
Water futures, a new concept, allow locking in prices for commodities like large consumers and sellers.
Speculation is enabled through futures, with price fluctuations driven by supply and demand.
The NASDAQ-Valez California Water Index, symbol NQH20, indicates significant price increases since its introduction in 2018.
Two billion people currently face water scarcity, with projections of two-thirds facing shortages by 2025.
Despite water covering the world, fresh water scarcity is a critical issue due to accessibility.
The futures market for water is a tangible tool to showcase the reality of water scarcity to skeptics of environmental issues.
The emergence of water futures signifies industry leaders recognizing the pressing need to address water scarcity.
Utilizing the futures market can help demonstrate the concrete impacts of climate change and environmental degradation.
Beau advocates for proactive changes to address water scarcity before it becomes an overwhelming issue for the general population.

Actions:

for climate advocates, environmentalists,
Educate communities on the significance of water futures and their implications (suggested)
Advocate for proactive measures to address water scarcity in local communities (implied)
</details>
<details>
<summary>
2020-12-07: Let's talk about Trump's legal challenges and the high-water mark.... (<a href="https://youtube.com/watch?v=aPXpzpY9WZc">watch</a> || <a href="/videos/2020/12/07/Lets_talk_about_Trump_s_legal_challenges_and_the_high-water_mark">transcript &amp; editable summary</a>)

Beau warns against celebrating too soon post-Trump, stressing the need for sustained political engagement to address policy issues.

</summary>

"We can't let the removal of President Trump become the high watermark."
"It's his policies that were bad. It's his policies that we have to fight to undo."
"We have to stay politically active and stay engaged so he responds, so the government responds and takes care of those of us down here on the bottom."
"If we make it about a person, if we make it about Trump out, and that's the goal, we're going to lose a whole lot of people who think the battle's already over."
"It's really unlikely that they're going to amount to anything, but it can create a situation in which people believe victory has been achieved when the fight hasn't even started yet."

### AI summary (High error rate! Edit errors on video page)

Analyzing President Trump's post-election legal endeavors and their impact.
Doubts Trump's ability to change election outcome through legal means.
Warns of the risks and damage caused by Trump's activities.
Shift in focus from policy to Trump himself due to legal battles.
Emphasizes the importance of not letting Trump's removal be the peak of political engagement.
Urges continued engagement post-Trump era to address existing problems and policies.
Stresses the need to hold Biden accountable and push for positive change post-Trump.
Concerns about people viewing Trump's exit as the end goal rather than a step in ongoing progress.
Calls for sustained political activism to ensure government responsiveness to people's needs.
Warns against prematurely celebrating victory when challenges still exist post-Trump.

Actions:

for activists, voters, citizens,
Stay politically active and engaged to ensure government responsiveness to people's needs. (implied)
Continue fighting against harmful policies enacted by President Trump even after his departure. (implied)
Push for positive change post-Trump era by holding Biden accountable and advocating for progress. (implied)
</details>
<details>
<summary>
2020-12-06: Let's talk about how the stock market isn't the economy.... (<a href="https://youtube.com/watch?v=80DeGZ_M-yg">watch</a> || <a href="/videos/2020/12/06/Lets_talk_about_how_the_stock_market_isn_t_the_economy">transcript &amp; editable summary</a>)

Beau breaks down the disconnect between the stock market and the struggling economy, urging for sustainable economic changes.

</summary>

"All of this is bad news for us."
"The stock market is not the economy."
"How the stock market is performing does not typically affect the way a whole lot of people are getting food on their table."

### AI summary (High error rate! Edit errors on video page)

Provides an overview of the economy, stock market, and jobs report for November.
Initially, the job report of almost a quarter million new jobs created seems positive.
However, job growth is actually slowing down and trending downwards.
Currently, there are still 10 million jobs less than before the economic downfall.
It is projected to take about four years to recover the lost jobs at the current rate.
The early job gains were mainly due to people returning to work after temporary layoffs.
Long-term unemployment is rising, making it harder for people to re-enter the job market.
Benefits for the unemployed are running out, indicated by a significant increase in long-term unemployed individuals.
The total number of long-term unemployed individuals has reached 3.9 million.
The jobless rate for the black community is significantly higher at 10.3% compared to 5.9% for white individuals.
Despite the grim job market outlook, the stock market reacted positively to the news, expecting more stimulus.
There is a significant disconnect between the stock market's performance and the real economy.
The stock market is not an accurate reflection of how many people are struggling to make ends meet.
Beau suggests that there needs to be a change in the way the economy operates to address these challenges effectively.

Actions:

for economic policymakers, activists,
Advocate for sustainable economic policies to address job market challenges (suggested)
Support initiatives that focus on helping long-term unemployed individuals re-enter the job market (implied)
</details>
<details>
<summary>
2020-12-05: Let's talk about leadership, mandates, Trump, and Biden.... (<a href="https://youtube.com/watch?v=w1pGDnmYxx4">watch</a> || <a href="/videos/2020/12/05/Lets_talk_about_leadership_mandates_Trump_and_Biden">transcript &amp; editable summary</a>)

Beau compares leadership styles, advocates for mask-wearing without mandating, and stresses following health guidelines to protect loved ones during the ongoing pandemic.

</summary>

"Biden is going to ask people to wear masks. He's not going to mandate it either."
"Encourage it, incentivize it, do everything you can to get people to get the vaccine, but you can't really mandate it."
"Do everything that you can, even if you're going to disregard that."
"Take every precaution that you can. Mitigate in every way that you can."
"If you mess this up, Granny June probably won't see spring."

### AI summary (High error rate! Edit errors on video page)

Comparing leadership styles of the outgoing Trump administration and incoming Biden administration.
Biden planning to ask people to wear masks for the first hundred days, facing pushback.
Emphasizing the use of numbers for certainty and drawing people in.
Duration of mask-wearing until a safe and available vaccine is widely distributed.
Lack of a public distribution plan despite assurances from the Trump administration.
Mentioning a website that may indicate where individuals fall in the vaccine distribution line.
Biden's approach of setting an example rather than mandating masks or vaccines.
Encouraging and incentivizing vaccine uptake without mandating it.
Acknowledging resistance but focusing on leading by example.
Urging people not to travel during the upcoming holidays for safety.
Stressing the importance of following health guidelines even if choosing to travel.
Reminding to take precautions to protect vulnerable loved ones during the pandemic.

Actions:

for health-conscious individuals,
Follow health guidelines strictly: wear masks, maintain distance, wash hands (implied)
Encourage and support vaccine uptake without mandating it (suggested)
</details>
<details>
<summary>
2020-12-05: Let's talk about Trump, Biden, and a win for DACA.... (<a href="https://youtube.com/watch?v=0eOZuKLcUhI">watch</a> || <a href="/videos/2020/12/05/Lets_talk_about_Trump_Biden_and_a_win_for_DACA">transcript &amp; editable summary</a>)

In 2012, DACA protected undocumented individuals brought to the US as children, and recent legal developments offer hope for their future under Biden's presidency.

</summary>

"Start off your weekend with some good news."
"This is a win."
"They can stay here, but they're not citizens."
"They're stuck in limbo."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

In 2012, President Obama established DACA to protect individuals brought to the US as children without authorization, providing them with a sense of belonging.
Trump attempted to end DACA in 2017, but the Supreme Court's ruling reinstated the program, safeguarding hundreds of thousands of individuals.
Chad Wolf, the acting head of DHS, hindered new DACA applicants and reduced work authorization to one year.
A federal judge found Wolf's appointment unlawful, rendering his memo invalid and ordering DHS to resume accepting new DACA applications.
With Biden's upcoming presidency, there is hope for DACA recipients as he is a vocal supporter of the program and may work towards a pathway to citizenship for them.
Despite potential appeals from the Trump administration, it is unlikely that DACA will be terminated, and progress towards citizenship for DACA recipients may be on the horizon.

Actions:

for advocates for immigrant rights,
Support organizations aiding DACA recipients (suggested)
Stay informed about DACA updates and advocacy opportunities (implied)
</details>
<details>
<summary>
2020-12-04: Let's talk about McConnell and fixing democracy.... (<a href="https://youtube.com/watch?v=1zz_awB3CN0">watch</a> || <a href="/videos/2020/12/04/Lets_talk_about_McConnell_and_fixing_democracy">transcript &amp; editable summary</a>)

Beau suggests two critical changes on Capitol Hill to enhance accountability and transparency in American politics, advocating for bills passed in one chamber to be voted on by the other and for negotiations between parties to be televised.

</summary>

"These two small changes if something is passed in one chamber it has to be voted on in the other and televising all negotiations between parties, live streaming, that would forever alter American politics and it would make our politicians more accountable."
"If we want a representative democracy and people keep talking about how democracy is being undermined because it is right now. Maybe we should make it a little more resilient."
"They view themselves as your rulers and because we can't hold them accountable for their actions. They kind of are."
"It's an important concept when you're talking about representative democracy and right now it's very hard to hold them accountable."
"If that happens and the Democratic Party gets control of the House and the Senate this should be one of the first changes to the rules they make."

### AI summary (High error rate! Edit errors on video page)

Proposes two small changes on Capitol Hill to enhance accountability of representatives and make American politics more transparent.
Suggests that any bill passed by one chamber of the House should be voted on by the other to avoid bills being held up by powerful individuals like Mitch McConnell.
Points out the issue of negotiations being conducted in secret, leading to blame games between parties when they fail.
Advocates for televising negotiations between parties to show the public the process and prevent politicians from blaming each other.
Emphasizes the importance of accountability in a representative democracy and the current challenges in holding politicians accountable.
Argues that these changes could significantly impact American politics by making politicians more accountable to the people.
Urges the people, especially in Georgia, to hold politicians like Mitch McConnell accountable through voting.
Calls for the Democratic Party, if in control, to prioritize implementing these changes to strengthen democracy and ensure accountability.
Criticizes politicians who shield themselves from accountability and view themselves as rulers rather than employees of the people.
Encourages viewers to think about making politicians more accountable and resilient in a representative democracy.

Actions:

for politically engaged citizens,
Contact your representatives to advocate for bills passed in one chamber to be voted on by the other and for negotiations between parties to be televised (suggested).
Vote in elections, especially in Georgia, to hold politicians like Mitch McConnell accountable (implied).
</details>
<details>
<summary>
2020-12-03: Let's talk about how we can help the police.... (<a href="https://youtube.com/watch?v=_sa56eYiuAU">watch</a> || <a href="/videos/2020/12/03/Lets_talk_about_how_we_can_help_the_police">transcript &amp; editable summary</a>)

Beau suggests reallocating responsibilities from law enforcement to more suitable professionals to improve efficiency and effectiveness in public safety, making complete sense and rejecting opposition based on manipulation.

</summary>

"We can't ask cops to handle everything."
"Opposing this is just a show of being manipulated."
"There's no reason to oppose it. It makes complete sense."
"Why not want the most effective people dealing with something?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Recognizes the strain on law enforcement in the U.S. and the need to alleviate their burden.
Suggests that law enforcement is tasked with too many responsibilities that could be handled better by other professionals.
Proposes reallocating some of law enforcement's responsibilities to counselors, social workers, and other programs.
Mentions situations like truancy, homeless calls, and petty possession of substances that could be handled more effectively by non-law enforcement personnel.
Advocates for allowing law enforcement to focus on situations where their presence is truly needed.
Addresses concerns about funding by suggesting that reducing law enforcement responsibilities can free up funds for other programs.
Comments on the political manipulation surrounding the issue of defunding the police at the federal level.
Argues that reducing law enforcement's involvement in certain areas can lead to safer outcomes for everyone.
Emphasizes the practicality and sense in supporting a more efficient allocation of resources and responsibilities.
Concludes by urging for a shift in perspective towards more effective and sensible approaches to public safety.

Actions:

for policy advocates, community leaders,
Advocate for reallocating certain responsibilities from law enforcement to counselors and social workers (suggested)
Support programs that assist with truancy, homelessness, and substance abuse to reduce law enforcement involvement (implied)
Educate communities on the benefits of reallocating funding to support alternative programs (exemplified)
</details>
<details>
<summary>
2020-12-03: Let's talk about how Trump's great speech left me confused.... (<a href="https://youtube.com/watch?v=CNrrhC0z5yI">watch</a> || <a href="/videos/2020/12/03/Lets_talk_about_how_Trump_s_great_speech_left_me_confused">transcript &amp; editable summary</a>)

Beau Gion listens to Trump's speech, questions donation allocation, and wonders about the lack of results despite four years in office.

</summary>

"Democrats are bad, all right? It's that simple."
"We expect to see, you know, a return on our investment."
"He just spent 45 minutes telling us nothing, acting like he had everything under control."
"And the other part about this bothered me. He's saying that he knew this was all coming and that he knew about all this stuff from the beginning."
"I don't know what to think anymore because he just spent 45 minutes telling us nothing."

### AI summary (High error rate! Edit errors on video page)

Subscribed to President Trump's updates and eagerly awaited a concise speech.
Prepared to defend his support after being called an uneducated voter by liberal in-laws.
Spent an hour and a half watching Trump's speech, hoping for evidence of voter fraud.
Believes Trump's focus on late mail-in votes was a strategic move rather than a concern.
Received a mass text message from Trump asking for more money for his defense fund.
Discovered that 75% of donations could go to a political action committee called Save America.
Expresses faith in Trump's honesty and integrity despite donation allocation concerns.
Simplifies his view by stating that Democrats are bad and Trump is a good businessman.
Expects a return on investment from the donations made to Trump's defense fund.
Feels conflicted about Trump's repetitive messaging and lack of concrete results.
Questions why Trump, with four years in office, didn't address the issues he now speaks about.

Actions:

for supporters reevaluating trump.,
Question the allocation of donations to political action committees (implied).
Demand transparency and results from political figures you support (implied).
</details>
<details>
<summary>
2020-12-02: Let's talk about Trump, the GOP, knowledge, and wisdom.... (<a href="https://youtube.com/watch?v=hxPey5-09CA">watch</a> || <a href="/videos/2020/12/02/Lets_talk_about_Trump_the_GOP_knowledge_and_wisdom">transcript &amp; editable summary</a>)

Trump's lingering influence on the GOP forces a critical choice: confront or enable his destructive power.

</summary>

"They can't cut him off. They had the chance. They had the chance."
"You have an opportunity now, just like you did before. Depends on whether or not you're going to take it."
"He will destroy the Republican Party and a whole lot of politicians along with him because you wouldn't stand up to your own creation."

### AI summary (High error rate! Edit errors on video page)

Trump hints at running in 2024, prompting the Republican Party to address their options.
The GOP initially viewed Trump as Frankenstein's monster, a creation they needed for votes.
Even after leaving office, Trump's influence over the party remains strong.
Republicans fear Trump's Twitter attacks if they oppose him.
Barr, a Trump loyalist, denies election wrongdoing to protect the party's political interests.
The GOP struggles to cut ties with Trump despite internal threats and lack of courage.
Beau questions if the GOP possesses knowledge or wisdom in handling the Trump dilemma.
Cutting ties with Trump now may anger the base temporarily, but the impact will fade over time.
Allowing Trump to remain relevant risks damaging exposés from former officials seeking profit.
Beau warns that failure to stand up to Trump will lead to long-term consequences for the party.

Actions:

for republicans, political activists,
Challenge Trump's influence within the party (implied)
Advocate for political courage and integrity within the GOP (implied)
</details>
<details>
<summary>
2020-12-02: Let's talk about Trump's promised veto.... (<a href="https://youtube.com/watch?v=mEEJ6LdKbnQ">watch</a> || <a href="/videos/2020/12/02/Lets_talk_about_Trump_s_promised_veto">transcript &amp; editable summary</a>)

President Trump might veto the defense spending bill over Twitter backlash, but it's likely his bluff will be called as the NDAA is expected to pass, providing an opportune moment for both parties to take a stand.

</summary>

"Call his bluff."
"Let him do it."
"It's the NDAA. It always gets through."
"Stop playing his games."
"He's on his way out."

### AI summary (High error rate! Edit errors on video page)

President Trump might veto the defense spending bill because he's upset with Twitter fact-checking him.
Trump is demanding to insert something into the bill that repels Section 230 of the Communications Decency Act.
Section 230 protects companies like Twitter from lawsuits related to user-generated content.
There's uncertainty if Trump also plans to veto the bill over renaming bases named after Confederate generals.
Politicians are unsure how to respond to Trump's declaration.
Beau suggests calling Trump's bluff or letting him veto the bill, which includes a pay raise for troops before Christmas.
The bill in question, the NDAA, has a long history of bipartisan support and is likely veto-proof.
Overriding Trump's veto on the NDAA could be the perfect end to his administration.
This is an opportune moment for the Republican Party to distance themselves from Trump.
Democrats should not miss the chance to send the bill for Trump to potentially veto.
Beau doubts Trump will actually veto the bill, as it may be a lasting source of mockery for him.
The NDAA bill typically passes without issues, and there's no need to give in to Trump's demands.
Beau encourages letting Trump upset the people of Georgia and believes the bill will eventually go through.

Actions:

for politically engaged citizens,
Call out Trump's bluff and urge politicians to stand firm against his demands (implied)
Support bipartisan efforts to ensure the NDAA bill passes without giving in to Trump's veto threats (implied)
</details>
<details>
<summary>
2020-12-01: The roads to the second channel.... (<a href="https://youtube.com/watch?v=9copM6bMXZE">watch</a> || <a href="/videos/2020/12/01/The_roads_to_the_second_channel">transcript &amp; editable summary</a>)

Beau introduces the longer, more in-depth second channel with historical deep dives, practical demonstrations, community networking focus, and plans for in-person events.

</summary>

"We'll be traveling and helping some of y'all set up your own community networks or meet up with those that are already running."
"So we're definitely moving into a new chapter here, and we're taking the show on the road."
"Don't want to force this on anybody."
"Y'all are going to become part of the show here."
"It may not be something that is for everybody."

### AI summary (High error rate! Edit errors on video page)

Introducing the second channel and its purpose.
The second channel will feature longer, more in-depth, and edited content than the first one.
Historical deep dives and practical demonstrations will be a part of the content.
Filming on location and using entire cities to provide context for issues.
Focus on community networking and actively demonstrating it.
Plans to help set up community networks and assist existing ones in achieving their goals.
Mention of in-person events once public health concerns clear up.
Viewers will become part of the show, with plans for more produced and edited satire.
Explanation for creating a second channel rather than combining all content on the first one.
Acknowledgment that content on the second channel may not be for everyone due to longer duration and different type.
Updates on the second channel will not be daily but aim for a weekly release once workflow is streamlined.

Actions:

for content creators, community organizers,
Set up your own community networks or meet with existing ones (implied)
Share feedback, suggestions, and topics in the comments (implied)
</details>
<details>
<summary>
2020-12-01: Let's talk about the GOP's talking point about mismanaged states.... (<a href="https://youtube.com/watch?v=lI_QZpT0KvU">watch</a> || <a href="/videos/2020/12/01/Lets_talk_about_the_GOP_s_talking_point_about_mismanaged_states">transcript &amp; editable summary</a>)

GOP stalls stimulus, blaming mismanaged states, while denying people access to their own money during a crisis.

</summary>

"If New York's in debt, why should Florida bear it?"
"They're denying you access to your money."
"They can suffer the consequences of the federal government's mismanagement."

### AI summary (High error rate! Edit errors on video page)

GOP stalling stimulus, citing not wanting to bail out mismanaged states.
Rick Scott from Florida supports GOP's stance, despite the state's own mismanagement issues.
Florida's economy heavily relies on tourism, which is at risk if other states' economies tank.
Unemployment funding comes from the people accessing it, not senators denying access.
States being called mismanaged contribute the most to federal taxes.
Mismanagement in states originated from the Republican Party at the federal level.
GOP had no issue bailing out large companies but hesitates to support average people.
Beau questions why average people should suffer the consequences of federal government mismanagement.

Actions:

for advocates for fair economic relief.,
Contact local representatives to advocate for fair distribution of stimulus funds (implied).
Support community organizations working to provide relief to those impacted by the economic crisis (generated).
</details>
<details>
<summary>
2020-12-01: Let's talk about Rosa Parks and some lesser known facts.... (<a href="https://youtube.com/watch?v=f-BT6erndV0">watch</a> || <a href="/videos/2020/12/01/Lets_talk_about_Rosa_Parks_and_some_lesser_known_facts">transcript &amp; editable summary</a>)

Beau sheds light on the lesser-known facts about Rosa Parks, stressing the depth of her activism and commitment beyond the familiar narrative of tiredness.

</summary>

"The courage and bravery that it takes to say, no, I'm not moving. That can't be taught."
"Boiling it down to her being a seamstress and just being tired, not wanting to move. I don't think that's a fair representation of the commitment it takes."

### AI summary (High error rate! Edit errors on video page)

Beau addresses Rosa Parks' story, pointing out some lesser-known facts about her.
Rosa Parks is not accurately represented in the story of her refusing to give up her seat on December 1st, 1955.
It is mentioned that Rosa Parks had been battling injustice for more than a decade before her famous act.
She had been involved with the NAACP since 1943, indicating a long history of activism.
Rosa Parks was carefully chosen by the NAACP to be a symbol and carry a message due to her potential to win in court.
Parks attended the Highlander Folk School in Tennessee, where she was mentored by Septima Clark, known as the "mother of the movement."
The courage to refuse to give up her seat cannot be taught, but the ability to carry a message can be learned.
Beau stresses the importance of workshops, training, and organization in social change efforts.
He mentions that Rosa Parks worked for a liberal couple, the Durrs, who were also active in the civil rights movement.
The story of Rosa Parks goes beyond just being tired; it's about a long-standing commitment to fighting injustice.

Actions:

for activists, history buffs,
Attend workshops or training sessions focused on activism and social change (suggested)
Support organizations like the NAACP that work towards justice and equality (exemplified)
</details>
