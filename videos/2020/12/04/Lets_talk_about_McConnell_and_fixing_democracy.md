---
title: Let's talk about McConnell and fixing democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1zz_awB3CN0) |
| Published | 2020/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Proposes two small changes on Capitol Hill to enhance accountability of representatives and make American politics more transparent.
- Suggests that any bill passed by one chamber of the House should be voted on by the other to avoid bills being held up by powerful individuals like Mitch McConnell.
- Points out the issue of negotiations being conducted in secret, leading to blame games between parties when they fail.
- Advocates for televising negotiations between parties to show the public the process and prevent politicians from blaming each other.
- Emphasizes the importance of accountability in a representative democracy and the current challenges in holding politicians accountable.
- Argues that these changes could significantly impact American politics by making politicians more accountable to the people.
- Urges the people, especially in Georgia, to hold politicians like Mitch McConnell accountable through voting.
- Calls for the Democratic Party, if in control, to prioritize implementing these changes to strengthen democracy and ensure accountability.
- Criticizes politicians who shield themselves from accountability and view themselves as rulers rather than employees of the people.
- Encourages viewers to think about making politicians more accountable and resilient in a representative democracy.

### Quotes

- "These two small changes if something is passed in one chamber it has to be voted on in the other and televising all negotiations between parties, live streaming, that would forever alter American politics and it would make our politicians more accountable."
- "If we want a representative democracy and people keep talking about how democracy is being undermined because it is right now. Maybe we should make it a little more resilient."
- "They view themselves as your rulers and because we can't hold them accountable for their actions. They kind of are."
- "It's an important concept when you're talking about representative democracy and right now it's very hard to hold them accountable."
- "If that happens and the Democratic Party gets control of the House and the Senate this should be one of the first changes to the rules they make."

### Oneliner

Beau suggests two critical changes on Capitol Hill to enhance accountability and transparency in American politics, advocating for bills passed in one chamber to be voted on by the other and for negotiations between parties to be televised.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to advocate for bills passed in one chamber to be voted on by the other and for negotiations between parties to be televised (suggested).
- Vote in elections, especially in Georgia, to hold politicians like Mitch McConnell accountable (implied).

### Whats missing in summary

The full transcript provides detailed insights into the current lack of accountability in American politics and offers practical solutions to strengthen democracy by making politicians more answerable to the public. 

### Tags

#Accountability #PoliticalTransparency #RepresentativeDemocracy #MitchMcConnell #CapitolHill


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about two small
changes that could be made up on Capitol Hill that would forever alter the American political
landscape. Most importantly it would make it easier for employers to review the performance
of their employees. Employers being you. Employees being those people we send up to DC.
Because you have two things that are just accepted. It's the way they've always been done.
That occur on a pretty regular basis that really undermine the ideals of a representative democracy.
Right now they both involve Senator Mitch McConnell.
See two things are going on at the same time. One you have the stimulus negotiations
and the other is that you had the House pass a bill on decriminalization and it's pretty
likely that McConnell's just going to do nothing with it and let it go stale. Let it go away.
These are both pretty normal things. We see it all the time. However slight tweaks in the way
these things get done would make our representatives more accountable. They're not rulers,
they're employees. So with the bill it would be a pretty simple task to make sure that any bill
gets passed by one chamber of the house has to be voted on by the other.
As it stands right now you have Mitch McConnell a person who is very secure in their re-election
and a person who is very efficient, capable of shielding the rest of his party from any scrutiny.
See he can hold this bill up and it just dies. It just goes away.
Which means the rest of the country doesn't get to know how their senator would have voted on it.
We can't hold them accountable because they can always blame McConnell.
Oh that wasn't me. I would have voted for it. I would have done something the overwhelming majority
of Americans want done but you know Senate Majority Leader McConnell wouldn't let me
and they get off the hook. We can't review their performance. We can't hold them accountable.
The other is these negotiations done in secret. No one else is in the room where it happens right.
It's a facet of American politics that needs to go away. We see it all the time.
Representatives from two opposing parties walk into a room. The negotiations fail. They walk out and blame each other.
Televise them. Don't let them be done in secret. Show us how the sausage is made.
Show us what happens. Then they wouldn't be able to pull that game. They wouldn't be able to blame
each other. We would get to see it happen. Now this has been proposed before and the standard
answer to this to those who don't want to do it because they don't want any accountability is that
well the deals would just get made somewhere else and then they put on a show for the camera
and yeah that would probably happen a few times until the very first time somebody agreed to
something in one of those backroom deals in principle and then got in front of the cameras
and didn't agree to it and then the negotiations start for real. Once that happens once
politicians won't trust that backdoor method. They won't put any faith in it. They certainly
wouldn't stake their political careers on it. Accountability. It's an important concept when
you're talking about representative democracy and right now it's very hard to hold them accountable.
These two small changes if something is passed in one chamber it has to be voted on in the other
and televising all negotiations between parties, live streaming, that would forever alter
American politics and it would make our politicians more accountable
and then it would be up to the employers to hold them accountable.
Right now there is no way for us to do this. We can't hold politicians accountable.
However, we can hold Mitch McConnell accountable. Right now he has all of this power. One person
is able to subvert the will of the American people because he's the Senate majority leader
and there's a way to change that. I'm looking at you Georgia.
If that happens and the Democratic Party gets control of the House and the Senate
this should be one of the first changes to the rules they make.
If we want a representative democracy and people keep talking about how democracy is
being undermined because it is right now. If we want a representative democracy
and people keep talking about how democracy is being undermined because it is right now.
Maybe we should make it a little more resilient. Make people a little bit more accountable for
their actions. This would be a way to do it and I find it hard to believe that the party of personal
responsibility would be too stiff in their opposition to this. It would be a bad move
politically for them. They always want employers to be able to fire their employees at will.
They just want to shield themselves from that because they don't view themselves as your
employees. They view themselves as your rulers and because we can't hold them accountable
for their actions. They kind of are. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}