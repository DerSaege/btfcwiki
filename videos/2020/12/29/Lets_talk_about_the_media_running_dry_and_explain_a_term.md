---
title: Let's talk about the media running dry and explain a term....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5Hw3SrCHxmw) |
| Published | 2020/12/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Media uses a term to keep viewers glued to the screen, letting imagination run wild.
- Talks about the bystander effect and how knowing about it can prevent succumbing to it.
- Explains the concept of "dry runs" in gauging security posture and rehearsal.
- Emphasizes that a dry run is not meant to be dramatic and won't make the news.
- Describes a dry run scenario of parking a vehicle to test locals' reactions without causing harm.
- States that a dry run will never end in a catastrophic event like a giant fireball.
- Mentions that destroying a street during a dry run makes observations irrelevant.
- Criticizes the manipulation of people through fear using the concept of dry runs inaccurately.
- Encourages viewers to be informed about what a dry run truly is to avoid being manipulated by fear tactics.
- Concludes by suggesting that understanding the concept can prevent anxiety and skepticism towards sensationalized media.

### Quotes

- "If it's a dry run, it doesn't make the news."
- "No dry run will ever be dramatic. It's designed to not be."
- "This gets used to manipulate people through fear."
- "Hopefully, now that you know what a dry run is, this won't work on you anymore."
- "Y'all have a good day."

### Oneliner

Beau debunks media sensationalism by explaining the truth behind "dry runs," urging viewers to resist fear manipulation through knowledge.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Educate others on the concept of "dry runs" to prevent fear manipulation (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of how media exploits the concept of dry runs to maintain viewer engagement and spread fear, urging individuals to stay informed and skeptical.

### Tags

#Media #FearManipulation #BystanderEffect #Security #Sensationalism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the media running dry
and how they use a term
to keep you glued to the screen,
to keep you tuned in, to let your imagination run wild.
You know, in psychology there's
an idea called the bystander effect.
The cool thing about it is that if you know about the bystander effect
you are less likely to succumb to the bystander effect because you know it's a thing.
So we're going to try to apply that logic to something else.
Almost any time something like what happened in Tennessee happens
and it's not quite dramatic enough,
the counts just aren't high enough and it's not getting the ratings that
it seems like it should,
some commentator will say
this could have been a dry run.
The viewer is left with the idea that
there's going to be a follow-up,
that something more dramatic is going to happen
and if they stay tuned they'll find out what it is, film at 11.
So today we're going to talk about dry runs,
what they are and what they are not.
A dry run is a method of gauging
the opposition's security posture.
It's a rehearsal
and it is dry.
Nothing happens.
If it's a dry run,
it doesn't make the news.
A dry run for that would have been like parking a similar vehicle in that area
and watching it
and seeing how long
it took
the locals to notice it, to investigate it.
That would be a dry run.
What I can assure you
is that unless something went horribly wrong,
no dry run will ever end with a giant fireball
in the middle of a major metropolitan area.
That's not a thing.
The idea is to gauge the opposition's security posture.
If you destroy a street,
their posture will probably change. They may go on a higher alert level.
It would stand to reason,
making all of your observations
completely irrelevant.
In all of the years
that I have been watching the news, I have seen this term used correctly
once.
Once.
And it was somebody trying to explain
why a whole bunch of small planes were flying around an area
like that they shouldn't be in.
And nobody knew why.
And to my knowledge, nobody ever found out why.
That makes more sense.
It's not dramatic.
No dry run will ever be dramatic.
It's designed to not be.
It's to test,
to see what it is possible
to get away with.
When it is done,
it is dry.
There is normally not anything
even remotely incriminating on the people,
in the vehicle,
anything like that.
So they can just say, oh, I didn't know I was supposed to park here. I wasn't supposed to park here.
Um...
This gets used to
manipulate people through fear.
And what's worse
is that most times
when this is said, it is said by somebody that has credentials
that lead me to believe
they know, or at least they should know,
that what they're saying is inaccurate.
But it keeps people tuned in.
Hopefully,
like the bystander effect,
now that you know what a dry run is,
this won't work on you anymore.
It won't elevate your
sense of anxiety
because you're going to sit there and look at the screen and be like, no it's not.
That's silly what they just said.
Because now you know.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}