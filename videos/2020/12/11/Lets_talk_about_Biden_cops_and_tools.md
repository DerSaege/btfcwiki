---
title: Let's talk about Biden, cops, and tools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Mn-ca6ZQYtw) |
| Published | 2020/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Story about a friend needing help with a privacy fence but actually needing help moving agricultural fencing in the backyard.
- Importance of having the right tools for the job, even if tasks seem similar.
- Studies show that access to military equipment doesn't reduce crime or increase officer safety.
- Lack of training for law enforcement agencies with access to military equipment.
- Pushback against terminating programs that provide military equipment to law enforcement.
- Emphasis on the difference between military and law enforcement roles and the tools they require.
- Call for law enforcement to focus on consent-based policing rather than militarization.
- Critique on the lack of evidence supporting the effectiveness of military equipment for law enforcement.
- Hope for President Biden to end programs providing military equipment to law enforcement.
- Emphasis on the risks associated with militarization without clear rewards.

### Quotes

- "Tools for different jobs don't help."
- "Law enforcement should be focusing more on moving towards consent-based policing than trying to turn itself into a military."
- "In the risk versus reward, apparently there's only risk."

### Oneliner

Beau shares a story about having the right tools for the job, questioning the effectiveness of military equipment for law enforcement, and advocating for consent-based policing over militarization.

### Audience

Community members, advocates.

### On-the-ground actions from transcript

- Advocate for consent-based policing to local law enforcement agencies (implied).
- Support organizations pushing to terminate programs providing military equipment to law enforcement (implied).

### Whats missing in summary

The nuances and detailed explanations Beau provides in the full transcript.

### Tags

#Tools #Militarization #LawEnforcement #CommunityPolicing #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So we're going to start off with a story today.
I had a buddy and he wants to put in a privacy fence around his raised beds up in the front
yard because as he is tending to his garden, he's bent over and the people on the road,
so he wants a privacy fence.
He calls me up and he's like, hey, I need your help putting in a fence.
Sure, no problem.
I grab my drill, my hammer, pry bar just in case, level, string, everything you need,
and I head over there.
I get out of the truck and he's got this puzzled look on his face because while he'd been talking
about that privacy fence, he actually needed help moving some agricultural fencing in the
backyard, which is green wire fencing.
You literally pick it up and move it.
It's not a big deal.
I didn't have the right tools with me is the point I'm getting at.
Right tools for the job.
Although both things are kind of the same, it's a fence, you need different tools.
In completely unrelated news, two studies have come out suggesting that access to military
equipment neither reduces crime nor increases officer safety.
Imagine that.
Tools for different jobs don't help.
They determined this by going back and looking because the program that supplies this stuff,
it's been suspended and restarted a number of times.
And what they found out was that there was no difference.
It didn't change any rights.
So the equipment didn't actually do anything.
If it doesn't deter crime and it does not increase officer safety, what good is it?
Apparently none.
However, we do know that when some law enforcement agencies have access to this kind of equipment,
they want to use it.
Oftentimes they are not necessarily trained in how to use it.
They want to engage in little operations that may not necessarily be well thought out because
they focus on that last couple of minutes after they go through the door because that's
the fun part.
They don't focus on the weeks of planning that is supposed to go into something like
that.
These studies will probably be used to suggest that this program is terminated again and
that makes sense.
Organizations are already pushing back against this idea because they want their toys.
One of my favorite comments so far is somebody saying that they needed this stuff, they needed
the military hardware because we are very often outgunned.
Our body armor is kind of minimalistic because it's something we have to wear 12 hours a
day.
Take as much time with that as you need to.
I'm going to suggest that if you are willing to sacrifice protection in exchange for comfort
as you ride around in your air conditioned patrol car, that perhaps getting shot at isn't
a big part of your job.
I remember all those guys coming back from over there saying, I really wish I just had
that Kevlar that the cops had instead of those heavy plates because those are uncomfortable.
No, of course not.
That never happened.
Nobody ever said that in the history of histories because it's a different job.
It is a big part of what the military does.
They are different jobs.
They require different tools.
Much like that fence, you need less tools for lighter work.
Law enforcement should be focusing more on moving towards consent based policing than
trying to turn itself into a military.
It doesn't do any good.
It doesn't increase officer safety.
It doesn't deter crime, but we know that it causes people to get hurt.
I would hope, given the fact that this is one of those things that Biden can end pretty
quickly, I would hope he does that because at this point there's no evidence suggesting
it helps, but we know it's a liability.
In the risk versus reward, apparently there's only risk.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}