---
title: Let's talk about how Trump's great speech left me confused....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CNrrhC0z5yI) |
| Published | 2020/12/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Gion says:

- Subscribed to President Trump's updates and eagerly awaited a concise speech.
- Prepared to defend his support after being called an uneducated voter by liberal in-laws.
- Spent an hour and a half watching Trump's speech, hoping for evidence of voter fraud.
- Believes Trump's focus on late mail-in votes was a strategic move rather than a concern.
- Received a mass text message from Trump asking for more money for his defense fund.
- Discovered that 75% of donations could go to a political action committee called Save America.
- Expresses faith in Trump's honesty and integrity despite donation allocation concerns.
- Simplifies his view by stating that Democrats are bad and Trump is a good businessman.
- Expects a return on investment from the donations made to Trump's defense fund.
- Feels conflicted about Trump's repetitive messaging and lack of concrete results.
- Questions why Trump, with four years in office, didn't address the issues he now speaks about.

### Quotes

- "Democrats are bad, all right? It's that simple."
- "We expect to see, you know, a return on our investment."
- "He just spent 45 minutes telling us nothing, acting like he had everything under control."
- "And the other part about this bothered me. He's saying that he knew this was all coming and that he knew about all this stuff from the beginning."
- "I don't know what to think anymore because he just spent 45 minutes telling us nothing."

### Oneliner

Beau Gion listens to Trump's speech, questions donation allocation, and wonders about the lack of results despite four years in office.

### Audience

Supporters reevaluating Trump.

### On-the-ground actions from transcript

- Question the allocation of donations to political action committees (implied).
- Demand transparency and results from political figures you support (implied).

### Whats missing in summary

The full context and nuances of Beau Gion's reflections on President Trump's recent speech.

### Tags

#PoliticalSpeech #TrumpSupporters #Donations #VoterFraud #PoliticalTransparency


## Transcript
Well howdy there internet people, it's Bo Gion.
So today we're going to talk about one of the great historical speeches that has been
given in recent memory.
And that was that very concise speech given last night by President Trump.
I was up at the store when my phone went off and let me know it was coming on because I
subscribed to all of his stuff.
I ran home and I got my pen and my paper ready to write everything down because over Thanksgiving
I was with my liberal in-laws and all of them kept telling me that I was an uneducated voter.
And they kept asking me questions.
I'll be honest, I didn't know the answers to.
I didn't know the answers to because he's the president.
You know, just, the man knows what he's doing.
So I didn't think I needed to concern myself with the details.
And then they kept asking me about the details.
I wanted to be ready for Christmas.
And I wrote everything down, all the evidence that he presented.
I watched that 45 minute thing twice.
An hour and a half I spent because I thought I missed the evidence the first time.
But it turns out he must be saving that for later.
All he talked about was how there was the big dump of votes late in the night.
I mean, I know he must be just lulling them into a false sense of security because we
all kind of knew that the mail-in ballots were going to be heavily in favor of Biden
because he told all of his supporters via every way possible that those weren't good
to use.
So I know it's just a trick, 4-D chess and all that.
And you know, so I got my blank piece of paper here and I'll be honest, I was a little despondent.
I was a little upset about it all.
And then I realized, you know, when my phone went off, because I got, you know, I'm a member
of his fan club and everything.
And I got the personal mass text message from him that was requesting more money for his
defense fund.
And then I got to looking at it and it said that 75% of each donation that we give him
as, you know, mostly hourly employees that don't really make that much, they're going
to go to a PAC, which I looked up and that's a political action committee, called Save
America.
75% of the money that's being given to him can go to that.
And then from there, that money can legally be used to pay him.
It could just go into his pocket.
But I still have faith that President Trump is not, you know, scamming people.
I mean, this is an honest man that's done a lot of good in the world and he's always
been very truthful and upfront.
He doesn't have a long history of making...
Okay, so look, Democrats are bad, all right?
It's that simple.
It is that simple.
And at the end of the day, we all know that...
So maybe we lost, all right?
Because at this point, we've been giving him money and as a good businessman that he is
and as successful as he is, he has to know that we expect to see, you know, a return
on our investment.
I mean, somebody that's a billionaire, he should know that.
So we would expect to see some kind of evidence, some kind of results, not just him saying
the same thing over and over and over again for a pretty long while now.
And yeah, so look, I got to be honest, y'all got to help me out here.
I don't know what to think anymore because he just spent 45 minutes telling us nothing,
acting like he had everything under control.
And the other part about this bothered me.
He's saying that he knew this was all coming and that he knew about all this stuff from
the beginning.
He had four years.
Why didn't he fix it?
I mean, that seems like a failure.
Anyway, it's just a thought.
I have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}