---
title: Let's talk about how we can help the police....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_sa56eYiuAU) |
| Published | 2020/12/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recognizes the strain on law enforcement in the U.S. and the need to alleviate their burden.
- Suggests that law enforcement is tasked with too many responsibilities that could be handled better by other professionals.
- Proposes reallocating some of law enforcement's responsibilities to counselors, social workers, and other programs.
- Mentions situations like truancy, homeless calls, and petty possession of substances that could be handled more effectively by non-law enforcement personnel.
- Advocates for allowing law enforcement to focus on situations where their presence is truly needed.
- Addresses concerns about funding by suggesting that reducing law enforcement responsibilities can free up funds for other programs.
- Comments on the political manipulation surrounding the issue of defunding the police at the federal level.
- Argues that reducing law enforcement's involvement in certain areas can lead to safer outcomes for everyone.
- Emphasizes the practicality and sense in supporting a more efficient allocation of resources and responsibilities.
- Concludes by urging for a shift in perspective towards more effective and sensible approaches to public safety.

### Quotes

- "We can't ask cops to handle everything."
- "Opposing this is just a show of being manipulated."
- "There's no reason to oppose it. It makes complete sense."
- "Why not want the most effective people dealing with something?"
- "Y'all have a good day."

### Oneliner

Beau suggests reallocating responsibilities from law enforcement to more suitable professionals to improve efficiency and effectiveness in public safety, making complete sense and rejecting opposition based on manipulation.

### Audience

Policy advocates, community leaders

### On-the-ground actions from transcript

- Advocate for reallocating certain responsibilities from law enforcement to counselors and social workers (suggested)
- Support programs that assist with truancy, homelessness, and substance abuse to reduce law enforcement involvement (implied)
- Educate communities on the benefits of reallocating funding to support alternative programs (exemplified)

### Whats missing in summary

The full transcript provides a detailed breakdown of how reallocating responsibilities from law enforcement can lead to more effective community support systems and enhanced public safety measures.

### Tags

#LawEnforcement #PublicSafety #CommunitySupport #ReallocatingFunding #SocialWorkers


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about something and I want you all to stick with me.
I don't know how this is going to sit with everybody who normally watches this channel,
but just hear me out.
Because we are in a situation in the United States where there's a group of people that
are stretched pretty thin.
They've hit their limit.
And we've got to figure out a way to kind of relieve some of the burden that's on them.
We have to figure a way to help law enforcement.
You know, and I think the easiest way to do this is to acknowledge the fact that they're
asked to do just way too much.
I think that's the best way to look at it.
We ask them to handle a whole bunch of situations.
And a lot of them, they don't really need to be handling.
They can be handled better by somebody else.
So maybe that's what we should do.
We're talking about taking all sorts of other stuff away from them.
Why don't we take away some of their responsibilities?
Make it easier for them to do their job.
I think most people in the United States would say we want law enforcement focused on the
important stuff.
You know, the stuff that actually has somebody that's negatively impacted.
So let's start there.
If there's an infraction and there isn't a person other than the person involved that's
negatively impacted, we can remove that from law enforcement's purview.
Something like truancy.
Think about it.
There's no reason for a cop to be involved in that.
Not really.
It's just the way we've always done it.
We could probably do it better in a different way.
I would suggest that counselors, social workers probably do a better job at that.
No reason for a cop to be involved in that.
Think about the homeless calls they have to deal with.
Most times, again, social workers, counselors, people who could refer them to programs they
need.
It would be helpful if they had housing programs and jobs programs, could refer them to get
any assistance they might need.
We can't ask cops to handle everything.
We don't want a nanny state, right?
Think about petty possession of substances.
A small change in the legislation, it's not even a crime anymore.
Gets handled by counselors.
And since there's no risk of getting like matching bracelets, there's probably not going
to be any resistance to it.
Would make sense.
And we can allow law enforcement to be more effective by focusing on the situations that
we really need them for.
And I know it's the US.
So the big question is going to be, well, how are you going to pay for this?
And it's simple.
We're asking law enforcement to do less so we can take some of their funding, put it
towards these other programs.
One might say you could defund them.
If you liked what I was saying up till that point, you support that.
Presented it in this way because President Obama said the slogan wasn't very good.
He's a smart guy.
So sure, why not give it a shot?
I don't think it matters to be completely honest.
I don't think anybody really misunderstands this if they looked into it.
I think either they were manipulated or they just don't understand how the government works.
And I know people get mad about that.
No, I understand.
I'm a patriot.
Sure you are.
Then why are you worried about what federal politicians say about this?
Your local cops are funded at the local level, but it's mostly federal politicians out there
riling people up about this, grandstanding on it.
Because they can.
Because they don't have to do anything about it.
They can talk about it and use it as an emotional issue to manipulate people.
But at the end of the day, they don't really control the purse strings.
They can't be held accountable for it.
Small changes in the way we do things reduces the burden on law enforcement and helps the
police and also removes them from situations in which they probably shouldn't be involved
in.
If there wasn't the culture in law enforcement right now, it would be safer for everybody
if they weren't involved in it.
This is a... this is one of those things that there's no reason to oppose it.
It makes complete sense.
Opposing this is just a show of being manipulated.
Why would you not want the most effective people dealing with something?
Why would you not want to put funding where it would do the most good?
I don't think the slogan matters because those who oppose it never really looked into the
policy to begin with.
But maybe that helped somebody.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}