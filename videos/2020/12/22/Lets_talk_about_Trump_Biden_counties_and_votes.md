---
title: Let's talk about Trump, Biden, counties, and votes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ckLt-QBL4EI) |
| Published | 2020/12/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the talking point of Trump winning more counties but Biden winning more votes.
- Counties vary significantly in population, so comparing them directly is not accurate.
- Illustrates with the example of Biden winning LA county with over three million votes.
- Contrasts Trump winning 142 counties with votes from Wyoming, North Dakota, and South Dakota.
- Emphasizes that Trump's vote count from those states includes every person, not just eligible voters.
- Points out that LA county has a larger population than most states that voted for Trump.
- Concludes that the talking point is misleading, not grounded in reality, and aims to confuse people.
- Encourages viewers to question sources spreading such misinformation.

### Quotes

- "Counties vary significantly in population, so comparing them directly is not accurate."
- "They're made up and they use different metrics and compare them to confuse people."
- "If a pundit, a commentator that you follow has pushed this idea, you might want to reconsider following them."

### Oneliner

Counties vary in population size, debunking the misleading talking point about Trump winning more counties but Biden winning more votes.

### Audience

Voters, Information Seekers

### On-the-ground actions from transcript

- Fact-check information from pundits or outlets spreading misleading narratives (implied).

### Whats missing in summary

The full video provides a detailed breakdown of the flawed comparison between Trump winning more counties and Biden winning more votes, offering clarity on how population discrepancies impact this analysis.

### Tags

#Election #Misinformation #Counties #Voting #FactChecking


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about a talking point
that's coming from the right right now. We're going to talk about Trump Biden counties and votes.
We're going to do this because there's a whole bunch of people right now asking the question
how could Biden get more votes when Trump got more counties? Now if you have heard this from
an outlet that you trust, from a pundit that you trust, please watch this video and then
consider whether or not you want to continue going to that person for information.
Okay so it's apples and oranges. The assumption there for that to matter would be that counties
have the same population. They don't. They do not. They're not even close. As an example,
let's say that Biden won LA county with just over three million votes because he did. Okay
and let's say that Trump got every person, man, woman, and child in the states of Wyoming,
North Dakota, and South Dakota to vote for him. Every single person. Okay that means that he's
going to get 142 counties versus Biden's one county of LA. Trump will end up with 2.2,
2.3 million votes. One county, 142 counties. Biden still wins the popular vote. Counties
aren't the same size. They don't have the same population. That's the reason Biden can win bigger
with a smaller number of counties. He just has to make up the difference in the population.
LA county has a larger population than most states. Particularly true of those states that vote
for Trump. And again, that 2.2 that Trump got in this scenario, that's not eligible voters.
That's not Republicans. That's every single person in the state. If you actually did the math,
which I'm not doing because I don't want to, it would be even less. Like substantially less.
But even with the ridiculous scenario that we've put together here, it's still a blowout
in favor of Biden off of one county versus 142. This is how a lot of the talking points surrounding
the election, this is how they work. They're not grounded in reality. They're not grounded in fact.
They're made up and they use different metrics and compare them to confuse people.
If a pundit, a commentator that you follow has pushed this idea,
you might want to consider whether or not you want to continue following them.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}