---
title: Let's talk about Pat Robertson turning on Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sUwpAK8IvGE) |
| Published | 2020/12/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People, including big names like Pat Robertson, are distancing themselves from Trump, not wanting to be associated with him when his support dwindles.
- Pat Robertson made a statement criticizing Trump, suggesting that he should retire and not run again, as his claims about Biden have not come to fruition.
- Trump is likely to lose support from evangelicals, but conservative people are slow to change, so the shift may take time.
- Pat Robertson is noted for his two different sides - the evangelical leader and the politically active figure who has supported progressive policies and candidates.
- Robertson's successful political career stems from his ability to read the political climate and transition between being in power and out of power.
- Establishment Republicans, who understand the political game, are expected to distance themselves from Trump publicly, making it harder for him to continue his tactics against reality.
- As support for Trump dwindles, it will be easier to sway his committed loyalists, particularly those who only know government under Trump, out of public office.

### Quotes

- "He should retire and not run again."
- "The more of the real power brokers who cut Trump loose, the harder it is for him to carry on his offensive against reality."
- "It will be easier to get the more committed of his loyalists gone."

### Oneliner

People, including Pat Robertson, are publicly distancing themselves from Trump as his support dwindles, making it harder for him to continue his offensive against reality.

### Audience

Political observers

### On-the-ground actions from transcript

- Publicly distance oneself from politicians or figures supporting harmful ideologies (suggested)
- Support policies and candidates that prioritize treatment over incarceration (exemplified)
- Be vocal about calling out political figures who enable harmful actions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics surrounding Trump's dwindling support and the potential implications for the future of his loyalists in public office. Watching the full video will offer a deeper understanding of the evolving political landscape.

### Tags

#Trump #PatRobertson #PoliticalShift #ConservativeSupport #PublicOffice


## Transcript
Well, howdy there, internet people, It's Bo again.
So today we're going to talk about more people taking a walk,
distancing themselves from Trump,
not wanting to be the last one standing when the music stops.
And today we're going to talk about a pretty big name doing it,
Pat Robertson.
Pat Robertson made a statement, and it was pretty pointed.
He said that Trump lived in his own reality,
that he said things that weren't true, but hey, you know, Trump believes they're true and that
he should retire, he should retire and not run again, and that his claims about Biden,
well, they really haven't come to fruition.
Now the immediate takeaway from this is that Trump is going to lose support from evangelicals.
And yeah, that's true, but that's going to take time.
is going to take time. They're conservative. Conservative people are slow to change. I
think there is a much bigger observation that can be made. When you talk about Pat Robertson,
you have to talk about him as if he is two different people because he has two different
sides. There is the evangelical leader, the commentary that he is known for. There's
there's that side to him. And then there's the political machine that has been active
on the national scene for decades. To date him a little bit, my understanding is that
he was somebody who raised funds for our friends in Nicaragua. And by our friends, I mean,
think back to like Oliver North's time. He's been around a very long time. He is very well
connected, he knows how the game is played. It's also worth noting that the political side of Pat
Robertson, well, he's kind of known for supporting policies and candidates that may be a bit more
progressive while still very conservative than you might imagine. This is a man who supports
treatment over locking people up. This is a person who endorsed candidates who let's just say had
a more progressive view on women's health or people's orientations. He endorsed one of them
for president, that person being Rudy Giuliani. I do not think that it is a coincidence that we
have reports of a quarrel between Giuliani and Trump and then shortly thereafter Robertson pulls
his support, Robertson knows how the game is played.
It's worth noting that his long career in politics has been pretty successful, and that's
because he's always been able to tell which way the winds are blowing.
He is somebody who has transitioned from being the party in power to being the party out
of power before, and it appears that he's setting himself up to do that again.
I think that we are going to see a whole lot of establishment Republicans, people who have
been around a while, who know how the game is played, begin to publicly distance themselves
Trump and to call him out in the process because while people on this channel, people who watch
this channel or channels like this, they're going to remember everybody who enabled Trump.
The average voter, they're just going to remember the last few who were there.
And if you look, you are seeing a lot of the political mainstays take a walk.
who are remaining loyal and vocal? Well, they're kind of new to the game. They may not really
understand what's happening. I think that's more important because the more of the real power
brokers, both politicians and political influencers, who kind of cut Trump loose,
the harder it is for him to carry on his offensive against reality. I would
expect to see more of this. I personally do not believe that coming clean at this
point in the game really means much. However, it should be noted just for the
sake of it's going to be easier as his support dwindles.
It will also be easier to get the more committed of his loyalists gone because
once all of the real power brokers have left and it's just the new people,
they're going to be on their own come election time and it's going to be easier
to get them out, and those are going to be people who do not have another
view of the political system.
They may only know what government was like under Trump, and those are
people who it is going to be important to have leave public office.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}