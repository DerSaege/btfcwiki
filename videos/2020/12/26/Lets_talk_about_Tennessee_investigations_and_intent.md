---
title: Let's talk about Tennessee, investigations, and intent....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qQfwskZ02Y4) |
| Published | 2020/12/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing questions about Tennessee, Beau jumps right into discussing what is known and a mistake people are making.
- Beau cautions against speculating about intent in situations like these, as there are often unintended consequences.
- He mentions that disrupting communications may not have been the actual goal and could have been an unintended consequence.
- Beau points out that without knowing the who, speculation on intent is premature.
- The method of delivery, technical expertise, and other factors can help identify the culprits and lead to understanding the why.
- Beau notes that despite the magnitude of the incident, technical expertise appears low based on available information.
- He suggests that the lack of a claim of responsibility indicates a blend of personal grievance and politics rather than solely political motives.
- The importance of following investigative steps to uncover intent is emphasized by Beau.
- Beau mentions the next steps for investigators, including tracing the origin of the vehicle to potentially identify the perpetrators.
- Keeping an open mind until more information is available is advised by Beau to prevent premature conclusions and wild claims.

### Quotes

- "The intent is the holy grail. That's what you find at the end."
- "It's really hard to do something like this in the United States and get away with it."
- "Keep an open mind on this until more information becomes available."

### Oneliner

Beau addresses speculation about intent in the Tennessee incident, stressing the importance of following investigative steps to uncover the truth and avoid premature conclusions.

### Audience

Investigators, Analysts, Concerned Citizens

### On-the-ground actions from transcript

- Analyze available information to contribute to understanding the incident (implied).
- Keep an open mind and await further information before forming conclusions (implied).

### What's missing in summary

Insights on the potential consequences of premature speculation and the impact on investigations could be better understood by watching the full video.

### Tags

#Tennessee #Speculation #Investigation #Intent #UnintendedConsequences


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So this morning, my inbox is full of questions about Tennessee.
So we're just going to jump right into that.
I'm going to talk a little bit about what we know.
And I'm going to talk about a mistake that I see people making.
Right now, there's a whole bunch of people speculating about intent.
You can't do that yet.
With stuff like this, there's a whole lot of unintended consequences.
And if you work from intent, you may pick an unintended consequence
and work backwards.
Like right now, there's a whole bunch of people
saying that because there was a disruption in communications,
that was the goal.
We don't know that that's true.
I've seen no evidence to suggest the actual goal was
to disrupt communications.
That could just be something that happened along the way.
The goal may have been that they were mad at the melting pot.
We don't know.
The disrupting communications very well may have been the goal.
But there's no evidence to suggest that yet.
There are other wilder theories out there, some of which
have just basic geography wrong.
But they're all making the same mistake.
They're working backwards from intent.
The why.
Who, what, when, where, why.
We know when, we know where, we know what.
We don't know who.
And until you know who, you can't speculate on intent.
In this case, the how is really important.
Method of delivery, level of technical expertise,
the type of vehicle used, the warning,
the fact that there wasn't a claim of responsibility, all of this stuff
can help lead you to who.
And then from there, you can get to the why.
That, to me, is something that needs to be addressed.
Because there's a lot of wild speculation.
And once an idea takes root that this was the intent,
it can alter an investigation and lead it in the wrong direction.
At the moment, we do not know the intent.
What do we know?
Technical expertise was low.
I know that's counterintuitive, because it was really big.
But think about it in terms of cooking.
What is harder to do?
Twice-baked potato or a 30-gallon pot of mashed potatoes from a bag?
The twice-baked potato.
When you're talking about this topic in particular, normally,
the smaller it is, the more expertise there is.
I've looked at a lot of the clips, the news clips, and this does not look,
it doesn't look like there was a lot of expertise.
It doesn't look like there was a lot of training involved in this.
But I don't have the forensics yet to really say that for certain.
But just based off of what I've been able to see in clips,
this looks like something that could have been pulled off by anybody
with a few hundred bucks and a library card.
The fact that there's no claim of responsibility
leads me to believe gut feeling, that it is a personal grievance blended
with something political, more than something that is just political.
If it is just political, people tend to tell you that there's a notice,
there's a claim, there's like, hey, this was us
and we're proud of it type of thing.
That hasn't happened.
The big part, the big lesson in the last 24 hours
is making sure you go through the steps.
The intent is the holy grail.
That's what you find at the end.
That isn't a basis to start from because you're going
to end up with the wrong information.
You can start to suffer from confirmation
bias that way.
At this point, we don't know a lot.
The next steps in this are going to be the investigators
tracing the origin of the vehicle.
From there, they can probably get to who.
From there, they'll go to their search engine.
And from there, you'll get to the why, where they may just
tell us at some point.
But right now, we don't have enough information to speculate on that.
And I think that trying to do so may be self-defeating.
Because if there's a whole bunch of information that comes out
saying, oh, well, we believe it was this because communications
were disrupted, if there is information that comes along
later that suggests that isn't true, people don't believe it.
And then that's where all of these wild claims come from.
Because they hear the reporting, and they get their mindset
to believe one thing, and then other information surfaces.
So I would just keep an open mind on this until more information
becomes available.
This shouldn't take long.
The area where this occurred, there's a lot of cameras.
Once all of that is recovered and analyzed,
they will be able to track it back.
And they'll probably be able to track it back
to a location where they got that vehicle.
And then they'll have to track back the vehicle that they arrived
at that location in.
And eventually, they'll get to where they need to be.
It's really hard to do something like this in the United States
and get away with it.
It shouldn't take much time for them to come to a conclusion about who.
Only then can we get to the why.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}