---
title: Let's talk about Pence, analogies, and the working class....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vxSoddoxw7M) |
| Published | 2020/12/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the analogy of a rising tide lifting all ships and its connection to the economy and working class.
- Criticizes the analogy for being inaccurate and misleading, particularly in relation to the stock market.
- Notes that the rich are getting richer while the working class is suffering, contrary to the analogy.
- Points out that most Americans are actually in the water, not on ships, hinting at the working class being the foundation that supports the wealthy.
- Mentions how senators favor the rich by artificially boosting their wealth, leading to income inequality.
- Emphasizes the importance of uplifting the working class for overall economic growth and prosperity.
- Advocates for stimulus efforts starting from the bottom to benefit those who need it most and boost economic activity.
- References the concept of velocity in economics, suggesting that money circulated among the working class is more impactful than stagnant wealth.
- Warns about the widening wealth gap and the detrimental effects of government policies favoring the wealthy.
- Concludes by encouraging reflection on these economic dynamics and their implications for society.

### Quotes

- "You want to raise the tide, you have to raise the working class."
- "The stimulus, yeah, it's got to start at the bottom."
- "This country has a widening gap between the haves and the have-nots."

### Oneliner

Beau explains a flawed analogy of rising tides in economics, criticizing wealth inequality and advocating for uplifting the working class to stimulate the economy.

### Audience

Policy Makers, Economic Activists

### On-the-ground actions from transcript

- Support grassroots movements advocating for economic equality (implied)
- Advocate for policies that prioritize supporting the working class (implied)

### Whats missing in summary

Deeper insights on the impact of economic policies on income inequality and societal well-being.

### Tags

#Economics #WealthInequality #WorkingClass #GovernmentPolicy #Stimulus #IncomeGap


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about an analogy, and
economics, and rising tides and all of that, and the
working class.
I love analogies.
I use them all the time.
But in order for them to be effective, well, they have to
make sense.
They have to be accurate.
And the components in the analogy, they have to be
readily identifiable.
If those things aren't true, the analogy is bad.
What's even worse is that it may lead to a false understanding.
The vice president recently said that something along the lines of a rising tide raises all
ships, the idea being that if the economy is doing well, particularly in this case,
talking about the stock market, then well everybody goes up. And I mean that sounds
good and it's based on a spurious correlation. The idea that because over the last 100 years
the standard of living or whatever has gone up across the board, well that has to do with
you know the stock market going up. Except the standard of living across the board has
gone up since the beginning of time, that's just human civilization. That has nothing to do with
with the stock market.
So this concept, it would hold true.
If it was a good analogy, it would hold true.
If the stock market is doing better,
everybody's ship would go up.
Everybody would be doing better.
Now, it can be an imperfect analogy.
And sure, the rich folk could go up more than the working class.
But that's not what's happening.
The reality is that the rich folk have their ships going up and everybody else is going
down.
Eight million people recently fell into poverty.
It doesn't sound like a rising tide to me, it sounds like people are going underwater.
But I actually think it's a good analogy as long as the components mean things other
than what the Vice President meant.
The reality is that most Americans do not own a ship.
They do not own a boat.
They do not own a yacht that they get tax breaks on.
Most Americans are in the water.
In fact, if you want to make the analogy more accurate, the working class of America is
the water.
You know, the place where those yachts are resting.
What keeps them afloat.
That makes more sense.
And the reality is, historically, when the working class goes up, so do the rich folk.
See the analogy works that way.
But what has been happening recently is that the ships are going up without the tide.
Because well, they have senators in the pocket.
And those senators send out a crane to artificially lift them up.
The funny thing about that is when that happens, well, the boat's not in the water as much,
which means it's not displacing as much water, which means the water level actually goes
down, the working class goes down.
This kind of explains the income inequality gap.
If that crane continues, eventually that boat is all the way out of the water and into dry
dock.
have any money in the game, so to speak.
So it is not displacing water, the water level goes down, and that boat is just locked away
and some screws McDuckie and Vault never to be seen again.
That's what's happening because these companies have these senators in their pocket.
You want to raise the tide, you have to raise the working class.
The stimulus, yeah, it's got to start at the bottom.
That's who needs it the most because they're going to spend it.
There's actually a concept in economics about this, it's called velocity.
They're going to use that money faster.
It is going to be moved around faster.
It is going to be worth more than just sitting on the deck on a boat that's in dry dock
not being used.
This country has a widening gap between the haves and the have-nots.
And the bigger that gap gets, the higher the government raises those boats artificially,
the worse it's going to be.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}