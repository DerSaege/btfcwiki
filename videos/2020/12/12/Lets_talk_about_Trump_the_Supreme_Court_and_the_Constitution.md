---
title: Let's talk about Trump, the Supreme Court, and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yax4slhRYyo) |
| Published | 2020/12/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Predicted the Supreme Court's decision accurately, based on expert opinions.
- Explained the real battle as between experts and those supporting Trump.
- Emphasized the importance of respecting the Supreme Court's decision for the Constitution.
- Criticized individuals undermining the Constitution while claiming to defend it.
- Urged supporters of Trump to re-evaluate their beliefs and allegiance.
- Encouraged people to choose between supporting the Constitution or the President.
- Pointed out the lack of true constitutional scholars being brought onto certain media shows.
- Anticipated internal turmoil for individuals as they grapple with their beliefs.
- Stressed the importance of moving the country forward rather than dwelling in the past.
- Reminded listeners of the goal of looking forward and progressing as a nation.

### Quotes

- "Experts versus Trump."
- "If you are willing to rip the country apart because you do not agree with the Supreme Court decision, you don't support the Constitution."
- "Those media pundits who are still out there saying, oh, constitutionality. They don't know anything about the Constitution."
- "You've given up on moving the country forward."
- "To move forward. You've given up on having this country succeed."

### Oneliner

Beau predicts Supreme Court decision accurately, underscores conflict between experts and Trump supporters, and challenges individuals to choose between supporting the Constitution or the President.

### Audience

Political observers, Constitution defenders

### On-the-ground actions from transcript

- Re-evaluate your beliefs and allegiance (implied)
- Support moving the country forward (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the current political climate and the struggle between upholding constitutional values and allegiance to political figures.

### Tags

#SupremeCourt #Constitution #Trump #InternalTurmoil #MovingForward


## Transcript
Well howdy there internet people, it's Beau again.
So we're gonna talk about the Supreme Court, what happened.
Tell you what happened, absolutely no surprises.
If you go back a couple of days, you'll find a video,
I said they're not gonna hear it, and even if it clears that hurdle,
they won't get the relief that they're seeking.
And that's what the Supreme Court judges said.
Why?
I don't have a crystal ball.
I talked to three constitutional scholars.
They all said the same thing.
There was a consensus among experts.
That's what you have.
That's the real battle here.
People are trying to frame it in different ways, and
we're gonna talk about that in a second.
But when you really boil it down, you have constitutional lawyers,
constitutional professors, constitutional scholars of every sort,
all of these judges, and the Supreme Court all saying one thing.
Then on the other side, you have a bunch of talking heads, media pundits, and
politicians who are so deep into supporting Trump,
they feel their political future is at stake.
Those are the sides.
Realistically, those are the sides.
Experts versus Trump.
It is something that has happened his entire administration,
because he feels he knows better than everybody.
How has it worked out in the past?
How much damage has been done because people deferred to him
rather than those who have subject matter expertise?
Now, once this decision was made, you had a bunch of emotional reactions, and
you had people saying some pretty drastic stuff.
Attempting to frame it as if they were defending the Constitution,
I would point out that under the Constitution, the very people being blamed,
the Supreme Court, get to determine the constitutionality of something.
It's a court that is currently, it was handpicked by the Trump administration.
Even with those handpicked appointees, that they couldn't get this through,
because it's not even close to constitutional,
because it isn't within the Constitution.
And the body of people who gets to determine constitutionality has said that.
If you, if you, if you, if you, if you,
if you, if you, if you, if you, if you, if you,
if you are willing to rip the country apart because you do not agree with
the Supreme Court decision, you don't support the Constitution.
Because the Constitution says the Supreme Court gets to decide
the constitutionality of something.
They're saying to judge it on its merits.
What if it really doesn't have any?
What if that's the reason it couldn't win in courts below?
There are a whole lot of people who have been had by this president.
He's good at branding.
He's good at rallying people.
He's not good at leadership.
And he doesn't understand this country.
He never did.
He understands creating emotional reactions.
That's why there are so many people at this moment who say they're defending
the Constitution while they are actively attempting to undermine it.
If you are still in support of this man, you need to take a step back.
And you need to look at all the times the expert said one thing and
he said something else.
And who was really right?
And you also need to take a look at your beliefs from 2015 or 2014,
or before this man got his hooks into you, and see if they line up with today.
Or see if you have betrayed them because dear leader said to.
That's where we're at.
There's going to be a lot of people who have to do some soul searching and
have to determine where they stand on issues.
Do you support the Constitution?
Which in this case means siding with the Supreme Court.
Or do you support President Trump?
For many, there are two things that they feel that they have to support.
And they're coming to a head.
They're going to impact each other.
You have to decide which side you're going to be on.
Those media pundits who are still out there saying, oh, constitutionality.
They don't know anything about the Constitution.
And you notice they're not bringing constitutional scholars onto their shows,
not real ones.
Wonder why.
They probably know they're wrong.
But at this point, they are in too deep.
They don't want to admit that they were wrong.
They don't want to admit that they got tricked because they have spent their
entire lives manipulating those people who watch their shows.
And they don't want to admit it.
And then you have all of these politicians who signed on to this.
And you have to wonder where they stand now because they are actively seeking to
undermine the Constitution of the United States.
I think that the next couple of months are going to be pretty rough for some
people because there's going to be a lot of internal turmoil.
And you're going to have to decide where you stand.
Are you going to be part of that group that moves the country forward?
Or are you going to be part of the group that tries to hold it back?
Any time you're looking to the past, you're not really living up to the way
America has been.
We've tried to look forward.
That's the promise.
That's the dream.
Have we always succeeded?
No.
But that was the goal, right?
To move forward.
You've given up on moving the country forward.
You've given up on having this country succeed.
If you were trying to make America great again, looking back, you've given up on
the future of your kids, the future of this country.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}