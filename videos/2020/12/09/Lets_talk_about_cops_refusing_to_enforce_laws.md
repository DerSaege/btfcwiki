---
title: Let's talk about cops refusing to enforce laws....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wl8BwT76frY) |
| Published | 2020/12/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises questions about law enforcement officers in California refusing to enforce mandates.
- Notes that all law enforcement officers in California refusing to enforce mandates are sheriffs.
- Explains that sheriffs have more power than chiefs of police and can prioritize law enforcement actions.
- Mentions that sheriffs may delay enforcement if they doubt the constitutionality of a mandate.
- Points out that sheriffs cannot simply refuse to enforce laws based on personal preference.
- Suggests that sheriffs may deprioritize enforcement based on perceived unconstitutionality.
- Argues that sheriffs choosing not to enforce certain laws show they have discretion and are not just robots.
- Raises concerns about potential biases when law enforcement officers choose which laws to enforce.
- Questions the source of authority for law enforcement officers if they can disregard elected officials' laws.
- Criticizes the idea of law enforcement being autonomous and unaccountable, advocating for them to be public servants.

### Quotes

- "Sheriffs all over the country right now are showing that's not true. They are showing that when they enforce an unjust law, they are culpable."
- "There are a lot of laws that significantly impact populations in a manner that is unfair. When officers choose to enforce that, they're making the choice."
- "They're not supposed to be unaccountable people with guns. They're supposed to be public servants."
- "If law enforcement does in fact have the ability to disregard the laws and regulations coming from elected officials, where does their authority come from?"
- "Y'all have a good day."

### Oneliner

Law enforcement officers' selective enforcement raises questions about authority and accountability, undermining their role as public servants.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Question law enforcement actions (suggested)
- Advocate for accountability in law enforcement (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the implications of law enforcement officers' discretion in enforcing laws and raises critical questions about their accountability and authority.

### Tags

#LawEnforcement #Accountability #Authority #PublicServants #California


## Transcript
Well howdy there, In-N-Out people, it's Beau again.
So today we're going to talk about a question I got,
which is a good question and it's interesting in and of itself,
but it prompts a whole bunch of other questions
that to me are honestly even more interesting.
The question was specifically about California.
Now if you don't know, there are law enforcement officers
all over the country just saying,
we're not going to enforce this.
We aren't going to enforce these mandates,
these regulations when it comes to the public health stuff.
And sometimes it's about lockdowns or masks or whatever.
They're just saying, we're not going to do it.
And the question is whether or not
they have the authority to do this.
Can they just say, we're not going to enforce this law legally?
I looked and every law enforcement officer I found
that was saying this in California was a sheriff.
I looked and every law enforcement officer I found, period,
anywhere that was saying they weren't going to enforce a mandate was a sheriff.
Sheriffs have an immense amount of power,
more so than a chief of police.
They don't know the laws in every state.
What I will say is that in most of the states that I'm aware of,
they do have the ability to kind of hold enforcing something
until they get a ruling if they doubt its constitutionality.
And since they really don't want to enforce the law anyway,
it may take a while to get that ruling.
But as far as them just saying, I don't like our Democratic governor,
so I'm not going to enforce this law, this mandate, this regulation, whatever,
that they generally don't have the authority to do.
But it's all in the way it's phrased.
Because in every single area, every single jurisdiction,
every single state that I am aware of,
the sheriff has the power to set law enforcement priorities.
Meaning if they don't agree with something,
they can just deprioritize the enforcement.
And that sounds, at first glance, that sounds like a power they shouldn't have.
But they really should, it makes sense.
Think about it like this.
If you don't want law enforcement to have to expend the same amount of resources
to catch the person who put in enough change
to get one newspaper out of the rack and took two,
how much does they expend to catch somebody who did something truly horrible?
That's why it exists.
So can they do it simply because they don't like the governor?
Probably not.
Can they do it based on at least a perceived belief that it's unconstitutional?
Maybe.
Can they deprioritize it to the point where it doesn't matter?
Everywhere that I'm aware of.
Depends on how it's done.
But effectively, the answer is yeah, they can do it.
They have that authority.
But see, that opens up a whole new set of questions.
It removes one of law enforcement's famous talking points.
One of their famous responses.
Anytime they're out there enforcing some unpopular law, what do they say?
I don't make the law, I just enforce it.
Sheriffs all over the country right now are showing that's not true.
They are showing that when they enforce an unjust law, they are culpable.
They're playing a part in it.
They're not a robot.
They have discretion.
And that discretion should be used wisely.
Aside from that, it opens up yet another question.
If they can pick and choose which laws to enforce, it seems like they'd be able to pick
and choose who they're enforcing them against.
At which point, individual officers have their biases that come into play.
It's a topic that's been real prominent over the last couple of years.
And these sheriffs who are saying we're not going to enforce these public health things,
they're demonstrating that it's true, even on a deeper level.
Not just do they have discretion of who they're going to enforce them against, that they can
choose not to enforce it.
There are a lot of laws that are unjust.
There are a lot of laws that significantly impact populations in a manner that is unfair.
When officers choose to enforce that, they're making the choice.
They're exercising the discretion.
That talking point of we don't make the law, we just enforce it, it's plainly being shown
to be false right now.
Then there's one other question.
If law enforcement does in fact have the ability to disregard the laws and regulations coming
from elected officials, where does their authority come from?
The idea is that they are theoretically exercising authority given by the consent of the governed.
But if the elected officials are ignored, they're not really doing that.
So where does that authority originate from?
Does it solely originate from their monopoly on violence?
It seems like it might.
That's the message that's being sent by these sheriffs.
I don't know that they get the long-term ramifications of what they're doing.
To most of them, the ones that I saw talk about it, it looks like a short-term political
stunt.
But they are undermining a lot of their own authority when they do this.
They feel as though they're exercising it.
They feel as though they're showing how autonomous they are.
And I mean, that's cool in the John Wayne sense and everything.
But as far as the way our society is structured, they're not supposed to be autonomous.
They're not supposed to be unaccountable people with guns.
They're supposed to be public servants, which means they answer to the elected officials.
Unless, of course, their authority doesn't derive from the public and it just derives
from that monopoly on violence.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}