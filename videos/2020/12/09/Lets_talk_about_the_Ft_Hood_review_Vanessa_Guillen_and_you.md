---
title: Let's talk about the Ft. Hood review, Vanessa Guillen,  and you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RFFiv-to9jk) |
| Published | 2020/12/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Rarely issues calls to action, but made one regarding an investigation at Fort Hood.
- Investigation at Fort Hood happened due to public pressure after incidents like Vanessa Guillen's case.
- Investigation findings revealed command failure and issued 70 recommendations for improvement.
- Key finding: command knew of high risk to female soldiers and did nothing to mitigate.
- Surveyed about 30,000 troops at Fort Hood, with 93 credible allegations from 500 women soldiers.
- 14 people in leadership positions relieved or suspended following the investigation.
- Independent civilian review took action, but military's own investigation is ongoing.
- Actions taken aim to change the climate at Fort Hood and prevent similar incidents in the future.
- Investigation did not bring justice for Vanessa Guillen or other soldiers, but may lead to positive changes.
- Acknowledges community's role in pressuring for the investigation and its outcomes.

### Quotes

- "Command failed. They made 70 recommendations to make sure this sort of thing does not happen again."
- "One of those who was relieved was a major general. One was a colonel. One was a command sergeant major."
- "At the end of the day, did this bring justice for Vanessa Guillen or the other soldiers? No. Absolutely not."
- "Your action combined with a whole bunch of other people's actions made this happen."
- "Without enough pressure, this wouldn't have occurred."

### Oneliner

Rare call to action led to investigation at Fort Hood revealing command failures; actions taken to prevent future incidents, though justice not served.

### Audience

Community members, advocates

### On-the-ground actions from transcript

- Support live stream fundraiser on Friday (suggested)
- Stay informed about ongoing actions and investigations at Fort Hood (implied)

### Whats missing in summary

The full video provides more details on the investigation findings and the context of the actions taken.

### Tags

#CallToAction #FortHood #Justice #Prevention #CommunityPressure


## Transcript
Well howdy there internet people, it's Beau again.
So if you've watched this channel for any length of time you know that I do
not really do calls to action. Not my thing, you
know. Provide information if you want to act
on it, good. But it's very rare that I ask y'all
to do anything. When I do it's because
I am fairly certain that my take is correct
and I truly believe that whatever the action
is that I'm asking for is going to have some kind of tangible positive effect.
I like to pick battles that are big enough to matter but small enough
to win, you know. I made a call to action
about five months ago and a lot of you, you know, helped.
It was asking for an investigation at Fort Hood.
A lot of you responded and a whole lot of other people did.
There was a lot of public pressure to get this done.
It was, for me, it was in regards to the case of Vanessa Guillen.
There were other incidents at Fort Hood around the same time
that prompted other people to act as well.
That investigation happened. Enough people reached out
and put pressure on DOD and Senate Armed Services.
The investigation occurred. There was an independent review.
We now have the findings. The takeaway is yes, command failed.
Period. Full stop. Command failed. They made 70 recommendations to make sure
this sort of thing does not happen again.
Some of it is preventative. Some of it is restructuring the
cops there on Fort Hood. Some of it is making sure that things that
should have already been happening actually start to happen as well as
providing guidance to lower ranked soldiers on how to
actually report somebody missing rather than just reporting them AWOL.
The key finding was that command knew or should have known of high risk to
female soldiers and did nothing to mitigate.
They surveyed about 30,000 troops at Hood.
To be clear, there's only, I want to say, like 36,000, 37,000 there.
So they surveyed pretty much everybody. They conducted a whole bunch of
interviews, 500 of which were with women soldiers. Out of those
500, 93 made credible
allegations, but only half had been reported out of fear of
retaliation. That's the climate.
Okay, so the review happened. Now what? 14 people in command in
leadership positions were either relieved of command or
suspended. Now the obvious follow-up question is,
is this the Army pushing a bunch of junior officers out the door
as part of a PR move? The answer to that is no.
One of those who was relieved was a major general. One was a colonel.
One was a command sergeant major. There is
another major general and another command sergeant major who were suspended
pending the outcome of a separate probe.
At the very least, they are attempting to make it better.
They're attempting to change the climate here. Something that is worth noting
is that this is the independent review. This is the civilian review.
A major general was relieved based on an independent review.
The military still has its own investigation going on,
their own probes. I would not be surprised
if, let's say, more formal disciplinary actions
occurred once that investigation is complete.
So, at the end of the day, did this bring justice
for Vanessa Guillen or the other soldiers? No.
Absolutely not. But hopefully it will create a climate at hood
that means we don't have to call for justice
again for other soldiers in the future.
I'll put the link down below to the original video if you're not familiar
with the case and what happened.
And other than that, I do want to point out that Friday we're going to be having
a live stream fundraiser, and I'll tell you all about it then.
So, definitely tune in. It'll be afternoon, evening,
sometime in their central time.
So, if you took part in this, if you reached out,
just a phone call on your part or, you know, reaching out over Twitter or email
or however you did it, your action combined with a whole bunch
of other people's actions made this happen. Without
enough pressure, this wouldn't have occurred.
The findings would have been kind of swept under the rug. You did this.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}