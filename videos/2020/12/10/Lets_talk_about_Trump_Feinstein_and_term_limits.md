---
title: Let's talk about Trump, Feinstein, and term limits....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ctSN4wXDwgM) |
| Published | 2020/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reports suggest Senator Feinstein may not be as sharp due to age, sparking the term limits debate.
- Beau questions the effectiveness of term limits in addressing the real issue.
- The concern for a second Trump term is his potential to act without restraint.
- The main reason people want term limits is due to corruption, ineffectiveness, and re-election of politicians.
- Beau likens implementing term limits to buying a fire extinguisher after a fire; not addressing the root problem.
- He explains how term limits could potentially lead to politicians rushing to exploit their positions.
- Beau argues that voters should be responsible for removing corrupt or ineffective politicians.
- Increasing civic engagement is presented as the key solution to addressing political issues.
- Beau believes that limiting terms may not solve the problem but could serve as a temporary solution.
- He stresses the importance of an informed and engaged populace in holding politicians accountable.

### Quotes

- "The voter is the term limit."
- "People have to get involved. That's the actual solution."
- "Increasing civic engagement is the key solution to addressing political issues."

### Oneliner

Beau questions the effectiveness of term limits, stressing that increased civic engagement is vital in holding politicians accountable and addressing corruption.

### Audience

Voters, Citizens, Activists

### On-the-ground actions from transcript

- Increase civic engagement by attending town hall meetings and engaging with local politics (implied).
- Educate oneself on politicians' policies and actions rather than voting solely based on party affiliation (implied).
- Actively participate in the political process by voting for candidates based on their policies and track record (implied).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the term limits debate, the role of civic engagement, and the potential consequences of implementing term limits without addressing underlying issues.

### Tags

#TermLimits #CivicEngagement #PoliticalAccountability #Corruption #Voting


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about President Trump, Senator Feinstein, term limits, and
asking the right question.
We're going to do this because I had a whole bunch of questions about term limits last
night.
One of them was like, you know, I've watched pretty much everything you've ever made and
I don't know that you've ever mentioned this.
And that may be true, I probably haven't.
The reason this topic is coming up is because reports are suggesting that perhaps Senator
Feinstein is not quite as sharp as she used to be.
She's almost 90.
There's probably some truth to that.
Because of that, people realize how long she's been up there and now they want term limits.
The reason I haven't talked about it before is because I'm not sure it's the answer that
a lot of people believe it is because I don't think they're asking the right question.
What was the most unnerving part of a second Trump term?
Of that thought?
He's going to be off the rails, right?
Because he doesn't have to run for re-election.
Because he doesn't have to worry about poll numbers.
He can do whatever he wants.
He's in office.
Why do people want term limits?
When you ask that, normally what people say is, well, politicians get up there and they
just stay up there.
That in and of itself isn't necessarily bad.
That's not really why people want them.
If politicians were effective, represented the will of the people, did their job well,
nobody would care how long they stay up there.
The real reason people want term limits is because politicians either arrive corrupt,
become corrupted, or are ineffective and then get re-elected.
That's the reason people want term limits.
To me, it's a lot like buying a fire extinguisher after the house is burnt down.
It's not really addressing the problem.
In fact, it may make it worse.
Let's say we enact term limits.
We'll say a senator can be re-elected once.
So that's 12 years.
We'll let the house stay the same period.
12 years is certainly long enough to become corrupted.
Six years is.
If they don't have to worry about being re-elected, if they don't have to worry about poll numbers,
if they don't have to worry about representing the will of the people, what happens?
Even if we just reduce it to one term, six years, it turns into a six year smash and
grab of them getting as much as they can as fast as they can.
And there is nothing that anybody can do to deter them because they don't run the risk
of losing their seat over it.
The reality is that our system has term limits built into it.
When a politician is discovered to be corrupt or ineffective, the voter is supposed to not
re-elect them.
But that's not what happens, right?
And I get that.
That is not what happens.
That's because the American populace is not engaged.
They don't stay curious about their politicians.
They don't even know what they're doing.
A lot of voters, they just vote for a party.
They're not even voting for a person, which is sad because you're not really even supposed
to be voting for a person.
You're supposed to be voting for policy.
Saying we need term limits is shirking our responsibility.
It is our job.
This is advanced citizenship.
It is our job to remove politicians who are ineffective or corrupt.
And I'm afraid that if term limits were enacted, it would make the problem worse.
Social cycling in and out like that would be a whole lot harder to track the corruption.
The money that goes to them, it just goes to them.
There's no reason to look at their finances again because they're not going to be in public
office.
I don't know that it's the answer because I don't think that people want to ask the
real question.
You want to solve this?
We have to increase civic engagement.
You want to get rid of corrupt politicians?
You want to get rid of ineffective politicians?
Those that spend 40 years up there just fleecing the taxpayer?
People have to get involved.
That's the actual solution.
That's how you stop the house from burning down.
Limiting them, I don't know that it would help.
Maybe it could be a band-aid as we develop a more informed, more curious, more civic-minded
populace.
But I would imagine that while that's happening, those people, they are going to be off the
rails up there.
They're going to have envelopes of cash changing hands because unless they're caught in the
act, there's no accountability.
The voter is the term limit.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}