---
title: Let's talk about Trump's latest move....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ts97r0zsCNo) |
| Published | 2020/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas filed a lawsuit supported by other states and Trump, but constitutional scholars doubt its success.
- The Supreme Court is unlikely to invalidate votes, as it could be seen as deciding to end the United States.
- Speculation exists that the lawsuit was filed for a possible pardon due to an indictment.
- Some supporters may have been misled by Trump, while others seek political advantage.
- Trump’s base may eventually realize they were deceived about the election outcome.
- The potential misuse of funds collected from supporters could lead to dissatisfaction.
- Money diverted to political action committees might explain some support for Trump.
- The situation reveals the normal but problematic workings of American politics taken to an extreme.
- Legal implications and precedents set by the lawsuit could lead to long-lasting legal issues.
- Despite concerns, experts believe the lawsuit will likely fail to gain traction.

### Quotes

- "It's the normal machinery of American politics."
- "It's what we have come to tolerate and accept."
- "Perhaps it is time to adjust some of the normal machinery of American politics."

### Oneliner

Texas lawsuit supported by Trump unlikely to succeed, revealing normal politics taken to an extreme.

### Audience

American citizens

### On-the-ground actions from transcript

- Question and stay informed about political actions and lawsuits (suggested)
- Advocate for adjustments to improve the political system (suggested)

### Whats missing in summary

The full transcript provides a deeper insight into the potential consequences of political actions and the need for reevaluation of the current political machinery.

### Tags

#Texas #lawsuit #Trump #AmericanPolitics #ConstitutionalScholars


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about that thing from Texas.
Whole bunch of other states have signed on in support of it.
Trump has signed on in support of it.
But if you talk to most constitutional scholars, they say it's not going to go anywhere.
Even if it does jump over a couple of hurdles, the plaintiffs, being Texas, are not going
to get the relief they're looking for.
The Supreme Court is not going to invalidate the votes.
Because realistically, if that's what they are considering, they are no longer deciding
about the election, they're voting on whether or not to end the United States as we know
it.
So it's highly unlikely that this goes anywhere.
But see that brings up a question.
If it's unlikely that it goes anywhere, why did it get filed?
And why are people supporting it?
Now one theory that gets thrown out as far as why it got filed is that the person responsible
for filing it is kind of fishing for a pardon because they're currently under an indictment.
I mean, okay, maybe that's the case.
Time will tell, I guess.
Some of the people that are supporting it, I think they're like a lot of the American
populace.
They were honestly tricked by Trump.
Some I think are doing it simply because they believe it will be politically advantageous
in the future.
And I don't think that was thought all the way through.
Because at some point, Trump's base is going to realize they were tricked.
At some point, they are going to understand that while Trump was on Twitter talking about
how he won and how they're going to win in court and how he's going to start his second term,
that the first lady was deciding whether or not to send their personal belongings to Mar-a-Lago
or Trump Towers.
And that while she was doing that, he was out there collecting money from his base in
this economy that they may not understand how it could end up being used.
They may not get that.
And I think that when they figure that out, they're probably not going to be really happy.
And they're probably going to hold that against him and, well, anybody who supported it or
enabled him.
But there is that money, that money that may possibly end up being diverted to a political
action committee.
That might explain some of the support that he's getting.
There may be politicians who are like, hey, maybe I can get my hands on some of that cash.
And when you look at it like that, it's just kind of boring.
It's the normal machinery of American politics.
I mean, it's not right, but it's gone on for so long that we view it as normal.
It's what we have come to tolerate and accept.
Just in this case, it has gone to such an extreme that it actually poses a problem for
the very foundations of American democracy.
Again, I don't think this was very well thought through.
I don't think the legal pleading was thought through because I can't imagine the precedent
being set that state A can sue state B based on state A's interpretation of state B's laws
in regards to the Constitution.
That would open the door for legal shenanigans for like decades.
I mean, it would be highly entertaining.
I cannot wait to see California's lawsuit against Texas because California doesn't really
think that under the second, Texas is very well regulated.
I don't believe this was thought through and I don't think that it's worth losing any sleep
over.
If you talk to the people who really understand this stuff, they're saying it's not going
to go anywhere, that it is highly unlikely that it gets any traction whatsoever.
So then, maybe just view it as a last ditch attempt by a wannabe tyrant and those who
would enable him or those who he's tricked.
Because when you boil it down, it's the normal machinery of American politics taken to an
extreme level.
If it is worrisome, perhaps it is time to adjust some of the normal machinery of American
politics.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}