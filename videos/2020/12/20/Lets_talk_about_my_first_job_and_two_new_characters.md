---
title: Let's talk about my first job and two new characters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=73Hf5SPOgbY) |
| Published | 2020/12/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls his first job experience at fourteen, washing dishes for an immigrant who was a happy and jovial guy but faced stereotyping due to his mannerisms.
- Initially found the stereotype of his employer being connected to organized crime cool as a teenager, but realized its harmful nature as he got older.
- Shares a memorable incident where the happy employer, who learned English from Sesame Street, admonished a young man for not taking his classes seriously.
- Expresses how Sesame Street, a timeless institution, has been a part of many people's childhoods and stories by not chasing trends and thinking in decades rather than years.
- Mentions the introduction of new characters Aziz and Nor who live in refugee camps on Sesame Street, indicating long-term educational support for children in such settings.
- Emphasizes the need to address the increasing migration of people due to climate change, requiring the eradication of stereotypes and nationalism.
- Acknowledges the changing world and the necessity for institutions to forecast and adapt to the movement of people globally.

### Quotes

- "I learned to speak English from Sesame Street."
- "A lot of the stereotypes that we have about people from other places they gotta go."
- "The world is changing and we're going to have to adjust."

### Oneliner

Beau shares insights from his first job, including the impact of stereotypes, learning English from Sesame Street, and the importance of adapting to global migration trends.

### Audience

Educators, policymakers, community leaders

### On-the-ground actions from transcript

- Support educational initiatives for children in refugee camps (implied)
- Challenge and dismantle stereotypes about people from different backgrounds (implied)
- Advocate for policies that address climate-induced migration and support displaced populations (implied)

### Whats missing in summary

The full transcript contains Beau's reflections on the lasting influence of Sesame Street, the role of institutions in addressing global challenges, and the need for societal adaptation to changing demographics.

### Tags

#SesameStreet #Migration #ClimateChange #Education #GlobalAdaptation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about my first job
and Sesame Street
and
why Sesame Street is an institution
and what that might tell us about the future.
My first job
I was fourteen maybe, somewhere in there.
Too young to actually have a job
but I was working washing dishes for this guy
and he had just come over, well not just come over, he'd been here a while
but he was an immigrant
and
he was a really cool guy
I mean, very happy, jovial
kind of guy, walks up and does the punches thing, hugs you, all that stuff which was
weird to me, my people do not do that
uh...
but he was a really really nice happy guy all the time but because of where he
was from
and his mannerisms because in a lot of ways he
he was a walking stereotype
people made assumptions and the rumor was
that he was connected
to some organized folks
Now at fourteen I thought that was cool, I did, I'm not going to lie, I thought that was awesome
As I got older
you know late teens early twenties
I was like, man that's a harmful stereotype
I have no reason to believe that this guy
was anything like that
as I got even older
and actually met some
some people
that would know, I found it funny that anytime they found out where I was from
they asked about him
today I just write that up to them, liking his cooking
but the point of all of this is one day
this very happy, jovial guy
he starts yelling at this kid
this kid's like nineteen years old
also just come over
he's poking him in the chest
and basically telling him that he needs to take his classes more seriously
because he's going to classes to learn how to speak english
and uh...
the kid's like you know you
your english isn't perfect
and the guy yells at him, he's like I learned to speak english
from Sesame Street
and that always stuck with me
it always stuck with me because I had this image of this guy, like this tough guy
and there he is watching Grover and Big Bird
I don't know if that's really how he learned to speak english
but Sesame Street
was a part of that story
it's part of a lot of people's stories
I bet you watched it growing up, I did
it's an institution
the reason it's a classic, the reason it's an institution is because it's been
around forever, it's been around forever because it does not chase trends
they do not
deal in fads
they make decisions thinking
in terms of decades rather than years, at least that's the way it's always
appeared to me
and that's uh... the reason they've been around so long
that's why they're a part of so many stories
they have two new characters
Aziz and Nor
and
they uh...
live in refugee camps
they don't make decisions on the short term
what's that tell you?
that that's an issue
that we're going to be facing for a while
they're making educational materials
for children
in refugee camps
and this is made possible
because Lego
good job guys, build big and all that
gave them like a hundred million dollars
and uh... the MacArthur Foundation
gave them a bunch too, I think actually another hundred million
but the point is
this institution
this organization that has been around for a very long time
sees this as a need and they probably see it as a need that's going to be one
that uh...
is around for a while
with that in mind
we need to realize we're gonna have to deal
with the migration of people
more and more
especially as climate change really starts to take its toll
a lot of the stereotypes
that we have about people from other places they gotta go
a lot of the nationalism
we have
it's gotta go
because the world is changing people are moving
there are a lot of institutions
that are not political
that uh...
have a long history
kind of forecasting what's gonna happen
that are taking note of this
we're gonna have to work on it
we're going to have to accept that people are going to move
they're gonna have to
their
current locations are going to be inhospitable
either through weather
or because of tensions caused
by the climate
the world is changing and we're going to have to adjust
anyway, it's just a thought
have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}