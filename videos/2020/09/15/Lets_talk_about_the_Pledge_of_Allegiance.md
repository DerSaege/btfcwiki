---
title: Let's talk about the Pledge of Allegiance...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EIz0C8r4ERc) |
| Published | 2020/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the historical revisions of the Pledge of Allegiance from 1892 to the current version in 1954.
- Talks about the importance of being concise in writing as a journalist.
- Emphasizes the need to eliminate irrelevant details to convey the main idea effectively.
- Suggests that focusing on the core values of liberty and justice for all is more impactful than pledging allegiance to a symbol.
- Advocates for a revised pledge that simply states "I pledge allegiance to liberty and justice for all" to represent the true essence of the idea.
- Points out that liberty and justice should be universal principles, transcending borders and applying to everyone.
- Encourages prioritizing ideas over symbols for a better world.
- Concludes with a reflection on the potential for positive change if people prioritize values over symbols.

### Quotes

- "I pledge allegiance to liberty and justice for all."
- "Not just does it better embody things, not just is it more concise, it's universal."
- "Gives us something to actually fight for, something worth fighting for."

### Oneliner

Beau suggests refocusing the Pledge of Allegiance on universal values of liberty and justice for all, transcending borders and symbols.

### Audience

Journalists, educators, activists

### On-the-ground actions from transcript

- Advocate for universal values of liberty and justice in your community (suggested)
- Prioritize fighting for equitable rights for all individuals (exemplified)

### Whats missing in summary

Importance of prioritizing core values over symbols for societal progress.

### Tags

#PledgeOfAllegiance #LibertyAndJustice #UniversalValues #Journalism #Activism


## Transcript
Well howdy there internet people, it's Beau again. So tonight we're going to talk about the pledge.
We're going to do this because Carrie posted something. Carrie was my editor in an outlet I
used to write for. Now she does stuff for this channel. She posted something and it got me
thinking. So I went back and looked and sure enough it's true. The Pledge of Allegiance has
gone through several revisions over the years. It started out in 1892. I pledge allegiance to my
flag and the republic for which it stands, one nation indivisible with liberty and justice for
all. Then an early revision 1892 to 1923. I pledge allegiance to my flag and to the republic for
which it stands, one nation indivisible with liberty and justice for all. 1923 to 1924 I pledge
allegiance to the flag of the United States and to the republic for which it stands, one nation
indivisible with liberty and justice for all. 1924 to 1954 I pledge allegiance to the flag of the
the United States of America and to the Republic for which it stands, one nation, indivisible,
with liberty and justice for all.
1954, current version.
I pledge allegiance to the flag of the United States of America and to the Republic for
which it stands, one nation, under God, indivisible, with liberty and justice for all.
The fact that this was inspired by an editor is just a happy coincidence in some ways.
As a journalist when you're writing, you have to be concise.
You have a word count.
You've heard it in movies.
Give me 300 words, give me 600 words.
If you have ever written, you know that 300 words is not much.
600 words is not much.
You have to be concise.
The easiest way to be concise is to get rid of anything that doesn't really matter to
the main story, the main idea, what you're trying to convey.
Now if you're up against a word count and you're trying to figure out what to get rid
of, the first place to go is your revisions.
Because if you had to revise it to make it clear and it's really not that important to
the main idea, it can go.
If you were to apply this concept to the pledge, you would end up with a pledge that sounds
something like, I pledge allegiance to liberty and justice for all.
The rest of the clauses at some point had to be revised.
I pledge allegiance to liberty and justice for all.
And it's not a whole lot better.
Doesn't it really get to the point?
You're not pledging allegiance to a symbol, but to an idea, something that matters.
Not the symbol that is supposed to represent that idea, but the actual idea.
That's where your allegiance lies.
So much better.
It truly embodies the promise that this country made.
Liberty and justice for all.
And it got it wrong in the beginning, right?
We all know that.
And as time went on, that umbrella of liberty and justice, well it covered more and more
people because that's what matters.
Not the flag.
Flag's a symbol representing that idea.
The idea is what matters.
That shortened pledge, I pledge allegiance to liberty and justice for all.
Not just does it better embody things, not just is it more concise, it's universal.
It doesn't stop at the borders, because beyond America's borders do not live a lesser people.
It applies to everybody.
Gives us a goal.
Gives us something to actually fight for, something worth fighting for.
I personally think that if we focused on the ideas rather than the symbols, the world would
be a whole lot better off.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}