---
title: Let's talk about Trump's climate conversation and curiosity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=66ZVpQ1ISZk) |
| Published | 2020/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's response to climate change concerns: "it'll start getting cooler, just you watch."
- President's lack of belief in science: "I don't think science knows actually."
- Beau criticizes recent presidents for lack of curiosity and thirst for knowledge.
- Beau suggests a daily two-hour discretionary briefing for the president to learn about any topic of interest.
- Beau envisions the president engaging with experts from diverse fields during these briefings.
- Criticism of leaders' lack of curiosity and tendency to dismiss education.
- Beau points out the danger in leaders not seeking more knowledge.
- President's ignorance on climate versus weather is concerning.
- Beau argues that a president who does not actively seek education does not deserve a second term.
- Critique on leaders who stop learning and refuse new ideas.
- Beau expresses embarrassment over having an ignorant president in office.
- Call for leaders to continuously learn and grow intellectually.

### Quotes

- "He doesn't know the difference between climate and weather."
- "Any president that is not remarkably better educated by the end of their first term doesn't deserve a second one."
- "It's an embarrassment to the United States to have a man like this sitting in the Oval Office."

### Oneliner

Beau criticizes the president's ignorance on climate change and advocates for leaders to prioritize continuous learning and curiosity.

### Audience

Citizens, Voters, Activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for leaders who prioritize education and curiosity (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the president's response to climate change, the importance of leaders being educated and curious, and the consequences of willful ignorance.

### Tags

#ClimateChange #Leadership #Education #Curiosity #PoliticalCritique


## Transcript
Well howdy there internet people, it's Bob again.
So today we're going to talk about that conversation that the president had and then I'm going
to kind of voice a personal complaint that I've had about most recent presidents.
Something that has really bothered me.
And this is truly a bipartisan issue.
Okay so if you don't know about the conversation, the president is out there and he's engaged
in this discussion and he's being told about climate change and the record temperatures
and how it's important to save vegetation and all of this stuff.
And the president of the United States replies by saying, it'll start getting cooler, just
you watch.
And the official said, I wish the science agreed with you.
To which the president of the United States replied, I don't think science knows actually.
Yeah, that happened.
Okay, let's get through all the jokes.
Yes, winter is coming.
Yeah, the last time the president made a weather prediction about something going away when
the weather changed, 200,000 Americans ceased to be.
He understands what's going on, he's just trying to make sure we don't panic.
Yeah, and all of that's fine.
But this underscores something that has bothered me about a lot of recent presidents.
You guys joke, you know, Bo 2024 and all of that.
If I was president, every day on that schedule that goes out to the press, there would be
a two hour block that just says discretionary briefing.
And that briefing would be about whatever piqued my curiosity the night before.
Because as president of the United States, you have access to the best and brightest,
the most brilliant minds on the planet will come explain anything to you at the drop of
a hat.
So for two hours a day, I would be sitting cross-legged on the briefing room floor, looking
up at whatever brilliant person they brought in to answer my question.
What is that face looking thing on Mars?
Some guy from NASA would show up and explain it to me and explain everything that I could
possibly want to know.
Is the Megalodon really extinct?
What about those teeth they found?
Some woman would climb out of her submarine and show up and explain it to me.
And it doesn't have to be somebody from one of the dozen scientific agencies within the
executive that have rooms upon rooms of experts and brilliant scientists sitting around doing
science stuff all day.
It could be from the private area too.
Call up an Ivy League college.
You know, were the Amazon women, were they really the Scythians?
Some professor would show up and explain it.
And if I mispronounce that word, it's because I've only ever read it.
Because I've never heard anybody say it.
Because I don't have a panel of experts on tap.
It's embarrassing on a lot of levels that our leaders don't have any curiosity.
Don't want to know more.
Because we've gotten into this habit of looking down our nose at education.
We need to stop.
They're really going to kill us.
At the end of the day, this conversation, it matches the rest of the president's scientific
prowess goes along with the disinfectants and everything else that he's ever said.
It's willful ignorance.
Because he has tens of thousands of experts on tap.
And at the end of the day, here we are talking about one of the world's most pressing issues.
And the president of the United States doesn't know the difference between climate and weather.
That's what happened there.
That's what this conversation is.
He doesn't know the difference between weather, short-term changes in the atmosphere, and
climate, long-term patterns in a region.
He doesn't know that.
He walked away from this conversation thinking he's going to look like a genius because he
knows what month it is.
He understands that it's going to get cooler.
He's this ignorant to the subject matter about one of the world's most pressing issues.
Any president that is not remarkably better educated by the end of their first term doesn't
deserve a second one.
They have no thirst for knowledge, no curiosity.
Then you have to wonder when they stopped learning, when they gave up learning.
How many new ideas they just refuse to hear because they think they already know it all.
It's an embarrassment to the United States to have a man like this sitting in the Oval
Office.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}