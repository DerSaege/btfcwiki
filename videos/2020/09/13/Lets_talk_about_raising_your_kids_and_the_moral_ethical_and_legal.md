---
title: Let's talk about raising your kids and the moral, ethical, and legal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wYCYh6xpxcs) |
| Published | 2020/09/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advises on discussing philosophy and politics with children to avoid creating ideological followers.
- Suggests discussing ideas and policies instead of focusing on individuals or parties.
- Encourages humanizing historical figures to prevent blind hero-worship and cult of personality.
- Differentiates between legal, ethical, and moral values to guide children in decision-making.
- Uses examples like slavery in the 1850s to illustrate the distinctions between legal, ethical, and moral standards.
- Emphasizes the importance of teaching children to develop their own values based on critical thinking.
- Warns against sensationalizing historical events and emotions when discussing sensitive topics.
- Urges to avoid personalizing historical figures or events and focus on analyzing ideas and policies.
- Stresses the significance of helping children think independently and critically about moral, ethical, and legal dilemmas.
- Recommends focusing on policy, ideas, and values rather than individuals or rigid ideologies to cultivate independent thinking.

### Quotes

- "Teach them history, not mythology."
- "Legal, ethical, and moral. These three things aren't the same."
- "Morals are what shapes ethics and ethics are what shapes the law."
- "Talk about policy and ideas. Talk about moral, ethical, and legal."
- "You don't want to create little ideological foot soldiers."

### Oneliner

Beau advises discussing ideas and policies, humanizing historical figures, and teaching children to classify values as legal, ethical, and moral to foster critical thinking.

### Audience

Parents, educators, caregivers

### On-the-ground actions from transcript

- Teach children about historical events factually and without sensationalizing (implied).
- Encourage children to think critically about values and decisions based on legal, ethical, and moral considerations (implied).
- Engage children in discussing policies and ideas rather than focusing on individuals or parties (implied).

### Whats missing in summary

Importance of guiding children to think independently and critically about complex issues.

### Tags

#Parenting #Values #CriticalThinking #Education #Children


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about how to raise your kids.
Not really, I'm not really going to tell you how to raise your kids.
But I've had a number of questions related to one thing in particular when it comes to
kids and instilling values.
And it's basically how do I discuss philosophy and politics with my children and not create
little ideological foot soldiers.
Okay first, let me just stop here.
If this is even remotely a concern of yours, you're not going to do it.
You'll discuss things in a way that allows them to develop their own values.
If this has even crossed your mind, you're not going to do it.
But there are some techniques that can be used to help people determine their own values.
And these are actually, you're familiar with them because I use them on this channel.
If you're talking to your kids about philosophy and politics, you're treating them as an adult.
If you're wanting them to develop their own values, you're treating them as an adult.
So you can use the same techniques that are used with adults.
The first is discuss ideas and policy, not people and parties or rigid ideological structures.
Name a politician I endorse.
If you can't, name policies I endorse.
You can name a whole bunch.
That's how it works.
If you remove the person from the equation and you're just talking about the policies,
they can't look to that person that they agreed with once and use that to shape their other
opinions.
So if you see somebody starting to get a hero, somebody that they're going to use to shape
their own beliefs, you have to humanize them.
Nobody is perfect.
Heroes were people.
People are fallible.
They make mistakes.
They're not right on everything.
So when you see your child start to, let's say, look up to FDR a whole lot, maybe it's
time to break out a book about what happened to the Japanese in the U.S. during World War
II.
Teach them history, not mythology.
Don't let them have heroes that they view as infallible.
It's okay to have heroes, but make sure you understand their mistakes, their errors.
If you do this, you remove the cult of personality that comes with historical figures.
Because if they fall into that, they'll ignore all of the negative stuff.
We see this today with a lot of Trump supporters.
They will ignore everything negative that he does because they don't support a policy
or an idea.
They support a person and maybe an emotional attachment.
We'll get to that in a minute.
Doing that will help them determine their own values.
Then you have to kind of demonstrate how to classify their values because not all values
are the same.
Morality is not the same as legality.
We've said it a hundred times on this channel, right?
There's also ethical, which is in the middle.
So you have moral, ethical, and legal.
Legal is what's permissible.
It's the bare minimum.
This is what you have to do to avoid direct negative repercussions from the state.
That's the bare minimum standard.
Ethical is society at large.
What society at large wants you to do.
What best practices are as determined by society.
And that is designed to benefit society as a whole.
Moral, those are your own.
And theoretically, those, at least I believe, should be more strenuous than just about anything
else.
I'm supposed to be guiding my belief on that.
Those are the ones that you determine for yourself.
Now an example that gets thrown out sometimes is not my baby, not my fountain.
It's kind of common, not as common as it used to be because the premise is you're at a park
and there's a baby in a fountain.
It's not your baby.
It's not your fountain.
Do you have an obligation to stop that child from getting hurt?
Legally, now let me stop here because there's the traditional way of explaining this argument
and then there's the reality of today.
If you were in the US, understand that in most jurisdictions, you kind of have a legal
responsibility to remove the child from the fountain.
But at the time this argument was made, initially you didn't, not in the US anyway.
Because you have no obligation because it's not your baby, it's not your fountain.
Legally, you can just sit there and watch.
Ethically, would you do that?
Would society want you to do that?
No, of course not.
Of course not.
Because what is best for society is that people work to defend the innocent, protect innocent
lives.
Moral, why do you get the baby out of the fountain?
Because not doing so is evil.
These three things aren't the same.
Legal, ethical, and moral.
I've seen grown men try to refute moral arguments with legal ones.
Don't let your kid fall into that.
Because if you're worried about creating little ideological foot soldiers, you also don't
want them to just be the government's, allow the government to determine what is best.
The government is a child of the people, the people are not children of the government.
So you don't want them to take their morals from the law.
You want them to go beyond that.
They don't need to be told what to do in every instance.
Let's take it out of the theoretical and into the tangible.
It's the 1850s.
Slavery.
It's legal in the southern US, right?
It's ethical in the south.
In the south in the 1850s, it was ethical.
That was how society viewed what was best.
In the north, it wasn't.
What if you lived in the south and Harriet Tubman was one of your friends?
You let her come over to your house.
You were operating from a moral base.
You were going above and beyond the ethical and the legal.
If you lived in the north, there was no law and no ethical restriction on you taking a
slaver's profits and using that to invest in the north.
But morally, you may decide to do that.
You may say, no, it's a hideous blot.
It has to be undone.
I want nothing to do with this.
That's moral.
Now this will help them classify their values that they develop on their own.
Beyond that, don't sensationalize.
Now with an example like slavery, it's kind of hard not to sensationalize.
I mean, it's objectively a horrible thing.
So it's hard to kind of not do that with that particular topic, but explain the realities
of it.
You don't have to become very emotional and say, look at how horrible this was.
Objectively, it's horrible.
There's no reason to bring emotion into it on that level when you're talking about today.
Now that's a really bad example because it is an emotional topic, but there are other
topics that you would want to avoid that with.
With something like slavery or a lot of the other more horrible events throughout history,
emotion is going to come into it.
But you want to limit it.
I know that doesn't make sense, but you want to keep it to a minimum because you want people
to develop their ideas based on the moral, ethical, and legal, not an emotional reaction.
At the same time, don't let anger come into it.
When you are talking about historical figures, people in general, don't let anger come into
it.
This person was horrible in every regard because they probably weren't.
Just like the heroes, the villains of history also probably did something right at some
point.
We don't necessarily know what it is, but it's probably true.
So you don't want to personalize it in either way.
You want to talk about ideas, policies, not people.
So don't let anger get the best of you.
And don't downplay it.
Right now there's a whole bunch of people saying slavery was a necessary evil.
And they're saying this to refute the 1619 Project, which oddly enough is the argument
of the 1619 Project.
That's a really bad argument if you're making that and saying slavery is a necessary evil,
therefore we shouldn't teach the 1619 Project.
You're failing on many, many levels.
But you don't want to downplay it either.
You don't want to say, oh, this was necessary at the time because it was it.
Was it really?
Or say it's just the way it was.
Because the legal standard of the day and the ethical standard of the day, yeah, those
are fixed.
Can't change those.
The moral standard of today, looking back, can be applied retroactively.
Not because it's going to change the outcome, but because it helps the person determining
their values understand that their views, their morals, will be viewed through the scope
of history written 100 years from now.
And decisions they make will shape the future because morals are what shapes ethics and
ethics are what shapes the law.
So if you're one of those people who uses the law to determine your morals, you're doing
it backwards.
You're doing the very bare minimum.
This is why you will find phrases like law and order resonating with conservatives because
conservatives like tradition.
The legal is the last to be impacted by new ideas, which is funny because conservatives
also like to paint themselves as most moral, which is...
Anyway.
So at the end of the day, if you're the least bit concerned about this, you don't want to
create little ideological foot soldiers, you choose to discuss policy and ideas instead
of people and parties and rigid ideological structures, you don't allow them to have infallible
heroes and you teach them how to categorize their values, moral, ethical, legal.
You do that, they will develop their values on their own.
They won't be little ideological foot soldiers.
But I would also warn you that it may seem like they are following in your ideological
footsteps because for many people, this, if you're being objective about it, it's going
to shape your values and it will end up being the same values.
There's a reason why most people on this channel agree on a lot of stuff.
It's because this is how the arguments get made.
This is how the sausage is made here.
So we frame things typically on the moral side because that to me is the most important.
Some people would suggest that ethics are.
Some people who are definitely wrong would suggest that the legal is.
But this is a good technique to get people to think for themselves and this can be used
also to help deprogram Trump supporters when the time comes.
Never mention Trump. Talk about policy and ideas.
Talk about moral, ethical, and legal.
Don't talk about him.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}