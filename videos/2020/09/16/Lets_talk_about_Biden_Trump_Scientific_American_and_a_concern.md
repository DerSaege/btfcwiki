---
title: Let's talk about Biden, Trump, Scientific American, and a concern....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qbLBFyBBMD8) |
| Published | 2020/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Scientific American endorsed Joe Biden for president after 175 years of not endorsing anyone.
- The endorsement seems more like an indictment of President Trump than praise for Biden.
- Beau is concerned that people may view a Biden win as the end of the fight, when in reality, it's just the beginning.
- He believes that Biden's victory will be about damage control, not a total victory for the little guy.
- Beau stresses that the fight for change, equity, and justice in the US doesn't end with a Biden presidency.
- Trump didn't create the problems in the country; he just brought them into the limelight through his incompetence and corruption.
- Beau calls on individuals, especially those at the bottom, to stay active and push for necessary change.
- He sees a Biden victory as a call to action, where people need to seize the initiative and work towards creating the change needed in the country.
- Beau points out that the problems Trump showcased have always existed but were not as visible to many.
- He concludes by reminding everyone that a Biden win is not the ultimate victory but a prompt to keep pushing for a fair and just system in the US.

### Quotes

- "A Biden victory isn't the end of the fight. It's the bugle sounding the charge."
- "If you want to change this country, if you want to create a fair and just system, if you want to have liberty and justice for all, the fight doesn't end when he takes office."

### Oneliner

Scientific American's endorsement of Biden doesn't signal enlightenment but rather indicts Trump; Beau warns that a Biden win is just the beginning of the fight for change and justice.

### Audience

Activists, Voters

### On-the-ground actions from transcript

- Seize the initiative to create necessary change (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on the implications of a Biden win beyond surface-level celebrations, stressing the ongoing fight for equity and justice in the US.

### Tags

#BidenAdministration #ScientificAmerican #Election2020 #Activism #Justice


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about my concerns with a possible Biden administration.
Well one of them really.
One big one.
The Biden campaign picked up a pretty cool endorsement.
Scientific American.
Endorsed Joe Biden for president.
That's weird to say.
It's weird to say because they've never endorsed anybody for president and they've been around
175 years.
So what's that mean?
Does it mean that a Biden presidency will usher us into an era of scientific enlightenment?
Or does it mean that the Trump administration was really just that bad?
That they felt compelled to break with 175 years of tradition?
If you read the endorsement, it's pretty obvious which of those two it is.
Doesn't heap much praise on Biden.
It reads like an indictment of President Trump.
A fair one.
Let's be real clear on that.
And that's my concern.
My concern is that people will view a Biden win as the end of the fight when in reality
it's the beginning.
Biden's damage control.
That's it.
Maybe he can stick his finger in the hole in the levy.
But the levy's still got a hole in it.
We still have to fix the system.
All of the inequities, all the problems that exist in the United States today will exist
the day after Joe Biden takes office, if he wins.
They're still going to be there.
And it is unlikely that the D.C. establishment will fix them.
It's got to be you and me.
Has to be those of us on the bottom.
We have to stay active.
A Biden victory isn't the end of the fight.
It's the bugle sounding the charge.
That's when we have to go on the initiative.
It's when we have to seize the initiative and move forward and try to create the change
that is so necessary in this country.
Trump didn't appear out of nowhere.
And the problems that he highlighted through his incompetence and corruption, they've always
been there.
They've always been there.
He was just too arrogant to conceal them.
So a large portion of the United States who never really saw it got to see it.
We have to keep moving.
A Biden victory is not a victory for the little guy, not a total victory.
It's just damage control.
If you want to change this country, if you want to create a fair and just system, if
you want to have liberty and justice for all, the fight doesn't end when he takes office.
That's when it begins.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}