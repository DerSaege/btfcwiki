---
title: Let's talk about Greta's speech one year later....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_bL5BVW_nT0) |
| Published | 2020/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a speech given a year ago, dismissed due to various reasons like speaker's age and appearance.
- Urges to revisit the message rather than dismissing it outright.
- Quotes the impactful message: "My message is that we'll be watching you."
- Speaker expresses frustration at being on stage instead of in school, accuses adults of stealing dreams and childhood.
- Condemns the focus on money and economic growth while the planet suffers.
- Criticizes the lack of action despite clear scientific evidence on climate change.
- Draws parallels to public health issues and government's prioritization of profits over people's well-being.
- Emphasizes that those in power often escape the consequences of their actions due to wealth and privilege.
- States the relevance of the speech today and the need to learn from historical instances of downplaying crises for personal gain.
- Urges preparation and action for the impending challenges, stressing that no borders or flags will offer protection.

### Quotes

- "My message is that we'll be watching you."
- "You've stolen my dreams and my childhood with your empty words."
- "This issue that we're facing today, it's a dress rehearsal for what's coming around the bend."

### Oneliner

Beau revisits a dismissed speech, warning about the consequences of dismissing urgent messages and the need for preparation amidst looming challenges.

### Audience

Activists, Climate Advocates

### On-the-ground actions from transcript

- Get involved in electoral politics to push for necessary changes (exemplified)
- Prepare and insulate yourself for future challenges (exemplified)

### Whats missing in summary

The emotional impact of Greta Thunberg's speech and the critical need to heed warnings about climate change and societal issues.

### Tags

#Speech #ClimateChange #Power #Action #Preparation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about an address, a speech.
It was given one year ago today.
Almost as soon as this little address was given, people dismissed it.
They dismissed it because of the delivery, because of the age of the speaker, because
of the way she looked, any possible reason to dismiss the content.
When that happens, sometimes it's good to go take a look at it again later, see if it
was really worth dismissing, or if it was a message that people just needed a real lesson
on first.
My message is that we'll be watching you.
This is all wrong.
I shouldn't be up here.
I should be back in school on the other side of the ocean.
Yet you all come up to us, young people, for hope.
How dare you?
You've stolen my dreams and my childhood with your empty words, and yet I'm one of the lucky
ones.
People are suffering.
People are dying.
Entire ecosystems are collapsing.
We are in the beginning of a mass extinction, and all you can talk about is money and fairy
tales of eternal economic growth.
How dare you?
For more than 30 years, the science has been crystal clear.
How dare you continue to look away and come here saying you're doing enough, when the
politics and solutions needed are still nowhere in sight?
You know, with the exception of just a couple of lines, that could really be talking about
the US handling of the public health issue.
This was dismissed.
Greta was dismissed.
Out of hand, because people didn't really want to believe it.
What she says here is completely true.
It's completely accurate.
Those in power, and this goes for public health, and it goes for our environment, well, they're
not going to let anything get in the way of their profits.
Not really.
Because it doesn't impact them.
Because they're at the top.
They'll mask the issue, you know, try to put a happy face on it.
But whether it is 200,000 people or 200 million, at the end of the day, they're nobodies.
Doesn't really affect many people at all.
We're doing a good job, because it doesn't impact them, the people they know.
Because the people they know, well, they've got money, and they can insulate themselves
from it.
This speech was given a year ago today, and it's probably more relevant today than it
was when it was given.
But at the time, we didn't have a concrete lesson in it.
I mean, we do, actually, throughout pretty much all of recorded history.
But we didn't have a recent one, one we could point to, one where it was extremely
clear the government knew there was an issue, those in power knew there was an issue, and
they downplayed it for their own benefit.
This issue that we're facing today, it's a dress rehearsal for what's coming around
the bend.
Something we should get ready for.
Now, that can mean really getting involved in electoral politics and trying to get those
in power to do what's needed.
Or it can mean preparing and insulating yourself.
But just like this thing that's out there now, it doesn't care.
It's not going to stop at the border.
The flag isn't going to protect you.
You've got to be ready to deal with it, or we have to address it.
Because it is coming.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}