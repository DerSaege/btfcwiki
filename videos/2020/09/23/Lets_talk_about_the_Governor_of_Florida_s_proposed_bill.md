---
title: Let's talk about the Governor of Florida's proposed bill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pZ5YbJAu2BA) |
| Published | 2020/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is discussing legislation coming out of Florida proposed by Governor Ron DeSantis, critiquing it as a way to mask the core issues rather than address them.
- The legislation includes a prohibition on disorderly assemblies of more than seven people and enhanced penalties for obstructing roadways, allowing vehicles to run over protestors, toppling monuments, and harassing people in public accommodations.
- It adds a RICO liability, mandatory minimum jail sentences for striking an officer, and denies bail for certain offenses.
- Local jurisdictions in Florida could lose state money if they defund the police, creating a victim compensation mechanism allowing residents to sue the government if it fails to protect them.
- Beau criticizes the legislation for not addressing the real problem: police violence leading to unarmed individuals being killed.
- He suggests implementing mandatory minimum sentences for police officers who shoot unarmed individuals and predicts the ACLU will challenge the unconstitutional aspects of the bill.
- Beau believes that increasing police presence and violence to suppress speech about law enforcement will only fuel the movement further.
- He concludes that the legislation is flawed, unconstitutional, and will likely be struck down, failing to address the underlying issue.

### Quotes

- "The problem isn't that there's people in the streets. The problem is people feeling they have to be in the streets."
- "The issue is folks getting killed by the cops while they're unarmed."
- "This is the type of legislation that gets proposed by failed leaders in failed states."

### Oneliner

Beau criticizes Florida's legislation as a flawed attempt to mask issues rather than address them, especially failing to tackle the core problem of police violence.

### Audience

Activists, Civil Rights Advocates

### On-the-ground actions from transcript

- Challenge unconstitutional laws through legal action (implied)
- Advocate for laws addressing police violence and accountability (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Florida's legislation and Beau's perspective on addressing police violence and systemic issues effectively.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about some legislation
coming out of Florida,
proposed by the Florida governor, Ron Santos.
We're gonna go through it,
simply because it fits with the theme
of what we're talking about,
masking the problem, hiding it,
not addressing the core issues,
but just trying to cover it up.
And that's what this legislation is, really.
In a lot of ways, to me, it doesn't go far enough.
There's some workable pieces in it,
but it just doesn't get there.
So, highlights of this legislation include
a prohibition on disorderly assemblies
of more than seven people,
and it appears to provide for collective punishment
of people who attend them,
which I'm fairly certain is unconstitutional.
Enhanced penalties for obstructing roadways
and allowing people to just run those folks over,
which I'm also fairly certain is...
Anyway, toppling monuments has enhanced penalties
for harassing people in public accommodations.
It adds a RICO liability,
creates mandatory minimum jail sentences
for striking an officer,
or an enhancement if you throw something
and it just randomly hits a normal person,
not one of the protected people.
Local jurisdictions in Florida
will not be able to defund the police,
or they risk losing state money,
which would cause them to make more budget cuts.
Creates a victim compensation mechanism
that allows residents to sue the government
if it neglects to protect them.
Remember that one.
Terminates benefits if somebody's convicted under this act,
and denies bail.
Okay, first things first,
I wouldn't worry too much about this.
The ACLU is gonna eat this alive.
There are just giant sections of this
that are blatantly unconstitutional.
But beyond that, it doesn't actually address the problem.
The problem isn't that there's people in the streets.
The problem is people feel they have to be in the streets.
That's the issue.
Why?
Because folks are getting killed by the cops
while they're unarmed.
That is actually the issue.
So there are some workable pieces in this,
like this mandatory minimum thing.
Let's do that, but let's say it's for police officers.
For police officers who shoot someone
who doesn't have a weapon.
Let's do that.
And I know, well, why would you do that?
I would suggest if you know a ton of bricks
will rain down on your head,
then I think people will think twice.
That's what Governor DeSantis said
to justify this monstrosity of a bill.
I know people are gonna say,
cops have to deal with a lot of stuff,
very tense situations,
and they may do something in the heat of the moment,
like a protest where it gets tense
and somebody throws something.
So I guess the average citizen who received no training
has to keep their cool,
however law enforcement can do whatever they want.
At the end of the day, if this passes,
the only piece that I think will really end up standing
is the part about being able to sue the government
if they fail to protect you,
which I think would apply to state agencies
that don't properly curtail officers.
This issue is not new.
It's been around a long time,
and it's gonna stay around until something's done about it.
The more lawmakers clamp down,
the bigger the movement will get.
This is a mistake.
I mean, not just in the fact
that large sections of it are unconstitutional,
it's gonna get struck down and all of that.
It's not addressing the issue,
and even the idea of using more police violence
and more police presence to curtail speech
about law enforcement is going to end up making it spread.
This is the type of legislation
that gets proposed by failed leaders in failed states.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}