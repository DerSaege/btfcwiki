---
title: Let's talk about a defunding declaration and an American idea....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Tg1ONaZDuho) |
| Published | 2020/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Declaration on funding and American identity discussed due to news from Mitch McConnell's Kentucky.
- Absence of progress, forward movement, reform, or justice apparent.
- Social media calls for defunding Louisville prompt Beau's reflection on American values.
- Beau questions the notion of defunding police being un-American.
- Compares current policing issues to grievances in the Declaration of Independence.
- No taxation without representation principle linked to police accountability.
- Defunding the police viewed as a patriotic act rooted in American history.
- Cutting police funding can prompt necessary priorities and accountability.
- Defunding specific problem departments is advocated for improved accountability.
- Politicians opposing defunding reveal their priorities lie elsewhere than with the people.

### Quotes

- "Defunding the police is as American as it gets."
- "No taxation without accountability."
- "Defunding the police is a just call, and it would be effective."
- "They don't get money if we can't hold them accountable."
- "It's literally one of the most American things you can say."

### Oneliner

Beau questions the American identity in relation to defunding the police, drawing parallels to the Declaration of Independence and advocating for police accountability.

### Audience

American citizens

### On-the-ground actions from transcript

- Advocate for defunding problem departments for improved police accountability (implied).
- Prioritize holding law enforcement agencies accountable in your community (implied).

### Whats missing in summary

Importance of linking historical American principles to contemporary issues for accountability and justice.

### Tags

#DefundThePolice #PoliceAccountability #AmericanValues #DeclarationOfIndependence #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a declaration on funding
and whether or not something is American.
We're going to do this because of the news coming out of Mitch McConnell's Kentucky.
I don't think anybody's surprised by the news, I think we all knew this was coming,
but even knowing it's coming doesn't actually make it okay.
It does not appear
that there's going to be any progress, doesn't appear that there's going to be any forward movement,
doesn't appear that there's going to be any reform,
and it certainly does not appear that there's going to be any justice.
As soon as this news broke,
some people took to social media to call for defunding Louisville.
Me. I'm some people in this case.
I was met by the idea that I shouldn't say that because it's un-American.
We back the blue here.
Man, that got me thinking.
Is it?
You know, it's hard to say for sure what the founders would do.
It is.
But what would they do if they had swarms of officers sent to harass them?
What would they do if they had militarized officers out on the street when they weren't necessary?
What would they do if those officers had been rendered immune to the civil authorities
and they were protected from punishment for any acts that they might do?
They'd write the Declaration of Independence.
That's what they'd do.
I know this may come as a shock to a lot of the sunshine patriots out there,
but the Declaration of Independence,
which is probably like your profile picture on your social media,
it's a lot more than the preamble.
There was actually a list of complaints in it.
Reasons that there were issues.
He has erected a multitude of new offices and sent hither swarms of officers to harass our people.
He has kept among us in times of peace standing armies.
Understand standing armies at the time,
those armies were tasked with enforcing the king's rule, enforcing the law.
Kept among us in times of peace.
Militarized officers when they weren't necessary.
He is effected to render the military independent of and superior to civil power.
Render them immune.
I don't know if he qualified it.
For protecting them by a mock trial from punishment for any murders
which they should commit on the inhabitants of these states.
The exact same complaints.
I'm using more flowery language here, but it's the exact same complaints.
Because this is a habit of governments in general.
What was the rallying cry in the 1700s?
How'd those patriots get people behind them?
No taxation without representation, right?
Everybody knows that.
You learn it in grade school.
What does that mean?
Representation.
Why do you want representation?
You can hold the government accountable, right?
So another way of saying this would be no taxation without accountability.
Because you don't want your tax money to go to fund something that's not
accountable to the people.
Defunding the police is as American as it gets.
It's literally in the Declaration of Independence.
It is the call of a patriot.
And I know there's some confusion over what that means, but
let's just run through it real quick.
If you cut funding to something, that entity has to decide what's important,
what their priorities are.
I would suggest that going out and playing soldier and kicking in doors
without doing the proper investigative work that goes along with it isn't a priority.
Defunding the police is a just call, and it would be effective.
It would make law enforcement agencies that are out of line,
it would make them prioritize.
And when I hear people complain about it, I hear people talk about it here.
We can't defund the Sheriff's Department.
Of course we can't here.
Well, they don't do any of this stuff either.
These calls are specific to problem departments,
to areas where they have forgotten what their actual duty is to protect and
serve, to areas where they're not accountable.
That's where this call gets made, and it makes complete sense.
Aside from it making complete sense,
it is literally one of the most American things you can say.
No taxation without accountability.
They don't get money if we can't hold them accountable.
That's how the country was started.
The fact that a lot of politicians are coming out against this shows where they
are, where their loyalties are, not to the American people,
not to the people that they're theoretically supposed to represent.
To themselves.
It's all they care about.
They're trying to put a mask on the problem, hide it away, give excuses.
That's not what we're supposed to do here.
The United States requires advanced citizenship.
You have to pay attention.
And if you have found yourself falling for
sloganeering that paints the idea that government entities should be held
accountable, otherwise they don't get money.
If you have fallen for the idea that that's somehow bad,
you have fallen for an un-American idea, because it's literally in our founding
documents.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}