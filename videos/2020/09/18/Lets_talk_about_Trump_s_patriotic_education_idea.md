---
title: Let's talk about Trump's patriotic education idea....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yqcu6C94j90) |
| Published | 2020/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- President wants a patriotic education program, but Beau believes it's a faulty premise.
- Quotes Thomas Paine, the founder of the American Revolution, to illustrate true patriotism.
- Thomas Paine's writings like "Common Sense" inspired rebellion and gave people something to believe in.
- Beau contrasts patriotism with nationalism through George Washington and Nathan Hale's quotes.
- Real patriots correct their government and defend a particular way of life, not seeking power or prestige.
- Patriotism is defensive and earned, not taught through indoctrination or mythology.
- Teaching only positive aspects of American history won't create patriots who question their government.
- To make America great, Beau suggests living up to the promises of past patriots like Thomas Paine.
- Real patriots question the government and strive to correct wrongs in society.
- Beau believes the president's patriotic education program is doomed to fail if it ignores critical aspects of history.

### Quotes

- "Patriotism is of its nature defensive, both militarily and culturally." 
- "Patriots do not obey no matter what, that's nationalists."
- "Just because you say something over and over again and people believe it doesn't mean that it's true."
- "Independence is my happiness, the world is my country, and my religion is to do good."
- "To make America great, that's how you do it. That was what moved people."

### Oneliner

President's patriotic education program is doomed to fail as true patriotism involves questioning the government and defending a way of life, not teaching mythology.

### Audience

Educators, policymakers, activists

### On-the-ground actions from transcript

- Question the government's actions and policies (implied)
- Defend a particular way of life that upholds values of true patriotism (implied)

### Whats missing in summary

The emotional impact and passion conveyed by Beau Young in his rejection of the president's patriotic education program.

### Tags

#Patriotism #AmericanHistory #Nationalism #Education #Government


## Transcript
Well howdy there internet people, it's Beau Young.
So the president would like to create a
patriotic education program
and I think we need to talk about that.
Not just for all the obvious reasons that people
should have some reservations about this idea
but because it's a faulty premise.
Actually can't be done, not really.
I'm going to open and close tonight
with quotes from Thomas Paine.
He was the spiritual and philosophical founder
of the revolution, the American Revolution.
He wrote a pamphlet called Common Sense
and it was read everywhere.
It was read aloud in churches,
taverns,
really solidified
the spirit of rebellion in the country.
There are many who would say
that without him, without his writings
being present at that time
the American Revolution would have happened.
Not then.
He gave people something to believe in,
laid out the philosophy,
gave them something to rally around.
One of the things that he said,
what he wrote,
these are the times that try men's souls.
The summer soldier and the sunshine patriot
will in this time of crisis shrink
from service to their country.
He wrote that in 1776.
George Washington said
every post is honorable
in which a man can serve his country.
He wrote that in a letter to Benedict Arnold
back when he was a good guy.
September 14, 1775.
I only regret that I have but one life
to lose for my country.
That's attributed to Nathan Hale.
September 22, 1776.
Give me liberty or give me death.
Patrick Henry.
March 23, 1775.
These are American patriots.
These are the people
that Trump's little reeducation program
would certainly try to lay claim to.
However, is he justified in doing that?
Is the US government justified
in laying claim to these people
in these passages in particular?
See, they were patriots, no doubt.
No doubt.
But they were American patriots
in the truest sense of the term.
They were patriots before July 2, 1776
when independence was declared.
They held no loyalty to the US as we know it,
not when these passages were said.
The Constitution wasn't even written
until much, much later.
When they said country,
they meant something else
because they were real patriots.
And as real patriots,
they tried to correct their government.
It's what real patriots do
because government is a child of the people.
Prior to independence being declared,
they were trying to correct the king.
All that stuff in the lead up
to independence being declared.
They were already patriots.
They had to be a patriot
just to show up to the meeting.
By patriotism, I mean devotion
to a particular place
and a particular way of life,
which one believes to be the best in the world
but has no wish to force on other people.
Patriotism is of its nature defensive,
both militarily and culturally.
Nationalism, on the other hand,
is inseparable from the desire for power.
The abiding purpose of every nationalist
is to secure more power and more prestige,
not for himself,
but for the nation or other unit
in which he has chosen
to sink his own individuality.
George Orwell.
Patriotism is loyalty to a place
and a way of life.
If that way of life is faulty,
if it is not living up to the promises,
patriots will attempt to correct it.
That's patriotism.
If you want people to be patriotic,
well, they have to have a way of life
they want to defend
because patriotism is defensive.
You can't teach patriotism.
It has to be earned.
Classes designed to indoctrinate children
and mythology breeds nationalism,
not patriotism.
Those two things are diametrically opposed.
This will not succeed in creating patriots
because patriots, by their very nature,
have to question their government,
question the things that it's doing.
If they are taught the mythology
that America is always making the right choices
and that everything's great,
they can't do that.
Any program designed to teach patriotism
will fail.
If you want to create patriots,
you have to give them a place
and a way of life they're willing to defend.
That's what creates patriots,
not lies, not fairy tales,
not mythology,
because this program is certainly
not going to teach American history.
If you want to teach history,
you have to teach the bad parts, too,
and in the United States,
there are volumes of it.
We haven't even lived up to the promises
in the Declaration of Independence
and the Constitution,
the promises set out by Thomas Paine.
We haven't even lived up to what he said
we should do
before we said we were great.
He said that
when it can be said by any country
in the world,
my poor are happy,
neither ignorance nor distress is to be found among them.
My jails are empty of prisoners,
my streets of beggars,
the aged are not in want,
the taxes are not oppressive,
the rational world is my friend
because I am the friend of happiness.
When these things can be said,
then may that country boast its constitution
and government.
Independence is my happiness,
the world is my country,
and my religion is to do good.
Thomas Paine.
That's how you make America great,
not with fairy tales.
Just because you say something over and over again
and people believe it
doesn't mean that it's true.
If you want to live up to the promises
of these patriots
that you so desperately want to claim
as your spiritual ancestors,
live up to their promises,
live up to their ideas,
embody it,
question the government.
Patriots do not obey no matter what,
that's nationalists.
Patriots
owe their allegiance to a place
and a way of life.
If the way of life is wrong,
they will attempt to correct it.
If it's right, they'll defend it.
You want to create patriots?
Make America great,
and that's how you do it.
That was what moved people.
This guy,
his words
cemented
the spirit of rebellion.
That's
our standard, that's what we should be trying to live up to.
Not just fairy tales for kids.
The president's program is doomed to fail,
and generally
when a country decides to rest on its laurels
and not improve itself,
the country fails.
Our goals are pretty clear.
Anyway,
it's just a thought. I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}