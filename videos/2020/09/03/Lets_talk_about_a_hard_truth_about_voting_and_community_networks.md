---
title: Let's talk about a hard truth about voting and community networks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=apGViv909eo) |
| Published | 2020/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Voting is the least involved and least effective method of civic engagement.
- Being actively involved in your community is key to producing immediate change.
- Forming a community network focused on making the community better generates power and strengthens the community.
- A network like this can reduce reliance on the government and confront issues directly.
- The network can circumvent red tape, be more flexible, and produce immediate results.
- Pooling resources within the community can accomplish significant goals.
- A community network consists of different groups like A group, B group, and a command group (C group).
- Recruiting people who can contribute specific skills can enhance the effectiveness of the network.
- People helped by the community network often become valuable resources in return.
- Ideological alignment is not necessary; the primary commitment should be to strengthen the community.
- Operating in urban and rural areas require different approaches in community networking.
- Limiting operations to a specific area initially and gradually expanding helps in building community support.
- Community networks can have a significant impact on local politics and governance.
- By publicly requesting politicians for assistance and taking action independently, the network can influence political decisions.
- Strengthening the local community through active involvement benefits everyone.

### Quotes

- "Voting is the least involved and least effective method of civic engagement."
- "Pooling resources within the community can accomplish significant goals."
- "Your little network did it."
- "This makes your local community stronger."
- "Just better your community, that's it."

### Oneliner

Voting is minimal; community involvement is key for immediate change, building networks, and influencing local politics positively, making communities stronger.

### Audience

Community members, activists, volunteers.

### On-the-ground actions from transcript

- Form a community network committed to bettering the community (exemplified).
- Recruit individuals with diverse skills to enhance the effectiveness of the network (exemplified).
- Actively involve in community initiatives to strengthen local governance (implied).

### Whats missing in summary

The full transcript provides detailed insights on forming a community network, recruiting members, and actively engaging in local initiatives for community betterment.

### Tags

#CommunityInvolvement #CivicEngagement #LocalGovernance #CommunityNetworking #Activism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about something I talk about a whole lot, only we're going
to talk about it in depth from the ground up, and we're going to talk about a hard truth.
The reason we're going to break everything down like this is because I had somebody tell
me that yeah I talk about this all the time, but I've never broken it down all the way.
And the only reason they even know what I'm talking about is because they know where I
took the template from, and that's fair, that is where I took it from.
And then the hard truth.
The hard truth, and this is going to be hard for a lot of people to accept, especially
in the United States, voting is the least involved and least effective method of civic
engagement.
It is literally the bare minimum you can do.
We cannot be surprised our government behaves the way it does when we really only hold it
to account every two or four years.
During that time they have free reign.
Of course they're going to behave the way they do, they have no accountability.
So what's the most you can do?
Be actively involved.
Be actively involved in your community.
A community network.
So what's the theory behind this?
It is a network of people who have committed to making their community better.
That is their only overt commitment.
That's what they're saying.
Now they may be in it for a specific cause, like they may be wanting to deal mostly with
food security, but when they join this little group, the only real mission statement is
to strengthen the community, to make the community better, period.
Don't make it too specific because you want to be adaptable.
A network like this can produce immediate change, immediate results.
It can reduce reliance on government, therefore making the government less powerful.
It can make your community stronger as people tend to rely on each other.
It lessens the effects of bad governance because you can deal with things yourself.
It also generates power if you want to engage in electoral politics.
So that's a whole lot of stuff.
How does it do this?
Because the network isn't the government.
It can circumvent red tape.
It is more flexible because it doesn't have a super defined mission.
It can confront issues directly and it can produce immediate results.
You want examples?
Imagine there is a pothole in the road in front of your house.
You can contact the city council and have them contact public works and hopefully a
requisition gets put in and eventually, maybe if they find the right address, they will
come out and fill it.
Or you can walk out there and fill it yourself.
Which one is going to get done first?
Same thing, a playground.
Well, it is rusty and not painted anymore.
You can ask the county commission to do something about it and maybe they will approve the funds
and maybe eventually they will get somebody out there.
It will probably take six months to a year or you can walk out there with your sander
and some spray paint.
The idea of pooling resources to accomplish something that the community feels is important
is something everybody watching this is aware of.
We all know what GoFundMe is.
That's what it is.
That's a digital community network.
No specific mission.
Just in theory, helping people achieve their goals.
What is the make-up of one of these groups?
You have your A group.
Your A group, those are the people that are most active.
Those are the people that are involved in this every day or every week, every month,
however you structure it.
It normally starts with a group of friends who are kind of close but not super close
and we will get to why here in a minute.
Co-workers, people from your religious institution, whatever.
The ideal number is 12.
You can start with less.
You can have more.
But in my experience, the ideal number has been 12.
Each one of those 12 people has friends who are going to be supportive of what they are
doing but they are not going to be active every day.
Those people that you can call on to occasionally assist.
That's your B group, your support group.
This is why it's important for those initial 12, that initial group, not to be super close
because if they are, they are going to share friends and it's going to cut down the number
of people in your support group.
Now as the A group becomes more active, people are going to want to join, especially those
from the B group, they are going to want to become more active when they see it produce
results.
It always happens.
Eventually, your group grows in size.
When it hits about 18, normally, it organically splits into two groups, naturally.
When that happens more than once, so you have 18 that splits and then they both grow up
to 18 again and they split again.
At this point, you need a C group and that's your command group, control group.
Each A group that exists designates one person to talk to the other designees from the other
A groups.
This could be as simple as a Facebook chat but just to keep everybody on the same page
and it also helps everybody share their B groups, those people that can support.
It increases the effectiveness of the groups.
Now if you used to live around Fort Bragg and all of this sounds really familiar, yeah,
that's where I stole the template.
ODA, ODB and ODC.
So what they are.
First term is going to sound really familiar too, force multiplication.
When you're recruiting, as you get more active and as you become more involved, you're going
to specifically identify things that need special attention within your community and
you're going to want to specifically recruit people that are good at that particular thing.
As an example, let's say food insecurity is something that's big in your area.
You have a big problem with this.
You're probably going to want to actively recruit somebody who works at a food pantry
or in a farmer's market because not just do you have somebody now that is really familiar
with these issues, you have access to their B group because whether or not people realize
it, everybody has a B group.
Everybody has those people that they can call on for support, hopefully.
Now if you can't recruit somebody that you're specifically trying to get, somebody from
a food pantry, there's nothing stopping one of your 12, one of your A group, from joining
their group, from volunteering at that pantry and gaining access to that network that way.
That's what this is really about.
What makes it effective is the networking aspect of it.
So there's always a way to gain access to the networks that you need, either by recruiting
somebody from that network or inserting somebody into it.
So how do you recruit, generally speaking?
You can start with social media and look at people who post about problems in the community
and see if they want to fix them.
A lot of them don't.
A lot of them just want to complain about it.
But you will find those who will get off the couch and get out there.
You can also look to other activist groups and see if you can find people who fit the
needs that you have in those and actively recruit them.
And then the most overlooked group of people, and this is especially true because every
single one of them is in your B group and you don't even think about it.
Those people you've helped, as your community network starts working and starts doing things,
you help people directly and indirectly.
Those people will almost always help you.
They're an incredibly important resource that a lot of times people forget about because
they look at them as those who need help.
Most times a need for help is temporary.
Once you help them with that problem, they can probably help you.
And even if they're in need of one form of help, let's say they have a food insecurity
issue, well stick with that.
They may be really good at graphic design and can help you with something else.
Just because somebody needs help doesn't mean they're helpless.
Now one important thing to remember is that the people in your group do not have to be
100% ideologically aligned.
All they have to commit to is making the community stronger.
My group is populated entirely by left-leaning anti-authoritarians and small government conservatives.
Two people who you would think would never talk to each other.
That's the entirety of my group.
And it's worked for a very long time.
Because it sounds weird, but those groups of people are natural allies at the community
level.
They only become opposition when you get to national level stuff, generally.
Okay now, whether you're in an urban area or a rural area determines a whole lot about
how you operate.
In a rural area, as an example, you don't need a name for your group because everybody
knows everybody.
Out here, our group does not have a name.
And you don't take credit for what you do.
You don't have a Facebook page talking about the things that you do.
You don't need to because you're in small towns and everybody talks.
In fact, if you had a Facebook page talking about the stuff that you did, people wouldn't
like it.
However, if you don't talk about it, other people will and they'll want to support it
and they'll know who to contact if they need help.
In an urban area, you can't do that.
There's too many people.
So the first thing I would do in an urban area is limit your area of operations to one
neighborhood.
You're not going to be able to do the whole city.
One area and pay specific attention to that.
And as you get that community support, you will get more people.
When your A group becomes two A groups, well then you can widen out and take on a larger
area.
This plays into the whole force multiplication thing.
And it helps you get to a point where you can effect real change pretty easily.
In rural areas, your area of work is not governed by ten square blocks.
You're talking about miles.
We have done stuff, I don't know, 60, 70 miles away.
Because it's a very rural area out here.
You got to go that far to get to people.
Let's see.
Either way, the organizations themselves, your little groups, they grow with community
support.
The more support you get, the more stuff you do, the more people will support you.
Once you have community support, you can effect a lot of change very quickly.
Because I started and because we're in an election season and that's what everybody's
thinking about, ballot access.
You need signatures, right?
If you have a group like this, you can get those signatures in a day.
Because your A and B groups will all sign and they know other people who would sign.
You can get it done very, very quickly.
I know there's probably some people that are like, I don't know why I would even want to
do that.
Imagine somebody of your ideological makeup as sheriff.
Yeah, wow, that would make it a whole lot easier to do stuff, right?
County commission, you can do it.
And this is how.
These small networks wield a lot of political power once they're successful.
And if you're not interested in electoral politics, like if you're the type of person
who just, I don't know, say absolutely refuses to endorse somebody because reasons, understand
that you can still wield the government.
Anytime before you do something, you make a public request to a politician, hey, we
need this done and then go do it.
You know they're not going to do it.
But then you point out that you asked them and that they did nothing and that you did
it.
Your little network did it.
You embarrass them a couple of times, they will respond when you ask them for something
because they don't want the negative press.
Whether you agree with government or not, you do know it exists.
So if you have the ability to exert some influence over it for good, maybe you should.
It doesn't matter what your goal is, what your long-term ideological goals are.
It doesn't matter where you sit on the political spectrum.
This makes your local community stronger.
That's always good.
That is always good.
There's never a downside to that.
This helps get people involved in a hands-on way in fixing their community and it's adaptable.
Don't come up with some specific mission statement.
Be very general.
Just better your community, that's it.
And that allows you to flow into a whole bunch of different stuff.
And the more you strengthen it, the better it is for everybody.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}