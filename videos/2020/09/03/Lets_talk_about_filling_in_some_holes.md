---
title: Let's talk about filling in some holes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=evdN5fdRKZc) |
| Published | 2020/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges feedback about holes in previous video examples.
- Views local government restrictions as an asset for community networks.
- Emphasizes changing societal mindset to effect real change.
- Advocates for focusing on actions not prohibited by local ordinances first.
- Suggests garnering public support before tackling controversial issues.
- Encourages using media attention to challenge unjust regulations.
- Questions the logic of prohibiting community improvement.
- Proposes government facilitation instead of control over community initiatives.
- Considers government employees as public servants, not rulers.
- Advises choosing battles wisely to achieve meaningful victories.
- Stresses the importance of swaying public opinion over direct confrontation.
- Advocates for creating parallel systems to support communities where the government fails.
- Announces plans for a workbook to further these ideas.

### Quotes

- "Your local government has made it illegal to better your community. That seems a little silly."
- "You're creating a parallel one. You're creating something that people can turn to when the government fails as it typically does."
- "You want that secondary safety net for your community."

### Oneliner

Beau views local government restrictions as assets, advocates for changing mindsets, and building community support to challenge unjust regulations and create parallel systems for community safety nets.

### Audience

Community members

### On-the-ground actions from transcript

- Reach out to local press to support community improvement initiatives (implied)
- Advocate for government facilitation rather than control over community projects (implied)
- Begin planning and organizing community improvement projects with public support (implied)

### Whats missing in summary

The full transcript provides detailed insights and examples on navigating local government restrictions, building community support, and advocating for positive change in society, complementing the summarized points. 

### Tags

#CommunityEmpowerment #GovernmentRelations #CommunitySupport #SocialChange #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So uh...
I guess today we're going to talk about filling in some holes.
Because I guess I left some holes in that last video.
uh...
Almost as soon as it went up
there were people that were like, you know, some of your examples
we can't actually do them where I live
because they won't let us.
And that sounds like something that would
severely curtail
the success of community network.
But no, it won't. It's actually a huge asset.
If your local government has enacted ordinances
that ridiculous
then
then that's actually in your benefit.
That's an asset to you.
I know that some people would probably advocate for
people to just go ahead and do it
and see what happens.
Uh... not me.
Because if you want to change society, if you want to change your community, you have
to change the way people think.
If you go out there
and fix the playground
and
you know, the cops show up and arrest you for fixing the playground
yeah, it's going to make news.
It's going to be viral.
But is it going to change the way people think? Probably not.
But if your community network
focused on other things first
focused on things there weren't
prohibitions against
and got a little bit of public support
and then you showed up at that playground
with your sanders and your paint
and said it would take three hours to fix this
we're gonna hang out here for three hours
and held a press conference
what would happen then?
You would probably be able to demonstrate
if you
had somebody who talked to the press
and made those points clearly
and I'm sure there are people that would advise you on
the key words to use if you reached out to them over twitter or whatever.
Um...
you could get a lot of public support this way because
there's not a prohibition
against improving public property which is probably how it's phrased.
There's a prohibition against making your community better.
Your local government has made it illegal to better your community.
That seems a little silly.
And if those were the talking points that went out when you were standing
there with the stuff to fix it and demonstrated that you would have put in
the time
you would get attention.
And you might be able to change that. Now you'll probably get pushback. I'm sure
at some point
there was probably some semi-legitimate reason for this
probably based on some overreaction.
Somebody used lead paint on a playground or something like that.
Um...
Wouldn't it make more sense
rather than
you know getting law enforcement involved, clogging up the criminal justice
system with people who are trying to help their community,
wouldn't it make more sense for the public works
uh... agency to
put out the list of
materials that were approved?
Rather than focusing on
controlling
the community
and stopping it from making itself better,
why doesn't the government facilitate it?
Isn't that really their job?
I mean after all they are our employees, not our rulers.
That's a golden opportunity.
That's a recruitment tool.
That's something that would get
the attention of politicians who would then pay attention to you and might be
more responsive to your little network.
It would get their attention. It would also get the attention of the public.
You want to pick
battles
big enough to matter but small enough to win. So I wouldn't suggest doing this
with the pothole thing.
Because I would imagine
that if there is some prohibition against that in your community
it's probably
has more to do with unions
uh...
and there's a lot of money in that and it's an amount of money you probably
will not be able to overcome
as a community network.
There are limitations to this.
uh...
But at the end of the day
if your local government has made it illegal for you to better your own
community
that's not a talking point against a community network.
That shows why you need one.
Because while one person
may recognize that that's wrong
and may recognize that there are other ways to achieve the same level of safety
which I'm
certain
is the reason they would have this
uh...
there are probably much more effective ways to do it than getting the cops involved.
It would make more sense
to help facilitate improvement
rather than ban it.
And I think that's the route to go.
Again, I wouldn't actually advocate
doing things directly in defiance of local ordinances
because most of the time it doesn't actually sway public opinion and that's
what this is about.
At the end of the day
most of this
is about swaying public opinion
not necessarily
about
directly confronting
the government establishment that exists.
You're creating a parallel one.
You're creating something that people can turn to when the government fails
as it typically does.
You want that secondary
safety net
for your community.
So when things fall through the cracks
slip through the holes
there's somebody there to catch them.
Anyway, it's just a thought.
Y'all have a good day.
Oh, and people said I need to write a book on this.
I'm putting together a workbook.
I'm hoping it'll be out at the beginning of next year.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}