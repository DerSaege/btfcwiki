---
title: Let's talk about things not going the way you planned....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S26-2RXRdOc) |
| Published | 2020/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the historical context of how the current situation unfolded, referencing past events like Bacon's Rebellion.
- Describing the catalyst of the current situation as involving a petty crime, "some pig," and an extrajudicial killing.
- Outlining the progression from petitions and outrage to protests, rioting, and looting, eventually leading to an open rebellion.
- Contrasting the common myth that Bacon's Rebellion was a forerunner to the American Revolution with the reality that it was not about independence from the king.
- Emphasizing that despite the genuine beliefs of those involved, nothing changed after Bacon's Rebellion.
- Warning against romanticizing rebellion and pointing out the potential consequences of a violent uprising in the modern world.
- Cautioning that hostile nations could exploit internal conflict in the US to weaken the country with devastating effects.
- Urging reflection on the mythological versus real-life implications of rebellion and the need for critical thinking about such actions.
- Stating that rebellion does not happen in isolation and could attract external interference, especially in a country with significant military power.
- Encouraging thoughtfulness and consideration of the broader implications before advocating for or engaging in rebellion.

### Quotes

- "A situation that nobody agrees on right now. It's disputed as to what happened. A petty crime, some pig, and an extrajudicial killing. That's how we wound up here."
- "The image of the glorious rebellion is myth. It's mythology. It's not real life."
- "Because this stuff doesn't occur in a vacuum. It's going to just sit idly by while a nuclear power, one of the strongest militaries in the world, rips itself apart."

### Oneliner

Beau delves into historical parallels to caution against romanticizing rebellion and the potential dire consequences of internal conflict in a powerful nation.

### Audience

History enthusiasts, critical thinkers

### On-the-ground actions from transcript

- Challenge romanticized views of rebellion (implied)
- Promote critical thinking before advocating for drastic actions (implied)
- Encourage thoughtful reflection on historical context and potential consequences (implied)

### Whats missing in summary

The full transcript provides a deep dive into historical events to underscore the dangers of romanticizing rebellion and the potential catastrophic outcomes of internal conflict in a powerful nation.

### Tags

#History #Rebellion #Consequences #CriticalThinking


## Transcript
Well howdy there internet people, it's BowieGowan.
So today
we're going to talk about how we got here.
How we got in this giant mess
with everybody
well, where they are.
So we're going to take a look back, we're going to summarize, we're going to recap.
Sometimes it is incredibly useful
to look back and see where you've been.
So you can see where you might be headed
especially if that's not really where you want to go.
You can adjust course if you need to.
So, how did we end up here?
I mean you could say unresponsive government and economic woes
but that's nothing new.
That's been true since colonizers first came to this land.
It's always been like that.
And so
that's not it.
What were the real ingredients? What started it?
A situation that nobody agrees on right now.
It's disputed as to what happened.
A petty crime,
some pig,
and an extrajudicial killing.
That's how we wound up here.
Then what happened?
Petitions,
outrage,
protest.
Protest spun out of control, turned into rioting and looting.
A group of people took land and held it.
Spun out of control. A government building got attacked.
Then it spun out of control further.
A whole bunch of innocents got hurt.
An open rebellion lasted a year.
Yeah, I'm talking about 1676, Bacon's Rebellion.
It really was over a pig.
That was the catalyst anyway.
You know, when people
hear about that in the US, when it's taught in schools, it's generally framed as
the forerunner
to the American Revolution.
That's American mythology.
It is American mythology. It is not American history.
Neither Bacon
nor the governor
had any intention of separating
from the king. In fact, both of them framed their arguments
in what was best for the king.
It was not
the forerunner to the American Revolution.
It's just not really true.
But it was the first
real rebellion.
And
an important thing to remember about it is
while both Bacon
and the governor
had people who truly believed
in their cause, and for the record
what they were arguing about was pretty horrible,
just so you know,
while they both had people who believed in them,
at the end of the day
nothing changed.
Nothing changed.
In fact,
some foreign
advisors
showed up. They brought some stuff with them.
And when all the dust settled
and everything was over with,
neither Bacon nor the governor
was in power.
The advisors
made sure that those who had their hands on the levers of power,
that they were loyal to them.
And as people rush towards
that image,
that mythological image
of rebellion
and what it looks like,
that's something really important to keep in mind.
Because as a whole bunch of people talk about how much they love this country,
and they need to do this because they love this country,
understand that the second it starts,
every hostile nation
on the planet
is going to flood
this country with weapons. They will keep it going
as long as they possibly can to weaken
that country
you say you love.
It will destroy it.
The image
of the glorious rebellion
is myth.
It's mythology.
It's not real life.
We might want to think about that.
A certain group of people who are really pushing for it
really might want to think about that.
Because the odds are
it's not going to go the way you think.
Because
this stuff doesn't occur in a vacuum.
It's going to just sit idly by
while a nuclear power,
one of the strongest militaries in the world,
rips itself apart.
They're going to get their fingers in the game.
It's probably not going to go well for the American people.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}