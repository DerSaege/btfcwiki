---
title: Let's talk about my biggest worry about November 3rd....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=abqyq6cs0eo) |
| Published | 2020/09/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Draws inspiration from Hunter S. Thompson.
- Recalls a specific passage about San Francisco in the 60s.
- Describes the universal sense of winning and inevitable victory.
- Talks about the energy prevailing without the need for a fight.
- Mentions the wave of high momentum they were riding.

- Mentions the peak and the feeling of being alive in that time.
- Talks about the madness in every direction in San Francisco.
- Expresses the importance of keeping the momentum going.
- Stresses the need for political engagement in the United States.
- Emphasizes the importance of not letting the wave break on November 3rd.

- Calls for continued fighting regardless of the election result.
- Talks about building a base of power for systemic change.
- Expresses the desire for promises to be fulfilled for all individuals.
- Warns against returning to "normal" after a Biden win.
- Urges to keep the momentum going and not waste the current opportunities.

### Quotes

- "We have to keep the momentum going."
- "A Biden win cannot be the high watermark of this."
- "The only way it changes is if we don't let that wave break."
- "We want all men created equal, liberty and justice for all."
- "We can't throw away this shot."

### Oneliner

Beau draws inspiration from Hunter S. Thompson, reflecting on the peak of San Francisco in the 60s, urging continuous momentum for systemic change beyond the election.

### Audience

Activists, Voters, Community Members

### On-the-ground actions from transcript

- Keep the momentum going by staying politically engaged (implied).
- Build a base of power for systemic change in your communities (implied).
- Ensure promises are fulfilled for all individuals by taking action locally (implied).

### Whats missing in summary

The full transcript captures Beau's reflection on historical moments, the importance of sustained momentum for systemic change, and the need to avoid regression post-election, calling for continuous engagement and action.

### Tags

#Inspiration #SystemicChange #PoliticalEngagement #CommunityAction #Momentum


## Transcript
Well howdy there internet people, it's Bo again.
So some of you may know, I think I've mentioned it before
in live streams, Hunter S. Thompson is somebody
that I draw a lot of inspiration from.
I don't think that he has anything published
that I haven't read.
For the last six months, maybe a year,
I've had one passage just bouncing around inside my head.
Seems like a lifetime, or at least a main era,
the kind of peak that never comes again.
San Francisco in the middle 60s was a very special time
and place to be a part of.
Maybe it meant something, maybe not in the long run,
but no explanation, no mix of words or music or memories
can touch that sense of knowing that you were there and alive
in that corner of time and the world, whatever it meant.
There was madness in any direction, at any hour.
You could strike sparks anywhere.
There was a fantastic universal sense
that whatever we were doing was right, that we were winning.
Sounds familiar, doesn't it?
And that, I think, was the handle,
that sense of inevitable victory over the forces of old and evil.
Not in any mean or military sense.
We didn't need that.
Our energy would simply prevail.
There was no point in fighting on our side or theirs.
We had the momentum.
We were riding the crest of a high and beautiful wave.
That's pretty motivational, right?
But that's not the end of the passage.
So now, less than five years later,
you can go up on a steep hill in Las Vegas and look west.
And with the right kind of eyes, you
can almost see the high watermark, that place
where the wave finally broke and rolled back.
We have more people politically engaged in the United States
than at any time in recent memory.
We can't let that wave break on November 3rd.
If Biden wins, we keep fighting.
Trump wins, we fight harder.
It doesn't end.
We can't let it roll back.
We have to keep the momentum going.
We have to keep that wave moving forward.
Because the change we're looking for starts with us,
in our backyards.
Us coming up with the systems and structures
to take care of each other.
That deep systemic change we want, it starts with us.
We have to build that base of power that can shape the country.
Because at the end of the day, when all is said and done,
we want the promises made good on, right?
That's really what we want.
We want all men created equal, liberty and justice for all,
the pursuit of happiness.
That's what we want.
That's really what sums up that deep systemic change.
That's the goal, to give everybody that.
What I fear, what I loathe, is the idea that Biden wins
and everything goes back to normal.
Because normal, while right now that sounds great,
normal is the system that gave us Trump.
It already existed.
It was already there for him to abuse.
It was just his ineptitude, his rhetoric,
his lack of compassion, his apathy, his incompetence.
It just showed us a line on it.
That's all that happened.
Yeah, he is the forces of old and evil.
Absolutely.
But he didn't create that system.
It was already there.
And a Biden win doesn't change that.
It still exists.
It's there for the next Trump.
The only way it changes is if we don't let that wave break,
if we keep that momentum going.
A Biden win cannot be the high watermark of this.
We can't throw away this shot.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}