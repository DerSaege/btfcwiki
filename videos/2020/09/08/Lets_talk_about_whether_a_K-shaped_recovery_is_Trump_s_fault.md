---
title: Let's talk about whether a K-shaped recovery is Trump's fault....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IlkWVIt6uXo) |
| Published | 2020/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Economists have been discussing the different shapes of economic recovery: V, W, U, and now K.
- A K-shaped recovery indicates different sectors of the economy performing differently.
- The sectors going up in the K-shaped recovery include tech stocks and real estate, benefiting the wealthy.
- Sectors like service industries and hospitality, employing hourly workers, are heading down in the K-shaped recovery.
- The disconnect arises from the president's positive economic narrative not matching the reality experienced by many.
- This economic trend is likely to worsen income inequality and the wealth gap in the country.
- The top 1% in the US own 50% of the sectors that are improving, further exacerbating economic disparities.
- Relief for those at the bottom may be unlikely as the Trump administration is keen on portraying a full economic recovery.
- The lack of unified response and prolonged closures in certain industries contributed to the economic downturn.
- While not solely caused by Trump, his administration's actions, or lack thereof, exacerbated the situation.
- The American dream may become more elusive as economic exploitation by the wealthy increases.
- The current political landscape may not provide much relief for the working class, urging them to adapt and prepare for challenging times.

### Quotes

- "The American dream will get further and further away because those at the top are going to be exploiting this economy."
- "Relief for those at the bottom may be unlikely as the Trump administration is keen on portraying a full economic recovery."

### Oneliner

Economists debate shapes of economic recovery, with a K-shaped one widening income inequality, as the wealthy benefit while service industries suffer, unlikely to see relief under the current administration.

### Audience

Working-class individuals

### On-the-ground actions from transcript

- Prepare for challenging times by securing your finances and making necessary adjustments (implied)
- Stay informed about economic trends and policies affecting you and your community (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current economic situation in the US, offering insights into the impact on different sectors and income inequality.

### Tags

#EconomicRecovery #IncomeInequality #TrumpAdministration #AmericanDream #WorkingClass


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
the economy and letters and the American dream. Okay, so ever since all this started, since
the very beginning, economists have been talking about the recovery and what it was going to
look like and they kept throwing out letters. It's going to be a V shape, a W shape, a U
shape, and now they're throwing out K shaped. That's bad. If you're watching this, that's
probably bad for you. So this isn't a Biden talking point. Economists have been talking
about this since at least August. This isn't something new that Biden just made up. It's
been around. A V shape is it looks like a V. The chart looks like a V. There's a sharp
decline and then a sharp recovery. That's best case scenario. A W is when there's a
sharp decline, a little bit of a recovery that's sharp, another sharp decline, and then
it goes back up. It looks like a W. A U is when it drops and it stays low for a while
and then it comes back up. A K is when different sectors of the economy perform differently
and that appears to be what's happening. This is why there is a disconnect between the president's
talking points and what everybody's seeing. The president and his buddies are up there
going, the economy's doing great and everybody down here on the bottom is like, what are
you talking about? It's really not. This is why. So the sector of the economy that is
going up, that is making that up part of the K, that's like tech stocks, real estate, stuff
like that, stuff the president and his friends are invested in. I think that's happenstance.
I don't think that there's any, for once, I don't think there's actually any corruption
there. I think that's just luck in this case. So that part of the economy is going up and
that's what they're paying attention to, the overall stock market. And most of the stock
market is going up just due to a few sectors. The down part of the K, the part of the K
that's headed down, that's like service industries, hospitality, stuff that employs hourly workers.
That's why we're down here going, what are you talking about? No, it's not getting better.
It's actually kind of, seems like it's getting worse. So this is the reason for the disconnect.
Now the worrisome part about this is not just that it's going to get a little worse before
it gets better. It's that it's going to increase income inequality in this country and the
wealth gap. The sectors of the economy that are headed up, when you look at the stocks
and all of this stuff, the top 1% of the US, they own 50% of what's headed up. The part
of the economy that's getting better has nothing to do with you and me. It's not really helping
us. So they're making money while everybody else is losing money. That means that they
can further their economic empires. Mom and pop stores will go out of business. Large
retail chains will reap the benefits. Yeah, this is all bad. This is all bad. And we are
unlikely to get any relief because the Trump administration is very set on painting the
economy as in full recovery. If they produce any form of relief to help those on the bottom,
they have to admit that it's not. So that seems unlikely. That doesn't seem like something
that's going to happen. The big question everybody's asking is, is this Trump's fault? I don't
necessarily think it's his fault. He certainly didn't help it though. If you look at the
industries that are falling, they're all industries that had to stay closed for prolonged periods.
They had to stay closed for prolonged periods because there was no unified response. We
couldn't do what other countries did because we didn't have the leadership. There was this
piecemeal response. That is a byproduct of the Trump administration's lack of action.
So he definitely didn't help, but I don't think it's fair to say he caused it. I think
that this to some degree was going to happen anyway. The service industries and those that
are dealing face to face with people, they were going to get hit harder than tech industries,
obviously. So I don't think it's fair to blame him. However, it's worth noting again that
we're not going to get any relief under this president because this president is very invested
in convincing his base that the economy's fine. Don't believe your eyes, don't believe
your ears, believe me and believe this chart that I'm going to show you. I'm doing okay,
therefore you need to pretend you are too. At the end of the day, this is going to set
a lot of people back. They're not going to find that pot of gold at the end of the rainbow.
The American dream will get further and further away because those at the top are going to
be exploiting this economy. Those on the bottom will be bought by those on the top. Is there
a way out of this? Maybe. Are we likely to see it with this Senate and House and President?
Probably not. Probably not because it's not impacting them really. So it's probably time
for those who are working class to get comfortable with the situation at hand and start making
whatever moves you can to be as comfortable as you can as we ride it out. Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}