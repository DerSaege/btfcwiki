---
title: Let's talk about Trump, generals, and defending the MIC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=egU80_HoRkQ) |
| Published | 2020/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- President Donald J. Trump's comment stirred mixed reactions regarding the military and defense contractors.
- The term "military industrial complex" was mentioned, pointing out a longstanding issue.
- The misconception lies in associating the complex with generals and not defense contractors.
- Generals, lacking monetary power, do not drive policy; it's influenced by defense contractors.
- These contractors use profits to sway policy by lobbying Congress and the executive branch.
- Policy decisions are ultimately made by Congress and the President, not generals.
- The system perpetuates itself as voters fail to hold accountable those involved in the complex.
- The real issue is the undue influence of campaign contributions on policy-making.
- Beau challenges people to take responsibility by voting out those perpetuating the complex.
- The root of the problem lies with Congress, the President, and the Secretary of Defense, not the military itself.

### Quotes

- "It's these campaign contributions. It's the undue influence that Eisenhower talked about. That's the problem, not a standing army."
- "The offenders aren't on the E-ring. They're on Capitol Hill."
- "The real issue is the undue influence of campaign contributions on policy-making."
- "The root of the problem lies with Congress, the President, and the Secretary of Defense, not the military itself."
- "Americans are more than happy to buy this fiction."

### Oneliner

President Trump's comments on the military-industrial complex expose a deeper issue: undue influence from defense contractors, urging voters to take action by holding accountable those in Congress and the executive branch.

### Audience

Voters, Activists

### On-the-ground actions from transcript

- Vote out officials perpetuating the military-industrial complex (implied)
- Hold accountable Congress and the President through active participation in elections (implied)

### Whats missing in summary

Beau's passionate call for accountability and action beyond mere awareness.

### Tags

#MilitaryIndustrialComplex #Accountability #PolicyChange #Voting #UndueInfluence


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a misconception and a fiction.
We're going to do this because President Donald J. Trump said something, made a comment.
Half the country was like yes, he's going to do something about this long standing issue,
an issue that has existed since Eisenhower, and the other half of the country was not
thrilled about the comment.
So we're going to clear up the misconception because there's a misunderstanding of the
term that's getting thrown around.
But first we're going to go through what Trump said.
And he said, I'm not saying the military's in love with me, the soldiers are.
The top people in the Pentagon probably aren't because they want to do nothing but fight
wars so that all of those wonderful companies that make the bombs and make the planes and
make everything else stay happy.
Okay, sure that kind of makes sense I guess in some weird fashion.
When he said this people started throwing out the term military industrial complex and
referencing Eisenhower.
That's cool.
I like that because the military industrial complex is a problem.
However that complex is not defense contractors, these companies, these wonderful companies
making deals with generals and generals trying to keep them happy.
That's not what it is.
Because generals don't have any money.
The military industrial complex starts with the defense contractors.
Those wonderful companies.
Starts with them.
They have some profits from previous sales.
They use those profits to influence policy.
Generals don't make policy.
Congress and the executive do.
So they lobby Congress.
They lobby the executive.
They give them money, campaign contributions, to get stuff through appropriations, to get
it through Congress, to get it signed, to get that product, whatever it is they're
pushing, to get it approved so they can make more money which will then again be used to
influence policy.
This goes on long enough and the policy is, well, we've got to use some of this stuff
so we can sell you more.
So then the policy is, well, we need to start a war.
And the generals get handed that directive.
The generals are a tool of policy.
They don't make it.
The president does.
Congress does.
Not the generals.
Yeah, you do have some generals, some military officers who take civilian positions afterward.
That's a fact.
And that's probably not always great.
Sometimes it's good.
Sometimes it's just to get advice on what the average soldier needs.
And that's good.
However, sometimes it's to kind of grease the wheels within the Pentagon.
That's bad.
But they're not taking cash, these generals, they're not taking cash while they're active
duty.
That is Congress.
That is the president.
That's who makes policy.
Not the generals.
This wasn't an attack on the military-industrial complex.
It was a defense of it because it was deflecting from the real problem.
Because Americans are more than happy to buy this fiction.
They want to believe that it's the generals who do this.
The generals and the contractors who do this.
Because then they don't have to accept any responsibility.
The average voter, well they have no control over the generals, but they do have control
over Congress and the executive, right?
So the military-industrial complex, it perpetuates itself because the voters, those people cheering
right now, don't actually care enough to vote these people out when they do it.
Now to be super clear on this, nobody who appoints a secretary of defense who was a
Raytheon lobbyist is against the military-industrial complex.
Nobody who brags about how much money they spend for the military is against the military-industrial
complex.
Trump is the ultimate tool of the military-industrial complex because he convinces everybody that
we have to spend tons of money for defense.
It's like one of his main things.
It's not the generals out there doing that.
It's Trump.
Trump did not attack this form of corruption.
He helped to conceal it.
He helped to bury it in this fiction that the American people have nothing to do with
it, that they have no responsibilities, that they can't change it, but you can.
It's these campaign contributions.
It's the undue influence that Eisenhower talked about.
That's the problem, not a standing army.
That wasn't part of the equation when he wrote it down, when he talked about it.
That wasn't the issue.
He defended that.
He said it was important to have everything ready to go, but the undue influence was the
problem.
That undue influence does not occur with the Joint Chiefs.
It occurs with the SecDef, the President, and Congress.
That's where the problem lies, and that's a problem the American people can fix.
You could vote these people out.
If you're cheering this on right now, get rid of the offenders.
The offenders aren't on the E-ring.
They're on Capitol Hill.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}