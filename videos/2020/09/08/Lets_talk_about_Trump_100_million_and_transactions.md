---
title: Let's talk about Trump, $100 million, and transactions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yBlAL8pP91k) |
| Published | 2020/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is reportedly considering investing a hundred million dollars of his own money into his campaign, sparking various questions and speculations.
- Media suggests that Trump's potential investment shows his campaign's financial struggles compared to Biden's fundraising success.
- Questions arise about the source of Trump's money, the authenticity of his claims, and his true wealth status.
- Despite the focus on financial aspects, the critical point is Trump's transactional nature and lack of guiding principles.
- Trump operates solely based on personal gain, treating everything as a deal with a "what's in it for me" mentality.
- This transactional approach extends to his foreign policy, relationships with governors, and understanding of soldiers.
- Speculations about the motives behind Trump's investment include seeking influence, directing money to his businesses, and buying support.
- The big question isn't the source of the money but what Trump aims to purchase with it – likely the American taxpayer's support.
- Trump's track record shows a lack of policy implementation benefiting the general population, focusing on rewarding friends and elites.
- The core concern is why a leader with no principles or policies seeks access to power, potentially for personal gains at the expense of the public.

### Quotes

- "Everything is about what's in it for Trump."
- "What is he trying to buy? And the answer is pretty clear. You."
- "He's trying to buy you, the American taxpayer."
- "Why does a man with no principles and no policy want access to the Oval Office?"
- "The question isn't where's the money coming from? The question is, what is it buying?"

### Oneliner

Trump's potential campaign investment reveals his transactional nature, aiming to buy support with a hundred million dollars from the American taxpayer.

### Audience
Voters

### On-the-ground actions from transcript
- Question motives behind political investments (suggested)
- Stay informed on political funding sources (exemplified)

### Whats missing in summary
Insights on the implications of transactional politics and the importance of questioning leaders' motives.

### Tags
#Trump #Politics #CampaignFinances #TransactionalPolitics #AmericanTaxpayer


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about
Trump and a hundred million dollars
and motive and transactions.
The media is reporting that Trump is considering
dropping a hundred million dollars of his own money
into his campaign and
they're running wild with this. They're asking all sorts of questions
and they're using this to illustrate all sorts of things. You know, they're pointing
out that this
shows how much of an edge Biden has
when it comes to fundraising. It shows that Trump is scrambling
and that he's losing on that front.
That he doesn't have the allies and support that he once had,
at least those with deep pockets. And yeah, that's worth pointing out.
It's bringing up the question of whether or not he's going to
show us his tax returns, because we have to know where that hundred million
dollars came from, right?
And yeah, that's worth bringing up.
And it's bringing up the question whether or not he even has this kind of
cash
or whether it's just something he's saying in hopes of loosening the pockets
of his supporters.
And yeah, that's worth bringing up, because I think most people have come to the
realization that he is nowhere near as wealthy as he likes to pretend that he
is.
All of this is worth mentioning,
but it's not the big question. It's not the real takeaway from this.
Trump is erratic. He has no real guiding principles except for one,
and he has no policies. He has his emotional reaction that he lets loose on
Twitter.
No policies, no real guiding principles
except for everything is transactional. Everything is a deal.
That's Trump. Everything is about what's
in it for Trump. Everything.
This is why his foreign policy, if you want to call it that,
is an unmitigated failure. It's why
he can't work with governors, because
everything is transactional. It's why he doesn't understand soldiers,
what's in it for them. Everything's transactional.
Everything's about what's in it for Trump.
And there's cute answers to this.
You know, a hundred million dollars. Well, he spends more than that
just on golf, and we pay for it as taxpayers.
Maybe it has to do with trademarks,
something like that, or access to
development in other countries.
All of these things, they're cute answers, but that's not enough.
A hundred million dollars for Trump, that's a lot of money.
He's going to expect a substantial return on that investment,
because everything's a deal. What is he investing in?
What is he trying to buy? And the answer
is pretty clear. You. He's trying to buy you,
the American taxpayer, so he can continue
to siphon money and direct it to his hotels.
So he can continue to reward his friends
and get influence with them. So he can continue to set them up,
so they'll return the favor later, because everything is transactional.
Everything's a deal. That's what we need to be asking.
Why does a man with no principles
and no policy want access to the Oval Office?
Because it's not any altruistic motive.
He's had four years and he's done nothing.
Nothing to benefit the American people. He's fulfilled
none of his promises to the little guy, just to those on top.
If anything happened and it benefited the little guy, it was just a
happy coincidence that came from him returning a favor,
because everything is transactional. The question isn't where's the money coming
from?
The question is, what is it buying?
And I believe it's buying you.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}