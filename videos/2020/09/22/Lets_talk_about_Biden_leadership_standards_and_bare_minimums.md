---
title: Let's talk about Biden, leadership, standards, and bare minimums....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Kl5mhG6SFV4) |
| Published | 2020/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the topic of presidential candidate Joe Biden's leadership, standards, and meeting bare minimums.
- People reached out to Beau with messages about Biden wearing a mask during a speech.
- Biden followed a local ordinance by wearing a mask throughout his speech, setting an example.
- Beau acknowledges the positive attributes of Biden following the ordinance and displaying leadership.
- Despite appreciating Biden's actions, Beau points out that wearing a mask is the bare minimum expected in the current climate.
- Beau argues that praising Biden for meeting the basic standard shouldn't be the norm but rather critiquing those who fall short of meeting standards.
- He suggests that rather than praising those who do the bare minimum, criticism should be directed towards those who don't meet standards to foster real leadership.
- Beau expresses the importance of not allowing Trump's lack of leadership to become the benchmark for judging other candidates.
- Biden's actions are viewed as standard behavior, and Beau stresses the need for real leaders in office.
- Beau, a strong critic of Biden, believes that applauding minimal displays of leadership shouldn't impede progress in the country.

### Quotes

- "You have to set the example."
- "But we can't let that become the standard."
- "Rather than heaping praise on somebody who does the bare minimum, we should criticize those who don't meet standards."
- "If we stopped to clap every time Joe Biden displayed more leadership than the president, we wouldn't get anything done in this country."
- "What Joe Biden is doing is standard."

### Oneliner

Beau addresses the importance of not settling for the bare minimum in leadership and the need to criticize those who fall short of meeting standards to cultivate real leaders.

### Audience

Voters, political observers

### On-the-ground actions from transcript

- Criticize those who fall short of meeting standards (implied)
- Advocate for real leaders in office by holding candidates accountable to higher standards (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on leadership expectations and standards, urging for a shift towards recognizing and fostering real leadership qualities rather than settling for the bare minimum.

### Tags

#JoeBiden #Leadership #Standards #BareMinimum #Criticism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about presidential candidate Joe Biden.
Leadership, standards, and bare minimums.
We're going to do this because I got a whole bunch of people sending me messages today
and I didn't understand them initially.
I had a whole bunch of people sending messages, hey, did you see what Joe Biden was wearing?
What?
I didn't get it.
And then somebody sent the message saying, hey, you know, you really took Trump to task
for not displaying leadership, for not wearing a mask.
Joe Biden wore a mask, are you going to point it out?
Fair enough.
Fair enough.
Joe Biden giving a speech, some address, he wore a mask the entire time.
There was a local ordinance saying that he should, and I'm sure he could have been exempted
from it, but the ordinance existed.
A presidential candidate didn't break the law.
Cool.
I mean, I'm sure that's wonderful for the law and order crowd.
He followed the local ordinance.
He did what was expected of him.
He set the example.
Yes, all of these are positive attributes.
They are.
My entire adult life I have been around people who understand what leadership is and understand
that you have to set the example.
Sometimes you have to be first through the door.
You have to show people what you want them to do.
You can't just tell them.
They have to see you do it.
You have to set the example.
And he did that.
And I am appreciative of that.
However, and this is a big however, that's standard.
That's the standard.
Joe Biden did the bare minimum.
Wearing a mask in public right now as a candidate or a current public official, that is the
bare minimum you can do to encourage people to wear masks.
Yes, is that leadership way beyond anything the Trump administration has shown?
Yes, it is.
Fair enough.
But we can't let that become the standard.
We can't let Trump's failure of an administration become the standard by which we judge other
candidates.
If we want leaders rather than commanders, we can't praise them for the standard, for
the bare minimum.
And that's what this was.
You know, it shouldn't even be something we recognize.
Wow, a candidate for the presidency did the bare minimum.
That wouldn't work in any other field of endeavor.
You don't get praise or accolades because you meet standards.
Yeah, it's better than Trump.
But showing more leadership than Trump isn't a goal.
That's most people's default.
I don't want to get in the habit of praising people for doing the bare minimum.
I don't think we should as a country.
I think that's part of the reason we wound up here.
If we stopped to clap every time Joe Biden displayed more leadership than the president,
we wouldn't get anything done in this country.
And this is coming from somebody who's a pretty strong critic of Joe Biden.
I mean, it's when it comes to leadership, Trump displays none.
So even the slightest display of leadership from Biden is going to appear like a huge
difference.
But it's not.
This is the standard.
What Joe Biden is doing is standard.
He meets standards as a candidate.
Cool.
Rather than heaping praise on somebody who does the bare minimum, we should criticize
those who don't meet standards.
That's how we can hopefully get some real leaders in office eventually.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}