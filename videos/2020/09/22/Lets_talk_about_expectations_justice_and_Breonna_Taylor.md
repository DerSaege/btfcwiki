---
title: Let's talk about expectations, justice, and Breonna Taylor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KKq_ZEstxw0) |
| Published | 2020/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tense atmosphere as preparations are made in a city for expected events.
- Plywood being put up as businesses prepare for potential unrest.
- Anticipation of an extreme injustice leading to citizen reactions.
- The story is about the expectation of injustice, not just physical preparations.
- Doubt on the outcome of the LeBron and Taylor case in Louisville.
- Indictment of the criminal justice system with the anticipation of injustice.
- Lack of accountability and frequent incidents stressing governance consent.
- Preparations by both businesses and law enforcement are seen as an indictment.
- The expectation of justice for all is waning, replaced by anticipation of injustice.
- Concern over how long a country can endure such expectations without reforms.

### Quotes

- "The story is that the expectation is to see an injustice."
- "Liberty and justice for all, right? Guess not."
- "The expectation is that something else will happen."
- "When the justice system in a country comes into question, either reforms get made or the country fails."

### Oneliner

In a city preparing for expected events, the story is about the anticipation of extreme injustice, stressing governance consent and the need for reforms in the justice system.

### Audience

Activists, Reformers, Concerned Citizens

### On-the-ground actions from transcript

- Prepare for potential unrest by securing businesses and community spaces (exemplified)
- Advocate for accountability and reforms in the criminal justice system (exemplified)
- Stay informed and engaged in addressing systemic issues (implied)

### Whats missing in summary

The full transcript provides a deeper reflection on the societal expectations of injustice and the urgent need for reforms in the justice system.


## Transcript
Well howdy there internet people, it's Beau again.
Kind of a tense day, isn't it?
Everybody's watching
on TV, on the internet.
All eyes are
looking to one city.
And we're all watching as they make preparations.
Plywood going up.
Everybody's getting ready
for what is expected to occur.
See, we don't actually know the outcome
of the LeBron and Taylor case right now.
No clue what's going to happen in Louisville.
But, one indictment has already been handed down.
The people in that city
are expecting
to see an injustice.
They're expecting to see an injustice so extreme
that it provokes a reaction
from the citizens.
That's the story.
That is the story.
It's not that people are putting up plywood and it looks like they're getting ready for a hurricane.
It's that businesses
feel it's necessary.
And you may think it's necessary.
Well, they have to protect themselves. Sure.
But that's not the story.
The story is that the expectation
is to see an injustice.
The story is that
what people think is going to happen
is an injustice so extreme
that people take to the streets.
That's the expectation.
And even if you think,
well, it might happen,
it's likely to happen, any of these things,
that's an indictment.
That is an indictment of the entire criminal justice system.
When incidents like this occur, when things like this happen,
it stresses the very idea that the United States has consent of the governed.
Because they may not.
And this is happening on a very frequent basis because we don't have
any accountability,
any real accountability.
We investigated ourselves and found we did nothing wrong.
Those pieces of plywood up over those windows,
the barricades put out by law enforcement in advance.
Yeah, it's preparation.
And it might be necessary. We don't know yet.
But
that preparation is an indictment.
Not just
do people think it might happen.
They're so convinced that it's going to,
they're getting ready for the inevitable outcome.
Liberty and justice for all, right?
Guess not.
We don't even expect it anymore.
The expectation
is that something else will happen.
The expectation
is that
the government will visit an injustice upon the people so severe
that it prompts a reaction.
I don't know how long
a country can suffer that,
to be honest.
When the justice system in a country comes into question as this one has,
either reforms get made
or the country fails.
We don't know
what's going to happen in that case.
But there's already been an indictment.
Anyway,
it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}