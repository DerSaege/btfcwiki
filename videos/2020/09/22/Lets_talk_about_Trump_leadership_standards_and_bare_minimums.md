---
title: Let's talk about Trump, leadership, standards, and bare minimums....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JqnSJ37mcrg) |
| Published | 2020/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes praising Biden for fulfilling basic civic duties as setting the bar too low.
- Warns against using Trump as the only standard for presidential candidates.
- Stresses the importance of addressing systemic issues and not just superficial fixes.
- Argues that the impact of Trump's presidency will be long-lasting regardless of the election outcome.
- References Sun Tzu's characteristics of a good leader and contrasts them with Trump's qualities.
- Emphasizes the need for competence, understanding of governance, and respect for others in a leader.
- Points out Biden's willingness to listen to experts and take responsibility, contrasting with Trump's behavior.
- Advocates for striving for higher standards in selecting public officials and government accountability.

### Quotes

- "If we set the bar as better than Trump, we could end up with a candidate whose only qualifications are that they don't lie constantly, they don't engage in nepotism, don't engage in overt corruption..."
- "Whether you think Biden has it or not, that's a decision you have to make."
- "That's what I had to do with."
- "We should set the standards pretty high."

### Oneliner

Beau criticizes setting the bar too low by using Trump as the sole standard for presidential candidates and advocates for higher standards in selecting public officials.

### Audience

Voters, citizens

### On-the-ground actions from transcript

- Advocate for higher standards in selecting public officials (implied)
- Strive for deep change in how candidates are evaluated and selected (implied)
- Hold public officials accountable for competence and integrity (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of leadership standards, contrasting Trump and Biden's qualities, and advocating for higher expectations in selecting public officials.

### Tags

#Leadership #PresidentialCandidates #Trump #Biden #Standards


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Trump, leadership, standards, and bare minimums.
We're going to do this because last night I released a video talking about Biden
because there were a lot of people who want to do praise Biden for doing
something that was basically his civic duty.
I'm not sure that's the route to go.
Yeah, cool.
He did something Trump didn't do and yeah, I get it, however, it's just
what's expected of everybody.
If we set the bar as better than Trump, we could end up with a candidate
whose only qualifications are that they don't lie constantly, they don't
engage in nepotism, don't engage in overt corruption, they're not constantly sexist
and xenophobic, and they're not a national embarrassment pretty much every day.
Yeah I mean those are things we want, but is it enough to be president?
The most powerful office in the United States and therefore one of the most powerful offices
in the world is better than Trump, the answer, or are we just doing what we get mad at politicians
for doing, slapping a Band-Aid on it, not really fixing the issue, not addressing the
systemic underlying problems, just putting a happy face on it.
you're talking about one of the most powerful officers in the world, I would
suggest that we need to go beyond not Trump.
And I understand this, this election is just, it is what it is, a lot of
the choices have already been made.
However, there's going to be another and Trump's impact on the
United States is going to be long lasting.
Whether or not he wins this election or not, his impact will be long
lasting. There will be negative impacts from this presidency for decades. One of
them will be how we view politicians if we're not careful. Sun Tzu, a work
attributed to Sun Tzu, because I know there's a lot of history buffs watching
this channel. I'll just say a work attributed to Sun Tzu. The Art of War, it laid out a few
characteristics that you needed in a good leader. Set the bar pretty high. They needed to be wise
and strategic. They needed to have trustworthiness or integrity. Benevolence is just love. Courage
courage, and they needed to be disciplined.
Yet none of that is President Trump, none of it.
You know, in this office, when you're talking about the presidency, wise and strategic,
competence, competence, it's really what it boils down to here.
So we would need somebody who had a basic understanding of the Constitution, a basic
understanding of foreign and domestic affairs, understanding how the separation of powers
works, understanding what's the responsibility of the state and what's the responsibility
of the feds, stuff like that.
Not something that Trump has.
Whether you think Biden has it or not, that's a decision you have to make.
Yeah, okay.
We don't even have to go through Trump on this one.
Benevolence.
This means you're treating people with respect.
When you put it in context of the art of war, it's winning hearts and minds.
The ability to turn your opposition to your friend.
That's what I had to do with.
It also ties into another quote by somebody else that often actually gets said is Sun Tzu, but it's not.
And that's that you should listen to your subordinates because gold lies underground.
You don't have such an ego that you refuse to listen to people who know more than you simply because they have a lower
station.
This, I will answer for you, Biden does this, he does, he listens to people who know more
than him, he's established that.
Courage, valor, Trump can't even accept responsibility for his own tweets, can't
accept responsibility for the things he says.
He certainly can't accept responsibility for a country, we need somebody who is willing
to take on that mantle, who is willing to make the decisions, who is willing to take
all of the information and do what is best for everybody in the United States, not just
red states or blue states, showing benevolence.
And then they need to be disciplined.
And this means a lot of different things.
It doesn't just mean disciplining your forces, your staff, in the case of the presidency.
It means being self-disciplined, you know, not sitting around eating hamburgers and watching
Fox all day.
Actually doing the job you're tasked with.
Whether or not Biden is these things, I don't know.
No clue.
I know Trump's not.
He's not any of them.
But again, not Trump.
That's not a goal.
That's not something to strive for.
Again, I know there's a lot of people watching this channel.
This is the hand we're dealt.
We have to go with one of these two.
Yeah, OK, I get it.
I get where you're coming from on that.
But that doesn't mean that we can't talk about the ideal,
what we're actually looking for.
Because if we don't do that, we don't
strive for that deep change in the way that we look at things,
in the way we do things, especially in selecting those who end up as public officials.
We can't expect public officials to.
This is a government of the people, at least in theory.
We got to do our part.
We should set the standards pretty high.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}