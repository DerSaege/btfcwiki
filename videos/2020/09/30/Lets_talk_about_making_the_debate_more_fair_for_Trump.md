---
title: Let's talk about making the debate more fair for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IwiB8jPUQAs) |
| Published | 2020/09/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Proposes restructuring the debates due to recent candidate performance.
- Outlines a format where candidates start off in green and move to yellow or red if they break rules.
- Suggests a system where candidates answer ten questions without breaking rules to receive a special surprise.
- Emphasizes the importance of candidates following basic rules of civil discourse.
- Argues that candidates unable to follow debate rules should be disqualified.
- States that the President of the United States is not fit for office and was never qualified.
- Criticizes the current President's inability to handle debates and real situations.
- Believes the debates should not be restructured but rather the candidate should be changed.

### Quotes
- "No, do not restructure these debates. Let him continue to embarrass himself."
- "Change the candidate."
- "He never should have been in that office to begin with."

### Oneliner
Beau proposes a debate restructuring system where candidates follow rules or face consequences, advocating for disqualifying candidates who cannot adhere to basic civil discourse.

### Audience
Debate organizers

### On-the-ground actions from transcript
- Implement a structured format for debates with consequences for rule-breaking (suggested)
- Advocate for disqualifying candidates who cannot follow basic rules of civil discourse (implied)

### Whats missing in summary
The full transcript provides a detailed breakdown of how debates could be restructured to ensure candidates follow basic rules of civil discourse and consequences for those who do not.

### Tags
#Debates #CandidatePerformance #CivilDiscourse #Disqualification #PresidentialElection


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about restructuring the debates.
We're going to do this because it's a legitimate topic right now.
Because of the performance of one of the candidates last night, there is a concerted effort to
come up with a different format to ensure that that doesn't happen again.
I have taken the liberty of speaking to somebody who is used to dealing with people who behave
in this manner.
And they have given me kind of a format, an outline to use for future debates.
Okay, so each candidate will start off in green.
And all they have to do to stay in green is tell the truth, not interrupt their partner,
and stick to their two minutes.
If they break any of these rules, they go to yellow.
If they break it again, they go to red.
And they have to go to timeout.
And during that time, their partner is going to have two minutes to speak without them
around and they can't respond to it.
That's not good.
We don't want that.
Aside from that, each candidate is going to get a little card with their name written
on it.
And it has ten.
Ten little spots because there's going to be ten questions.
And if they can go through all ten questions, they'll get a hole punch each time.
And if they can go through all ten questions without breaking the rules, they'll get a
special surprise.
It's going to be great.
Everybody will make out.
Because that's what we're here to do.
We're here to learn and facilitate education.
Right?
These men are interviewing for the most powerful office in the United States.
The person who wins the election will be in charge of the most powerful military in the
world.
I would suggest that if we cannot expect them to abide by the basic rules of civil discussion,
they should be disqualified.
We do not need to change the rules because one of the candidates can't follow them.
I would strongly suggest that if a candidate can't not interrupt their opponent, can't
stick to the time provided, can't refrain from lying for 90 minutes, that they probably
can't follow federal law, can't follow the Constitution.
Those things are a little bit more complicated than stuff you knew in kindergarten.
No don't change the debates.
Change the candidate.
The President of the United States is not fit for the Oval Office.
He never was.
He was a sideshow that occurred during the Republican primary and a whole lot of low
information voters were like, oh he's a billionaire.
That means he's successful.
He'll be a good leader.
Those things have nothing to do with each other.
He was born rich.
He didn't achieve anything to be pushed into the situation he was in.
And now because of the sunk cost fallacy, people refuse to stop supporting him.
He can't handle a debate against Joe Biden.
He's not going to be able to handle any real situation as if that wasn't proven over the
last four years.
No, do not restructure these debates.
Let him continue to embarrass himself.
He never should have been in that office to begin with.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}