---
title: Let's talk about Labor Day, definitions, reactions, and political belief....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WW7gZZ938eY) |
| Published | 2020/09/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges Labor Day and urges viewers to put in labor for the day.
- Addresses the narrow scope of acceptable political beliefs in the United States.
- Points out the emotional reactions and lack of understanding towards socialism, communism, and fascism among Americans.
- Explains how fear of authoritarianism is the common thread that opposes these isms.
- Talks about how propaganda during the Cold War shaped American views on political beliefs.
- Suggests that most viewers of his channel can define political terms when stripped of emotional connotations.
- Mentions how people in the southern United States tend to lean towards socialism when ideologies are presented without labels.
- Encourages viewers to take a political compass quiz and answer questions based on ideal society rather than the current broken system.
- Emphasizes the importance of understanding one's true ideology beyond the narrow scope of acceptable beliefs in the US.
- Connects the exploration of political beliefs on Labor Day to celebrating American organized labor and their vision for a better world.

### Quotes

- "Your ideology shouldn't be based on the best you can do in a system that most people agree is broke."
- "Labor Day is the celebration of American organized labor. People who had a vision of a better world and worked to get it."

### Oneliner

Beau addresses the narrow scope of political beliefs in the US, encourages viewers to understand their true ideology beyond fear-driven reactions, and connects exploring political beliefs on Labor Day to celebrating organized labor's vision for a better world.

### Audience

Viewers

### On-the-ground actions from transcript

- Take a political compass quiz answering questions based on an ideal society, not the status quo (suggested).
- Understand and define political terms beyond emotional reactions (implied).

### Whats missing in summary

The full transcript provides a deep dive into political ideologies and encourages viewers to critically analyze their beliefs beyond the narrow scope imposed by society.


## Transcript
Well howdy there internet people, it's Beau again.
Happy Labor Day.
And in honor of Labor Day,
today's going to be a little bit different. I'm going to ask you to put in some labor.
Today
we are going to talk about
definitions,
emotional reactions, and political belief.
Because in the United States we have a very narrow scope
of acceptable political belief.
And when you step outside that man, people get worried real quick.
There are a lot of isms
that people in the United States
have a
very emotional reaction to.
And I say that it's emotional
because they can't define the terms.
In the U.S. there are three isms
that create worry among most people.
Those isms are
socialism,
communism,
and fascism.
The thing is most Americans can't
define those. They'll tell you that they're opposed to them.
But they don't know what they are.
And if you ask why they're opposed to them,
you get the same answer for all three.
They're opposed to authoritarianism.
That's the real fear.
The funny part is that only one of those three actually requires
authoritarianism.
The not so funny part is
that if you allow your fear to define you
rather than attempting to define your fears,
any system
will lead to authoritarianism because those in power will use that fear
to get you to wave your rights
while you wave your flag.
And authoritarianism exists and you'll cheer it on
as it arrives.
Most people in the U.S.
have that reaction
because of the Cold War.
During the Cold War
we had to unite
in the U.S.
so a lot of propaganda went out
and it was designed
to narrow the scope of acceptable political belief
because we had to defeat the evil empire
so we had to come together, right?
And that's why.
And that's also why a lot of people associate socialism and communism with
authoritarianism.
But those aren't necessarily
part of it.
They can exist
depending on
how those societies are structured
but they're not required.
In the U.S.
something that's funny
because most people who watch this channel, you can define these terms.
If you ever sit down and describe
the individual societies,
the way they're supposed to work
and I'm talking about all of them, not just those three,
and allow people to pick
but when you describe them you don't use
any ism.
You don't use any word
that elicits an emotional reaction
and you don't
give it away as to which one is which.
Most people, at least in the southern United States,
most people I've done this to, they've chosen socialism
which to me is funny.
If you're new to the channel, I'm not a socialist. That's not me pushing my ideology.
Just statement of fact.
And it makes sense if you really think about it. In the southern United States
there's a
a common religious belief
and if that bleeds through to people's politics, yeah, socialism is where you'd end up.
That makes sense.
I would
wager
that most of those people would actually side with social democracy but I didn't
get that far into it when I was doing this.
At some point I'll go back through and do it again.
But that's one of those things that's
worth exploring.
A whole bunch of people have no idea
what their real ideology is, what they really believe.
They know
the best they can do
within this narrow scope of acceptable political belief in the US.
So maybe today,
on Labor Day, it's a good day to do it,
take a political compass quiz
but don't answer the questions
with an eye towards getting the results you want.
And don't answer the questions based on the status quo.
Don't answer them in the sense of
oh, this is the best we can do in the world we currently live in.
Answer them as if you were creating a world,
an ideal society.
And see where you end up.
Because your
ideology shouldn't be based on
the best you can do in a system that most people agree is broke.
That's
not really a goal.
That's surrender.
I think today's a good day to do it because
Labor Day
is the celebration of American organized labor.
People who had a vision
of a better world and worked to get it.
So,
it seems fitting.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}