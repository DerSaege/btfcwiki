---
title: Let's talk about when progressives will be satisfied....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WEHM5Cegmko) |
| Published | 2020/09/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about when people will be satisfied, when it'll be enough, and when it will stop.
- Exploring when those seeking change, progress, and justice will be satisfied.
- There are no new conservative ideas, but there may be new ways to package them.
- Conservatives typically defend old progressive ideas.
- The Republican Party, known as the party of Lincoln, was actually progressive.
- Lincoln received fan mail from Karl Marx, showing a connection between progressive ideas and conservatism.
- New ideas tend to come from the progressive side, while conservatives try to hold things back.
- When will people be satisfied? It doesn't end; there will always be new injustices.
- People asking when others will be satisfied are often wondering what the minimum acceptable change is.
- Beau suggests doing what you can, when you can, where you can, for as long as you can to make a difference.

### Quotes

- "When will those seeking change, progress, and justice be satisfied?"
- "There are no new conservative ideas."
- "New ideas come from the progressive side."
- "It doesn't end. Ever."
- "Do what you can, when you can, where you can, for as long as you can."

### Oneliner

Beau asks when those seeking change will be satisfied, explains the absence of new conservative ideas, and advocates for continuous efforts towards progress and justice.

### Audience

Activists, changemakers, advocates

### On-the-ground actions from transcript

- Do what you can, when you can, where you can, for as long as you can (suggested)

### Whats missing in summary

The full transcript provides a deeper understanding of the relationship between conservative and progressive ideas and encourages continuous efforts towards change and justice.

### Tags

#Change #Progress #Justice #ConservativeIdeas #ProgressiveIdeas


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about
when people will be satisfied,
when it'll be enough,
when it will stop.
We're going to do this because somebody asked me.
Somebody brought it up.
And the context
was really
when will those who are seeking change,
those looking for progress,
those searching for justice,
when will they be satisfied?
When will it be enough?
And it gets to the heart
of something I said recently.
There are no new conservative ideas
because this question, there's really two questions there.
But the idea
really boils down to understanding that fact.
There are no new conservative ideas.
There may be new ways to package
a conservative idea.
Defend it, market it, brand it.
But there aren't new conservative ideas by definition.
It's not really a thing.
It's not in any major way.
Holding to traditional attitudes and values
and cautious about change or innovation,
typically in relation to politics or religion.
By definition,
new conservative ideas,
not a thing, not really.
Now that isn't to say that a person who is generally conservative
can't come up with a new idea.
But that new idea is progressive.
Just coming from somebody who is generally conservative.
Which makes sense because
conservatives typically just defend
old progressive ideas.
And I know that doesn't make sense.
Women voting.
I would assume that most conservatives of today
would say yes, women should be able to vote.
They would defend that idea.
What about a hundred years ago?
A hundred and fifty years ago?
Probably not.
They wouldn't defend it because they'd be trying to stick to the status quo,
which is what conservatives do.
To take this further, because it does seem odd
to suggest that conservatives really
just hold on to old progressive ideas,
the conservative party in the United States is the Republican Party.
Known as the party of Lincoln.
Right?
Lincoln was a progressive.
Lincoln was a progressive.
And I know somebody's going to say he's a Republican,
he's a conservative.
I've got a video on the Southern Strategy.
I'll put it down there.
I don't think anybody would argue though that ending slavery
was maintaining the status quo.
It was certainly change, innovation.
It was progressive.
You know, when Lincoln got re-elected,
he got a letter, an address,
from the International Working Men's Association.
And this little address, it really covered four main points.
The first, congratulations on your continued opposition to slavery.
This is good.
The second was that the working class of Europe were behind him.
The third was that the contest going on in the United States
was really a global battle between property, capital, and labor.
And that the working men of Europe were behind him.
And the fourth was that it was a historically progressive event
and that it would usher in the era of the working class.
This little letter was drafted by a guy in London.
He was in London because he was kind of like exiled there,
in Santa Nangrata, because he wrote this little pamphlet
about this new economic theory called the Communist Manifesto.
The spiritual ancestor of the Republican Party,
the party of Lincoln.
Lincoln was getting fan mail drafted by Karl Marx.
I'm willing to bet they don't talk about that at their meetings.
This isn't to say that all new ideas are good.
Or that all old ideas are bad.
It's just saying that the new ideas come from the progressive side.
And the conservative side of things tries to hold things back.
Maybe on some level that's good.
Maybe the conservative side, that anchor on society and the world at large,
maybe at times it stops the progressives from chasing a new bad idea too far.
So maybe there's a benefit to it.
But at the end of the day, when you're really talking about it on a global scale,
when you're looking at it through a historical scope,
when are people satisfied? When does it end?
It doesn't. Ever.
It will continue to go on.
Even once all the problems we have today are solved,
there will be other injustices.
It doesn't end.
But see, when people ask this, when will they be satisfied,
they're not really asking what would satisfy people.
What they really want to know is, when is it enough for me personally?
What can I do?
And the answer to that, my answer, because it's not an unanswerable question,
I would suggest that it's you do what you can, when you can, where you can,
for as long as you can.
You play out your part in changing the world,
in helping it progress, in seeking justice.
But most people who ask this, they're not really asking what they can do.
They're asking what the minimum those people will accept.
What's the minimum that we can give up and allow us to return to the status quo?
That's really the question.
I would suggest that just attempting to do the minimum in regards to seeking justice
is probably not the best way to go about it.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}