---
title: Let's talk about Trump, the Constitution, and the machinery for change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d7Yh722gLZs) |
| Published | 2020/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States Constitution acknowledges imperfection and the need for change from the very first line of the document.
- The Constitution enshrines mechanisms for change, including the right to petition for a redress of grievances and freedoms like speech, press, and assembly.
- Elected officials are not as responsive as they should be, regardless of party affiliation.
- The Republican Party is noted for attacking the machinery for change and methods of voicing concerns, such as suppressing the vote and criminalizing assembly and speech.
- Republicans tend to ignore issues like climate, public health, the economy, and racism, pretending they don't exist when it suits them.
- Republicans demonstrate a lack of interest in listening to the people by attempting to suppress the vote and undermine the integrity of elections.
- Beau criticizes the Republican Party for abandoning their duty to represent the people and instead seeking to rule over them.
- He questions why a party claiming to be for the working class and looking out for the little guy wouldn't want more people to vote.
- Beau argues that by suppressing the vote, Republicans show they are not interested in having the consent of the governed and are focused on ruling rather than representing.
- The duty of the government is to listen to the people, but Beau suggests the Republican Party has strayed from this principle.

### Quotes

- "You are always doing it wrong. There's never a right way to do it."
- "They attack that machinery so they don't have to listen to the people."
- "We're not going to listen to you. We're not going to give you a voice in your representative democracy."
- "Your rights don't matter."
- "Rather than protecting the machinery for change and encouraging its use, they're holding a lighter to the Constitution."

### Oneliner

The United States Constitution acknowledges imperfection and enshrines mechanisms for change, but Beau criticizes the Republican Party for attacking the machinery for change and undermining the people's voice.

### Audience

Voters, activists, concerned citizens

### On-the-ground actions from transcript

- Join organizations advocating for voting rights and fair elections (implied)
- Contact elected officials to express support for protecting mechanisms for change (implied)

### Whats missing in summary

Beau's passionate delivery and detailed analysis on Republican actions and their implications. 

### Tags

#Constitution #RepublicanParty #VotingRights #Government #Representation


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we're going to talk about the government generally,
Republicans specifically, and the Constitution.
The United States Constitution is a very unique document in a whole lot of ways,
not the least of which is a contradiction that exists.
It has all of these lofty goals, these just amazing ideas,
but it's also very self-aware. In the preamble, we the people of the United
States, in order to form a more perfect union, establish justice, ensure domestic
tranquility, provide for the common defense, promote the general welfare,
secure the blessings of liberty to ourselves and our posterity, do ordain
and establish this Constitution for the United States of America. First line, form
a more perfect union. More perfect, not perfect, better, not best. In the very first line of the
document, they acknowledge it's not right, that it's not perfect, that it's going to need to be
changed. So they enshrined the machinery for change. They created mechanisms and protected them
them, because the whole idea here is to have a responsive government, one of we the people.
So they enshrined the right to petition for a redress of grievances.
They enshrined freedom of speech, freedom of press, freedom of assembly.
They created a document that built a representative democracy, because the idea is that our elected
officials represent us.
They don't rule us.
They have to listen, and in order to achieve this, you need the machinery for people to
have their voice heard.
Now the U.S. government is less than responsive, and that's across the board.
Republicans, Democrats, independents, third parties, doesn't matter.
When you're talking about elected officials at the federal level, they are not very responsive.
You have some outliers.
you have some people who truly listen to their constituents,
but there's not many.
On top of this, you have a giant bureaucracy
that isn't responsive at all.
So that part is government-wide.
However, it is the Republican Party
that constantly seeks to attack the machinery for change,
the method in which the average person can
have their voice heard.
This is why they suppress the vote.
This is why they seek to criminalize assembly, speech.
It's why they attack the media.
Fake news.
This is a method outlined by the founders
so the government can hear the opinions of the people.
When it comes to petitioning for a redress of grievances,
when it comes to the Republican Party,
you are always doing it wrong.
There's never a right way to do it.
Take a knee during a song, nope, that's wrong,
not gonna listen to you.
Stage an assembly, nope, that's wrong,
not gonna listen to you.
Write something on social media, nope, that's wrong,
not gonna listen to you.
They're undermining the machinery for change
because they think they know better,
because they have switched from representative to ruler.
They attack that machinery so they don't have to listen to the people.
And as they do that, they attack the very foundation of the United States, the Constitution.
They attack that machinery for change.
On the off chance that the people do actually get the attention of those in DC, what does
the Republican Party tend to do?
be climate, public health, the economy, racism, it doesn't matter.
They just pretend like the issue doesn't exist.
Fake news.
Or they ignore it.
Because they don't listen to we the people.
And that's incredibly clear when they attempt to suppress the vote.
They know they don't.
When they attempt to stop people from voting by whatever means, it's really saying the
same thing.
We're not going to listen to you.
We're not going to give you a voice in your representative democracy.
You don't get to have a voice.
We know better.
The Constitution doesn't matter.
Your rights don't matter.
This machinery for change, well, it's just wrong.
We know what's best for you little people.
You better listen to your betters.
The problem is it's the government's duty to listen.
That's their job.
representatives, not rulers. If you were in elected office and you were the sort
that the Republican Party paints themselves to be, the people who are
always looking out for the little guy, friend of the working class and all of
that, that's a popular thing right there. Why would you not want a referendum? Why
would you not want more people to vote? If the image, if the propaganda, the
Republican Party pushes is true, they should want everybody to vote. They
should want mandatory voting because it would show that they have consent of
the governed. When they attempt to suppress the vote and undermine the
integrity of the election, they show very clearly that they know they don't, that
their goal is to rule, not represent, that they have abandoned their duty and
abandon the Constitution. Rather than protecting the machinery for change and encouraging its use,
which is their job, they're holding a lighter to the Constitution and attempting to undermine
at every opportunity. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}