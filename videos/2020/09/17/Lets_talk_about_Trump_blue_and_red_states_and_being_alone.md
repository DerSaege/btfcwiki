---
title: Let's talk about Trump, blue and red states, and being alone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AElcnUgPkeQ) |
| Published | 2020/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump made a statement about red and blue states, aiming to deflect blame for mishandling public health issues.
- Trump's comment suggested that without the blue states, the toll of the pandemic in the U.S. might be lower, but this is misleading.
- Initially, 53% of COVID-19 deaths were in blue states, but red states caught up and by July, 70% of deaths were in red states.
- The idea that blue states are somehow worse is not true; they were hit first due to being more populous.
- Trump's handling of the pandemic has been criticized, and even without blue states, the U.S. death toll is significant.
- The division between red and blue states is not as clear-cut as it seems, with many states having diverse political leanings within.
- Trump's tactics of dividing Americans and fueling fear and hate to boost his re-election chances are condemned.
- Despite political affiliations, the average American does not harbor hate towards each other; politicians are blamed for promoting division.
- People in conservative areas are reminded that there are like-minded individuals around, and speaking up can provide support to others feeling isolated.

### Quotes

- "He has turned the country against itself."
- "This country isn't that divided."
- "Politicians do everything they can to make us put party over country."

### Oneliner

President Trump's divisive tactics and misleading statements about red and blue states aim to shift blame, but unity and shared values among Americans prevail.

### Audience

Americans, Community Members

### On-the-ground actions from transcript

- Reach out and connect with like-minded individuals in your community (implied)
- Speak up to show support and let others know they are not alone (implied)

### Whats missing in summary

The emotional impact of Trump's divisive tactics and the importance of unity in the face of political polarization.

### Tags

#Trump #RedStates #BlueStates #PoliticalDivision #Unity #CommunityNetworking


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about President Trump's statement
and red and blue states, what that really means.
Because it's not quite as defined as people might believe it is.
And Trump's little comment, while absurd,
there's a whole lot that we can learn from it.
So what did he say?
He was trying to deflect blame for the mishandling
of the public health issue.
And he said, if you take the blue states out,
we're at a level that I don't think anybody in the world
would be at.
We're really at a very low level.
But some states, they were blue states, and blue state managed.
He's talking about the toll.
If you subtract the blue states, the toll would be lower.
Yes, that's true.
If you subtract the most populous areas of the country,
you will have a lower toll.
Give that man a Nobel Prize for mathematics.
But the thing is, the idea that he was trying to get across
is that blue states are somehow worse.
Is that true?
No.
No, it's not.
If you go back to the very beginning,
it's 53% of the lost are from blue states.
47% are from red states.
If you just go from July, 70% are in red states.
See, the blue states got hit first.
That's where it started.
So of course, their tolls are higher.
They're also generally more populous.
So the fact that even with a head start and having more
people, red states have done their best to catch up,
kind of shows that the opposite is true.
And his comment about saying that he
didn't think anybody else would be there
if we took away the blue states, which
is about 100,000 people.
We take that away.
Nobody else would be there.
That's true, but not the way he's saying.
If he subtracted that, we would still be the number two
and lost.
Only Brazil would reach that level.
That's how bad he has handled this.
OK.
But here's the thing.
Red states and blue states, that's not a thing.
Not really.
Not the way it comes across.
If you mean red state as a state that typically votes red,
or blue, a state that typically votes blue on the national
level, sure, yeah, I guess that's a thing.
But that doesn't really tell you much.
Lately, Portland, it's been in the news a lot.
Democrat state, blue state, 50.02%.
That's not an overwhelming majority at all, now is it?
New York, another blue state, 59%.
California, 61%.
That means 39% of people in California
are more conservative, or voted third party, I guess.
Not even 2 thirds voted blue.
Same thing the other way.
Texas, red state, 52%.
The highest in the country is West Virginia, with 68 voting
red.
We're not as divided by states as the president
would like us to believe.
But that's what he has to do.
You know, he has to divide the country.
He has to go out of his way to pit Americans
against each other, because that's
what all authoritarian goons do.
They start off with a little bit of a push.
They start off by going after those outside the country.
Foreigners, maybe they say something like, America first,
and build from there.
Once that isn't satisfying their crowd anymore,
have to give them somebody else to look down at,
well, then they go after those who
are in the country who shouldn't really be there.
In this case, immigration was used.
Then it's those who are a little different somehow.
Somehow.
And then, as a last resort, the leader
turns the country against itself.
Anybody who opposes dear leader, well, they're the opposition.
You have to hate them.
And they have to do this because reactionary people,
they live on a diet of fear and hate.
So you have to give them somebody to be afraid of
and somebody to hate.
And that's what he's done.
He has turned the country against itself.
He is asking Americans to forget about 100,000 lost,
simply because they resided in a state where a little more
than half voted against him.
Can you picture any other president in US history
doing that, anybody else being that arrogant, that ignorant,
that self-serving, dividing the country like that,
to hopefully boost his re-election chances?
It's the only reason he's doing it.
People should be forgotten about.
Them being lost, well, it shouldn't matter.
Because they, well, maybe not even them,
but some people in their state voted for the party
that President Trump himself gave a whole bunch of money
to over the years and used to be a member of.
Because he has no principles.
He has no morals.
He never has.
But see, there's some good news that comes out of this.
In my county, where I'm at, oh, this is red.
This is deep, deep Republican, deep Republican county.
If 100% of registered Democrats showed up, 100% voter turnout,
and half Republicans did, Republicans
would still win by a pretty comfortable margin.
When you say it like that, it seems
like anybody that is progressive thinking in this area
would be alone.
However, if you say it another way,
doesn't really shake out like that.
If I get in line at Walmart and there's
10 people in front of me, two or three
are going to be Democrats.
That's how it works out.
This country isn't that divided.
And that's just party affiliation.
When you get down to the actual issues,
they're even less divided.
The average person out here, we don't hate each other.
Not people.
But a lot of our politicians do everything
they can to make us put party over country,
even at the cost of 100,000 people.
If you're in a red area, a very conservative area,
and you feel like there are no progressives around you,
please remember there are a whole bunch of people
who think just like you in every way,
including thinking they're alone.
Sometimes when you speak up, you're not really
saying it to convince anybody.
You're saying it so that people around you who already
think the way you do know they're not alone.
And in a lot of those red areas, it's desperately needed.
There are people waiting on you.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}