---
title: Let's talk about Republicans calling to jail Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7QuPPE-3_ak) |
| Published | 2020/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces news from Nashville involving emails surfacing about downplaying data connected to the mayor's office.
- Emails suggest a decision to not highlight the lack of spread in bars and restaurants in Nashville.
- Republicans have criticized this decision, focusing on downplaying data leading to keeping bars closed.
- Beau hints at missing context, possibly related to similar events in Florida and Texas with Republican governors.
- Beau shifts the focus to Republican reactions, quoting Charlie Kirk and Donald Trump Jr. advocating for consequences for downplaying data.
- Beau challenges the Republican stance, suggesting accountability should also extend to the President if downplaying data is a punishable offense.
- Beau questions whether these statements are genuine concerns or simply political rhetoric.
- Beau insists that if downplaying data is a punishable offense, it should be applied uniformly.
- Beau calls for accountability and application of standards across the board in such cases.
- Beau stresses the importance of upholding the law and ensuring accountability for all public officials.

### Quotes

- "Any politician like the Nashville Mayor John Cooper, who intentionally covers up data on public health deaths in order to keep restaurants and bars closed should be removed from office and tried immediately." - Charlie Kirk
- "The dim mayor of Nashville knowingly lied about data to justify shutting down bars and restaurants, killing countless jobs and small businesses in the process. Everyone involved should face jail time." - Donald Trump Jr.

### Oneliner

Beau calls for accountability and uniform application of standards in response to Republican criticisms of downplaying data in Nashville.

### Audience

Public officials

### On-the-ground actions from transcript

- Hold public officials accountable for downplaying data. (implied)
- Advocate for uniform application of standards in similar situations. (implied)

### Whats missing in summary

Full context and details on the events in Nashville and Republican reactions. 

### Tags

#Nashville #DataDownplaying #RepublicanReactions #Accountability #PublicOfficials


## Transcript
Well howdy there internet people, it's Beau again and we're back in the shop.
And today we're going to talk about some news coming out of Nashville, Tennessee.
And I have to admit that it is interesting news.
I will give you that.
If you don't know, some emails have surfaced and they appear to show people connected to
the mayor's office up there having a conversation.
In this conversation, it does seem that they decide to downplay some information, to downplay
some data.
See the data in Nashville didn't show a lot of spread inside bars and restaurants.
And these emails do, at least appear to show them making the decision to not highlight
this fact and keep the bars closed.
Republicans, predictably, have jumped all over this.
I'm going to suggest that perhaps there is some additional context that is missing from
the initial reporting.
Perhaps hypothetically speaking and not being very specific at all, this context may have
to do with events that occurred in Florida and Texas around the same time dealing with
bars and decisions undertaken by Republican governors DeSantis and Abbott.
Hypothetically speaking, not very specific at all.
But I don't want to talk about that.
I'll let the mayor's office respond to this because they probably have even more context
than I do.
I want to talk about the reaction coming out of Republican circles because there are some
interesting takes out there and I think we need to make sure that they are on record
and everybody knows that this was said.
Okay.
Any politician like the Nashville Mayor John Cooper, who intentionally covers up data on
??? I am not calling it that.
We're just going to call it the public health deaths ??? in order to keep restaurants and
bars closed should be removed from office and tried immediately.
That's Charlie Kirk.
But wait, it gets better.
The dim mayor of Nashville knowingly lied about data ??? all capital letters ??? to
justify shutting down bars and restaurants, killing countless jobs and small businesses
in the process.
Everyone involved should face jail time.
That is Donald Trump Jr.
Okay Jr., deal.
On behalf of the American people, I accept your offer.
We will provide you with one Nashville Mayor in exchange for everybody in the administration
who downplayed data.
Because certainly, if downplaying data and it leading to the loss of jobs is a jail-able
offense, I would assume that downplaying data and it leading to the loss of lives is also.
Very bold take coming from the Republican Party right now.
Because either they have to admit that they are putting profits over people very openly,
that that's what they care about.
Or they have to admit that every one of these calls ??? and there are a bunch of other ones
??? also apply to the President of the United States.
Those are the options.
I guess the third is that they could just say, no, we're a bunch of third-rate authoritarian
goons who are attempting to use the threat of jail time to stifle political opposition.
I guess that's another option as well.
But that's it.
Do these statements mean anything?
Or is it just political rhetoric?
If they mean anything at all, they must also apply to the President.
Again, I don't believe the email thing is going to be a big deal, but his emails ??? we
can hear it coming.
I do believe there's some additional context that may shed some light on this conversation.
And I do think it would be better coming from the mayor's office, just in case I'm wrong.
But these statements coming from the Republican side of the aisle, those who make them need
to be held accountable for them.
They need to make sure ??? we all need to make sure ??? that this is what happens.
If that's really the case, if this is really the standard, downplaying data and it leading
to a negative outcome is a jailable offense for a public official, then we have to apply
it across the board.
We are a nation of laws, are we not?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}