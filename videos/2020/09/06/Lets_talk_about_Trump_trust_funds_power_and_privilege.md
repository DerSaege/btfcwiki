---
title: Let's talk about Trump, trust funds, power, and privilege....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PH4m0ZWGD-4) |
| Published | 2020/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a world where some are born with trust funds, influencing laws and systems in their favor.
- Talks about privileges like access to education, health care, financial institutions, and preferential treatment by law enforcement.
- Mentions privileges that can come from family wealth, having a rich uncle, and being a veteran.
- Points out exclusionary traits that individuals may have, like being born poor or having visible traits like scars or skin tone.
- Addresses the importance of acknowledging privilege and how it's not an attack but a statement of fact.
- Emphasizes the need to speak out against inequalities and inequities if born into privilege.
- States that acknowledging privilege is not about making life seem easy but recognizing advantages.
- Urges for continued discourse on privilege and the need to address and fix systemic issues in society.

### Quotes

- "Having a privilege doesn't mean your life isn't hard. It just means it's not harder because of this."
- "If you're born into a position of privilege and you don't use your voice to speak out against the inequities, against the inequalities, yeah, that kind of makes you a bad person."
- "If we don't talk about it, we can't fix it, and it needs to be fixed."
- "It's not a trust fund, and having a privilege isn't an attack on you."
- "It's almost like he doesn't want to accept that he is in his station due to luck, not through any work of his own."

### Oneliner

Acknowledging privilege is not an attack but a statement of fact, urging the need to speak out against inequalities for a more equitable society.

### Audience

Activists, Allies

### On-the-ground actions from transcript

- Speak out against inequalities and inequities (exemplified)
- Continue the discourse on privilege and systemic issues (exemplified)

### Whats missing in summary

The full transcript provides a comprehensive analysis of privilege, including examples of trust funds, privileges, exclusionary traits, and the importance of acknowledging and speaking out against inequalities for societal change.

### Tags

#Privilege #Inequality #SystemicIssues #SocialJustice #Activism


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about trust funds
and wealth,
power,
privilege,
exclusionary traits,
unseen forces,
all kinds of stuff.
To start off with,
I want you to picture you live in a world
where some people are born with trust funds,
born with money.
When they're born they have a million dollars.
And let's say
that this set of people,
they have a lot of influence over how laws are written,
and sometimes they tailor those laws to benefit themselves.
That they get preferential treatment
when it comes to financial institutions.
That they get preferential treatment
by law enforcement and the criminal justice system.
That they have better access to
education and health care. Okay, so picture you live in the United States.
I would suggest
that if you were part of that group of people,
you have a privilege.
In fact,
we used to call this class of people,
people who were born to wealth and privilege.
It's not a new concept, guys.
But that's an extreme example.
Let's take it a step back.
Let's say
your family doesn't have enough to give you a trust fund with a million dollars in it,
but they got enough to pay
for you to go to college
without an academic or athletic scholarship.
That's a privilege.
Your grades can be so-so, you can just buy your way in.
It's a privilege.
Something you're born with.
Let's say you just have a rich uncle.
It means you have access to that generational wealth.
You want to start a business, maybe he gives you a loan.
It's a privilege.
These are all unseen forces
that alter
a person's life.
You don't really notice them up front, really.
Now, these are ones that you're born with. What about ones that you're not born with?
Like, let's say you joined the military.
You're a veteran.
Most people in the United States,
with the exception of those in public office,
they respect veterans.
You have better access to certain financial instruments, like home loans.
It's a privilege.
You can call it an earned privilege if you want to,
but it's a privilege.
Let's say you took advantage
of your parents having that cash,
and you went to college. You got that degree.
That degree confers a certain amount of privilege.
Again, if you want to call it an earned privilege, you can.
Privilege exists.
We have established this.
What about exclusionary traits? The other side of the coin there.
Like, let's say
you were born poor,
born in a trailer.
Is that something you necessarily are going to advertise to the world?
Probably not, because there's a stereotype that goes along with it.
Let's just say you were born in Mississippi or West Virginia.
What's the stereotype?
You're ignorant. I mean, you could kind of apply this to the whole South.
For the rest of the country, we do have schools and education and universities and stuff like that down here.
But there's that image, that stereotype,
that exclusionary thing.
No, don't hire that guy.
So,
there are privileges and exclusionary traits that you can be born with.
What about exclusionary traits that you pick up along the way?
Got a friend.
He stopped to check out that semi-interesting thing on the side of the road.
Boom! He's got a prosthetic now.
It's hot down here.
When he's wearing shorts,
you can treat him differently.
Let me get that for you.
This is an able-bodied guy, and he has one of those new prosthetics.
Looks like a Terminator-leg guy can outrun me.
He didn't like the way he was treated, so he started wearing pants all the time.
It could be 110 degrees, he's wearing pants.
Now, another guy
has an accent.
He was working in a field where critical thinking
was a big part of it.
Didn't think people would take him seriously, so he hid it.
You know, if you sound like a character from Justified, most times people don't
assume you're Boyd Crowder.
They assume you're one of the other characters that's not so bright, because you talk slow.
Therefore, you think slow.
Exclusionary trait.
No, another guy took a round to the face.
In here, out here. Nasty scar.
People thought he was mean.
People were intimidated by him.
Guy's a teddy bear.
But, the stereotype.
He grew out a beard.
Like mine. Hide it.
What if it's an exclusionary trait that's very visible,
but you can't hide it?
What if it's plain as day?
On your face. Your skin tone.
In the United States,
I don't even think I need to say
which skin tone
would be a privilege,
and which skin tones
might be considered an exclusionary trait.
It's a thing.
It exists.
Now, there's also the idea
that if you don't acknowledge
this kind of privilege,
well, then you're racist automatically.
That's silly.
That's not true.
It's not true because they're skipping a step.
People who say that are skipping a step.
If you are aware
of the statistics
that are taken down by racial demographic
in this country when it comes to poverty,
crime, health care, education,
all of this stuff,
and you don't acknowledge
that there's some unseen privilege,
the only other conclusion you can come to
is that black people are somehow less capable.
And yeah, that's racist.
So there's a step missing there.
Ignorance alone doesn't make somebody racist.
It's not a good thing most times.
If you're aware of those stats
and you don't acknowledge
that your skin tone might be a privilege,
you need to do some soul searching
because the reality is
most laws are made by people who look like me,
and sometimes they tailor those laws
to benefit people who look like me.
People who look like me are generally treated better
by law enforcement and the criminal justice system.
They have better access to financial institutions,
better health care access,
better access to education,
just like a trust fund.
It's not a trust fund,
and having a privilege isn't an attack on you.
If you're born a certain way,
that doesn't mean you're a bad person.
In fact, that's literally the whole point
of acknowledging these.
It's not a personal attack.
It's just a statement of fact.
Because of the station you were born into,
you have these privileges.
You don't have any control over some of this.
However, if you were born into a position of privilege
and you don't use your voice to speak out
against the inequities, against the inequalities,
yeah, that kind of makes you a bad person.
You know it exists.
You know it exists.
People just don't like the terminology
that comes along with it.
They don't want to compare themselves
to the trust fund kids,
those who think they hit a home run,
but they were born on third base.
Having a privilege doesn't mean your life isn't hard.
It just means it's not harder because of this.
Your life isn't harder because you're white.
It's harder for a whole bunch of different reasons.
You could be born poor in West Virginia
with a thick accent and a prosthetic.
That's going to make your life harder.
But even with all that,
you probably still have a skin tone privilege.
With everything going on in the country,
with us finally starting to address some of our history,
I would suggest that right now is a really bad time
to stop talking about this.
Because if we don't talk about it, we can't fix it,
and it needs to be fixed.
I do find it funny that the one person
who is screaming about privilege
and how it's not a thing and how it's un-American
is one of the most privileged people in the country.
It's almost like he doesn't want to accept
that he is in his station due to luck,
not through any work of his own.
I would also point out that when you call the concept
of white privilege un-American,
it makes it pretty clear who you think Americans are.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}