---
title: Let's talk about Trump's admission of downplaying....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HG7yZ_4SBLQ) |
| Published | 2020/09/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President admitted to downplaying COVID-19, as confirmed by Bob Woodward's tapes.
- Trump tried to justify downplaying by claiming he didn't want to cause panic.
- Beau criticizes Trump for his fear-mongering tactics on various issues.
- Trump's response to the public health crisis was indecisive and ultimately harmful.
- Beau points out Trump's hypocrisy in not addressing the seriousness of the pandemic.
- Trump's pattern of fear-mongering is contrasted with his handling of the COVID-19 situation.
- Beau suggests that Trump's inaction and indecisiveness led to significant loss of life.
- The president's failure to lead effectively in the face of crisis is criticized.
- Beau questions Trump's claim of being a cheerleader for the country.
- The transcript ends with Beau expressing disbelief at Trump's handling of the pandemic.

### Quotes

- "He assumed the American people would panic because he did."
- "What, almost 200,000 gone because of his indecisiveness."
- "He froze. Like every other wannabe tough guy."
- "He's not a cheerleader for the country."
- "The president's whole MO is fear mongering and creating panic."

### Oneliner

President admitted to downplaying COVID-19, fear-mongering, and creating panic, leading to significant consequences and loss of life.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Hold elected officials accountable for their actions (suggested)
- Advocate for transparency and honesty in leadership (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's response to the COVID-19 pandemic and criticizes his leadership style, fear-mongering, and impact on public health.

### Tags

#COVID-19 #Leadership #Accountability #FearMongering #PublicHealth #Trump


## Transcript
Well, howdy there, Internet people. It's Beau again.
So to everybody's surprise, today the president admitted that Bob Woodward's releases are true.
I mean, it'd be hard for him to deny it. They're on tape.
But, I mean, I think a whole lot of us were expecting him to deny it anyway.
But he admitted it. He admitted that he was downplaying all of this.
I mean, he spun it, tried to. I don't think it resonated with many people.
But he tried to spin it in his own way so that he didn't want to frighten people.
What he actually said...
Well, I think if you said in order to reduce panic, perhaps that's so.
The fact is, I'm a cheerleader for this country.
I love our country, and I don't want people to be frightened.
I don't want to create a panic, says the guy who on December 18th of 2019 tweeted out,
Well, they're not after me. They're after you.
Who's they? We don't know. Just something to be afraid of.
Some mythical creature. Some mythical enemy.
This is a man who fear-mongered so much, the American people are building a wall
on the southern border and locking up toddlers over caravans that have been coming for years.
He had people afraid of bathrooms. He said the suburbs were going to be destroyed.
There's the deep state out to get them.
Shadowy dark forces. The stock market's going to crash if he doesn't get re-elected.
Cities are going to burn because Biden's going to defund the police,
and the American dream will be destroyed by low-income housing.
Sounds like he wants to frighten people. So what's the difference?
What is the difference between all of this and the public health issue he was faced with?
The public health thing's real.
See, the president showed himself to be just like every other wanna-be tough guy,
talking about what they do in a certain situation.
Yeah, it's real easy to be tough and talk tough.
When your opposition is imagined.
When it's something you've just willed out of thin air.
When there's nothing to be afraid of.
When there is no reasonable fear.
He assumed the American people would panic because he did.
Because he actually had a test.
He heard the information, realized how bad it could be,
didn't know what to do, and he froze.
Fight, flight, or freeze.
Mr. Tough Guy President, when he was faced with a challenge, froze.
Just like every other wanna-be tough guy out there.
And because he couldn't handle it, because he couldn't find the courage to lead,
he assumed the American people would panic as well.
And because of his indecisiveness, we lost a lot.
He heard that information, couldn't find the courage to fight,
didn't have the decency to flee and resign, he just froze.
What, almost 200,000 gone because of his indecisiveness.
I think it's gonna be real hard to spin this.
The reality is, after he knew and had a full understanding of what was going on,
he went out there and told people there was nothing to worry about.
Go on about your day.
Because he panicked.
Because he was worried that unlike these things that he's more than willing to
fear monger about, well this one might hurt him.
Might hurt his poll numbers.
He's not a cheerleader for the country.
A cheerleader for the country would have gone out there and said, hey,
this is gonna be bad.
But we gotta get through it.
We're gonna have to shut down.
We're gonna have to wear masks.
We're in for a long ride.
Instead of just saying, it's gonna magically disappear like the caravans did.
Once they had served their usefulness.
Once he couldn't scare people with it anymore.
The president's whole MO is fear mongering and creating panic.
I find it very hard to believe that he chose not to do it here.
He didn't choose.
It was a biological response.
He froze.
Like every other wannabe tough guy.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}