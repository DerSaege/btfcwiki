---
title: Let's talk about President Trump's attempts to change the story....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Lbix0KEWbmU) |
| Published | 2020/09/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- President's attempts at deflection amid public health crisis reveal his deceit.
- American people not falling for distractions like Afghanistan withdrawal or Supreme Court suggestions.
- President's ineffective leadership led to more lives lost in months than in 20 years of armed conflict.
- Tom Cotton's suggestion to use US Army for restoring order raises concerns about his Supreme Court nomination.
- Advocating no-quarter reveals troubling mindset about extrajudicial execution of American citizens.
- Woodward's release of audio exposes President's downplaying of public health crisis, costing American lives.

### Quotes

- "President's willful deceit cost tens of thousands of American lives."
- "This other stuff, who he might suggest for the Supreme Court, you know, a possible withdrawal, that's not news."
- "The news is a bunch of bags that are full that didn't have to be."

### Oneliner

President's deflective tactics fail to distract from his deceit in handling the public health crisis, costing American lives.

### Audience

American citizens

### On-the-ground actions from transcript

- Call for accountability from leaders regarding handling of public health crises (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the President's attempts at deflection and deceit amid the public health crisis, exposing the consequences of his actions on American lives.

### Tags

#Deception #PublicHealthCrisis #Accountability #TomCotton #BobWoodward


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the president's attempts to change the story.
His attempts at deflection and why those attempts don't really seem to be working.
Why this time the American people aren't following his distractions.
Aren't following for his deflections.
I'm no political strategist, I'm certainly no Lindsey Graham, but I think I can provide
a little bit of insight into why this isn't going to work this time.
If you missed the news yesterday, the president was shown to have been willfully deceiving
the American people in regards to the severity of the current public health crisis.
It appears he has settled on two specific lines of deflection.
The first is bringing up the possible withdrawal from Afghanistan.
Nobody really seems to care and people seem to be less than receptive to this news.
I'm going to suggest that it's not in the president's best interest to talk about that
right now.
Because all it really does is draw attention to the fact that due to his ineffective leadership,
his inability to lead, and his willful deceit, we lost more people in the last few months
than we did in almost 20 years of armed conflict there.
And by more than, to be really clear, I mean like almost 50 times as many.
I'm not sure I'd be drawing attention to that if I was him.
The other distraction is his Supreme Court suggestions.
One of which is Tom Cotton.
Tom Cotton is an individual who, during the recent unpleasantness in the cities, suggested
sending in the US Army to restore order, specifically the 101st.
And when questioned on it, he dumbled down and said, and if necessary, the 10th Mountain,
82nd Airborne, 1st Cav, 3rd Infantry, making sure to name a whole bunch of huge units to
get everybody's attention so everybody sees the next part and knows exactly how tough
he really is.
He then goes on to say, whatever it takes to restore order.
I'm going to stop here and suggest that if somebody would advocate the use of the US
Army rather than the National Guard to restore order in a US city, they probably shouldn't
be on the Supreme Court.
This is especially true if the next two words are no-quarter.
For the uninitiated, those who, I don't know, were never briefed on war crimes, no-quarter
is exactly what it sounds like.
You do not provide quarters, housing, to your opposition in the event they attempt to surrender.
You just deal with it there on the field.
I'm not sure that I feel 100% comfortable with a man who advocated the extrajudicial
execution of American citizens, making determinations about inalienable rights.
This may be why the American people do not care about his distractions right now.
Now to be very clear, and to make sure that I'm not doing what the President wants everybody
to do and chase this shiny ball away from the real story, to recap, yesterday, Woodward,
famed journalist of half a century, released some audio from a series of interviews he
did with the President in which the President openly said that he was downplaying the severity
of the public health crisis, specifically in regards to how it is transmitted, which
means the American people were denied hearing their President advocate for the things that
would have helped mitigate it, such as masks.
The President's willful deceit cost tens of thousands of American lives.
There's no way around that, and that's the story.
That's the news.
This other stuff, who he might suggest for the Supreme Court, you know, a possible withdrawal,
that's not news.
The news is a bunch of bags that are full that didn't have to be.
And that's the President's fault.
And Bob Woodward had the receipts.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}