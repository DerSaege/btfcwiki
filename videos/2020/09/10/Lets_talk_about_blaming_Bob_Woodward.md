---
title: Let's talk about blaming Bob Woodward....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WHY1YEvH-d8) |
| Published | 2020/09/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's attempt to blame Bob Woodward for his actions.
- Everyone already knew that the president was aware of the information.
- Woodward's effectiveness lies in showing the contrast between what was known privately and publicly.
- Americans were already aware of the information that Woodward revealed.
- Woodward informs the American people and gives them a choice for the future.
- The president did not let the experts do their job, actively undermining their work.
- Woodward's work demonstrates the president's prioritization of his approval rating over American lives.
- Had Woodward released the information earlier, it wouldn't have had the same impact.
- The president's base might have dismissed the information as just an offhand comment.
- Woodward's form of journalism is exactly what he did, and blaming him is not fair.

### Quotes

- "Everybody knew back then that the president was aware of this."
- "He informed the American people and he's giving them a choice."
- "He actively undermined their work and it cost tens of thousands of American lives."
- "His job, his form of journalism, what he does is exactly what he did."
- "Let's just hope that next year there are people who will listen to the experts."

### Oneliner

President's attempt to blame Woodward falls flat as everyone already knew, showcasing Woodward's contrast between public and private knowledge while underscoring the president's failure to prioritize American lives over his own approval ratings.

### Audience

American citizens

### On-the-ground actions from transcript

- Listen to the experts and prioritize public health over personal gain (implied).
- Support investigative journalists like Bob Woodward who inform the public and hold leaders accountable (implied).

### Whats missing in summary

The detailed emotional impact of Woodward's work on revealing the truth and its potential to influence future decision-making.

### Tags

#BobWoodward #InvestigativeJournalism #PublicHealth #Accountability #AmericanLives


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about blaming Bob Woodward.
That's apparently the president's next move in his attempt to squirm out of this is to
blame Woodward.
And there's a lot of people who seem willing to accept this narrative.
I don't know that they should, but before we get too far into this I feel like I should
make a disclosure.
I have read everything Woodward has pretty much ever written.
I'm a huge fan.
The character Deep Goat that sometimes appears on this channel is a nod to his work.
Woodward is a long format investigative journalist.
That's what he does.
That's his job.
The theory is that if Woodward had come out with this back then, back in March, that everything
would have been different.
I don't think that's true.
I don't think that's true.
Because the reality is everybody knew that the president knew this.
Logically, everybody knew that.
The fact that he knew it, that's not a surprise and that's not the story.
What makes Woodward's work effective is that it shows the date that he knew, at least a
date that we know and we can confirm that he knew this stuff, and then you can contrast
it with what he said publicly.
Had this been released at the time, most Americans would go, yeah, we know this.
And it wouldn't have mattered to most people because Trump would have just spun it.
One little comment and he would have spun it and it would have disappeared like magic.
Just would have gone away.
Wouldn't have had the impact that it does.
We all knew everything that was in those tapes and we knew the president knew.
And I get to do something now that I have been waiting to do since January.
On January 23rd, I released a video, the first video I released talking about this subject.
And I tried to convey calm.
I said there wasn't much to worry about.
I talked about how the fact that some people were asymptomatic, well that was a worry because
it could help it spread.
Talked about how we probably have to distance from each other.
Social distancing hadn't come into the popular vocabulary.
I didn't use that term.
Talked about having to wash your hands, sneeze into a tissue or the crease of your arm.
Talked about how we might have to shut down cities.
I am nowhere near as plugged in as the president or Bob Woodward and I knew in January.
It's not a surprise that the president knew.
The reason Woodward's work is effective is because he gathers all of the information
and he puts it into a story so those who don't follow the events every single day can get
it all at once.
That's why he's Bob Woodward.
That's why he is probably America's best known, most celebrated journalist.
That's the reason.
Had he just dropped the audio then nobody would have cared.
But now it stops us hopefully from having the same poor leadership that we've had.
He informed the American people and he's giving them a choice.
Do you want this for another four years or not?
Because we all knew.
Now in that video back in January I wasn't worried.
In fact I kind of made a joke about some of it saying I don't think it's time to break
out gas masks because we have public health officials.
We have people who this is what they do and they're good at their job.
And in fact the last line in that video is something to the effect of do what you do,
let them do what they do.
That's actually what Woodward is showing to the American people.
That the president didn't do that.
He didn't let the experts do their job.
He didn't follow their lead.
He actively undermined their work and it cost tens of thousands of American lives.
It helped it spread all over the world.
That's the value of Woodward's work.
Everybody knew back then that the president was aware of this.
It was on every single network television station.
Everybody was broadcasting this information.
It's not a surprise that he knew.
It's the clear demonstration of the contrast between what he knew, what he was willing
to admit privately versus what he was telling the American people.
Had he said what he said privately in public he would have been backing up the public health
professionals, those people who are good at their job, who were trying to mitigate, and
it would have made a difference.
But he didn't do that.
He chose to look out for himself and his approval rating rather than the American people.
That's the story.
That's the news.
And that story wouldn't have existed if Woodward had released this back then.
This would have been another bump that the president would have been able to spin or
deflect, blame on somebody else.
His base would have just said, oh, it's just an offhand comment.
Of course he knows that.
It's a lot harder to do it with months of him denying it, with months of him putting
American lives at risk.
The thing about it, and I'm pretty certain that I can say this, if you want to drag Woodward's
name through the mud for this, I'm pretty sure he'll be okay with it.
His name's been through worse.
But I don't think it's fair.
His job, his form of journalism, what he does is exactly what he did.
And yeah, in a perfect world, he could have released that video, that audio back then
and it would have made a difference.
But we don't live in a perfect world.
If we lived in a perfect world, we wouldn't have a president who would lie to the American
people about a public health issue for months.
Let's just hope that next year there are people who will listen to the experts, who won't
actively undermine their work.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}