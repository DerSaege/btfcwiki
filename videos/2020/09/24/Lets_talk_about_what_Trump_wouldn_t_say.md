---
title: Let's talk about what Trump wouldn't say....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=srPUsL3PQ5U) |
| Published | 2020/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Donald J. Trump's alarming refusal to commit to a peaceful transfer of power is more critical than any campaign promise.
- Trump's lack of commitment to a peaceful transferal of power should concern every citizen.
- The nation's security and soul may hinge on ensuring Trump loses in a landslide.
- Trump's refusal to pledge a peaceful transfer of power reveals his true character.
- Beau urges everyone to pay attention to Trump's concerning actions and statements.
- Trump's failure to commit to a peaceful transition raises doubts about his future behavior.
- The importance of ensuring a peaceful transition of power transcends party lines and individual issues.
- Trump's non-commitment to a peaceful transfer of power is a significant issue that should weigh on everyone's mind.
- Beau stresses the need for Americans to recognize the gravity of Trump's refusal to commit to a peaceful transfer of power.
- Beau concludes by encouraging people to stay vigilant and engaged.

### Quotes

- "When somebody tells you who they are, when they show you who they are, who they really are, you better believe them."
- "He just showed the entire country who he is."
- "That lack of a commitment is more critical than any campaign promise."
- "The only way I know of to avoid that concern is to make sure he loses in a landslide."
- "It's incredibly significant that it happens."

### Oneliner

President Trump's refusal to commit to a peaceful transfer of power is more critical than any campaign promise, urging citizens to ensure his defeat for the nation's security and soul.

### Audience

American voters

### On-the-ground actions from transcript

- Ensure Trump loses in a landslide (implied)
- Stay vigilant and engaged (implied)

### Whats missing in summary

The full transcript provides more context on Trump's statement and Beau's urgent call for people to pay attention to the implications of his refusal to commit to a peaceful transfer of power.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what President Donald J. Trump didn't say yesterday.
Because everybody was talking about what he did.
What he did say doesn't matter, not really.
He said something to the effect of we need to get rid of the ballots, talking about mail
in ballots or something.
And everybody feigned surprise.
It's not shocking, it's not surprising.
He's a modern Republican, yeah, and he wants to suppress the vote.
That's like their thing.
The only thing they like suppressing more than the vote is suppressing poor people.
What he didn't say, what he wouldn't commit to, that's what matters.
Because he said that when he was asked another question.
He was asked directly if he would commit to making sure there was a peaceful transferal
of power.
And he wouldn't.
He wouldn't make that commitment.
He wasn't asked if he would accept the election results.
He wasn't asked if he would, you know, avoid taking it to court.
He wasn't asked if he would concede election night.
He was asked if he would commit to a peaceful transferal of power.
And he wouldn't.
That's shocking.
That's surprising.
That's alarming.
That's something everybody in the country should be paying attention to.
That lack of a commitment is more important than any campaign promise.
That needs to be weighing on everybody's mind.
If he won't commit to a peaceful transferal of power, what makes you think he'll commit
to it in four years?
What he wouldn't say, what he wouldn't commit to, is incredibly important.
It is more important than any single issue.
It is more important than party lines.
The only way I know of to avoid that concern is to make sure he loses in a landslide.
That's what needs to happen.
It's incredibly important that it happens.
It was important for the nation's soul before.
It may be important to the nation's security.
He wouldn't commit to a peaceful transferal of power.
He refused to commit to that.
When somebody tells you who they are, when they show you who they are, who they really
are, you better believe them.
And he just showed the entire country who he is.
I hope people are paying attention.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}