# All videos from September, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-09-30: Let's talk about the first debate between Trump and Biden.... (<a href="https://youtube.com/watch?v=vRJllbpXoFo">watch</a> || <a href="/videos/2020/09/30/Lets_talk_about_the_first_debate_between_Trump_and_Biden">transcript &amp; editable summary</a>)

The first presidential debate was a chaotic dumpster fire, with a stark contrast in encouraging voter turnout reflecting deep-seated fears of failure and unpopularity.

</summary>

"The only real divide, the only real thing that we saw that I don't think anybody expected was how stark the difference is when it comes to encouraging people to vote."
"He knows he's a failure. He knows that people don't support him."
"That was the least presidential thing I've ever seen in my life."

### AI summary (High error rate! Edit errors on video page)

Recap of the first presidential debate with only one key takeaway.
Biden's mental faculties were intact, contrary to Trump's claims.
Trump consistently attempted to interrupt and bully Biden, as expected.
Biden did not take Trump's bait and remained composed.
Trump promoted baseless theories without much pushback from Wallace.
Trump refused to condemn a certain group, a known behavior.
The stark difference in encouraging people to vote was the most significant observation.
Trump actively tried to undermine the election and suppress the vote.
Biden, on the other hand, wanted to encourage voter turnout.
Trump's actions suggest a fear of his unpopularity and failed policies being reflected in the election results.
The debate was chaotic and unproductive, with little chance of changing minds.
Biden appeared dismissive of Trump's attacks and may have felt embarrassed to share the stage with him.
Trump's behavior during the debate was deemed unpresidential and below expectations.

Actions:

for voters, concerned citizens,
Support voter registration drives to increase voter turnout (implied)
Educate others on the importance of voting and the impact of policies on everyday lives (implied)
</details>
<details>
<summary>
2020-09-30: Let's talk about making the debate more fair for Trump.... (<a href="https://youtube.com/watch?v=IwiB8jPUQAs">watch</a> || <a href="/videos/2020/09/30/Lets_talk_about_making_the_debate_more_fair_for_Trump">transcript &amp; editable summary</a>)

Beau proposes a debate restructuring system where candidates follow rules or face consequences, advocating for disqualifying candidates who cannot adhere to basic civil discourse.

</summary>

"No, do not restructure these debates. Let him continue to embarrass himself."
"Change the candidate."
"He never should have been in that office to begin with."

### AI summary (High error rate! Edit errors on video page)

Proposes restructuring the debates due to recent candidate performance.
Outlines a format where candidates start off in green and move to yellow or red if they break rules.
Suggests a system where candidates answer ten questions without breaking rules to receive a special surprise.
Emphasizes the importance of candidates following basic rules of civil discourse.
Argues that candidates unable to follow debate rules should be disqualified.
States that the President of the United States is not fit for office and was never qualified.
Criticizes the current President's inability to handle debates and real situations.
Believes the debates should not be restructured but rather the candidate should be changed.

Actions:

for debate organizers,
Implement a structured format for debates with consequences for rule-breaking (suggested)
Advocate for disqualifying candidates who cannot follow basic rules of civil discourse (implied)
</details>
<details>
<summary>
2020-09-29: Let's talk about my biggest worry about November 3rd.... (<a href="https://youtube.com/watch?v=abqyq6cs0eo">watch</a> || <a href="/videos/2020/09/29/Lets_talk_about_my_biggest_worry_about_November_3rd">transcript &amp; editable summary</a>)

Beau draws inspiration from Hunter S. Thompson, reflecting on the peak of San Francisco in the 60s, urging continuous momentum for systemic change beyond the election.

</summary>

"We have to keep the momentum going."
"A Biden win cannot be the high watermark of this."
"The only way it changes is if we don't let that wave break."
"We want all men created equal, liberty and justice for all."
"We can't throw away this shot."

### AI summary (High error rate! Edit errors on video page)

Draws inspiration from Hunter S. Thompson.
Recalls a specific passage about San Francisco in the 60s.
Describes the universal sense of winning and inevitable victory.
Talks about the energy prevailing without the need for a fight.
Mentions the wave of high momentum they were riding.
Mentions the peak and the feeling of being alive in that time.
Talks about the madness in every direction in San Francisco.
Expresses the importance of keeping the momentum going.
Stresses the need for political engagement in the United States.
Emphasizes the importance of not letting the wave break on November 3rd.
Calls for continued fighting regardless of the election result.
Talks about building a base of power for systemic change.
Expresses the desire for promises to be fulfilled for all individuals.
Warns against returning to "normal" after a Biden win.
Urges to keep the momentum going and not waste the current opportunities.

Actions:

for activists, voters, community members,
Keep the momentum going by staying politically engaged (implied).
Build a base of power for systemic change in your communities (implied).
Ensure promises are fulfilled for all individuals by taking action locally (implied).
</details>
<details>
<summary>
2020-09-28: Let's talk about Trump, the debate, fiction, and poll numbers.... (<a href="https://youtube.com/watch?v=NRxGFTzlxPA">watch</a> || <a href="/videos/2020/09/28/Lets_talk_about_Trump_the_debate_fiction_and_poll_numbers">transcript &amp; editable summary</a>)

President won't be fact-checked, debates lack accountability, polls and promises are worthless - voting is what truly matters.

</summary>

"President of the United States will get up there and say whatever he feels like."
"It's embarrassing that we need fact-checkers at the presidential debates."
"These debates are worthless. They're as worthless as his promises."
"Those polls do not matter. They mean absolutely nothing."
"Who shows up the day of? Who votes?"

### AI summary (High error rate! Edit errors on video page)

President of the United States won't be fact checked during upcoming debates.
Lack of fact-checking gives President Trump a free pass to lie during the debate.
It's embarrassing that fact-checkers won't be present at the presidential debates.
The debate will likely be filled with lies, as Trump is known for not telling the truth.
Beau quotes Octavia Butler about choosing leaders wisely.
Debates and polls are deemed worthless; only actual voting matters in the end.
Democrats should not be complacent based on poll numbers; what matters is voter turnout.
The focus is on the importance of people showing up to vote rather than relying on polls.
Beau questions the value of debates if candidates won't be held accountable for their statements.
The emphasis is on the significance of action through voting rather than passive reliance on polls or promises.

Actions:

for voters,
Show up and vote on election day (implied)
Avoid complacency based on poll numbers; prioritize active voting participation (implied)
</details>
<details>
<summary>
2020-09-28: Let's talk about Trump's taxes and how the Dems are missing.... (<a href="https://youtube.com/watch?v=doENavQOvmc">watch</a> || <a href="/videos/2020/09/28/Lets_talk_about_Trump_s_taxes_and_how_the_Dems_are_missing">transcript &amp; editable summary</a>)

Beau advises the Democratic Party to focus on Trump's financial failures rather than his tax payments to weaken his hold on supporters who believe he can do no wrong.

</summary>

"They care only about themselves. They've put their hope in this man based on what he told them. And it was a lie."
"If you don't think Trump will sell out the American citizens as he has done everybody, you're more under his spell than most."
"He couldn't even turn a profit in the middle of one of the best economies we've ever had."
"The idea that he somehow scammed the government, it's not going to fly with them."
"You're talking about selfish people. That's his base."

### AI summary (High error rate! Edit errors on video page)

Advises the Democratic Party on how they are mishandling information damaging to Trump's campaign regarding his tax documents.
Trump's base views him as an establishment outsider and may actually see it as a good thing if he paid very little in taxes.
Points out that Trump's base is skilled at "double think," believing contradictory ideas simultaneously.
Emphasizes that the focus should be on how Trump's financial losses and inability to profit during a strong economy undermine his image as a successful businessman.
Suggests that Trump's core supporters care only about themselves and have blind faith in him despite any lies or misrepresentations.
Argues that the Democratic Party should frame the narrative around Trump's financial failures rather than just his tax payments to weaken his hold on supporters.

Actions:

for democratic party strategists,
Reframe the narrative around Trump's financial failures and inability to profit during a strong economy to undermine his image (implied).
</details>
<details>
<summary>
2020-09-26: Let's talk about what Republicans must do by November 2nd.... (<a href="https://youtube.com/watch?v=_tYupMnkkGo">watch</a> || <a href="/videos/2020/09/26/Lets_talk_about_what_Republicans_must_do_by_November_2nd">transcript &amp; editable summary</a>)

Beau urges Republicans to think critically, find common ground with opposing parties, and judge ideas on their merits rather than blindly following political figures.

</summary>

"Ideas stand and fall on their own."
"Don't fall in line. Think for yourself."
"Judge these ideas, these policies on their merits."

### AI summary (High error rate! Edit errors on video page)

Urges Republicans to think critically before November 2nd.
Encourages Republicans to identify and agree with some policies from AOC and Pelosi.
Acknowledges his criticism of President Trump but still finds common ground on certain policy aspects.
Emphasizes the importance of advanced citizenship and thinking independently rather than blindly following a political figure.
Warns against falling into a cult of personality and urges individuals to judge ideas based on their merits, not their political origin.
Reminds Americans that representatives should follow the lead of the people, not the other way around.
Advocates for individuals to think independently, make informed decisions, and not blindly follow a political party.

Actions:

for republicans, independents,
Identify and agree with policies from opposing parties before November 2nd (suggested).
Think independently and judge ideas based on their merits (implied).
Encourage critical thinking and informed decision-making (implied).
</details>
<details>
<summary>
2020-09-26: Let's talk about Florida reopening.... (<a href="https://youtube.com/watch?v=-Q68nIHKPOw">watch</a> || <a href="/videos/2020/09/26/Lets_talk_about_Florida_reopening">transcript &amp; editable summary</a>)

Florida opens for tourists without restrictions, prioritizing the economy over safety, urging visitors to rely on medical experts, not politicians.

</summary>

"If you are from out of state, please, please, do not let anything a politician in Florida says ever influence any of your decisions about anything, ever."
"Listen to the medical experts, listen to the people that know what they're doing, not the guy who didn't know how to put on his mask."
"We don't need to become a nexus point for transmission."

### AI summary (High error rate! Edit errors on video page)

Florida is open for business with no restrictions, aiming to attract tourists despite the ongoing pandemic.
The decision to reopen is primarily driven by the need for economic stability and attracting visitors for financial reasons.
Politicians in Tallahassee have made decisions based on selective information to suit their agenda.
Beau dismisses concerns about Florida's reputation and handling of the pandemic, suggesting that the state is already considered poorly managed in this regard.
He mentions that the influx of tourists is seen as beneficial as they spend money and then return home, reducing the strain on Florida's healthcare system.
Beau mentions the large older population in Florida, referring to it as "God's waiting room," and downplays concerns about their vulnerability.
Capacity is expected to be an issue due to pent-up demand, but Beau assures that the crowded tourist areas are part of the appeal.
Beau advises people not to trust politicians' statements from Florida and instead rely on medical experts for guidance.
He urges visitors to make decisions based on expert advice rather than political influences.
Beau expresses his willingness for a slower reopening if it ensures the safety and well-being of the people in Florida.

Actions:

for travelers, concerned citizens,
Listen to medical experts for guidance on travel decisions (implied)
Prioritize safety and well-being over economic interests when considering travel plans (implied)
</details>
<details>
<summary>
2020-09-25: Let's talk about Trump, the Constitution, and the machinery for change.... (<a href="https://youtube.com/watch?v=d7Yh722gLZs">watch</a> || <a href="/videos/2020/09/25/Lets_talk_about_Trump_the_Constitution_and_the_machinery_for_change">transcript &amp; editable summary</a>)

The United States Constitution acknowledges imperfection and enshrines mechanisms for change, but Beau criticizes the Republican Party for attacking the machinery for change and undermining the people's voice.

</summary>

"You are always doing it wrong. There's never a right way to do it."
"They attack that machinery so they don't have to listen to the people."
"We're not going to listen to you. We're not going to give you a voice in your representative democracy."
"Your rights don't matter."
"Rather than protecting the machinery for change and encouraging its use, they're holding a lighter to the Constitution."

### AI summary (High error rate! Edit errors on video page)

The United States Constitution acknowledges imperfection and the need for change from the very first line of the document.
The Constitution enshrines mechanisms for change, including the right to petition for a redress of grievances and freedoms like speech, press, and assembly.
Elected officials are not as responsive as they should be, regardless of party affiliation.
The Republican Party is noted for attacking the machinery for change and methods of voicing concerns, such as suppressing the vote and criminalizing assembly and speech.
Republicans tend to ignore issues like climate, public health, the economy, and racism, pretending they don't exist when it suits them.
Republicans demonstrate a lack of interest in listening to the people by attempting to suppress the vote and undermine the integrity of elections.
Beau criticizes the Republican Party for abandoning their duty to represent the people and instead seeking to rule over them.
He questions why a party claiming to be for the working class and looking out for the little guy wouldn't want more people to vote.
Beau argues that by suppressing the vote, Republicans show they are not interested in having the consent of the governed and are focused on ruling rather than representing.
The duty of the government is to listen to the people, but Beau suggests the Republican Party has strayed from this principle.

Actions:

for voters, activists, concerned citizens,
Join organizations advocating for voting rights and fair elections (implied)
Contact elected officials to express support for protecting mechanisms for change (implied)
</details>
<details>
<summary>
2020-09-24: Let's talk about what Trump wouldn't say.... (<a href="https://youtube.com/watch?v=srPUsL3PQ5U">watch</a> || <a href="/videos/2020/09/24/Lets_talk_about_what_Trump_wouldn_t_say">transcript &amp; editable summary</a>)

President Trump's refusal to commit to a peaceful transfer of power is more critical than any campaign promise, urging citizens to ensure his defeat for the nation's security and soul.

</summary>

"When somebody tells you who they are, when they show you who they are, who they really are, you better believe them."
"He just showed the entire country who he is."
"That lack of a commitment is more critical than any campaign promise."
"The only way I know of to avoid that concern is to make sure he loses in a landslide."
"It's incredibly significant that it happens."

### AI summary (High error rate! Edit errors on video page)

President Donald J. Trump's alarming refusal to commit to a peaceful transfer of power is more critical than any campaign promise.
Trump's lack of commitment to a peaceful transferal of power should concern every citizen.
The nation's security and soul may hinge on ensuring Trump loses in a landslide.
Trump's refusal to pledge a peaceful transfer of power reveals his true character.
Beau urges everyone to pay attention to Trump's concerning actions and statements.
Trump's failure to commit to a peaceful transition raises doubts about his future behavior.
The importance of ensuring a peaceful transition of power transcends party lines and individual issues.
Trump's non-commitment to a peaceful transfer of power is a significant issue that should weigh on everyone's mind.
Beau stresses the need for Americans to recognize the gravity of Trump's refusal to commit to a peaceful transfer of power.
Beau concludes by encouraging people to stay vigilant and engaged.

Actions:

for american voters,
Ensure Trump loses in a landslide (implied)
Stay vigilant and engaged (implied)
</details>
<details>
<summary>
2020-09-24: Let's talk about Trump's comments and the Republican response.... (<a href="https://youtube.com/watch?v=3muoxysvKiw">watch</a> || <a href="/videos/2020/09/24/Lets_talk_about_Trump_s_comments_and_the_Republican_response">transcript &amp; editable summary</a>)

Republicans must hold the President accountable for his refusal to commit to a peaceful transfer of power to prevent further harm and uphold their duty as a hedge against executive tyranny.

</summary>

"Your man has to be held accountable for his words."
"The president needs to commit to this and you need to make him."
"A man whose words can start a war."
"You're proving it now."
"It is your obligation, your duty to stop it."

### AI summary (High error rate! Edit errors on video page)

Republicans are usually silent on the President's comments to avoid Twitter ridicule.
They broke their silence this time due to the seriousness of the President's statement.
The President refused to commit to a peaceful transfer of power when directly asked.
GOP leaders McCarthy and McConnell made vague assurances about a peaceful transfer.
Beau calls out Republicans for not holding the President accountable for his words.
He criticizes Republicans for defending and downplaying the President's refusal.
Beau questions whether Republicans will act if the President refuses to leave peacefully.
He points out Republicans' history of underestimating the President, leading to his election.
Beau stresses the importance of making the President commit to a peaceful transfer of power.
He reminds Republicans of their duty to act as a hedge against executive tyranny.
Beau condemns the senator from Nebraska for dismissing the President's behavior as "crazy stuff."
He argues that the President's words have serious consequences, impacting lives and national security.
Beau urges Republicans to stop underestimating the President's influence and take action.
He warns that if Republicans fail to act, they risk enabling further harm to the country.
Beau concludes by stating that it is the Republican Party's moral, legal, and ethical duty to pressure the President.

Actions:

for politically engaged citizens,
Pressure the President to commit to a peaceful transfer of power (suggested)
Hold elected officials accountable for upholding democracy (implied)
</details>
<details>
<summary>
2020-09-23: Let's talk about the Governor of Florida's proposed bill.... (<a href="https://youtube.com/watch?v=pZ5YbJAu2BA">watch</a> || <a href="/videos/2020/09/23/Lets_talk_about_the_Governor_of_Florida_s_proposed_bill">transcript &amp; editable summary</a>)

Beau criticizes Florida's legislation as a flawed attempt to mask issues rather than address them, especially failing to tackle the core problem of police violence.

</summary>

"The problem isn't that there's people in the streets. The problem is people feeling they have to be in the streets."
"The issue is folks getting killed by the cops while they're unarmed."
"This is the type of legislation that gets proposed by failed leaders in failed states."

### AI summary (High error rate! Edit errors on video page)

Beau is discussing legislation coming out of Florida proposed by Governor Ron DeSantis, critiquing it as a way to mask the core issues rather than address them.
The legislation includes a prohibition on disorderly assemblies of more than seven people and enhanced penalties for obstructing roadways, allowing vehicles to run over protestors, toppling monuments, and harassing people in public accommodations.
It adds a RICO liability, mandatory minimum jail sentences for striking an officer, and denies bail for certain offenses.
Local jurisdictions in Florida could lose state money if they defund the police, creating a victim compensation mechanism allowing residents to sue the government if it fails to protect them.
Beau criticizes the legislation for not addressing the real problem: police violence leading to unarmed individuals being killed.
He suggests implementing mandatory minimum sentences for police officers who shoot unarmed individuals and predicts the ACLU will challenge the unconstitutional aspects of the bill.
Beau believes that increasing police presence and violence to suppress speech about law enforcement will only fuel the movement further.
He concludes that the legislation is flawed, unconstitutional, and will likely be struck down, failing to address the underlying issue.

Actions:

for activists, civil rights advocates,
Challenge unconstitutional laws through legal action (implied)
Advocate for laws addressing police violence and accountability (implied)
</details>
<details>
<summary>
2020-09-23: Let's talk about a defunding declaration and an American idea.... (<a href="https://youtube.com/watch?v=Tg1ONaZDuho">watch</a> || <a href="/videos/2020/09/23/Lets_talk_about_a_defunding_declaration_and_an_American_idea">transcript &amp; editable summary</a>)

Beau questions the American identity in relation to defunding the police, drawing parallels to the Declaration of Independence and advocating for police accountability.

</summary>

"Defunding the police is as American as it gets."
"No taxation without accountability."
"Defunding the police is a just call, and it would be effective."
"They don't get money if we can't hold them accountable."
"It's literally one of the most American things you can say."

### AI summary (High error rate! Edit errors on video page)

Declaration on funding and American identity discussed due to news from Mitch McConnell's Kentucky.
Absence of progress, forward movement, reform, or justice apparent.
Social media calls for defunding Louisville prompt Beau's reflection on American values.
Beau questions the notion of defunding police being un-American.
Compares current policing issues to grievances in the Declaration of Independence.
No taxation without representation principle linked to police accountability.
Defunding the police viewed as a patriotic act rooted in American history.
Cutting police funding can prompt necessary priorities and accountability.
Defunding specific problem departments is advocated for improved accountability.
Politicians opposing defunding reveal their priorities lie elsewhere than with the people.

Actions:

for american citizens,
Advocate for defunding problem departments for improved police accountability (implied).
Prioritize holding law enforcement agencies accountable in your community (implied).
</details>
<details>
<summary>
2020-09-23: Let's talk about Greta's speech one year later.... (<a href="https://youtube.com/watch?v=_bL5BVW_nT0">watch</a> || <a href="/videos/2020/09/23/Lets_talk_about_Greta_s_speech_one_year_later">transcript &amp; editable summary</a>)

Beau revisits a dismissed speech, warning about the consequences of dismissing urgent messages and the need for preparation amidst looming challenges.

</summary>

"My message is that we'll be watching you."
"You've stolen my dreams and my childhood with your empty words."
"This issue that we're facing today, it's a dress rehearsal for what's coming around the bend."

### AI summary (High error rate! Edit errors on video page)

Addressing a speech given a year ago, dismissed due to various reasons like speaker's age and appearance.
Urges to revisit the message rather than dismissing it outright.
Quotes the impactful message: "My message is that we'll be watching you."
Speaker expresses frustration at being on stage instead of in school, accuses adults of stealing dreams and childhood.
Condemns the focus on money and economic growth while the planet suffers.
Criticizes the lack of action despite clear scientific evidence on climate change.
Draws parallels to public health issues and government's prioritization of profits over people's well-being.
Emphasizes that those in power often escape the consequences of their actions due to wealth and privilege.
States the relevance of the speech today and the need to learn from historical instances of downplaying crises for personal gain.
Urges preparation and action for the impending challenges, stressing that no borders or flags will offer protection.

Actions:

for activists, climate advocates,
Get involved in electoral politics to push for necessary changes (exemplified)
Prepare and insulate yourself for future challenges (exemplified)
</details>
<details>
<summary>
2020-09-22: Let's talk about expectations, justice, and Breonna Taylor.... (<a href="https://youtube.com/watch?v=KKq_ZEstxw0">watch</a> || <a href="/videos/2020/09/22/Lets_talk_about_expectations_justice_and_Breonna_Taylor">transcript &amp; editable summary</a>)

In a city preparing for expected events, the story is about the anticipation of extreme injustice, stressing governance consent and the need for reforms in the justice system.

</summary>

"The story is that the expectation is to see an injustice."
"Liberty and justice for all, right? Guess not."
"The expectation is that something else will happen."
"When the justice system in a country comes into question, either reforms get made or the country fails."

### AI summary (High error rate! Edit errors on video page)

Tense atmosphere as preparations are made in a city for expected events.
Plywood being put up as businesses prepare for potential unrest.
Anticipation of an extreme injustice leading to citizen reactions.
The story is about the expectation of injustice, not just physical preparations.
Doubt on the outcome of the LeBron and Taylor case in Louisville.
Indictment of the criminal justice system with the anticipation of injustice.
Lack of accountability and frequent incidents stressing governance consent.
Preparations by both businesses and law enforcement are seen as an indictment.
The expectation of justice for all is waning, replaced by anticipation of injustice.
Concern over how long a country can endure such expectations without reforms.

Actions:

for activists, reformers, concerned citizens,
Prepare for potential unrest by securing businesses and community spaces (exemplified)
Advocate for accountability and reforms in the criminal justice system (exemplified)
Stay informed and engaged in addressing systemic issues (implied)
</details>
<details>
<summary>
2020-09-22: Let's talk about Trump, leadership, standards, and bare minimums.... (<a href="https://youtube.com/watch?v=JqnSJ37mcrg">watch</a> || <a href="/videos/2020/09/22/Lets_talk_about_Trump_leadership_standards_and_bare_minimums">transcript &amp; editable summary</a>)

Beau criticizes setting the bar too low by using Trump as the sole standard for presidential candidates and advocates for higher standards in selecting public officials.

</summary>

"If we set the bar as better than Trump, we could end up with a candidate whose only qualifications are that they don't lie constantly, they don't engage in nepotism, don't engage in overt corruption..."
"Whether you think Biden has it or not, that's a decision you have to make."
"That's what I had to do with."
"We should set the standards pretty high."

### AI summary (High error rate! Edit errors on video page)

Criticizes praising Biden for fulfilling basic civic duties as setting the bar too low.
Warns against using Trump as the only standard for presidential candidates.
Stresses the importance of addressing systemic issues and not just superficial fixes.
Argues that the impact of Trump's presidency will be long-lasting regardless of the election outcome.
References Sun Tzu's characteristics of a good leader and contrasts them with Trump's qualities.
Emphasizes the need for competence, understanding of governance, and respect for others in a leader.
Points out Biden's willingness to listen to experts and take responsibility, contrasting with Trump's behavior.
Advocates for striving for higher standards in selecting public officials and government accountability.

Actions:

for voters, citizens,
Advocate for higher standards in selecting public officials (implied)
Strive for deep change in how candidates are evaluated and selected (implied)
Hold public officials accountable for competence and integrity (implied)
</details>
<details>
<summary>
2020-09-22: Let's talk about Biden, leadership, standards, and bare minimums.... (<a href="https://youtube.com/watch?v=Kl5mhG6SFV4">watch</a> || <a href="/videos/2020/09/22/Lets_talk_about_Biden_leadership_standards_and_bare_minimums">transcript &amp; editable summary</a>)

Beau addresses the importance of not settling for the bare minimum in leadership and the need to criticize those who fall short of meeting standards to cultivate real leaders.

</summary>

"You have to set the example."
"But we can't let that become the standard."
"Rather than heaping praise on somebody who does the bare minimum, we should criticize those who don't meet standards."
"If we stopped to clap every time Joe Biden displayed more leadership than the president, we wouldn't get anything done in this country."
"What Joe Biden is doing is standard."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the topic of presidential candidate Joe Biden's leadership, standards, and meeting bare minimums.
People reached out to Beau with messages about Biden wearing a mask during a speech.
Biden followed a local ordinance by wearing a mask throughout his speech, setting an example.
Beau acknowledges the positive attributes of Biden following the ordinance and displaying leadership.
Despite appreciating Biden's actions, Beau points out that wearing a mask is the bare minimum expected in the current climate.
Beau argues that praising Biden for meeting the basic standard shouldn't be the norm but rather critiquing those who fall short of meeting standards.
He suggests that rather than praising those who do the bare minimum, criticism should be directed towards those who don't meet standards to foster real leadership.
Beau expresses the importance of not allowing Trump's lack of leadership to become the benchmark for judging other candidates.
Biden's actions are viewed as standard behavior, and Beau stresses the need for real leaders in office.
Beau, a strong critic of Biden, believes that applauding minimal displays of leadership shouldn't impede progress in the country.

Actions:

for voters, political observers,
Criticize those who fall short of meeting standards (implied)
Advocate for real leaders in office by holding candidates accountable to higher standards (implied)
</details>
<details>
<summary>
2020-09-19: Let's talk about electoral politics and Ruth Bader Ginsburg.... (<a href="https://youtube.com/watch?v=edRE7KNayPI">watch</a> || <a href="/videos/2020/09/19/Lets_talk_about_electoral_politics_and_Ruth_Bader_Ginsburg">transcript &amp; editable summary</a>)

One person's loss underscores the peril of concentrated power and the need for diverse strategies to reclaim power and protect rights.

</summary>

"There is too much power in too few hands."
"The problem is, I'm afraid it's going to be lost among the tragedy and the upheaval."
"It's two buses headed to the same spot."
"We need people fighting a holding action."
"We have got to limit the power. We have to disperse it at the very least."

### AI summary (High error rate! Edit errors on video page)

One person's loss raised concerns for 160 million Americans about their rights being imperiled.
There is too much power concentrated in too few hands, leading to potential jeopardy for many.
The tragedy and upheaval overshadow the critical issue of power distribution.
The current events served as a powerful illustration of a political compass point made earlier.
People on the bottom left of the political spectrum are engaging in debates despite a shared direction.
The divide lies between those heavily involved in electoral politics and those advocating for tangible actions for change.
The recent impact underscores the importance of electoral politics while also acknowledging the limitations of voting.
Building a separate power structure and engaging in electoral politics can coexist as complementary strategies.
The need for a diversity of tactics and an understanding between different political approaches.
Collaboration and efforts to reclaim power are necessary to prevent further rights jeopardization.

Actions:

for activists, voters, organizers,
Collaborate with local organizations to strengthen community networks (implied)
Engage in electoral politics to influence immediate tangible results (implied)
Advocate for a diversity of tactics to reclaim power and prevent authoritarian creep (implied)
</details>
<details>
<summary>
2020-09-19: Let's talk about Trump doing things without delay.... (<a href="https://youtube.com/watch?v=Zvwa9_ppJts">watch</a> || <a href="/videos/2020/09/19/Lets_talk_about_Trump_doing_things_without_delay">transcript &amp; editable summary</a>)

President rushes Supreme Court appointment without proper vetting, Senate complies, prioritizing power over duty to the people.

</summary>

"You're willing to undermine the Constitution and do whatever that man in the Oval Office says as long as you can get his crumbs."
"When it comes to helping out the little guy, you don't seem to care very much."
"Y'all are incredibly active. But when it comes to helping out the little guy, you don't seem to care very much."
"Don't think we don't notice that when it comes to shoring up your power, all of a sudden, y'all are ready to go."
"It's crystal clear. You're willing to undermine the Constitution and do whatever that man in the Oval Office says as long as you can get his crumbs."

### AI summary (High error rate! Edit errors on video page)

President of the United States pushing for a quick appointment without delay and Senate is willing to oblige.
President has a history of hiring individuals, praising them, then firing and criticizing them publicly.
Beau questions the lack of thorough vetting for a lifetime appointment to the Supreme Court.
Criticizes Senate for potentially forsaking their constitutional obligation by rushing the appointment.
Calls out Senate Republicans for prioritizing power consolidation over the well-being of the people.
Suggests that Senate is too eager to please the President at the expense of upholding their duties as a separate branch of government.
Points out the hypocrisy of Senate's swift action for power-related matters compared to issues affecting the general population.
Implies that Senate Republicans' actions are noticed and may influence public opinion and decision-making.

Actions:

for activists, concerned citizens,
Hold elected officials accountable for their actions and decisions (implied)
Educate others about the importance of a balanced government and the responsibilities of each branch (implied)
</details>
<details>
<summary>
2020-09-19: Let's talk about Ruth Bader Ginsburg and a message to Senators.... (<a href="https://youtube.com/watch?v=a9QL5Z2VS5Y">watch</a> || <a href="/videos/2020/09/19/Lets_talk_about_Ruth_Bader_Ginsburg_and_a_message_to_Senators">transcript &amp; editable summary</a>)

Beau warns Republican senators against hastily confirming a Supreme Court nominee tied to Trump's failures, urging them to take a moral stance and let the American people decide.

</summary>

"The American people should have a voice in the selection of their next Supreme Court justice."
"I'm not sure that this close to the election, tying yourselves to such a failure of a president is a good idea if you're a Republican."
"Which Republicans are going to be in that race to be the first ones to say they're not going to do it?"
"Because, man, that's a moral stance."
"And with all of the behavior the Republican Party has been associated with lately, taking a moral stance right before the election, that might be a good move."

### AI summary (High error rate! Edit errors on video page)

Ruth Bader Ginsburg has died, and the President will likely push through a Supreme Court nominee close to the election.
President Trump believes the nominee will be loyal to him in any decisions regarding the election.
Beau recalls Senator McConnell's statements in 2016 advocating for the American people to have a voice in the selection of the next Supreme Court justice.
McConnell's role in pushing through a nominee is emphasized, noting the potential impact on closely contested Senate races.
Beau raises concerns about a potential nominee on Trump's list who advocated for American soldiers to shoot unarmed citizens.
Republican senators may face backlash from voters if they support a nominee tied to Trump's failures.
Beau questions the wisdom of Republican senators closely associating themselves with Trump so close to the election.
Democrats may only need a few Republican senators to halt the nomination process before the election.
Beau urges smart Republicans to take a moral stance and let the American people decide on the Supreme Court vacancy.
Democrats have obstructionist tactics at their disposal, but Beau hopes Republicans will act ethically without needing such tactics.

Actions:

for senators,
Hold off on confirming a Supreme Court nominee before the election (implied).
Smart Republicans should take a moral stance and prioritize the voice of the American people in selecting the next Supreme Court justice (exemplified).
</details>
<details>
<summary>
2020-09-18: Let's talk about Trump's patriotic education idea.... (<a href="https://youtube.com/watch?v=Yqcu6C94j90">watch</a> || <a href="/videos/2020/09/18/Lets_talk_about_Trump_s_patriotic_education_idea">transcript &amp; editable summary</a>)

President's patriotic education program is doomed to fail as true patriotism involves questioning the government and defending a way of life, not teaching mythology.

</summary>

"Patriotism is of its nature defensive, both militarily and culturally."
"Patriots do not obey no matter what, that's nationalists."
"Just because you say something over and over again and people believe it doesn't mean that it's true."
"Independence is my happiness, the world is my country, and my religion is to do good."
"To make America great, that's how you do it. That was what moved people."

### AI summary (High error rate! Edit errors on video page)

President wants a patriotic education program, but Beau believes it's a faulty premise.
Quotes Thomas Paine, the founder of the American Revolution, to illustrate true patriotism.
Thomas Paine's writings like "Common Sense" inspired rebellion and gave people something to believe in.
Beau contrasts patriotism with nationalism through George Washington and Nathan Hale's quotes.
Real patriots correct their government and defend a particular way of life, not seeking power or prestige.
Patriotism is defensive and earned, not taught through indoctrination or mythology.
Teaching only positive aspects of American history won't create patriots who question their government.
To make America great, Beau suggests living up to the promises of past patriots like Thomas Paine.
Real patriots question the government and strive to correct wrongs in society.
Beau believes the president's patriotic education program is doomed to fail if it ignores critical aspects of history.

Actions:

for educators, policymakers, activists,
Question the government's actions and policies (implied)
Defend a particular way of life that upholds values of true patriotism (implied)
</details>
<details>
<summary>
2020-09-17: Let's talk about Trump, blue and red states, and being alone.... (<a href="https://youtube.com/watch?v=AElcnUgPkeQ">watch</a> || <a href="/videos/2020/09/17/Lets_talk_about_Trump_blue_and_red_states_and_being_alone">transcript &amp; editable summary</a>)

President Trump's divisive tactics and misleading statements about red and blue states aim to shift blame, but unity and shared values among Americans prevail.

</summary>

"He has turned the country against itself."
"This country isn't that divided."
"Politicians do everything they can to make us put party over country."

### AI summary (High error rate! Edit errors on video page)

President Trump made a statement about red and blue states, aiming to deflect blame for mishandling public health issues.
Trump's comment suggested that without the blue states, the toll of the pandemic in the U.S. might be lower, but this is misleading.
Initially, 53% of COVID-19 deaths were in blue states, but red states caught up and by July, 70% of deaths were in red states.
The idea that blue states are somehow worse is not true; they were hit first due to being more populous.
Trump's handling of the pandemic has been criticized, and even without blue states, the U.S. death toll is significant.
The division between red and blue states is not as clear-cut as it seems, with many states having diverse political leanings within.
Trump's tactics of dividing Americans and fueling fear and hate to boost his re-election chances are condemned.
Despite political affiliations, the average American does not harbor hate towards each other; politicians are blamed for promoting division.
People in conservative areas are reminded that there are like-minded individuals around, and speaking up can provide support to others feeling isolated.

Actions:

for americans, community members,
Reach out and connect with like-minded individuals in your community (implied)
Speak up to show support and let others know they are not alone (implied)
</details>
<details>
<summary>
2020-09-17: Let's talk about Republicans calling to jail Trump.... (<a href="https://youtube.com/watch?v=7QuPPE-3_ak">watch</a> || <a href="/videos/2020/09/17/Lets_talk_about_Republicans_calling_to_jail_Trump">transcript &amp; editable summary</a>)

Beau calls for accountability and uniform application of standards in response to Republican criticisms of downplaying data in Nashville.

</summary>

"Any politician like the Nashville Mayor John Cooper, who intentionally covers up data on public health deaths in order to keep restaurants and bars closed should be removed from office and tried immediately." - Charlie Kirk
"The dim mayor of Nashville knowingly lied about data to justify shutting down bars and restaurants, killing countless jobs and small businesses in the process. Everyone involved should face jail time." - Donald Trump Jr.

### AI summary (High error rate! Edit errors on video page)

Beau introduces news from Nashville involving emails surfacing about downplaying data connected to the mayor's office.
Emails suggest a decision to not highlight the lack of spread in bars and restaurants in Nashville.
Republicans have criticized this decision, focusing on downplaying data leading to keeping bars closed.
Beau hints at missing context, possibly related to similar events in Florida and Texas with Republican governors.
Beau shifts the focus to Republican reactions, quoting Charlie Kirk and Donald Trump Jr. advocating for consequences for downplaying data.
Beau challenges the Republican stance, suggesting accountability should also extend to the President if downplaying data is a punishable offense.
Beau questions whether these statements are genuine concerns or simply political rhetoric.
Beau insists that if downplaying data is a punishable offense, it should be applied uniformly.
Beau calls for accountability and application of standards across the board in such cases.
Beau stresses the importance of upholding the law and ensuring accountability for all public officials.

Actions:

for public officials,
Hold public officials accountable for downplaying data. (implied)
Advocate for uniform application of standards in similar situations. (implied)
</details>
<details>
<summary>
2020-09-16: Let's talk about understanding the question you're being asked.... (<a href="https://youtube.com/watch?v=gr1OZeISqLk">watch</a> || <a href="/videos/2020/09/16/Lets_talk_about_understanding_the_question_you_re_being_asked">transcript &amp; editable summary</a>)

Beau stresses the importance of understanding questions, building community networks, and the critical role they play in emergencies like fires and hurricanes.

</summary>

"That question doesn't mean what we think it means."
"All that matters is that commitment to that goal."
"Don't focus too much on the production value, but focus on the content."
"These community networks are incredibly important."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Recording on the edge of a storm due to heavy rain preventing recording in his shop with a tin roof.
Talks about the importance of understanding the question you're being asked, which he didn't grasp for years.
Shares his experience of helping people build community networks and the common query from younger individuals on how to start.
Explains that younger generations, being more technology-based, struggle with the concept of building core groups in community networks.
Mentions stumbling upon a video by Working Stiff USA that addresses this issue effectively.
Emphasizes that the starting core group can be anybody from various walks of life, not necessarily close friends.
Assures that personal connections don't have to happen immediately; commitment to the common goal is what matters.
Points out that as networks grow, they naturally split into groups where personal connections form.
Recommends a YouTube video (by Working Stiff USA) that focuses on building the initial core group for community networks.
Stresses the importance of community networks, especially during emergencies like fires and hurricanes.

Actions:

for community builders and emergency responders.,
Reach out to diverse individuals in your community to form the initial core group (exemplified).
Prioritize commitment to common goals over immediate personal connections when building community networks (exemplified).
Watch the suggested YouTube video by Working Stiff USA to learn more about starting community networks (suggested).
</details>
<details>
<summary>
2020-09-16: Let's talk about Biden, Trump, Scientific American, and a concern.... (<a href="https://youtube.com/watch?v=qbLBFyBBMD8">watch</a> || <a href="/videos/2020/09/16/Lets_talk_about_Biden_Trump_Scientific_American_and_a_concern">transcript &amp; editable summary</a>)

Scientific American's endorsement of Biden doesn't signal enlightenment but rather indicts Trump; Beau warns that a Biden win is just the beginning of the fight for change and justice.

</summary>

"A Biden victory isn't the end of the fight. It's the bugle sounding the charge."
"If you want to change this country, if you want to create a fair and just system, if you want to have liberty and justice for all, the fight doesn't end when he takes office."

### AI summary (High error rate! Edit errors on video page)

Scientific American endorsed Joe Biden for president after 175 years of not endorsing anyone.
The endorsement seems more like an indictment of President Trump than praise for Biden.
Beau is concerned that people may view a Biden win as the end of the fight, when in reality, it's just the beginning.
He believes that Biden's victory will be about damage control, not a total victory for the little guy.
Beau stresses that the fight for change, equity, and justice in the US doesn't end with a Biden presidency.
Trump didn't create the problems in the country; he just brought them into the limelight through his incompetence and corruption.
Beau calls on individuals, especially those at the bottom, to stay active and push for necessary change.
He sees a Biden victory as a call to action, where people need to seize the initiative and work towards creating the change needed in the country.
Beau points out that the problems Trump showcased have always existed but were not as visible to many.
He concludes by reminding everyone that a Biden win is not the ultimate victory but a prompt to keep pushing for a fair and just system in the US.

Actions:

for activists, voters,
Seize the initiative to create necessary change (implied)
</details>
<details>
<summary>
2020-09-15: Let's talk about the Pledge of Allegiance... (<a href="https://youtube.com/watch?v=EIz0C8r4ERc">watch</a> || <a href="/videos/2020/09/15/Lets_talk_about_the_Pledge_of_Allegiance">transcript &amp; editable summary</a>)

Beau suggests refocusing the Pledge of Allegiance on universal values of liberty and justice for all, transcending borders and symbols.

</summary>

"I pledge allegiance to liberty and justice for all."
"Not just does it better embody things, not just is it more concise, it's universal."
"Gives us something to actually fight for, something worth fighting for."

### AI summary (High error rate! Edit errors on video page)

Explains the historical revisions of the Pledge of Allegiance from 1892 to the current version in 1954.
Talks about the importance of being concise in writing as a journalist.
Emphasizes the need to eliminate irrelevant details to convey the main idea effectively.
Suggests that focusing on the core values of liberty and justice for all is more impactful than pledging allegiance to a symbol.
Advocates for a revised pledge that simply states "I pledge allegiance to liberty and justice for all" to represent the true essence of the idea.
Points out that liberty and justice should be universal principles, transcending borders and applying to everyone.
Encourages prioritizing ideas over symbols for a better world.
Concludes with a reflection on the potential for positive change if people prioritize values over symbols.

Actions:

for journalists, educators, activists,
Advocate for universal values of liberty and justice in your community (suggested)
Prioritize fighting for equitable rights for all individuals (exemplified)
</details>
<details>
<summary>
2020-09-15: Let's talk about Trump's climate conversation and curiosity.... (<a href="https://youtube.com/watch?v=66ZVpQ1ISZk">watch</a> || <a href="/videos/2020/09/15/Lets_talk_about_Trump_s_climate_conversation_and_curiosity">transcript &amp; editable summary</a>)

Beau criticizes the president's ignorance on climate change and advocates for leaders to prioritize continuous learning and curiosity.

</summary>

"He doesn't know the difference between climate and weather."
"Any president that is not remarkably better educated by the end of their first term doesn't deserve a second one."
"It's an embarrassment to the United States to have a man like this sitting in the Oval Office."

### AI summary (High error rate! Edit errors on video page)

President's response to climate change concerns: "it'll start getting cooler, just you watch."
President's lack of belief in science: "I don't think science knows actually."
Beau criticizes recent presidents for lack of curiosity and thirst for knowledge.
Beau suggests a daily two-hour discretionary briefing for the president to learn about any topic of interest.
Beau envisions the president engaging with experts from diverse fields during these briefings.
Criticism of leaders' lack of curiosity and tendency to dismiss education.
Beau points out the danger in leaders not seeking more knowledge.
President's ignorance on climate versus weather is concerning.
Beau argues that a president who does not actively seek education does not deserve a second term.
Critique on leaders who stop learning and refuse new ideas.
Beau expresses embarrassment over having an ignorant president in office.
Call for leaders to continuously learn and grow intellectually.

Actions:

for citizens, voters, activists,
Contact local representatives to advocate for leaders who prioritize education and curiosity (implied).
</details>
<details>
<summary>
2020-09-14: Let's talk about the US as a failed state.... (<a href="https://youtube.com/watch?v=AjtzXryZ-7o">watch</a> || <a href="/videos/2020/09/14/Lets_talk_about_the_US_as_a_failed_state">transcript &amp; editable summary</a>)

Is the United States becoming a failed state due to erosion of legitimate authority, loss of control on physical force, failure in public services, and violations of international law?

</summary>

"Society's beliefs help shape the law."
"It's up to you as an individual to help change this."
"The national level has become too corrupt."
"The United States is failing as it benefits those in power temporarily."
"People just out for themselves and failing in their obligations as stewards of the state."

### AI summary (High error rate! Edit errors on video page)

Exploring if the United States is behaving as a failed state due to erosion of legitimate authority to make collective decisions.
Government's failure to obtain consent of the governed through polarizing society and not basing support on ideas and policies.
Society's belief in enforcing laws rather than just writing them down on paper.
Loss of control on the legitimate use of physical force by the government.
Society's view on excessive force by law enforcement and its impact on legitimacy.
Inability of the government to provide public services like clean water and healthcare.
The United States' violations of international law and isolationist tendencies.
The importance of individual action in shaping society's beliefs and laws.
Encouraging individuals to be active and involved at the community level.
The government's focus on self-benefit and division of society for their advantage.

Actions:

for activists, concerned citizens,
Be active and involved in your community to shape society's beliefs (implied)
Advocate for policies based on ideas rather than parties (implied)
Support initiatives that aim to provide public services (implied)
</details>
<details>
<summary>
2020-09-13: Let's talk about raising your kids and the moral, ethical, and legal.... (<a href="https://youtube.com/watch?v=wYCYh6xpxcs">watch</a> || <a href="/videos/2020/09/13/Lets_talk_about_raising_your_kids_and_the_moral_ethical_and_legal">transcript &amp; editable summary</a>)

Beau advises discussing ideas and policies, humanizing historical figures, and teaching children to classify values as legal, ethical, and moral to foster critical thinking.

</summary>

"Teach them history, not mythology."
"Legal, ethical, and moral. These three things aren't the same."
"Morals are what shapes ethics and ethics are what shapes the law."
"Talk about policy and ideas. Talk about moral, ethical, and legal."
"You don't want to create little ideological foot soldiers."

### AI summary (High error rate! Edit errors on video page)

Advises on discussing philosophy and politics with children to avoid creating ideological followers.
Suggests discussing ideas and policies instead of focusing on individuals or parties.
Encourages humanizing historical figures to prevent blind hero-worship and cult of personality.
Differentiates between legal, ethical, and moral values to guide children in decision-making.
Uses examples like slavery in the 1850s to illustrate the distinctions between legal, ethical, and moral standards.
Emphasizes the importance of teaching children to develop their own values based on critical thinking.
Warns against sensationalizing historical events and emotions when discussing sensitive topics.
Urges to avoid personalizing historical figures or events and focus on analyzing ideas and policies.
Stresses the significance of helping children think independently and critically about moral, ethical, and legal dilemmas.
Recommends focusing on policy, ideas, and values rather than individuals or rigid ideologies to cultivate independent thinking.

Actions:

for parents, educators, caregivers,
Teach children about historical events factually and without sensationalizing (implied).
Encourage children to think critically about values and decisions based on legal, ethical, and moral considerations (implied).
Engage children in discussing policies and ideas rather than focusing on individuals or parties (implied).
</details>
<details>
<summary>
2020-09-12: Let's talk about when progressives will be satisfied.... (<a href="https://youtube.com/watch?v=WEHM5Cegmko">watch</a> || <a href="/videos/2020/09/12/Lets_talk_about_when_progressives_will_be_satisfied">transcript &amp; editable summary</a>)

Beau asks when those seeking change will be satisfied, explains the absence of new conservative ideas, and advocates for continuous efforts towards progress and justice.

</summary>

"When will those seeking change, progress, and justice be satisfied?"
"There are no new conservative ideas."
"New ideas come from the progressive side."
"It doesn't end. Ever."
"Do what you can, when you can, where you can, for as long as you can."

### AI summary (High error rate! Edit errors on video page)

Talking about when people will be satisfied, when it'll be enough, and when it will stop.
Exploring when those seeking change, progress, and justice will be satisfied.
There are no new conservative ideas, but there may be new ways to package them.
Conservatives typically defend old progressive ideas.
The Republican Party, known as the party of Lincoln, was actually progressive.
Lincoln received fan mail from Karl Marx, showing a connection between progressive ideas and conservatism.
New ideas tend to come from the progressive side, while conservatives try to hold things back.
When will people be satisfied? It doesn't end; there will always be new injustices.
People asking when others will be satisfied are often wondering what the minimum acceptable change is.
Beau suggests doing what you can, when you can, where you can, for as long as you can to make a difference.

Actions:

for activists, changemakers, advocates,
Do what you can, when you can, where you can, for as long as you can (suggested)
</details>
<details>
<summary>
2020-09-10: Let's talk about blaming Bob Woodward.... (<a href="https://youtube.com/watch?v=WHY1YEvH-d8">watch</a> || <a href="/videos/2020/09/10/Lets_talk_about_blaming_Bob_Woodward">transcript &amp; editable summary</a>)

President's attempt to blame Woodward falls flat as everyone already knew, showcasing Woodward's contrast between public and private knowledge while underscoring the president's failure to prioritize American lives over his own approval ratings.

</summary>

"Everybody knew back then that the president was aware of this."
"He informed the American people and he's giving them a choice."
"He actively undermined their work and it cost tens of thousands of American lives."
"His job, his form of journalism, what he does is exactly what he did."
"Let's just hope that next year there are people who will listen to the experts."

### AI summary (High error rate! Edit errors on video page)

President's attempt to blame Bob Woodward for his actions.
Everyone already knew that the president was aware of the information.
Woodward's effectiveness lies in showing the contrast between what was known privately and publicly.
Americans were already aware of the information that Woodward revealed.
Woodward informs the American people and gives them a choice for the future.
The president did not let the experts do their job, actively undermining their work.
Woodward's work demonstrates the president's prioritization of his approval rating over American lives.
Had Woodward released the information earlier, it wouldn't have had the same impact.
The president's base might have dismissed the information as just an offhand comment.
Woodward's form of journalism is exactly what he did, and blaming him is not fair.

Actions:

for american citizens,
Listen to the experts and prioritize public health over personal gain (implied).
Support investigative journalists like Bob Woodward who inform the public and hold leaders accountable (implied).
</details>
<details>
<summary>
2020-09-10: Let's talk about Trump's admission of downplaying.... (<a href="https://youtube.com/watch?v=HG7yZ_4SBLQ">watch</a> || <a href="/videos/2020/09/10/Lets_talk_about_Trump_s_admission_of_downplaying">transcript &amp; editable summary</a>)

President admitted to downplaying COVID-19, fear-mongering, and creating panic, leading to significant consequences and loss of life.

</summary>

"He assumed the American people would panic because he did."
"What, almost 200,000 gone because of his indecisiveness."
"He froze. Like every other wannabe tough guy."
"He's not a cheerleader for the country."
"The president's whole MO is fear mongering and creating panic."

### AI summary (High error rate! Edit errors on video page)

President admitted to downplaying COVID-19, as confirmed by Bob Woodward's tapes.
Trump tried to justify downplaying by claiming he didn't want to cause panic.
Beau criticizes Trump for his fear-mongering tactics on various issues.
Trump's response to the public health crisis was indecisive and ultimately harmful.
Beau points out Trump's hypocrisy in not addressing the seriousness of the pandemic.
Trump's pattern of fear-mongering is contrasted with his handling of the COVID-19 situation.
Beau suggests that Trump's inaction and indecisiveness led to significant loss of life.
The president's failure to lead effectively in the face of crisis is criticized.
Beau questions Trump's claim of being a cheerleader for the country.
The transcript ends with Beau expressing disbelief at Trump's handling of the pandemic.

Actions:

for voters, concerned citizens,
Hold elected officials accountable for their actions (suggested)
Advocate for transparency and honesty in leadership (implied)
</details>
<details>
<summary>
2020-09-10: Let's talk about President Trump's attempts to change the story.... (<a href="https://youtube.com/watch?v=Lbix0KEWbmU">watch</a> || <a href="/videos/2020/09/10/Lets_talk_about_President_Trump_s_attempts_to_change_the_story">transcript &amp; editable summary</a>)

President's deflective tactics fail to distract from his deceit in handling the public health crisis, costing American lives.

</summary>

"President's willful deceit cost tens of thousands of American lives."
"This other stuff, who he might suggest for the Supreme Court, you know, a possible withdrawal, that's not news."
"The news is a bunch of bags that are full that didn't have to be."

### AI summary (High error rate! Edit errors on video page)

President's attempts at deflection amid public health crisis reveal his deceit.
American people not falling for distractions like Afghanistan withdrawal or Supreme Court suggestions.
President's ineffective leadership led to more lives lost in months than in 20 years of armed conflict.
Tom Cotton's suggestion to use US Army for restoring order raises concerns about his Supreme Court nomination.
Advocating no-quarter reveals troubling mindset about extrajudicial execution of American citizens.
Woodward's release of audio exposes President's downplaying of public health crisis, costing American lives.

Actions:

for american citizens,
Call for accountability from leaders regarding handling of public health crises (implied)
</details>
<details>
<summary>
2020-09-09: Let's talk about Trump, Woodward, and bad days.... (<a href="https://youtube.com/watch?v=_kolaHLYExw">watch</a> || <a href="/videos/2020/09/09/Lets_talk_about_Trump_Woodward_and_bad_days">transcript &amp; editable summary</a>)

Beau points out President Trump's failure to act during the pandemic, resulting in needless deaths and economic repercussions, questioning his suitability for future crises.

</summary>

"He did nothing."
"We could have mitigated this."
"He never should have been in that role."

### AI summary (High error rate! Edit errors on video page)

Defines the role of a security consultant as being paid for what you can do on the days when your client has a bad day, needing help, advice, and leadership.
Draws parallels between the role of a security consultant and that of the President of the United States, as someone who should be called upon during collective bad days.
Points out that the President knew early on about asymptomatic transmission and should have advocated for masks but chose to downplay it.
Mentions that the President was aware of the deadliness of the virus, comparing it to being five times as deadly as the most severe flu.
Criticizes the President for failing in leadership, passing on information, and actively working against the American people during the pandemic.
States that the President failed to act, which resulted in tens of thousands of needless deaths that could have been mitigated.
Emphasizes that taking responsibility for the failure to act translates to accepting responsibility for the unnecessary loss of lives.
Suggests that had the President acted differently, lives could have been saved, and the economy could have been better off.
Condemns the President as unqualified for his role and unfit to handle future crises effectively.
Expresses concern about the potential for more bad days in the future and does not want someone who failed at this level to be relied upon.

Actions:

for american citizens,
Contact elected officials to demand accountability and action (suggested)
</details>
<details>
<summary>
2020-09-09: Let's talk about Trump, Biden, civics, and funding the police.... (<a href="https://youtube.com/watch?v=qawVdpmN-hw">watch</a> || <a href="/videos/2020/09/09/Lets_talk_about_Trump_Biden_civics_and_funding_the_police">transcript &amp; editable summary</a>)

President Trump's claims about Joe Biden defunding police lack merit; federal funding is a small portion of what local departments rely on.

</summary>

"Either President Trump just does not understand basic civics or he is intentionally misleading people to scare them."
"Joe Biden defunding the police should not be a concern. It's not a thing."
"At the local level, somehow fund your local police department. The feds don't foot that bill."

### AI summary (High error rate! Edit errors on video page)

Addresses President Trump's repeated claim that Joe Biden will defund the police, leading to chaos.
Breaks down the funding of police departments, showcasing various grant programs and assistance.
Mentions the COPS program with a $400 million budget, Edward Byrne Memorial Justice Assistance Grant program with $264 million, and DHS Preparedness Grant program with a budget of $450 million.
Explains the Department of Defense's program 1033, which provides old military equipment to law enforcement.
Calculates the total funding from these sources to be $2.6 billion, enough to run the NYPD for three months.
Points out that federal funding is a small percentage of what state and local officials spend on law enforcement.
Concludes that whether intentionally misleading or lacking understanding, President Trump's emphasis on defunding police by Biden is not a significant concern for voters.

Actions:

for voters, civics learners,
Contact local officials to understand how your community funds its police department (implied)
</details>
<details>
<summary>
2020-09-08: Let's talk about whether a K-shaped recovery is Trump's fault.... (<a href="https://youtube.com/watch?v=IlkWVIt6uXo">watch</a> || <a href="/videos/2020/09/08/Lets_talk_about_whether_a_K-shaped_recovery_is_Trump_s_fault">transcript &amp; editable summary</a>)

Economists debate shapes of economic recovery, with a K-shaped one widening income inequality, as the wealthy benefit while service industries suffer, unlikely to see relief under the current administration.

</summary>

"The American dream will get further and further away because those at the top are going to be exploiting this economy."
"Relief for those at the bottom may be unlikely as the Trump administration is keen on portraying a full economic recovery."

### AI summary (High error rate! Edit errors on video page)

Economists have been discussing the different shapes of economic recovery: V, W, U, and now K.
A K-shaped recovery indicates different sectors of the economy performing differently.
The sectors going up in the K-shaped recovery include tech stocks and real estate, benefiting the wealthy.
Sectors like service industries and hospitality, employing hourly workers, are heading down in the K-shaped recovery.
The disconnect arises from the president's positive economic narrative not matching the reality experienced by many.
This economic trend is likely to worsen income inequality and the wealth gap in the country.
The top 1% in the US own 50% of the sectors that are improving, further exacerbating economic disparities.
Relief for those at the bottom may be unlikely as the Trump administration is keen on portraying a full economic recovery.
The lack of unified response and prolonged closures in certain industries contributed to the economic downturn.
While not solely caused by Trump, his administration's actions, or lack thereof, exacerbated the situation.
The American dream may become more elusive as economic exploitation by the wealthy increases.
The current political landscape may not provide much relief for the working class, urging them to adapt and prepare for challenging times.

Actions:

for working-class individuals,
Prepare for challenging times by securing your finances and making necessary adjustments (implied)
Stay informed about economic trends and policies affecting you and your community (implied)
</details>
<details>
<summary>
2020-09-08: Let's talk about Trump, generals, and defending the MIC.... (<a href="https://youtube.com/watch?v=egU80_HoRkQ">watch</a> || <a href="/videos/2020/09/08/Lets_talk_about_Trump_generals_and_defending_the_MIC">transcript &amp; editable summary</a>)

President Trump's comments on the military-industrial complex expose a deeper issue: undue influence from defense contractors, urging voters to take action by holding accountable those in Congress and the executive branch.

</summary>

"It's these campaign contributions. It's the undue influence that Eisenhower talked about. That's the problem, not a standing army."
"The offenders aren't on the E-ring. They're on Capitol Hill."
"The real issue is the undue influence of campaign contributions on policy-making."
"The root of the problem lies with Congress, the President, and the Secretary of Defense, not the military itself."
"Americans are more than happy to buy this fiction."

### AI summary (High error rate! Edit errors on video page)

President Donald J. Trump's comment stirred mixed reactions regarding the military and defense contractors.
The term "military industrial complex" was mentioned, pointing out a longstanding issue.
The misconception lies in associating the complex with generals and not defense contractors.
Generals, lacking monetary power, do not drive policy; it's influenced by defense contractors.
These contractors use profits to sway policy by lobbying Congress and the executive branch.
Policy decisions are ultimately made by Congress and the President, not generals.
The system perpetuates itself as voters fail to hold accountable those involved in the complex.
The real issue is the undue influence of campaign contributions on policy-making.
Beau challenges people to take responsibility by voting out those perpetuating the complex.
The root of the problem lies with Congress, the President, and the Secretary of Defense, not the military itself.

Actions:

for voters, activists,
Vote out officials perpetuating the military-industrial complex (implied)
Hold accountable Congress and the President through active participation in elections (implied)
</details>
<details>
<summary>
2020-09-08: Let's talk about Trump, $100 million, and transactions.... (<a href="https://youtube.com/watch?v=yBlAL8pP91k">watch</a> || <a href="/videos/2020/09/08/Lets_talk_about_Trump_100_million_and_transactions">transcript &amp; editable summary</a>)

Trump's potential campaign investment reveals his transactional nature, aiming to buy support with a hundred million dollars from the American taxpayer.

</summary>

"Everything is about what's in it for Trump."
"What is he trying to buy? And the answer is pretty clear. You."
"He's trying to buy you, the American taxpayer."
"Why does a man with no principles and no policy want access to the Oval Office?"
"The question isn't where's the money coming from? The question is, what is it buying?"

### AI summary (High error rate! Edit errors on video page)

Trump is reportedly considering investing a hundred million dollars of his own money into his campaign, sparking various questions and speculations.
Media suggests that Trump's potential investment shows his campaign's financial struggles compared to Biden's fundraising success.
Questions arise about the source of Trump's money, the authenticity of his claims, and his true wealth status.
Despite the focus on financial aspects, the critical point is Trump's transactional nature and lack of guiding principles.
Trump operates solely based on personal gain, treating everything as a deal with a "what's in it for me" mentality.
This transactional approach extends to his foreign policy, relationships with governors, and understanding of soldiers.
Speculations about the motives behind Trump's investment include seeking influence, directing money to his businesses, and buying support.
The big question isn't the source of the money but what Trump aims to purchase with it – likely the American taxpayer's support.
Trump's track record shows a lack of policy implementation benefiting the general population, focusing on rewarding friends and elites.
The core concern is why a leader with no principles or policies seeks access to power, potentially for personal gains at the expense of the public.

Actions:

for voters,
Question motives behind political investments (suggested)
Stay informed on political funding sources (exemplified)
</details>
<details>
<summary>
2020-09-07: Let's talk about Labor Day, definitions, reactions, and political belief.... (<a href="https://youtube.com/watch?v=WW7gZZ938eY">watch</a> || <a href="/videos/2020/09/07/Lets_talk_about_Labor_Day_definitions_reactions_and_political_belief">transcript &amp; editable summary</a>)

Beau addresses the narrow scope of political beliefs in the US, encourages viewers to understand their true ideology beyond fear-driven reactions, and connects exploring political beliefs on Labor Day to celebrating organized labor's vision for a better world.

</summary>

"Your ideology shouldn't be based on the best you can do in a system that most people agree is broke."
"Labor Day is the celebration of American organized labor. People who had a vision of a better world and worked to get it."

### AI summary (High error rate! Edit errors on video page)

Acknowledges Labor Day and urges viewers to put in labor for the day.
Addresses the narrow scope of acceptable political beliefs in the United States.
Points out the emotional reactions and lack of understanding towards socialism, communism, and fascism among Americans.
Explains how fear of authoritarianism is the common thread that opposes these isms.
Talks about how propaganda during the Cold War shaped American views on political beliefs.
Suggests that most viewers of his channel can define political terms when stripped of emotional connotations.
Mentions how people in the southern United States tend to lean towards socialism when ideologies are presented without labels.
Encourages viewers to take a political compass quiz and answer questions based on ideal society rather than the current broken system.
Emphasizes the importance of understanding one's true ideology beyond the narrow scope of acceptable beliefs in the US.
Connects the exploration of political beliefs on Labor Day to celebrating American organized labor and their vision for a better world.

Actions:

for viewers,
Take a political compass quiz answering questions based on an ideal society, not the status quo (suggested).
Understand and define political terms beyond emotional reactions (implied).
</details>
<details>
<summary>
2020-09-06: Let's talk about Trump, trust funds, power, and privilege.... (<a href="https://youtube.com/watch?v=PH4m0ZWGD-4">watch</a> || <a href="/videos/2020/09/06/Lets_talk_about_Trump_trust_funds_power_and_privilege">transcript &amp; editable summary</a>)

Acknowledging privilege is not an attack but a statement of fact, urging the need to speak out against inequalities for a more equitable society.

</summary>

"Having a privilege doesn't mean your life isn't hard. It just means it's not harder because of this."
"If you're born into a position of privilege and you don't use your voice to speak out against the inequities, against the inequalities, yeah, that kind of makes you a bad person."
"If we don't talk about it, we can't fix it, and it needs to be fixed."
"It's not a trust fund, and having a privilege isn't an attack on you."
"It's almost like he doesn't want to accept that he is in his station due to luck, not through any work of his own."

### AI summary (High error rate! Edit errors on video page)

Describes a world where some are born with trust funds, influencing laws and systems in their favor.
Talks about privileges like access to education, health care, financial institutions, and preferential treatment by law enforcement.
Mentions privileges that can come from family wealth, having a rich uncle, and being a veteran.
Points out exclusionary traits that individuals may have, like being born poor or having visible traits like scars or skin tone.
Addresses the importance of acknowledging privilege and how it's not an attack but a statement of fact.
Emphasizes the need to speak out against inequalities and inequities if born into privilege.
States that acknowledging privilege is not about making life seem easy but recognizing advantages.
Urges for continued discourse on privilege and the need to address and fix systemic issues in society.

Actions:

for activists, allies,
Speak out against inequalities and inequities (exemplified)
Continue the discourse on privilege and systemic issues (exemplified)
</details>
<details>
<summary>
2020-09-05: Let's talk about the UK, newspapers, the climate, and the US.... (<a href="https://youtube.com/watch?v=-bWQoGUZ45w">watch</a> || <a href="/videos/2020/09/05/Lets_talk_about_the_UK_newspapers_the_climate_and_the_US">transcript &amp; editable summary</a>)

Groups in the UK blockading a newspaper printing facility to halt climate misinformation distribution, urging global action against climate change.

</summary>

"If we are to sort out this mess we're in, the mainstream media must stop profiting from clickbait culture."
"We are past the tipping point on some pieces of it."
"We're still dealing with issues from the 1860s and 1960s, while there are people in other countries looking to the 2060s."
"Europe, they're already in the game and they're waiting for us to catch up."
"People are trying to prove a point."

### AI summary (High error rate! Edit errors on video page)

Groups in the UK are blockading a newspaper printing facility to prevent newspapers, especially those with climate misinformation, from being distributed.
Outlets impacted by the blockade include News Corp publications like the Sun, Times, Sun on Sunday, Sunday Times, Telegraph, and Mail.
The blockade is a response to the facility printing content that downplays the seriousness of climate change.
A spokesperson mentioned the mainstream media profiting from clickbait culture that spreads misinformation and incites hate.
Generation Z in the US views climate change as a significant issue, with 26% believing it cannot be stopped and 49% thinking it can only be slowed.
Beau warns that the world is nearing a tipping point with regards to climate change, urging for immediate action to avert a disaster movie scenario.
In the US, ongoing issues from the past are hindering progress towards equity and equality, preventing the nation from joining the fight against climate change.
Beau draws parallels between the US's current situation and World War II, where the US lagged behind Europe in taking necessary action.
The US's willingness to make critical changes is pivotal in combating climate change and catching up with other nations.
Beau concludes by explaining that the blockade in the UK aims to send a message peacefully, underscoring the urgency for global action.

Actions:

for activists, climate advocates,
Contact local newspapers to ensure they are not spreading climate misinformation (implied)
Join or support climate action groups in your community (implied)
</details>
<details>
<summary>
2020-09-04: Let's talk about why Trump is talking about voting twice.... (<a href="https://youtube.com/watch?v=Tfkbz-Nd88g">watch</a> || <a href="/videos/2020/09/04/Lets_talk_about_why_Trump_is_talking_about_voting_twice">transcript &amp; editable summary</a>)

Trump's double voting suggestion may serve as an excuse for potential election loss, showing a lack of concern for supporters' well-being.

</summary>

"He's worried he's going to lose."
"That's what it looks like to me."
"He obviously does not care about you."

### AI summary (High error rate! Edit errors on video page)

Trump appears to be suggesting his supporters vote twice, first by mail and then in person.
Trump's statements in Pennsylvania hint at concerns about losing the election.
The tactic of encouraging supporters to vote twice could serve as an excuse for Trump if he loses.
Encouraging supporters to potentially commit voter fraud shows a lack of concern for their well-being.
Beau advises Trump supporters against following this illegal suggestion.
Trump's actions suggest a refusal to accept responsibility for any potential election loss.
The President's contradictory stance on mail-in voting and in-person voting raises concerns about his motives.
Trump's fear of losing the election seems to be driving his controversial statements.
Beau warns supporters that following Trump's advice could lead to legal consequences without Trump caring.
The underlying message is a call for Trump's supporters to avoid engaging in illegal voting practices.

Actions:

for supporters and voters,
Inform fellow Trump supporters not to follow Trump's suggestion to vote twice (suggested)
Educate others on the importance of following legal voting procedures (suggested)
</details>
<details>
<summary>
2020-09-04: Let's talk about the Belleau Wood and Trump's remarks.... (<a href="https://youtube.com/watch?v=kpiT1dcus4Q">watch</a> || <a href="/videos/2020/09/04/Lets_talk_about_the_Belleau_Wood_and_Trump_s_remarks">transcript &amp; editable summary</a>)

Beau delves into the forgotten battle of Belleau Wood, its impact on Marines, and the indifference of politicians towards military history and heroes.

</summary>

"That tough guy line you've heard in a dozen action movies actually said."
"Well, come on, do you want to live forever?"
"The deadliest weapon in the world is a Marine and his rifle."
"This is an attack on the very foundations of the Marine Corps."
"For all of the talk about supporting the troops, they don't care."

### AI summary (High error rate! Edit errors on video page)

Explains the forgotten battle of Belleau Wood and its significance to the Marines.
Mentions how the battle is part of Marine lore but largely unknown outside the Marine Corps.
Describes key moments from the battle, like Marines filling in the line when French forces retreated and a Marine leading a charge across an open field.
Talks about the renaming of Belleau Wood to the Wood of the Marine Brigade due to the intensity of the fighting.
Addresses the impact of President Trump's alleged remarks on modern Marines and their connection to their spiritual ancestors.
Points out the difference in reactions between modern vets who understand political aspects of deployments and the emotional response to attacks on their heroes.
Emphasizes the significance of the Marine Corps and their history, especially in the face of political indifference towards military personnel.

Actions:

for history enthusiasts, marines, veterans,
Honor the memory of forgotten battles like Belleau Wood by sharing its story and significance within your community (implied).
Support veterans and active-duty military personnel through local initiatives or organizations to show appreciation for their sacrifices (implied).
</details>
<details>
<summary>
2020-09-03: Let's talk about things not going the way you planned.... (<a href="https://youtube.com/watch?v=S26-2RXRdOc">watch</a> || <a href="/videos/2020/09/03/Lets_talk_about_things_not_going_the_way_you_planned">transcript &amp; editable summary</a>)

Beau delves into historical parallels to caution against romanticizing rebellion and the potential dire consequences of internal conflict in a powerful nation.

</summary>

"A situation that nobody agrees on right now. It's disputed as to what happened. A petty crime, some pig, and an extrajudicial killing. That's how we wound up here."
"The image of the glorious rebellion is myth. It's mythology. It's not real life."
"Because this stuff doesn't occur in a vacuum. It's going to just sit idly by while a nuclear power, one of the strongest militaries in the world, rips itself apart."

### AI summary (High error rate! Edit errors on video page)

Exploring the historical context of how the current situation unfolded, referencing past events like Bacon's Rebellion.
Describing the catalyst of the current situation as involving a petty crime, "some pig," and an extrajudicial killing.
Outlining the progression from petitions and outrage to protests, rioting, and looting, eventually leading to an open rebellion.
Contrasting the common myth that Bacon's Rebellion was a forerunner to the American Revolution with the reality that it was not about independence from the king.
Emphasizing that despite the genuine beliefs of those involved, nothing changed after Bacon's Rebellion.
Warning against romanticizing rebellion and pointing out the potential consequences of a violent uprising in the modern world.
Cautioning that hostile nations could exploit internal conflict in the US to weaken the country with devastating effects.
Urging reflection on the mythological versus real-life implications of rebellion and the need for critical thinking about such actions.
Stating that rebellion does not happen in isolation and could attract external interference, especially in a country with significant military power.
Encouraging thoughtfulness and consideration of the broader implications before advocating for or engaging in rebellion.

Actions:

for history enthusiasts, critical thinkers,
Challenge romanticized views of rebellion (implied)
Promote critical thinking before advocating for drastic actions (implied)
Encourage thoughtful reflection on historical context and potential consequences (implied)
</details>
<details>
<summary>
2020-09-03: Let's talk about filling in some holes.... (<a href="https://youtube.com/watch?v=evdN5fdRKZc">watch</a> || <a href="/videos/2020/09/03/Lets_talk_about_filling_in_some_holes">transcript &amp; editable summary</a>)

Beau views local government restrictions as assets, advocates for changing mindsets, and building community support to challenge unjust regulations and create parallel systems for community safety nets.

</summary>

"Your local government has made it illegal to better your community. That seems a little silly."
"You're creating a parallel one. You're creating something that people can turn to when the government fails as it typically does."
"You want that secondary safety net for your community."

### AI summary (High error rate! Edit errors on video page)

Acknowledges feedback about holes in previous video examples.
Views local government restrictions as an asset for community networks.
Emphasizes changing societal mindset to effect real change.
Advocates for focusing on actions not prohibited by local ordinances first.
Suggests garnering public support before tackling controversial issues.
Encourages using media attention to challenge unjust regulations.
Questions the logic of prohibiting community improvement.
Proposes government facilitation instead of control over community initiatives.
Considers government employees as public servants, not rulers.
Advises choosing battles wisely to achieve meaningful victories.
Stresses the importance of swaying public opinion over direct confrontation.
Advocates for creating parallel systems to support communities where the government fails.
Announces plans for a workbook to further these ideas.

Actions:

for community members,
Reach out to local press to support community improvement initiatives (implied)
Advocate for government facilitation rather than control over community projects (implied)
Begin planning and organizing community improvement projects with public support (implied)
</details>
<details>
<summary>
2020-09-03: Let's talk about a hard truth about voting and community networks.... (<a href="https://youtube.com/watch?v=apGViv909eo">watch</a> || <a href="/videos/2020/09/03/Lets_talk_about_a_hard_truth_about_voting_and_community_networks">transcript &amp; editable summary</a>)

Voting is minimal; community involvement is key for immediate change, building networks, and influencing local politics positively, making communities stronger.

</summary>

"Voting is the least involved and least effective method of civic engagement."
"Pooling resources within the community can accomplish significant goals."
"Your little network did it."
"This makes your local community stronger."
"Just better your community, that's it."

### AI summary (High error rate! Edit errors on video page)

Voting is the least involved and least effective method of civic engagement.
Being actively involved in your community is key to producing immediate change.
Forming a community network focused on making the community better generates power and strengthens the community.
A network like this can reduce reliance on the government and confront issues directly.
The network can circumvent red tape, be more flexible, and produce immediate results.
Pooling resources within the community can accomplish significant goals.
A community network consists of different groups like A group, B group, and a command group (C group).
Recruiting people who can contribute specific skills can enhance the effectiveness of the network.
People helped by the community network often become valuable resources in return.
Ideological alignment is not necessary; the primary commitment should be to strengthen the community.
Operating in urban and rural areas require different approaches in community networking.
Limiting operations to a specific area initially and gradually expanding helps in building community support.
Community networks can have a significant impact on local politics and governance.
By publicly requesting politicians for assistance and taking action independently, the network can influence political decisions.
Strengthening the local community through active involvement benefits everyone.

Actions:

for community members, activists, volunteers.,
Form a community network committed to bettering the community (exemplified).
Recruit individuals with diverse skills to enhance the effectiveness of the network (exemplified).
Actively involve in community initiatives to strengthen local governance (implied).
</details>
