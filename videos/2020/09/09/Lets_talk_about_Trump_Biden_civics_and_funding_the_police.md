---
title: Let's talk about Trump, Biden, civics, and funding the police....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qawVdpmN-hw) |
| Published | 2020/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses President Trump's repeated claim that Joe Biden will defund the police, leading to chaos.
- Breaks down the funding of police departments, showcasing various grant programs and assistance.
- Mentions the COPS program with a $400 million budget, Edward Byrne Memorial Justice Assistance Grant program with $264 million, and DHS Preparedness Grant program with a budget of $450 million.
- Explains the Department of Defense's program 1033, which provides old military equipment to law enforcement.
- Calculates the total funding from these sources to be $2.6 billion, enough to run the NYPD for three months.
- Points out that federal funding is a small percentage of what state and local officials spend on law enforcement.
- Concludes that whether intentionally misleading or lacking understanding, President Trump's emphasis on defunding police by Biden is not a significant concern for voters.

### Quotes

- "Either President Trump just does not understand basic civics or he is intentionally misleading people to scare them."
- "Joe Biden defunding the police should not be a concern. It's not a thing."
- "At the local level, somehow fund your local police department. The feds don't foot that bill."

### Oneliner

President Trump's claims about Joe Biden defunding police lack merit; federal funding is a small portion of what local departments rely on.

### Audience

Voters, Civics Learners

### On-the-ground actions from transcript

- Contact local officials to understand how your community funds its police department (implied)

### Whats missing in summary

In-depth analysis of the potential impact of misleading political claims on public perception and voter decisions.

### Tags

#Politics #Civics #PoliceFunding #PresidentialElection #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a talking point.
A presidential talking point no less.
We're going to do this because Donald Trump keeps bringing something up and talking about
it over and over and over again and the more he talked about it the more I remembered that
I took basic civics and what he was saying just didn't add up.
So I decided to start adding stuff up and in the process I found out that NPR had done
pretty much all the work for me.
Thanks guys.
If you don't know, President Trump is attempting to paint this image that Joe Biden is going
to defund the police and then there will be chaos in the streets.
It's just going to be uncontrollable.
It's going to be madness.
Now if you're a Biden supporter, yes I understand Biden didn't say this, the President is just
making it up.
I've got that.
But let's just pretend for a moment that Biden does.
He becomes President and then he defunds the police to the best of his ability.
What does that look like?
Okay so you have the COPS program which is Community Oriented Policing Services.
In 2020 it had $400 million to give away.
You have the Edward Byrne Memorial Justice Assistance Grant program and in 2019 it had
$264 million.
I don't have this year's numbers.
There's also a DHS Preparedness Grant program.
It has a budget of $1.8 billion but only 25% of that has to go to law enforcement.
So that's $450 million.
But it doesn't matter.
We're going to go ahead and take the full $1.8 billion because there are some smaller
grant programs that we don't have the numbers for.
But they will not add up to the additional $1.35 billion of the DHS program.
It's stuff like we'll help you buy vests or body cams.
The Department of Agriculture will help towns that have less than 5,000 people buy a patrol
car, stuff like that.
As far as cash changing hands, there you go.
Now on top of this, the Department of Defense has a program called 1033 and that hands old
military equipment to law enforcement.
The value of the equipment that gets transferred, it fluctuates wildly year by year.
But if you average it all out from 1990 to 2018, the average per year of the value is
about $200 million.
So you add all of this up, you get a whopping $2.6 billion.
That's enough to run NYPD for about three months.
The feds don't fund your local department.
That's not a thing.
Even if Joe Biden cut off all the assistance, it wouldn't matter.
That's not where they get their money.
The state and local officials spend, depending on how you figure it, between $115 billion
and $194 billion on law enforcement and corrections, depending on how you figure that.
So let's just take the $115 billion.
It's what, 2%?
The feds chip in 2% on that.
I don't think that's going to be the difference between a functioning society and chaos.
That seems ridiculous.
So at the end of the day, we are left with one of two options.
Either President Trump just does not understand basic civics and how local departments get
funded, or he is intentionally misleading people to scare them.
From where I sit, either one of those is plausible, because he has not proven himself to understand
how the government works, and he has proven that he will happily fearmonger at any opportunity.
So it could be both, to be honest.
But either way, Joe Biden defunding the police should not be a concern.
This is not something that should weigh heavily on your vote.
It's not a thing.
You at the local level somehow fund your local police department.
The feds don't foot that bill.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}