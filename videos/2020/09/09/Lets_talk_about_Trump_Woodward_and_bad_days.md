---
title: Let's talk about Trump, Woodward, and bad days....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_kolaHLYExw) |
| Published | 2020/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines the role of a security consultant as being paid for what you can do on the days when your client has a bad day, needing help, advice, and leadership.
- Draws parallels between the role of a security consultant and that of the President of the United States, as someone who should be called upon during collective bad days.
- Points out that the President knew early on about asymptomatic transmission and should have advocated for masks but chose to downplay it.
- Mentions that the President was aware of the deadliness of the virus, comparing it to being five times as deadly as the most severe flu.
- Criticizes the President for failing in leadership, passing on information, and actively working against the American people during the pandemic.
- States that the President failed to act, which resulted in tens of thousands of needless deaths that could have been mitigated.
- Emphasizes that taking responsibility for the failure to act translates to accepting responsibility for the unnecessary loss of lives.
- Suggests that had the President acted differently, lives could have been saved, and the economy could have been better off.
- Condemns the President as unqualified for his role and unfit to handle future crises effectively.
- Expresses concern about the potential for more bad days in the future and does not want someone who failed at this level to be relied upon.

### Quotes

- "He did nothing."
- "We could have mitigated this."
- "He never should have been in that role."

### Oneliner

Beau points out President Trump's failure to act during the pandemic, resulting in needless deaths and economic repercussions, questioning his suitability for future crises.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact elected officials to demand accountability and action (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the President's handling of the pandemic, raising concerns about leadership, accountability, and future crisis management.

### Tags

#Leadership #PandemicResponse #Accountability #PublicHealth #CrisisManagement


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about bad days.
Responsibility.
And Woodward.
Talk about what he has released.
You know, I was a security consultant.
Security consultants, you're not paid for what you do on a daily basis, because you
don't do much, to be completely honest.
You're paid for what you can do on the days when your client has a bad day.
When something goes wrong, they need help, they need advice, they need leadership.
They need somebody with the right information to step in.
That's really what the job is.
In a lot of ways, that's the President's job as well.
He is, in theory, the person the United States calls on when we have a collective bad day.
Somebody who has access to the information, who can advise and lead.
I'm not going to go through all of Woodward's release.
I'm not going to go through all the quotes, because I think you need to go listen to it
yourself.
I think you need to read those quotes yourself.
I think you need to pay special attention to the dates of those interviews.
Early on, the President knew that there was asymptomatic transmission.
Early on, he knew transmitted by breathing, which means from that point forward, he should
have been advocating for masks, but he didn't, because he likes to downplay it.
He knew how deadly it was, saying it's five times as deadly as our most strenuous flu.
They gave numbers associated with the number of lost to flu each year.
Five times that, 150,000.
He knew.
He knew that.
We're sitting at almost 200,000 now, because he did nothing, because he didn't act, because
it was politically advantageous to ignore reality, to lie to the American people, to
fail in every regard when it comes to leadership, when it comes to just passing on information.
Maybe had he not actively worked against the American people, they would have made the
right choice themselves.
They just had the information.
Most times people will.
At the end of the day, the President of the United States, Donald Trump, failed the United
States when it called on him, when we were having a bad day.
He accepts no responsibility, and to be crystal clear, whoever accepts responsibility for
it is accepting responsibility for tens of thousands of needless deaths.
He did nothing.
We could have mitigated this.
This could have been nothing.
Not just would it have saved lives, it would have helped the economy.
We'd be done, at least on the declining side of it.
Whereas right now, we don't know what's going to happen, because he refused to act.
He refused to step up.
He never should have been in that role.
He's completely unqualified.
There are more bad days coming.
The United States will have more bad days in the coming years.
I for one would not want somebody who has failed at this level to be the person I have
to call.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}