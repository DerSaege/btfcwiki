---
title: Let's talk about Trump's taxes and how the Dems are missing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=doENavQOvmc) |
| Published | 2020/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advises the Democratic Party on how they are mishandling information damaging to Trump's campaign regarding his tax documents.
- Trump's base views him as an establishment outsider and may actually see it as a good thing if he paid very little in taxes.
- Points out that Trump's base is skilled at "double think," believing contradictory ideas simultaneously.
- Emphasizes that the focus should be on how Trump's financial losses and inability to profit during a strong economy undermine his image as a successful businessman.
- Suggests that Trump's core supporters care only about themselves and have blind faith in him despite any lies or misrepresentations.
- Argues that the Democratic Party should frame the narrative around Trump's financial failures rather than just his tax payments to weaken his hold on supporters.

### Quotes

- "They care only about themselves. They've put their hope in this man based on what he told them. And it was a lie."
- "If you don't think Trump will sell out the American citizens as he has done everybody, you're more under his spell than most."
- "He couldn't even turn a profit in the middle of one of the best economies we've ever had."
- "The idea that he somehow scammed the government, it's not going to fly with them."
- "You're talking about selfish people. That's his base."

### Oneliner

Beau advises the Democratic Party to focus on Trump's financial failures rather than his tax payments to weaken his hold on supporters who believe he can do no wrong.

### Audience

Democratic Party strategists

### On-the-ground actions from transcript

- Reframe the narrative around Trump's financial failures and inability to profit during a strong economy to undermine his image (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of how Trump's base views him and suggests a strategic approach for the Democratic Party to counter his appeal.

### Tags

#DemocraticParty #Trump #TaxDocuments #PoliticalStrategy #UndermineSupport


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about understanding other people and making sure that you frame
things in a way that appeals to them.
And of course we're going to talk about Trump's tax documents.
But we're going to do it in a different way.
We're going to give some advice to the Democratic Party because flat out they're doing it wrong.
They are doing it wrong.
There is information that is damaging to the Trump campaign in the New York Times' allegations.
But it is not that he paid $750 in taxes.
That is not going to impact his base at all.
To the contrary, it is going to endear him to them.
Smart soldiers will take the time to learn their opposition's tactics.
Really smart soldiers will take the time to learn how their opposition thinks and what
motivates them.
It may not make any sense to you, but Trump's base views him as an establishment outsider.
Some maverick who is always bucking the system.
The possibility that he might have been less than truthful on his tax forms, that isn't
going to impact them.
They're going to think that's a good thing.
I promise you, somebody is going to say he paid $750 too much.
Think about what you're appealing to when you make this statement.
You're appealing to a sense of fair play, civic duty, honesty.
Does any of this sound like a Trump supporter?
Probably not going to appeal to them.
Trump's base, they are experts at double think.
They truly believe the trust fund baby billionaire who had the exact right connections to get
bone spurs at the perfect time, that he's not part of the establishment.
They think he's like them, when in reality he doesn't even like them.
But that's what they believe.
Because of that, they can simultaneously hold the opinions that they want a strong man leader,
some authoritarian like Trump, and pretend they're anti-government.
The idea that he somehow scammed the government, it's not going to fly with them.
They don't care about that.
However, there is information here that is incredibly damaging to him.
It's just not that.
Trump in the years leading up to 2012, some years he had hundreds of millions of dollars
in revenue.
We're talking about a lot of money coming in.
And then in 2012, he started losing money.
Losing money in 2012.
Man can't even balance a checkbook.
How is he going to make America great again?
2012, yeah, he's losing money in the middle of that bull economy.
Started what, July of 2009 I think, is when it really started kicking up.
He couldn't even turn a profit then, in the middle of the greatest economy the world has
ever seen.
The one he's been taking credit for this whole time that started way before he took office.
He couldn't even do it then.
This whole idea that he left a lap of luxury to become a public servant and help the common
folk.
That's not true.
In the years before the election, he's losing like $50 million a year.
And I would point out that once he got into office, well he made, well I don't want to
say he made money because he still didn't turn a profit, but he lost less.
I'm assuming it probably has at least something to do with all those trips to his resorts.
Maybe.
We don't know.
Because he doesn't have the most transparent administration ever.
You have the key here to undermining his hold on people.
One of their core beliefs is that this man can do nothing wrong.
That he's a great businessman.
That he's going to lead us out of whatever horrible situation they perceive us to be
in.
When in reality, he couldn't turn a profit in the middle of one of the best economies
we've ever had.
That's what you have to focus on.
They do not care about fair play.
None of the things that matter when it comes to the idea of a billionaire, in quotation
marks with a little asterisk by it I guess, paying $750 in taxes, none of that appeals
to them.
They don't care about civic duty.
They don't care about civic responsibility.
They won't even wear a mask.
You think they're going to care he didn't pay his taxes?
Course not.
You're talking about selfish people.
That's his base.
Those are his true supporters.
Those are the people who are truly under his spell.
They care only about themselves.
They've put their hope in this man based on what he told them.
And it was a lie.
They don't care that he lied to the feds.
They might care that he lied to them.
That he's misrepresented himself this whole time.
That's the framing the Democratic Party needs to use.
Not that he didn't pay his taxes.
They're going to see that as a good thing.
They're going to see that as, well he didn't want to fund things he was morally opposed
to.
Because again, that double think, they believe he has a moral compass of sorts.
Rather than somebody who goes from deal to deal.
Everything's transactional.
And with that in mind, and everything being transactional about him, he has hundreds of
millions of dollars in debt and it's coming due.
What do you think he's going to offer them if he's sitting in the Oval Office?
Them.
If you don't think Trump will sell out the American citizens as he has done everybody,
you're more under his spell than most.
I think even his most ardent supporters would admit that he is very quick to cut ties and
shove people under the bus.
If he is in the Oval Office when that debt comes due, those people who he is indebted
to, well he has a whole lot of collateral that he can put up.
And that's the American people.
Anyway, it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}