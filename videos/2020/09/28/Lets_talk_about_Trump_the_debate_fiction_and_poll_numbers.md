---
title: Let's talk about Trump, the debate, fiction, and poll numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NRxGFTzlxPA) |
| Published | 2020/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States won't be fact checked during upcoming debates.
- Lack of fact-checking gives President Trump a free pass to lie during the debate.
- It's embarrassing that fact-checkers won't be present at the presidential debates.
- The debate will likely be filled with lies, as Trump is known for not telling the truth.
- Beau quotes Octavia Butler about choosing leaders wisely.
- Debates and polls are deemed worthless; only actual voting matters in the end.
- Democrats should not be complacent based on poll numbers; what matters is voter turnout.
- The focus is on the importance of people showing up to vote rather than relying on polls.
- Beau questions the value of debates if candidates won't be held accountable for their statements.
- The emphasis is on the significance of action through voting rather than passive reliance on polls or promises.

### Quotes

- "President of the United States will get up there and say whatever he feels like."
- "It's embarrassing that we need fact-checkers at the presidential debates."
- "These debates are worthless. They're as worthless as his promises."
- "Those polls do not matter. They mean absolutely nothing."
- "Who shows up the day of? Who votes?"

### Oneliner

President won't be fact-checked, debates lack accountability, polls and promises are worthless - voting is what truly matters.

### Audience

Voters

### On-the-ground actions from transcript

- Show up and vote on election day (implied)
- Avoid complacency based on poll numbers; prioritize active voting participation (implied)

### Whats missing in summary

The full transcript provides a critical perspective on the lack of fact-checking in debates, the importance of voter turnout over polls, and the need for accountability in political leadership.

### Tags

#PresidentialDebates #FactChecking #VoterTurnout #PoliticalLeadership #Accountability


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the debates, the upcoming debates.
It's already been announced that the President of the United States will not be fact checked.
During the debate, the moderators aren't going to fact check him.
Well I guess we know what we can expect from the debate then.
The President of the United States will get up there and say whatever he feels like.
He will make no effort to tell the truth about anything because he's not going to be held accountable.
It doesn't matter where you lie on the political spectrum.
I think it is safe to say that everybody knows President Trump is, let's just say, economical with the truth.
He's not known for telling the truth, ever, about anything.
And he's not going to be fact checked.
So it's a green light for him to just lie to the American people.
It's embarrassing that we need fact checkers at the presidential debates.
It's even more of an embarrassment that we're not getting them.
It shows that those in charge of this process do not care about informing the American people,
do not care about making it better.
They care about politics as usual and that's it.
This debate will be nothing but the President lying because that's all he does.
He's a fear monger, fool, grifter, liar, tyrant.
Whom I describe.
Got a quote from Octavia Butler for you.
Choose your leaders with wisdom and forethought.
To be led by a coward is to be controlled by all that the coward fears.
Led by a fool is to be led by the opportunists who control the fool.
To be led by a thief is to offer up your most precious treasures to be stolen.
To be led by a liar is to ask to be told lies.
To be led by a tyrant is to sell yourself and those you love into slavery.
This debate is worthless.
It means nothing.
Because at the end of the day, whatever Trump says as far as policy considerations, they're
not true.
It's not what he's going to do because he's just going to say whatever he feels at the
moment.
They're worthless.
These debates are worthless.
They're as worthless as his promises.
They're going to mean nothing.
They're not going to do anything to inform the American people about their positions
because they will not be held accountable.
And this is the kind of leader we'll get.
Another fiction that I think needs to be avoided right now, something Democrats really need
to pay attention to, is that those polls, they don't matter.
They do not matter.
They are fictitious.
All that matters is who shows up the day of.
Who votes?
One of the reasons I think that we have this man in the Oval Office now is because a lot
of Democrats, well, they think it's in the bag.
They thought, they knew Clinton was going to win.
So why take the time?
Right now, yeah, Biden may be up by 10 points.
But if people don't show up, we'll get Trump again.
It's another convenient fiction.
It's more politics as usual.
Those polls do not matter.
They mean absolutely nothing.
They mean as much as the President's promises, as much as the words that he says.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}