---
title: Let's talk about the US as a failed state....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AjtzXryZ-7o) |
| Published | 2020/09/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring if the United States is behaving as a failed state due to erosion of legitimate authority to make collective decisions.
- Government's failure to obtain consent of the governed through polarizing society and not basing support on ideas and policies.
- Society's belief in enforcing laws rather than just writing them down on paper.
- Loss of control on the legitimate use of physical force by the government.
- Society's view on excessive force by law enforcement and its impact on legitimacy.
- Inability of the government to provide public services like clean water and healthcare.
- The United States' violations of international law and isolationist tendencies.
- The importance of individual action in shaping society's beliefs and laws.
- Encouraging individuals to be active and involved at the community level.
- The government's focus on self-benefit and division of society for their advantage.

### Quotes

- "Society's beliefs help shape the law."
- "It's up to you as an individual to help change this."
- "The national level has become too corrupt."
- "The United States is failing as it benefits those in power temporarily."
- "People just out for themselves and failing in their obligations as stewards of the state."

### Oneliner

Is the United States becoming a failed state due to erosion of legitimate authority, loss of control on physical force, failure in public services, and violations of international law?

### Audience

Activists, concerned citizens

### On-the-ground actions from transcript

- Be active and involved in your community to shape society's beliefs (implied)
- Advocate for policies based on ideas rather than parties (implied)
- Support initiatives that aim to provide public services (implied)

### Whats missing in summary

The full transcript delves deeper into the characteristics of a failed state and the importance of individual action in societal change.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a term that keeps getting thrown out by academics
and the media.
And I think this term is being ignored because people believe it's hyperbole.
They think that it's being said simply as a reaction to the Trump administration or
because of the Trump administration.
So today we're going to talk about that phrase.
Is the United States behaving as a failed state?
What is a failed state?
What are the characteristics of it?
There are four.
The first is erosion of legitimate authority to make collective decisions.
The second is loss of control of its territory or of the monopoly on the legitimate use of
physical force therein.
The third, inability to provide public services.
The fourth, inability to interact with other states as a full member of the international
community.
Okay let's go to that first one.
Erosion of legitimate authority to make collective decisions.
Does the government make collective decisions with authority?
Does it have consent of the governed?
A concept so important it's in the Declaration of Independence.
Does that exist with our current government?
The answer is no.
Why?
Because people in the current government, the politicians, have pitted society against
itself, divided it up.
They're basing support and hoping to obtain support for their new laws on parties and
people, rather than ideas and policies.
They don't attempt to make their case for a new law in the court of public opinion.
That's not how they do it.
They pass the law and then try to ram it through the court system, the legal system.
If they don't make their case in the court of public opinion, they don't get consent
of the governed because we're pretty polarized.
So it's very hard to believe that the U.S. government has legitimate authority to make
collective decisions for everybody because they don't get consent of the governed.
They use those legal courts rather than the court of public opinion.
And as we know, it is the social belief that actually enforces laws, not just writing it
down on a piece of paper.
If writing something down on a piece of paper and calling it a law stopped people from doing
that activity, there would be no people in prison.
It has to come from society as a whole.
They have to get consent of the governed.
Okay.
So they use the legal system.
And then once you start using the legal system, you have to rely on force.
And that takes us to number two.
Loss of control of its territory.
That's not really happening.
Or of the monopoly on the legitimate use of physical force.
Is that happening?
Is the government losing the monopoly on the legitimate use of physical force?
Absolutely.
You can point to outlier cases which really aren't as important in the grand scheme of
things such as the checkpoints up in the Pacific Northwest carried out by non-state actors
or the non-state actors that are duking it out in the streets.
Yeah, that's important to note, but beyond that, it's the legitimate use of physical
force.
What's one of the main...
That's rain.
What's one of the main discussions going on in the country right now?
Excessive force by law enforcement, right?
Society, which is what matters, not the legal definition, society has determined that the
force being used isn't legitimate.
Doesn't matter what the courts find because the courts are an arm of the state.
It doesn't matter if the courts say, yes, this is okay because it has to do with consent
of the governed and they don't have it.
So every time there's an incident of excessive force by law enforcement and the back of the
blue crowd comes out and says, no, we have to have law and order to protect the country,
the reality is they're pushing it further into the failed state category because society
at large is not viewing this force as legitimate.
Third, inability to protect, or sorry, inability to provide public services like clean water
in Flint, working levees, emergency response to fires, hurricanes, healthcare.
The United States doesn't have that.
It's failing in that regard.
Inability to interact with other states as a full member of the international community.
This we still have for the moment.
This we still have for the moment solely because we're a nuclear power with a really strong
military.
If we didn't have that, would the rest of the world take us seriously?
Our economy used to help us but that's not working so much anymore because we've become
so isolationist under this administration.
We are a country that violates international law all the time.
We sell out our allies and we break treaties.
We withdraw from treaties.
The only thing making the international community still listen to us is our military.
So is the government failing?
Are we becoming a failed state?
Yeah.
We're failing them because it benefits those in power temporarily.
They've pitted society against itself, divided it, don't have consent of the governed.
They encourage the polarization because as long as they can keep those people on the
bottom fighting each other, they can stay at the top for a time.
At the end of the day, it's up to you as an individual to help change this.
Your personal beliefs helps shape society's beliefs.
Society's beliefs help shape the law.
That's how it's supposed to work.
But in order to do that, you have to be active.
You have to be involved.
You have to start at your community level because the national level has become too
corrupt.
It has become a failed state.
People just out for themselves and failing in their obligations as stewards of the state.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}