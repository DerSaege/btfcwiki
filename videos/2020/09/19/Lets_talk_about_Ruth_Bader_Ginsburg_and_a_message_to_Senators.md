---
title: Let's talk about Ruth Bader Ginsburg and a message to Senators....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a9QL5Z2VS5Y) |
| Published | 2020/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ruth Bader Ginsburg has died, and the President will likely push through a Supreme Court nominee close to the election.
- President Trump believes the nominee will be loyal to him in any decisions regarding the election.
- Beau recalls Senator McConnell's statements in 2016 advocating for the American people to have a voice in the selection of the next Supreme Court justice.
- McConnell's role in pushing through a nominee is emphasized, noting the potential impact on closely contested Senate races.
- Beau raises concerns about a potential nominee on Trump's list who advocated for American soldiers to shoot unarmed citizens.
- Republican senators may face backlash from voters if they support a nominee tied to Trump's failures.
- Beau questions the wisdom of Republican senators closely associating themselves with Trump so close to the election.
- Democrats may only need a few Republican senators to halt the nomination process before the election.
- Beau urges smart Republicans to take a moral stance and let the American people decide on the Supreme Court vacancy.
- Democrats have obstructionist tactics at their disposal, but Beau hopes Republicans will act ethically without needing such tactics.

### Quotes

- "The American people should have a voice in the selection of their next Supreme Court justice."
- "I'm not sure that this close to the election, tying yourselves to such a failure of a president is a good idea if you're a Republican."
- "Which Republicans are going to be in that race to be the first ones to say they're not going to do it?"
- "Because, man, that's a moral stance."
- "And with all of the behavior the Republican Party has been associated with lately, taking a moral stance right before the election, that might be a good move."

### Oneliner

Beau warns Republican senators against hastily confirming a Supreme Court nominee tied to Trump's failures, urging them to take a moral stance and let the American people decide.

### Audience

Senators

### On-the-ground actions from transcript

- Hold off on confirming a Supreme Court nominee before the election (implied).
- Smart Republicans should take a moral stance and prioritize the voice of the American people in selecting the next Supreme Court justice (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of rushing a Supreme Court nominee before the election, urging Republican senators to prioritize ethical decision-making.

### Tags

#SupremeCourt #Senate #Ethics #Election #Republican #Democratic


## Transcript
Well, howdy there, internet people.
It's Beau again.
Ruth Bader Ginsburg has died.
So where do we go from here?
What's next?
Undoubtedly, the President of the United States
will attempt to push through a Supreme Court nominee this
close to the election.
It's going to be very important to him
because he believes that nominee will be beholden to him come
election time.
And if any decisions have to be made in regards
to the election, he will assume that person
will decide in his favor because that's
how President quid pro quo works.
I, on the other hand, believe what Senator McConnell
said he believed on February 13, 2016.
The American people should have a voice
in the selection of their next Supreme Court justice.
And I believe what he said on March 2, 2016,
when he said, once the political season is underway,
action on a Supreme Court nomination
must be put off until after the election campaign is over.
So if you don't know, McConnell will
be the person really kind of responsible for pushing
through any nominee, continuing to be the President's
little secretary.
If he does that, it's very clear that McConnell is not
a man of his word, that what he says
means nothing because he's on the record multiple times
saying that this isn't the right thing to do.
While the President may want to push through a nominee
right now, I'm not sure those in the Senate are going to want to
because the reality of it is there's
a lot of pretty closely contested races.
And the attempt to do something this against tradition
might backfire on them.
It might show that senators now have their loyalty fully
vested in President Trump, a man consistently
failing in the polls.
This would be especially true of those
that are really at risk, like in Arizona, Maine,
and North Carolina.
Goes for the other ones as well.
There's also the fact that on the President's list
of potential Supreme Court nominees,
well, there's one name where the person actually
advocated for American soldiers to gun down
American citizens on American soil,
even if they were unarmed and trying to surrender.
That's a potential Supreme Court nominee.
If I was a Republican senator, I don't
know that I would want that broadcast.
I don't know that I would want those people who
would be deciding whether or not to re-elect me to hear that
right before they vote.
That might have a negative outcome.
Might cause people to vote against me.
Not something I'd want to do.
I'm not sure that this close to the election,
tying yourselves to such a failure of a president
is a good idea if you're a Republican.
However, the Republicans have really just done whatever
Trump's wanted the last few years.
They've shown no spine, no backbone,
no direction of their own.
They've just followed the President
as he's allowed a couple hundred thousand Americans to disappear,
as he waited three years to provide hurricane relief,
as he failed in every regard with every duty
that he was trusted.
I'm not sure that I would want to be
responsible for backing another lifetime
appointment selected by him.
I don't think it will play out the way you
might picture at the polls.
Now, the reality is the Democrats
don't have to get that many people to stop this.
There have already been indications,
I think one in Alaska, a Republican
has said they're not going to move to support a nomination
prior to the election.
Because that's the ethical thing to do.
They don't need many, just a few, a handful,
and it stops it.
Now, the real question is, which Republicans are smart?
Which Republicans are going to be in that race
to be the first ones to say they're not going to do it?
To be the first ones to say, no, this isn't right.
We're going to let the American people decide.
The vacancy can sit for a couple of months
until after the election.
Because, man, that's a moral stance.
And with all of the behavior the Republican Party
has been associated with lately, taking a moral stance
right before the election, that might be a good move.
And if Trump wins, hey, you make the appointment then.
And if he doesn't win, well, you didn't betray the nation.
There's a lot of little obstructionist tactics
that Democrats can use, Democrats in the Senate can use.
I suggest they use them all, to be honest.
But I'm hoping that Republicans are smart enough
to understand that they really shouldn't make them necessary.
Because if they become necessary,
it becomes a political spectacle.
And it shows how closely each Senate candidate is to Trump,
how tied they are to those policies,
the ones the American people are consistently
rejecting at the polls.
In short, if you're a Republican senator
and you want to keep your seat, I
would suggest you drag your feet on this,
no matter how much that crybaby in the Oval Office
whines about it.
Because otherwise, I don't think you have a chance.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}