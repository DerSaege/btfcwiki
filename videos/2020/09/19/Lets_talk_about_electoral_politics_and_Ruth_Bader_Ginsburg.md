---
title: Let's talk about electoral politics and Ruth Bader Ginsburg....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=edRE7KNayPI) |
| Published | 2020/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- One person's loss raised concerns for 160 million Americans about their rights being imperiled.
- There is too much power concentrated in too few hands, leading to potential jeopardy for many.
- The tragedy and upheaval overshadow the critical issue of power distribution.
- The current events served as a powerful illustration of a political compass point made earlier.
- People on the bottom left of the political spectrum are engaging in debates despite a shared direction.
- The divide lies between those heavily involved in electoral politics and those advocating for tangible actions for change.
- The recent impact underscores the importance of electoral politics while also acknowledging the limitations of voting.
- Building a separate power structure and engaging in electoral politics can coexist as complementary strategies.
- The need for a diversity of tactics and an understanding between different political approaches.
- Collaboration and efforts to reclaim power are necessary to prevent further rights jeopardization.

### Quotes

- "There is too much power in too few hands."
- "The problem is, I'm afraid it's going to be lost among the tragedy and the upheaval."
- "It's two buses headed to the same spot."
- "We need people fighting a holding action."
- "We have got to limit the power. We have to disperse it at the very least."

### Oneliner

One person's loss underscores the peril of concentrated power and the need for diverse strategies to reclaim power and protect rights.

### Audience

Activists, Voters, Organizers

### On-the-ground actions from transcript

- Collaborate with local organizations to strengthen community networks (implied)
- Engage in electoral politics to influence immediate tangible results (implied)
- Advocate for a diversity of tactics to reclaim power and prevent authoritarian creep (implied)

### Whats missing in summary

The full transcript provides a deep dive into the importance of power distribution, diverse strategies for change, and the imperative need to reclaim power collectively.

### Tags

#PowerDistribution #PoliticalCompass #CommunityOrganizing #ElectoralPolitics #RightsProtection


## Transcript
Well howdy there internet people, it's Beau again.
A lot going on today, right?
I would imagine.
So, what happened?
I mean really, at the end of the day, what went down?
One person, one person,
one person was lost.
And suddenly, 160 million Americans have a legitimate concern
about whether or not their rights are about to be imperiled.
That happened.
That's what happened.
One person, I'm fairly certain it's not supposed to work that way.
I'm fairly certain that one person is not supposed to serve as a hedge,
as the check in our checks and balances.
It shouldn't come down to one person at any point in time.
What does that tell us?
That there's too much power in too few hands.
That's the key takeaway here.
If there's a lesson to be learned here, that's it.
The problem is, I'm afraid it's going to be lost
among
the tragedy
and the upheaval and the political jockeying that is certainly going to occur.
There's too much power in too few hands.
There is no reason
for the loss of one person
to jeopardize the rights of that many people.
Something is wrong. Something is broke.
So how do we fix it?
That's the question.
And it's funny.
I recently saw somebody talking about this channel.
They said, all I really do is map out some key points that I want to hit
and then wait for current events to be able to make me talk about those points.
When you say it like that, it sounds like it should be a whole lot easier.
But I mean, that's pretty accurate.
I mean, to be honest, that kind of is the way I do things.
My current path started
with
the video where I was talking about a political compass.
And it's supposed to end here.
Now, I had no idea we were going to have such a
powerful illustration of the point.
But yeah,
I mean, that occurred.
When I made that video,
a whole bunch of people
took that test.
A lot of them posted it to Twitter,
posted the image,
showed where their little dot was.
Pretty much everybody,
I mean all the ones I saw,
were
bottom left.
How far left, how far bottom,
that varied a little,
but bottom left.
So
why
are there so many debates down in the comments section?
You know, using that bus analogy,
we're all
headed in the same direction, we should be able to ride the bus together.
See, this is a little different
because
there's two main groups of people
that watch this channel.
Those who are
in favor,
very supportive and very active
in electoral politics,
and those who believe that
more tangible action
is how you actually
create change.
But both
headed bottom left.
The uh,
the thing is here, we're not talking about that bus analogy,
not the same way anymore.
What we're talking about is two buses that decided to take different roads to get to
the same spot.
And there's uh,
a lot of bickering between the buses at times.
The comments section, it's
entertaining to watch.
Here's the thing,
right now
I'm sure that those who are heavily into electoral politics are looking at those who aren't
and are like, how's that protest vote going?
Or don't you wish you voted? Or whatever.
Because this,
this had such a big impact.
And this is a tangible result
of the last election.
Electoral politics is important.
That's, that's the message there.
And there's also the belief that
you know, you're never just going to be able to ignore the government and do things your own way.
Now on the other side,
you have
them looking
at those who are in
electoral politics and saying,
yeah, how'd that work out for you?
Doesn't look like it
protected your rights very well.
And
ultimately
wanting to say there's no way you can vote your way out of this.
And there's some truth to that too.
Trump
is the culmination
of years of this system consolidating power
and becoming more and more authoritarian.
All it took was for somebody
with his ilk
to get into office
and we're here.
You probably can't vote your way out of this.
I'm not going to say it's impossible, but I'm going to say it's highly unlikely.
Those who are on the bottom left and those who vote that way,
these are going to be supporters of
you know, Jim Perlman, Shaheed,
AOC, these types, right?
If you want that to progress,
if you want to get those kind of candidates,
not just do you have to battle the conservatives,
you have to battle those within the democratic establishment.
You know who you need to do that?
People who are really good
at obtaining immediate tangible results.
The people who are good at mass networking.
The people who really
aren't that much into electoral politics.
Because if you want those type of people getting up there,
you're going to need them.
And those who,
let's just say aren't fond of those type of politics,
yeah,
yeah,
yeah, but at the same time,
wouldn't it be nice to have some breathing room?
I think everybody watching this channel knows
that four more years of Trump is going to be
chaotic, to say the least.
If you want to build that separate power structure,
you want to go that way with it.
It would be nice to have some breathing room.
At the very least, it would be nice to have people
in electoral politics who are fighting a holding action,
trying to stop the government from becoming more and more intrusive,
more and more powerful.
Would be nice.
It's a diversity of tactics.
This isn't one bus
with two people on it getting off at different stops.
Those who are into more direct forms of organizing
need to understand that most of the people
who are into electoral politics that are watching this channel,
if they could, they would vote in the system you want.
They believe that's the way to get there.
And those who are just very much in favor of voting,
you're looking over at them.
Understand they're trying to build what you want without the government.
It's two buses headed to the same spot.
I'm not sure why we're so eager to run each other off the road.
I understand there are significant differences in philosophy.
But at the end of the day, the goal is the same.
For most people watching this channel.
Most people, the differences are pretty small.
Right now, both of these groups,
they need to start working to take the power back.
That needs to be the goal.
Because if one person can be lost
and in peril this many people's rights,
imagine what's going to happen when you have somebody
who is willing to force a lot of people out to get their way.
There's too much power in too few hands.
And yeah, we absolutely need those other power structures
that we can rely on.
We need those local organizations that can see us through
and that we can grow from.
But right now, we certainly need people fighting a holding action.
We need people out there trying to stop the creep.
Because that's what it is.
It's a slow movement
that grants more and more authoritarian power to the government.
Even if Biden wins, it's not over.
It's not over because another Trump can win in four years.
It doesn't end.
We have got to limit the power.
We have to disperse it at the very least.
Get it in more hands.
Because this is unacceptable.
This is a recipe for disaster.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}