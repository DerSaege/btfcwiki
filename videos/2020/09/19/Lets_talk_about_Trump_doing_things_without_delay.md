---
title: Let's talk about Trump doing things without delay....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zvwa9_ppJts) |
| Published | 2020/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States pushing for a quick appointment without delay and Senate is willing to oblige.
- President has a history of hiring individuals, praising them, then firing and criticizing them publicly.
- Beau questions the lack of thorough vetting for a lifetime appointment to the Supreme Court.
- Criticizes Senate for potentially forsaking their constitutional obligation by rushing the appointment.
- Calls out Senate Republicans for prioritizing power consolidation over the well-being of the people.
- Suggests that Senate is too eager to please the President at the expense of upholding their duties as a separate branch of government.
- Points out the hypocrisy of Senate's swift action for power-related matters compared to issues affecting the general population.
- Implies that Senate Republicans' actions are noticed and may influence public opinion and decision-making.

### Quotes

- "You're willing to undermine the Constitution and do whatever that man in the Oval Office says as long as you can get his crumbs."
- "When it comes to helping out the little guy, you don't seem to care very much."
- "Y'all are incredibly active. But when it comes to helping out the little guy, you don't seem to care very much."
- "Don't think we don't notice that when it comes to shoring up your power, all of a sudden, y'all are ready to go."
- "It's crystal clear. You're willing to undermine the Constitution and do whatever that man in the Oval Office says as long as you can get his crumbs."

### Oneliner

President rushes Supreme Court appointment without proper vetting, Senate complies, prioritizing power over duty to the people.

### Audience

Activists, concerned citizens

### On-the-ground actions from transcript

- Hold elected officials accountable for their actions and decisions (implied)
- Educate others about the importance of a balanced government and the responsibilities of each branch (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how the current administration and Senate are prioritizing political gains over constitutional duties, urging viewers to pay attention and take action.

### Tags

#SupremeCourt #Senate #Constitution #PowerPrioritization #Accountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're gonna talk about doing things in a hurry,
getting stuff done, being productive,
doing stuff without delay and fulfilling our obligations.
We're gonna do this because the President
of the United States is going all in on this appointment.
He wants it done, he wants it done now,
without delay, he says.
And I get it, it makes sense.
It makes sense from his point of view.
And the Senate, they seem more than willing to oblige him.
That makes sense to me too.
I get it, I understand that point of view too
because the President has a long and illustrious history
of choosing the right person for the job.
That's like his thing.
It's what he's done his entire life,
like he did with Michael Cohen or Steve Bannon
or Rex Tillerson or John Bolton or John Kelly.
These are all people who he hired, thought were wonderful,
and then had to fire and go on Twitter
and basically call incompetent and bully
because he can't accept responsibility for his own actions
and poor hiring practices.
Then there's another list of people who he hired
and thought were wonderful and he had to fire them
but didn't really go after them too much on Twitter.
And then there's also the people who he hired,
he thought they were wonderful,
and they wound up with mug shots.
As I said, the President is really good at this.
I'm sure that these are just flukes.
And there's no reason to worry about vetting a candidate
recommended by a person with a long and illustrious history
of being an utter failure when it comes to hiring.
There's no reason to worry about vetting them,
especially considering it's only a lifetime appointment.
Why would we need to worry about this?
It's only going to matter for, I don't know,
the next three decades.
We'll be all right.
It's totally okay for the Senate to forego
their constitutional obligation to consent and advise.
Makes sense to me, I get it, I get it.
It's not that those in the Senate just don't have a spine
and are putting their party before their country
and don't understand that the purpose of the Supreme Court
is to interpret the Constitution
and not actually make policy.
Maybe they don't understand that.
But anyway, it makes sense because the Congress,
the legislative branches,
it's not like they're a co-equal branch of government
who are supposed to serve as a check on executive power
or anything.
It's not like that's how the Constitution
was actually laid out.
But it was.
And perhaps now would be the opportune time
to try to reclaim some of that power
right before the election.
Because right now, Mitch McConnell,
there's a whole bunch of people wondering
how when one Supreme Court person is lost,
you're ready to go.
However, when 200,000 Americans are, you don't care.
And this goes for the rest of the Republicans in the Senate.
Don't think we don't notice
that when it comes to shoring up your power,
all of a sudden, y'all are ready to go.
Y'all are incredibly active.
But when it comes to helping out the little guy,
you don't seem to care very much.
We see it.
It's crystal clear.
You're willing to undermine the Constitution
and do whatever that man in the Oval Office says
as long as you can get his crumbs.
We got it.
We understand.
And believe me, it's influencing some decisions.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}