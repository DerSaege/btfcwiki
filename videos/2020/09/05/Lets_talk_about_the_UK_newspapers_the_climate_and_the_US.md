---
title: Let's talk about the UK, newspapers, the climate, and the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-bWQoGUZ45w) |
| Published | 2020/09/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Groups in the UK are blockading a newspaper printing facility to prevent newspapers, especially those with climate misinformation, from being distributed.
- Outlets impacted by the blockade include News Corp publications like the Sun, Times, Sun on Sunday, Sunday Times, Telegraph, and Mail.
- The blockade is a response to the facility printing content that downplays the seriousness of climate change.
- A spokesperson mentioned the mainstream media profiting from clickbait culture that spreads misinformation and incites hate.
- Generation Z in the US views climate change as a significant issue, with 26% believing it cannot be stopped and 49% thinking it can only be slowed.
- Beau warns that the world is nearing a tipping point with regards to climate change, urging for immediate action to avert a disaster movie scenario.
- In the US, ongoing issues from the past are hindering progress towards equity and equality, preventing the nation from joining the fight against climate change.
- Beau draws parallels between the US's current situation and World War II, where the US lagged behind Europe in taking necessary action.
- The US's willingness to make critical changes is pivotal in combating climate change and catching up with other nations.
- Beau concludes by explaining that the blockade in the UK aims to send a message peacefully, underscoring the urgency for global action.

### Quotes

- "If we are to sort out this mess we're in, the mainstream media must stop profiting from clickbait culture."
- "We are past the tipping point on some pieces of it."
- "We're still dealing with issues from the 1860s and 1960s, while there are people in other countries looking to the 2060s."
- "Europe, they're already in the game and they're waiting for us to catch up."
- "People are trying to prove a point."

### Oneliner

Groups in the UK blockading a newspaper printing facility to halt climate misinformation distribution, urging global action against climate change.

### Audience

Activists, Climate Advocates

### On-the-ground actions from transcript

- Contact local newspapers to ensure they are not spreading climate misinformation (implied)
- Join or support climate action groups in your community (implied)

### Whats missing in summary

Additional details on the impact of mainstream media's role in fueling misinformation and hate.

### Tags

#ClimateChange #MediaEthics #GlobalAction #Equity #Activism


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about why you might
not get your newspaper if you live in the UK tomorrow, the climate,
what this has to do with the US and moving forward.
Right now as I'm filming this there are a couple groups of people who are
blocking off the entrances to a facility that prints newspapers in the UK.
Most of the impacted outlets are from News Corp but not all of them.
It looks like the impacted outlets are the Sun, Times, Sun on Sunday, Sunday Times,
and then Telegraph and Mail as well.
The reason they're doing this is because they have identified this facility as printing.
A lot of outlets that kind of plant the seeds of climate denial.
Oh it's not really a thing we don't need to worry about it.
Oh it happens but it's natural it has nothing to do with humanity.
We shouldn't make any changes we just need to make that money.
One of the spokespeople said,
If we are to sort out this mess we're in, the mainstream media must stop profiting from clickbait culture
that is swimming in misinformation that makes us hate our neighbors,
suspect foreigners and vulnerable groups, and rally the nation into action.
Nice, nice.
World, I would say world.
Here in the US there was recently a study of Generation Z.
It found that 26% felt that climate change could not be stopped.
49% thought that it could be slowed but not stopped.
That's a pretty huge chunk.
That's a pretty big chunk of people right there.
And the thing is they're not wrong.
We are going to fill it.
We are past the tipping point on some pieces of it.
We haven't crossed over the tipping point for it to turn into a full-blown disaster movie.
But we're not that far off.
So because of that you have people now willing to take pretty drastic action.
The reason I want to talk about this, because I don't often cover stuff that happens outside the US,
but this relates to the US in a really important way.
Right now in the US we have actions like this going on.
We have stuff like this happening.
Most of them are dealing with centuries-old issues.
Stuff that should have been resolved in the 1860s or the 1960s at the very latest.
But they're still going on because people in this country want to kick down.
Want to kick down at their neighbors, at the vulnerable groups.
We haven't moved past that yet.
And while we're still dealing with issues from the 1860s and 1960s,
there are people in other countries that are looking to the 2060s.
It would be just great if the people in the US, if we could rally this nation into action.
Get a little equity and equality going on quickly.
Resolve some of the issues that have been plaguing this country since it was founded.
Get to where we need to be so we can join this fight.
We are way behind the times.
And the United States and the attitude of the people in the United States
and their willingness to make the changes necessary, it's critical to winning.
This is very similar to World War II in a way.
Europe, they're already in the game and they're waiting for us to catch up.
It'd be great if we could oblige.
So if you are somebody in the UK, that's why you may not get your paper today.
People are trying to prove a point.
My understanding is that they're doing it in a completely non-violent fashion, which is awesome.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}