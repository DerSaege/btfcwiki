---
title: Let's talk about the Belleau Wood and Trump's remarks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kpiT1dcus4Q) |
| Published | 2020/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the forgotten battle of Belleau Wood and its significance to the Marines.
- Mentions how the battle is part of Marine lore but largely unknown outside the Marine Corps.
- Describes key moments from the battle, like Marines filling in the line when French forces retreated and a Marine leading a charge across an open field.
- Talks about the renaming of Belleau Wood to the Wood of the Marine Brigade due to the intensity of the fighting.
- Addresses the impact of President Trump's alleged remarks on modern Marines and their connection to their spiritual ancestors.
- Points out the difference in reactions between modern vets who understand political aspects of deployments and the emotional response to attacks on their heroes.
- Emphasizes the significance of the Marine Corps and their history, especially in the face of political indifference towards military personnel.

### Quotes

- "That tough guy line you've heard in a dozen action movies actually said."
- "Well, come on, do you want to live forever?"
- "The deadliest weapon in the world is a Marine and his rifle."
- "This is an attack on the very foundations of the Marine Corps."
- "For all of the talk about supporting the troops, they don't care."

### Oneliner

Beau delves into the forgotten battle of Belleau Wood, its impact on Marines, and the indifference of politicians towards military history and heroes.

### Audience

History enthusiasts, Marines, Veterans

### On-the-ground actions from transcript

- Honor the memory of forgotten battles like Belleau Wood by sharing its story and significance within your community (implied).
- Support veterans and active-duty military personnel through local initiatives or organizations to show appreciation for their sacrifices (implied).
  
### Whats missing in summary

Beau's emotional delivery and personal connection to the story adds depth and authenticity to the retelling.

### Tags

#MarineCorps #BelleauWood #Trump #MilitaryHistory #Veterans


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Bella Wood.
Obviously, because of the President's alleged remarks, although we're not really going to
talk about that much, we will get to why some people are going to ignore him and why it
deeply impacted others.
But mainly we're actually going to talk about Bella Wood because it's forgotten.
It's not really known outside of the Marine Corps.
In fact, I didn't know about it until a Marine, in fact the toughest guy at the table, until
he told me the story.
Until he filled me in on it.
To them, this is one of their defining moments.
It's up there with Khe Sanh, Chosen, Guadalcanal, Iwo Jima.
It's part of their lore.
But it is forgotten.
So we're going to talk about it.
I'm not going to narrate the whole battle, mainly because there was a whole bunch of
maneuvering and unless you understand the terminology, it's not going to make any sense
without a map.
So I'm going to hit the highlights.
Because while it is forgotten Marine lore to most people, a lot of it became American
lore.
So we can talk about the pieces that transferred over.
When the Marines first show up, a group of them is sent to kind of back up some French
forces.
When they get there, the French are coming off the line.
They're retreating.
Think about the image of the French military in the United States.
This is part of where that image comes from.
The French commander, they're like, you guys need to retreat too.
You need to get out of here.
And the Marine commander, retreat!
We just got here.
That tough guy line you've heard in a dozen action movies actually said.
And then the Marines fill in the gap, fill in the line.
Along the way, as this progresses, the first Marine to receive a Medal of Honor during
World War I at this battle.
Later on, they're trying to stop the Germans from coming around their side and they want
to secure an area.
The area is on the other side of this wheat field, this open field.
That's not good because you never know what's on the other side of it.
However, they did know what was on the other side of it.
A bunch of German machine guns.
Well, come on, do you want to live forever?
Actually said Gunnery Sergeant Dan Daly.
We remember the lines, we forget the battles.
He said that and then led his men in a charge across that open wheat field through that
wall of lead.
The battle was so ferocious and the Marines were so unforgiving that it is believed, this
isn't actually confirmed, but it is believed that the Germans referred to them as devil
dogs, a nickname the Marines accepted happily and still use today.
And it's not really fair to call it the Battle of Belleau Wood either because that's not
what it's called anymore.
Because the fighting was so vicious, the French renamed it.
It is now the Wood of the Marine Brigade.
Very defining moment for the Marines.
Now for a large portion of Trump's base, they're going to ignore this.
Fake news can't be true.
He wouldn't say anything like that, even though it lines up with the dozens of other times
he has disparaged American troops.
But if you look on social media, there's a whole bunch of Marines.
This was the last straw.
And that seems weird because it's not the first time he said something like this, right?
But there's a difference.
Many modern vets, they understand that a whole lot of their deployments are political.
They understand the economic reasons.
They understand the corruption.
There is nothing that will convince you of the failings of the system faster than being
the tool of that system's implementation.
So they understand that.
But this wasn't about them.
This was about their spiritual ancestors, their forerunners.
Those people who were fighting the good fight, those people who stayed put or charged ahead
when others fled.
That's the difference.
Because it's not about the person they know.
It's about their heroes.
You know, I'm sure there's another line from it.
Afterward, one of the generals said the deadliest weapon in the world is a Marine and his rifle.
You've probably heard that before.
This is an attack on the very foundations of the Marine Corps.
That's why a whole lot of people had the reaction that they did.
That's why a whole lot of people for them it was the last straw.
And in some ways, it may be useful to see that politicians, and this isn't limited to
Trump, they don't care.
For all of the talk about supporting the troops, they don't care.
And yeah, it's true for Trump, but it's true for a lot of others too.
It might be something we should keep in mind.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}