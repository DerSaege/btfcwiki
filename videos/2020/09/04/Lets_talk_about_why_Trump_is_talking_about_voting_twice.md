---
title: Let's talk about why Trump is talking about voting twice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Tfkbz-Nd88g) |
| Published | 2020/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump appears to be suggesting his supporters vote twice, first by mail and then in person.
- Trump's statements in Pennsylvania hint at concerns about losing the election.
- The tactic of encouraging supporters to vote twice could serve as an excuse for Trump if he loses.
- Encouraging supporters to potentially commit voter fraud shows a lack of concern for their well-being.
- Beau advises Trump supporters against following this illegal suggestion.
- Trump's actions suggest a refusal to accept responsibility for any potential election loss.
- The President's contradictory stance on mail-in voting and in-person voting raises concerns about his motives.
- Trump's fear of losing the election seems to be driving his controversial statements.
- Beau warns supporters that following Trump's advice could lead to legal consequences without Trump caring.
- The underlying message is a call for Trump's supporters to avoid engaging in illegal voting practices.

### Quotes

- "He's worried he's going to lose."
- "That's what it looks like to me."
- "He obviously does not care about you."

### Oneliner

Trump's double voting suggestion may serve as an excuse for potential election loss, showing a lack of concern for supporters' well-being.

### Audience

Supporters and voters

### On-the-ground actions from transcript

- Inform fellow Trump supporters not to follow Trump's suggestion to vote twice (suggested)
- Educate others on the importance of following legal voting procedures (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's controversial statements regarding voting practices and the potential implications on the election.

### Tags

#Trump #Election #VoterFraud #Responsibility #LegalActions


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about why the President of the United States, Donald J.
Trump, appears to be telling his supporters to vote twice.
I'm not covering this late, I'm not talking about North Carolina.
He did it again in Pennsylvania after his press people clarified what he was saying.
You know, because that's what they do, they clarify what the guy who always tells it like
it is says.
That's their entire job and they're very busy.
In Pennsylvania he said, sign your mail-in ballot okay, you sign it and send it in and
then you have to follow it.
And if on election day or early voting that is not tabulated and counted, you go vote.
And if for some reason after that it shouldn't take that long, they're not going to be able
to tabulate it because you would have voted.
It seems like what he's saying here is go mail in your ballot and then go to the polls
and check to see if you voted and then vote if it's not there yet and then if it gets
tabulated, if it shows up later or they try to tabulate it later, they won't count it
because you've already voted.
To be super clear, according to North Carolina, this is illegal.
They flat out said do not do this.
But to be honest, I don't think this is a miscommunication.
It's not the way it reads to me.
To me it seems like Trump's worried he's going to lose.
He's worried he's going to lose.
So he's trying to build in an excuse as to why he lost.
He's hoping his most indoctrinated supporters will do exactly what he's saying and vote
twice.
Then when their votes get tossed, he'll be able to say, see, look, the Democrats are
trying to steal the election.
They threw away all these votes and of course they were all going to vote for me.
See it's not me.
I'm not a failure.
I can't be a failure.
I'm spoiled and rich.
There's no way this is me being rejected at the polls.
It's the Democrats trying to steal it.
We need to contest the election.
Not to put too fine a point on it, but that's what it looks like to me.
It seems he's willing to risk the freedom of his biggest supporters, those people who
trust him the most, just so he can have an excuse as to why he loses.
So he doesn't have to accept responsibility for his failures like his entire presidency.
That's what it seems like.
I don't see a person who has gone out of his way to paint mail-in voting as not secure
and full of fraud, contrary to all evidence, to then advocate for it and then send them
to the polls anyway.
You would just send them to the polls to begin with.
If you want your vote to count, you have to do it in person.
This added layer of the mail-in vote, he's hoping those votes get tossed so he can claim
there was election fraud.
So he doesn't have to accept the results if he loses.
Trump's afraid he's going to lose.
That's why he's making these statements.
At least that's how it appears to me.
Anyway, if you are one of his supporters and you plan on voting for him, don't do this.
I feel like that should go without saying, but don't do this.
You'll probably get arrested and he's not going to care.
He obviously does not care about you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}