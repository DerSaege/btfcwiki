---
title: Let's talk about what Republicans must do by November 2nd....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_tYupMnkkGo) |
| Published | 2020/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges Republicans to think critically before November 2nd.
- Encourages Republicans to identify and agree with some policies from AOC and Pelosi.
- Acknowledges his criticism of President Trump but still finds common ground on certain policy aspects.
- Emphasizes the importance of advanced citizenship and thinking independently rather than blindly following a political figure.
- Warns against falling into a cult of personality and urges individuals to judge ideas based on their merits, not their political origin.
- Reminds Americans that representatives should follow the lead of the people, not the other way around.
- Advocates for individuals to think independently, make informed decisions, and not blindly follow a political party.


### Quotes

- "Ideas stand and fall on their own."
- "Don't fall in line. Think for yourself."
- "Judge these ideas, these policies on their merits."


### Oneliner

Beau urges Republicans to think critically, find common ground with opposing parties, and judge ideas on their merits rather than blindly following political figures.


### Audience

Republicans, Independents


### On-the-ground actions from transcript

- Identify and agree with policies from opposing parties before November 2nd (suggested).
- Think independently and judge ideas based on their merits (implied).
- Encourage critical thinking and informed decision-making (implied).


### Whats missing in summary

The full transcript provides a detailed perspective on the importance of critical thinking, independent decision-making, and avoiding blind allegiance to political figures or parties. Viewing the entire transcript offers a comprehensive understanding of Beau's call for advanced citizenship and thoughtful engagement in democracy.


### Tags

#CriticalThinking #Bipartisanship #IndependentThought #Democracy #PoliticalEngagement


## Transcript
Well howdy there internet people, it's Beau again.
So today's going to be a little bit different.
Today we're going to engage in a thought exercise of sorts.
Today we're going to talk about something that everybody, really, but specifically Republicans
should do before November 2nd.
Help them play out their part.
Help them do their part to make sure the United States gets on the right track.
To make sure the United States maintains at least some semblance of a semi-functioning,
semi-healthy representative democracy.
If you are a Republican, I want you to picture AOC and Pelosi.
Think about their ideas, their policies.
And I want you to come up with a few that you agree with.
I know, sounds weird right?
If you are new to the channel, if you do not know me, understand that I believe that President
Trump is quite literally the worst president in American history.
He's just awful.
That being said, I can think of a couple of major policy things that I agree with him
on.
I think the Second Amendment is important.
Probably more so than he does.
I think it was a good idea to reach out to Kim, try to normalize relations.
I think that was a good move.
Granted, I think he probably should have sent a diplomat because he was in way over his
head and that's why it failed.
But I think the idea was good.
Think about that.
I am somebody who believes he is literally the worst president in history.
And here's a failure of his.
I don't point to it and say, ha, he's even worse than I thought.
He just wasn't up to the task.
The idea may still be sound.
The United States requires advanced citizenship.
You have to think for yourself.
And if you believe your chosen politician is always right and therefore their political
opposition is always wrong, you aren't part of a political party.
You're part of a cult.
Ideas stand and fall on their own.
Doesn't matter who says it, who presents it, who came up with it.
The idea is either good or bad.
Stands on its own merits.
So if you can find nothing that you agree with AOC or Pelosi on, what does that tell
you?
Are you judging their policies, their ideas on their merits or are you judging based on
where they originated?
If you believe the president is always right, do you really agree with his policies or have
you fallen for a cult of personality?
If you can't come up with anything that you agree with when it comes to any Democrat,
I'm going to suggest that your decision making is compromised and that maybe you shouldn't
be making any decisions for a while because you're under somebody's spell.
Because you are part of that cult of personality.
If the United States is to remain, we have to remember that there are representatives,
not our rulers.
They follow your lead, not the other way around.
That means you have to determine whether or not the idea stands or falls.
And you have to determine that based on the merits of the idea, not the political party.
If you are only looking at the political party, you are not doing your part as an American.
You are not doing your part as a thinking person.
You have given up and you are giving up the United States with it.
Don't fall in line.
Think for yourself.
Judge these ideas, these policies on their merits.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}