---
title: Let's talk about Florida reopening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-Q68nIHKPOw) |
| Published | 2020/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Florida is open for business with no restrictions, aiming to attract tourists despite the ongoing pandemic.
- The decision to reopen is primarily driven by the need for economic stability and attracting visitors for financial reasons.
- Politicians in Tallahassee have made decisions based on selective information to suit their agenda.
- Beau dismisses concerns about Florida's reputation and handling of the pandemic, suggesting that the state is already considered poorly managed in this regard.
- He mentions that the influx of tourists is seen as beneficial as they spend money and then return home, reducing the strain on Florida's healthcare system.
- Beau mentions the large older population in Florida, referring to it as "God's waiting room," and downplays concerns about their vulnerability.
- Capacity is expected to be an issue due to pent-up demand, but Beau assures that the crowded tourist areas are part of the appeal.
- Beau advises people not to trust politicians' statements from Florida and instead rely on medical experts for guidance.
- He urges visitors to make decisions based on expert advice rather than political influences.
- Beau expresses his willingness for a slower reopening if it ensures the safety and well-being of the people in Florida.

### Quotes

- "If you are from out of state, please, please, do not let anything a politician in Florida says ever influence any of your decisions about anything, ever."
- "Listen to the medical experts, listen to the people that know what they're doing, not the guy who didn't know how to put on his mask."
- "We don't need to become a nexus point for transmission."

### Oneliner

Florida opens for tourists without restrictions, prioritizing the economy over safety, urging visitors to rely on medical experts, not politicians.

### Audience

Travelers, concerned citizens

### On-the-ground actions from transcript

- Listen to medical experts for guidance on travel decisions (implied)
- Prioritize safety and well-being over economic interests when considering travel plans (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the decision-making process behind Florida's reopening and the potential consequences for public health and safety. Viewing the entire transcript gives a comprehensive understanding of Beau's perspective on the situation.

### Tags

#Florida #Tourism #COVID19 #Economy #TravelSafety


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about Florida
and vacationing in Florida.
Because in case you haven't heard,
we are open for business down here.
And I know what that means.
Y'all are already loading up, ready to hit the trail.
If you are headed to the Panhandle,
you're probably picturing those white, sandy beaches
and that emerald green water.
If you're heading to South Florida,
you're picturing those big cities
with all of those cultural attractions.
So much to see and do.
And if you're heading to the Northeast corner,
you're probably somebody really into history
and you're looking at those landmarks
that have been there for like 500 years.
And if you're heading to Central Florida,
I'm sure you have your reasons.
But if you don't know, our government has decided
that we are open for business, no restrictions whatsoever.
And we're good.
Now as soon as that news went out,
I had a whole bunch of people ask questions.
I'm gonna do my best to answer those questions.
The number one one was, Bo, are you worried?
And the answer is no, no, of course not.
If there's anything the rest of the country knows
about Florida and Florida man,
it's that we do things the right, safe
and cautious way all the time.
Our politicians in Tallahassee took a look
at all the information on all of the evidence,
except the stuff that wouldn't agree
with what they wanted to do
and decided this was best for everybody's wallet.
At the end of the day, Florida is a tourist state
and we are broke, we need your money.
So, y'all come on down.
Now, some of the other questions that have come up,
are you worried that this is gonna impact your case rate?
And again, the answer is no, no.
First, we're not worried about looking like a laughing stock
because we're already handling this really poorly.
So it's not like it's gonna make us more
of a laughing stock to be honest.
It is what it is, we're Florida.
Now, on top of that, it's probably not actually
gonna impact our case rates much
because most people only come here for a week.
So you're gonna come here, spend your money,
which that's the important part,
then you're gonna go home and be a drain
on the healthcare system there, not here.
This is win, win, win for everybody in Florida.
Now, one of the other questions
is about our aging population
because we have a lot of older people here in Florida
and am I worried about that?
And again, the answer is no, of course not.
For decades, everybody has referred to Florida
as God's waiting room.
This is just the government doing what governments do,
trying to be more efficient.
Nobody likes hanging out in a waiting room.
They're just trying to get people where they wanna go.
And it's good for the economy and that's what matters.
Now, the other question is, what about capacity?
Okay, now on that one, yeah, it's gonna be packed.
People haven't been able to come here for a while
so people are gonna come down, but let's be honest.
If you come here as a tourist, the fact that it's packed,
that's one of the reasons you come.
That's how you know you're eating a good restaurant
in a tourist area because all the other tourists
are eating there.
So yes, it's gonna be a little crowded,
but that's all right, don't worry about it.
We got room for everybody, we'll be all right.
Look, at the end of the day,
it's not like our politicians in Florida would lie
like they do all the time.
If you are from out of state, please, please,
do not let anything a politician in Florida says
ever influence any of your decisions about anything,
ever.
Listen to the medical experts,
listen to the people that know what they're doing,
not the guy who didn't know how to put on his mask.
I am not joking, that's who made this decision.
You do what's right for you and your family.
People in Florida will be fine.
The people who are pushing this are the people
at the very top who are concerned about their bottom line.
And I personally am totally okay with things being slow here
if it means that people are gonna be okay.
We don't need to become a nexus point for transmission.
Listen to the people who know what they're talking about,
not the people who from the very beginning sat there
and said, Florida's not gonna worry about this,
we have it under control because we don't.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}