---
title: Let's talk about the Redeye, Kentucky State Police training, and journalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=09weDGk0I-o) |
| Published | 2020/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau praises the high school newspaper Manual Red Eye for publishing materials and uncovered training materials from the Kentucky State Police.
- The training materials for peace officers are alarming, promoting a "warrior's mindset" and extreme aggression.
- The materials suggest law enforcement should view themselves as ruthless killers and prioritize violence over other tactics.
- Beau criticizes the training materials for being inappropriate and promoting dangerous concepts like disregarding policy and dehumanizing the public.
- He questions if officers trained under these materials have been retrained and calls for accountability.
- Beau stresses the importance of peace officers understanding their role in protecting the community rather than adopting a warrior mentality.
- He expresses concern about the loss of public confidence due to these training materials and their impact on current events like protests.

### Quotes

- "This isn't training. This is a justification for barbarism."
- "Regular employment of violence. That's how they intend on doing it. This is disturbing."
- "This is appalling on so many levels. This is not law enforcement. This is not being a peace officer."

### Oneliner

Beau praises a high school paper for exposing alarming training materials that push a warrior mindset onto peace officers, urging accountability to protect communities.

### Audience

Law Enforcement Oversight Organizations

### On-the-ground actions from transcript

- Contact local law enforcement oversight organizations for accountability and reform (implied).
- Join community efforts to advocate for peaceful policing and de-escalation training (implied).

### Whats missing in summary

Full context and emotional depth of Beau's passionate call for accountability and reform in law enforcement training.

### Tags

#LawEnforcement #TrainingMaterials #PeaceOfficers #CommunityOversight #Accountability


## Transcript
Well, howdy there, Internet of People.
It's Beau again.
So today we're going to talk about journalism,
and we are going to talk about some specific journalists,
in particular, the material they uncovered and published,
the effects and impacts that material is probably
going to have on them and on the community at large.
I want to start off by pointing out
that the material we're about to go over
was published by the Manual Red Eye.
That's a high school newspaper.
It's fantastic.
I was enthralled when I was able to elicit a response
from the principal when I ran a little unauthorized school
newspaper.
They got a response from the governor.
Good stuff.
So good, in fact, this channel is going to send them something.
Use it to get new cameras, lav mics, a gimbal,
whatever it is you need.
If you guys don't need anything, throw a party.
We don't care.
All right, so what did they get?
They got training materials from the Kentucky State Police.
They published it.
We're going to go through this little PowerPoint.
If you are ex-military or an ex-contractor,
you're going to recognize it.
It's gross, man.
And six people just laughed.
But the point is, these are talking points
that are designed for warriors.
They don't have any business being
taught to peace officers, this concept, this mindset.
I'm going to go through and hit the slides
I think are most important.
The whole thing will be linked down below.
If you want, you can follow it along.
OK, so first slide, it says warrior's mindset.
Next slide is the objectives.
The first slide with actual information on it
is the warrior's chosen path.
And from the very beginning, they
have to start to stretch definitions,
deny historical realities, so they can include themselves
in this warrior image that they want to cast.
Has a quote on it.
Wolves travel in packs, but eagles soar alone
because they are often out there alone as officers.
So they have to make it seem as though that's how warriors
normally operate.
And we know that's true.
That's why we always talk about our great SEAL individual,
our special forces person.
No, SEAL teams, SF groups.
Yes, you may fight on and complete the ranger objective,
though you be the lone survivor, but that's only
because everybody else is gone.
Warriors work in groups, squads, platoons, companies, so on.
This idea of separating the officer
and making them above everybody else,
and that carries throughout the whole presentation.
It creates the idea that the officer is not only
more ethical and more moral, but also
that they're above the law.
And that's a pretty dangerous concept.
OK, the slide everybody's talking about,
most people when they cover this,
the thin gray line slide.
Yeah, yeah, it's wildly inappropriate
to be quoting Robert E. Lee in law enforcement training
materials today.
That's not a good idea.
And that's what everybody's focusing on.
I would like to point out that the high school
students got it right.
They pointed to what was said.
It's not where the quote came from that matters.
It's the contents of the quote.
Manliness will carry you through the world much better
than policy.
That is a wild statement to see in law enforcement training.
If you've watched this channel for any length of time,
you have seen me deconstruct law enforcement use of force
incidents, right?
If something is bad, what do I often say?
Yeah, this is bad.
This isn't really the greatest thing in the world,
but it's within policy.
Because policy is what matters.
At the end of the day, that's going to determine whether or not
the department gets sued, whether or not it's legal.
Because if it's within policy, they
can always fall back on the idea that that's
what they were trained to do.
It certainly appears as though Kentucky State Police,
their policy is to disregard policy.
If they are being trained to disregard policy,
that's something that, well, every attorney
in the state of Kentucky needs to know about,
because it's going to impact a whole lot of lawsuits.
Again, I want to point out that the high school students properly
highlighted that.
Most of the coverage from the quote real journalist didn't.
I'd also point out that that's just a bunch of toxic garbage.
Manliness, really?
Anyway, the next important slide is titled
the progression of digression.
Says police tend to degenerate into the mold of the people
they are trying to control.
Now, I have objection with the use of the term
trying to control.
But maybe that's just a turn of phrase.
It's not.
We'll see later.
But I have an issue with that.
Beyond that, police tend to degenerate
into the mold of the people they're trying to control.
I think it's really important for the people of Kentucky
to understand law enforcement is being trained
to believe you are degenerates.
You are below them.
That's not good.
When you start looking at people that way,
you begin to dehumanize them.
And that's how excessive force happens.
If you can't tell, these training materials
are appalling.
On that same slide, it says loss of public confidence
is the ultimate sanction.
No comment on it at this point, but keep that one in mind.
There's another slide.
It says 1% at the top of it.
And it has a quote from Heraclitus.
I'm not going to read the quote because it doesn't matter
because it's not a real quote.
Heraclitus never said it.
This is made up.
But the idea behind it is that if there
are 100 soldiers on a battlefield, 10 of them
don't need to be there.
80 of them are just targets.
Nine are fighters, and we're lucky we have them.
But one, one is the real warrior,
and he's going to bring the rest of them home.
And of course, the cops are the real warrior.
Again, it's garbage.
It's not reality.
This is not something that is taught.
This is just made up.
I would point out, though, that I do find it interesting,
even though they managed to misattribute a quote,
they managed to use somebody who was opposed
to equality and democracy.
At least you're keeping with the theme here.
There's another one, looming thought.
And it says something to the effect
of that if we were left without doctors and engineers,
well, it would be difficult.
But if we were without warriors for a generation,
we would be doomed.
What?
No.
No.
If the world was without warriors for a generation
and had engineers and doctors, we would have Star Trek.
War is bad.
Violence is bad.
The fact that this needs to be explained
to adults who are in the profession of arms
is a little unnerving.
The soldier, the warrior above everyone else
prays for peace, unless that's not really what they are.
Then they have a slide, can you do it on fire?
Finally, finally they start to address what it actually means
to be a warrior.
And it says to make peace with your maker,
make peace with yourself.
Yes.
Finally they touch on it.
That's really what it means, because the way of the warrior
is death, which means you can't say stuff like,
I want to go home at the end of my shift,
because it doesn't matter.
It does not matter.
They go on another slide, what is combat?
And again, they have to adjust the definition,
because generally speaking, officers
don't engage in combat, not by any traditional understanding
of it.
So they define it as any situation
where individuals are confronted with death
and must respond to the situation.
So a car accident is combat.
A heart attack is combat.
They have to stretch the definition
and make it so vague that literally anything life
threatening becomes combat, because they're not warriors.
They're not supposed to be.
That's not an insult.
It's just not the job.
On that slide it says you must be the one to walk away,
because you've got to go home at the end of your shift.
But that's not what it's about.
Recent video, somebody said the best advice
that I'd ever gotten, asked what it was.
Said you're not going to make it,
because when you accept that, it becomes
less of an inevitability.
Then there's the slide that bothers me the most.
There's a slide titled violence of action.
Long time viewers, you all have heard that term.
And since I am skipping around, I
want to take the time to point out
there are no accompanying slides for planning, speed, surprise,
any of that.
Just violence.
Just violence.
Ruthlessness without anger, controlled aggression
without anger, able to meet violence with greater violence.
Yeah, that's not actually what wins.
It's not what wins.
Violence begets violence.
Professionally executed violence ends violence.
And if you want to do that, you're
going to need those other slides, those
that were just omitted.
Because you don't care about actually being a warrior.
You don't care about actually engaging.
You just want to use brute force.
That's what this is training.
Goes on to say that perceptions and actions are not hindered
by the potential of death.
Yeah, I mean, that's true.
In war, that's how that happens.
That's not appropriate in a civilian setting.
Because you're not warriors.
You're peace officers.
Yeah, that's a huge part of it.
If you want to be a warrior, the way of the warrior is death.
You need to be prepared to hesitate,
to make sure that your actions don't cause
the death of an innocent.
Because as a warrior, your job is to protect that community.
That's your mission.
And mission first, right?
So it's OK if you go to protect them, even from your mistakes.
Then in red and underlined, it says,
be the loving father, spouse, and friend,
as well as the ruthless killer.
Yeah, that's how we want to think of our law enforcement.
Ruthless killers.
Why do you all have that image?
Because it's in your training material.
I would also point out that this isn't a thing.
Yeah, it's an ideal to strive for and everything.
But there's a reason divorce rates are
so high among combat vets.
There's a reason it takes so long
to re-acclimate to a peaceful environment
when this is your mindset.
Because if you're going to be void of emotion, which
is one of the other ones here, a mindset void of emotion,
you don't have any.
You can't turn it on and off.
And warriors will tell you that.
You're not supposed to be warriors.
You're supposed to be peace officers.
On this, there's another quote.
The very first essential for success
is a perpetually constant and regular employment of violence.
Success, what's the goal?
To control the people.
Remember on that other slide.
Regular employment of violence.
That's how they intend on doing it.
This is disturbing.
Even more disturbing if you know where that quote came from.
In this brief presentation, they managed
to quote Heraclitus, Robert E. Lee, and Hitler.
That's how our law enforcement's being trained.
It is appalling.
The last slide, it all boils down to this.
This value mindset, well, it eliminates hesitation.
Yeah, it does if it's applied properly.
This isn't.
But let's say that it was.
It does eliminate hesitation.
Problem is, you're supposed to hesitate.
You are not in war.
You are not in combat.
You are on a city street.
Your job is to protect the people around you.
So for those who don't know and who didn't get the joke
earlier, this is basically a really bad rip off
of some stuff put out by a guy named Grossman.
Now, unlike many others who are critical of law enforcement
training, I have an immense amount of respect for Grossman.
I do.
His work is incredibly important.
It is invaluable to soldiers headed into combat.
It has no business, no business being taught to cops.
That isn't the mindset they need to have.
Now, for what it's worth, these high school students,
this high school newspaper, they got a response
from the government.
Basically, they were told, well, this
hasn't been used in a while.
That's fantastic.
That's not enough.
What was it replaced with?
Is it this same mindset?
Is it the same idea that the public are degenerates?
That law enforcement might one day stoop to their level?
Are they taught to regularly use violence to control the people?
Like some third rate regime.
These are questions we need answered.
It's old, doesn't cut it.
And the officers who were trained under this,
are they still employed?
Have they been retrained?
That one quote I said to file away,
loss of public confidence is the ultimate sanction.
It's why there's so many protests right now.
Because you have lost confidence of the people.
Because you view them as lesser.
That might have something to do with it.
I would take a long, hard look at yourself.
When I was a high school student doing the paper,
I pushed the envelope for the time.
I talked about the Fourth Amendment and lockers being
searched.
How the staff was allowed to have facial hair,
but we weren't.
Because I've been able to grow this since I was like 15.
They went through police records.
You have to wonder why.
Is it because they're tired of seeing people
their age with a chalk outline?
Are they trying to determine if the people they fear,
law enforcement, if they're as bad as they're made out
to be, as the perception?
Are they really ruthless killers?
Your training material confirmed everything that people fear.
This is appalling.
This is appalling on so many levels.
This is not law enforcement.
This is not being a peace officer.
This is what you are trained to understand
before you go into combat.
And it even does a poor job of that.
It was distilled down to talking points,
to cute little phrases.
This isn't training.
This is a justification for barbarism.
And it needs to go away.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}