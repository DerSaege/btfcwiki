---
title: Let's talk about Stanford's study about Trump's rallies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a6noqGHaMjk) |
| Published | 2020/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Stanford University conducted a study on the impact of Trump's rallies, linking 30,000 cases to just 18 rallies.
- The study suggests that 700 deaths may have resulted from these rallies, painting a grim picture of the consequences.
- Comparisons are drawn between the impact of Trump's actions and significant historical events like Oklahoma City and the Vietnam War.
- Despite the known consequences of his actions, there is still a chance of Trump being re-elected due to loyalty over country.
- Beau expresses concern over the long-lasting scars of Trump's leadership, stating that the country may not withstand another four years of the same.
- He points out the importance of loyalty to the people and community over blind allegiance to a political figure.
- The focus shifts to the need for responsible leadership and decision-making rather than seeking sensational headlines or social media victories.
- Beau warns about the potential worsening of immigration policies and overall leadership under another term of Trump.
- He underscores the impact of decisions made by the president on millions of lives and the importance of admitting mistakes in choosing leadership.
- Beau concludes by urging reflection on the consequences of supporting ineffective leadership and the implications for the future of the country.

### Quotes

- "His goal isn't to own the libs on Twitter. His goal is to run a country."
- "It's about waving flags rather than yard signs."
- "He's certainly not going to become more tame."
- "It's supposed to lie with your neighbors, who are the people that are missing."
- "If you can overlook the devastation that this man has caused, you never get to talk about the vets again."

### Oneliner

Stanford study links Trump's rallies to 30,000 cases and 700 deaths, raising concerns over loyalty over country and the lasting impact of ineffective leadership.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Reconnect with the community and prioritize loyalty to neighbors over blind allegiance to political figures (implied).
- Encourage open discourse and critical thinking about leadership choices within communities (implied).
- Support responsible leadership and decision-making by holding elected officials accountable for their actions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of Trump's actions and calls for reflection on loyalty, leadership, and the future impact on the country.

### Tags

#Trump #Leadership #Community #Responsibility #Election #Consequences


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about that study out of Stanford University.
Now for the record I had no idea that was going to be released. That's just a
happy coincidence
that it
relates to the earlier video.
If you don't know what I'm talking about,
Stanford did a study
and they tried to determine the impact
from eighteen,
just eighteen,
of Trump's rallies.
They tried to
quantify the effect
that it had
and how much it impacted the country.
The answer they came up with was thirty thousand.
Thirty thousand cases
likely
took seven hundred out.
That's pretty steep.
That makes him
about
four times as bad
as that guy
in Oklahoma City with the truck.
If you factored in all the events, all the rallies,
do you think it would reach the
total
from that day in September?
I'm willing to bet it would.
And if you added in
those that aren't here anymore
because he
downplayed it,
ignored the advice of scientists,
wouldn't lead,
I'd be willing to bet it would reach
the total of Vietnam.
In months,
in months he was able to accomplish this.
Worse than
our worst homegrown,
worse
than our worst ever,
worse
than our stiffest opposition.
None of this is really a surprise, to be honest.
I mean, we all knew this.
It was weird to see it in black and white,
but we all kind of knew this was happening.
The thing is,
that's not the scary part.
The scary part
is that
even with this and with people knowing this,
he still stands a chance of being re-elected.
Because it's not about looking out for the country anymore.
It's about waving flags
rather than yard signs.
It's turned into some
weird patriotic type duty
where you have
pledged your loyalty to a man rather than the country.
And that's messed up.
Because I want you to think about the scars
that Vietnam left on this country.
I've actually got a pretty nasty scar on my arm.
When it happened at the time,
it didn't seem that bad. To be honest, I looked down and said,
that's not that bad. I was wrong.
But at the time you don't notice it.
But the impacts from this man's ineffective leadership,
those scars are going to be around
for years.
I don't know that we can take another four years of it.
He's certainly not going to become more tame.
He's not going to become more rational.
He's not going to care about the country all of a sudden.
If anything, he's going to be more bold, more brazen, more careless
with the people
he's entrusted
to look over.
That's what's going to happen.
And that's just with this.
We already know
that on the immigration side of things,
he's going to get worse.
Miller's told us.
And it's because people stopped thinking.
They forgot where
their loyalty
is supposed to lie.
It's not supposed to lie with some guy in D.C.
It's supposed to lie with your neighbors,
who are the people that are missing.
The president is not an entertainer.
His goal isn't to create headlines to talk about.
His goal isn't to own the libs on Twitter.
His goal is to run a country.
And there are 360 million or so people
who are impacted by his poor decisions.
And because some people will not admit that they were wrong,
will not admit that a reality TV star
maybe wasn't the best choice,
we run the risk
of this man
being able to make those poor decisions
for another four years.
It's definitely something to think about.
It's definitely something to mull over.
If you can overlook
the devastation
that this man has caused,
you never
get to talk about
the vets again,
those lost again.
Because it
doesn't matter.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}