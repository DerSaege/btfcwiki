---
title: Let's talk about Texas and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qyKTyAl_XqY) |
| Published | 2020/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party in Texas is seeking to invalidate over 100,000 votes cast through drive-thru voting, fearing losing the state.
- They are attempting to suppress these votes to undermine the election and ensure their victory.
- This move by the Republican Party is seen as undermining the basic principles of democracy and representation.
- Beau questions if the GOP cares about representation or just about winning elections through undemocratic means.
- He criticizes the Republican Party for straying from its core ideas and principles, especially under Trumpism.
- The focus is on the Republican Party's fear of losing power and their attempts to manipulate the democratic process to maintain control.
- Beau points out that the GOP's actions are driving more people away from the party towards the Democrats.
- The fear of losing Texas seems to be pushing the Republican Party towards anti-democratic actions.
- Beau expresses concern over the erosion of democracy and representation in the country due to these actions.
- He suggests that the Republican Party's focus on power rather than principles is leading to a loss of support and trust from the people.

### Quotes

- "Is it about interfering with the democratic process because they're worried they're going to lose Texas?"
- "The Republican Party has lost its way."
- "Their worry is that the people's voice is going to be heard because they aren't for the people."
- "At this point, they are trying to turn this country into a nation that doesn't have a representative democracy because it doesn't have representation."
- "This move should encourage even more people to switch their vote from Republican to Democrat."

### Oneliner

The Republican Party in Texas is undermining democracy by attempting to invalidate over 100,000 votes, driven by fear of losing power and resorting to anti-democratic means.

### Audience

Voters, activists

### On-the-ground actions from transcript

- Join voter protection organizations to ensure fair elections (implied)
- Encourage voter turnout and awareness in Texas (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's actions in Texas, shedding light on the erosion of democratic principles and the importance of upholding representation in elections.

### Tags

#Texas #RepublicanParty #Democracy #Representation #ElectionFraud #VoterRights


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about what's going on in Texas.
The Republican Party there is attempting
to invalidate the votes of more than 100,000 people
who voted in accordance with what they were told
by government employees, by government officials.
One of the counties there set up little drive-thrus.
So you could pull in, get your identity verified,
get your ballot, fill it out in private, and hand it back.
OK?
Now, right before the election, the Republican Party
is taking it before a federal judge
in the attempts of invalidating all of these votes,
making sure that their voice isn't heard.
They're doing this because the Republican Party
is at risk of losing Texas.
Perhaps if the Republican Party has strayed so far
from its ideas that it's at risk of losing Texas,
maybe straying further isn't really the answer.
Because at the end of the day, they
can wave their flag all they want.
But if they seek to deny people representation,
deny people their voice, well, they're not
really honoring that flag.
What was the battle cry?
No taxation without representation.
I'm assuming the GOP is going to make sure that these 100,000
people aren't taxed, right?
Or is it really just about stealing the election?
Is it about interfering with the democratic process
because they're worried they're going to lose Texas?
The Republican Party is worried they're going to lose Texas.
If they weren't worried, they wouldn't be doing this.
This type of behavior, suppression on this scale,
100,000 people, this is why we have election monitors turning
their eyes to the United States.
This country used to have elections that were, well,
they went relatively smooth.
In fact, we were kind of the example held up
to the rest of the world.
But after four years of whatever the Republican Party has turned
into, now election monitors are looking to us for problems.
Maybe that's the issue.
Maybe that's why you had 100,000 people in Texas
ready to cast their vote against the Republican Party.
And that's what it's about.
They've picked an area that is very
likely to turn out for Biden.
And they're trying to invalidate those votes because they
want to undermine the election.
They're trying to steal the election
because they know they can't win unless they cheat.
All of that talk about election security and voter fraud
and all of that, none of it was true.
None of it was true.
They didn't care about that.
Their worry is that the people's voice is going to be heard
because they aren't for the people.
They aren't for the working class.
They are for the establishment.
Donald Trump is not anti-establishment.
He is the establishment.
He's a billionaire who hung out with all,
all of the political establishment.
The Republican Party has lost its way.
It's going to continue to stray until Trumpism is
removed from their platform.
This move should encourage even more people
to switch their vote from Republican to Democrat
because it doesn't matter about policy at this point.
Now you're talking about the basic functions of democracy
that the Republican Party is attempting
to undermine in Texas because they're
afraid they're going to lose one of their strongholds
because they keep undermining the Constitution,
keep undermining the founding principles of this country.
That's their fear.
That's their worry.
And it's a valid one.
And every time they try to take it another step,
there are more people who turn away from the Republican Party.
And they should because at this point,
they are trying to turn this country into a nation that
doesn't have a representative democracy because it
doesn't have representation.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}