---
title: Let's talk about why Trump probably fired Krebs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Cf6hra_Bpnc) |
| Published | 2020/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The President fired the person responsible for maintaining election integrity who debunked his baseless claims.
- The fired person was impartial and tried to balance loyalty to the President with doing his job.
- Concerns about the termination are mostly unfounded; it doesn't change much as the information exists elsewhere.
- The fired person's job involved processing information and providing assessments, but his termination won't lead to critical information loss.
- Experts, commissions, and agencies all agree that the President's election fraud claims are baseless and unrealistic.
- The position terminated doesn't have significant control over altering information outcomes.
- The President's likely motive for the firing is pettiness and vindictiveness rather than a strategic change in election outcome.
- Trump fired the person because he didn't comply with his baseless claims, showing his true colors.
- There's no need to attribute a sinister motive to the firing; it's likely a reflection of the President's pettiness.

### Quotes

- "He was fired because he didn't do what Trump said."
- "The most likely explanation is that he's just petty and vindictive."

### Oneliner

The President's firing of the election integrity official likely stems from pettiness rather than a strategic motive, with little impact on critical information.

### Audience

Political analysts, activists, voters

### On-the-ground actions from transcript

- Stay informed about election integrity issues and stand against baseless claims (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the President's decision to fire the election integrity official, revealing insights into the reasoning behind the action and its implications.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the President's personnel decisions from last night and what
the most reasonable explanation for them is because there's already some ideas floating
around and I think it may be a little bit simpler than that.
Okay so if you don't know, last night over Twitter the President fired the person who
is responsible for maintaining the integrity of the election.
This is a person who debunked a lot of the President's baseless claims because that's
his job.
By pretty much all accounts he was pretty impartial, tried to walk a fine line between
being the President's guy and doing his job.
Once the President started spreading less than accurate information he started pushing
back, knowing he was going to get fired.
So that's what went down.
There are a lot of concerns about his termination.
They're mostly unfounded.
You don't need to be concerned about this.
His job was a clearinghouse job.
He got information from a whole bunch of different places and processed it.
That's what he did.
He provided assessments and risk mitigation stuff.
The point is that terminating his employment does not change anything.
The documentation that he used all existed in other places.
So it's not like there's going to be some late night shredder party.
It's not like there's going to be a new person who is going to come to different conclusions
because the evidence, the information that he had came from other locations and they
still have copies of it.
This has been gone through over and over and over again.
The President's allegations are baseless.
The idea that what he is claiming happened on any large scale is just not really feasible.
It's not just DHS saying this.
It's not just the individual commission saying this.
It's not just the individual experts.
It's everybody.
What the President claims isn't realistic.
So I don't think that there's a big worry here or that we should be worried.
That position doesn't have a lot of control over the outcome of the information.
He could theoretically put somebody there who could cherry pick information and attempt
to cast it in a light favorable to the President.
But number one, that's going to take a lot of time.
And number two, there's really not enough supporting, even isolated incidents to support
the President's claims.
The most reasonable explanation for this is not that the President thinks that there will
be a change in outcome.
The most reasonable explanation is that he's just petty and vindictive.
That's the most likely explanation for what just happened.
This guy was fired because he didn't do what Trump said.
It's really that simple.
I don't know that we should put a more sinister motive to this because I really think it's
just the President showing his true colors and showing exactly how petty he is.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}