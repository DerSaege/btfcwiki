---
title: Let's talk about pardoning President Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c66IQQrmXeo) |
| Published | 2020/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Semi-official news confirmed Joe Biden as the next US president, sparking talks on the importance of the presidency.
- Some suggest pardoning Trump to preserve the office’s integrity, despite his lack of guaranteed peaceful transfer of power.
- Beau agrees in theory about focusing on preserving the presidency’s integrity over individuals.
- He refuses to overlook Trump’s actions like misinformation leading to deaths and attacks on constitutional values.
- Trump's divisive rhetoric, fear-mongering, and authoritarian tendencies almost tore the country apart.
- Beau calls Trump ineffective, lacking in strategy, and a danger if not held accountable.
- Concerns arise about future leaders emulating authoritarian tactics, potentially more polished and dangerous than Trump.
- Despite Biden's win, millions still support Trump without fully comprehending the risks.
- Beau warns of the ongoing threat of authoritarianism and the importance of investigating Trump's administration.
- Pardoning Trump could set a dangerous precedent, encouraging future tyrants with nothing to lose.

### Quotes

- "Pardoning him just encourages the next generation."
- "The President of the United States needs to be investigated."
- "Before we can reconcile, before we can heal, we have to know what damage was caused."
- "Those who want to keep some semblance of a democratic republic, they have to hit every time."
- "This is a horrible idea."

### Oneliner

The push to pardon Trump for the sake of preserving the presidency's integrity overlooks the dangers and lessons from his administration.

### Audience

US citizens, activists

### On-the-ground actions from transcript
- Investigate Trump's administration publicly to understand the damage caused (implied)
- Educate others on the dangers of authoritarianism and the importance of accountability in leadership (generated)

### Whats missing in summary

The full transcript provides a detailed analysis of the risks of pardoning Trump and the importance of public investigations into his administration.

### Tags

#Presidency #Trump #Accountability #Authoritarianism #Investigation


## Transcript
Well howdy there internet people, it's Beau again.
So it's been all of like five minutes since semi-official word came down that Joe Biden
will be the next president of the United States.
In those five minutes, people have already begun speaking about the importance of the
idea of the office of the presidency.
The establishment of that office.
What that office means to the American people and how we can't allow Americans to lose faith
in that office.
And in pursuit of this goal of preserving the integrity of the office of the president,
the best thing for the United States is to pardon outgoing president Donald Trump.
A man who as of yet still has not guaranteed a peaceful transfer of power.
No.
That's a horrible idea.
That is a horrible idea.
But the premise is sound.
I actually agree with it in theory.
With the argument that's being made as far as the office of the presidency being more
important than one person.
The idea that faith in the institution itself is important for the United States as it exists
today.
I agree with that.
I really do.
So that's all we're going to talk about.
We're going to wipe everything else off the board.
We're not going to talk about the willful misinformation that was put out that contributed
to the deaths of a quarter million people.
We're not going to talk about Trump's constant attacks on the Constitution and the very foundations
of this nation.
We're not going to talk about any alleged international crimes that he might have been
involved with.
Those aren't on the table for discussion right now.
We're just going to talk about the idea of preserving the integrity of the office of
the presidency.
Let's take stock of Donald Trump.
He's a man who employed divisive rhetoric, fear-mongering, that almost tore this country
apart in pursuit of nationalism and an overt glide into fascism.
That happened.
That's the reality.
That occurred.
Let's continue to take stock of him.
He's a joke.
He's ineffective.
President Trump is not a man I would consider an eloquent speaker.
He's not a planner.
He's not a grand strategist.
He's not a person who can build coalitions.
If he is not held accountable through legal means publicly so the American people actually
understand what happened, what message does that send to others who are like him?
Others who would pursue that same vein of authoritarianism?
They have nothing to lose.
We need to keep in mind that Biden didn't win in a landslide.
The American people did not cry out in one voice that we are not going to accept this.
That didn't happen.
Tens of millions of Americans still support him right now.
They don't understand the dangers.
They don't understand what he represents.
Maybe because he's so cartoonish.
But what happens when in four years or eight years the next generation of this brand of
authoritarianism is an eloquent speaker?
Is somebody who can build coalitions?
Somebody who smiles more and talks less?
Holds their cards a little closer to their chest?
Doesn't telegraph every move they're going to make on Twitter?
What happens then?
We might get a United States crying out in one voice, but it may be in favor of them.
It may be in favor of that candidate.
Biden himself almost won a second term, and had he won a second term, I am of the firm
belief he would have sought a third.
That's the road we were on.
We're not off of it.
We've just kind of angled off.
We're just steering towards the exit now.
The idea that that can happen here is still very real, because the Republican Party saw
that that rhetoric and that push for authoritarianism worked.
It worked.
In order to avoid that, we would have needed a landslide.
Had Biden won in a landslide, sure, pardon him, don't care.
The American people saw it for what it was.
But they didn't.
That didn't happen.
We are still in a very precarious place.
This idea is the worst idea in a long string of bad ideas related to embracing this particular
brand of authoritarianism with civility.
It's not going to work.
It isn't going to work.
The President of the United States needs to be investigated.
Donald Trump needs to be investigated.
His administration needs to be investigated.
If nothing is found, fine.
Cool.
Everybody was wrong.
Everybody who understands this particular type of politics was incorrect.
Cool.
Fine.
Then we can move on.
But if they're not wrong, then we need to have a public accounting of what occurred.
The American people have to see what happened, what was planned, and how it was shaping up.
Before we can reconcile, before we can heal, we have to know what damage was caused.
We have to know how it occurred.
Tens of millions of Americans still support him.
Pardoning him just encourages the next generation.
It just encourages the next would-be tyrant because they don't have anything to lose.
They know that the precedent will have been set.
If you attempt to subvert this country, well, as long as you leave at the end of your four
years.
Those people who want that form of government, they can hit and miss then.
They can continue to try, and they don't lose anything if they fail.
Those who want to keep some semblance of a democratic republic, they have to hit every
time.
They have to win every single time.
The other side only has to hit once.
They only have to get one candidate who can build those coalitions, who can smile for
the camera, who can shut up on Twitter.
That's it.
This is a horrible idea.
I am not somebody who believes in the idea of chanting, lock him up.
I don't really think that that's a good precedent to set either.
But there needs to be an investigation.
It needs to be public.
We need to know what happened.
No interest of national security.
No interest in protecting the office of the presidency.
Because if you want to protect that office, you have to keep others like him out of it.
Otherwise, that office isn't going to mean anything.
Because as soon as somebody gets in that can manipulate the public the way Trump wanted
to, that's it.
It's over.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}