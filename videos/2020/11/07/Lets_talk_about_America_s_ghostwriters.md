---
title: Let's talk about America's ghostwriters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jiPmoTOBSg4) |
| Published | 2020/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Day 732 of waiting for election results, trying to shift focus from election-related content.
- Incorporates Easter eggs like hidden quotes from movies or songs in videos.
- Mentions a profound line from Snow the Product in the Hamilton mixtape: "America's Ghost Riders."
- Describes "America's Ghost Riders" as referencing undocumented immigrants and individuals throughout history who did the work but didn't receive credit.
- Points out that marginalized individuals often contribute significantly to shaping America's history.
- Emphasizes that those without power often do the work while those in power receive the credit.
- States that the country's future depends on ordinary people rather than those in power.
- Acknowledges that individuals have more control over the country's direction than they may realize.
- Encourages viewers to recognize their role in shaping the country's history as "America's Ghost Riders."

### Quotes

- "America's Ghost Riders."
- "The reality is that it's normally people who have no power who are writing America's story."
- "If you feel like you don't have any control, if you feel like you don't have any power… America's Ghost Riders."

### Oneliner

Beau shares the concept of "America's Ghost Riders," underscoring the often uncredited contributions of marginalized individuals throughout history and encouraging viewers to recognize their power in shaping the country's future.

### Audience

Viewers

### On-the-ground actions from transcript

- Recognize and appreciate the contributions of marginalized individuals in shaping history (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of the concept of "America's Ghost Riders" and the importance of acknowledging the uncredited work of marginalized individuals in shaping history.

### Tags

#History #MarginalizedVoices #America #Influence #Community


## Transcript
Well, howdy there, internet people.
It's Bo again, and it is day 732 of waiting for the
election results.
Not really, but it feels like it, right?
I'm at the point now where I'm trying to record content that
isn't directly related to the election, that doesn't talk
about the results, because I can sense and read the tweets.
People are getting a little stressed, and I think maybe
People could use a different message at the moment,
something to kind of ease tensions a little bit.
You know, if you're a long time viewer of this channel,
you know I slide little Easter eggs into videos.
Sometimes it's something hidden on the shelves behind me,
the shirt matches, stuff like that.
Some people have picked up on the fact
that I will often slide quotes from movies or songs
into a video.
One of the most profound lines from a song that I think I
may have ever heard is from Snow the Product.
And I think I used it once in a video, but never to the
extent I feel I should, because it's worth a
video on its own.
It's from the Hamilton mixtape, and the song is Immigrants.
We get the job done.
The line is America's Ghost Riders.
Just that term, America's Ghost Riders.
In the context of the song, it's talking about immigrants,
specifically undocumented immigrants,
who kind of live in the shadows.
They don't get the credit they deserve for the work they do.
And yeah, I mean, absolutely that's true.
That is absolutely true.
But that term has so much more meaning than that really,
because it's true on so many other levels.
The United States has had ghost writers,
the people who did the work that didn't get the credit
since before the United States existed, from the Natives who were first their helping colonizers to
slaves to the the immigrants that came over to build railroads while some industrialists got all
the credit. You know, I looked, there was not a single guy that looked like the dude from Monopoly
driving railroad spikes. It didn't happen. But that's what's in the history books.
That's who gets the credit. The reality is that it's normally people who have no power
who are writing America's story, who are creating the history, that later on gets referenced
through important people, people who had power. Most times, those events, the people credited
in history books, they didn't actually do it. They're more like bookmarks. This is
the period that it happened, but somebody else did the work. And it's always like
that in this country, even today. Even today, that's how it plays out. And typically,
The further away from the levers of power the person is, the less credit they get and
the more work they do.
And they're rarely recorded the way they should.
You know, we always talk about the general that did this.
The general was nowhere near the battle most times.
Came up with a plan.
Somebody else executed it.
Somebody else put in the work.
And this is true about everything, everything that has happened in this country.
And I think that may be something that people need to hear right now, because at the end
of the day, Biden, he's just the important person that's going to get the credit.
If the country is going to be turned around, it's going to be turned around by us, by
those who aren't holding on to the levers of power.
If Trump gets re-elected and destroys the country, it's because we let him.
At the end of the day, if you feel like you don't have any control, if you feel like
you don't have any power. If you feel like this country is just spinning out
of control and there's nothing you can do about it, understand the entire
country's history is there because of people like you. America's Ghost Riders.
Those that didn't get the credit but in the end end up doing all the work. Anyway
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}