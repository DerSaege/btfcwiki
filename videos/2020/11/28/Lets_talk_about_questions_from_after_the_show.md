---
title: Let's talk about questions from after the show....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MMzPaS4vM4Y) |
| Published | 2020/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau did a live stream where he answered questions from Twitter, but didn't get to them all.
- Beau shares his thoughts on living in different countries, conservatives salvaging the GOP, and Trump's end game.
- He touches on modern feminism and the importance of masculinity not equating to weakness.
- Beau delves into politics, special operations forces, ranked choice voting, and the relationship between church and state.
- He also addresses the Trump administration, the census, US-Kurd relationship, and the future of US politics.

### Quotes

- "Masculine men are not afraid of competition. They don't mind equality with other people because competition is good."
- "Biden isn't a savior. Biden really isn't a progressive. If you want that deep systemic change, you can't change. You can't stop. You have to keep going."
- "I think by fact-checking the inarguable stuff, it may be better than trying to debate the opinions."

### Oneliner

Beau shares insights on various topics from living abroad to political strategies, urging continuous engagement for systemic change.

### Audience

Engaged citizens seeking political insights.

### On-the-ground actions from transcript

- Fact-check information shared by others online (implied).
- Engage in political activism to push for systemic change (implied).
- Keep informed and voice opinions on political matters (implied).

### Whats missing in summary

Beau's detailed analysis and recommendations on various political and social issues.

### Tags

#Beau #LiveStream #Politics #Masculinity #Feminism #RankedChoiceVoting #ChurchStateSeparation #Biden


## Transcript
Well howdy there internet people, it's Bob again.
So today I did a live stream.
And I normally ask for questions on Twitter.
If I don't get to them, I do a video like this.
Didn't get to them.
So I haven't read these questions,
so you're still getting an off the cuff answer.
But this looks like it's gonna become part of the routine
when I do live streams.
So, okay.
So,
if you had to live in another country,
which would you choose and why?
Ireland or Nicaragua?
I know those don't seem similar.
But I would want to live somewhere
where I could be left alone.
What are the real options for the conservatives
who want to salvage the GOP from a cult of personality?
I'm more curious and amused than invested in the answer.
So, they're gonna have to become
an ideological organization again.
Which means they're gonna have to move towards,
you know, maybe move back to talking about small government.
And let principles guide them rather than people.
Otherwise they will continually just be
the party of whoever's in charge.
And they won't wield any real long term benefit
to anybody except for themselves.
It'll be about power for power's sake.
45 seems to be flying by the seat of his pants.
What is his end game?
I don't know that he has one.
Personal enrichment?
I think that's really, it's really that simple.
Thanks, Bo, you're a gem.
Thank you.
Vosh, in one of his latest videos,
brought up some really great points about modern feminism,
about what modern feminism has for men
and how it's totally okay to be strong and masculine
and not a bad person.
And that you don't have to be a latte drinking,
to be a feminist.
Yeah, I have a whole bunch of videos on this topic.
Masculinity in the United States has become hypermasculine.
And that's not good.
That's the source of a lot of problems in the US.
There is that idea that if you want to be a man
who supports feminism, that you're going to be weak.
I would point out that a lot of feminists who were men
were like Medal of Honor winners.
It, that's made up.
That's made up by insecure little boys.
The ultimate irony being that it is those who
cast those who want equality as weak,
who are the ones that have the insecurity issues,
who are the ones who act like little boys.
Masculine men are not afraid of competition.
They don't mind equality with other people
because competition is good.
If they're better, then you have to get better.
And that's part of the masculine stereotype
is that constant drive to improve yourself in that way.
That's not to say that femininity doesn't have
the same thing, it's just accomplished in a different manner.
So yeah, I have, I do, I have probably a dozen videos
on this topic.
What size pitchforks do we need
and where can we purchase them?
Buy, buy locally and think globally.
Do you agree that Trump administration
had to covertly sign off on this?
I'm gonna guess that's in reference what just happened.
I don't think they had to.
They likely gave a nod, but there is this image
that anything in the Middle East has to be approved
by the US or Russia.
That's not true.
There are three very powerful countries there,
Saudi Arabia, Iran, and Israel.
Any of them can act unilaterally.
They don't necessarily need a nod
from one of the major powers
because they're regional superpowers.
And yes, there's a follow-up statement to that
and that is what that was about.
That, I have a video coming out on,
so I gotta skip that one.
There's a whole bunch about what just happened over there,
a whole bunch of questions about that.
And we'll probably end up doing a whole video
as more information becomes available.
The real question that seems to be getting asked in general
with this is, is this going to precipitate something
before Biden takes office?
And I hope not, I hope not.
I would like to believe that Iran will be the adult
in the room and understand that things
are gonna change drastically in a couple of months
and show some restraint between now and then.
Because to escalate it and cause a bunch of chaos
just to have it deescalate in a couple of months,
it seems unnecessary.
But I don't know how that's gonna play out.
Yeah, I don't know how that's gonna play out.
Let's see, you're much older than I am, thanks.
Have things always been this bad?
Have things always been this messed up and weird?
Yeah, I mean, when you hear stories
about before you were born
or before you started really paying attention to things,
you get a sanitized version of it.
Yeah, it's always been messed up.
Trump is unprecedented in a lot of ways in the US,
but it's not anything new globally.
2020 has been a year, I'll give you that,
but it's always been a rapid developing situation,
rapidly developing situation that seems out of control
because it is.
Still waiting to hear what oil and filter,
Mobile One is what I normally use.
Oh, I get it.
Yeah, all right.
Let's see, outside of financial donations,
how can a Pennsylvanian best support our desired outcome
in the Georgia runoff?
She could probably phone bank.
That might be helpful.
What can we do to prevent the leasing of public lands
in the Alaskan wildlife refuge
that Trump seems bent on delivering to the highest bidder?
I don't know that it can be completely prevented
at this point.
If he pushes it through, he pushes it through,
but there will be the ability to regulate it.
And sometimes regulation can make things
to where they're not cost-effective.
That would probably be the strategy I would employ
if I was in Biden's shoes.
I don't know that he's gonna do that though.
Remember that Biden isn't as progressive
as many of the people who watch this channel.
He doesn't have the same views.
So I don't know that he's actually going to act on that.
Which civilians do you like for a SEC def?
I would like to see somebody
from the special operations community
as secretary of defense.
That's what I would like to see.
The main reason being our special operations forces
are incredibly misunderstood and underutilized.
There are a lot of times where we are deploying divisions
of men and women when we don't need to.
Our special operations forces,
the special operations community can take decisive action
when it's needed and not impact a whole bunch of civilians,
which is always the desired thing,
or should be.
I would like to see somebody high up
that actually understands how they can be used
to the greatest effect.
It would be great to say that we're not gonna have
any military operations,
but realistically, we're not there yet.
That should be the goal.
That's where we're trying to get to.
And using special operations in that capacity
would be a step in that direction.
I know that may seem weird from the outside looking in,
but even though they're the most elite forces
and all of that and all of the imagery that goes with it,
they're also the most surgical.
They're the most effective
without destroying the countryside,
which should be the goal.
And I don't know, I mean, conventional generals,
people that are in the military
don't necessarily understand the capabilities
of the special operations community.
I don't know that any civilian would.
So I think that's the next step to getting,
to getting to the point where we are not constantly
deploying tens of thousands of people all the time.
Who's winning skater of the year?
I don't even know what that means.
I don't even know what that means.
Your thoughts on the census.
Yeah, I get it.
It's important.
I'll be honest.
It hasn't really been on my radar.
It determines a lot in theory
about how money gets spent and stuff like that,
but it doesn't really in practice.
Corruption determines that in practice.
The little deals made behind the scene
determines that more than the actual census count.
There are some things that it comes into play for,
but I don't know that it's as important
as everybody's making it out to be.
See, about Somalia, my understanding is that
that was a, that that person was trying to lock down assets
because we're preparing to withdraw.
There's still a whole lot of information
that still has to come out about that.
We don't know exactly what went down there yet.
Do you think the US can rebuild its relationship
with the Kurds, with the new State Department?
I think they'll try.
I don't know that they're gonna be able to.
That's a huge task.
We really let them down.
I don't know that that's gonna be rebuilt.
It's probably an objective.
It's probably something they're gonna strive for,
but I don't know that they're gonna be successful at it.
You like learning from events,
so now the end of the Trump is in sight.
What lessons must the USA learn
and what changes need to come?
We're gonna do a whole video about this.
We're gonna hot wash the entire Trump administration
and some of the institutions and organizations.
We're gonna determine which ones are resilient
and which ones aren't, which institutions let us down.
What should our response as a species be
if the capital class refuses to do enough
to arrest climate collapse?
Wow.
I think that working class people
need to start paying attention to it
a lot more than they are.
As far as what we should end up doing, I don't know.
A lot of time is going to pass between now
and when people become motivated to act.
So we don't know how it's gonna play out.
There's too many variables to really come up with anything.
Let's see, prosecute or let bygones be bygones.
Can we crack the misinformation machine
without holding DT and company accountable?
I definitely think we need to get a baseline of facts.
So I think that there needs to be an investigation
into a lot of things.
I don't want to say now,
oh, these people need to be prosecuted.
I think it's a bad idea to find the person
and then find the crime.
Don't determine who you want to prosecute
before you have the information.
Now, if during the investigation,
criminal activity shows up, then yeah.
I mean, that's part of it.
But I would be very leery about setting the precedent
of using the Justice Department to go after predecessors.
Do the investigation and see where it goes from there.
Until then, I wouldn't talk about charges.
Let's see, would you talk about ranked choice voting?
Yeah, I've talked about it here and there
every once in a while.
I would suggest it's a good move.
That's where we need to head towards
because then we can break up the two-party system,
which tends to pit people against each other more
and make things about culture rather than policy.
So I think that's a good move.
And that's probably somewhere where we need to head.
Can you talk about separation of church and state,
evangelical megachurches and the IRS?
I'm gonna assume this is about removing tax-exempt status
and all that stuff.
It sounds good in theory.
You know, you have a bigoted church doing something
and you yank their tax-exempt status
because they're bigoted.
It sounds good.
But constitutionally, the government cannot impose
a purity test on a religious organization.
It won't make it.
I don't know that that's somewhere
I would waste political capital right now.
Now, the alternative to that would be
to yank tax-exempt status for all churches.
As a non-US British person,
what rights, responsibilities do I have
when I engage with US politics?
If you're talking about debate online, voice your opinion.
You know, there is that common thing,
well, you're not an American, you don't know.
Right, but they're from the outside.
Maybe they have a better view of things.
I don't believe, you're a citizen of the world.
Our decisions, what happens here,
impacts people in other places.
And I don't think that you would ever hear an American say,
oh, we shouldn't voice our opinion
about what happens in these countries over here.
So.
Do you think Trump will try something in the next week
since he was voted out?
I don't know.
I don't, I mean, he's gonna try stuff.
I don't know that he's gonna be successful,
and I don't know that Melania has anything to do with it.
Let's see.
Right-wing militias are getting ready.
How successful can they be against the A10?
Most of the right-wing groups
are very conventionally structured,
and that's not good for what they're intending to do.
So I don't, I do not believe that they have the logistical
and organizational structure necessary to accomplish
what they think they're going to.
Question, is there a path to dissolving
our two-party system in Congress
so that it's just congressman and congresswoman, not R&D?
I mean, I'd love to get rid of parties,
but that seems really unlikely.
Your best, your most likely thing
is gonna be ranked choice voting,
and then the parties have to form coalitions.
This is what happens in a whole lot of other countries.
Let's see.
To vaccinate or not to vaccinate?
Until I actually see information on this,
I'm not putting out an opinion.
Generally, I will tell you that I'm vaccinated
against pretty much like everything.
I am a strong supporter of vaccines in general,
and I have a video about this where I went through
and did some research on some of the ones
that are most controversial,
and maybe it's worth watching.
Now, it's been a while, but generally speaking,
I'm very in favor of vaccination.
You just have the worst timing.
The only live stream I've ever been able to catch
any of was the election.
I shouldn't have stayed up for that one,
but you rocked too hard for that.
Yeah.
Well, that was a really long one, so let's see.
Not a question.
Simply a thank you.
He's also good at name-calling.
I guess I am.
What do you think will happen with all of the followers
that trust the plan now that it has failed?
They'll probably change their theory,
and I don't think that it's going...
I think what people are waiting for is a moment
where everybody wakes up that was involved in that.
I don't think that's going to happen.
I think it's going to be a long, slow decline
rather than an event.
Let's see.
What is the best way for those on the left
to organize to push Biden further
less than FDR was pushed?
Also, can you please give a shout-out to...
OK, if you're on Twitter, go to Trev Ken,
and there's a post there.
OK.
Pushing him.
The same way that...
The same level of political engagement
that has been going on the last four years needs to continue.
Biden isn't a savior.
Biden really isn't a progressive.
If you want that deep systemic change, you can't change.
You can't stop.
You have to keep going.
And I think there's enough people that realize that,
that may keep it up.
And Biden may be pushed.
Again, that asylum, his decision when it comes to asylum
and the announcement there, that's big.
That's big.
And I know that's not a policy he would have supported
just a few years ago.
So he can be moved.
Whether or not he can be moved by individuals
or they have to be people in his select circle,
that we don't know yet.
But I would certainly believe it's worth the attempt.
Do you have Trumpers in your personal life
and how do you handle that?
I get frustrated.
One thing I recently tried was actually sitting down
and watching Fox News with my phone in hand
and fact-checking.
Not the opinions, but the basic facts, the simple stuff.
One of the ones that came up and they're like,
we're turning a corner.
Everything's getting better.
And this was just a couple of days ago.
I'm like, no, that's not true.
Here are the numbers.
We're at record highs.
I think that by fact-checking the inarguable stuff,
it may be better than trying to debate the opinions.
Show that they air stuff that is just factually incorrect.
Let's see.
Watched.
You were cool.
Cool as always.
Hope and support from the UK.
Thank you.
What do we actually do to hold Biden's feet to the fire
once he's in office?
I'm going to end up doing videos on this, especially
after the inauguration and being way more specific.
But generally, everything that we're doing now,
we have to keep doing.
The difference is we do it more in an encouraging fashion
than one of outright resistance.
That would be my suggestion.
What are your thoughts on the resurgence of Marxist politics
in the US?
I didn't know there had been.
I know the term gets thrown out a lot,
but I don't see people calling for the seizure
of the means of production.
We don't have a real leftist party in mainstream politics
in the US today.
There are leftists.
There are leftist groups, but they're not mainstream.
I wouldn't call it a resurgence.
It's an idea, and it's going to stay kind of on the outskirts
for a while.
Again, without major events, everything's incremental.
Let's see.
All right, so those were all the questions, at least I think.
Twitter is kind of hard to navigate at times.
All right, so there we go.
I don't know.
We'll probably do another live stream
when we hit half a million subscribers, which hopefully
won't be too long.
And other than that, I hope you enjoyed your holidays,
and everybody can just kind of relax for a little bit.
Trump is facing defeat after defeat
after defeat in his attempt to subvert the election
through the courts.
So hopefully everybody's mind is set at ease by that.
If I missed any, it's either because I'm
going to do a video about it specifically,
or I literally just missed it and forgot, or I didn't see it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}