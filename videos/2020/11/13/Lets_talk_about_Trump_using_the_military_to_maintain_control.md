---
title: Let's talk about Trump using the military to maintain control....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yb2iRT6BU34) |
| Published | 2020/11/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the hypothetical scenario of a sitting president using the U.S. military to maintain control after losing an election.
- Explaining the necessity of actual control of the military and the challenges in achieving it.
- Detailing the logistics required for such a scenario, including planning, surprise, and violence of action.
- Analyzing the lack of manpower and material needed to pacify a country the size of the United States.
- Emphasizing that the current administration lacks the resources and logistics to make this scenario a reality.
- Stating that while chaos is possible, the realistic possibility of using the military to maintain control after losing an election is slim.
- Advocating for total mobilization and unity in political movements as a way to resist any potential subversion of government.
- Stressing that government authority is based on consent of the governed and not just might.
- Urging the media to avoid sensationalizing unrealistic scenarios that could heighten tensions.

### Quotes

- "Government authority is an illusion."
- "At the end of the day, any government should have consent of the governed."
- "If he's going to attempt to maintain control, he's going to do it through the courts."
- "Realistically, not just Trump, but any president, using the military to maintain control of the population after losing an election, it's not a realistic possibility."
- "We need everybody. Everybody along the spectrum."

### Oneliner

Beau explains the implausibility of a sitting president using the military to maintain control after losing an election, stressing the importance of unity in political movements and the illusion of government authority.

### Audience

Concerned citizens, political activists

### On-the-ground actions from transcript

- Mobilize all segments of society for political resistance (suggested)
- Advocate for unity and cooperation among groups with shared goals (suggested)
- Stay engaged in political movements to prevent defeat (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the logistical, manpower, and material challenges that make it improbable for a sitting president to use the military to maintain control after losing an election.

### Tags

#ElectionSecurity #UnityInPolitics #GovernmentAuthority #PoliticalResistance #MediaResponsibility


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the elephant in the room.
We're going to talk about something I have attempted to avoid talking about
because I don't actually think it's beneficial in most ways to talk about it
and treat it as a realistic possibility.
It's one of those things that the media is currently leaning into
and treating as if it's something that realistically could happen.
And I don't think they should be.
In fact, I personally believe it's pretty irresponsible of them to do that.
And I think they know that it's not realistic
because they're not bringing in the experts who know about it to say,
yeah, this isn't really going to happen.
So today we're going to talk about what a sitting president would
need to use the U.S. military to maintain control
after he or she loses an election.
Let me start by saying, in context of today and what's going on right now,
if Trump is going to attempt to subvert the election, undermine the Constitution,
he's going to attempt to do it through the courts.
He's not going to use the military.
And we're going to go through the logistics necessary
to pull off what is being suggested and kind of hopefully set people's minds at ease.
Okay.
In this hypothetical scenario, you have a sitting president.
He has lost his popularity contest.
He is outgoing.
He doesn't want to leave.
Fine.
He wants to use the U.S. military to pacify the country in order to maintain control.
What does he need first?
Actual control of the military, real control of the actual military.
The media is making it seem as though these positions that were shuffled
around recently gave him control of the military.
That didn't happen.
Those are political appointees.
They're not even in the military.
That is civilian control of the military.
It's a concept we have in this country.
It's really important.
This is one of the reasons why.
If Trump or any president was to give the order to those appointees,
the next thing that has to happen is they have to tell the actual military,
people with stars on their shoulder, that that's about to go down.
They have to convince them to go along with it.
In this case, if we're using today as an example,
those political appointees would be meeting with flag-rank officers
who have already expressed their displeasure over much, much less.
They didn't want active-duty military involved in suppressing civil
disturbances a few months ago.
They didn't want them down at the border.
They actively undermined these measures, giving the people down at the border
orders to not actually have any contact whatsoever with those crossing.
Those political appointees have to convince them to mobilize the U.S.
military to subvert an election.
I can tell you right now, I would not want to be the person walking
into that room to try to explain that to them.
I don't know that that person would leave.
It's not likely.
He doesn't have the active-duty assets required to make it happen.
So his other alternative would be to go to retired generals.
And there are a handful of retired generals who, A,
commanded the respect necessary to get the troops to go along with it,
and B, have the skill set necessary to make it a thing, to make it happen.
The person that fits the bill most, in my opinion, is General Thomas,
who has already actively and openly spoken against the idea of not even
using troops, but just casting that opinion,
casting that idea through words.
He got mad because somebody referred to the U.S. as a battle space.
This is a person who normally keeps their opinions to themselves.
But just the idea caused him to speak out.
He's not going to go along with it.
Those with the skill set to make this happen know the dangers of it,
and they're not going to participate.
So then Trump has to go to people who are less than qualified.
Historically, when this happens, the generals who were passed over
in that process, they go to the opposition.
In this case, in this hypothetical scenario, they'd go to Biden.
That would be the most lopsided strategic battle in history,
realistically, from the flag-rank officers needed to make this happen.
He doesn't have it.
That is step one.
Step two is the logistics.
So let's just forget about the fact that he doesn't have the people necessary,
the high-ranking people necessary to make it happen.
The logistics, what do you need?
Same thing you need for anything else.
Planning, speed, surprise, and violence of action.
That's what you need.
The Trump administration is not known for their planning capabilities.
Speed, once the decision is made, once people have been shuffled around
and you've got your little meeting with everybody smoking cigars
and drinking brandy, talking about being masters of the universe and all that,
the plan has to be enacted pretty much right then.
Otherwise, people get cold feet.
Otherwise, people talk to others and risk telegraphing the plan.
There's a whole lot of stuff.
It has to be done immediately.
It wasn't.
You need surprise.
In this case, it would be surprising the American population.
If Trump attempted this, would you be surprised?
No, of course not.
I mean, he might as well just put it on Twitter that he was thinking about it
or at least entertaining it.
Well, he did, actually, kind of, when he talked about the peaceful elect.
Anyway, the point being is that he doesn't have the element of surprise,
and you need that for this to go down, because the way it works is the person
who is taking power or is trying to maintain power basically has to walk out
and be like, hey, that guy who was going to take power or who was already in power,
he fell down the stairs, and now I'm in charge.
Don't worry.
It's just temporary.
We're going to have a new election, and everything will be back to normal.
That's how it works.
There has to be the element of legitimacy so it can't appear planned.
If it went down right now, it would certainly appear planned.
So that's out.
He doesn't have that, and then you need violence of action.
Against whom?
Even generals that don't have the skill set to actually pull this off,
they've read the manual.
They know that sending out troops and providing that security clampdown
is going to create resistance, not pacify the country.
So on the logistical side of things, the four things he needs
to actually make it happen, he has none.
He doesn't have the flag rank officers, and he doesn't have the logistics.
Now let's talk about material.
He doesn't have the numbers.
I have an entire video somewhere where I actually break down the numbers on this.
To pacify a country the size of the United States
with the population of the United States,
you would have to have an immense amount of manpower.
If we were to pull back every single active duty
troop from all over the world, deploy them domestically,
deploy everybody that's garrisoned in the United States,
activate the inactive and active reserves, and bring them all back,
we would almost have the minimum number necessary to pretty much
guarantee a failure.
The manpower doesn't exist.
And that isn't taking into consideration the just massive amounts
of variations in terrain and the fact that the United States has
more guns than people.
These are things that aren't really even factored into this.
At the end of the day, out of the stuff necessary to make this a reality,
the current administration has none.
They don't have any of it.
This isn't realistic.
Could he attempt it and cause a bunch of chaos?
Absolutely.
He's Captain Chaos.
He could certainly do that.
But as far as it getting anywhere, it's not realistic.
It isn't something you should be scared about, certainly.
It's not something you should be worried about.
Consider it as a thought exercise more than anything,
because it's good to think about things like this.
But realistically, given the current climate, it's not a thing.
If he's going to attempt to hold on to power and subvert the Constitution,
he's going to do it through the courts, not with the military.
OK.
So that being said, hopefully that puts your mind at ease.
There is a learning experience here, though.
What, uh, let's say it does happen.
OK, the percent of a percent of a percent of a percent of a percent.
And it does happen.
All of a sudden, we find ourselves outgunned, outmanned, outnumbered,
outplanned, right?
What do we need?
How do we resist the subversion of the United States government,
the destruction of the Constitution, absolute tyranny?
What do we need?
Who do we need?
We need everybody, right?
We need everybody, everybody along the spectrum.
Because when people think about soldiers,
they think about people holding rifles, right?
That's what they're really picturing.
They're picturing the guy out there in the field,
the person out there actually engaging the opposition.
In reality, that's only 10% of the military.
For every one person you have out there pulling a trigger,
you have 10 people typing on keyboards, building bridges,
both figurative and literal, running logistics, supplies,
all of this stuff, engaging in civil affairs, propaganda,
that kind of stuff.
You need all of it.
War is a continuation of politics by other means.
With that in mind, how should we approach politics?
We need everybody.
Everybody along the spectrum.
Yes, some people are going to advance further, faster,
but you still need everybody else.
You still need those people that make up the main body.
You still need those people in the rear.
That's how you build a movement,
rather than a bunch of individual moments.
Right now, and I'm hoping it's just due to everybody
being charged because of the election,
there is a lot of people,
a lot of different groups out there
who are generally aligned.
They're headed the same direction,
but they're fighting with each other instead of cooperating.
That should probably stop.
Hopefully this shows that, because at the end of the day,
if we're treating politics as war, we have to act like it.
We need everybody.
We need total mobilization.
We have to stay engaged.
Otherwise, we'll suffer defeat.
Now, I want to just reiterate, realistically,
not just Trump, but any president,
using the military to maintain control of the population
after losing an election,
it's not a realistic possibility.
It's not something you should be worried about.
It shouldn't be keeping you up at night.
Is there a slim chance that it could occur?
Yeah.
I mean, there's a slim chance that anything could occur,
but there are a lot of safeguards in place,
intentional ones.
And then there's the pragmatic considerations
of what it would take to actually succeed.
Again, he could cause a bunch of chaos.
Absolutely.
It's like his thing.
Sure.
But as far as it getting anywhere,
it's just not a realistic possibility,
and I don't think the media should be treating it
as if it is, because it's scaring people,
and it's heightening tensions.
When we should be going the other way,
we should be treating this as if there is no realistic way
for him to maintain control,
because when it comes to government authority,
it's all an illusion.
If people believe he can,
well, that actually makes it more likely that he can.
Government authority is an illusion.
Government has authority because people believe it does.
If they don't believe it, it doesn't have any authority.
All it has is might, and that might gets divided.
At the end of the day,
any government should have consent of the governed.
It does not appear that President Trump has that.
So that's how we should treat it, in my opinion.
And if the media is going to cover it,
and they are going to continually push this idea,
they need to bring people on who actually understand
this particular type of political violence.
And they're out there, they exist,
and they're all pretty much saying that this isn't a thing.
It's not realistic.
If he's going to attempt to maintain control,
he's going to do it through the courts.
I just, I want to reiterate that as much as possible
because we keep seeing the media lean into it,
and I think that's a horrible idea on a bunch of levels.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}