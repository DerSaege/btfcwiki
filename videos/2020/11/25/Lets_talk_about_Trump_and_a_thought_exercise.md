---
title: Let's talk about Trump and a thought exercise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QO8OBG3ymWI) |
| Published | 2020/11/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the possibility of the outgoing President of the United States, Donald Trump, sharing secrets with a foreign power.
- Emphasizing the value of recognizing the potential reality behind such a question.
- Mentioning the common occurrence of leaders becoming assets for foreign powers.
- Delving into why secrets are classified and the importance of protecting means and methods.
- Speculating on the limited value Trump might have in terms of offering valuable information.
- Noting that top-tier countries likely already have the information Trump possesses.
- Questioning the credibility of Trump's statements and the potential use of him as an agent of influence.
- Suggesting that using Trump for gathering information might not be the most strategic move.
- Contemplating the idea of Trump being used to influence policy and public opinion by a foreign power.
- Encouraging awareness that such scenarios could unfold in the United States, prompting vigilance.

### Quotes

- "The American people are having to entertain ideas that they've never had to entertain before."
- "Is our president a puppet?"
- "It really flies in the face of that American exceptionalism."

### Oneliner

Exploring the possibility of Trump sharing secrets with foreign powers, questioning credibility, and urging vigilance against unprecedented scenarios.

### Audience

American citizens

### On-the-ground actions from transcript

- Stay informed about potential security threats and espionage activities (implied).

### Whats missing in summary

The full transcript provides detailed insights into the potential risks associated with leaders sharing sensitive information and the need for public vigilance in safeguarding national interests.

### Tags

#NationalSecurity #ForeignInfluence #Espionage #Vigilance #Leadership


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about a question I've gotten multiple times over
the last week, so much so that I have to assume that somebody on like a major
network mentioned it as a possibility and I just haven't heard about it.
We're gonna go through it as kind of a thought exercise,
because there's something in the question that I think is very valuable,
and I think that we should consciously recognize it.
The question is whether or not the outgoing President of the United States,
Donald Trump, might go to a foreign power with the secrets that he has.
It's a cool question.
Like most things of a clandestine nature, the answer is it depends.
It depends.
I mean, the idea of a president, a head of state,
flipping and becoming an asset of a foreign power, that is amazing.
It's unthinkable, right?
Not really.
Happens all the time.
It happens all the time.
And realistically, there is not a service on the entire planet who wouldn't love to
debrief a President of the United States.
So on its face, and this isn't a silly question,
it happens all the time with countries that we view as lesser.
Okay, so now that we've established, yeah, I mean, sure,
the opposition would definitely try to do something like that.
The next question is the pragmatic stuff, the realistic stuff.
Does he actually have anything that they can use?
Does he have anything to offer them?
We've talked about it before on this channel, why secrets become secrets,
why secrets are secret.
It's not the information itself.
Things become classified to obscure means and methods,
to hide how the information was obtained.
Put it in a Cold War setting.
We find out that the Soviets have a new secret plane.
They can know that we know.
That they have a secret plane, they can know the specs that we have, because they're their specs.
They can know that we know that they know.
What they can't know is how we know,
because that would endanger the assets on the ground.
In the very early days of the Trump administration,
there were people that had trouble getting clearances.
20 people have asked this.
20 people have asked this.
20 people have asked me about whether or not Trump could end up working for a foreign power.
I'm assuming those people aren't in the intelligence field.
If 20 average Americans have considered it as a possibility,
I assure you that the professional paranoids in our intelligence services have.
Obscuring means and methods when writing a report for intelligence officers,
that's just Tuesday.
It's not a big deal.
It's something they do all the time.
I would bet just about anything that from the very beginning,
any report that the president received had the means and methods obscured,
unless he absolutely had to know them.
So what does that mean that he has?
What does he have to offer?
Loose information about programs, you know, no technical specifics,
no means and methods, just the idea that yes, this exists somewhere.
But to be honest, the opposition services probably already know all of that.
Historically, the U.S. actually isn't top tier at this.
The U.S. isn't the best.
The opposition countries right now are.
Russia, China, Israel and the U.K. are really good, too.
Those are the best countries.
They probably already know everything the president???
they probably know more than he knows because they actually read their reports.
And we know that he doesn't always read his daily briefing.
We know that he didn't really pay attention in them.
So even if there was some means and methods that got disclosed,
he probably didn't hear it or didn't read the report that it was in.
It's unlikely that he has anything of value for them.
It doesn't mean they wouldn't debrief him just as a propaganda win,
but doesn't really have anything of value.
And they can't just go off of what he says, either.
Would you trust him?
If he was your asset, would you trust him?
How credible are his statements?
A person who makes up numbers on the fly, who embellishes everything,
his information wouldn't be worth anything.
But to be honest, if a foreign power got access to Trump,
he wouldn't be used for gathering information.
It's not what he???that would be a really poor use of him.
He would be used as an agent of influence,
somebody who uses his position, whether it be an official one
or just a thought leader, to influence policy,
to influence public opinion in a manner that is beneficial
to the opposition service.
That's how he would be used.
Is that possible?
Yeah, sure.
Realistically, yeah.
I mean, that???absolutely, it is possible.
I haven't seen any hard proof of that.
I wouldn't discount it, but I haven't seen anything
that makes me believe he is anything more than incompetent.
A lot of his decisions did in fact benefit one of the opposition nations.
That's true.
But I don't know that he was instructed to do that.
I think that he probably just???he was probably just bad at what he did.
So why do I think this is cool?
Why do I think this was a cool thought exercise?
Because it can happen here.
Right now, because of President Trump,
the American people are having to entertain ideas
that they've never had to entertain before,
such as whether or not there's going to be a peaceful transfer of power.
Is our president a puppet?
I think that this is something that we should be consciously aware of.
It really flies in the face of that American exceptionalism,
that it can't happen here.
And the more people that recognize that, yeah, absolutely it can happen here,
the more we're on guard for it and the less likely it is to happen.
So did Trump or will Trump sell secrets to the bad guys?
We don't know.
If he does, we will know, because the most valuable thing
to an intelligence service when it comes to this would be disclosing
that they planted somebody as the head of state of the United States.
That would be the ultimate propaganda win.
It would force the United States to spend billions upon billions
upon billions of dollars to increase security and background checks.
It would just strike at the very foundations of democracy in this country.
So just rest assured that if this is the case, oh, they're going to tell us,
because it would be the ultimate black eye for the United States.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}