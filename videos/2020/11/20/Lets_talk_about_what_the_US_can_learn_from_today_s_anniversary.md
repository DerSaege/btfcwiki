---
title: Let's talk about what the US can learn from today's anniversary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=O4eGJRzno10) |
| Published | 2020/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commemorating the seventy-fifth anniversary of Nuremberg trials.
- International community's unity against atrocities as a lesson.
- Questioning the different treatment standards by governments towards their people.
- Pondering the evolution of the line on permissible government force over seventy-five years.
- Emphasizing the importance of establishing a baseline of facts for societal progress.
- Drawing parallels between confronting past actions in Germany and the need for the US to face its history.
- Advocating for addressing past wrongs and establishing a factual basis for moving forward.
- Stressing the need for continuous vigilance to ensure atrocities are prevented before they escalate.
- Calling for a proactive approach to prevent history from repeating itself.
- Advocating for moving the line of what is unacceptable for humanity collectively.

### Quotes

- "All governments use force against their own people, but there was a line drawn seventy-five years ago that said this is too much."
- "We have to know what happened in order to move forward."
- "If you want never again, we should move that line and keep moving it to make sure that it's an affront to all of humanity."
- "If we want to reclaim our position as a world leader, it's a pretty good step in that direction."
- "Y'all have a good day."

### Oneliner

Commemorating Nuremberg trials' seventy-fifth anniversary, Beau questions the global community's progress in setting limits on government force and calls for establishing factual baselines to confront history and prevent future atrocities.

### Audience

Global citizens, policymakers, historians

### On-the-ground actions from transcript

- Establish a factual baseline by uncovering and addressing past wrongs (implied).
- Advocate for setting stricter limits on government force through activism and advocacy (implied).
- Proactively work towards preventing atrocities by collectively moving the line of what is unacceptable (implied).

### Whats missing in summary

Deeper insights on the importance of historical accountability and proactive measures to prevent future atrocities.

### Tags

#Nuremberg #GovernmentForce #HistoricalAccountability #Prevention #GlobalCitizenship


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about an anniversary
and what
the entire world
and specifically the United States
can learn
from the events
that happened seventy five years ago.
Given that it is the seventy fifth anniversary
I would imagine that a whole lot of people
are going to be talking about this subject.
I would imagine that there's going to be a lot of
discussion
about Nuremberg today.
And I think
as is usually the case
people are going to focus on the aspect of justice
which yeah
that's worth discussing obviously.
But there's two other things that occurred at Nuremberg
that because of the gravity of the situation
I think they often get overlooked.
There's two other big points.
The first
is that it was kind of the first time
that the international community
came together
and said you know what this is too much, this is too far.
You can't do this. A nation state cannot do this
to the people
in its geographic area.
This is too much.
This is over the line.
And it's interesting
because I mean that's a cool thing to happen.
A loud proclamation
that this is too much.
But the countries that did it
the Soviet Union
the UK, France, the US
is what happened
in Germany really that dissimilar
from what happened in the colonies
of the UK or France? I'm not talking about how systematic it was
or the scale.
I'm talking about the intent.
Was it really that different than
what the US did to the natives
or
what it was doing to black folk in the south
or really any minority group
that came to this country?
The point being
all governments
use force against their own people
but there was a line drawn seventy five years ago
that said this is too much.
This is too far.
You know the only thing that really separates any organization
from a government
is the perception
of the legitimate use of force.
That's really it.
I mean that's the difference between a homeowners association and a city.
But there was a line drawn.
My question is
after seventy five years is that still where the line is?
Or
have we as a society, as a world, moved forward?
Has that line progressed as it should?
Are there things that
are an affront to all of humanity
today
that weren't in nineteen forty five?
Have we progressed? Have we evolved?
I think that when we
think about this
we should probably try to define
how much force
is allowable.
How much force can a government use against its own people?
How horrible
is a government allowed to be
to the people within its geographic area?
I think we need to come up with an answer
to that
for today.
I would hope
that it's
moved since nineteen forty five.
Now, the other point
that I think is incredibly relevant to the United States
is yes,
you had the trials,
justice, all of that,
but it also established a baseline of facts.
See, there were things that occurred in the preceding years
that a lot of people in Germany didn't want to address. They didn't want to confront that.
Who would want to?
Can you blame them?
But this trial
put it out there.
This is the chain of events
as we know it.
This is the baseline of facts
as we know it.
Shoved it in their face.
Made them confronted.
Made them look at what was done in their name.
And when they had to confront it,
they had to address it.
And once they addressed it,
they could move forward.
There's a whole lot of talk in the U.S. right now
about
reuniting the country, reconciliation.
Yeah, it should happen.
Absolutely.
But we need a baseline of facts
because there may be some things done in the preceding years,
some things done in our name
that we need to confront,
that we need to address,
that we need to look at and say, are we okay with this
being part of our national character?
And that's where that other question comes in.
Because there were some things done
over the last few years
that to me aren't a front.
And I think we need to address them.
Now I'm not suggesting we have Nuremberg-style trials.
In fact, in the U.S. we can't.
But a real accounting,
nothing swept under the rug.
All of the allegations looked into
and answered.
We have to know what happened
in order to move forward.
We have to have a baseline of facts.
We can say, this is what happened.
Now what are we going to do about it?
How do we move forward?
Now,
I'm certain that today
everybody all over the world,
anybody that has any sense of decency,
is going to cry out, never again,
as they should.
Just remember
that we don't have to
wait for things to get that bad.
If you want never again,
we should move that line
and keep moving it
to make sure that it's an affront
to all of humanity
before it ever even gets close
to what occurred then.
Then not only
are we moving the country forward,
we're moving everybody forward.
If we want to reclaim our position
as a world leader,
as a nation with some form of moral authority,
it's a pretty good step in that direction.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}