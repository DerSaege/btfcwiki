---
title: Let's talk about innovations and logical fallacies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VJ_oEq3Q1pI) |
| Published | 2020/11/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discussed logical fallacies like the genetic fallacy and the fallacy of composition.
- Pointed out that ideas and inventions stand on their own merit, regardless of their origin.
- Mentioned historical dangers of these fallacies being used together.
- Credited US military for various innovations and inventions.
- Argued that military challenges and self-interest spur innovation more effectively than just research funding.
- Suggested that a global threat, like climate change, could mobilize humanity and lead to technological breakthroughs.
- Advocated for framing climate change as a jobs program to appeal to a broader audience.
- Proposed that the technological benefits of combating climate change could sway skeptics.
- Called for a World War II style mobilization to address climate change effectively.
- Stressed the importance of utilizing all available tools to combat climate change and achieve real progress.

### Quotes

- "Ideas, innovations, inventions, they stand and fall on their own."
- "But it's normally used in the negative sense. This isn't true."
- "We need a World War II style mobilization to get any real traction."
- "If we engage in this kind of mobilization, we will get technological breakthroughs that will send us ahead."
- "Anyway, it's just a thought."

### Oneliner

Beau discussed logical fallacies, credited the military for innovations, and advocated for a World War II style mobilization to combat climate change effectively.

### Audience

Activists, policymakers, educators

### On-the-ground actions from transcript

- Mobilize for climate action with community-based initiatives (implied)
- Advocate for framing climate change as a jobs program to reach a wider audience (implied)
- Engage skeptics by discussing the technological benefits of combatting climate change (implied)

### Whats missing in summary

Beau's detailed examples and explanations on the importance of challenging logical fallacies and spurring innovation through military-like mobilizations are worth exploring further in the full transcript.

### Tags

#LogicalFallacies #Innovation #USMilitary #ClimateChange #Mobilization #TechnologicalBreakthroughs


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about invention,
necessity,
innovation,
and logical fallacies.
There are a couple of logical fallacies that anytime they arise
I try to push back on
because, well we'll get to that.
I did this last night on Twitter
because I did it on Twitter
and the conversation of course didn't go where I wanted it to go but
I
tried to
push back against the idea of a genetic fallacy.
What this is
it's when
because something originated
with something bad
it has to be bad.
Now it can go the other way
and if something originated with something good therefore it has to be
good.
But it's normally used in the negative sense.
This isn't true.
Ideas, innovations, inventions, they stand and fall on their own. It doesn't matter
where they originated.
This is a pretty dangerous
logical fallacy
historically speaking because it normally gets used in conjunction with
something called the fallacy of composition.
And the way that works is
let's say one person from demographic X
does something bad.
Fallacy of composition states well then that whole demographic is bad.
This of course is wrong.
When you have that
and then you apply the genetic fallacy to it
this whole demographic is bad
and anything that comes from them is bad.
Well then, I mean you really only have one option, right? You've got to get rid of them.
Those people are just no good.
Some of the worst things in human history
have come from these two logical fallacies
working together. So when I see them I try to push back against them.
I did this by trying to use the US military
and things that uh...
innovations
that they could be credited with.
If you don't know
it's worth googling. There's a giant list of stuff you use probably every day
that originated with the US military.
Now on to the actual point.
During this conversation a lot of people said that it didn't have to come from the military.
We could have just poured funding
into civilian research and gotten this stuff.
Yes and no.
It didn't have to come from the military. Fact.
But research alone isn't going to do it
because that's not...
that isn't how the military spurs so much innovation.
You need a challenge.
You need self-interest.
There aren't a lot of things that are as high stakes
as
the endeavors of the military.
So what happens is you get the researchers,
the engineers, and the people who are going to use this stuff, use whatever new technology
is developed,
they're going to use it in the field
and they're all brought together.
They're all in one spot. So you get that applied technology immediately.
So just pumping the funding into research,
that alone isn't going to do it.
You need something pressing,
a challenge,
self-interest.
Say landing on the moon.
A big challenge.
Something like that
can spur technological innovation.
But things like that don't come around so often.
What is more effective
is if it's a threat,
if it's something that provides that self-interest that truly motivates people,
because they have to be motivated.
But it still doesn't have to be the military.
Imagine
if there was some
global threat,
something that all of humanity was facing,
that we had to mobilize to deal with.
Say climate change, for example.
When climate change gets presented,
when the mobilizations that are going to be required to deal with this gets presented,
a lot of times it gets framed as a jobs program,
and that is a good way to frame it.
If it's a threat that's going to reach more of the people who are skeptical of it,
then it's an environmental thing.
If the environmental
reasons for it
were going to sway them,
they already would have been swayed.
The jobs program is a good one.
But there are still those people that that isn't going to help.
Entitlement people just wanting a handout.
The fact that
a mobilization,
the size of what we would need
to combat climate change,
would probably develop more technological breakthroughs and more innovations than World
War II did
might be another selling point.
That might be something you can tuck away,
keep in your back pocket for those conversations because people
with pretty much everything need self-interest.
If they don't care about the environment
and they're not somebody who would benefit from the jobs program aspect of it,
the technological benefits might.
That might be something that could sway them.
As we're
trying to
rally support for this,
we need to have every tool available
because
it's going to take a lot.
It is going to take a lot. We need a World War II style mobilization
to
get any real traction,
to get anything done in the time frame that we have.
This
is just one more little tool
in the toolbox.
If we engage in this kind of mobilization, we will get technological breakthroughs that
will send us ahead.
Maybe that will help sway somebody.
Anyway,
it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}