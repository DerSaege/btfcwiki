---
title: Let's talk about Trump over the weekend (50 days left)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TGAYsNLypAo) |
| Published | 2020/11/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau criticizes President Trump for his unsubstantiated claims about election security.
- A Republican congressperson, Paul Mitchell, started the hashtag #stopthestupid on Twitter, urging the president to concede and stop making baseless claims.
- President Trump expanded his list of perceived enemies to include the Department of Justice (DOJ) and the FBI, both organizations headed by his appointees.
- Trump’s continuous allegations about election security contradict experts and his own administration.
- Beau questions the logic behind Trump's claim that everyone is out to get him, suggesting it could backfire politically.
- Despite Trump's claims, experts confirm the security of the election, making his allegations baseless.
- Beau suggests that if Trump's claims were true, it'd showcase a significant failure in his administration's ability to secure the election.
- Concerns arise about Trump's allegations eroding faith in the U.S. election system, though Beau doubts it has much impact at this stage.
- Beau notes that some who initially had doubts about the election may now see Trump's behavior as the real issue, not election security.
- Beau anticipates Joe Biden's inauguration on January 20th, marking the end of Trump's presidency.

### Quotes

- "I think that there may be people who are watching this who might have believed something was amiss in the beginning, who are now starting to believe otherwise."
- "His continued clownish behavior may actually help to make people realize it's a person that has a problem, not election security."

### Oneliner

Beau criticizes Trump's baseless claims on election security, hinting at potential erosion of faith in the U.S. election system.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Follow updates on the presidential transition and inauguration (suggested)
- Share accurate information about election security to combat misinformation (suggested)

### Whats missing in summary

Insights on the potential long-term effects of Trump's claims and behavior on public trust in elections.

### Tags

#Trump #ElectionSecurity #JoeBiden #Inauguration #Misinformation


## Transcript
Well howdy there internet people it's Beau again.
So today we're going to talk about President Trump and his claims, his unsubstantiated,
unevidenced, unbelievable claims that he continued to reiterate over the weekend.
It got so bad that a Republican congressperson, Paul Mitchell,
started the hashtag stopthestupid on Twitter and pleaded with the president to just concede and
stop making unevidenced claims.
The president, for his part, over the weekend expanded the list of people that he believes
is out to get him. It apparently now includes DOJ and the FBI. I'd like to point out that
those organizations are headed by his appointees, one of whom has been ridiculously loyal.
I'm not sure that planting the idea that literally everybody who has contact with him
wants him to lose is necessarily a good move politically. At this point, if what he is
alleging is true, I mean we kind of just have to assume they have their reasons because that's a
mighty wide coalition they built to go after him. Of course it's not true.
But he then went on to again claim that the election wasn't secure, despite the fact that
literally every expert on election security says that it was, including his people.
If we were to accept his allegations, which we shouldn't because they're not true, but if we're
going to play the game, I would like to point out that that would just be yet another failure in a
long string of failures for the Trump administration. It's not like the administration didn't know there
were issues in 2016, and if in four years, with the entire weight of the federal government behind him,
he couldn't secure the election, I would suggest he doesn't need to retain his position.
There's a worry that his continued allegations may erode faith in the U.S. election system.
At the beginning, I kind of felt the same way, to be honest, but it's at a point now where it is so
farcical that I'm not sure it is. I think that there may be people who are watching this who
might have believed something was amiss in the beginning, who are now starting to believe otherwise.
At this point, I don't know that faith in the electoral system is going to be undermined
any further than it has. If anything, his continued clownish behavior may actually help to
make people realize it's a person that has a problem, not election security.
Again, I don't know. It's just kind of a gut feeling. I don't really have anything to back that one up.
But if you missed the news over the weekend, that was it. The president continued to be Donald Trump
and continued to behave like a spoiled billionaire who had his toy taken away.
January 20th, we should see the inauguration of Joe Biden, and we'll see what happens from there.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}