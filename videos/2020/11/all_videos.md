# All videos from November, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-11-30: Let's talk about innovations and logical fallacies.... (<a href="https://youtube.com/watch?v=VJ_oEq3Q1pI">watch</a> || <a href="/videos/2020/11/30/Lets_talk_about_innovations_and_logical_fallacies">transcript &amp; editable summary</a>)

Beau discussed logical fallacies, credited the military for innovations, and advocated for a World War II style mobilization to combat climate change effectively.

</summary>

"Ideas, innovations, inventions, they stand and fall on their own."
"But it's normally used in the negative sense. This isn't true."
"We need a World War II style mobilization to get any real traction."
"If we engage in this kind of mobilization, we will get technological breakthroughs that will send us ahead."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Discussed logical fallacies like the genetic fallacy and the fallacy of composition.
Pointed out that ideas and inventions stand on their own merit, regardless of their origin.
Mentioned historical dangers of these fallacies being used together.
Credited US military for various innovations and inventions.
Argued that military challenges and self-interest spur innovation more effectively than just research funding.
Suggested that a global threat, like climate change, could mobilize humanity and lead to technological breakthroughs.
Advocated for framing climate change as a jobs program to appeal to a broader audience.
Proposed that the technological benefits of combating climate change could sway skeptics.
Called for a World War II style mobilization to address climate change effectively.
Stressed the importance of utilizing all available tools to combat climate change and achieve real progress.

Actions:

for activists, policymakers, educators,
Mobilize for climate action with community-based initiatives (implied)
Advocate for framing climate change as a jobs program to reach a wider audience (implied)
Engage skeptics by discussing the technological benefits of combatting climate change (implied)
</details>
<details>
<summary>
2020-11-30: Let's talk about Trump over the weekend (50 days left).... (<a href="https://youtube.com/watch?v=TGAYsNLypAo">watch</a> || <a href="/videos/2020/11/30/Lets_talk_about_Trump_over_the_weekend_50_days_left">transcript &amp; editable summary</a>)

Beau criticizes Trump's baseless claims on election security, hinting at potential erosion of faith in the U.S. election system.

</summary>

"I think that there may be people who are watching this who might have believed something was amiss in the beginning, who are now starting to believe otherwise."
"His continued clownish behavior may actually help to make people realize it's a person that has a problem, not election security."

### AI summary (High error rate! Edit errors on video page)

Beau criticizes President Trump for his unsubstantiated claims about election security.
A Republican congressperson, Paul Mitchell, started the hashtag #stopthestupid on Twitter, urging the president to concede and stop making baseless claims.
President Trump expanded his list of perceived enemies to include the Department of Justice (DOJ) and the FBI, both organizations headed by his appointees.
Trump’s continuous allegations about election security contradict experts and his own administration.
Beau questions the logic behind Trump's claim that everyone is out to get him, suggesting it could backfire politically.
Despite Trump's claims, experts confirm the security of the election, making his allegations baseless.
Beau suggests that if Trump's claims were true, it'd showcase a significant failure in his administration's ability to secure the election.
Concerns arise about Trump's allegations eroding faith in the U.S. election system, though Beau doubts it has much impact at this stage.
Beau notes that some who initially had doubts about the election may now see Trump's behavior as the real issue, not election security.
Beau anticipates Joe Biden's inauguration on January 20th, marking the end of Trump's presidency.

Actions:

for voters, concerned citizens,
Follow updates on the presidential transition and inauguration (suggested)
Share accurate information about election security to combat misinformation (suggested)
</details>
<details>
<summary>
2020-11-29: Let's talk about my dogs and why I still reach out.... (<a href="https://youtube.com/watch?v=vtiKrOoS9dQ">watch</a> || <a href="/videos/2020/11/29/Lets_talk_about_my_dogs_and_why_I_still_reach_out">transcript &amp; editable summary</a>)

Beau explains his persistence in reaching out to those resistant to change through a heartfelt canine analogy, revealing the importance of care and understanding even for the easily misled.

</summary>

"At the end of the day, they placed their trust in the institution of the presidency."
"And they got tricked."
"Even if you can trick him with a tennis ball over and over and over again for months."
"Despite all of that, he's still a good boy."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains why he continues to reach out to people who resist being reached out to, using a story about his dogs as an analogy.
Describes his German Shepherd, Baroness, as incredibly smart and capable of problem-solving.
Shares that Baroness is like the raptor from Jurassic Park in terms of intelligence and abilities.
Recounts how Baroness used to hide his keys when she noticed him packing a bag at the age of two.
Talks about how Baroness can see through tricks like the fake tennis ball throw with contempt.
Mentions that Baroness is still open to learning new things despite her age.
States that Baroness is well-trained and proficient in various tasks.
Introduces his other dog, Destro, a husky, who is not as intelligent in problem-solving as Baroness.
Comments on Destro being easily tricked, specifically mentioning his confusion with glass even as an adult.
Emphasizes Destro's love for children and his affectionate nature towards them despite being easily fooled.
Draws a parallel between his dogs and people who trusted misinformation about mask-wearing from the presidency.
Conveys the message that even those who are easily deceived have people who care for them and who they matter to.
Acknowledges the frustration in trying to reach out to those who resist understanding the importance of certain issues.
Ends with a reflection on the situation and wishes the audience a good day.

Actions:

for empathetic individuals,
Reach out to those resistant to understanding with patience and compassion (exemplified)
</details>
<details>
<summary>
2020-11-29: Let's talk about an underutilized group of progressives.... (<a href="https://youtube.com/watch?v=llcbvsDMD3I">watch</a> || <a href="/videos/2020/11/29/Lets_talk_about_an_underutilized_group_of_progressives">transcript &amp; editable summary</a>)

Beau addresses progressive vets, urging them to recognize their value in building resilient communities and shaping a better future for the country, stressing that they are needed as advisors and guides, not necessarily leaders.

</summary>

"You're needed."
"You don't have to lead. They don't need leaders. They are leaders."
"We don't have to accept the way things are."
"You can help shape this country."
"What did you do it all for?"

### AI summary (High error rate! Edit errors on video page)

Beau addresses a certain group of people, particularly progressive vets, who are underutilized and may not realize their potential in building communities.
He shares a story about a veteran friend who feels he's done his part and just wants to stay isolated, despite the skills and experiences he possesses.
Beau points out that young progressives, fresh out of AIT, might understand concepts in theory but lack the practical experience needed for upcoming challenges.
He stresses the importance of veterans' skills in logistics, team-building, and community resilience, urging them to share their knowledge.
Beau encourages veterans to utilize their skill set and mindset to contribute to making the country better and helping those who lack the necessary skills.
He reminds veterans of the community-focused mindset they had during their service and how that level of community support can be beneficial in facing challenges.
Beau acknowledges that some veterans may not want to lead but can still serve as valuable advisors and guides to those willing to learn.
He underscores the idea that veterans can help shape the country by applying their skills differently to address ongoing issues.
Beau concludes by expressing his belief that veterans have the potential to contribute to creating a better world or country, using their existing skills in new ways.

Actions:

for progressive vets,
Reach out to progressive vets in your community and encourage them to share their skills and experiences with building resilient communities (exemplified).
Organize virtual or future in-person team-building events or workshops for community members with the help of progressive vets (exemplified).
Support progressive vets in connecting with individuals who are willing to learn and build self-reliant communities (exemplified).
</details>
<details>
<summary>
2020-11-28: Let's talk about questions from after the show.... (<a href="https://youtube.com/watch?v=MMzPaS4vM4Y">watch</a> || <a href="/videos/2020/11/28/Lets_talk_about_questions_from_after_the_show">transcript &amp; editable summary</a>)

Beau shares insights on various topics from living abroad to political strategies, urging continuous engagement for systemic change.

</summary>

"Masculine men are not afraid of competition. They don't mind equality with other people because competition is good."
"Biden isn't a savior. Biden really isn't a progressive. If you want that deep systemic change, you can't change. You can't stop. You have to keep going."
"I think by fact-checking the inarguable stuff, it may be better than trying to debate the opinions."

### AI summary (High error rate! Edit errors on video page)

Beau did a live stream where he answered questions from Twitter, but didn't get to them all.
Beau shares his thoughts on living in different countries, conservatives salvaging the GOP, and Trump's end game.
He touches on modern feminism and the importance of masculinity not equating to weakness.
Beau delves into politics, special operations forces, ranked choice voting, and the relationship between church and state.
He also addresses the Trump administration, the census, US-Kurd relationship, and the future of US politics.

Actions:

for engaged citizens seeking political insights.,
Fact-check information shared by others online (implied).
Engage in political activism to push for systemic change (implied).
Keep informed and voice opinions on political matters (implied).
</details>
<details>
<summary>
2020-11-27: Let's talk about climate change, the wealthy, and Rome.... (<a href="https://youtube.com/watch?v=GilUuFWfMMU">watch</a> || <a href="/videos/2020/11/27/Lets_talk_about_climate_change_the_wealthy_and_Rome">transcript &amp; editable summary</a>)

An archaeological find in ancient Rome prompts reflections on wealth, environmental issues, and security, challenging the effectiveness of appealing to the better nature of the wealthy to address climate change.

</summary>

"ashes to ashes, lava to lava, wealthy and poor alike, well we are all just matter in the ground."
"I don't know that appealing to the better nature of the wealthier in the world is going to produce any results."
"You and I don't have helicopters. They do."

### AI summary (High error rate! Edit errors on video page)

An archaeological find in ancient Rome has sparked popular imagination, leading to two different subtexts being circulated in popular culture.
In 79 AD, Mount Vesuvius erupted, preserving the remains of two individuals, speculated to be a slave and a wealthier person, possibly the slave's owner.
One subtext from this find is the idea that ultimately, whether wealthy or poor, we all turn to matter in the ground, a message Beau finds reasonable and acceptable.
The second subtext suggests that both wealthy and poor suffer equally from environmental issues, particularly climate change, which Beau finds questionable due to the wealthy's access to information and resources.
Beau believes that instead of focusing on environmental issues to prompt action from the wealthy, a more impactful message could be derived from ancient Rome's Praetorian Guard.
The Praetorian Guard, responsible for protecting the Roman elite, could serve as a better analogy for the need for security in climate change scenarios, as the wealthy may require protection against potential threats as resources dwindle.
Beau suggests that security professionals may turn against their wealthy employers in survival situations when resources become scarce, underscoring the importance of human elements in security, even in highly automated scenarios.
He points out the potential for corruption and ethical decline among security personnel when faced with survival pressures, indicating the necessity for preparedness and caution among the wealthy.
Beau argues that appealing to the better nature of the wealthy to address climate change may not be effective, as it has not yielded significant action thus far, suggesting that a more powerful motivating tool is needed.
He concludes by noting the stark differences in resources between the wealthy and the general population, implying that strategies for survival and security will vary vastly.

Actions:

for climate activists,
Build community resilience plans for climate emergencies (suggested)
Invest in security measures for vulnerable communities (implied)
</details>
<details>
<summary>
2020-11-26: Let's talk about how Trump brought manufacturing back and Yang.... (<a href="https://youtube.com/watch?v=x-24eJCZtUU">watch</a> || <a href="/videos/2020/11/26/Lets_talk_about_how_Trump_brought_manufacturing_back_and_Yang">transcript &amp; editable summary</a>)

Trump's policies have led to a resurgence in manufacturing in the US, but automation means traditional jobs aren't returning as promised.

</summary>

"Manufacturing is coming back, but those jobs are probably not coming with it."
"The robots are coming for your job."
"Those good union jobs, they aren't."
"It's not going to be like the old days because it isn't the old days."
"Those manufacturing jobs, those that he promised, they're not coming. They are phased out."

### AI summary (High error rate! Edit errors on video page)

Trump's campaign promise to bring manufacturing back to the US is somewhat happening, attributed to companies wanting to move production closer to home to reduce supply chain length.
Reasons for this include concerns about climate change and its impact on stability, exposure of weaknesses during crisis due to mishandling public health, national security, and trade disputes caused by Trump's tariff wars.
Trump's actions worsened climate change, leading to companies bringing production closer to home to avoid interruptions in the supply chain.
Companies are also responding to Trump's incompetence in handling public health crises.
Trump's focus on national security led to companies reconsidering manufacturing components in countries that could become opposition in the future.
The erratic nature of Trump's tariff wars and tweets created uncertainty for companies, prompting them to adjust their supply chains by bringing manufacturing closer to home.
Despite claims of promises made and promises kept, Beau notes that manufacturing is returning, but not the manufacturing jobs due to automation and the increasing use of robots to reduce labor costs.
Beau references Merrill, Bank of America's report suggesting a doubling of robot use in industries over the next five years due to decreasing automation costs.
While manufacturing may be resurging, traditional union manufacturing jobs are not returning at the scale people imagine.
Beau mentions Andrew Yang's warnings about automation and job displacement during the Democratic primaries, criticizing the Trump administration for not foreseeing this shift.
The return of manufacturing does not equate to the return of traditional manufacturing jobs due to automation and evolving technologies replacing human labor.
Beau questions the trade-off of worsening climate change and increasing the potential for war with major powers in exchange for a limited resurgence in manufacturing.
He concludes by suggesting that the promised manufacturing jobs are obsolete and phased out, with the current landscape not resembling the past industrial era.

Actions:

for policy analysts, activists,
Advocate for policies that support workers transitioning to new industries (implied)
Support education and training programs for emerging technologies and industries (implied)
Stay informed about the impact of automation on job markets and advocate for measures to protect workers (implied)
</details>
<details>
<summary>
2020-11-25: Let's talk about Trump and a thought exercise.... (<a href="https://youtube.com/watch?v=QO8OBG3ymWI">watch</a> || <a href="/videos/2020/11/25/Lets_talk_about_Trump_and_a_thought_exercise">transcript &amp; editable summary</a>)

Exploring the possibility of Trump sharing secrets with foreign powers, questioning credibility, and urging vigilance against unprecedented scenarios.

</summary>

"The American people are having to entertain ideas that they've never had to entertain before."
"Is our president a puppet?"
"It really flies in the face of that American exceptionalism."

### AI summary (High error rate! Edit errors on video page)

Exploring the possibility of the outgoing President of the United States, Donald Trump, sharing secrets with a foreign power.
Emphasizing the value of recognizing the potential reality behind such a question.
Mentioning the common occurrence of leaders becoming assets for foreign powers.
Delving into why secrets are classified and the importance of protecting means and methods.
Speculating on the limited value Trump might have in terms of offering valuable information.
Noting that top-tier countries likely already have the information Trump possesses.
Questioning the credibility of Trump's statements and the potential use of him as an agent of influence.
Suggesting that using Trump for gathering information might not be the most strategic move.
Contemplating the idea of Trump being used to influence policy and public opinion by a foreign power.
Encouraging awareness that such scenarios could unfold in the United States, prompting vigilance.

Actions:

for american citizens,
Stay informed about potential security threats and espionage activities (implied).
</details>
<details>
<summary>
2020-11-24: Let's talk about John Kerry's position.... (<a href="https://youtube.com/watch?v=Ea51I0AxUWM">watch</a> || <a href="/videos/2020/11/24/Lets_talk_about_John_Kerry_s_position">transcript &amp; editable summary</a>)

John Kerry's appointment signals hope for climate action but raises concerns about potential complacency on domestic efforts post-Trump, urging continued pressure on Biden for sustained commitment.

</summary>

"The goal shouldn't be simply to return the U.S. to pre-Trump levels of commitment."
"This is one of those issues we are running out of time to deal with."
"Don't take the appointment of a political heavyweight to this position as a guaranteed win."
"We're probably going to have to push Biden on for the next four years."
"This is one that we're probably going to have to push Biden on for the next four years because it is going to take him."

### AI summary (High error rate! Edit errors on video page)

John Kerry is becoming the U.S. representative on climate change, aiming to restore commitments from Trump's era.
Kerry, as an American political heavyweight, is well-connected and signifies continuity post-Trump.
He is expected to quickly accumulate wins internationally to counter the damage caused by Trump.
However, a potential pitfall is Biden feeling less pressure to take domestic action if Kerry secures early international wins.
Merely returning to pre-Trump levels of commitment is not enough; the U.S. must go beyond due to the lost time on climate change.
Biden's choice of Kerry may indicate a strong commitment, but there's a concern that easy wins internationally may reduce the drive for necessary domestic action.
The political capital gained swiftly from international wins might hinder the willingness to tackle challenging domestic decisions.
Vigilance is needed to ensure Biden follows through on climate action despite the initial wins Kerry might secure.
The fight for climate change action will be ongoing and will require significant political capital expenditure.
Upholding commitments on climate change will clash with certain business interests and campaign donations, making it a tough battle.

Actions:

for climate activists, concerned citizens,
Stay informed on climate policies and hold elected officials accountable for climate action (implied)
Advocate for strong domestic climate policies by engaging with local representatives and communities (implied)
</details>
<details>
<summary>
2020-11-23: Let's talk about calm, the election, Trump, and waiting... (<a href="https://youtube.com/watch?v=x9lvLnTM_Xw">watch</a> || <a href="/videos/2020/11/23/Lets_talk_about_calm_the_election_Trump_and_waiting">transcript &amp; editable summary</a>)

Beau shares a story of waiting in uncertainty, linking it to the current state of the country, urging calmness, focus on actionable issues, and waiting for decisions beyond their control.

</summary>

"The pieces were on the board and we were all aware of the situation and what might happen, but there wasn't anything we could do because we didn't have the information."
"We're all keeping an eye on it. But there's nothing you can really do until the people not at that table decide what they're going to do."
"It's not something that is likely to occur. So it might be best if we focused on the things in the room that we can change rather than stuff that realistically we can't."
"And there's no sense in getting your blood pressure up over it until those who are in a position to directly affect the outcome make their decisions and those variables get decided."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Tells a story from years ago about a concerning situation where friends were missing.
Friends rushed to a house, preparing for action but were faced with uncertainty.
The group sat around, waiting for more information, likening it to a board game.
Draws parallels between the past situation and the current state of the country.
Mentions the current election situation and Trump's attempts to undermine it.
Expresses that the outcome of the election is beyond their direct influence.
Acknowledges economic and public health issues that they can impact directly.
Emphasizes the need to focus on things they can change rather than stressing about what they can't.
Suggests staying calm, keeping an eye on the situation, and waiting for decisions from others.
Urges against panicking and instead focusing on actionable areas while being aware of the larger picture.

Actions:

for concerned citizens,
Focus on addressing economic and public health issues within your community (implied).
Stay informed about current events and remain committed to positive change (implied).
</details>
<details>
<summary>
2020-11-23: Let's talk about Biden's pick for State Department.... (<a href="https://youtube.com/watch?v=1pkswgpLKDk">watch</a> || <a href="/videos/2020/11/23/Lets_talk_about_Biden_s_pick_for_State_Department">transcript &amp; editable summary</a>)

Beau debates Blinken's potential foreign policy under Biden, noting uncertainties and speculations on its effectiveness.

</summary>

"There is no way to overstate how bad I believe Trump's foreign policy is."
"Anything that is said now is pure speculation."
"We're not going to get a good read on it until we see them in action."

### AI summary (High error rate! Edit errors on video page)

Ran across a foreign policy thread online where they mentioned him and debated Trump's foreign policy.
Beau believes Trump's foreign policy cannot be overstated in its badness.
Blinken is rumored to be the next Secretary of State, prompting questions about his progressiveness and potential foreign policy.
Blinken is described as a classic liberal, not a progressive.
If State Department runs as usual, Blinken’s foreign policy likely mirrors Obama's, better than Trump's but not a complete fix.
Biden campaign's creation of a second State Department suggests awareness of challenges.
Blinken's long advisory role with Biden and logistics skills may make him a good fit to provide quick information.
Speculation on potential scenario where area heads create options for Secretary of State to present to the president.
Uncertainty on whether Blinken's appointment will be good or bad, as it's all speculation until actions are seen.
Beau acknowledges disagreements with Blinken's past foreign policy decisions but sees potential for him to play a key role in the administration.

Actions:

for political analysts, concerned citizens,
Monitor Blinken's actions once in office to gauge the impact of his foreign policy decisions (implied)
</details>
<details>
<summary>
2020-11-22: Let's talk about Trump's base and evidence.... (<a href="https://youtube.com/watch?v=ZAeJwgassYA">watch</a> || <a href="/videos/2020/11/22/Lets_talk_about_Trump_s_base_and_evidence">transcript &amp; editable summary</a>)

Trump’s baseless claims have led to a dangerous erosion of trust in American democracy, with supporters willing to subvert the Constitution based on his word alone.

</summary>

"He was a reflection of them."
"They handed their critical thinking to President Trump."
"They gave him their trust, and he abused it."

### AI summary (High error rate! Edit errors on video page)

Trump's presidency was marked by people recognizing his differences from past presidents, both supporters and critics alike.
Trump is now openly asking state legislatures to undermine the election without presenting evidence to support his claims.
Despite numerous opportunities, Trump has failed to present any evidence of election fraud in court.
By asking state legislatures to take drastic actions without evidence, Trump is undermining American democracy.
The lack of evidence for Trump's claims has led to a segment of the population willing to subvert the Constitution based solely on his word.
Trump's ability to sway his supporters to believe him, even in the absence of evidence, is concerning.
There is a looming end to this situation in less than two months, but until then, the trust placed in Trump by his supporters continues to be abused.
Beau underscores the importance of demanding evidence and critical thinking, rather than blind trust in political leaders.

Actions:

for american citizens,
Demand evidence and critical thinking from political leaders (implied)
</details>
<details>
<summary>
2020-11-22: Let's talk about JFK and incremental changes in society.... (<a href="https://youtube.com/watch?v=eGgaGm2xlCU">watch</a> || <a href="/videos/2020/11/22/Lets_talk_about_JFK_and_incremental_changes_in_society">transcript &amp; editable summary</a>)

Society changes incrementally through shifting thought, not just laws, focusing on shaping the future, and seizing key moments for progress.

</summary>

"Change is the law of life."
"Effort and courage are not enough without purpose and direction."
"Progressives tend to want to shape the future a little bit more."
"Seize moments when you can make the big changes."
"A nation reveals itself not only by the minute produces but also by the minute honors the minute remembers."

### AI summary (High error rate! Edit errors on video page)

Society changes incrementally over time by shifting thought, not just laws.
Progressives tend to win because they focus on shaping the future, not just controlling the present.
Change is a process that requires looking ahead to how we want people to behave in the future.
Effort and courage need purpose and direction to make a meaningful impact.
Small changes in behavior now can influence thought and societal norms later.
Time is a tool for change, not a barrier, and change should happen gradually.
Historical movements were built on slowly changing opinions and eroding barriers.
Seize moments when big changes are possible, but also focus on daily progress towards long-term goals.
Building towards a better future requires understanding that progress may extend beyond our lifetime.
Incremental change is a proven template for societal progress, both in positive and negative aspects.

Actions:

for community members,
Build parallel structures like community gardens or networks to gradually shift societal norms (exemplified).
Educate and instill progressive values in future generations to continue the path towards a better world (implied).
</details>
<details>
<summary>
2020-11-21: Let's talk about societal change and pronouns.... (<a href="https://youtube.com/watch?v=FaFK9uqbqrY">watch</a> || <a href="/videos/2020/11/21/Lets_talk_about_societal_change_and_pronouns">transcript &amp; editable summary</a>)

To change societal norms, start by normalizing sharing preferred pronouns in everyday interactions, fostering inclusivity and acceptance.

</summary>

"It's a good example of how little actions can over time shift the way society looks at an issue that today is very divisive."
"The widespread use of this can help them feel more accepted, can help them more included."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Changing societal thought doesn't always require changing laws but altering people's perspectives is key.
Received a message suggesting putting preferred pronouns on Twitter bio to challenge societal norms.
Society currently relies on guessing preferred pronouns based on appearance, creating potential for misidentification.
Identifying preferred pronouns is specific to certain groups, leading to further differentiation and othering.
Encourages those without an obvious need to state preferred pronouns to do so to normalize the practice.
Simple actions like adding pronouns in bios can gradually shift societal views and make it a common practice.
Inclusion of pronouns can act as a signal of acceptance and create a welcoming space for others.
The widespread adoption of sharing pronouns can help individuals who don't neatly fit into traditional gender categories.
Small actions like this can contribute to changing divisive issues over time.
Predicts that in the future, assumptions about gender roles based on appearance will diminish.

Actions:

for social media users,
Add preferred pronouns to your social media bio (suggested)
Encourage others to include their preferred pronouns in their profiles (implied)
</details>
<details>
<summary>
2020-11-21: Let's talk about Trump, legitimacy, the media, and phone calls.... (<a href="https://youtube.com/watch?v=ExdEPbH5YgM">watch</a> || <a href="/videos/2020/11/21/Lets_talk_about_Trump_legitimacy_the_media_and_phone_calls">transcript &amp; editable summary</a>)

Pundits create a convenient fiction as Trump's control slips, media sensationalizes, and democracy's foundations are undermined.

</summary>

"Prank caller, prank caller."
"It's over. They're the conduit by which that message is spread."
"I don't see a lot of ways for him to even pretend to have legitimate control."
"I don't think it's right to lean into this idea and scare people simply for ratings."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Pundits and commentators are still suggesting that Trump can somehow retain power through phone calls in a convenient fiction for the masses.
American people have never had to worry about the recognized government due to peaceful transfer of power, a concept foreign to other countries.
Trump's control is slipping as world leaders, international organizations, media outlets, and large companies are recognizing Biden.
The transition of power is already happening, with state governments needing to fall in line for Trump to retain power, which is unlikely.
The media's portrayal of Trump still having a shot undermines faith in American democracy and is more damaging than his actions.
The blame rests with the media for not treating Trump's chances realistically and instead sensationalizing for views.
Continuing to treat Trump as having a shot undermines faith in democracy and harms the foundations of the country.
Some individuals are not acting in good faith by perpetuating the idea of Trump's legitimacy for personal gain or ratings.

Actions:

for media consumers,
Recognize and question sensationalized media narratives (implied)
Uphold faith in democracy by promoting realistic political discourse (implied)
Hold media outlets accountable for their coverage of political events (implied)
</details>
<details>
<summary>
2020-11-20: Let's talk about what the US can learn from today's anniversary.... (<a href="https://youtube.com/watch?v=O4eGJRzno10">watch</a> || <a href="/videos/2020/11/20/Lets_talk_about_what_the_US_can_learn_from_today_s_anniversary">transcript &amp; editable summary</a>)

Commemorating Nuremberg trials' seventy-fifth anniversary, Beau questions the global community's progress in setting limits on government force and calls for establishing factual baselines to confront history and prevent future atrocities.

</summary>

"All governments use force against their own people, but there was a line drawn seventy-five years ago that said this is too much."
"We have to know what happened in order to move forward."
"If you want never again, we should move that line and keep moving it to make sure that it's an affront to all of humanity."
"If we want to reclaim our position as a world leader, it's a pretty good step in that direction."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Commemorating the seventy-fifth anniversary of Nuremberg trials.
International community's unity against atrocities as a lesson.
Questioning the different treatment standards by governments towards their people.
Pondering the evolution of the line on permissible government force over seventy-five years.
Emphasizing the importance of establishing a baseline of facts for societal progress.
Drawing parallels between confronting past actions in Germany and the need for the US to face its history.
Advocating for addressing past wrongs and establishing a factual basis for moving forward.
Stressing the need for continuous vigilance to ensure atrocities are prevented before they escalate.
Calling for a proactive approach to prevent history from repeating itself.
Advocating for moving the line of what is unacceptable for humanity collectively.

Actions:

for global citizens, policymakers, historians,
Establish a factual baseline by uncovering and addressing past wrongs (implied).
Advocate for setting stricter limits on government force through activism and advocacy (implied).
Proactively work towards preventing atrocities by collectively moving the line of what is unacceptable (implied).
</details>
<details>
<summary>
2020-11-19: Let's talk about standard responses to political ideas and why they're wrong.... (<a href="https://youtube.com/watch?v=GiwP_LxmuaA">watch</a> || <a href="/videos/2020/11/19/Lets_talk_about_standard_responses_to_political_ideas_and_why_they_re_wrong">transcript &amp; editable summary</a>)

Beau questions societal norms by examining basic needs provision in prison and advocating for collective well-being over individual success.

</summary>

"If they were committed to the mission of I don't know making the world a better place. Might be a better world."
"Because it is a human right. Because it's something that's required to live."
"Maybe the way we look at things should change too."
"Maybe we should be focused on building that society where people do have a mission."
"Not just making sure our betters stay in power."

### AI summary (High error rate! Edit errors on video page)

Talks about standardized responses in political discourse.
Received a message critiquing his views on housing, healthcare, and ending hunger, comparing guaranteed services to prison or slavery.
Responds by acknowledging that prisoners do receive basic necessities like shelter, food, and healthcare.
Contrasts the provision of services in the military to society, where it's geared towards productivity and effectiveness.
Points out that if everyone in society was supported like the military, focusing on a collective mission, the world could be better.
Argues that the provision of basic needs in prison doesn't imply that society values these rights for all.
Suggests that societal structures are designed to control people through coercion and fear of losing basic needs.
Criticizes the focus on individual success rather than collective well-being and societal change.
Advocates for a shift towards a society where people unite behind principles for the common good.
Encourages reevaluation of societal norms and motivations handed down through generations.

Actions:

for social activists,
Question societal norms and advocate for collective well-being (exemplified)
</details>
<details>
<summary>
2020-11-19: Let's talk about 2020, 2021, food, and billionaires.... (<a href="https://youtube.com/watch?v=G6J8NAeKrR4">watch</a> || <a href="/videos/2020/11/19/Lets_talk_about_2020_2021_food_and_billionaires">transcript &amp; editable summary</a>)

Beau addresses the urgent need for $15 billion to prevent global famine in 2021, calling out systemic failures and the reliance on individual billionaires for critical aid.

</summary>

"15 billion dollars. All the nations of the world may not scrape together 15 billion dollars to stop people from starving."
"Starvation causes destabilization, which causes migration."
"In a world as plentiful as this one, this really shouldn't be a thing anymore, but it is."
"The world is getting bigger. We are not as insulated from these issues as we used to be."
"If things do not change and change pretty quickly, 2020 might have been the year of the plague. 2021 will be the year of famine."

### AI summary (High error rate! Edit errors on video page)

Closing out 2020, looking forward to 2021, discussing billionaires, and food issues.
United Nations released $100 million to help seven nations facing food insecurity.
Predictions of 36 to 40 countries experiencing severe food insecurity in 2021.
Challenges due to the world economy, public health crisis, climate issues, and more.
The cost to prevent famine is $5 billion, with an additional $10 billion to ease malnourishment.
Lack of funding from nation states poses a significant concern.
World Food Program warns of potential famines of biblical proportions.
Hoping for altruistic billionaires to step up and provide the needed $15 billion.
Criticizes the broken system where individual billionaires are relied upon for global crises.
Systemic issues leading to potential worsening of food insecurity globally.
Political reluctance to address food insecurity compared to other priorities.
Urges for international aid to prevent starvation and subsequent destabilization.
Points out the interconnectedness of global issues and the impact of nationalism.
Emphasizes the need for collective action and challenging nationalist perspectives.
Warns that without swift changes, 2020's plague may be followed by 2021's famine.

Actions:

for global citizens,
Advocate for increased government funding for food security programs (implied)
Support organizations working to address food insecurity through donations or volunteering (implied)
</details>
<details>
<summary>
2020-11-18: Let's talk about why Trump probably fired Krebs.... (<a href="https://youtube.com/watch?v=Cf6hra_Bpnc">watch</a> || <a href="/videos/2020/11/18/Lets_talk_about_why_Trump_probably_fired_Krebs">transcript &amp; editable summary</a>)

The President's firing of the election integrity official likely stems from pettiness rather than a strategic motive, with little impact on critical information.

</summary>

"He was fired because he didn't do what Trump said."
"The most likely explanation is that he's just petty and vindictive."

### AI summary (High error rate! Edit errors on video page)

The President fired the person responsible for maintaining election integrity who debunked his baseless claims.
The fired person was impartial and tried to balance loyalty to the President with doing his job.
Concerns about the termination are mostly unfounded; it doesn't change much as the information exists elsewhere.
The fired person's job involved processing information and providing assessments, but his termination won't lead to critical information loss.
Experts, commissions, and agencies all agree that the President's election fraud claims are baseless and unrealistic.
The position terminated doesn't have significant control over altering information outcomes.
The President's likely motive for the firing is pettiness and vindictiveness rather than a strategic change in election outcome.
Trump fired the person because he didn't comply with his baseless claims, showing his true colors.
There's no need to attribute a sinister motive to the firing; it's likely a reflection of the President's pettiness.

Actions:

for political analysts, activists, voters,
Stay informed about election integrity issues and stand against baseless claims (implied)
</details>
<details>
<summary>
2020-11-18: Let's talk about needing your advice.... (<a href="https://youtube.com/watch?v=-M0NC4fO9oE">watch</a> || <a href="/videos/2020/11/18/Lets_talk_about_needing_your_advice">transcript &amp; editable summary</a>)

Former security consultant warns against hiring problematic troubleshooter, drawing parallels to a larger political context and advocating for a change in leadership.

</summary>

"The guy is no good. I'm telling you this. I've been telling you this."
"He's blaming people who have done a decent job."
"The real boss here, the people, have decided it is time for a personnel change."

### AI summary (High error rate! Edit errors on video page)

Used to be a security consultant, gave advice on security issues.
Worked for a company owner who wanted to hire someone despite red flags.
Advised against hiring the person due to inflated resume and bad reputation.
Boss ignored advice and hired the person as a troubleshooter.
Troubleshooter's projects failed, blamed others for his mistakes.
Beau continued to warn the boss about the troubleshooter's incompetence.
Troubleshooter may have broken laws, but the boss didn't take action.
Despite causing problems, the troubleshooter was kept on for years.
Compares the situation to President Trump and his followers.
Concludes that the people, as the real boss, decided it was time for a change.

Actions:

for workplace colleagues,
Confront problematic hires in the workplace (exemplified)
Advocate for responsible leadership in your workplace (exemplified)
Engage with Trump supporters respectfully to challenge their beliefs (exemplified)
</details>
<details>
<summary>
2020-11-17: Let's talk about reuniting the country and unity.... (<a href="https://youtube.com/watch?v=NxneZbFZzko">watch</a> || <a href="/videos/2020/11/17/Lets_talk_about_reuniting_the_country_and_unity">transcript &amp; editable summary</a>)

Beau stresses uniting behind principles and ideas to move the country forward, not compromising for the sake of unity.

</summary>

"We unite behind principle. We unite behind ideas."
"Compromise isn't the only way to unite. You can lead."
"We can't just reach across the aisle for the sake of doing it because it looks good in a photo op."
"We have to look to the future. We have to look forward."
"Not simply unite with dinosaurs who don't have the best interest of the American people at heart."

### AI summary (High error rate! Edit errors on video page)

Addresses the importance of unity in the country.
Emphasizes uniting behind principles and ideas, not parties or personalities.
States that unity should focus on moving the country forward, not compromising.
Warns against compromising with irrational, fear-based individuals for the sake of unity.
Advocates for leadership in uniting people by presenting and defending ideas effectively.
Urges to address issues like climate change, economic inequality, and affordable housing to move forward.
Argues that reaching across the aisle just for appearances can lead to regression.
Stresses the need to unite behind principles that propel the country forward.
Encourages looking towards the future and not clinging to outdated ideas.
Calls for presenting and defending good ideas to progress as a nation.

Actions:

for leaders and activists.,
Convince and campaign for ideas that unite people (implied).
Advocate for addressing issues like climate change and economic inequality (implied).
Present and defend good ideas to push the country forward (implied).
</details>
<details>
<summary>
2020-11-17: Let's talk about Hamilton and why it resonates.... (<a href="https://youtube.com/watch?v=GHCnoa-MLag">watch</a> || <a href="/videos/2020/11/17/Lets_talk_about_Hamilton_and_why_it_resonates">transcript &amp; editable summary</a>)

Beau analyzes why "Hamilton" resonates by challenging American mythology, subtly addressing flaws, and advocating for a realistic portrayal of historical figures to inspire change.

</summary>

"Facts and truth aren't always the same thing."
"The image of the founders is a myth. It's not real."
"They were brash, they had flaws, and they changed things."
"It's incredibly relatable. This should teach us a lot about how we need to address history."
"Don't give them figures that are myths."

### AI summary (High error rate! Edit errors on video page)

Analyzes why "Hamilton" continues to resonate after five years without focusing on the plot or music.
Argues that the musical's impact lies in its relatability to modern issues and its challenge to American mythology.
Points out how "Hamilton" subtly addresses flaws and corruption in both past and present American society.
Comments on the mythologizing of the Founding Fathers and the need to shatter these idealized images.
Examines how the musical tackles issues like social advancement through military service and class disparities.
Criticizes the tendency to overlook the flaws of historical figures, advocating for a more realistic portrayal.
Emphasizes the importance of understanding that the image of the founders is a myth and not a reflection of reality.
Explores how "Hamilton" addresses the founders' stance on slavery and reveals early instances of corruption in government.
Challenges the notion of historical figures as flawless heroes and encourages a more critical examination of history.
Concludes by stressing the need to present historical figures realistically to inspire positive change.

Actions:

for young activists for change.,
Challenge the idealized image of historical figures by promoting a more realistic portrayal (suggested).
Encourage critical examination of history to inspire positive change (suggested).
Support young activists by acknowledging their voices against societal issues (implied).
</details>
<details>
<summary>
2020-11-16: Let's talk about a message from one of your daughters and caution.... (<a href="https://youtube.com/watch?v=by1xCUrQTaM">watch</a> || <a href="/videos/2020/11/16/Lets_talk_about_a_message_from_one_of_your_daughters_and_caution">transcript &amp; editable summary</a>)

Beau stresses the seriousness of the current COVID-19 situation, urging everyone to wear masks and follow safety protocols as it's worse than ever and precautions are vital with colder weather approaching.

</summary>

"Please wear a mask, wash your hands, don't touch your face, stay at home if you can."
"It is worse now than it has ever been."
"He's focused on other things. There was little response to begin with. There is no response right now."

### AI summary (High error rate! Edit errors on video page)

An email from a daughter expresses concerns about her 67-year-old father with multiple medical conditions who is at risk but refuses to wear a mask, waiting for Beau to shave his beard first.
Beau, a hermit who rarely leaves his property, initially shaved his beard to set a good example and comply with mask-wearing.
Beau stresses the seriousness of the current situation, pointing out that it's worse than ever and the President is not adequately focused on it.
Urging everyone to wear masks, wash hands, avoid touching faces, and stay home if possible, Beau underlines the importance of following safety protocols.
Despite the dire situation, Beau reminds listeners to exercise caution as it's going to get worse with colder weather approaching.

Actions:

for general public,
Wear a mask, wash hands, avoid touching face, and stay at home if possible (suggested)
Exercise caution and follow safety protocols diligently (suggested)
</details>
<details>
<summary>
2020-11-16: Let's talk about $50,000 in student debt forgiveness.... (<a href="https://youtube.com/watch?v=uK9k0xi0yZo">watch</a> || <a href="/videos/2020/11/16/Lets_talk_about_50_000_in_student_debt_forgiveness">transcript &amp; editable summary</a>)

Beau challenges misconceptions on forgiving student debt, advocating for education as a community asset and critiquing class-based barriers to higher education.

</summary>

"Education is a community asset."
"The problem is this doesn't go far enough. It's a starting point."
"Congratulations. You're admitting that it's a class issue."
"You're saying flat out that you want to use a college education as a barrier for entry into higher paying jobs."
"Perhaps we should change and we should start looking at things from a community standpoint."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the proposal to forgive the first $50,000 of student debt, discussing the pushback it's receiving and analyzing the arguments.
He points out historical instances where community efforts could have prevented the need for certain agencies like FEMA, FDIC, and OSHA.
Beau questions the argument against forgiving student debt, stating that education is a community asset that benefits everyone.
He argues that education strengthens the community, and educated individuals contribute significantly to society.
Beau challenges the idea that forgiving student debt is a handout, asserting that it doesn't go far enough and should be seen as a starting point.
He criticizes the opposition to universal education, noting that it reveals underlying class issues and a desire to maintain barriers to higher-paying jobs.
Beau suggests shifting perspectives towards community welfare and away from perpetuating inequality through credentialing.

Actions:

for community members, activists,
Promote community education initiatives to strengthen overall welfare (suggested)
Advocate for policies that support equitable access to education and opportunities (suggested)
</details>
<details>
<summary>
2020-11-15: Let's talk about why Trump might have lost the military vote.... (<a href="https://youtube.com/watch?v=UyWdOQpKwDU">watch</a> || <a href="/videos/2020/11/15/Lets_talk_about_why_Trump_might_have_lost_the_military_vote">transcript &amp; editable summary</a>)

Military personnel surprised by Trump's actions didn't vote as expected, showing the diversity and individuality within the ranks.

</summary>

"Saved it to the end because it's the only thing that everybody that I talked to said."
"The Kurds were our greatest ally in the Middle East, and Trump sold them out."
"No demographic should be judged by the most visible members of that demographic."

### AI summary (High error rate! Edit errors on video page)

Explains why a certain demographic didn't vote as expected in the recent election.
Shares favorite quote about military personnel voting Democrat, contrary to expectations.
Challenges the stereotype of the ultra conservative, socially regressive military.
Points out the diversity within the military and reasons why some may have issues with Trump.
Mentions specific concerns like Trump's handling of military installations' names and hurricane response.
Talks about soldiers' dissatisfaction with Trump's attitude towards personal protective gear.
Notes opposition to Trump's refusal of a peaceful transition of power and treatment of respected generals.
Reveals that the betrayal of Kurdish allies by Trump was a major concern for every soldier Beau talked to.
Suggests that judging a demographic by its most visible members is not representative of the whole.
Warns Democrats not to assume they have secured the military vote long-term due to Trump's unpopularity.

Actions:

for military personnel and political analysts,
Reach out to military personnel to understand their concerns and perspectives (suggested)
Advocate for policies that support military members and address their grievances (suggested)
</details>
<details>
<summary>
2020-11-14: Let's talk about the Trump news network.... (<a href="https://youtube.com/watch?v=frN0EYXws7U">watch</a> || <a href="/videos/2020/11/14/Lets_talk_about_the_Trump_news_network">transcript &amp; editable summary</a>)

Beau speculates on a potential Trump news network, predicting financial challenges, advertiser hesitancy, and Trump's tendency to blame others for failure post-presidency.

</summary>

"Realistically if this network is to mimic Trump's style it is going to be controversial nonstop."
"The reason I'm excited, the reason I'm kind of looking forward to this is because the president accepts responsibility for nothing."
"I honestly see the final days of this news network with him holding up buckets of survival food to sell."

### AI summary (High error rate! Edit errors on video page)

Speculates on Trump's post-public life plans, particularly focusing on the idea of a Trump cable news network.
Points out potential financial challenges for the network due to Trump's controversial style and the risk of lawsuits.
Raises concerns about advertisers being hesitant to associate with such a network due to potential boycotts.
Suggests that an internet outlet might be more economically viable short-term for Trump, but social media restrictions could hinder reach.
Anticipates Trump blaming others if his post-presidency venture fails, reflecting his pattern of not taking responsibility.
Predicts that a Trump news network could end up being another bankruptcy in Trump's history, potentially revealing his true colors to his base without the shield of the presidency.

Actions:

for political observers,
Analyze and critically question the credibility and impact of potential media outlets post-Trump presidency (implied).
</details>
<details>
<summary>
2020-11-14: Let's talk about Trump having faith in electors.... (<a href="https://youtube.com/watch?v=fpjr4_lIkEo">watch</a> || <a href="/videos/2020/11/14/Lets_talk_about_Trump_having_faith_in_electors">transcript &amp; editable summary</a>)

Beau dives into election laws in key states, debunks the elector substitution theory, and praises American democracy's resilience against schemes to keep Trump in power.

</summary>

"American democracy appears to be more resilient than a lot of us pictured."
"This is how we could win type of thing, but not really."

### AI summary (High error rate! Edit errors on video page)

Spent hours researching election laws in Arizona, Michigan, Pennsylvania, and Wisconsin due to a theory about GOP substituting electors.
Found laws prohibiting the substitution of electors in these states, causing confusion.
Republican leadership in Arizona, Michigan, Pennsylvania, and Wisconsin confirmed that elector substitution won't happen.
In Michigan and Arizona, electors are mandated by statute to vote in accordance with the people.
Pennsylvania GOP leaders stated they will not have a hand in choosing electors or intervene.
Laws on the books in two states prevent the elector substitution scheme.
American democracy appears to be more resilient than anticipated.
Beau doubts the realistic possibility of the elector substitution scheme, relying on politicians' word.
Considers this as the last attempt to keep Trump in power.
Encourages staying vigilant for any future schemes.

Actions:

for political observers,
Stay informed about election laws in your state (implied)
Be vigilant against potential threats to democracy (implied)
</details>
<details>
<summary>
2020-11-13: Let's talk about Trump using the military to maintain control.... (<a href="https://youtube.com/watch?v=yb2iRT6BU34">watch</a> || <a href="/videos/2020/11/13/Lets_talk_about_Trump_using_the_military_to_maintain_control">transcript &amp; editable summary</a>)

Beau explains the implausibility of a sitting president using the military to maintain control after losing an election, stressing the importance of unity in political movements and the illusion of government authority.

</summary>

"Government authority is an illusion."
"At the end of the day, any government should have consent of the governed."
"If he's going to attempt to maintain control, he's going to do it through the courts."
"Realistically, not just Trump, but any president, using the military to maintain control of the population after losing an election, it's not a realistic possibility."
"We need everybody. Everybody along the spectrum."

### AI summary (High error rate! Edit errors on video page)

Addressing the hypothetical scenario of a sitting president using the U.S. military to maintain control after losing an election.
Explaining the necessity of actual control of the military and the challenges in achieving it.
Detailing the logistics required for such a scenario, including planning, surprise, and violence of action.
Analyzing the lack of manpower and material needed to pacify a country the size of the United States.
Emphasizing that the current administration lacks the resources and logistics to make this scenario a reality.
Stating that while chaos is possible, the realistic possibility of using the military to maintain control after losing an election is slim.
Advocating for total mobilization and unity in political movements as a way to resist any potential subversion of government.
Stressing that government authority is based on consent of the governed and not just might.
Urging the media to avoid sensationalizing unrealistic scenarios that could heighten tensions.

Actions:

for concerned citizens, political activists,
Mobilize all segments of society for political resistance (suggested)
Advocate for unity and cooperation among groups with shared goals (suggested)
Stay engaged in political movements to prevent defeat (suggested)
</details>
<details>
<summary>
2020-11-12: Let's talk about Biden and 440,000 people.... (<a href="https://youtube.com/watch?v=wUY7RfSonBI">watch</a> || <a href="/videos/2020/11/12/Lets_talk_about_Biden_and_440_000_people">transcript &amp; editable summary</a>)

Biden's refugee program reform, though not perfect, could save lives and sets a positive starting point for more changes, as emphasized by Beau.

</summary>

"If you want change, you have to create it."
"Reform comes from the government."
"You can't let the good become the enemy of the perfect."
"I will give him some breathing room to see what he's going to do."
"At the end of the day, this move can save almost half a million lives."

### AI summary (High error rate! Edit errors on video page)

Biden is actively moving forward with his transition, already having advisors and making decisions.
He is focusing on announcing plans rather than campaign promises now that the campaign is over.
One significant plan he has shared is to revamp the US refugee program, which Beau cares deeply about.
The current refugee program is described as broken and in need of deep reform due to its difficulty and low cap.
Biden's proposed increase in the refugee cap to 125,000 is seen as a reform Beau can support, even if it's not the complete change he desires.
Beau acknowledges that real change needs to come from individuals, while reform is the role of the government.
He expresses willingness to give Biden time to see the impact of his policies before critiquing them, noting the potential positive impact of the refugee program reform.
Beau stresses the importance of not letting perfect be the enemy of good, supporting policies that may not be ideal but still have a positive impact.

Actions:

for advocates for refugee rights,
Advocate for comprehensive reform of the US refugee program (implied)
Support organizations working to aid refugees and advocate for their rights (implied)
</details>
<details>
<summary>
2020-11-11: Let's talk about rewards, Texas, and mysteries.... (<a href="https://youtube.com/watch?v=N6hQdMyucsQ">watch</a> || <a href="/videos/2020/11/11/Lets_talk_about_rewards_Texas_and_mysteries">transcript &amp; editable summary</a>)

Lieutenant Governor offers rewards for evidence; lack of proof raises doubts about Trump's claims as supporters risk being remembered in history books.

</summary>

"A million dollars is a million dollars."
"There's no evidence."
"All of these people, when this is all said and done, their name is going to go down alongside Trump's."

### AI summary (High error rate! Edit errors on video page)

Lieutenant Governor of Texas, Dan Patrick, offers a million dollar reward for evidence backing up Trump's claims.
Rewards are typically offered when there is evidence, leads, and a clear picture of what occurred.
Lack of evidence and leads raises questions about the validity of Trump's claims.
Ted Lieu offers a million dollar reward to find evidence of Bigfoot, which seems easier.
Focus on offering rewards rather than addressing public health issues and the transition.
People who support Trump's claims risk being recorded in history as attempting to undermine elections and the U.S.
Trump's refusal to accept defeat was expected, but the extent of people supporting him was surprising.

Actions:

for concerned citizens,
Contact local officials to prioritize addressing public health issues (implied)
Join organizations advocating for election integrity (implied)
</details>
<details>
<summary>
2020-11-10: Let's talk about the type of political party we want.... (<a href="https://youtube.com/watch?v=xA92AeFVymo">watch</a> || <a href="/videos/2020/11/10/Lets_talk_about_the_type_of_political_party_we_want">transcript &amp; editable summary</a>)

Beau talks about the type of government and political party we want, encouraging forward-thinking individuals to keep looking for the next issue and engagement, as change comes from people, not government.

</summary>

"Change comes from you, not from DC."
"Governments create reform. People create change."
"You're leading the way."

### AI summary (High error rate! Edit errors on video page)

Talks about the type of government and political party we want.
Expresses surprise that forward-thinking individuals expect major political parties to fully represent their beliefs.
Encourages forward-thinking individuals to keep looking for the next issue and engagement.
Suggests that major political parties and governments will never completely match radical beliefs.
Emphasizes the importance of continuous progress and change, as there will always be new issues.
Asserts that change comes from people, not government, and governments create reform, not real change.
Encourages radical individuals to stay in the fight and lead by example.
States that those on the fringe have volunteered to be on point and lead society through obstacles.
Reminds that if you change enough minds, laws may not even need to change because society has shifted.
Concludes by reassuring that those leading the way will eventually bring others along.

Actions:

for forward-thinking individuals,
Lead by example and keep looking for the next issue and engagement (exemplified)
Change minds to influence laws and society (exemplified)
Clear obstacles for others as you lead the way (implied)
</details>
<details>
<summary>
2020-11-09: Let's talk about whether Democrats and Republicans are the same.... (<a href="https://youtube.com/watch?v=Ysd7V3JkBkc">watch</a> || <a href="/videos/2020/11/09/Lets_talk_about_whether_Democrats_and_Republicans_are_the_same">transcript &amp; editable summary</a>)

Examining the common belief that Democrats and Republicans are indistinguishable, Beau underscores the critical role of rhetoric in shaping societal perspectives for long-term systemic change.

</summary>

"You have to have long-term strategic thought."
"Speech, rhetoric, that is designed to influence thought, is probably a lot more valuable than you think when it comes to achieving long-term systemic change."
"Your goal is probably not going to be accomplished in your lifetime."
"Saying that Democrats and Republicans are exactly the same doesn't help your cause because it comes across as an excuse to not get involved."
"If you want that deep change, if you want to build a society where everybody gets a fair shake, where there is liberty and justice for all regardless of where you live or what you look like, it's not happening overnight."

### AI summary (High error rate! Edit errors on video page)

Examines the common statement that there's no difference between Republicans and Democrats, acknowledging some truth to it.
Points out similarities between the two parties in terms of corporate interests, militaristic operations, and intervention in other countries.
Acknowledges policy differences between the parties, such as immigration and climate change.
Hypothetically considers a scenario where there are no policy differences and focuses on the importance of rhetoric.
Illustrates the impact of rhetoric by contrasting Republican and Democratic statements on military intervention and how they can influence future outcomes.
Stresses the significance of changing societal thought to drive legislative change.
Encourages long-term strategic thinking for those seeking systemic change and societal improvement.
Compares the role of speech and rhetoric in influencing societal perspectives and achieving long-term systemic change.
Emphasizes the value of rhetoric in reinforcing ideas like human rights and regret over harmful actions, even if it's perceived as just words.
Urges radicals aiming for deep systemic change to understand the power of rhetoric in shaping public discourse and influencing future generations.

Actions:

for activists, political advocates,
Think long-term strategic thought (implied)
Understand the power of speech and rhetoric in influencing societal change (implied)
Reinforce ideas like human rights through speech (implied)
Get involved in shaping public discourse and influencing future generations (implied)
</details>
<details>
<summary>
2020-11-09: Let's talk about Biden outsmarting Trump's transition trap.... (<a href="https://youtube.com/watch?v=U9OklK9JBmA">watch</a> || <a href="/videos/2020/11/09/Lets_talk_about_Biden_outsmarting_Trump_s_transition_trap">transcript &amp; editable summary</a>)

Emily Murphy's refusal to acknowledge Biden as the election winner and Trump's attempts to disrupt the transition of power are countered by Biden's well-prepared expert team, showing Trump's true intent.

</summary>

"Trump is trying to disrupt the transition of power, but he was outsmarted."
"Yes, it's going to be a talking point, and there are probably going to be some really scary things said because it is incredibly wrong for President Trump's administration to do this."
"It does, however, show President Trump's intent, and it does answer the question of whether or not he actually cares about the United States or just himself."

### AI summary (High error rate! Edit errors on video page)

Emily Murphy, head of the Government Services Administration, is refusing to acknowledge Biden as the apparent election winner.
Murphy's refusal means Biden is denied access to nearly $10 million for his transition team.
The Trump administration appears to be making foreign policy moves to hinder a Biden administration.
Trump's intent seems to be disrupting the transition of power.
Biden had prepared for this scenario months ago, assembling an expert team in various fields.
Biden's team already picked, including public health experts and cabinet-level department selections.
Despite the disruption, Biden's decision-making and transition plans are intact.
Biden had already raised $7 million for his transition team before this obstacle.
Trump's attempts to disrupt the transition appear to have been anticipated and outmaneuvered by Biden.
Biden's team is well-prepared, with office space ready in the Department of Commerce if needed.

Actions:

for politically active individuals,
Support Biden's transition team by staying informed and spreading awareness (implied)
Stand against attempts to disrupt the transition of power through advocacy and information sharing (implied)
</details>
<details>
<summary>
2020-11-08: Let's talk about blending systems of government.... (<a href="https://youtube.com/watch?v=rC2OrWtuS5o">watch</a> || <a href="/videos/2020/11/08/Lets_talk_about_blending_systems_of_government">transcript &amp; editable summary</a>)

Beau outlines characteristics of fascism in a light version, drawing parallels to the United States' blend between capitalism, Western governments, and subtle authoritarianism.

</summary>

"If you want to know what fascism light looks like, look out the window."
"The United States is the blend."
"The major parties keep everything in house and really only give you a couple options."
"Y'all have a good day."
"The biggest dangers to the United States is the creep towards authoritarian rule."

### AI summary (High error rate! Edit errors on video page)

Exploring the idea of comparable systems to social democracy, discussing a blend between normal capitalism, Western governments, and fascism.
Outlining 14 characteristics of fascism according to Lawrence Britt, focusing on practical examples for Americans.
Describing a light version of powerful nationalism, with flags symbolizing the nation rather than overt displays like marches.
Touching on the disdain for human rights in a more subtle, de facto manner rather than legislated.
Differentiating between internal and external enemies as a unifying cause in fascist regimes.
Imagining a light version of military supremacy with revered status and a significant budget share.
Addressing rampant sexism in a de facto manner in a male-dominated society.
Comparing controlled mass media in terms of media collusion with government talking points.
Exploring the obsession with national security and its impact on government compliance.
Touching on the intertwining of religion and government with subtle rituals to show allegiance.
Mentioning protection of corporate power, suppression of labor power, and disdain for intellectuals in the arts.
Noting the obsession with crime and punishment leading to a large prison population.
Describing rampant cronyism and corruption through examples like no-bid contracts and nepotism.
Hinting at fraudulent elections and the facade of legitimacy in such regimes, with parallels to the United States.

Actions:

for american citizens,
Analyze the existing political systems and their implications (suggested)
Stay vigilant about authoritarian creep towards right-wing ideologies (implied)
</details>
<details>
<summary>
2020-11-08: Let's talk about Florida's minimum wage and the American Dream.... (<a href="https://youtube.com/watch?v=wZlDRhRv940">watch</a> || <a href="/videos/2020/11/08/Lets_talk_about_Florida_s_minimum_wage_and_the_American_Dream">transcript &amp; editable summary</a>)

Beau shares an American Dream story of a laborer turned successful general contractor who prioritizes rewarding labor and employee well-being over profit margins.

</summary>

"Those people who make it, you know, who came from kind of the bottom and worked their way up, they always tend to be the best boss."
"The best capitalists that I have ever met all behaved like socialists when it came to their employees."
"Maybe we shouldn't be striving to pay employees as little as possible."

### AI summary (High error rate! Edit errors on video page)

Shares a personal story within his circle of friends, describing an inspirational journey from a laborer on a construction crew to a successful general contractor.
The individual started off making $7 an hour, worked hard, made good decisions, and got lucky to achieve success.
Despite his success, the general contractor still spends a day each week working with his crews to maintain quality and show solidarity.
Another contractor in the same industry noticed that the general contractor's crews worked faster and asked for the secret.
The key to the general contractor's success was sharing the early completion bonus with his employees.
Beau mentions a recent minimum wage increase in Florida to $15 an hour, with the general contractor paying his employees a minimum of $19.
Beau observes a common trend where individuals who rise from humble beginnings tend to be the best leaders rather than bosses.
Successful capitalists often exhibit socialist behaviors by ensuring their employees have a stake in the company's success.
Beau suggests that rewarding labor and giving employees a stake in the company's success leads to better performance and profitability.
He questions the idea of striving to pay employees as little as possible and proposes that rewarding labor is key to generating wealth.

Actions:

for business owners, employers,
Share early completion bonuses with employees (implied)
Pay above minimum wage to show appreciation for labor (implied)
</details>
<details>
<summary>
2020-11-07: Let's talk about pardoning President Trump.... (<a href="https://youtube.com/watch?v=c66IQQrmXeo">watch</a> || <a href="/videos/2020/11/07/Lets_talk_about_pardoning_President_Trump">transcript &amp; editable summary</a>)

The push to pardon Trump for the sake of preserving the presidency's integrity overlooks the dangers and lessons from his administration.

</summary>

"Pardoning him just encourages the next generation."
"The President of the United States needs to be investigated."
"Before we can reconcile, before we can heal, we have to know what damage was caused."
"Those who want to keep some semblance of a democratic republic, they have to hit every time."
"This is a horrible idea."

### AI summary (High error rate! Edit errors on video page)

Semi-official news confirmed Joe Biden as the next US president, sparking talks on the importance of the presidency.
Some suggest pardoning Trump to preserve the office’s integrity, despite his lack of guaranteed peaceful transfer of power.
Beau agrees in theory about focusing on preserving the presidency’s integrity over individuals.
He refuses to overlook Trump’s actions like misinformation leading to deaths and attacks on constitutional values.
Trump's divisive rhetoric, fear-mongering, and authoritarian tendencies almost tore the country apart.
Beau calls Trump ineffective, lacking in strategy, and a danger if not held accountable.
Concerns arise about future leaders emulating authoritarian tactics, potentially more polished and dangerous than Trump.
Despite Biden's win, millions still support Trump without fully comprehending the risks.
Beau warns of the ongoing threat of authoritarianism and the importance of investigating Trump's administration.
Pardoning Trump could set a dangerous precedent, encouraging future tyrants with nothing to lose.

Actions:

for us citizens, activists,
Investigate Trump's administration publicly to understand the damage caused (implied)
Educate others on the dangers of authoritarianism and the importance of accountability in leadership (generated)
</details>
<details>
<summary>
2020-11-07: Let's talk about America's ghostwriters.... (<a href="https://youtube.com/watch?v=jiPmoTOBSg4">watch</a> || <a href="/videos/2020/11/07/Lets_talk_about_America_s_ghostwriters">transcript &amp; editable summary</a>)

Beau shares the concept of "America's Ghost Riders," underscoring the often uncredited contributions of marginalized individuals throughout history and encouraging viewers to recognize their power in shaping the country's future.

</summary>

"America's Ghost Riders."
"The reality is that it's normally people who have no power who are writing America's story."
"If you feel like you don't have any control, if you feel like you don't have any power… America's Ghost Riders."

### AI summary (High error rate! Edit errors on video page)

Day 732 of waiting for election results, trying to shift focus from election-related content.
Incorporates Easter eggs like hidden quotes from movies or songs in videos.
Mentions a profound line from Snow the Product in the Hamilton mixtape: "America's Ghost Riders."
Describes "America's Ghost Riders" as referencing undocumented immigrants and individuals throughout history who did the work but didn't receive credit.
Points out that marginalized individuals often contribute significantly to shaping America's history.
Emphasizes that those without power often do the work while those in power receive the credit.
States that the country's future depends on ordinary people rather than those in power.
Acknowledges that individuals have more control over the country's direction than they may realize.
Encourages viewers to recognize their role in shaping the country's history as "America's Ghost Riders."

Actions:

for viewers,
Recognize and appreciate the contributions of marginalized individuals in shaping history (implied).
</details>
<details>
<summary>
2020-11-06: Let's talk about family, coworkers, and reconciliation after Trump.... (<a href="https://youtube.com/watch?v=d4IJYIf7WNs">watch</a> || <a href="/videos/2020/11/06/Lets_talk_about_family_coworkers_and_reconciliation_after_Trump">transcript &amp; editable summary</a>)

Beau explains the importance of standing firm on morality over politics in relationships with Trump supporters and advocates for reconciliation based on moral principles, not forgiveness.

</summary>

"It's not politics, it's morality."
"It's about undermining the moral fabric of this country."
"Sometimes it's not the message, it's the messenger."
"Arguing over politics is fine. That happens all the time. It's the plot of movies."
"I'm not sure I want to waste my time with people that I don't have respect for anymore."

### AI summary (High error rate! Edit errors on video page)

Explains his stance on rekindling relationships with Trump supporters due to moral reasons, not politics.
Addresses questions about dealing with family members and colleagues with different political beliefs.
Advocates for focusing on morality rather than politics in these relationships.
Suggests having a moral obligation to help family members right themselves.
Stresses the importance of standing firm on moral principles, even if it means cutting ties.
Emphasizes the deep divide between right and wrong, not just political differences, in relationships.
Points out the moral failings of the Trump administration and the impact on the country's moral fabric.
Argues that reconciliation may not require forgiving and forgetting, especially in cases of deep moral disagreements.
Expresses reluctance to waste time with those he no longer respects due to moral differences.
Concludes by leaving viewers with food for thought on navigating relationships with differing moral beliefs.

Actions:

for viewers seeking guidance on navigating relationships with conflicting moral beliefs.,
Have candid, morality-focused conversations with family members to address deep moral divides and encourage self-reflection (implied).
Stand firm on moral principles and values in relationships, prioritizing right and wrong over politics (exemplified).
</details>
<details>
<summary>
2020-11-06: Let's talk about Biden winning and what's next.... (<a href="https://youtube.com/watch?v=cTBVgqN-ZEg">watch</a> || <a href="/videos/2020/11/06/Lets_talk_about_Biden_winning_and_what_s_next">transcript &amp; editable summary</a>)

Beau reminds us that regardless of the election outcome, there's still much work to be done, and "brunch is still canceled."

</summary>

"Biden's not a savior. He's one person."
"Brunch is still canceled."
"If we want the promises kept, if we want the future that the people who watch this channel really want, it's not over."
"We were on an incredibly dark path."
"We've got a whole lot of work to do and we're going to have to keep at it."

### AI summary (High error rate! Edit errors on video page)

Biden is projected to win, but legal challenges may arise in the coming days.
Regardless of the outcome, there is still a lot of work ahead.
Biden is not a savior; he is just one person.
The focus should be on undoing the damage caused by the previous administration.
It is vital to remove those who enabled Trump from public life and prevent authoritarianism from resurfacing.
The country was on a dark path that may have shifted slightly, but the work is far from over.
Progress requires sustained effort beyond January and into the future.
The struggle for a better future is long and challenging.
Beau humorously reminds that "brunch is still canceled."
The message is reiterated multiple times with slight variations in English and Spanish.
Despite the cancellation of brunch, Beau suggests that it can still be taken to go.
The transcript ends with a wish for a good day.

Actions:

for progressive activists,
Keep pushing for positive change and accountability in government (implied)
Stay engaged in activism and advocacy efforts (implied)
Work towards building power structures to prevent authoritarianism (implied)
Take the reminder to heart: "brunch is still canceled" (implied)
</details>
<details>
<summary>
2020-11-05: Let's talk about how the election isn't all politics.... (<a href="https://youtube.com/watch?v=mz_zAgrA6O0">watch</a> || <a href="/videos/2020/11/05/Lets_talk_about_how_the_election_isn_t_all_politics">transcript &amp; editable summary</a>)

Beau stresses the importance of addressing moral failings before seeking reconciliation and healing in the aftermath of political divisions.

</summary>

"It's not politics. That wasn't politics. That was morality."
"Morality doesn't run on an election cycle. You either have it or you don't."
"We can't move forward until it's all exposed."
"Until the moral failings of large portions of this country are laid bare, discussed, examined, and we figure out why they happened, I'm not ready for reconciliation."
"Because I have no desire to interact with them."

### AI summary (High error rate! Edit errors on video page)

Talks about a prevailing tone in the nation post-election.
Describes his close circle of friends who support each other no matter what.
Mentions how some friends supported Trump initially but withdrew support over border policies.
Explains that his friends, former military contractors, have their own set of morals.
Recounts how debates during elections ceased when real actions unfolded.
Differentiates between close friends with integrity and others who didn't withdraw support for Trump.
Shares a personal experience of cutting off contact with friends who supported immoral actions.
Emphasizes that the issue was about morality, not politics.
Asserts his stance on not reconciling with those who crossed moral lines.
Stresses the importance of addressing moral failures before healing and reconciliation can occur.
Compares the need for facing moral failures in America to Germany's handling of its past.
Calls for public acknowledgment of past wrongdoings and faults before moving forward.
Expresses the need for transparency and examination of moral failings before reconciliation.
States that forgiveness is a personal matter between individuals and their beliefs.
Concludes by advocating for laying bare and discussing the moral failings of the nation before seeking reconciliation.

Actions:

for people reflecting on moral integrity.,
Have open, honest dialogues about past moral failings and faults (implied).
Encourage public acknowledgment and examination of moral failings in the nation (implied).
</details>
<details>
<summary>
2020-11-05: Let's talk about a Trump Biden election update.... (<a href="https://youtube.com/watch?v=RQhvhKBwwx0">watch</a> || <a href="/videos/2020/11/05/Lets_talk_about_a_Trump_Biden_election_update">transcript &amp; editable summary</a>)

Beau stresses the need for ongoing organization and education post-election to counter Trump's rhetoric and shape a better future.

</summary>

"It's up to us, me and you. We have to organize, we have to keep educating, nothing changed."
"Biden may have been able to remove President Trump, okay? But he doesn't have a mandate."
"It was always up to us. It's gonna require a lot of work, a lot of organization."
"We have to continue to turn people away from this. It's going to require a lot of work."
"You're still gonna have to organize to shape those around you, and to show them the failings of the Trump administration."

### AI summary (High error rate! Edit errors on video page)

The world is anxiously awaiting the ballot count results, but patience is needed, as it is not a big deal to wait a few more days.
Despite Biden's potential win, the Democrats did not secure firm control of Congress, meaning Biden won't have a legislative mandate for significant policy changes.
A substantial portion of the American population supports Trump's policies and does not see them as morally wrong, leading to limited policy changes under Biden.
It is up to individuals to organize, continue educating, and push for change as Biden may only have the power to undo certain policies through executive orders.
Trump's successful rhetoric will likely persist even if he loses, requiring individuals to make certain political positions untenable for the Republican Party.
The current situation underscores the necessity for ongoing organization, education, and activism, as the responsibility lies with the people to drive change.
Biden's potential victory, even if narrow, could send a message to those disengaged from politics and help combat Trump's rhetoric in the long run.
The need for sustained engagement post-election remains critical to shape public opinion and demand a country that young Americans can be proud of.
The intensity and activism seen in the past four years will need to continue to counter the influence of Trump's administration and shape a better future.
The onus is on individuals to stay active, organize, and influence those around them to create a country worth being proud of.

Actions:

for activists, organizers, educators,
Organize grassroots efforts to counter harmful rhetoric and policies (suggested)
Continue educating and engaging with individuals to drive positive change (implied)
Shape public opinion through activism and organization within communities (implied)
</details>
<details>
<summary>
2020-11-03: Let's talk about today's Trump Biden election.... (<a href="https://youtube.com/watch?v=yu0JPjjSflU">watch</a> || <a href="/videos/2020/11/03/Lets_talk_about_today_s_Trump_Biden_election">transcript &amp; editable summary</a>)

It's election day in 2020, a referendum on Trump, reminding us that voting is just the beginning and the responsibility falls on the people.

</summary>

"Voting is not the end, it's the beginning."
"Regardless of what occurs in this election, it's up to us."
"At the end of the day, this is a referendum on Trump."
"Ignore it. Ignore him. Wait for the votes to be counted."
"We have a lot of damage to undo."

### AI summary (High error rate! Edit errors on video page)

It's election day in 2020, red versus blue, democracy versus whatever Trump represents.
Joe Biden is significantly favored in the polls, with Trump having a 10% chance of winning.
GOP strategists suggest Trump needs Ohio, North Carolina, and Florida to win.
Biden, on the other hand, primarily needs Pennsylvania to secure victory.
Pennsylvania's results may not be available immediately due to high mail-in voting.
Media reminds people to stay in line after polls close and not to believe premature victory claims.
Trump's doubt in mail-in voting may lead to initial misleading results favoring Biden in some states.
The election is a referendum on Trump, and the outcome will depend on how it plays out.
Regardless of the election result, the responsibility lies with the people to hold leaders accountable.
Voting is just the beginning, with much work ahead to address any damage done.

Actions:

for voters, citizens,
Stay in line after polls close and ensure your vote is counted (implied)
Hold leaders accountable by pushing representatives to keep authoritarian tendencies in check (implied)
Motivate elected officials to move in the right direction post-election (implied)
</details>
<details>
<summary>
2020-11-02: Let's talk about one of the biggest failings of progressives.... (<a href="https://youtube.com/watch?v=MuYhvt7BE1A">watch</a> || <a href="/videos/2020/11/02/Lets_talk_about_one_of_the_biggest_failings_of_progressives">transcript &amp; editable summary</a>)

Beau talks about the importance of diverse messengers, including religious figures, to reach all demographics and advocate for a better world.

</summary>

"Sometimes it's not the message, it's the messenger."
"Everybody can be reached."
"Most people want a better world."

### AI summary (High error rate! Edit errors on video page)

Beau talks about his inspirations for communication style on his channel, mentioning Thomas Paine and Sophie Schall.
Thomas Paine and Sophie Schall tried to meet everybody where they were at, avoiding jargon and academic words to make their messages more accessible.
They framed their arguments philosophically and religiously, connecting with a broader audience.
Beau admits he doesn't consciously frame his arguments in a religious or philosophical context due to the current polarization.
Beau acknowledges the importance of reaching out to all demographics, including those who may need a religious or spiritual framing in their messages.
He mentions the need to connect with people who have been left behind by progressives, mentioning Mary Ann Williams as an example.
Beau shares an experience of seeing a priest on YouTube who framed a message about helping others as a sermon, realizing the impact of religious figures in delivering messages.
Beau stresses the importance of having different messengers for different audiences, including religious figures who can reach people that others might not.
He encourages actively seeking out religious leaders who advocate for a better world and using their messages to reach those who need moral and religious framing.
Beau concludes by suggesting the value of having a diverse toolbox of messengers to connect with various demographics, including religious figures.

Actions:

for progressive activists,
Reach out to religious leaders advocating for positive change (suggested)
Use diverse messengers to connect with different demographics (implied)
Seek out and amplify messages from religious figures promoting a better world (implied)
</details>
<details>
<summary>
2020-11-02: Let's talk about Trump's election night plans.... (<a href="https://youtube.com/watch?v=YQaPqCKSr1Q">watch</a> || <a href="/videos/2020/11/02/Lets_talk_about_Trump_s_election_night_plans">transcript &amp; editable summary</a>)

Beau shares the importance of rejecting Trump and Trumpism for unity and progress while criticizing the administration's divisive actions.

</summary>

"It's always told from the point of view of these great leaders. History isn't really made that way, though."
"I think it's incredibly poetic that while that's hopefully happening, the president will be waiting behind a wall."
"We're trying to build a better country, not go backwards."

### AI summary (High error rate! Edit errors on video page)

Trump initially planned to wait for election results at one of his hotels but changed to the White House.
Reports emerged that a non-scalable fence or wall will be built around the White House and Lafayette Park.
Concerns rose as Trump planned to declare victory even if the votes weren't finalized.
Beau expresses the importance of Trump losing by a landslide for the country.
Trump's administration marginalized and "othered" people for political gain.
Beau views rejecting Trump and Trumpism as vital for unity and progress.
History is made by average people, not just great leaders.
Beau hopes rejecting Trump will help bring the nation together and move forward positively.
Rejecting the marginalization of large portions of the population is a critical goal.
Beau plans to livestream during the election results with his team on YouTube, covering breaking events and state outcomes.

Actions:

for american citizens,
Tune in to Beau's livestream on YouTube to stay updated on election results and breaking events (exemplified).
</details>
<details>
<summary>
2020-11-01: Let's talk about the Redeye, Kentucky State Police training, and journalism.... (<a href="https://youtube.com/watch?v=09weDGk0I-o">watch</a> || <a href="/videos/2020/11/01/Lets_talk_about_the_Redeye_Kentucky_State_Police_training_and_journalism">transcript &amp; editable summary</a>)

Beau praises a high school paper for exposing alarming training materials that push a warrior mindset onto peace officers, urging accountability to protect communities.

</summary>

"This isn't training. This is a justification for barbarism."
"Regular employment of violence. That's how they intend on doing it. This is disturbing."
"This is appalling on so many levels. This is not law enforcement. This is not being a peace officer."

### AI summary (High error rate! Edit errors on video page)

Beau praises the high school newspaper Manual Red Eye for publishing materials and uncovered training materials from the Kentucky State Police.
The training materials for peace officers are alarming, promoting a "warrior's mindset" and extreme aggression.
The materials suggest law enforcement should view themselves as ruthless killers and prioritize violence over other tactics.
Beau criticizes the training materials for being inappropriate and promoting dangerous concepts like disregarding policy and dehumanizing the public.
He questions if officers trained under these materials have been retrained and calls for accountability.
Beau stresses the importance of peace officers understanding their role in protecting the community rather than adopting a warrior mentality.
He expresses concern about the loss of public confidence due to these training materials and their impact on current events like protests.

Actions:

for law enforcement oversight organizations,
Contact local law enforcement oversight organizations for accountability and reform (implied).
Join community efforts to advocate for peaceful policing and de-escalation training (implied).
</details>
<details>
<summary>
2020-11-01: Let's talk about Texas and Trump.... (<a href="https://youtube.com/watch?v=qyKTyAl_XqY">watch</a> || <a href="/videos/2020/11/01/Lets_talk_about_Texas_and_Trump">transcript &amp; editable summary</a>)

The Republican Party in Texas is undermining democracy by attempting to invalidate over 100,000 votes, driven by fear of losing power and resorting to anti-democratic means.

</summary>

"Is it about interfering with the democratic process because they're worried they're going to lose Texas?"
"The Republican Party has lost its way."
"Their worry is that the people's voice is going to be heard because they aren't for the people."
"At this point, they are trying to turn this country into a nation that doesn't have a representative democracy because it doesn't have representation."
"This move should encourage even more people to switch their vote from Republican to Democrat."

### AI summary (High error rate! Edit errors on video page)

The Republican Party in Texas is seeking to invalidate over 100,000 votes cast through drive-thru voting, fearing losing the state.
They are attempting to suppress these votes to undermine the election and ensure their victory.
This move by the Republican Party is seen as undermining the basic principles of democracy and representation.
Beau questions if the GOP cares about representation or just about winning elections through undemocratic means.
He criticizes the Republican Party for straying from its core ideas and principles, especially under Trumpism.
The focus is on the Republican Party's fear of losing power and their attempts to manipulate the democratic process to maintain control.
Beau points out that the GOP's actions are driving more people away from the party towards the Democrats.
The fear of losing Texas seems to be pushing the Republican Party towards anti-democratic actions.
Beau expresses concern over the erosion of democracy and representation in the country due to these actions.
He suggests that the Republican Party's focus on power rather than principles is leading to a loss of support and trust from the people.

Actions:

for voters, activists,
Join voter protection organizations to ensure fair elections (implied)
Encourage voter turnout and awareness in Texas (suggested)
</details>
<details>
<summary>
2020-11-01: Let's talk about Stanford's study about Trump's rallies.... (<a href="https://youtube.com/watch?v=a6noqGHaMjk">watch</a> || <a href="/videos/2020/11/01/Lets_talk_about_Stanford_s_study_about_Trump_s_rallies">transcript &amp; editable summary</a>)

Stanford study links Trump's rallies to 30,000 cases and 700 deaths, raising concerns over loyalty over country and the lasting impact of ineffective leadership.

</summary>

"His goal isn't to own the libs on Twitter. His goal is to run a country."
"It's about waving flags rather than yard signs."
"He's certainly not going to become more tame."
"It's supposed to lie with your neighbors, who are the people that are missing."
"If you can overlook the devastation that this man has caused, you never get to talk about the vets again."

### AI summary (High error rate! Edit errors on video page)

Stanford University conducted a study on the impact of Trump's rallies, linking 30,000 cases to just 18 rallies.
The study suggests that 700 deaths may have resulted from these rallies, painting a grim picture of the consequences.
Comparisons are drawn between the impact of Trump's actions and significant historical events like Oklahoma City and the Vietnam War.
Despite the known consequences of his actions, there is still a chance of Trump being re-elected due to loyalty over country.
Beau expresses concern over the long-lasting scars of Trump's leadership, stating that the country may not withstand another four years of the same.
He points out the importance of loyalty to the people and community over blind allegiance to a political figure.
The focus shifts to the need for responsible leadership and decision-making rather than seeking sensational headlines or social media victories.
Beau warns about the potential worsening of immigration policies and overall leadership under another term of Trump.
He underscores the impact of decisions made by the president on millions of lives and the importance of admitting mistakes in choosing leadership.
Beau concludes by urging reflection on the consequences of supporting ineffective leadership and the implications for the future of the country.

Actions:

for voters, concerned citizens,
Reconnect with the community and prioritize loyalty to neighbors over blind allegiance to political figures (implied).
Encourage open discourse and critical thinking about leadership choices within communities (implied).
Support responsible leadership and decision-making by holding elected officials accountable for their actions (implied).
</details>
