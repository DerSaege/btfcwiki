---
title: Let's talk about standard responses to political ideas and why they're wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GiwP_LxmuaA) |
| Published | 2020/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about standardized responses in political discourse.
- Received a message critiquing his views on housing, healthcare, and ending hunger, comparing guaranteed services to prison or slavery.
- Responds by acknowledging that prisoners do receive basic necessities like shelter, food, and healthcare.
- Contrasts the provision of services in the military to society, where it's geared towards productivity and effectiveness.
- Points out that if everyone in society was supported like the military, focusing on a collective mission, the world could be better.
- Argues that the provision of basic needs in prison doesn't imply that society values these rights for all.
- Suggests that societal structures are designed to control people through coercion and fear of losing basic needs.
- Criticizes the focus on individual success rather than collective well-being and societal change.
- Advocates for a shift towards a society where people unite behind principles for the common good.
- Encourages reevaluation of societal norms and motivations handed down through generations.

### Quotes

- "If they were committed to the mission of I don't know making the world a better place. Might be a better world."
- "Because it is a human right. Because it's something that's required to live."
- "Maybe the way we look at things should change too."
- "Maybe we should be focused on building that society where people do have a mission."
- "Not just making sure our betters stay in power."

### Oneliner

Beau questions societal norms by examining basic needs provision in prison and advocating for collective well-being over individual success.

### Audience

Social Activists

### On-the-ground actions from transcript

- Question societal norms and advocate for collective well-being (exemplified)

### Whats missing in summary

The full transcript provides a deeper insight into societal structures and the need for a shift towards prioritizing collective well-being over individual success.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about standardized responses.
Responses that you hear any time something comes up in political discussion.
It happens a lot.
We're going to take one as an example because it just happened to me and until tonight I
didn't know that this was a standard response.
I got a message in response to my last video where I talked about uniting behind principles
rather than parties or personalities.
And there's a long tirade about what he thinks my principles are and a whole lot of let me
tell you about you type of thing.
In relevant part it says you've talked about housing as a human right and universal health
care and ending hunger.
You know where you can get those things.
Do you know where those things are guaranteed prison or slavery?
Okay I mean sure, kinda.
I mean I guess that in a prison you are guaranteed shelter and food and some form of health care.
You can get those things there.
In fact, you know what you can also get there?
An orange jumpsuit.
But I'm pretty sure I could get an orange jumpsuit outside of prison as well.
That's probably true for these other things.
There may be another subculture that gets all of these things.
I don't know.
The military for example.
They have food security, they have health care, they have housing.
Why?
For very different reasons.
In the military they provide this stuff because it allows the soldier to focus on the mission.
It makes them more productive.
It makes them more effective.
Imagine if all of society was more productive and more effective.
Wouldn't that be great?
If they were committed to the mission of I don't know making the world a better place.
Might be a better world.
The funny part about this is that the argument that is made, the standard response, proves
that it is a right.
People in prison are those who society has deemed worthy of throwing in a cage and stripping
away most of their rights.
Yet, they get shelter.
They get food.
They get health care of some sort.
Because it is a human right.
Because it's something that's required to live.
The fact that prisoners get it and the average person out in free society doesn't may not
say what you think it says.
The reality is we don't want to provide these things as a society, as a country, because
our betters want the warning there.
If you don't do what you're told, if you don't toe the line, if you don't go to work,
engage in the financial transactions that they suggest, if you want to quit because
the working conditions are unsafe or because your boss is harassing you, well you better
give it a second thought.
You better just do what you're told because you may end up hungry or out in the street.
Seems awfully coercive to me, but that is how our society functions.
The carrot is that you may be that one in a million shot that makes it.
The stick is the fact that you may end up one of those that everybody looks down at,
who says that they don't deserve basic human rights that are so necessary they are provided
to people in prison.
This is not a good standard response.
It would be great if we stopped focusing on motivations that were handed to us.
We weren't there when these rules were made.
Society and the world at large has changed a whole lot.
Maybe the way we look at things should change too.
Maybe we should be focused on building that society where people do have a mission, where
we are united behind an idea or a principle, and that that mission is making this a better
place for everyone, not just making sure our betters stay in power.
Anyway, it's just a thought.
I have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}