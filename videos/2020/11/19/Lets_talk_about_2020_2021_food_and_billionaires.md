---
title: Let's talk about 2020, 2021, food, and billionaires....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G6J8NAeKrR4) |
| Published | 2020/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Closing out 2020, looking forward to 2021, discussing billionaires, and food issues.
- United Nations released $100 million to help seven nations facing food insecurity.
- Predictions of 36 to 40 countries experiencing severe food insecurity in 2021.
- Challenges due to the world economy, public health crisis, climate issues, and more.
- The cost to prevent famine is $5 billion, with an additional $10 billion to ease malnourishment.
- Lack of funding from nation states poses a significant concern.
- World Food Program warns of potential famines of biblical proportions.
- Hoping for altruistic billionaires to step up and provide the needed $15 billion.
- Criticizes the broken system where individual billionaires are relied upon for global crises.
- Systemic issues leading to potential worsening of food insecurity globally.
- Political reluctance to address food insecurity compared to other priorities.
- Urges for international aid to prevent starvation and subsequent destabilization.
- Points out the interconnectedness of global issues and the impact of nationalism.
- Emphasizes the need for collective action and challenging nationalist perspectives.
- Warns that without swift changes, 2020's plague may be followed by 2021's famine.

### Quotes

- "15 billion dollars. All the nations of the world may not scrape together 15 billion dollars to stop people from starving."
- "Starvation causes destabilization, which causes migration."
- "In a world as plentiful as this one, this really shouldn't be a thing anymore, but it is."
- "The world is getting bigger. We are not as insulated from these issues as we used to be."
- "If things do not change and change pretty quickly, 2020 might have been the year of the plague. 2021 will be the year of famine."

### Oneliner

Beau addresses the urgent need for $15 billion to prevent global famine in 2021, calling out systemic failures and the reliance on individual billionaires for critical aid.

### Audience

Global citizens

### On-the-ground actions from transcript

- Advocate for increased government funding for food security programs (implied)
- Support organizations working to address food insecurity through donations or volunteering (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the impending global food crisis and the systemic issues contributing to it, urging immediate action and international cooperation to prevent widespread famine in 2021.

### Tags

#FoodSecurity #GlobalCrisis #FaminePrevention #SystemicIssues #InternationalCooperation


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about closing out
2020, looking forward to 2021 and see what's in store for us. We're going to talk about billionaires
and we're going to talk about food. And a little bit of foreshadowing, the United Nations just
released 100 million dollars in funding to help seven nations deal with their food issues. If
things don't change and change quick, in 2021 we can expect 36 to 40 countries to have severe
food insecurity issues. This is because of a combination of the current state of the world
economy, the public health thing, weather caused by climate issues, just a whole bunch of stuff.
The real worry though is that nation states will not come off the cash
to help because their economies aren't doing so great either. The price tag to stop the famine
is five billion dollars. The price tag to ease the malnourishment is an additional 10 billion dollars.
So we're talking about a total of 15 billion dollars that may not be coming from nations,
all the nations in the world, 15 billion dollars. To stop with the world food program,
the Nobel Prize winners, described as famines of biblical proportions.
They are pinning their hopes on individual billionaires coming forward.
Billionaires who probably made a small fortune over the last year.
They need 15 altruistic billionaires to come forward. That's who has all of these people's
lives in their hands because they don't think governments are going to do it.
Sounds to me like we have a little bit of a resource management issue.
Sounds to me like the system's kind of broke. In a world as plentiful as this one, this really
shouldn't be a thing anymore, but it is and it's probably going to get worse because the system's
broke. And at the end of the day, I don't necessarily think that uh, that the world
food program's wrong because let's be honest, if some well-meaning congressperson proposed
legislation to help out with this, it wouldn't get through. That is a politically untenable
position because of nationalism. America first and all that. Why are we helping them?
Now if it was 15 billion dollars for the air force to drop stuff on them, well that'd get
through congress without a fight. But 15 billion dollars for food? I don't know about that.
Who are those people? Why should we help them? I'm going to provide a little bit of self-interest
here. Starvation could be a big issue. Starvation could be a big issue.
I'm going to provide a little bit of self-interest here. Starvation causes destabilization,
which causes migration. Y'all remember that battle cry in the early 2000s?
We gotta fight them over there so we don't have to fight them here.
You better help them over there or you're going to end up helping them here. The world is getting
bigger. We are not as insulated from these issues as we used to be. And it is nationalism that is
to blame for this. Lines on a map. People thinking that because the flag that flew over the hospital
these people were born in is different, well that somehow makes them lesser. Beyond America's
do not live a lesser people. 15 billion dollars. All the nations of the world may not scrape
together 15 billion dollars to stop people from starving. That's pretty disheartening.
And that is some pretty hard evidence that this system is broke. If things do not change
and change pretty quickly, 2020 might have been the year of the plague. 2021 will be the year of famine.
But anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}