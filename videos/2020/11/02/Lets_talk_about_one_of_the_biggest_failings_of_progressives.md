---
title: Let's talk about one of the biggest failings of progressives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MuYhvt7BE1A) |
| Published | 2020/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about his inspirations for communication style on his channel, mentioning Thomas Paine and Sophie Schall.
- Thomas Paine and Sophie Schall tried to meet everybody where they were at, avoiding jargon and academic words to make their messages more accessible.
- They framed their arguments philosophically and religiously, connecting with a broader audience.
- Beau admits he doesn't consciously frame his arguments in a religious or philosophical context due to the current polarization.
- Beau acknowledges the importance of reaching out to all demographics, including those who may need a religious or spiritual framing in their messages.
- He mentions the need to connect with people who have been left behind by progressives, mentioning Mary Ann Williams as an example.
- Beau shares an experience of seeing a priest on YouTube who framed a message about helping others as a sermon, realizing the impact of religious figures in delivering messages.
- Beau stresses the importance of having different messengers for different audiences, including religious figures who can reach people that others might not.
- He encourages actively seeking out religious leaders who advocate for a better world and using their messages to reach those who need moral and religious framing.
- Beau concludes by suggesting the value of having a diverse toolbox of messengers to connect with various demographics, including religious figures.

### Quotes

- "Sometimes it's not the message, it's the messenger."
- "Everybody can be reached."
- "Most people want a better world."

### Oneliner

Beau talks about the importance of diverse messengers, including religious figures, to reach all demographics and advocate for a better world.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Reach out to religious leaders advocating for positive change (suggested)
- Use diverse messengers to connect with different demographics (implied)
- Seek out and amplify messages from religious figures promoting a better world (implied)

### Whats missing in summary

Beau's personal reflections on the impact of diverse messengers in reaching different audiences could provide valuable insights for communication strategies in advocacy and activism. 

### Tags

#Communication #DiverseMessaging #Activism #ProgressiveChange #InclusiveOutreach


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a couple of my inspirations.
People whose style of communication
I consciously attempt to mimic
on this channel
and uh... where I've failed.
Where I've failed to implement their style.
And the reason I want to do that is because
this movement of people,
global movement of people,
want deep systemic change,
want liberty and justice for all, want everybody
regardless of where they live or what they look like to have a fair shake.
We uh...
almost all of us
fail at this.
We almost all fail at this.
And many of us actually have slogans
demanding that we fail at this.
The two people
that I think are the best at this are Thomas Paine and Sophie Schall.
I've talked about both of them in videos.
The thing that
they did, their style, and I say their style, they never met.
They never met.
They were separated by more than a hundred years but they used the same tactics.
The most important thing I think they did
was they tried to meet everybody where they were at.
They didn't use a lot of jargon.
They didn't try to
use overly academic words to make themselves seem more authoritative.
You don't need
a PhD in philosophy
to understand right from wrong.
You don't need to be an expert in geopolitics to understand what's going on in the world.
When you use that jargon,
you limit how far that message can go
because it can only go to those people who know the jargon.
I think that was the most important thing they did.
The second most important thing they did is something that I
don't do consciously, and it's a failing.
They framed their arguments
both philosophically
and religiously.
I don't do that for two reasons. One,
it's really hard.
It is incredibly hard.
And
two, today we're so polarized
that if I did it,
there would be large groups of people that would be like,
I don't want to hear that.
I just don't want to hear that.
And tune out immediately.
And it's fine. There's nothing wrong with that.
Especially given the platforms and the
wide range of communication
that is available today. Which religion?
How are you going to frame it?
We're, as a movement, we're very inclusive.
We wouldn't want to use
one specific
religious framing because it might
alienate others.
This all makes sense in a modern context.
And it seemed
like it was insurmountable.
Like it was a problem that there was no solution to.
But it always bothered me.
Because
when you make the conscious decision not to engage on that level,
you are writing off that demographic of people.
Because
they only have one group
of people who generally do not have their best interest at heart
talking to them.
There's a reason
why in the United States
that voting block is pretty secure.
Because it's only
the conservatives
who are talking to them.
Which is odd
because in the United States, if you're talking about trying to reach the
majority
of religious people in the U.S.,
you're talking about one figure, you're talking about one guy
who was progressive.
He was progressive for the times.
He's progressive today.
Love thy neighbor as thyself.
Look out for the stranger in your land. Feed the poor and all that.
Does that sound like the GOP?
Of course not.
But that's who talks to them.
It's one of the reasons I liked Mary Ann Williams so much, because she had that
spiritual undertone
and could connect
with that demographic of people
who progressives
generally never even try to reach out to.
Because it's too hard.
It is too hard.
And I couldn't come up
with a way to do it.
And then two things happened.
You guys constantly kept telling me that you send my videos to your racist redneck
uncles.
And it makes sense.
I can connect on that level
because of the way I look, the way I sound.
We speak the same language.
Makes complete sense.
And then about a month ago,
I saw a YouTube thumbnail.
It's a priest.
And there's writing on the thumbnail, as oftentimes there is on YouTube.
And one of the words that jumped out at me,
Bo's Rule 303.
I will be honest, when I clicked
play, I felt like I was walking into the principal's office.
And I thought, as it started playing,
that man, it would be great
if we had religious figures
who would
talk about climate change,
talk about toxic masculinity,
talk about limiting beliefs
or encouraging
their followers to read other religious texts
to help break down the divides.
And as the video plays,
I listen to him
add two words
to Rule 303.
He says,
if you have the means to help,
you have the responsibility to help.
And turned it into a sermon.
It's great.
And then I went through his videos
and found everything I was just talking about.
His intro is really cool too.
He says,
he makes a point
of
saying that
his parish sits on the unceded land of the Mi'kmaq people.
Not what you would expect
from a priest, not necessarily.
Especially not in the southern United States
because if they're talking about climate change here,
they're hoping it hurries up.
They're looking forward
to everything going bad.
And as I've listened to him over the last month or so,
I've realized
that these people
who need
that
religious framing, that spiritual framing,
they are no different
than
the people who need
somebody who looks and sounds like me.
Sometimes it's not the message, it's the messenger.
And this group of people, this demographic of people
who we have left behind,
we've just said, no, we're not going to try to reach them
because, you know,
the conservatives already have control over them.
That's a
pretty bad stance for us to take.
Would we willingly ride off any other demographic of people?
Probably not.
Everybody can be reached.
And most people want a better world.
Most people want that world where everybody gets a fair shake.
So maybe it's just the messenger.
It might be good
to have some religious figures
in your toolbox, so to speak,
to send to
your religious uncle.
I didn't mention his name. His name is Ed Trevers.
Again, I understand that there are a whole lot of people here
who
aren't religious
in any way, and that's fine.
That's fine.
But if you acknowledge the value
in having somebody who looks and sounds like me say this,
you probably would acknowledge the value
in having somebody who has a collar
say it, too.
Because
he can reach people
that I can't.
He can reach people that you probably can't
for the same reasons.
They speak the same language,
share those similarities.
It's something that we probably
really need to start actively looking for
because those religious figures exist.
Those religious leaders
who want that better world,
they exist. They're out there.
We just have to find them,
help that message get out,
use their message
to reach those
who are right there.
I mean,
in the United States especially,
they want that moral framing.
They want that religious framing.
And it exists in their text.
You just have to find the people who aren't trying to exploit them
to show them that it's already
what they believe.
Anyway,
it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}