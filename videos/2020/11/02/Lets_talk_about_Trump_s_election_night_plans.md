---
title: Let's talk about Trump's election night plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YQaPqCKSr1Q) |
| Published | 2020/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump initially planned to wait for election results at one of his hotels but changed to the White House.
- Reports emerged that a non-scalable fence or wall will be built around the White House and Lafayette Park.
- Concerns rose as Trump planned to declare victory even if the votes weren't finalized.
- Beau expresses the importance of Trump losing by a landslide for the country.
- Trump's administration marginalized and "othered" people for political gain.
- Beau views rejecting Trump and Trumpism as vital for unity and progress.
- History is made by average people, not just great leaders.
- Beau hopes rejecting Trump will help bring the nation together and move forward positively.
- Rejecting the marginalization of large portions of the population is a critical goal.
- Beau plans to livestream during the election results with his team on YouTube, covering breaking events and state outcomes.

### Quotes

- "It's always told from the point of view of these great leaders. History isn't really made that way, though."
- "I think it's incredibly poetic that while that's hopefully happening, the president will be waiting behind a wall."
- "We're trying to build a better country, not go backwards."

### Oneliner

Beau shares the importance of rejecting Trump and Trumpism for unity and progress while criticizing the administration's divisive actions.

### Audience

American citizens

### On-the-ground actions from transcript

- Tune in to Beau's livestream on YouTube to stay updated on election results and breaking events (exemplified).

### Whats missing in summary

Importance of unity and progress through rejecting divisive leadership and marginalization. 

### Tags

#ElectionNight #RejectTrump #Unity #Progress #Livestream


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the President's plans for election night.
Initially, Trump had planned on waiting for the results at one of his hotels.
Those plans changed, and he's going to be waiting at the White House.
Late last night, by the time you'll watch this,
reports started breaking that he intended on building a non-scalable fence or wall
around the White House and Lafayette Park.
This caused some concern among a lot of people,
especially so because yesterday reports also started breaking
that he planned on claiming victory even if he hadn't won,
even if the votes weren't finalized.
He just planned on saying he won and hoping people went along with it.
And I get the calls for concern, but see, I read this a different way.
I really do.
You know, I've made no secret of the fact that I'm not a huge supporter of Joe Biden,
but I really want to see Trump lose.
I think it's important for the country that Trump lose by a landslide.
This man, this administration, has marginalized people at every turn,
has othered people at every opportunity,
tore the fabric of the American tapestry apart for his own political gain.
From the moment he started campaigning, it was fear,
and he drove the country into this irrational frenzy,
so much so that people wanted to build a wall on the southern border to cower behind.
I think it's incredibly important for all of those people he slighted and marginalized
to see their neighbors, their country, turn out en masse to reject Trump and Trumpism.
I think that's really important.
And I think it's incredibly poetic that while that's hopefully happening,
the president will be waiting behind a wall.
I know there's other concerns, but while history is written to showcase those at the top,
that's how history is told.
It's always told from the point of view of these great leaders.
History isn't really made that way, though.
It's made by average people.
Washington didn't win the Revolutionary War.
A whole bunch of people we will never know the names of did.
It was their action.
And I think that if the people of this country reject Trump and Trumpism,
it'll put us back on the right path.
It will help bring this nation back together,
partially because we won't have somebody intentionally trying to divide it for their own benefit.
But beyond that, all of those people who for the last four years saw news of Americans cheering on
them being marginalized, them being othered, them being pushed to the side,
they get to see that that was blown out of proportion,
that it wasn't really half of the country who opposed them as individuals.
I think that's important.
I think it's one of the most important things that's on the table right now,
is the rejection of the idea that this country can marginalize large portions of its population.
We're trying to get away from that.
That's the goal.
We're trying to build a better country, not go backwards.
So hopefully that's what's going to happen.
Now, while you're waiting to find out what's going to happen,
I will tell you while the results are coming back, I'm going to be live streaming.
Don't know how long.
Somebody told me it's going to be like seven hours.
We'll find out.
I'm going to try to do it the whole time.
The team that is helping me with the second channel, almost all of them, are going to be here.
So it should go smoother.
We're going to cover any breaking events.
We're going to talk about which states are going what direction, all of that stuff.
And we'll have people helping to organize the questions,
which should make things move a little bit smoother.
So make sure you tune in for that.
That's going to be on YouTube if you're watching this somewhere else.
At the end of the day, America's long national nightmare may finally be coming to a close.
But we've still got a little bit of time to wait to see.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}