---
title: Let's talk about rewards, Texas, and mysteries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N6hQdMyucsQ) |
| Published | 2020/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lieutenant Governor of Texas, Dan Patrick, offers a million dollar reward for evidence backing up Trump's claims.
- Rewards are typically offered when there is evidence, leads, and a clear picture of what occurred.
- Lack of evidence and leads raises questions about the validity of Trump's claims.
- Ted Lieu offers a million dollar reward to find evidence of Bigfoot, which seems easier.
- Focus on offering rewards rather than addressing public health issues and the transition.
- People who support Trump's claims risk being recorded in history as attempting to undermine elections and the U.S.
- Trump's refusal to accept defeat was expected, but the extent of people supporting him was surprising.

### Quotes

- "A million dollars is a million dollars."
- "There's no evidence."
- "All of these people, when this is all said and done, their name is going to go down alongside Trump's."

### Oneliner

Lieutenant Governor offers rewards for evidence; lack of proof raises doubts about Trump's claims as supporters risk being remembered in history books.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Contact local officials to prioritize addressing public health issues (implied)
- Join organizations advocating for election integrity (implied)

### Whats missing in summary

The full transcript provides deeper insights into the implications of supporting baseless claims and the importance of focusing on critical issues rather than false narratives.

### Tags

#Texas #Trump #Election #Rewards #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about rewards, Texas, and mysteries.
So if you don't know, the Lieutenant Governor of Texas, Dan Patrick,
has offered a reward, a million dollar reward, for
evidence that would back up Trump's claims.
As anybody who has ever run an investigation will tell you,
you offer a reward when you have evidence, when you have proof,
when you have everything lined up and you have a completely clear picture of what occurred.
That's when you offer a reward.
Now of course not, you offer a reward when you have no evidence,
when you have no leads, when you have nothing to go on.
That's when you offer a reward.
Perhaps the reason nobody has any evidence, nobody has any leads,
nobody has anything to go on, is Trump's lying?
Maybe that's it.
Maybe it's really that simple.
He just made it up.
Maybe he's just acting like a spoiled child, throwing a temper tantrum.
Maybe that's what's happening.
Would make a whole lot more sense.
Because as widespread as this would have to be to actually influence the election,
there would be evidence.
It should be pretty easy to find, but it's not showing up anywhere.
Those people who say they have it, they recant, or whatever.
It never materializes.
Now, that being said, a million dollars is a million dollars.
A million dollars is a million dollars, I was gonna go look.
But it turns out that Ted Lieu is offering a reward of his own.
He's offering a million bucks to go find evidence of Bigfoot.
And to be honest, that seems a whole lot easier.
So I think that's probably the route I'm gonna go.
At the same time, I wanna point something out.
There's no evidence.
There's no evidence.
If there was, they wouldn't be offering a million dollar reward.
They have no leads.
They have nothing to go on.
Yet that's what they're focused on.
They're not focused on the public health issue that is spiraling out of control.
They're not focused on the transition, because a lot of these guys are outgoing.
Perhaps it's because they constantly follow Trump's lead without any critical thought,
rather than, I don't know, paying attention to the public health issue.
Maybe those two things are related.
At the end of the day, I think we all expected Trump to throw a temper tantrum
and claim that he was cheated or whatever.
I think we all saw that coming.
But I'll be honest, what I didn't see was so many people throwing their lot in with his.
I didn't see that happening, to be honest.
All of these people, when this is all said and done,
when this gets recorded in the history books,
their name is going to go down alongside Trump's.
They're going to be the people who get mentioned as those who attempted to end the American experiment,
those who attempted to undermine the elections,
those who attempted to end the U.S. as we know it,
and those who attempted to end the world as a spoiled billionaire couldn't take losing.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}