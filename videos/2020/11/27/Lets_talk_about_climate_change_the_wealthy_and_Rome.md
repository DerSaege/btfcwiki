---
title: Let's talk about climate change, the wealthy, and Rome....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GilUuFWfMMU) |
| Published | 2020/11/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An archaeological find in ancient Rome has sparked popular imagination, leading to two different subtexts being circulated in popular culture.
- In 79 AD, Mount Vesuvius erupted, preserving the remains of two individuals, speculated to be a slave and a wealthier person, possibly the slave's owner.
- One subtext from this find is the idea that ultimately, whether wealthy or poor, we all turn to matter in the ground, a message Beau finds reasonable and acceptable.
- The second subtext suggests that both wealthy and poor suffer equally from environmental issues, particularly climate change, which Beau finds questionable due to the wealthy's access to information and resources.
- Beau believes that instead of focusing on environmental issues to prompt action from the wealthy, a more impactful message could be derived from ancient Rome's Praetorian Guard.
- The Praetorian Guard, responsible for protecting the Roman elite, could serve as a better analogy for the need for security in climate change scenarios, as the wealthy may require protection against potential threats as resources dwindle.
- Beau suggests that security professionals may turn against their wealthy employers in survival situations when resources become scarce, underscoring the importance of human elements in security, even in highly automated scenarios.
- He points out the potential for corruption and ethical decline among security personnel when faced with survival pressures, indicating the necessity for preparedness and caution among the wealthy.
- Beau argues that appealing to the better nature of the wealthy to address climate change may not be effective, as it has not yielded significant action thus far, suggesting that a more powerful motivating tool is needed.
- He concludes by noting the stark differences in resources between the wealthy and the general population, implying that strategies for survival and security will vary vastly.

### Quotes

- "ashes to ashes, lava to lava, wealthy and poor alike, well we are all just matter in the ground."
- "I don't know that appealing to the better nature of the wealthier in the world is going to produce any results."
- "You and I don't have helicopters. They do."

### Oneliner

An archaeological find in ancient Rome prompts reflections on wealth, environmental issues, and security, challenging the effectiveness of appealing to the better nature of the wealthy to address climate change.

### Audience

Climate activists

### On-the-ground actions from transcript

- Build community resilience plans for climate emergencies (suggested)
- Invest in security measures for vulnerable communities (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the intersection between historical analogies, wealth, environmental concerns, and security, offering nuanced perspectives on addressing climate change and inequality.

### Tags

#AncientRome #ClimateChange #WealthDisparity #Security #EnvironmentalIssues


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about ancient Rome
and what ancient Rome
can teach us about today.
We're going to do this because an archaeological find has just captured the
imagination of people.
And news of this find is being used to send
two different subtexts
out into popular culture.
One of them to me makes sense, the other one not so much.
So we're going to talk about that.
So in
79 AD
Mount Vesuvius erupted.
They have recently found the remains of two people, very well preserved.
So well preserved in fact they can
guess that one of them was a slave,
the other one was a wealthier person.
The speculation is that it's the slave's owner.
Now the subtext going out
is you know when it's all said and done
ashes to ashes, lava to lava,
wealthy and poor alike,
well we are all just matter in the ground.
And I can get behind that,
that makes sense.
Good message to have out there.
The other is that wealthy and poor alike
suffer the same
from environmental issues.
That one I'm not so sure about.
I don't know if that will make sense.
Because today the wealthy have access to a whole lot of information.
The obvious nod
in this subtext that's being sent out about the environment is climate change.
They have access to better information.
They know where the most habitable areas will be
in the various scenarios.
And they can construct their little fallback locations and go there with
their resources and supplies and family and personal security and Scrooge
McDuckie and vault.
They feel they have this already beat.
I don't know that it's going to prompt any positive action from them.
I think that if you want to
create a teachable moment
using Rome
for the wealthy of the world
in regards to climate change,
it might be better to talk about the Praetorian Guard.
The Praetorian Guard was
the personal guard and intelligence service of the Roman elite,
particularly the emperor. Their main job
was to keep the emperor safe.
And they did a really good job with this,
with the notable exceptions of Caligula
and Commodus
and Numerian
and Gordian III. There's like a dozen of them
that they took out
or that they assisted in taking out.
That might be a better subtext to be putting out there.
Because unless the wealthy plan on sealing themselves inside bunkers,
they're going to need security professionals
because there's going to be a human element on the outside. So you need one on the
inside to meet it.
Once those security professionals realize that with fewer people those
resources will last longer,
they will turn on their employers
the way Trump turns on a new hire.
That might be a better
message to have out there
in public discourse
because they're going to need them.
And there's no real way around it. It doesn't matter how much automation you
include,
you're going to need a human element
on the inside
to protect against the human element on the outside.
And the human element on the inside can be corrupted.
And ethics tend to slip pretty quickly
in survival situations.
That may be the best message out there.
I don't know that appealing to the better nature
of the wealthier in the world
is going to produce any results.
If appealing to the better nature
was going to do anything we probably would already see action.
And we really don't.
Not enough.
So maybe that's a more powerful motivating tool
rather than just the idea
that at the end we'll all be the same
because we won't.
You and I don't have helicopters. They do.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}