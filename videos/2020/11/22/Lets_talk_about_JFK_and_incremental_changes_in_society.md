---
title: Let's talk about JFK and incremental changes in society....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eGgaGm2xlCU) |
| Published | 2020/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Society changes incrementally over time by shifting thought, not just laws.
- Progressives tend to win because they focus on shaping the future, not just controlling the present.
- Change is a process that requires looking ahead to how we want people to behave in the future.
- Effort and courage need purpose and direction to make a meaningful impact.
- Small changes in behavior now can influence thought and societal norms later.
- Time is a tool for change, not a barrier, and change should happen gradually.
- Historical movements were built on slowly changing opinions and eroding barriers.
- Seize moments when big changes are possible, but also focus on daily progress towards long-term goals.
- Building towards a better future requires understanding that progress may extend beyond our lifetime.
- Incremental change is a proven template for societal progress, both in positive and negative aspects.

### Quotes

- "Change is the law of life."
- "Effort and courage are not enough without purpose and direction."
- "Progressives tend to want to shape the future a little bit more."
- "Seize moments when you can make the big changes."
- "A nation reveals itself not only by the minute produces but also by the minute honors the minute remembers."

### Oneliner

Society changes incrementally through shifting thought, not just laws, focusing on shaping the future, and seizing key moments for progress.

### Audience

Community members

### On-the-ground actions from transcript

- Build parallel structures like community gardens or networks to gradually shift societal norms (exemplified).
- Educate and instill progressive values in future generations to continue the path towards a better world (implied).

### Whats missing in summary

The full transcript provides a detailed roadmap for achieving societal change through incremental shifts in behavior and thought over time.

### Tags

#SocietalChange #Progressives #Future #CommunityGardens #IncrementalChange


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk a little bit more
about societal change and the idea of doing it incrementally
over time.
We talk a lot on this channel about how
to change society, to change the world,
you don't need to change the law.
You have to change thought, the way people think.
And then you don't need the law if you do it that way.
Had a question.
Somebody asked about, well, how exactly does this happen?
First, things do not happen.
Things are made to happen.
It's a process.
You have to look 10 or 15 years in the future and say, this
is how we want people to behave at this point.
And then we come up with the small changes in behavior
that we can make now that will impact thought later, OK?
This is why progressives tend to pretty much always win.
Because those who want the status quo, well,
those who only look at the past or the present
are certain to miss the future.
They're concerned about controlling now.
And they're looking to the past with nostalgia, whereas progressives tend to want to shape
the future a little bit more.
So you come up with your little behavior, and you have to have a plan because effort
and courage are not enough without purpose and direction.
So you have to have the goal of where you want to get to.
And let's say, use the pronoun thing.
We want this group of people more widely accepted.
So then people can just start engaging in small behavior, something like that, that
makes it more visible, makes it more within a societal norm.
Change is the law of life.
It's going to happen over time.
So we use time as a tool, not a couch.
We use the time and the fact that things will change over time to help shift and help get
to where we want to be.
And it's done incrementally.
It's very slowly.
There was a time when having a meal in public with somebody of another race was just unheard
of, but people did it.
That behavior changed.
change thought and it eventually built a movement and when those movements arrive and that moment
in time comes where you can make the big changes, that's when you do it.
And those moments arrive pretty often and you just have to be ready to seize it.
You have to be ready to seize it and just jump on it when it happens.
can achieve a lot in those perfect storm moments as long as you dare to fail miserably.
But in the meantime, when we're not in one of those perfect moments when there can be
a lot of societal change at one time, like the 1960s for example, we have to acknowledge
that moving to where we want to be, moving to that better world where we have that deep
systemic change is a daily, weekly, monthly process that is gradually changing opinions,
slowly eroding old barriers, and quietly building those new structures. Those parallel structures
that are there, they're in place, they're ready when the structures that exist become obsolete
and fade away. It could be a community garden today, a community network, y'all
knew I was gonna say that, and that's how you can shape things that are
realistically probably going to occur after you're gone. And as we do this we
have to understand that we're building towards something that is going to occur
after we're gone, which means our progress in this little goal can be no
swifter than our progress in education because those who come after us have to
share the same values or be more progressive. They have to be willing to
pick up where we left off. In that process, that incremental change, that
template for societal change over time, it's tried and true. It works. It's how
we got here in the good aspects and it's how we can get there and get away from
the bad aspects. Now I want to point out this is filmed and going to be released
on November 22nd and with that in mind I'd like to say that a nation reveals
itself not only by the minute produces but also by the minute honors the minute
remembers.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}