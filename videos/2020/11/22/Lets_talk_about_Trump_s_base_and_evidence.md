---
title: Let's talk about Trump's base and evidence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZAeJwgassYA) |
| Published | 2020/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's presidency was marked by people recognizing his differences from past presidents, both supporters and critics alike.
- Trump is now openly asking state legislatures to undermine the election without presenting evidence to support his claims.
- Despite numerous opportunities, Trump has failed to present any evidence of election fraud in court.
- By asking state legislatures to take drastic actions without evidence, Trump is undermining American democracy.
- The lack of evidence for Trump's claims has led to a segment of the population willing to subvert the Constitution based solely on his word.
- Trump's ability to sway his supporters to believe him, even in the absence of evidence, is concerning.
- There is a looming end to this situation in less than two months, but until then, the trust placed in Trump by his supporters continues to be abused.
- Beau underscores the importance of demanding evidence and critical thinking, rather than blind trust in political leaders.

### Quotes

- "He was a reflection of them."
- "They handed their critical thinking to President Trump."
- "They gave him their trust, and he abused it."

### Oneliner

Trump’s baseless claims have led to a dangerous erosion of trust in American democracy, with supporters willing to subvert the Constitution based on his word alone.

### Audience

American citizens

### On-the-ground actions from transcript

- Demand evidence and critical thinking from political leaders (implied)

### What's missing in the summary

The full transcript provides a detailed analysis of the dangers of blind trust in political leaders and the erosion of democratic principles. Viewing the full transcript gives a comprehensive understanding of the impact of baseless claims on society.

### Tags

#Trump #ElectionFraud #Democracy #Trust #CriticalThinking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the state
of the outgoing president, Donald Trump.
Where he's at, what he's up to.
You know, when he got elected,
everybody knew he was gonna be different.
Everybody knew he was gonna be different.
Didn't matter if you supported him or not.
You knew he was not going to be like other presidents.
That was clear from the get-go.
And right away, there were people saying,
you know, this guy, he's a would-be dictator.
And there was pushback on those statements.
People didn't see it.
The president of the United States
is now at the point where he is tweeting openly,
asking for state legislatures to undermine the vote,
to undermine the election.
In relevant part, he says,
hopefully the courts and or legislatures
will have the courage to do what has to be done
to maintain the integrity of our elections
and the United States of America itself.
You know, every time you think you've hit rock bottom
with this guy, he crawls into some pathetic underground cave
just a little bit further down.
This is asking state legislatures
to subvert the Constitution,
to strike at the very foundations
of American democracy,
200 years of the integrity of the United States, if you will.
And he's doing this with no evidence.
He has had dozens of opportunities
to present evidence in court.
So far, it hadn't happened.
At this point, you have to assume it doesn't exist.
The president isn't going to exhaust his legal options
without presenting evidence if it existed.
And when you're openly asking state legislatures
to do something this drastic,
seems like that evidence should not only be presented in court,
it should be public.
Everybody should have access to it.
We should all be able to review it.
And if that was the case
and it was out there for people to look at,
it wouldn't just be his base cheering this on.
I would imagine that if the allegations he's making were true,
even partisan Democrats would support some kind of action
because it's that much of an affront to the United States.
That evidence doesn't exist.
I've asked people who believe this to show me why.
They can't.
His attorneys haven't been able to present it in court.
It doesn't exist.
And what that means is that you have a decent-sized segment
of the population who is willing to subvert the Constitution,
subvert the United States over a tweet from dear leader
with no evidence.
Just because dear leader says so, we're going to believe it.
This is why those statements were made initially.
This is why people called him a would-be dictator,
because they could see that support from his base.
Because he said the things they wanted to hear.
He was a reflection of them.
Somehow he got these people to see him as one of them.
Somehow they were able to identify
with the spoiled billionaire.
And now, even with years of very clear,
let's just say less than accurate statements,
they're willing to throw away the U.S. Constitution
over a tweet.
That's pretty disheartening.
Now, in two months, this is all going to come out in the wash.
Less than two months.
This is going to be over.
But until then, we have to acknowledge
that there is a group of Americans
who have placed their trust in the president,
and that he's abusing that trust.
And when he does it, we have to be there.
We have to say, okay, sure, that's your allegation.
Where's the evidence?
Let's see it.
Because basically now he's just pulling the dog ate my homework stuff.
You'll get to see it eventually.
Oh, next Tuesday.
Sure.
The United States came pretty close
to something really bad happening,
to truly losing everything that the United States ever was,
all of the positive aspects of the United States,
almost wiped away,
because people chose to believe a tweet
over evidence or lack thereof.
They handed their critical thinking to President Trump.
They gave him their trust, and he abused it.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}