---
title: Let's talk about today's Trump Biden election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yu0JPjjSflU) |
| Published | 2020/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- It's election day in 2020, red versus blue, democracy versus whatever Trump represents.
- Joe Biden is significantly favored in the polls, with Trump having a 10% chance of winning.
- GOP strategists suggest Trump needs Ohio, North Carolina, and Florida to win.
- Biden, on the other hand, primarily needs Pennsylvania to secure victory.
- Pennsylvania's results may not be available immediately due to high mail-in voting.
- Media reminds people to stay in line after polls close and not to believe premature victory claims.
- Trump's doubt in mail-in voting may lead to initial misleading results favoring Biden in some states.
- The election is a referendum on Trump, and the outcome will depend on how it plays out.
- Regardless of the election result, the responsibility lies with the people to hold leaders accountable.
- Voting is just the beginning, with much work ahead to address any damage done.

### Quotes

- "Voting is not the end, it's the beginning."
- "Regardless of what occurs in this election, it's up to us."
- "At the end of the day, this is a referendum on Trump."
- "Ignore it. Ignore him. Wait for the votes to be counted."
- "We have a lot of damage to undo."

### Oneliner

It's election day in 2020, a referendum on Trump, reminding us that voting is just the beginning and the responsibility falls on the people.

### Audience

Voters, Citizens

### On-the-ground actions from transcript

- Stay in line after polls close and ensure your vote is counted (implied)
- Hold leaders accountable by pushing representatives to keep authoritarian tendencies in check (implied)
- Motivate elected officials to move in the right direction post-election (implied)

### Whats missing in summary

The full transcript provides a detailed analysis and reminder of the importance of citizen engagement beyond just casting votes.

### Tags

#ElectionDay #Trump #Biden #Responsibility #Voting


## Transcript
Well howdy there internet people, it's Beau again.
So it's election day, election day 2020,
red versus blue, donkey versus elephant,
democrat versus republican,
democracy versus whatever Trump represents.
Walking into this, it is Joe Biden's presidency to lose.
He is significantly favored in the polls.
Trump only has like a 10% shot of winning.
And because of that, there's absolutely no apprehension at all about today, right?
No, people are worried, of course they are.
Because President Trump has made it through a lot of stuff politically
that realistically he shouldn't have.
So with Trump, a 10% shot is substantial.
It's something to be concerned about.
Now, GOP strategists say that he needs Ohio, North Carolina, and Florida to win.
If Biden gets one of those states, Trump's kind of out of the game.
For Biden's part, he doesn't actually need any of them to win.
He needs Pennsylvania.
If he gets Pennsylvania and the reliably blue states stay blue, he's gonna win.
Doesn't mean he's gonna win in big enough numbers to send a message, but
it means he's probably gonna win.
The problem there is that Pennsylvania, well,
their results may not be back today.
The media is doing a really, really good job of reminding everybody that because of
the high amount of mail-in voting, it's unlikely that we're going to
have a really good exact count today, okay?
They're also doing a good job of reminding people to stay in line
when the polls close.
Now, the reason the media is telling everybody, hey, it's not over till it's
over, is because the report's suggesting that President Trump is going to claim
victory early and just see if people will go with it.
That shouldn't come as a surprise.
He's made stuff up for four years.
He's going to continue to make stuff up.
That shouldn't be a surprise.
Ignore it.
Ignore him.
Wait for the votes to be counted.
Something I don't think they're doing a good job of preparing people for
is because Trump kind of cast doubt on absentee voting, on mail-in voting,
his supporters probably didn't use it as much.
Which means, let's say here in Florida, let's say they count the mail-in votes
first, those numbers come out early.
It's going to look like Biden swept the state, because it is likely
that more Democrats used mail-in voting.
Then as the in-person voting starts getting tabulated, it will trend Republican.
That doesn't mean that something weird has happened.
It doesn't mean the election's been undermined.
That's expected.
The opposite is true too.
In areas where they're going to count the in-person vote first, and
then the mail-in votes, it may appear that Trump's winning,
when he ends up losing by a pretty big margin, because the mail-in votes come in
later.
We need to be ready for that.
Don't panic.
That is something that is highly likely to occur.
At the end of the day, this is a referendum on Trump.
That's what it is.
And we're going to see how it plays out.
I will be live streaming later.
And even though we won't have an exact count on who wins by the end of today,
more than likely, we should have a good idea on how it's going.
So I'll start live streaming later this evening and
go until the batteries die or whatever, and we'll just see how it plays out.
We'll see what happens.
At the end of the day, we've talked about how there's the great man theory,
that leaders are held up when history is talked about.
This happened because of President Trump.
This happened because of Obama, whatever.
It's not how it works, not in real life.
It happens because of the average people,
the people that don't get mentioned in history books.
Regardless of what occurs in this election, it's up to us.
If Trump wins, it's up to us to keep his
authoritarian streak in check, to push our representatives, to keep him in check.
If Biden wins, it's up to us to motivate him to move in the right direction.
Voting is not the end, it's the beginning,
especially if Biden wins.
If Biden wins, it's the beginning, because we have a lot of damage to undo.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}