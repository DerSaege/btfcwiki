---
title: Let's talk about Trump, legitimacy, the media, and phone calls....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ExdEPbH5YgM) |
| Published | 2020/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pundits and commentators are still suggesting that Trump can somehow retain power through phone calls in a convenient fiction for the masses.
- American people have never had to worry about the recognized government due to peaceful transfer of power, a concept foreign to other countries.
- Trump's control is slipping as world leaders, international organizations, media outlets, and large companies are recognizing Biden.
- The transition of power is already happening, with state governments needing to fall in line for Trump to retain power, which is unlikely.
- The media's portrayal of Trump still having a shot undermines faith in American democracy and is more damaging than his actions.
- The blame rests with the media for not treating Trump's chances realistically and instead sensationalizing for views.
- Continuing to treat Trump as having a shot undermines faith in democracy and harms the foundations of the country.
- Some individuals are not acting in good faith by perpetuating the idea of Trump's legitimacy for personal gain or ratings.

### Quotes

- "Prank caller, prank caller."
- "It's over. They're the conduit by which that message is spread."
- "I don't see a lot of ways for him to even pretend to have legitimate control."
- "I don't think it's right to lean into this idea and scare people simply for ratings."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Pundits create a convenient fiction as Trump's control slips, media sensationalizes, and democracy's foundations are undermined.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Recognize and question sensationalized media narratives (implied)
- Uphold faith in democracy by promoting realistic political discourse (implied)
- Hold media outlets accountable for their coverage of political events (implied)

### Whats missing in summary

The full transcript provides a nuanced analysis of the media's role in undermining democracy and sensationalizing politics for ratings, urging viewers to critically analyze information and uphold faith in democratic processes.

### Tags

#Trump #Legitimacy #Media #Democracy #TransitionOfPower


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about legitimacy and President Trump and phone calls in fiction.
Right now there are still pundits and commentators leaning into the idea that somehow Trump can
still pull it off, and you have the attorneys out there saying one thing in public and then
something else in court, creating this convenient fiction for the masses in hopes of securing
their brand and creating a situation that, well there's a bunch of reasons they could
be doing it.
But realistically at this point they're probably not trying to retain control.
That's over.
Something the American people have never really had to worry about is who the recognized government
is.
It's something that has happened in other countries, but it doesn't happen here.
Because we've always had a peaceful transfer of power.
It's always been real simple, real easy.
That transition is already occurring because it's not just government offices that matter.
It's not just the executive branch that matters.
On January 21st, do you picture world leaders taking Trump's phone calls?
Prank caller, prank caller.
They're not talking to him.
What about international organizations?
The UN, NATO?
No, of course not.
They'll talk to Joe Biden.
Most respectable media outlets have already called it.
Large companies within the United States, they would have to be on board for Trump to
retain power.
They're not.
Twitter has already said that January 20th the POTUS account belongs to Joe Biden.
This is all de facto recognition.
This is part of the transition.
It's already occurring.
What about state governments?
Governors, mayors, all of these people, they're also going to have to fall in line for him
to retain power.
That's not likely either.
While the federal government has ceded more and more power to the executive branch, our
elections are still very decentralized.
They're determined at the county level.
To undermine that requires an amazing amount of consensus.
Trump would have to build a coalition, not something he's really known for being able
to do.
Decentralized things like that, once they're in place, they're very resilient.
They last.
They're hard to break, even when somebody's trying.
This idea that the longer he acts the way he's acting, the more he undermines faith
in the American institution of democracy, I don't know that that's true.
I think that a large part of the blame rests with the media, who continually leans into
the idea that maybe he still has a realistic shot.
They're not treating it as the one in a billion chance that it really is.
There aren't a lot of pathways left for the president.
There's not a lot of ways that he can even feign legitimacy anymore, but they still keep
treating it as though he's got a shot of pulling it off.
He doesn't.
He lost.
The longer they go without treating him as the grumpy old man who lost and can't accept
it, the more people are losing faith.
They're just as guilty, if not more so, because if all of these pundits who have chosen to
lean into this idea for ratings or views, if they all just said, no, well, it's over,
it's over.
They're the conduit by which that message is spread.
They're the ones out there undermining the foundations of democracy in this country.
That's realistically what it is.
I think that should be remembered, because I think there are some people who aren't doing
this in good faith.
They don't truly believe that he has a shot, but they're still treating it like that, because
it is good for views.
That drama, if they can drag that out until January, it'll be great.
I don't see a lot of ways for him to even pretend to have legitimate control.
I don't see any way for him to actually maintain control, because most of the state-level governments
and the international community are already recognizing Biden.
I don't think it's right to lean into this idea and not treat it as the outside chance
that it is and scare people simply for ratings.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}