---
title: Let's talk about societal change and pronouns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FaFK9uqbqrY) |
| Published | 2020/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Changing societal thought doesn't always require changing laws but altering people's perspectives is key.
- Received a message suggesting putting preferred pronouns on Twitter bio to challenge societal norms.
- Society currently relies on guessing preferred pronouns based on appearance, creating potential for misidentification.
- Identifying preferred pronouns is specific to certain groups, leading to further differentiation and othering.
- Encourages those without an obvious need to state preferred pronouns to do so to normalize the practice.
- Simple actions like adding pronouns in bios can gradually shift societal views and make it a common practice.
- Inclusion of pronouns can act as a signal of acceptance and create a welcoming space for others.
- The widespread adoption of sharing pronouns can help individuals who don't neatly fit into traditional gender categories.
- Small actions like this can contribute to changing divisive issues over time.
- Predicts that in the future, assumptions about gender roles based on appearance will diminish.

### Quotes

- "It's a good example of how little actions can over time shift the way society looks at an issue that today is very divisive."
- "The widespread use of this can help them feel more accepted, can help them more included."
- "It's just a thought."

### Oneliner

To change societal norms, start by normalizing sharing preferred pronouns in everyday interactions, fostering inclusivity and acceptance.

### Audience

Social media users

### On-the-ground actions from transcript

- Add preferred pronouns to your social media bio (suggested)
- Encourage others to include their preferred pronouns in their profiles (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of how small actions like sharing preferred pronouns can lead to significant shifts in societal perspectives. It also underlines the importance of inclusivity and acceptance for individuals who don't conform to traditional gender roles. 

### Tags

#SocietalChange #Inclusivity #Acceptance #GenderIdentity #SmallActions


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
how to shift societal thought. It's a very common theme on this channel that to change
society you don't have to change the law, you have to change the way people think. And
in some cases it's a whole lot easier to shift the way people think than it is to get legislation
passed. With that in mind I'm going to tell you about a message I received. In relevant
part, the body of it starts with, I'd love to see you put your preferred pronouns in
your bio on Twitter. What? Why on earth would I need to do that? Look at me. Nobody is going
to look at me and mistake, oh that's the whole point isn't it? Yeah. As it stands right now,
the societal norm is to not state your preferred pronouns. To say whether you prefer to be
called he or she or whatever. The idea, the common way of doing it is to just look at
the person and guess. Right? That's why nobody would think to look at me and say she, because
I fit very neatly inside the he box. Right? Identifying your pronouns is something that
is currently pretty specific to a certain demographic of people, to a certain group.
And it's yet another thing that identifies them. It's something else that is out of the
norm of current societal thought that can be used to other them in some way. To change
this you need people who have no obvious reason to state their preferred pronoun to do it.
Because then it's not something that is specific to that demographic. It's just a thing that
everybody does. This is something that is ridiculously simple to do that over time will
shift the way society views this. It won't be something that is specific, that is out
of the norm. It's just an everyday occurrence. When I tweeted about it, because I was like
this never would have occurred to me, thanks for pointing it out, somebody else brought
something else out. It also kind of acts like a flag. It acts like a flag or a bumper sticker.
If you have that in your profile, it's safe to assume that you are not going to be opposed
to the people who currently do that. To the people who are stereotyped with this request.
If you have that in your profile, it's kind of like saying, hey, friendly space here.
And it makes sense. This is one of those things that it doesn't take anything for me to do
it. It doesn't take anything for you to do it. But there are some people who do not fit
nicely, neatly, perfectly into those little boxes. That widespread use of this can help
them feel more accepted, can help them feel more included. And it's harder for people
to other them with this if there's a whole bunch of people doing it. Because it's not
a practice that then is specifically associated with them. It's just something that occurs.
It's a good example of how little actions can over time shift the way society looks
at an issue that today is very divisive. Fifteen, twenty years from now, probably won't be.
Probably won't be a big deal at all. And I would imagine that by the time it's all said
and done, it won't matter. It will get to the point where we don't look at somebody
and assume anything about their gender role based on their appearance. That's something
that I would imagine is going to occur probably within my lifetime. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}