---
title: Let's talk about how Trump brought manufacturing back and Yang....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x-24eJCZtUU) |
| Published | 2020/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's campaign promise to bring manufacturing back to the US is somewhat happening, attributed to companies wanting to move production closer to home to reduce supply chain length.
- Reasons for this include concerns about climate change and its impact on stability, exposure of weaknesses during crisis due to mishandling public health, national security, and trade disputes caused by Trump's tariff wars.
- Trump's actions worsened climate change, leading to companies bringing production closer to home to avoid interruptions in the supply chain.
- Companies are also responding to Trump's incompetence in handling public health crises.
- Trump's focus on national security led to companies reconsidering manufacturing components in countries that could become opposition in the future.
- The erratic nature of Trump's tariff wars and tweets created uncertainty for companies, prompting them to adjust their supply chains by bringing manufacturing closer to home.
- Despite claims of promises made and promises kept, Beau notes that manufacturing is returning, but not the manufacturing jobs due to automation and the increasing use of robots to reduce labor costs.
- Beau references Merrill, Bank of America's report suggesting a doubling of robot use in industries over the next five years due to decreasing automation costs.
- While manufacturing may be resurging, traditional union manufacturing jobs are not returning at the scale people imagine.
- Beau mentions Andrew Yang's warnings about automation and job displacement during the Democratic primaries, criticizing the Trump administration for not foreseeing this shift.
- The return of manufacturing does not equate to the return of traditional manufacturing jobs due to automation and evolving technologies replacing human labor.
- Beau questions the trade-off of worsening climate change and increasing the potential for war with major powers in exchange for a limited resurgence in manufacturing.
- He concludes by suggesting that the promised manufacturing jobs are obsolete and phased out, with the current landscape not resembling the past industrial era.

### Quotes

- "Manufacturing is coming back, but those jobs are probably not coming with it."
- "The robots are coming for your job."
- "Those good union jobs, they aren't."
- "It's not going to be like the old days because it isn't the old days."
- "Those manufacturing jobs, those that he promised, they're not coming. They are phased out."

### Oneliner

Trump's policies have led to a resurgence in manufacturing in the US, but automation means traditional jobs aren't returning as promised.

### Audience

Policy Analysts, Activists

### On-the-ground actions from transcript

- Advocate for policies that support workers transitioning to new industries (implied)
- Support education and training programs for emerging technologies and industries (implied)
- Stay informed about the impact of automation on job markets and advocate for measures to protect workers (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how Trump's policies impacted the resurgence of manufacturing in the US and the lack of traditional manufacturing jobs due to automation and evolving technologies. Watching the full video can provide a deeper understanding of these dynamics and their implications. 

### Tags

#Manufacturing #Trump #Automation #JobDisplacement #ClimateChange #NationalSecurity #TradeWars


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about how
Trump brought manufacturing back to the United States.
This was a big campaign promise of his in 2016.
And I mean, it's kind of happening.
So what we're going to do is go through the reasons it's
happening and see if you can really take credit for it.
If this really is promises made, promises kept.
To give you an overview, short version
is companies want to move production closer to home.
Now, that may be within their nation of origin
or just within the region.
But they want to reduce the length of their supply chain.
That's their goal.
OK, the reasons they're doing this.
Number one, climate change and the instability
it's likely to cause.
Given the fact that President Trump did a whole bunch
of things to make climate change worse, yeah, sure,
we can give him credit for this.
This reason falls under him, at least in part,
in that 4D chess kind of way.
The second, the exposure of weaknesses in times of crisis.
Basically, he mishandled the public health thing so much
that companies experienced interruptions in their supply
chain.
So they don't want that to happen again.
They're responding to his incompetence.
The third thing, national security.
And this one he intended to do.
Trump rightfully pointed out that a lot of components
for military hardware are made in countries
that at some point in the future could become opposition.
That's probably going to change.
I would point out that he missed the fact
that the reason they're likely to become opposition
is climate change, and that generally speaking,
economic ties between countries prevents war.
The fourth is trade disputes, his tariff wars.
Not so much the tariffs themselves,
but the erratic nature.
Basically, companies could not respond fast enough
to his tweets to keep their supply chain intact.
It was too erratic.
They didn't know what was going on.
Uncertainty is bad for business.
So they're changing that.
They're bringing manufacturing closer to home.
It's four of the five reasons.
And if you were a Trump supporter right now,
despite my little jabs, you're probably saying, hey,
promises made, promises kept.
I would like to point out that I have
been saying manufacturing is coming back, not manufacturing
jobs, because the fifth reason is labor costs.
This is from Merrill, Bank of America.
Firms in almost all industries plan
to make the transition work using robots and automation.
They're suggesting that the use of robots
will double in the next five years,
maybe even more than that, because the cost of automation
keeps dropping.
So while manufacturing may be coming back,
those good union jobs, they aren't.
It's not going to be like the old days,
because it isn't the old days.
The United States historically is not
a country that looks to the past, which
was a critical error in the Trump administration.
I would also like to point out that it's pretty telling that
he didn't see this coming, that nobody in the administration
saw this coming, given the fact that
during the Democratic primaries, there
was a candidate who was pretty much just saying this over
and over and over and over again.
Andrew Yang did a really good job
of explaining that the robots are coming for your job.
At the end of the day, manufacturing is coming back,
but those jobs are probably not coming with it,
not to the scale that people are imagining.
There will be those who maybe the robots will be made here.
Maybe there's that.
There will be some technician jobs.
But the idea of those manufacturing jobs,
those that he promised, they're not coming.
They are phased out.
They are obsolete.
And again, I would point out that this
was accomplished by making climate change worse
and increasing the chance of war with major powers.
I'm not sure that was a good trade,
despite the fact it came from the guy behind Art of the Deal.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}