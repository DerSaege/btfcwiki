---
title: Let's talk about calm, the election, Trump, and waiting...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x9lvLnTM_Xw) |
| Published | 2020/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tells a story from years ago about a concerning situation where friends were missing.
- Friends rushed to a house, preparing for action but were faced with uncertainty.
- The group sat around, waiting for more information, likening it to a board game.
- Draws parallels between the past situation and the current state of the country.
- Mentions the current election situation and Trump's attempts to undermine it.
- Expresses that the outcome of the election is beyond their direct influence.
- Acknowledges economic and public health issues that they can impact directly.
- Emphasizes the need to focus on things they can change rather than stressing about what they can't.
- Suggests staying calm, keeping an eye on the situation, and waiting for decisions from others.
- Urges against panicking and instead focusing on actionable areas while being aware of the larger picture.

### Quotes

- "The pieces were on the board and we were all aware of the situation and what might happen, but there wasn't anything we could do because we didn't have the information."
- "We're all keeping an eye on it. But there's nothing you can really do until the people not at that table decide what they're going to do."
- "It's not something that is likely to occur. So it might be best if we focused on the things in the room that we can change rather than stuff that realistically we can't."
- "And there's no sense in getting your blood pressure up over it until those who are in a position to directly affect the outcome make their decisions and those variables get decided."
- "Y'all have a good day."

### Oneliner

Beau shares a story of waiting in uncertainty, linking it to the current state of the country, urging calmness, focus on actionable issues, and waiting for decisions beyond their control.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Focus on addressing economic and public health issues within your community (implied).
- Stay informed about current events and remain committed to positive change (implied).

### Whats missing in summary

The full transcript provides a reflective perspective on waiting for outcomes beyond control, encouraging focus on actionable issues while keeping an eye on larger events.

### Tags

#Uncertainty #CurrentEvents #Election #CommunityAction #Focus


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to start off with a story.
Years ago, seems like a lifetime ago, I was a consultant, some of y'all know that.
Had these guys I knew, they took a gig and they missed a check in, which that's not good.
When that happened, everybody who knew them, their friends, waking up in the middle of
the night, driving 100 miles an hour to get to this house where we're all meeting up.
Guys showing up with their bags packed in case they had to go over and get them.
Once we were all there, we sat down at the dining room table and the owner of the house
brought out the board game resk and we sat around.
We set up all the pieces and then the owner of the house had to go and look for the dice
because there weren't any dice in the box.
He's going to take them out on like Monopoly and stuff like that.
While he's doing that, somebody else at the table was like, man, this isn't an apt analogy,
I don't know what is.
All the pieces on the board, but the die hadn't been cast yet.
The variables hadn't been decided, just waiting.
That's what was going on in that room.
The pieces were on the board and we were all aware of the situation and what might happen,
but there wasn't anything we could do because we didn't have the information.
Whatever was going to happen was beyond our control.
It was going to be decided by other people who weren't going to be influenced by us.
That's where we were at in that room and that's where we're at as a country right now.
Anything that happens is going to be decided by people who aren't sitting at that table.
They're considering engaging in something that is so extreme, they're not going to be
influenced by the people at that table.
I got a message saying that I was very calming, talking about the current election stuff
and Trump's attempts to undermine it, and they pointed out that other people were going
the other way with it and they didn't know whether or not they should panic or not.
I'm not going to put the other people out there, their names out there, because I like them
and I don't think they're necessarily wrong.
I just think they have a different perspective on it.
At the end of the day, we're running out of chips.
We have economic issues.
We have a public health issue in the middle of the holidays.
We have stuff that we can directly impact, that we can be stressed about.
The election stuff is going to be decided by people not at that table.
Just like that night, we're all aware of it.
We're all keeping an eye on it.
But there's nothing we can really do until the people not at that table decide what they're
going to do.
Their actions are so extreme that it doesn't matter if we call them and talk to them.
They're going to do what they're going to do.
Until that happens, until the die is cast and the variables are decided, we just have
to wait, play the game, relax, keep an eye on it, be aware of it.
You know it's happening, but there's nothing you can do directly to influence it right
now.
That time may come later.
If the die is cast and that's the way it rolls, well then we have to address it then.
But there's no sense in panicking about it until that happens because it's an outside
shot.
It's not something that is likely to occur.
So it might be best if we focused on the things in the room that we can change rather than
stuff that realistically we can't.
We can watch it.
We can pay attention.
We all know what's going on.
And we all know that we're committed to that not happening.
But at the moment, all we can do is wait.
And there's no sense in getting your blood pressure up over it until those who are in
a position to directly affect the outcome make their decisions and those variables get
decided.
Then there will be courses of action that become apparent.
So then we're just waiting.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}