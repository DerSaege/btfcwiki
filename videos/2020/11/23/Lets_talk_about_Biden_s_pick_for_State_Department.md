---
title: Let's talk about Biden's pick for State Department....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1pkswgpLKDk) |
| Published | 2020/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ran across a foreign policy thread online where they mentioned him and debated Trump's foreign policy.
- Beau believes Trump's foreign policy cannot be overstated in its badness.
- Blinken is rumored to be the next Secretary of State, prompting questions about his progressiveness and potential foreign policy.
- Blinken is described as a classic liberal, not a progressive.
- If State Department runs as usual, Blinken’s foreign policy likely mirrors Obama's, better than Trump's but not a complete fix.
- Biden campaign's creation of a second State Department suggests awareness of challenges.
- Blinken's long advisory role with Biden and logistics skills may make him a good fit to provide quick information.
- Speculation on potential scenario where area heads create options for Secretary of State to present to the president.
- Uncertainty on whether Blinken's appointment will be good or bad, as it's all speculation until actions are seen.
- Beau acknowledges disagreements with Blinken's past foreign policy decisions but sees potential for him to play a key role in the administration.

### Quotes

- "There is no way to overstate how bad I believe Trump's foreign policy is."
- "Anything that is said now is pure speculation."
- "We're not going to get a good read on it until we see them in action."

### Oneliner

Beau debates Blinken's potential foreign policy under Biden, noting uncertainties and speculations on its effectiveness. 

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Monitor Blinken's actions once in office to gauge the impact of his foreign policy decisions (implied)

### Whats missing in summary

Insights on the potential implications of Blinken's foreign policy decisions and the need for vigilant observation post-appointment.

### Tags

#ForeignPolicy #Blinken #Trump #Biden #StateDepartment


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to start off with a story.
I was online and I ran across a discussion on foreign policy and started reading the
thread.
About halfway down, they mentioned me and one of them was saying that the other was
overstating how bad I believed Trump's foreign policy is.
Let me be clear, that's impossible.
There is no way to overstate how bad I believe Trump's foreign policy is.
You can't do it.
I found that funny because it just happened and it leads right into the topic today.
Because it does appear that Blinken is the heir apparent to become the new Secretary
of State.
Because of that there are questions.
The first, is he a progressive?
The second, what would his foreign policy look like?
The third, would it be better than Trump's?
And the fourth, is this good or bad?
Okay, is he a progressive?
No, absolutely not.
He's a classic liberal.
He's a classic liberal.
And that leads to what would his foreign policy look like.
If left to his own devices and the State Department was run the way the State Department is most
times run, the foreign policy would look just like Obama's.
Better than Trump's?
Sure, sure.
But Obama's foreign policy would not fix all of the problems that Trump has caused when
it comes to foreign policy.
There are issues all over the world.
Obama's foreign policy would not address them.
It wouldn't be enough.
So that sounds like bad news.
And yeah, it is in a way.
However, that second State Department that the Biden campaign built, that leads me to
believe that Biden is not underestimating the challenges ahead of him.
It should be noted that Blinken has been in his orbit, one of his advisors, for a very,
very long time, and is said to have had a pretty big role in creating that second State
Department, which makes sense because he is a logistics guy.
He is somebody who can bring pieces together.
Now if the State Department was run a little bit differently, if the Secretary of State
was a clearinghouse, you know, the way it normally runs, the area heads, the working
groups, they provide advice to the Secretary of State, who then creates options and presents
them to the president.
The president says, this is what we're going to do.
If those area heads, those working groups, were to create the options, because they're
the real experts, and then present them to the Secretary of State, probably with one
option underlined, saying this is what we think we should do, and really the president
and the Secretary of State be kind of final approval, the decision was kind of already
made, it would make perfect sense to pick Blinken, because he is somebody who has been
in Biden's orbit a long time, can probably quickly provide him the information needed,
because they've talked to each other before.
They have a rapport.
That is more in line with Biden's previous actions in regards to foreign policy.
It may be that that's what the plan is, because Blinken picked a lot of these people, even
those who would have more progressive policies.
So that is a very realistic possibility.
That would be enough to fix all of the problems that Trump caused.
Is it good or bad?
We don't know.
We don't know.
Anything that is said now is pure speculation.
President Trump was unprecedented.
The actions of the person who follows him are going to be unprecedented too, because
he's got a big mess to clean up.
We can't guess.
There are things in Blinken's past that I do not agree with.
He's made some foreign policy decisions that I do not agree with.
However, if he is being brought in to serve as the clearinghouse, as the focal point,
the person to bring all the pieces together and present it to Biden with, you know, underlined
options, he's probably a really good choice.
But we don't know.
We have no idea and we won't know until Biden takes office and a few months passes and we
see how they behave.
Until then, everything is speculation.
We're not going to get a good read on it until we see them in action.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}