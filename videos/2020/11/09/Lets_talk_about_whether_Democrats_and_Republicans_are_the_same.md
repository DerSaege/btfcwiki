---
title: Let's talk about whether Democrats and Republicans are the same....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ysd7V3JkBkc) |
| Published | 2020/11/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines the common statement that there's no difference between Republicans and Democrats, acknowledging some truth to it.
- Points out similarities between the two parties in terms of corporate interests, militaristic operations, and intervention in other countries.
- Acknowledges policy differences between the parties, such as immigration and climate change.
- Hypothetically considers a scenario where there are no policy differences and focuses on the importance of rhetoric.
- Illustrates the impact of rhetoric by contrasting Republican and Democratic statements on military intervention and how they can influence future outcomes.
- Stresses the significance of changing societal thought to drive legislative change.
- Encourages long-term strategic thinking for those seeking systemic change and societal improvement.
- Compares the role of speech and rhetoric in influencing societal perspectives and achieving long-term systemic change.
- Emphasizes the value of rhetoric in reinforcing ideas like human rights and regret over harmful actions, even if it's perceived as just words.
- Urges radicals aiming for deep systemic change to understand the power of rhetoric in shaping public discourse and influencing future generations.

### Quotes

- "You have to have long-term strategic thought."
- "Speech, rhetoric, that is designed to influence thought, is probably a lot more valuable than you think when it comes to achieving long-term systemic change."
- "Your goal is probably not going to be accomplished in your lifetime."
- "Saying that Democrats and Republicans are exactly the same doesn't help your cause because it comes across as an excuse to not get involved."
- "If you want that deep change, if you want to build a society where everybody gets a fair shake, where there is liberty and justice for all regardless of where you live or what you look like, it's not happening overnight."

### Oneliner

Examining the common belief that Democrats and Republicans are indistinguishable, Beau underscores the critical role of rhetoric in shaping societal perspectives for long-term systemic change.

### Audience

Activists, Political Advocates

### On-the-ground actions from transcript

- Think long-term strategic thought (implied)
- Understand the power of speech and rhetoric in influencing societal change (implied)
- Reinforce ideas like human rights through speech (implied)
- Get involved in shaping public discourse and influencing future generations (implied)

### Whats missing in summary

The full transcript provides a deep dive into the significance of rhetoric in shaping societal thought and achieving long-term systemic change.

### Tags

#PoliticalRhetoric #SystemicChange #Activism #Democracy #SocietalImpact


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a common statement,
something I hear a lot,
and uh...
it's coming up a whole lot right now.
And
I want to get to the meat of the statement.
See, first off,
if it's true,
and second off,
even if it is true,
does that mean that it's bad?
Okay, so the statement. We've all heard it.
There's no difference between Republicans and Democrats.
And on many levels that's true.
It is.
On many levels that is a true statement.
They're both right.
On the international spectrum, they're both right.
Um...
they are
both pro-corporate interest by and large.
They're both
very likely
to
engage in militaristic operations.
They're likely to intervene in other countries. There's a whole lot of things
about them
that are the same.
But are they really exactly the same? No, of course not. There are policy
differences.
You know, notable ones would be
immigration,
climate change, which is like kind of super important.
Tons.
There are tons of actual
differences in policy.
But let's pretend
that those don't exist.
Let's pretend for the sake of argument
that there is no difference
in policy.
It's just the way they frame things.
Because that would be the only alternative
is that Democrats put a
kindler, gentler face
on the exact same policies as their Republican counterparts. I understand
that this isn't true, but we're accepting this falsity as fact right now.
Because I want to demonstrate something.
Okay, so it's exactly the same.
Take an issue. Let's take
those locations
all over the world
where the United States military has made children afraid of the sky.
Let's take that for example.
And let's say the only difference
is rhetoric.
The way they talk about it.
The Republican Party says, hey,
it's war.
Accidents happen.
No big deal.
Move on.
Better them than me.
And the Democratic Party says it's a regrettable consequence
of our intervention there.
Neither one of these statements
are going to save the kid that gets ripped apart six days from now.
But one of them
might save the kid that's going to get ripped apart six years from now.
Because that statement, it's regrettable.
It's something that shouldn't happen.
That
might change thought.
I've talked about it before on this channel.
To change society, you don't have to change the law.
You have to change thought.
If you change thought,
laws will change.
So that rhetoric alone
may be the starting point
to get to a better place.
And if you're one of the people that says this,
that Democrats and Republicans are exactly the same,
you're a radical of some sort.
You have to be, otherwise you wouldn't be saying that.
You would have your chosen political party, and you would be backing them.
You're a radical. You want deep systemic change. You want something better.
You're talking about changing the world.
It's not going to happen overnight.
You have to have long-term strategic thought.
And I know
right now there's somebody saying, empty words don't mean anything.
You know, speaking
in and of itself,
I guess not.
But sometimes a calling to speak
is a vocation
of agony.
But we must speak.
What law did Martin Luther King write?
Speech, rhetoric,
that is designed to influence thought,
is probably a lot more valuable than you think
when it comes to achieving long-term systemic change.
Because that's not accomplished by law.
That's accomplished by shifting the way people view the world.
So,
even if you just accept
that there's no meaningful difference
in policy between Democrats and Republicans,
you must acknowledge there's a difference in the rhetoric.
And that rhetoric alone may be valuable,
is valuable,
because those people
who would never say
that Democrats and Republicans are the same,
they hear that.
They hear that
children being afraid of the sky is regrettable.
They hear
that people who cross an imaginary line on a map
deserve human rights.
These are ideas that need to be reinforced through speech because they
aren't givens in this country.
So even if it's just rhetoric,
it is rhetoric that gets carried across
those mainstream networks.
It's rhetoric that influences millions.
It's rhetoric that
eventually
can save lives.
Even if you accept the idea
that they are exactly the same,
you know their speech is different.
If you are a radical, if you want deep systemic change,
if you want to open up those gates and free other people,
you have to think long-term.
Your goal is probably not going to be accomplished in your lifetime.
You have to think beyond that.
You may just be able to file one notch on that key that's going to open the gate
before your time is up.
But somebody else can come along
and file the next one.
If you want that deep change,
if you want to build a society
where everybody gets a fair shake, where there is liberty and justice for all
regardless of where you live or what you look like,
it's not happening overnight.
Saying that Democrats and Republicans are exactly the same
doesn't help your cause
because it comes across
as an excuse
to not get involved.
That's how it comes across.
It doesn't come across as,
I have a deep understanding of politics,
because if you did, you would understand that the rhetoric alone is worth something.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}