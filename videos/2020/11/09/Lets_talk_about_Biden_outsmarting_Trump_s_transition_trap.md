---
title: Let's talk about Biden outsmarting Trump's transition trap....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U9OklK9JBmA) |
| Published | 2020/11/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Emily Murphy, head of the Government Services Administration, is refusing to acknowledge Biden as the apparent election winner.
- Murphy's refusal means Biden is denied access to nearly $10 million for his transition team.
- The Trump administration appears to be making foreign policy moves to hinder a Biden administration.
- Trump's intent seems to be disrupting the transition of power.
- Biden had prepared for this scenario months ago, assembling an expert team in various fields.
- Biden's team already picked, including public health experts and cabinet-level department selections.
- Despite the disruption, Biden's decision-making and transition plans are intact.
- Biden had already raised $7 million for his transition team before this obstacle.
- Trump's attempts to disrupt the transition appear to have been anticipated and outmaneuvered by Biden.
- Biden's team is well-prepared, with office space ready in the Department of Commerce if needed.

### Quotes

- "Trump is trying to disrupt the transition of power, but he was outsmarted."
- "Yes, it's going to be a talking point, and there are probably going to be some really scary things said because it is incredibly wrong for President Trump's administration to do this."
- "It does, however, show President Trump's intent, and it does answer the question of whether or not he actually cares about the United States or just himself."

### Oneliner

Emily Murphy's refusal to acknowledge Biden as the election winner and Trump's attempts to disrupt the transition of power are countered by Biden's well-prepared expert team, showing Trump's true intent.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Support Biden's transition team by staying informed and spreading awareness (implied)
- Stand against attempts to disrupt the transition of power through advocacy and information sharing (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's transition trap and Biden's preparedness for the obstacles, offering insights into the current political situation and strategic moves.

### Tags

#Trump #Biden #TransitionOfPower #Politics #Preparedness


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump's transition trap.
And for once, I have some good news for everybody.
So we're going to talk about the news of today.
We're going to talk about what is likely to become news if things don't change, and then
we're going to talk about the reality of the situation because it's not going to be the
same.
Okay, so Emily Murphy, who is head of the Government Services Administration, is refusing
to say that Biden is the apparent winner of the election.
What this means is that she can not sign off on him getting a little less than $10 million
to flesh out his transition team, to build his administration.
And he won't have access that he really should have.
At the same time, the Trump administration is making moves, the moves that I noticed
were foreign policy moves, that seemed specifically designed to hinder a Biden administration.
From this, we can discern that it is Trump's intent to disrupt the transition of power.
That's today's news.
Now unless this changes very quickly and she signs off on the transition, releases the
funding, gives him access, we can expect for this to become incredibly politicized.
You will have Democrats out there talking about how Trump is disrupting the transition
of power, which means that Biden won't be able to pick his people, which means he won't
have access to information, which means this will impact decision making.
It may very well jeopardize continuity of government.
It won't.
It won't.
Everybody take a breath.
Biden saw this coming.
Biden saw this coming.
Three months ago, I released a video talking about a rumor.
The rumor was later confirmed, talking about Biden's foreign policy team for his campaign.
And I'll put the link down below.
Go watch it.
See me just in utter awe and shock of this team that he put together.
All-star team.
Subject matter experts for every country on the planet.
Working groups for every region, every country.
It was amazing.
I referred to it as a second state department.
I want to say it had almost 2,000 people in it.
Didn't really make any sense at the time because foreign policy wasn't a campaign issue.
It was cool and it gave us some insight into Biden's possible foreign policy.
But it was definitely way more than a campaign needed.
It was really surprising because it wasn't for the campaign.
He never used them for that.
He saw this coming.
That is his state department.
He's already picked his people, which means he's already getting their advice.
It's a kitchen cabinet.
If he did it for foreign policy when foreign policy wasn't a campaign issue, I have to
assume that he also has his public health people picked.
Every cabinet-level department, the selections have already been made.
Sure, it hasn't been finalized.
They haven't had their background checks and clearances updated and all of that, but that
can be done pretty quickly.
I wouldn't worry too much about Biden's decision-making.
He already has his team.
He had it three months ago.
Now as far as the money goes, yeah, there's a little less than $10 million that is to
help out his transition team.
Before this news broke, Biden had already raised $7 million for his transition team.
He knew this was going to happen.
He predicted President Trump acting like a child and acted accordingly, rightfully predicted
that he was going to act like a child.
So at the end of the day, yeah, Trump is trying to disrupt the transition of power, but he
was outsmarted.
He was outplayed.
Biden already has his team.
I don't foresee this actually becoming an issue.
Yes, it's going to be a talking point, and there are probably going to be some really
scary things said because it is incredibly wrong for President Trump's administration
to do this.
However Biden's team was prepared for it.
From everything that I can see, they saw this coming and they acted accordingly.
They have office space in the Department of Commerce, I want to say 20,000 square feet,
a little more than, to set up shop if need be.
This is not going to impact continuity of government.
It does, however, show President Trump's intent, and it does answer the question of whether
or not he actually cares about the United States or just himself.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}