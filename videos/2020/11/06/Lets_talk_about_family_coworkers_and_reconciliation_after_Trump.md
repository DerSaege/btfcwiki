---
title: Let's talk about family, coworkers, and reconciliation after Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d4IJYIf7WNs) |
| Published | 2020/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains his stance on rekindling relationships with Trump supporters due to moral reasons, not politics.
- Addresses questions about dealing with family members and colleagues with different political beliefs.
- Advocates for focusing on morality rather than politics in these relationships.
- Suggests having a moral obligation to help family members right themselves.
- Stresses the importance of standing firm on moral principles, even if it means cutting ties.
- Emphasizes the deep divide between right and wrong, not just political differences, in relationships.
- Points out the moral failings of the Trump administration and the impact on the country's moral fabric.
- Argues that reconciliation may not require forgiving and forgetting, especially in cases of deep moral disagreements.
- Expresses reluctance to waste time with those he no longer respects due to moral differences.
- Concludes by leaving viewers with food for thought on navigating relationships with differing moral beliefs.

### Quotes

- "It's not politics, it's morality."
- "It's about undermining the moral fabric of this country."
- "Sometimes it's not the message, it's the messenger."
- "Arguing over politics is fine. That happens all the time. It's the plot of movies."
- "I'm not sure I want to waste my time with people that I don't have respect for anymore."

### Oneliner

Beau explains the importance of standing firm on morality over politics in relationships with Trump supporters and advocates for reconciliation based on moral principles, not forgiveness. 

### Audience

Viewers seeking guidance on navigating relationships with conflicting moral beliefs.

### On-the-ground actions from transcript

- Have candid, morality-focused conversations with family members to address deep moral divides and encourage self-reflection (implied).
- Stand firm on moral principles and values in relationships, prioritizing right and wrong over politics (exemplified).

### Whats missing in summary

The full transcript provides a detailed exploration of Beau's perspective on dealing with relationships strained by differing moral beliefs, offering insights into reconciliation and the importance of standing firm on moral principles.

### Tags

#Relationships #Morality #TrumpSupporters #Reconciliation #FamilyRelations


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to answer the questions that arose from that last video.
If you haven't seen it, short version, I mean I think it's worth going and watching, but
short version is I'm not ready to rekindle relationships with people that I kind of let
go because of their support of the Trump administration, because of their support of his policies over
the last four years.
That prompted three questions.
First was, okay that's fine for friends, but what about family?
What are we going to do about that?
Are we going to keep these people cut out of our lives?
The next was, well how are we supposed to heal if we don't reach out to them and show
them the error of their ways?
And the third wasn't super common, but I think it's worth addressing.
I actually think that they were digs at me, but it's worth addressing either way.
What about those people you work with if they have different political beliefs like you,
or me?
Fair enough, so we're going to answer all three of them.
We'll start with that last one.
Yeah, I work with a whole bunch of people who have different political beliefs.
In fact, the person that was given as an example, that's not past tense.
Odds are while you're watching this, she's preparing this video to go on Instagram.
She actually does work for this channel.
And yes, she is to the right of me.
She's also ridiculously pro-freedom, ridiculously anti-authoritarian, and ridiculously moral.
If you go to her channel, you will find that even though she is to the right of me, every
single moral failing of the Trump administration was called out.
Because it's not politics.
That's the whole point of that video.
It isn't politics, it's morality.
It's right and wrong, good and evil.
Not left versus right, not Democrat versus Republican.
It's morality.
I do not think that I will ever have to doubt her morality.
That's not something I see happening.
There are many people who did not see the Trump administration's moral failings because
they were blinded by party.
They were blinded by politics.
And they thought that saying, oh, it's just a political stance, would excuse or at least
overshadow the moral implications of what they were okay with.
I can assure you that as many ways as Carrie and I disagree on various topics, I'm never
going to have to tell her, oh, you know, it's kind of wrong to use violence against children
because their parents crossed a line in the sand.
It's never going to be a conversation.
It's not politics, it's morality.
Now to family members, yeah, I get it.
It's going to be a whole lot harder to keep them cut out.
And I don't know that you should.
Again, I don't know that I'm even giving advice here.
I'm giving my actions, what I'm going to do.
Everybody's life is different.
This is pretty personal choices that are going to have to be made.
When it comes to family, I would suggest that you have a moral obligation to try to help
them right themselves.
I would frame the conversation not in left versus right, not in Republican versus Democrat,
but in right and wrong.
In many cases, this is going to be a generational gap.
And I would remind them where you got your sense of right and wrong from.
And I would point out that that's what it's about, not politics.
This isn't about politics.
It's about undermining the moral fabric of this country.
Right now, as we speak, the president of the United States is trying to stop people's votes
from being counted because he doesn't like what they say.
That's not politics.
That's levying an attack against the very founding principles of this country.
The idea that just saying, well, it's just politics, washes away the moral implications,
I don't think that holds water.
Now for that last one, how are people supposed to understand what they did wrong if you don't
tell them, if you don't let them back into your life, if you don't forgive and forget?
If I cut Bob out of my life because Bob was like, hey, it's a great thing that they're
taking these kids from their parents and throwing them in cages and saying that they don't need
toothbrushes or beds, yeah, I don't think this is somebody I need in my life.
I'll be honest.
I have no obligation to rekindle some relationship with him.
And I would suggest that me refusing to do so might actually help with reconciliation.
It might help with healing because six months from now, a year from now, two years from
now, he's going to say something like, you know, I lost friends over politics because
I supported the Trump administration.
And somebody standing nearby can say, you know, that really wasn't over politics.
That was over a view of right and wrong.
Sometimes it's not the message, it's the messenger.
It might be more motivating to have lost friends and then have it explained why than simply,
oh, yeah, you know, we didn't talk for a couple of years because I thought you were a horrible
human being.
But now that this influence is gone, well, everything's going to be OK.
Because you've returned to somebody who can still be swayed by that.
No, I have no reason to put this person back in my life.
Fighting over politics, fighting over who should rule a country or how it should be
ruled, that's incredibly common.
Arguing over politics is fine.
That happens all the time.
It's the plot of movies.
And yes, if it's politics, you can let it go.
But not if it's morality.
Not if it's a deep divide between what one person sees as right and wrong and the other
person sees as tolerable.
And that's where this was.
The fact that tens of millions of Americans after four years of the Trump regime do not
realize that he led the country down this morally bankrupt road is heartbreaking.
And it shows us how much work we have to do.
And given the amount of work that needs to be done, I'm not sure I want to waste my time
with people that I don't have respect for anymore.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}