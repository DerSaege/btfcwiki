---
title: Let's talk about Biden winning and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cTBVgqN-ZEg) |
| Published | 2020/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden is projected to win, but legal challenges may arise in the coming days.
- Regardless of the outcome, there is still a lot of work ahead.
- Biden is not a savior; he is just one person.
- The focus should be on undoing the damage caused by the previous administration.
- It is vital to remove those who enabled Trump from public life and prevent authoritarianism from resurfacing.
- The country was on a dark path that may have shifted slightly, but the work is far from over.
- Progress requires sustained effort beyond January and into the future.
- The struggle for a better future is long and challenging.
- Beau humorously reminds that "brunch is still canceled."
- The message is reiterated multiple times with slight variations in English and Spanish.
- Despite the cancellation of brunch, Beau suggests that it can still be taken to go.
- The transcript ends with a wish for a good day.

### Quotes

- "Biden's not a savior. He's one person."
- "Brunch is still canceled."
- "If we want the promises kept, if we want the future that the people who watch this channel really want, it's not over."
- "We were on an incredibly dark path."
- "We've got a whole lot of work to do and we're going to have to keep at it."

### Oneliner

Beau reminds us that regardless of the election outcome, there's still much work to be done, and "brunch is still canceled."

### Audience

Progressive activists

### On-the-ground actions from transcript

- Keep pushing for positive change and accountability in government (implied)
- Stay engaged in activism and advocacy efforts (implied)
- Work towards building power structures to prevent authoritarianism (implied)
- Take the reminder to heart: "brunch is still canceled" (implied)

### Whats missing in summary

The full transcript provides a humorous yet thought-provoking take on the need for continued activism and vigilance post-election.


## Transcript
Well howdy there, Internet people, it's Beau again.
So yeah, right now, as of right now, it looks like Biden won.
Now of course, there's probably going to be some legal shenanigans or whatever coming
over the next couple of days.
More than likely going to happen.
That being said, assuming everything goes as planned and he takes office in January,
we have to remember that nothing has changed.
Biden's not a savior.
He's one person.
Even if you have complete faith in him, he's one person.
We still have a lot of work to do.
Aside from undoing all of the damage that the Trump administration has caused, we have
to make sure that not only are those people who assisted and enabled his reign pushed
from public life, we have to make sure that we build the power structures to prohibit
his particular brand of authoritarianism from ever rearing its ugly head in this country
again.
We were on an incredibly dark path.
But maybe we've shifted a little, but it's just a little.
If we want the promises kept, if we want the future that the people who watch this channel
really want, it's not over.
We've got a whole lot of work to do and we're going to have to keep at it.
Doesn't end in January.
Doesn't end in January of the next year.
This is a long, hard struggle.
So at the end of the day, I think we all need to remember brunch is still canceled.
Just so you know, brunch is still canceled.
Just so you know, brunch is still canceled.
Just so you know, brunch is still canceled.
Solo para que sepas que el desayuno tard??o a??n est?? cancelado.
Just so you know, brunch is still canceled.
Brunch is still canceled.
Just so you know, still canceled.
Cunch is still branceled.
Brunch is still canceled.
Brunch is still canceled.
Brunch is still canceled.
Still canceled.
Still canceled.
Still canceled.
You can always get it to go.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}