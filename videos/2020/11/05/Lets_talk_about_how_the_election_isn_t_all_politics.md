---
title: Let's talk about how the election isn't all politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mz_zAgrA6O0) |
| Published | 2020/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a prevailing tone in the nation post-election.
- Describes his close circle of friends who support each other no matter what.
- Mentions how some friends supported Trump initially but withdrew support over border policies.
- Explains that his friends, former military contractors, have their own set of morals.
- Recounts how debates during elections ceased when real actions unfolded.
- Differentiates between close friends with integrity and others who didn't withdraw support for Trump.
- Shares a personal experience of cutting off contact with friends who supported immoral actions.
- Emphasizes that the issue was about morality, not politics.
- Asserts his stance on not reconciling with those who crossed moral lines.
- Stresses the importance of addressing moral failures before healing and reconciliation can occur.
- Compares the need for facing moral failures in America to Germany's handling of its past.
- Calls for public acknowledgment of past wrongdoings and faults before moving forward.
- Expresses the need for transparency and examination of moral failings before reconciliation.
- States that forgiveness is a personal matter between individuals and their beliefs.
- Concludes by advocating for laying bare and discussing the moral failings of the nation before seeking reconciliation.

### Quotes

- "It's not politics. That wasn't politics. That was morality."
- "Morality doesn't run on an election cycle. You either have it or you don't."
- "We can't move forward until it's all exposed."
- "Until the moral failings of large portions of this country are laid bare, discussed, examined, and we figure out why they happened, I'm not ready for reconciliation."
- "Because I have no desire to interact with them."

### Oneliner

Beau stresses the importance of addressing moral failings before seeking reconciliation and healing in the aftermath of political divisions.

### Audience

People reflecting on moral integrity.

### On-the-ground actions from transcript
- Have open, honest dialogues about past moral failings and faults (implied).
- Encourage public acknowledgment and examination of moral failings in the nation (implied).

### Whats missing in summary

The emotional depth and personal conviction Beau conveys through his experience and reflections on morality and reconciliation.

### Tags

#MoralFailings #Reconciliation #Healing #AddressingHistory #Friendship


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a tone that is kind of sweeping the nation.
The election didn't even resolve yet at the time of filming and it wasn't resolved at
the time of this message.
But something happened to me and I guess because it happened to me I have to assume that it's
going to happen to other people and they're going to be in this situation.
I'm not really giving advice but I'm going to tell you how I handled it and why.
I've told you before I have a tight group of friends, very close circle of friends.
We've known each other a very long time.
I think the newest addition to this little circle was like ten years ago.
Most of us have known each other twenty years and we are tight.
We are a group of people who will back each other up no matter what.
In any situation we're there for each other.
In 2016 some of them supported Trump.
As soon as he started enacting the policies down on the border nobody did.
That was a line too far.
That was something they wouldn't cross.
Most of my friends are former military contractors.
It's not a demographic known for their ethical compass but that was too much for them.
At that moment I'm like nope, can't support this.
Because although they may not seem like the most moral group from the outside, they do
have morals.
They're just different than everybody else's.
Most of them had seen asylum seekers or refugees with their own eyes.
When you see people in that situation just trying to get somewhere safe it becomes very
hard to condemn them ever again.
So although during the election back then, yeah, we had arguments and debates and they
got heated, as soon as it became real they chose the right side and those conversations
never happened again.
So a lot of the problems that other people have had I haven't had to deal with.
Not among my close friends because my close friends had the integrity to see wrong for
wrong regardless of party.
However there's that other group of friends that you have.
You know, you have your close circle and then you have those others that you know, you hang
out with, you like, you know, you're friends with but they're not like those other friends.
They can't be counted on in the same way.
They're not that tight circle.
Some of them did not withdraw their support from Trump when those policies started getting
enacted.
And I never spoke to them again.
And I don't mean this in some form of hyperbole.
I never spoke to them again.
And I didn't miss anything.
I don't feel like I lost anything in that time.
Tonight one of them messaged me and said, hey, you know, one way or the other, no matter
how the election turns out, you know, the intense political cycle or however it was
phrased is over.
We can move past this now.
And I remember looking at my laptop and saying, can we, aloud, and closed my laptop.
I didn't respond.
And I have no intention of responding.
It's not politics.
That wasn't politics.
That was morality.
This was somebody who had no problem supporting the idea of ripping children from parents
because they crossed some imaginary line on a map.
But apparently it's too much to distance yourself from somebody who crosses a moral line.
I don't believe that's true.
I don't have any intention of trying to rekindle relationships that were terminated over the
last four years.
None.
I don't intend on doing that because it's not politics.
Morality doesn't run on an election cycle.
You either have it or you don't.
I don't feel I need people like that in my life.
And I understand this runs counter to the tone that a lot of people are trying to set
right now.
They want reconciliation.
They want to begin the healing.
We should forgive.
You can't heal until you address the wound.
Until then, it's not going to heal.
Not properly.
And it's something that we have a problem with in this country from the very beginning.
From the very beginning.
We don't like to address our moral failures.
You know, we justify it and say, oh, well, you know, that was just the times.
Nobody knew any better.
It was a necessary evil.
No it wasn't.
Other countries had already done away with slavery.
We had founders who were abolitionists.
It wasn't necessary and people did know better.
But we keep up that lie because we never addressed it.
And there are incidents like that throughout all of American history where we just refuse
to address it until it got so distant, so far into the past, we could pretend that it
was okay.
It was just a different time.
No.
It doesn't work.
Eventually the scars of those wounds, the scars of history will burst open.
The reason Germany was able to move on was because they addressed it.
We did this.
They looked at it.
They saw it.
Then they were able to move on.
We need to do the same thing.
And I'm worried because of the tone that people are already striking.
We don't even have the election determined yet.
And people are already talking about reaching across the aisle and healing.
It's easy for us out here to heal.
There are still kids who don't know where their parents are right now.
This didn't happen in the past.
It's still occurring at this moment.
And we're talking about healing.
No.
We have to address the wound first.
It needs to be very public.
We have to talk about what we did.
Everything that happened needs to be laid bare.
And we have to admit our faults.
Until that happens, there's no reconciliation.
There's no healing.
It's not something that's going to move the country forward.
It's just a cover-up.
It's just omitting history.
And it's one of those things that, you know, the people who are going to want to do that
the most and just move on, they're the same people who have been fighting over statues.
From 100 years ago.
I'm not ready for that.
Until the moral failings of large portions of this country are laid bare, discussed,
examined, and we figure out why they happened, I'm not ready for reconciliation.
I'm not ready to heal because the knife's still in the wound.
We can't move forward until it's all exposed.
Until then, yeah, there may be forgiveness for these people.
But it's between them and their God.
Has nothing to do with me.
Because I have no desire to interact with them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}