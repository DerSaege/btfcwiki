---
title: Let's talk about a Trump Biden election update....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RQhvhKBwwx0) |
| Published | 2020/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The world is anxiously awaiting the ballot count results, but patience is needed, as it is not a big deal to wait a few more days.
- Despite Biden's potential win, the Democrats did not secure firm control of Congress, meaning Biden won't have a legislative mandate for significant policy changes.
- A substantial portion of the American population supports Trump's policies and does not see them as morally wrong, leading to limited policy changes under Biden.
- It is up to individuals to organize, continue educating, and push for change as Biden may only have the power to undo certain policies through executive orders.
- Trump's successful rhetoric will likely persist even if he loses, requiring individuals to make certain political positions untenable for the Republican Party.
- The current situation underscores the necessity for ongoing organization, education, and activism, as the responsibility lies with the people to drive change.
- Biden's potential victory, even if narrow, could send a message to those disengaged from politics and help combat Trump's rhetoric in the long run.
- The need for sustained engagement post-election remains critical to shape public opinion and demand a country that young Americans can be proud of.
- The intensity and activism seen in the past four years will need to continue to counter the influence of Trump's administration and shape a better future.
- The onus is on individuals to stay active, organize, and influence those around them to create a country worth being proud of.

### Quotes

- "It's up to us, me and you. We have to organize, we have to keep educating, nothing changed."
- "Biden may have been able to remove President Trump, okay? But he doesn't have a mandate."
- "It was always up to us. It's gonna require a lot of work, a lot of organization."
- "We have to continue to turn people away from this. It's going to require a lot of work."
- "You're still gonna have to organize to shape those around you, and to show them the failings of the Trump administration."

### Oneliner

Beau stresses the need for ongoing organization and education post-election to counter Trump's rhetoric and shape a better future.

### Audience

Activists, Organizers, Educators

### On-the-ground actions from transcript

- Organize grassroots efforts to counter harmful rhetoric and policies (suggested)
- Continue educating and engaging with individuals to drive positive change (implied)
- Shape public opinion through activism and organization within communities (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the importance of sustained activism, education, and organization post-election to combat harmful rhetoric and shape a better future.


## Transcript
Well howdy there internet people, it's Beau again.
So, I had a lot of people ask for like a situation report.
Because the whole world is holding its breath right now
as ballots get counted.
And that's fine, that's fine.
We've waited four years, we can wait four days.
It's not a big deal.
But the situation report is probably not what people wanna hear.
I do believe that Biden is gonna win.
However, the Democrats did not get firm control of Congress,
no matter how the rest of this plays out.
So Biden doesn't have a mandate.
Biden may have been able to remove President Trump, okay?
But he doesn't have a mandate.
There's not gonna be a lot of policy changes that are legislative.
He'll be able to undo some stuff via executive order, but that's about it.
This occurred because a substantial portion of the American population
sees nothing wrong with Trump's policies.
They don't find his policies morally repugnant.
They're okay with it.
That means it's up to us, me and you.
We have to organize, we have to educate, we have to continue.
Nothing changed.
A little bit of breathing room, maybe, if we're lucky.
But we're not gonna get any relief.
It goes on.
It's gonna require you to be the anvil that breaks the hammer.
It's gonna require you to keep your moral compass,
even though a large percentage of the American population has lost theirs.
It's imperative that once the announcement is made,
you understand from the beginning, he's not gonna be able to do much.
The rhetoric that Trump used was successful.
Even if he loses, it was successful, which means it will stay in play.
The Republican Party will continue to go down that road,
unless we, as individuals, make it a politically untenable position.
We have to continue to turn people away from this.
It's going to require a lot of work.
It's gonna require a lot of organization.
It's not over.
It was never gonna be over.
The reality is it was always up to us.
This playing out the way it did, where if we're lucky,
Biden will win by the skin of his teeth,
that may be the best thing in the long run.
Because hopefully, we'll be able to win by the skin of our teeth.
Hopefully, it will send a message to those who
are less inclined to stay politically engaged,
that it's not over, and that Trump's rhetoric worked.
Trump was not great at using that rhetoric.
What's going to happen when that rhetoric is deployed by somebody who is competent,
who is capable, who is effective?
There is a very large portion of the American population who will love it,
and that's what we have to change.
We have to continue to educate, even while Biden is in office,
we can't stop.
So the good news, I still think Biden's gonna win.
The bad news doesn't mean anything, not really.
The same level of intensity that you've had for the last four years
is gonna have to carry on.
It's gonna have to come through, and you're gonna have to stay active.
You're still gonna have to organize to shape those around you,
and to show them the failings of the Trump administration.
When people say that they want young Americans to be proud of this country,
you have to be ready to demand that we give them something to be proud of.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}