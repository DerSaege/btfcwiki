---
title: Let's talk about John Kerry's position....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ea51I0AxUWM) |
| Published | 2020/11/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- John Kerry is becoming the U.S. representative on climate change, aiming to restore commitments from Trump's era.
- Kerry, as an American political heavyweight, is well-connected and signifies continuity post-Trump.
- He is expected to quickly accumulate wins internationally to counter the damage caused by Trump.
- However, a potential pitfall is Biden feeling less pressure to take domestic action if Kerry secures early international wins.
- Merely returning to pre-Trump levels of commitment is not enough; the U.S. must go beyond due to the lost time on climate change.
- Biden's choice of Kerry may indicate a strong commitment, but there's a concern that easy wins internationally may reduce the drive for necessary domestic action.
- The political capital gained swiftly from international wins might hinder the willingness to tackle challenging domestic decisions.
- Vigilance is needed to ensure Biden follows through on climate action despite the initial wins Kerry might secure.
- The fight for climate change action will be ongoing and will require significant political capital expenditure.
- Upholding commitments on climate change will clash with certain business interests and campaign donations, making it a tough battle.

### Quotes

- "The goal shouldn't be simply to return the U.S. to pre-Trump levels of commitment."
- "This is one of those issues we are running out of time to deal with."
- "Don't take the appointment of a political heavyweight to this position as a guaranteed win."
- "We're probably going to have to push Biden on for the next four years."
- "This is one that we're probably going to have to push Biden on for the next four years because it is going to take him."

### Oneliner

John Kerry's appointment signals hope for climate action but raises concerns about potential complacency on domestic efforts post-Trump, urging continued pressure on Biden for sustained commitment.

### Audience

Climate activists, concerned citizens

### On-the-ground actions from transcript

- Stay informed on climate policies and hold elected officials accountable for climate action (implied)
- Advocate for strong domestic climate policies by engaging with local representatives and communities (implied)

### Whats missing in summary

The full transcript delves into the delicate balance between international wins in climate action and the necessity for continued pressure on political leaders to prioritize domestic climate policies and go beyond pre-Trump levels of commitment.

### Tags

#ClimateChange #JohnKerry #BidenAdministration #PoliticalPressure #Sustainability


## Transcript
Well howdy there internet people it's Beau again.
So today we're going to talk about John Kerry becoming the climate change guy.
He's going to be out there representing the U.S. in a bid to restore our commitments
to climate change, those that Trump has yanked us out of.
Kerry's a good choice for this. He knows all of these people.
Everybody that's involved with this, he knows them. He is an American political heavyweight.
He also sends the message that there is some kind of continuity in the United States despite four
years of Trump. This is all good on the international scene. He's also not going to be
really facing any opposition. You know, it's not like other countries are going to be like,
no don't reduce your carbon emissions. That's not going to happen.
With all of these things together, Kerry should be able to rack up wins incredibly quickly for Biden.
He should be able to score a lot of wins very, very quickly and undo a lot of the damage that
Trump caused. That's good, but there's also a potential pitfall. If Biden racks up a bunch
of wins on the environment really early on, very quickly, thanks to Kerry, and they're all on the
international front, he may not feel as much pressure to do things domestically,
to take action here in the United States that needs to be taken. The goal shouldn't be simply
to return the U.S. to pre-Trump levels of commitment. We've got to go way beyond that
because we lost four years. This is one of those issues we are running out of time to deal with,
and we've got to be careful in accepting normal political wins on this one. We have to go beyond
where we were at four years ago. We have to be really committed to this. Hopefully,
Biden tapping somebody who is an American political heavyweight on the international
scene is a sign of commitment to doing that. That's how a lot of the media is reading it.
I don't know that they're wrong, but I'm just worried that the amount of political capital
they can gain very, very quickly with little effort is going to make them less likely to
expend political capital to do what we really have to get done. It's something that we really
have to watch for. The appointment of Kerry to do this is probably a good thing, but we have to
stay on Biden about this. What he has said, yeah, he's moving in the right direction, but this is
something we're going to have to push on. This isn't something that is going to be second nature.
It's not something he's necessarily going to want to do because a lot of what has to be done
is going to upset business interests, which will upset campaign donations, and
it's going to be a fight. Don't take the appointment of a political heavyweight
to this position as a guaranteed win. We're going to get those wins up front very quickly,
but this is a long fight on this one. This is one that we're probably going to have to push Biden on
for the next four years because it is going to take him, it's going to take expending political
capital to get a lot of the stuff done that needs to be done, and this is one of those things we
don't have a choice on. We have to do it. Anyway, it's just a thought. You know,
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}