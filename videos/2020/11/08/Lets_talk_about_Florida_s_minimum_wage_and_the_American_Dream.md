---
title: Let's talk about Florida's minimum wage and the American Dream....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wZlDRhRv940) |
| Published | 2020/11/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares a personal story within his circle of friends, describing an inspirational journey from a laborer on a construction crew to a successful general contractor.
- The individual started off making $7 an hour, worked hard, made good decisions, and got lucky to achieve success.
- Despite his success, the general contractor still spends a day each week working with his crews to maintain quality and show solidarity.
- Another contractor in the same industry noticed that the general contractor's crews worked faster and asked for the secret.
- The key to the general contractor's success was sharing the early completion bonus with his employees.
- Beau mentions a recent minimum wage increase in Florida to $15 an hour, with the general contractor paying his employees a minimum of $19.
- Beau observes a common trend where individuals who rise from humble beginnings tend to be the best leaders rather than bosses.
- Successful capitalists often exhibit socialist behaviors by ensuring their employees have a stake in the company's success.
- Beau suggests that rewarding labor and giving employees a stake in the company's success leads to better performance and profitability.
- He questions the idea of striving to pay employees as little as possible and proposes that rewarding labor is key to generating wealth.

### Quotes

- "Those people who make it, you know, who came from kind of the bottom and worked their way up, they always tend to be the best boss."
- "The best capitalists that I have ever met all behaved like socialists when it came to their employees."
- "Maybe we shouldn't be striving to pay employees as little as possible."

### Oneliner

Beau shares an American Dream story of a laborer turned successful general contractor who prioritizes rewarding labor and employee well-being over profit margins.

### Audience

Business Owners, Employers

### On-the-ground actions from transcript

- Share early completion bonuses with employees (implied)
- Pay above minimum wage to show appreciation for labor (implied)

### Whats missing in summary

The full transcript includes insights on the importance of treating employees well and rewarding their efforts to drive success and profitability in a business.

### Tags

#Leadership #LaborRewards #EmployeeWellBeing #SuccessStories #Capitalism


## Transcript
Well howdy there internet people, it's Bo again.
So today we've got a story.
Something that happened in my little circle of friends that I just find hilarious.
And it's a cool story, inspirational story really.
One of those American Dream stories, rags to riches type things.
Now this guy, and he started off years ago as a laborer on a construction crew.
He's making like seven bucks an hour.
Over the years, through a lot of hard work, some good decision making, and a whole lot
of luck, and he'll be the first one to tell you there was a whole lot of luck involved,
he made it big.
He's a general contractor now, and he makes a boat load of money.
He drives like an $80,000 truck and has a house down on 30A.
It's like redneck 90210.
It's down on the beach.
Most houses down there run a million bucks or more.
Even at this stage, where he is definitely the boss, you know, one day each week he goes
and spends with each one of his crews swinging a hammer, like actually doing manual work.
And he says it's quality assurance and to let the guys know that he's still going to
work with them, they don't work for him.
It's a cool idea, you know, and it's paid off for him.
So much so that one of the other general contractors who works for the same big developer, basically
what they do is they build subdivisions.
So they're building the same house like over and over again, the same floor plan.
And the other general contractor asked him, he's like, you know, your guys finish a house
almost twice as fast as mine do.
You know, that means you're getting more profit out of them for the labor and you're getting
the early completion bonus.
You know, what gives?
How do you do this?
My friend was like, share the early completion bonus with them.
And if you don't know, we just voted in here in Florida that everybody's going to be bumped
up to 15 bucks an hour.
Over the next few years, that's going to be the new minimum wage.
It's going to raise steadily.
The guy asks him if he's still going to do that now that he has to pay him $15 an hour.
My friend said nobody in the company makes less than 19.
Man, that's a cool story when you really think about it.
And it's something that has proven true time and time again in my experience.
Those people who make it, you know, who came from kind of the bottom and worked their way
up, they always tend to be the best boss.
More importantly, they tend to not be a boss.
They tend to be a leader.
They tend to be somebody that is working with their employees.
Their employees aren't working for them.
And it makes a lot of difference.
It also seems to be the case that when employees have a stake in the company and they kind
of control the production and benefit from it, that they perform better.
It has never let me down, this general idea that the best capitalists that I have ever
met all behaved like socialists when it came to their employees.
They made sure that they had a stake in it.
They made sure that they were rewarded for their efforts.
It might be something that corporate America takes note of because the most profitable
of all of those general contractors that work for that developer is the one that pays the
most.
Maybe we shouldn't be striving to pay employees as little as possible.
Maybe the idea of a minimum wage shouldn't be necessary, not because we can pay people
whatever meager amount we can get away with, but because we know that rewarding labor is
how you generate money.
It's how you generate capital.
Anyway it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}