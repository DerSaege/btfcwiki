---
title: Let's talk about blending systems of government....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rC2OrWtuS5o) |
| Published | 2020/11/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the idea of comparable systems to social democracy, discussing a blend between normal capitalism, Western governments, and fascism.
- Outlining 14 characteristics of fascism according to Lawrence Britt, focusing on practical examples for Americans.
- Describing a light version of powerful nationalism, with flags symbolizing the nation rather than overt displays like marches.
- Touching on the disdain for human rights in a more subtle, de facto manner rather than legislated.
- Differentiating between internal and external enemies as a unifying cause in fascist regimes.
- Imagining a light version of military supremacy with revered status and a significant budget share.
- Addressing rampant sexism in a de facto manner in a male-dominated society.
- Comparing controlled mass media in terms of media collusion with government talking points.
- Exploring the obsession with national security and its impact on government compliance.
- Touching on the intertwining of religion and government with subtle rituals to show allegiance.
- Mentioning protection of corporate power, suppression of labor power, and disdain for intellectuals in the arts.
- Noting the obsession with crime and punishment leading to a large prison population.
- Describing rampant cronyism and corruption through examples like no-bid contracts and nepotism.
- Hinting at fraudulent elections and the facade of legitimacy in such regimes, with parallels to the United States.

### Quotes

- "If you want to know what fascism light looks like, look out the window."
- "The United States is the blend."
- "The major parties keep everything in house and really only give you a couple options."
- "Y'all have a good day."
- "The biggest dangers to the United States is the creep towards authoritarian rule."

### Oneliner

Beau outlines characteristics of fascism in a light version, drawing parallels to the United States' blend between capitalism, Western governments, and subtle authoritarianism.

### Audience

American citizens

### On-the-ground actions from transcript

- Analyze the existing political systems and their implications (suggested)
- Stay vigilant about authoritarian creep towards right-wing ideologies (implied)

### Whats missing in summary

The full transcript provides a detailed comparison between social democracy and fascism light, prompting viewers to critically analyze their societal and political structures.

### Tags

#Fascism #SocialDemocracy #PoliticalSystems #UnitedStates #Authoritarianism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about a question I got,
because it kind of threw me through a loop.
Pretty thought provoking, actually, when you really get
down to it.
It was about systems of government.
And it was trying to determine if there was a comparable
system to social democracy on the other side and basically the idea of the
question was I understand that social democracy is kind of a blend of
socialism and capitalism. It's socialism light. Is there something on the other
side? Is there something in the right wing that is a blend between normal
capitalism, normal Western governments as we know it, and fascism leads to an
interesting place. So we've got our list, the one we've been using on this
channel, which is put out by Lawrence Britt, I think is his name, 14
characteristics of fascism. To be clear, I use this one because it is, it tends to
speak to Americans more. It's more practical. There are other lists that
break down the characteristics. There's one by Umberto Eco, but this is more
practical rather than philosophical. So it's the one that I tend to use. If you
really want to get into the subject, you might want to look at the others too.
Okay, so the first one is powerful and continuing nationalism. Now, if you're
looking at a light version you would just kind of lighten it up so maybe you
don't have the marches and parades all the time, but you have like flags
everywhere. So much so that it kind of loses meaning, becomes more like a bumper
sticker, a sports team logo, than a display of the nation. It's something that
that everybody feels is their symbol.
Distain for the recognition of human rights.
So you wanna lighten it up.
Maybe it's not overt, you know?
Maybe it's not legislated that people don't have human rights.
However, if somebody that's accused of a crime gets roughed up a little bit,
nobody's gonna cry over it, you know?
In this case, it's going to be more de facto, it's going to be stuff that just happens rather than it being
institutionalized.
Identification of enemies and scapegoats as a unifying cause.
Now, when you're talking about the overt version of this, it's typically an internal enemy that gets scapegoated.
get scapegoated. If it's a light version, it would probably be external. You know,
well, the ones of that group that are inside the country, they're different,
they're okay. But we're gonna all rally around the idea that those on the other
side of the line on the map, they're bad.
Supremacy of the military. So in a light version, you could imagine that rather
than it just basically having control of the entire government it it maybe has a
place of honor that is you know really revered and probably gets the biggest
share of the budget. Rampant sexism. Again this would be something that's de
facto. It's a male-dominated society but there's not legislation that backs up a
whole lot of it, it's just the way it is. Because tradition is also a characteristic
that goes along with this system, but that gets into the philosophical list. Controlled mass media.
So rather than it being overt and the government just basically telling the news organizations
what to say, maybe it's just more collusion, maybe it's more of the media parroting the
government talking points so they can get access and so everybody's on the same page.
Again, it's just de facto, it's just the way it is in practice rather than something
that's institutionalized.
Obsession with national security.
In regimes like this, I don't think you would find a light version.
I think you would still have a maze of agencies that were devoted to national security, and
any time the government needed something done, that's the card they would pull, because
they would have that reverence for the military already established.
So they'd be able to gain compliance from the population by kind of pulling that card.
need to do this so we don't have to send our troops over there. We just need you
to comply and do what we ask. It's going to keep you safe. Religion and government
are intertwined. Again, they're going to want to keep up the facade of some form
of liberal democracy. So it probably wouldn't be legislation, it
wouldn't be institutionalized, but there would be little rituals to show that you
were on the right team. Maybe you swear in on a holy text when you take office.
You know, there'd be little things that you would do to demonstrate that you were
one of the good guys. You were part of the club. Corporate power is protected,
and that's one of the ones on this list that I really object to, in the overt
form. It's not protected, it's blended. The government has a lot of control,
direct control of corporate power in that system. If you're just talking about
corporate power being protected, it's what you have in the United States. I mean
that's what it is. The government looks out for corporate interest for the sake
of the economy and that's how it gets framed, but it's also for their own
personal benefit. Labor power is suppressed. It probably wouldn't be as
brutal as you find in the overt regimes. It would be more legislation that just undermines
collective bargaining, makes it harder to unionize, and just undercuts the rights of
the worker. Distain for intellectuals in the arts. So it probably wouldn't be open hostility.
It would just be something that they didn't encourage. Maybe they don't fund art in
school. They don't teach the appreciation of it and they don't encourage the youth to be
intellectual. So it just goes by the wayside on itself, all by itself. There's no reason for the
government to push against it too hard. They're just going to let it fade out. Obsession with
crime and punishment. So you'd probably have a huge prison population in a regime operating
under a blend like this.
Rampant cronyism and corruption.
So what you'd have is like no-bid contracts.
You'd have government officials giving jobs and construction contracts to their cousins
and stuff like that.
That's what you'd find.
Again, it wouldn't be overt.
The last one is fraudulent elections.
You probably wouldn't see any of this, not much, because they'd want to keep up the
facade that it isn't one of those regimes.
So it would just be controlled in a different manner.
Perhaps the major parties would keep everything in house and really only give you a couple
options but they'd want to keep that one at least the appearance of legitimate
elections. Sounds really familiar, doesn't it? Yeah, if you want to know what fascism
light looks like, look out the window. The United States is the blend. That's one
One of the reasons it's so important that we watch for the creep towards the overt real
thing because we're already really close to it.
We've talked about it before on this channel.
Even our left-wing party in the United States is center-right because the country is that
far right to begin with.
If you're talking about a blend between fascism and the idea of Western liberal democracy,
it's the U.S. That's where we're at already.
We wouldn't need to go anywhere.
It already exists.
Again, I think it's an interesting little thought exercise and something I'd never
thought of before.
has ever asked. That's probably one of the biggest dangers to the United States as a
whole is the creep that direction further and further right to an authoritarian rule,
especially when it's done slowly and it's this soft form of it that we don't even realize
It's there so much so that we don't even think about it because it's just the way it is.
It's just tradition.
It's de facto.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}