---
title: Let's talk about Biden and 440,000 people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wUY7RfSonBI) |
| Published | 2020/11/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden is actively moving forward with his transition, already having advisors and making decisions.
- He is focusing on announcing plans rather than campaign promises now that the campaign is over.
- One significant plan he has shared is to revamp the US refugee program, which Beau cares deeply about.
- The current refugee program is described as broken and in need of deep reform due to its difficulty and low cap.
- Biden's proposed increase in the refugee cap to 125,000 is seen as a reform Beau can support, even if it's not the complete change he desires.
- Beau acknowledges that real change needs to come from individuals, while reform is the role of the government.
- He expresses willingness to give Biden time to see the impact of his policies before critiquing them, noting the potential positive impact of the refugee program reform.
- Beau stresses the importance of not letting perfect be the enemy of good, supporting policies that may not be ideal but still have a positive impact.

### Quotes

- "If you want change, you have to create it."
- "Reform comes from the government."
- "You can't let the good become the enemy of the perfect."
- "I will give him some breathing room to see what he's going to do."
- "At the end of the day, this move can save almost half a million lives."

### Oneliner

Biden's refugee program reform, though not perfect, could save lives and sets a positive starting point for more changes, as emphasized by Beau.

### Audience

Advocates for refugee rights

### On-the-ground actions from transcript

- Advocate for comprehensive reform of the US refugee program (implied)
- Support organizations working to aid refugees and advocate for their rights (implied)

### Whats missing in summary

Beau's thoughtful analysis and perspective on Biden's refugee program reform and the importance of incremental progress can best be fully understood by watching the full video. 

### Tags

#Biden #RefugeeProgram #Reform #IncrementalChange #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Biden, what he plans to do,
because he is going on with the transition as best he can,
given the current circumstances.
He already has some of his advisors.
He has that kitchen cabinet already meeting, already giving him advice,
and he's already making decisions.
And he's pushing information back out to the press
related to some of his decisions to remind them of what he's going to do.
Now that the campaign is over, he's not really making campaign promises,
he's announcing plans.
One of the things that his team pushed back out to the press
is how he plans to revamp the refugee program, the US refugee program.
If you don't know, this is something I care very deeply about.
Something I watch very closely.
The US program is broke.
It is broken.
It needs real change, like deep change.
It needs to be completely redone.
It is way too hard to get in.
It is way too hard to qualify for refugee status.
And there's a cap.
Trump pushed that cap down to $15,000 per year.
That is ridiculously low.
It has been much, much, much higher than that in the past, without issue.
And before we go any further, just to cut off some comments
that I'm sure might show up, gaining the refugee program
to gain admittance to the United States to be an undocumented worker
isn't really a thing.
That's something that President Trump made up.
To go through the refugee program, you have to have contact with law enforcement.
Generally speaking, if you're going to try to exist in the shadows
in the United States, you don't want to provide fingerprints
and biometrics and all of that stuff, which is part of the refugee program.
We are moving into a post-Trump facts matter era.
That's not a thing.
He made that up.
The cap is currently at $15,000.
Biden could have scored liberal brownie points by doubling that number.
That would have been the story.
Biden doubles Trump's cap on refugees, tripling it even, taking it to $45,000.
Rank and file Democrats would have clapped.
They would have been okay with it.
More than likely, they would have been like, yeah, look, he's doing something about it.
Wouldn't have been okay with me because I want real change.
That's not even a reform that's really that good.
The number that Biden dropped was 125,000.
It's pretty substantial.
It's not the change I want.
I don't think there should be a cap, but that's a reform I can get behind.
Odds are we won't reach that number.
More than likely, we will not reach that number, but nobody's going
to get turned away because the cap's full.
And the thing about the refugee program is that if somebody gets turned away
because the cap's been met when they're sent home after walking up here,
they're going to die.
That's the purpose of the refugee program.
It's why it's so hard to qualify because you basically have to prove that if they
send you back, you're not going to be here anymore like anywhere on the planet.
So even if Biden is a one-term president, four years, every person that comes in,
in excess of the 15,000 that Trump had, is a life saved.
That could be 440,000 people over the course of four years.
It's not the change I want, but I'd be remiss to not accept a reform that's going
to save almost half a million lives.
So yeah, I can get behind it.
I can defend it even.
Not the change I want, but a reform I'm willing to accept.
And I'm certain that there are people out there right now saying that our foreign
policy is behind a lot of these refugees.
And that's true.
It's a true statement.
Fact.
We need real change there.
The reason I knew this was coming with the refugee program,
mentioned it three months ago because of the people he brought on board.
I knew they were going to do something of this scope.
Now to be honest, I didn't think it'd be this big.
Just saying.
But I knew they were going to do something big if he listened to them.
He's apparently listening to them.
If he listens to the people that he brought on that advocate arms control,
the way he listened to the people who advocated this,
you're going to have a different foreign policy.
Because we're going to be selling less weapons.
We're going to be distributing less weapons.
That means there's less people walking away from stuff falling out of the sky
that says Made in the USA on it.
Is it the change you want?
Probably not.
It probably won't go that far.
But it's a reform that might save lives.
We have to see it.
We don't know.
Haven't seen any announcements on that one yet.
When it comes to Biden, you have to understand you're only going to get reform.
That's all you're going to get from President Biden.
It's all you're going to get from any president.
Change comes from you.
Reform comes from the government.
If you want change, you have to create it.
One of the things that was incredibly hard for me to learn over the course of my life
is that you can't let the good become the enemy of the perfect.
This isn't perfect, but it's good.
It's something I can accept.
And that's one of the things.
I've been asked a bunch when I'm going to go after Biden the way I do Trump.
It's probably going to happen.
Realistically, at some point, that's going to happen.
Biden's not even in office yet.
The first real decision that has tangible numbers behind it that I have seen that has
come from him is something that could save almost half a million lives.
I will give him some breathing room to see what he's going to do.
Probably give him a couple of months after he takes office.
What I can assure you I will not do is criticize a decent, a good policy simply to create false
controversy to get clicks on YouTube.
At the end of the day, this move can save almost half a million lives.
I'm not going to criticize it.
I think it's a good move.
It's a good start.
Is it what I want?
No, of course not.
But it's way better than what we had, and it's something that we can build off of.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}