---
title: Let's talk about the type of political party we want....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xA92AeFVymo) |
| Published | 2020/11/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the type of government and political party we want.
- Expresses surprise that forward-thinking individuals expect major political parties to fully represent their beliefs.
- Encourages forward-thinking individuals to keep looking for the next issue and engagement.
- Suggests that major political parties and governments will never completely match radical beliefs.
- Emphasizes the importance of continuous progress and change, as there will always be new issues.
- Asserts that change comes from people, not government, and governments create reform, not real change.
- Encourages radical individuals to stay in the fight and lead by example.
- States that those on the fringe have volunteered to be on point and lead society through obstacles.
- Reminds that if you change enough minds, laws may not even need to change because society has shifted.
- Concludes by reassuring that those leading the way will eventually bring others along.

### Quotes

- "Change comes from you, not from DC."
- "Governments create reform. People create change."
- "You're leading the way."

### Oneliner

Beau talks about the type of government and political party we want, encouraging forward-thinking individuals to keep looking for the next issue and engagement, as change comes from people, not government.

### Audience

Forward-thinking individuals

### On-the-ground actions from transcript

- Lead by example and keep looking for the next issue and engagement (exemplified)
- Change minds to influence laws and society (exemplified)
- Clear obstacles for others as you lead the way (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the difference between reform and real change, as well as the continuous nature of progress and societal evolution.

### Tags

#Government #PoliticalParty #Change #ForwardThinking #Leadership


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about the kind of government we want,
the type of political party we want.
And we're gonna talk about being on point.
Because I was surprised by something yesterday.
A whole bunch of people who know they are forward thinking,
know they are forward looking,
know that their ideas would lead to deep systemic change,
seemed surprised, bothered even, by the fact that a major
political party does not reflect their beliefs.
I found that odd.
I found that odd.
If you're one of those people, you're on point.
You're up front.
You are looking for that next problem, that next issue,
that next engagement.
You have to acknowledge that you're never going to have
a major political party reflect your beliefs.
Certainly no government is ever going to.
Further, I'm gonna suggest that you don't want them to.
I can't think of many things more disheartening than the idea
of waking up tomorrow to find that all of the ideas that I
hold that I believe are forward thinking and forward looking,
are being debated up on Capitol Hill.
That would be a sad day for me.
Because it means I stopped looking for the next engagement.
It means I gave up.
It means I stopped forward movement.
If you're one of those people who is up front,
one of those people that possesses those ideas that
are radical in nature, those that are looking to create
utopia, you have to acknowledge you are never going to be
normalized because the second your idea becomes normalized,
becomes mainstreamed, becomes a part of a party platform,
you're looking for the next engagement.
You're looking for the next engagement.
There's never gonna be a government that aligns with you
because governments create reform, people create change.
And if you're one of those forward thinking people,
that's what you're looking for.
You're looking for change.
And there's always going to be another issue.
Let's say tomorrow climate change is solved.
Racism is gone, sexism is gone.
Doesn't matter how, it was magic, okay?
Then what?
Well, we got to address poverty, right?
Universal education, universal healthcare,
housing is a human right.
Then what?
By the time we solve all this, it'll probably have something
to do with the rights of colonists on the moon.
But rest assured, when that time comes,
there will be somebody on point.
There will be somebody up front already looking to the next
engagement, because progress doesn't stop.
It's never ending.
There's always gonna be some injustice,
always gonna be some form of barbarism that requires
forward thinking people, that requires somebody to be on point.
If you're a radical, you are always gonna be in the fight.
Because as soon as your idea gets accepted by that main body
of people, you're looking for that next obstacle.
You are looking for that next engagement.
That's a good thing, but it's not for everybody.
It's not for everybody.
There are always gonna be those people who are in the center,
who are behind you.
They just haven't caught up yet.
They will.
As you move forward, they'll move to your previous position.
That's how it works.
And I get it.
There are a lot of people right now going, look,
we got some really pressing issues, and
we're running out of time to deal with them.
I've got it.
Change comes from you.
It's gonna come from you.
It's not gonna come from DC.
It's not gonna come from some president or senator.
That's great man theory.
It's always come from people.
Yesterday, there were a whole bunch of paraphrases, quotes,
direct citations in some cases, from great thinkers.
People who created deep systemic change.
People who altered the course of history.
They had one thing in common.
None of them were in government.
Because change doesn't come from government.
It comes from people.
Reform comes from government.
Those people that were quoted yesterday,
they created social movements.
They created their own power structures.
And they pulled the government into change, kicking and screaming.
And when it came through, it really came through in the form of reforms,
not real change, because governments don't create change.
People do.
Governments create reform.
If you're up front, if you're on the fringe, it's not a bad thing.
It's not a bad thing.
You have volunteered to be on point.
And hopefully you are leading those people behind you through the obstacles.
Hopefully you are changing minds.
If you change minds, laws will change.
If you change enough minds, you don't even need the laws to change
because you have changed society.
But it comes from you.
Change comes from you, not from DC.
The best you're going to get from DC is reform.
If you want deep systemic change, if you're one of those people who looks
at the Democratic Party and says, y'all got to catch up, you're on point.
You're leading the way.
And that's a good thing.
Don't get disheartened by the fact that there's a lot of people
who aren't there yet.
They'll catch up.
Just keep clearing the obstacles for them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}