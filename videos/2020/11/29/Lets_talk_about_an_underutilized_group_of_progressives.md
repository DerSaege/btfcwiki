---
title: Let's talk about an underutilized group of progressives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=llcbvsDMD3I) |
| Published | 2020/11/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses a certain group of people, particularly progressive vets, who are underutilized and may not realize their potential in building communities.
- He shares a story about a veteran friend who feels he's done his part and just wants to stay isolated, despite the skills and experiences he possesses.
- Beau points out that young progressives, fresh out of AIT, might understand concepts in theory but lack the practical experience needed for upcoming challenges.
- He stresses the importance of veterans' skills in logistics, team-building, and community resilience, urging them to share their knowledge.
- Beau encourages veterans to utilize their skill set and mindset to contribute to making the country better and helping those who lack the necessary skills.
- He reminds veterans of the community-focused mindset they had during their service and how that level of community support can be beneficial in facing challenges.
- Beau acknowledges that some veterans may not want to lead but can still serve as valuable advisors and guides to those willing to learn.
- He underscores the idea that veterans can help shape the country by applying their skills differently to address ongoing issues.
- Beau concludes by expressing his belief that veterans have the potential to contribute to creating a better world or country, using their existing skills in new ways.

### Quotes

- "You're needed."
- "You don't have to lead. They don't need leaders. They are leaders."
- "We don't have to accept the way things are."
- "You can help shape this country."
- "What did you do it all for?"

### Oneliner

Beau addresses progressive vets, urging them to recognize their value in building resilient communities and shaping a better future for the country, stressing that they are needed as advisors and guides, not necessarily leaders.

### Audience

Progressive Vets

### On-the-ground actions from transcript

- Reach out to progressive vets in your community and encourage them to share their skills and experiences with building resilient communities (exemplified).
- Organize virtual or future in-person team-building events or workshops for community members with the help of progressive vets (exemplified).
- Support progressive vets in connecting with individuals who are willing to learn and build self-reliant communities (exemplified).

### Whats missing in summary

The full transcript provides a heartfelt call to action for progressive vets to recognize their potential in community-building and shaping a better future for the country through their unique skill set and experiences.

### Tags

#ProgressiveVets #CommunityBuilding #SkillSet #Resilience #Guidance


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a certain group of people
who I think are being underutilized,
who may not even realize they're being underutilized.
I've got a friend, I have a local project that I want to do,
and I'm starting to put the pieces together,
and I talk to this guy,
and he's like, no, not really interested.
Just, I'm done, I've done my part, I'm going to stay here, stay at the house.
He's a vet, and he has seen a lot.
And he's at that point where living's enough.
You know, he lives in a place even more remote than I live,
and I get it on some level.
And if he wants to do that, he's entitled to it.
But there are a whole bunch of progressive vets out there
who may not realize how needed they are.
Because you have a bunch of young progressives,
they're just out of AIT.
They get it in theory, but they've never done it.
Not really, not on the scale that's going to be necessary
in just a couple of months.
You do.
You do get it, because you saw it, even if it wasn't your MOS,
even if it wasn't your job, specifically, you saw it.
You saw people building communities.
Literal communities, in your case.
Towns, villages, trying to make them more self-reliant
and resilient so they don't have to rely on outside forces,
so they can't be influenced, right?
You saw it, and you have that skill set,
because you understand logistics.
You understand team building.
You understand how important rights of passage
are to team building.
You get the usefulness of hip pocket classes.
You can explain with personal experience
how sometimes taking the long way is what ensures success.
You can show them how to build that community,
and they don't need leaders.
They just need advice at times.
And some of them really just need the fine points.
They've got the rough sketch figured out.
Some of them are actively doing it,
and you could just help them do it more efficiently.
But you have the skills and the mindset.
You also can be a good spokesperson,
because you understand tradition
and how tradition is important.
But you also understand the need to embrace the new,
the new technologies, the new situation,
the new obstacles, so you can overcome them.
Wouldn't it be great to use the skill set and mindset
that you already possess to make the country better?
And to touch on a theme that is probably
going to be recurring over the next few months,
what did you do it all for?
What did you do it all for?
Yeah, you made it there and back.
This guy's case, quite a few times.
Why?
No matter how you look at it now, at this moment,
because how you view your time over there changes.
But I bet at that moment, you were looking out
for your community.
That was at the top of your list,
your community being your unit.
You know how important it is.
You know no matter how dire the situation,
that level of community can get you through it.
And yeah, it may not be something you want to do,
but you're needed.
You can help guide people who have the energy, who
have the drive, who are willing to get out there.
They just don't have the skill set.
And they're willing to learn.
There are people in your community,
in your physical community right now, who I am certain
could benefit from a little bit of team building.
And yeah, you can't really do it right now
because of the public health thing, but that's going to end.
And the situations and the problems that we're facing,
they're still going to be there.
You can help shape this country.
You don't have to lead.
I know that's a part of it for a lot of people.
They don't want to be in that situation.
And I get it.
They don't need leaders.
They are leaders.
They need advisors.
And you can do it.
We don't have to accept the way things are.
I'm willing to bet you wanted a better world, at the very least
a better country.
And you can help achieve that here.
Same skills, just being applied differently.
You're needed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}