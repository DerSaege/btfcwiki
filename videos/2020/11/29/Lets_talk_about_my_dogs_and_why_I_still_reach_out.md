---
title: Let's talk about my dogs and why I still reach out....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vtiKrOoS9dQ) |
| Published | 2020/11/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he continues to reach out to people who resist being reached out to, using a story about his dogs as an analogy.
- Describes his German Shepherd, Baroness, as incredibly smart and capable of problem-solving.
- Shares that Baroness is like the raptor from Jurassic Park in terms of intelligence and abilities.
- Recounts how Baroness used to hide his keys when she noticed him packing a bag at the age of two.
- Talks about how Baroness can see through tricks like the fake tennis ball throw with contempt.
- Mentions that Baroness is still open to learning new things despite her age.
- States that Baroness is well-trained and proficient in various tasks.
- Introduces his other dog, Destro, a husky, who is not as intelligent in problem-solving as Baroness.
- Comments on Destro being easily tricked, specifically mentioning his confusion with glass even as an adult.
- Emphasizes Destro's love for children and his affectionate nature towards them despite being easily fooled.
- Draws a parallel between his dogs and people who trusted misinformation about mask-wearing from the presidency.
- Conveys the message that even those who are easily deceived have people who care for them and who they matter to.
- Acknowledges the frustration in trying to reach out to those who resist understanding the importance of certain issues.
- Ends with a reflection on the situation and wishes the audience a good day.

### Quotes

- "At the end of the day, they placed their trust in the institution of the presidency."
- "And they got tricked."
- "Even if you can trick him with a tennis ball over and over and over again for months."
- "Despite all of that, he's still a good boy."
- "Y'all have a good day."

### Oneliner

Beau explains his persistence in reaching out to those resistant to change through a heartfelt canine analogy, revealing the importance of care and understanding even for the easily misled.

### Audience

Empathetic individuals

### On-the-ground actions from transcript

- Reach out to those resistant to understanding with patience and compassion (exemplified)

### Whats missing in summary

The full transcript provides a touching analogy using dogs to illustrate the importance of persistence, compassion, and empathy in reaching out to those who may resist change or understanding.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about why I still try to reach
out to a certain group of people who do not want to be
reached out to.
I was asked why I still do this.
And I thought about it, and I couldn't really come up with a
good way to put it into words, to be completely honest.
So instead of even trying to do that, I'm going to tell you about my dogs.
I have two dogs.
I have Baroness, who is my German Shepherd.
And this dog is incredibly smart.
Problem solving smart.
She's the raptor from Jurassic Park.
All the stuff that a smart dog can do, she can do.
When she was two, if she noticed that I had a bag packed, she would take my keys and hide
them so I couldn't leave.
You know the tennis ball trick where you throw the ball and you don't really throw it?
You try that with her, she just stares at you with contempt.
Like you can see it on her face.
Even at her age, she's still very capable of learning new things.
She's a very well-trained dog at this point.
She knows how to do a lot of stuff.
My other dog is a husky named Destro.
He is not problem-solving smart.
He's not.
I don't expect him to learn a lot in the future.
He is very easily tricked by the tennis ball thing or by glass.
And he's not a puppy anymore, and he still hasn't completely figured out how glass works.
But man, he loves those kids.
Just dotes on them.
And they love him.
He's a good dog, just easily tricked.
That is why, after all of these months, I still try to reach out to people who don't
understand that they need to wear a mask.
Because that's what happened.
At the end of the day, they placed their trust in the institution of the presidency.
Something that, in theory, at least on some level, they should be able to trust to at
least give them accurate information about this.
And they got tricked.
because somebody's easily tricked doesn't mean they don't have people who
love them or who would miss them. So yeah on some level it is it seems pointless
at times but despite all of that he's still a good boy even if even if you can
trick him with a tennis ball over and over and over again for months. And those
kids would miss it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}