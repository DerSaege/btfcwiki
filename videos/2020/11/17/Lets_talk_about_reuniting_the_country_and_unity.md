---
title: Let's talk about reuniting the country and unity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NxneZbFZzko) |
| Published | 2020/11/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the importance of unity in the country.
- Emphasizes uniting behind principles and ideas, not parties or personalities.
- States that unity should focus on moving the country forward, not compromising.
- Warns against compromising with irrational, fear-based individuals for the sake of unity.
- Advocates for leadership in uniting people by presenting and defending ideas effectively.
- Urges to address issues like climate change, economic inequality, and affordable housing to move forward.
- Argues that reaching across the aisle just for appearances can lead to regression.
- Stresses the need to unite behind principles that propel the country forward.
- Encourages looking towards the future and not clinging to outdated ideas.
- Calls for presenting and defending good ideas to progress as a nation.

### Quotes

- "We unite behind principle. We unite behind ideas."
- "Compromise isn't the only way to unite. You can lead."
- "We can't just reach across the aisle for the sake of doing it because it looks good in a photo op."
- "We have to look to the future. We have to look forward."
- "Not simply unite with dinosaurs who don't have the best interest of the American people at heart."

### Oneliner

Beau stresses uniting behind principles and ideas to move the country forward, not compromising for the sake of unity.

### Audience

Leaders and activists.

### On-the-ground actions from transcript

- Convince and campaign for ideas that unite people (implied).
- Advocate for addressing issues like climate change and economic inequality (implied).
- Present and defend good ideas to push the country forward (implied).

### Whats missing in summary

The detailed nuances and examples Beau provides to illustrate his points.

### Tags

#Unity #Principles #Leadership #MovingForward #Future


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about unity.
We're going to talk about uniting.
Reuniting the country.
It's a huge talking point right now.
Everybody's talking about it.
It is the end thing to do.
And it is a good thing.
It is a good idea to reunite the country.
However, it's not unity for unity's sake.
That doesn't make sense.
I will not find a way to unite with people chanting build the wall.
It's not going to happen.
We don't unite parties just to grease political wills.
That's not a good idea.
We unite behind principle.
We unite behind ideas.
Principle not parties, not personalities.
Principle.
That means that those who were recently elected are going to have to put forth ideas that
can unite people.
And those stragglers who can't get behind those ideas are going to have to campaign.
They're going to have to convince them.
You're going to have to politic.
Show them that this is what's best for the country.
Unite people behind that idea.
Not behind a letter, behind a name.
Not behind a donkey or an elephant.
But behind the idea this is how we move forward.
This idea of compromising for the sake of unity doesn't help.
That's how we got here.
That's how we got Trump.
You do not compromise with the most irrational, fear-based people in the room.
That's a recipe for disaster.
If you want to unite the country, it's going to take leadership.
It's going to take going out there and convincing people that yeah, we have to address climate
change.
Yes, we have to do something about the widening economic inequality in this country.
We have to do something about affordable housing.
We have to move the country forward, not hold on to ideas from the 1960s.
Compromise isn't the only way to unite.
You can lead.
You can move forward.
And people will follow.
If you lead, if you present the ideas and you defend them well, people will follow.
Yeah, it's harder.
It's not as politically expedient, but it's better for the country.
That's how we have to unite.
We can't just reach across the aisle for the sake of doing it because it looks good in
a photo op.
If we do that, what happens is the country regresses further.
We normalize the bad policies that put Trump in place to begin with.
If we want to unite, we have to unite behind principles that will move the country forward.
Anything else is failure.
Anything else is surrender.
You have a lot of people in a bad way.
There are a lot of good ideas out there.
They just have to be presented well and defended well by people with a platform, people who
can push the ideas out there and make people see that we have to move forward.
We can't move back.
The United States is not a country that should be looking behind.
It's not all good back there.
There's a lot of bad stuff.
We have to look to the future.
We have to look forward.
We have to build towards that.
Not simply unite with dinosaurs who don't have the best interest of the American people
at heart.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}