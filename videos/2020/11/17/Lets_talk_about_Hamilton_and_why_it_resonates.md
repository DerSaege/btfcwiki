---
title: Let's talk about Hamilton and why it resonates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GHCnoa-MLag) |
| Published | 2020/11/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzes why "Hamilton" continues to resonate after five years without focusing on the plot or music.
- Argues that the musical's impact lies in its relatability to modern issues and its challenge to American mythology.
- Points out how "Hamilton" subtly addresses flaws and corruption in both past and present American society.
- Comments on the mythologizing of the Founding Fathers and the need to shatter these idealized images.
- Examines how the musical tackles issues like social advancement through military service and class disparities.
- Criticizes the tendency to overlook the flaws of historical figures, advocating for a more realistic portrayal.
- Emphasizes the importance of understanding that the image of the founders is a myth and not a reflection of reality.
- Explores how "Hamilton" addresses the founders' stance on slavery and reveals early instances of corruption in government.
- Challenges the notion of historical figures as flawless heroes and encourages a more critical examination of history.
- Concludes by stressing the need to present historical figures realistically to inspire positive change.

### Quotes

- "Facts and truth aren't always the same thing."
- "The image of the founders is a myth. It's not real."
- "They were brash, they had flaws, and they changed things."
- "It's incredibly relatable. This should teach us a lot about how we need to address history."
- "Don't give them figures that are myths."

### Oneliner

Beau analyzes why "Hamilton" resonates by challenging American mythology, subtly addressing flaws, and advocating for a realistic portrayal of historical figures to inspire change.

### Audience

Young activists for change.

### On-the-ground actions from transcript

- Challenge the idealized image of historical figures by promoting a more realistic portrayal (suggested).
- Encourage critical examination of history to inspire positive change (suggested).
- Support young activists by acknowledging their voices against societal issues (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how "Hamilton" challenges American mythology, subtly addresses flaws in society, and advocates for a realistic portrayal of historical figures to inspire change.

### Tags

#Hamilton #AmericanMythology #HistoricalFigures #Activism #Change


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Hamilton.
Don't worry if you haven't seen it, it's not going to ruin it for you. You can still enjoy it.
We're going to kind of take the back roads.
Not really going to talk about the plot very much.
Not really going to talk about the music.
We're going to talk about why it resonated
and is still resonating
five years after it premiered.
Why people who
generally don't enjoy theater
are drawing inspiration from it.
Why it isn't typical.
And the easy answer here of course
is
the music.
Granted, very different than
most stuff on Broadway.
Absolutely.
But it's way more than that.
It's resonating because it's relatable.
While it is still
the classic American story, you know, the immigrant, orphan,
rags to riches, self-educated, self-made man
who lived the American dream.
Yeah, it's that. It is.
But it does a really good job
of shining a light
on American mythology
as opposed to American history.
I'm of the firm belief
that facts
and truth aren't always the same thing. You can use facts to paint a false
narrative.
Sometimes
fiction
is the easiest way to get it at truth.
Especially if a false narrative has been accepted for so long
that it appears true.
You know, today
when we talk about the government
it's become
so corrupt.
Become
as if it wasn't corrupt before.
The scandals of today.
Because all of our founders were very moral people.
None of this would have happened back then. They were all ideologically motivated
with the purest intentions.
That's how it's framed.
We treat our founders and the founding of this country as a myth.
We don't really care about the truth, about the realities of it.
They were people.
They were flawed,
guaranteed.
Hamilton does a really good job
of showing that.
And showing that there are a lot of things that are wrong today
that were wrong then.
And it does it so subtly
that I'm not sure
people
realize that they're hearing what they heard.
It doesn't matter really if they recognize it.
They may not know they heard it, but they did.
And I think that is why
it's inspiring a lot of young people today.
Because they're getting a
very artistic representation of the fact that a lot of the things they believe
are true.
That a lot of the
issues
that they're facing,
they're not bugs in the American system, they're features of it.
As a kid in the Caribbean,
I wished for a war.
I knew that I was poor.
I knew it was the only way I could rise up.
1-800-GO-ARMY
Get your enlistment bonus.
If you're in poverty today,
what is one of the most accessible means
of getting out of it?
Military service.
There's always been the tendency
of those on the top
to use those of lesser means
for military service.
That's a theme that is very prevalent
in this show.
Those who want to
socially advance
through the revolution,
through military service.
It addresses other class issues as well.
It shows how the wealthy, prior to the revolution,
kind of
came down to
see what the riffraff were up to,
kind of explore it.
I would imagine that many of those who are out in the street today
who are
out in the street, out of self-defense,
self-preservation, they're speaking out because they literally do not have a choice.
They might feel the same way
about a lot of the people who show up just to take a selfie and then leave.
Tourists,
not truly committed.
It shows
Hamilton and his crew
heckling
those speaking out
in favor of the king.
It's not the image we have of our founders.
We don't really picture them meeting in bars.
We don't picture them
heckling people on the street.
We don't picture them making brash decisions
and still their cannons.
But they did.
They were aggressive young men.
Should have shot them in the mouth. That would have shut them up.
These aren't actual quotes from these people,
but they definitely reflect their attitudes,
definitely reflect
the actual events
better than what we hear
in our history books,
where they're painted as constantly elegant and eloquent.
They weren't.
That's mythology.
They were normal guys.
And that
myth
is something that probably should be shattered.
Hamilton
is painted as arrogant,
brash, and flawed
because he was.
He was.
Washington
is painted as having a lot of self-doubt
and completely aware of the fact
that his men were embellishing his elegance and eloquence.
In the U.S. today,
the Constitution itself
has become holy,
sacred, yet a public figure recently say
something to the effect of
we don't change the Constitution. We can't just change the Constitution. Apparently completely unaware
that the machinery to change the Constitution is in the Constitution
because it's part of that myth.
It's perfect. It's infallible.
It's divine.
If it was perfect and infallible and divine,
Hamilton and company wouldn't have needed to write eighty-five essays to defend it to the public
and they probably would put their names on
instead of publishing them anonymously.
These are things that
really
should be addressed.
You have a whole lot of people in this country right now that want change,
that want to change the country,
want to fix the problems.
But when they look in their history books,
what do they see?
People who are completely unlike them
as the only people who were ever successful
at changing anything.
Because we overlooked the flaws,
we subscribed to great man theory.
The reality is
everybody, they were all just people.
They were brash, they had flaws,
and they changed things.
It's probably incredibly useful
for young people today to understand
that that image
of the founders is a myth.
It's not real.
Hamilton also addresses
some of our original sins.
A common belief today
is that basically all the founders were okay with slavery.
It's just what was done at the time. They didn't know any better. It was a necessary evil.
That's not true.
In fact, the spiritual and philosophical founder of the revolution
wrote abolitionist texts before he ever penned anything
in favor of revolution.
Most of Hamilton's circle
were abolitionists to some degree.
And it points us out in a
cabinet meeting,
a scene of a cabinet meeting which is
done in the format of a rap battle
between Hamilton and Jefferson.
And Hamilton points out that maybe it not,
may not be the best idea
to
take a civics lesson from a slaver.
Because hey neighbor, your debts are paid because you don't pay for labor.
That was the whole idea at the time.
Hamilton wanted the federal government to assume states' debts after the war.
Those in the south, including Jefferson, didn't want to do that
because their states didn't have much debt.
So they didn't feel like they should pay a share of
New York's,
those states in the north.
This little disagreement
highlighted some other stuff.
If you say quid pro quo today,
you think of corruption, right?
Something that's just unthinkable.
Except
Madison and Hamilton
had a dinner
arranged by Jefferson in private, no one else was in the room where it happened,
where they kind of hammered out the
details
of assuming states' debts.
Hamilton got his banks
and the southerners got D.C.
That kind of corruption,
it's been here since the beginning. If you want to look into this, it's called the Compromise of 1790.
1790.
The government didn't become corrupt.
It was always corrupt because it always had people in it.
And then you have
Aaron Burr
who is cast as just wanting power for power's sake.
And again, that's something that
we today
see as a new development.
All of those older politicians, especially our founders,
they had the purest ideological reasons for wanting to be in government.
They didn't want power, they didn't want control.
They were all doing it for the right reasons.
Because that's what we do.
We like to
overlook stuff,
turn a blind eye to it.
Until it's been so long we can pretend like
those people are so far removed from us we would never do that. But the reality is there's
stuff happening today that we know is wrong.
And we're not looking at it.
We're looking away.
And
a hundred years from now
I'm sure Americans may tell themselves,
oh, that's really how they just had to do it.
Nobody knew any better.
It was a necessary evil.
Our government didn't become tainted.
It didn't become corrupt.
It was always tainted
because it had people in it.
People are corruptible, people are flawed.
They're not myths.
They're not infallible, they're not perfect.
And this is something that a generation
that wants change
probably needs to hear.
If you are one of those people who
really wants that deep systemic change in this country,
and you're young,
and all you've heard
are the images of
self-founders in your textbooks,
how are you supposed to compare to that?
It does all of this
without attacking
the ideals of America.
Because it still is.
That orphaned immigrant rags to riches, self-made, self-educated man who
got to live the American dream because
he didn't throw away his shot,
because the fates brought him and this group of people together
and they worked like they were running out of time because they were just non-stop in
pursuit of their goals.
And that's why they succeeded.
It's still framed
in a very American context,
but it undermines that mythology
that has to be undermined at this point.
It's holding the country back.
There are a lot of people
who probably hear this,
and it resonates with them even if they don't
consciously acknowledge
what they're hearing or what they're seeing.
They're seeing that story taught in a new way,
and in a way that is relevant to them.
Burr's big criticism
of Hamilton
is asking, why do you always say what you believe?
I would imagine
that there are young people today
who feel like they're being asked that all the time
when they speak out against problems that
we really are running out of time to deal with.
And they're mocked.
They're told that they don't understand the world,
that politics is more important.
These are themes that
I think a lot of people could
benefit from exploring.
And at the end,
that person who could have changed so much more
could have been
an even greater asset
to the country and to his community
was gunned down
by somebody who wanted power for power's sake.
I don't know how it couldn't resonate today.
It's incredibly relatable.
This should teach us a lot about how we need to address
history.
If you want to create those people
who believe
they can make positive changes,
don't give them figures that are myths.
Don't give them people to compare to that they can't
because that image that we give them, it's not real.
And it's something that they will never
be able to live up to,
which will always be a reason
for them to say that they can't change anything.
Discourage them
when we need
to be encouraging them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}