---
title: Let's talk about why Trump might have lost the military vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UyWdOQpKwDU) |
| Published | 2020/11/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why a certain demographic didn't vote as expected in the recent election.
- Shares favorite quote about military personnel voting Democrat, contrary to expectations.
- Challenges the stereotype of the ultra conservative, socially regressive military.
- Points out the diversity within the military and reasons why some may have issues with Trump.
- Mentions specific concerns like Trump's handling of military installations' names and hurricane response.
- Talks about soldiers' dissatisfaction with Trump's attitude towards personal protective gear.
- Notes opposition to Trump's refusal of a peaceful transition of power and treatment of respected generals.
- Reveals that the betrayal of Kurdish allies by Trump was a major concern for every soldier Beau talked to.
- Suggests that judging a demographic by its most visible members is not representative of the whole.
- Warns Democrats not to assume they have secured the military vote long-term due to Trump's unpopularity.

### Quotes

"Saved it to the end because it's the only thing that everybody that I talked to said."
"The Kurds were our greatest ally in the Middle East, and Trump sold them out."
"No demographic should be judged by the most visible members of that demographic."

### Oneliner

Military personnel surprised by Trump's actions didn't vote as expected, showing the diversity and individuality within the ranks.

### Audience

Military personnel and political analysts

### On-the-ground actions from transcript

- Reach out to military personnel to understand their concerns and perspectives (suggested)
- Advocate for policies that support military members and address their grievances (suggested)

### Whats missing in summary

The full video provides more in-depth insights into the diverse reasons why military personnel may have voted unexpectedly and the implications for future elections. 

### Tags

#Military #Election #Trump #Demographics #Diversity


## Transcript
Well howdy there internet people. It's Beau again. So today we are going to talk about why a certain
demographic didn't go the way everybody expected him to go. There's a stereotype that exists
when you're talking about active duty. Everybody expected them to vote heavily in favor of Trump.
That apparently didn't play out that way. My favorite quote, something that was in a statement,
Although I cannot provide specific numbers or names, I can estimate that at least 80%
of the military ballots I saw were straight ticket Democrat or simply had Joe Biden's name
filled in on them. I had always been told that military personnel tended to be more conservative,
so this stuck out to me as the day went on. This was somebody who believed that there might be
some trickery going on. Okay, the military is incredibly diverse. It is an incredibly diverse
organization and something that happens with the military is something that happens with a lot of
demographics. Those who proclaim themselves to be part of that demographic the loudest
often get taken to represent the entire demographic. That's not true. This idea
of the ultra conservative, socially regressive military, that's not true.
It's not even historically accurate. The military was desegregated before the country was.
You could join the military and be gay before you could get married. There's a whole bunch of things
where the military was on the cutting edge of social progress. Aside from that, the military
is incredibly diverse. There might be a whole bunch of people who are serving who really took issue
with Trump defending the current names of some of the military installations. There might be some
who took issue with the way he marginalized certain groups. There might be a whole bunch
of Puerto Rican soldiers who are really unhappy with the hurricane response.
There could be medical people, a lot of them, who take issue with Trump's handling or mishandling
of the current public health issue. They're one that surprised me, to be honest. Guys in the
infantry who know how important personal protective gear is, they've got to wear it. If they don't
wear it, they get in trouble. To watch the commander-in-chief downplay and make fun of
personal protective gear that could save somebody apparently bothered a couple people. Didn't see
that one coming, to be honest. There were those who took issue with the idea of him refusing to
agree to a peaceful transition of power, and there were quite a few who had issue with him bad-mouthing
various respected generals. Those were the reasons I got, and then there was one more.
Saved it to the end because it's the only thing that everybody that I talked to said. Now, granted,
I only talked to like a dozen people, not exactly scientific, but they all mentioned this.
That.
A soldier retiring today spent 20 years in. 17 of them were spent fighting alongside people wearing
these, and Trump sold them out, left them twisting in the wind. That bothered people a lot and
understandably. The Kurds were our greatest ally in the Middle East, and Trump sold them out.
That was the only thing that every single person I talked to mentioned. All of these other ones,
yeah, they mentioned them, but it wasn't unique. It was the only thing that I talked to that
I talked to mentioned. All of these other ones, yeah, they mentioned them, but it wasn't unanimous.
So it may turn out something that I believe may end up being the biggest foreign policy mistake
in U.S. history is also what helped him lose the election, which is fitting.
But one thing we should take away from this is that no demographic should be judged
by the most visible members of that demographic, because oftentimes they don't reflect the whole,
and that is definitely what happened here. Now, to the Democrats cheering that you now
have the military vote, no. No. You probably don't. Overall, the military is still pretty
conservative. They will probably vote Republican in the next election. You got them this time
because they know how bad Trump is. I wouldn't count on a repeat of that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}