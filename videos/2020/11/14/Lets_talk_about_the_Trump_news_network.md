---
title: Let's talk about the Trump news network....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=frN0EYXws7U) |
| Published | 2020/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on Trump's post-public life plans, particularly focusing on the idea of a Trump cable news network.
- Points out potential financial challenges for the network due to Trump's controversial style and the risk of lawsuits.
- Raises concerns about advertisers being hesitant to associate with such a network due to potential boycotts.
- Suggests that an internet outlet might be more economically viable short-term for Trump, but social media restrictions could hinder reach.
- Anticipates Trump blaming others if his post-presidency venture fails, reflecting his pattern of not taking responsibility.
- Predicts that a Trump news network could end up being another bankruptcy in Trump's history, potentially revealing his true colors to his base without the shield of the presidency.

### Quotes

- "Realistically if this network is to mimic Trump's style it is going to be controversial nonstop."
- "The reason I'm excited, the reason I'm kind of looking forward to this is because the president accepts responsibility for nothing."
- "I honestly see the final days of this news network with him holding up buckets of survival food to sell."

### Oneliner

Beau speculates on a potential Trump news network, predicting financial challenges, advertiser hesitancy, and Trump's tendency to blame others for failure post-presidency.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze and critically question the credibility and impact of potential media outlets post-Trump presidency (implied).

### Whats missing in summary

Insights into the potential consequences of Trump's post-presidency endeavors beyond financial viability.

### Tags

#Trump #NewsNetwork #PostPresidency #MediaSpeculation #FinancialChallenges


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what Trump may do after he leaves public service, isn't
the right word, after he leaves public life.
What the president reject may engage in when it comes to business.
There's one idea that keeps popping up and there are people on the right that are just
like super excited about it.
I think I may be the only person who doesn't support Trump who just can't wait to see what
happens.
And then there are people who oppose Trump who have genuine concerns about what he might
be able to do with a platform.
And this idea of course is the idea of a Trump cable news network.
Realistically despite the president's long history of very successful business endeavors,
I'm not sure he understands where the money comes from.
Sure this network would certainly have an audience.
Millions would tune in.
However there are two things going against the financial viability of this network.
The first is the whole idea is to reflect Trump's style, his brand.
The president is let's just say economical with facts.
That combined with the way he describes people at times might be a recipe for a lawsuit if
you're presenting it as news.
That would probably impact the bottom line.
Aside from that I am just, I can't wait to see who the advertisers are.
I don't know of many companies that would willingly expose themselves to boycott like
that.
Realistically if this network is to mimic Trump's style it is going to be controversial
nonstop.
There is going to be some inflammatory statement made every five minutes.
Would you want your company logo, your brand to show up right after that?
Probably not.
If a company doesn't advertise on that channel they don't risk alienating anybody.
Nobody is going to be mad.
However if they do and Trump or one of the pundits that he hires says something inflammatory
and then their logo shows up they catch the heat.
They catch the boycott.
I don't know many marketing agencies that would say hey this is a great idea.
This is a way you can reach people.
I don't see it happening.
I don't see it being economically viable.
Now the alternative to a cable news network would be an internet outlet.
Now that he could probably make some money at in the short term.
He's definitely going to have the audience and the internet provides him with advertisers
who are more insulated from the risk.
So yeah I can see that.
However there's another issue there.
A lot of the stuff that the president says the only reason it's allowed on social media
is because he's the president.
A lot of his inflammatory statements go against the terms of service of major social media
networks.
When he is no longer the president they won't carry those.
He will probably not be able to get any reach on social media which is what drives traffic
which is what gets people to see the ads.
See the reason I'm excited, the reason I'm kind of looking forward to this is because
the president accepts responsibility for nothing.
So when inevitably this happens and his viewership tanks, who's he going to blame?
He won't take responsibility for himself.
He will lash out at those who are too lazy to go to his website on their own.
He will lash out at his base.
He will turn on them the same way he has turned on everybody else.
And that may be the great failure, the dismal failure that pushes Trump out of the public
eye.
I personally cannot wait because I honestly think that's what's going to happen.
I would imagine that a Trump news network will be the next bankruptcy in a long string
of bankruptcies.
But more importantly I think that it might show his base, his true colors in a way they
haven't seen because he won't be able to wrap it in the flag anymore.
He won't be able to wrap it in this air of legitimacy that comes with being the president
of the United States.
I honestly see the final days of this news network with him holding up buckets of survival
food to sell.
That's where I see this going.
And I will be there for it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}