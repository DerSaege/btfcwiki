---
title: Let's talk about Trump having faith in electors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fpjr4_lIkEo) |
| Published | 2020/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Spent hours researching election laws in Arizona, Michigan, Pennsylvania, and Wisconsin due to a theory about GOP substituting electors.
- Found laws prohibiting the substitution of electors in these states, causing confusion.
- Republican leadership in Arizona, Michigan, Pennsylvania, and Wisconsin confirmed that elector substitution won't happen.
- In Michigan and Arizona, electors are mandated by statute to vote in accordance with the people.
- Pennsylvania GOP leaders stated they will not have a hand in choosing electors or intervene.
- Laws on the books in two states prevent the elector substitution scheme.
- American democracy appears to be more resilient than anticipated.
- Beau doubts the realistic possibility of the elector substitution scheme, relying on politicians' word.
- Considers this as the last attempt to keep Trump in power.
- Encourages staying vigilant for any future schemes.

### Quotes

- "American democracy appears to be more resilient than a lot of us pictured."
- "This is how we could win type of thing, but not really."

### Oneliner

Beau dives into election laws in key states, debunks the elector substitution theory, and praises American democracy's resilience against schemes to keep Trump in power.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed about election laws in your state (implied)
- Be vigilant against potential threats to democracy (implied)

### Whats missing in summary

Details on the specific laws in Arizona, Michigan, Pennsylvania, and Wisconsin that prevent elector substitution.

### Tags

#ElectionLaws #ElectoralCollege #AmericanDemocracy #GOP #Trump


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about electors.
I have spent the last few hours learning more
about election law in Arizona, Michigan, Pennsylvania,
and Wisconsin than I ever need to know.
And by the end of it, I was a little confused, to be honest.
Because these are the states that
are central to this theory that's going around,
that the GOP, that the Republican Party,
was going to substitute electors and basically just
take the election and re-elect Trump using this process.
Now, as I was looking through it,
I found laws on the books that said they couldn't do this.
But I didn't know if they had been rescinded or whatever.
Because it had been talked about so much.
I was kind of at my wits end, to be honest,
because it is very confusing.
And this isn't something I know a lot about.
Luckily, as I'm researching, the AP broke a story.
So to get to the point, the Republican leadership
in the states that are in question, Arizona, Michigan,
Pennsylvania, and Wisconsin, all four said it isn't happening.
In Michigan and Arizona, they said, no,
they're mandated by statute.
The electors will vote in accordance with the people.
Now, in Michigan, they said that they
were investigating the election.
But they didn't expect to find anything
that was going to change the outcome.
In Pennsylvania, the top two GOP people
said that the assembly there does not and will not
have a hand in even choosing the electors,
and that they're definitely not going to intervene.
And apparently, the top GOP people in Wisconsin
made a statement about this like a month ago.
And apparently, nobody knew.
So it looks like the elector scheme
doesn't have the cooperation of the people
required to make it a thing.
And it looks like in two of the states,
it's actually against the law.
There are laws on the books stopping it.
Now, I thought I saw one in Arizona as well.
But to be honest, it's late and our eyes are blurry.
So this appears to be yet another,
well, this is how we could win type of thing, but not really.
It doesn't look like this is a realistic possibility either.
I can't say with as much certainty
because we're kind of relying on the word of politicians, which
is not something I like to do.
And I don't know a whole lot about it.
But from everything I've seen, it doesn't look realistic.
It turns out it looks like American democracy
is a little bit more resilient than a lot of us pictured.
I think this is actually the last of the little
keep Trump in power schemes.
So we'll keep our eyes open for what they come up with next.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}