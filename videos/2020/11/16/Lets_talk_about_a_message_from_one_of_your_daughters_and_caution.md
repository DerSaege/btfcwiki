---
title: Let's talk about a message from one of your daughters and caution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=by1xCUrQTaM) |
| Published | 2020/11/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An email from a daughter expresses concerns about her 67-year-old father with multiple medical conditions who is at risk but refuses to wear a mask, waiting for Beau to shave his beard first.
- Beau, a hermit who rarely leaves his property, initially shaved his beard to set a good example and comply with mask-wearing.
- Beau stresses the seriousness of the current situation, pointing out that it's worse than ever and the President is not adequately focused on it.
- Urging everyone to wear masks, wash hands, avoid touching faces, and stay home if possible, Beau underlines the importance of following safety protocols.
- Despite the dire situation, Beau reminds listeners to exercise caution as it's going to get worse with colder weather approaching.

### Quotes

- "Please wear a mask, wash your hands, don't touch your face, stay at home if you can."
- "It is worse now than it has ever been."
- "He's focused on other things. There was little response to begin with. There is no response right now."

### Oneliner

Beau stresses the seriousness of the current COVID-19 situation, urging everyone to wear masks and follow safety protocols as it's worse than ever and precautions are vital with colder weather approaching.

### Audience

General public

### On-the-ground actions from transcript

- Wear a mask, wash hands, avoid touching face, and stay at home if possible (suggested)
- Exercise caution and follow safety protocols diligently (suggested)

### Whats missing in summary

The importance of taking precautions and following safety protocols to combat the worsening COVID-19 situation.

### Tags

#COVID19 #MaskUp #SafetyProtocols #Precautions #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
So today
we're going to talk about
taking all the precautions
that you need to.
uh...
and a
email I got from one of your daughters.
My dad is sixty seven years old.
He watches you every day. He also goes about his normal life
even though
he's, and there is a giant list of medical conditions here.
At the end of the day, short version, he's at risk.
It's been a battle to get him to wear a mask over his beard.
He told me you initially shaved your beard and so did he when this all
started
and that he'll worry about it
when you do.
Can you remind everybody it's still serious please?
Okay so first
uh...
I'm a hermit.
I leave this property like three times a month for real.
I don't go anywhere.
uh...
When I do
I wear a mask.
I shaved my beard, I made it mask compliant initially
just to
set
a good example I guess. There were a lot of people who didn't want to shave
initially.
uh...
Because they weren't certain of how serious it is. It is incredibly serious.
It's worse now than it's ever been.
The president
is not
focused on this.
He's focused on other things.
There was little response to begin with.
There is no response right now.
So that's something that you should definitely keep in mind.
Aside from that.
I'll trim it up later.
Wear a mask. What are you doing?
We're at the highest rates we've had in a very very long time.
uh...
Please wear a mask, wash your hands, don't touch your face, stay at home
if you can.
Do everything you're supposed to.
It is worse now than it has ever been.
It's not better yet.
uh...
And it's still going to be a while. It's going to get worse as it gets colder.
Please exercise all of the caution that you can.
Anyway,
it's just a thought. You have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}