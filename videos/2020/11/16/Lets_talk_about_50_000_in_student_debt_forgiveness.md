---
title: Let's talk about $50,000 in student debt forgiveness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uK9k0xi0yZo) |
| Published | 2020/11/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the proposal to forgive the first $50,000 of student debt, discussing the pushback it's receiving and analyzing the arguments.
- He points out historical instances where community efforts could have prevented the need for certain agencies like FEMA, FDIC, and OSHA.
- Beau questions the argument against forgiving student debt, stating that education is a community asset that benefits everyone.
- He argues that education strengthens the community, and educated individuals contribute significantly to society.
- Beau challenges the idea that forgiving student debt is a handout, asserting that it doesn't go far enough and should be seen as a starting point.
- He criticizes the opposition to universal education, noting that it reveals underlying class issues and a desire to maintain barriers to higher-paying jobs.
- Beau suggests shifting perspectives towards community welfare and away from perpetuating inequality through credentialing.

### Quotes

- "Education is a community asset."
- "The problem is this doesn't go far enough. It's a starting point."
- "Congratulations. You're admitting that it's a class issue."
- "You're saying flat out that you want to use a college education as a barrier for entry into higher paying jobs."
- "Perhaps we should change and we should start looking at things from a community standpoint."

### Oneliner

Beau challenges misconceptions on forgiving student debt, advocating for education as a community asset and critiquing class-based barriers to higher education.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Promote community education initiatives to strengthen overall welfare (suggested)
- Advocate for policies that support equitable access to education and opportunities (suggested)

### Whats missing in summary

The full transcript provides a comprehensive analysis of student debt forgiveness, community benefits of education, and the need to address class-based barriers in higher education.

### Tags

#StudentDebt #Education #CommunityAsset #ClassEquality #SocialJustice


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the proposal to forgive the first $50,000 of student debt
because that was something that came out, I guess Schumer mentioned it, it's an idea
from him and Warren that Biden may be able to do it with a pen stroke.
There's already people pushing back against it.
So let's run through it.
Let's run through the argument and let's see if it holds up.
The main one that I've seen is, well I didn't get my debts forgiven.
Okay and that's not an argument, especially coming from the people who are so fond of
saying life's not fair.
It doesn't make a whole lot of sense.
I would point out that a whole bunch of people lost money in the banks before the FDIC existed.
I would point out that there were a whole bunch of people left homeless after major
issues before FEMA.
I would point out that there were a whole lot of people hurt before OSHA existed.
Now the reality is that if we would come together as a community and work together to benefit
the community, we wouldn't need any of these organizations.
We wouldn't need any of these agencies.
They wouldn't have to exist because we would be working together to benefit the entire
community, right?
Makes sense.
I don't think that, hey this problem existed for me so other people should have to go through
it is a really good argument.
Not when the idea is to get rid of problems that exist.
The real question you have to ask is, education something that benefits the community as a
whole?
Is education a community asset?
I would assume that you like knowing that your docs and nurses know what they're doing.
I would assume that you take solace in the fact that the engineers who worked on the
products you use know how to make them safely.
I would assume that you like the idea of the teachers who teach your children being well
versed in subject matter.
I would assume that you would like for the people who are going to make that vaccine to have
some clue of what they're doing and that you will benefit from their education.
You gain some tangible benefit, some value from them being educated in that field.
It makes sense.
Education is a community asset.
The more educated people you have, the stronger the community.
It's not like these people are getting a handout because they're lazy.
They did the work.
The problem with this isn't that it's forgiving the first 50,000 and that other people didn't
have that happen for them.
The problem is this doesn't go far enough.
It's a starting point.
That's it.
And the argument against universal education boils down to, well, if you do that and it's
free then anybody can get it, which is literally the point because education is a community
asset.
And then that's followed up with, but then they're less valuable.
And that's where you get to the real meat of it.
Congratulations.
You're admitting that it's a class issue.
You're saying flat out that you want to use a college education as a barrier for entry
into higher paying jobs.
Those whose parents can't afford to send them or who can't get the loans, well, they can
just stay in those low paying jobs.
You don't want to have to compete against them.
You don't have to compete based on merit.
You want a handout.
You want some way to prevent them from possibly raising above their station, getting out of
the situation they were born into.
When that argument is made that, well, then everybody will have one, it'll be worthless.
It's an admission that the goal of credentialing is to keep others down, to keep the poor in
their place.
Perhaps we should change and we should start looking at things from a community standpoint.
We should stop kicking down and I don't know, maybe promote the general welfare.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}