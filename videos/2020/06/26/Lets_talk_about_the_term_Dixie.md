---
title: Let's talk about the term "Dixie"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F2s7brvyYa8) |
| Published | 2020/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the origin of the term "Dixie" in the context of a group wanting to create a left-leaning space in the South.
- Three theories on the origin of the term: a slave owner named Dixie, the Mason-Dixon line, and French banknotes in Louisiana.
- Beau dismisses the theory of a benevolent slave owner named Dixie and finds the French banknotes theory the most plausible.
- The term "Dixie" became popular during the Civil War due to a song called "Dixie," written by a man from Ohio, Daniel Emmett.
- Emmett, the writer of the song, had racist tendencies by liking blackface, although he was not a Confederate.
- The term "Dixie" had racist connotations during its popularity and a resurgence in the 1960s.
- The term is fluid in meaning and can be non-racist when referring to certain activities or geography.
- Beau questions whether using the term "Dixie" to market a left-leaning group in the South may attract unintended individuals.
- Beau encourages feedback and comments on the topic to gain diverse perspectives.

### Quotes

- "The term is very fluid."
- "I have no idea whether or not the perception of it from black Americans is racist."
- "There are certain areas that seem like entirely different countries within the South."

### Oneliner

Beau navigates the complex origins and evolving meanings of the term "Dixie," prompting reflection on its potential racial implications in different contexts within the Southern region.

### Audience

Southern activists

### On-the-ground actions from transcript

- Research the historical context behind terms before using them (suggested)
- Seek diverse perspectives and feedback on potentially sensitive terms (suggested)

### Whats missing in summary

Deeper insights into the racial implications of the term "Dixie" in different Southern regions and communities.

### Tags

#Southern #Dixie #Racism #History #Geography #Community


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about an email question
I got, a quick history lesson on a term,
and then we're gonna answer it,
and by we, I mean you guys,
because I don't have a hard answer on this one.
I mean, I do, but mine has nothing to do
with the actual question at hand.
Okay, so the email basically boils down to this.
There's a group of people who are wanting to set up
either a Facebook page or group, something,
that is for people in the South who are left-leaning.
One of the suggested names that they came up with
is Left End Dixie.
If you don't know, the Dixie Chicks,
within the last like 48 hours,
have dropped Dixie from their name.
Now they're just the chicks, okay?
This of course sparked conversation
about whether or not they should use this term
and whether or not the term Dixie is racist.
Okay, so to answer this question,
we have to know the origin, the origin of the term.
There are three theories on where it came from.
One of them is just trash.
One to me seems less likely, and one seems more likely.
I have the remaining two.
The first theory is that there was
this really nice slave owner named Dixie,
and all the slaves wanted to go and work on Dixie's land,
which then became Dixieland.
That's not real.
There's nothing to suggest that's true
as far as I know and I've looked.
It doesn't even make sense
that the white slave-owning population
would choose to self-identify as a term
that came from slaves who weren't happy at their plantations.
It just doesn't even make sense.
So I don't believe that one at all.
Another one is that it came from the Mason-Dixon line,
which is the dividing line between the North and the South.
And that kind of makes sense.
Dixon, Dixie, I get it.
However, I know that maybe from the outside,
looking down here, it all looks like one thing.
It's not.
If you're outside of the South and you say Dixie,
yeah, generally it refers to the whole South.
But if you're inside the South and you say Dixie,
you're really talking about a specific area,
Alabama, Mississippi, Louisiana, where I'm at.
But as you're gonna find out,
terms are kind of fluid down here.
So it doesn't make sense to me
that those states closest to the Mason-Dixon line
don't really refer to themselves as Dixie,
but those almost as far away as you can get do.
The third theory, the one I believe,
has nothing to do with any of this.
It has to do with French.
In Louisiana, prior to the Civil War,
they printed their own banknotes, the Bank of Louisiana.
They spoke French and English.
On the $10 bill, it said 10 on the front,
and on the back, it had 10 written in French, DIX.
These bills were Dixie's.
The trade that went up and down the Mississippi River,
went into New Orleans, all of this,
mainly came from Mobile and up and down the Mississippi,
Alabama, Mississippi, Louisiana,
the area that we still call Dixie.
That, to me, makes a whole lot more sense,
that it came from people who were working on boats
and carrying this money with them.
That seems to make a whole lot more sense.
Neither one of the origins is inherently really racist.
They're geography things.
However, that's not the end of the story.
The term became popularized during the Civil War
because of a song called Dixie.
Oddly enough, written by a guy in Ohio, Daniel Emmett.
Emmett's the last name.
May be wrong on the first name.
How would he know the term?
Because of the Mississippi and Ohio rivers.
People going up and down it.
It makes sense.
It falls into, it supports that theory a little bit more.
Okay, so Emmett liked the blackface.
Liked it blackface.
Racist, okay?
However, not a Confederate.
In fact, he said that if he had known
the South was gonna use the song,
he never would have wrote it.
The song was rewritten several times.
One of them, most notably, by a guy named Albert Pike.
You might remember that name from recent events.
So when it became popular, it was definitely racist.
And then it had a resurgence of use in the 1960s.
Definite racist connotation there.
And then you had the Dixiecrats as well.
Now, as today, how it's used, it's either a geographic area
or it all depends on how it's said.
If I was to say, you know, I'm in a Dixie mood tonight,
that means I'm gonna go drink shine on somebody's porch
and listen to Cajun music.
Nothing inherently racist about that.
However, if somebody's like, you know you in Dixie, right?
That's, yeah, there's definitely some racist undertones to that.
Even the term whistling Dixie, based on context,
can mean that person is over there goofing off
and wasting time, that person's over there talking to a girl,
or that person's in a bad mood and looking for a fight.
The term is very fluid.
So I don't know that I would use it,
but strictly based on clarity,
I have no idea whether or not the perception of it
from black Americans is racist.
Gonna have to ask them, and hopefully they're gonna answer
in the comments section.
I would like to know, actually, to be completely honest,
because where I'm at on the Gulf Coast,
it doesn't really get used in that manner.
It really does have more to do with Cajun music and food
or a certain geographic area.
However, there are other parts of the South
where it's gonna have a different meaning.
It really is very...
There are certain areas that seem like
entirely different countries within the South.
So I'm not sure that using that term
to try to market a left-leaning group
is going to produce the results you want.
You may get the exact opposite type of people
being interested in it.
So I can't wait to see the comments section
on this one, to be honest.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}