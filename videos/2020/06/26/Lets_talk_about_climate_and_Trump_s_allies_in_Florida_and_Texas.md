---
title: Let's talk about climate and Trump's allies in Florida and Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-DcXXv-LwVg) |
| Published | 2020/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the aftermath of failed reopenings in states like Florida and Texas led by President Trump's allies.
- Points out how politicians, influenced by economic power, ignored scientists' advice and pushed for early reopening.
- Describes the pattern of creating counter-narratives against scientific consensus in various issues, like climate change.
- Emphasizes that unlike rolling back reopenings, there's no turning off the irreversible effects of climate change once it reaches a critical point.
- Urges to prioritize addressing climate change over playing into the culture war led by profit-driven politicians.

### Quotes

- "We have to stop listening to the people who put profits over people's lives."
- "We have one planet and the governors and leaders will not be able to do anything once it gets past a certain point."

### Oneliner

Failed reopenings show the danger of prioritizing profit over lives, urging action against climate change before irreversible damage.

### Audience

Climate activists, Environmentalists, Advocates

### On-the-ground actions from transcript

- Push for policies that prioritize environmental sustainability and address climate change (suggested)
- Support and vote for leaders who prioritize science and people over profit-driven interests (implied)

### Whats missing in summary

Importance of immediate action to prevent irreversible consequences of climate change.

### Tags

#ClimateChange #ScientificConsensus #PoliticalInfluence #ProfitOverPeople #EnvironmentalActivism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a teachable moment
that's happening right now.
Something that's coming out of the failed reopenings,
that's what they are.
If you don't know, a lot of President Trump's
greatest allies, those who stood beside him
when he said, you know, everything was fine,
don't worry about it, we don't need to close down,
we need to reopen, we need to get the economy
back on track, that's our main focus.
It's not really gonna be that bad.
Those who stood by him, those who reopened their states,
those who started that process before scientists
said it was advisable, well, who could have guessed it,
they're having to stop the reopenings,
they're having to scale things back.
In Florida and in Texas, this is happening.
Yeah, we can focus on the political aspects of this
and how a whole bunch of people are now going to show up
to vote against Governor DeSantis because of this
and therefore they'll end up voting against Trump.
Yeah, that's worth noting, but there's a much bigger
teachable moment here.
What happened?
Scientists came out and they said,
hey, this is gonna happen, this is what the models say,
this is what we have to do to fix it, these are our options.
Those with the money in the US,
those with the economic power, they didn't like
the suggestions of the scientists,
so they created this counter-narrative.
And they had the politicians who they greatly influence
support that counter-narrative.
And then a bunch of people playing partisan politics,
a bunch of people who wanted to play their part
in the culture war, well, they signed on to it
and they agreed with it and they started promoting
this counter-narrative.
And then, big surprise, the scientists were right.
So now we have to roll back all the reopenings.
Let's apply that to something else.
You have an overwhelming consensus of scientists
saying, hey, this is the projection,
this is what's gonna happen, it's really dangerous.
We have to make changes, we need to reduce our carbon output,
we have a whole bunch of stuff we need to do
to save the entire planet.
And what happened?
A whole bunch of people with money,
those who were threatened by this advice,
they created a counter-narrative.
And those politicians who they greatly influence,
they promoted it.
And people who wanted to play their part in the culture war,
they promoted it.
So it took hold, this anti-science narrative.
The difference between the two scenarios
when you're talking about us having to reclose now
and climate change is that if it gets too far with climate
change, there's no turning it off.
We have to make the changes now.
This was a great moment.
It showed exactly what's happening with that debate.
And it showed the outcome, the difference
is if climate change gets way out of control
and becomes an extinction level event, which it can,
a governor's not gonna be able to sign a piece of paper
and roll things back, it's done.
We've got to get rid of these politicians,
these politicians who are supporting moneyed interests
over your life.
You've seen it now, you've seen it happen.
This same thing is happening with climate change.
We have to address it.
We don't have a choice.
You can continue to play your part in the culture war,
but it's gonna turn out the same way.
We have one planet and the governors and the presidents
and the leaders of the world will not be able to do anything
once it gets past a certain point.
We have to stop listening to the people who would put profits
over people's lives.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}