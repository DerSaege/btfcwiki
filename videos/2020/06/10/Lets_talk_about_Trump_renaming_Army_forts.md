---
title: Let's talk about Trump renaming Army forts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5bc2oAl93oE) |
| Published | 2020/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the issue of renaming army installations named after Confederate Civil War generals, which has been a long-standing topic but is now at the forefront.
- The Trump administration displayed a lack of courage by refusing to make the changes, opting to delay the decision for someone else to handle.
- Active duty vets Beau spoke with showed unanimous agreement that the installations should be renamed, with some debate on how they should be renamed.
- Beau suggests renaming Fort Bragg to Fort Charles Beckwith, Fort Gordon to Fort Alvin York, Fort Binning to Fort Robert Rogers, and Fort Campbell to honor the first division commander of the 101st, not the Confederate general.
- Beau criticizes the Trump administration for missing an opportunity to show progress and move forward by renaming the installations, instead leaving soldiers stationed at bases named after those who fought to dehumanize them.
- He argues that renaming the installations with more fitting names can give soldiers a sense of pride and accurately represent the modern nature of the US Army.

### Quotes

- "They're going to kick that can down the road, let somebody else deal with it, let somebody else make the changes."
- "We can move forward. We can give a large portion of soldiers an installation name they can be proud of instead of one they don't want to talk about."
- "These guys, the names I gave, these would not be surrendering to the PC crowd."
- "The absolute cowardice of the Trump administration is leaving a large number of those serving in the United States Army stationed at installations named after people who fought to keep them seen as less than human."
- "That's pretty much peak 2020 right there."

### Oneliner

Renaming army installations named after Confederate generals is long overdue, with unanimous support from active duty vets, missed by the Trump administration's lack of courage, leaving soldiers stationed at bases named after oppressors.

### Audience

Soldiers, Activists, Politicians

### On-the-ground actions from transcript

- Rename army installations with names that accurately represent the modern nature of the US Army (suggested).
- Advocate for renaming army installations named after Confederate generals to honor more fitting individuals (implied).

### Whats missing in summary

The emotional impact and detailed reasoning behind each proposed new name for the army installations.

### Tags

#RenamingArmyInstallations #ConfederateGenerals #TrumpAdministration #SystemicRacism #Modernization


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about renaming army installations.
It's a topic, it's been a topic for a long time, but now it's in the forefront.
It was being considered.
The Trump administration has, per usual, displayed a complete, total, and utter lack of courage
and is refusing to do so.
They're going to kick that can down the road, let somebody else deal with it, let somebody
else make the changes, because these changes will eventually be made.
If you are unaware, there are a number of US Army forts that are named after Confederate
Civil War generals.
It's been a sore spot for a while.
I actually talked to some active duty vets today about it.
For the first time, there was no disagreement.
Everybody was okay with the idea of them being renamed.
Now granted, admittedly, my circle is a little different than most, but no disagreement whatsoever.
There was a little debate over how they should be renamed.
I'd like to provide my own suggestions for that.
Fort Bragg.
How about Fort Charles Beckwith?
Fitting isn't it?
Is there anybody that believes that some Confederate general is more deserving of that honor than
Charles Beckwith?
I don't think so.
Not really.
Fort Gordon could become Fort Alvin York.
What's a more American story than that?
The guy who didn't want to fight, who came home a hero.
Cyber Command's there, that's a lot of PSYOPs.
Maybe they could do something involving a turkey call or something.
That's only going to be funny for like five people.
Fort Binning.
How about Fort Robert Rogers?
The 75th is there, it's a training installation, everybody needs to know the standing orders.
It's fitting and I know some people are going to say, well he was actually a colonial, he
was British really.
Well they were Confederates, not Americans, so that shouldn't really factor into it.
Fort Campbell.
We can name that one after Lee.
No not that Lee.
The first division commander of the 101st.
The father of Airborne.
The guy who got them ready for D-Day.
Harkened back to a time when the United States Army filled their canteens with the tears
of fascists rather than taking orders from one.
This could have been a very good thing for the Trump administration and for the United
States as a whole.
Could have showed that we were moving forward, that advancement was being made, that we were
changing and these changes will come, but we could have shown it now.
Instead, the absolute cowardice of the Trump administration is leaving a large number of
those serving in the United States Army stationed at installations named after people who fought
to keep them seen as less than human, while we deny that systemic racism exists.
That's pretty much peak 2020 right there.
These guys, the names I gave, these would not be surrendering to the PC crowd.
They all have their own issues, but they're a more accurate reflection of the US Army.
They more accurately reflect the modern nature of it.
We can move forward.
We can give a large portion of soldiers an installation name they can be proud of instead
of one they don't want to talk about.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}