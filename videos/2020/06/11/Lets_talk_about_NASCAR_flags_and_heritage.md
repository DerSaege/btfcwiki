---
title: Let's talk about NASCAR, flags, and heritage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RfcfoZFDKZU) |
| Published | 2020/06/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASCAR took a moral stand by banning Confederate flags at their events, showing more courage than the President of the United States.
- The President dodged addressing the issue involving the names of some U.S. Army installations, opting to pass it on to someone else.
- NASCAR has a history of striving to be inclusive as an organization, in contrast to some of its fan base.
- The ban on Confederate flags at NASCAR events has stirred controversy, particularly in the South.
- NASCAR's roots in hot-rodding and bootlegging tie into a historical defiance against unjust authority, including against taxes and law enforcement.
- NASCAR's essence has always been about defunding the police and resisting unjust systems.
- People criticizing NASCAR's stance on police accountability fail to recognize the organization's heritage and lineage.
- Organizations like NASCAR are no longer pandering to certain audiences and are pushing for growth and change.
- Failure to evolve over time and adapt to changing norms indicates a lack of personal growth and development.
- Self-reflection is necessary for personal and societal growth, especially when faced with changing ideologies and values.

### Quotes

- "NASCAR took a moral stand by banning Confederate flags at their events, showing more courage than the President of the United States."
- "Failure to evolve over time and adapt to changing norms indicates a lack of personal growth and development."
- "If all of your idols, if all of the organizations you like are suddenly turning against you, maybe it's because you didn't keep up with the times."

### Oneliner

NASCAR's bold stance on Confederate flags and police accountability exposes the need for growth and reflection in the face of evolving values.

### Audience

Fans of NASCAR and advocates for social change.

### On-the-ground actions from transcript

- Support organizations and companies that take a stand against injustice, like NASCAR (exemplified).
- Engage in self-reflection and personal growth to adapt to changing values and ideologies (implied).

### Whats missing in summary

The full transcript expands on the historical roots of NASCAR, the reactions to their stance on Confederate flags and police accountability, and the importance of evolving with the times to foster personal and societal growth.

### Tags

#NASCAR #ConfederateFlags #PoliceAccountability #Growth #SocialChange


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about NASCAR.
I know, right?
I'm not actually joking, we really are.
Because today, I guess yesterday, NASCAR showed more courage than the President of the United
States.
NASCAR saw an opportunity to take a moral stand, and they took it.
The President today kicked the can down the road, refused to address an issue involving
the United States Army and the names of some of its installations, and he just chickened
out.
Refused to do it.
Kicked the can down the road, let somebody else do it.
Fair enough, fine, whatever.
NASCAR didn't do that.
Now for those that don't know, NASCAR as an organization actually has a long history of
trying to be inclusive.
They really do.
The fan base not so much, but the organization, they do.
So it's no longer permitted to bring a Confederate flag to a NASCAR event.
They're banned.
Don't bring them.
Keep them at home.
So of course this has some people upset.
There are some in the South who were talking about how it wasn't justified to do certain
things and they look like they're about ready to burn so much stuff it's going to look like
Sherman came back.
But the big complaint is that they're giving in to this movement, this movement to hold
police accountable.
They give in to it as if it's not actually part of NASCAR's heritage, their lineage.
How did NASCAR come about?
Why did people start hot-rodding, souping up cars?
Shine.
Shine.
Now for the T-men out there, that's water, don't come raid me.
T-men, huh?
Treasury agents, revenuers, taxes.
They souped up their cars so they could sell untaxed booze, so they could outrun them.
Now part of that was because they didn't want to pay the taxes.
The other part of that was they didn't want the money going to places they didn't approve
of.
Go ahead and make the connection.
It's right there.
From the very beginning, because NASCAR is a descendant of hot-rodding which came from
shining, it's always been defund the police.
It's what it was about.
It's about not handing over your money to something that's unjust.
They didn't give in to it, they always were it.
For people who always talk about their heritage, really don't seem to know much about history.
At the end of the day, people are going to be mad for a little bit, and they'll threaten
a boycott for a little bit.
It might be worth noting that all of these organizations are refusing to pander to this
base audience anymore.
They're done with it.
When NASCAR asked people not to bring them a few years ago, it set the stage.
At that point, people had a moment where they could have paused and reflected.
Maybe the problem isn't that people aren't sticking to their guns.
Maybe the problem is that there are people that haven't changed in 10 years.
That's not consistency.
That's a lack of growth.
If you look back on the person you were 10 years ago, and you can't see a marked difference,
you should cringe.
That should be a painful moment for you, because you're not advancing as a person.
You're not growing.
And that's pretty sad, because in order for the country to grow, you have to grow.
In order for the species to grow, you have to grow.
This might be a good time for some self-reflection.
If all of your idols, if all of the organizations you like are suddenly turning against you,
maybe it's because you didn't keep up with the times.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}