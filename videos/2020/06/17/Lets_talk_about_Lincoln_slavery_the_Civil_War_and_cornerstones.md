---
title: Let's talk about Lincoln, slavery, the Civil War, and cornerstones....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=r_34p9UwjZ4) |
| Published | 2020/06/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Historians sometimes assume Americans know American history, but most know American mythology.
- The Emancipation Proclamation freed slaves only in rebellious areas, not ending slavery nationwide.
- The theory suggests the Proclamation was a political maneuver to deter Europe from supporting the South.
- Southern states seceded and opened fire on Fort Sumter due to their strong belief in slavery.
- Confederate leaders openly stated that slavery was the cornerstone of their founding documents.
- The Vice President of the Confederacy emphasized the belief in white supremacy and slavery's necessity.
- The Civil War was inherently about slavery, as evidenced by Confederate documents.
- Some still argue the war wasn't about slavery, but historical facts from the Confederate side prove otherwise.

### Quotes

- "Most Americans know American mythology, not American history."
- "The Civil War was about slavery. You can't argue this."
- "The confederates admit that's what it was about."

### Oneliner

Beau breaks down the myth surrounding the Civil War, affirming its true cause: slavery.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Educate others on the true history of the Civil War by sharing this information (implied).

### Whats missing in summary

The full video provides a comprehensive breakdown of the misconception surrounding the true cause of the Civil War and the importance of historical context.


## Transcript
Well howdy there internet people, it's Beau again.
So this is going to be another one of those videos in the series about history that you
should know, but you might not.
This is something that gets debated pretty often, and it's funny because there's no debate
on this one.
The historical evidence is pretty heavy.
There's a metric ton of historical documentation deciding this little debate.
It's funny because it is historians who are to blame for this debate.
Historians tend to assume that Americans know American history.
It's not really the case.
Most Americans know American mythology.
The two aren't always the same.
So when a historian gets a hot take, gets a piece of information they think will spark
interest in history, they put it out there and they don't always put it in context.
And that's kind of what happened here.
The theory that got advanced was that, well, the Emancipation Proclamation wasn't really
about ending slavery.
And that's true.
There's a lot of truth in that statement.
In fact, in the U.S., slavery still isn't abolished.
Not really, but that's a topic for another video.
The Emancipation Proclamation freed slaves within a defined geographic area, the areas
that were still rebelling.
That's it.
That's what it did.
And there is the theory, one that I personally believe, that there was a political maneuver
going on at that time because a lot of Europe was considering entering the war on the side
of the South.
However, once freeing the slaves became an explicit goal of the Union, they're openly
saying we're going to do this.
Well, Europe didn't want to get involved anymore because nobody wants to be on that side of
history, you know.
So this idea that the Emancipation Proclamation wasn't really about ending slavery gave rise
to the idea that Lincoln didn't start the war to end slavery, therefore the American
Civil War was not about slavery.
You're asking the wrong person.
Lincoln's view of why the war happened or didn't happen, his motivations are irrelevant.
The South saw it.
Why did the South secede?
Why did the South Carolina militia open up on Fort Sumpter?
The Confederate Army didn't exist yet.
Those are the real questions.
And we have answers to them because the Confederate leaders believed they were founding a new
country that was going to last a long time, therefore there are founding documents.
Many states issued statements of reasons, and in those statements of reasons, many of
them included the word slavery, most times in the first paragraph.
Texas went all in on it.
Not just was it important economically, God wanted it that way.
Mississippi said that because it was so hot, they had to have black people as slaves because
nobody else could handle it.
I am not joking.
They put that in what was supposed to be the founding documents of a country.
A lot of other states did.
Some were a little more cryptic about it, but not much.
You know, Georgia talked about how criminals weren't being returned to them from the northern
states, criminals being runaway slaves.
When they talked about the other states that they were forming a confederacy with, they
said other slave-holding states because slavery was the thing uniting them.
That's what it was all about.
There's no denying this.
That's why it happened.
And if that doesn't do it for you, we also have the statement from the vice president
of the confederacy.
He talked about how the original founding fathers of the US, they believed that slavery
would just go away on its own eventually.
They're probably right.
On a long enough timeline, machinery would have been more cost-effective.
So yeah, there's probably some truth to what they believed, but that's not what happened.
He said, those ideas, however, were fundamentally wrong.
They rested upon the assumption of the equality of the races.
This was an error.
It was a sandy foundation and the idea of a government built upon it.
When the storm came and the wind blew, it fell.
Our new government is founded upon exactly the opposite ideas.
Its foundations are laid.
Its cornerstone rests upon the great truth that the black man is not equal to the white
man.
That slavery, subordination to the superior race, is his natural and normal condition.
He did not say black man in this.
He used a different word.
That seems pretty clear.
The cornerstone of the confederacy, the reason they did this, the immediate cause, was slavery.
The Civil War was about slavery.
You can't argue this.
The confederate side, their documents say it was about slavery.
People like to hold onto this because they like the mythology of the war of northern
aggression and all of that.
And even now, I'm sure somebody is typing out that, well, you know, they told the people
at Fort Sumter to leave.
They didn't want there to be any, you know, fighting.
Of course not.
They would prefer to keep their slaves without fighting.
I find it hard to believe that the people who support the idea that this wasn't over
slavery, I find it hard to believe they would justify the US military abandoning a post
today.
They're calling for troops to be sent in to Seattle over a few square blocks.
And we're supposed to believe that they would be okay with half the country leaving, being
occupied.
Nobody's buying that.
The Civil War was about slavery.
It was inherently racist.
It is what it is.
It's historical facts coming from the confederate side.
You don't have to look to the Union.
You don't have to get the northern side of it.
The confederates admit that's what it was about.
So Lincoln's little political maneuvering doesn't really matter.
It's not really that important.
You're trying to expand that little piece of information just a wee bit too far.
Anyway, it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}