---
title: Let's talk about Officer Stacey and McMuffins....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Tc8YwzrKzuw) |
| Published | 2020/06/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses his anxiety while waiting in the fast food line after being asked to pull forward, suspecting that the employees think he is a cop.
- He draws a comparison between his anxiety and the fear that black Americans experience when getting pulled over by the police.
- Beau questions the anxiety level of an officer waiting for an Egg McMuffin at a fast food joint, suggesting that such anxiety may not be suitable for being armed on the streets.
- He advocates for officers with high anxiety levels to take a leave of absence to avoid potentially tragic consequences due to irrational fears.
- Beau points out the negative bias law enforcement has faced recently and compares it to the historical bias faced by black Americans for centuries.
- He underscores the importance of this moment as a wake-up call for law enforcement to address these biases and anxieties.
- Beau mentions the impact of being stereotyped based on appearance and uniform, acknowledging that some officers may be unfairly judged.
- He concludes by encouraging law enforcement to recognize the deeper implications of bias and anxiety within their profession.

### Quotes

- "That anxiety level that that officer has, that may be an indication that she needs to take a leave of absence."
- "Imagine if that negative bias had existed for hundreds of years."
- "This should be a wake-up moment for law enforcement."
- "But to be honest, most of America right now is just like, well, I mean, it's really bad that you fit the description."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Beau expresses anxiety in a fast food line, drawing parallels between his experience and the fears of black Americans when faced with law enforcement bias.

### Audience

Law enforcement officers

### On-the-ground actions from transcript

- Take a leave of absence to address high anxiety levels (suggested)
- Recognize and address biases within law enforcement (suggested)

### Whats missing in summary

The full transcript provides a deeper exploration of anxiety, bias, and the need for introspection within law enforcement.

### Tags

#Anxiety #Bias #LawEnforcement #CommunityPolicing #RacialJustice


## Transcript
Well howdy there internet people, it's Bo again.
So here we are in the fast food line.
I gotta be honest, I'm a little worried right now.
They asked me to pull forward.
I'm pretty sure that they think I'm a cop.
So I'm worried, my anxiety level has been raised.
I feel like that officer that posted that video earlier,
because they did, they took my money
and asked me to pull forward, and that's never happened.
That's not something that happens just because they're busy.
They must be up to no good.
They must be up to something nefarious.
They must be out to get me, because of how I look.
I imagine the fear that I am feeling right now
is a fraction of what black Americans feel
when they get pulled over, and they
have to sit there and wait while their tags get
ran before they get to find out whether or not
the cop that walks up to the window
is going to be somebody who fears for their life.
That anxiety level that that officer has,
that may be an indication that she needs
to take a leave of absence.
If you have that much anxiety over waiting in line
at a fast food joint for an Egg McMuffin,
perhaps you shouldn't be on the streets armed.
I feel like that level of anxiety, that level of fear,
may cause someone to fear for their life in a situation
where they really shouldn't, where it's not
a rational fear, and that may lead to tragic consequences.
It's probably time to take a leave of absence,
to get off the street, to put that gun away.
You don't want to hurt anybody.
For all I know, this officer may be one of the few good apples.
And yeah, if that's the case, I imagine
it's pretty bad to be subjected to a stereotype based
on how you look, based on your uniform.
But I want to point out that law enforcement has been subjected
to this negative bias for a couple of weeks,
and you have officers and those associated
with law enforcement posting videos of themselves crying,
or videos of full-blown meltdowns to social media.
Imagine if that negative bias had
existed for hundreds of years.
Imagine if that negative bias wasn't due to a uniform
that you could take off whenever you wanted to,
but was due to your skin tone.
This should be a wake-up moment for law enforcement,
because at the anxiety level, yeah, I'm sure it's real.
I'm sure there are people who are genuinely nervous right now,
who are genuinely upset, who are genuinely worried.
But to be honest, most of America right now
is just like, well, I mean, it's really bad
that you fit the description.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}