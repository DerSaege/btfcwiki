---
title: Let's talk about bias in today's discussions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LVlE0fu7E54) |
| Published | 2020/06/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains that the channel is interactive based on viewer comments and ethical considerations.
- Points out the misuse of statistics from the FBI's Uniform Crime Reporting Program.
- Clarifies that the statistics show demographics suspected or arrested, not actual criminals.
- Addresses the misconception of linking demographics to predisposition for crime as racist.
- Suggests adjusting data for poverty levels to reveal racial disparities disappear.
- Emphasizes that facts aren't racist, but people can be.
- Talks about the debate on the lethality of tasers and the need for consistency in terminology.
- Argues against justifying lethal force based on vague "what-if" scenarios.
- Warns against granting officers the power for summary execution based on unreasonable scenarios.
- Encourages self-examination of beliefs, morals, and biases in light of current events.

### Quotes

- "If you believe somebody's skin tone will increase their likelihood of being a criminal, that's racist."
- "Facts aren't racist. People are."
- "The device does not become more lethal simply because the scary black man is holding it."
- "We don't want facts to get in the way of a good story."
- "Now is the time to see where you really fit in. What your moral fiber is. What your biases are."

### Oneliner

Beau explains the misuse of statistics to establish causal relationships between demographics and crime, and calls for consistency in evaluating police actions and examining personal biases.

### Audience

Viewers, Activists, Educators

### On-the-ground actions from transcript

- Examine your beliefs and biases (suggested)
- Advocate for consistent terminology in evaluating police actions (implied)
- Educate others on statistics and biases (implied)

### Whats missing in summary

In-depth examples and explanations on statistical misuse and racial biases in crime analysis.

### Tags

#Statistics #Bias #Racism #PoliceReform #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about statistics, and confirmation bias, and bias in general,
and expectations, and spurious correlations, and causal effects, a whole bunch of stuff
that does not sound interesting, but trust me it will be.
We're going to do this because there's a whole bunch of people who are new subscribers who
do not understand how interactive this channel is.
This is basically a choose your own story book on YouTube.
If something shows up in the comments section enough, rest assured we're going to talk about
it, especially if it's something I have an ethical or moral qualm with and don't really
want down there.
There is a certain group of statistics that keeps getting linked, and the people linking
it are attempting to show a causal relationship between two things.
The problem is that's not what those statistics show.
It's not what they were designed to show.
It's not even the information contained in them.
However, the people who are linking them don't know that.
So we're going to go through it.
Now those stats come from the Uniform Crime Reporting Program of the FBI.
Now what these stats are is they show a type of crime, and then the demographic that's
most likely to commit those crimes, right?
No!
That's not what's shown on those stats.
It shows a type of crime, and then the demographics that are most often suspected or arrested
in cleared cases.
We've talked about it before on this channel.
Law enforcement doesn't clear most cases.
It's actually a really small percentage.
In most categories, it's below 50%.
In some, it's in the teens.
So it's an incomplete data set to begin with, and then you add into that people don't know
what it's actually showing.
So what's most often linked is the demographic information on race.
Now we have millions of people in the streets right now saying that law enforcement has
a systemic issue with racism.
It shouldn't be a real big surprise that black people get arrested or suspected more.
That shouldn't come as a shock to anyone.
There's a whole bunch of people out there saying that.
But the idea behind linking this is to establish a causal relationship between skin tone and
a predisposition to committing a crime.
That's racist.
Period.
Full stop.
That's racist.
If you believe somebody's skin tone will increase their likelihood of being a criminal, that's
racist.
That's not what that information was even designed to show.
Now if you were to take the data set that's provided and get the arrest locations and
then adjust those for poverty levels and the level of income inequality in those areas,
what you will find out is that all of the racial disparities disappear.
Because facts aren't racist.
People are.
That isn't something... if you're attempting to establish that causal relationship that
a certain skin tone is more likely to be a criminal, take it somewhere else.
I don't want it down there.
That's not a real argument.
It's not rooted in fact.
It's rooted in your own biases.
Okay, the other thing people want to talk about is the whole taser thing.
It's non-lethal.
It's lethal.
It's less lethal.
Sure, whatever.
We'll go through them, but whatever it is, we're going to apply it across the board.
We're going to be consistent about it.
The taser does not become more lethal simply because a scary black man picked it up.
That's not how it works.
Whatever category you want to call it, we're going to call it that across the board.
Now less than lethal, less lethal, that's not a thing.
Less than lethal means it isn't lethal, so it's non-lethal.
Less lethal means it is still lethal, it's just not as lethal as something else.
The term doesn't really have a definition.
Let me show you how this works.
Let's say I'm holding a pipe and an AR, and I say the AR is less lethal.
You're going to look at me like I've lost it, rightfully so, because it doesn't make
sense.
However, if we were to base that definition on murder statistics, right?
Blunt objects, well, they get used more, so they're more lethal.
Without a definition for the term, it means nothing.
Nothing can either be reasonably expected to be non-lethal or reasonably be expected
to be lethal.
Those are the options.
Now if it's non-lethal, law enforcement had no reason to fire.
If it is lethal, all of the suspect's actions were justified, because please explain to
me what exactly he had done at the point where law enforcement used it to warrant lethal
force.
It's not there.
It is not there.
Now if you want to create a category and say it's less lethal, fine, do it.
But, apply it across the board.
The device does not become more lethal simply because the scary black man is holding it.
And what you will find out is either there was no justification, or the suspect was completely
justified in fleeing.
No justification to shoot, or the suspect was justified in fleeing.
If you apply whatever term you want to use consistently, that's what you'll find out.
You don't engage in mental gymnastics, that is what you will find out.
Now the other argument is, well, he may have taken his gun.
Sure, whatever.
Let's set aside the fact that he was fleeing and attempting to end the engagement.
We don't want facts to get in the way of a good story, so we'll just forget about that.
We'll just create this what-if scenario in which, yes, the person running, who was apparently
intoxicated, well, he turned, fired, hit the person, engaged the taser, it functioned,
incapacitated the officer, then the officer's partner decided to go get a hamburger at Wendy's
and the guy took his gun.
Sure, yeah, I guess that could happen.
I mean, that seems like really realistic.
The whole idea is that it has to be reasonable.
That's not reasonable.
That isn't a reasonable storyline.
I would be really cautious about granting officers the power to engage in summary execution
based on whatever what-if scenario they can come up with.
Because if you do that, they can shoot anybody at any time.
Most people have function of their hands.
Most people could take an officer's weapon.
Well he was going to punch me and take it, but he was running away.
Doesn't matter, had to shoot him.
Well he was standing by a rock that in theory he could have picked up and thrown and hit
me in the head, knocked me out and taken my gun.
We cannot use that for a basis for lethal force.
Would you do the same thing?
Would you engage in this same amount of mental gymnastics if it was a 14-year-old white girl?
Probably not.
Probably not.
Most of the defenses I have seen of this have been firmly rooted in racism.
And in case you can't pick it up, not welcome here.
Take it somewhere else.
It's not what this channel is about.
It's about moving forward.
Not fostering ideas from the 1960s or 1930s or 1860s.
I don't want it here.
Now is the time to examine your own beliefs.
Now is the time to see where you really fit in.
What your moral fiber is.
What your biases are.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}