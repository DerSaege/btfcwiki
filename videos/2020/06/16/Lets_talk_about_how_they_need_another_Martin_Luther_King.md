---
title: Let's talk about how they need another Martin Luther King....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xjRg7qqeEno) |
| Published | 2020/06/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- People keep calling for another Martin Luther King, but he argues that it's not about needing a leader like MLK, but rather people who look like him taking responsibility to bring about change.
- Acknowledges that some may be speaking out for social credit, but points out that speaking out is often a vocation of agony and a necessary action.
- Notes the urgency within the black community who believe that white America determines how long change will take, leading to continual protests.
- Beau points out that riots don't happen out of thin air and stem from certain societal conditions that need to be condemned just as vigorously as the riots themselves.
- Urges people to listen to what is being said and understand that riots are often the result of being unheard.
- Emphasizes that Martin Luther King preached nonviolence, not non-confrontation, citing his work in Letters from Birmingham Jail as an example of his willingness to challenge the system.
- Compares current protests to historical lunch counter protests, indicating that the struggle for equality continues and those staying silent are on the wrong side of history.

### Quotes

- "Because in the final analysis, riots are the language of the unheard."
- "If you don't like what I just said, you don't really want another Martin Luther King."
- "There comes a time when silence is betrayal."

### Oneliner

People calling for another Martin Luther King misunderstand the need for personal responsibility in creating change and the urgency within marginalized communities.

### Audience

Activists, Allies, Community Members

### On-the-ground actions from transcript

- Join protests and demonstrations (implied)
- Speak out against societal conditions that lead to unrest (implied)
- Support marginalized voices and communities (implied)

### Whats missing in summary

The emotional depth and nuanced perspective offered by Beau in discussing the current state of protests and social movements.

### Tags

#SocialJustice #CivilRights #CommunityAction #Activism #Responsibility


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a
common theme, something that's continuing to be said
and a question I got asked.
Now the thing that I keep hearing is they
need another Martin Luther King.
They need a leader like Martin Luther King.
They need somebody who's going to make the speeches
and hold hands in the streets and do all of those wonderful things that we learned
about in grade school.
And that is what's going to bring about real change.
Okay I've talked about it in other videos.
They don't need a leader.
We do.
People who look like me need a leader.
It's our mess.
We've got to clean it up.
But we're going to come back to this idea that they need another Martin Luther King.
Now the question I got asked was don't you think that some of the people who are speaking
out right now, don't you think they're just doing it for social cred, they're trying to
get points, you know, they're just pandering.
I'll acknowledge that on some level there are probably some people who are doing that.
As long as the people who think they want another Martin Luther King acknowledge that
a calling to speak is often a vocation of agony.
But we must speak, right?
Because the mood of the black community is one of urgency.
They're saying we waited too long and that we're going to have this kind of vigorous
protest every summer and that it's going to be white America who determines how long it's
going to take.
And because I have a small platform here, I have people asking me to condemn certain
things.
I want to point out that riots don't develop out of thin air.
Certain conditions continue to exist in our society that must be condemned as vigorously
as you want me to condemn a riot.
Because in the final analysis, riots are the language of the unheard.
So what are you not hearing?
Now if you don't like what I just said, you don't really want another Martin Luther King.
Because pretty much all of that's a quote, a couple quotes.
You got to get that grade school image of him out of your head.
Yeah he had a dream and he preached nonviolence.
He didn't preach non-confrontation.
It's a very different thing.
You know, if you ask me, his best work was something called Letters from Birmingham Jail.
Guess where he wrote it?
He wasn't somebody who was afraid to buck the system.
He preached nonviolence, not non-confrontation.
And I know there's people right now saying, but they're occupying all these places and
they're shutting down the streets.
We can't get anything done.
Yeah, it's a great big lunch counter protest.
Same principle.
It's the same principle.
Only it's not the middle of the 1900s.
It's 2020.
It's not in small rural towns in the South.
It's all over the country in major cities.
It's got to be bigger because they can't get heard.
They're unheard.
They're trying to raise their voice and that's how they think they have to do it.
Now with that frame of mind, with viewing it as a lunch counter protest, there was probably
a photo in your history book.
Who do you want to be?
Do you want to be the person sitting next to them at the counter?
Or do you want to be the person smashing an egg on their head?
Because this is the same fight.
It's the same struggle.
And if you're not lending your voice to them right now, if you're silent, you're on the
wrong side of history.
Yeah, I will acknowledge that there are probably people out there just talking about it for
social cred.
But I know there's a lot of people out there talking about it because there comes a time
when silence is betrayal.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}