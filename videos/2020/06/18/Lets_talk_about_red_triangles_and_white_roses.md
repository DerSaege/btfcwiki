---
title: Let's talk about red triangles and white roses....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U2C8hUE7zVM) |
| Published | 2020/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States ran 88 Facebook ads featuring a red triangle to identify his opposition.
- The red triangle historically was reserved for those who spoke out against the administration in Germany and rescued Jews.
- Trump wants to identify his enemies using the red triangle, drawing parallels to a dark historical context.
- Beau acknowledges taking inspiration from Sophie Scholl and the White Rose Society in meeting people where they're at.
- Trump seeks undying loyalty like certain supporters of the German regime during the war.
- Despite Trump's efforts to draw lineage, there is a fundamental difference in effectiveness between him and the German dictator.
- Trump's failures are evident in his inability to deliver on promises and manage various aspects of governance.
- Beau stresses the importance of pointing out Trump's failures repeatedly to reach his remaining supporters.
- Trump's appeal to uneducated voters who prioritize winning over moral character is a key aspect of his strategy.
- The need to continuously label Trump as a failure and point out his shortcomings to make his true nature clear.
- Beau advocates for meeting people where they're at, like Sophie Scholl, to effectively communicate with those who support Trump.
- Emphasizing Trump's incompetence and inability to fulfill promises is a critical strategy in addressing his supporters.
- The significance of Facebook removing the ads featuring the red triangle, exposing Trump's failed attempt at a dog whistle.
- Beau underscores the importance of consistently showcasing Trump's shortcomings and failures to counter his appeal to certain supporters.
- Encouraging the audience to be resilient in addressing Trump's inadequacies and communicating effectively with his supporters.

### Quotes

- "He wanted to make America great again. The country's on fire."
- "He's a loser. He's always been a loser and he's always going to be a loser."
- "He can't even racist right."
- "Be like Sophie."
- "It's just a thought. Y'all have a good night."

### Oneliner

President Trump's use of a red triangle reveals historical parallels, urging us to address his failures and meet his supporters where they're at to combat his divisive tactics.

### Audience

Political activists, concerned citizens

### On-the-ground actions from transcript

- Point out Trump's failures and shortcomings to his supporters (implied)
- Advocate for meeting people where they're at in political discourse (exemplified)
- Communicate effectively with those who support Trump by addressing his failures (implied)

### Whats missing in summary

The emotional intensity and depth of historical context captured in Beau's message may be best experienced in its entirety.

### Tags

#Trump #SophieScholl #WhiteRoseSociety #History #PoliticalActivism


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about red triangles and white roses.
They're meaning,
meeting people where they're at.
If you don't know,
the campaign
of the President of the United States
ran eighty-eight
Facebook ads.
A specific number
featuring a very specific red triangle
that was used to identify his opposition, those opposed
to the Trump administration.
The first sentence
of these posts,
if you
count up the words you're going to get a specific number.
And that's also
a pretty specific
number with some meaning to a certain group of people.
History buffs, we know this. We look at this and we're outraged.
We're outraged. We can't believe that this is happening
because that red triangle,
well it was reserved for certain kinds of people in history,
in Germany.
Those who spoke out against
that administration,
those who rescued Jews,
that's who got the honor
of wearing one of those.
And that's how Trump wants to identify his enemies,
his opposition.
Yeah, you can be outraged, but it's not going to do any good.
Anybody who has not recognized
what the President is by this point in the game,
they're not going to.
Pointing out that he's a racist is not going to matter. They know he's a racist.
That's not news.
Pointing out that he is very overtly
trying to draw lineage
from one of the most notorious figures of the twentieth century
isn't going to matter.
You know, when people speak positively
about this channel and the way I structure arguments,
they say that, you know,
I try to meet people where they're at rather than where I want them to be, and I
structure the argument so hopefully it will
resonate on one of several levels,
address the practical, the moral, you know,
and go through it.
I always feel guilty because I ripped all of that off.
I stole it.
Straight up, just highway robbery, all right?
Took it from one of my heroes, a woman named Sophie Scholl and her crew, the
White Rose Society,
who incidentally
would have been a group who had the honor
of wearing one of those triangles.
See, what we have to remember
about that German regime,
that short little German dude with the mustache,
is that even at the end of the war
he still had supporters,
still had people who wanted to, quote,
be good Germans.
That's what Trump wants. He wants that undying loyalty. We see it all the time.
But see, there's a big difference
between that German guy and Trump.
And as much as Trump wants to draw his lineage
from him,
you know, there's a
big difference that can be exploited.
See, that German dude,
he was effective at delivering on his promises.
Trump's not.
Trump's a failure.
He wanted to make America great again.
The country's on fire.
Well, it's going to all go away. It'll be like a miracle. We're entering the second wave.
The economy's going to be fantastic.
Guess not. And we need to remember to point out
that the recession started before the lockdowns.
The man can't even build a fence.
My fourteen-year-old can build a fence.
Can't even handle a simple construction project.
He's a loser.
He's always been a loser and he's always going to be a loser.
See, his remaining supporters,
they're never going to catch the subtle dog whistles in the sense of
being able to
recognize him for what they are. They're never going to see him
as
as that parallel.
They're never going to recognize what he wants to be. They're not going to ever take the time
to research and realize
that he has checked off all fourteen of the characteristics.
They're never going to see that
because they don't want to.
And because he specifically chose
those supporters.
I love uneducated voters.
Of course he does.
They're the only ones that won't see through him.
They care about winning.
They don't care that he's a racist. They know he's a racist.
That appeals to them.
They don't care about what good he does for them.
They care about how much he hurts those he doesn't like.
We have to point out that he's a loser.
Over and over and over again.
Because those that haven't woken up to what he wants to be yet, they're not going to.
We have to meet them where they're at.
We need to be like Sophie.
And meet them where they're at.
Yeah, you can be outraged. I am.
It's upsetting.
But that's not the story.
The story
is that he tried to do a dog whistle and got caught.
Facebook removed the ads.
Once again, he's a loser. He can't even racist right.
That needs to be the point
that gets hammered home.
Because those who don't
don't recognize him for what he is now,
they're not going to.
Even at the end of the war,
the German guy
had supporters.
We've got to remember that.
We have to meet them where they're at.
We have to address them in terms they will understand. Fine, we can't meet them on the moral level,
let's hit it on the practical level.
He's a failure.
And he'll always be a failure.
He doesn't have support of the military.
He's not good at defending the second amendment. All of the things that they say they care about,
he's an utter failure at.
He's a loser.
And he'll always be a loser.
Be like Sophie.
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}