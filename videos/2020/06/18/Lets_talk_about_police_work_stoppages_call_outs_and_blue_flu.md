---
title: Let's talk about police work stoppages, call outs, and "blue flu"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S2D85BrFGpw) |
| Published | 2020/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of "blue flu," where cops participate in work stoppages or slowdowns to draw attention to perceived injustices.
- Mentions the recent incident in Atlanta where the blue flu occurred after a cop was arrested, citing it as the perceived injustice.
- Criticizes the mindset behind the blue flu, which puts communities at risk to achieve law enforcement's demands.
- Comments on the flawed criminal justice system and how the blue flu is a misguided show of solidarity among officers.
- Notes that the actions of law enforcement during the blue flu inadvertently support the complaints of the Black Lives Matter movement regarding the broken criminal justice system and lack of care for communities.
- Points out that previous instances of work stoppages or slowdowns by law enforcement have not led to the chaos they predicted, as most people are not in need of excessive policing.
- Suggests that the blue flu incident in Atlanta proves that certain law enforcement positions may not be necessary and could be candidates for defunding.
- Encourages departments where such actions are considered to proceed, as it may help identify redundant positions within law enforcement.
- Advocates for community involvement in identifying unnecessary law enforcement positions through platforms like Facebook groups.
- Concludes by critiquing law enforcement's focus on self-preservation rather than truly serving the community.

### Quotes

- "When the city descends into chaos, let it burn."
- "You just proved everything that BLM is saying is true."
- "Those positions can be defunded."
- "It is still about protect and serve, but you just want to protect and serve yourselves."

### Oneliner

Law enforcement's "blue flu" actions inadvertently support criticisms from the Black Lives Matter movement, revealing flaws in the criminal justice system and the need for potential defunding of certain law enforcement positions.

### Audience

Community members, activists, policymakers

### On-the-ground actions from transcript

- Identify unnecessary law enforcement positions for potential defunding (suggested)
- Engage in community-led efforts, like Facebook groups, to gather data on law enforcement numbers (suggested)

### Whats missing in summary

Deeper insights on the implications of law enforcement actions and the potential impact on ongoing calls for police reform.

### Tags

#LawEnforcement #BlueFlu #BlackLivesMatter #CommunityPolicing #Defunding #CriminalJustice


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're gonna talk about how things
sometimes don't have the effect
that you believe they're gonna have.
How it's probably not a good idea
to base a course of action, a strategy,
on your own propaganda.
And how there are two groups in the United States right now
who seem like they are just completely opposed
to each other.
But one group last night, intentionally or not,
proved everything that the other side is saying is true.
So we're gonna talk about blue flu sweeping the nation,
particularly in Atlanta right now.
If you don't know, this is where cops engage
in work stoppages or slowdowns, or they call out.
And they do this as a method of addressing
something that they perceive as a wrong.
They're trying to draw attention to something
that they believe in their minds is an injustice.
That's where it comes from, that's why they do it.
Now this happened in Atlanta hours
after the cop got arrested.
That's when it started, so that's the injustice.
We're just gonna dive into their mindset for a second.
The idea is that if they do this,
the city will descend into chaos.
And that the community that they're supposed to serve
will become victims of crime.
And therefore, they will just be overjoyed
when law enforcement returns to full strength.
This is basically the government's version of,
look at what you made me do.
So the idea is to put the community at risk
in order to get what they want.
They're doing this because the criminal justice system
is broke.
That's why they're doing it.
Their buddy got arrested and they think that that's wrong.
So the criminal justice system does not work.
And they're doing it as a show of solidarity
to back each other, even at the expense of innocent lives.
Those who they are hoping will become victims.
When the city descends into chaos, let it burn.
When the city descends into chaos, let it burn, quote.
That's the idea.
Okay.
What are the main complaints
from the Black Lives Matter movement?
That the criminal justice system is broke.
That law enforcement doesn't actually care
about the community.
They're not there to serve the community.
And that law enforcement will back each other
at the expense of innocent lives, particularly black ones.
Congratulations, officers.
You just proved everything that BLM is saying is true.
This wasn't really well thought out
because these little temper tantrums,
these little stunts have happened before.
That's why there's a cute name for it.
The cities never descend into chaos.
They never work.
These little work stoppages and slowdowns,
they don't actually produce the results you're hoping
they will because most people are good.
Most people don't actually need to be over-policed.
Most people don't need a gun pointed at their back
and they certainly don't need it fired.
The reality is the last time this happened
in any big numbers, the crime rates went down.
That's what happened.
People were able to breathe a little easier.
It didn't have the effect that you thought it was going to.
And that's what's going to happen here.
That's what always happens.
The propaganda you push isn't true
and you need to be aware of that
as you're walking into the negotiating
of reforming the entire culture of law enforcement.
You should be aware of that.
What you have told everybody isn't accurate,
but you've told everybody it for so long
that you believe it to be true.
Now, if you, now to be very clear, I'm not against this.
I'm not against the blue flu.
I'm not against these work stoppages and slowdowns.
I think they need to happen.
In any department where this seems like a good idea,
if the culture in your department is like,
yes, we're going to stop and hope that the city falls apart
and people become victims of crime, by all means do it.
Please, seriously, this isn't a joke.
Do it because what we're working towards now
is restructuring all of law enforcement,
defunding, right?
You've heard that.
What we learned last night in Atlanta
is that every cop who engaged in this slowdown,
every cop who didn't show up,
every cop who played little games on the radio,
all of this stuff, none of them are needed.
Those positions can be defunded.
The city didn't descend into chaos.
Even with the idea of pushing it out there as a story
and letting everybody know,
hey, the community's not defended.
The thin blue line isn't out there protecting everybody.
It really didn't go crazy, did it?
Not really.
So, yeah, I think this is a good idea.
Let us know exactly which positions aren't necessary.
That's going to be a really good place to start.
If you could, maybe do a little Facebook groups
and everybody joined to let us know
the numbers in each department.
That way, you know, we don't have to count them ourselves.
If you're going to do all of the work for us
add that to your list of stuff to do.
What these little stunts show
is that it is still about protect and serve,
but you just want to protect and serve yourselves.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}