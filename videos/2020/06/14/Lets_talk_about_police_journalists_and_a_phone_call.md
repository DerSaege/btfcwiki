---
title: Let's talk about police, journalists, and a phone call....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S6lxlgzvgpU) |
| Published | 2020/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the need for systemic reform and change in law enforcement culture.
- Describing the frustration of trying to explain the necessity for deep systemic change in law enforcement to others.
- Expressing anger and frustration when others try to excuse the flaws in law enforcement despite presenting statistics and evidence.
- Proposing to illustrate the need for systemic change by dissecting a specific case.
- Recalling various cases of unarmed individuals killed by police officers, struggling to identify a specific case mentioned in an article.
- Ending with the acknowledgment of numerous unarmed people killed by the police and suggesting a need for system change.

### Quotes

- "There are so many unarmed people killed by police, journalists can't even keep track."
- "We might want to take that as a sign that we need to change the system."
- "I think the easiest way to do it is to illustrate something that actually happened."

### Oneliner

Beau addresses the need for systemic reform in law enforcement, expressing frustration over the failure to understand the necessity for change, and recalling cases of unarmed individuals killed by the police.

### Audience

Activists, Advocates, Citizens

### On-the-ground actions from transcript

- Advocate for systemic reform in law enforcement (suggested)
- Educate others on the importance of systemic change in law enforcement (suggested)

### Whats missing in summary

The full transcript provides a deeper understanding of the challenges faced in advocating for systemic change in law enforcement and the emotional toll of trying to explain the necessity for reform.

### Tags

#SystemicReform #LawEnforcementCulture #PoliceViolence #UnarmedVictims #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
Tonight we're going to talk about the need
for systemic reform, systemic change.
I'm getting a lot of questions right now and they all boil down to pretty much the same thing.
You know, it's, hey, I'm trying to explain to my
friend,
co-worker, relative,
whoever,
that we need deep systemic change in the culture of law enforcement.
And, you know, I've given them the statistics, I've shown how often they're wrong
when they engage and they use force.
I've shown how many people a year it affects, I've shown all of this.
But they still don't seem to get it.
They still try to find some way to excuse it.
And it gets to the point where I'm just angry.
I understand it.
Makes sense.
I think the easiest way to do it
is
to illustrate something that actually happened.
So what we're going to do is we're going to take one case
and we're going to pick it apart
and demonstrate how if
law enforcement was more intent on public service
rather than
maintaining an image as a warrior cop
that things would have gone differently.
So what we're going to do tonight is we're going to talk about the case of...
It's 2 AM.
Hey, what was the name of
the person that...
I wrote the...
unarmed person that got shot.
I wrote the article about it.
I don't know.
You edited it.
Tamir Rice.
No, I remember that one.
That was the kid in the park.
John Crawford.
Mm-mm.
That was the guy in the Walmart with the BB gun
in an open carry state.
Daniel Shaver.
Which one was that?
The one where the cop challenged him to life or death, Simon Says.
Ugh, that was horrible.
That's a horrible video.
No, that's not it either.
Um...
No.
No.
They killed her?
No.
Mm-mm.
Keep going.
No.
No.
Which one was...
Oh.
Yeah.
No.
Mm-mm.
Mm-mm.
No.
Mm-mm.
Alton Sterling.
Yes.
Yes.
That's it.
That's it.
That's it.
Thank you.
Thank you.
Great.
Good night.
This video is based on a real conversation.
There are so many unarmed people killed by police, journalists can't even keep track.
We might want to take that as a sign that we need to change the system.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}