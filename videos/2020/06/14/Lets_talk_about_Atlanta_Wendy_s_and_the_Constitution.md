---
title: Let's talk about Atlanta, Wendy's, and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UUufUvy9LZ4) |
| Published | 2020/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Viral videos from 2018 on police tactics are still relevant due to the lack of change in training practices.
- Institutions in the US are reluctant to change, reform, or apply best practices in law enforcement.
- Law enforcement is trained to get the public to excuse, justify, or look away from their actions.
- Beau addresses the incident at Wendy's in Atlanta in relation to the US Constitution and due process.
- Due process is a fundamental concept enshrined in the Constitution, except in cases of immediate threat to life or bodily harm.
- Resistance alone does not warrant the use of lethal force by law enforcement according to Beau.
- Beau recounts the events at Wendy's where a man was shot by law enforcement after resisting arrest and running.
- The argument that law enforcement was justified in using lethal force because the suspect had a taser is challenged by Beau.
- Beau questions the justification of lethal force based on the presence of a non-lethal weapon being used against the suspect.
- Beau criticizes the idea that law enforcement officers can use lethal force without being held accountable, leading to authoritarianism.

### Quotes

- "Resistance alone is not enough for the use of lethal force."
- "If lethal force is justified, everything else is."
- "The premise that leads to authoritarianism."
- "There was no reason to do this."
- "Maybe all of these patriots should actually read the Constitution."

### Oneliner

Beau challenges the justification of lethal force by law enforcement and criticizes the lack of accountability, pointing out the dangers of authoritarianism and the importance of upholding due process for all. 

### Audience

Activists, Reformers, Advocates

### On-the-ground actions from transcript

- Read and understand the US Constitution (suggested)
- Advocate for accountability and transparency in law enforcement practices (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of a specific incident at Wendy's and its implications on law enforcement practices and the US Constitution, urging for accountability and reform.

### Tags

#PoliceTactics #DueProcess #Accountability #LethalForce #Reform


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about something that we talk about a lot.
You know I've had people congratulate me because over the weekend some videos from 2018 or
so went viral talking about police tactics.
They went viral because they're relevant.
The videos were made a couple years ago and they were discussing training that took place
a decade before and they're still relevant because that training still isn't being applied.
I would love it when, I'd love to see the day when those videos are not relevant anymore.
But they are, they are because there are institutions within the United States that are very reluctant
to change.
They don't want to change.
They don't want to be reformed.
They don't want to apply best practices.
What they do want to do, as far as training goes, is train the American people to excuse
and justify or just look the other way for anything that they do.
Get them to defend the indefensible.
And they've done a really good job about it.
They're experts at it at this point.
So tonight we're going to talk about Wendy's, Atlanta, and the Constitution.
You know, there's a premise that's very popular pretty much all over the world.
And the idea basically boils down to the concept that before you face punishment for something,
you are going to go through some formalized structure that is going to render judgment
and determine whether or not you should face punishment.
It's called due process.
It's enshrined in the U.S. Constitution.
The only times that it's okay to circumvent due process is if there's an immediate threat
of loss of life or great bodily injury to yourself or society at large.
So if you are one of those people who believe that any resistance to law enforcement warrants
the use of lethal force, you do not support the Constitution.
Take that We the People banner off of your social media.
You don't support it.
You don't care about it.
You've probably never read it.
To you, it's a bumper sticker just like any sports team.
It means nothing.
Resistance alone is not enough for the use of lethal force.
Sorry.
If you don't know what happened, a gentleman pulls into a Wendy's drive-thru, nods off,
falls asleep.
Law enforcement gets called.
Law enforcement responds.
They talk to him.
They believe him to be intoxicated from the video.
He appears to be.
He resists.
They deploy a taser.
Taser fails to subdue the suspect.
He takes the taser and he runs.
Cops shoot him.
Now there's an argument that's going around and it's super popular.
I want to make sure I'm real clear on this because I don't want it to appear to be a
straw man.
The argument is that because the suspect had that weapon, that taser, well then the cops
were justified because they had a reasonable fear of loss of life.
Just forget for a second that the taser is a non-lethal device.
Let's not let facts get in the way of a good narrative.
The idea is that law enforcement was justified because of that.
They had a reasonable fear because of that.
The trained officer with a belt full of tools and a partner there had a reasonable fear
because of the taser.
Fine.
Where did the suspect get the taser?
It was being used against him.
Lethal force is the end of the continuum.
It's the end of the spectrum.
If lethal force is justified, everything else is.
If the trained officer believed that the taser could cause loss of life, certainly the intoxicated
person could.
That justifies everything they did.
Completely absolves the suspect of everything.
From the second the taser came out, everything that happened after that, well it's okay.
That's the argument, isn't it?
That the taser alone, that that taser is what made lethal force okay.
If lethal force is okay, everything else is.
It's the way the spectrum works.
So certainly running away would be okay.
I think it's sad that the American people have bought into this idea that the average
citizen even while intoxicated is supposed to remember their training and do everything
they're supposed to do and do everything right and comply 100%.
But the officer who gets paid to deal with this and who is supposed to be trained in
this, well they can just shoot whoever they want without accountability.
That's a premise that leads to authoritarianism.
And I get it.
I know.
We've always had two sets of laws, right, for some people, for those others, for people
who look like this guy.
You know, they gotta do things better, cleaner.
They gotta make sure they do everything right because laws, well they apply to them, but
only a little because they're different.
There's a whole lot of people who are sick of that.
The other idea is that, well, he did something wrong.
Maybe, maybe.
You know what, I'm gonna go further.
Not just was he intoxicated.
He had all kinds of illicit stuff in the car too.
Guess what?
Doesn't change a thing.
That doesn't make the force justified.
From the evidence that we have seen, it wasn't.
It was wholly unjustified.
From where I'm standing, based on everything we've seen, there was no reason to do this.
And as long as we only focus on the choir boys, those people that we can hold up and
say, look, this person was 100% completely innocent of everything for their entire life,
this is never gonna change.
This isn't.
We have to stop that.
We have to stop just looking for those people who had never done anything wrong in their
life because that's what law enforcement does.
They go through their entire history and drag out stuff like parking tickets or the time
the person got busted in 1997.
It doesn't matter.
It doesn't relate to what happened right then.
If the officer believed that the taser created a reasonable fear of loss of life, the suspect
certainly could have.
And then that justifies all of their actions, especially in today's climate.
Maybe all of these patriots should actually read the Constitution and understand that
it applies to everybody.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}