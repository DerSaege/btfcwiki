---
title: Let's talk about Trump's Tulsa rally and what we're missing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aB1JuEbiVDY) |
| Published | 2020/06/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the recent events in Tulsa, focusing on the importance of discussing the serious aspects rather than making jokes.
- The controversy surrounding the date and location of the event is mentioned, especially considering the current health concerns of gathering people closely.
- Trump exaggerated the expected crowd size, claiming a million people were interested when only 6,200 showed up.
- The discrepancy in numbers was due to younger Americans trolling by requesting tickets with multiple email addresses, leading to an inflated count.
- Despite the low turnout, the attendees were packed closely together without social distancing measures, risking their health for a photo op.
- Beau criticizes Trump for prioritizing his ego over the safety of his supporters and the country, showcasing his lack of leadership qualities and disregard for public health.
- The rally's failure is attributed to Trump's unwillingness to prioritize safety over appearances, ultimately showing his true priorities and character.

### Quotes

- "He jeopardized his biggest supporters."
- "He could have protected the country, but he chose not to for a photo op."
- "That's embarrassing."

### Oneliner

Beau addresses the serious implications of Trump's Tulsa rally, where his ego was prioritized over public safety, jeopardizing his supporters and showcasing his lack of leadership.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact local representatives to advocate for responsible and safe public events (implied)
- Organize community initiatives promoting public health and safety during gatherings (implied)

### Whats missing in summary

The full transcript provides a detailed analysis and commentary on Trump's Tulsa rally, offering a critical perspective on leadership, public health, and priorities that cannot be fully captured in this summary.

### Tags

#Trump #Tulsa #Leadership #PublicHealth #CommunityPolicing


## Transcript
Well, howdy there, Internet of People.
It's Beau again.
So today we're gonna talk about Tulsa,
like everybody else, I'm sure.
The difference is I'm gonna try to keep the jokes
to a minimum.
I mean, there's no way to talk about this
and it not be funny,
but I'm gonna try to keep the jokes to a minimum
because something actually important happened there,
something we should really be talking about
because Trump, without a doubt,
displayed that he is completely unfit to lead anything ever.
But because what happened is so funny,
we're all so busy laughing,
we're not really talking about the important thing
that happened.
To get to the important thing, though,
we do have to go through the chain of events.
Yeah.
So before we get into that,
we need to just kind of remind ourselves
that before any of this actually started,
there was already controversy
because of the date and the location
and the fact that right now it's a really bad idea
to pack a whole bunch of people together
in a building tightly like that.
That's not a good idea right now.
Keep that in mind as we talk about this.
Okay.
So the chain of events goes like this.
Trump says there's gonna be giant crowds
because a million people are interested in it or whatever.
Yeah, he blew the numbers out of proportion.
He exaggerated.
It's Trump.
We know he does it.
That's not a big deal.
The realistic number of people that were interested
was like 800,000, kind of, but that's not true either.
We'll get to that.
With that number, had that number been true,
you could reasonably expect like 100,000 people, right?
So you would have to prepare for that.
The venue only holds 20,000.
So they built a stage outside to handle the overflow
of the just swarms of the great unwashed masses
that would be traveling from all over the country
to come see dear leader.
Okay.
6,200 people showed up as reported by Forbes.
6,200.
So we go a million, 800,000, 100,000, 6,200.
Yes, it's funny.
Make all the jokes you want.
Okay, so how did that happen?
Because the Trump campaign is completely out of touch
with America.
The demographic that he likes to make fun of
and that he and his crew call millennials,
they're not, but whatever,
you know, the ones that he paints as lazy
and can't get anything done, those people,
they trolled him.
They trolled him.
They used multiple email addresses
to express interest and request tickets.
So it gave them this inflated number
that was completely unrealistic,
and they should have known that.
But they didn't because they are out of touch with America.
This little gag that the younger Americans
were playing on Trump was not a secret.
It was all over Twitter.
There are TikTok videos with like a million views
talking about it.
Everybody knew this was happening
except the Trump campaign, apparently.
So they get their 6,200 people there.
Yeah, they had to tear down the stage outside.
It never got used.
They didn't have to give two speeches.
But this is where it gets serious.
A venue for 20,000 people,
and they had 6,200.
What did they do?
They packed them all together anyway.
With a crowd as tiny as Trump's hands,
they packed them together anyway.
They could have socially distanced.
They could have spread them out.
They could have led.
They could have protected the health of the country,
the health of their biggest supporters.
But they didn't.
They chose not to for a photo op
to appease Trump's ego,
to make it look like he was speaking to a huge crowd,
like we wouldn't notice
that the top of it was pretty much empty.
Jeopardized the health of their biggest supporters,
and apparently nobody on the campaign
thought to space them out,
to try to protect them,
to try to mitigate the danger they had put them in,
even though they had them sign a waiver
because they don't care.
They don't care about the average voter.
They care about Trump and that ego.
If he is willing to jeopardize
the health of his biggest supporters,
he is certainly willing to jeopardize national security,
the economy, or standing on the international stage
for his own interests.
He showed his true self there.
He jeopardized his biggest supporters.
This isn't a red state, blue state thing.
Everybody there was on his team
and did nothing to protect them.
You know, and from a PR standpoint,
think about it just from public relations.
Had they spread people out,
it wouldn't have looked so empty.
And yeah, well, yeah, it's a small crowd,
but, you know, we had to stop letting people in
because we want to keep people safe.
I mean, yeah, that would have been a lie,
but it's the Trump administration.
They're going to lie anyway.
So, I mean, that doesn't really factor into it.
They could have kept people safe,
and he chose not to for his own ego.
That's embarrassing.
That is embarrassing.
He could have protected the country.
He could have protected Tulsa,
but he chose not to for a photo op,
for a fail of a rally.
He's not fit to lead anything.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}