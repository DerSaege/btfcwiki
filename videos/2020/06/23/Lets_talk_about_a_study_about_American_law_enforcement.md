---
title: Let's talk about a study about American law enforcement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p3bFJ0FOtjA) |
| Published | 2020/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of reform and drastic change in policing.
- Mentions a study from the University of Chicago Law School conducted between 2015-2018 focusing on 20 major US cities.
- The study, named Deadly Discretion, assessed whether these cities were in compliance with basic human rights law.
- Out of the four main categories - accountability, proportionality, necessity, and legality - no department was legally required to maintain compliance with basic standards.
- Provides recommendations from the study, such as ending no-knock raids, using lethal force as a last resort, and eliminating qualified immunity.
- Suggests that the study is easily digestible, clear, and can help sway individuals who may not understand the experiences of dealing with law enforcement.
- Emphasizes the need for everyone to be interested in these recommendations, as compliance is not a legal requirement for any city.
- Encourages viewers to keep the study handy for educating others and facilitating informed discussions.

### Quotes

- "Is this whole movement out in the streets, is this even necessary?"
- "Getting rid of no knock raids, requiring lethal force to be as a last resort rather than the officer was scared."
- "A lot of people need self-interest."

### Oneliner

Beau explains the necessity of police reform through a study revealing the lack of legal requirements for departments to comply with basic standards, providing recommendations for necessary changes.

### Audience

Advocates for police reform

### On-the-ground actions from transcript

- Share the study "Deadly Discretion" and its recommendations with others to raise awareness (suggested).
- Advocate for the implementation of recommendations from the study in local law enforcement policies (implied).

### Whats missing in summary

The full transcript provides detailed insights on the lack of legal requirements for police compliance and offers clear recommendations for reform efforts.

### Tags

#PoliceReform #DeadlyDiscretionStudy #CommunityAction #Advocacy #PolicyChange


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to try to answer a question for people.
Because there's a whole lot of people out there saying, do we really need reform?
Do we really need this drastic change?
Is this whole movement out in the streets, is this even necessary?
Is there anything that could maybe shed some light on that?
And the answer is yes.
A study out of the University of Chicago Law School took place from 2015 to 2018, just
kind of when it's been released now.
What it set out to do was to monitor 20 cities, the 20 largest in the US, and those are New
York, Los Angeles, Chicago, Houston, Phoenix, Philadelphia, San Antonio, San Diego, Dallas,
San Jose, Austin, Jacksonville, San Francisco, Columbus, Indianapolis, Fort Worth, Charlotte,
Seattle, Denver, and El Paso.
Look at them through a few different metrics and try to determine if they were in basic
minimum compliance with just standard human rights law.
We're not looking for anything drastic here.
Basic minimum compliance.
Every bottom of the barrel here.
The name of the study is Deadly Discretion.
I highly suggest you take a look at it.
It's not a long read.
Measured on four main categories.
Accountability, proportionality, necessity, and legality.
Now on the first three, accountability, proportionality, and necessity, some departments, some of those
departments were in compliance by policy.
But it's that last one where things got a little weird.
Legally, none, not a single one of those departments is required to maintain compliance with basic
norms, with basic standards.
None.
Now aside from just going through and saying everything that's wrong, which it does, it
also has a whole bunch of recommendations.
And the recommendations are there, are the ones you're hearing in the street right now,
for the most part.
Getting rid of no knock raids, requiring lethal force to be as a last resort rather than the
officer was scared.
Getting rid of qualified immunity.
All of this stuff is on there.
If you run across anybody that is questioning whether or not it's really necessary, if we
really do have a problem, this might be a study to have handy.
Because it's easily digestible, it has a whole bunch of suggestions, and it's pretty clear.
The methodology is pretty decent, and it's easy to grasp.
This might be the thing that can help sway somebody who just, through whatever privilege,
has no clue what dealing with law enforcement might be like for some people.
I'll try to get a link and put it down there.
At the end of the day, we have a big problem.
And it's being highlighted because of a particular group, because they're suffering the most
from it in certain aspects.
And in other aspects, it's a different group.
But it's normally not people who look like me.
So it seems as though this is something that only applies to them.
But the fact that none of these cities are operating under a legal requirement to maintain
compliance, that applies to everybody.
A lot of people need self-interest.
You can hand it to them right there.
The recommendations, they're very simple.
Everything's laid out really nicely.
So it's just something maybe to put in your bookmarks, so when you're talking to people,
you have this handy.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}