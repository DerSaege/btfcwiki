---
title: Let's talk about feelings over America's past....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WOaPr2KUb4E) |
| Published | 2020/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a common sentiment among some individuals who believe they shouldn't have to feel guilty about current events in the country.
- Beau shares a personal story about his son breaking a cabinet door and feeling guilty about it.
- Despite his son feeling remorse, Beau emphasized the importance of fixing the broken door.
- Drawing parallels between fixing a broken door and addressing societal issues, Beau stresses the need to focus on solutions rather than guilt.
- Beau mentions his interactions with activists and points out that they prioritize action over people feeling guilty.
- He likens the current societal situation to being on the way to the hardware store to fix the door, indicating that addressing issues is necessary before moving forward.

### Quotes

- "Nobody cares if you feel guilty."
- "They just wanted the door fixed."
- "Maybe what's important is fixing the door."
- "Let's just fix the door."
- "Once we fix the door, we can move on."

### Oneliner

Addressing the sentiment of not wanting to feel guilty, Beau's analogy of fixing a broken door stresses the importance of taking action over dwelling on guilt in societal issues.

### Audience

Individuals reflecting on their role in addressing societal problems.

### On-the-ground actions from transcript

- Fix a tangible issue in your community (implied).
- Focus on concrete solutions rather than dwelling on guilt (implied).

### Whats missing in summary

The emotional weight and personal reflection embedded in Beau's storytelling.

### Tags

#Guilt #ActionOverGuilt #FixTheDoor #CommunitySolutions #SocialResponsibility


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about a common emotion, a feeling, a sentiment, a comment
that keeps getting made.
It's made by people who look like me and it's about everything that's going on in the country
right now.
And we're going to talk about it because while this has shown up in various comment sections
for a while, it showed up in mine last night.
So basically all the comments are different, but they boil down to the idea of, well I
don't think that I should have to feel guilty about this.
Okay cool, so we're going to talk a little bit about guilt tonight.
Years ago my eldest son, for whatever reason, loved running around the island in the kitchen
with the cabinet doors open, jumping over them like hurdles.
And his younger brothers loved it too, they would just laugh and laugh.
And of course, the first time I see him doing it I'm like, don't do this, this is a bad
idea.
Eventually you're going to fall, you're going to get hurt, you're going to break the door,
whatever.
And being a kid, of course that means he stopped immediately when I was around.
One day I'm out in the garage and I hear him running, I hear his younger brothers laughing,
and then I hear the crash.
And I walk inside, he's laying on the floor, I'm like, are you alright?
And he's like, yeah.
And he's just staring at the cabinet door, half of it, the half that's still attached
to the island, the other half is in the floor.
So I grab the screwdriver and I start taking the part that's still attached, I start taking
it off.
And while I'm doing it he tells me, he's like, I'm sorry, I feel bad.
He felt guilt.
We couldn't go straight to the hardware store, so the broken door, the two pieces of the
door were leaning up against the island, and there was a giant open space where there used
to be a door for like three or four days.
This whole time he gets to see it, and he feels bad about it.
Eventually we go to the hardware store, get the stuff, bring it home.
I have him fix it.
Mainly to give him some basic carpentry skills, and also because if you break it, you fix
it.
His younger brothers would have been out there too had they been a little bit older, because
they were in on it, they benefited from it, they were laughing.
So he fixes it, and puts it back together, puts it up himself, looks good, he did a good
job with it.
While he was fixing it, he told me again how bad it made him feel.
I would imagine in the years since, he hasn't thought about that door.
If he did, he didn't feel guilty about it, because he fixed the door.
Hope you get where I'm going with this.
Nobody cares if you feel guilty.
That's not an actual part of this.
I have worked with activists, BLM activists, Panthers, from Michigan to Florida, precisely
zero of them have ever cared whether or not I felt guilty.
That didn't factor into it at all.
They just wanted the door fixed.
That was it.
That was it.
People say, you know, we need to get over it, we need to move on.
We can't get over it.
Right now we're in that period where we're on our way to the hardware store.
You're going to have to hear about it.
Sorry.
And, I feel like this should go without saying, but I guess not, perhaps the minor emotional
inconvenience that is occurring to guys that look like me right now isn't really a big
piece of the puzzle.
Maybe that's not something that is really that important.
Maybe what's important is fixing the door.
Once we fix the door, we can move on.
It can become a part of history rather than something that's currently going on.
Then we can get over it.
Then we can move on.
Nobody cares if you feel guilty.
Nobody cares if you've got your wood glue.
Let's just fix the door.
Anyway, it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}