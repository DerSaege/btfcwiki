---
title: Let's talk about parenting and sparking curiosity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DgM2dSf6jNE) |
| Published | 2020/06/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discloses receiving an educational sample from a company coincidentally before making a positive video mentioning them.
- Shares a message for Mr. Richter on his graduation, acknowledging the challenging times ahead.
- Provides insights into parenting, mentioning that he avoids giving specific advice due to his kids' unique personalities.
- Emphasizes instilling a moral compass and sparking curiosity in children to make education and career choices easier.
- Shares different tactics used to provoke curiosity in his kids, like Minecraft for his eldest and tangible items for his youngest.
- Describes giving his son a lunar material necklace from a company called Mini Museum, sparking curiosity and interest in history.
- Talks about Mini Museum's collection of small samples, including historical artifacts like a piece of the Enigma and Steve Jobs' turtleneck.
- Stresses the importance of leading by example for children to emulate curiosity, reading, and other positive behaviors.
- Concludes by suggesting that recognizing and embracing the uniqueness of each child is vital in parenting.

### Quotes

- "You can't tell a kid anything ever. You have to show them."
- "If you want your kid to be curious, you have to be curious."
- "Recognizing and embracing the uniqueness of each child is an important part of parenting."

### Oneliner

Beau shares insights on parenting, advocating for instilling a moral compass and sparking curiosity while recognizing the uniqueness of each child.

### Audience

Parents, caregivers, educators

### On-the-ground actions from transcript

- Order educational samples or tangible items related to your child's interests to spark curiosity (suggested).
- Lead by example by showing curiosity, reading, and engaging in educational activities in front of children (suggested).

### Whats missing in summary

The full transcript provides a nuanced perspective on parenting, stressing individualized approaches and leading by example to foster curiosity and moral values in children.


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about parenting
and sparking curiosity.
I have this video marked as a paid promotion.
I'm not sure if y'all get to see that or not.
I don't know that that's accurate in any legal sense,
but there was a bizarre chain of events
I felt like I should disclose.
I'm going to mention a company in this video.
I've spoken highly of this company in the past.
I'm sure I will speak highly of this company in the future.
I'm a customer of this company.
The decision to make this video was made weeks ago.
However, because the world caught fire,
I didn't have time to make it.
In the meantime, that same company
sent me an educational sample.
So I feel like I should disclose that.
There's no deal where they're like,
hey, we're going to send you this
and make a video about us or anything along those lines.
There's no advertised relationship.
It was just a weird chain of events
that has led to me receiving something from this company
in like the last week and a half
and me making a video that mentions them
in a positive light shortly thereafter.
There is no deal, but at the same time,
I feel like that's something I should disclose.
I would feel like a huge hypocrite
to rel about politicians not disclosing
who's given them stuff and then for me to do the same thing.
Okay, the second announcement is Mr. Richter,
congratulations on your graduation.
Ari gave me something to say,
but I actually forgot to bring it out here.
It was short.
I'm sure it was something like best wishes, love you,
or something along those lines.
I would like to say that people my age
are leaving you a mess,
but I am sure you are equal to the task.
You've got this.
Good luck moving forward.
All right, now onto the actual video.
I get asked about my parenting style
or for parenting advice pretty often
because I have a whole bunch of kids.
The reason I don't really ever give it
is because I have a whole bunch of kids.
They're all different.
I don't think that most parenting advice is very sound
because I know among my kids,
you would get different advice for each one.
So I can't see how the advice I would have
in any specific way would be real helpful.
What I can say is there are two things
that I try to do with all of my kids.
The first is to give them a moral compass,
teach them right from wrong.
That seems like it should go without saying.
The second is I try to instill and provoke curiosity
because if you do that,
the battles later in life about education
and all that stuff, they're a whole lot easier
because they're gonna educate themselves.
They'll also be happier
because they'll have interests that they enjoy.
They may even find a career that they love.
Now, the problem is how do you spark that curiosity?
It's different.
Diversity of tactics.
My eldest, Minecraft.
It was a video game called Minecraft.
It's like Legos inside a video game, kind of.
He started playing it when he was young
and so much so that I was kind of,
you know, I thought it was a little weird to be honest,
but I saw him reading books about geology and architecture.
Play your video game, dude.
Whatever provokes your curiosity.
My five-year-old, you know, he loves dinosaurs.
Absolutely.
Just, but on some level,
I think he always thought that they were like big monsters.
They never really seemed real when he would talk about them,
even though we'd taken him to museums
and he'd seen, you know, the fossils.
Recently, I had an anniversary with my wife.
A long time ago, she told me she never expected me
to give her the moon.
She wanted me to be here on Earth one day.
She wanted me to be here on Earth with her.
Really sweet, right?
I found a company that sells necklaces
that has lunar material in them.
Gave her the moon.
This same company sold Moses' Warty
and I ordered one for my son at the time.
And the moment I put that thing in his hand
and it became real, tangible, something he could touch,
you could see his whole attitude towards it change
as he's studying this thing in his hand.
It requires different techniques.
You know, with my eldest, it was all in his head.
It was all imaginary.
It was Minecraft, it was reading, you know.
With my five-year-old, it took physically touching it
to get that spark.
The company I ordered it from
is the company I was talking about earlier.
It's called Mini Museum.
The funny thing is, the thing that they sent me
is like the exact opposite of what I was just talking about.
They sell these, which I don't know if you can see that,
if it's in focus or not.
But basically, this is a collection
of a bunch of small samples,
everything from an SR-71 to a piece of Alcatraz
to a piece of the Enigma,
all kinds of cool little stuff in it.
They also sell this stuff separately
that's not encased in something,
and you can put it in your kid's hand.
And it's not just dinosaur teeth.
You could get a piece of the Hindenburg,
a deck chair from the Lusitania, all kinds of stuff.
And it helps spark that curiosity.
Even this, even though they couldn't touch it,
there's one of the things in here
is a piece of Steve Jobs' turtleneck.
And as I was talking about the stuff that was in it
with my kid, that sparked a conversation
because to them, that seemed way out of place.
They're like, why is that in there?
Who is this person and why are they important?
Because to them, computers had already existed.
They were always there.
So this, the PC-Mac war, none of that ever existed for them.
So explaining how he brought that company back
from where it was at and turned it into the company
that makes the iPhone, it sparked curiosity.
And I think that's a big part of creating happy kids
and creating productive adults.
The other thing you have to remember is
you can't tell a kid anything ever.
You have to show them.
If you want your kid to be curious, you have to be curious.
If you want your kid to read, they have to see you read.
You want your kid to watch documentaries,
you better watch some documentaries.
They're going to emulate you.
You know that old commercial, I learned it from you, dad.
That can be used for good things too.
I mean, you really had to set the example
and you had to show them what you want from them.
And they'll do it.
They like you, even though they may not always seem like it.
That's really the only advice I have.
As far as cookie cutter across the board,
this is something I think everybody should do
with their kids.
That's it.
That's all I got because they're all different.
All of them, drastically.
So I think maybe recognizing that
is an important part of it too.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}