---
title: Let's talk about coping with a therapist....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FeM3EWSqI9w) |
| Published | 2020/06/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about coping mechanisms and reaching out for help from Jody Strach, a therapist and mindfulness practitioner.
- Jody shares her journey into mindfulness, starting from marriage and family therapy to practicing yoga and meditation.
- Jody explains the importance of personal mindfulness practice in being able to help others effectively.
- Jody addresses coping with the stress and changes in society, focusing on acknowledging emotions and finding inner strength.
- The importance of turning inwards, embracing difficulties, and using mindfulness to navigate challenging situations is discussed.
- Jody encourages self-compassion and self-care practices to alleviate stress and anxiety during turbulent times.
- Dealing with feelings of hopelessness and paralysis by turning inward, grounding oneself, and taking deep breaths is emphasized.
- The concept of community service as a way to connect, support others, and find empowerment is explored.
- Jody stresses the significance of looking inward, exploring emotions, and connecting with others to create positive change.
- Recommendations for resources like Deborah Edenthal's work and Ruth King's book "Mindful of Race" are shared for further exploration.

### Quotes

- "Anger is a natural response when there's a violation of a boundary, that anxiety is such a human response to feeling threatened."
- "If there's a part of you that feels completely hopeless right now, really turning towards that part, finding a way to just meet that part and know that's not all of who we are."
- "Trusting that we start with looking at our own responses to things."

### Oneliner

Beau and Jody dive into coping mechanisms, mindfulness practices, and the power of community service amid societal challenges.

### Audience

Individuals seeking coping strategies and inner strength.

### On-the-ground actions from transcript

- Connect with a local community service group to volunteer and support those in need (suggested).
- Practice self-compassion and mindfulness techniques in daily life to alleviate stress (exemplified).
- Read "Mindful of Race" by Ruth King for insights on navigating racial issues (suggested).

### Whats missing in summary

Exploration of the interconnectedness between personal mindfulness practice, community engagement, and societal change.

### Tags

#CopingMechanisms #Mindfulness #CommunityService #SelfCompassion #InnerStrength


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to answer a question that I've been getting asked a lot for the
last couple of months.
It's a question I don't have an answer to.
You know, people are asking, how do you cope?
What's the best method of coping?
How do I deal with all of this that's going around?
And truthfully, I don't know.
I tend to just throw myself into the grinder.
And I'm self-aware enough to know that that is probably not good advice.
From a mental health standpoint, that's probably not the right move.
So I reached out to somebody named Jody Strach.
And we're going to get a view from somebody who actually understands this stuff.
So let's listen to Jody's thoughts.
Well howdy there internet people, it's Beau again.
So today we're going to be talking with Jody Strach.
She's a therapist and practitioner of mindfulness.
And we're about to find out what that means.
So Jody, could you start us off with telling us a little bit about what it is you do?
Sure.
And first I just want to thank you so much for having me on here.
I feel just so grateful.
And I really appreciate the opportunity to give voice to whatever comes out here today.
So as far as how I get to meet people in this world through my job, it started out with
marriage and family therapy.
That's what I went to graduate school for.
And I learned what most of us learn in those programs, which is a lot of theory.
I was really fortunate with the school that I went to that we had a lot of live practice
before we became licensed.
And kind of paralleling my experience with my education was this whole other education
of practicing yoga in my own life.
And kind of straddling this place of thinking that was for other people and a little granola
crunchy and kind of wishing I was in that world, but also feeling really drawn to the
underlying themes and messages in yoga.
And then when I met my partner that I'm with now, finding the value of meditation practice.
He was a meditation practitioner and I saw how much that supported our relationship.
And meditation seemed once again like something that was for these other people in my mind
that I could kind of wish that I might one day be, but it didn't really resonate for
me at that time where it felt so far away.
And I think through just being together with him and seeing that here's this regular person
I'm in relationship with bringing in this practice, it became much more accessible to
me and I started to practice with a meditation group weekly and start learning some Buddhist
teachings and receiving those teachings, learning from them.
And then there's this boom of secular mindfulness that seemed to happen over the past few years
that made it so accessible to so many more people.
And I found that intriguing and I just kept following my curiosity there.
And I found that as I had my own mindfulness practice and really was doing my own inner
work, I just saw that there is just no way I could work with people and really be with
people's inner world without taking full responsibility and really being honest about my own inner
world.
I would eventually kind of hit this place if I wasn't doing my own practice where I
couldn't fully be of service and just go to the darkest and deepest places with others.
So that's a huge motivation to me and I really don't see it as two separate things.
I really see just the connectedness between my own personal work and the work I do with
other individuals, but also the work that is there to be done in the world.
Okay.
Okay.
So how would this apply and how would this help when we are talking about people today
with everything that's going on and you can pick your topic, all of the different stuff
that's occurring in the United States and the world, we're all going through a lot of
let's just say changes and stressors, things that are out of the norm.
So they're causing stress, even if it's not a huge amount of stress.
So what I'm wondering is if there is anything that you can share that can help people adjust
to all of the changes that are coming along, because it's a question we get a lot.
Sure.
I mean, this is such a huge topic and especially the week that you and I are doing this, it's
where everything is really just coming to a head even more.
And I think it has been for a long time, but just around the relationship with race in
our country and people's responses to that and real pain and grief and rage, the anticipation
and anxiety that we're all holding in our own way around stay at home orders and as
those soften and where people stand on that with COVID-19.
So yeah, I have to say just for myself, as we're talking about this, I'm just wanting
to pause because I know here for me, it's a lot.
I mean, it's a lot.
And I'm feeling that.
If I'm being completely transparent and honest, I have spent the last few weeks in a really
tender place of just being with my own grief and watching how quickly that can trigger
reactivity when I'm not paying attention and really taking the time to pause and look inward.
So I think on a more practical level for people listening, I just want to pause so I can get
clear on this.
It's really easy right now, I think for so many of us, and I'm including myself, to fall
into a place of hopelessness, helplessness, letting fear either paralyze us or kind of
catapult us into reactivity.
So either we're acting in, which can be negative self-talk, self-aggression, comparison to
others, standards that we create, striving, feeling like we're not doing enough, or we
can project it outward and really other people and judge and blame and point fingers and
those are such easy places to spin out, whether it's happening in an inward way or an outward
way.
And for so many of us, it kind of is a ping pong match back and forth when we get caught
in those places.
So first just acknowledging the collective experience of this that I've been seeing,
again, in myself and in the world around.
And first, I think it can be incredibly helpful when we can remember that there is so much
value in finding the courage to just stop and be with our own experience.
I think we're taught, we're kind of conditioned as a culture to run away from discomfort,
to find a way to numb out from it or view it as a big problem that we need to solve.
And it's such a relief, it's been such a relief to me to recognize that these feelings that
we have in these really big moments are often the healthiest response we could be having
to a collective trauma.
And that anger is a natural response when there's a violation of a boundary, that anxiety
is such a human response to feeling threatened.
And so there's a compassion piece here when we can just sit, and even right now as you're
listening, it might be nice for us all to gather to just close our eyes, even if this
feels horribly foreign to you.
Just knowing that maybe for you, closing your eyes is a thing of courage to just pause and
take a breath.
And to give ourselves permission to feel into what's arising in the physical body.
So this sounds a lot like you're, different phrasing, but I mean, it sounds like you're
kind of saying embrace the suck.
I mean, like, but you're going to have to sit with it.
This is, you know, you need to acknowledge the emotions, all of the things that you're
feeling at the moment.
I think there's great benefit in that, and I also want to just acknowledge that for some
of us, this is such a big piece of it, and sometimes I can get a little scattered in
trying to name it, but we all come to this world coming from different circumstances.
And so there are some of us who have received a foundation for how to meet embracing the
suck, and there's many of us who actually the best thing that we could have done in
our childhood was to just survive.
And it wasn't the time to embrace the suck.
You know, it was the time to just survive.
And so this is really, it might be very scary and very new for many people, and for some
of us, it might be something that we're practicing regularly, but no matter where it is that
you find yourself falling in that space, it really is as simple as beginning to turn towards.
And yes, if you want to call it embracing the suck, that's fine languaging for it, but
I think that the benefit of that and where it can really turn a corner is that when only
I find only when I've been able to learn how to sit with these places in myself, do I have
access to then be able to tolerate it more in the world around, and that when I can then
start to tolerate it more in the world around me, action becomes available that I didn't
even know was possible before.
And that is really empowering, whether that action is simply learning how to more tolerate
and listen to somebody's perspective that five years ago, I would have argued with them
or gotten caught in kind of an ego space with them.
Now there's a place that can just hold space and not be a passive recipient of violence
or negativity or any of that.
It's not about that.
It's about really being able to deeply listen to see the person fully that's in front of
me and then from that place, knowing when there's an opportunity to plant a seed or
to say something that might allow more spaciousness in the conversation.
And so whatever form it's taking, right, it's a way for us as we turn to our own experience,
using that, trusting that our own inner work is what will then ripple out and make an impact
in the world.
And I find that incredibly hopeful.
Yeah, I like it.
Are there any specific things that people can do to help alleviate the stress and the
tension and the anxiety that's coming along with all of these changes and certain the
ones that will come next week because it seems to continue?
Yeah.
Let me just pause there for a moment.
I think the first thing is to just ask if there's any ways that you can be taking care
of yourself right now to really make space to do that.
And I don't mean the self-care like this isn't about what you might find on an Instagram
page about here's 10 things for self-care and not that kind of self-care.
That's fine, but that's not what I'm talking about.
This is more of is there a need for more rest?
You know, some people right now that I've worked with have shared that sleep has been
really hard because the mind is running so much and there's so much fear or overwhelm,
whatever it is, that it's just really hard to sleep at night.
And along with this comes a pressure from either losing a job and feeling like you need
to make ends meet or having a job that's ramped up right now and a fear that if you don't
keep up with it, you're going to lose your job, wherever the anxiety is coming from.
Let's just say, for example, maybe you're having a difficult time sleeping from all
of this.
I found it really useful in those moments when I've had trouble sleeping to lay in bed
and just have a kind voice that can say, eventually sleep will happen.
Eventually sleep will happen and I can rest while I'm not sleeping.
And to trust that rest is very restorative, even if sleep isn't happening.
So I'd say that's one thing people can do is just find little ways.
That's just one example to bring real tenderness to themselves right now, compassion practices.
And that can just be as simple as noticing how do you talk to yourself?
When you're walking around in your day and maybe you have a human moment and you make
a mistake, what's the dialogue?
How do you talk to yourself in that moment?
And it was something my teacher asked me that is funny languaging, but it really changed
the way that I've looked at life is, who is that?
Who's that voice?
Is that a familiar voice?
Where did that come from?
What part of me is that?
Where's that credit coming from?
Things like this just begin to give us a little bit of space to take that step backwards and
be able to see, to have a little bit of spaciousness between.
It's changing the languaging from saying like, I'm so hard on myself to, huh, there's a part
of me that can be really critical.
And how can I take care of myself when that part of me begins to arise?
Okay.
All right.
So one of the larger things that we have going on, right, is people feeling that they can't
act, they can't affect the world, they can't make things better.
Is there any advice on how to deal with those emotions specifically?
With that kind of paralysis, is that what you're asking that can come with how overwhelming
this can all be right now?
Yeah.
Yeah.
That's a good way to describe it.
Yeah.
It might sound a little bit like a broken record, but that's on purpose.
It really does start and it really is enough to turn.
It's like Tara Brock is a teacher that is pretty known in the mindfulness world.
She refers to this as doing a U-turn, right?
So it's like when we catch ourselves looking out in the world and thinking like, oh my
gosh, this is so much, what can I do?
This is so overwhelming.
And you feel paralyzed by that.
It's such a great opportunity to just do a little U-turn and turn your attention again
inward.
And what this allows for is we can actually meet ourselves in this moment exactly as we
are.
So, huh, I'm feeling really paralyzed right now.
What is it that's needed?
What's needed when the sense of paralysis is here?
And simply giving yourself a few moments to take deep breaths, right?
When we get caught in our head, it's like if you could see me right now, I'm going to
verbalize this.
It's like I'm taking my hand and moving it up to my head.
We spend so much time in our head, in our mind, and looping around what can I do?
How can I do this differently?
Oh no, I feel really paralyzed and hopeless and I'm not making a big enough impact in
the world and so much change needs to happen.
And here I am just sitting here doing nothing.
We can be really in our head.
It's so valuable to just almost imagine you're taking an elevator from your head down your
face, down your throat, into your body.
And just notice the impact this is having on your body in this moment.
Just go down into the body.
It's a really grounding practice to have.
It roots you back down and it starts to inform us so that we're not just obeying these thoughts
that are chattering at us in our mind, but it allows us to access more of our whole experience.
And we can take care of ourselves there.
And through that, I promise you that how you show up in the world as you meet yourself
in this way of just turning towards yourself, I promise you it will make a huge impact of
how you show up in the world around you.
So this sounds a lot, we have a widely, we got a pretty varied group of people here.
This sounds to me like Be Here Now, is that, that's the same premise?
Okay.
Yeah, yeah.
There's the Be Here Now.
My teacher has a wonderful book called Relational Mindfulness and I think it's been the single
most useful resource to me in my life to giving practical hands-on support to how to meet
ourself in difficult times.
Her name is Deborah Edenthal and she also has a lot of free resources and support for
this right now available through her website, which is deborahedenthal.com.
But I mean, there's so many spaces out there.
It's actually a really wonderful thing that's coming out of this time right now.
And that's actually reminding me and if it's okay with you, Beau, if I say a little something,
if I elaborate on that, is that okay?
Or did you have another direction you wanted to go in?
Oh, no, absolutely.
Go right ahead.
There's a teacher named Joanna Macy.
I was listening to an interview with her and she's in her 90s now.
And I thought she had the most fascinating perspective on what's arising in the world.
And she gave the definition of apocalypse.
And she was saying often when we think of the apocalypse, we just think of doomsday
and just the end of the world and everything falling apart.
And she said that when she looked up the definition, what she found is that it also means a revelation
and an unveiling, that there's an unveiling, there's a veil that's lifting up.
And we have this ability to see things now that we didn't have access to seeing before,
that there are things being exposed that we can really see.
And yes, it can be shocking, right?
And there's time that we need to integrate and let that shock be there and let our whatever
experience around that be.
But also to acknowledge the importance of having the blinders taken off, of having the
veil lifted so that we can actually see clearly what's here.
Because when we can see clearly what's here, we can then meet what's here in a way that
can have an impact.
And I found that to be an awesome view from somebody who has a lot of life experience
and a lot of wisdom.
So I think in holding the heaviness and the darkness and the weight and the challenge
of this time, it's so important to value the importance of the darkness.
Part of what Eden, my teacher, says is that it's through darkness where life is really
born, where it comes out of, and that this is also a really fertile time, as challenging
as it is for the world, for the collective.
That's deep.
Yeah, I thought so.
It blew me away.
It's like a waking up.
So given that we're looking at all of this stuff happening, and so it's don't lose hope,
is basically, I mean, well, that should go without saying.
But I mean, that kind of seems as though be here now, be aware of all of this stuff.
I'm trying to sum all of this up and keep it.
So be here now, be aware of what's going on.
Don't lose hope because things are going to get better.
But embrace what it is that you're feeling.
Yes.
Yeah.
Feeling a loss of hope, if there's a part of you that feels completely hopeless right
now, I don't know that it would be helpful for me to say, don't lose hope, because that's
not going to really meet you where you are.
If really, if that's where there's a part of you that's losing hope, really turning
towards that part, finding a way to just meet that part and know that's not all of who we
are.
That's just a part of who we are.
And so it's just fine.
There's no problem with that.
It's not a problem to feel hopeless in one moment.
And it will change.
So I heard that some of the people in your field use, like they encourage community service
events, kind of.
Can you give me some of the rationale behind that, if that is something that you agree
with?
Can you say more when you say community service events?
Can you clarify what you're speaking about?
Usually they'll encourage people to get involved in volunteer activities or anything along
those lines.
And I've heard of a couple of different people doing it in different ways.
I don't know if it's common or I don't know if this is just a coincidence that the few
that I'm aware of do this.
Yeah.
You know, first of all, community service is such a, to me, it serves all of us.
You know, yes, it directly serves the community we're showing up for, but it also has an impact
on the person who's showing up for the service.
And it just has this huge ripple effect.
And I know that it's such an important thing for me.
My dear friend Lauren and I started about four years ago, actually, something called
Living Practice.
And it's where we get some people in our community, whoever's interested, we send out an email
and we go out and do service here where we live in California.
And we do various acts of service from serving people who experience homelessness to planting
trees to going out and helping to start a garden for a local elementary school.
And yeah, absolutely.
I mean, what's beautiful about that, and I think at this time, especially when we can
feel so isolated and missing that face to face connection, there's something so valuable
about finding ways to plug into community.
So yes, there's a lot of value in that.
And I highly recommend exploring that if that's something you haven't done before, because
community is something that I think at this time where we're on our computer screens,
and I'm not just talking about the time of COVID, I'm talking about this time in our
lives where connecting over social media, any type of computer media, we're spending
more time isolated and less in community, many of us, at least that's been my experience
here in Los Angeles, that we can sometimes forget the amazing power and energy that comes
from connecting with community.
And also, there's such an opportunity to put a face to people that we might think when
we're isolated alone are so different from us.
Having a face to it brings us humanity in, and it allows us to erase that veil of separation
that we can get so caught in believing that I'm me and you're you, and it invites us into
a more we consciousness experience.
And it's my personal belief that there is incredible power to meeting the world right
now when we meet it from that place of remembering the we instead of the I versus you.
Yeah.
Okay.
Yeah, it was just, yeah, I found that really interesting, because most of the people that
I'm aware of that encourage the inward looking, they also do this thing where they push for
the people that they're working with to go out and get involved in the community and
do all of this stuff.
I always found it a unique, it's not a contradiction because it makes complete sense when you think
about it, but on the surface, it kind of doesn't when you first glance at it.
But I always thought it was very interesting.
Okay.
So this is a question I ask pretty much everybody who comes on the show, and this is, if you
have one piece of advice for the people who are going to listen to this on how they personally
could help change the world, make their community better, whatever, something they could do,
what would it be?
Gosh, that's a huge question.
And I feel a lot of vulnerability in my self arise because it feels like a big responsibility
to provide an answer to it.
No pressure.
I mean, the reality is I would say to look inward because there's not, I don't think
that, I think if you're really looking into your own experience, you'll find something
that moves you.
You know, it could be as simple as a family member who has a different view on something
than you do, just being able to reach out and connect with them.
Not even, I'm not suggesting that you connect with them about the topic that you have a
challenge around, but just to simply bridge a connection to, again, it's putting that
face, it's making more human.
And it could also be as simple as when an emotion that's challenging rises up inside
of you, having the courage and the patience and the willingness to explore it, to investigate,
to be with yourself just a little longer than you may want to or have previously chosen
to.
I really have experienced in my own life and believe with my whole body, my whole heart,
that that is the stepping stones to being able to create real change in the world is
trusting that we start with looking at our own responses to things. Okay.
All right.
So parting shot, is there anything you want to talk about, anything you want to plug,
anything you think people should definitely look more into on their own, anything like
that?
I mean, I suggested some resources and I definitely think those are great.
Something right now that I do want to speak to, there is a woman named Ruth King who wrote
a book called Mindful of Race, and she is just amazing.
And so if right now some of what's coming up in the world around race is really touching
you in any way, that book and her work is an amazing, amazing resource.
So I would just offer that to anybody who that's up for right now.
Okay.
So unless you have anything else, I think we're going to wrap it up.
It's definitely given people a lot to process.
It's kind of a change of pace.
The channel is normally like extremely political and, you know, so, but with everybody asking,
hey, maybe we should slow down.
It seemed like a good idea to maybe slow down a little bit.
So yeah, I guess we're going to wrap it up unless you have anything else.
That's everything.
Thank you so much, Bo.
It was really just, I was really touched to be invited on.
Thank you.
Okay.
Well, thanks for coming on.
We enjoyed it and I'm sure that people are going to benefit from it.
All right.
So, all right, everybody, that's it for the show.
It's just a thought.
Y'all have a good night. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}