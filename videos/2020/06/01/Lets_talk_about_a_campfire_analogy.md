---
title: Let's talk about a campfire analogy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8Y7FDDt8EBM) |
| Published | 2020/06/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces an analogy about a campfire to explain a complex issue.
- Describes sticks near the campfire warming up and being wary of sparks.
- Links sparks to the inappropriate use of force.
- Talks about how more sparks can lead to sticks catching fire easily.
- Mentions the temperature rising and the campground being on edge.
- Criticizes the idea of putting out campfires with more sparks.
- Talks about a person in a fireproof bunker advising to apply more sparks.
- Emphasizes the need for water, not sparks, to put out campfires.
- Refers to experts in DC who understand campfires and advocate against more sparks.
- Urges for understanding the theory of campfires and avoiding more sparks.
- Calls out Senator Tom Cotton for not comprehending the situation.
- Stresses the importance of using water to cool off the situation and prevent more campfires.
- Mentions areas like Flint, Michigan, that used water effectively.
- Warns about the cycle of campfires and the need to break it before it escalates.
- Advocates for making the sticks comfortable and cooling them off to prevent further issues.

### Quotes

- "You do not put out campfires with more sparks."
- "More inappropriate use of force is not going to help here."
- "Applying more sparks will cause more campfires."
- "This isn't complex. The spark was bad."
- "More sparks are not the answer."

### Oneliner

Beau explains using a campfire analogy how inappropriate force sparks issues and why water, not more sparks, is needed to prevent further escalation.

### Audience

Campground residents

### On-the-ground actions from transcript

- Call for community leaders to prioritize de-escalation tactics over aggressive responses (implied)
- Advocate for peaceful resolutions and understanding in the community (implied)
- Support initiatives that focus on cooling down tense situations rather than adding fuel to the fire (implied)

### Whats missing in summary

Beau's engaging storytelling and powerful analogy can best be experienced by watching the full video to grasp the depth of his message.

### Tags

#CommunityPolicing #Deescalation #InappropriateUseOfForce #CampfireAnalogy #WaterNotSparks


## Transcript
Well howdy there internet people, it's Beau again.
So today's going to be a little different.
We're going to do pretty much this entire video in an analogy
because I'm hoping it reaches the right people.
This is something you might want to tweet out to your camper.
What I want you to do is I want you to picture a campfire,
normal campfire, where it's supposed
to be inside the little rings, all right?
A little ring of stones around it.
Everything's all good.
It's a campfire.
Around that campfire, there's a bunch of sticks.
Sticks generally do not like campfires
because sometimes sticks end up getting destroyed in campfires.
So they don't like them.
They are generally opposed to campfires.
However, they're close to it.
And they're kind of warming up.
They're heating up because they're near the campfire.
More importantly, they know that the fire isn't the problem.
The spark is.
And they all saw the spark, almost
like there was a video that went around
and every stick saw the spark.
And that spark, in case you're missing it,
is the inappropriate use of force.
The spark is the problem, not the fire.
So that fire is there.
And as all the sticks are looking at that fire,
they see more sparks.
They see sparks in places where there wasn't a fire.
They see journalist sticks and peaceful sticks
having sparks thrown at them, all the while
the temperature's raising.
As their temperature raises, does it
require as large a spark to get that stick to catch fire?
It doesn't.
It doesn't.
In fact, sparks from that main fire,
they can fly up and just land on it.
And there's a whole bunch of areas
all over the campground, United States,
that were smoldering already.
They were already on edge.
They were already right at that point.
And then a spark hit them.
That's where we're at.
You do not put out campfires with more sparks.
That's not how it works.
Then you've got this guy hiding, cowering in a fireproof bunker,
calling the campers, that's the governors,
and telling them they need to apply more sparks.
He doesn't know what he's talking about.
He will fail this test of leadership
like he has every other one.
He does not understand this.
These campfires are largely his responsibility.
If you want to put out the campfires,
you have to use water, not sparks.
More inappropriate use of force is not going to help here.
Now, there are entire rooms full of people in DC
that understand campfires.
They're experts in campfires.
They have written manuals on campfires.
And incidentally, it actually says in the manuals,
no more sparks.
That's a big piece of it.
Now, these people, they can't work domestically.
But the campers all have people who are dual-badged now
who can call them.
They can't work domestically, but they
can give some friendly advice.
They can explain the theory of campfires
and explain that the more sparks that are applied,
the more sticks join in.
Now, I understand that it's cool to get on Twitter and act tough.
Try to look tough on Twitter because it
appeals to your base.
But you end up just looking ignorant
because you don't understand the theory of campfires.
I am looking at you, Senator Tom Cotton.
That was appalling.
You should resign, my honest opinion.
You, sir, are not somebody who can put out a campfire.
You're going to create more.
There are 140-plus campfires going on on the campground
right now.
We don't need more sparks because there
are a whole bunch of dry areas that
have been hit with spark after spark after spark for years.
One of them will catch.
You need water.
There are areas that have used water.
Flint, Michigan.
Kind of.
I mean, it's not funny, but I mean,
it's entertaining in the analogy.
Flint, Michigan used water.
Their fire's not that bad, is it?
There are a whole bunch of areas that used water.
And you know what?
They're not on the news because their campfire
isn't out of control.
This isn't complex.
The spark was bad.
Everybody knows that spark never should have happened.
Applying more sparks will cause more campfires.
You need water.
You need to make the sticks feel comfortable.
You need to cool them off.
You need to explain to them, yes, we understand.
That spark was really bad.
Because if you apply more sparks,
this is going to get out of control.
This is all cyclical.
And these campfires are directly related
to the previous campfires.
And this is probably our last cycle.
This is probably our last chance to get it before it gets bad.
And understand what I'm saying.
This isn't bad.
Not in comparison to what it can be.
Not in comparison to what it will
be if you listen to the guy in the fireproof bunker.
More sparks are not the answer.
Anyway, it's just a thought.
Y'all have a good day.
Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}