# All videos from June, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-06-30: Let's talk about Trump and new developments about Russia.... (<a href="https://youtube.com/watch?v=DGkLBTgnQlw">watch</a> || <a href="/videos/2020/06/30/Lets_talk_about_Trump_and_new_developments_about_Russia">transcript &amp; editable summary</a>)

Beau questions the president's knowledge, criticizes inaction on critical intelligence, and challenges blind trust in Putin.

</summary>

"Intelligence is about establishing intent. It's about telling the future. It's a crystal ball."
"Have skepticism. Have doubts. But please keep in mind, these reports didn't originate with the intelligence community."
"Leave them as live action target practice for the opposition."
"Whether you agree with them being there or not, the US sent them there."
"While the president is planning rallies and tweeting about his enemies in social media, there are bags being filled and he doesn't care."

### AI summary (High error rate! Edit errors on video page)

Questions what the president knew and when he knew it, a frequent concern during this administration.
Reports from The Military Times suggest the president received information on Russia's activities over a year ago.
Raises doubts about the president's evolving position, shifting from denial to unverified claims.
Points out that intelligence is about establishing intent and predicting the future, not about verification.
Criticizes the lack of action taken by the administration to verify critical intelligence.
Expresses disbelief at the president's trust in Putin, a former Russian intelligence officer.
Challenges the administration's inaction and failure to act on significant intelligence.
Notes the contradiction in distrusting intelligence reports while trusting Russian sources.
Emphasizes that reports did not originate from the intelligence community but from troops on the ground.
Criticizes the president for neglecting critical issues while focusing on campaigning and self-promotion.

Actions:

for concerned citizens,
Support troops by advocating for proper action to protect them (implied)
Pressure politicians to prioritize national security over personal interests (implied)
Advocate for transparency and accountability in intelligence handling (implied)
</details>
<details>
<summary>
2020-06-29: Let's talk about the spirit of the law & order.... (<a href="https://youtube.com/watch?v=XM5FN1mN-qQ">watch</a> || <a href="/videos/2020/06/29/Lets_talk_about_the_spirit_of_the_law_order">transcript &amp; editable summary</a>)

Beau reveals the overwhelming support for police reform, advocates for enforcing the spirit of the law, and proposes elected top law enforcement officers for community-oriented policing.

</summary>

"An overwhelming majority of people are behind you."
"The spirit of the law will provide order without providing injustice."
"They exercise the spirit of the law."

### AI summary (High error rate! Edit errors on video page)

A recent poll reveals that only 9% of Americans believe law enforcement does not need reform, with a majority of 55% calling for major reform.
Police accountability activists have successfully garnered support from an overwhelming majority of people.
Beau points out that while many police accountability proposals are good, they may miss the importance of enforcing the spirit of the law.
Beau shares a personal story about a friend from New York experiencing the unique law enforcement culture in the South.
He delves into the concept of the "spirit of the law" and how it can lead to more just law enforcement and better community outcomes.
Using examples like fireworks laws and stand your ground laws, Beau illustrates the difference between enforcing the spirit versus the letter of the law.
In rural areas, law enforcement officers may adhere more to the spirit of the law due to the accountability enforced by elected sheriffs.
Elected law enforcement officials are more likely to focus on violent and property crimes to secure re-election, fostering community policing.
Beau advocates for top law enforcement officers in every agency to be elected and have absolute power over hiring and firing, eliminating the influence of unions.
He stresses that prioritizing the community's desires for peace and order can lead to effective and just law enforcement practices.

Actions:

for law enforcement reform advocates,
Elect top law enforcement officers for community policing (suggested)
Advocate for absolute power over hiring and firing for elected law enforcement officials (suggested)
Prioritize violent and property crimes as elected officials to satisfy community needs (exemplified)
</details>
<details>
<summary>
2020-06-29: Let's talk about Trump's warrants and an inside joke.... (<a href="https://youtube.com/watch?v=tKJnu_jZjK0">watch</a> || <a href="/videos/2020/06/29/Lets_talk_about_Trump_s_warrants_and_an_inside_joke">transcript &amp; editable summary</a>)

Iran issues warrants for Trump and officials, mocking Trump internationally, seen as trolling and political satire.

</summary>

"Iran's less than subtle way of saying one of their generals was better at protecting American forces than the President of the United States."
"It's trolling on an international level."
"It's a joke. It's a political statement."
"Mostly, it's just making fun of Trump to the entire world."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

President Trump and senior administration officials have warrants issued for them by Iran, with Interpol involved.
The warrants are related to the killing of Soleimani.
Iran's actions are seen as a way of mocking and making fun of President Trump internationally.
General Soleimani once provided specific intelligence to protect US forces, contrasting with Trump's situation.
Iran's actions are interpreted as suggesting their general was better at protecting American forces than Trump.
The move is considered trolling on an international level, entertaining but with serious implications.
There is uncertainty about the potential outcomes of this situation due to the lack of precedent.
The warrants are not expected to lead to the actual arrest of the US President, seen more as a political statement and diplomatic protest.
The primary aim appears to be making a mockery of Trump to the world.
Overall, the situation is viewed as a form of political satire and criticism.

Actions:

for global citizens,
Stay informed about international political developments (implied)
Advocate for diplomatic solutions to international conflicts (implied)
</details>
<details>
<summary>
2020-06-28: Let's talk about President Trump's denials.... (<a href="https://youtube.com/watch?v=NkFlBxyS9ac">watch</a> || <a href="/videos/2020/06/28/Lets_talk_about_President_Trump_s_denials">transcript &amp; editable summary</a>)

Beau questions White House denials, analyzes concerning actions, and concludes on President's unsuitability to lead.

</summary>

"Just accept them as true."
"His denials get focused."
"There's no situation in which this [lack of briefing] is acceptable."
"He's not fit to lead the country."
"So yeah, there's our president out there doing good, leading from the front."

### AI summary (High error rate! Edit errors on video page)

Beau questions the truthfulness of denials coming from the White House and suggests assuming they are true for the purpose of analysis.
He points out a tweet where Trump supporters in Florida were confronted, and one of them used a term associated with nationalist and white movements. Despite the denial that he heard it, Beau questions why the President retweeted the video.
Beau speculates on the possible motives behind the President's actions, suggesting that the tweet may have been to signal his base.
Regarding the denial about intelligence on Russia, Beau questions why the President and Vice President weren't briefed, raising concerns about their trustworthiness and potential leaks.
He suggests scenarios where the intelligence community might not have trusted the President with sensitive information.
Beau concludes that the President's actions indicate mismanagement and his unsuitability to lead the country.
He criticizes the lack of response from the administration after the President was supposedly briefed on the intelligence issue.
Beau mentions an incident in Tulsa where instructions to social distance were removed, questioning the President's leadership.

Actions:

for concerned citizens,
Question denials and demand transparency from public officials (suggested)
Stay informed about political actions and hold leaders accountable (suggested)
Advocate for responsible leadership and decision-making (suggested)
</details>
<details>
<summary>
2020-06-28: Let's talk about Mississippi, Trump, and the coolest job in the world.... (<a href="https://youtube.com/watch?v=JQI1KlXHguY">watch</a> || <a href="/videos/2020/06/28/Lets_talk_about_Mississippi_Trump_and_the_coolest_job_in_the_world">transcript &amp; editable summary</a>)

Beau describes how Trump's actions served as a stress test, leading to positive changes in Mississippi's flag and conservative attitudes, showcasing a shift towards progress and inclusivity in the South.

</summary>

"It does not get more Old South than Mississippi."
"This is the South rising again."
"People have talked about the South rising again. This is it."
"He's the one that brought everybody out."
"I want to thank Donald Trump for providing that stress test."

### AI summary (High error rate! Edit errors on video page)

Describes the role of an "op-4" as the coolest job in the world, involving disrupting security systems to expose flaws.
Credits President Trump for playing the role of "op-for" in the U.S. by bringing flaws to the surface and providing a starting point for improvement.
Reads a message from the Mississippi Historical Society supporting changing the state flag due to its divisive Confederate imagery.
Notes the Mississippi legislature's decision to remove Confederate symbols from the state flag, signaling a significant change.
Emphasizes that this move was mainly supported by Republicans and conservatives in Mississippi, showcasing a desire for progress and inclusivity.
Views this change in Mississippi as a significant step forward, indicating the South's readiness to embrace progress and move beyond the past.
Acknowledges the surprising nature of this change, especially considering past resistance from the establishment in Mississippi.
Thanks Trump for bringing attention to the issue and praises conservatives in Mississippi for taking steps towards positive change.

Actions:

for southern residents, activists,
Support the removal of divisive symbols in your community (exemplified)
Advocate for inclusivity and progress in your region (exemplified)
Acknowledge and address historical flaws for a better future (exemplified)
</details>
<details>
<summary>
2020-06-27: Let's talk about Trump, believability, and the allegation.... (<a href="https://youtube.com/watch?v=xw1YyMJ6NUg">watch</a> || <a href="/videos/2020/06/27/Lets_talk_about_Trump_believability_and_the_allegation">transcript &amp; editable summary</a>)

President faced wild allegation of inaction amid Russian intel on attacks against US soldiers; his history makes the disturbingly believable allegation enough for resignation or non-re-election.

</summary>

"It is completely believable that Russian intelligence would do this. It is completely believable that Trump would look the other way."
"The president of the United States does not have the confidence of the American people."
"If the Republican Party hands him the nomination, they own this."

### AI summary (High error rate! Edit errors on video page)

President of the United States faced wild allegation involving Russian intelligence incentivizing attacks against American soldiers in Afghanistan.
Legal definition of treason in the US is narrow; looking the other way during attacks isn't legally treasonous.
People are justifying President's inaction with excuses, rather than denying the allegation's truth.
Russia's motive could be to derail peace deals and weaken the US military by incentivizing attacks.
Allegation's truth doesn't matter; what's relevant is the President's history of self-centered actions benefiting Russia.
American people lack confidence in the President; the allegation should be unbelievable but is disturbingly believable.
The allegation's believability should be reason enough for resignation or non-re-election.
Republican Party's support for the President implicates them in his actions, including this latest allegation.
President mobilized to protect statues of racists but didn't act to protect American soldiers in Afghanistan over months.
President's track record of lies makes it plausible he'd allow soldiers to die to avoid admitting fault or losing support.

Actions:

for american voters,
Pressure elected officials to hold the President accountable (implied)
Stay informed and engaged in politics to ensure accountability (implied)
</details>
<details>
<summary>
2020-06-26: Let's talk about the term "Dixie".... (<a href="https://youtube.com/watch?v=F2s7brvyYa8">watch</a> || <a href="/videos/2020/06/26/Lets_talk_about_the_term_Dixie">transcript &amp; editable summary</a>)

Beau navigates the complex origins and evolving meanings of the term "Dixie," prompting reflection on its potential racial implications in different contexts within the Southern region.

</summary>

"The term is very fluid."
"I have no idea whether or not the perception of it from black Americans is racist."
"There are certain areas that seem like entirely different countries within the South."

### AI summary (High error rate! Edit errors on video page)

Exploring the origin of the term "Dixie" in the context of a group wanting to create a left-leaning space in the South.
Three theories on the origin of the term: a slave owner named Dixie, the Mason-Dixon line, and French banknotes in Louisiana.
Beau dismisses the theory of a benevolent slave owner named Dixie and finds the French banknotes theory the most plausible.
The term "Dixie" became popular during the Civil War due to a song called "Dixie," written by a man from Ohio, Daniel Emmett.
Emmett, the writer of the song, had racist tendencies by liking blackface, although he was not a Confederate.
The term "Dixie" had racist connotations during its popularity and a resurgence in the 1960s.
The term is fluid in meaning and can be non-racist when referring to certain activities or geography.
Beau questions whether using the term "Dixie" to market a left-leaning group in the South may attract unintended individuals.
Beau encourages feedback and comments on the topic to gain diverse perspectives.

Actions:

for southern activists,
Research the historical context behind terms before using them (suggested)
Seek diverse perspectives and feedback on potentially sensitive terms (suggested)
</details>
<details>
<summary>
2020-06-26: Let's talk about climate and Trump's allies in Florida and Texas.... (<a href="https://youtube.com/watch?v=-DcXXv-LwVg">watch</a> || <a href="/videos/2020/06/26/Lets_talk_about_climate_and_Trump_s_allies_in_Florida_and_Texas">transcript &amp; editable summary</a>)

Failed reopenings show the danger of prioritizing profit over lives, urging action against climate change before irreversible damage.

</summary>

"We have to stop listening to the people who put profits over people's lives."
"We have one planet and the governors and leaders will not be able to do anything once it gets past a certain point."

### AI summary (High error rate! Edit errors on video page)

Explains the aftermath of failed reopenings in states like Florida and Texas led by President Trump's allies.
Points out how politicians, influenced by economic power, ignored scientists' advice and pushed for early reopening.
Describes the pattern of creating counter-narratives against scientific consensus in various issues, like climate change.
Emphasizes that unlike rolling back reopenings, there's no turning off the irreversible effects of climate change once it reaches a critical point.
Urges to prioritize addressing climate change over playing into the culture war led by profit-driven politicians.

Actions:

for climate activists, environmentalists, advocates,
Push for policies that prioritize environmental sustainability and address climate change (suggested)
Support and vote for leaders who prioritize science and people over profit-driven interests (implied)
</details>
<details>
<summary>
2020-06-25: Let's talk about history, culture, and moving America forward.... (<a href="https://youtube.com/watch?v=xSLhJpj0gXs">watch</a> || <a href="/videos/2020/06/25/Lets_talk_about_history_culture_and_moving_America_forward">transcript &amp; editable summary</a>)

Beau addresses the need to rebuild history by learning from its mistakes and replacing controversial statues with monuments that make us proud.

</summary>

"Freedom will crash through anything eventually."
"We have to change it all."
"Let's rebuild history."
"Make America great."
"They're embarrassing to the country. Take them down."

### AI summary (High error rate! Edit errors on video page)

Addressing the concern of erasing history by putting disclaimers in front of movies, not idolizing certain songs and cartoons, or taking down statues.
Proposing a need to rebuild history to benefit from the warnings it provides.
Sharing a piece of the Berlin Wall as a symbol of history in the making and a celebration of freedom.
Acknowledging the necessity of warnings in history, especially in light of recent events.
Suggesting replacing statues of slave owners with monuments to abolitionists like Harriet Tubman and John Brown.
Emphasizing the importance of learning from history's mistakes and not romanticizing or mythologizing it.
Pointing out the hypocrisy of those defending racist historical figures while denying the existence of systemic racism today.
Advocating for cultural changes through popular culture to address deep systemic issues.
Urging for a change in mindset alongside changes in laws to create a more inclusive society.
Encouraging people to be on the right side of history by acknowledging and addressing the country's past mistakes.
Calling for the rebuilding of history through monuments that represent positive change and progress.
Arguing that keeping up statues of controversial figures is embarrassing to the country and suggests putting up monuments that make people proud instead.

Actions:

for activists, historians, educators,
Replace statues of controversial figures with monuments to those who have positively impacted history (implied).
Advocate for cultural changes through popular culture by promoting inclusive songs, cartoons, movies, and statues (implied).
</details>
<details>
<summary>
2020-06-25: Let's talk about Splash Mountain and Princess and the Frog.... (<a href="https://youtube.com/watch?v=9G7qe-4ODI4">watch</a> || <a href="/videos/2020/06/25/Lets_talk_about_Splash_Mountain_and_Princess_and_the_Frog">transcript &amp; editable summary</a>)

Disney's decision to rebrand Splash Mountain sparks debate revealing deep-seated racism and resistance to change, challenging individuals to confront their role in upholding systemic issues.

</summary>

"You're the reason we can't move forward."
"If you have an issue with this occurring at a children's park, you are the problem."
"It's what we do. It's how we move forward."
"Is there anybody who thinks the average child today going to Disney would rather have a ride reference Song of the South instead of Princess and the Frog?"
"This is a private company doing what they think is best for their customers."

### AI summary (High error rate! Edit errors on video page)

Disney decided to rebrand Splash Mountain to focus on Princess and the Frog, sparking national debate.
The outrage stems from right-wing sources prompting anger or deep-seated racism.
The original theme of Splash Mountain was inspired by Song of the South, which had problematic imagery.
The rebranding decision is not unusual, as companies frequently update their branding.
Opposition to the change likely stems from discomfort with the removal of racist imagery.
Beau questions why some people are upset over a theme park ride being updated to a more modern and inclusive theme.
He challenges individuals to confront their reasons for resisting change, suggesting it may be rooted in upholding systemic racism.
The debate over a theme park rebranding shows how absurd and divisive public discourse has become.
Beau points out the absurdity of believing that children today prefer references to outdated and problematic content over more contemporary and inclusive themes.
He stresses the importance of evolving as a society and addressing past mistakes to move forward.

Actions:

for theme park visitors,
Support and appreciate efforts by companies to update their branding to be more inclusive (exemplified).
Challenge individuals who resist change and confront them about the root of their opposition (exemplified).
</details>
<details>
<summary>
2020-06-24: Let's talk about a police reform bill blocked by Senate Dems.... (<a href="https://youtube.com/watch?v=c2AnEd-ky9k">watch</a> || <a href="/videos/2020/06/24/Lets_talk_about_a_police_reform_bill_blocked_by_Senate_Dems">transcript &amp; editable summary</a>)

Beau criticizes misleading headlines on Democrats blocking a GOP bill misrepresented as police reform, clarifying the lack of true reform efforts and the importance of satisfying police accountability groups.

</summary>

"Republicans attempt to mislead constituents."
"If it doesn't satisfy Black Lives Matter, if it doesn't satisfy the police accountability groups, if it doesn't satisfy those people in the streets, it doesn't satisfy me."

### AI summary (High error rate! Edit errors on video page)

Criticizes misleading headlines on Democrats blocking police reform bills.
Republican bill misrepresented as police reform actually incentivizes no-knock raids, chokeholds, and addresses lynching.
Democrats blocked the Republican bill due to lack of real reform.
Democrats have their own bill banning chokeholds, no-knock raids in federal drug warrants, and limiting qualified immunity.
Points out the importance of having a police misconduct database in reform bills.
Emphasizes the need to be cautious of mainstream narratives on police reform and prioritize satisfying Black Lives Matter and police accountability groups.

Actions:

for reform advocates,
Contact your representatives to push for comprehensive police reform bills (suggested)
Join local advocacy groups working towards police accountability (implied)
</details>
<details>
<summary>
2020-06-24: Let's talk about Trump, flags, and fire.... (<a href="https://youtube.com/watch?v=HhyuXhJ5WNM">watch</a> || <a href="/videos/2020/06/24/Lets_talk_about_Trump_flags_and_fire">transcript &amp; editable summary</a>)

Beau questions the true symbolism of the American flag, criticizing shallow patriotism and political theatrics over real societal issues.

</summary>

"Those people who love the symbol more than what the symbol is supposed to represent."
"It's just a thought."
"If you're not fighting to keep those promises, how can you really care about the symbol of those promises?"
"The logo, the American flag, doesn't mean as much anymore because of people like him."
"Not let's make my country right."

### AI summary (High error rate! Edit errors on video page)

Questions the symbolism behind a proposed constitutional amendment banning flag burning.
Challenges the idea that the flag symbolizes freedom and the American way of life.
Argues that the founding fathers' concept of "country" was different from a nationalist perspective.
Emphasizes that the flag should symbolize promises like freedom, liberty, and justice for all.
Criticizes the lack of progress in fulfilling these promises and addressing injustices in society.
Points out that burning the flag can hold more meaning than merely displaying it.
Asserts that true patriotism involves working towards equal rights and helping the marginalized.
Condemns politicians using flag-related issues for political gain rather than focusing on real societal problems.
Calls out the hypocrisy of prioritizing flag protection over addressing pressing issues like social reform.
Expresses disappointment in the diminishing significance of the flag due to shallow patriotism and political theatrics.

Actions:

for patriotic americans,
Challenge shallow patriotism by actively working towards equal rights and helping marginalized communities (implied).
Resist political theatrics and focus on pressing societal issues such as social reform (implied).
</details>
<details>
<summary>
2020-06-23: Let's talk about feelings over America's past.... (<a href="https://youtube.com/watch?v=WOaPr2KUb4E">watch</a> || <a href="/videos/2020/06/23/Lets_talk_about_feelings_over_America_s_past">transcript &amp; editable summary</a>)

Addressing the sentiment of not wanting to feel guilty, Beau's analogy of fixing a broken door stresses the importance of taking action over dwelling on guilt in societal issues.

</summary>

"Nobody cares if you feel guilty."
"They just wanted the door fixed."
"Maybe what's important is fixing the door."
"Let's just fix the door."
"Once we fix the door, we can move on."

### AI summary (High error rate! Edit errors on video page)

Addressing a common sentiment among some individuals who believe they shouldn't have to feel guilty about current events in the country.
Beau shares a personal story about his son breaking a cabinet door and feeling guilty about it.
Despite his son feeling remorse, Beau emphasized the importance of fixing the broken door.
Drawing parallels between fixing a broken door and addressing societal issues, Beau stresses the need to focus on solutions rather than guilt.
Beau mentions his interactions with activists and points out that they prioritize action over people feeling guilty.
He likens the current societal situation to being on the way to the hardware store to fix the door, indicating that addressing issues is necessary before moving forward.

Actions:

for individuals reflecting on their role in addressing societal problems.,
Fix a tangible issue in your community (implied).
Focus on concrete solutions rather than dwelling on guilt (implied).
</details>
<details>
<summary>
2020-06-23: Let's talk about a study about American law enforcement.... (<a href="https://youtube.com/watch?v=p3bFJ0FOtjA">watch</a> || <a href="/videos/2020/06/23/Lets_talk_about_a_study_about_American_law_enforcement">transcript &amp; editable summary</a>)

Beau explains the necessity of police reform through a study revealing the lack of legal requirements for departments to comply with basic standards, providing recommendations for necessary changes.

</summary>

"Is this whole movement out in the streets, is this even necessary?"
"Getting rid of no knock raids, requiring lethal force to be as a last resort rather than the officer was scared."
"A lot of people need self-interest."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of reform and drastic change in policing.
Mentions a study from the University of Chicago Law School conducted between 2015-2018 focusing on 20 major US cities.
The study, named Deadly Discretion, assessed whether these cities were in compliance with basic human rights law.
Out of the four main categories - accountability, proportionality, necessity, and legality - no department was legally required to maintain compliance with basic standards.
Provides recommendations from the study, such as ending no-knock raids, using lethal force as a last resort, and eliminating qualified immunity.
Suggests that the study is easily digestible, clear, and can help sway individuals who may not understand the experiences of dealing with law enforcement.
Emphasizes the need for everyone to be interested in these recommendations, as compliance is not a legal requirement for any city.
Encourages viewers to keep the study handy for educating others and facilitating informed discussions.

Actions:

for advocates for police reform,
Share the study "Deadly Discretion" and its recommendations with others to raise awareness (suggested).
Advocate for the implementation of recommendations from the study in local law enforcement policies (implied).
</details>
<details>
<summary>
2020-06-22: Let's talk about which statue is next.... (<a href="https://youtube.com/watch?v=6LjXcobjnpQ">watch</a> || <a href="/videos/2020/06/22/Lets_talk_about_which_statue_is_next">transcript &amp; editable summary</a>)

Beau delves into the systemic issues behind statue removals, using Teddy Roosevelt as a lens to challenge perspectives and advocate for progress.

</summary>

"Hidden in this question, which statue is next, is the admission of systemic racism."
"Taking down these statues, it's not going to change the world. It's one more thing."
"He was about change. He was about what you as a person did."
"It's really important to help other people get up, not hold them down."
"And strong men, tough men, men like Teddy Roosevelt, they weren't afraid of the competition."

### AI summary (High error rate! Edit errors on video page)

Raises the question of which statue is next, signaling a shift in perspective.
Engages in a meaningful exchange with someone seeking to understand systemic issues.
Dissects the reluctance to acknowledge systemic racism, using Teddy Roosevelt as an example.
Points out the flaws of historical figures like Teddy Roosevelt while acknowledging their positive aspects.
Emphasizes the need to confront the systemic issues represented by certain statues.
Advocates for taking down statues that perpetuate institutional issues.
Contrasts the historical context of figures like Teddy Roosevelt with modern values.
Stresses the importance of continuous progress and growth as a society.
Encourages focusing on individual achievements and embracing change rather than clinging to tradition.
Reminds viewers of Teddy Roosevelt's stance on criticism and the importance of building a better future for the country.

Actions:

for viewers, activists, educators,
Examine statues in your community that may perpetuate institutional issues and advocate for their removal (implied).
Engage in constructive dialogues about systemic racism and historical figures to foster understanding and progress (implied).
</details>
<details>
<summary>
2020-06-21: Let's talk about Trump's Tulsa rally and what we're missing.... (<a href="https://youtube.com/watch?v=aB1JuEbiVDY">watch</a> || <a href="/videos/2020/06/21/Lets_talk_about_Trump_s_Tulsa_rally_and_what_we_re_missing">transcript &amp; editable summary</a>)

Beau addresses the serious implications of Trump's Tulsa rally, where his ego was prioritized over public safety, jeopardizing his supporters and showcasing his lack of leadership.

</summary>

"He jeopardized his biggest supporters."
"He could have protected the country, but he chose not to for a photo op."
"That's embarrassing."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the recent events in Tulsa, focusing on the importance of discussing the serious aspects rather than making jokes.
The controversy surrounding the date and location of the event is mentioned, especially considering the current health concerns of gathering people closely.
Trump exaggerated the expected crowd size, claiming a million people were interested when only 6,200 showed up.
The discrepancy in numbers was due to younger Americans trolling by requesting tickets with multiple email addresses, leading to an inflated count.
Despite the low turnout, the attendees were packed closely together without social distancing measures, risking their health for a photo op.
Beau criticizes Trump for prioritizing his ego over the safety of his supporters and the country, showcasing his lack of leadership qualities and disregard for public health.
The rally's failure is attributed to Trump's unwillingness to prioritize safety over appearances, ultimately showing his true priorities and character.

Actions:

for politically engaged individuals,
Contact local representatives to advocate for responsible and safe public events (implied)
Organize community initiatives promoting public health and safety during gatherings (implied)
</details>
<details>
<summary>
2020-06-20: Let's talk about statues, heritage, and history.... (<a href="https://youtube.com/watch?v=VL5O_RbHhxI">watch</a> || <a href="/videos/2020/06/20/Lets_talk_about_statues_heritage_and_history">transcript &amp; editable summary</a>)

Many statues mythologize rather than depict history, hindering progress; real American history is about embracing change and moving forward.

</summary>

"These statues aren't about history."
"Our most important heritage is the machinery for change."
"It's time to move forward."
"You're the person holding it back."
"We should probably focus more on American history, real history, rather than mythology."

### AI summary (High error rate! Edit errors on video page)

Many statues are not historically significant or accurate, but rather mythologize and create a false narrative about American leaders.
Statues in the South were often erected to send a message of dominance and segregation.
American mythology, perpetuated by these statues, fails to address the country's problems and can lead to unrest.
Beau mentions the example of Albert Pike, a Confederate officer whose statues downplay his controversial past.
While mythologizing historical figures is okay to an extent, it must not be confused with history.
Thomas Jefferson, despite his flaws, played a significant role in American history and should be remembered accurately.
The U.S. was founded on flawed principles, but its strength lies in the ability to change and evolve.
Preserving statues that are being taken down can serve as a reminder of a pivotal moment in American history.
Beau stresses the importance of embracing change and moving forward as a nation.
He advocates for a focus on real American history rather than mythological narratives.

Actions:

for history enthusiasts, activists,
Preserve statues being taken down for historical significance (suggested)
Focus on teaching real American history in educational settings (implied)
</details>
<details>
<summary>
2020-06-19: Let's talk about a lesson from the University of Florida for all of us.... (<a href="https://youtube.com/watch?v=vN1m734i8ec">watch</a> || <a href="/videos/2020/06/19/Lets_talk_about_a_lesson_from_the_University_of_Florida_for_all_of_us">transcript &amp; editable summary</a>)

Beau explains the importance of acknowledging history, making small changes, and moving forward for a better experience at UF and in the country.

</summary>

"Let the small change happen. Enhance everybody's experience."
"Little baby changes that can add up."
"Don't be the person that stands in the way of that just for tradition's sake."
"Let's move forward at UF. Let's move forward in this country."
"Small changes. A bunch of them. By themselves, they mean nothing. But together, it's a revolution."

### AI summary (High error rate! Edit errors on video page)

Explains the perception, association, and tradition lesson from the University of Florida.
Conducts a demonstration using military contractors to illustrate a point about perception and association.
Talks about the decision at the University of Florida to stop using a specific cheer due to its term.
Describes the origins of the cheer and how it may evoke different connotations for different people.
Emphasizes the importance of considering history and perceptions in making small changes for a better experience.
Urges for moving forward and honoring more forward-thinking traditions.
Advocates for acknowledging different perspectives and making small changes to enhance everyone's experience.
Suggests that small changes, when combined, can lead to a significant shift in societal norms.
Encourages embracing change and not hindering progress for the sake of tradition.

Actions:

for students, alumni, community members,
Support the decision to drop offensive cheers at local events (exemplified)
Advocate for small changes to enhance experiences in your community (suggested)
</details>
<details>
<summary>
2020-06-19: Let's talk about Gil Scott-Heron and it being televised.... (<a href="https://youtube.com/watch?v=Q-gy4BFvaUA">watch</a> || <a href="/videos/2020/06/19/Lets_talk_about_Gil_Scott-Heron_and_it_being_televised">transcript &amp; editable summary</a>)

Exploring the true meaning behind "the revolution will not be televised" by Gil Scott Heron and advocating for active participation and unity to drive real change.

</summary>

"The revolution will not be televised."
"The first change that takes place is in your mind."
"If you want real change, if you want revolution, it's going to occur with us."

### AI summary (High error rate! Edit errors on video page)

Exploring the meaning behind a popular line from a song that has become a touchstone in popular culture.
The line "the revolution will not be televised" is often used as a battle cry against network news censorship, but the true meaning may have been lost.
Beau delves into the original intention behind the line by Gil Scott Heron and its significance.
Contrary to common belief, the phrase suggests active participation in the revolution rather than lack of media coverage.
The song conveys the message that real change starts with a shift in individuals' minds before it manifests in actions and movements.
Emphasizes the importance of people coming together, being in sync, and not allowing the media or establishment to control the narrative.
Advocates for unity, communication, and active engagement to bring about meaningful change.
Encourages disregarding divisive narratives and focusing on collective evolution and progress.
The message is to reject mainstream narratives, unite with others, and drive change from within, not relying on external sources.
Beau stresses the relevance of these ideas in the present context and the power of united action for genuine transformation.

Actions:

for activists, community members,
Unite with others in your community to drive meaningful change (exemplified)
Reject divisive narratives and focus on collective evolution (implied)
Communicate effectively with others to stay in sync and bring about positive transformations (exemplified)
</details>
<details>
<summary>
2020-06-18: Let's talk about red triangles and white roses.... (<a href="https://youtube.com/watch?v=U2C8hUE7zVM">watch</a> || <a href="/videos/2020/06/18/Lets_talk_about_red_triangles_and_white_roses">transcript &amp; editable summary</a>)

President Trump's use of a red triangle reveals historical parallels, urging us to address his failures and meet his supporters where they're at to combat his divisive tactics.

</summary>

"He wanted to make America great again. The country's on fire."
"He's a loser. He's always been a loser and he's always going to be a loser."
"He can't even racist right."
"Be like Sophie."
"It's just a thought. Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

President of the United States ran 88 Facebook ads featuring a red triangle to identify his opposition.
The red triangle historically was reserved for those who spoke out against the administration in Germany and rescued Jews.
Trump wants to identify his enemies using the red triangle, drawing parallels to a dark historical context.
Beau acknowledges taking inspiration from Sophie Scholl and the White Rose Society in meeting people where they're at.
Trump seeks undying loyalty like certain supporters of the German regime during the war.
Despite Trump's efforts to draw lineage, there is a fundamental difference in effectiveness between him and the German dictator.
Trump's failures are evident in his inability to deliver on promises and manage various aspects of governance.
Beau stresses the importance of pointing out Trump's failures repeatedly to reach his remaining supporters.
Trump's appeal to uneducated voters who prioritize winning over moral character is a key aspect of his strategy.
The need to continuously label Trump as a failure and point out his shortcomings to make his true nature clear.
Beau advocates for meeting people where they're at, like Sophie Scholl, to effectively communicate with those who support Trump.
Emphasizing Trump's incompetence and inability to fulfill promises is a critical strategy in addressing his supporters.
The significance of Facebook removing the ads featuring the red triangle, exposing Trump's failed attempt at a dog whistle.
Beau underscores the importance of consistently showcasing Trump's shortcomings and failures to counter his appeal to certain supporters.
Encouraging the audience to be resilient in addressing Trump's inadequacies and communicating effectively with his supporters.

Actions:

for political activists, concerned citizens,
Point out Trump's failures and shortcomings to his supporters (implied)
Advocate for meeting people where they're at in political discourse (exemplified)
Communicate effectively with those who support Trump by addressing his failures (implied)
</details>
<details>
<summary>
2020-06-18: Let's talk about police work stoppages, call outs, and "blue flu".... (<a href="https://youtube.com/watch?v=S2D85BrFGpw">watch</a> || <a href="/videos/2020/06/18/Lets_talk_about_police_work_stoppages_call_outs_and_blue_flu">transcript &amp; editable summary</a>)

Law enforcement's "blue flu" actions inadvertently support criticisms from the Black Lives Matter movement, revealing flaws in the criminal justice system and the need for potential defunding of certain law enforcement positions.

</summary>

"When the city descends into chaos, let it burn."
"You just proved everything that BLM is saying is true."
"Those positions can be defunded."
"It is still about protect and serve, but you just want to protect and serve yourselves."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of "blue flu," where cops participate in work stoppages or slowdowns to draw attention to perceived injustices.
Mentions the recent incident in Atlanta where the blue flu occurred after a cop was arrested, citing it as the perceived injustice.
Criticizes the mindset behind the blue flu, which puts communities at risk to achieve law enforcement's demands.
Comments on the flawed criminal justice system and how the blue flu is a misguided show of solidarity among officers.
Notes that the actions of law enforcement during the blue flu inadvertently support the complaints of the Black Lives Matter movement regarding the broken criminal justice system and lack of care for communities.
Points out that previous instances of work stoppages or slowdowns by law enforcement have not led to the chaos they predicted, as most people are not in need of excessive policing.
Suggests that the blue flu incident in Atlanta proves that certain law enforcement positions may not be necessary and could be candidates for defunding.
Encourages departments where such actions are considered to proceed, as it may help identify redundant positions within law enforcement.
Advocates for community involvement in identifying unnecessary law enforcement positions through platforms like Facebook groups.
Concludes by critiquing law enforcement's focus on self-preservation rather than truly serving the community.

Actions:

for community members, activists, policymakers,
Identify unnecessary law enforcement positions for potential defunding (suggested)
Engage in community-led efforts, like Facebook groups, to gather data on law enforcement numbers (suggested)
</details>
<details>
<summary>
2020-06-17: Let's talk about Officer Stacey and McMuffins.... (<a href="https://youtube.com/watch?v=Tc8YwzrKzuw">watch</a> || <a href="/videos/2020/06/17/Lets_talk_about_Officer_Stacey_and_McMuffins">transcript &amp; editable summary</a>)

Beau expresses anxiety in a fast food line, drawing parallels between his experience and the fears of black Americans when faced with law enforcement bias.

</summary>

"That anxiety level that that officer has, that may be an indication that she needs to take a leave of absence."
"Imagine if that negative bias had existed for hundreds of years."
"This should be a wake-up moment for law enforcement."
"But to be honest, most of America right now is just like, well, I mean, it's really bad that you fit the description."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau expresses his anxiety while waiting in the fast food line after being asked to pull forward, suspecting that the employees think he is a cop.
He draws a comparison between his anxiety and the fear that black Americans experience when getting pulled over by the police.
Beau questions the anxiety level of an officer waiting for an Egg McMuffin at a fast food joint, suggesting that such anxiety may not be suitable for being armed on the streets.
He advocates for officers with high anxiety levels to take a leave of absence to avoid potentially tragic consequences due to irrational fears.
Beau points out the negative bias law enforcement has faced recently and compares it to the historical bias faced by black Americans for centuries.
He underscores the importance of this moment as a wake-up call for law enforcement to address these biases and anxieties.
Beau mentions the impact of being stereotyped based on appearance and uniform, acknowledging that some officers may be unfairly judged.
He concludes by encouraging law enforcement to recognize the deeper implications of bias and anxiety within their profession.

Actions:

for law enforcement officers,
Take a leave of absence to address high anxiety levels (suggested)
Recognize and address biases within law enforcement (suggested)
</details>
<details>
<summary>
2020-06-17: Let's talk about Lincoln, slavery, the Civil War, and cornerstones.... (<a href="https://youtube.com/watch?v=r_34p9UwjZ4">watch</a> || <a href="/videos/2020/06/17/Lets_talk_about_Lincoln_slavery_the_Civil_War_and_cornerstones">transcript &amp; editable summary</a>)

Beau breaks down the myth surrounding the Civil War, affirming its true cause: slavery.

</summary>

"Most Americans know American mythology, not American history."
"The Civil War was about slavery. You can't argue this."
"The confederates admit that's what it was about."

### AI summary (High error rate! Edit errors on video page)

Historians sometimes assume Americans know American history, but most know American mythology.
The Emancipation Proclamation freed slaves only in rebellious areas, not ending slavery nationwide.
The theory suggests the Proclamation was a political maneuver to deter Europe from supporting the South.
Southern states seceded and opened fire on Fort Sumter due to their strong belief in slavery.
Confederate leaders openly stated that slavery was the cornerstone of their founding documents.
The Vice President of the Confederacy emphasized the belief in white supremacy and slavery's necessity.
The Civil War was inherently about slavery, as evidenced by Confederate documents.
Some still argue the war wasn't about slavery, but historical facts from the Confederate side prove otherwise.

Actions:

for history enthusiasts,
Educate others on the true history of the Civil War by sharing this information (implied).
</details>
<details>
<summary>
2020-06-16: Let's talk about how they need another Martin Luther King.... (<a href="https://youtube.com/watch?v=xjRg7qqeEno">watch</a> || <a href="/videos/2020/06/16/Lets_talk_about_how_they_need_another_Martin_Luther_King">transcript &amp; editable summary</a>)

People calling for another Martin Luther King misunderstand the need for personal responsibility in creating change and the urgency within marginalized communities.

</summary>

"Because in the final analysis, riots are the language of the unheard."
"If you don't like what I just said, you don't really want another Martin Luther King."
"There comes a time when silence is betrayal."

### AI summary (High error rate! Edit errors on video page)

People keep calling for another Martin Luther King, but he argues that it's not about needing a leader like MLK, but rather people who look like him taking responsibility to bring about change.
Acknowledges that some may be speaking out for social credit, but points out that speaking out is often a vocation of agony and a necessary action.
Notes the urgency within the black community who believe that white America determines how long change will take, leading to continual protests.
Beau points out that riots don't happen out of thin air and stem from certain societal conditions that need to be condemned just as vigorously as the riots themselves.
Urges people to listen to what is being said and understand that riots are often the result of being unheard.
Emphasizes that Martin Luther King preached nonviolence, not non-confrontation, citing his work in Letters from Birmingham Jail as an example of his willingness to challenge the system.
Compares current protests to historical lunch counter protests, indicating that the struggle for equality continues and those staying silent are on the wrong side of history.

Actions:

for activists, allies, community members,
Join protests and demonstrations (implied)
Speak out against societal conditions that lead to unrest (implied)
Support marginalized voices and communities (implied)
</details>
<details>
<summary>
2020-06-15: Let's talk about bias in today's discussions.... (<a href="https://youtube.com/watch?v=LVlE0fu7E54">watch</a> || <a href="/videos/2020/06/15/Lets_talk_about_bias_in_today_s_discussions">transcript &amp; editable summary</a>)

Beau explains the misuse of statistics to establish causal relationships between demographics and crime, and calls for consistency in evaluating police actions and examining personal biases.

</summary>

"If you believe somebody's skin tone will increase their likelihood of being a criminal, that's racist."
"Facts aren't racist. People are."
"The device does not become more lethal simply because the scary black man is holding it."
"We don't want facts to get in the way of a good story."
"Now is the time to see where you really fit in. What your moral fiber is. What your biases are."

### AI summary (High error rate! Edit errors on video page)

Explains that the channel is interactive based on viewer comments and ethical considerations.
Points out the misuse of statistics from the FBI's Uniform Crime Reporting Program.
Clarifies that the statistics show demographics suspected or arrested, not actual criminals.
Addresses the misconception of linking demographics to predisposition for crime as racist.
Suggests adjusting data for poverty levels to reveal racial disparities disappear.
Emphasizes that facts aren't racist, but people can be.
Talks about the debate on the lethality of tasers and the need for consistency in terminology.
Argues against justifying lethal force based on vague "what-if" scenarios.
Warns against granting officers the power for summary execution based on unreasonable scenarios.
Encourages self-examination of beliefs, morals, and biases in light of current events.

Actions:

for viewers, activists, educators,
Examine your beliefs and biases (suggested)
Advocate for consistent terminology in evaluating police actions (implied)
Educate others on statistics and biases (implied)
</details>
<details>
<summary>
2020-06-14: Let's talk about police, journalists, and a phone call.... (<a href="https://youtube.com/watch?v=S6lxlgzvgpU">watch</a> || <a href="/videos/2020/06/14/Lets_talk_about_police_journalists_and_a_phone_call">transcript &amp; editable summary</a>)

Beau addresses the need for systemic reform in law enforcement, expressing frustration over the failure to understand the necessity for change, and recalling cases of unarmed individuals killed by the police.

</summary>

"There are so many unarmed people killed by police, journalists can't even keep track."
"We might want to take that as a sign that we need to change the system."
"I think the easiest way to do it is to illustrate something that actually happened."

### AI summary (High error rate! Edit errors on video page)

Addressing the need for systemic reform and change in law enforcement culture.
Describing the frustration of trying to explain the necessity for deep systemic change in law enforcement to others.
Expressing anger and frustration when others try to excuse the flaws in law enforcement despite presenting statistics and evidence.
Proposing to illustrate the need for systemic change by dissecting a specific case.
Recalling various cases of unarmed individuals killed by police officers, struggling to identify a specific case mentioned in an article.
Ending with the acknowledgment of numerous unarmed people killed by the police and suggesting a need for system change.

Actions:

for activists, advocates, citizens,
Advocate for systemic reform in law enforcement (suggested)
Educate others on the importance of systemic change in law enforcement (suggested)
</details>
<details>
<summary>
2020-06-14: Let's talk about Atlanta, Wendy's, and the Constitution.... (<a href="https://youtube.com/watch?v=UUufUvy9LZ4">watch</a> || <a href="/videos/2020/06/14/Lets_talk_about_Atlanta_Wendy_s_and_the_Constitution">transcript &amp; editable summary</a>)

Beau challenges the justification of lethal force by law enforcement and criticizes the lack of accountability, pointing out the dangers of authoritarianism and the importance of upholding due process for all.

</summary>

"Resistance alone is not enough for the use of lethal force."
"If lethal force is justified, everything else is."
"The premise that leads to authoritarianism."
"There was no reason to do this."
"Maybe all of these patriots should actually read the Constitution."

### AI summary (High error rate! Edit errors on video page)

Viral videos from 2018 on police tactics are still relevant due to the lack of change in training practices.
Institutions in the US are reluctant to change, reform, or apply best practices in law enforcement.
Law enforcement is trained to get the public to excuse, justify, or look away from their actions.
Beau addresses the incident at Wendy's in Atlanta in relation to the US Constitution and due process.
Due process is a fundamental concept enshrined in the Constitution, except in cases of immediate threat to life or bodily harm.
Resistance alone does not warrant the use of lethal force by law enforcement according to Beau.
Beau recounts the events at Wendy's where a man was shot by law enforcement after resisting arrest and running.
The argument that law enforcement was justified in using lethal force because the suspect had a taser is challenged by Beau.
Beau questions the justification of lethal force based on the presence of a non-lethal weapon being used against the suspect.
Beau criticizes the idea that law enforcement officers can use lethal force without being held accountable, leading to authoritarianism.

Actions:

for activists, reformers, advocates,
Read and understand the US Constitution (suggested)
Advocate for accountability and transparency in law enforcement practices (implied)
</details>
<details>
<summary>
2020-06-13: Let's talk about the fruit of freedom.... (<a href="https://youtube.com/watch?v=o01KLrual3E">watch</a> || <a href="/videos/2020/06/13/Lets_talk_about_the_fruit_of_freedom">transcript &amp; editable summary</a>)

Beau sheds light on the historical significance of black individuals growing watermelons post-slavery and condemns the racist stereotypes attached to it.

</summary>

"Watermelon should be the fruit of freedom."
"It's been turned into a stereotype, and when I imagine that if I was black, I don't know that I would want to eat watermelon in public because of the stereotype."
"Those jokes need to go away, not just because they're racist and they're stupid, but because it should be something that they could take pride in."

### AI summary (High error rate! Edit errors on video page)

Beau addresses historical facts and events that are often overlooked or not given enough coverage.
Public school teachers can suggest topics for Beau to cover, regardless of his comfort level.
Freedom can be intimidating, leading individuals to stick with what is familiar and predictable.
The topic of black people and watermelons is discussed, shedding light on the historical significance.
After slavery ended, many newly freed individuals turned to growing watermelons as a means of sustenance and income.
Growing watermelons provided hope and a chance for advancement for these individuals.
Watermelon should symbolize pride and freedom for black people, but it was twisted into a derogatory stereotype.
The perpetuation of jokes and stereotypes about watermelons robbed black individuals of a source of pride.
The tactic of using one group to look down upon to elevate others is a manipulation by those in power.
Beau condemns the racist jokes and stereotypes surrounding watermelons, advocating for their eradication.

Actions:

for history enthusiasts, educators, activists,
Challenge and confront racist jokes and stereotypes (implied)
Educate others about the historical significance of watermelons for black individuals (implied)
Support and uplift black voices and narratives (implied)
</details>
<details>
<summary>
2020-06-12: Let's talk about Juneteenth and myths.... (<a href="https://youtube.com/watch?v=vuzXnhQswds">watch</a> || <a href="/videos/2020/06/12/Lets_talk_about_Juneteenth_and_myths">transcript &amp; editable summary</a>)

Juneteenth symbolizes the initial steps into freedom and serves as a reminder for those who choose to ignore necessary changes, with valuable lessons for white individuals to learn from the events.

</summary>

"Freedom is scary, especially if you've never experienced it before."
"Nobody was good to their slaves. If they were, they would have freed them."
"Juneteenth got celebrated. And it continued to be celebrated."
"It's become a symbol of those first scary steps into freedom."
"White folk can learn a whole lot more from the events surrounding Juneteenth than black people can."

### AI summary (High error rate! Edit errors on video page)

Juneteenth is in the news due to the President holding a rally in Tulsa, which raises questions about the message being sent.
The Emancipation Proclamation was issued on January 1st, 1863, but it took until June 19th, 1865, for federal troops to enforce it in Galveston.
The delay in news reaching Galveston had various theories, but the reality is that they did know about the end of slavery.
The enforcement of the proclamation changed the owner-slave relationship to an employer-employee dynamic.
Freedom was a scary concept for many former slaves who chose to stay where they were due to fear and lack of means.
The myth that some slaves stayed because they were treated well is debunked, as nobody was truly good to their slaves.
Juneteenth's celebration fluctuated over the years but gained significance during the Civil Rights Movement.
Juneteenth symbolizes the initial steps into freedom and serves as a reminder for those who choose to ignore necessary changes.
The holiday holds vital lessons for those who ignore signs of change and maintain the status quo.
White individuals can learn more from Juneteenth's events than black individuals, especially regarding the importance of acknowledging and addressing societal issues.

Actions:

for white individuals,
Attend Juneteenth celebrations to show support and learn more about its historical significance (suggested)
Educate oneself and others on the true history of Juneteenth and its importance in American history (implied)
Advocate for the recognition and celebration of Juneteenth in your community and workplace (implied)
</details>
<details>
<summary>
2020-06-11: Let's talk about NASCAR, flags, and heritage.... (<a href="https://youtube.com/watch?v=RfcfoZFDKZU">watch</a> || <a href="/videos/2020/06/11/Lets_talk_about_NASCAR_flags_and_heritage">transcript &amp; editable summary</a>)

NASCAR's bold stance on Confederate flags and police accountability exposes the need for growth and reflection in the face of evolving values.

</summary>

"NASCAR took a moral stand by banning Confederate flags at their events, showing more courage than the President of the United States."
"Failure to evolve over time and adapt to changing norms indicates a lack of personal growth and development."
"If all of your idols, if all of the organizations you like are suddenly turning against you, maybe it's because you didn't keep up with the times."

### AI summary (High error rate! Edit errors on video page)

NASCAR took a moral stand by banning Confederate flags at their events, showing more courage than the President of the United States.
The President dodged addressing the issue involving the names of some U.S. Army installations, opting to pass it on to someone else.
NASCAR has a history of striving to be inclusive as an organization, in contrast to some of its fan base.
The ban on Confederate flags at NASCAR events has stirred controversy, particularly in the South.
NASCAR's roots in hot-rodding and bootlegging tie into a historical defiance against unjust authority, including against taxes and law enforcement.
NASCAR's essence has always been about defunding the police and resisting unjust systems.
People criticizing NASCAR's stance on police accountability fail to recognize the organization's heritage and lineage.
Organizations like NASCAR are no longer pandering to certain audiences and are pushing for growth and change.
Failure to evolve over time and adapt to changing norms indicates a lack of personal growth and development.
Self-reflection is necessary for personal and societal growth, especially when faced with changing ideologies and values.

Actions:

for fans of nascar and advocates for social change.,
Support organizations and companies that take a stand against injustice, like NASCAR (exemplified).
Engage in self-reflection and personal growth to adapt to changing values and ideologies (implied).
</details>
<details>
<summary>
2020-06-10: Let's talk about Trump renaming Army forts.... (<a href="https://youtube.com/watch?v=5bc2oAl93oE">watch</a> || <a href="/videos/2020/06/10/Lets_talk_about_Trump_renaming_Army_forts">transcript &amp; editable summary</a>)

Renaming army installations named after Confederate generals is long overdue, with unanimous support from active duty vets, missed by the Trump administration's lack of courage, leaving soldiers stationed at bases named after oppressors.

</summary>

"They're going to kick that can down the road, let somebody else deal with it, let somebody else make the changes."
"We can move forward. We can give a large portion of soldiers an installation name they can be proud of instead of one they don't want to talk about."
"These guys, the names I gave, these would not be surrendering to the PC crowd."
"The absolute cowardice of the Trump administration is leaving a large number of those serving in the United States Army stationed at installations named after people who fought to keep them seen as less than human."
"That's pretty much peak 2020 right there."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the issue of renaming army installations named after Confederate Civil War generals, which has been a long-standing topic but is now at the forefront.
The Trump administration displayed a lack of courage by refusing to make the changes, opting to delay the decision for someone else to handle.
Active duty vets Beau spoke with showed unanimous agreement that the installations should be renamed, with some debate on how they should be renamed.
Beau suggests renaming Fort Bragg to Fort Charles Beckwith, Fort Gordon to Fort Alvin York, Fort Binning to Fort Robert Rogers, and Fort Campbell to honor the first division commander of the 101st, not the Confederate general.
Beau criticizes the Trump administration for missing an opportunity to show progress and move forward by renaming the installations, instead leaving soldiers stationed at bases named after those who fought to dehumanize them.
He argues that renaming the installations with more fitting names can give soldiers a sense of pride and accurately represent the modern nature of the US Army.

Actions:

for soldiers, activists, politicians,
Rename army installations with names that accurately represent the modern nature of the US Army (suggested).
Advocate for renaming army installations named after Confederate generals to honor more fitting individuals (implied).
</details>
<details>
<summary>
2020-06-09: Let's talk about a march in a small southern town.... (<a href="https://youtube.com/watch?v=pNKUHrZLRzo">watch</a> || <a href="/videos/2020/06/09/Lets_talk_about_a_march_in_a_small_southern_town">transcript &amp; editable summary</a>)

Beau attended a march in a small southern town, challenging stereotypes and finding hope in the inherent goodness of people.

</summary>

"Maybe we should take stock of that and remember not to believe the media stereotypes that they want us to believe."
"Deep down, most people are good. The last few years has brought the worst out of people. But deep down, most people are good."
"Hope may be what we need the most."

### AI summary (High error rate! Edit errors on video page)

Beau attended a march in a small town in North Florida, known for its small population and southern stereotypes.
Despite stereotypes, Beau believed in the goodness of his community and attended the march to challenge those preconceptions.
Law enforcement at the march maintained a positive presence, with minimal interaction and no aggressive behavior.
The community showed support during the march, with businesses providing water, people clapping, honking, and showing positivity.
The march was organized by Black individuals, with a diverse group of participants including white individuals, teachers, college students, and blue-collar workers.
Beau's key takeaway is to not believe in stereotypes about one's own community and to recognize the inherent goodness in people.
The march showcased a positive change in the community, challenging historical perceptions and bringing hope for the future.

Actions:

for community members,
Attend local community events to show support and solidarity (exemplified)
Challenge stereotypes through personal interactions and community engagement (exemplified)
</details>
<details>
<summary>
2020-06-09: Let's talk Trump's tweet about Buffalo.... (<a href="https://youtube.com/watch?v=xdPgX8ssNA4">watch</a> || <a href="/videos/2020/06/09/Lets_talk_Trump_s_tweet_about_Buffalo">transcript &amp; editable summary</a>)

Beau addresses President's baseless claims about a protester and stresses that unwarranted force is never justified.

</summary>

"Even if all the allegations are true, it doesn't justify force."
"None of this allegation is probably true, okay? But even if it was, it does not justify the force."
"We can't just go chasing our tails trying to argue this, because this argument is something that, it doesn't need to take place."

### AI summary (High error rate! Edit errors on video page)

Addressing President's tweet about a protester being a provocateur.
Pointing out the inaccuracy in the President's claim about the protester's actions.
Noting the anger and responses from people regarding the President's statements.
Stating that even if the allegations were true, it still doesn't justify the use of force.
Emphasizing that legality of the protester's actions and lack of justification for force.
Suggesting that the President lacks understanding of when law enforcement can use force.
Criticizing the President for not taking the time to understand appropriate use of force.
Critiquing the tactic of attacking the victim in cases of law enforcement misconduct.
Stating that even if the allegations were true, unwarranted force cannot be justified.
Encouraging to focus on the fact that unwarranted force is never justified.

Actions:

for advocates for justice,
Advocate for transparency and accountability in law enforcement (implied)
</details>
<details>
<summary>
2020-06-08: Let's talk about removing qualified immunity.... (<a href="https://youtube.com/watch?v=kgYuH3SyrGM">watch</a> || <a href="/videos/2020/06/08/Lets_talk_about_removing_qualified_immunity">transcript &amp; editable summary</a>)

Beau supports removing qualified immunity, advocating for officers to respond only to real emergencies to prevent over-policing and improve safety.

</summary>

"We don't want to be over-policed."
"Only respond if it's an actual emergency, if there's a victim."
"Can we please just give freedom a chance?"

### AI summary (High error rate! Edit errors on video page)

Explains qualified immunity as a shield that limits officers' liability, shifting it to taxpayers.
Notes the current momentum to remove qualified immunity and make officers carry insurance.
Defends officers' response that they will only show up for real emergencies if personally liable.
Views officers' unintentional laying of foundation for police reform positively.
Supports the idea of officers responding only to actual emergencies to prevent unnecessary situations.
Suggests officers should limit involvement to emergencies, benefiting both them and the public.
Advocates for giving freedom a chance by reducing over-policing.

Actions:

for advocates for police reform.,
Support initiatives aiming to remove qualified immunity from law enforcement (suggested).
Advocate for policies that encourage officers to respond only to actual emergencies (suggested).
</details>
<details>
<summary>
2020-06-08: Let's talk about if cops are effective.... (<a href="https://youtube.com/watch?v=zQz0YL9X0P8">watch</a> || <a href="/videos/2020/06/08/Lets_talk_about_if_cops_are_effective">transcript &amp; editable summary</a>)

Beau questions the effectiveness of law enforcement, using case clearance rates to show the system's ineffectiveness and advocate for reform.

</summary>

"They're not effective now."
"We need reform. We need it."

### AI summary (High error rate! Edit errors on video page)

Defining effectiveness as producing the desired result or outcome.
Questioning the current effectiveness of law enforcement.
Suggesting to use case clearance rate as a metric rather than conviction rate.
Providing statistics from 2017 to analyze effectiveness.
Homicide clearance rate at 61.6% - not highly effective.
Rape clearance rate at 34.5% - especially low due to underreporting.
Robbery clearance rate at 29.7% - not effective.
Aggravated assault clearance rate at 53.3% - still lacking.
Property crimes like burglary and theft with clearance rates below 20%.
Advocating for reform to focus on crimes with victims and improve effectiveness.
Stating that the current system's ineffectiveness is not a reason to avoid reform.

Actions:

for community members, activists, policymakers,
Advocate for reforms to improve law enforcement effectiveness (implied)
Support initiatives focusing on crimes with victims (implied)
</details>
<details>
<summary>
2020-06-07: Let's talk about parenting and sparking curiosity.... (<a href="https://youtube.com/watch?v=DgM2dSf6jNE">watch</a> || <a href="/videos/2020/06/07/Lets_talk_about_parenting_and_sparking_curiosity">transcript &amp; editable summary</a>)

Beau shares insights on parenting, advocating for instilling a moral compass and sparking curiosity while recognizing the uniqueness of each child.

</summary>

"You can't tell a kid anything ever. You have to show them."
"If you want your kid to be curious, you have to be curious."
"Recognizing and embracing the uniqueness of each child is an important part of parenting."

### AI summary (High error rate! Edit errors on video page)

Discloses receiving an educational sample from a company coincidentally before making a positive video mentioning them.
Shares a message for Mr. Richter on his graduation, acknowledging the challenging times ahead.
Provides insights into parenting, mentioning that he avoids giving specific advice due to his kids' unique personalities.
Emphasizes instilling a moral compass and sparking curiosity in children to make education and career choices easier.
Shares different tactics used to provoke curiosity in his kids, like Minecraft for his eldest and tangible items for his youngest.
Describes giving his son a lunar material necklace from a company called Mini Museum, sparking curiosity and interest in history.
Talks about Mini Museum's collection of small samples, including historical artifacts like a piece of the Enigma and Steve Jobs' turtleneck.
Stresses the importance of leading by example for children to emulate curiosity, reading, and other positive behaviors.
Concludes by suggesting that recognizing and embracing the uniqueness of each child is vital in parenting.

Actions:

for parents, caregivers, educators,
Order educational samples or tangible items related to your child's interests to spark curiosity (suggested).
Lead by example by showing curiosity, reading, and engaging in educational activities in front of children (suggested).
</details>
<details>
<summary>
2020-06-07: Let's talk about coping with a therapist.... (<a href="https://youtube.com/watch?v=FeM3EWSqI9w">watch</a> || <a href="/videos/2020/06/07/Lets_talk_about_coping_with_a_therapist">transcript &amp; editable summary</a>)

Beau and Jody dive into coping mechanisms, mindfulness practices, and the power of community service amid societal challenges.

</summary>

"Anger is a natural response when there's a violation of a boundary, that anxiety is such a human response to feeling threatened."
"If there's a part of you that feels completely hopeless right now, really turning towards that part, finding a way to just meet that part and know that's not all of who we are."
"Trusting that we start with looking at our own responses to things."

### AI summary (High error rate! Edit errors on video page)

Beau talks about coping mechanisms and reaching out for help from Jody Strach, a therapist and mindfulness practitioner.
Jody shares her journey into mindfulness, starting from marriage and family therapy to practicing yoga and meditation.
Jody explains the importance of personal mindfulness practice in being able to help others effectively.
Jody addresses coping with the stress and changes in society, focusing on acknowledging emotions and finding inner strength.
The importance of turning inwards, embracing difficulties, and using mindfulness to navigate challenging situations is discussed.
Jody encourages self-compassion and self-care practices to alleviate stress and anxiety during turbulent times.
Dealing with feelings of hopelessness and paralysis by turning inward, grounding oneself, and taking deep breaths is emphasized.
The concept of community service as a way to connect, support others, and find empowerment is explored.
Jody stresses the significance of looking inward, exploring emotions, and connecting with others to create positive change.
Recommendations for resources like Deborah Edenthal's work and Ruth King's book "Mindful of Race" are shared for further exploration.

Actions:

for individuals seeking coping strategies and inner strength.,
Connect with a local community service group to volunteer and support those in need (suggested).
Practice self-compassion and mindfulness techniques in daily life to alleviate stress (exemplified).
Read "Mindful of Race" by Ruth King for insights on navigating racial issues (suggested).
</details>
<details>
<summary>
2020-06-06: Let's talk about why this D-Day anniversary is so important.... (<a href="https://youtube.com/watch?v=-ubBUl7Ja4U">watch</a> || <a href="/videos/2020/06/06/Lets_talk_about_why_this_D-Day_anniversary_is_so_important">transcript &amp; editable summary</a>)

Beau contemplates the chaos of current events, the need for leadership, and the importance of supporting the fight against injustice, culminating in a commitment to back Black Lives Matter at a march.

</summary>

"It was a turning point for the country."
"It is a turning point, if we're smart enough to allow it to be."
"They don't need a leader, we do."
"Black lives matter."
"Eventually, changes will be made."

### AI summary (High error rate! Edit errors on video page)

Commemorating D-Day in the US annually, glorifying it, and its significance as a turning point for the country.
Lack of clear knowledge about the casualties on D-Day due to the chaos of the events.
Thousands of Americans sacrificing themselves on D-Day against tyranny and injustice.
Drawing parallels between the chaos of current events and the sacrifices made on D-Day.
The importance of allowing the current chaos to be a turning point in history.
Mention of Martin Luther King Jr. as a catalyst for the Civil Rights Movement and his role in spreading awareness.
Encouraging to research the Long Hot Summer and its relation to civil rights.
The need for a leader like Martin Luther King Jr. in the current movement against injustice.
Reference to the controversy surrounding a football player protesting during the national anthem.
Criticism of those who ignored the message then and now criticize those who take a knee for justice.
Acknowledgment that change requires more than just 12% of the population to act.
Emphasizing the importance of supporting and amplifying voices fighting against injustice.
Urging action to address the systemic issues creating injustice in the country.
Commitment to join a march supporting black lives matter and being a witness to history.
Ending with a reminder that change is necessary to prevent cycling back to injustice.

Actions:

for activists, supporters, allies,
Attend marches supporting movements against injustice (exemplified)
Be a witness to history by actively participating in events supporting marginalized communities (exemplified)
</details>
<details>
<summary>
2020-06-05: Let's talk about how we've seen enough.... (<a href="https://youtube.com/watch?v=rgGORxzuoxQ">watch</a> || <a href="/videos/2020/06/05/Lets_talk_about_how_we_ve_seen_enough">transcript &amp; editable summary</a>)

Beau challenges concerns about law enforcement reforms and supports drastic change, asserting that quitting over refusing to adapt to new policies is warranted.

</summary>

"If they're gonna quit because new policies are coming along that are gonna stop them from engaging in excessive force and hurting people, they shouldn't be cops."
"We need drastic change, and I will defer to the experts on what that change needs to be."
"It's exactly what should happen because we have seen enough."

### AI summary (High error rate! Edit errors on video page)

People are expressing they've "seen enough" in response to footage from across the United States, leading to forward-thinking ideas for law enforcement reform.
Suggestions range from drastic reforms to possible abolition, replacing law enforcement duties with social workers.
Beau doesn't advocate for any particular idea but addresses common questions: what if cops quit with reforms, and what if reforms go too far?
Beau dismisses concerns about cops quitting due to reforms, saying it's a reason to celebrate if they do because they shouldn't be cops if they resist positive change.
He challenges worries about reforms being ineffective compared to the current state of law enforcement ineffectiveness captured in news footage.
Beau believes that the level of force used by law enforcement is unacceptable, criminal in many cases, and far beyond mere unprofessionalism.
He criticizes departments for merely suspending officers involved in misconduct, suggesting that such behavior should result in jail time.
Beau insists on the need for drastic change in law enforcement and defers to experts on determining the specifics.
While unsure of the best solution, Beau is certain that the current situation involving excessive force is entirely wrong.
He supports officers quitting if they can't adapt to the necessary changes, as the public has witnessed more than enough.

Actions:

for law enforcement reform advocates,
Celebrate and support officers quitting if they resist reforms (exemplified)
</details>
<details>
<summary>
2020-06-04: Let's talk about the generals.... (<a href="https://youtube.com/watch?v=pasSXWerTYg">watch</a> || <a href="/videos/2020/06/04/Lets_talk_about_the_generals">transcript &amp; editable summary</a>)

Top military officials break tradition to address political issues with a message of listening and unity against current events.

</summary>

"NCOs say exactly what they mean."
"All of the influential people within the military community who have spoken out, they're all on the same side."
"Listen is not what you'd expect to hear from them if they believed what was going on was a good idea."
"It's worth noting. It's worth paying attention to."
"The idea of the far right disavowing soldiers who have taken a knee with protesters has arisen."

### AI summary (High error rate! Edit errors on video page)

A number of top military officials, including General Thomas, have issued statements, notably using the word "listen," which is significant.
General Thomas, the former commander of Special Operations Command, expressed displeasure at referring to the United States as a "battle space."
The tradition of civilian control of the military usually keeps military officials from speaking out politically, but recent statements indicate a shift.
The Sergeant Major of the Army, the highest-ranking enlisted person in the Army, also made statements about racial inequality that stirred controversy.
The Sergeant Major's concise and direct messages were seen as controversial by some, especially his identification of racial inequality as a key issue.
The use of the word "listen" in the statements by military officials is significant and suggests a message of importance.
Beau notes the significance of these statements by military leaders who could potentially mobilize volunteers with a single tweet.
All the generals and influential figures within the military community who have spoken out are unified in opposing the current situation.
Beau expresses support for military members who have taken a knee with protesters, contrasting it with potential far-right disavowal.
The overarching theme in the statements from military officials is about the importance of listening and paying attention.

Actions:

for military members, activists, community leaders.,
Contact military officials to express support for their statements and encourage further action (suggested).
Engage in open dialogues with community members about racial inequality and the importance of listening (implied).
</details>
<details>
<summary>
2020-06-04: Let's talk about people wearing certain patches.... (<a href="https://youtube.com/watch?v=VKt6mzWa9oE">watch</a> || <a href="/videos/2020/06/04/Lets_talk_about_people_wearing_certain_patches">transcript &amp; editable summary</a>)

Beau explains the origins of the "3%" symbol, criticizes those who misuse it, and suggests law enforcement should avoid morale patches.

</summary>

"If you are currently holding a shield and a club, staring at Americans in the street exercising their free speech and peaceably assembling, and you're attempting to antagonize it and intimidate them and turn it to a situation where it is not peaceable assembly, you are not a threeper."
"You're literally actively standing against what that patch means while wearing it."
"Take that patch off."
"They're not boot lickers. They're the boot."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the origin of the symbol "3%" in the United States, tied to historical inaccuracies about the colonists.
Mentions that groups using the "3%" symbol are decentralized, with each group being different from the next.
Describes the mission statement of the "3%" movement as defending the Bill of Rights and stopping government overreach.
Criticizes individuals wearing the "3%" patch who go against the principles it supposedly stands for.
Expresses disappointment in law enforcement wearing morale patches without understanding their meaning.
Concludes by suggesting that law enforcement should refrain from wearing morale patches.

Actions:

for community members,
Remove any patches or symbols that represent ideologies you do not fully understand or support (implied).
Refrain from wearing morale patches that carry meanings beyond your understanding (implied).
</details>
<details>
<summary>
2020-06-03: Let's talk to the Guard and Active Duty right now.... (<a href="https://youtube.com/watch?v=WyDVYriZB9Y">watch</a> || <a href="/videos/2020/06/03/Lets_talk_to_the_Guard_and_Active_Duty_right_now">transcript &amp; editable summary</a>)

Addressing the guard on active duty and politicians' misunderstanding, Beau empowers soldiers to protect American freedom with reason and discipline amidst unrest.

</summary>

"Keep us safe. Keep us safe. [...] That was the moment it all made sense, why they wanted you."
"They don't understand the modern military."
"You're the weapon. A walking, talking, breathing, thinking, reasoning weapon."
"You can truly protect American freedom."
"Make sure you act in a manner befitting who you are, not who they want you to be."

### AI summary (High error rate! Edit errors on video page)

Addressing the guard on active duty, acknowledging their conflicted feelings.
Describing a powerful interaction where a captain couldn't march with protesters due to duty constraints.
Criticizing politicians who misunderstand and misrepresent the military for their own gain.
Recognizing the soldiers' training, discipline, and reasoning skills within the modern military.
Emphasizing the importance of soldiers policing their own people professionally.
Noting the misunderstanding of soldiers as mere tools by politicians.
Encouraging soldiers to maintain restraint and uphold American freedoms during unrest.
Presenting an unprecedented chance for soldiers to protect American freedom by using their training and discipline.
Urging soldiers to show politicians their loyalty to the American people and act accordingly.
Ending with a message of empowerment and quiet determination for soldiers.

Actions:

for soldiers on active duty,
Show restraint and uphold American freedoms in interactions with protesters (implied)
Act professionally and in accordance with values when deployed (implied)
</details>
<details>
<summary>
2020-06-02: Let's talk about that designation Trump wants to make.... (<a href="https://youtube.com/watch?v=rX-zwAC4-uQ">watch</a> || <a href="/videos/2020/06/02/Lets_talk_about_that_designation_Trump_wants_to_make">transcript &amp; editable summary</a>)

President's desire for a controversial designation raises legal and ethical concerns, warning of potential consequences for individual freedoms.

</summary>

"In a country, you are only as free as the least free person."
"You are only as free as the least free person."
"You will be first."
"If you give them an inch, they'll take a mile."
"You guys fit the definition."

### AI summary (High error rate! Edit errors on video page)

President's desire for a certain designation prompts varied opinions, often based on the present moment, but this issue is of universal significance.
Making such a designation about a domestic group is not currently legally permissible.
The term in question does not entirely apply to the group in question as they operate beyond the immediate area of action.
Implementing such a designation wouldn't be effective due to the complex nature of the group's ideology and structure.
Historical precedents show that legality, effectiveness, and logic do not always dictate government actions.
Resisting any infringement on rights like the Second Amendment is vital to prevent a slippery slope of restrictions.
The potential consequences of setting a precedent for designating groups could lead to targeting more fitting organizations.
The group mentioned fits the criteria of using violence or its threat to achieve certain goals beyond their immediate actions.
Failure to recognize the danger in supporting such mechanisms can lead to significant consequences for the group itself.
The importance of maintaining due process and avoiding dangerous rhetoric to protect individual freedoms and rights.

Actions:

for citizens,
Reassess thought leaders and strategists within movements to ensure they protect individual freedoms (implied).
Advocate for the inclusion of due process in constitutional protections (implied).
</details>
<details>
<summary>
2020-06-02: Let's talk about how Trump can create immediate reform.... (<a href="https://youtube.com/watch?v=M71z6K7IkjU">watch</a> || <a href="/videos/2020/06/02/Lets_talk_about_how_Trump_can_create_immediate_reform">transcript &amp; editable summary</a>)

Beau proposes an executive order to hold law enforcement accountable, ensuring immediate reform and cultural change through federal funding leverage.

</summary>

"The end goal is to get rid of the sparks, right?"
"Super quick, super easy. Can be done with a pen stroke."
"No debate. It's just done."
"Immediate reform."
"Hopefully, it could change the culture within the departments."

### AI summary (High error rate! Edit errors on video page)

Addressing the need for solutions to complex issues.
Proposing a solution involving accountability for law enforcement officers.
Suggesting the use of executive orders to enforce consequences.
Creating a new agency to monitor use of force complaints.
Holding departments accountable for addressing complaints.
Using federal funding eligibility as a leverage point.
Emphasizing the potential for immediate impact through this solution.
Not claiming it will solve all problems but will reduce them significantly.
Anticipating a shift in law enforcement culture with this accountability.
Stressing the simplicity and legality of implementing this solution.

Actions:

for law enforcement reform advocates,
Advocate for accountability measures in law enforcement (implied)
Support initiatives that ensure consequences for officer misconduct (implied)
</details>
<details>
<summary>
2020-06-01: Let's talk about businesses not going back to those neighborhoods.... (<a href="https://youtube.com/watch?v=Zu1RqB-Jsgw">watch</a> || <a href="/videos/2020/06/01/Lets_talk_about_businesses_not_going_back_to_those_neighborhoods">transcript &amp; editable summary</a>)

Businesses not returning to certain neighborhoods reveal a stark reality: money matters more than lives in these communities, prompting a call for economic empowerment and change.

</summary>

"A pile of unarmed people, that's just the cost of doing business in those neighborhoods."
"Money's what matters, not a pile of people."
"Maybe it's better if all of those people who are so concerned about the shopping choices of those people in those neighborhoods, maybe a fund could be put together."
"It really might be time for the angel investors of the world to help out the people in those neighborhoods."
"It's a tragedy that it's going to happen again, and that it's something we need to fix because it's going to happen again, but because there's going to be a loss of money."

### AI summary (High error rate! Edit errors on video page)

Addresses the common statement about businesses not returning to certain neighborhoods due to potential risks.
Questions the subtext of the statement and what it implies about the value of lives in those neighborhoods.
Raises concerns about the economic decisions made by businesses and the disregard for the well-being of the residents.
Suggests that the focus should shift towards supporting the communities economically rather than solely on businesses returning.
Calls for change and investment in the people of these neighborhoods to break the cycle of neglect and lack of accountability.
Challenges the notion that financial concerns outweigh the importance of people's lives in these areas.
Emphasizes the need for representation and economic empowerment to prevent future tragedies.
Criticizes the prioritization of money over human lives in the context of businesses abandoning neighborhoods.
Urges for a shift in perspective towards prioritizing the well-being and economic stability of the residents.
Encourages angel investors to support and empower the communities for meaningful change.

Actions:

for community members, activists, investors,
Support community funds for economic empowerment (suggested)
Encourage angel investors to invest in neighborhoods (suggested)
</details>
<details>
<summary>
2020-06-01: Let's talk about a campfire analogy.... (<a href="https://youtube.com/watch?v=8Y7FDDt8EBM">watch</a> || <a href="/videos/2020/06/01/Lets_talk_about_a_campfire_analogy">transcript &amp; editable summary</a>)

Beau explains using a campfire analogy how inappropriate force sparks issues and why water, not more sparks, is needed to prevent further escalation.

</summary>

"You do not put out campfires with more sparks."
"More inappropriate use of force is not going to help here."
"Applying more sparks will cause more campfires."
"This isn't complex. The spark was bad."
"More sparks are not the answer."

### AI summary (High error rate! Edit errors on video page)

Introduces an analogy about a campfire to explain a complex issue.
Describes sticks near the campfire warming up and being wary of sparks.
Links sparks to the inappropriate use of force.
Talks about how more sparks can lead to sticks catching fire easily.
Mentions the temperature rising and the campground being on edge.
Criticizes the idea of putting out campfires with more sparks.
Talks about a person in a fireproof bunker advising to apply more sparks.
Emphasizes the need for water, not sparks, to put out campfires.
Refers to experts in DC who understand campfires and advocate against more sparks.
Urges for understanding the theory of campfires and avoiding more sparks.
Calls out Senator Tom Cotton for not comprehending the situation.
Stresses the importance of using water to cool off the situation and prevent more campfires.
Mentions areas like Flint, Michigan, that used water effectively.
Warns about the cycle of campfires and the need to break it before it escalates.
Advocates for making the sticks comfortable and cooling them off to prevent further issues.

Actions:

for campground residents,
Call for community leaders to prioritize de-escalation tactics over aggressive responses (implied)
Advocate for peaceful resolutions and understanding in the community (implied)
Support initiatives that focus on cooling down tense situations rather than adding fuel to the fire (implied)
</details>
