---
title: Let's talk Trump's tweet about Buffalo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xdPgX8ssNA4) |
| Published | 2020/06/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing President's tweet about a protester being a provocateur.
- Pointing out the inaccuracy in the President's claim about the protester's actions.
- Noting the anger and responses from people regarding the President's statements.
- Stating that even if the allegations were true, it still doesn't justify the use of force.
- Emphasizing that legality of the protester's actions and lack of justification for force.
- Suggesting that the President lacks understanding of when law enforcement can use force.
- Criticizing the President for not taking the time to understand appropriate use of force.
- Critiquing the tactic of attacking the victim in cases of law enforcement misconduct.
- Stating that even if the allegations were true, unwarranted force cannot be justified.
- Encouraging to focus on the fact that unwarranted force is never justified.

### Quotes

- "Even if all the allegations are true, it doesn't justify force."
- "None of this allegation is probably true, okay? But even if it was, it does not justify the force."
- "We can't just go chasing our tails trying to argue this, because this argument is something that, it doesn't need to take place."

### Oneliner

Beau addresses President's baseless claims about a protester and stresses that unwarranted force is never justified.

### Audience

Advocates for justice

### On-the-ground actions from transcript

- Advocate for transparency and accountability in law enforcement (implied)

### Whats missing in summary

The detailed analysis and breakdown of the President's tweet and the implications it holds.

### Tags

#Justice #PoliceMisconduct #UseOfForce #Protesters #Accountability


## Transcript
Well howdy there internet people, it's Beau again.
So we've got to talk about what the president said this morning again,
because that's like a common thing that the entire country has to do now,
because he's the president.
He tweeted out, Buffalo protester shoved by police could be a provocateur.
The 75 year old, he gives his name, I'm not putting it out there yet again,
was pushed away after appearing to scan police communications in order to black out the equipment.
I want to pause right here.
That's not how that works.
That's not how that happens.
He gives the name of a news outlet, news in quotes,
I watched, he fell harder than he was pushed.
Was aiming scanner, could be a setup?
Okay, in response to this, there's a lot of people that are angry.
People are rightfully pointing out that he is saying could be a provocateur,
which is very different than saying is, and that he could be a setup,
whatever that means, which is different than saying is.
A lot of people are pointing out that yet again, he's just peddling baseless conspiracy theories.
A lot of people are pointing out that that's not how that works.
As far as blacking out the equipment, blacking out the equipment.
But in addition to all of that, I would like to add something that I haven't seen mentioned yet.
There's another piece of messaging that it may be even more important.
Let's take it the other way rather than denying what the president is saying.
Let's just say it's all true.
Let's say every single allegation he is making here is 100% accurate.
Still doesn't justify force.
Even if all the allegations are true, it doesn't justify force.
I keep in mind using a scanner is, as far as I know, completely legal activity,
and it doesn't black out communications.
And if he was a provocateur, the key thing to do there would to avoid being provoked.
You don't use force.
Nothing the president is alleging here justifies the use of force.
So either the president is what everybody believes him to be,
or we can add not knowing when it is appropriate for law enforcement to use force
to the list of things that he doesn't understand,
which seems like it would be a really important topic to understand at the moment.
It seems like the president of the United States would take the time
to kind of get some basic understanding of when it's okay for law enforcement to use force,
given the fact that the country is on fire because of it.
That seems like something that he might should go to a briefing on.
I don't like this because it does that same thing.
It disparages and attacks the victim, which is a common tactic that gets used a lot
when we're dealing with law enforcement misconduct or certain kinds of assault,
or that kind of assault coming from law enforcement.
None of this is, none of this allegation is probably true, okay?
But even if it was, it does not justify the force.
And that's something that we need to keep in mind.
We can't just go chasing our tails trying to argue this,
because this argument is something that, it doesn't need to take place.
Even if all of this is true, the force was unwarranted.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}