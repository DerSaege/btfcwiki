---
title: Let's talk about why this D-Day anniversary is so important....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-ubBUl7Ja4U) |
| Published | 2020/06/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commemorating D-Day in the US annually, glorifying it, and its significance as a turning point for the country.
- Lack of clear knowledge about the casualties on D-Day due to the chaos of the events.
- Thousands of Americans sacrificing themselves on D-Day against tyranny and injustice.
- Drawing parallels between the chaos of current events and the sacrifices made on D-Day.
- The importance of allowing the current chaos to be a turning point in history.
- Mention of Martin Luther King Jr. as a catalyst for the Civil Rights Movement and his role in spreading awareness.
- Encouraging to research the Long Hot Summer and its relation to civil rights.
- The need for a leader like Martin Luther King Jr. in the current movement against injustice.
- Reference to the controversy surrounding a football player protesting during the national anthem.
- Criticism of those who ignored the message then and now criticize those who take a knee for justice.
- Acknowledgment that change requires more than just 12% of the population to act.
- Emphasizing the importance of supporting and amplifying voices fighting against injustice.
- Urging action to address the systemic issues creating injustice in the country.
- Commitment to join a march supporting black lives matter and being a witness to history.
- Ending with a reminder that change is necessary to prevent cycling back to injustice.

### Quotes

- "It was a turning point for the country."
- "It is a turning point, if we're smart enough to allow it to be."
- "They don't need a leader, we do."
- "Black lives matter."
- "Eventually, changes will be made."

### Oneliner

Beau contemplates the chaos of current events, the need for leadership, and the importance of supporting the fight against injustice, culminating in a commitment to back Black Lives Matter at a march.

### Audience

Activists, Supporters, Allies

### On-the-ground actions from transcript

- Attend marches supporting movements against injustice (exemplified)
- Be a witness to history by actively participating in events supporting marginalized communities (exemplified)

### Whats missing in summary

The emotional impact and personal reflection that Beau shares throughout the transcript.

### Tags

#D-Day #CivilRights #Injustice #BlackLivesMatter #Support


## Transcript
Well howdy there internet people, it's Beau again.
So it's D-Day, the day of days.
It's a day we commemorate pretty much every year in the US.
We talk about it, we love to glorify it.
And it makes sense on some levels.
It was a turning point for the country.
The people who saw it were witnesses to history.
They saw the United States really become a superpower.
Because if you had to pick a day, that's the day it happened.
They saw the day that the new world began to exert some influence over the old in a
very tangible way.
And that's all fine and good, but what do we actually know about D-Day?
Not as much as you might think.
We don't even know how many we lost.
There's no clear number on that, because it was a mess.
It was a jumble.
The counts from D-Day and D-Day plus one, plus two, and so on, they all just kind of
blurred together.
This important moment, and we don't even know how many we lost.
What do we actually know about D-Day?
Why is it such a major event in American consciousness?
That image, that image of thousands upon thousands of Americans throwing themselves into the
grinder to oppose tyranny and injustice, that's an American thing.
I think it's fitting that this mess, this jumble that we're currently in is occurring
now.
Because it's thousands upon thousands of Americans throwing themselves into the grinder to oppose
injustice and tyranny.
It is a turning point, if we're smart enough to allow it to be.
We are witnesses to history.
And I know, I know, they should have done it better.
They should have had a leader like Martin Luther King.
Yeah, maybe, maybe.
But see, the thing is, Martin Luther King, he was the catalyst.
In a lot of ways, he built the Civil Rights Movement.
And his most important contribution, from where I'm sitting, was getting the message
out.
Letting everybody know what the grievances were.
Getting it into the mainstream so people who looked and sounded like me got it, at least
a little bit, if they listened.
But at the same time, I would like to point out that other things were going on during
the Civil Rights Movement.
You might want to Google the Long Hot Summer in relation to civil rights, rather than the
movie.
And as you read about it, maybe ask yourself why that part isn't in your history book.
But yeah, they need a leader.
Somebody who could do that.
Somebody who could make people listen, get attention, enter the public narrative, get
people talking about it.
They had that.
They had that.
Now granted, he was no Martin Luther King.
Nobody saw him as such a threat that they looked at him through a scope.
But people certainly did it to his character.
Football man didn't stand for the magic song.
I gotta go burn my Nikes now.
If you burned your Nikes then, if you had issues with it then, all of this is your fault.
Because you could have listened then.
He got it out into the public narrative.
He got it into the conversation.
Not many wanted to listen, even fewer wanted to act.
And then, after that, after that, when people who may have a sticker on your car saying
that you support them, the Sergeant Major of the Army, the Joint Chiefs, when they issued
a statement, saying the exact same thing, that we need to listen, nah, nah, they've
gone soft.
And then, when those soldiers take a knee, well that's just horrible.
When they take a knee and listen.
That's a bad thing.
Because they need a leader.
They.
Twelve percent of the population needs a leader.
In a country where it takes fifty percent to do anything, they need a leader.
Not us.
You know, there's their stereotypes.
Entitled and lazy.
We won't even clean up our own mess.
The system that creates the injustice is a system of our creation.
It's a system that we defend.
We maintain.
We built.
There's only so much that twelve percent of the population can do without backup, without
assistance.
They don't need a leader, we do.
We need to back their plays, and we need to make some of our own.
Because we can get it done a whole lot faster.
Because this is gonna happen.
We are going to have to address the original sin of the United States.
One of them.
We're going to have to do it, otherwise it'll keep cycling back around, over and over again.
Eventually, the changes will be made, the only thing that we're determining now is how
long it takes and how much injustice occurs before.
They don't need a leader, we do.
I'm not going to be active in the comments section today like I normally am, because
even in the small, tiny little town near where I live, there's a march today.
And I'm going to be there to back their play.
I'm going to be there to be a witness to history, to see the turning point.
And I'm going to be there because black lives matter.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}