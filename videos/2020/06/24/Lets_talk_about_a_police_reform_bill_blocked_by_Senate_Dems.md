---
title: Let's talk about a police reform bill blocked by Senate Dems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c2AnEd-ky9k) |
| Published | 2020/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes misleading headlines on Democrats blocking police reform bills.
- Republican bill misrepresented as police reform actually incentivizes no-knock raids, chokeholds, and addresses lynching.
- Democrats blocked the Republican bill due to lack of real reform.
- Democrats have their own bill banning chokeholds, no-knock raids in federal drug warrants, and limiting qualified immunity.
- Points out the importance of having a police misconduct database in reform bills.
- Emphasizes the need to be cautious of mainstream narratives on police reform and prioritize satisfying Black Lives Matter and police accountability groups.

### Quotes

- "Republicans attempt to mislead constituents."
- "If it doesn't satisfy Black Lives Matter, if it doesn't satisfy the police accountability groups, if it doesn't satisfy those people in the streets, it doesn't satisfy me."

### Oneliner

Beau criticizes misleading headlines on Democrats blocking a GOP bill misrepresented as police reform, clarifying the lack of true reform efforts and the importance of satisfying police accountability groups.

### Audience

Reform advocates

### On-the-ground actions from transcript

- Contact your representatives to push for comprehensive police reform bills (suggested)
- Join local advocacy groups working towards police accountability (implied)

### Whats missing in summary

The full transcript provides in-depth analysis and criticism of the political dynamics surrounding police reform efforts, shedding light on the importance of accurate information dissemination and genuine reform initiatives.

### Tags

#PoliceReform #MisleadingHeadlines #Democrats #Republicans #BlackLivesMatter


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about why it's important
to read past the headline.
Because man, there's some doozies out there today.
Keep seeing these headlines.
Democrats block police reform.
Senate Dems block GOP police reform bill.
All of this stuff.
Like man, that seems odd.
That seems really strange.
It seems like that might actually be so weird
and unbelievable that it may not be true.
Because it's not.
Okay, for something to count as police reform today,
you would have to operate under the assumption
that it would take into account on some level
police accountability activists,
the people out there in the streets,
all of the things that have been talked about
for, you know, the last month.
It seems like those things would come into play.
They would be addressed by the bill,
especially if you were gonna package it
and market it during this, right?
So, this Republican bill that was put forward,
it ended or limited qualified immunity, right?
False.
Established a police database for misconduct?
False.
Banned no-knock raids?
False.
Banned chokeholds at least.
I mean, that one's a given.
False.
It didn't do any of those things
because it's not actually about reforming police.
What the bill actually did was it codified
reporting no-knock raids.
So, it's actually an implicit encouragement
to continue doing it.
It incentivized not using chokeholds,
but didn't say you couldn't.
And it made lynching a federal hate crime
in a police reform bill.
That's funny.
I don't know who slipped that part in, but that's funny.
Just for all of you back-to-blue people out there,
please acknowledge the Republican Party,
in order to reform police,
wants to stop lynching because, I mean,
at least that part was honest.
Okay, so, of course, the Democratic Party in the Senate,
they voted against this.
They blocked it.
Why?
Because the people that want reform asked them to,
is how that played out.
There's another bill coming forward,
put forward by Democrats, that does ban chokeholds,
does ban no-knock raids in federal drug warrants,
and limits qualified immunity.
It still doesn't have a police database, though.
That needs to be in there.
That's super important.
But it's not there.
You know, I think the easiest way to look at this
is you have one bill that is supported by old white guys
from Kentucky,
and another bill that is supported by the NAACP.
Which one do you actually think is going to kind of speak
to the issues of Black Lives Matter?
Yeah, these articles with these headlines,
they're utter garbage.
The real headline there should be something along the lines
of, Republicans attempt to mislead constituents,
because that's what they're doing.
This was not police reform.
It's not what it was about.
It was about throwing something out there
that the cops would get behind
to make it seem like they did something
because they have no interest in reforming the police.
That's not on their agenda,
because most of these senators...
Well, you can read their quotes.
This is something that we're going to have to fix.
It's going to have to happen.
We might as well do it right.
There's no reason to allow a political party
to pretend like they fixed it when they didn't.
And to be honest,
I don't really support the Democratic proposal,
because without a database for police misconduct,
it's kind of pointless.
That needs to be included.
That needs to be added.
If it's going to have any teeth, that needs to be added.
So let's be careful when we're consuming information
about this particular topic,
because remember, the establishment in general
does not want to reform its enforcement class.
It doesn't want to reform the police.
It wants them to have as much leeway as possible
so they'll do whatever they're told.
So we really need to be careful
when we're reading stuff about this and opinions about this.
If it doesn't satisfy Black Lives Matter,
if it doesn't satisfy the police accountability groups,
if it doesn't satisfy those people in the streets,
it doesn't satisfy me.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}