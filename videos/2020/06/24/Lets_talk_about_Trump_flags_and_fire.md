---
title: Let's talk about Trump, flags, and fire....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HhyuXhJ5WNM) |
| Published | 2020/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the symbolism behind a proposed constitutional amendment banning flag burning.
- Challenges the idea that the flag symbolizes freedom and the American way of life.
- Argues that the founding fathers' concept of "country" was different from a nationalist perspective.
- Emphasizes that the flag should symbolize promises like freedom, liberty, and justice for all.
- Criticizes the lack of progress in fulfilling these promises and addressing injustices in society.
- Points out that burning the flag can hold more meaning than merely displaying it.
- Asserts that true patriotism involves working towards equal rights and helping the marginalized.
- Condemns politicians using flag-related issues for political gain rather than focusing on real societal problems.
- Calls out the hypocrisy of prioritizing flag protection over addressing pressing issues like social reform.
- Expresses disappointment in the diminishing significance of the flag due to shallow patriotism and political theatrics.

### Quotes

- "Those people who love the symbol more than what the symbol is supposed to represent."
- "It's just a thought."
- "If you're not fighting to keep those promises, how can you really care about the symbol of those promises?"
- "The logo, the American flag, doesn't mean as much anymore because of people like him."
- "Not let's make my country right."

### Oneliner

Beau questions the true symbolism of the American flag, criticizing shallow patriotism and political theatrics over real societal issues.

### Audience

Patriotic Americans

### On-the-ground actions from transcript

- Challenge shallow patriotism by actively working towards equal rights and helping marginalized communities (implied).
- Resist political theatrics and focus on pressing societal issues such as social reform (implied).

### Whats missing in summary

The full transcript provides a deep reflection on the symbolism of the American flag and the importance of true patriotism in addressing societal injustices.

### Tags

#AmericanFlag #Patriotism #Symbolism #SocialJustice #PoliticalTheatrics


## Transcript
Well howdy there internet people, it's Beau again.
So this video is a little different.
This part of it is just to let you know, after the normal sign off, stick around.
There's some important information about the video that you're about to watch after it.
Well howdy there internet people, it's Beau again.
We got us a hot topic today.
Constitutional amendment banning flag burning.
It's a good way to drum up the bass I guess.
But it seems odd to me.
What does this symbolize?
It's a symbol.
What's it symbolize?
Well the land of the free and the home of the brave of course.
The land of the free with the highest incarceration rate in the world and the home of the brave
who is so terrified of the other they want to cower behind a wall.
Not buying it, try again.
Well it symbolizes our country.
Okay that's a good route.
Go the nationalist route.
Bring up the founding fathers and all of that.
The problem is when the founding fathers spoke of country, they weren't talking about the
United States.
And I don't mean that in some linguistic quirk.
I mean it didn't exist yet.
When they said country, when they spoke of country, they meant the country, their countryside,
their neighbors, the people who are so often forgotten in conversations like this.
That's who they were talking about.
They didn't speak that way very often either.
You know they, they're pretty bright guys, they understood that when you're trying to
get a whole bunch of people to commit treason, it's not a good idea to talk like a nationalist.
They spoke of ideas.
They spoke of freedom and liberty.
That's what they talked about.
That's what stirred people to action.
They made promises to the future and created the machinery for change.
Well it symbolizes those promises then.
It should, I will give you that, it certainly should.
But does it?
Do we as a society move forward in hopes of fulfilling those promises?
Do we try to make sure that while there is the ability to pursue happiness, that all
men are created equal, that there's liberty and justice for all, or do we do everything
we can to make sure that the other is painted as, well, less than equal?
Do we turn a blind eye to grave injustices and try to justify them in some manner?
It's horrible what's happening to those kids, but, you know, they just should have done
whatever.
The people who want this criminalized, they're not doing anything to fulfill those promises,
so I can't believe that that's what it symbolizes to them.
It symbolizes those who fought and died for our freedom.
Maybe at one time.
Maybe at one time.
Today, how'd our freedom get in Iraq, Afghanistan, Libya, Mali, Sudan, Panama, Grenada?
How'd our freedom get there?
As is evidenced by this, the only people who can take away your freedom is your government.
That's who can take away your freedom, unless we're talking about an invasion, and we haven't
been at risk of an invasion in this country in a very, very long time.
Well, fine, they weren't, you know, fighting for freedom exactly, but they were protecting
the American way of life.
Yes.
Yes, they were.
And that's what this symbolizes today.
It symbolizes the American way of life.
Absolutely.
It does.
Now, let's say you had a problem with an aspect of the American way of life, and you wanted
to express your displeasure.
How would you do it?
What would be a good symbol to use, not violently, not hurting anybody?
And that's the ultimate ha in this.
It's the ultimate punchline.
Those people who would burn the American flag, it has more meaning to them than those who
simply slap a bumper sticker on their truck.
They understood the promises.
They want to improve the American way of life, and this is a way to express their displeasure.
They understand what it was supposed to be.
And instead, as our understanding of these promises has been whittled away, those who
consider themselves patriots, they spit on those ideas.
If seeing someone burn your symbol of freedom causes you to decide that they need to have
their freedom taken away, well, your symbol doesn't do any good, does it?
It doesn't remind you of what it's supposed to, and that's the whole job of a symbol.
Our country, if you want to be a patriot today, you have to be working for equal rights, to
make sure that all men are created equal.
You have to be looking out for the downtrodden.
You have to be hoping to help the huddled masses yearning to breathe free.
All of those things that make up the American dream and are supposed to make up the American
character, you have to do that if you want to be a patriot.
Otherwise, they're just taglines beneath a logo, no more sacred than the logo of GE or
McDonald's.
If you don't act on those promises, this means nothing.
It means nothing.
You have to act on those promises.
The main idea of it was to make sure that all men could stand under this one day as
equals.
That was the idea, but today it's a logo, just like all of the other logos.
It doesn't really mean much at all.
I can't fault somebody for burning a logo.
At least to them it means something.
At the end of the day, if you're not fighting to keep those promises, how can you really
care about the symbol of those promises?
You've got these people up in DC who want to criminalize this and rewrite the Constitution
so they can do it because they're interested in keeping the promises.
The thing is, they're spitting on the very ideas that are supposed to be America.
And what's worse is that they know a large portion of this country who consider themselves
patriots.
They lost the plot too.
They're just using this to play to the base, to play to people who think they're patriots.
That's what they're doing because they know this isn't going to go anywhere.
It's got to get through the House, the Senate, and be ratified by the states.
It's just to drum up support from the people they know don't understand this country and
what it was supposed to be.
There were points in time where we worked as a society to make sure people could stand
under this as equals.
The problem is those points, those high points in the American experience, they're growing
fewer and further apart.
Meanwhile, there's more flags than ever.
The person burning this flag, they didn't disgrace it.
They didn't desecrate it.
You did when you failed to keep the promises, when you forgot what it was supposed to be,
when you turned your back on the very ideas that this country was founded on.
And it's happening all over the country.
And it's being cheered on by the same people who want to change the Constitution to protect
the cloth.
The logo.
Anyway, it's just a thought.
Y'all have a good night.
Okay so if that video seemed really familiar to you, it's because it was originally uploaded
on June 17th, 2019.
Almost one year ago to the day.
Why?
Because every year at this time, the powers that be talk about this.
It's not that they actually want to do it, it's that they want to fire up their base
right before July 4th.
So they're getting a head start on being able to downplay any protests that occurs on July
4th.
Last night, by the time y'all watch this, the President tweeted out, it is a shame,
just whatever, that Congress doesn't do something about the lowlifes that burned the American
flag.
It should be stopped and now.
But they were talking about last year and didn't do anything about it.
Because they know they can't.
Because they know in order to do it, they would have to alter the First Amendment.
This is political theater designed to appeal to the bumper sticker patriots of the country.
Those people who love the symbol more than what the symbol is supposed to represent.
With everything going on in the country right now, the President of the United States has
chosen to talk about this rather than climbing numbers, rather than cities burning, rather
than drastic reform that's desperately needed, rather than the fact that the symbol, the
logo, the American flag, doesn't mean as much anymore because of people like him.
Because of people who appeal to this shallow sense of patriotism.
Because of those people who look at that symbol and say, my country right or wrong.
Not let's make my country right.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}