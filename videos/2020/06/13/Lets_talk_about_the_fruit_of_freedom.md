---
title: Let's talk about the fruit of freedom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=o01KLrual3E) |
| Published | 2020/06/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses historical facts and events that are often overlooked or not given enough coverage.
- Public school teachers can suggest topics for Beau to cover, regardless of his comfort level.
- Freedom can be intimidating, leading individuals to stick with what is familiar and predictable.
- The topic of black people and watermelons is discussed, shedding light on the historical significance.
- After slavery ended, many newly freed individuals turned to growing watermelons as a means of sustenance and income.
- Growing watermelons provided hope and a chance for advancement for these individuals.
- Watermelon should symbolize pride and freedom for black people, but it was twisted into a derogatory stereotype.
- The perpetuation of jokes and stereotypes about watermelons robbed black individuals of a source of pride.
- The tactic of using one group to look down upon to elevate others is a manipulation by those in power.
- Beau condemns the racist jokes and stereotypes surrounding watermelons, advocating for their eradication.

### Quotes

- "Watermelon should be the fruit of freedom."
- "It's been turned into a stereotype, and when I imagine that if I was black, I don't know that I would want to eat watermelon in public because of the stereotype."
- "Those jokes need to go away, not just because they're racist and they're stupid, but because it should be something that they could take pride in."

### Oneliner

Beau sheds light on the historical significance of black individuals growing watermelons post-slavery and condemns the racist stereotypes attached to it.

### Audience

History enthusiasts, educators, activists

### On-the-ground actions from transcript

- Challenge and confront racist jokes and stereotypes (implied)
- Educate others about the historical significance of watermelons for black individuals (implied)
- Support and uplift black voices and narratives (implied)

### Whats missing in summary

The emotional impact and depth of historical injustice addressed by Beau can be fully grasped by watching the full transcript. 

### Tags

#History #Racism #Watermelon #Freedom #Stereotypes #Injustice


## Transcript
Well howdy there internet people, it's Beau again.
So today is another installment in the series of videos about historical facts and events
that don't get the coverage they should, or at all.
This particular topic was suggested by a history teacher.
If you have watched this channel for any length of time or you've seen the channel trailer,
you know that public school teachers can ask for, well, pretty much anything, and I will
try to accommodate them regardless of how uncomfortable it might make me.
We talked in the last video in this series about freedom and how freedom is scary, and
normally when people are faced with freedom they go with what they know, and this is probably
true even in your life.
When you're looking for a new job you could reinvent yourself, but the odds are you're
going to look for something you have experience in, and it's not just because it's easier
to get hired.
There's more to it than that.
You go with what you know, something you can predict the outcome of, something that is
more certain.
This would especially be true if you were experiencing freedom for the first time.
So with all of that in mind, today we are going to talk about black people and watermelons.
Everybody watching this just went, yeah, those jokes are even worse than you might imagine.
A lot of slave owners allowed their slaves to grow watermelon, to eat, some even allowed
them to sell them.
So when slavery ended, what did these newly free people, what did they know, what outcomes
could they predict, what did they have the skills to do, what markets did they understand?
So a lot of them grew watermelon.
Gave them the means, helped get them on their feet before the hope was damped out.
It gave them a glimmer of hope that they might be able to advance, get ahead.
In a lot of ways, watermelon should be the fruit of freedom.
Should be something that they look at as a symbol of pride, but because that means and
them getting on their feet was a threat to the white establishment in the south, well
they had to repackage some old jokes.
They weren't even original.
Most of those jokes were made about Italians and Arabs before, but it stuck.
Became a stereotype, became a trope, and the whole idea, you know, because it's sugary
and it's sweet, makes it childlike so they're simple and then they get sticky and they're
dirty, all of it, and it stuck.
So it's horrible, not just because it's racist, but because it took something that should
be a symbol of pride, something that helped get a lot of people on their feet, and it
robbed them of it.
Took it away.
Made it something to be embarrassed of.
You know, and that's something that happens a lot.
The powers that be, they use one group as the other, those to look down on, so they
can make everybody else feel better about themselves and their station in life.
Everybody kicking down, keeping those other people in their place.
And that's gone on for so long that that attitude and those jokes and all that stuff, that scene
is like a symbol of strength.
Doing that to other people, that makes you strong.
No it doesn't.
Strong people aren't afraid of competition.
It displays an inherent insecurity.
Those jokes need to go away, not just because, yeah, they're racist and they're stupid, but
because it should be something that they could take pride in.
This is a moment that could be viewed through the lens of, this is the beginning of our
freedom.
This is what helped us.
You know, they can't reach back further than that, because it wasn't in the slave owner's
interest to keep track of where they're from.
So they can't go that far back with their cultural identity.
But this could be the beginning.
That could be something that they could use.
A watermelon.
But it's been taken from them by bigots.
It's been turned into a stereotype, and when I would imagine that if I was black, I don't
know that I would want to eat watermelon in public because of the stereotype.
Because it's gone on so long and it's so funny, right?
Can't let anybody have anything.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}