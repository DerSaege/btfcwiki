---
title: Let's talk to the Guard and Active Duty right now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WyDVYriZB9Y) |
| Published | 2020/06/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the guard on active duty, acknowledging their conflicted feelings.
- Describing a powerful interaction where a captain couldn't march with protesters due to duty constraints.
- Criticizing politicians who misunderstand and misrepresent the military for their own gain.
- Recognizing the soldiers' training, discipline, and reasoning skills within the modern military.
- Emphasizing the importance of soldiers policing their own people professionally.
- Noting the misunderstanding of soldiers as mere tools by politicians.
- Encouraging soldiers to maintain restraint and uphold American freedoms during unrest.
- Presenting an unprecedented chance for soldiers to protect American freedom by using their training and discipline.
- Urging soldiers to show politicians their loyalty to the American people and act accordingly.
- Ending with a message of empowerment and quiet determination for soldiers.

### Quotes

- "Keep us safe. Keep us safe. [...] That was the moment it all made sense, why they wanted you."
- "They don't understand the modern military."
- "You're the weapon. A walking, talking, breathing, thinking, reasoning weapon."
- "You can truly protect American freedom."
- "Make sure you act in a manner befitting who you are, not who they want you to be."

### Oneliner

Addressing the guard on active duty and politicians' misunderstanding, Beau empowers soldiers to protect American freedom with reason and discipline amidst unrest.

### Audience

Soldiers on active duty

### On-the-ground actions from transcript

- Show restraint and uphold American freedoms in interactions with protesters (implied)
- Act professionally and in accordance with values when deployed (implied)

### Whats missing in summary

The full transcript offers a deeper understanding of the complex dynamics soldiers face in policing their own people and the unique chance to protect American freedom in the midst of unrest.

### Tags

#Soldiers #AmericanFreedom #Policing #Professionalism #Empowerment


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk to the guard in the active duty.
Some of you are already out, some of you may be going out soon.
And you're probably pretty conflicted about it.
Makes sense, I didn't understand it at all.
I didn't understand why these politicians wanted you to be honest.
Didn't make any sense.
We're not quite at that level yet.
That's not really what you guys do.
Then I'm scrolling Twitter, looking at footage from the different events, and I see this
captain.
And the people in the street are talking to him, this woman's talking to him, and she's
like, you know, march with us.
You know, walk with us.
Show us that you're with us.
Keep us safe.
Keep us safe.
Remember that line.
And he's telling her, he's like, I can't.
And for those of you who've seen the clip, understand he literally can't.
He can't leave that area.
He offered to walk the area that he could with her.
But she can tell that he's kind of connecting with her.
And she's getting excited.
Eventually he has to put his hand on his rifle.
To move it out of his way so he can take a knee with him, along with two other soldiers.
And the politicians, I'm sure they were shocked by that.
Because that was the moment it clicked for me.
That was the moment it all made sense, why they wanted you.
Because they don't understand you.
They don't know you.
They have this image of you.
Compare the image of those three soldiers kneeling in the street to those tweets.
Those tweets talking about your big rifle.
Yet Senator Tom Cotton over there indicating that 10th Mountain's going to open up on American
civilians.
Because they don't understand you.
They don't understand the pride you take in your unit legacy.
Or the fact that you understand that the people from your unit who fell overseas did not do
that so their unit could come home and open up on Americans.
That would seem like a pretty large betrayal.
But they don't get that.
See they see the dumb brute.
They want you to give them Kent State.
That's what they want.
Because it's going to give them a bump in the polls.
They want you to drop a bunch of Americans so they can go up a few points.
You can read it in their tweets.
Because they don't understand you.
You're dumb brutes.
Yours is not to reason why.
Because that's the image they have of the military.
They don't understand the modern military.
The US military today is the greatest fighting force the world has ever seen.
And you're part of that.
And the reason it's the greatest fighting force is because it's highly trained, well
disciplined and its soldiers do reason.
They send you to class to learn how to reason.
To OODA loop.
Observe.
Orient.
Right?
You've already done that.
You have already done that.
You've observed what's actually sparking the unrest at these things.
You know.
An overreaction.
You've oriented.
You know that this isn't Iraq.
This isn't a battle space.
This is your neighborhood.
These people, they're not opposition.
They're the people you signed a contract to protect.
But they don't understand that.
Because most of these politicians sending out these tweets, these tough guy tweets,
they've never served anything other than themselves.
They're counting on you to be their tool.
To get them a few points.
So they can run on a law and order ticket.
In the process they have handed you the most difficult task in the profession of arms.
Policing your own people.
You may not want to do it.
You probably don't.
But you're a professional and therefore you want to win.
The funny part about it is, because you're a professional, you know how you have to win
it.
They're over there tweeting about your tools.
The tools of your trade.
Your rifle.
And you know you can't touch it.
Because you can't win.
That's not how you win this fight.
See, to them, you and that rifle, you're interchangeable.
To you, the rifle's a tool.
You're the weapon.
A walking, talking, breathing, thinking, reasoning weapon.
And they don't understand your reasons.
Sid, keep us safe.
She said that.
Keep us safe.
Yeah.
Sounds like people bent on destruction to me.
But I'm sure you've already observed what's sparking most of the unrest each night.
An overreaction by people who aren't trained.
Who aren't disciplined.
Who don't reason.
So in a weird way, your presence may actually accomplish that.
The law enforcement overreaction is what's causing the unrest.
You don't have to pretend you're a tough guy.
You're part of the greatest fighting force the world has ever seen.
You can show restraint and go home with your head held high, because it's not about ego
for you, because they don't know your reasons.
While all of this is a really horrible situation for you guys to be put in, I do have to point
something out.
You are being handed an opportunity to do something that most American warriors never
get a chance to do.
Protect American freedom.
For real, not just as a slogan.
I spent years trying to figure out how American freedom was in Central Africa or Iraq.
It is on these streets.
And you, if you use that training and that reason and that discipline, can truly protect
American freedom.
You can give them their ability to use, their right to speak, their right to assemble, their
right to ask for a redress of grievances.
You can protect their due process.
You can truly protect American freedom.
It's not what they want you to do.
It's not why they're sending you out.
They're sending you out because you're dumb, bloodthirsty brutes.
Because they don't know you.
All they know is how to use you.
You can win this.
It's funny.
It's going to be the quietest and most important deployment of your career.
You want to win this.
You keep your weapon on safe.
And you make sure that when you're out there, you show the politicians that you're there
for the American people.
That's who you're loyal to.
Make sure you act in a manner befitting who you are, not who they want you to be.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}