---
title: Let's talk about President Trump's denials....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NkFlBxyS9ac) |
| Published | 2020/06/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau questions the truthfulness of denials coming from the White House and suggests assuming they are true for the purpose of analysis.
- He points out a tweet where Trump supporters in Florida were confronted, and one of them used a term associated with nationalist and white movements. Despite the denial that he heard it, Beau questions why the President retweeted the video.
- Beau speculates on the possible motives behind the President's actions, suggesting that the tweet may have been to signal his base.
- Regarding the denial about intelligence on Russia, Beau questions why the President and Vice President weren't briefed, raising concerns about their trustworthiness and potential leaks.
- He suggests scenarios where the intelligence community might not have trusted the President with sensitive information.
- Beau concludes that the President's actions indicate mismanagement and his unsuitability to lead the country.
- He criticizes the lack of response from the administration after the President was supposedly briefed on the intelligence issue.
- Beau mentions an incident in Tulsa where instructions to social distance were removed, questioning the President's leadership.

### Quotes

- "Just accept them as true."
- "His denials get focused."
- "There's no situation in which this [lack of briefing] is acceptable."
- "He's not fit to lead the country."
- "So yeah, there's our president out there doing good, leading from the front."

### Oneliner

Beau questions White House denials, analyzes concerning actions, and concludes on President's unsuitability to lead.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Question denials and demand transparency from public officials (suggested)
- Stay informed about political actions and hold leaders accountable (suggested)
- Advocate for responsible leadership and decision-making (suggested)

### Whats missing in summary

Deeper insights into the implications of lack of transparency and leadership in government.

### Tags

#WhiteHouse #Denials #Leadership #Intelligence #Transparency


## Transcript
Well, howdy there, internet people.
It's Beau again.
So I had a couple of denials coming out of the White House
last couple of days.
And the debate is whether or not the denials are true.
Let's do this a different way.
Let's just assume they are.
Let's assume, against all previous history,
that the President of the United States is telling the truth.
That what he is saying, what's coming out of his administration, is accurate.
Okay, so we're going to make that assumption.
And then we're going to try to figure out what's next.
What's left after the denial.
What the options are.
Okay so we'll start with the tweet from this morning.
If you don't know, he retweeted a scene from here in Florida.
In which a bunch of people were riding in golf carts and they were being protested.
Bunch of his supporters were riding around in golf carts and people from the area were
saying things to them.
At which point one of his supporters screams out a term that is 100% associated with nationalist
movements, with white movements.
That caused an issue and he said that he didn't hear it.
Okay so we're going to set aside the fact that it's the beginning of the video.
It is one of the most prominent things in the video and that people in the crowd were
like did you hear that?
It would have been almost impossible for this to be true.
What he's saying is almost impossible to be true but let's just pretend that it is.
Okay so why'd he tweet it then?
What in the rest of that video did he think cast him in a positive light?
What was the purpose of showing that in an area that is very red, very supportive of
him, his supporters are being asked where their hoods are?
It doesn't make a whole lot of sense.
Most politicians know better than to draw attention to the criticisms of them.
It doesn't make sense.
There is no other reason to tweet that.
It's an embarrassment to him.
The fact that in the villages he's getting protested like that, that's an embarrassment.
It shouldn't be happening.
That's not something that should happen to a Republican president.
So if we take his assumption as true, what does it mean?
What's the alternate reason for him tweeting it?
There's not one.
The only reason that he would have tweeted that would be to highlight that remark and
whistle to his base because he thinks there's enough people who support that kind of rhetoric
to get him re-elected.
Okay so let's go to the other denial, the big one, about the intelligence about Russia.
He's saying, the administration, their version is that they're not going to dispute the merits
of the intelligence.
They're not going to say that that's not true.
But they're going to say that the president and the vice president weren't briefed on
it.
Okay fine, they weren't briefed on it.
Why exactly during the middle of a peace process would he not be briefed on it?
Either they knew he wasn't going to act or we have to make, at least entertain the possibility
that there was something else going on inside the intelligence community that rendered this
moot like US intelligence has actually infiltrated this Russian group and they were using it
to pull surveillance on bad guys.
We have to entertain that possibility.
It's not a likely one, but we have to entertain it because all that stuff's compartmentalized.
The people that would be coming up with this assessment would not know that.
So they could turn it in to be reported and somebody higher up the chain could stop it.
It's a possibility.
It's not likely.
So when we get rid of that, we have either they just knew he wasn't going to act on it
or knew he wasn't going to listen during the briefing or whatever, or they didn't trust
him.
They didn't want to expose their means and methods, something we've talked about on this
channel before.
Secrets aren't really secret because of the information.
They're secret because of how the information was obtained.
So they don't want to disclose that to the president of the United States, the commander
in chief, because I don't know, he'd share it with Putin.
He'd tweet it out.
We don't know.
There's no acceptable reason for him not to have been briefed on this at that time.
In any situation, it would be information he needed.
Even if it was something that was being orchestrated by US intelligence, it's still something he
would have needed to know, but he wasn't told it.
So it's just mismanagement.
He's not fit to lead the country.
He's not fit to be commander in chief.
This is based off of his denial.
There's no situation in which this would be acceptable.
Now I want to add into that.
Wasn't briefed on it at the time, but he knows now.
What's the response been?
Nothing.
So I guess they were right not to brief him on it because he's completely ineffectual.
His denials get focused.
They focus people on arguing whether or not they're true.
Just accept them as true.
Nine times out of 10, his denials make it worse.
I would also point out that we did find out that in Tulsa, his staff removed the instructions
to social distance in that stadium.
So yeah, there's our president out there doing good, leading from the front.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}