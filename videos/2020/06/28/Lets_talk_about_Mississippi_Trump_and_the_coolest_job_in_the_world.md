---
title: Let's talk about Mississippi, Trump, and the coolest job in the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JQI1KlXHguY) |
| Published | 2020/06/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes the role of an "op-4" as the coolest job in the world, involving disrupting security systems to expose flaws.
- Credits President Trump for playing the role of "op-for" in the U.S. by bringing flaws to the surface and providing a starting point for improvement.
- Reads a message from the Mississippi Historical Society supporting changing the state flag due to its divisive Confederate imagery.
- Notes the Mississippi legislature's decision to remove Confederate symbols from the state flag, signaling a significant change.
- Emphasizes that this move was mainly supported by Republicans and conservatives in Mississippi, showcasing a desire for progress and inclusivity.
- Views this change in Mississippi as a significant step forward, indicating the South's readiness to embrace progress and move beyond the past.
- Acknowledges the surprising nature of this change, especially considering past resistance from the establishment in Mississippi.
- Thanks Trump for bringing attention to the issue and praises conservatives in Mississippi for taking steps towards positive change.

### Quotes

- "It does not get more Old South than Mississippi."
- "This is the South rising again."
- "People have talked about the South rising again. This is it."
- "He's the one that brought everybody out."
- "I want to thank Donald Trump for providing that stress test."

### Oneliner

Beau describes how Trump's actions served as a stress test, leading to positive changes in Mississippi's flag and conservative attitudes, showcasing a shift towards progress and inclusivity in the South.

### Audience

Southern residents, activists

### On-the-ground actions from transcript

- Support the removal of divisive symbols in your community (exemplified)
- Advocate for inclusivity and progress in your region (exemplified)
- Acknowledge and address historical flaws for a better future (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into how political actions and rhetoric can lead to positive societal changes, especially in terms of addressing historical symbols and promoting progress in the South.

### Tags

#South #Mississippi #Progress #Inclusivity #PoliticalChange


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about the coolest job, possibly the coolest job in the world,
definitely the coolest job in the security industry.
We're going to talk about the winds of change here in the south and we're going to thank
Donald J. Trump, President of the United States.
I'm not joking.
Okay, so for me, the coolest job in the world is being op-4.
Now what that means is large companies or facilities or individuals that have large
security staffs, at times they will hire individuals who are outside of their normal security staff
to come in and provide a stress test to try to disrupt their security ecosystem so they
can figure out what's wrong.
And to be very clear about what I'm talking about here, when I say disrupt, I mean break
it.
I mean flat out break it.
If this is your profession, if this is something you do, you can get hired to break into facilities,
hired to take trucks, documents, snatch people, all depending on who your client is.
It provides an amazing tool for the permanent security staff.
Now most times they really don't like it when it's happening because it's embarrassing.
It exposes all of their flaws.
But if you have good people, they want to change it.
They want to fix it.
With that in mind, I would like to thank the President of the United States for playing
that role for the U.S., for being op-for, for being opposition force to the United States.
Because his rhetoric brought it all to the surface, highlighted every flaw.
And it gave us a starting point.
It showed us where we needed to put in some serious work.
And the cool part is that we found out we have good people, because most Americans want
the problems addressed.
They want them fixed.
It's amazing.
At the end of the day, Donald J. Trump may actually end up making the country more progressive
than anybody who ever actually set out to do it.
It's cool.
So thank you, Mr. President.
Now with that in mind, I would like to read a message from the Mississippi Historical
Society.
The Mississippi Historical Society enthusiastically and unequivocally supports changing the state
flag.
Our position comes out of acknowledgement that the inclusion of Confederate imagery
on the flag in 1894 did not represent all Mississippians.
The persistent use of Confederate imagery to signify opposition to civil rights and
racial equality throughout the 20th century further underscores our position.
We support retiring the current flag as a historical artifact and selecting a new one
that will unify and not divide.
So much for destroying history, right?
Now the really cool part about this is that the state legislature's already acted on it.
House and Senate in Mississippi, they already voted.
They're getting rid of the Confederate imagery in the state flag.
Governor said it's going to be signed.
If you don't know anything about the South, don't know the differences between the states
here, just understand this is huge.
It does not get more Old South than Mississippi.
I would imagine this is going to start a trend.
Now if you are one of those clinging to the past, I have an additional piece of information
for you.
This was mainly pushed by Republicans in Mississippi.
Conservatives in Mississippi are ready for a change.
That shows you how out of date you are.
Conservatives in Mississippi are done with it.
It's time to let this go.
People have talked about the South rising again.
This is it.
This is it's happening.
This is how it's going to go down.
I know this isn't what most people meant when they said that, but this is the South rising
again.
Raising its standards.
Its levels of inclusion.
Its desire to move forward.
This is the South rising again.
It doesn't get any more poignant than Mississippi deciding it's time to make the change.
If you're still holding on, you are way behind the times.
And this isn't really a slap at Mississippi.
Not in general.
I know there are people who have been ready for this for a long, long time there, but
the establishment in Mississippi has pushed against it.
The fact that that has changed is amazing.
It is amazing.
That's something I honestly didn't think I'd see in my lifetime, to be completely honest.
Anyway I know to those outside of the South this may not seem like a big deal.
It seems like it should be a given to get rid of it, right?
It's not.
It wasn't.
This is huge.
This is huge.
And the fact that it was pushed by Republicans, that just makes it even bigger.
At the end of the day, we have Donald Trump to thank for this.
He's the one that highlighted it.
He's the one who brought everybody out.
Brought them out of the woodwork.
Made them think it was okay to behave in this manner.
And then normal people, good people, were like, no, no, no, no, no, no.
We honestly thought we left all that behind or we were at least never going to talk about
it again.
If you're going to talk about it, we got to fix it.
So I want to thank Donald Trump for providing that stress test.
And I would like to thank conservatives in Mississippi, a sentence I never thought I'd
say, for starting to fix the door.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}