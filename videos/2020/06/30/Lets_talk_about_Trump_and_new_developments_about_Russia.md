---
title: Let's talk about Trump and new developments about Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DGkLBTgnQlw) |
| Published | 2020/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions what the president knew and when he knew it, a frequent concern during this administration.
- Reports from The Military Times suggest the president received information on Russia's activities over a year ago.
- Raises doubts about the president's evolving position, shifting from denial to unverified claims.
- Points out that intelligence is about establishing intent and predicting the future, not about verification.
- Criticizes the lack of action taken by the administration to verify critical intelligence.
- Expresses disbelief at the president's trust in Putin, a former Russian intelligence officer.
- Challenges the administration's inaction and failure to act on significant intelligence.
- Notes the contradiction in distrusting intelligence reports while trusting Russian sources.
- Emphasizes that reports did not originate from the intelligence community but from troops on the ground.
- Criticizes the president for neglecting critical issues while focusing on campaigning and self-promotion.

### Quotes

- "Intelligence is about establishing intent. It's about telling the future. It's a crystal ball."
- "Have skepticism. Have doubts. But please keep in mind, these reports didn't originate with the intelligence community."
- "Leave them as live action target practice for the opposition."
- "Whether you agree with them being there or not, the US sent them there."
- "While the president is planning rallies and tweeting about his enemies in social media, there are bags being filled and he doesn't care."

### Oneliner

Beau questions the president's knowledge, criticizes inaction on critical intelligence, and challenges blind trust in Putin.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Support troops by advocating for proper action to protect them (implied)
- Pressure politicians to prioritize national security over personal interests (implied)
- Advocate for transparency and accountability in intelligence handling (implied)

### Whats missing in summary

The full transcript provides detailed insights into the administration's handling of intelligence and the implications of prioritizing personal interests over national security. Viewing the full transcript gives a comprehensive understanding of these critical issues.

### Tags

#Knowledge #Intelligence #NationalSecurity #Putin #Troops


## Transcript
Well howdy there internet people, it's Beau again.
So today, once again, we're going to be talking about what the president knew and when he
knew it.
A question we have to ask a lot since this administration took power.
The president's position on what he knew and when he knew it is falling apart more and
more every hour.
The Military Times is now reporting that the presidential daily briefing of more than a
year ago included information on Russia's activities.
It is also suggesting, although not confirmed, that John Bolton briefed the president in
person about it.
John Bolton has not confirmed that though.
More than a year ago, more than a year ago, that's what's being reported by the Military
Times, you know, that great bastion of liberal thought.
We're not talking about CNN, we're not talking about MSNBC, we're not talking about the New
York Times.
I cannot wait to see the president of the United States call the Military Times fake
news.
I'll be honest, I'm waiting for that.
It's going to be great.
The administration's position is evolving.
As this develops, first he didn't know and now, well, it wasn't verified.
That's not a thing, just to let you know.
Intelligence is never really verified.
It's not a term.
Intelligence is about establishing intent.
It's about telling the future.
It's a crystal ball.
The intelligence on the Berlin Wall falling wasn't verified until the bricks hit the ground.
But sure, let's suggest for a moment that the failed casino owner, that he had questions
and didn't think that the people who do this for a living, that they knew what they were
talking about.
What was done to verify it?
What was done to gather more information on it?
If this was just background chatter, it should have launched a massive operation.
This is a huge thing.
It's not a minor development.
This is a big thing.
So what was done to verify it?
What actions did the administration take?
What did they direct the intelligence community or those in the field to do?
Nothing.
We have no information about anything like that at all.
It was ignored.
Why?
Because the president, the failed casino owner, well, he believes that he can trust Putin,
the former Russian intelligence officer.
And understand when I say that, if you don't know, Putin's not former Russian intelligence
the way that Bush was former CIA.
No, Putin was a field officer.
He's one of the most distinguished intelligence officers of the 20th century and Trump, well,
Trump has his number because he had somebody ghostwrite a book called The Art of the Deal.
Nothing was done because the president was too busy back here campaigning, trying to
energize his base, hugging the American flag and saying that he supports the troops.
Unless he means he physically supports them as like a pallbearer, I don't see how that
can be true.
The other argument that's coming out is that, well, we can't trust this.
We can't trust this because it's coming from the intelligence community and you know they're
all liars.
Sure but we'll trust the Russian one.
Look, I get it.
The intelligence community does not have the best public track record.
I understand that.
And that's all fine and good.
Have skepticism.
Have doubts.
But please keep in mind, these reports didn't originate with the intelligence community.
They originated with the troops on the ground.
So when you say you don't trust this, that they're lying, what you're saying is that
the SEALs and Delta that are over there are lying.
You support the troops but they're liars.
You support liars.
Hey, you know what?
If you support Trump, I guess that's at least ideologically consistent.
There were a whole bunch of options that could have been done.
They weren't all military.
The easiest thing to help put pressure on Russia to stop this would have been for President
Trump to stop acting like he's their ambassador on the world stage.
To stop advocating for them.
That really probably would have done it.
Didn't have to be military options, although military options were certainly warranted.
There were a whole bunch of things that could have been done.
The one thing that shouldn't have been done is to just leave them twisting in the wind.
Leave them as live action target practice for the opposition.
Whether you agree with them being there or not, the US sent them there.
Either bring them home or back them up because while the president is planning rallies and
tweeting about his enemies in social media, there are bags being filled and he doesn't
care.
It's appalling.
It's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}