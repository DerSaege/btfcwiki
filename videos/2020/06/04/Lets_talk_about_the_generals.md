---
title: Let's talk about the generals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pasSXWerTYg) |
| Published | 2020/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A number of top military officials, including General Thomas, have issued statements, notably using the word "listen," which is significant.
- General Thomas, the former commander of Special Operations Command, expressed displeasure at referring to the United States as a "battle space."
- The tradition of civilian control of the military usually keeps military officials from speaking out politically, but recent statements indicate a shift.
- The Sergeant Major of the Army, the highest-ranking enlisted person in the Army, also made statements about racial inequality that stirred controversy.
- The Sergeant Major's concise and direct messages were seen as controversial by some, especially his identification of racial inequality as a key issue.
- The use of the word "listen" in the statements by military officials is significant and suggests a message of importance.
- Beau notes the significance of these statements by military leaders who could potentially mobilize volunteers with a single tweet.
- All the generals and influential figures within the military community who have spoken out are unified in opposing the current situation.
- Beau expresses support for military members who have taken a knee with protesters, contrasting it with potential far-right disavowal.
- The overarching theme in the statements from military officials is about the importance of listening and paying attention.

### Quotes

- "NCOs say exactly what they mean."
- "All of the influential people within the military community who have spoken out, they're all on the same side."
- "Listen is not what you'd expect to hear from them if they believed what was going on was a good idea."
- "It's worth noting. It's worth paying attention to."
- "The idea of the far right disavowing soldiers who have taken a knee with protesters has arisen."

### Oneliner

Top military officials break tradition to address political issues with a message of listening and unity against current events.

### Audience

Military members, activists, community leaders.

### On-the-ground actions from transcript

- Contact military officials to express support for their statements and encourage further action (suggested).
- Engage in open dialogues with community members about racial inequality and the importance of listening (implied).

### Whats missing in summary

The full transcript provides additional context on the significance of military officials breaking tradition to speak out against political issues and the importance of unity in addressing current events.

### Tags

#Military #Leadership #Unity #RacialInequality #Listening


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about the brass.
We're gonna talk about the generals,
what they said, what they did,
because in a very, very short window of time,
a whole bunch of stars, a constellation,
have issued statements.
And people are familiar with Mattis and Mullin,
so we'll skip over those.
If you're not, look them up, they're worth reading.
There were some others that came out as well.
The Joint Chiefs pretty much all made statements,
and they all included the word listen,
and we're gonna get to why that's important
here in a minute.
And then General Thomas said something
that I think is noteworthy.
General Thomas is not well known,
however there are generals that are well known,
there are generals that are liked,
there are generals that are loved,
there are generals that are respected,
and there are generals that you don't want angry at you.
I would put General Thomas in that category.
General Thomas was commander of SOCOM.
If you're not a gamer, or you're not familiar
with the military, Special Operations Command.
In your favorite action movie,
in the backstory of the lead character,
they go through his resume, and they're like,
he was a SEAL, or he was Delta.
All of these units are part of SOCOM.
He commanded all of SOCOM.
He's not somebody I'd want mad at me.
I don't think he coordinated his statements
with anybody else.
I think he saw something that upset him,
and he responded immediately.
He took exception to the fact that the United States
was referred to as a battle space.
He thought that was the wrong term to use,
and he is 100% correct.
And that has come up a few times.
A lot of people have asked,
why haven't the brass spoken out sooner?
There's a concept, and it's really important.
I've done a video on it in the past
about civilian control of the military.
The idea is that the US military is apolitical.
It serves the commander in chief, period, full stop.
It does not interject into domestic politics.
That's why we haven't become a dictatorship.
Most of these men feel ethically bound not to say anything.
They feel that that is the best way
they can serve the country,
because we're a republic, we're a democracy,
and therefore, they keep their opinions to themselves.
The fact that you have had so many come out
in such a short window of time to say something
that is political or could be taken in a political way
is very telling.
It's something that we should pay attention to,
because the desire to preserve their service
or the country is more important
than a very long tradition of keeping their mouth shut.
It's something we should really pay attention to.
Now, the far right can write a lot of these generals off,
because there is the idea that once you hit 05,
well, you're really just a politician.
I don't believe that's true with a lot of these people.
However, it's a pretty common concept.
There's somebody else who made a statement,
a couple, actually, that can't be written off as easily.
The Sergeant Major of the Army,
not a Sergeant Major in the Army,
the Sergeant Major of the Army.
He is the senior most enlisted person in the Army.
Not to put too fine a point on it,
he's a god in the Army.
He sent out a few tweets,
and they're basically the last video.
It's incredibly similar.
The only difference is,
it took me like nine minutes to get all of that out.
He's an NCO, a non-commissioned officer.
He's very direct.
You can probably read all of his tweets in like 45 seconds,
and it carries the exact same meaning.
They're very concise.
They say exactly what they mean.
And that is what has caused some controversy.
After his tweets, he made a video,
a very short video, because he's an NCO.
It starts off by saying that the country and the Army
is struggling with racial inequality.
This is not a controversial statement on this channel.
NCOs say exactly what they mean.
So the far right is livid,
because the struggle is racial inequality.
It's not with protesters.
He has correctly identified the source of the problem,
and he said it.
Sometimes when you're talking to NCOs,
what isn't said is just as important as what is.
And then what really drove him over the edge
is they think he was sending a third message as well.
And I'll be honest, I heard the same thing.
However, I would not presume to put words
into the sergeant major of the Army's mouth.
He also used, listen.
The reason this matters,
and you can ask the vets in the comment sections,
if an NCO walks into a room and says, listen up,
everybody stops.
It doesn't matter what they were doing, they stop.
Because there's more information
that may change what they're doing at that exact moment.
The fact that that term was in his statement,
the Joint Chiefs' statements,
and the overall theme of all of the statements
is about listening.
It's important, and I am reading it the same way
as the far right.
The difference is I don't think it's a bad idea.
These are people of action.
Listen is not what you would expect to hear from them
if they believed what was going on was a good idea.
So again, I don't know that that is what he was
intending to convey.
I heard it that way.
A lot of other people heard it that way.
But by what he said, he was sending a message
to the country and a message to the Army.
NCOs say exactly what they mean.
So whether or not the country includes the President,
that's for him to say.
I don't know.
In the midst of all of this, it has of course riled people up,
and the idea of the far right disavowing, quote,
soldiers who have taken a knee with protesters has arisen.
I don't know what disavowing means,
but what I do know is that under normal circumstances,
when we're not all staying at home,
I travel a great deal.
I go to Columbus, Phoenix City, Fort Walton, Tampa,
Clarksville, Dothan.
I'm knee in the bar, and you have a photo on your phone
of you in uniform taking a knee with the protesters.
The next round's on me.
I disavow, because we support the troops,
unless we can't use them for our own political agenda, I guess.
It's important to keep an eye on these statements,
because these are people who realistically could raise
a division of volunteers with a tweet.
All of the generals who have spoken out, all of the brass who have spoken out,
all of the influential people within the military community who have spoken out,
they're all on the same side.
They're all very clear that they think that this is a bad idea.
It's worth the noting. It's worth paying attention to.
Anyway, it's just a thought. Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}