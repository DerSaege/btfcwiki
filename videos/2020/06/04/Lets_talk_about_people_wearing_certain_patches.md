---
title: Let's talk about people wearing certain patches....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VKt6mzWa9oE) |
| Published | 2020/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the origin of the symbol "3%" in the United States, tied to historical inaccuracies about the colonists.
- Mentions that groups using the "3%" symbol are decentralized, with each group being different from the next.
- Describes the mission statement of the "3%" movement as defending the Bill of Rights and stopping government overreach.
- Criticizes individuals wearing the "3%" patch who go against the principles it supposedly stands for.
- Expresses disappointment in law enforcement wearing morale patches without understanding their meaning.
- Concludes by suggesting that law enforcement should refrain from wearing morale patches.

### Quotes

- "If you are currently holding a shield and a club, staring at Americans in the street exercising their free speech and peaceably assembling, and you're attempting to antagonize it and intimidate them and turn it to a situation where it is not peaceable assembly, you are not a threeper."
- "You're literally actively standing against what that patch means while wearing it."
- "Take that patch off."
- "They're not boot lickers. They're the boot."
- "Y'all have a good day."

### Oneliner

Beau explains the origins of the "3%" symbol, criticizes those who misuse it, and suggests law enforcement should avoid morale patches.

### Audience

Community members

### On-the-ground actions from transcript

- Remove any patches or symbols that represent ideologies you do not fully understand or support (implied).
- Refrain from wearing morale patches that carry meanings beyond your understanding (implied).

### Whats missing in summary

Beau's engaging storytelling and passionate delivery convey a strong message against misrepresenting symbols and ideologies.

### Tags

#Symbols #Misuse #LawEnforcement #CommunityPolicing #Accountability


## Transcript
Well howdy there internet people, it's Bo again.
So I was out working in the yard and somebody tweeted me something, so here we are making
a video about it.
Once again, a group of people is wearing a patch that they have no clue what it means
apparently and we're going to talk about that real quick because it's entertaining on some
levels and disturbing on others.
Okay so, in the United States there's a group of people who use 3% as their symbol.
Sometimes it's the number 3 followed by the percent sign, sometimes it's Roman numeral
3 followed by the percent sign.
The name originates from the idea that it only took 3% of colonists to get rid of King
George.
It only took 3% to get active.
Now I'll go ahead and tell you now that's probably historically inaccurate.
But that's where the name comes from.
Okay so, this group of people, people who use this symbol, they're pretty decentralized.
Each individual group that uses it is different from the one right next door.
Some of them are incredibly great guys.
There are some of them that are in the streets with the people right now, no doubt in my
mind.
There are also some that are not great guys.
And now we have a third group of people using this patch.
Okay, the whole mission statement, the whole idea behind this is to defend the Bill of
Rights and stop government overreach.
That's the concept of the 3% movement.
So if you are currently holding a shield and a club, staring at Americans in the street
exercising their free speech and peaceably assembling, and you're attempting to antagonize
it and intimidate them and turn it to a situation where it is not peaceable assembly, you are
not a threeper.
If you are attempting to silence people's free speech, you are not a threeper.
If you are engaging in mass arrests, you are not a threeper.
If you think that the situation that sparked all of this is in any way, shape, or form
justified, or excusable, you are not a threeper.
If you think the solution to the current situation is to drop a bunch of Americans without due
process, you are not a threeper.
I've never seen this much cognitive dissonance in my entire life.
You're literally actively standing against what that patch means while wearing it.
That's something else.
It's embarrassing.
Take that patch off.
I've come to the conclusion that law enforcement just should not wear morale patches.
This is just like the Rule 303 thing.
You have no clue what it meant.
If you think that it just means I support the Second Amendment, you're wrong.
Take that patch off.
Now there are a lot of people who are calling these guys boot lickers.
I don't think that's fair.
I don't think it's fair because it's not accurate.
They're not boot lickers.
They're the boot.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}