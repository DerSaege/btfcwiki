---
title: Let's talk about that designation Trump wants to make....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rX-zwAC4-uQ) |
| Published | 2020/06/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's desire for a certain designation prompts varied opinions, often based on the present moment, but this issue is of universal significance.
- Making such a designation about a domestic group is not currently legally permissible.
- The term in question does not entirely apply to the group in question as they operate beyond the immediate area of action.
- Implementing such a designation wouldn't be effective due to the complex nature of the group's ideology and structure.
- Historical precedents show that legality, effectiveness, and logic do not always dictate government actions.
- Resisting any infringement on rights like the Second Amendment is vital to prevent a slippery slope of restrictions.
- The potential consequences of setting a precedent for designating groups could lead to targeting more fitting organizations.
- The group mentioned fits the criteria of using violence or its threat to achieve certain goals beyond their immediate actions.
- Failure to recognize the danger in supporting such mechanisms can lead to significant consequences for the group itself.
- The importance of maintaining due process and avoiding dangerous rhetoric to protect individual freedoms and rights.

### Quotes

- "In a country, you are only as free as the least free person."
- "You are only as free as the least free person."
- "You will be first."
- "If you give them an inch, they'll take a mile."
- "You guys fit the definition."

### Oneliner

President's desire for a controversial designation raises legal and ethical concerns, warning of potential consequences for individual freedoms.

### Audience

Citizens

### On-the-ground actions from transcript

- Reassess thought leaders and strategists within movements to ensure they protect individual freedoms (implied).
- Advocate for the inclusion of due process in constitutional protections (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the legal and ethical implications surrounding the President's desired designation, urging caution and critical thinking in addressing potential threats to individual freedoms.

### Tags

#ConstitutionalRights #LegalImplications #Designation #IndividualFreedoms #GovernmentActions


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about that designation the President wants to make.
Everybody has an opinion on this, but most people have their opinion based on the here
and now.
It's definitely the wrong take.
It does not matter where you fall on the ideological spectrum, this matters to you.
In fact, those who think that it doesn't matter to them, it matters to them the most.
We're going to go through the frequently asked questions that I've gotten about this.
If you don't like the answers that I'm giving, you more than anybody needs to watch the entire
video because it is personally important to you.
Okay, so number one, is this legal?
No, no.
There is no mechanism currently available that allows the President to make such a designation
about a domestic group.
That's not a thing.
Doesn't exist.
Are they really that term?
Not really.
That term has a couple of main components and they're missing some.
The most important of which is that when they do something, they're doing it on scene and
it's not intended to influence those beyond the immediate area of action.
If you're not doing that, that term really doesn't apply to you in an academic sense
by definition.
Would it be effective?
Not really.
The main reason is because it's not just a bunch of small autonomous groups that barely
even talk to each other.
It's also an ideology and once a designation like that is made, somebody would have to
go through and try to differentiate between those who are just adherence to the ideology
and those who are active members in some group.
That's going to be almost impossible.
So no, it's not legal.
No, it doesn't make sense and no, it wouldn't be effective.
But that doesn't mean the government wouldn't do it.
The government has a long history of doing things that aren't legal, aren't effective
and don't make sense.
So that alone means nothing.
Okay, but here's the important part.
And if you didn't like those answers and you want to debate those, just skip it.
Just fine.
Whatever you say is true.
But listen to the next part real carefully because you're probably on the right.
If you didn't like those answers, you're on the right.
Why do you guys oppose any infringement on the second?
Because if you give them an inch, they'll take a mile.
The precedent gets set, right?
Hope you see where I'm going with this.
Okay, so let's say the designation gets made.
What's next?
Well, we round them up.
No, that's not what I mean.
I understand the plan.
What group is next?
A group that actually fits the definition and has the organizational structure.
I will give you a hint.
Roman numeral three, buddy.
You better get that sticker off your car.
Wait, no, no, no.
I support the Constitution.
Yeah, first, that has nothing to do with it.
Apparently, all of it, but due process.
Yeah, so let me just go ahead and tell you the part that matters.
The unlawful use of violence or the threat of violence to influence those beyond the
immediate area of action to achieve a political, religious, ideological, or monetary goal.
That's you guys.
That's three percent.
And your organizational structure, dude, a lot of you guys have rank zones.
I can assure you that if this precedent is set, it will be used against right-wing groups.
Trump would never let that happen.
Yeah, right.
That's probably true.
But unless you plan on making him king and totally scrapping the Constitution, you would
have to assume that at some point a group that isn't favorable to you would take power
and the precedent is set.
You guys fit the definition.
You have the organizational structure, and you have been linked to some pretty dramatic
events.
You will be first.
As soon as the precedent is set with somebody that has been successfully villainized, you're
next, guaranteed.
This is why rhetoric is really dangerous.
This is why soldiers are supposed to be apolitical, because that kind of rhetoric blinds their
judgment like this.
I would strongly suggest that if your thought leaders in your movement haven't realized
this, you need new thought leaders, you need new strategists, because they've got a lot
of you cheering the very mechanism that is going to be used to crush you.
In a country, you are only as free as the least free person.
I would strongly suggest that you add due process back into the parts of the Constitution
you like.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}