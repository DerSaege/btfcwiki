---
title: Let's talk about how Trump can create immediate reform....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M71z6K7IkjU) |
| Published | 2020/06/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the need for solutions to complex issues.
- Proposing a solution involving accountability for law enforcement officers.
- Suggesting the use of executive orders to enforce consequences.
- Creating a new agency to monitor use of force complaints.
- Holding departments accountable for addressing complaints.
- Using federal funding eligibility as a leverage point.
- Emphasizing the potential for immediate impact through this solution.
- Not claiming it will solve all problems but will reduce them significantly.
- Anticipating a shift in law enforcement culture with this accountability.
- Stressing the simplicity and legality of implementing this solution.

### Quotes

- "The end goal is to get rid of the sparks, right?"
- "Super quick, super easy. Can be done with a pen stroke."
- "No debate. It's just done."
- "Immediate reform."
- "Hopefully, it could change the culture within the departments."

### Oneliner

Beau proposes an executive order to hold law enforcement accountable, ensuring immediate reform and cultural change through federal funding leverage.

### Audience

Law Enforcement Reform Advocates

### On-the-ground actions from transcript

- Advocate for accountability measures in law enforcement (implied)
- Support initiatives that ensure consequences for officer misconduct (implied)

### Whats missing in summary

Details on how this proposed solution can be effectively communicated and advocated for in various communities and to policymakers.

### Tags

#LawEnforcement #Accountability #PoliceReform #ExecutiveOrder #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about solutions to the problems.
Because I had a couple people message me saying, you know, well you talk about this, but you're
not offering any solutions.
This is a very complex issue, and it's going to take a lot of time to unravel.
There is no shortcut.
No there is.
Okay.
So what's the goal?
The end goal is to get rid of the sparks, right?
That's the end goal.
We want to stop the inciting incidents from occurring.
That's the whole idea.
We can do that, we can achieve peace and love and harmony and all that good stuff.
So why make it more complicated than it needs to be?
We can use a tactic that President Trump likes to use all the time.
Money.
Doesn't even require going through Congress or the Senate.
It's a pen stroke.
Okay.
So Trump signs an executive order.
And that executive order requires that any officer who has had more than one use of force
complaint within the last 12 months, whether it is founded or not, is terminated within
30 days or the department loses all eligibility for federal funding of any kind.
The sharing programs, the equipment transfer programs, the training grants, everything.
It's all gone.
From that point forward, we have a new agency.
Put it in the Bureau of Crime Statistics or whatever.
Okay.
This agency's job is to evaluate use of force complaints from that point forward.
Now the department still evaluates them, but this agency evaluates them too.
And it's not staffed by cops.
It's staffed by civil rights advocates.
I'm sure there are a whole bunch of people with the ACLU that would be happy to go through
the investigative training required to determine whether or not these cases are founded or
unfounded.
Now if they find that any complaint is founded, any complaint, even just one, that person's
gone.
Permanently.
Their name goes into a database.
If you are a department and you hire them, you become ineligible for federal funding.
Now that seems like a whole lot of work for this new agency.
So let's make the departments kind of police themselves, clean themselves up a little bit,
right?
Okay so the next phase of this is to say that if a department finds a complaint unfounded
and the new agency finds it as founded, they lose eligibility for federal funding.
There you go.
Super quick, super easy.
Can be done with a pen stroke.
No debate.
It's just done.
If Trump wants to lead, wants to show that he's tough, wants to show that he can handle
it, there's a solution.
Because it will have an immediate impact.
Is it going to solve all of the problems?
No but it's going to drastically reduce them real quick and it is going to change the culture
within law enforcement because the leadership is going to require it.
Because otherwise, they can't have all those admin positions.
They're not going to be able to afford them.
They're not going to have their cool little toys.
It's going to be real hard to pay for a lot of the stuff that they have because they're
not getting that funding.
It's an executive order.
There's nothing unconstitutional about it, nothing illegal about it.
Real simple.
Immediate reform.
And hopefully, it would change the culture within the departments because they have to
accurately assess whether or not a complaint is found because they know they're going to
be checked.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}