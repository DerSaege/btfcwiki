---
title: Let's talk about removing qualified immunity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kgYuH3SyrGM) |
| Published | 2020/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains qualified immunity as a shield that limits officers' liability, shifting it to taxpayers.
- Notes the current momentum to remove qualified immunity and make officers carry insurance.
- Defends officers' response that they will only show up for real emergencies if personally liable.
- Views officers' unintentional laying of foundation for police reform positively.
- Supports the idea of officers responding only to actual emergencies to prevent unnecessary situations.
- Suggests officers should limit involvement to emergencies, benefiting both them and the public.
- Advocates for giving freedom a chance by reducing over-policing.

### Quotes

- "We don't want to be over-policed."
- "Only respond if it's an actual emergency, if there's a victim."
- "Can we please just give freedom a chance?"

### Oneliner

Beau supports removing qualified immunity, advocating for officers to respond only to real emergencies to prevent over-policing and improve safety.

### Audience

Advocates for police reform.

### On-the-ground actions from transcript

- Support initiatives aiming to remove qualified immunity from law enforcement (suggested).
- Advocate for policies that encourage officers to respond only to actual emergencies (suggested).

### Whats missing in summary

Beau's reasoning behind supporting officers responding solely to real emergencies and the potential positive impact on society.

### Tags

#QualifiedImmunity #PoliceReform #OverPolicing #CommunitySafety #Freedom


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk a little about qualified immunity and how people are responding
to the idea of taking it away.
If you don't know, qualified immunity is basically a shield.
It's what limits the liability of individual officers so their actions are borne more by
the taxpayer, by us, than them as individuals.
A concept that is gaining momentum right now is to remove this, make them carry insurance
or something like that on their own.
Now on social media right now you can see a bunch of officers responding and giving
their thoughts on this.
I'm here to defend their thoughts because I think they're right.
I think they're right.
Basically what they're saying is that if that happens and qualified immunity does get stripped
away and they become personally liable for their actions, that they're only going to
show up if it's like a real emergency or there's a victim.
Yes please do that, that's actually what we want.
Now they're saying this out of spite, to them it's an edgy comment, but the reality is they're
laying down a pretty solid foundation for real police reform.
They're doing it by accident, but yes, that's a great idea.
Think about what this threat actually says.
They're worried that they may get sued because they used force in a situation in which there
was no victim.
There was nobody being harmed until they arrived.
Yeah, you should stop that.
That's a great idea.
If officers actually stuck to this, it would limit a lot of situations from happening.
It really would.
I think this is a fantastic idea.
Yes, only respond if it's an actual emergency, if there's a victim, if somebody's at risk.
Otherwise, sit in your car, eat your donuts, flip through Ranger Joe's catalogs or whatever
it is you do when you're waiting.
The American people will be just fine with that.
They're really okay with it.
We don't want to be over-policed.
Yeah, respond if there's an actual emergency, if people's lives are at risk, if there's
a victim.
Other than that, chill.
It's going to make your life a whole lot easier.
It really will.
You're going to have less work.
The people are going to feel more safe.
Everybody wins.
Can we please just give freedom a chance?
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}