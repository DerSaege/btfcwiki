---
title: Let's talk about if cops are effective....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zQz0YL9X0P8) |
| Published | 2020/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defining effectiveness as producing the desired result or outcome.
- Questioning the current effectiveness of law enforcement.
- Suggesting to use case clearance rate as a metric rather than conviction rate.
- Providing statistics from 2017 to analyze effectiveness.
- Homicide clearance rate at 61.6% - not highly effective.
- Rape clearance rate at 34.5% - especially low due to underreporting.
- Robbery clearance rate at 29.7% - not effective.
- Aggravated assault clearance rate at 53.3% - still lacking.
- Property crimes like burglary and theft with clearance rates below 20%.
- Advocating for reform to focus on crimes with victims and improve effectiveness.
- Stating that the current system's ineffectiveness is not a reason to avoid reform.

### Quotes

- "They're not effective now."
- "We need reform. We need it."

### Oneliner

Beau questions the effectiveness of law enforcement, using case clearance rates to show the system's ineffectiveness and advocate for reform.

### Audience

Community members, activists, policymakers

### On-the-ground actions from transcript

- Advocate for reforms to improve law enforcement effectiveness (implied)
- Support initiatives focusing on crimes with victims (implied)

### Whats missing in summary

Detailed breakdown of specific reforms needed to enhance law enforcement effectiveness.

### Tags

#LawEnforcement #Effectiveness #Reform #Crime #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to be talking about
the effectiveness of law enforcement.
We're gonna do that because apparently
some people don't know what the word effective means.
Effective means it is producing
the desired result or outcome.
Pretty simple definition.
That's what it means.
It doesn't mean status quo.
Right now you're hearing a whole bunch of people say,
if we reform law enforcement,
well we'll be taking away the tools they need to do their job.
They won't be effective anymore.
The question is, are they effective now?
And I'm not talking about the obvious stuff
that we've been talking about all week.
I'm talking about in general, is law enforcement effective?
Now, the metric that you should use to determine this
is not conviction rate.
Because that's not fair to law enforcement,
to be completely honest.
A lot of times law enforcement can do their job,
and when it gets turned over to prosecution,
the ball gets dropped.
So that's not a good metric.
The metric you should use is case clearance rate.
Now what that means is a case gets opened,
law enforcement conducts their investigation,
they drop charges on somebody, and they turn it over.
That's a cleared case.
Now sometimes cases get cleared through what
is called exceptional circumstances
or exceptional means.
This is when the suspect takes themself out
before the cops get to them, or they flee the country,
but they know who did it.
They have solved the case.
That's the metric to use.
Now we're going to use the numbers from 2017
because it's a pretty good year.
It shows a pretty effective year for law enforcement.
We're going to use those numbers.
You can find these at the FBI's statistics on this.
I'll put a link down there.
OK, so what about the big one, homicide?
What would you imagine the clearance rate is on this?
61.6.
It's not really high.
It's not incredibly effective.
Keep in mind, this is only factoring in cases that were opened.
The person that just turns up missing,
they're not involved in this.
They're not factored into these numbers.
Realistically, the real number here on solved
is probably about half.
It's probably about 50%.
Not incredibly effective.
Rape, 34.5%.
Not incredibly effective, especially given the fact
that we know that is the most underreported crime
in the country.
If you factor in the information and the data we have on that,
the true effectiveness of law enforcement
in dealing with that crime drops to these single digits.
It's not effective.
Robbery, 29.7.
OK.
Ag assault, 53.3.
And keep in mind, that normally occurs
between people who know each other.
Really easy to identify.
Yeah, that's not great.
That is not effective.
I would suggest that that's not effective.
Let's go to property crimes that have a victim.
Burglary, 13.5.
Keep in mind, this is also heavily underreported.
When your neighbor had their garage broken into
and they didn't call the cops because they didn't want
to spend three hours filling out paperwork
and waiting for them to show up, knowing they weren't going
to do anything about it, that doesn't get factored in.
This is only where cases were opened.
Larceny theft, 19.2.
Motor vehicle theft, 13.7.
If this was grade school and this was your report card,
I would imagine your parents would be pretty upset with you.
This is not effective.
The system as it stands is not effective.
So we cannot use the effectiveness
of the current system as a reason not to reform.
The reality is, some of these reforms would allow law
enforcement to spend more time dealing with stuff
that actually matters.
Crimes with a victim.
I would suggest that limiting the scope of law enforcement
would allow them to refocus their training on perhaps
basic investigative skills rather than learning how
to play with their cool new toys.
The idea that we should postpone reform
because it would hamper the effectiveness of law
enforcement is completely blown out of the water
when you realize law enforcement is not
effective to begin with.
It does not produce the desired result or outcome.
We need reform.
We need it.
Now, just to be clear, I have not
read the legislation that the Democrats just put out.
I haven't seen it yet.
So this isn't really in favor of that
because it may be completely watered down.
It may have snuck stuff in.
I haven't looked at it yet.
I'll be doing a video on it later.
But what this is, this is the information
you need to be able to pull out of your hat
when you encounter that argument.
Well, we can't stop them from doing
all of these horrible things because it may make them
ineffective.
They're not effective now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}