---
title: Let's talk about Gil Scott-Heron and it being televised....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q-gy4BFvaUA) |
| Published | 2020/06/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the meaning behind a popular line from a song that has become a touchstone in popular culture.
- The line "the revolution will not be televised" is often used as a battle cry against network news censorship, but the true meaning may have been lost.
- Beau delves into the original intention behind the line by Gil Scott Heron and its significance.
- Contrary to common belief, the phrase suggests active participation in the revolution rather than lack of media coverage.
- The song conveys the message that real change starts with a shift in individuals' minds before it manifests in actions and movements.
- Emphasizes the importance of people coming together, being in sync, and not allowing the media or establishment to control the narrative.
- Advocates for unity, communication, and active engagement to bring about meaningful change.
- Encourages disregarding divisive narratives and focusing on collective evolution and progress.
- The message is to reject mainstream narratives, unite with others, and drive change from within, not relying on external sources.
- Beau stresses the relevance of these ideas in the present context and the power of united action for genuine transformation.

### Quotes

- "The revolution will not be televised."
- "The first change that takes place is in your mind."
- "If you want real change, if you want revolution, it's going to occur with us."

### Oneliner

Exploring the true meaning behind "the revolution will not be televised" by Gil Scott Heron and advocating for active participation and unity to drive real change.

### Audience

Activists, Community Members

### On-the-ground actions from transcript

- Unite with others in your community to drive meaningful change (exemplified)
- Reject divisive narratives and focus on collective evolution (implied)
- Communicate effectively with others to stay in sync and bring about positive transformations (exemplified)

### Whats missing in summary

The full transcript provides a deep dive into the original message behind a popular cultural reference and urges individuals to actively participate in driving real change through unity and collective action.

### Tags

#Unity #CollectiveAction #SocialChange #CommunityEmpowerment #GilScottHeron


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're gonna talk about a single line
out of a song that has become a touchstone.
It's become something that pretty much everybody's heard.
They may not even know it came from a song,
but they've heard this one sentence.
They've probably seen it on memes.
Maybe even heard it in movies
because it gets referenced a lot.
But because of that,
because how it was used in the memes and in popular culture,
some of the meaning may be lost.
Now, it gets shown most times as a battle cry,
as a way of saying that network news is censoring things,
and therefore the revolution will not be televised.
But that may not have been what the author meant.
Now, we have that idea that once something enters
public consumption, once it's out there,
it can be interpreted however the public wants,
and that's fair game.
And that's true.
It's a concept called the death of the author.
But normally when we invoke that,
we can't actually ask the person that wrote it.
And in this case we could, and somebody did.
So we actually know what he meant.
The revolution will not be televised.
Gil Scott Heron.
If you haven't heard it yet,
stop now and go ahead and listen to it,
and then come back.
You'll notice the first two lines.
You will not be able to stay home, brother.
You will not be able to plug in, turn on, and cop out.
By the way, that's a play on words right there.
That's funny.
From those first two lines,
you realize that it's not really that it won't be televised.
It's that you're not gonna be home.
You're gonna be participating in the revolution live.
You're gonna be out there doing it.
That's what it's saying.
Just in those first two lines, you can pick that up.
Yes, there's definitely a whole bunch of stuff
that follows this that speaks to the way,
the powers that be, the establishment frame things
and control a narrative.
But the real message here may not be
that censorship is the problem, not necessarily.
Because when he was asked, he said,
the first change that takes place is in your mind.
You have to change your mind
before you change the way you live and the way you move.
The thing that's going to change people
is something that nobody will ever be able
to capture on film.
It's just something that you see and you'll think,
oh, I'm on the wrong page,
or I'm on the right page, but the wrong note.
And I've got to get in sync with everyone else
to find out what's happening in this country.
It's not that the media is going to censor the revolution.
It's that we're going to ignore the media,
that we're not going to buy into the narrative.
We're not going to play red team versus blue team.
We're not going to allow the establishment
to control our thoughts
and take control of that narrative and hijack it
and get it to where everybody gets angry for a few weeks
and then everybody goes back to the status quo
because that's normally what they want.
You ignore it. You change.
The people get in sync.
Man, that seems really, really relevant right now.
If you want real change, if you want revolution,
it's going to occur with us.
We're going to evolve.
We're going to get on the same page.
We're going to move in the same direction,
and we are going to change things.
And it doesn't matter what the establishment thinks.
It doesn't matter what the narratives in the news are
because we're plugged in to each other. We're communicating with each other.
We're doing it live. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}