---
title: Let's talk about a lesson from the University of Florida for all of us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vN1m734i8ec) |
| Published | 2020/06/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the perception, association, and tradition lesson from the University of Florida.
- Conducts a demonstration using military contractors to illustrate a point about perception and association.
- Talks about the decision at the University of Florida to stop using a specific cheer due to its term.
- Describes the origins of the cheer and how it may evoke different connotations for different people.
- Emphasizes the importance of considering history and perceptions in making small changes for a better experience.
- Urges for moving forward and honoring more forward-thinking traditions.
- Advocates for acknowledging different perspectives and making small changes to enhance everyone's experience.
- Suggests that small changes, when combined, can lead to a significant shift in societal norms.
- Encourages embracing change and not hindering progress for the sake of tradition.

### Quotes

- "Let the small change happen. Enhance everybody's experience."
- "Little baby changes that can add up."
- "Don't be the person that stands in the way of that just for tradition's sake."
- "Let's move forward at UF. Let's move forward in this country."
- "Small changes. A bunch of them. By themselves, they mean nothing. But together, it's a revolution."

### Oneliner

Beau explains the importance of acknowledging history, making small changes, and moving forward for a better experience at UF and in the country.

### Audience

Students, alumni, community members

### On-the-ground actions from transcript

- Support the decision to drop offensive cheers at local events (exemplified)
- Advocate for small changes to enhance experiences in your community (suggested)

### Whats missing in summary

The full transcript provides a detailed example of how perceptions can differ based on history and tradition, urging for small changes to improve collective experiences.

### Tags

#Perception #Tradition #SmallChanges #Community #MovingForward


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what the entire country, all of the United States,
can learn from the University of Florida.
We're going to talk about perception, and association, and tradition.
I'm going to say something incredibly surprising, and when I do I need you to stick with me
because there's a point to it.
And we're going to do all of this because right now in Gainesville, Florida at UF, there's
a conversation happening that should be happening all over the U.S.
It encapsulates it.
Okay, but before we get into that I need to conduct a little demonstration.
What I'm going to say is 100% true, but I need you to stick with me no matter how surprising
it may seem.
If you don't know, I know literally hundreds of current or former military contractors.
Something I know to be 100% true with absolutely no exceptions is that every white contractor
follows the law, and every black contractor I have ever met is a criminal.
I imagine there's a whole bunch of raised eyebrows, except for those who are contractors
because they know where I'm going with this.
White and black in this context have absolutely nothing to do with skin tone.
Has to do with the type of assignments you would be willing to accept.
Black contracting is illegal.
That's what it means.
So yeah, every black contractor is a criminal.
I would like to point out just for the sake of clarity that probably 97% of the contractors
I know that have done black work are white.
They look like me.
But it made you uncomfortable when I first said it because that's not the connotation
it has for you.
That's not the association you make.
You don't know that terminology.
You don't know the origins of those terms.
So it's a little uncomfortable.
Now for me to alleviate that and elevate everybody's experience, all I have to say is those who
engage in black contracting are criminals.
Then it's clear.
Small change on my part means absolutely nothing, enhances everybody's experience.
Sounds good.
Okay, now let's get to UF.
The University of Florida has decided to stop using a very specific cheer because it has
a term in it.
Now if you went to UF or you're an ACR, you know that the term doesn't, it didn't originate
the way a lot of people think it did.
It has no racist connotation.
It came from actually a black guy saying if you're not a gator, you're gator bait.
Has to do with the school's mascot.
Nothing else.
However, outside of that circle, the perception may be a little different.
Maybe not to everybody, but to a significant portion of people, it might be different.
It may evoke images of something that's pretty horrible, especially in the South, especially
among black Americans.
If you are overseas and don't know or you're from another part of the country and may not
know, gator bait was a term that was used in a very derogatory fashion up to the early
1900s, I think.
And then during slavery, it was literal.
It was literal.
I don't want to get too far into it because it is absolutely horrible.
This picture, slavery, babies, gator bait, in the most literal sense you can imagine.
So to those from Gainesville or those who went to UF, you know what it means.
You know it has nothing to do with that whatsoever.
In the context it's being used at these games, that's not what it means.
But to everybody else, maybe a significant portion, it's going to evoke some other images.
It's something to consider, especially given when that cheer gets used at a sporting event
where everybody's supposed to be having a good time.
Why evoke those images if you don't have to?
And I get it.
It's tradition, kind of.
I mean it's been around like 25 years.
It's a tradition, but it's not a huge one.
You know, it's not a long-lasting one.
It's a cool saying.
And believe me, I spent enough time throwing up on the side of University Avenue.
I know.
I get it.
But there's another tradition at UF.
There's another one that I think is probably more important.
When you're talking about the South, name a more forward-thinking school.
It's going to be real hard.
I think that may be the tradition that's more important to honor.
We can look back or we can look to the future.
Yeah, it doesn't really have that meaning.
It doesn't.
That's not where it came from.
But the perception is there.
And all it takes is a little change to enhance everybody's experience.
And that's what's happening nationwide.
That's the conversation that's going on.
We have the way things were and the way things could be.
And all it takes is a bunch of little changes to enhance everybody's experience.
And all it takes to make that happen is just to acknowledge that everybody doesn't see
things the same way.
That the perceptions of things, because of history, color everybody's view of different
events.
And all we have to do is to take that into consideration.
Not everybody knows the origin stories to stuff.
Little changes can enhance everybody's experience.
Especially when you're talking about stuff that's deep-rooted.
Small changes.
A bunch of them.
By themselves, they mean nothing.
But together, it's a revolution.
It's a complete shift in the way we do things.
Little baby changes that can add up.
I think it's fitting that it's happening at UF.
And the school made the decision.
They're dropping the cheers.
However I also know enough about Gainesville to know that the students may do it on their
own.
Or those who, the alumni, who come back for games.
Don't.
Don't.
Let the small change happen.
Enhance everybody's experience.
Don't be the person that stands in the way of that just for tradition's sake.
Let's move forward.
Let's move forward at UF.
Let's move forward in this country.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}