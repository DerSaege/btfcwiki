---
title: Let's talk about how we've seen enough....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rgGORxzuoxQ) |
| Published | 2020/06/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People are expressing they've "seen enough" in response to footage from across the United States, leading to forward-thinking ideas for law enforcement reform.
- Suggestions range from drastic reforms to possible abolition, replacing law enforcement duties with social workers.
- Beau doesn't advocate for any particular idea but addresses common questions: what if cops quit with reforms, and what if reforms go too far?
- Beau dismisses concerns about cops quitting due to reforms, saying it's a reason to celebrate if they do because they shouldn't be cops if they resist positive change.
- He challenges worries about reforms being ineffective compared to the current state of law enforcement ineffectiveness captured in news footage.
- Beau believes that the level of force used by law enforcement is unacceptable, criminal in many cases, and far beyond mere unprofessionalism.
- He criticizes departments for merely suspending officers involved in misconduct, suggesting that such behavior should result in jail time.
- Beau insists on the need for drastic change in law enforcement and defers to experts on determining the specifics.
- While unsure of the best solution, Beau is certain that the current situation involving excessive force is entirely wrong.
- He supports officers quitting if they can't adapt to the necessary changes, as the public has witnessed more than enough.

### Quotes

- "If they're gonna quit because new policies are coming along that are gonna stop them from engaging in excessive force and hurting people, they shouldn't be cops."
- "We need drastic change, and I will defer to the experts on what that change needs to be."
- "It's exactly what should happen because we have seen enough."

### Oneliner

Beau challenges concerns about law enforcement reforms and supports drastic change, asserting that quitting over refusing to adapt to new policies is warranted.

### Audience

Law enforcement reform advocates

### On-the-ground actions from transcript

- Celebrate and support officers quitting if they resist reforms (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the current state of law enforcement and the urgent need for drastic change to address excessive force and misconduct.

### Tags

#LawEnforcementReform #ExcessiveForce #SocialChange #CommunityAction #Accountability


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about how we've seen enough.
Been hearing that a lot.
Hearing that a lot over the last 24 hours.
We've seen enough.
And the more footage that comes out
from the various corners of the United States,
the more we hear that phrase, we've seen enough.
And that phrase is normally followed by
what I would call forward thinking ideas.
They are forward thinking ideas.
They range from just drastic reforms of law enforcement
to things bordering on abolition,
where we're replacing large chunks of the duties
of law enforcement with social workers.
I've seen calls for total abolition of law enforcement.
I'm not gonna stand here and make the case
for any of those ideas.
It's kinda outside of my wheelhouse.
There are people that are better suited
for making those cases.
However, when those forward thinking ideas are brought up,
there are two questions that keep popping up.
I can answer those.
The first is, well, if we enact these reforms,
what do we do if all of our cops quit?
If you enact reforms to stop your officers
from engaging in excessive force and they quit,
you throw a party.
That's what you do.
You throw a party, you celebrate, dance in the streets.
Nobody's gonna stop you.
If they're gonna quit because new policies are coming along
that are gonna stop them from engaging in excessive force
and hurting people, they shouldn't be cops.
You should be happy they're quitting.
That's not an argument.
That's not a real question.
That doesn't even make sense.
Oh, what would we do if we didn't let them do this?
The next is, well, what if the reforms go too far
and they become ineffective?
Ineffective.
As compared to what exactly?
The pinnacle of effectiveness that we've seen this week?
What are you worried about happening?
Unaccountable men running through the streets,
hurting people?
We see it.
They're wearing badges, most of them.
Cities burning?
What are you worried about specifically happening
if they become ineffective?
We've seen a montage of ineffectiveness all over the news.
We are past the point of retraining in most departments,
specifically most large departments.
We are past the point of retraining.
There needs to be drastic change.
Now, I don't know which idea is best.
I can't tell you that.
That's probably something that's gonna have to be decided
on a community-by-community basis.
What I can tell you is the amount of force
that has been used this week is unacceptable.
It is inexcusable.
I believe that in a whole lot of cases, it's criminal.
We're well beyond simple unprofessionalism here.
Some of the stuff that is captured on video
is just wrong on every level.
And the department's responses are typically,
oh, well, we suspended those officers.
I don't know where I'm from.
Assault and battery typically put somebody in jail.
I don't know about the rest of the country,
but I'm fairly certain that if that happened here,
our sheriff would throw the guy in jail.
I don't think that suspension would be enough,
especially when there's a video.
We need drastic change, and I will defer to the experts
on what that change needs to be.
But what I do know about is force,
and this is all wrong, every bit of it.
And if your cops, if your officers,
if your deputies want to quit
because they can't use excessive force, good.
It's exactly what should happen because we have seen enough.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}