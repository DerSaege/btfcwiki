---
title: Let's talk about Trump, believability, and the allegation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xw1YyMJ6NUg) |
| Published | 2020/06/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States faced wild allegation involving Russian intelligence incentivizing attacks against American soldiers in Afghanistan.
- Legal definition of treason in the US is narrow; looking the other way during attacks isn't legally treasonous.
- People are justifying President's inaction with excuses, rather than denying the allegation's truth.
- Russia's motive could be to derail peace deals and weaken the US military by incentivizing attacks.
- Allegation's truth doesn't matter; what's relevant is the President's history of self-centered actions benefiting Russia.
- American people lack confidence in the President; the allegation should be unbelievable but is disturbingly believable.
- The allegation's believability should be reason enough for resignation or non-re-election.
- Republican Party's support for the President implicates them in his actions, including this latest allegation.
- President mobilized to protect statues of racists but didn't act to protect American soldiers in Afghanistan over months.
- President's track record of lies makes it plausible he'd allow soldiers to die to avoid admitting fault or losing support.

### Quotes

- "It is completely believable that Russian intelligence would do this. It is completely believable that Trump would look the other way."
- "The president of the United States does not have the confidence of the American people."
- "If the Republican Party hands him the nomination, they own this."

### Oneliner

President faced wild allegation of inaction amid Russian intel on attacks against US soldiers; his history makes the disturbingly believable allegation enough for resignation or non-re-election.

### Audience

American voters

### On-the-ground actions from transcript

- Pressure elected officials to hold the President accountable (implied)
- Stay informed and engaged in politics to ensure accountability (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the wild allegation against the President and the implications of his inaction, urging for accountability and reflection on his actions.


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about
the President of the United States,
the Commander in Chief,
a wild, wild allegation,
believability and treason.
At the end of the day, believability is all that matters,
to be honest, but we'll get there.
An allegation has surfaced,
made against the President of the United States,
that is just wild.
It is wild.
It has been called
gross dereliction of duty,
gross inaction,
cowardice, treason.
Now if you don't know what the allegation is, just to clue you in,
it has been reported
that the President of the United States received an intelligence assessment
months ago,
indicating
that Russian intelligence
was operating in Afghanistan
and attempting to incentivize attacks
against
American soldiers,
and
that he just looked the other way
and responded
in no way whatsoever.
Wow.
I know, sounds like treason.
It's not. I'm going to go ahead and tell you that now.
It's not. Legally speaking, it's not.
The legal definition of treason in the United States
is so narrow
that you pretty much have to try
to commit it.
That's why President Trump's accusation against President Obama
is so laughable.
This,
looking the other way while Americans get hit,
that's not treason
under the law.
Now, in general conversation, if somebody was to be like, that's treasonous,
you wouldn't hear an argument from me.
Um...
But, it's not that legally speaking.
Okay, so
the allegation's been made, now what?
People are already manning the battle stations,
coming up with
excuses
for the President's actions, or inactions.
Um...
You know,
the funny thing about it,
and this is going to be real important later,
is that nobody's saying it's not true.
They're just trying to justify it.
Well, see, what was going on was,
you know, he wanted to get out of Afghanistan so bad, and they were just
acceptable losses.
Yeah, that sounds great, but that's proven you've got no idea what's going on
over there.
Um...
There was a peace deal
in the works.
These attacks
would serve to derail it.
That's why
Russia
would be doing something like this,
to keep us bogged down in a quagmire, sap our military might.
It is completely believable
that Russian intelligence would do this.
It is completely believable
that Trump would look the other way.
That's why people are already attempting to justify it,
because they don't want to look dumb
and say, well, he didn't do that, and then, of course, find out later on that he did.
So they're just trying to justify the behavior now.
Just get out in front of it.
Um...
I'm going to suggest
that the argument
over whether or not
it happened the way it's being reported,
I'm going to suggest that that argument doesn't matter
at all.
It's completely irrelevant
whether or not this allegation is true.
This allegation is so wild
that it should be unbelievable.
But it's not.
It's not, because the president has shown time and time again
that he only cares about himself.
He's shown time and time again that pretty much any action he takes on the
international stage
benefits Russia.
It's completely believable.
That should be enough.
The fact that when this report came out
nobody doubted it,
that should be enough.
The president of the United States
does not have the confidence of the American people.
An allegation
that should be dismissed out of hand
as just being
just unbelievable,
there's no way this could be true, isn't.
It's not being dismissed
because it probably is true
as he hugs the American flag.
I would suggest that it does not matter at all
whether or not it's true.
Just the fact that it was believable
should be enough
for him to resign
and certainly
for him to not be
re-elected.
If the Republican Party hands him the nomination,
they own this.
They own this.
They've owned everything else that this administration has done
and they've co-signed it all.
This
is something that you have to get into the technicalities
about whether or not it's treason.
Give him the nomination again.
I would like to point out
that it is completely believable
that the president of the United States
mobilized on a national level
to protect statues of dead racists
but couldn't mobilize
to protect American soldiers
serving overseas.
In months.
This wasn't something that was just in the heat of the battle,
something that just
got away from him.
Months.
Not even a diplomatic protest.
The Kremlin didn't know about it
until they were asked
by reporters.
They said nobody had made that accusation but they would respond if somebody did.
That's the state of the presidency today.
And it is because we have elected a man
who is unbelievable.
A man who has a track record of lying openly so much
that it's completely
within the realm of possibility
that he would choose to allow American soldiers to die
so he didn't have to admit he's wrong.
So he didn't have to
take a hit in the polls.
Anyway.
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}