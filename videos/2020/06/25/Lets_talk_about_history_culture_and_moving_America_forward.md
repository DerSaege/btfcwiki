---
title: Let's talk about history, culture, and moving America forward....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xSLhJpj0gXs) |
| Published | 2020/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Addressing the concern of erasing history by putting disclaimers in front of movies, not idolizing certain songs and cartoons, or taking down statues.
- Proposing a need to rebuild history to benefit from the warnings it provides.
- Sharing a piece of the Berlin Wall as a symbol of history in the making and a celebration of freedom.
- Acknowledging the necessity of warnings in history, especially in light of recent events.
- Suggesting replacing statues of slave owners with monuments to abolitionists like Harriet Tubman and John Brown.
- Emphasizing the importance of learning from history's mistakes and not romanticizing or mythologizing it.
- Pointing out the hypocrisy of those defending racist historical figures while denying the existence of systemic racism today.
- Advocating for cultural changes through popular culture to address deep systemic issues.
- Urging for a change in mindset alongside changes in laws to create a more inclusive society.
- Encouraging people to be on the right side of history by acknowledging and addressing the country's past mistakes.
- Calling for the rebuilding of history through monuments that represent positive change and progress.
- Arguing that keeping up statues of controversial figures is embarrassing to the country and suggests putting up monuments that make people proud instead.

### Quotes
- "Freedom will crash through anything eventually."
- "We have to change it all."
- "Let's rebuild history."
- "Make America great."
- "They're embarrassing to the country. Take them down."

### Oneliner
Beau addresses the need to rebuild history by learning from its mistakes and replacing controversial statues with monuments that make us proud.

### Audience
Activists, Historians, Educators

### On-the-ground actions from transcript
- Replace statues of controversial figures with monuments to those who have positively impacted history (implied).
- Advocate for cultural changes through popular culture by promoting inclusive songs, cartoons, movies, and statues (implied).

### Whats missing in summary
The full transcript provides detailed insights into the importance of acknowledging and learning from history's mistakes, advocating for cultural changes, and replacing controversial statues with monuments that represent positive change.

### Tags
#RebuildingHistory #Monuments #CulturalChange #SystemicRacism #Inclusivity


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about rebuilding history.
Because that's a major concern right now among some people.
That we are going to erase history by putting disclaimers in front of movies and not idolizing
certain songs and cartoons or taking down statues, any of this stuff.
It is going to erase history, therefore we will not benefit from the warnings of history,
so we have to rebuild it.
We need to figure out a way to appease those people who are concerned about this.
Because maybe it's a legitimate concern.
Maybe it is a legitimate concern.
See that, that's a piece of the Berlin Wall off my keychain.
When that wall came down it was history.
History in the making.
It was a symbol too.
It was something that showed the entire world that nothing, nothing, no wall, no concrete
and rebar, no set of laws, no unjust system, will be able to contain freedom.
Freedom will crash through anything eventually.
When these little chunks were spread all over the world, yeah I mean there's a little bit
of a warning in it, but mostly it was a celebration.
Mostly it was a celebration.
It was a way of saying, look, freedom will find a way.
I don't know that I ever really thought that we needed a warning to not militarize borders
and put up walls on borders.
I didn't think that was something that the species needed to be reminded of until the
Trump administration.
So maybe I was wrong about that.
Maybe we do need warnings.
Fair enough.
So let's figure out how to do it.
We take down a statue of a slave owner, right?
Well let's have a statue of an abolitionist put up.
Harriet Tubman, John Brown, somebody like that.
So not just does it serve as a warning, it inspires.
I think it's a great idea.
If the government takes down a statue of Jackson, well then let's replace it with a monument
to those lost on the Trail of Tears.
Because we need a warning.
Because Americans are apparently so lacking in morality that we need a monument, a statue,
to tell us that slavery and something called the Trail of Tears is bad.
Because we can't read that in a book.
We have to have a statue up.
See I imagine this is not appeasing those who say they want to preserve history because
they don't care about history.
They care about a myth.
They care about a national identity.
They care about the idea, preserving that concept that the United States is without
flaw, that our leaders are great.
We did some messed up stuff.
Our history is littered with mistakes and racism.
Fact.
That's history.
The monuments and all of this stuff, Gone with the Wind, it's a romanticization of history.
It's mythologizing it.
That's not actually history.
A disclaimer in front of Gone with the Wind that says, hey, this is romanticized.
It's not really historically accurate portrayal.
It's preserving history, my friends.
So maybe that's not what it's about.
Maybe it's not about preserving history.
It is about maintaining that myth.
But the funny thing about it is these people who are saying this.
It's you know, if we get rid of all of the stuff that has to do with racism in this country,
we won't have any history left.
And then in the next breath, they will say that systemic racism doesn't exist today.
You can choose one.
You can choose one of those.
You can't have them both.
If our entire history is littered with racism, is littered with oppression, is littered with
horrible things, the system that came out of that is going to have issues, deep systemic
issues, and they need to be addressed.
That's what all of this is part of.
If you don't like hearing about all of these statues and all of these examples of racism,
imagine if you had to go through your life and they were directed at you.
That would probably be pretty tiring.
It would probably make you feel like this isn't your country because this country doesn't
love you.
And watching as a bunch of Americans, leaders, defend the indefensible, that probably makes
that feeling a little bit more pronounced.
Because you know for a pretty large subset of the country, it's true.
It's true.
Yeah, we need to make changes, cultural changes, because you can change the law, that's fine.
But you have to change the way people think too.
And laws don't always do that.
Popular culture does.
Songs, cartoons, movies, statues.
Yeah we have to change it all.
We have to change it all.
I'll say it.
We have to change it all.
Too bad.
It's what's going to happen, just so you know.
This isn't a, you know, if this is going to occur, it's a matter of when.
Do you want to be on the right side of history or not?
Some of the stuff that occurred in this country was horrible.
It was horrible.
And continuing to mythologize it, romanticize it, downplay it, it's just going to lead to
more problems, more horrible things.
We have to address it.
We have to fix the door.
We have to fix the door.
You can be a part of that solution, or we're just going to wait you out.
The majority of Americans feel this way, and eventually you'll age out, you know.
So you can be that last generation that held on to a bunch of antiquated, out of date,
and wrong ideas, led by a guy who couldn't learn without a monument.
Or you can help the country move forward.
You can help create that country that fulfills the promises.
You want to make America great.
Remember the thing that made America the beacon of liberty, that place where everybody wanted
to go, is because they got to watch as that umbrella of freedom expanded.
It got bigger and bigger and bigger and included more people and genders and races.
That's what made America great.
Not bigotry, not keeping people down.
It was lifting people up.
That was the idea.
That's why America has that image.
Yeah, it's slow, it's painful, but we're almost done with the race here.
We don't have much further to go, and a whole bunch of people just want to quit.
Let's rebuild history.
Let's put up monuments to those who changed the country for the better, instead of those
who made it worse.
Let's put up monuments to those who make us proud, rather than those who embarrass us.
Because most of these people that we're fighting about right now, we're arguing over, and the
governments and the city council are deciding whether or not to take these statues down,
at the end of the day, they're embarrassments.
They're embarrassing to the country.
Take them down.
Put up somebody who makes us proud.
Make America great.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}