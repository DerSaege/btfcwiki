---
title: Let's talk about Splash Mountain and Princess and the Frog....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9G7qe-4ODI4) |
| Published | 2020/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Disney decided to rebrand Splash Mountain to focus on Princess and the Frog, sparking national debate.
- The outrage stems from right-wing sources prompting anger or deep-seated racism.
- The original theme of Splash Mountain was inspired by Song of the South, which had problematic imagery.
- The rebranding decision is not unusual, as companies frequently update their branding.
- Opposition to the change likely stems from discomfort with the removal of racist imagery.
- Beau questions why some people are upset over a theme park ride being updated to a more modern and inclusive theme.
- He challenges individuals to confront their reasons for resisting change, suggesting it may be rooted in upholding systemic racism.
- The debate over a theme park rebranding shows how absurd and divisive public discourse has become.
- Beau points out the absurdity of believing that children today prefer references to outdated and problematic content over more contemporary and inclusive themes.
- He stresses the importance of evolving as a society and addressing past mistakes to move forward.

### Quotes

- "You're the reason we can't move forward."
- "If you have an issue with this occurring at a children's park, you are the problem."
- "It's what we do. It's how we move forward."
- "Is there anybody who thinks the average child today going to Disney would rather have a ride reference Song of the South instead of Princess and the Frog?"
- "This is a private company doing what they think is best for their customers."

### Oneliner

Disney's decision to rebrand Splash Mountain sparks debate revealing deep-seated racism and resistance to change, challenging individuals to confront their role in upholding systemic issues.

### Audience

Theme park visitors

### On-the-ground actions from transcript

- Support and appreciate efforts by companies to update their branding to be more inclusive (exemplified).
- Challenge individuals who resist change and confront them about the root of their opposition (exemplified).
  
### Whats missing in summary

The full transcript provides a detailed analysis of the controversy surrounding Disney's decision to rebrand Splash Mountain, challenging individuals to confront their resistance to change and underlying motivations.

### Tags

#Disney #SplashMountain #Rebranding #SystemicRacism #Inclusivity


## Transcript
Well howdy there internet people, it's Beau again.
So, today we're gonna talk about Splash Mountain
because that's a debate in this country now.
If you don't know, Disney has decided to rebrand
Splash Mountain and make it about Princess and the Frog.
Okay, that has turned into a debate.
So to recap, a private company has decided to rebrand
their own property in a manner that makes it
reference something from this century,
increase customer satisfaction,
and make it more appealing to their guests.
That's what's happened.
It's just good business sense.
This is why Disney dominates the planet.
So somehow this turned into a national discussion.
Now, stick with me, I know there's people saying,
but wait, there was a reason.
There is.
The right wing outrage machine told a bunch of people
to be angry about it.
Now, the original inspiration theme of Splash Mountain
had to do with Song of the South,
and let's just say, yeah, it's got some issues.
Okay, great, but it was rebranded
to reference something modern,
reference something from today.
Companies rebrand all the time.
Daily, companies are rebranding stuff.
Did anybody care when McDonald's got rid of the clown
and went with the box with the little smiley face on it?
No, of course not.
But people care about this.
Why?
Why, why do you care specifically about this incident?
The answer may surprise you.
Either it's because you were told to be angry about it
and you want to play out your part in the culture war,
or you're a racist.
That's the only reason you would care.
Song of the South has some issues.
It's going away.
It's being rebranded.
The fact that companies rebrand their stuff all the time
is nothing new.
So it's not something being rebranded that bothers you.
It's the fact that something with some racist imagery
is going away.
Yeah, you want to uphold systemic racism.
You want to uphold those things that tell those other people,
y'all better stay in your place.
It's the only reason to be mad about this.
This is a private company doing what they feel
is best for their customers.
And you have an issue with it because, I don't know,
maybe you don't want those folk at Disney World.
Another subset of questions related to this
is where does it end?
It doesn't.
It doesn't end.
This will continue to go on forever.
It always has.
Society, humanity, we try to move forward.
Yes, every once in a while there comes
a group of people that claw at the ground,
and they have to be drug forward by their ankles
so we can move forward as a species.
That's where we're at right now.
Who do you want to be?
There are major political figures right now
upset over a theme park rebrand.
That should show you how ridiculous the conversation
has gotten in this country.
Is there anybody who thinks the average child today going
to Disney would rather have a ride reference
Song of the South instead of Princess and the Frog?
Anybody actually believe that?
No.
Nobody does.
Nobody genuinely believes that.
They're upset because they were either told to be upset
or they don't like the idea of getting rid of that imagery
because they want it surrounding everybody.
If you had an issue with this, you
are the problem with this country.
You're the reason we can't move forward.
You're the reason they can't just get over it.
It's a children's ride being rebranded to meet up
with a modern cartoon.
And somehow this is an affront to your sensibilities.
This doesn't end.
It continues.
Once we solve the race issue, we're
going to solve all kind of other problems.
We are going to continue as a species, as a society,
to clean up the messes that were before us.
All that stuff that got left behind,
we're going to try to fix it.
It's what we do.
It's how we move forward.
If you have an issue with this occurring at a children's park,
you are the problem.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}