---
title: Let's talk about which statue is next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6LjXcobjnpQ) |
| Published | 2020/06/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises the question of which statue is next, signaling a shift in perspective.
- Engages in a meaningful exchange with someone seeking to understand systemic issues.
- Dissects the reluctance to acknowledge systemic racism, using Teddy Roosevelt as an example.
- Points out the flaws of historical figures like Teddy Roosevelt while acknowledging their positive aspects.
- Emphasizes the need to confront the systemic issues represented by certain statues.
- Advocates for taking down statues that perpetuate institutional issues.
- Contrasts the historical context of figures like Teddy Roosevelt with modern values.
- Stresses the importance of continuous progress and growth as a society.
- Encourages focusing on individual achievements and embracing change rather than clinging to tradition.
- Reminds viewers of Teddy Roosevelt's stance on criticism and the importance of building a better future for the country.

### Quotes

- "Hidden in this question, which statue is next, is the admission of systemic racism."
- "Taking down these statues, it's not going to change the world. It's one more thing."
- "He was about change. He was about what you as a person did."
- "It's really important to help other people get up, not hold them down."
- "And strong men, tough men, men like Teddy Roosevelt, they weren't afraid of the competition."

### Oneliner

Beau delves into the systemic issues behind statue removals, using Teddy Roosevelt as a lens to challenge perspectives and advocate for progress.

### Audience

Viewers, Activists, Educators

### On-the-ground actions from transcript

- Examine statues in your community that may perpetuate institutional issues and advocate for their removal (implied).
- Engage in constructive dialogues about systemic racism and historical figures to foster understanding and progress (implied).

### Whats missing in summary

Emotional impact of confronting historical figures' flaws and advocating for societal progress through introspection and action.

### Tags

#SystemicIssues #StatueRemoval #TeddyRoosevelt #Progress #InstitutionalChange


## Transcript
Well howdy there internet people, it's Beau again.
Tonight we're going to talk about which statue is next.
It's a question we're hearing a lot right now,
said in a rhetorical sense.
And after tonight,
well, last night by the time you watch this,
it's a question I love. I want more people to ask me this question
because it leads to an incredibly revealing place
if we let it.
So tonight we're going to talk about statues and systemic issues.
For the last couple of weeks
I've had this back and forth going on with this guy
because he reached out
very sincerely
asked for some help in understanding some of the current issues and he's been
very genuine in this conversation.
He wants to grow.
However, he's got some baggage. He's got some things that he doesn't want to let
go of.
He doesn't want to acknowledge certain things. One thing in particular
he doesn't want to acknowledge because he knows that once he does
a whole bunch of his other talking points just crumble.
So he's done everything within his power
to deny that systemic racism exists.
Until tonight.
Until tonight.
He reaches out out of the blue
and he's like, how do you feel about them
taking down statues now that they're going after your boy?
Who's my boy?
Teddy Roosevelt.
I bet I know which statue it is.
Sure enough that's the one. For those that don't know the statue in front of
the museum
that's not a new debate.
That's been going on for years.
People who are
Teddy Roosevelt supporters want that thing gone.
It is not just a statue of Teddy Roosevelt.
There are two other people in it and if you look at that statue for any length of
time
you'll understand why people want it gone.
Now I happen to be
somebody who finds a lot of the things that
Teddy Roosevelt did admirable.
I think there's a lot of inspiration that can be pulled from his life. I've
talked about it on this channel before.
Now before anybody starts writing all of the horrible stuff
that relates to Teddy Roosevelt, I know.
He was a person.
He was a historical figure. He was an actual human being. He wasn't infallible.
He wasn't flawless. He made mistakes and he believed some messed up stuff.
Yes,
he was a racist. Period.
But there are some positive things
that can be taken from him
and he did accomplish
a lot of good.
I explained to him that Teddy probably would have been cool with the statue
being taken down.
And I have to break it down and I'm like, look,
this is a guy who, one of the bad things about him, was that he definitely
understood sacrificing things or people
in pursuit of a wider campaign.
And if that campaign
had a large component
that was about
battling police corruption,
oh, he would have been down for that.
I mean, Teddy would be masking up right now.
That was kind of his whole thing when he started.
He was a police commissioner
who worked
to weed out corruption in New York.
He literally
fought corrupt cops.
So I think he would have been alright with it. At this moment in history,
it coming down as part of this,
I think he would have been okay with it.
And he's like, I don't understand that. You like the guy.
I like some things about him, but
either way, me liking him has nothing to do with whether or not he's a racist.
I mean, that's kind of a weird measure to use right there.
And he's like, well, I don't get it. You said he did all these good things and
these progressive things that he pushed.
Yeah, all that's true.
That doesn't change the fact that he was a racist, though.
He's like, I don't get it. If you use that metric,
if you use that standard,
we won't have any
leaders left. We won't have any statues left.
Go on.
And he goes on to tell me that, like, okay, fine, not all of them, but like 95%
of them were racist to some degree.
That's kind of the point.
Hidden in this
question,
which statue is next,
is the admission
of systemic racism.
They're saying,
well, we'd have a hard time coming up with a statue of somebody in this
country that wasn't racist.
That's a pretty big indictment of the country right there.
And if 95%,
90%, 80%
of them were,
what does that say about the society
they helped create, the system they put in place, the laws they wrote?
It permeates the whole culture.
It's everywhere.
And in that question,
well, which statue is next? You could do it to any of them.
Is it admission
that it is systemic, that it is institutionalized?
I love this question now.
Whenever that comes up, that's where the conversation needs to go.
Because
with this guy, who I've been talking to for weeks about this,
he wasn't budging
until tonight. I actually heard him.
Oh,
yeah, yeah.
It's really simple.
Look, taking down these statues,
it's not going to change the world.
It's not going to end racism tomorrow
any more than the Civil War or the Civil Rights Act did.
But it's one more thing.
It's one more thing.
And do we have to take them all down? No.
We don't.
But maybe take down the ones
like that one
that definitely highlight
that institutional issue.
Those should probably be at the top of the list.
Now,
to those people
who all of a sudden
are super interested
in defending Teddy Roosevelt's statue,
I got a couple things I want to point out.
First is that I am
pretty sure he would be okay with it going right now.
The second is he would not have liked you.
Teddy was a person who was all about individual achievement,
all about what you did.
He was not somebody
who was like, oh,
we have to keep the statue because of tradition,
or America's great because of what my daddy did. He would not have cared.
He wouldn't have liked you at all.
He was about change. He was about
what you
as a person did.
If you're clinging to the past
and you're wanting to get your sense of pride from yesterday,
he wouldn't have wanted to be around you.
I'm certain of that.
I would also point out
one of his more famous quotes says, to announce that there must be no
criticism of the president
is morally treasonable
to the American public.
He would have been fine
with people pointing out
his issues.
He would have been okay with it
because he was about the country growing.
He didn't want to live in the past. He wanted to build a future.
I find it hilarious
that this is the one
that people are really mad about.
The one guy
who probably would have taken him into a boxing ring
because that's what he did.
If you want to learn something from Teddy,
understand that one of the most important lessons that man's life can
teach you
is that it's really important
to help other people get up,
not hold them down.
Because if they get up,
they may be better than you.
And you have to get better.
And strong men,
tough men,
men like Teddy Roosevelt,
they weren't afraid of the competition.
Anyway,
it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}