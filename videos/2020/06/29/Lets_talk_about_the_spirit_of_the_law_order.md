---
title: Let's talk about the spirit of the law & order....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XM5FN1mN-qQ) |
| Published | 2020/06/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A recent poll reveals that only 9% of Americans believe law enforcement does not need reform, with a majority of 55% calling for major reform.
- Police accountability activists have successfully garnered support from an overwhelming majority of people.
- Beau points out that while many police accountability proposals are good, they may miss the importance of enforcing the spirit of the law.
- Beau shares a personal story about a friend from New York experiencing the unique law enforcement culture in the South.
- He delves into the concept of the "spirit of the law" and how it can lead to more just law enforcement and better community outcomes.
- Using examples like fireworks laws and stand your ground laws, Beau illustrates the difference between enforcing the spirit versus the letter of the law.
- In rural areas, law enforcement officers may adhere more to the spirit of the law due to the accountability enforced by elected sheriffs.
- Elected law enforcement officials are more likely to focus on violent and property crimes to secure re-election, fostering community policing.
- Beau advocates for top law enforcement officers in every agency to be elected and have absolute power over hiring and firing, eliminating the influence of unions.
- He stresses that prioritizing the community's desires for peace and order can lead to effective and just law enforcement practices.

### Quotes

- "An overwhelming majority of people are behind you."
- "The spirit of the law will provide order without providing injustice."
- "They exercise the spirit of the law."

### Oneliner

Beau reveals the overwhelming support for police reform, advocates for enforcing the spirit of the law, and proposes elected top law enforcement officers for community-oriented policing.

### Audience

Law Enforcement Reform Advocates

### On-the-ground actions from transcript

- Elect top law enforcement officers for community policing (suggested)
- Advocate for absolute power over hiring and firing for elected law enforcement officials (suggested)
- Prioritize violent and property crimes as elected officials to satisfy community needs (exemplified)

### Whats missing in summary

The full transcript provides deeper insights into the importance of enforcing the "spirit of the law" and advocating for elected law enforcement officials to prioritize community needs for effective policing.

### Tags

#PoliceReform #SpiritOfTheLaw #CommunityPolicing #ElectedOfficials #LawEnforcement


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the spirit of the law
and order and my friend from New York.
So a very interesting poll came out
and it proved without a doubt
that running on a law and order platform,
a platform that says law enforcement doesn't need reform
is a losing platform.
The poll was by YouGov
and the percentage of Americans that believe
law enforcement is good to go as is,
does not need reform is nine.
Nine percent, single digits.
Everybody else believes that reform is necessary.
55% of Americans believe that major reform is necessary.
That's huge.
That is huge.
So if you are a police accountability activist,
you've been involved with these campaigns,
take a bow, you have won hearts and minds.
The people are behind you.
An overwhelming majority of people are behind you.
Now I have read most of the proposals,
most of the things that police accountability activists want.
They're good, they're good,
but there's something that I think's missing
for most of them and I think it's really important.
Years ago, I had a friend come down from New York.
Now the only time he had been in the South prior to this
was a brief stopover at Fort Benning, Georgia.
So he gets down here, he's down here a few days
and he's experiencing the local customs.
And after a few days, we go to this gas station
and this is a rural Southern gas station.
So it's a gas station, a diner, a grocery store,
a butcher, a auto parts store, all in one spot
because it's the only thing for like 30 miles.
He was amazed that you could buy ammo at a gas station.
We get something to eat and then we go outside
and we're sitting there around the picnic tables
like King of the Hill and a cop pulls up.
Cop goes inside and grabs something to eat too.
My buddy looks in his car, just glances over
and he's like, are they all armed like that?
I'm like, yeah, pretty much.
And inside his car is an M4, a Mosse, sorry,
a compact AR-15, a shotgun and a long range rifle.
And this is all in addition to a sidearm.
And my buddy's like, why?
And kind of motions around because it's Mayberry, you know?
Like, cause that's the only cop for 20 miles.
He's on his own.
And the guy's like, got it.
You know, if you're 15 minutes away from backup,
you want to be well armed.
Generally speaking, rural cops are better armed
than those in major cities.
Cop comes out and because of the amount of firepower
in the car, my buddy's kind of like,
kind of keeping his distance
and the guy starts talking to him.
And after a few minutes, the cop leaves and he's like,
man, that guy was really nice.
And I'm like, yeah, he's gotta be.
And he's like, I do not understand.
Y'all are way more wild than we are up there.
Why is law enforcement like this?
Because they exercise the spirit of the law.
And I've talked about this in other videos,
but haven't really gone deep into it.
Assuming we're in a, at least semi-just society,
a law has an intent.
It has an intent.
It's designed to do something.
It's designed, hopefully, to protect people.
That's the idea behind it.
Most of the reforms that are being suggested
are gonna take care of a lot of the problems
that already exist, but you're always going to have
some kind of laws in this system.
If those laws are enforced by the spirit of the law,
rather than the letter of the law,
you will have more just law enforcement.
You will have a better community,
following the legislative intent.
I'm gonna give you two examples.
One's a minor example.
One's a major one.
A minor one would be, in a lot of states, fireworks.
If they fly, they're banned.
Why?
Because they can start a fire, right?
Goes without saying.
So around here, a bunch of North Florida boys
go up to Alabama, buy some stuff
they're not supposed to have, bring it back down,
and do a bunch of North Florida boy stuff
on Fourth of July.
If it has been raining the week before,
cops are not gonna care.
Spirit of the law, it was to stop fires.
That's the reason.
If it is in the middle of a drought,
they're gonna pull up, and at first,
they're gonna be like, what are you doing?
And basically lecture them and educate them,
then they'll drive off.
If they keep doing it, they'll come back
and write them a ticket.
That's how it works out here.
A more prominent example is stand your ground.
That's a pretty controversial law.
It shouldn't be.
The legislative intent there is actually really good.
We've talked about it.
Fight, flight, or freeze.
Prior to this, a lot of laws required somebody
to try to get away.
You may not be able to overcome a biological response.
If you're response is to fight,
you may end up getting in trouble,
even though you were the victim.
That's what it was designed to fix.
It was never intended so you could go out
and start something and then escalate it.
That's the letter of the law.
Enforcing the spirit of the law
creates community policing.
So why do cops do it in rural areas
and not in major cities?
Because their boss is elected.
They don't have a choice.
The reason cops out here don't have the same attitude
as NYPD is because if they do,
somebody's going to call the sheriff
and they're going to get fired.
They can't act like that because the sheriff
doesn't want her officer representing her
like that to the voters.
That'll make sure she doesn't get reelected.
Elected law enforcement,
the top law enforcement officer being elected,
forces departments to engage in community policing.
They have to know what the community wants.
And it's really simple.
Most elected law enforcement agencies,
they focus on violent and property crime,
period, full stop.
That's what they care about.
Why?
Because that's what's going to get them reelected.
They don't care about the trivial stuff.
One, it angers voters
because everybody they arrest has five cousins.
And two, the more important crimes
are the ones that get the most coverage.
Those are the ones that upset the community the most.
They're voters.
If they are accountable because they are elected,
they're more likely to engage in community policing.
I have not seen that in most proposals.
The top law enforcement officer
in every agency needs to be elected.
And they need to have absolute power
over hiring and firing.
No union.
It does not matter.
That would solve a whole lot of problems real fast
because officers, street cops,
will change the way they do things
or they will no longer be employed
because the politician cop,
the one that's running for office,
they're going to want to get reelected.
And in order to do that,
they have to satisfy the voters.
Quotas for arrests and tickets,
none of that stuff matters.
What matters is keeping the voters happy.
And what do the voters want?
Peace, period.
They want order, right?
The spirit of the law will provide order
without providing injustice.
So that needs to be something
that gets added to these platforms, my opinion.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}